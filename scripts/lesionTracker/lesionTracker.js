/**
 * Javascript for Lesion Tracker
 */

var jq, $body, $wrapper, $collection, $notes, $template;

if (jQuery) jq = jQuery ;
//else {if (console.log) console.log('We need jQuery!')}

$.ajaxSetup({async:true});

var scripts_dir = serverRoot+'/scripts' ;
//var scripts_dir = '.' ;

// make sure XNAT and XNAT.app objects exist
if (typeof XNAT == 'undefined') var XNAT={};
if (typeof XNAT.app == 'undefined') XNAT.app={};
if (typeof XNAT.utils == 'undefined') XNAT.utils={};
//var CONDR ;
if (typeof CONDR == 'undefined') var CONDR={};
if (typeof CONDR.lesionTracker == 'undefined') CONDR.lesionTracker={};



//////////////////////////////////////////////////
// GENERAL UTILITY FUNCTIONS
//////////////////////////////////////////////////

// utility for getting URL query string value
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
    var results = regex.exec(location.search);
    return (results == null) ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

// feed in a _source string (could be HTML), replace with values from _replacements object
function templateStringReplace(_source,_replacements){
    var the_string = _source.toString();
    // iterate over _replacements object x=key, y=value
    $.each(_replacements,function(x,y){
        var _x = new RegExp(x,'g');
        //var replace_with = y;
        the_string = the_string.replace(_x,y);
        //if (console.log) console.log('replace this: '+ x +' with this: '+ y +'; ');
    });
    return the_string ;
}

// apply value from __source to __target (jQuery objects)
function applyValue(__source,__target){
    var source_val = __source.val();
    __target.attr('data-value',source_val).val(source_val);
}



//////////////////////////////////////////////////
// XNAT UTILITY FUNCTIONS
//////////////////////////////////////////////////

// fetch the existing records
// if '_array' paramater is passed (with an existing array)
// the ResultSet.Result array will be appended to it
// no duplicate check is done
XNAT.app.ResultSetResultArray = function(_url,_array,_async,_sort_by,_callbacks){

    _url = _url || '';
    _array = _array || [];
    _async = _async || false ;
    _sort_by = _sort_by || null ;

    // if the _async parameter is either 'async' or true, make it async, otherwise not
    var async_ = (_async == 'async');

    if (_url > ''){
        var doAJAX = $.ajax({
            type: 'GET',
            url: _url,
            dataType: 'json',
            async: async_,
            //async: false,
            success: function(json){
                $.each(json.ResultSet.Result, function(){
                    _array.push(this);
                });
            }
        });
        // callbacks with returned ajax data
        if (typeof _callbacks == 'object'){
            if (typeof _callbacks.done == 'function'){
                doAJAX.done(function(data,textStatus,jqXHR){
                    _callbacks.done(data,textStatus,jqXHR);
                });
            }
            if (typeof _callbacks.fail == 'function'){
                doAJAX.fail(function(data,textStatus,error){
                    _callbacks.fail(data,textStatus,error);
                });
            }
            if (typeof _callbacks.always == 'function'){
                doAJAX.always(function(data_or_jqXHR,textStatus,jqXHR_or_error){
                    _callbacks.always(data_or_jqXHR,textStatus,jqXHR_or_error);
                });
            }
        }
        // sort the _array
        if (_sort_by !== null && _sort_by > ''){
            _array.sort(function(a,b){
                if (a[_sort_by] > b[_sort_by]){
                    return 1;
                }
                if (a[_sort_by] < b[_sort_by]){
                    return -1;
                }
                return 0;
            });
        }
    }

    // we can do something with the data above, or return the array here
    return _array ;
};




// get the data returned in an 'items' JSON object
XNAT.app.itemData = function(_url,_async,_callbacks){

    _url = _url || '';
    _async = (_async && _async !== '' && _async !== null) ? _async : false ;

    var data_obj={}, children=[], meta_obj={};

    // if the _async parameter is either 'async' or true, make (async_ == true), otherwise not
    var async_ = (_async == 'async');

    if (_url > ''){
        var doAJAX = $.ajax({
            type: 'GET',
            url: _url,
            dataType: 'json',
            async: async_,
            success: function(json){
                $.each(json.items, function(){
                    var _items = this ;
                    if (_items.meta.isHistory === false){ // make sure it's the latest record
                        data_obj = _items.data_fields ;
                        children = _items.children ;
                        meta_obj = _items.meta ;
                    }
                });
            }
        });
        // callbacks with returned ajax data
        if (typeof _callbacks == 'object'){
            if (typeof _callbacks.handleDone == 'function'){
                doAJAX.done(function(data,textStatus,jqXHR){
                    _callbacks.handleDone(data,textStatus,jqXHR);
                });
            }
            if (typeof _callbacks.handleFail == 'function'){
                doAJAX.fail(function(data,textStatus,error){
                    _callbacks.handleFail(data,textStatus,error);
                });
            }
            if (typeof _callbacks.handleAlways == 'function'){
                doAJAX.always(function(data_or_jqXHR,textStatus,jqXHR_or_error){
                    _callbacks.handleAlways(data_or_jqXHR,textStatus,jqXHR_or_error);
                });
            }
        }
    }
    XNAT.data.itemDataFields = data_obj ;
    XNAT.data.itemMeta = meta_obj ;
    return { data: data_obj , meta: meta_obj } ;
};





//////////////////////////////////////////////////
// SETUP THE LESION TRACKER(TM)
//////////////////////////////////////////////////
CONDR.lesionTracker.recordCount = 0 ;
CONDR.lesionTracker.collURI = serverRoot +
    '/data' +
    //'/projects/' + XNAT.data.context.projectID +
    //'/subjects/' + XNAT.data.context.subjectID +
    '/experiments' +
    '?condr_mets:lesionData/saCollID=' +
        '{{LESION_COLLECTION_ID}}' + // replace this with actual collection ID
    '&xsiType=condr_mets:lesionData' +
    '&columns=' +
        'label,' +
        'insert_date,' +
        'condr_mets:lesionData/hemisphere,' +
        'condr_mets:lesionData/location,' +
        'condr_mets:lesionData/dateOfDiagnosis,' +
        'condr_mets:lesionData/description,' +
        'condr_mets:lesionData/saCollID,' +
        'condr_mets:lesionData/saLinkID,' +
        'condr_mets:lesionData/nodeNum' +
    '&format=json';

CONDR.lesionTracker.changedForm = false ; // has the form been changed? (not currently using this)
CONDR.lesionTracker.completedForm = false ; // is the form complete?
CONDR.lesionTracker.newRecords = []; // what records have we created at this time?
CONDR.lesionTracker.editedRecords = []; // which records have beed edited?


//////////////////////////////////////////////////
// GET THE COLLECTION
//////////////////////////////////////////////////
CONDR.lesionTracker.collURI = CONDR.lesionTracker.collURI.replace(/{{LESION_COLLECTION_ID}}/, CONDR.lesionTracker.collID);
CONDR.lesionTracker.collection = new XNAT.app.ResultSetResultArray(CONDR.lesionTracker.collURI, [], false);





//////////////////////////////////////////////////
// GET THE NOTES
//////////////////////////////////////////////////
CONDR.lesionTracker.getCollNotes = function(_id){

    _id = _id || CONDR.lesionTracker.collID ;

    CONDR.lesionTracker.collGetJSON = serverRoot + '/data/experiments/'+ _id +'?format=json';
    CONDR.lesionTracker.collJSON = XNAT.app.itemData( CONDR.lesionTracker.collGetJSON , false );

    if (typeof CONDR.lesionTracker.collJSON.data.note != 'undefined'){
        CONDR.lesionTracker.collNotes = CONDR.lesionTracker.collJSON.data.note ;
    }
    else {
        CONDR.lesionTracker.collNotes = false ;
    }
};
CONDR.lesionTracker.getCollNotes();



//////////////////////////////////////////////////
// RENDER THE COLLECTION
//////////////////////////////////////////////////
// renderRecords takes an array of the lesion records and spits them into
// a table inside the $container element (a jQuery DOM object)
CONDR.lesionTracker.renderRecords = function(_array,$container){

    // 1) the _array param comes in from AJAX
    // 2) (re?)render the stuff

    // setting these strings as vars here for easy future modification
    var hemi_val    = 'condr_mets:lesiondata/hemisphere';
    var hemi_disp   = 'condr_mets:lesiondata/hemisphere'; // do we want a different 'formatted' version for the display?
    var loc_val     = 'condr_mets:lesiondata/location';
    var loc_disp    = 'condr_mets:lesiondata/location'; // do we want a different 'formatted' version for the display?
    var date_value  = 'condr_mets:lesiondata/dateofdiagnosis';
    var description = 'condr_mets:lesiondata/description';
    var lesion_num  = 'condr_mets:lesiondata/nodenum';
    var id          = 'condr_mets:lesiondata/id';
    var label       = 'label';
    var uri         = 'URI';
    var insert      = 'insert_date';

    var records_html = '\n' +
        '<table id="lesion_records" ' +
        //'cellpadding="0" ' +
        //'cellspacing="0" ' +
        'data-subject="">' + '\n' +
        '   <tr>' + '\n' +
        '       <th class="underscore">#</th>' + '\n' +
        '       <th class="underscore">Hemisphere</th>' + '\n' +
        '       <th class="underscore">Location</th>' + '\n' +
        '       <th class="underscore">Date of Diagnosis</th>' + '\n' +
        '       <th class="underscore">Lesion Description</th>' + '\n' +
        '       <th class="underscore">&nbsp;</th>' + '\n' +
        '   </tr>' +
        '' ;

    var orphan='', orphans=[], _count=0 ;

    if ($.isArray(_array) && _array.length){

        _array.sort(function(a,b){
            return a[lesion_num] - b[lesion_num];
        });

        $.each(_array,function(){

            //if (console.log) console.log('lesion #'+this[lesion_num]);

            if (this[lesion_num]+'' != '0'){

                _count++ ;

                var date={}, lesion={};

                // date stuff
                date.val = this[date_value];
                // we expect to receive the date in ISO format yyyy-mm-dd
                date.arr = (date.val > '') ? date.val.split('-') : null ;
                if (date.arr !== null){
                    date.yyyy = date.arr[0] || '0000';
                    date.mm   = date.arr[1] || '00';
                    date.dd   = date.arr[2] || '00';
                    //var date_display = date_mm +'/'+ date_dd +'/'+ date_yyyy ;  // 09/01/2013
                    date.display = date.yyyy +'-'+ date.mm +'-'+ date.dd ;  // 2013-09-01
                }
                else {
                    date.display = '&mdash;';
                }

                if (this[lesion_num] > ''){
                    _count = lesion.count = this[lesion_num];
                }
                else {
                    lesion.count = _count+'?' ;
                }

                records_html += '\n' +
                    '<tr class="record" id="'+ this[id] +'" title="'+ this[label] +'">' + '\n' +
                    '   <td class="lesion" data-nodenum="'+ this[lesion_num] +'" data-count="'+ lesion.count +'">'+ /* this[lesion_num] */ lesion.count +'</td>' + '\n' +
                    '   <td class="hemisphere" data-val="'+ this[hemi_val] +'">'+ this[hemi_disp] +'</td>' + '\n' +
                    '   <td class="location" data-val="'+ this[loc_val] +'">'+ this[loc_disp] +'</td>' + '\n' +
                    '   <td class="date" data-val="'+ date.val +'" data-year="'+ date.yyyy +'" data-month="'+ date.mm +'" data-day="'+ date.dd +'" style="font-family:Courier,monospace">'+ date.display +'</td>' + '\n' +
                    '   <td class="description">'+ this[description] +'</td>' + '\n' +
//                    '       <table class="no_border" style="width:100%;">' + '\n' +
//                    '           <tr>' + '\n' +
//                    '               <td align="left" valign="top" style="padding:0;border:none;text-align:left">'+ this[description] +'</td>' + '\n' +
//                    '               <td align="right" valign="top" style="padding:0;white-space:nowrap;border:none;text-align:right;">' + '\n' +
//                    '                   <a class="edit btn nolink" href="'+ this[uri] +'">edit record</a>' + '\n' +
//                    '               </td>' + '\n' +
//                    '           </tr>' + '\n' +
//                    '       </table>' + '\n' +
//                '       <p style="display:inline-block;width:70%;margin:0;float:left;">'+ this[description] +'</p>' + '\n' +
//                '       <a class="edit btn nolink" href="'+ this[uri] +'" style="display:inline-block;width:20%;float:right;text-align:right;white-space:nowrap;">edit record</a>' + '\n' +
                    '';

                records_html += '<td>';

                if (XNAT.app.user.canEdit){
                    records_html += '<a class="edit btn nolink" href="'+ this[uri] +'">edit</a>';
                }

                records_html += '' +
                    '   </td>' + '\n' +
                    '</tr>';

            }
            else {
                // grab the first orphan
                if (orphan === ''){
                    CONDR.lesionTracker.orphan = orphan = this[id];
                }
                // keep a list of all the orphans
                orphans.push(this[id]);
                //has_records = false ;
            }
        });

        CONDR.lesionTracker.orphans = orphans ; // array of orphans (with nodenum:0)

        if (_count === 0){
            records_html += '\n' +
                '<tr class="no_records">' + '\n' +
                '   <td colspan="5" style="text-align:center;"><p style="margin:20px auto;">(no records)</p></td>' +
                '</tr>' + '\n' +
                '';
        }
    }
    else {
        records_html += '\n' +
            '<tr class="no_records">' + '\n' +
            '   <td colspan="5" style="text-align:center;"><p style="margin:20px auto;">(no records)</p></td>' +
            '</tr>' + '\n' +
            '';
    }

    records_html += '</table>';

    $container.html(records_html);

    // style the new and edited records
    // have to do this every time the list is rendered
    if (CONDR.lesionTracker.newRecords.length > 0){
        (function(){
            var records = '#'+CONDR.lesionTracker.newRecords.join('.record, #');
            $(records).css('color','#180');
        })();
    }

    CONDR.lesionTracker.recordCount = parseInt(_count);

};





//////////////////////////////////////////////////
// CUSTOM FILE MANAGER FOR LESION
//////////////////////////////////////////////////
//CONDR.lesionTracker.fileManager = function(_lesion){
//
//    // Pass the session ID to the function
//    this.openFileManager = function(sessionID) {
//
//        // Create a new object representing the session. Must include the sessions REST URL
//        var obj=new Object();
//        obj.uri=serverRoot + "/REST/experiments/" + sessionID;
//        obj.refresh = serverRoot + "/data/services/refresh/catalog?options=" + encodeURIComponent("populateStats,append,delete,checksum") + "&XNAT_CSRF=" + csrfToken + "&resource=/archive/experiments/" + sessionID;
//        obj.objectType = "experiment";
//        obj.objectId = sessionID;
//        obj.canEdit=true;
//        obj.canDelete=true;
//        obj.catalogs={};
//        obj.catalogs.ids=[];
//
//        // Create a new FileViewer object and pass along out session object
//        var lesionFileViewer = new FileViewer(obj);
//        // Open the File Viewer modal
//        lesionFileViewer.render();
//    };
//
//    YAHOO.util.Connect.asyncRequest('POST',serverRoot+"/REST/experiments/"+ this.expt_id +"/resources/"+ this.resource_label +"/files?XNAT_CSRF=" + csrfToken + "&"+params,callback);
//
//    var obj = {};
//    obj.uri = serverRoot + '/data/experiments/' + _lesion;
//    obj.refresh = serverRoot + "/data/services/refresh/catalog?options=" + encodeURIComponent("populateStats,append,delete,checksum") + "&XNAT_CSRF=" + csrfToken + "&resource=/archive/experiments/" + _lesion;
//    obj.canEdit = true;
//    obj.objectId = _lesion;
//
//    var lesionFileViewer = new FileViewer(obj);
//    lesionFileViewer.render();
//
//};
//// Pass the session ID to the function
//function openFileManager(sessionID) {
//// Create a new object representing the session. Must include the session’s REST URL
//    var obj={};
//    obj.uri=serverRoot + "/REST/experiments/" + sessionID;
//
//// Create a new FileViewer object and pass along out session object
//    var myFileViewer = new FileViewer(obj);
//// Open the File Viewer modal
//    myFileViewer.render();
//}





// general default setter
// feed it an object with property names and default values
function defaultSetter(_props,_global,_data){
    if (_props > {}){
        _data = _data || {}; // existing data object - optional
        _props = _props || {}; // property names and default values
        _global = _global || {}; // optional global var if we want to attach values to that
        $.each(_props, function(_prop,_default){
            _global[_prop] = _data[_prop] = (_data && _data[_prop]) ? _data[_prop] : _default ;
        });
    }
    return _data ;
}






//////////////////////////////////////////////////
// MODIFY MODAL CONTENT BEFORE DISPLAY
//////////////////////////////////////////////////
CONDR.lesionTracker.modalContent = function($template,_data){
    // if we already have some data,
    // start with that, otherwise: new Object!
    var data = _data || {};

    var props = {
        collID: CONDR.lesionTracker.collID,
        lesionID: '',
        nodeNum: '0',
        subjLabel: '',
        subjID: '',
        count: '0',
        hemisphere: '',
        location: '',
        description: '',
        default_date: XNAT.data.todaysDate.ISO,
        date: ''
    };

    $.each(props, function(_prop,_default){
        CONDR.lesionTracker[_prop] = data[_prop] = (data && data[_prop]) ? data[_prop] : _default ;
    });

//    $.extend(true,CONDR.lesionTracker=data,props);

    // the above code does the same as the code below (is it better or worse?)

//    data.collID      = (_data && _data.collID)      ? _data.collID      : CONDR.lesionTracker.collID ;
//    data.lesionID    = (_data && _data.lesionID)    ? _data.lesionID    : '' ;
//    data.nodeNum     = (_data && _data.nodeNum)     ? _data.nodeNum     : '0' ;
//    data.subjLabel   = (_data && _data.subjLabel)   ? _data.subjLabel   : '' ;
//    data.subjID      = (_data && _data.subjID)      ? _data.subjID      : '' ;
//    data.count       = (_data && _data.count)       ? _data.count       : '0' ;
//    data.hemisphere  = (_data && _data.hemisphere)  ? _data.hemisphere  : '' ;
//    data.location    = (_data && _data.location)    ? _data.location    : '' ;
//    data.description = (_data && _data.description) ? _data.description : '' ;

    $template.find('input:hidden.subject_label').val(data.subjLabel);
    $template.find('input:hidden.subject_id').val(data.subjID);
    $template.find('input:hidden.lesion_id').val(data.lesionID);
    $template.find('input:hidden.collection_id').val(data.collID);
    //$template.find('[value="{{TODAY_ISO}}"]').val(XNAT.data.todaysDate.ISO);
    $template.find('input:hidden.diagnosis.date').val(data.date);

    var source_html = $template.html();

    var new_html = source_html.
        replace(/{{COLLECTION_ID}}/g, data.collID).
        replace(/{{LESION_ID}}/g, data.lesionID).
        replace(/{{NODE}}/g, data.nodeNum).
        replace(/{{SUBJECT_LABEL}}/g, data.subjLabel).
        replace(/{{SUBJECT_ID}}/g, data.subjID).
        replace(/{{COUNT}}/g, data.count).
        replace(/{{HEMISPHERE}}/g, data.hemisphere).
        replace(/{{LOCATION}}/g, data.location).
        replace(/{{DEFAULT_DATE}}/g, data.default_date).
        replace(/{{DATE}}/g, data.date).
        replace(/{{DESCRIPTION}}/g, data.description);

    $template.html(new_html);

    var $hemisphere = $template.find('select[name="hemisphere"]');
    if (data.hemisphere > ''){
        //$hemisphere.val(data.hemisphere);
        $hemisphere.find('option[value="' + data.hemisphere + '"]').prop('selected',true);
        //$hemisphere.change();
    }

    var $location = $template.find('select[name="location"]');
    if (data.location > ''){
        //$location.val(data.location);
        $location.find('option[value="' + data.location + '"]').prop('selected',true);
        //$location.change();
    }

    var $date = $template.find('input[name="date_of_diagnosis"]');
    $date.val(data.date);

    var data_out = $.each(data,function(x,y){
        return 'property: ' + x + ', value: ' + y + '\n' ;
    });

    //if (console.log) console.log(data_out);

    return data ;

};





//////////////////////////////////////////////////
// OPEN MODAL
//////////////////////////////////////////////////
// a little setup
CONDR.lesionTracker.modalCount = 0;
CONDR.lesionTracker.modalOpts = {};
//
CONDR.lesionTracker.addRecord = function($template,_content,_opts){
    // modalCount makes sure our modal has a unique id every time
    // and that our openModal method knows what it is
    // so we can assign new 'ok' and 'cancel' functions
    CONDR.lesionTracker.modalCount++;
    var new_record_count = CONDR.lesionTracker.recordCount+1 ;
    _opts = _opts || {} ;
    var __template = $template ;
    var modal_opts = CONDR.lesionTracker.modalOpts || {} ;
    var modal_id = modal_opts.id = modal_opts.id || 'lesion_modal'+CONDR.lesionTracker.modalCount;
    modal_opts.title = 'Create New Lesion Record';
    modal_opts.closeBtn = 'hide';
    modal_opts.esc = false;
    //modal_opts.enter = false;
    modal_opts.content = '';
    modal_opts.content = _content || __template.html();
    modal_opts.footerContent = '<a href="#" class="cancel_record btn">Cancel</a>';

    modal_opts.beforeShow = function(obj){
        var $this_modal = obj.$modal;
        //$this_modal.drags({handle:'.title'});
        var $form = $this_modal.find('form.lesion');
//        $fieldset.attr('data-collection',CONDR.lesionTracker.collID);
//        $fieldset.attr('data-subject-id',CONDR.lesionTracker.subjID);
//        $fieldset.attr('data-lesion',new_record_count);
//        $fieldset.attr('data-count',new_record_count);
        $form.find('h4 > b').append(new_record_count + ' - new');
        var replacements = {
            lesionID: _opts.lesion_id
        };
        CONDR.lesionTracker.modalContent($this_modal,replacements);
        var $date_span = $this_modal.find('.datepicker');
//        $date_span.each(function(){XNAT.app.datePicker.init($(this))});
//        XNAT.app.datePicker.init('#'+modal_id+' .datepicker');
        XNAT.app.datePicker.init($date_span);
        var $date_input_us = $this_modal.find('input.date.single.us');
        $date_input_us.mask("99/99/9999",{placeholder:" "});
        var $date_input_iso = $this_modal.find('input.date.single.iso');
        $date_input_iso.mask("9999-99-99",{placeholder:" "});
        // maybe we should use this jQuery date picker?
        //$date_input.Zebra_DatePicker();
        //$date_input.val('2012-12-12');
    };

    modal_opts.buttons={};
    modal_opts.buttons.save = {
        label: 'Save and Close',
        close: false,
        className: 'ok',
        isDefault: true,
        action: function(modal) {
            var $modal = $('#' + modal.id);
            CONDR.lesionTracker.saveRecord(
                $modal.find('fieldset'), // container with our data
                null,                             // any data we want to send
                function () {                       // callback function after saving
                    xmodal.close($modal)
                }
            )
        }
    };
    modal_opts.buttons.more = {
        label: 'Record Another Lesion',
        close: false,
        className: 'cancel',
        action: function(modal) {
            var $modal = $('#' + modal.id);
            CONDR.lesionTracker.saveRecord(
                $modal.find('fieldset'),
                null,
                function () {
                    xmodal.close($modal);
                    CONDR.lesionTracker.addRecord($template);
                }
            )
        }
    };

    xmodal.open(modal_opts);
};





//////////////////////////////////////////////////
// SAVE LESION RECORD
//////////////////////////////////////////////////
CONDR.lesionTracker.saveRecord = function ($fieldset, _data, _afterSave) {

    if (CONDR.lesionTracker.formCompleted($fieldset)) {

        //$('#loading').show();
        xmodal.loading.open('Saving...');

        _data = (_data && _data !== null) ? _data : {} ;
        _afterSave = _afterSave || function () {};

        CONDR.lesionTracker.recordCount++;

        var count, subject_label, subject_id, lesion_id, collection_id, hemisphere = {}, location = {}, date = {}, description;

        var $select_hemisphere = $fieldset.find('[name="hemisphere"]');
        hemisphere.val = $select_hemisphere.val();
        hemisphere.label = (hemisphere.val > '') ? $select_hemisphere.find('option:selected').text() : '&mdash;';

        var $select_location = $fieldset.find('[name="location"]');
        location.val = $select_location.val();
        location.label = (location.val > '') ? $select_location.find('option:selected').text() : '&mdash;';

        var $datepicker = $fieldset.find('.datepicker');
        var $date, year, month, day ;
        // if using a single input for the date - use 'single' class
        if ($datepicker.hasClass('single')){
            $date = $datepicker.find('input.date');
            if ($date.hasClass('us')){ // use 'us' class for 01/01/2001 format
                date = new SplitDate($date.val(),'us');
            }
            else {  // otherwise we're assuming it's ISO format
                date = new SplitDate($date.val(),'iso');
            }
        }
        // if we're using 3 separate inputs for day, month, year
        // we'll use a span or div container with class="date multi"
        if ($datepicker.hasClass('multi')){
            date.yyyy = $datepicker.find('input.year').val();
            date.mm = $datepicker.find('input.month').val();
            date.dd = $datepicker.find('input.day').val();
        }

        date.us = date.mm + '/' + date.dd + '/' + date.yyyy;
        date.iso = date.yyyy + '-' + date.mm + '-' + date.dd;

        description = $fieldset.find('textarea[name="description"]').val();

        count = CONDR.lesionTracker.recordCount;

        var submitLesion = $.ajax({
            type: 'PUT',
            cache: false,
            async: true,
            url: serverRoot + '/data/' +
                'projects/CONDR_METS/' +
                'subjects/' + XNAT.data.context.subjectID +
                //'/experiments/' + CONDR.lesionTracker.lesionID +
                '/experiments/' + XNAT.data.context.subjectLabel + '_lesion_' + $.now() +
                '?' + 'xsiType=condr_mets:lesionData' +
                '&' + 'condr_mets:lesionData/saCollID=' + CONDR.lesionTracker.collID +
                //'&' + 'condr_mets:lesionData/saLinkID=' + CONDR.lesionTracker.lesionID +
                '&' + 'condr_mets:lesionData/nodeNum=' + count +
                '&' + 'condr_mets:lesionData/hemisphere=' + hemisphere.label +
                '&' + 'condr_mets:lesionData/location=' + location.label +
                '&' + 'condr_mets:lesionData/dateOfDiagnosis=' + date.iso +
                '&' + 'condr_mets:lesionData/description=' + encodeURIComponent(description) +
                '&' + 'allowDataDeletion=true' +
                '&' + 'XNAT_CSRF=' + csrfToken +
                ''
        });
        submitLesion.done(function (lesionID) {
            var reRenderLesionCollection = setInterval(function () {
                if (lesionID > '') { //check if selected option is loaded
                    clearInterval(reRenderLesionCollection);
                    CONDR.lesionTracker.newRecords.push(lesionID);
                    var url = CONDR.lesionTracker.collURI.replace(/{{LESION_COLLECTION_ID}}/, CONDR.lesionTracker.collID);
                    CONDR.lesionTracker.collection = new XNAT.app.ResultSetResultArray(url);
                    CONDR.lesionTracker.renderRecords(CONDR.lesionTracker.collection, $collection);
                    //$('#'+lesionID+'.record').css('color','#180');
                    xmodal.loading.close();
                    _afterSave();
                }
            }, 100);
        });
        submitLesion.fail(function (data, status, error) {
            CONDR.lesionTracker.recordCount--;
            xmodal.message('error', 'data: ' + data + '<br><br> status: ' + status + '<br><br> error: ' + error);
            xmodal.loading.close();
        });
    }
    else {
        xmodal.message('Missing Data', 'Please choose a <b>hemisphere</b> and <b>location</b> and enter the <b>date of diagnosis</b> and submit the data again.', 'OK');
    }
};




CONDR.lesionTracker.notesModal = function(_content){
    var modal_opts={};
    modal_opts.id = 'notes_modal';
    modal_opts.width = 500;
    modal_opts.height = 300;
    modal_opts.title = 'Collection Notes';
    modal_opts.content = _content ;
    modal_opts.scroll = false;
    modal_opts.closeBtn = false;
    modal_opts.esc = true;
    modal_opts.enter = false;
    modal_opts.okLabel = 'Save and Close';
    modal_opts.okAction = function(modal){
        xmodal.loading.open('Saving notes...');
        //xmodal.closeModal = false ;
        var $this_modal = $('#'+modal.id);
        var notes = $this_modal.find('textarea').val();
        //alert(notes);
        var saveNotes = $.ajax({
            type: 'PUT',
            cache: false,
            async: true,
            url: serverRoot +
                '/data/projects/CONDR_METS' +
                '/subjects/'+XNAT.data.context.subjectID+'' +
                '/experiments/'+CONDR.lesionTracker.collID+
                '?xsiType=condr_mets:lesionCollection' +
                '&condr_mets:lesionCollection/note=' + encodeURIComponent(notes) +
                '&allowDataDeletion=true' +
                '&XNAT_CSRF=' + csrfToken
        });
        saveNotes.done(function(_id){
            var reRenderNotes = setInterval(function(){
                if (_id > ''){ //check if selected option is loaded
                    clearInterval(reRenderNotes); // do we clear the interval first or last?
                    CONDR.lesionTracker.getCollNotes(_id);
                    if (CONDR.lesionTracker.collNotes > ''){
                        $('#add_notes').hide();
                        $notes.find('p').text(CONDR.lesionTracker.collNotes);
                        $notes.show();
                    }
                    else {
                        $('#add_notes').show();
                        $notes.find('p').text('');
                        $notes.hide();
                    }
                    xmodal.loading.close();
                }
            },100);
        });
    };
    modal_opts.beforeShow = function(modal){
        // not really using this?
        //xmodal.closeModal = true ;
        //alert('Wait!');
        //var $this_modal = $('#'+modal.id);
        //$this_modal.drags({handle:'.title'});
    };
    xmodal.open(modal_opts);
};





CONDR.lesionTracker.formCompleted=function($modal){
    return (
        $modal.find('select[name="hemisphere"]').val() > '' &&
        $modal.find('select[name="location"]').val() > '' &&
        $modal.find('input[name="date_of_diagnosis"]').val().length === 10
    )
};




//////////////////////////////////////////////////
// DOM READY
//////////////////////////////////////////////////
$(function(){

    $body = $('body');
    //countLesions();
    $wrapper = $('#CONDR_lesions');
    $collection = $('#lesion_collection');
    $notes = $('#collection_notes');
    $template = $('#lesion_modal');

    // make sure the lesionTracker.css is loaded
    if (!($('link[href*="lesionTracker.css"]').length)){
        $('head').append('<link type="text/css" rel="stylesheet" href="'+ scripts_dir + '/lesionTracker/lesionTracker.css">');
    }



    $body.on('click', 'a.btn, a.nolink, a[href="#"]', function(e){
        e.preventDefault();
    });


    // NEW LESION RECORD
    //
    $wrapper.on('click','#record_lesions',function(){
        CONDR.lesionTracker.addRecord($template);
    });


    // ADD NOTES
    //
    $wrapper.on('click','#add_notes',function(){
        var modal_content = '' +
            '<textarea style="width:450px;height:175px;"></textarea>' + //
            '';
        CONDR.lesionTracker.notesModal(modal_content);
    });


    // EDIT NOTES
    //
    $notes.find('h3 > a.edit').click(function(){
        var notes = $notes.find('p').text();
        var modal_content = '' +
            '<textarea style="width:450px;height:175px;">' + //
            notes +
            '</textarea>' +
            '';
        CONDR.lesionTracker.notesModal(modal_content);
    });


    // FILE MANAGER - TODO
    //
//    $body.on('click','button.file_manager',function(){
//        //var lesion = $(this).attr('data-expt');
//        //CONDR.lesionTracker.fileManager(lesion);
//        xmodal.message('Coming Soon','New file manager development in progress.','OK',{height:200});
//    });


    // CANCEL EDITING
    //
    $body.on('click','a.cancel_record',function(){

        //var modal_id = $(this).closest('div.xmodal').attr('id');
        xmodal.close($(this).closest('div.xmodal'));
        //xmodal.closeModal = true ;

//        var modal_opts = {};
//        modal_opts.width = 360 ;
//        modal_opts.height = 160 ;
//        modal_opts.title = 'Discard changes?';
//        modal_opts.closeBtn = 'hide';
//        modal_opts.content = '<p style="font-size:13px;">Would you like to cancel and discard any changes?</p>';
//        modal_opts.okLabel = 'Discard Changes';
//        modal_opts.okAction = function(){
//            xmodal.close(modal_id);
//        };
//        modal_opts.beforeShow = function(){
//            xModal.closeModal = true ;
//        };
//        modal_opts.cancelLabel = 'Continue Editing';
//        new xModal.Modal(modal_opts);
//        xmodal.loading.close();

    });


    // EDIT RECORD
    //
    $wrapper.on('click','tr.record a.edit',function(){
        xmodal.loading.open('Loading data...');
        var $record = $(this).closest('tr.record');
        var uri = $(this).attr('href');
        var page = uri+'?format=html';

        CONDR.lesionTracker.completedForm = false ;
        CONDR.lesionTracker.lesionID = $record.attr('id');

        var recordJSON = XNAT.app.itemData(uri+'?format=json');
        var recordData = recordJSON.data ;

        //if (console.log) console.log(recordData);

        var modal_opts = {};
        modal_opts.id = 'edit_'+recordData.id;
        modal_opts.title = 'Edit Lesion Record';
        modal_opts.closeBtn = 'hide';
        modal_opts.content = $template.html();
        modal_opts.footerContent = '<a href="#" class="cancel_record btn">Cancel</a>';

        modal_opts.buttons = {};

        modal_opts.buttons.save = {
            label: 'Save and Close',
            close: false,
            isDefault: true,
            action: function(modal){
                var $modal = $('#'+modal.id);
                if (CONDR.lesionTracker.formCompleted($modal)){
                    xmodal.loading.open('Saving...');
                    var saveLesion = $.ajax({
                        type: 'PUT',
                        cache: false,
                        async: true,
                        url:
                            serverRoot + '/data/projects/CONDR_METS' +
                            '/subjects/' + XNAT.data.context.subjectID +
                            '/experiments/' + recordData.ID +
                            '?' + 'xsiType=condr_mets:lesionData' +
                            // we're not changing these things, so don't submit the data
                            //'&' + 'condr_mets:lesionData/saCollID=' + CONDR.lesionTracker.collID +
                            //'&' + 'condr_mets:lesionData/saLinkID=' + CONDR.lesionTracker.lesionID +
                            //'&' + 'condr_mets:lesionData/nodeNum=' + count +
                            '&' + 'condr_mets:lesionData/hemisphere=' + $modal.find('select[name="hemisphere"]').val() +
                            '&' + 'condr_mets:lesionData/location=' + $modal.find('select[name="location"]').val() +
                            '&' + 'condr_mets:lesionData/dateOfDiagnosis=' + $modal.find('input[name="date_of_diagnosis"]').val() +
                            '&' + 'condr_mets:lesionData/description=' + encodeURIComponent($modal.find('textarea[name="description"]').val()) +
                            '&' + 'allowDataDeletion=true' +
                            '&' + 'XNAT_CSRF=' + csrfToken +
                            ''
                    });
                    saveLesion.done(function(lesionID){
                        var reRenderLesionCollection = setInterval(function(){
                            if (lesionID > ''){ //check if selected option is loaded
                                clearInterval(reRenderLesionCollection); // do we clear the interval first or last?
                                CONDR.lesionTracker.editedRecords.push(lesionID);
                                var url = CONDR.lesionTracker.collURI.replace(/{{LESION_COLLECTION_ID}}/,CONDR.lesionTracker.collID);
                                CONDR.lesionTracker.collection = new XNAT.app.ResultSetResultArray(url);
                                CONDR.lesionTracker.renderRecords(CONDR.lesionTracker.collection,$collection);
                                xmodal.close($modal);
                                xmodal.loading.close();
                            }
                        },100);
                    });
                    saveLesion.fail(function(data, status, error){
                        xmodal.close($modal);
                        xmodal.message('error','data: '+data+'<br><br> status: '+status+'<br><br> error: '+error);
                        xmodal.loading.close();
                    });
                }
                else {
                    xmodal.message('Missing Data', 'Please choose a <b>hemisphere</b> and <b>location</b> and enter the <b>date of diagnosis</b> and submit the data again.', 'OK');
                }
            }
        };


        modal_opts.beforeShow = function(modal){
            xmodal.loading.close();
            var $modal = $('#'+modal.id);
            //$modal.drags({handle:'.title'});
            var $form = $modal.find('form.lesion');
            $form.find('h4 > b').append(recordData.nodeNum + ' - edit');
            var replacements = {
                collID: recordData.saCollID,
                lesionID: recordData.ID,
                nodeNum: recordData.nodeNum,
                subjLabel: recordData.label,
                subjID: recordData.subject_ID,
                hemisphere: recordData.hemisphere,
                location: recordData.location,
                default_date: recordData.dateOfDiagnosis,
                date: recordData.dateOfDiagnosis,
                description: recordData.description
            };
            CONDR.lesionTracker.modalContent($modal,replacements);
            var $date_span = $modal.find('.datepicker');
            XNAT.app.datePicker.init($date_span);
            //var $date_input_us = $modal.find('input.date.single.us');
            //$date_input_us.mask("99/99/9999",{placeholder:" "});
            var $date_input_iso = $modal.find('input.date.single.iso');
            $date_input_iso.mask("9999-99-99",{placeholder:" "});
            $date_input_iso.val(recordData.dateOfDiagnosis);
        };

        modal_opts.afterShow = function(){
            //xmodal.loading.close();
        };

        xmodal.open(modal_opts);
    });

    $body.on('change','select.source',function(){
        var $select = $(this);
        applyValue($select.find('option:selected'), $select.next('input[data-from-name]'));
        //console.log('value selected: '+ $select.val());
    });
});





//////////////////////////////////////////////////
// FINISHED LOADING
//////////////////////////////////////////////////
$(window).load(function(){
    // render the collection on page load
    CONDR.lesionTracker.renderRecords(CONDR.lesionTracker.collection,$collection);

    if (CONDR.lesionTracker.collNotes !== false){
        $notes.find('p').text(CONDR.lesionTracker.collNotes);
        $notes.show();
    }
    else {
        $('#add_notes').show();
    }

});

// this really shouldn't be over 1,000 lines of code |*L*|
