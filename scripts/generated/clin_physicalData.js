/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function clin_physicalData(){
this.xsiType="clin:physicalData";

	this.getSchemaElementName=function(){
		return "physicalData";
	}

	this.getFullSchemaElementName=function(){
		return "clin:physicalData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Pxgenapp=null;


	function getPxgenapp() {
		return this.Pxgenapp;
	}
	this.getPxgenapp=getPxgenapp;


	function setPxgenapp(v){
		this.Pxgenapp=v;
	}
	this.setPxgenapp=setPxgenapp;

	this.Pxgendes=null;


	function getPxgendes() {
		return this.Pxgendes;
	}
	this.getPxgendes=getPxgendes;


	function setPxgendes(v){
		this.Pxgendes=v;
	}
	this.setPxgendes=setPxgendes;

	this.Pxheadey=null;


	function getPxheadey() {
		return this.Pxheadey;
	}
	this.getPxheadey=getPxheadey;


	function setPxheadey(v){
		this.Pxheadey=v;
	}
	this.setPxheadey=setPxheadey;

	this.Pxheadde=null;


	function getPxheadde() {
		return this.Pxheadde;
	}
	this.getPxheadde=getPxheadde;


	function setPxheadde(v){
		this.Pxheadde=v;
	}
	this.setPxheadde=setPxheadde;

	this.Pxneck=null;


	function getPxneck() {
		return this.Pxneck;
	}
	this.getPxneck=getPxneck;


	function setPxneck(v){
		this.Pxneck=v;
	}
	this.setPxneck=setPxneck;

	this.Pxnecdes=null;


	function getPxnecdes() {
		return this.Pxnecdes;
	}
	this.getPxnecdes=getPxnecdes;


	function setPxnecdes(v){
		this.Pxnecdes=v;
	}
	this.setPxnecdes=setPxnecdes;

	this.Pxchest=null;


	function getPxchest() {
		return this.Pxchest;
	}
	this.getPxchest=getPxchest;


	function setPxchest(v){
		this.Pxchest=v;
	}
	this.setPxchest=setPxchest;

	this.Pxchedes=null;


	function getPxchedes() {
		return this.Pxchedes;
	}
	this.getPxchedes=getPxchedes;


	function setPxchedes(v){
		this.Pxchedes=v;
	}
	this.setPxchedes=setPxchedes;

	this.Pxheart=null;


	function getPxheart() {
		return this.Pxheart;
	}
	this.getPxheart=getPxheart;


	function setPxheart(v){
		this.Pxheart=v;
	}
	this.setPxheart=setPxheart;

	this.Pxheades=null;


	function getPxheades() {
		return this.Pxheades;
	}
	this.getPxheades=getPxheades;


	function setPxheades(v){
		this.Pxheades=v;
	}
	this.setPxheades=setPxheades;

	this.Pxabdom=null;


	function getPxabdom() {
		return this.Pxabdom;
	}
	this.getPxabdom=getPxabdom;


	function setPxabdom(v){
		this.Pxabdom=v;
	}
	this.setPxabdom=setPxabdom;

	this.Pxabddes=null;


	function getPxabddes() {
		return this.Pxabddes;
	}
	this.getPxabddes=getPxabddes;


	function setPxabddes(v){
		this.Pxabddes=v;
	}
	this.setPxabddes=setPxabddes;

	this.Pxextrem=null;


	function getPxextrem() {
		return this.Pxextrem;
	}
	this.getPxextrem=getPxextrem;


	function setPxextrem(v){
		this.Pxextrem=v;
	}
	this.setPxextrem=setPxextrem;

	this.Pxextdes=null;


	function getPxextdes() {
		return this.Pxextdes;
	}
	this.getPxextdes=getPxextdes;


	function setPxextdes(v){
		this.Pxextdes=v;
	}
	this.setPxextdes=setPxextdes;

	this.Pxedema=null;


	function getPxedema() {
		return this.Pxedema;
	}
	this.getPxedema=getPxedema;


	function setPxedema(v){
		this.Pxedema=v;
	}
	this.setPxedema=setPxedema;

	this.Pxededes=null;


	function getPxededes() {
		return this.Pxededes;
	}
	this.getPxededes=getPxededes;


	function setPxededes(v){
		this.Pxededes=v;
	}
	this.setPxededes=setPxededes;

	this.Pxperiph=null;


	function getPxperiph() {
		return this.Pxperiph;
	}
	this.getPxperiph=getPxperiph;


	function setPxperiph(v){
		this.Pxperiph=v;
	}
	this.setPxperiph=setPxperiph;

	this.Pxperdes=null;


	function getPxperdes() {
		return this.Pxperdes;
	}
	this.getPxperdes=getPxperdes;


	function setPxperdes(v){
		this.Pxperdes=v;
	}
	this.setPxperdes=setPxperdes;

	this.Pxskin=null;


	function getPxskin() {
		return this.Pxskin;
	}
	this.getPxskin=getPxskin;


	function setPxskin(v){
		this.Pxskin=v;
	}
	this.setPxskin=setPxskin;

	this.Pxskides=null;


	function getPxskides() {
		return this.Pxskides;
	}
	this.getPxskides=getPxskides;


	function setPxskides(v){
		this.Pxskides=v;
	}
	this.setPxskides=setPxskides;

	this.Pxmuscul=null;


	function getPxmuscul() {
		return this.Pxmuscul;
	}
	this.getPxmuscul=getPxmuscul;


	function setPxmuscul(v){
		this.Pxmuscul=v;
	}
	this.setPxmuscul=setPxmuscul;

	this.Pxmusdes=null;


	function getPxmusdes() {
		return this.Pxmusdes;
	}
	this.getPxmusdes=getPxmusdes;


	function setPxmusdes(v){
		this.Pxmusdes=v;
	}
	this.setPxmusdes=setPxmusdes;

	this.Pxother=null;


	function getPxother() {
		return this.Pxother;
	}
	this.getPxother=getPxother;


	function setPxother(v){
		this.Pxother=v;
	}
	this.setPxother=setPxother;

	this.Pxotrcom=null;


	function getPxotrcom() {
		return this.Pxotrcom;
	}
	this.getPxotrcom=getPxotrcom;


	function setPxotrcom(v){
		this.Pxotrcom=v;
	}
	this.setPxotrcom=setPxotrcom;

	this.Pxgencom=null;


	function getPxgencom() {
		return this.Pxgencom;
	}
	this.getPxgencom=getPxgencom;


	function setPxgencom(v){
		this.Pxgencom=v;
	}
	this.setPxgencom=setPxgencom;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="PXGENAPP"){
				return this.Pxgenapp ;
			} else 
			if(xmlPath=="PXGENDES"){
				return this.Pxgendes ;
			} else 
			if(xmlPath=="PXHEADEY"){
				return this.Pxheadey ;
			} else 
			if(xmlPath=="PXHEADDE"){
				return this.Pxheadde ;
			} else 
			if(xmlPath=="PXNECK"){
				return this.Pxneck ;
			} else 
			if(xmlPath=="PXNECDES"){
				return this.Pxnecdes ;
			} else 
			if(xmlPath=="PXCHEST"){
				return this.Pxchest ;
			} else 
			if(xmlPath=="PXCHEDES"){
				return this.Pxchedes ;
			} else 
			if(xmlPath=="PXHEART"){
				return this.Pxheart ;
			} else 
			if(xmlPath=="PXHEADES"){
				return this.Pxheades ;
			} else 
			if(xmlPath=="PXABDOM"){
				return this.Pxabdom ;
			} else 
			if(xmlPath=="PXABDDES"){
				return this.Pxabddes ;
			} else 
			if(xmlPath=="PXEXTREM"){
				return this.Pxextrem ;
			} else 
			if(xmlPath=="PXEXTDES"){
				return this.Pxextdes ;
			} else 
			if(xmlPath=="PXEDEMA"){
				return this.Pxedema ;
			} else 
			if(xmlPath=="PXEDEDES"){
				return this.Pxededes ;
			} else 
			if(xmlPath=="PXPERIPH"){
				return this.Pxperiph ;
			} else 
			if(xmlPath=="PXPERDES"){
				return this.Pxperdes ;
			} else 
			if(xmlPath=="PXSKIN"){
				return this.Pxskin ;
			} else 
			if(xmlPath=="PXSKIDES"){
				return this.Pxskides ;
			} else 
			if(xmlPath=="PXMUSCUL"){
				return this.Pxmuscul ;
			} else 
			if(xmlPath=="PXMUSDES"){
				return this.Pxmusdes ;
			} else 
			if(xmlPath=="PXOTHER"){
				return this.Pxother ;
			} else 
			if(xmlPath=="PXOTRCOM"){
				return this.Pxotrcom ;
			} else 
			if(xmlPath=="PXGENCOM"){
				return this.Pxgencom ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="PXGENAPP"){
				this.Pxgenapp=value;
			} else 
			if(xmlPath=="PXGENDES"){
				this.Pxgendes=value;
			} else 
			if(xmlPath=="PXHEADEY"){
				this.Pxheadey=value;
			} else 
			if(xmlPath=="PXHEADDE"){
				this.Pxheadde=value;
			} else 
			if(xmlPath=="PXNECK"){
				this.Pxneck=value;
			} else 
			if(xmlPath=="PXNECDES"){
				this.Pxnecdes=value;
			} else 
			if(xmlPath=="PXCHEST"){
				this.Pxchest=value;
			} else 
			if(xmlPath=="PXCHEDES"){
				this.Pxchedes=value;
			} else 
			if(xmlPath=="PXHEART"){
				this.Pxheart=value;
			} else 
			if(xmlPath=="PXHEADES"){
				this.Pxheades=value;
			} else 
			if(xmlPath=="PXABDOM"){
				this.Pxabdom=value;
			} else 
			if(xmlPath=="PXABDDES"){
				this.Pxabddes=value;
			} else 
			if(xmlPath=="PXEXTREM"){
				this.Pxextrem=value;
			} else 
			if(xmlPath=="PXEXTDES"){
				this.Pxextdes=value;
			} else 
			if(xmlPath=="PXEDEMA"){
				this.Pxedema=value;
			} else 
			if(xmlPath=="PXEDEDES"){
				this.Pxededes=value;
			} else 
			if(xmlPath=="PXPERIPH"){
				this.Pxperiph=value;
			} else 
			if(xmlPath=="PXPERDES"){
				this.Pxperdes=value;
			} else 
			if(xmlPath=="PXSKIN"){
				this.Pxskin=value;
			} else 
			if(xmlPath=="PXSKIDES"){
				this.Pxskides=value;
			} else 
			if(xmlPath=="PXMUSCUL"){
				this.Pxmuscul=value;
			} else 
			if(xmlPath=="PXMUSDES"){
				this.Pxmusdes=value;
			} else 
			if(xmlPath=="PXOTHER"){
				this.Pxother=value;
			} else 
			if(xmlPath=="PXOTRCOM"){
				this.Pxotrcom=value;
			} else 
			if(xmlPath=="PXGENCOM"){
				this.Pxgencom=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="PXGENAPP"){
			return "field_data";
		}else if (xmlPath=="PXGENDES"){
			return "field_data";
		}else if (xmlPath=="PXHEADEY"){
			return "field_data";
		}else if (xmlPath=="PXHEADDE"){
			return "field_data";
		}else if (xmlPath=="PXNECK"){
			return "field_data";
		}else if (xmlPath=="PXNECDES"){
			return "field_data";
		}else if (xmlPath=="PXCHEST"){
			return "field_data";
		}else if (xmlPath=="PXCHEDES"){
			return "field_data";
		}else if (xmlPath=="PXHEART"){
			return "field_data";
		}else if (xmlPath=="PXHEADES"){
			return "field_data";
		}else if (xmlPath=="PXABDOM"){
			return "field_data";
		}else if (xmlPath=="PXABDDES"){
			return "field_data";
		}else if (xmlPath=="PXEXTREM"){
			return "field_data";
		}else if (xmlPath=="PXEXTDES"){
			return "field_data";
		}else if (xmlPath=="PXEDEMA"){
			return "field_data";
		}else if (xmlPath=="PXEDEDES"){
			return "field_data";
		}else if (xmlPath=="PXPERIPH"){
			return "field_data";
		}else if (xmlPath=="PXPERDES"){
			return "field_data";
		}else if (xmlPath=="PXSKIN"){
			return "field_data";
		}else if (xmlPath=="PXSKIDES"){
			return "field_data";
		}else if (xmlPath=="PXMUSCUL"){
			return "field_data";
		}else if (xmlPath=="PXMUSDES"){
			return "field_data";
		}else if (xmlPath=="PXOTHER"){
			return "field_data";
		}else if (xmlPath=="PXOTRCOM"){
			return "field_data";
		}else if (xmlPath=="PXGENCOM"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<clin:PHYSICAL";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</clin:PHYSICAL>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Pxgenapp!=null){
			xmlTxt+="\n<clin:PXGENAPP";
			xmlTxt+=">";
			xmlTxt+=this.Pxgenapp;
			xmlTxt+="</clin:PXGENAPP>";
		}
		if (this.Pxgendes!=null){
			xmlTxt+="\n<clin:PXGENDES";
			xmlTxt+=">";
			xmlTxt+=this.Pxgendes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:PXGENDES>";
		}
		if (this.Pxheadey!=null){
			xmlTxt+="\n<clin:PXHEADEY";
			xmlTxt+=">";
			xmlTxt+=this.Pxheadey;
			xmlTxt+="</clin:PXHEADEY>";
		}
		if (this.Pxheadde!=null){
			xmlTxt+="\n<clin:PXHEADDE";
			xmlTxt+=">";
			xmlTxt+=this.Pxheadde.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:PXHEADDE>";
		}
		if (this.Pxneck!=null){
			xmlTxt+="\n<clin:PXNECK";
			xmlTxt+=">";
			xmlTxt+=this.Pxneck;
			xmlTxt+="</clin:PXNECK>";
		}
		if (this.Pxnecdes!=null){
			xmlTxt+="\n<clin:PXNECDES";
			xmlTxt+=">";
			xmlTxt+=this.Pxnecdes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:PXNECDES>";
		}
		if (this.Pxchest!=null){
			xmlTxt+="\n<clin:PXCHEST";
			xmlTxt+=">";
			xmlTxt+=this.Pxchest;
			xmlTxt+="</clin:PXCHEST>";
		}
		if (this.Pxchedes!=null){
			xmlTxt+="\n<clin:PXCHEDES";
			xmlTxt+=">";
			xmlTxt+=this.Pxchedes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:PXCHEDES>";
		}
		if (this.Pxheart!=null){
			xmlTxt+="\n<clin:PXHEART";
			xmlTxt+=">";
			xmlTxt+=this.Pxheart;
			xmlTxt+="</clin:PXHEART>";
		}
		if (this.Pxheades!=null){
			xmlTxt+="\n<clin:PXHEADES";
			xmlTxt+=">";
			xmlTxt+=this.Pxheades.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:PXHEADES>";
		}
		if (this.Pxabdom!=null){
			xmlTxt+="\n<clin:PXABDOM";
			xmlTxt+=">";
			xmlTxt+=this.Pxabdom;
			xmlTxt+="</clin:PXABDOM>";
		}
		if (this.Pxabddes!=null){
			xmlTxt+="\n<clin:PXABDDES";
			xmlTxt+=">";
			xmlTxt+=this.Pxabddes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:PXABDDES>";
		}
		if (this.Pxextrem!=null){
			xmlTxt+="\n<clin:PXEXTREM";
			xmlTxt+=">";
			xmlTxt+=this.Pxextrem;
			xmlTxt+="</clin:PXEXTREM>";
		}
		if (this.Pxextdes!=null){
			xmlTxt+="\n<clin:PXEXTDES";
			xmlTxt+=">";
			xmlTxt+=this.Pxextdes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:PXEXTDES>";
		}
		if (this.Pxedema!=null){
			xmlTxt+="\n<clin:PXEDEMA";
			xmlTxt+=">";
			xmlTxt+=this.Pxedema;
			xmlTxt+="</clin:PXEDEMA>";
		}
		if (this.Pxededes!=null){
			xmlTxt+="\n<clin:PXEDEDES";
			xmlTxt+=">";
			xmlTxt+=this.Pxededes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:PXEDEDES>";
		}
		if (this.Pxperiph!=null){
			xmlTxt+="\n<clin:PXPERIPH";
			xmlTxt+=">";
			xmlTxt+=this.Pxperiph;
			xmlTxt+="</clin:PXPERIPH>";
		}
		if (this.Pxperdes!=null){
			xmlTxt+="\n<clin:PXPERDES";
			xmlTxt+=">";
			xmlTxt+=this.Pxperdes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:PXPERDES>";
		}
		if (this.Pxskin!=null){
			xmlTxt+="\n<clin:PXSKIN";
			xmlTxt+=">";
			xmlTxt+=this.Pxskin;
			xmlTxt+="</clin:PXSKIN>";
		}
		if (this.Pxskides!=null){
			xmlTxt+="\n<clin:PXSKIDES";
			xmlTxt+=">";
			xmlTxt+=this.Pxskides.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:PXSKIDES>";
		}
		if (this.Pxmuscul!=null){
			xmlTxt+="\n<clin:PXMUSCUL";
			xmlTxt+=">";
			xmlTxt+=this.Pxmuscul;
			xmlTxt+="</clin:PXMUSCUL>";
		}
		if (this.Pxmusdes!=null){
			xmlTxt+="\n<clin:PXMUSDES";
			xmlTxt+=">";
			xmlTxt+=this.Pxmusdes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:PXMUSDES>";
		}
		if (this.Pxother!=null){
			xmlTxt+="\n<clin:PXOTHER";
			xmlTxt+=">";
			xmlTxt+=this.Pxother;
			xmlTxt+="</clin:PXOTHER>";
		}
		if (this.Pxotrcom!=null){
			xmlTxt+="\n<clin:PXOTRCOM";
			xmlTxt+=">";
			xmlTxt+=this.Pxotrcom.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:PXOTRCOM>";
		}
		if (this.Pxgencom!=null){
			xmlTxt+="\n<clin:PXGENCOM";
			xmlTxt+=">";
			xmlTxt+=this.Pxgencom.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:PXGENCOM>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Pxgenapp!=null) return true;
		if (this.Pxgendes!=null) return true;
		if (this.Pxheadey!=null) return true;
		if (this.Pxheadde!=null) return true;
		if (this.Pxneck!=null) return true;
		if (this.Pxnecdes!=null) return true;
		if (this.Pxchest!=null) return true;
		if (this.Pxchedes!=null) return true;
		if (this.Pxheart!=null) return true;
		if (this.Pxheades!=null) return true;
		if (this.Pxabdom!=null) return true;
		if (this.Pxabddes!=null) return true;
		if (this.Pxextrem!=null) return true;
		if (this.Pxextdes!=null) return true;
		if (this.Pxedema!=null) return true;
		if (this.Pxededes!=null) return true;
		if (this.Pxperiph!=null) return true;
		if (this.Pxperdes!=null) return true;
		if (this.Pxskin!=null) return true;
		if (this.Pxskides!=null) return true;
		if (this.Pxmuscul!=null) return true;
		if (this.Pxmusdes!=null) return true;
		if (this.Pxother!=null) return true;
		if (this.Pxotrcom!=null) return true;
		if (this.Pxgencom!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
