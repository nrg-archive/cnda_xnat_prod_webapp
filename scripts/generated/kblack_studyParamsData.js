/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function kblack_studyParamsData(){
this.xsiType="kblack:studyParamsData";

	this.getSchemaElementName=function(){
		return "studyParamsData";
	}

	this.getFullSchemaElementName=function(){
		return "kblack:studyParamsData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Subjectcode=null;


	function getSubjectcode() {
		return this.Subjectcode;
	}
	this.getSubjectcode=getSubjectcode;


	function setSubjectcode(v){
		this.Subjectcode=v;
	}
	this.setSubjectcode=setSubjectcode;

	this.Wasscreeningday=null;


	function getWasscreeningday() {
		return this.Wasscreeningday;
	}
	this.getWasscreeningday=getWasscreeningday;


	function setWasscreeningday(v){
		this.Wasscreeningday=v;
	}
	this.setWasscreeningday=setWasscreeningday;


	this.isWasscreeningday=function(defaultValue) {
		if(this.Wasscreeningday==null)return defaultValue;
		if(this.Wasscreeningday=="1" || this.Wasscreeningday==true)return true;
		return false;
	}

	this.Studytype=null;


	function getStudytype() {
		return this.Studytype;
	}
	this.getStudytype=getStudytype;


	function setStudytype(v){
		this.Studytype=v;
	}
	this.setStudytype=setStudytype;

	this.Weight=null;


	function getWeight() {
		return this.Weight;
	}
	this.getWeight=getWeight;


	function setWeight(v){
		this.Weight=v;
	}
	this.setWeight=setWeight;

	this.Weight_units=null;


	function getWeight_units() {
		return this.Weight_units;
	}
	this.getWeight_units=getWeight_units;


	function setWeight_units(v){
		this.Weight_units=v;
	}
	this.setWeight_units=setWeight_units;

	this.Infused=null;


	function getInfused() {
		return this.Infused;
	}
	this.getInfused=getInfused;


	function setInfused(v){
		this.Infused=v;
	}
	this.setInfused=setInfused;

	this.Cddose=null;


	function getCddose() {
		return this.Cddose;
	}
	this.getCddose=getCddose;


	function setCddose(v){
		this.Cddose=v;
	}
	this.setCddose=setCddose;

	this.Cdtime=null;


	function getCdtime() {
		return this.Cdtime;
	}
	this.getCdtime=getCdtime;


	function setCdtime(v){
		this.Cdtime=v;
	}
	this.setCdtime=setCdtime;

	this.Infusionstarttime=null;


	function getInfusionstarttime() {
		return this.Infusionstarttime;
	}
	this.getInfusionstarttime=getInfusionstarttime;


	function setInfusionstarttime(v){
		this.Infusionstarttime=v;
	}
	this.setInfusionstarttime=setInfusionstarttime;

	this.Method=null;


	function getMethod() {
		return this.Method;
	}
	this.getMethod=getMethod;


	function setMethod(v){
		this.Method=v;
	}
	this.setMethod=setMethod;

	this.Loadduration=null;


	function getLoadduration() {
		return this.Loadduration;
	}
	this.getLoadduration=getLoadduration;


	function setLoadduration(v){
		this.Loadduration=v;
	}
	this.setLoadduration=setLoadduration;

	this.Loadrate=null;


	function getLoadrate() {
		return this.Loadrate;
	}
	this.getLoadrate=getLoadrate;


	function setLoadrate(v){
		this.Loadrate=v;
	}
	this.setLoadrate=setLoadrate;

	this.Maintrate=null;


	function getMaintrate() {
		return this.Maintrate;
	}
	this.getMaintrate=getMaintrate;


	function setMaintrate(v){
		this.Maintrate=v;
	}
	this.setMaintrate=setMaintrate;

	this.Bloodisavailable=null;


	function getBloodisavailable() {
		return this.Bloodisavailable;
	}
	this.getBloodisavailable=getBloodisavailable;


	function setBloodisavailable(v){
		this.Bloodisavailable=v;
	}
	this.setBloodisavailable=setBloodisavailable;


	this.isBloodisavailable=function(defaultValue) {
		if(this.Bloodisavailable==null)return defaultValue;
		if(this.Bloodisavailable=="1" || this.Bloodisavailable==true)return true;
		return false;
	}

	this.Samplesite=null;


	function getSamplesite() {
		return this.Samplesite;
	}
	this.getSamplesite=getSamplesite;


	function setSamplesite(v){
		this.Samplesite=v;
	}
	this.setSamplesite=setSamplesite;

	this.Nausea=null;


	function getNausea() {
		return this.Nausea;
	}
	this.getNausea=getNausea;


	function setNausea(v){
		this.Nausea=v;
	}
	this.setNausea=setNausea;


	this.isNausea=function(defaultValue) {
		if(this.Nausea==null)return defaultValue;
		if(this.Nausea=="1" || this.Nausea==true)return true;
		return false;
	}

	this.Vomiting=null;


	function getVomiting() {
		return this.Vomiting;
	}
	this.getVomiting=getVomiting;


	function setVomiting(v){
		this.Vomiting=v;
	}
	this.setVomiting=setVomiting;


	this.isVomiting=function(defaultValue) {
		if(this.Vomiting==null)return defaultValue;
		if(this.Vomiting=="1" || this.Vomiting==true)return true;
		return false;
	}

	this.Sedation=null;


	function getSedation() {
		return this.Sedation;
	}
	this.getSedation=getSedation;


	function setSedation(v){
		this.Sedation=v;
	}
	this.setSedation=setSedation;


	this.isSedation=function(defaultValue) {
		if(this.Sedation==null)return defaultValue;
		if(this.Sedation=="1" || this.Sedation==true)return true;
		return false;
	}

	this.Dizzy=null;


	function getDizzy() {
		return this.Dizzy;
	}
	this.getDizzy=getDizzy;


	function setDizzy(v){
		this.Dizzy=v;
	}
	this.setDizzy=setDizzy;


	this.isDizzy=function(defaultValue) {
		if(this.Dizzy==null)return defaultValue;
		if(this.Dizzy=="1" || this.Dizzy==true)return true;
		return false;
	}

	this.Dyskinesias=null;


	function getDyskinesias() {
		return this.Dyskinesias;
	}
	this.getDyskinesias=getDyskinesias;


	function setDyskinesias(v){
		this.Dyskinesias=v;
	}
	this.setDyskinesias=setDyskinesias;


	this.isDyskinesias=function(defaultValue) {
		if(this.Dyskinesias==null)return defaultValue;
		if(this.Dyskinesias=="1" || this.Dyskinesias==true)return true;
		return false;
	}

	this.Psychosis=null;


	function getPsychosis() {
		return this.Psychosis;
	}
	this.getPsychosis=getPsychosis;


	function setPsychosis(v){
		this.Psychosis=v;
	}
	this.setPsychosis=setPsychosis;


	this.isPsychosis=function(defaultValue) {
		if(this.Psychosis==null)return defaultValue;
		if(this.Psychosis=="1" || this.Psychosis==true)return true;
		return false;
	}

	this.Ticsworse=null;


	function getTicsworse() {
		return this.Ticsworse;
	}
	this.getTicsworse=getTicsworse;


	function setTicsworse(v){
		this.Ticsworse=v;
	}
	this.setTicsworse=setTicsworse;


	this.isTicsworse=function(defaultValue) {
		if(this.Ticsworse==null)return defaultValue;
		if(this.Ticsworse=="1" || this.Ticsworse==true)return true;
		return false;
	}

	this.Ticsbetter=null;


	function getTicsbetter() {
		return this.Ticsbetter;
	}
	this.getTicsbetter=getTicsbetter;


	function setTicsbetter(v){
		this.Ticsbetter=v;
	}
	this.setTicsbetter=setTicsbetter;


	this.isTicsbetter=function(defaultValue) {
		if(this.Ticsbetter==null)return defaultValue;
		if(this.Ticsbetter=="1" || this.Ticsbetter==true)return true;
		return false;
	}

	this.Othersideeffects=null;


	function getOthersideeffects() {
		return this.Othersideeffects;
	}
	this.getOthersideeffects=getOthersideeffects;


	function setOthersideeffects(v){
		this.Othersideeffects=v;
	}
	this.setOthersideeffects=setOthersideeffects;


	this.isOthersideeffects=function(defaultValue) {
		if(this.Othersideeffects==null)return defaultValue;
		if(this.Othersideeffects=="1" || this.Othersideeffects==true)return true;
		return false;
	}

	this.Problem=null;


	function getProblem() {
		return this.Problem;
	}
	this.getProblem=getProblem;


	function setProblem(v){
		this.Problem=v;
	}
	this.setProblem=setProblem;


	this.isProblem=function(defaultValue) {
		if(this.Problem==null)return defaultValue;
		if(this.Problem=="1" || this.Problem==true)return true;
		return false;
	}

	this.Knownforivld1=null;


	function getKnownforivld1() {
		return this.Knownforivld1;
	}
	this.getKnownforivld1=getKnownforivld1;


	function setKnownforivld1(v){
		this.Knownforivld1=v;
	}
	this.setKnownforivld1=setKnownforivld1;


	this.isKnownforivld1=function(defaultValue) {
		if(this.Knownforivld1==null)return defaultValue;
		if(this.Knownforivld1=="1" || this.Knownforivld1==true)return true;
		return false;
	}

	this.Rejectedforivld1=null;


	function getRejectedforivld1() {
		return this.Rejectedforivld1;
	}
	this.getRejectedforivld1=getRejectedforivld1;


	function setRejectedforivld1(v){
		this.Rejectedforivld1=v;
	}
	this.setRejectedforivld1=setRejectedforivld1;


	this.isRejectedforivld1=function(defaultValue) {
		if(this.Rejectedforivld1==null)return defaultValue;
		if(this.Rejectedforivld1=="1" || this.Rejectedforivld1==true)return true;
		return false;
	}

	this.Blindnesssubject=null;


	function getBlindnesssubject() {
		return this.Blindnesssubject;
	}
	this.getBlindnesssubject=getBlindnesssubject;


	function setBlindnesssubject(v){
		this.Blindnesssubject=v;
	}
	this.setBlindnesssubject=setBlindnesssubject;


	this.isBlindnesssubject=function(defaultValue) {
		if(this.Blindnesssubject==null)return defaultValue;
		if(this.Blindnesssubject=="1" || this.Blindnesssubject==true)return true;
		return false;
	}

	this.Blindnessinvestigator=null;


	function getBlindnessinvestigator() {
		return this.Blindnessinvestigator;
	}
	this.getBlindnessinvestigator=getBlindnessinvestigator;


	function setBlindnessinvestigator(v){
		this.Blindnessinvestigator=v;
	}
	this.setBlindnessinvestigator=setBlindnessinvestigator;


	this.isBlindnessinvestigator=function(defaultValue) {
		if(this.Blindnessinvestigator==null)return defaultValue;
		if(this.Blindnessinvestigator=="1" || this.Blindnessinvestigator==true)return true;
		return false;
	}

	this.Blindnesscoordinator=null;


	function getBlindnesscoordinator() {
		return this.Blindnesscoordinator;
	}
	this.getBlindnesscoordinator=getBlindnesscoordinator;


	function setBlindnesscoordinator(v){
		this.Blindnesscoordinator=v;
	}
	this.setBlindnesscoordinator=setBlindnesscoordinator;


	this.isBlindnesscoordinator=function(defaultValue) {
		if(this.Blindnesscoordinator==null)return defaultValue;
		if(this.Blindnesscoordinator=="1" || this.Blindnesscoordinator==true)return true;
		return false;
	}

	this.Pregnancytest=null;


	function getPregnancytest() {
		return this.Pregnancytest;
	}
	this.getPregnancytest=getPregnancytest;


	function setPregnancytest(v){
		this.Pregnancytest=v;
	}
	this.setPregnancytest=setPregnancytest;


	this.isPregnancytest=function(defaultValue) {
		if(this.Pregnancytest==null)return defaultValue;
		if(this.Pregnancytest=="1" || this.Pregnancytest==true)return true;
		return false;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="subjectCode"){
				return this.Subjectcode ;
			} else 
			if(xmlPath=="wasScreeningDay"){
				return this.Wasscreeningday ;
			} else 
			if(xmlPath=="studyType"){
				return this.Studytype ;
			} else 
			if(xmlPath=="weight"){
				return this.Weight ;
			} else 
			if(xmlPath=="weight/units"){
				return this.Weight_units ;
			} else 
			if(xmlPath=="infused"){
				return this.Infused ;
			} else 
			if(xmlPath=="CDdose"){
				return this.Cddose ;
			} else 
			if(xmlPath=="CDtime"){
				return this.Cdtime ;
			} else 
			if(xmlPath=="infusionStartTime"){
				return this.Infusionstarttime ;
			} else 
			if(xmlPath=="method"){
				return this.Method ;
			} else 
			if(xmlPath=="loadDuration"){
				return this.Loadduration ;
			} else 
			if(xmlPath=="loadRate"){
				return this.Loadrate ;
			} else 
			if(xmlPath=="maintRate"){
				return this.Maintrate ;
			} else 
			if(xmlPath=="bloodIsAvailable"){
				return this.Bloodisavailable ;
			} else 
			if(xmlPath=="sampleSite"){
				return this.Samplesite ;
			} else 
			if(xmlPath=="nausea"){
				return this.Nausea ;
			} else 
			if(xmlPath=="vomiting"){
				return this.Vomiting ;
			} else 
			if(xmlPath=="sedation"){
				return this.Sedation ;
			} else 
			if(xmlPath=="dizzy"){
				return this.Dizzy ;
			} else 
			if(xmlPath=="dyskinesias"){
				return this.Dyskinesias ;
			} else 
			if(xmlPath=="psychosis"){
				return this.Psychosis ;
			} else 
			if(xmlPath=="ticsWorse"){
				return this.Ticsworse ;
			} else 
			if(xmlPath=="ticsBetter"){
				return this.Ticsbetter ;
			} else 
			if(xmlPath=="otherSideEffects"){
				return this.Othersideeffects ;
			} else 
			if(xmlPath=="problem"){
				return this.Problem ;
			} else 
			if(xmlPath=="knownForIVLD1"){
				return this.Knownforivld1 ;
			} else 
			if(xmlPath=="rejectedForIVLD1"){
				return this.Rejectedforivld1 ;
			} else 
			if(xmlPath=="blindnessSubject"){
				return this.Blindnesssubject ;
			} else 
			if(xmlPath=="blindnessInvestigator"){
				return this.Blindnessinvestigator ;
			} else 
			if(xmlPath=="blindnessCoordinator"){
				return this.Blindnesscoordinator ;
			} else 
			if(xmlPath=="pregnancyTest"){
				return this.Pregnancytest ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="subjectCode"){
				this.Subjectcode=value;
			} else 
			if(xmlPath=="wasScreeningDay"){
				this.Wasscreeningday=value;
			} else 
			if(xmlPath=="studyType"){
				this.Studytype=value;
			} else 
			if(xmlPath=="weight"){
				this.Weight=value;
			} else 
			if(xmlPath=="weight/units"){
				this.Weight_units=value;
			} else 
			if(xmlPath=="infused"){
				this.Infused=value;
			} else 
			if(xmlPath=="CDdose"){
				this.Cddose=value;
			} else 
			if(xmlPath=="CDtime"){
				this.Cdtime=value;
			} else 
			if(xmlPath=="infusionStartTime"){
				this.Infusionstarttime=value;
			} else 
			if(xmlPath=="method"){
				this.Method=value;
			} else 
			if(xmlPath=="loadDuration"){
				this.Loadduration=value;
			} else 
			if(xmlPath=="loadRate"){
				this.Loadrate=value;
			} else 
			if(xmlPath=="maintRate"){
				this.Maintrate=value;
			} else 
			if(xmlPath=="bloodIsAvailable"){
				this.Bloodisavailable=value;
			} else 
			if(xmlPath=="sampleSite"){
				this.Samplesite=value;
			} else 
			if(xmlPath=="nausea"){
				this.Nausea=value;
			} else 
			if(xmlPath=="vomiting"){
				this.Vomiting=value;
			} else 
			if(xmlPath=="sedation"){
				this.Sedation=value;
			} else 
			if(xmlPath=="dizzy"){
				this.Dizzy=value;
			} else 
			if(xmlPath=="dyskinesias"){
				this.Dyskinesias=value;
			} else 
			if(xmlPath=="psychosis"){
				this.Psychosis=value;
			} else 
			if(xmlPath=="ticsWorse"){
				this.Ticsworse=value;
			} else 
			if(xmlPath=="ticsBetter"){
				this.Ticsbetter=value;
			} else 
			if(xmlPath=="otherSideEffects"){
				this.Othersideeffects=value;
			} else 
			if(xmlPath=="problem"){
				this.Problem=value;
			} else 
			if(xmlPath=="knownForIVLD1"){
				this.Knownforivld1=value;
			} else 
			if(xmlPath=="rejectedForIVLD1"){
				this.Rejectedforivld1=value;
			} else 
			if(xmlPath=="blindnessSubject"){
				this.Blindnesssubject=value;
			} else 
			if(xmlPath=="blindnessInvestigator"){
				this.Blindnessinvestigator=value;
			} else 
			if(xmlPath=="blindnessCoordinator"){
				this.Blindnesscoordinator=value;
			} else 
			if(xmlPath=="pregnancyTest"){
				this.Pregnancytest=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="subjectCode"){
			return "field_data";
		}else if (xmlPath=="wasScreeningDay"){
			return "field_data";
		}else if (xmlPath=="studyType"){
			return "field_data";
		}else if (xmlPath=="weight"){
			return "field_data";
		}else if (xmlPath=="weight/units"){
			return "field_data";
		}else if (xmlPath=="infused"){
			return "field_data";
		}else if (xmlPath=="CDdose"){
			return "field_data";
		}else if (xmlPath=="CDtime"){
			return "field_data";
		}else if (xmlPath=="infusionStartTime"){
			return "field_data";
		}else if (xmlPath=="method"){
			return "field_data";
		}else if (xmlPath=="loadDuration"){
			return "field_data";
		}else if (xmlPath=="loadRate"){
			return "field_data";
		}else if (xmlPath=="maintRate"){
			return "field_data";
		}else if (xmlPath=="bloodIsAvailable"){
			return "field_data";
		}else if (xmlPath=="sampleSite"){
			return "field_data";
		}else if (xmlPath=="nausea"){
			return "field_data";
		}else if (xmlPath=="vomiting"){
			return "field_data";
		}else if (xmlPath=="sedation"){
			return "field_data";
		}else if (xmlPath=="dizzy"){
			return "field_data";
		}else if (xmlPath=="dyskinesias"){
			return "field_data";
		}else if (xmlPath=="psychosis"){
			return "field_data";
		}else if (xmlPath=="ticsWorse"){
			return "field_data";
		}else if (xmlPath=="ticsBetter"){
			return "field_data";
		}else if (xmlPath=="otherSideEffects"){
			return "field_data";
		}else if (xmlPath=="problem"){
			return "field_data";
		}else if (xmlPath=="knownForIVLD1"){
			return "field_data";
		}else if (xmlPath=="rejectedForIVLD1"){
			return "field_data";
		}else if (xmlPath=="blindnessSubject"){
			return "field_data";
		}else if (xmlPath=="blindnessInvestigator"){
			return "field_data";
		}else if (xmlPath=="blindnessCoordinator"){
			return "field_data";
		}else if (xmlPath=="pregnancyTest"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<kblack:StudyParams";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</kblack:StudyParams>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Subjectcode!=null){
			xmlTxt+="\n<kblack:subjectCode";
			xmlTxt+=">";
			xmlTxt+=this.Subjectcode.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</kblack:subjectCode>";
		}
		if (this.Wasscreeningday!=null){
			xmlTxt+="\n<kblack:wasScreeningDay";
			xmlTxt+=">";
			xmlTxt+=this.Wasscreeningday;
			xmlTxt+="</kblack:wasScreeningDay>";
		}
		if (this.Studytype!=null){
			xmlTxt+="\n<kblack:studyType";
			xmlTxt+=">";
			xmlTxt+=this.Studytype.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</kblack:studyType>";
		}
		var WeightATT = ""
		if (this.Weight_units!=null)
			WeightATT+=" units=\"" + this.Weight_units.replace(/>/g,"&gt;").replace(/</g,"&lt;") + "\"";
		if (this.Weight!=null){
			xmlTxt+="\n<kblack:weight";
			xmlTxt+=WeightATT;
			xmlTxt+=">";
			xmlTxt+=this.Weight;
			xmlTxt+="</kblack:weight>";
		}
		else if(WeightATT!=""){
			xmlTxt+="\n<kblack:weight";
			xmlTxt+=WeightATT;
			xmlTxt+="/>";
		}

		if (this.Infused!=null){
			xmlTxt+="\n<kblack:infused";
			xmlTxt+=">";
			xmlTxt+=this.Infused.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</kblack:infused>";
		}
		if (this.Cddose!=null){
			xmlTxt+="\n<kblack:CDdose";
			xmlTxt+=">";
			xmlTxt+=this.Cddose;
			xmlTxt+="</kblack:CDdose>";
		}
		if (this.Cdtime!=null){
			xmlTxt+="\n<kblack:CDtime";
			xmlTxt+=">";
			xmlTxt+=this.Cdtime;
			xmlTxt+="</kblack:CDtime>";
		}
		if (this.Infusionstarttime!=null){
			xmlTxt+="\n<kblack:infusionStartTime";
			xmlTxt+=">";
			xmlTxt+=this.Infusionstarttime;
			xmlTxt+="</kblack:infusionStartTime>";
		}
		if (this.Method!=null){
			xmlTxt+="\n<kblack:method";
			xmlTxt+=">";
			xmlTxt+=this.Method.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</kblack:method>";
		}
		if (this.Loadduration!=null){
			xmlTxt+="\n<kblack:loadDuration";
			xmlTxt+=">";
			xmlTxt+=this.Loadduration.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</kblack:loadDuration>";
		}
		if (this.Loadrate!=null){
			xmlTxt+="\n<kblack:loadRate";
			xmlTxt+=">";
			xmlTxt+=this.Loadrate;
			xmlTxt+="</kblack:loadRate>";
		}
		if (this.Maintrate!=null){
			xmlTxt+="\n<kblack:maintRate";
			xmlTxt+=">";
			xmlTxt+=this.Maintrate;
			xmlTxt+="</kblack:maintRate>";
		}
		if (this.Bloodisavailable!=null){
			xmlTxt+="\n<kblack:bloodIsAvailable";
			xmlTxt+=">";
			xmlTxt+=this.Bloodisavailable;
			xmlTxt+="</kblack:bloodIsAvailable>";
		}
		if (this.Samplesite!=null){
			xmlTxt+="\n<kblack:sampleSite";
			xmlTxt+=">";
			xmlTxt+=this.Samplesite.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</kblack:sampleSite>";
		}
		if (this.Nausea!=null){
			xmlTxt+="\n<kblack:nausea";
			xmlTxt+=">";
			xmlTxt+=this.Nausea;
			xmlTxt+="</kblack:nausea>";
		}
		if (this.Vomiting!=null){
			xmlTxt+="\n<kblack:vomiting";
			xmlTxt+=">";
			xmlTxt+=this.Vomiting;
			xmlTxt+="</kblack:vomiting>";
		}
		if (this.Sedation!=null){
			xmlTxt+="\n<kblack:sedation";
			xmlTxt+=">";
			xmlTxt+=this.Sedation;
			xmlTxt+="</kblack:sedation>";
		}
		if (this.Dizzy!=null){
			xmlTxt+="\n<kblack:dizzy";
			xmlTxt+=">";
			xmlTxt+=this.Dizzy;
			xmlTxt+="</kblack:dizzy>";
		}
		if (this.Dyskinesias!=null){
			xmlTxt+="\n<kblack:dyskinesias";
			xmlTxt+=">";
			xmlTxt+=this.Dyskinesias;
			xmlTxt+="</kblack:dyskinesias>";
		}
		if (this.Psychosis!=null){
			xmlTxt+="\n<kblack:psychosis";
			xmlTxt+=">";
			xmlTxt+=this.Psychosis;
			xmlTxt+="</kblack:psychosis>";
		}
		if (this.Ticsworse!=null){
			xmlTxt+="\n<kblack:ticsWorse";
			xmlTxt+=">";
			xmlTxt+=this.Ticsworse;
			xmlTxt+="</kblack:ticsWorse>";
		}
		if (this.Ticsbetter!=null){
			xmlTxt+="\n<kblack:ticsBetter";
			xmlTxt+=">";
			xmlTxt+=this.Ticsbetter;
			xmlTxt+="</kblack:ticsBetter>";
		}
		if (this.Othersideeffects!=null){
			xmlTxt+="\n<kblack:otherSideEffects";
			xmlTxt+=">";
			xmlTxt+=this.Othersideeffects;
			xmlTxt+="</kblack:otherSideEffects>";
		}
		if (this.Problem!=null){
			xmlTxt+="\n<kblack:problem";
			xmlTxt+=">";
			xmlTxt+=this.Problem;
			xmlTxt+="</kblack:problem>";
		}
		if (this.Knownforivld1!=null){
			xmlTxt+="\n<kblack:knownForIVLD1";
			xmlTxt+=">";
			xmlTxt+=this.Knownforivld1;
			xmlTxt+="</kblack:knownForIVLD1>";
		}
		if (this.Rejectedforivld1!=null){
			xmlTxt+="\n<kblack:rejectedForIVLD1";
			xmlTxt+=">";
			xmlTxt+=this.Rejectedforivld1;
			xmlTxt+="</kblack:rejectedForIVLD1>";
		}
		if (this.Blindnesssubject!=null){
			xmlTxt+="\n<kblack:blindnessSubject";
			xmlTxt+=">";
			xmlTxt+=this.Blindnesssubject;
			xmlTxt+="</kblack:blindnessSubject>";
		}
		if (this.Blindnessinvestigator!=null){
			xmlTxt+="\n<kblack:blindnessInvestigator";
			xmlTxt+=">";
			xmlTxt+=this.Blindnessinvestigator;
			xmlTxt+="</kblack:blindnessInvestigator>";
		}
		if (this.Blindnesscoordinator!=null){
			xmlTxt+="\n<kblack:blindnessCoordinator";
			xmlTxt+=">";
			xmlTxt+=this.Blindnesscoordinator;
			xmlTxt+="</kblack:blindnessCoordinator>";
		}
		if (this.Pregnancytest!=null){
			xmlTxt+="\n<kblack:pregnancyTest";
			xmlTxt+=">";
			xmlTxt+=this.Pregnancytest;
			xmlTxt+="</kblack:pregnancyTest>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Subjectcode!=null) return true;
		if (this.Wasscreeningday!=null) return true;
		if (this.Studytype!=null) return true;
		if (this.Weight_units!=null)
			return true;
		if (this.Weight!=null) return true;
		if (this.Infused!=null) return true;
		if (this.Cddose!=null) return true;
		if (this.Cdtime!=null) return true;
		if (this.Infusionstarttime!=null) return true;
		if (this.Method!=null) return true;
		if (this.Loadduration!=null) return true;
		if (this.Loadrate!=null) return true;
		if (this.Maintrate!=null) return true;
		if (this.Bloodisavailable!=null) return true;
		if (this.Samplesite!=null) return true;
		if (this.Nausea!=null) return true;
		if (this.Vomiting!=null) return true;
		if (this.Sedation!=null) return true;
		if (this.Dizzy!=null) return true;
		if (this.Dyskinesias!=null) return true;
		if (this.Psychosis!=null) return true;
		if (this.Ticsworse!=null) return true;
		if (this.Ticsbetter!=null) return true;
		if (this.Othersideeffects!=null) return true;
		if (this.Problem!=null) return true;
		if (this.Knownforivld1!=null) return true;
		if (this.Rejectedforivld1!=null) return true;
		if (this.Blindnesssubject!=null) return true;
		if (this.Blindnessinvestigator!=null) return true;
		if (this.Blindnesscoordinator!=null) return true;
		if (this.Pregnancytest!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
