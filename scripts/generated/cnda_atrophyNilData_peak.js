/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cnda_atrophyNilData_peak(){
this.xsiType="cnda:atrophyNilData_peak";

	this.getSchemaElementName=function(){
		return "atrophyNilData_peak";
	}

	this.getFullSchemaElementName=function(){
		return "cnda:atrophyNilData_peak";
	}

	this.Location=null;


	function getLocation() {
		return this.Location;
	}
	this.getLocation=getLocation;


	function setLocation(v){
		this.Location=v;
	}
	this.setLocation=setLocation;

	this.Magnitude=null;


	function getMagnitude() {
		return this.Magnitude;
	}
	this.getMagnitude=getMagnitude;


	function setMagnitude(v){
		this.Magnitude=v;
	}
	this.setMagnitude=setMagnitude;

	this.PercWhite=null;


	function getPercWhite() {
		return this.PercWhite;
	}
	this.getPercWhite=getPercWhite;


	function setPercWhite(v){
		this.PercWhite=v;
	}
	this.setPercWhite=setPercWhite;

	this.Type=null;


	function getType() {
		return this.Type;
	}
	this.getType=getType;


	function setType(v){
		this.Type=v;
	}
	this.setType=setType;

	this.CndaAtrophynildataPeakId=null;


	function getCndaAtrophynildataPeakId() {
		return this.CndaAtrophynildataPeakId;
	}
	this.getCndaAtrophynildataPeakId=getCndaAtrophynildataPeakId;


	function setCndaAtrophynildataPeakId(v){
		this.CndaAtrophynildataPeakId=v;
	}
	this.setCndaAtrophynildataPeakId=setCndaAtrophynildataPeakId;

	this.peaks_peak_cnda_atrophyNilData_id_fk=null;


	this.getpeaks_peak_cnda_atrophyNilData_id=function() {
		return this.peaks_peak_cnda_atrophyNilData_id_fk;
	}


	this.setpeaks_peak_cnda_atrophyNilData_id=function(v){
		this.peaks_peak_cnda_atrophyNilData_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="location"){
				return this.Location ;
			} else 
			if(xmlPath=="magnitude"){
				return this.Magnitude ;
			} else 
			if(xmlPath=="perc_white"){
				return this.PercWhite ;
			} else 
			if(xmlPath=="type"){
				return this.Type ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="cnda_atrophyNilData_peak_id"){
				return this.CndaAtrophynildataPeakId ;
			} else 
			if(xmlPath=="peaks_peak_cnda_atrophyNilData_id"){
				return this.peaks_peak_cnda_atrophyNilData_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="location"){
				this.Location=value;
			} else 
			if(xmlPath=="magnitude"){
				this.Magnitude=value;
			} else 
			if(xmlPath=="perc_white"){
				this.PercWhite=value;
			} else 
			if(xmlPath=="type"){
				this.Type=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="cnda_atrophyNilData_peak_id"){
				this.CndaAtrophynildataPeakId=value;
			} else 
			if(xmlPath=="peaks_peak_cnda_atrophyNilData_id"){
				this.peaks_peak_cnda_atrophyNilData_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="location"){
			return "field_data";
		}else if (xmlPath=="magnitude"){
			return "field_data";
		}else if (xmlPath=="perc_white"){
			return "field_data";
		}else if (xmlPath=="type"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cnda:atrophyNilData_peak";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cnda:atrophyNilData_peak>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.CndaAtrophynildataPeakId!=null){
				if(hiddenCount++>0)str+=",";
				str+="cnda_atrophyNilData_peak_id=\"" + this.CndaAtrophynildataPeakId + "\"";
			}
			if(this.peaks_peak_cnda_atrophyNilData_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="peaks_peak_cnda_atrophyNilData_id=\"" + this.peaks_peak_cnda_atrophyNilData_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.Type!=null)
			attTxt+=" type=\"" +this.Type +"\"";
		else attTxt+=" type=\"\"";//REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Location!=null){
			xmlTxt+="\n<cnda:location";
			xmlTxt+=">";
			xmlTxt+=this.Location;
			xmlTxt+="</cnda:location>";
		}
		else{
			xmlTxt+="\n<cnda:location";
			xmlTxt+="/>";
		}

		if (this.Magnitude!=null){
			xmlTxt+="\n<cnda:magnitude";
			xmlTxt+=">";
			xmlTxt+=this.Magnitude;
			xmlTxt+="</cnda:magnitude>";
		}
		else{
			xmlTxt+="\n<cnda:magnitude";
			xmlTxt+="/>";
		}

		if (this.PercWhite!=null){
			xmlTxt+="\n<cnda:perc_white";
			xmlTxt+=">";
			xmlTxt+=this.PercWhite;
			xmlTxt+="</cnda:perc_white>";
		}
		else{
			xmlTxt+="\n<cnda:perc_white";
			xmlTxt+="/>";
		}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.CndaAtrophynildataPeakId!=null) return true;
			if (this.peaks_peak_cnda_atrophyNilData_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Location!=null) return true;
		return true;//REQUIRED location
	}
}
