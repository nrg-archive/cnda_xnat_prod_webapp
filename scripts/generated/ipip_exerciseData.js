/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function ipip_exerciseData(){
this.xsiType="ipip:exerciseData";

	this.getSchemaElementName=function(){
		return "exerciseData";
	}

	this.getFullSchemaElementName=function(){
		return "ipip:exerciseData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Walk=null;


	function getWalk() {
		return this.Walk;
	}
	this.getWalk=getWalk;


	function setWalk(v){
		this.Walk=v;
	}
	this.setWalk=setWalk;

	this.Jog=null;


	function getJog() {
		return this.Jog;
	}
	this.getJog=getJog;


	function setJog(v){
		this.Jog=v;
	}
	this.setJog=setJog;

	this.Run=null;


	function getRun() {
		return this.Run;
	}
	this.getRun=getRun;


	function setRun(v){
		this.Run=v;
	}
	this.setRun=setRun;

	this.Bike=null;


	function getBike() {
		return this.Bike;
	}
	this.getBike=getBike;


	function setBike(v){
		this.Bike=v;
	}
	this.setBike=setBike;

	this.Tennis=null;


	function getTennis() {
		return this.Tennis;
	}
	this.getTennis=getTennis;


	function setTennis(v){
		this.Tennis=v;
	}
	this.setTennis=setTennis;

	this.Swim=null;


	function getSwim() {
		return this.Swim;
	}
	this.getSwim=getSwim;


	function setSwim(v){
		this.Swim=v;
	}
	this.setSwim=setSwim;

	this.Aerobics=null;


	function getAerobics() {
		return this.Aerobics;
	}
	this.getAerobics=getAerobics;


	function setAerobics(v){
		this.Aerobics=v;
	}
	this.setAerobics=setAerobics;

	this.Lowint=null;


	function getLowint() {
		return this.Lowint;
	}
	this.getLowint=getLowint;


	function setLowint(v){
		this.Lowint=v;
	}
	this.setLowint=setLowint;

	this.Vigorous=null;


	function getVigorous() {
		return this.Vigorous;
	}
	this.getVigorous=getVigorous;


	function setVigorous(v){
		this.Vigorous=v;
	}
	this.setVigorous=setVigorous;

	this.Weight=null;


	function getWeight() {
		return this.Weight;
	}
	this.getWeight=getWeight;


	function setWeight(v){
		this.Weight=v;
	}
	this.setWeight=setWeight;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="WALK"){
				return this.Walk ;
			} else 
			if(xmlPath=="JOG"){
				return this.Jog ;
			} else 
			if(xmlPath=="RUN"){
				return this.Run ;
			} else 
			if(xmlPath=="BIKE"){
				return this.Bike ;
			} else 
			if(xmlPath=="TENNIS"){
				return this.Tennis ;
			} else 
			if(xmlPath=="SWIM"){
				return this.Swim ;
			} else 
			if(xmlPath=="AEROBICS"){
				return this.Aerobics ;
			} else 
			if(xmlPath=="LOWINT"){
				return this.Lowint ;
			} else 
			if(xmlPath=="VIGOROUS"){
				return this.Vigorous ;
			} else 
			if(xmlPath=="WEIGHT"){
				return this.Weight ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="WALK"){
				this.Walk=value;
			} else 
			if(xmlPath=="JOG"){
				this.Jog=value;
			} else 
			if(xmlPath=="RUN"){
				this.Run=value;
			} else 
			if(xmlPath=="BIKE"){
				this.Bike=value;
			} else 
			if(xmlPath=="TENNIS"){
				this.Tennis=value;
			} else 
			if(xmlPath=="SWIM"){
				this.Swim=value;
			} else 
			if(xmlPath=="AEROBICS"){
				this.Aerobics=value;
			} else 
			if(xmlPath=="LOWINT"){
				this.Lowint=value;
			} else 
			if(xmlPath=="VIGOROUS"){
				this.Vigorous=value;
			} else 
			if(xmlPath=="WEIGHT"){
				this.Weight=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="WALK"){
			return "field_data";
		}else if (xmlPath=="JOG"){
			return "field_data";
		}else if (xmlPath=="RUN"){
			return "field_data";
		}else if (xmlPath=="BIKE"){
			return "field_data";
		}else if (xmlPath=="TENNIS"){
			return "field_data";
		}else if (xmlPath=="SWIM"){
			return "field_data";
		}else if (xmlPath=="AEROBICS"){
			return "field_data";
		}else if (xmlPath=="LOWINT"){
			return "field_data";
		}else if (xmlPath=="VIGOROUS"){
			return "field_data";
		}else if (xmlPath=="WEIGHT"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<ipip:EXERCISE";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</ipip:EXERCISE>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Walk!=null){
			xmlTxt+="\n<ipip:WALK";
			xmlTxt+=">";
			xmlTxt+=this.Walk;
			xmlTxt+="</ipip:WALK>";
		}
		if (this.Jog!=null){
			xmlTxt+="\n<ipip:JOG";
			xmlTxt+=">";
			xmlTxt+=this.Jog;
			xmlTxt+="</ipip:JOG>";
		}
		if (this.Run!=null){
			xmlTxt+="\n<ipip:RUN";
			xmlTxt+=">";
			xmlTxt+=this.Run;
			xmlTxt+="</ipip:RUN>";
		}
		if (this.Bike!=null){
			xmlTxt+="\n<ipip:BIKE";
			xmlTxt+=">";
			xmlTxt+=this.Bike;
			xmlTxt+="</ipip:BIKE>";
		}
		if (this.Tennis!=null){
			xmlTxt+="\n<ipip:TENNIS";
			xmlTxt+=">";
			xmlTxt+=this.Tennis;
			xmlTxt+="</ipip:TENNIS>";
		}
		if (this.Swim!=null){
			xmlTxt+="\n<ipip:SWIM";
			xmlTxt+=">";
			xmlTxt+=this.Swim;
			xmlTxt+="</ipip:SWIM>";
		}
		if (this.Aerobics!=null){
			xmlTxt+="\n<ipip:AEROBICS";
			xmlTxt+=">";
			xmlTxt+=this.Aerobics;
			xmlTxt+="</ipip:AEROBICS>";
		}
		if (this.Lowint!=null){
			xmlTxt+="\n<ipip:LOWINT";
			xmlTxt+=">";
			xmlTxt+=this.Lowint;
			xmlTxt+="</ipip:LOWINT>";
		}
		if (this.Vigorous!=null){
			xmlTxt+="\n<ipip:VIGOROUS";
			xmlTxt+=">";
			xmlTxt+=this.Vigorous;
			xmlTxt+="</ipip:VIGOROUS>";
		}
		if (this.Weight!=null){
			xmlTxt+="\n<ipip:WEIGHT";
			xmlTxt+=">";
			xmlTxt+=this.Weight;
			xmlTxt+="</ipip:WEIGHT>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Walk!=null) return true;
		if (this.Jog!=null) return true;
		if (this.Run!=null) return true;
		if (this.Bike!=null) return true;
		if (this.Tennis!=null) return true;
		if (this.Swim!=null) return true;
		if (this.Aerobics!=null) return true;
		if (this.Lowint!=null) return true;
		if (this.Vigorous!=null) return true;
		if (this.Weight!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
