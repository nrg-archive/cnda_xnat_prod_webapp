/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function fs_aparcRegionAnalysis_hemisphere(){
this.xsiType="fs:aparcRegionAnalysis_hemisphere";

	this.getSchemaElementName=function(){
		return "aparcRegionAnalysis_hemisphere";
	}

	this.getFullSchemaElementName=function(){
		return "fs:aparcRegionAnalysis_hemisphere";
	}

	this.Numvert=null;


	function getNumvert() {
		return this.Numvert;
	}
	this.getNumvert=getNumvert;


	function setNumvert(v){
		this.Numvert=v;
	}
	this.setNumvert=setNumvert;

	this.Surfarea=null;


	function getSurfarea() {
		return this.Surfarea;
	}
	this.getSurfarea=getSurfarea;


	function setSurfarea(v){
		this.Surfarea=v;
	}
	this.setSurfarea=setSurfarea;
	this.Regions_region =new Array();

	function getRegions_region() {
		return this.Regions_region;
	}
	this.getRegions_region=getRegions_region;


	function addRegions_region(v){
		this.Regions_region.push(v);
	}
	this.addRegions_region=addRegions_region;

	this.Name=null;


	function getName() {
		return this.Name;
	}
	this.getName=getName;


	function setName(v){
		this.Name=v;
	}
	this.setName=setName;

	this.FsAparcregionanalysisHemisphereId=null;


	function getFsAparcregionanalysisHemisphereId() {
		return this.FsAparcregionanalysisHemisphereId;
	}
	this.getFsAparcregionanalysisHemisphereId=getFsAparcregionanalysisHemisphereId;


	function setFsAparcregionanalysisHemisphereId(v){
		this.FsAparcregionanalysisHemisphereId=v;
	}
	this.setFsAparcregionanalysisHemisphereId=setFsAparcregionanalysisHemisphereId;

	this.fs_aparcRegionAnalysis_id_fk=null;


	this.getfs_aparcRegionAnalysis_id=function() {
		return this.fs_aparcRegionAnalysis_id_fk;
	}


	this.setfs_aparcRegionAnalysis_id=function(v){
		this.fs_aparcRegionAnalysis_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="NumVert"){
				return this.Numvert ;
			} else 
			if(xmlPath=="SurfArea"){
				return this.Surfarea ;
			} else 
			if(xmlPath=="regions/region"){
				return this.Regions_region ;
			} else 
			if(xmlPath.startsWith("regions/region")){
				xmlPath=xmlPath.substring(14);
				if(xmlPath=="")return this.Regions_region ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Regions_region.length;whereCount++){

					var tempValue=this.Regions_region[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Regions_region[whereCount]);

					}

				}
				}else{

				whereArray=this.Regions_region;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="name"){
				return this.Name ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="fs_aparcRegionAnalysis_hemisphere_id"){
				return this.FsAparcregionanalysisHemisphereId ;
			} else 
			if(xmlPath=="fs_aparcRegionAnalysis_id"){
				return this.fs_aparcRegionAnalysis_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="NumVert"){
				this.Numvert=value;
			} else 
			if(xmlPath=="SurfArea"){
				this.Surfarea=value;
			} else 
			if(xmlPath=="regions/region"){
				this.Regions_region=value;
			} else 
			if(xmlPath.startsWith("regions/region")){
				xmlPath=xmlPath.substring(14);
				if(xmlPath=="")return this.Regions_region ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Regions_region.length;whereCount++){

					var tempValue=this.Regions_region[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Regions_region[whereCount]);

					}

				}
				}else{

				whereArray=this.Regions_region;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("fs:aparcRegionAnalysis_hemisphere_region");//omUtils.js
					}
					this.addRegions_region(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="name"){
				this.Name=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="fs_aparcRegionAnalysis_hemisphere_id"){
				this.FsAparcregionanalysisHemisphereId=value;
			} else 
			if(xmlPath=="fs_aparcRegionAnalysis_id"){
				this.fs_aparcRegionAnalysis_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="regions/region"){
			this.addRegions_region(v);
		}
		else{
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="regions/region"){
			return "http://nrg.wustl.edu/fs:aparcRegionAnalysis_hemisphere_region";
		}
		else{
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="NumVert"){
			return "field_data";
		}else if (xmlPath=="SurfArea"){
			return "field_data";
		}else if (xmlPath=="regions/region"){
			return "field_multi_reference";
		}else if (xmlPath=="name"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<fs:aparcRegionAnalysis_hemisphere";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</fs:aparcRegionAnalysis_hemisphere>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.FsAparcregionanalysisHemisphereId!=null){
				if(hiddenCount++>0)str+=",";
				str+="fs_aparcRegionAnalysis_hemisphere_id=\"" + this.FsAparcregionanalysisHemisphereId + "\"";
			}
			if(this.fs_aparcRegionAnalysis_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="fs_aparcRegionAnalysis_id=\"" + this.fs_aparcRegionAnalysis_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.Name!=null)
			attTxt+=" name=\"" +this.Name +"\"";
		else attTxt+=" name=\"\"";//REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Numvert!=null){
			xmlTxt+="\n<fs:NumVert";
			xmlTxt+=">";
			xmlTxt+=this.Numvert;
			xmlTxt+="</fs:NumVert>";
		}
		if (this.Surfarea!=null){
			xmlTxt+="\n<fs:SurfArea";
			xmlTxt+=">";
			xmlTxt+=this.Surfarea;
			xmlTxt+="</fs:SurfArea>";
		}
			var child0=0;
			var att0=0;
			child0+=this.Regions_region.length;
			if(child0>0 || att0>0){
				xmlTxt+="\n<fs:regions";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var Regions_regionCOUNT=0;Regions_regionCOUNT<this.Regions_region.length;Regions_regionCOUNT++){
			xmlTxt +="\n<fs:region";
			xmlTxt +=this.Regions_region[Regions_regionCOUNT].getXMLAtts();
			if(this.Regions_region[Regions_regionCOUNT].xsiType!="fs:aparcRegionAnalysis_hemisphere_region"){
				xmlTxt+=" xsi:type=\"" + this.Regions_region[Regions_regionCOUNT].xsiType + "\"";
			}
			if (this.Regions_region[Regions_regionCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Regions_region[Regions_regionCOUNT].getXMLBody(preventComments);
					xmlTxt+="</fs:region>";
			}else {xmlTxt+="/>";}
		}
				xmlTxt+="\n</fs:regions>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.FsAparcregionanalysisHemisphereId!=null) return true;
			if (this.fs_aparcRegionAnalysis_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Numvert!=null) return true;
		if (this.Surfarea!=null) return true;
			if(this.Regions_region.length>0)return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
