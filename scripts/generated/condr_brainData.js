/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:04 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function condr_brainData(){
this.xsiType="condr:brainData";

	this.getSchemaElementName=function(){
		return "brainData";
	}

	this.getFullSchemaElementName=function(){
		return "condr:brainData";
	}
this.extension=dynamicJSLoad('tissue_specData','generated/tissue_specData.js');

	this.Descloc=null;


	function getDescloc() {
		return this.Descloc;
	}
	this.getDescloc=getDescloc;


	function setDescloc(v){
		this.Descloc=v;
	}
	this.setDescloc=setDescloc;

	this.Coordx=null;


	function getCoordx() {
		return this.Coordx;
	}
	this.getCoordx=getCoordx;


	function setCoordx(v){
		this.Coordx=v;
	}
	this.setCoordx=setCoordx;

	this.Coordy=null;


	function getCoordy() {
		return this.Coordy;
	}
	this.getCoordy=getCoordy;


	function setCoordy(v){
		this.Coordy=v;
	}
	this.setCoordy=setCoordy;

	this.Coordz=null;


	function getCoordz() {
		return this.Coordz;
	}
	this.getCoordz=getCoordz;


	function setCoordz(v){
		this.Coordz=v;
	}
	this.setCoordz=setCoordz;

	this.Coordsource=null;


	function getCoordsource() {
		return this.Coordsource;
	}
	this.getCoordsource=getCoordsource;


	function setCoordsource(v){
		this.Coordsource=v;
	}
	this.setCoordsource=setCoordsource;

	this.Sampsplit=null;


	function getSampsplit() {
		return this.Sampsplit;
	}
	this.getSampsplit=getSampsplit;


	function setSampsplit(v){
		this.Sampsplit=v;
	}
	this.setSampsplit=setSampsplit;


	this.isSampsplit=function(defaultValue) {
		if(this.Sampsplit==null)return defaultValue;
		if(this.Sampsplit=="1" || this.Sampsplit==true)return true;
		return false;
	}

	this.Surgpath=null;


	function getSurgpath() {
		return this.Surgpath;
	}
	this.getSurgpath=getSurgpath;


	function setSurgpath(v){
		this.Surgpath=v;
	}
	this.setSurgpath=setSurgpath;


	this.isSurgpath=function(defaultValue) {
		if(this.Surgpath==null)return defaultValue;
		if(this.Surgpath=="1" || this.Surgpath==true)return true;
		return false;
	}

	this.Tumorbank=null;


	function getTumorbank() {
		return this.Tumorbank;
	}
	this.getTumorbank=getTumorbank;


	function setTumorbank(v){
		this.Tumorbank=v;
	}
	this.setTumorbank=setTumorbank;


	this.isTumorbank=function(defaultValue) {
		if(this.Tumorbank==null)return defaultValue;
		if(this.Tumorbank=="1" || this.Tumorbank==true)return true;
		return false;
	}

	this.Surgerytimenotreported=null;


	function getSurgerytimenotreported() {
		return this.Surgerytimenotreported;
	}
	this.getSurgerytimenotreported=getSurgerytimenotreported;


	function setSurgerytimenotreported(v){
		this.Surgerytimenotreported=v;
	}
	this.setSurgerytimenotreported=setSurgerytimenotreported;


	this.isSurgerytimenotreported=function(defaultValue) {
		if(this.Surgerytimenotreported==null)return defaultValue;
		if(this.Surgerytimenotreported=="1" || this.Surgerytimenotreported==true)return true;
		return false;
	}

	this.Coordcoll=null;


	function getCoordcoll() {
		return this.Coordcoll;
	}
	this.getCoordcoll=getCoordcoll;


	function setCoordcoll(v){
		this.Coordcoll=v;
	}
	this.setCoordcoll=setCoordcoll;


	this.isCoordcoll=function(defaultValue) {
		if(this.Coordcoll==null)return defaultValue;
		if(this.Coordcoll=="1" || this.Coordcoll==true)return true;
		return false;
	}

	this.Screencapcoll=null;


	function getScreencapcoll() {
		return this.Screencapcoll;
	}
	this.getScreencapcoll=getScreencapcoll;


	function setScreencapcoll(v){
		this.Screencapcoll=v;
	}
	this.setScreencapcoll=setScreencapcoll;


	this.isScreencapcoll=function(defaultValue) {
		if(this.Screencapcoll==null)return defaultValue;
		if(this.Screencapcoll=="1" || this.Screencapcoll==true)return true;
		return false;
	}

	this.Rgraphapp=null;


	function getRgraphapp() {
		return this.Rgraphapp;
	}
	this.getRgraphapp=getRgraphapp;


	function setRgraphapp(v){
		this.Rgraphapp=v;
	}
	this.setRgraphapp=setRgraphapp;

	this.Rgraphappnote=null;


	function getRgraphappnote() {
		return this.Rgraphappnote;
	}
	this.getRgraphappnote=getRgraphappnote;


	function setRgraphappnote(v){
		this.Rgraphappnote=v;
	}
	this.setRgraphappnote=setRgraphappnote;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="specData"){
				return this.Specdata ;
			} else 
			if(xmlPath.startsWith("specData")){
				xmlPath=xmlPath.substring(8);
				if(xmlPath=="")return this.Specdata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Specdata!=undefined)return this.Specdata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="descLoc"){
				return this.Descloc ;
			} else 
			if(xmlPath=="coordX"){
				return this.Coordx ;
			} else 
			if(xmlPath=="coordY"){
				return this.Coordy ;
			} else 
			if(xmlPath=="coordZ"){
				return this.Coordz ;
			} else 
			if(xmlPath=="coordSource"){
				return this.Coordsource ;
			} else 
			if(xmlPath=="sampSplit"){
				return this.Sampsplit ;
			} else 
			if(xmlPath=="surgPath"){
				return this.Surgpath ;
			} else 
			if(xmlPath=="tumorBank"){
				return this.Tumorbank ;
			} else 
			if(xmlPath=="surgeryTimeNotReported"){
				return this.Surgerytimenotreported ;
			} else 
			if(xmlPath=="coordColl"){
				return this.Coordcoll ;
			} else 
			if(xmlPath=="screenCapColl"){
				return this.Screencapcoll ;
			} else 
			if(xmlPath=="rgraphApp"){
				return this.Rgraphapp ;
			} else 
			if(xmlPath=="rgraphAppNote"){
				return this.Rgraphappnote ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="specData"){
				this.Specdata=value;
			} else 
			if(xmlPath.startsWith("specData")){
				xmlPath=xmlPath.substring(8);
				if(xmlPath=="")return this.Specdata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Specdata!=undefined){
					this.Specdata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Specdata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Specdata= instanciateObject("tissue:specData");//omUtils.js
						}
						if(options && options.where)this.Specdata.setProperty(options.where.field,options.where.value);
						this.Specdata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="descLoc"){
				this.Descloc=value;
			} else 
			if(xmlPath=="coordX"){
				this.Coordx=value;
			} else 
			if(xmlPath=="coordY"){
				this.Coordy=value;
			} else 
			if(xmlPath=="coordZ"){
				this.Coordz=value;
			} else 
			if(xmlPath=="coordSource"){
				this.Coordsource=value;
			} else 
			if(xmlPath=="sampSplit"){
				this.Sampsplit=value;
			} else 
			if(xmlPath=="surgPath"){
				this.Surgpath=value;
			} else 
			if(xmlPath=="tumorBank"){
				this.Tumorbank=value;
			} else 
			if(xmlPath=="surgeryTimeNotReported"){
				this.Surgerytimenotreported=value;
			} else 
			if(xmlPath=="coordColl"){
				this.Coordcoll=value;
			} else 
			if(xmlPath=="screenCapColl"){
				this.Screencapcoll=value;
			} else 
			if(xmlPath=="rgraphApp"){
				this.Rgraphapp=value;
			} else 
			if(xmlPath=="rgraphAppNote"){
				this.Rgraphappnote=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="descLoc"){
			return "field_data";
		}else if (xmlPath=="coordX"){
			return "field_data";
		}else if (xmlPath=="coordY"){
			return "field_data";
		}else if (xmlPath=="coordZ"){
			return "field_data";
		}else if (xmlPath=="coordSource"){
			return "field_data";
		}else if (xmlPath=="sampSplit"){
			return "field_data";
		}else if (xmlPath=="surgPath"){
			return "field_data";
		}else if (xmlPath=="tumorBank"){
			return "field_data";
		}else if (xmlPath=="surgeryTimeNotReported"){
			return "field_data";
		}else if (xmlPath=="coordColl"){
			return "field_data";
		}else if (xmlPath=="screenCapColl"){
			return "field_data";
		}else if (xmlPath=="rgraphApp"){
			return "field_data";
		}else if (xmlPath=="rgraphAppNote"){
			return "field_LONG_DATA";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<condr:Brain";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</condr:Brain>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Descloc!=null){
			xmlTxt+="\n<condr:descLoc";
			xmlTxt+=">";
			xmlTxt+=this.Descloc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:descLoc>";
		}
		if (this.Coordx!=null){
			xmlTxt+="\n<condr:coordX";
			xmlTxt+=">";
			xmlTxt+=this.Coordx;
			xmlTxt+="</condr:coordX>";
		}
		if (this.Coordy!=null){
			xmlTxt+="\n<condr:coordY";
			xmlTxt+=">";
			xmlTxt+=this.Coordy;
			xmlTxt+="</condr:coordY>";
		}
		if (this.Coordz!=null){
			xmlTxt+="\n<condr:coordZ";
			xmlTxt+=">";
			xmlTxt+=this.Coordz;
			xmlTxt+="</condr:coordZ>";
		}
		if (this.Coordsource!=null){
			xmlTxt+="\n<condr:coordSource";
			xmlTxt+=">";
			xmlTxt+=this.Coordsource.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:coordSource>";
		}
		if (this.Sampsplit!=null){
			xmlTxt+="\n<condr:sampSplit";
			xmlTxt+=">";
			xmlTxt+=this.Sampsplit;
			xmlTxt+="</condr:sampSplit>";
		}
		if (this.Surgpath!=null){
			xmlTxt+="\n<condr:surgPath";
			xmlTxt+=">";
			xmlTxt+=this.Surgpath;
			xmlTxt+="</condr:surgPath>";
		}
		if (this.Tumorbank!=null){
			xmlTxt+="\n<condr:tumorBank";
			xmlTxt+=">";
			xmlTxt+=this.Tumorbank;
			xmlTxt+="</condr:tumorBank>";
		}
		if (this.Surgerytimenotreported!=null){
			xmlTxt+="\n<condr:surgeryTimeNotReported";
			xmlTxt+=">";
			xmlTxt+=this.Surgerytimenotreported;
			xmlTxt+="</condr:surgeryTimeNotReported>";
		}
		if (this.Coordcoll!=null){
			xmlTxt+="\n<condr:coordColl";
			xmlTxt+=">";
			xmlTxt+=this.Coordcoll;
			xmlTxt+="</condr:coordColl>";
		}
		if (this.Screencapcoll!=null){
			xmlTxt+="\n<condr:screenCapColl";
			xmlTxt+=">";
			xmlTxt+=this.Screencapcoll;
			xmlTxt+="</condr:screenCapColl>";
		}
		if (this.Rgraphapp!=null){
			xmlTxt+="\n<condr:rgraphApp";
			xmlTxt+=">";
			xmlTxt+=this.Rgraphapp.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:rgraphApp>";
		}
		if (this.Rgraphappnote!=null){
			xmlTxt+="\n<condr:rgraphAppNote";
			xmlTxt+=">";
			xmlTxt+=this.Rgraphappnote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:rgraphAppNote>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Descloc!=null) return true;
		if (this.Coordx!=null) return true;
		if (this.Coordy!=null) return true;
		if (this.Coordz!=null) return true;
		if (this.Coordsource!=null) return true;
		if (this.Sampsplit!=null) return true;
		if (this.Surgpath!=null) return true;
		if (this.Tumorbank!=null) return true;
		if (this.Surgerytimenotreported!=null) return true;
		if (this.Coordcoll!=null) return true;
		if (this.Screencapcoll!=null) return true;
		if (this.Rgraphapp!=null) return true;
		if (this.Rgraphappnote!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
