/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function dian_dianValidationData(){
this.xsiType="dian:dianValidationData";

	this.getSchemaElementName=function(){
		return "dianValidationData";
	}

	this.getFullSchemaElementName=function(){
		return "dian:dianValidationData";
	}
this.extension=dynamicJSLoad('xnat_validationData','generated/xnat_validationData.js');

	this.Id=null;


	function getId() {
		return this.Id;
	}
	this.getId=getId;


	function setId(v){
		this.Id=v;
	}
	this.setId=setId;

	this.Sx=null;


	function getSx() {
		return this.Sx;
	}
	this.getSx=getSx;


	function setSx(v){
		this.Sx=v;
	}
	this.setSx=setSx;

	this.Rid=null;


	function getRid() {
		return this.Rid;
	}
	this.getRid=getRid;


	function setRid(v){
		this.Rid=v;
	}
	this.setRid=setRid;

	this.Siteid=null;


	function getSiteid() {
		return this.Siteid;
	}
	this.getSiteid=getSiteid;


	function setSiteid(v){
		this.Siteid=v;
	}
	this.setSiteid=setSiteid;

	this.Viscode=null;


	function getViscode() {
		return this.Viscode;
	}
	this.getViscode=getViscode;


	function setViscode(v){
		this.Viscode=v;
	}
	this.setViscode=setViscode;

	this.Entry=null;


	function getEntry() {
		return this.Entry;
	}
	this.getEntry=getEntry;


	function setEntry(v){
		this.Entry=v;
	}
	this.setEntry=setEntry;

	this.Verify=null;


	function getVerify() {
		return this.Verify;
	}
	this.getVerify=getVerify;


	function setVerify(v){
		this.Verify=v;
	}
	this.setVerify=setVerify;

	this.Userid=null;


	function getUserid() {
		return this.Userid;
	}
	this.getUserid=getUserid;


	function setUserid(v){
		this.Userid=v;
	}
	this.setUserid=setUserid;

	this.Userdate=null;


	function getUserdate() {
		return this.Userdate;
	}
	this.getUserdate=getUserdate;


	function setUserdate(v){
		this.Userdate=v;
	}
	this.setUserdate=setUserdate;

	this.Userid2=null;


	function getUserid2() {
		return this.Userid2;
	}
	this.getUserid2=getUserid2;


	function setUserid2(v){
		this.Userid2=v;
	}
	this.setUserid2=setUserid2;

	this.Userdate2=null;


	function getUserdate2() {
		return this.Userdate2;
	}
	this.getUserdate2=getUserdate2;


	function setUserdate2(v){
		this.Userdate2=v;
	}
	this.setUserdate2=setUserdate2;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="validationData"){
				return this.Validationdata ;
			} else 
			if(xmlPath.startsWith("validationData")){
				xmlPath=xmlPath.substring(14);
				if(xmlPath=="")return this.Validationdata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Validationdata!=undefined)return this.Validationdata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="ID"){
				return this.Id ;
			} else 
			if(xmlPath=="SX"){
				return this.Sx ;
			} else 
			if(xmlPath=="RID"){
				return this.Rid ;
			} else 
			if(xmlPath=="SITEID"){
				return this.Siteid ;
			} else 
			if(xmlPath=="VISCODE"){
				return this.Viscode ;
			} else 
			if(xmlPath=="ENTRY"){
				return this.Entry ;
			} else 
			if(xmlPath=="VERIFY"){
				return this.Verify ;
			} else 
			if(xmlPath=="USERID"){
				return this.Userid ;
			} else 
			if(xmlPath=="USERDATE"){
				return this.Userdate ;
			} else 
			if(xmlPath=="USERID2"){
				return this.Userid2 ;
			} else 
			if(xmlPath=="USERDATE2"){
				return this.Userdate2 ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="validationData"){
				this.Validationdata=value;
			} else 
			if(xmlPath.startsWith("validationData")){
				xmlPath=xmlPath.substring(14);
				if(xmlPath=="")return this.Validationdata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Validationdata!=undefined){
					this.Validationdata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Validationdata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Validationdata= instanciateObject("xnat:validationData");//omUtils.js
						}
						if(options && options.where)this.Validationdata.setProperty(options.where.field,options.where.value);
						this.Validationdata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="ID"){
				this.Id=value;
			} else 
			if(xmlPath=="SX"){
				this.Sx=value;
			} else 
			if(xmlPath=="RID"){
				this.Rid=value;
			} else 
			if(xmlPath=="SITEID"){
				this.Siteid=value;
			} else 
			if(xmlPath=="VISCODE"){
				this.Viscode=value;
			} else 
			if(xmlPath=="ENTRY"){
				this.Entry=value;
			} else 
			if(xmlPath=="VERIFY"){
				this.Verify=value;
			} else 
			if(xmlPath=="USERID"){
				this.Userid=value;
			} else 
			if(xmlPath=="USERDATE"){
				this.Userdate=value;
			} else 
			if(xmlPath=="USERID2"){
				this.Userid2=value;
			} else 
			if(xmlPath=="USERDATE2"){
				this.Userdate2=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="ID"){
			return "field_data";
		}else if (xmlPath=="SX"){
			return "field_data";
		}else if (xmlPath=="RID"){
			return "field_data";
		}else if (xmlPath=="SITEID"){
			return "field_data";
		}else if (xmlPath=="VISCODE"){
			return "field_data";
		}else if (xmlPath=="ENTRY"){
			return "field_data";
		}else if (xmlPath=="VERIFY"){
			return "field_data";
		}else if (xmlPath=="USERID"){
			return "field_data";
		}else if (xmlPath=="USERDATE"){
			return "field_data";
		}else if (xmlPath=="USERID2"){
			return "field_data";
		}else if (xmlPath=="USERDATE2"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<dian:dianValidationData";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</dian:dianValidationData>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Id!=null){
			xmlTxt+="\n<dian:ID";
			xmlTxt+=">";
			xmlTxt+=this.Id;
			xmlTxt+="</dian:ID>";
		}
		if (this.Sx!=null){
			xmlTxt+="\n<dian:SX";
			xmlTxt+=">";
			xmlTxt+=this.Sx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:SX>";
		}
		if (this.Rid!=null){
			xmlTxt+="\n<dian:RID";
			xmlTxt+=">";
			xmlTxt+=this.Rid;
			xmlTxt+="</dian:RID>";
		}
		if (this.Siteid!=null){
			xmlTxt+="\n<dian:SITEID";
			xmlTxt+=">";
			xmlTxt+=this.Siteid;
			xmlTxt+="</dian:SITEID>";
		}
		if (this.Viscode!=null){
			xmlTxt+="\n<dian:VISCODE";
			xmlTxt+=">";
			xmlTxt+=this.Viscode.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:VISCODE>";
		}
		if (this.Entry!=null){
			xmlTxt+="\n<dian:ENTRY";
			xmlTxt+=">";
			xmlTxt+=this.Entry;
			xmlTxt+="</dian:ENTRY>";
		}
		if (this.Verify!=null){
			xmlTxt+="\n<dian:VERIFY";
			xmlTxt+=">";
			xmlTxt+=this.Verify;
			xmlTxt+="</dian:VERIFY>";
		}
		if (this.Userid!=null){
			xmlTxt+="\n<dian:USERID";
			xmlTxt+=">";
			xmlTxt+=this.Userid.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:USERID>";
		}
		if (this.Userdate!=null){
			xmlTxt+="\n<dian:USERDATE";
			xmlTxt+=">";
			xmlTxt+=this.Userdate;
			xmlTxt+="</dian:USERDATE>";
		}
		if (this.Userid2!=null){
			xmlTxt+="\n<dian:USERID2";
			xmlTxt+=">";
			xmlTxt+=this.Userid2.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:USERID2>";
		}
		if (this.Userdate2!=null){
			xmlTxt+="\n<dian:USERDATE2";
			xmlTxt+=">";
			xmlTxt+=this.Userdate2;
			xmlTxt+="</dian:USERDATE2>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Id!=null) return true;
		if (this.Sx!=null) return true;
		if (this.Rid!=null) return true;
		if (this.Siteid!=null) return true;
		if (this.Viscode!=null) return true;
		if (this.Entry!=null) return true;
		if (this.Verify!=null) return true;
		if (this.Userid!=null) return true;
		if (this.Userdate!=null) return true;
		if (this.Userid2!=null) return true;
		if (this.Userdate2!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
