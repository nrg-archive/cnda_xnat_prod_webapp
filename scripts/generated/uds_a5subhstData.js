/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:04 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function uds_a5subhstData(){
this.xsiType="uds:a5subhstData";

	this.getSchemaElementName=function(){
		return "a5subhstData";
	}

	this.getFullSchemaElementName=function(){
		return "uds:a5subhstData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Examdate=null;


	function getExamdate() {
		return this.Examdate;
	}
	this.getExamdate=getExamdate;


	function setExamdate(v){
		this.Examdate=v;
	}
	this.setExamdate=setExamdate;

	this.Cvhatt=null;


	function getCvhatt() {
		return this.Cvhatt;
	}
	this.getCvhatt=getCvhatt;


	function setCvhatt(v){
		this.Cvhatt=v;
	}
	this.setCvhatt=setCvhatt;

	this.Cvafib=null;


	function getCvafib() {
		return this.Cvafib;
	}
	this.getCvafib=getCvafib;


	function setCvafib(v){
		this.Cvafib=v;
	}
	this.setCvafib=setCvafib;

	this.Cvangio=null;


	function getCvangio() {
		return this.Cvangio;
	}
	this.getCvangio=getCvangio;


	function setCvangio(v){
		this.Cvangio=v;
	}
	this.setCvangio=setCvangio;

	this.Cvbypass=null;


	function getCvbypass() {
		return this.Cvbypass;
	}
	this.getCvbypass=getCvbypass;


	function setCvbypass(v){
		this.Cvbypass=v;
	}
	this.setCvbypass=setCvbypass;

	this.Cvpace=null;


	function getCvpace() {
		return this.Cvpace;
	}
	this.getCvpace=getCvpace;


	function setCvpace(v){
		this.Cvpace=v;
	}
	this.setCvpace=setCvpace;

	this.Cvchf=null;


	function getCvchf() {
		return this.Cvchf;
	}
	this.getCvchf=getCvchf;


	function setCvchf(v){
		this.Cvchf=v;
	}
	this.setCvchf=setCvchf;

	this.Cvothr=null;


	function getCvothr() {
		return this.Cvothr;
	}
	this.getCvothr=getCvothr;


	function setCvothr(v){
		this.Cvothr=v;
	}
	this.setCvothr=setCvothr;

	this.Cvothrx=null;


	function getCvothrx() {
		return this.Cvothrx;
	}
	this.getCvothrx=getCvothrx;


	function setCvothrx(v){
		this.Cvothrx=v;
	}
	this.setCvothrx=setCvothrx;

	this.Cbstroke=null;


	function getCbstroke() {
		return this.Cbstroke;
	}
	this.getCbstroke=getCbstroke;


	function setCbstroke(v){
		this.Cbstroke=v;
	}
	this.setCbstroke=setCbstroke;

	this.Strok1yr=null;


	function getStrok1yr() {
		return this.Strok1yr;
	}
	this.getStrok1yr=getStrok1yr;


	function setStrok1yr(v){
		this.Strok1yr=v;
	}
	this.setStrok1yr=setStrok1yr;

	this.Strok2yr=null;


	function getStrok2yr() {
		return this.Strok2yr;
	}
	this.getStrok2yr=getStrok2yr;


	function setStrok2yr(v){
		this.Strok2yr=v;
	}
	this.setStrok2yr=setStrok2yr;

	this.Strok3yr=null;


	function getStrok3yr() {
		return this.Strok3yr;
	}
	this.getStrok3yr=getStrok3yr;


	function setStrok3yr(v){
		this.Strok3yr=v;
	}
	this.setStrok3yr=setStrok3yr;

	this.Strok4yr=null;


	function getStrok4yr() {
		return this.Strok4yr;
	}
	this.getStrok4yr=getStrok4yr;


	function setStrok4yr(v){
		this.Strok4yr=v;
	}
	this.setStrok4yr=setStrok4yr;

	this.Strok5yr=null;


	function getStrok5yr() {
		return this.Strok5yr;
	}
	this.getStrok5yr=getStrok5yr;


	function setStrok5yr(v){
		this.Strok5yr=v;
	}
	this.setStrok5yr=setStrok5yr;

	this.Strok6yr=null;


	function getStrok6yr() {
		return this.Strok6yr;
	}
	this.getStrok6yr=getStrok6yr;


	function setStrok6yr(v){
		this.Strok6yr=v;
	}
	this.setStrok6yr=setStrok6yr;

	this.Cbtia=null;


	function getCbtia() {
		return this.Cbtia;
	}
	this.getCbtia=getCbtia;


	function setCbtia(v){
		this.Cbtia=v;
	}
	this.setCbtia=setCbtia;

	this.Tia1yr=null;


	function getTia1yr() {
		return this.Tia1yr;
	}
	this.getTia1yr=getTia1yr;


	function setTia1yr(v){
		this.Tia1yr=v;
	}
	this.setTia1yr=setTia1yr;

	this.Tia2yr=null;


	function getTia2yr() {
		return this.Tia2yr;
	}
	this.getTia2yr=getTia2yr;


	function setTia2yr(v){
		this.Tia2yr=v;
	}
	this.setTia2yr=setTia2yr;

	this.Tia3yr=null;


	function getTia3yr() {
		return this.Tia3yr;
	}
	this.getTia3yr=getTia3yr;


	function setTia3yr(v){
		this.Tia3yr=v;
	}
	this.setTia3yr=setTia3yr;

	this.Tia4yr=null;


	function getTia4yr() {
		return this.Tia4yr;
	}
	this.getTia4yr=getTia4yr;


	function setTia4yr(v){
		this.Tia4yr=v;
	}
	this.setTia4yr=setTia4yr;

	this.Tia5yr=null;


	function getTia5yr() {
		return this.Tia5yr;
	}
	this.getTia5yr=getTia5yr;


	function setTia5yr(v){
		this.Tia5yr=v;
	}
	this.setTia5yr=setTia5yr;

	this.Tia6yr=null;


	function getTia6yr() {
		return this.Tia6yr;
	}
	this.getTia6yr=getTia6yr;


	function setTia6yr(v){
		this.Tia6yr=v;
	}
	this.setTia6yr=setTia6yr;

	this.Cbothr=null;


	function getCbothr() {
		return this.Cbothr;
	}
	this.getCbothr=getCbothr;


	function setCbothr(v){
		this.Cbothr=v;
	}
	this.setCbothr=setCbothr;

	this.Cbothrx=null;


	function getCbothrx() {
		return this.Cbothrx;
	}
	this.getCbothrx=getCbothrx;


	function setCbothrx(v){
		this.Cbothrx=v;
	}
	this.setCbothrx=setCbothrx;

	this.Pd=null;


	function getPd() {
		return this.Pd;
	}
	this.getPd=getPd;


	function setPd(v){
		this.Pd=v;
	}
	this.setPd=setPd;

	this.Pdyr=null;


	function getPdyr() {
		return this.Pdyr;
	}
	this.getPdyr=getPdyr;


	function setPdyr(v){
		this.Pdyr=v;
	}
	this.setPdyr=setPdyr;

	this.Pdothr=null;


	function getPdothr() {
		return this.Pdothr;
	}
	this.getPdothr=getPdothr;


	function setPdothr(v){
		this.Pdothr=v;
	}
	this.setPdothr=setPdothr;

	this.Pdothryr=null;


	function getPdothryr() {
		return this.Pdothryr;
	}
	this.getPdothryr=getPdothryr;


	function setPdothryr(v){
		this.Pdothryr=v;
	}
	this.setPdothryr=setPdothryr;

	this.Seizures=null;


	function getSeizures() {
		return this.Seizures;
	}
	this.getSeizures=getSeizures;


	function setSeizures(v){
		this.Seizures=v;
	}
	this.setSeizures=setSeizures;

	this.Traumbrf=null;


	function getTraumbrf() {
		return this.Traumbrf;
	}
	this.getTraumbrf=getTraumbrf;


	function setTraumbrf(v){
		this.Traumbrf=v;
	}
	this.setTraumbrf=setTraumbrf;

	this.Traumext=null;


	function getTraumext() {
		return this.Traumext;
	}
	this.getTraumext=getTraumext;


	function setTraumext(v){
		this.Traumext=v;
	}
	this.setTraumext=setTraumext;

	this.Traumchr=null;


	function getTraumchr() {
		return this.Traumchr;
	}
	this.getTraumchr=getTraumchr;


	function setTraumchr(v){
		this.Traumchr=v;
	}
	this.setTraumchr=setTraumchr;

	this.Ncothr=null;


	function getNcothr() {
		return this.Ncothr;
	}
	this.getNcothr=getNcothr;


	function setNcothr(v){
		this.Ncothr=v;
	}
	this.setNcothr=setNcothr;

	this.Ncothrx=null;


	function getNcothrx() {
		return this.Ncothrx;
	}
	this.getNcothrx=getNcothrx;


	function setNcothrx(v){
		this.Ncothrx=v;
	}
	this.setNcothrx=setNcothrx;

	this.Hyperten=null;


	function getHyperten() {
		return this.Hyperten;
	}
	this.getHyperten=getHyperten;


	function setHyperten(v){
		this.Hyperten=v;
	}
	this.setHyperten=setHyperten;

	this.Hypercho=null;


	function getHypercho() {
		return this.Hypercho;
	}
	this.getHypercho=getHypercho;


	function setHypercho(v){
		this.Hypercho=v;
	}
	this.setHypercho=setHypercho;

	this.Diabetes=null;


	function getDiabetes() {
		return this.Diabetes;
	}
	this.getDiabetes=getDiabetes;


	function setDiabetes(v){
		this.Diabetes=v;
	}
	this.setDiabetes=setDiabetes;

	this.B12def=null;


	function getB12def() {
		return this.B12def;
	}
	this.getB12def=getB12def;


	function setB12def(v){
		this.B12def=v;
	}
	this.setB12def=setB12def;

	this.Thyroid=null;


	function getThyroid() {
		return this.Thyroid;
	}
	this.getThyroid=getThyroid;


	function setThyroid(v){
		this.Thyroid=v;
	}
	this.setThyroid=setThyroid;

	this.Incontu=null;


	function getIncontu() {
		return this.Incontu;
	}
	this.getIncontu=getIncontu;


	function setIncontu(v){
		this.Incontu=v;
	}
	this.setIncontu=setIncontu;

	this.Incontf=null;


	function getIncontf() {
		return this.Incontf;
	}
	this.getIncontf=getIncontf;


	function setIncontf(v){
		this.Incontf=v;
	}
	this.setIncontf=setIncontf;

	this.Dep2yrs=null;


	function getDep2yrs() {
		return this.Dep2yrs;
	}
	this.getDep2yrs=getDep2yrs;


	function setDep2yrs(v){
		this.Dep2yrs=v;
	}
	this.setDep2yrs=setDep2yrs;

	this.Depothr=null;


	function getDepothr() {
		return this.Depothr;
	}
	this.getDepothr=getDepothr;


	function setDepothr(v){
		this.Depothr=v;
	}
	this.setDepothr=setDepothr;

	this.Alcohol=null;


	function getAlcohol() {
		return this.Alcohol;
	}
	this.getAlcohol=getAlcohol;


	function setAlcohol(v){
		this.Alcohol=v;
	}
	this.setAlcohol=setAlcohol;

	this.Tobac30=null;


	function getTobac30() {
		return this.Tobac30;
	}
	this.getTobac30=getTobac30;


	function setTobac30(v){
		this.Tobac30=v;
	}
	this.setTobac30=setTobac30;

	this.Tobac100=null;


	function getTobac100() {
		return this.Tobac100;
	}
	this.getTobac100=getTobac100;


	function setTobac100(v){
		this.Tobac100=v;
	}
	this.setTobac100=setTobac100;

	this.Smokyrs=null;


	function getSmokyrs() {
		return this.Smokyrs;
	}
	this.getSmokyrs=getSmokyrs;


	function setSmokyrs(v){
		this.Smokyrs=v;
	}
	this.setSmokyrs=setSmokyrs;

	this.Packsper=null;


	function getPacksper() {
		return this.Packsper;
	}
	this.getPacksper=getPacksper;


	function setPacksper(v){
		this.Packsper=v;
	}
	this.setPacksper=setPacksper;

	this.Quitsmok=null;


	function getQuitsmok() {
		return this.Quitsmok;
	}
	this.getQuitsmok=getQuitsmok;


	function setQuitsmok(v){
		this.Quitsmok=v;
	}
	this.setQuitsmok=setQuitsmok;

	this.Abusothr=null;


	function getAbusothr() {
		return this.Abusothr;
	}
	this.getAbusothr=getAbusothr;


	function setAbusothr(v){
		this.Abusothr=v;
	}
	this.setAbusothr=setAbusothr;

	this.Abusx=null;


	function getAbusx() {
		return this.Abusx;
	}
	this.getAbusx=getAbusx;


	function setAbusx(v){
		this.Abusx=v;
	}
	this.setAbusx=setAbusx;

	this.Psycdis=null;


	function getPsycdis() {
		return this.Psycdis;
	}
	this.getPsycdis=getPsycdis;


	function setPsycdis(v){
		this.Psycdis=v;
	}
	this.setPsycdis=setPsycdis;

	this.Psycdisx=null;


	function getPsycdisx() {
		return this.Psycdisx;
	}
	this.getPsycdisx=getPsycdisx;


	function setPsycdisx(v){
		this.Psycdisx=v;
	}
	this.setPsycdisx=setPsycdisx;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="EXAMDATE"){
				return this.Examdate ;
			} else 
			if(xmlPath=="CVHATT"){
				return this.Cvhatt ;
			} else 
			if(xmlPath=="CVAFIB"){
				return this.Cvafib ;
			} else 
			if(xmlPath=="CVANGIO"){
				return this.Cvangio ;
			} else 
			if(xmlPath=="CVBYPASS"){
				return this.Cvbypass ;
			} else 
			if(xmlPath=="CVPACE"){
				return this.Cvpace ;
			} else 
			if(xmlPath=="CVCHF"){
				return this.Cvchf ;
			} else 
			if(xmlPath=="CVOTHR"){
				return this.Cvothr ;
			} else 
			if(xmlPath=="CVOTHRX"){
				return this.Cvothrx ;
			} else 
			if(xmlPath=="CBSTROKE"){
				return this.Cbstroke ;
			} else 
			if(xmlPath=="STROK1YR"){
				return this.Strok1yr ;
			} else 
			if(xmlPath=="STROK2YR"){
				return this.Strok2yr ;
			} else 
			if(xmlPath=="STROK3YR"){
				return this.Strok3yr ;
			} else 
			if(xmlPath=="STROK4YR"){
				return this.Strok4yr ;
			} else 
			if(xmlPath=="STROK5YR"){
				return this.Strok5yr ;
			} else 
			if(xmlPath=="STROK6YR"){
				return this.Strok6yr ;
			} else 
			if(xmlPath=="CBTIA"){
				return this.Cbtia ;
			} else 
			if(xmlPath=="TIA1YR"){
				return this.Tia1yr ;
			} else 
			if(xmlPath=="TIA2YR"){
				return this.Tia2yr ;
			} else 
			if(xmlPath=="TIA3YR"){
				return this.Tia3yr ;
			} else 
			if(xmlPath=="TIA4YR"){
				return this.Tia4yr ;
			} else 
			if(xmlPath=="TIA5YR"){
				return this.Tia5yr ;
			} else 
			if(xmlPath=="TIA6YR"){
				return this.Tia6yr ;
			} else 
			if(xmlPath=="CBOTHR"){
				return this.Cbothr ;
			} else 
			if(xmlPath=="CBOTHRX"){
				return this.Cbothrx ;
			} else 
			if(xmlPath=="PD"){
				return this.Pd ;
			} else 
			if(xmlPath=="PDYR"){
				return this.Pdyr ;
			} else 
			if(xmlPath=="PDOTHR"){
				return this.Pdothr ;
			} else 
			if(xmlPath=="PDOTHRYR"){
				return this.Pdothryr ;
			} else 
			if(xmlPath=="SEIZURES"){
				return this.Seizures ;
			} else 
			if(xmlPath=="TRAUMBRF"){
				return this.Traumbrf ;
			} else 
			if(xmlPath=="TRAUMEXT"){
				return this.Traumext ;
			} else 
			if(xmlPath=="TRAUMCHR"){
				return this.Traumchr ;
			} else 
			if(xmlPath=="NCOTHR"){
				return this.Ncothr ;
			} else 
			if(xmlPath=="NCOTHRX"){
				return this.Ncothrx ;
			} else 
			if(xmlPath=="HYPERTEN"){
				return this.Hyperten ;
			} else 
			if(xmlPath=="HYPERCHO"){
				return this.Hypercho ;
			} else 
			if(xmlPath=="DIABETES"){
				return this.Diabetes ;
			} else 
			if(xmlPath=="B12DEF"){
				return this.B12def ;
			} else 
			if(xmlPath=="THYROID"){
				return this.Thyroid ;
			} else 
			if(xmlPath=="INCONTU"){
				return this.Incontu ;
			} else 
			if(xmlPath=="INCONTF"){
				return this.Incontf ;
			} else 
			if(xmlPath=="DEP2YRS"){
				return this.Dep2yrs ;
			} else 
			if(xmlPath=="DEPOTHR"){
				return this.Depothr ;
			} else 
			if(xmlPath=="ALCOHOL"){
				return this.Alcohol ;
			} else 
			if(xmlPath=="TOBAC30"){
				return this.Tobac30 ;
			} else 
			if(xmlPath=="TOBAC100"){
				return this.Tobac100 ;
			} else 
			if(xmlPath=="SMOKYRS"){
				return this.Smokyrs ;
			} else 
			if(xmlPath=="PACKSPER"){
				return this.Packsper ;
			} else 
			if(xmlPath=="QUITSMOK"){
				return this.Quitsmok ;
			} else 
			if(xmlPath=="ABUSOTHR"){
				return this.Abusothr ;
			} else 
			if(xmlPath=="ABUSX"){
				return this.Abusx ;
			} else 
			if(xmlPath=="PSYCDIS"){
				return this.Psycdis ;
			} else 
			if(xmlPath=="PSYCDISX"){
				return this.Psycdisx ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="EXAMDATE"){
				this.Examdate=value;
			} else 
			if(xmlPath=="CVHATT"){
				this.Cvhatt=value;
			} else 
			if(xmlPath=="CVAFIB"){
				this.Cvafib=value;
			} else 
			if(xmlPath=="CVANGIO"){
				this.Cvangio=value;
			} else 
			if(xmlPath=="CVBYPASS"){
				this.Cvbypass=value;
			} else 
			if(xmlPath=="CVPACE"){
				this.Cvpace=value;
			} else 
			if(xmlPath=="CVCHF"){
				this.Cvchf=value;
			} else 
			if(xmlPath=="CVOTHR"){
				this.Cvothr=value;
			} else 
			if(xmlPath=="CVOTHRX"){
				this.Cvothrx=value;
			} else 
			if(xmlPath=="CBSTROKE"){
				this.Cbstroke=value;
			} else 
			if(xmlPath=="STROK1YR"){
				this.Strok1yr=value;
			} else 
			if(xmlPath=="STROK2YR"){
				this.Strok2yr=value;
			} else 
			if(xmlPath=="STROK3YR"){
				this.Strok3yr=value;
			} else 
			if(xmlPath=="STROK4YR"){
				this.Strok4yr=value;
			} else 
			if(xmlPath=="STROK5YR"){
				this.Strok5yr=value;
			} else 
			if(xmlPath=="STROK6YR"){
				this.Strok6yr=value;
			} else 
			if(xmlPath=="CBTIA"){
				this.Cbtia=value;
			} else 
			if(xmlPath=="TIA1YR"){
				this.Tia1yr=value;
			} else 
			if(xmlPath=="TIA2YR"){
				this.Tia2yr=value;
			} else 
			if(xmlPath=="TIA3YR"){
				this.Tia3yr=value;
			} else 
			if(xmlPath=="TIA4YR"){
				this.Tia4yr=value;
			} else 
			if(xmlPath=="TIA5YR"){
				this.Tia5yr=value;
			} else 
			if(xmlPath=="TIA6YR"){
				this.Tia6yr=value;
			} else 
			if(xmlPath=="CBOTHR"){
				this.Cbothr=value;
			} else 
			if(xmlPath=="CBOTHRX"){
				this.Cbothrx=value;
			} else 
			if(xmlPath=="PD"){
				this.Pd=value;
			} else 
			if(xmlPath=="PDYR"){
				this.Pdyr=value;
			} else 
			if(xmlPath=="PDOTHR"){
				this.Pdothr=value;
			} else 
			if(xmlPath=="PDOTHRYR"){
				this.Pdothryr=value;
			} else 
			if(xmlPath=="SEIZURES"){
				this.Seizures=value;
			} else 
			if(xmlPath=="TRAUMBRF"){
				this.Traumbrf=value;
			} else 
			if(xmlPath=="TRAUMEXT"){
				this.Traumext=value;
			} else 
			if(xmlPath=="TRAUMCHR"){
				this.Traumchr=value;
			} else 
			if(xmlPath=="NCOTHR"){
				this.Ncothr=value;
			} else 
			if(xmlPath=="NCOTHRX"){
				this.Ncothrx=value;
			} else 
			if(xmlPath=="HYPERTEN"){
				this.Hyperten=value;
			} else 
			if(xmlPath=="HYPERCHO"){
				this.Hypercho=value;
			} else 
			if(xmlPath=="DIABETES"){
				this.Diabetes=value;
			} else 
			if(xmlPath=="B12DEF"){
				this.B12def=value;
			} else 
			if(xmlPath=="THYROID"){
				this.Thyroid=value;
			} else 
			if(xmlPath=="INCONTU"){
				this.Incontu=value;
			} else 
			if(xmlPath=="INCONTF"){
				this.Incontf=value;
			} else 
			if(xmlPath=="DEP2YRS"){
				this.Dep2yrs=value;
			} else 
			if(xmlPath=="DEPOTHR"){
				this.Depothr=value;
			} else 
			if(xmlPath=="ALCOHOL"){
				this.Alcohol=value;
			} else 
			if(xmlPath=="TOBAC30"){
				this.Tobac30=value;
			} else 
			if(xmlPath=="TOBAC100"){
				this.Tobac100=value;
			} else 
			if(xmlPath=="SMOKYRS"){
				this.Smokyrs=value;
			} else 
			if(xmlPath=="PACKSPER"){
				this.Packsper=value;
			} else 
			if(xmlPath=="QUITSMOK"){
				this.Quitsmok=value;
			} else 
			if(xmlPath=="ABUSOTHR"){
				this.Abusothr=value;
			} else 
			if(xmlPath=="ABUSX"){
				this.Abusx=value;
			} else 
			if(xmlPath=="PSYCDIS"){
				this.Psycdis=value;
			} else 
			if(xmlPath=="PSYCDISX"){
				this.Psycdisx=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="EXAMDATE"){
			return "field_data";
		}else if (xmlPath=="CVHATT"){
			return "field_data";
		}else if (xmlPath=="CVAFIB"){
			return "field_data";
		}else if (xmlPath=="CVANGIO"){
			return "field_data";
		}else if (xmlPath=="CVBYPASS"){
			return "field_data";
		}else if (xmlPath=="CVPACE"){
			return "field_data";
		}else if (xmlPath=="CVCHF"){
			return "field_data";
		}else if (xmlPath=="CVOTHR"){
			return "field_data";
		}else if (xmlPath=="CVOTHRX"){
			return "field_data";
		}else if (xmlPath=="CBSTROKE"){
			return "field_data";
		}else if (xmlPath=="STROK1YR"){
			return "field_data";
		}else if (xmlPath=="STROK2YR"){
			return "field_data";
		}else if (xmlPath=="STROK3YR"){
			return "field_data";
		}else if (xmlPath=="STROK4YR"){
			return "field_data";
		}else if (xmlPath=="STROK5YR"){
			return "field_data";
		}else if (xmlPath=="STROK6YR"){
			return "field_data";
		}else if (xmlPath=="CBTIA"){
			return "field_data";
		}else if (xmlPath=="TIA1YR"){
			return "field_data";
		}else if (xmlPath=="TIA2YR"){
			return "field_data";
		}else if (xmlPath=="TIA3YR"){
			return "field_data";
		}else if (xmlPath=="TIA4YR"){
			return "field_data";
		}else if (xmlPath=="TIA5YR"){
			return "field_data";
		}else if (xmlPath=="TIA6YR"){
			return "field_data";
		}else if (xmlPath=="CBOTHR"){
			return "field_data";
		}else if (xmlPath=="CBOTHRX"){
			return "field_data";
		}else if (xmlPath=="PD"){
			return "field_data";
		}else if (xmlPath=="PDYR"){
			return "field_data";
		}else if (xmlPath=="PDOTHR"){
			return "field_data";
		}else if (xmlPath=="PDOTHRYR"){
			return "field_data";
		}else if (xmlPath=="SEIZURES"){
			return "field_data";
		}else if (xmlPath=="TRAUMBRF"){
			return "field_data";
		}else if (xmlPath=="TRAUMEXT"){
			return "field_data";
		}else if (xmlPath=="TRAUMCHR"){
			return "field_data";
		}else if (xmlPath=="NCOTHR"){
			return "field_data";
		}else if (xmlPath=="NCOTHRX"){
			return "field_data";
		}else if (xmlPath=="HYPERTEN"){
			return "field_data";
		}else if (xmlPath=="HYPERCHO"){
			return "field_data";
		}else if (xmlPath=="DIABETES"){
			return "field_data";
		}else if (xmlPath=="B12DEF"){
			return "field_data";
		}else if (xmlPath=="THYROID"){
			return "field_data";
		}else if (xmlPath=="INCONTU"){
			return "field_data";
		}else if (xmlPath=="INCONTF"){
			return "field_data";
		}else if (xmlPath=="DEP2YRS"){
			return "field_data";
		}else if (xmlPath=="DEPOTHR"){
			return "field_data";
		}else if (xmlPath=="ALCOHOL"){
			return "field_data";
		}else if (xmlPath=="TOBAC30"){
			return "field_data";
		}else if (xmlPath=="TOBAC100"){
			return "field_data";
		}else if (xmlPath=="SMOKYRS"){
			return "field_data";
		}else if (xmlPath=="PACKSPER"){
			return "field_data";
		}else if (xmlPath=="QUITSMOK"){
			return "field_data";
		}else if (xmlPath=="ABUSOTHR"){
			return "field_data";
		}else if (xmlPath=="ABUSX"){
			return "field_data";
		}else if (xmlPath=="PSYCDIS"){
			return "field_data";
		}else if (xmlPath=="PSYCDISX"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<uds:A5SUBHST";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</uds:A5SUBHST>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Examdate!=null){
			xmlTxt+="\n<uds:EXAMDATE";
			xmlTxt+=">";
			xmlTxt+=this.Examdate;
			xmlTxt+="</uds:EXAMDATE>";
		}
		if (this.Cvhatt!=null){
			xmlTxt+="\n<uds:CVHATT";
			xmlTxt+=">";
			xmlTxt+=this.Cvhatt;
			xmlTxt+="</uds:CVHATT>";
		}
		if (this.Cvafib!=null){
			xmlTxt+="\n<uds:CVAFIB";
			xmlTxt+=">";
			xmlTxt+=this.Cvafib;
			xmlTxt+="</uds:CVAFIB>";
		}
		if (this.Cvangio!=null){
			xmlTxt+="\n<uds:CVANGIO";
			xmlTxt+=">";
			xmlTxt+=this.Cvangio;
			xmlTxt+="</uds:CVANGIO>";
		}
		if (this.Cvbypass!=null){
			xmlTxt+="\n<uds:CVBYPASS";
			xmlTxt+=">";
			xmlTxt+=this.Cvbypass;
			xmlTxt+="</uds:CVBYPASS>";
		}
		if (this.Cvpace!=null){
			xmlTxt+="\n<uds:CVPACE";
			xmlTxt+=">";
			xmlTxt+=this.Cvpace;
			xmlTxt+="</uds:CVPACE>";
		}
		if (this.Cvchf!=null){
			xmlTxt+="\n<uds:CVCHF";
			xmlTxt+=">";
			xmlTxt+=this.Cvchf;
			xmlTxt+="</uds:CVCHF>";
		}
		if (this.Cvothr!=null){
			xmlTxt+="\n<uds:CVOTHR";
			xmlTxt+=">";
			xmlTxt+=this.Cvothr;
			xmlTxt+="</uds:CVOTHR>";
		}
		if (this.Cvothrx!=null){
			xmlTxt+="\n<uds:CVOTHRX";
			xmlTxt+=">";
			xmlTxt+=this.Cvothrx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:CVOTHRX>";
		}
		if (this.Cbstroke!=null){
			xmlTxt+="\n<uds:CBSTROKE";
			xmlTxt+=">";
			xmlTxt+=this.Cbstroke;
			xmlTxt+="</uds:CBSTROKE>";
		}
		if (this.Strok1yr!=null){
			xmlTxt+="\n<uds:STROK1YR";
			xmlTxt+=">";
			xmlTxt+=this.Strok1yr;
			xmlTxt+="</uds:STROK1YR>";
		}
		if (this.Strok2yr!=null){
			xmlTxt+="\n<uds:STROK2YR";
			xmlTxt+=">";
			xmlTxt+=this.Strok2yr;
			xmlTxt+="</uds:STROK2YR>";
		}
		if (this.Strok3yr!=null){
			xmlTxt+="\n<uds:STROK3YR";
			xmlTxt+=">";
			xmlTxt+=this.Strok3yr;
			xmlTxt+="</uds:STROK3YR>";
		}
		if (this.Strok4yr!=null){
			xmlTxt+="\n<uds:STROK4YR";
			xmlTxt+=">";
			xmlTxt+=this.Strok4yr;
			xmlTxt+="</uds:STROK4YR>";
		}
		if (this.Strok5yr!=null){
			xmlTxt+="\n<uds:STROK5YR";
			xmlTxt+=">";
			xmlTxt+=this.Strok5yr;
			xmlTxt+="</uds:STROK5YR>";
		}
		if (this.Strok6yr!=null){
			xmlTxt+="\n<uds:STROK6YR";
			xmlTxt+=">";
			xmlTxt+=this.Strok6yr;
			xmlTxt+="</uds:STROK6YR>";
		}
		if (this.Cbtia!=null){
			xmlTxt+="\n<uds:CBTIA";
			xmlTxt+=">";
			xmlTxt+=this.Cbtia;
			xmlTxt+="</uds:CBTIA>";
		}
		if (this.Tia1yr!=null){
			xmlTxt+="\n<uds:TIA1YR";
			xmlTxt+=">";
			xmlTxt+=this.Tia1yr;
			xmlTxt+="</uds:TIA1YR>";
		}
		if (this.Tia2yr!=null){
			xmlTxt+="\n<uds:TIA2YR";
			xmlTxt+=">";
			xmlTxt+=this.Tia2yr;
			xmlTxt+="</uds:TIA2YR>";
		}
		if (this.Tia3yr!=null){
			xmlTxt+="\n<uds:TIA3YR";
			xmlTxt+=">";
			xmlTxt+=this.Tia3yr;
			xmlTxt+="</uds:TIA3YR>";
		}
		if (this.Tia4yr!=null){
			xmlTxt+="\n<uds:TIA4YR";
			xmlTxt+=">";
			xmlTxt+=this.Tia4yr;
			xmlTxt+="</uds:TIA4YR>";
		}
		if (this.Tia5yr!=null){
			xmlTxt+="\n<uds:TIA5YR";
			xmlTxt+=">";
			xmlTxt+=this.Tia5yr;
			xmlTxt+="</uds:TIA5YR>";
		}
		if (this.Tia6yr!=null){
			xmlTxt+="\n<uds:TIA6YR";
			xmlTxt+=">";
			xmlTxt+=this.Tia6yr;
			xmlTxt+="</uds:TIA6YR>";
		}
		if (this.Cbothr!=null){
			xmlTxt+="\n<uds:CBOTHR";
			xmlTxt+=">";
			xmlTxt+=this.Cbothr;
			xmlTxt+="</uds:CBOTHR>";
		}
		if (this.Cbothrx!=null){
			xmlTxt+="\n<uds:CBOTHRX";
			xmlTxt+=">";
			xmlTxt+=this.Cbothrx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:CBOTHRX>";
		}
		if (this.Pd!=null){
			xmlTxt+="\n<uds:PD";
			xmlTxt+=">";
			xmlTxt+=this.Pd;
			xmlTxt+="</uds:PD>";
		}
		if (this.Pdyr!=null){
			xmlTxt+="\n<uds:PDYR";
			xmlTxt+=">";
			xmlTxt+=this.Pdyr;
			xmlTxt+="</uds:PDYR>";
		}
		if (this.Pdothr!=null){
			xmlTxt+="\n<uds:PDOTHR";
			xmlTxt+=">";
			xmlTxt+=this.Pdothr;
			xmlTxt+="</uds:PDOTHR>";
		}
		if (this.Pdothryr!=null){
			xmlTxt+="\n<uds:PDOTHRYR";
			xmlTxt+=">";
			xmlTxt+=this.Pdothryr;
			xmlTxt+="</uds:PDOTHRYR>";
		}
		if (this.Seizures!=null){
			xmlTxt+="\n<uds:SEIZURES";
			xmlTxt+=">";
			xmlTxt+=this.Seizures;
			xmlTxt+="</uds:SEIZURES>";
		}
		if (this.Traumbrf!=null){
			xmlTxt+="\n<uds:TRAUMBRF";
			xmlTxt+=">";
			xmlTxt+=this.Traumbrf;
			xmlTxt+="</uds:TRAUMBRF>";
		}
		if (this.Traumext!=null){
			xmlTxt+="\n<uds:TRAUMEXT";
			xmlTxt+=">";
			xmlTxt+=this.Traumext;
			xmlTxt+="</uds:TRAUMEXT>";
		}
		if (this.Traumchr!=null){
			xmlTxt+="\n<uds:TRAUMCHR";
			xmlTxt+=">";
			xmlTxt+=this.Traumchr;
			xmlTxt+="</uds:TRAUMCHR>";
		}
		if (this.Ncothr!=null){
			xmlTxt+="\n<uds:NCOTHR";
			xmlTxt+=">";
			xmlTxt+=this.Ncothr;
			xmlTxt+="</uds:NCOTHR>";
		}
		if (this.Ncothrx!=null){
			xmlTxt+="\n<uds:NCOTHRX";
			xmlTxt+=">";
			xmlTxt+=this.Ncothrx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:NCOTHRX>";
		}
		if (this.Hyperten!=null){
			xmlTxt+="\n<uds:HYPERTEN";
			xmlTxt+=">";
			xmlTxt+=this.Hyperten;
			xmlTxt+="</uds:HYPERTEN>";
		}
		if (this.Hypercho!=null){
			xmlTxt+="\n<uds:HYPERCHO";
			xmlTxt+=">";
			xmlTxt+=this.Hypercho;
			xmlTxt+="</uds:HYPERCHO>";
		}
		if (this.Diabetes!=null){
			xmlTxt+="\n<uds:DIABETES";
			xmlTxt+=">";
			xmlTxt+=this.Diabetes;
			xmlTxt+="</uds:DIABETES>";
		}
		if (this.B12def!=null){
			xmlTxt+="\n<uds:B12DEF";
			xmlTxt+=">";
			xmlTxt+=this.B12def;
			xmlTxt+="</uds:B12DEF>";
		}
		if (this.Thyroid!=null){
			xmlTxt+="\n<uds:THYROID";
			xmlTxt+=">";
			xmlTxt+=this.Thyroid;
			xmlTxt+="</uds:THYROID>";
		}
		if (this.Incontu!=null){
			xmlTxt+="\n<uds:INCONTU";
			xmlTxt+=">";
			xmlTxt+=this.Incontu;
			xmlTxt+="</uds:INCONTU>";
		}
		if (this.Incontf!=null){
			xmlTxt+="\n<uds:INCONTF";
			xmlTxt+=">";
			xmlTxt+=this.Incontf;
			xmlTxt+="</uds:INCONTF>";
		}
		if (this.Dep2yrs!=null){
			xmlTxt+="\n<uds:DEP2YRS";
			xmlTxt+=">";
			xmlTxt+=this.Dep2yrs;
			xmlTxt+="</uds:DEP2YRS>";
		}
		if (this.Depothr!=null){
			xmlTxt+="\n<uds:DEPOTHR";
			xmlTxt+=">";
			xmlTxt+=this.Depothr;
			xmlTxt+="</uds:DEPOTHR>";
		}
		if (this.Alcohol!=null){
			xmlTxt+="\n<uds:ALCOHOL";
			xmlTxt+=">";
			xmlTxt+=this.Alcohol;
			xmlTxt+="</uds:ALCOHOL>";
		}
		if (this.Tobac30!=null){
			xmlTxt+="\n<uds:TOBAC30";
			xmlTxt+=">";
			xmlTxt+=this.Tobac30;
			xmlTxt+="</uds:TOBAC30>";
		}
		if (this.Tobac100!=null){
			xmlTxt+="\n<uds:TOBAC100";
			xmlTxt+=">";
			xmlTxt+=this.Tobac100;
			xmlTxt+="</uds:TOBAC100>";
		}
		if (this.Smokyrs!=null){
			xmlTxt+="\n<uds:SMOKYRS";
			xmlTxt+=">";
			xmlTxt+=this.Smokyrs;
			xmlTxt+="</uds:SMOKYRS>";
		}
		if (this.Packsper!=null){
			xmlTxt+="\n<uds:PACKSPER";
			xmlTxt+=">";
			xmlTxt+=this.Packsper;
			xmlTxt+="</uds:PACKSPER>";
		}
		if (this.Quitsmok!=null){
			xmlTxt+="\n<uds:QUITSMOK";
			xmlTxt+=">";
			xmlTxt+=this.Quitsmok;
			xmlTxt+="</uds:QUITSMOK>";
		}
		if (this.Abusothr!=null){
			xmlTxt+="\n<uds:ABUSOTHR";
			xmlTxt+=">";
			xmlTxt+=this.Abusothr;
			xmlTxt+="</uds:ABUSOTHR>";
		}
		if (this.Abusx!=null){
			xmlTxt+="\n<uds:ABUSX";
			xmlTxt+=">";
			xmlTxt+=this.Abusx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:ABUSX>";
		}
		if (this.Psycdis!=null){
			xmlTxt+="\n<uds:PSYCDIS";
			xmlTxt+=">";
			xmlTxt+=this.Psycdis;
			xmlTxt+="</uds:PSYCDIS>";
		}
		if (this.Psycdisx!=null){
			xmlTxt+="\n<uds:PSYCDISX";
			xmlTxt+=">";
			xmlTxt+=this.Psycdisx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:PSYCDISX>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Examdate!=null) return true;
		if (this.Cvhatt!=null) return true;
		if (this.Cvafib!=null) return true;
		if (this.Cvangio!=null) return true;
		if (this.Cvbypass!=null) return true;
		if (this.Cvpace!=null) return true;
		if (this.Cvchf!=null) return true;
		if (this.Cvothr!=null) return true;
		if (this.Cvothrx!=null) return true;
		if (this.Cbstroke!=null) return true;
		if (this.Strok1yr!=null) return true;
		if (this.Strok2yr!=null) return true;
		if (this.Strok3yr!=null) return true;
		if (this.Strok4yr!=null) return true;
		if (this.Strok5yr!=null) return true;
		if (this.Strok6yr!=null) return true;
		if (this.Cbtia!=null) return true;
		if (this.Tia1yr!=null) return true;
		if (this.Tia2yr!=null) return true;
		if (this.Tia3yr!=null) return true;
		if (this.Tia4yr!=null) return true;
		if (this.Tia5yr!=null) return true;
		if (this.Tia6yr!=null) return true;
		if (this.Cbothr!=null) return true;
		if (this.Cbothrx!=null) return true;
		if (this.Pd!=null) return true;
		if (this.Pdyr!=null) return true;
		if (this.Pdothr!=null) return true;
		if (this.Pdothryr!=null) return true;
		if (this.Seizures!=null) return true;
		if (this.Traumbrf!=null) return true;
		if (this.Traumext!=null) return true;
		if (this.Traumchr!=null) return true;
		if (this.Ncothr!=null) return true;
		if (this.Ncothrx!=null) return true;
		if (this.Hyperten!=null) return true;
		if (this.Hypercho!=null) return true;
		if (this.Diabetes!=null) return true;
		if (this.B12def!=null) return true;
		if (this.Thyroid!=null) return true;
		if (this.Incontu!=null) return true;
		if (this.Incontf!=null) return true;
		if (this.Dep2yrs!=null) return true;
		if (this.Depothr!=null) return true;
		if (this.Alcohol!=null) return true;
		if (this.Tobac30!=null) return true;
		if (this.Tobac100!=null) return true;
		if (this.Smokyrs!=null) return true;
		if (this.Packsper!=null) return true;
		if (this.Quitsmok!=null) return true;
		if (this.Abusothr!=null) return true;
		if (this.Abusx!=null) return true;
		if (this.Psycdis!=null) return true;
		if (this.Psycdisx!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
