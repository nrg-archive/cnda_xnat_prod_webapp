/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function ados_ados2001Module2Data(){
this.xsiType="ados:ados2001Module2Data";

	this.getSchemaElementName=function(){
		return "ados2001Module2Data";
	}

	this.getFullSchemaElementName=function(){
		return "ados:ados2001Module2Data";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Commtot=null;


	function getCommtot() {
		return this.Commtot;
	}
	this.getCommtot=getCommtot;


	function setCommtot(v){
		this.Commtot=v;
	}
	this.setCommtot=setCommtot;

	this.CommtotNote=null;


	function getCommtotNote() {
		return this.CommtotNote;
	}
	this.getCommtotNote=getCommtotNote;


	function setCommtotNote(v){
		this.CommtotNote=v;
	}
	this.setCommtotNote=setCommtotNote;

	this.Scintot=null;


	function getScintot() {
		return this.Scintot;
	}
	this.getScintot=getScintot;


	function setScintot(v){
		this.Scintot=v;
	}
	this.setScintot=setScintot;

	this.ScintotNote=null;


	function getScintotNote() {
		return this.ScintotNote;
	}
	this.getScintotNote=getScintotNote;


	function setScintotNote(v){
		this.ScintotNote=v;
	}
	this.setScintotNote=setScintotNote;

	this.Cmsitot=null;


	function getCmsitot() {
		return this.Cmsitot;
	}
	this.getCmsitot=getCmsitot;


	function setCmsitot(v){
		this.Cmsitot=v;
	}
	this.setCmsitot=setCmsitot;

	this.CmsitotNote=null;


	function getCmsitotNote() {
		return this.CmsitotNote;
	}
	this.getCmsitotNote=getCmsitotNote;


	function setCmsitotNote(v){
		this.CmsitotNote=v;
	}
	this.setCmsitotNote=setCmsitotNote;

	this.Playtot=null;


	function getPlaytot() {
		return this.Playtot;
	}
	this.getPlaytot=getPlaytot;


	function setPlaytot(v){
		this.Playtot=v;
	}
	this.setPlaytot=setPlaytot;

	this.PlaytotNote=null;


	function getPlaytotNote() {
		return this.PlaytotNote;
	}
	this.getPlaytotNote=getPlaytotNote;


	function setPlaytotNote(v){
		this.PlaytotNote=v;
	}
	this.setPlaytotNote=setPlaytotNote;

	this.Imaginetot=null;


	function getImaginetot() {
		return this.Imaginetot;
	}
	this.getImaginetot=getImaginetot;


	function setImaginetot(v){
		this.Imaginetot=v;
	}
	this.setImaginetot=setImaginetot;

	this.ImaginetotNote=null;


	function getImaginetotNote() {
		return this.ImaginetotNote;
	}
	this.getImaginetotNote=getImaginetotNote;


	function setImaginetotNote(v){
		this.ImaginetotNote=v;
	}
	this.setImaginetotNote=setImaginetotNote;

	this.Sbritot=null;


	function getSbritot() {
		return this.Sbritot;
	}
	this.getSbritot=getSbritot;


	function setSbritot(v){
		this.Sbritot=v;
	}
	this.setSbritot=setSbritot;

	this.SbritotNote=null;


	function getSbritotNote() {
		return this.SbritotNote;
	}
	this.getSbritotNote=getSbritotNote;


	function setSbritotNote(v){
		this.SbritotNote=v;
	}
	this.setSbritotNote=setSbritotNote;

	this.Scoreform_diagnosis_adosclas=null;


	function getScoreform_diagnosis_adosclas() {
		return this.Scoreform_diagnosis_adosclas;
	}
	this.getScoreform_diagnosis_adosclas=getScoreform_diagnosis_adosclas;


	function setScoreform_diagnosis_adosclas(v){
		this.Scoreform_diagnosis_adosclas=v;
	}
	this.setScoreform_diagnosis_adosclas=setScoreform_diagnosis_adosclas;

	this.Scoreform_diagnosis_overalldiag=null;


	function getScoreform_diagnosis_overalldiag() {
		return this.Scoreform_diagnosis_overalldiag;
	}
	this.getScoreform_diagnosis_overalldiag=getScoreform_diagnosis_overalldiag;


	function setScoreform_diagnosis_overalldiag(v){
		this.Scoreform_diagnosis_overalldiag=v;
	}
	this.setScoreform_diagnosis_overalldiag=setScoreform_diagnosis_overalldiag;

	this.Scoreform_observation_constrtasknote=null;


	function getScoreform_observation_constrtasknote() {
		return this.Scoreform_observation_constrtasknote;
	}
	this.getScoreform_observation_constrtasknote=getScoreform_observation_constrtasknote;


	function setScoreform_observation_constrtasknote(v){
		this.Scoreform_observation_constrtasknote=v;
	}
	this.setScoreform_observation_constrtasknote=setScoreform_observation_constrtasknote;

	this.Scoreform_observation_respname=null;


	function getScoreform_observation_respname() {
		return this.Scoreform_observation_respname;
	}
	this.getScoreform_observation_respname=getScoreform_observation_respname;


	function setScoreform_observation_respname(v){
		this.Scoreform_observation_respname=v;
	}
	this.setScoreform_observation_respname=setScoreform_observation_respname;

	this.Scoreform_observation_makebelieve=null;


	function getScoreform_observation_makebelieve() {
		return this.Scoreform_observation_makebelieve;
	}
	this.getScoreform_observation_makebelieve=getScoreform_observation_makebelieve;


	function setScoreform_observation_makebelieve(v){
		this.Scoreform_observation_makebelieve=v;
	}
	this.setScoreform_observation_makebelieve=setScoreform_observation_makebelieve;

	this.Scoreform_observation_jointplay=null;


	function getScoreform_observation_jointplay() {
		return this.Scoreform_observation_jointplay;
	}
	this.getScoreform_observation_jointplay=getScoreform_observation_jointplay;


	function setScoreform_observation_jointplay(v){
		this.Scoreform_observation_jointplay=v;
	}
	this.setScoreform_observation_jointplay=setScoreform_observation_jointplay;

	this.Scoreform_observation_convs=null;


	function getScoreform_observation_convs() {
		return this.Scoreform_observation_convs;
	}
	this.getScoreform_observation_convs=getScoreform_observation_convs;


	function setScoreform_observation_convs(v){
		this.Scoreform_observation_convs=v;
	}
	this.setScoreform_observation_convs=setScoreform_observation_convs;

	this.Scoreform_observation_respjointattn=null;


	function getScoreform_observation_respjointattn() {
		return this.Scoreform_observation_respjointattn;
	}
	this.getScoreform_observation_respjointattn=getScoreform_observation_respjointattn;


	function setScoreform_observation_respjointattn(v){
		this.Scoreform_observation_respjointattn=v;
	}
	this.setScoreform_observation_respjointattn=setScoreform_observation_respjointattn;

	this.Scoreform_observation_demonst=null;


	function getScoreform_observation_demonst() {
		return this.Scoreform_observation_demonst;
	}
	this.getScoreform_observation_demonst=getScoreform_observation_demonst;


	function setScoreform_observation_demonst(v){
		this.Scoreform_observation_demonst=v;
	}
	this.setScoreform_observation_demonst=setScoreform_observation_demonst;

	this.Scoreform_observation_descpicture=null;


	function getScoreform_observation_descpicture() {
		return this.Scoreform_observation_descpicture;
	}
	this.getScoreform_observation_descpicture=getScoreform_observation_descpicture;


	function setScoreform_observation_descpicture(v){
		this.Scoreform_observation_descpicture=v;
	}
	this.setScoreform_observation_descpicture=setScoreform_observation_descpicture;

	this.Scoreform_observation_tellstory=null;


	function getScoreform_observation_tellstory() {
		return this.Scoreform_observation_tellstory;
	}
	this.getScoreform_observation_tellstory=getScoreform_observation_tellstory;


	function setScoreform_observation_tellstory(v){
		this.Scoreform_observation_tellstory=v;
	}
	this.setScoreform_observation_tellstory=setScoreform_observation_tellstory;

	this.Scoreform_observation_freeplay=null;


	function getScoreform_observation_freeplay() {
		return this.Scoreform_observation_freeplay;
	}
	this.getScoreform_observation_freeplay=getScoreform_observation_freeplay;


	function setScoreform_observation_freeplay(v){
		this.Scoreform_observation_freeplay=v;
	}
	this.setScoreform_observation_freeplay=setScoreform_observation_freeplay;

	this.Scoreform_observation_bdayparty=null;


	function getScoreform_observation_bdayparty() {
		return this.Scoreform_observation_bdayparty;
	}
	this.getScoreform_observation_bdayparty=getScoreform_observation_bdayparty;


	function setScoreform_observation_bdayparty(v){
		this.Scoreform_observation_bdayparty=v;
	}
	this.setScoreform_observation_bdayparty=setScoreform_observation_bdayparty;

	this.Scoreform_observation_snack=null;


	function getScoreform_observation_snack() {
		return this.Scoreform_observation_snack;
	}
	this.getScoreform_observation_snack=getScoreform_observation_snack;


	function setScoreform_observation_snack(v){
		this.Scoreform_observation_snack=v;
	}
	this.setScoreform_observation_snack=setScoreform_observation_snack;

	this.Scoreform_observation_antcproutine=null;


	function getScoreform_observation_antcproutine() {
		return this.Scoreform_observation_antcproutine;
	}
	this.getScoreform_observation_antcproutine=getScoreform_observation_antcproutine;


	function setScoreform_observation_antcproutine(v){
		this.Scoreform_observation_antcproutine=v;
	}
	this.setScoreform_observation_antcproutine=setScoreform_observation_antcproutine;

	this.Scoreform_observation_bubbleplaynotes=null;


	function getScoreform_observation_bubbleplaynotes() {
		return this.Scoreform_observation_bubbleplaynotes;
	}
	this.getScoreform_observation_bubbleplaynotes=getScoreform_observation_bubbleplaynotes;


	function setScoreform_observation_bubbleplaynotes(v){
		this.Scoreform_observation_bubbleplaynotes=v;
	}
	this.setScoreform_observation_bubbleplaynotes=setScoreform_observation_bubbleplaynotes;

	this.Scoreform_observation_bubbplaystrats_pointfing=null;


	function getScoreform_observation_bubbplaystrats_pointfing() {
		return this.Scoreform_observation_bubbplaystrats_pointfing;
	}
	this.getScoreform_observation_bubbplaystrats_pointfing=getScoreform_observation_bubbplaystrats_pointfing;


	function setScoreform_observation_bubbplaystrats_pointfing(v){
		this.Scoreform_observation_bubbplaystrats_pointfing=v;
	}
	this.setScoreform_observation_bubbplaystrats_pointfing=setScoreform_observation_bubbplaystrats_pointfing;

	this.Scoreform_observation_bubbplaystrats_openreach=null;


	function getScoreform_observation_bubbplaystrats_openreach() {
		return this.Scoreform_observation_bubbplaystrats_openreach;
	}
	this.getScoreform_observation_bubbplaystrats_openreach=getScoreform_observation_bubbplaystrats_openreach;


	function setScoreform_observation_bubbplaystrats_openreach(v){
		this.Scoreform_observation_bubbplaystrats_openreach=v;
	}
	this.setScoreform_observation_bubbplaystrats_openreach=setScoreform_observation_bubbplaystrats_openreach;

	this.Scoreform_observation_bubbplaystrats_othergest=null;


	function getScoreform_observation_bubbplaystrats_othergest() {
		return this.Scoreform_observation_bubbplaystrats_othergest;
	}
	this.getScoreform_observation_bubbplaystrats_othergest=getScoreform_observation_bubbplaystrats_othergest;


	function setScoreform_observation_bubbplaystrats_othergest(v){
		this.Scoreform_observation_bubbplaystrats_othergest=v;
	}
	this.setScoreform_observation_bubbplaystrats_othergest=setScoreform_observation_bubbplaystrats_othergest;

	this.Scoreform_observation_bubbplaystrats_vocaliz=null;


	function getScoreform_observation_bubbplaystrats_vocaliz() {
		return this.Scoreform_observation_bubbplaystrats_vocaliz;
	}
	this.getScoreform_observation_bubbplaystrats_vocaliz=getScoreform_observation_bubbplaystrats_vocaliz;


	function setScoreform_observation_bubbplaystrats_vocaliz(v){
		this.Scoreform_observation_bubbplaystrats_vocaliz=v;
	}
	this.setScoreform_observation_bubbplaystrats_vocaliz=setScoreform_observation_bubbplaystrats_vocaliz;

	this.Scoreform_observation_bubbplaystrats_eyecont=null;


	function getScoreform_observation_bubbplaystrats_eyecont() {
		return this.Scoreform_observation_bubbplaystrats_eyecont;
	}
	this.getScoreform_observation_bubbplaystrats_eyecont=getScoreform_observation_bubbplaystrats_eyecont;


	function setScoreform_observation_bubbplaystrats_eyecont(v){
		this.Scoreform_observation_bubbplaystrats_eyecont=v;
	}
	this.setScoreform_observation_bubbplaystrats_eyecont=setScoreform_observation_bubbplaystrats_eyecont;

	this.Scoreform_observation_bubbplaystrats_words=null;


	function getScoreform_observation_bubbplaystrats_words() {
		return this.Scoreform_observation_bubbplaystrats_words;
	}
	this.getScoreform_observation_bubbplaystrats_words=getScoreform_observation_bubbplaystrats_words;


	function setScoreform_observation_bubbplaystrats_words(v){
		this.Scoreform_observation_bubbplaystrats_words=v;
	}
	this.setScoreform_observation_bubbplaystrats_words=setScoreform_observation_bubbplaystrats_words;

	this.Scoreform_observation_bubbplaystrats_alone=null;


	function getScoreform_observation_bubbplaystrats_alone() {
		return this.Scoreform_observation_bubbplaystrats_alone;
	}
	this.getScoreform_observation_bubbplaystrats_alone=getScoreform_observation_bubbplaystrats_alone;


	function setScoreform_observation_bubbplaystrats_alone(v){
		this.Scoreform_observation_bubbplaystrats_alone=v;
	}
	this.setScoreform_observation_bubbplaystrats_alone=setScoreform_observation_bubbplaystrats_alone;

	this.Scoreform_observation_bubbplaystrats_withvocaliz=null;


	function getScoreform_observation_bubbplaystrats_withvocaliz() {
		return this.Scoreform_observation_bubbplaystrats_withvocaliz;
	}
	this.getScoreform_observation_bubbplaystrats_withvocaliz=getScoreform_observation_bubbplaystrats_withvocaliz;


	function setScoreform_observation_bubbplaystrats_withvocaliz(v){
		this.Scoreform_observation_bubbplaystrats_withvocaliz=v;
	}
	this.setScoreform_observation_bubbplaystrats_withvocaliz=setScoreform_observation_bubbplaystrats_withvocaliz;

	this.Scoreform_observation_bubbplaystrats_nonverb=null;


	function getScoreform_observation_bubbplaystrats_nonverb() {
		return this.Scoreform_observation_bubbplaystrats_nonverb;
	}
	this.getScoreform_observation_bubbplaystrats_nonverb=getScoreform_observation_bubbplaystrats_nonverb;


	function setScoreform_observation_bubbplaystrats_nonverb(v){
		this.Scoreform_observation_bubbplaystrats_nonverb=v;
	}
	this.setScoreform_observation_bubbplaystrats_nonverb=setScoreform_observation_bubbplaystrats_nonverb;

	this.Scoreform_observation_bubbplaystrats_withpoint=null;


	function getScoreform_observation_bubbplaystrats_withpoint() {
		return this.Scoreform_observation_bubbplaystrats_withpoint;
	}
	this.getScoreform_observation_bubbplaystrats_withpoint=getScoreform_observation_bubbplaystrats_withpoint;


	function setScoreform_observation_bubbplaystrats_withpoint(v){
		this.Scoreform_observation_bubbplaystrats_withpoint=v;
	}
	this.setScoreform_observation_bubbplaystrats_withpoint=setScoreform_observation_bubbplaystrats_withpoint;

	this.Scoreform_observation_bubbplayobjs_mechanml=null;


	function getScoreform_observation_bubbplayobjs_mechanml() {
		return this.Scoreform_observation_bubbplayobjs_mechanml;
	}
	this.getScoreform_observation_bubbplayobjs_mechanml=getScoreform_observation_bubbplayobjs_mechanml;


	function setScoreform_observation_bubbplayobjs_mechanml(v){
		this.Scoreform_observation_bubbplayobjs_mechanml=v;
	}
	this.setScoreform_observation_bubbplayobjs_mechanml=setScoreform_observation_bubbplayobjs_mechanml;

	this.Scoreform_observation_bubbplayobjs_balloon=null;


	function getScoreform_observation_bubbplayobjs_balloon() {
		return this.Scoreform_observation_bubbplayobjs_balloon;
	}
	this.getScoreform_observation_bubbplayobjs_balloon=getScoreform_observation_bubbplayobjs_balloon;


	function setScoreform_observation_bubbplayobjs_balloon(v){
		this.Scoreform_observation_bubbplayobjs_balloon=v;
	}
	this.setScoreform_observation_bubbplayobjs_balloon=setScoreform_observation_bubbplayobjs_balloon;

	this.Scoreform_observation_bubbplayobjs_bubbles=null;


	function getScoreform_observation_bubbplayobjs_bubbles() {
		return this.Scoreform_observation_bubbplayobjs_bubbles;
	}
	this.getScoreform_observation_bubbplayobjs_bubbles=getScoreform_observation_bubbplayobjs_bubbles;


	function setScoreform_observation_bubbplayobjs_bubbles(v){
		this.Scoreform_observation_bubbplayobjs_bubbles=v;
	}
	this.setScoreform_observation_bubbplayobjs_bubbles=setScoreform_observation_bubbplayobjs_bubbles;

	this.Scoreform_observation_bubbplayobjs_otherobj=null;


	function getScoreform_observation_bubbplayobjs_otherobj() {
		return this.Scoreform_observation_bubbplayobjs_otherobj;
	}
	this.getScoreform_observation_bubbplayobjs_otherobj=getScoreform_observation_bubbplayobjs_otherobj;


	function setScoreform_observation_bubbplayobjs_otherobj(v){
		this.Scoreform_observation_bubbplayobjs_otherobj=v;
	}
	this.setScoreform_observation_bubbplayobjs_otherobj=setScoreform_observation_bubbplayobjs_otherobj;

	this.Scoreform_sectiona_nonechoedlang=null;


	function getScoreform_sectiona_nonechoedlang() {
		return this.Scoreform_sectiona_nonechoedlang;
	}
	this.getScoreform_sectiona_nonechoedlang=getScoreform_sectiona_nonechoedlang;


	function setScoreform_sectiona_nonechoedlang(v){
		this.Scoreform_sectiona_nonechoedlang=v;
	}
	this.setScoreform_sectiona_nonechoedlang=setScoreform_sectiona_nonechoedlang;

	this.Scoreform_sectiona_amtsocoverture=null;


	function getScoreform_sectiona_amtsocoverture() {
		return this.Scoreform_sectiona_amtsocoverture;
	}
	this.getScoreform_sectiona_amtsocoverture=getScoreform_sectiona_amtsocoverture;


	function setScoreform_sectiona_amtsocoverture(v){
		this.Scoreform_sectiona_amtsocoverture=v;
	}
	this.setScoreform_sectiona_amtsocoverture=setScoreform_sectiona_amtsocoverture;

	this.Scoreform_sectiona_speechabnorm=null;


	function getScoreform_sectiona_speechabnorm() {
		return this.Scoreform_sectiona_speechabnorm;
	}
	this.getScoreform_sectiona_speechabnorm=getScoreform_sectiona_speechabnorm;


	function setScoreform_sectiona_speechabnorm(v){
		this.Scoreform_sectiona_speechabnorm=v;
	}
	this.setScoreform_sectiona_speechabnorm=setScoreform_sectiona_speechabnorm;

	this.Scoreform_sectiona_immedecholalia=null;


	function getScoreform_sectiona_immedecholalia() {
		return this.Scoreform_sectiona_immedecholalia;
	}
	this.getScoreform_sectiona_immedecholalia=getScoreform_sectiona_immedecholalia;


	function setScoreform_sectiona_immedecholalia(v){
		this.Scoreform_sectiona_immedecholalia=v;
	}
	this.setScoreform_sectiona_immedecholalia=setScoreform_sectiona_immedecholalia;

	this.Scoreform_sectiona_stidiosyncwords=null;


	function getScoreform_sectiona_stidiosyncwords() {
		return this.Scoreform_sectiona_stidiosyncwords;
	}
	this.getScoreform_sectiona_stidiosyncwords=getScoreform_sectiona_stidiosyncwords;


	function setScoreform_sectiona_stidiosyncwords(v){
		this.Scoreform_sectiona_stidiosyncwords=v;
	}
	this.setScoreform_sectiona_stidiosyncwords=setScoreform_sectiona_stidiosyncwords;

	this.Scoreform_sectiona_conversation=null;


	function getScoreform_sectiona_conversation() {
		return this.Scoreform_sectiona_conversation;
	}
	this.getScoreform_sectiona_conversation=getScoreform_sectiona_conversation;


	function setScoreform_sectiona_conversation(v){
		this.Scoreform_sectiona_conversation=v;
	}
	this.setScoreform_sectiona_conversation=setScoreform_sectiona_conversation;

	this.Scoreform_sectiona_pointing=null;


	function getScoreform_sectiona_pointing() {
		return this.Scoreform_sectiona_pointing;
	}
	this.getScoreform_sectiona_pointing=getScoreform_sectiona_pointing;


	function setScoreform_sectiona_pointing(v){
		this.Scoreform_sectiona_pointing=v;
	}
	this.setScoreform_sectiona_pointing=setScoreform_sectiona_pointing;

	this.Scoreform_sectiona_descinformgest=null;


	function getScoreform_sectiona_descinformgest() {
		return this.Scoreform_sectiona_descinformgest;
	}
	this.getScoreform_sectiona_descinformgest=getScoreform_sectiona_descinformgest;


	function setScoreform_sectiona_descinformgest(v){
		this.Scoreform_sectiona_descinformgest=v;
	}
	this.setScoreform_sectiona_descinformgest=setScoreform_sectiona_descinformgest;

	this.Scoreform_sectionb_unusleyecont=null;


	function getScoreform_sectionb_unusleyecont() {
		return this.Scoreform_sectionb_unusleyecont;
	}
	this.getScoreform_sectionb_unusleyecont=getScoreform_sectionb_unusleyecont;


	function setScoreform_sectionb_unusleyecont(v){
		this.Scoreform_sectionb_unusleyecont=v;
	}
	this.setScoreform_sectionb_unusleyecont=setScoreform_sectionb_unusleyecont;

	this.Scoreform_sectionb_facexproth=null;


	function getScoreform_sectionb_facexproth() {
		return this.Scoreform_sectionb_facexproth;
	}
	this.getScoreform_sectionb_facexproth=getScoreform_sectionb_facexproth;


	function setScoreform_sectionb_facexproth(v){
		this.Scoreform_sectionb_facexproth=v;
	}
	this.setScoreform_sectionb_facexproth=setScoreform_sectionb_facexproth;

	this.Scoreform_sectionb_shareenjoy=null;


	function getScoreform_sectionb_shareenjoy() {
		return this.Scoreform_sectionb_shareenjoy;
	}
	this.getScoreform_sectionb_shareenjoy=getScoreform_sectionb_shareenjoy;


	function setScoreform_sectionb_shareenjoy(v){
		this.Scoreform_sectionb_shareenjoy=v;
	}
	this.setScoreform_sectionb_shareenjoy=setScoreform_sectionb_shareenjoy;

	this.Scoreform_sectionb_respname=null;


	function getScoreform_sectionb_respname() {
		return this.Scoreform_sectionb_respname;
	}
	this.getScoreform_sectionb_respname=getScoreform_sectionb_respname;


	function setScoreform_sectionb_respname(v){
		this.Scoreform_sectionb_respname=v;
	}
	this.setScoreform_sectionb_respname=setScoreform_sectionb_respname;

	this.Scoreform_sectionb_showing=null;


	function getScoreform_sectionb_showing() {
		return this.Scoreform_sectionb_showing;
	}
	this.getScoreform_sectionb_showing=getScoreform_sectionb_showing;


	function setScoreform_sectionb_showing(v){
		this.Scoreform_sectionb_showing=v;
	}
	this.setScoreform_sectionb_showing=setScoreform_sectionb_showing;

	this.Scoreform_sectionb_spontinitjntatten=null;


	function getScoreform_sectionb_spontinitjntatten() {
		return this.Scoreform_sectionb_spontinitjntatten;
	}
	this.getScoreform_sectionb_spontinitjntatten=getScoreform_sectionb_spontinitjntatten;


	function setScoreform_sectionb_spontinitjntatten(v){
		this.Scoreform_sectionb_spontinitjntatten=v;
	}
	this.setScoreform_sectionb_spontinitjntatten=setScoreform_sectionb_spontinitjntatten;

	this.Scoreform_sectionb_respjntatten=null;


	function getScoreform_sectionb_respjntatten() {
		return this.Scoreform_sectionb_respjntatten;
	}
	this.getScoreform_sectionb_respjntatten=getScoreform_sectionb_respjntatten;


	function setScoreform_sectionb_respjntatten(v){
		this.Scoreform_sectionb_respjntatten=v;
	}
	this.setScoreform_sectionb_respjntatten=setScoreform_sectionb_respjntatten;

	this.Scoreform_sectionb_qualsocoverture=null;


	function getScoreform_sectionb_qualsocoverture() {
		return this.Scoreform_sectionb_qualsocoverture;
	}
	this.getScoreform_sectionb_qualsocoverture=getScoreform_sectionb_qualsocoverture;


	function setScoreform_sectionb_qualsocoverture(v){
		this.Scoreform_sectionb_qualsocoverture=v;
	}
	this.setScoreform_sectionb_qualsocoverture=setScoreform_sectionb_qualsocoverture;

	this.Scoreform_sectionb_qualsocresp=null;


	function getScoreform_sectionb_qualsocresp() {
		return this.Scoreform_sectionb_qualsocresp;
	}
	this.getScoreform_sectionb_qualsocresp=getScoreform_sectionb_qualsocresp;


	function setScoreform_sectionb_qualsocresp(v){
		this.Scoreform_sectionb_qualsocresp=v;
	}
	this.setScoreform_sectionb_qualsocresp=setScoreform_sectionb_qualsocresp;

	this.Scoreform_sectionb_amtrecipsoccomm=null;


	function getScoreform_sectionb_amtrecipsoccomm() {
		return this.Scoreform_sectionb_amtrecipsoccomm;
	}
	this.getScoreform_sectionb_amtrecipsoccomm=getScoreform_sectionb_amtrecipsoccomm;


	function setScoreform_sectionb_amtrecipsoccomm(v){
		this.Scoreform_sectionb_amtrecipsoccomm=v;
	}
	this.setScoreform_sectionb_amtrecipsoccomm=setScoreform_sectionb_amtrecipsoccomm;

	this.Scoreform_sectionb_overallqualrapp=null;


	function getScoreform_sectionb_overallqualrapp() {
		return this.Scoreform_sectionb_overallqualrapp;
	}
	this.getScoreform_sectionb_overallqualrapp=getScoreform_sectionb_overallqualrapp;


	function setScoreform_sectionb_overallqualrapp(v){
		this.Scoreform_sectionb_overallqualrapp=v;
	}
	this.setScoreform_sectionb_overallqualrapp=setScoreform_sectionb_overallqualrapp;

	this.Scoreform_sectionc_funcplayobj=null;


	function getScoreform_sectionc_funcplayobj() {
		return this.Scoreform_sectionc_funcplayobj;
	}
	this.getScoreform_sectionc_funcplayobj=getScoreform_sectionc_funcplayobj;


	function setScoreform_sectionc_funcplayobj(v){
		this.Scoreform_sectionc_funcplayobj=v;
	}
	this.setScoreform_sectionc_funcplayobj=setScoreform_sectionc_funcplayobj;

	this.Scoreform_sectionc_imagcreativ=null;


	function getScoreform_sectionc_imagcreativ() {
		return this.Scoreform_sectionc_imagcreativ;
	}
	this.getScoreform_sectionc_imagcreativ=getScoreform_sectionc_imagcreativ;


	function setScoreform_sectionc_imagcreativ(v){
		this.Scoreform_sectionc_imagcreativ=v;
	}
	this.setScoreform_sectionc_imagcreativ=setScoreform_sectionc_imagcreativ;

	this.Scoreform_sectiond_unuslsensint=null;


	function getScoreform_sectiond_unuslsensint() {
		return this.Scoreform_sectiond_unuslsensint;
	}
	this.getScoreform_sectiond_unuslsensint=getScoreform_sectiond_unuslsensint;


	function setScoreform_sectiond_unuslsensint(v){
		this.Scoreform_sectiond_unuslsensint=v;
	}
	this.setScoreform_sectiond_unuslsensint=setScoreform_sectiond_unuslsensint;

	this.Scoreform_sectiond_handfgrcomplx=null;


	function getScoreform_sectiond_handfgrcomplx() {
		return this.Scoreform_sectiond_handfgrcomplx;
	}
	this.getScoreform_sectiond_handfgrcomplx=getScoreform_sectiond_handfgrcomplx;


	function setScoreform_sectiond_handfgrcomplx(v){
		this.Scoreform_sectiond_handfgrcomplx=v;
	}
	this.setScoreform_sectiond_handfgrcomplx=setScoreform_sectiond_handfgrcomplx;

	this.Scoreform_sectiond_selfinjurbehav=null;


	function getScoreform_sectiond_selfinjurbehav() {
		return this.Scoreform_sectiond_selfinjurbehav;
	}
	this.getScoreform_sectiond_selfinjurbehav=getScoreform_sectiond_selfinjurbehav;


	function setScoreform_sectiond_selfinjurbehav(v){
		this.Scoreform_sectiond_selfinjurbehav=v;
	}
	this.setScoreform_sectiond_selfinjurbehav=setScoreform_sectiond_selfinjurbehav;

	this.Scoreform_sectiond_unusrepetinrst=null;


	function getScoreform_sectiond_unusrepetinrst() {
		return this.Scoreform_sectiond_unusrepetinrst;
	}
	this.getScoreform_sectiond_unusrepetinrst=getScoreform_sectiond_unusrepetinrst;


	function setScoreform_sectiond_unusrepetinrst(v){
		this.Scoreform_sectiond_unusrepetinrst=v;
	}
	this.setScoreform_sectiond_unusrepetinrst=setScoreform_sectiond_unusrepetinrst;

	this.Scoreform_sectione_overactiv=null;


	function getScoreform_sectione_overactiv() {
		return this.Scoreform_sectione_overactiv;
	}
	this.getScoreform_sectione_overactiv=getScoreform_sectione_overactiv;


	function setScoreform_sectione_overactiv(v){
		this.Scoreform_sectione_overactiv=v;
	}
	this.setScoreform_sectione_overactiv=setScoreform_sectione_overactiv;

	this.Scoreform_sectione_tantrmagress=null;


	function getScoreform_sectione_tantrmagress() {
		return this.Scoreform_sectione_tantrmagress;
	}
	this.getScoreform_sectione_tantrmagress=getScoreform_sectione_tantrmagress;


	function setScoreform_sectione_tantrmagress(v){
		this.Scoreform_sectione_tantrmagress=v;
	}
	this.setScoreform_sectione_tantrmagress=setScoreform_sectione_tantrmagress;

	this.Scoreform_sectione_anxiety=null;


	function getScoreform_sectione_anxiety() {
		return this.Scoreform_sectione_anxiety;
	}
	this.getScoreform_sectione_anxiety=getScoreform_sectione_anxiety;


	function setScoreform_sectione_anxiety(v){
		this.Scoreform_sectione_anxiety=v;
	}
	this.setScoreform_sectione_anxiety=setScoreform_sectione_anxiety;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="commtot"){
				return this.Commtot ;
			} else 
			if(xmlPath=="commtot_note"){
				return this.CommtotNote ;
			} else 
			if(xmlPath=="scintot"){
				return this.Scintot ;
			} else 
			if(xmlPath=="scintot_note"){
				return this.ScintotNote ;
			} else 
			if(xmlPath=="cmsitot"){
				return this.Cmsitot ;
			} else 
			if(xmlPath=="cmsitot_note"){
				return this.CmsitotNote ;
			} else 
			if(xmlPath=="playtot"){
				return this.Playtot ;
			} else 
			if(xmlPath=="playtot_note"){
				return this.PlaytotNote ;
			} else 
			if(xmlPath=="imaginetot"){
				return this.Imaginetot ;
			} else 
			if(xmlPath=="imaginetot_note"){
				return this.ImaginetotNote ;
			} else 
			if(xmlPath=="sbritot"){
				return this.Sbritot ;
			} else 
			if(xmlPath=="sbritot_note"){
				return this.SbritotNote ;
			} else 
			if(xmlPath=="ScoreForm/diagnosis/adosclas"){
				return this.Scoreform_diagnosis_adosclas ;
			} else 
			if(xmlPath=="ScoreForm/diagnosis/overallDiag"){
				return this.Scoreform_diagnosis_overalldiag ;
			} else 
			if(xmlPath=="ScoreForm/observation/constrTaskNote"){
				return this.Scoreform_observation_constrtasknote ;
			} else 
			if(xmlPath=="ScoreForm/observation/respName"){
				return this.Scoreform_observation_respname ;
			} else 
			if(xmlPath=="ScoreForm/observation/makeBelieve"){
				return this.Scoreform_observation_makebelieve ;
			} else 
			if(xmlPath=="ScoreForm/observation/jointPlay"){
				return this.Scoreform_observation_jointplay ;
			} else 
			if(xmlPath=="ScoreForm/observation/convs"){
				return this.Scoreform_observation_convs ;
			} else 
			if(xmlPath=="ScoreForm/observation/respJointAttn"){
				return this.Scoreform_observation_respjointattn ;
			} else 
			if(xmlPath=="ScoreForm/observation/demonst"){
				return this.Scoreform_observation_demonst ;
			} else 
			if(xmlPath=="ScoreForm/observation/descPicture"){
				return this.Scoreform_observation_descpicture ;
			} else 
			if(xmlPath=="ScoreForm/observation/tellStory"){
				return this.Scoreform_observation_tellstory ;
			} else 
			if(xmlPath=="ScoreForm/observation/freePlay"){
				return this.Scoreform_observation_freeplay ;
			} else 
			if(xmlPath=="ScoreForm/observation/bdayParty"){
				return this.Scoreform_observation_bdayparty ;
			} else 
			if(xmlPath=="ScoreForm/observation/snack"){
				return this.Scoreform_observation_snack ;
			} else 
			if(xmlPath=="ScoreForm/observation/antcpRoutine"){
				return this.Scoreform_observation_antcproutine ;
			} else 
			if(xmlPath=="ScoreForm/observation/bubblePlayNotes"){
				return this.Scoreform_observation_bubbleplaynotes ;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayStrats/pointFing"){
				return this.Scoreform_observation_bubbplaystrats_pointfing ;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayStrats/openReach"){
				return this.Scoreform_observation_bubbplaystrats_openreach ;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayStrats/otherGest"){
				return this.Scoreform_observation_bubbplaystrats_othergest ;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayStrats/vocaliz"){
				return this.Scoreform_observation_bubbplaystrats_vocaliz ;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayStrats/eyeCont"){
				return this.Scoreform_observation_bubbplaystrats_eyecont ;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayStrats/words"){
				return this.Scoreform_observation_bubbplaystrats_words ;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayStrats/alone"){
				return this.Scoreform_observation_bubbplaystrats_alone ;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayStrats/withVocaliz"){
				return this.Scoreform_observation_bubbplaystrats_withvocaliz ;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayStrats/nonverb"){
				return this.Scoreform_observation_bubbplaystrats_nonverb ;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayStrats/withPoint"){
				return this.Scoreform_observation_bubbplaystrats_withpoint ;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayObjs/mechAnml"){
				return this.Scoreform_observation_bubbplayobjs_mechanml ;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayObjs/balloon"){
				return this.Scoreform_observation_bubbplayobjs_balloon ;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayObjs/bubbles"){
				return this.Scoreform_observation_bubbplayobjs_bubbles ;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayObjs/otherObj"){
				return this.Scoreform_observation_bubbplayobjs_otherobj ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/nonechoedLang"){
				return this.Scoreform_sectiona_nonechoedlang ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/amtSocOverture"){
				return this.Scoreform_sectiona_amtsocoverture ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/speechAbnorm"){
				return this.Scoreform_sectiona_speechabnorm ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/immedEcholalia"){
				return this.Scoreform_sectiona_immedecholalia ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/stIdiosyncWords"){
				return this.Scoreform_sectiona_stidiosyncwords ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/conversation"){
				return this.Scoreform_sectiona_conversation ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/pointing"){
				return this.Scoreform_sectiona_pointing ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/descInformGest"){
				return this.Scoreform_sectiona_descinformgest ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/unuslEyeCont"){
				return this.Scoreform_sectionb_unusleyecont ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/facExprOth"){
				return this.Scoreform_sectionb_facexproth ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/shareEnjoy"){
				return this.Scoreform_sectionb_shareenjoy ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/respName"){
				return this.Scoreform_sectionb_respname ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/showing"){
				return this.Scoreform_sectionb_showing ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/spontInitJntAtten"){
				return this.Scoreform_sectionb_spontinitjntatten ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/respJntAtten"){
				return this.Scoreform_sectionb_respjntatten ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/qualSocOverture"){
				return this.Scoreform_sectionb_qualsocoverture ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/qualSocResp"){
				return this.Scoreform_sectionb_qualsocresp ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/amtRecipSocComm"){
				return this.Scoreform_sectionb_amtrecipsoccomm ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/overallQualRapp"){
				return this.Scoreform_sectionb_overallqualrapp ;
			} else 
			if(xmlPath=="ScoreForm/sectionC/funcPlayObj"){
				return this.Scoreform_sectionc_funcplayobj ;
			} else 
			if(xmlPath=="ScoreForm/sectionC/imagCreativ"){
				return this.Scoreform_sectionc_imagcreativ ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/unuslSensInt"){
				return this.Scoreform_sectiond_unuslsensint ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/handFgrComplx"){
				return this.Scoreform_sectiond_handfgrcomplx ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/selfInjurBehav"){
				return this.Scoreform_sectiond_selfinjurbehav ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/unusRepetInrst"){
				return this.Scoreform_sectiond_unusrepetinrst ;
			} else 
			if(xmlPath=="ScoreForm/sectionE/overactiv"){
				return this.Scoreform_sectione_overactiv ;
			} else 
			if(xmlPath=="ScoreForm/sectionE/tantrmAgress"){
				return this.Scoreform_sectione_tantrmagress ;
			} else 
			if(xmlPath=="ScoreForm/sectionE/anxiety"){
				return this.Scoreform_sectione_anxiety ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="commtot"){
				this.Commtot=value;
			} else 
			if(xmlPath=="commtot_note"){
				this.CommtotNote=value;
			} else 
			if(xmlPath=="scintot"){
				this.Scintot=value;
			} else 
			if(xmlPath=="scintot_note"){
				this.ScintotNote=value;
			} else 
			if(xmlPath=="cmsitot"){
				this.Cmsitot=value;
			} else 
			if(xmlPath=="cmsitot_note"){
				this.CmsitotNote=value;
			} else 
			if(xmlPath=="playtot"){
				this.Playtot=value;
			} else 
			if(xmlPath=="playtot_note"){
				this.PlaytotNote=value;
			} else 
			if(xmlPath=="imaginetot"){
				this.Imaginetot=value;
			} else 
			if(xmlPath=="imaginetot_note"){
				this.ImaginetotNote=value;
			} else 
			if(xmlPath=="sbritot"){
				this.Sbritot=value;
			} else 
			if(xmlPath=="sbritot_note"){
				this.SbritotNote=value;
			} else 
			if(xmlPath=="ScoreForm/diagnosis/adosclas"){
				this.Scoreform_diagnosis_adosclas=value;
			} else 
			if(xmlPath=="ScoreForm/diagnosis/overallDiag"){
				this.Scoreform_diagnosis_overalldiag=value;
			} else 
			if(xmlPath=="ScoreForm/observation/constrTaskNote"){
				this.Scoreform_observation_constrtasknote=value;
			} else 
			if(xmlPath=="ScoreForm/observation/respName"){
				this.Scoreform_observation_respname=value;
			} else 
			if(xmlPath=="ScoreForm/observation/makeBelieve"){
				this.Scoreform_observation_makebelieve=value;
			} else 
			if(xmlPath=="ScoreForm/observation/jointPlay"){
				this.Scoreform_observation_jointplay=value;
			} else 
			if(xmlPath=="ScoreForm/observation/convs"){
				this.Scoreform_observation_convs=value;
			} else 
			if(xmlPath=="ScoreForm/observation/respJointAttn"){
				this.Scoreform_observation_respjointattn=value;
			} else 
			if(xmlPath=="ScoreForm/observation/demonst"){
				this.Scoreform_observation_demonst=value;
			} else 
			if(xmlPath=="ScoreForm/observation/descPicture"){
				this.Scoreform_observation_descpicture=value;
			} else 
			if(xmlPath=="ScoreForm/observation/tellStory"){
				this.Scoreform_observation_tellstory=value;
			} else 
			if(xmlPath=="ScoreForm/observation/freePlay"){
				this.Scoreform_observation_freeplay=value;
			} else 
			if(xmlPath=="ScoreForm/observation/bdayParty"){
				this.Scoreform_observation_bdayparty=value;
			} else 
			if(xmlPath=="ScoreForm/observation/snack"){
				this.Scoreform_observation_snack=value;
			} else 
			if(xmlPath=="ScoreForm/observation/antcpRoutine"){
				this.Scoreform_observation_antcproutine=value;
			} else 
			if(xmlPath=="ScoreForm/observation/bubblePlayNotes"){
				this.Scoreform_observation_bubbleplaynotes=value;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayStrats/pointFing"){
				this.Scoreform_observation_bubbplaystrats_pointfing=value;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayStrats/openReach"){
				this.Scoreform_observation_bubbplaystrats_openreach=value;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayStrats/otherGest"){
				this.Scoreform_observation_bubbplaystrats_othergest=value;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayStrats/vocaliz"){
				this.Scoreform_observation_bubbplaystrats_vocaliz=value;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayStrats/eyeCont"){
				this.Scoreform_observation_bubbplaystrats_eyecont=value;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayStrats/words"){
				this.Scoreform_observation_bubbplaystrats_words=value;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayStrats/alone"){
				this.Scoreform_observation_bubbplaystrats_alone=value;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayStrats/withVocaliz"){
				this.Scoreform_observation_bubbplaystrats_withvocaliz=value;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayStrats/nonverb"){
				this.Scoreform_observation_bubbplaystrats_nonverb=value;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayStrats/withPoint"){
				this.Scoreform_observation_bubbplaystrats_withpoint=value;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayObjs/mechAnml"){
				this.Scoreform_observation_bubbplayobjs_mechanml=value;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayObjs/balloon"){
				this.Scoreform_observation_bubbplayobjs_balloon=value;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayObjs/bubbles"){
				this.Scoreform_observation_bubbplayobjs_bubbles=value;
			} else 
			if(xmlPath=="ScoreForm/observation/bubbPlayObjs/otherObj"){
				this.Scoreform_observation_bubbplayobjs_otherobj=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/nonechoedLang"){
				this.Scoreform_sectiona_nonechoedlang=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/amtSocOverture"){
				this.Scoreform_sectiona_amtsocoverture=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/speechAbnorm"){
				this.Scoreform_sectiona_speechabnorm=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/immedEcholalia"){
				this.Scoreform_sectiona_immedecholalia=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/stIdiosyncWords"){
				this.Scoreform_sectiona_stidiosyncwords=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/conversation"){
				this.Scoreform_sectiona_conversation=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/pointing"){
				this.Scoreform_sectiona_pointing=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/descInformGest"){
				this.Scoreform_sectiona_descinformgest=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/unuslEyeCont"){
				this.Scoreform_sectionb_unusleyecont=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/facExprOth"){
				this.Scoreform_sectionb_facexproth=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/shareEnjoy"){
				this.Scoreform_sectionb_shareenjoy=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/respName"){
				this.Scoreform_sectionb_respname=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/showing"){
				this.Scoreform_sectionb_showing=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/spontInitJntAtten"){
				this.Scoreform_sectionb_spontinitjntatten=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/respJntAtten"){
				this.Scoreform_sectionb_respjntatten=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/qualSocOverture"){
				this.Scoreform_sectionb_qualsocoverture=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/qualSocResp"){
				this.Scoreform_sectionb_qualsocresp=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/amtRecipSocComm"){
				this.Scoreform_sectionb_amtrecipsoccomm=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/overallQualRapp"){
				this.Scoreform_sectionb_overallqualrapp=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC/funcPlayObj"){
				this.Scoreform_sectionc_funcplayobj=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC/imagCreativ"){
				this.Scoreform_sectionc_imagcreativ=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/unuslSensInt"){
				this.Scoreform_sectiond_unuslsensint=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/handFgrComplx"){
				this.Scoreform_sectiond_handfgrcomplx=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/selfInjurBehav"){
				this.Scoreform_sectiond_selfinjurbehav=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/unusRepetInrst"){
				this.Scoreform_sectiond_unusrepetinrst=value;
			} else 
			if(xmlPath=="ScoreForm/sectionE/overactiv"){
				this.Scoreform_sectione_overactiv=value;
			} else 
			if(xmlPath=="ScoreForm/sectionE/tantrmAgress"){
				this.Scoreform_sectione_tantrmagress=value;
			} else 
			if(xmlPath=="ScoreForm/sectionE/anxiety"){
				this.Scoreform_sectione_anxiety=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="commtot"){
			return "field_data";
		}else if (xmlPath=="commtot_note"){
			return "field_data";
		}else if (xmlPath=="scintot"){
			return "field_data";
		}else if (xmlPath=="scintot_note"){
			return "field_data";
		}else if (xmlPath=="cmsitot"){
			return "field_data";
		}else if (xmlPath=="cmsitot_note"){
			return "field_data";
		}else if (xmlPath=="playtot"){
			return "field_data";
		}else if (xmlPath=="playtot_note"){
			return "field_data";
		}else if (xmlPath=="imaginetot"){
			return "field_data";
		}else if (xmlPath=="imaginetot_note"){
			return "field_data";
		}else if (xmlPath=="sbritot"){
			return "field_data";
		}else if (xmlPath=="sbritot_note"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/diagnosis/adosclas"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/diagnosis/overallDiag"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/constrTaskNote"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/respName"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/makeBelieve"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/jointPlay"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/convs"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/respJointAttn"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/demonst"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/descPicture"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/tellStory"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/freePlay"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/bdayParty"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/snack"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/antcpRoutine"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/bubblePlayNotes"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/bubbPlayStrats/pointFing"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/bubbPlayStrats/openReach"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/bubbPlayStrats/otherGest"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/observation/bubbPlayStrats/vocaliz"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/bubbPlayStrats/eyeCont"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/bubbPlayStrats/words"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/bubbPlayStrats/alone"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/bubbPlayStrats/withVocaliz"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/bubbPlayStrats/nonverb"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/bubbPlayStrats/withPoint"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/bubbPlayObjs/mechAnml"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/bubbPlayObjs/balloon"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/bubbPlayObjs/bubbles"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/bubbPlayObjs/otherObj"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionA/nonechoedLang"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/amtSocOverture"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/speechAbnorm"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/immedEcholalia"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/stIdiosyncWords"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/conversation"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/pointing"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/descInformGest"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/unuslEyeCont"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/facExprOth"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/shareEnjoy"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/respName"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/showing"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/spontInitJntAtten"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/respJntAtten"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/qualSocOverture"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/qualSocResp"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/amtRecipSocComm"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/overallQualRapp"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC/funcPlayObj"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC/imagCreativ"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/unuslSensInt"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/handFgrComplx"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/selfInjurBehav"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/unusRepetInrst"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionE/overactiv"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionE/tantrmAgress"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionE/anxiety"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<ados:ADOS2001Module2";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</ados:ADOS2001Module2>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Commtot!=null){
			xmlTxt+="\n<ados:commtot";
			xmlTxt+=">";
			xmlTxt+=this.Commtot;
			xmlTxt+="</ados:commtot>";
		}
		if (this.CommtotNote!=null){
			xmlTxt+="\n<ados:commtot_note";
			xmlTxt+=">";
			xmlTxt+=this.CommtotNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:commtot_note>";
		}
		if (this.Scintot!=null){
			xmlTxt+="\n<ados:scintot";
			xmlTxt+=">";
			xmlTxt+=this.Scintot;
			xmlTxt+="</ados:scintot>";
		}
		if (this.ScintotNote!=null){
			xmlTxt+="\n<ados:scintot_note";
			xmlTxt+=">";
			xmlTxt+=this.ScintotNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:scintot_note>";
		}
		if (this.Cmsitot!=null){
			xmlTxt+="\n<ados:cmsitot";
			xmlTxt+=">";
			xmlTxt+=this.Cmsitot;
			xmlTxt+="</ados:cmsitot>";
		}
		if (this.CmsitotNote!=null){
			xmlTxt+="\n<ados:cmsitot_note";
			xmlTxt+=">";
			xmlTxt+=this.CmsitotNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:cmsitot_note>";
		}
		if (this.Playtot!=null){
			xmlTxt+="\n<ados:playtot";
			xmlTxt+=">";
			xmlTxt+=this.Playtot;
			xmlTxt+="</ados:playtot>";
		}
		if (this.PlaytotNote!=null){
			xmlTxt+="\n<ados:playtot_note";
			xmlTxt+=">";
			xmlTxt+=this.PlaytotNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:playtot_note>";
		}
		if (this.Imaginetot!=null){
			xmlTxt+="\n<ados:imaginetot";
			xmlTxt+=">";
			xmlTxt+=this.Imaginetot;
			xmlTxt+="</ados:imaginetot>";
		}
		if (this.ImaginetotNote!=null){
			xmlTxt+="\n<ados:imaginetot_note";
			xmlTxt+=">";
			xmlTxt+=this.ImaginetotNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:imaginetot_note>";
		}
		if (this.Sbritot!=null){
			xmlTxt+="\n<ados:sbritot";
			xmlTxt+=">";
			xmlTxt+=this.Sbritot;
			xmlTxt+="</ados:sbritot>";
		}
		if (this.SbritotNote!=null){
			xmlTxt+="\n<ados:sbritot_note";
			xmlTxt+=">";
			xmlTxt+=this.SbritotNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:sbritot_note>";
		}
			var child0=0;
			var att0=0;
			if(this.Scoreform_sectionb_showing!=null)
			child0++;
			if(this.Scoreform_sectionb_amtrecipsoccomm!=null)
			child0++;
			if(this.Scoreform_observation_bubbplaystrats_openreach!=null)
			child0++;
			if(this.Scoreform_observation_bubbplaystrats_words!=null)
			child0++;
			if(this.Scoreform_sectionb_spontinitjntatten!=null)
			child0++;
			if(this.Scoreform_observation_demonst!=null)
			child0++;
			if(this.Scoreform_observation_bubbplaystrats_alone!=null)
			child0++;
			if(this.Scoreform_sectiona_stidiosyncwords!=null)
			child0++;
			if(this.Scoreform_diagnosis_overalldiag!=null)
			child0++;
			if(this.Scoreform_sectiona_conversation!=null)
			child0++;
			if(this.Scoreform_sectionb_overallqualrapp!=null)
			child0++;
			if(this.Scoreform_sectiona_speechabnorm!=null)
			child0++;
			if(this.Scoreform_sectiond_selfinjurbehav!=null)
			child0++;
			if(this.Scoreform_observation_bubbplaystrats_vocaliz!=null)
			child0++;
			if(this.Scoreform_sectionb_qualsocresp!=null)
			child0++;
			if(this.Scoreform_observation_bubbplaystrats_withvocaliz!=null)
			child0++;
			if(this.Scoreform_observation_respname!=null)
			child0++;
			if(this.Scoreform_observation_bdayparty!=null)
			child0++;
			if(this.Scoreform_sectionc_funcplayobj!=null)
			child0++;
			if(this.Scoreform_observation_tellstory!=null)
			child0++;
			if(this.Scoreform_sectiond_unuslsensint!=null)
			child0++;
			if(this.Scoreform_sectionb_unusleyecont!=null)
			child0++;
			if(this.Scoreform_diagnosis_adosclas!=null)
			child0++;
			if(this.Scoreform_observation_freeplay!=null)
			child0++;
			if(this.Scoreform_sectione_anxiety!=null)
			child0++;
			if(this.Scoreform_observation_bubbplayobjs_bubbles!=null)
			child0++;
			if(this.Scoreform_sectionb_qualsocoverture!=null)
			child0++;
			if(this.Scoreform_sectiona_immedecholalia!=null)
			child0++;
			if(this.Scoreform_sectione_tantrmagress!=null)
			child0++;
			if(this.Scoreform_sectionb_respname!=null)
			child0++;
			if(this.Scoreform_observation_descpicture!=null)
			child0++;
			if(this.Scoreform_observation_convs!=null)
			child0++;
			if(this.Scoreform_sectiona_nonechoedlang!=null)
			child0++;
			if(this.Scoreform_observation_snack!=null)
			child0++;
			if(this.Scoreform_observation_bubbplayobjs_mechanml!=null)
			child0++;
			if(this.Scoreform_sectionb_facexproth!=null)
			child0++;
			if(this.Scoreform_observation_bubbplaystrats_othergest!=null)
			child0++;
			if(this.Scoreform_observation_bubbplaystrats_pointfing!=null)
			child0++;
			if(this.Scoreform_observation_bubbplaystrats_eyecont!=null)
			child0++;
			if(this.Scoreform_observation_antcproutine!=null)
			child0++;
			if(this.Scoreform_observation_bubbplayobjs_otherobj!=null)
			child0++;
			if(this.Scoreform_sectione_overactiv!=null)
			child0++;
			if(this.Scoreform_sectiond_unusrepetinrst!=null)
			child0++;
			if(this.Scoreform_sectiona_pointing!=null)
			child0++;
			if(this.Scoreform_observation_bubbplaystrats_withpoint!=null)
			child0++;
			if(this.Scoreform_sectiond_handfgrcomplx!=null)
			child0++;
			if(this.Scoreform_observation_constrtasknote!=null)
			child0++;
			if(this.Scoreform_sectiona_descinformgest!=null)
			child0++;
			if(this.Scoreform_sectionc_imagcreativ!=null)
			child0++;
			if(this.Scoreform_sectionb_shareenjoy!=null)
			child0++;
			if(this.Scoreform_observation_jointplay!=null)
			child0++;
			if(this.Scoreform_sectionb_respjntatten!=null)
			child0++;
			if(this.Scoreform_observation_bubbplayobjs_balloon!=null)
			child0++;
			if(this.Scoreform_observation_makebelieve!=null)
			child0++;
			if(this.Scoreform_observation_respjointattn!=null)
			child0++;
			if(this.Scoreform_sectiona_amtsocoverture!=null)
			child0++;
			if(this.Scoreform_observation_bubbplaystrats_nonverb!=null)
			child0++;
			if(this.Scoreform_observation_bubbleplaynotes!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<ados:ScoreForm";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child1=0;
			var att1=0;
			if(this.Scoreform_diagnosis_overalldiag!=null)
			child1++;
			if(this.Scoreform_diagnosis_adosclas!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<ados:diagnosis";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_diagnosis_adosclas!=null){
			xmlTxt+="\n<ados:adosclas";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_diagnosis_adosclas.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:adosclas>";
		}
		if (this.Scoreform_diagnosis_overalldiag!=null){
			xmlTxt+="\n<ados:overallDiag";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_diagnosis_overalldiag.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:overallDiag>";
		}
				xmlTxt+="\n</ados:diagnosis>";
			}
			}

			var child2=0;
			var att2=0;
			if(this.Scoreform_observation_bubbplaystrats_words!=null)
			child2++;
			if(this.Scoreform_observation_tellstory!=null)
			child2++;
			if(this.Scoreform_observation_bubbplaystrats_alone!=null)
			child2++;
			if(this.Scoreform_observation_respjointattn!=null)
			child2++;
			if(this.Scoreform_observation_snack!=null)
			child2++;
			if(this.Scoreform_observation_bubbplayobjs_mechanml!=null)
			child2++;
			if(this.Scoreform_observation_bubbplaystrats_vocaliz!=null)
			child2++;
			if(this.Scoreform_observation_demonst!=null)
			child2++;
			if(this.Scoreform_observation_bubbplaystrats_openreach!=null)
			child2++;
			if(this.Scoreform_observation_jointplay!=null)
			child2++;
			if(this.Scoreform_observation_bubbplayobjs_otherobj!=null)
			child2++;
			if(this.Scoreform_observation_makebelieve!=null)
			child2++;
			if(this.Scoreform_observation_descpicture!=null)
			child2++;
			if(this.Scoreform_observation_bdayparty!=null)
			child2++;
			if(this.Scoreform_observation_bubbplaystrats_withpoint!=null)
			child2++;
			if(this.Scoreform_observation_bubbplaystrats_pointfing!=null)
			child2++;
			if(this.Scoreform_observation_respname!=null)
			child2++;
			if(this.Scoreform_observation_freeplay!=null)
			child2++;
			if(this.Scoreform_observation_bubbplaystrats_withvocaliz!=null)
			child2++;
			if(this.Scoreform_observation_bubbleplaynotes!=null)
			child2++;
			if(this.Scoreform_observation_bubbplaystrats_nonverb!=null)
			child2++;
			if(this.Scoreform_observation_bubbplayobjs_balloon!=null)
			child2++;
			if(this.Scoreform_observation_constrtasknote!=null)
			child2++;
			if(this.Scoreform_observation_convs!=null)
			child2++;
			if(this.Scoreform_observation_bubbplayobjs_bubbles!=null)
			child2++;
			if(this.Scoreform_observation_antcproutine!=null)
			child2++;
			if(this.Scoreform_observation_bubbplaystrats_eyecont!=null)
			child2++;
			if(this.Scoreform_observation_bubbplaystrats_othergest!=null)
			child2++;
			if(child2>0 || att2>0){
				xmlTxt+="\n<ados:observation";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_observation_constrtasknote!=null){
			xmlTxt+="\n<ados:constrTaskNote";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_constrtasknote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:constrTaskNote>";
		}
		if (this.Scoreform_observation_respname!=null){
			xmlTxt+="\n<ados:respName";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_respname.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:respName>";
		}
		if (this.Scoreform_observation_makebelieve!=null){
			xmlTxt+="\n<ados:makeBelieve";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_makebelieve.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:makeBelieve>";
		}
		if (this.Scoreform_observation_jointplay!=null){
			xmlTxt+="\n<ados:jointPlay";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_jointplay.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:jointPlay>";
		}
		if (this.Scoreform_observation_convs!=null){
			xmlTxt+="\n<ados:convs";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_convs.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:convs>";
		}
		if (this.Scoreform_observation_respjointattn!=null){
			xmlTxt+="\n<ados:respJointAttn";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_respjointattn.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:respJointAttn>";
		}
		if (this.Scoreform_observation_demonst!=null){
			xmlTxt+="\n<ados:demonst";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_demonst.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:demonst>";
		}
		if (this.Scoreform_observation_descpicture!=null){
			xmlTxt+="\n<ados:descPicture";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_descpicture.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:descPicture>";
		}
		if (this.Scoreform_observation_tellstory!=null){
			xmlTxt+="\n<ados:tellStory";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_tellstory.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:tellStory>";
		}
		if (this.Scoreform_observation_freeplay!=null){
			xmlTxt+="\n<ados:freePlay";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_freeplay.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:freePlay>";
		}
		if (this.Scoreform_observation_bdayparty!=null){
			xmlTxt+="\n<ados:bdayParty";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_bdayparty.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:bdayParty>";
		}
		if (this.Scoreform_observation_snack!=null){
			xmlTxt+="\n<ados:snack";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_snack.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:snack>";
		}
		if (this.Scoreform_observation_antcproutine!=null){
			xmlTxt+="\n<ados:antcpRoutine";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_antcproutine.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:antcpRoutine>";
		}
		if (this.Scoreform_observation_bubbleplaynotes!=null){
			xmlTxt+="\n<ados:bubblePlayNotes";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_bubbleplaynotes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:bubblePlayNotes>";
		}
			var child3=0;
			var att3=0;
			if(this.Scoreform_observation_bubbplaystrats_pointfing!=null)
			child3++;
			if(this.Scoreform_observation_bubbplaystrats_withvocaliz!=null)
			child3++;
			if(this.Scoreform_observation_bubbplaystrats_nonverb!=null)
			child3++;
			if(this.Scoreform_observation_bubbplaystrats_othergest!=null)
			child3++;
			if(this.Scoreform_observation_bubbplaystrats_vocaliz!=null)
			child3++;
			if(this.Scoreform_observation_bubbplaystrats_words!=null)
			child3++;
			if(this.Scoreform_observation_bubbplaystrats_openreach!=null)
			child3++;
			if(this.Scoreform_observation_bubbplaystrats_withpoint!=null)
			child3++;
			if(this.Scoreform_observation_bubbplaystrats_alone!=null)
			child3++;
			if(this.Scoreform_observation_bubbplaystrats_eyecont!=null)
			child3++;
			if(child3>0 || att3>0){
				xmlTxt+="\n<ados:bubbPlayStrats";
			if(child3==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_observation_bubbplaystrats_pointfing!=null){
			xmlTxt+="\n<ados:pointFing";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_bubbplaystrats_pointfing;
			xmlTxt+="</ados:pointFing>";
		}
		if (this.Scoreform_observation_bubbplaystrats_openreach!=null){
			xmlTxt+="\n<ados:openReach";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_bubbplaystrats_openreach;
			xmlTxt+="</ados:openReach>";
		}
		if (this.Scoreform_observation_bubbplaystrats_othergest!=null){
			xmlTxt+="\n<ados:otherGest";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_bubbplaystrats_othergest.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:otherGest>";
		}
		if (this.Scoreform_observation_bubbplaystrats_vocaliz!=null){
			xmlTxt+="\n<ados:vocaliz";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_bubbplaystrats_vocaliz;
			xmlTxt+="</ados:vocaliz>";
		}
		if (this.Scoreform_observation_bubbplaystrats_eyecont!=null){
			xmlTxt+="\n<ados:eyeCont";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_bubbplaystrats_eyecont;
			xmlTxt+="</ados:eyeCont>";
		}
		if (this.Scoreform_observation_bubbplaystrats_words!=null){
			xmlTxt+="\n<ados:words";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_bubbplaystrats_words;
			xmlTxt+="</ados:words>";
		}
		if (this.Scoreform_observation_bubbplaystrats_alone!=null){
			xmlTxt+="\n<ados:alone";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_bubbplaystrats_alone;
			xmlTxt+="</ados:alone>";
		}
		if (this.Scoreform_observation_bubbplaystrats_withvocaliz!=null){
			xmlTxt+="\n<ados:withVocaliz";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_bubbplaystrats_withvocaliz;
			xmlTxt+="</ados:withVocaliz>";
		}
		if (this.Scoreform_observation_bubbplaystrats_nonverb!=null){
			xmlTxt+="\n<ados:nonverb";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_bubbplaystrats_nonverb;
			xmlTxt+="</ados:nonverb>";
		}
		if (this.Scoreform_observation_bubbplaystrats_withpoint!=null){
			xmlTxt+="\n<ados:withPoint";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_bubbplaystrats_withpoint;
			xmlTxt+="</ados:withPoint>";
		}
				xmlTxt+="\n</ados:bubbPlayStrats>";
			}
			}

			var child4=0;
			var att4=0;
			if(this.Scoreform_observation_bubbplayobjs_bubbles!=null)
			child4++;
			if(this.Scoreform_observation_bubbplayobjs_otherobj!=null)
			child4++;
			if(this.Scoreform_observation_bubbplayobjs_mechanml!=null)
			child4++;
			if(this.Scoreform_observation_bubbplayobjs_balloon!=null)
			child4++;
			if(child4>0 || att4>0){
				xmlTxt+="\n<ados:bubbPlayObjs";
			if(child4==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_observation_bubbplayobjs_mechanml!=null){
			xmlTxt+="\n<ados:mechAnml";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_bubbplayobjs_mechanml;
			xmlTxt+="</ados:mechAnml>";
		}
		if (this.Scoreform_observation_bubbplayobjs_balloon!=null){
			xmlTxt+="\n<ados:balloon";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_bubbplayobjs_balloon;
			xmlTxt+="</ados:balloon>";
		}
		if (this.Scoreform_observation_bubbplayobjs_bubbles!=null){
			xmlTxt+="\n<ados:bubbles";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_bubbplayobjs_bubbles;
			xmlTxt+="</ados:bubbles>";
		}
		if (this.Scoreform_observation_bubbplayobjs_otherobj!=null){
			xmlTxt+="\n<ados:otherObj";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_bubbplayobjs_otherobj.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:otherObj>";
		}
				xmlTxt+="\n</ados:bubbPlayObjs>";
			}
			}

				xmlTxt+="\n</ados:observation>";
			}
			}

			var child5=0;
			var att5=0;
			if(this.Scoreform_sectiona_speechabnorm!=null)
			child5++;
			if(this.Scoreform_sectiona_amtsocoverture!=null)
			child5++;
			if(this.Scoreform_sectiona_nonechoedlang!=null)
			child5++;
			if(this.Scoreform_sectiona_descinformgest!=null)
			child5++;
			if(this.Scoreform_sectiona_conversation!=null)
			child5++;
			if(this.Scoreform_sectiona_immedecholalia!=null)
			child5++;
			if(this.Scoreform_sectiona_pointing!=null)
			child5++;
			if(this.Scoreform_sectiona_stidiosyncwords!=null)
			child5++;
			if(child5>0 || att5>0){
				xmlTxt+="\n<ados:sectionA";
			if(child5==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiona_nonechoedlang!=null){
			xmlTxt+="\n<ados:nonechoedLang";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_nonechoedlang;
			xmlTxt+="</ados:nonechoedLang>";
		}
		if (this.Scoreform_sectiona_amtsocoverture!=null){
			xmlTxt+="\n<ados:amtSocOverture";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_amtsocoverture;
			xmlTxt+="</ados:amtSocOverture>";
		}
		if (this.Scoreform_sectiona_speechabnorm!=null){
			xmlTxt+="\n<ados:speechAbnorm";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_speechabnorm;
			xmlTxt+="</ados:speechAbnorm>";
		}
		if (this.Scoreform_sectiona_immedecholalia!=null){
			xmlTxt+="\n<ados:immedEcholalia";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_immedecholalia;
			xmlTxt+="</ados:immedEcholalia>";
		}
		if (this.Scoreform_sectiona_stidiosyncwords!=null){
			xmlTxt+="\n<ados:stIdiosyncWords";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_stidiosyncwords;
			xmlTxt+="</ados:stIdiosyncWords>";
		}
		if (this.Scoreform_sectiona_conversation!=null){
			xmlTxt+="\n<ados:conversation";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_conversation;
			xmlTxt+="</ados:conversation>";
		}
		if (this.Scoreform_sectiona_pointing!=null){
			xmlTxt+="\n<ados:pointing";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_pointing;
			xmlTxt+="</ados:pointing>";
		}
		if (this.Scoreform_sectiona_descinformgest!=null){
			xmlTxt+="\n<ados:descInformGest";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_descinformgest;
			xmlTxt+="</ados:descInformGest>";
		}
				xmlTxt+="\n</ados:sectionA>";
			}
			}

			var child6=0;
			var att6=0;
			if(this.Scoreform_sectionb_qualsocoverture!=null)
			child6++;
			if(this.Scoreform_sectionb_overallqualrapp!=null)
			child6++;
			if(this.Scoreform_sectionb_qualsocresp!=null)
			child6++;
			if(this.Scoreform_sectionb_respname!=null)
			child6++;
			if(this.Scoreform_sectionb_showing!=null)
			child6++;
			if(this.Scoreform_sectionb_spontinitjntatten!=null)
			child6++;
			if(this.Scoreform_sectionb_respjntatten!=null)
			child6++;
			if(this.Scoreform_sectionb_amtrecipsoccomm!=null)
			child6++;
			if(this.Scoreform_sectionb_shareenjoy!=null)
			child6++;
			if(this.Scoreform_sectionb_facexproth!=null)
			child6++;
			if(this.Scoreform_sectionb_unusleyecont!=null)
			child6++;
			if(child6>0 || att6>0){
				xmlTxt+="\n<ados:sectionB";
			if(child6==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionb_unusleyecont!=null){
			xmlTxt+="\n<ados:unuslEyeCont";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_unusleyecont;
			xmlTxt+="</ados:unuslEyeCont>";
		}
		if (this.Scoreform_sectionb_facexproth!=null){
			xmlTxt+="\n<ados:facExprOth";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_facexproth;
			xmlTxt+="</ados:facExprOth>";
		}
		if (this.Scoreform_sectionb_shareenjoy!=null){
			xmlTxt+="\n<ados:shareEnjoy";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_shareenjoy;
			xmlTxt+="</ados:shareEnjoy>";
		}
		if (this.Scoreform_sectionb_respname!=null){
			xmlTxt+="\n<ados:respName";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_respname;
			xmlTxt+="</ados:respName>";
		}
		if (this.Scoreform_sectionb_showing!=null){
			xmlTxt+="\n<ados:showing";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_showing;
			xmlTxt+="</ados:showing>";
		}
		if (this.Scoreform_sectionb_spontinitjntatten!=null){
			xmlTxt+="\n<ados:spontInitJntAtten";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_spontinitjntatten;
			xmlTxt+="</ados:spontInitJntAtten>";
		}
		if (this.Scoreform_sectionb_respjntatten!=null){
			xmlTxt+="\n<ados:respJntAtten";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_respjntatten;
			xmlTxt+="</ados:respJntAtten>";
		}
		if (this.Scoreform_sectionb_qualsocoverture!=null){
			xmlTxt+="\n<ados:qualSocOverture";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_qualsocoverture;
			xmlTxt+="</ados:qualSocOverture>";
		}
		if (this.Scoreform_sectionb_qualsocresp!=null){
			xmlTxt+="\n<ados:qualSocResp";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_qualsocresp;
			xmlTxt+="</ados:qualSocResp>";
		}
		if (this.Scoreform_sectionb_amtrecipsoccomm!=null){
			xmlTxt+="\n<ados:amtRecipSocComm";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_amtrecipsoccomm;
			xmlTxt+="</ados:amtRecipSocComm>";
		}
		if (this.Scoreform_sectionb_overallqualrapp!=null){
			xmlTxt+="\n<ados:overallQualRapp";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_overallqualrapp;
			xmlTxt+="</ados:overallQualRapp>";
		}
				xmlTxt+="\n</ados:sectionB>";
			}
			}

			var child7=0;
			var att7=0;
			if(this.Scoreform_sectionc_funcplayobj!=null)
			child7++;
			if(this.Scoreform_sectionc_imagcreativ!=null)
			child7++;
			if(child7>0 || att7>0){
				xmlTxt+="\n<ados:sectionC";
			if(child7==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionc_funcplayobj!=null){
			xmlTxt+="\n<ados:funcPlayObj";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc_funcplayobj;
			xmlTxt+="</ados:funcPlayObj>";
		}
		if (this.Scoreform_sectionc_imagcreativ!=null){
			xmlTxt+="\n<ados:imagCreativ";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc_imagcreativ;
			xmlTxt+="</ados:imagCreativ>";
		}
				xmlTxt+="\n</ados:sectionC>";
			}
			}

			var child8=0;
			var att8=0;
			if(this.Scoreform_sectiond_selfinjurbehav!=null)
			child8++;
			if(this.Scoreform_sectiond_handfgrcomplx!=null)
			child8++;
			if(this.Scoreform_sectiond_unuslsensint!=null)
			child8++;
			if(this.Scoreform_sectiond_unusrepetinrst!=null)
			child8++;
			if(child8>0 || att8>0){
				xmlTxt+="\n<ados:sectionD";
			if(child8==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiond_unuslsensint!=null){
			xmlTxt+="\n<ados:unuslSensInt";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_unuslsensint;
			xmlTxt+="</ados:unuslSensInt>";
		}
		if (this.Scoreform_sectiond_handfgrcomplx!=null){
			xmlTxt+="\n<ados:handFgrComplx";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_handfgrcomplx;
			xmlTxt+="</ados:handFgrComplx>";
		}
		if (this.Scoreform_sectiond_selfinjurbehav!=null){
			xmlTxt+="\n<ados:selfInjurBehav";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_selfinjurbehav;
			xmlTxt+="</ados:selfInjurBehav>";
		}
		if (this.Scoreform_sectiond_unusrepetinrst!=null){
			xmlTxt+="\n<ados:unusRepetInrst";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_unusrepetinrst;
			xmlTxt+="</ados:unusRepetInrst>";
		}
				xmlTxt+="\n</ados:sectionD>";
			}
			}

			var child9=0;
			var att9=0;
			if(this.Scoreform_sectione_tantrmagress!=null)
			child9++;
			if(this.Scoreform_sectione_overactiv!=null)
			child9++;
			if(this.Scoreform_sectione_anxiety!=null)
			child9++;
			if(child9>0 || att9>0){
				xmlTxt+="\n<ados:sectionE";
			if(child9==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectione_overactiv!=null){
			xmlTxt+="\n<ados:overactiv";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectione_overactiv;
			xmlTxt+="</ados:overactiv>";
		}
		if (this.Scoreform_sectione_tantrmagress!=null){
			xmlTxt+="\n<ados:tantrmAgress";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectione_tantrmagress;
			xmlTxt+="</ados:tantrmAgress>";
		}
		if (this.Scoreform_sectione_anxiety!=null){
			xmlTxt+="\n<ados:anxiety";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectione_anxiety;
			xmlTxt+="</ados:anxiety>";
		}
				xmlTxt+="\n</ados:sectionE>";
			}
			}

				xmlTxt+="\n</ados:ScoreForm>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Commtot!=null) return true;
		if (this.CommtotNote!=null) return true;
		if (this.Scintot!=null) return true;
		if (this.ScintotNote!=null) return true;
		if (this.Cmsitot!=null) return true;
		if (this.CmsitotNote!=null) return true;
		if (this.Playtot!=null) return true;
		if (this.PlaytotNote!=null) return true;
		if (this.Imaginetot!=null) return true;
		if (this.ImaginetotNote!=null) return true;
		if (this.Sbritot!=null) return true;
		if (this.SbritotNote!=null) return true;
			if(this.Scoreform_sectionb_showing!=null) return true;
			if(this.Scoreform_sectionb_amtrecipsoccomm!=null) return true;
			if(this.Scoreform_observation_bubbplaystrats_openreach!=null) return true;
			if(this.Scoreform_observation_bubbplaystrats_words!=null) return true;
			if(this.Scoreform_sectionb_spontinitjntatten!=null) return true;
			if(this.Scoreform_observation_demonst!=null) return true;
			if(this.Scoreform_observation_bubbplaystrats_alone!=null) return true;
			if(this.Scoreform_sectiona_stidiosyncwords!=null) return true;
			if(this.Scoreform_diagnosis_overalldiag!=null) return true;
			if(this.Scoreform_sectiona_conversation!=null) return true;
			if(this.Scoreform_sectionb_overallqualrapp!=null) return true;
			if(this.Scoreform_sectiona_speechabnorm!=null) return true;
			if(this.Scoreform_sectiond_selfinjurbehav!=null) return true;
			if(this.Scoreform_observation_bubbplaystrats_vocaliz!=null) return true;
			if(this.Scoreform_sectionb_qualsocresp!=null) return true;
			if(this.Scoreform_observation_bubbplaystrats_withvocaliz!=null) return true;
			if(this.Scoreform_observation_respname!=null) return true;
			if(this.Scoreform_observation_bdayparty!=null) return true;
			if(this.Scoreform_sectionc_funcplayobj!=null) return true;
			if(this.Scoreform_observation_tellstory!=null) return true;
			if(this.Scoreform_sectiond_unuslsensint!=null) return true;
			if(this.Scoreform_sectionb_unusleyecont!=null) return true;
			if(this.Scoreform_diagnosis_adosclas!=null) return true;
			if(this.Scoreform_observation_freeplay!=null) return true;
			if(this.Scoreform_sectione_anxiety!=null) return true;
			if(this.Scoreform_observation_bubbplayobjs_bubbles!=null) return true;
			if(this.Scoreform_sectionb_qualsocoverture!=null) return true;
			if(this.Scoreform_sectiona_immedecholalia!=null) return true;
			if(this.Scoreform_sectione_tantrmagress!=null) return true;
			if(this.Scoreform_sectionb_respname!=null) return true;
			if(this.Scoreform_observation_descpicture!=null) return true;
			if(this.Scoreform_observation_convs!=null) return true;
			if(this.Scoreform_sectiona_nonechoedlang!=null) return true;
			if(this.Scoreform_observation_snack!=null) return true;
			if(this.Scoreform_observation_bubbplayobjs_mechanml!=null) return true;
			if(this.Scoreform_sectionb_facexproth!=null) return true;
			if(this.Scoreform_observation_bubbplaystrats_othergest!=null) return true;
			if(this.Scoreform_observation_bubbplaystrats_pointfing!=null) return true;
			if(this.Scoreform_observation_bubbplaystrats_eyecont!=null) return true;
			if(this.Scoreform_observation_antcproutine!=null) return true;
			if(this.Scoreform_observation_bubbplayobjs_otherobj!=null) return true;
			if(this.Scoreform_sectione_overactiv!=null) return true;
			if(this.Scoreform_sectiond_unusrepetinrst!=null) return true;
			if(this.Scoreform_sectiona_pointing!=null) return true;
			if(this.Scoreform_observation_bubbplaystrats_withpoint!=null) return true;
			if(this.Scoreform_sectiond_handfgrcomplx!=null) return true;
			if(this.Scoreform_observation_constrtasknote!=null) return true;
			if(this.Scoreform_sectiona_descinformgest!=null) return true;
			if(this.Scoreform_sectionc_imagcreativ!=null) return true;
			if(this.Scoreform_sectionb_shareenjoy!=null) return true;
			if(this.Scoreform_observation_jointplay!=null) return true;
			if(this.Scoreform_sectionb_respjntatten!=null) return true;
			if(this.Scoreform_observation_bubbplayobjs_balloon!=null) return true;
			if(this.Scoreform_observation_makebelieve!=null) return true;
			if(this.Scoreform_observation_respjointattn!=null) return true;
			if(this.Scoreform_sectiona_amtsocoverture!=null) return true;
			if(this.Scoreform_observation_bubbplaystrats_nonverb!=null) return true;
			if(this.Scoreform_observation_bubbleplaynotes!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
