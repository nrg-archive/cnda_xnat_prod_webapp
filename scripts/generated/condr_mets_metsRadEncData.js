/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function condr_mets_metsRadEncData(){
this.xsiType="condr_mets:metsRadEncData";

	this.getSchemaElementName=function(){
		return "metsRadEncData";
	}

	this.getFullSchemaElementName=function(){
		return "condr_mets:metsRadEncData";
	}
this.extension=dynamicJSLoad('cnda_ext_saNode','generated/cnda_ext_saNode.js');

	this.Treated=null;


	function getTreated() {
		return this.Treated;
	}
	this.getTreated=getTreated;


	function setTreated(v){
		this.Treated=v;
	}
	this.setTreated=setTreated;


	this.isTreated=function(defaultValue) {
		if(this.Treated==null)return defaultValue;
		if(this.Treated=="1" || this.Treated==true)return true;
		return false;
	}

	this.Treatmentvolume=null;


	function getTreatmentvolume() {
		return this.Treatmentvolume;
	}
	this.getTreatmentvolume=getTreatmentvolume;


	function setTreatmentvolume(v){
		this.Treatmentvolume=v;
	}
	this.setTreatmentvolume=setTreatmentvolume;

	this.Treatmentdose=null;


	function getTreatmentdose() {
		return this.Treatmentdose;
	}
	this.getTreatmentdose=getTreatmentdose;


	function setTreatmentdose(v){
		this.Treatmentdose=v;
	}
	this.setTreatmentdose=setTreatmentdose;

	this.Isodoseline=null;


	function getIsodoseline() {
		return this.Isodoseline;
	}
	this.getIsodoseline=getIsodoseline;


	function setIsodoseline(v){
		this.Isodoseline=v;
	}
	this.setIsodoseline=setIsodoseline;

	this.Lesiontype=null;


	function getLesiontype() {
		return this.Lesiontype;
	}
	this.getLesiontype=getLesiontype;


	function setLesiontype(v){
		this.Lesiontype=v;
	}
	this.setLesiontype=setLesiontype;

	this.Numberoffractions=null;


	function getNumberoffractions() {
		return this.Numberoffractions;
	}
	this.getNumberoffractions=getNumberoffractions;


	function setNumberoffractions(v){
		this.Numberoffractions=v;
	}
	this.setNumberoffractions=setNumberoffractions;

	this.Encnotes=null;


	function getEncnotes() {
		return this.Encnotes;
	}
	this.getEncnotes=getEncnotes;


	function setEncnotes(v){
		this.Encnotes=v;
	}
	this.setEncnotes=setEncnotes;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="saNode"){
				return this.Sanode ;
			} else 
			if(xmlPath.startsWith("saNode")){
				xmlPath=xmlPath.substring(6);
				if(xmlPath=="")return this.Sanode ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Sanode!=undefined)return this.Sanode.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="treated"){
				return this.Treated ;
			} else 
			if(xmlPath=="treatmentVolume"){
				return this.Treatmentvolume ;
			} else 
			if(xmlPath=="treatmentDose"){
				return this.Treatmentdose ;
			} else 
			if(xmlPath=="isodoseLine"){
				return this.Isodoseline ;
			} else 
			if(xmlPath=="lesionType"){
				return this.Lesiontype ;
			} else 
			if(xmlPath=="numberOfFractions"){
				return this.Numberoffractions ;
			} else 
			if(xmlPath=="encNotes"){
				return this.Encnotes ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="saNode"){
				this.Sanode=value;
			} else 
			if(xmlPath.startsWith("saNode")){
				xmlPath=xmlPath.substring(6);
				if(xmlPath=="")return this.Sanode ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Sanode!=undefined){
					this.Sanode.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Sanode= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Sanode= instanciateObject("cnda_ext:saNode");//omUtils.js
						}
						if(options && options.where)this.Sanode.setProperty(options.where.field,options.where.value);
						this.Sanode.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="treated"){
				this.Treated=value;
			} else 
			if(xmlPath=="treatmentVolume"){
				this.Treatmentvolume=value;
			} else 
			if(xmlPath=="treatmentDose"){
				this.Treatmentdose=value;
			} else 
			if(xmlPath=="isodoseLine"){
				this.Isodoseline=value;
			} else 
			if(xmlPath=="lesionType"){
				this.Lesiontype=value;
			} else 
			if(xmlPath=="numberOfFractions"){
				this.Numberoffractions=value;
			} else 
			if(xmlPath=="encNotes"){
				this.Encnotes=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="treated"){
			return "field_data";
		}else if (xmlPath=="treatmentVolume"){
			return "field_data";
		}else if (xmlPath=="treatmentDose"){
			return "field_data";
		}else if (xmlPath=="isodoseLine"){
			return "field_data";
		}else if (xmlPath=="lesionType"){
			return "field_data";
		}else if (xmlPath=="numberOfFractions"){
			return "field_data";
		}else if (xmlPath=="encNotes"){
			return "field_LONG_DATA";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<condr_mets:MetsRadEnc";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</condr_mets:MetsRadEnc>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Treated!=null){
			xmlTxt+="\n<condr_mets:treated";
			xmlTxt+=">";
			xmlTxt+=this.Treated;
			xmlTxt+="</condr_mets:treated>";
		}
		if (this.Treatmentvolume!=null){
			xmlTxt+="\n<condr_mets:treatmentVolume";
			xmlTxt+=">";
			xmlTxt+=this.Treatmentvolume;
			xmlTxt+="</condr_mets:treatmentVolume>";
		}
		if (this.Treatmentdose!=null){
			xmlTxt+="\n<condr_mets:treatmentDose";
			xmlTxt+=">";
			xmlTxt+=this.Treatmentdose;
			xmlTxt+="</condr_mets:treatmentDose>";
		}
		if (this.Isodoseline!=null){
			xmlTxt+="\n<condr_mets:isodoseLine";
			xmlTxt+=">";
			xmlTxt+=this.Isodoseline;
			xmlTxt+="</condr_mets:isodoseLine>";
		}
		if (this.Lesiontype!=null){
			xmlTxt+="\n<condr_mets:lesionType";
			xmlTxt+=">";
			xmlTxt+=this.Lesiontype.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr_mets:lesionType>";
		}
		if (this.Numberoffractions!=null){
			xmlTxt+="\n<condr_mets:numberOfFractions";
			xmlTxt+=">";
			xmlTxt+=this.Numberoffractions;
			xmlTxt+="</condr_mets:numberOfFractions>";
		}
		if (this.Encnotes!=null){
			xmlTxt+="\n<condr_mets:encNotes";
			xmlTxt+=">";
			xmlTxt+=this.Encnotes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr_mets:encNotes>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Treated!=null) return true;
		if (this.Treatmentvolume!=null) return true;
		if (this.Treatmentdose!=null) return true;
		if (this.Isodoseline!=null) return true;
		if (this.Lesiontype!=null) return true;
		if (this.Numberoffractions!=null) return true;
		if (this.Encnotes!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
