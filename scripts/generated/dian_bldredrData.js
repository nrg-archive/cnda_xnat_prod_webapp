/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function dian_bldredrData(){
this.xsiType="dian:bldredrData";

	this.getSchemaElementName=function(){
		return "bldredrData";
	}

	this.getFullSchemaElementName=function(){
		return "dian:bldredrData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Biocoll=null;


	function getBiocoll() {
		return this.Biocoll;
	}
	this.getBiocoll=getBiocoll;


	function setBiocoll(v){
		this.Biocoll=v;
	}
	this.setBiocoll=setBiocoll;

	this.Biodate=null;


	function getBiodate() {
		return this.Biodate;
	}
	this.getBiodate=getBiodate;


	function setBiodate(v){
		this.Biodate=v;
	}
	this.setBiodate=setBiodate;

	this.Biotime=null;


	function getBiotime() {
		return this.Biotime;
	}
	this.getBiotime=getBiotime;


	function setBiotime(v){
		this.Biotime=v;
	}
	this.setBiotime=setBiotime;

	this.Biofast=null;


	function getBiofast() {
		return this.Biofast;
	}
	this.getBiofast=getBiofast;


	function setBiofast(v){
		this.Biofast=v;
	}
	this.setBiofast=setBiofast;

	this.Plasvol=null;


	function getPlasvol() {
		return this.Plasvol;
	}
	this.getPlasvol=getPlasvol;


	function setPlasvol(v){
		this.Plasvol=v;
	}
	this.setPlasvol=setPlasvol;

	this.Plasfroz=null;


	function getPlasfroz() {
		return this.Plasfroz;
	}
	this.getPlasfroz=getPlasfroz;


	function setPlasfroz(v){
		this.Plasfroz=v;
	}
	this.setPlasfroz=setPlasfroz;

	this.Bioship=null;


	function getBioship() {
		return this.Bioship;
	}
	this.getBioship=getBioship;


	function setBioship(v){
		this.Bioship=v;
	}
	this.setBioship=setBioship;

	this.Bioshdt=null;


	function getBioshdt() {
		return this.Bioshdt;
	}
	this.getBioshdt=getBioshdt;


	function setBioshdt(v){
		this.Bioshdt=v;
	}
	this.setBioshdt=setBioshdt;

	this.Bioshnum=null;


	function getBioshnum() {
		return this.Bioshnum;
	}
	this.getBioshnum=getBioshnum;


	function setBioshnum(v){
		this.Bioshnum=v;
	}
	this.setBioshnum=setBioshnum;

	this.Bufvol=null;


	function getBufvol() {
		return this.Bufvol;
	}
	this.getBufvol=getBufvol;


	function setBufvol(v){
		this.Bufvol=v;
	}
	this.setBufvol=setBufvol;

	this.Buffroz=null;


	function getBuffroz() {
		return this.Buffroz;
	}
	this.getBuffroz=getBuffroz;


	function setBuffroz(v){
		this.Buffroz=v;
	}
	this.setBuffroz=setBuffroz;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="BIOCOLL"){
				return this.Biocoll ;
			} else 
			if(xmlPath=="BIODATE"){
				return this.Biodate ;
			} else 
			if(xmlPath=="BIOTIME"){
				return this.Biotime ;
			} else 
			if(xmlPath=="BIOFAST"){
				return this.Biofast ;
			} else 
			if(xmlPath=="PLASVOL"){
				return this.Plasvol ;
			} else 
			if(xmlPath=="PLASFROZ"){
				return this.Plasfroz ;
			} else 
			if(xmlPath=="BIOSHIP"){
				return this.Bioship ;
			} else 
			if(xmlPath=="BIOSHDT"){
				return this.Bioshdt ;
			} else 
			if(xmlPath=="BIOSHNUM"){
				return this.Bioshnum ;
			} else 
			if(xmlPath=="BUFVOL"){
				return this.Bufvol ;
			} else 
			if(xmlPath=="BUFFROZ"){
				return this.Buffroz ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="BIOCOLL"){
				this.Biocoll=value;
			} else 
			if(xmlPath=="BIODATE"){
				this.Biodate=value;
			} else 
			if(xmlPath=="BIOTIME"){
				this.Biotime=value;
			} else 
			if(xmlPath=="BIOFAST"){
				this.Biofast=value;
			} else 
			if(xmlPath=="PLASVOL"){
				this.Plasvol=value;
			} else 
			if(xmlPath=="PLASFROZ"){
				this.Plasfroz=value;
			} else 
			if(xmlPath=="BIOSHIP"){
				this.Bioship=value;
			} else 
			if(xmlPath=="BIOSHDT"){
				this.Bioshdt=value;
			} else 
			if(xmlPath=="BIOSHNUM"){
				this.Bioshnum=value;
			} else 
			if(xmlPath=="BUFVOL"){
				this.Bufvol=value;
			} else 
			if(xmlPath=="BUFFROZ"){
				this.Buffroz=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="BIOCOLL"){
			return "field_data";
		}else if (xmlPath=="BIODATE"){
			return "field_data";
		}else if (xmlPath=="BIOTIME"){
			return "field_data";
		}else if (xmlPath=="BIOFAST"){
			return "field_data";
		}else if (xmlPath=="PLASVOL"){
			return "field_data";
		}else if (xmlPath=="PLASFROZ"){
			return "field_data";
		}else if (xmlPath=="BIOSHIP"){
			return "field_data";
		}else if (xmlPath=="BIOSHDT"){
			return "field_data";
		}else if (xmlPath=="BIOSHNUM"){
			return "field_data";
		}else if (xmlPath=="BUFVOL"){
			return "field_data";
		}else if (xmlPath=="BUFFROZ"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<dian:BLDREDR";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</dian:BLDREDR>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Biocoll!=null){
			xmlTxt+="\n<dian:BIOCOLL";
			xmlTxt+=">";
			xmlTxt+=this.Biocoll;
			xmlTxt+="</dian:BIOCOLL>";
		}
		if (this.Biodate!=null){
			xmlTxt+="\n<dian:BIODATE";
			xmlTxt+=">";
			xmlTxt+=this.Biodate;
			xmlTxt+="</dian:BIODATE>";
		}
		if (this.Biotime!=null){
			xmlTxt+="\n<dian:BIOTIME";
			xmlTxt+=">";
			xmlTxt+=this.Biotime;
			xmlTxt+="</dian:BIOTIME>";
		}
		if (this.Biofast!=null){
			xmlTxt+="\n<dian:BIOFAST";
			xmlTxt+=">";
			xmlTxt+=this.Biofast;
			xmlTxt+="</dian:BIOFAST>";
		}
		if (this.Plasvol!=null){
			xmlTxt+="\n<dian:PLASVOL";
			xmlTxt+=">";
			xmlTxt+=this.Plasvol;
			xmlTxt+="</dian:PLASVOL>";
		}
		if (this.Plasfroz!=null){
			xmlTxt+="\n<dian:PLASFROZ";
			xmlTxt+=">";
			xmlTxt+=this.Plasfroz;
			xmlTxt+="</dian:PLASFROZ>";
		}
		if (this.Bioship!=null){
			xmlTxt+="\n<dian:BIOSHIP";
			xmlTxt+=">";
			xmlTxt+=this.Bioship;
			xmlTxt+="</dian:BIOSHIP>";
		}
		if (this.Bioshdt!=null){
			xmlTxt+="\n<dian:BIOSHDT";
			xmlTxt+=">";
			xmlTxt+=this.Bioshdt;
			xmlTxt+="</dian:BIOSHDT>";
		}
		if (this.Bioshnum!=null){
			xmlTxt+="\n<dian:BIOSHNUM";
			xmlTxt+=">";
			xmlTxt+=this.Bioshnum.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:BIOSHNUM>";
		}
		if (this.Bufvol!=null){
			xmlTxt+="\n<dian:BUFVOL";
			xmlTxt+=">";
			xmlTxt+=this.Bufvol;
			xmlTxt+="</dian:BUFVOL>";
		}
		if (this.Buffroz!=null){
			xmlTxt+="\n<dian:BUFFROZ";
			xmlTxt+=">";
			xmlTxt+=this.Buffroz;
			xmlTxt+="</dian:BUFFROZ>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Biocoll!=null) return true;
		if (this.Biodate!=null) return true;
		if (this.Biotime!=null) return true;
		if (this.Biofast!=null) return true;
		if (this.Plasvol!=null) return true;
		if (this.Plasfroz!=null) return true;
		if (this.Bioship!=null) return true;
		if (this.Bioshdt!=null) return true;
		if (this.Bioshnum!=null) return true;
		if (this.Bufvol!=null) return true;
		if (this.Buffroz!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
