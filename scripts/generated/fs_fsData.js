/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function fs_fsData(){
this.xsiType="fs:fsData";

	this.getSchemaElementName=function(){
		return "fsData";
	}

	this.getFullSchemaElementName=function(){
		return "fs:fsData";
	}
this.extension=dynamicJSLoad('xnat_imageAssessorData','generated/xnat_imageAssessorData.js');

	this.FsVersion=null;


	function getFsVersion() {
		return this.FsVersion;
	}
	this.getFsVersion=getFsVersion;


	function setFsVersion(v){
		this.FsVersion=v;
	}
	this.setFsVersion=setFsVersion;

	this.IlpAge=null;


	function getIlpAge() {
		return this.IlpAge;
	}
	this.getIlpAge=getIlpAge;


	function setIlpAge(v){
		this.IlpAge=v;
	}
	this.setIlpAge=setIlpAge;

	this.Measures_volumetric_icv=null;


	function getMeasures_volumetric_icv() {
		return this.Measures_volumetric_icv;
	}
	this.getMeasures_volumetric_icv=getMeasures_volumetric_icv;


	function setMeasures_volumetric_icv(v){
		this.Measures_volumetric_icv=v;
	}
	this.setMeasures_volumetric_icv=setMeasures_volumetric_icv;

	this.Measures_volumetric_lhcortexvol=null;


	function getMeasures_volumetric_lhcortexvol() {
		return this.Measures_volumetric_lhcortexvol;
	}
	this.getMeasures_volumetric_lhcortexvol=getMeasures_volumetric_lhcortexvol;


	function setMeasures_volumetric_lhcortexvol(v){
		this.Measures_volumetric_lhcortexvol=v;
	}
	this.setMeasures_volumetric_lhcortexvol=setMeasures_volumetric_lhcortexvol;

	this.Measures_volumetric_rhcortexvol=null;


	function getMeasures_volumetric_rhcortexvol() {
		return this.Measures_volumetric_rhcortexvol;
	}
	this.getMeasures_volumetric_rhcortexvol=getMeasures_volumetric_rhcortexvol;


	function setMeasures_volumetric_rhcortexvol(v){
		this.Measures_volumetric_rhcortexvol=v;
	}
	this.setMeasures_volumetric_rhcortexvol=setMeasures_volumetric_rhcortexvol;

	this.Measures_volumetric_cortexvol=null;


	function getMeasures_volumetric_cortexvol() {
		return this.Measures_volumetric_cortexvol;
	}
	this.getMeasures_volumetric_cortexvol=getMeasures_volumetric_cortexvol;


	function setMeasures_volumetric_cortexvol(v){
		this.Measures_volumetric_cortexvol=v;
	}
	this.setMeasures_volumetric_cortexvol=setMeasures_volumetric_cortexvol;

	this.Measures_volumetric_subcortgrayvol=null;


	function getMeasures_volumetric_subcortgrayvol() {
		return this.Measures_volumetric_subcortgrayvol;
	}
	this.getMeasures_volumetric_subcortgrayvol=getMeasures_volumetric_subcortgrayvol;


	function setMeasures_volumetric_subcortgrayvol(v){
		this.Measures_volumetric_subcortgrayvol=v;
	}
	this.setMeasures_volumetric_subcortgrayvol=setMeasures_volumetric_subcortgrayvol;

	this.Measures_volumetric_totalgrayvol=null;


	function getMeasures_volumetric_totalgrayvol() {
		return this.Measures_volumetric_totalgrayvol;
	}
	this.getMeasures_volumetric_totalgrayvol=getMeasures_volumetric_totalgrayvol;


	function setMeasures_volumetric_totalgrayvol(v){
		this.Measures_volumetric_totalgrayvol=v;
	}
	this.setMeasures_volumetric_totalgrayvol=setMeasures_volumetric_totalgrayvol;

	this.Measures_volumetric_supratentorialvol=null;


	function getMeasures_volumetric_supratentorialvol() {
		return this.Measures_volumetric_supratentorialvol;
	}
	this.getMeasures_volumetric_supratentorialvol=getMeasures_volumetric_supratentorialvol;


	function setMeasures_volumetric_supratentorialvol(v){
		this.Measures_volumetric_supratentorialvol=v;
	}
	this.setMeasures_volumetric_supratentorialvol=setMeasures_volumetric_supratentorialvol;

	this.Measures_volumetric_lhcorticalwhitemattervol=null;


	function getMeasures_volumetric_lhcorticalwhitemattervol() {
		return this.Measures_volumetric_lhcorticalwhitemattervol;
	}
	this.getMeasures_volumetric_lhcorticalwhitemattervol=getMeasures_volumetric_lhcorticalwhitemattervol;


	function setMeasures_volumetric_lhcorticalwhitemattervol(v){
		this.Measures_volumetric_lhcorticalwhitemattervol=v;
	}
	this.setMeasures_volumetric_lhcorticalwhitemattervol=setMeasures_volumetric_lhcorticalwhitemattervol;

	this.Measures_volumetric_rhcorticalwhitemattervol=null;


	function getMeasures_volumetric_rhcorticalwhitemattervol() {
		return this.Measures_volumetric_rhcorticalwhitemattervol;
	}
	this.getMeasures_volumetric_rhcorticalwhitemattervol=getMeasures_volumetric_rhcorticalwhitemattervol;


	function setMeasures_volumetric_rhcorticalwhitemattervol(v){
		this.Measures_volumetric_rhcorticalwhitemattervol=v;
	}
	this.setMeasures_volumetric_rhcorticalwhitemattervol=setMeasures_volumetric_rhcorticalwhitemattervol;

	this.Measures_volumetric_corticalwhitemattervol=null;


	function getMeasures_volumetric_corticalwhitemattervol() {
		return this.Measures_volumetric_corticalwhitemattervol;
	}
	this.getMeasures_volumetric_corticalwhitemattervol=getMeasures_volumetric_corticalwhitemattervol;


	function setMeasures_volumetric_corticalwhitemattervol(v){
		this.Measures_volumetric_corticalwhitemattervol=v;
	}
	this.setMeasures_volumetric_corticalwhitemattervol=setMeasures_volumetric_corticalwhitemattervol;

	this.Measures_volumetric_brainsegvol=null;


	function getMeasures_volumetric_brainsegvol() {
		return this.Measures_volumetric_brainsegvol;
	}
	this.getMeasures_volumetric_brainsegvol=getMeasures_volumetric_brainsegvol;


	function setMeasures_volumetric_brainsegvol(v){
		this.Measures_volumetric_brainsegvol=v;
	}
	this.setMeasures_volumetric_brainsegvol=setMeasures_volumetric_brainsegvol;

	this.Measures_volumetric_brainsegvolnotvent=null;


	function getMeasures_volumetric_brainsegvolnotvent() {
		return this.Measures_volumetric_brainsegvolnotvent;
	}
	this.getMeasures_volumetric_brainsegvolnotvent=getMeasures_volumetric_brainsegvolnotvent;


	function setMeasures_volumetric_brainsegvolnotvent(v){
		this.Measures_volumetric_brainsegvolnotvent=v;
	}
	this.setMeasures_volumetric_brainsegvolnotvent=setMeasures_volumetric_brainsegvolnotvent;

	this.Measures_volumetric_brainsegvolnotventsurf=null;


	function getMeasures_volumetric_brainsegvolnotventsurf() {
		return this.Measures_volumetric_brainsegvolnotventsurf;
	}
	this.getMeasures_volumetric_brainsegvolnotventsurf=getMeasures_volumetric_brainsegvolnotventsurf;


	function setMeasures_volumetric_brainsegvolnotventsurf(v){
		this.Measures_volumetric_brainsegvolnotventsurf=v;
	}
	this.setMeasures_volumetric_brainsegvolnotventsurf=setMeasures_volumetric_brainsegvolnotventsurf;

	this.Measures_volumetric_supratentorialvolnotvent=null;


	function getMeasures_volumetric_supratentorialvolnotvent() {
		return this.Measures_volumetric_supratentorialvolnotvent;
	}
	this.getMeasures_volumetric_supratentorialvolnotvent=getMeasures_volumetric_supratentorialvolnotvent;


	function setMeasures_volumetric_supratentorialvolnotvent(v){
		this.Measures_volumetric_supratentorialvolnotvent=v;
	}
	this.setMeasures_volumetric_supratentorialvolnotvent=setMeasures_volumetric_supratentorialvolnotvent;

	this.Measures_volumetric_supratentorialvolnotventvox=null;


	function getMeasures_volumetric_supratentorialvolnotventvox() {
		return this.Measures_volumetric_supratentorialvolnotventvox;
	}
	this.getMeasures_volumetric_supratentorialvolnotventvox=getMeasures_volumetric_supratentorialvolnotventvox;


	function setMeasures_volumetric_supratentorialvolnotventvox(v){
		this.Measures_volumetric_supratentorialvolnotventvox=v;
	}
	this.setMeasures_volumetric_supratentorialvolnotventvox=setMeasures_volumetric_supratentorialvolnotventvox;

	this.Measures_volumetric_maskvol=null;


	function getMeasures_volumetric_maskvol() {
		return this.Measures_volumetric_maskvol;
	}
	this.getMeasures_volumetric_maskvol=getMeasures_volumetric_maskvol;


	function setMeasures_volumetric_maskvol(v){
		this.Measures_volumetric_maskvol=v;
	}
	this.setMeasures_volumetric_maskvol=setMeasures_volumetric_maskvol;

	this.Measures_volumetric_brainsegvolToEtiv=null;


	function getMeasures_volumetric_brainsegvolToEtiv() {
		return this.Measures_volumetric_brainsegvolToEtiv;
	}
	this.getMeasures_volumetric_brainsegvolToEtiv=getMeasures_volumetric_brainsegvolToEtiv;


	function setMeasures_volumetric_brainsegvolToEtiv(v){
		this.Measures_volumetric_brainsegvolToEtiv=v;
	}
	this.setMeasures_volumetric_brainsegvolToEtiv=setMeasures_volumetric_brainsegvolToEtiv;

	this.Measures_volumetric_maskvolToEtiv=null;


	function getMeasures_volumetric_maskvolToEtiv() {
		return this.Measures_volumetric_maskvolToEtiv;
	}
	this.getMeasures_volumetric_maskvolToEtiv=getMeasures_volumetric_maskvolToEtiv;


	function setMeasures_volumetric_maskvolToEtiv(v){
		this.Measures_volumetric_maskvolToEtiv=v;
	}
	this.setMeasures_volumetric_maskvolToEtiv=setMeasures_volumetric_maskvolToEtiv;

	this.Measures_volumetric_lhsurfaceholes=null;


	function getMeasures_volumetric_lhsurfaceholes() {
		return this.Measures_volumetric_lhsurfaceholes;
	}
	this.getMeasures_volumetric_lhsurfaceholes=getMeasures_volumetric_lhsurfaceholes;


	function setMeasures_volumetric_lhsurfaceholes(v){
		this.Measures_volumetric_lhsurfaceholes=v;
	}
	this.setMeasures_volumetric_lhsurfaceholes=setMeasures_volumetric_lhsurfaceholes;

	this.Measures_volumetric_rhsurfaceholes=null;


	function getMeasures_volumetric_rhsurfaceholes() {
		return this.Measures_volumetric_rhsurfaceholes;
	}
	this.getMeasures_volumetric_rhsurfaceholes=getMeasures_volumetric_rhsurfaceholes;


	function setMeasures_volumetric_rhsurfaceholes(v){
		this.Measures_volumetric_rhsurfaceholes=v;
	}
	this.setMeasures_volumetric_rhsurfaceholes=setMeasures_volumetric_rhsurfaceholes;

	this.Measures_volumetric_surfaceholes=null;


	function getMeasures_volumetric_surfaceholes() {
		return this.Measures_volumetric_surfaceholes;
	}
	this.getMeasures_volumetric_surfaceholes=getMeasures_volumetric_surfaceholes;


	function setMeasures_volumetric_surfaceholes(v){
		this.Measures_volumetric_surfaceholes=v;
	}
	this.setMeasures_volumetric_surfaceholes=setMeasures_volumetric_surfaceholes;
	this.Measures_volumetric_regions_region =new Array();

	function getMeasures_volumetric_regions_region() {
		return this.Measures_volumetric_regions_region;
	}
	this.getMeasures_volumetric_regions_region=getMeasures_volumetric_regions_region;


	function addMeasures_volumetric_regions_region(v){
		this.Measures_volumetric_regions_region.push(v);
	}
	this.addMeasures_volumetric_regions_region=addMeasures_volumetric_regions_region;
	this.Measures_surface_hemisphere =new Array();

	function getMeasures_surface_hemisphere() {
		return this.Measures_surface_hemisphere;
	}
	this.getMeasures_surface_hemisphere=getMeasures_surface_hemisphere;


	function addMeasures_surface_hemisphere(v){
		this.Measures_surface_hemisphere.push(v);
	}
	this.addMeasures_surface_hemisphere=addMeasures_surface_hemisphere;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="imageAssessorData"){
				return this.Imageassessordata ;
			} else 
			if(xmlPath.startsWith("imageAssessorData")){
				xmlPath=xmlPath.substring(17);
				if(xmlPath=="")return this.Imageassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Imageassessordata!=undefined)return this.Imageassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="fs_version"){
				return this.FsVersion ;
			} else 
			if(xmlPath=="ilp_age"){
				return this.IlpAge ;
			} else 
			if(xmlPath=="measures/volumetric/ICV"){
				return this.Measures_volumetric_icv ;
			} else 
			if(xmlPath=="measures/volumetric/lhCortexVol"){
				return this.Measures_volumetric_lhcortexvol ;
			} else 
			if(xmlPath=="measures/volumetric/rhCortexVol"){
				return this.Measures_volumetric_rhcortexvol ;
			} else 
			if(xmlPath=="measures/volumetric/CortexVol"){
				return this.Measures_volumetric_cortexvol ;
			} else 
			if(xmlPath=="measures/volumetric/SubCortGrayVol"){
				return this.Measures_volumetric_subcortgrayvol ;
			} else 
			if(xmlPath=="measures/volumetric/TotalGrayVol"){
				return this.Measures_volumetric_totalgrayvol ;
			} else 
			if(xmlPath=="measures/volumetric/SupraTentorialVol"){
				return this.Measures_volumetric_supratentorialvol ;
			} else 
			if(xmlPath=="measures/volumetric/lhCorticalWhiteMatterVol"){
				return this.Measures_volumetric_lhcorticalwhitemattervol ;
			} else 
			if(xmlPath=="measures/volumetric/rhCorticalWhiteMatterVol"){
				return this.Measures_volumetric_rhcorticalwhitemattervol ;
			} else 
			if(xmlPath=="measures/volumetric/CorticalWhiteMatterVol"){
				return this.Measures_volumetric_corticalwhitemattervol ;
			} else 
			if(xmlPath=="measures/volumetric/BrainSegVol"){
				return this.Measures_volumetric_brainsegvol ;
			} else 
			if(xmlPath=="measures/volumetric/BrainSegVolNotVent"){
				return this.Measures_volumetric_brainsegvolnotvent ;
			} else 
			if(xmlPath=="measures/volumetric/BrainSegVolNotVentSurf"){
				return this.Measures_volumetric_brainsegvolnotventsurf ;
			} else 
			if(xmlPath=="measures/volumetric/SupraTentorialVolNotVent"){
				return this.Measures_volumetric_supratentorialvolnotvent ;
			} else 
			if(xmlPath=="measures/volumetric/SupraTentorialVolNotVentVox"){
				return this.Measures_volumetric_supratentorialvolnotventvox ;
			} else 
			if(xmlPath=="measures/volumetric/MaskVol"){
				return this.Measures_volumetric_maskvol ;
			} else 
			if(xmlPath=="measures/volumetric/BrainSegVol-to-eTIV"){
				return this.Measures_volumetric_brainsegvolToEtiv ;
			} else 
			if(xmlPath=="measures/volumetric/MaskVol-to-eTIV"){
				return this.Measures_volumetric_maskvolToEtiv ;
			} else 
			if(xmlPath=="measures/volumetric/lhSurfaceHoles"){
				return this.Measures_volumetric_lhsurfaceholes ;
			} else 
			if(xmlPath=="measures/volumetric/rhSurfaceHoles"){
				return this.Measures_volumetric_rhsurfaceholes ;
			} else 
			if(xmlPath=="measures/volumetric/SurfaceHoles"){
				return this.Measures_volumetric_surfaceholes ;
			} else 
			if(xmlPath=="measures/volumetric/regions/region"){
				return this.Measures_volumetric_regions_region ;
			} else 
			if(xmlPath.startsWith("measures/volumetric/regions/region")){
				xmlPath=xmlPath.substring(34);
				if(xmlPath=="")return this.Measures_volumetric_regions_region ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Measures_volumetric_regions_region.length;whereCount++){

					var tempValue=this.Measures_volumetric_regions_region[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Measures_volumetric_regions_region[whereCount]);

					}

				}
				}else{

				whereArray=this.Measures_volumetric_regions_region;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="measures/surface/hemisphere"){
				return this.Measures_surface_hemisphere ;
			} else 
			if(xmlPath.startsWith("measures/surface/hemisphere")){
				xmlPath=xmlPath.substring(27);
				if(xmlPath=="")return this.Measures_surface_hemisphere ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Measures_surface_hemisphere.length;whereCount++){

					var tempValue=this.Measures_surface_hemisphere[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Measures_surface_hemisphere[whereCount]);

					}

				}
				}else{

				whereArray=this.Measures_surface_hemisphere;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="imageAssessorData"){
				this.Imageassessordata=value;
			} else 
			if(xmlPath.startsWith("imageAssessorData")){
				xmlPath=xmlPath.substring(17);
				if(xmlPath=="")return this.Imageassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Imageassessordata!=undefined){
					this.Imageassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Imageassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Imageassessordata= instanciateObject("xnat:imageAssessorData");//omUtils.js
						}
						if(options && options.where)this.Imageassessordata.setProperty(options.where.field,options.where.value);
						this.Imageassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="fs_version"){
				this.FsVersion=value;
			} else 
			if(xmlPath=="ilp_age"){
				this.IlpAge=value;
			} else 
			if(xmlPath=="measures/volumetric/ICV"){
				this.Measures_volumetric_icv=value;
			} else 
			if(xmlPath=="measures/volumetric/lhCortexVol"){
				this.Measures_volumetric_lhcortexvol=value;
			} else 
			if(xmlPath=="measures/volumetric/rhCortexVol"){
				this.Measures_volumetric_rhcortexvol=value;
			} else 
			if(xmlPath=="measures/volumetric/CortexVol"){
				this.Measures_volumetric_cortexvol=value;
			} else 
			if(xmlPath=="measures/volumetric/SubCortGrayVol"){
				this.Measures_volumetric_subcortgrayvol=value;
			} else 
			if(xmlPath=="measures/volumetric/TotalGrayVol"){
				this.Measures_volumetric_totalgrayvol=value;
			} else 
			if(xmlPath=="measures/volumetric/SupraTentorialVol"){
				this.Measures_volumetric_supratentorialvol=value;
			} else 
			if(xmlPath=="measures/volumetric/lhCorticalWhiteMatterVol"){
				this.Measures_volumetric_lhcorticalwhitemattervol=value;
			} else 
			if(xmlPath=="measures/volumetric/rhCorticalWhiteMatterVol"){
				this.Measures_volumetric_rhcorticalwhitemattervol=value;
			} else 
			if(xmlPath=="measures/volumetric/CorticalWhiteMatterVol"){
				this.Measures_volumetric_corticalwhitemattervol=value;
			} else 
			if(xmlPath=="measures/volumetric/BrainSegVol"){
				this.Measures_volumetric_brainsegvol=value;
			} else 
			if(xmlPath=="measures/volumetric/BrainSegVolNotVent"){
				this.Measures_volumetric_brainsegvolnotvent=value;
			} else 
			if(xmlPath=="measures/volumetric/BrainSegVolNotVentSurf"){
				this.Measures_volumetric_brainsegvolnotventsurf=value;
			} else 
			if(xmlPath=="measures/volumetric/SupraTentorialVolNotVent"){
				this.Measures_volumetric_supratentorialvolnotvent=value;
			} else 
			if(xmlPath=="measures/volumetric/SupraTentorialVolNotVentVox"){
				this.Measures_volumetric_supratentorialvolnotventvox=value;
			} else 
			if(xmlPath=="measures/volumetric/MaskVol"){
				this.Measures_volumetric_maskvol=value;
			} else 
			if(xmlPath=="measures/volumetric/BrainSegVol-to-eTIV"){
				this.Measures_volumetric_brainsegvolToEtiv=value;
			} else 
			if(xmlPath=="measures/volumetric/MaskVol-to-eTIV"){
				this.Measures_volumetric_maskvolToEtiv=value;
			} else 
			if(xmlPath=="measures/volumetric/lhSurfaceHoles"){
				this.Measures_volumetric_lhsurfaceholes=value;
			} else 
			if(xmlPath=="measures/volumetric/rhSurfaceHoles"){
				this.Measures_volumetric_rhsurfaceholes=value;
			} else 
			if(xmlPath=="measures/volumetric/SurfaceHoles"){
				this.Measures_volumetric_surfaceholes=value;
			} else 
			if(xmlPath=="measures/volumetric/regions/region"){
				this.Measures_volumetric_regions_region=value;
			} else 
			if(xmlPath.startsWith("measures/volumetric/regions/region")){
				xmlPath=xmlPath.substring(34);
				if(xmlPath=="")return this.Measures_volumetric_regions_region ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Measures_volumetric_regions_region.length;whereCount++){

					var tempValue=this.Measures_volumetric_regions_region[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Measures_volumetric_regions_region[whereCount]);

					}

				}
				}else{

				whereArray=this.Measures_volumetric_regions_region;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("fs:fsData_region");//omUtils.js
					}
					this.addMeasures_volumetric_regions_region(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="measures/surface/hemisphere"){
				this.Measures_surface_hemisphere=value;
			} else 
			if(xmlPath.startsWith("measures/surface/hemisphere")){
				xmlPath=xmlPath.substring(27);
				if(xmlPath=="")return this.Measures_surface_hemisphere ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Measures_surface_hemisphere.length;whereCount++){

					var tempValue=this.Measures_surface_hemisphere[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Measures_surface_hemisphere[whereCount]);

					}

				}
				}else{

				whereArray=this.Measures_surface_hemisphere;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("fs:fsData_hemisphere");//omUtils.js
					}
					this.addMeasures_surface_hemisphere(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="measures/volumetric/regions/region"){
			this.addMeasures_volumetric_regions_region(v);
		}else if (xmlPath=="measures/surface/hemisphere"){
			this.addMeasures_surface_hemisphere(v);
		}
		else{
			this.extension.setReferenceField(xmlPath,v);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="measures/volumetric/regions/region"){
			return "http://nrg.wustl.edu/fs:fsData_region";
		}else if (xmlPath=="measures/surface/hemisphere"){
			return "http://nrg.wustl.edu/fs:fsData_hemisphere";
		}
		else{
			return this.extension.getReferenceFieldName(xmlPath);
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="fs_version"){
			return "field_data";
		}else if (xmlPath=="ilp_age"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/ICV"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/lhCortexVol"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/rhCortexVol"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/CortexVol"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/SubCortGrayVol"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/TotalGrayVol"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/SupraTentorialVol"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/lhCorticalWhiteMatterVol"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/rhCorticalWhiteMatterVol"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/CorticalWhiteMatterVol"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/BrainSegVol"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/BrainSegVolNotVent"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/BrainSegVolNotVentSurf"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/SupraTentorialVolNotVent"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/SupraTentorialVolNotVentVox"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/MaskVol"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/BrainSegVol-to-eTIV"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/MaskVol-to-eTIV"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/lhSurfaceHoles"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/rhSurfaceHoles"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/SurfaceHoles"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/regions/region"){
			return "field_multi_reference";
		}else if (xmlPath=="measures/surface/hemisphere"){
			return "field_multi_reference";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<fs:Freesurfer";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</fs:Freesurfer>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.FsVersion!=null){
			xmlTxt+="\n<fs:fs_version";
			xmlTxt+=">";
			xmlTxt+=this.FsVersion.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</fs:fs_version>";
		}
		if (this.IlpAge!=null){
			xmlTxt+="\n<fs:ilp_age";
			xmlTxt+=">";
			xmlTxt+=this.IlpAge;
			xmlTxt+="</fs:ilp_age>";
		}
			var child0=0;
			var att0=0;
			if(this.Measures_volumetric_rhcortexvol!=null)
			child0++;
			if(this.Measures_volumetric_rhsurfaceholes!=null)
			child0++;
			if(this.Measures_volumetric_totalgrayvol!=null)
			child0++;
			if(this.Measures_volumetric_supratentorialvolnotvent!=null)
			child0++;
			if(this.Measures_volumetric_brainsegvolnotvent!=null)
			child0++;
			if(this.Measures_volumetric_brainsegvolnotventsurf!=null)
			child0++;
			if(this.Measures_volumetric_brainsegvolToEtiv!=null)
			child0++;
			if(this.Measures_volumetric_lhcortexvol!=null)
			child0++;
			if(this.Measures_volumetric_supratentorialvol!=null)
			child0++;
			if(this.Measures_volumetric_subcortgrayvol!=null)
			child0++;
			if(this.Measures_volumetric_surfaceholes!=null)
			child0++;
			if(this.Measures_volumetric_supratentorialvolnotventvox!=null)
			child0++;
			if(this.Measures_volumetric_lhcorticalwhitemattervol!=null)
			child0++;
			if(this.Measures_volumetric_lhsurfaceholes!=null)
			child0++;
			if(this.Measures_volumetric_rhcorticalwhitemattervol!=null)
			child0++;
			child0+=this.Measures_volumetric_regions_region.length;
			if(this.Measures_volumetric_maskvol!=null)
			child0++;
			if(this.Measures_volumetric_brainsegvol!=null)
			child0++;
			if(this.Measures_volumetric_icv!=null)
			child0++;
			if(this.Measures_volumetric_cortexvol!=null)
			child0++;
			if(this.Measures_volumetric_corticalwhitemattervol!=null)
			child0++;
			if(this.Measures_volumetric_maskvolToEtiv!=null)
			child0++;
			child0+=this.Measures_surface_hemisphere.length;
			if(child0>0 || att0>0){
				xmlTxt+="\n<fs:measures";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child1=0;
			var att1=0;
			if(this.Measures_volumetric_rhcortexvol!=null)
			child1++;
			if(this.Measures_volumetric_rhsurfaceholes!=null)
			child1++;
			if(this.Measures_volumetric_totalgrayvol!=null)
			child1++;
			if(this.Measures_volumetric_supratentorialvolnotvent!=null)
			child1++;
			if(this.Measures_volumetric_brainsegvolnotvent!=null)
			child1++;
			if(this.Measures_volumetric_brainsegvolnotventsurf!=null)
			child1++;
			if(this.Measures_volumetric_brainsegvolToEtiv!=null)
			child1++;
			if(this.Measures_volumetric_lhcortexvol!=null)
			child1++;
			if(this.Measures_volumetric_surfaceholes!=null)
			child1++;
			if(this.Measures_volumetric_supratentorialvol!=null)
			child1++;
			if(this.Measures_volumetric_subcortgrayvol!=null)
			child1++;
			if(this.Measures_volumetric_supratentorialvolnotventvox!=null)
			child1++;
			if(this.Measures_volumetric_lhcorticalwhitemattervol!=null)
			child1++;
			if(this.Measures_volumetric_lhsurfaceholes!=null)
			child1++;
			if(this.Measures_volumetric_rhcorticalwhitemattervol!=null)
			child1++;
			child1+=this.Measures_volumetric_regions_region.length;
			if(this.Measures_volumetric_maskvol!=null)
			child1++;
			if(this.Measures_volumetric_icv!=null)
			child1++;
			if(this.Measures_volumetric_brainsegvol!=null)
			child1++;
			if(this.Measures_volumetric_cortexvol!=null)
			child1++;
			if(this.Measures_volumetric_maskvolToEtiv!=null)
			child1++;
			if(this.Measures_volumetric_corticalwhitemattervol!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<fs:volumetric";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Measures_volumetric_icv!=null){
			xmlTxt+="\n<fs:ICV";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_icv;
			xmlTxt+="</fs:ICV>";
		}
		if (this.Measures_volumetric_lhcortexvol!=null){
			xmlTxt+="\n<fs:lhCortexVol";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_lhcortexvol;
			xmlTxt+="</fs:lhCortexVol>";
		}
		if (this.Measures_volumetric_rhcortexvol!=null){
			xmlTxt+="\n<fs:rhCortexVol";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_rhcortexvol;
			xmlTxt+="</fs:rhCortexVol>";
		}
		if (this.Measures_volumetric_cortexvol!=null){
			xmlTxt+="\n<fs:CortexVol";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_cortexvol;
			xmlTxt+="</fs:CortexVol>";
		}
		if (this.Measures_volumetric_subcortgrayvol!=null){
			xmlTxt+="\n<fs:SubCortGrayVol";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_subcortgrayvol;
			xmlTxt+="</fs:SubCortGrayVol>";
		}
		if (this.Measures_volumetric_totalgrayvol!=null){
			xmlTxt+="\n<fs:TotalGrayVol";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_totalgrayvol;
			xmlTxt+="</fs:TotalGrayVol>";
		}
		if (this.Measures_volumetric_supratentorialvol!=null){
			xmlTxt+="\n<fs:SupraTentorialVol";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_supratentorialvol;
			xmlTxt+="</fs:SupraTentorialVol>";
		}
		if (this.Measures_volumetric_lhcorticalwhitemattervol!=null){
			xmlTxt+="\n<fs:lhCorticalWhiteMatterVol";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_lhcorticalwhitemattervol;
			xmlTxt+="</fs:lhCorticalWhiteMatterVol>";
		}
		if (this.Measures_volumetric_rhcorticalwhitemattervol!=null){
			xmlTxt+="\n<fs:rhCorticalWhiteMatterVol";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_rhcorticalwhitemattervol;
			xmlTxt+="</fs:rhCorticalWhiteMatterVol>";
		}
		if (this.Measures_volumetric_corticalwhitemattervol!=null){
			xmlTxt+="\n<fs:CorticalWhiteMatterVol";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_corticalwhitemattervol;
			xmlTxt+="</fs:CorticalWhiteMatterVol>";
		}
		if (this.Measures_volumetric_brainsegvol!=null){
			xmlTxt+="\n<fs:BrainSegVol";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_brainsegvol;
			xmlTxt+="</fs:BrainSegVol>";
		}
		if (this.Measures_volumetric_brainsegvolnotvent!=null){
			xmlTxt+="\n<fs:BrainSegVolNotVent";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_brainsegvolnotvent;
			xmlTxt+="</fs:BrainSegVolNotVent>";
		}
		if (this.Measures_volumetric_brainsegvolnotventsurf!=null){
			xmlTxt+="\n<fs:BrainSegVolNotVentSurf";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_brainsegvolnotventsurf;
			xmlTxt+="</fs:BrainSegVolNotVentSurf>";
		}
		if (this.Measures_volumetric_supratentorialvolnotvent!=null){
			xmlTxt+="\n<fs:SupraTentorialVolNotVent";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_supratentorialvolnotvent;
			xmlTxt+="</fs:SupraTentorialVolNotVent>";
		}
		if (this.Measures_volumetric_supratentorialvolnotventvox!=null){
			xmlTxt+="\n<fs:SupraTentorialVolNotVentVox";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_supratentorialvolnotventvox;
			xmlTxt+="</fs:SupraTentorialVolNotVentVox>";
		}
		if (this.Measures_volumetric_maskvol!=null){
			xmlTxt+="\n<fs:MaskVol";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_maskvol;
			xmlTxt+="</fs:MaskVol>";
		}
		if (this.Measures_volumetric_brainsegvolToEtiv!=null){
			xmlTxt+="\n<fs:BrainSegVol-to-eTIV";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_brainsegvolToEtiv;
			xmlTxt+="</fs:BrainSegVol-to-eTIV>";
		}
		if (this.Measures_volumetric_maskvolToEtiv!=null){
			xmlTxt+="\n<fs:MaskVol-to-eTIV";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_maskvolToEtiv;
			xmlTxt+="</fs:MaskVol-to-eTIV>";
		}
		if (this.Measures_volumetric_lhsurfaceholes!=null){
			xmlTxt+="\n<fs:lhSurfaceHoles";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_lhsurfaceholes;
			xmlTxt+="</fs:lhSurfaceHoles>";
		}
		if (this.Measures_volumetric_rhsurfaceholes!=null){
			xmlTxt+="\n<fs:rhSurfaceHoles";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_rhsurfaceholes;
			xmlTxt+="</fs:rhSurfaceHoles>";
		}
		if (this.Measures_volumetric_surfaceholes!=null){
			xmlTxt+="\n<fs:SurfaceHoles";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_surfaceholes;
			xmlTxt+="</fs:SurfaceHoles>";
		}
			var child2=0;
			var att2=0;
			child2+=this.Measures_volumetric_regions_region.length;
			if(child2>0 || att2>0){
				xmlTxt+="\n<fs:regions";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var Measures_volumetric_regions_regionCOUNT=0;Measures_volumetric_regions_regionCOUNT<this.Measures_volumetric_regions_region.length;Measures_volumetric_regions_regionCOUNT++){
			xmlTxt +="\n<fs:region";
			xmlTxt +=this.Measures_volumetric_regions_region[Measures_volumetric_regions_regionCOUNT].getXMLAtts();
			if(this.Measures_volumetric_regions_region[Measures_volumetric_regions_regionCOUNT].xsiType!="fs:fsData_region"){
				xmlTxt+=" xsi:type=\"" + this.Measures_volumetric_regions_region[Measures_volumetric_regions_regionCOUNT].xsiType + "\"";
			}
			if (this.Measures_volumetric_regions_region[Measures_volumetric_regions_regionCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Measures_volumetric_regions_region[Measures_volumetric_regions_regionCOUNT].getXMLBody(preventComments);
					xmlTxt+="</fs:region>";
			}else {xmlTxt+="/>";}
		}
				xmlTxt+="\n</fs:regions>";
			}
			}

				xmlTxt+="\n</fs:volumetric>";
			}
			}

			var child3=0;
			var att3=0;
			child3+=this.Measures_surface_hemisphere.length;
			if(child3>0 || att3>0){
				xmlTxt+="\n<fs:surface";
			if(child3==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var Measures_surface_hemisphereCOUNT=0;Measures_surface_hemisphereCOUNT<this.Measures_surface_hemisphere.length;Measures_surface_hemisphereCOUNT++){
			xmlTxt +="\n<fs:hemisphere";
			xmlTxt +=this.Measures_surface_hemisphere[Measures_surface_hemisphereCOUNT].getXMLAtts();
			if(this.Measures_surface_hemisphere[Measures_surface_hemisphereCOUNT].xsiType!="fs:fsData_hemisphere"){
				xmlTxt+=" xsi:type=\"" + this.Measures_surface_hemisphere[Measures_surface_hemisphereCOUNT].xsiType + "\"";
			}
			if (this.Measures_surface_hemisphere[Measures_surface_hemisphereCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Measures_surface_hemisphere[Measures_surface_hemisphereCOUNT].getXMLBody(preventComments);
					xmlTxt+="</fs:hemisphere>";
			}else {xmlTxt+="/>";}
		}
				xmlTxt+="\n</fs:surface>";
			}
			}

				xmlTxt+="\n</fs:measures>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.FsVersion!=null) return true;
		if (this.IlpAge!=null) return true;
			if(this.Measures_volumetric_rhcortexvol!=null) return true;
			if(this.Measures_volumetric_rhsurfaceholes!=null) return true;
			if(this.Measures_volumetric_totalgrayvol!=null) return true;
			if(this.Measures_volumetric_supratentorialvolnotvent!=null) return true;
			if(this.Measures_volumetric_brainsegvolnotvent!=null) return true;
			if(this.Measures_volumetric_brainsegvolnotventsurf!=null) return true;
			if(this.Measures_volumetric_brainsegvolToEtiv!=null) return true;
			if(this.Measures_volumetric_lhcortexvol!=null) return true;
			if(this.Measures_volumetric_supratentorialvol!=null) return true;
			if(this.Measures_volumetric_subcortgrayvol!=null) return true;
			if(this.Measures_volumetric_surfaceholes!=null) return true;
			if(this.Measures_volumetric_supratentorialvolnotventvox!=null) return true;
			if(this.Measures_volumetric_lhcorticalwhitemattervol!=null) return true;
			if(this.Measures_volumetric_lhsurfaceholes!=null) return true;
			if(this.Measures_volumetric_rhcorticalwhitemattervol!=null) return true;
			if(this.Measures_volumetric_regions_region.length>0)return true;
			if(this.Measures_volumetric_maskvol!=null) return true;
			if(this.Measures_volumetric_brainsegvol!=null) return true;
			if(this.Measures_volumetric_icv!=null) return true;
			if(this.Measures_volumetric_cortexvol!=null) return true;
			if(this.Measures_volumetric_corticalwhitemattervol!=null) return true;
			if(this.Measures_volumetric_maskvolToEtiv!=null) return true;
			if(this.Measures_surface_hemisphere.length>0)return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
