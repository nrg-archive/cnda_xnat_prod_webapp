/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:04 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function uds_a4drugsData_a4drugs(){
this.xsiType="uds:a4drugsData_a4drugs";

	this.getSchemaElementName=function(){
		return "a4drugsData_a4drugs";
	}

	this.getFullSchemaElementName=function(){
		return "uds:a4drugsData_a4drugs";
	}

	this.Recno=null;


	function getRecno() {
		return this.Recno;
	}
	this.getRecno=getRecno;


	function setRecno(v){
		this.Recno=v;
	}
	this.setRecno=setRecno;

	this.Drugid=null;


	function getDrugid() {
		return this.Drugid;
	}
	this.getDrugid=getDrugid;


	function setDrugid(v){
		this.Drugid=v;
	}
	this.setDrugid=setDrugid;

	this.Drugname=null;


	function getDrugname() {
		return this.Drugname;
	}
	this.getDrugname=getDrugname;


	function setDrugname(v){
		this.Drugname=v;
	}
	this.setDrugname=setDrugname;

	this.UdsA4drugsdataA4drugsId=null;


	function getUdsA4drugsdataA4drugsId() {
		return this.UdsA4drugsdataA4drugsId;
	}
	this.getUdsA4drugsdataA4drugsId=getUdsA4drugsdataA4drugsId;


	function setUdsA4drugsdataA4drugsId(v){
		this.UdsA4drugsdataA4drugsId=v;
	}
	this.setUdsA4drugsdataA4drugsId=setUdsA4drugsdataA4drugsId;

	this.a4drugslist_a4drugs_uds_a4drugs_id_fk=null;


	this.geta4drugslist_a4drugs_uds_a4drugs_id=function() {
		return this.a4drugslist_a4drugs_uds_a4drugs_id_fk;
	}


	this.seta4drugslist_a4drugs_uds_a4drugs_id=function(v){
		this.a4drugslist_a4drugs_uds_a4drugs_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="RECNO"){
				return this.Recno ;
			} else 
			if(xmlPath=="DRUGID"){
				return this.Drugid ;
			} else 
			if(xmlPath=="DRUGNAME"){
				return this.Drugname ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="uds_a4drugsData_a4drugs_id"){
				return this.UdsA4drugsdataA4drugsId ;
			} else 
			if(xmlPath=="a4drugslist_a4drugs_uds_a4drugs_id"){
				return this.a4drugslist_a4drugs_uds_a4drugs_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="RECNO"){
				this.Recno=value;
			} else 
			if(xmlPath=="DRUGID"){
				this.Drugid=value;
			} else 
			if(xmlPath=="DRUGNAME"){
				this.Drugname=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="uds_a4drugsData_a4drugs_id"){
				this.UdsA4drugsdataA4drugsId=value;
			} else 
			if(xmlPath=="a4drugslist_a4drugs_uds_a4drugs_id"){
				this.a4drugslist_a4drugs_uds_a4drugs_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="RECNO"){
			return "field_data";
		}else if (xmlPath=="DRUGID"){
			return "field_data";
		}else if (xmlPath=="DRUGNAME"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<uds:a4drugsData_a4drugs";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</uds:a4drugsData_a4drugs>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.UdsA4drugsdataA4drugsId!=null){
				if(hiddenCount++>0)str+=",";
				str+="uds_a4drugsData_a4drugs_id=\"" + this.UdsA4drugsdataA4drugsId + "\"";
			}
			if(this.a4drugslist_a4drugs_uds_a4drugs_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="a4drugslist_a4drugs_uds_a4drugs_id=\"" + this.a4drugslist_a4drugs_uds_a4drugs_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Recno!=null){
			xmlTxt+="\n<uds:RECNO";
			xmlTxt+=">";
			xmlTxt+=this.Recno;
			xmlTxt+="</uds:RECNO>";
		}
		if (this.Drugid!=null){
			xmlTxt+="\n<uds:DRUGID";
			xmlTxt+=">";
			xmlTxt+=this.Drugid.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:DRUGID>";
		}
		if (this.Drugname!=null){
			xmlTxt+="\n<uds:DRUGNAME";
			xmlTxt+=">";
			xmlTxt+=this.Drugname.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:DRUGNAME>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.UdsA4drugsdataA4drugsId!=null) return true;
			if (this.a4drugslist_a4drugs_uds_a4drugs_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Recno!=null) return true;
		if (this.Drugid!=null) return true;
		if (this.Drugname!=null) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
