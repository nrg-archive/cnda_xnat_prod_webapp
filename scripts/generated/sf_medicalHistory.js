/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function sf_medicalHistory(){
this.xsiType="sf:medicalHistory";

	this.getSchemaElementName=function(){
		return "medicalHistory";
	}

	this.getFullSchemaElementName=function(){
		return "sf:medicalHistory";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');
	this.Conditions_condition =new Array();

	function getConditions_condition() {
		return this.Conditions_condition;
	}
	this.getConditions_condition=getConditions_condition;


	function addConditions_condition(v){
		this.Conditions_condition.push(v);
	}
	this.addConditions_condition=addConditions_condition;
	this.Hypertension =null;
	function getHypertension() {
		return this.Hypertension;
	}
	this.getHypertension=getHypertension;


	function setHypertension(v){
		this.Hypertension =v;
	}
	this.setHypertension=setHypertension;

	this.Hypertension_HypertensionSfConditionId=null;


	function getHypertension_HypertensionSfConditionId(){
		return this.Hypertension_HypertensionSfConditionId;
	}
	this.getHypertension_HypertensionSfConditionId=getHypertension_HypertensionSfConditionId;


	function setHypertension_HypertensionSfConditionId(v){
		this.Hypertension_HypertensionSfConditionId=v;
	}
	this.setHypertension_HypertensionSfConditionId=setHypertension_HypertensionSfConditionId;
	this.Diabetes =null;
	function getDiabetes() {
		return this.Diabetes;
	}
	this.getDiabetes=getDiabetes;


	function setDiabetes(v){
		this.Diabetes =v;
	}
	this.setDiabetes=setDiabetes;

	this.Diabetes_DiabetesSfConditionId=null;


	function getDiabetes_DiabetesSfConditionId(){
		return this.Diabetes_DiabetesSfConditionId;
	}
	this.getDiabetes_DiabetesSfConditionId=getDiabetes_DiabetesSfConditionId;


	function setDiabetes_DiabetesSfConditionId(v){
		this.Diabetes_DiabetesSfConditionId=v;
	}
	this.setDiabetes_DiabetesSfConditionId=setDiabetes_DiabetesSfConditionId;
	this.Hypercholesterolemia =null;
	function getHypercholesterolemia() {
		return this.Hypercholesterolemia;
	}
	this.getHypercholesterolemia=getHypercholesterolemia;


	function setHypercholesterolemia(v){
		this.Hypercholesterolemia =v;
	}
	this.setHypercholesterolemia=setHypercholesterolemia;

	this.Hypercholesterolemia_HypercholesterolemiaSfConditionId=null;


	function getHypercholesterolemia_HypercholesterolemiaSfConditionId(){
		return this.Hypercholesterolemia_HypercholesterolemiaSfConditionId;
	}
	this.getHypercholesterolemia_HypercholesterolemiaSfConditionId=getHypercholesterolemia_HypercholesterolemiaSfConditionId;


	function setHypercholesterolemia_HypercholesterolemiaSfConditionId(v){
		this.Hypercholesterolemia_HypercholesterolemiaSfConditionId=v;
	}
	this.setHypercholesterolemia_HypercholesterolemiaSfConditionId=setHypercholesterolemia_HypercholesterolemiaSfConditionId;
	this.Stroke =null;
	function getStroke() {
		return this.Stroke;
	}
	this.getStroke=getStroke;


	function setStroke(v){
		this.Stroke =v;
	}
	this.setStroke=setStroke;

	this.Stroke_StrokeSfConditionId=null;


	function getStroke_StrokeSfConditionId(){
		return this.Stroke_StrokeSfConditionId;
	}
	this.getStroke_StrokeSfConditionId=getStroke_StrokeSfConditionId;


	function setStroke_StrokeSfConditionId(v){
		this.Stroke_StrokeSfConditionId=v;
	}
	this.setStroke_StrokeSfConditionId=setStroke_StrokeSfConditionId;
	this.Autoimmunedisease =null;
	function getAutoimmunedisease() {
		return this.Autoimmunedisease;
	}
	this.getAutoimmunedisease=getAutoimmunedisease;


	function setAutoimmunedisease(v){
		this.Autoimmunedisease =v;
	}
	this.setAutoimmunedisease=setAutoimmunedisease;

	this.Autoimmunedisease_AutoimmunediseaseSfConditionId=null;


	function getAutoimmunedisease_AutoimmunediseaseSfConditionId(){
		return this.Autoimmunedisease_AutoimmunediseaseSfConditionId;
	}
	this.getAutoimmunedisease_AutoimmunediseaseSfConditionId=getAutoimmunedisease_AutoimmunediseaseSfConditionId;


	function setAutoimmunedisease_AutoimmunediseaseSfConditionId(v){
		this.Autoimmunedisease_AutoimmunediseaseSfConditionId=v;
	}
	this.setAutoimmunedisease_AutoimmunediseaseSfConditionId=setAutoimmunedisease_AutoimmunediseaseSfConditionId;
	this.Headache =null;
	function getHeadache() {
		return this.Headache;
	}
	this.getHeadache=getHeadache;


	function setHeadache(v){
		this.Headache =v;
	}
	this.setHeadache=setHeadache;

	this.Headache_HeadacheSfConditionId=null;


	function getHeadache_HeadacheSfConditionId(){
		return this.Headache_HeadacheSfConditionId;
	}
	this.getHeadache_HeadacheSfConditionId=getHeadache_HeadacheSfConditionId;


	function setHeadache_HeadacheSfConditionId(v){
		this.Headache_HeadacheSfConditionId=v;
	}
	this.setHeadache_HeadacheSfConditionId=setHeadache_HeadacheSfConditionId;
	this.Seizure =null;
	function getSeizure() {
		return this.Seizure;
	}
	this.getSeizure=getSeizure;


	function setSeizure(v){
		this.Seizure =v;
	}
	this.setSeizure=setSeizure;

	this.Seizure_SeizureSfConditionId=null;


	function getSeizure_SeizureSfConditionId(){
		return this.Seizure_SeizureSfConditionId;
	}
	this.getSeizure_SeizureSfConditionId=getSeizure_SeizureSfConditionId;


	function setSeizure_SeizureSfConditionId(v){
		this.Seizure_SeizureSfConditionId=v;
	}
	this.setSeizure_SeizureSfConditionId=setSeizure_SeizureSfConditionId;
	this.Allergies_allergy =new Array();

	function getAllergies_allergy() {
		return this.Allergies_allergy;
	}
	this.getAllergies_allergy=getAllergies_allergy;


	function addAllergies_allergy(v){
		this.Allergies_allergy.push(v);
	}
	this.addAllergies_allergy=addAllergies_allergy;

	this.Smokingstatus=null;


	function getSmokingstatus() {
		return this.Smokingstatus;
	}
	this.getSmokingstatus=getSmokingstatus;


	function setSmokingstatus(v){
		this.Smokingstatus=v;
	}
	this.setSmokingstatus=setSmokingstatus;

	this.Smokingppd=null;


	function getSmokingppd() {
		return this.Smokingppd;
	}
	this.getSmokingppd=getSmokingppd;


	function setSmokingppd(v){
		this.Smokingppd=v;
	}
	this.setSmokingppd=setSmokingppd;

	this.Alcoholstatus=null;


	function getAlcoholstatus() {
		return this.Alcoholstatus;
	}
	this.getAlcoholstatus=getAlcoholstatus;


	function setAlcoholstatus(v){
		this.Alcoholstatus=v;
	}
	this.setAlcoholstatus=setAlcoholstatus;

	this.Alcoholuse=null;


	function getAlcoholuse() {
		return this.Alcoholuse;
	}
	this.getAlcoholuse=getAlcoholuse;


	function setAlcoholuse(v){
		this.Alcoholuse=v;
	}
	this.setAlcoholuse=setAlcoholuse;

	this.Illicitdrugstatus=null;


	function getIllicitdrugstatus() {
		return this.Illicitdrugstatus;
	}
	this.getIllicitdrugstatus=getIllicitdrugstatus;


	function setIllicitdrugstatus(v){
		this.Illicitdrugstatus=v;
	}
	this.setIllicitdrugstatus=setIllicitdrugstatus;

	this.Illicitdrugdetails=null;


	function getIllicitdrugdetails() {
		return this.Illicitdrugdetails;
	}
	this.getIllicitdrugdetails=getIllicitdrugdetails;


	function setIllicitdrugdetails(v){
		this.Illicitdrugdetails=v;
	}
	this.setIllicitdrugdetails=setIllicitdrugdetails;
	this.Deficits =null;
	function getDeficits() {
		return this.Deficits;
	}
	this.getDeficits=getDeficits;


	function setDeficits(v){
		this.Deficits =v;
	}
	this.setDeficits=setDeficits;

	this.Deficits_DeficitsSfDeficitsId=null;


	function getDeficits_DeficitsSfDeficitsId(){
		return this.Deficits_DeficitsSfDeficitsId;
	}
	this.getDeficits_DeficitsSfDeficitsId=getDeficits_DeficitsSfDeficitsId;


	function setDeficits_DeficitsSfDeficitsId(v){
		this.Deficits_DeficitsSfDeficitsId=v;
	}
	this.setDeficits_DeficitsSfDeficitsId=setDeficits_DeficitsSfDeficitsId;

	this.Deficitnotes=null;


	function getDeficitnotes() {
		return this.Deficitnotes;
	}
	this.getDeficitnotes=getDeficitnotes;


	function setDeficitnotes(v){
		this.Deficitnotes=v;
	}
	this.setDeficitnotes=setDeficitnotes;

	this.Medicalhistorynotes=null;


	function getMedicalhistorynotes() {
		return this.Medicalhistorynotes;
	}
	this.getMedicalhistorynotes=getMedicalhistorynotes;


	function setMedicalhistorynotes(v){
		this.Medicalhistorynotes=v;
	}
	this.setMedicalhistorynotes=setMedicalhistorynotes;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="conditions/condition"){
				return this.Conditions_condition ;
			} else 
			if(xmlPath.startsWith("conditions/condition")){
				xmlPath=xmlPath.substring(20);
				if(xmlPath=="")return this.Conditions_condition ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Conditions_condition.length;whereCount++){

					var tempValue=this.Conditions_condition[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Conditions_condition[whereCount]);

					}

				}
				}else{

				whereArray=this.Conditions_condition;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="hypertension"){
				return this.Hypertension ;
			} else 
			if(xmlPath.startsWith("hypertension")){
				xmlPath=xmlPath.substring(12);
				if(xmlPath=="")return this.Hypertension ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Hypertension!=undefined)return this.Hypertension.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="diabetes"){
				return this.Diabetes ;
			} else 
			if(xmlPath.startsWith("diabetes")){
				xmlPath=xmlPath.substring(8);
				if(xmlPath=="")return this.Diabetes ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Diabetes!=undefined)return this.Diabetes.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="hypercholesterolemia"){
				return this.Hypercholesterolemia ;
			} else 
			if(xmlPath.startsWith("hypercholesterolemia")){
				xmlPath=xmlPath.substring(20);
				if(xmlPath=="")return this.Hypercholesterolemia ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Hypercholesterolemia!=undefined)return this.Hypercholesterolemia.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="stroke"){
				return this.Stroke ;
			} else 
			if(xmlPath.startsWith("stroke")){
				xmlPath=xmlPath.substring(6);
				if(xmlPath=="")return this.Stroke ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Stroke!=undefined)return this.Stroke.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="autoimmuneDisease"){
				return this.Autoimmunedisease ;
			} else 
			if(xmlPath.startsWith("autoimmuneDisease")){
				xmlPath=xmlPath.substring(17);
				if(xmlPath=="")return this.Autoimmunedisease ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Autoimmunedisease!=undefined)return this.Autoimmunedisease.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="headache"){
				return this.Headache ;
			} else 
			if(xmlPath.startsWith("headache")){
				xmlPath=xmlPath.substring(8);
				if(xmlPath=="")return this.Headache ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Headache!=undefined)return this.Headache.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="seizure"){
				return this.Seizure ;
			} else 
			if(xmlPath.startsWith("seizure")){
				xmlPath=xmlPath.substring(7);
				if(xmlPath=="")return this.Seizure ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Seizure!=undefined)return this.Seizure.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="allergies/allergy"){
				return this.Allergies_allergy ;
			} else 
			if(xmlPath.startsWith("allergies/allergy")){
				xmlPath=xmlPath.substring(17);
				if(xmlPath=="")return this.Allergies_allergy ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Allergies_allergy.length;whereCount++){

					var tempValue=this.Allergies_allergy[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Allergies_allergy[whereCount]);

					}

				}
				}else{

				whereArray=this.Allergies_allergy;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="smokingStatus"){
				return this.Smokingstatus ;
			} else 
			if(xmlPath=="smokingPpd"){
				return this.Smokingppd ;
			} else 
			if(xmlPath=="alcoholStatus"){
				return this.Alcoholstatus ;
			} else 
			if(xmlPath=="alcoholUse"){
				return this.Alcoholuse ;
			} else 
			if(xmlPath=="illicitDrugStatus"){
				return this.Illicitdrugstatus ;
			} else 
			if(xmlPath=="illicitDrugDetails"){
				return this.Illicitdrugdetails ;
			} else 
			if(xmlPath=="deficits"){
				return this.Deficits ;
			} else 
			if(xmlPath.startsWith("deficits")){
				xmlPath=xmlPath.substring(8);
				if(xmlPath=="")return this.Deficits ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Deficits!=undefined)return this.Deficits.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="deficitNotes"){
				return this.Deficitnotes ;
			} else 
			if(xmlPath=="medicalHistoryNotes"){
				return this.Medicalhistorynotes ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="conditions/condition"){
				this.Conditions_condition=value;
			} else 
			if(xmlPath.startsWith("conditions/condition")){
				xmlPath=xmlPath.substring(20);
				if(xmlPath=="")return this.Conditions_condition ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Conditions_condition.length;whereCount++){

					var tempValue=this.Conditions_condition[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Conditions_condition[whereCount]);

					}

				}
				}else{

				whereArray=this.Conditions_condition;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("sf:condition");//omUtils.js
					}
					this.addConditions_condition(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="hypertension"){
				this.Hypertension=value;
			} else 
			if(xmlPath.startsWith("hypertension")){
				xmlPath=xmlPath.substring(12);
				if(xmlPath=="")return this.Hypertension ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Hypertension!=undefined){
					this.Hypertension.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Hypertension= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Hypertension= instanciateObject("sf:condition");//omUtils.js
						}
						if(options && options.where)this.Hypertension.setProperty(options.where.field,options.where.value);
						this.Hypertension.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="diabetes"){
				this.Diabetes=value;
			} else 
			if(xmlPath.startsWith("diabetes")){
				xmlPath=xmlPath.substring(8);
				if(xmlPath=="")return this.Diabetes ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Diabetes!=undefined){
					this.Diabetes.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Diabetes= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Diabetes= instanciateObject("sf:condition");//omUtils.js
						}
						if(options && options.where)this.Diabetes.setProperty(options.where.field,options.where.value);
						this.Diabetes.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="hypercholesterolemia"){
				this.Hypercholesterolemia=value;
			} else 
			if(xmlPath.startsWith("hypercholesterolemia")){
				xmlPath=xmlPath.substring(20);
				if(xmlPath=="")return this.Hypercholesterolemia ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Hypercholesterolemia!=undefined){
					this.Hypercholesterolemia.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Hypercholesterolemia= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Hypercholesterolemia= instanciateObject("sf:condition");//omUtils.js
						}
						if(options && options.where)this.Hypercholesterolemia.setProperty(options.where.field,options.where.value);
						this.Hypercholesterolemia.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="stroke"){
				this.Stroke=value;
			} else 
			if(xmlPath.startsWith("stroke")){
				xmlPath=xmlPath.substring(6);
				if(xmlPath=="")return this.Stroke ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Stroke!=undefined){
					this.Stroke.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Stroke= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Stroke= instanciateObject("sf:condition");//omUtils.js
						}
						if(options && options.where)this.Stroke.setProperty(options.where.field,options.where.value);
						this.Stroke.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="autoimmuneDisease"){
				this.Autoimmunedisease=value;
			} else 
			if(xmlPath.startsWith("autoimmuneDisease")){
				xmlPath=xmlPath.substring(17);
				if(xmlPath=="")return this.Autoimmunedisease ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Autoimmunedisease!=undefined){
					this.Autoimmunedisease.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Autoimmunedisease= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Autoimmunedisease= instanciateObject("sf:condition");//omUtils.js
						}
						if(options && options.where)this.Autoimmunedisease.setProperty(options.where.field,options.where.value);
						this.Autoimmunedisease.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="headache"){
				this.Headache=value;
			} else 
			if(xmlPath.startsWith("headache")){
				xmlPath=xmlPath.substring(8);
				if(xmlPath=="")return this.Headache ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Headache!=undefined){
					this.Headache.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Headache= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Headache= instanciateObject("sf:condition");//omUtils.js
						}
						if(options && options.where)this.Headache.setProperty(options.where.field,options.where.value);
						this.Headache.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="seizure"){
				this.Seizure=value;
			} else 
			if(xmlPath.startsWith("seizure")){
				xmlPath=xmlPath.substring(7);
				if(xmlPath=="")return this.Seizure ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Seizure!=undefined){
					this.Seizure.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Seizure= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Seizure= instanciateObject("sf:condition");//omUtils.js
						}
						if(options && options.where)this.Seizure.setProperty(options.where.field,options.where.value);
						this.Seizure.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="allergies/allergy"){
				this.Allergies_allergy=value;
			} else 
			if(xmlPath.startsWith("allergies/allergy")){
				xmlPath=xmlPath.substring(17);
				if(xmlPath=="")return this.Allergies_allergy ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Allergies_allergy.length;whereCount++){

					var tempValue=this.Allergies_allergy[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Allergies_allergy[whereCount]);

					}

				}
				}else{

				whereArray=this.Allergies_allergy;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("sf:medicalHistory_allergy");//omUtils.js
					}
					this.addAllergies_allergy(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="smokingStatus"){
				this.Smokingstatus=value;
			} else 
			if(xmlPath=="smokingPpd"){
				this.Smokingppd=value;
			} else 
			if(xmlPath=="alcoholStatus"){
				this.Alcoholstatus=value;
			} else 
			if(xmlPath=="alcoholUse"){
				this.Alcoholuse=value;
			} else 
			if(xmlPath=="illicitDrugStatus"){
				this.Illicitdrugstatus=value;
			} else 
			if(xmlPath=="illicitDrugDetails"){
				this.Illicitdrugdetails=value;
			} else 
			if(xmlPath=="deficits"){
				this.Deficits=value;
			} else 
			if(xmlPath.startsWith("deficits")){
				xmlPath=xmlPath.substring(8);
				if(xmlPath=="")return this.Deficits ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Deficits!=undefined){
					this.Deficits.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Deficits= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Deficits= instanciateObject("sf:deficits");//omUtils.js
						}
						if(options && options.where)this.Deficits.setProperty(options.where.field,options.where.value);
						this.Deficits.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="deficitNotes"){
				this.Deficitnotes=value;
			} else 
			if(xmlPath=="medicalHistoryNotes"){
				this.Medicalhistorynotes=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="conditions/condition"){
			this.addConditions_condition(v);
		}else if (xmlPath=="hypertension"){
			this.setHypertension(v);
		}else if (xmlPath=="diabetes"){
			this.setDiabetes(v);
		}else if (xmlPath=="hypercholesterolemia"){
			this.setHypercholesterolemia(v);
		}else if (xmlPath=="stroke"){
			this.setStroke(v);
		}else if (xmlPath=="autoimmuneDisease"){
			this.setAutoimmunedisease(v);
		}else if (xmlPath=="headache"){
			this.setHeadache(v);
		}else if (xmlPath=="seizure"){
			this.setSeizure(v);
		}else if (xmlPath=="allergies/allergy"){
			this.addAllergies_allergy(v);
		}else if (xmlPath=="deficits"){
			this.setDeficits(v);
		}
		else{
			this.extension.setReferenceField(xmlPath,v);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="conditions/condition"){
			return "http://nrg.wustl.edu/sf:condition";
		}else if (xmlPath=="hypertension"){
			return "http://nrg.wustl.edu/sf:condition";
		}else if (xmlPath=="diabetes"){
			return "http://nrg.wustl.edu/sf:condition";
		}else if (xmlPath=="hypercholesterolemia"){
			return "http://nrg.wustl.edu/sf:condition";
		}else if (xmlPath=="stroke"){
			return "http://nrg.wustl.edu/sf:condition";
		}else if (xmlPath=="autoimmuneDisease"){
			return "http://nrg.wustl.edu/sf:condition";
		}else if (xmlPath=="headache"){
			return "http://nrg.wustl.edu/sf:condition";
		}else if (xmlPath=="seizure"){
			return "http://nrg.wustl.edu/sf:condition";
		}else if (xmlPath=="allergies/allergy"){
			return "http://nrg.wustl.edu/sf:medicalHistory_allergy";
		}else if (xmlPath=="deficits"){
			return "http://nrg.wustl.edu/sf:deficits";
		}
		else{
			return this.extension.getReferenceFieldName(xmlPath);
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="conditions/condition"){
			return "field_multi_reference";
		}else if (xmlPath=="hypertension"){
			return "field_single_reference";
		}else if (xmlPath=="diabetes"){
			return "field_single_reference";
		}else if (xmlPath=="hypercholesterolemia"){
			return "field_single_reference";
		}else if (xmlPath=="stroke"){
			return "field_single_reference";
		}else if (xmlPath=="autoimmuneDisease"){
			return "field_single_reference";
		}else if (xmlPath=="headache"){
			return "field_single_reference";
		}else if (xmlPath=="seizure"){
			return "field_single_reference";
		}else if (xmlPath=="allergies/allergy"){
			return "field_inline_repeater";
		}else if (xmlPath=="smokingStatus"){
			return "field_data";
		}else if (xmlPath=="smokingPpd"){
			return "field_data";
		}else if (xmlPath=="alcoholStatus"){
			return "field_data";
		}else if (xmlPath=="alcoholUse"){
			return "field_data";
		}else if (xmlPath=="illicitDrugStatus"){
			return "field_data";
		}else if (xmlPath=="illicitDrugDetails"){
			return "field_LONG_DATA";
		}else if (xmlPath=="deficits"){
			return "field_single_reference";
		}else if (xmlPath=="deficitNotes"){
			return "field_LONG_DATA";
		}else if (xmlPath=="medicalHistoryNotes"){
			return "field_LONG_DATA";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<sf:MedicalHistory";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</sf:MedicalHistory>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
			var child0=0;
			var att0=0;
			child0+=this.Conditions_condition.length;
			if(child0>0 || att0>0){
				xmlTxt+="\n<sf:conditions";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var Conditions_conditionCOUNT=0;Conditions_conditionCOUNT<this.Conditions_condition.length;Conditions_conditionCOUNT++){
			xmlTxt +="\n<sf:condition";
			xmlTxt +=this.Conditions_condition[Conditions_conditionCOUNT].getXMLAtts();
			if(this.Conditions_condition[Conditions_conditionCOUNT].xsiType!="sf:condition"){
				xmlTxt+=" xsi:type=\"" + this.Conditions_condition[Conditions_conditionCOUNT].xsiType + "\"";
			}
			if (this.Conditions_condition[Conditions_conditionCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Conditions_condition[Conditions_conditionCOUNT].getXMLBody(preventComments);
					xmlTxt+="</sf:condition>";
			}else {xmlTxt+="/>";}
		}
				xmlTxt+="\n</sf:conditions>";
			}
			}

		if (this.Hypertension!=null){
			xmlTxt+="\n<sf:hypertension";
			xmlTxt+=this.Hypertension.getXMLAtts();
			if(this.Hypertension.xsiType!="sf:condition"){
				xmlTxt+=" xsi:type=\"" + this.Hypertension.xsiType + "\"";
			}
			if (this.Hypertension.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Hypertension.getXMLBody(preventComments);
				xmlTxt+="</sf:hypertension>";
			}else {xmlTxt+="/>";}
		}
		//NOT REQUIRED

		if (this.Diabetes!=null){
			xmlTxt+="\n<sf:diabetes";
			xmlTxt+=this.Diabetes.getXMLAtts();
			if(this.Diabetes.xsiType!="sf:condition"){
				xmlTxt+=" xsi:type=\"" + this.Diabetes.xsiType + "\"";
			}
			if (this.Diabetes.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Diabetes.getXMLBody(preventComments);
				xmlTxt+="</sf:diabetes>";
			}else {xmlTxt+="/>";}
		}
		//NOT REQUIRED

		if (this.Hypercholesterolemia!=null){
			xmlTxt+="\n<sf:hypercholesterolemia";
			xmlTxt+=this.Hypercholesterolemia.getXMLAtts();
			if(this.Hypercholesterolemia.xsiType!="sf:condition"){
				xmlTxt+=" xsi:type=\"" + this.Hypercholesterolemia.xsiType + "\"";
			}
			if (this.Hypercholesterolemia.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Hypercholesterolemia.getXMLBody(preventComments);
				xmlTxt+="</sf:hypercholesterolemia>";
			}else {xmlTxt+="/>";}
		}
		//NOT REQUIRED

		if (this.Stroke!=null){
			xmlTxt+="\n<sf:stroke";
			xmlTxt+=this.Stroke.getXMLAtts();
			if(this.Stroke.xsiType!="sf:condition"){
				xmlTxt+=" xsi:type=\"" + this.Stroke.xsiType + "\"";
			}
			if (this.Stroke.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Stroke.getXMLBody(preventComments);
				xmlTxt+="</sf:stroke>";
			}else {xmlTxt+="/>";}
		}
		//NOT REQUIRED

		if (this.Autoimmunedisease!=null){
			xmlTxt+="\n<sf:autoimmuneDisease";
			xmlTxt+=this.Autoimmunedisease.getXMLAtts();
			if(this.Autoimmunedisease.xsiType!="sf:condition"){
				xmlTxt+=" xsi:type=\"" + this.Autoimmunedisease.xsiType + "\"";
			}
			if (this.Autoimmunedisease.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Autoimmunedisease.getXMLBody(preventComments);
				xmlTxt+="</sf:autoimmuneDisease>";
			}else {xmlTxt+="/>";}
		}
		//NOT REQUIRED

		if (this.Headache!=null){
			xmlTxt+="\n<sf:headache";
			xmlTxt+=this.Headache.getXMLAtts();
			if(this.Headache.xsiType!="sf:condition"){
				xmlTxt+=" xsi:type=\"" + this.Headache.xsiType + "\"";
			}
			if (this.Headache.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Headache.getXMLBody(preventComments);
				xmlTxt+="</sf:headache>";
			}else {xmlTxt+="/>";}
		}
		//NOT REQUIRED

		if (this.Seizure!=null){
			xmlTxt+="\n<sf:seizure";
			xmlTxt+=this.Seizure.getXMLAtts();
			if(this.Seizure.xsiType!="sf:condition"){
				xmlTxt+=" xsi:type=\"" + this.Seizure.xsiType + "\"";
			}
			if (this.Seizure.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Seizure.getXMLBody(preventComments);
				xmlTxt+="</sf:seizure>";
			}else {xmlTxt+="/>";}
		}
		//NOT REQUIRED

			var child1=0;
			var att1=0;
			child1+=this.Allergies_allergy.length;
			if(child1>0 || att1>0){
				xmlTxt+="\n<sf:allergies";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var Allergies_allergyCOUNT=0;Allergies_allergyCOUNT<this.Allergies_allergy.length;Allergies_allergyCOUNT++){
			xmlTxt+=this.Allergies_allergy[Allergies_allergyCOUNT].getXMLBody(preventComments);
		}
				xmlTxt+="\n</sf:allergies>";
			}
			}

		if (this.Smokingstatus!=null){
			xmlTxt+="\n<sf:smokingStatus";
			xmlTxt+=">";
			xmlTxt+=this.Smokingstatus.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:smokingStatus>";
		}
		if (this.Smokingppd!=null){
			xmlTxt+="\n<sf:smokingPpd";
			xmlTxt+=">";
			xmlTxt+=this.Smokingppd;
			xmlTxt+="</sf:smokingPpd>";
		}
		if (this.Alcoholstatus!=null){
			xmlTxt+="\n<sf:alcoholStatus";
			xmlTxt+=">";
			xmlTxt+=this.Alcoholstatus.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:alcoholStatus>";
		}
		if (this.Alcoholuse!=null){
			xmlTxt+="\n<sf:alcoholUse";
			xmlTxt+=">";
			xmlTxt+=this.Alcoholuse.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:alcoholUse>";
		}
		if (this.Illicitdrugstatus!=null){
			xmlTxt+="\n<sf:illicitDrugStatus";
			xmlTxt+=">";
			xmlTxt+=this.Illicitdrugstatus.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:illicitDrugStatus>";
		}
		if (this.Illicitdrugdetails!=null){
			xmlTxt+="\n<sf:illicitDrugDetails";
			xmlTxt+=">";
			xmlTxt+=this.Illicitdrugdetails.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:illicitDrugDetails>";
		}
		if (this.Deficits!=null){
			xmlTxt+="\n<sf:deficits";
			xmlTxt+=this.Deficits.getXMLAtts();
			if(this.Deficits.xsiType!="sf:deficits"){
				xmlTxt+=" xsi:type=\"" + this.Deficits.xsiType + "\"";
			}
			if (this.Deficits.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Deficits.getXMLBody(preventComments);
				xmlTxt+="</sf:deficits>";
			}else {xmlTxt+="/>";}
		}
		//NOT REQUIRED

		if (this.Deficitnotes!=null){
			xmlTxt+="\n<sf:deficitNotes";
			xmlTxt+=">";
			xmlTxt+=this.Deficitnotes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:deficitNotes>";
		}
		if (this.Medicalhistorynotes!=null){
			xmlTxt+="\n<sf:medicalHistoryNotes";
			xmlTxt+=">";
			xmlTxt+=this.Medicalhistorynotes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:medicalHistoryNotes>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
			if(this.Conditions_condition.length>0)return true;
		if (this.Hypertension!=null){
			if (this.Hypertension.hasXMLBodyContent()) return true;
		}
		//NOT REQUIRED

		if (this.Diabetes!=null){
			if (this.Diabetes.hasXMLBodyContent()) return true;
		}
		//NOT REQUIRED

		if (this.Hypercholesterolemia!=null){
			if (this.Hypercholesterolemia.hasXMLBodyContent()) return true;
		}
		//NOT REQUIRED

		if (this.Stroke!=null){
			if (this.Stroke.hasXMLBodyContent()) return true;
		}
		//NOT REQUIRED

		if (this.Autoimmunedisease!=null){
			if (this.Autoimmunedisease.hasXMLBodyContent()) return true;
		}
		//NOT REQUIRED

		if (this.Headache!=null){
			if (this.Headache.hasXMLBodyContent()) return true;
		}
		//NOT REQUIRED

		if (this.Seizure!=null){
			if (this.Seizure.hasXMLBodyContent()) return true;
		}
		//NOT REQUIRED

			if(this.Allergies_allergy.length>0)return true;
		if (this.Smokingstatus!=null) return true;
		if (this.Smokingppd!=null) return true;
		if (this.Alcoholstatus!=null) return true;
		if (this.Alcoholuse!=null) return true;
		if (this.Illicitdrugstatus!=null) return true;
		if (this.Illicitdrugdetails!=null) return true;
		if (this.Deficits!=null){
			if (this.Deficits.hasXMLBodyContent()) return true;
		}
		//NOT REQUIRED

		if (this.Deficitnotes!=null) return true;
		if (this.Medicalhistorynotes!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
