/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function mpet_manualpetTimeCourseData_roi(){
this.xsiType="mpet:manualpetTimeCourseData_roi";

	this.getSchemaElementName=function(){
		return "manualpetTimeCourseData_roi";
	}

	this.getFullSchemaElementName=function(){
		return "mpet:manualpetTimeCourseData_roi";
	}

	this.Slope=null;


	function getSlope() {
		return this.Slope;
	}
	this.getSlope=getSlope;


	function setSlope(v){
		this.Slope=v;
	}
	this.setSlope=setSlope;

	this.Correlation=null;


	function getCorrelation() {
		return this.Correlation;
	}
	this.getCorrelation=getCorrelation;


	function setCorrelation(v){
		this.Correlation=v;
	}
	this.setCorrelation=setCorrelation;

	this.Bp=null;


	function getBp() {
		return this.Bp;
	}
	this.getBp=getBp;


	function setBp(v){
		this.Bp=v;
	}
	this.setBp=setBp;

	this.Suvr=null;


	function getSuvr() {
		return this.Suvr;
	}
	this.getSuvr=getSuvr;


	function setSuvr(v){
		this.Suvr=v;
	}
	this.setSuvr=setSuvr;

	this.Numvoxels=null;


	function getNumvoxels() {
		return this.Numvoxels;
	}
	this.getNumvoxels=getNumvoxels;


	function setNumvoxels(v){
		this.Numvoxels=v;
	}
	this.setNumvoxels=setNumvoxels;

	this.Intercept=null;


	function getIntercept() {
		return this.Intercept;
	}
	this.getIntercept=getIntercept;


	function setIntercept(v){
		this.Intercept=v;
	}
	this.setIntercept=setIntercept;

	this.Rsquared=null;


	function getRsquared() {
		return this.Rsquared;
	}
	this.getRsquared=getRsquared;


	function setRsquared(v){
		this.Rsquared=v;
	}
	this.setRsquared=setRsquared;

	this.Name=null;


	function getName() {
		return this.Name;
	}
	this.getName=getName;


	function setName(v){
		this.Name=v;
	}
	this.setName=setName;

	this.MpetManualpettimecoursedataRoiId=null;


	function getMpetManualpettimecoursedataRoiId() {
		return this.MpetManualpettimecoursedataRoiId;
	}
	this.getMpetManualpettimecoursedataRoiId=getMpetManualpettimecoursedataRoiId;


	function setMpetManualpettimecoursedataRoiId(v){
		this.MpetManualpettimecoursedataRoiId=v;
	}
	this.setMpetManualpettimecoursedataRoiId=setMpetManualpettimecoursedataRoiId;

	this.rois_roi_mpet_manualpetTimeCour_id_fk=null;


	this.getrois_roi_mpet_manualpetTimeCour_id=function() {
		return this.rois_roi_mpet_manualpetTimeCour_id_fk;
	}


	this.setrois_roi_mpet_manualpetTimeCour_id=function(v){
		this.rois_roi_mpet_manualpetTimeCour_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="Slope"){
				return this.Slope ;
			} else 
			if(xmlPath=="Correlation"){
				return this.Correlation ;
			} else 
			if(xmlPath=="BP"){
				return this.Bp ;
			} else 
			if(xmlPath=="SUVR"){
				return this.Suvr ;
			} else 
			if(xmlPath=="NumVoxels"){
				return this.Numvoxels ;
			} else 
			if(xmlPath=="Intercept"){
				return this.Intercept ;
			} else 
			if(xmlPath=="RSquared"){
				return this.Rsquared ;
			} else 
			if(xmlPath=="name"){
				return this.Name ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="mpet_manualpetTimeCourseData_roi_id"){
				return this.MpetManualpettimecoursedataRoiId ;
			} else 
			if(xmlPath=="rois_roi_mpet_manualpetTimeCour_id"){
				return this.rois_roi_mpet_manualpetTimeCour_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="Slope"){
				this.Slope=value;
			} else 
			if(xmlPath=="Correlation"){
				this.Correlation=value;
			} else 
			if(xmlPath=="BP"){
				this.Bp=value;
			} else 
			if(xmlPath=="SUVR"){
				this.Suvr=value;
			} else 
			if(xmlPath=="NumVoxels"){
				this.Numvoxels=value;
			} else 
			if(xmlPath=="Intercept"){
				this.Intercept=value;
			} else 
			if(xmlPath=="RSquared"){
				this.Rsquared=value;
			} else 
			if(xmlPath=="name"){
				this.Name=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="mpet_manualpetTimeCourseData_roi_id"){
				this.MpetManualpettimecoursedataRoiId=value;
			} else 
			if(xmlPath=="rois_roi_mpet_manualpetTimeCour_id"){
				this.rois_roi_mpet_manualpetTimeCour_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="Slope"){
			return "field_data";
		}else if (xmlPath=="Correlation"){
			return "field_data";
		}else if (xmlPath=="BP"){
			return "field_data";
		}else if (xmlPath=="SUVR"){
			return "field_data";
		}else if (xmlPath=="NumVoxels"){
			return "field_data";
		}else if (xmlPath=="Intercept"){
			return "field_data";
		}else if (xmlPath=="RSquared"){
			return "field_data";
		}else if (xmlPath=="name"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<mpet:manualpetTimeCourseData_roi";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</mpet:manualpetTimeCourseData_roi>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.MpetManualpettimecoursedataRoiId!=null){
				if(hiddenCount++>0)str+=",";
				str+="mpet_manualpetTimeCourseData_roi_id=\"" + this.MpetManualpettimecoursedataRoiId + "\"";
			}
			if(this.rois_roi_mpet_manualpetTimeCour_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="rois_roi_mpet_manualpetTimeCour_id=\"" + this.rois_roi_mpet_manualpetTimeCour_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.Name!=null)
			attTxt+=" name=\"" +this.Name +"\"";
		else attTxt+=" name=\"\"";//REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Slope!=null){
			xmlTxt+="\n<mpet:Slope";
			xmlTxt+=">";
			xmlTxt+=this.Slope;
			xmlTxt+="</mpet:Slope>";
		}
		if (this.Correlation!=null){
			xmlTxt+="\n<mpet:Correlation";
			xmlTxt+=">";
			xmlTxt+=this.Correlation;
			xmlTxt+="</mpet:Correlation>";
		}
		if (this.Bp!=null){
			xmlTxt+="\n<mpet:BP";
			xmlTxt+=">";
			xmlTxt+=this.Bp;
			xmlTxt+="</mpet:BP>";
		}
		if (this.Suvr!=null){
			xmlTxt+="\n<mpet:SUVR";
			xmlTxt+=">";
			xmlTxt+=this.Suvr;
			xmlTxt+="</mpet:SUVR>";
		}
		if (this.Numvoxels!=null){
			xmlTxt+="\n<mpet:NumVoxels";
			xmlTxt+=">";
			xmlTxt+=this.Numvoxels;
			xmlTxt+="</mpet:NumVoxels>";
		}
		if (this.Intercept!=null){
			xmlTxt+="\n<mpet:Intercept";
			xmlTxt+=">";
			xmlTxt+=this.Intercept;
			xmlTxt+="</mpet:Intercept>";
		}
		if (this.Rsquared!=null){
			xmlTxt+="\n<mpet:RSquared";
			xmlTxt+=">";
			xmlTxt+=this.Rsquared;
			xmlTxt+="</mpet:RSquared>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.MpetManualpettimecoursedataRoiId!=null) return true;
			if (this.rois_roi_mpet_manualpetTimeCour_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Slope!=null) return true;
		if (this.Correlation!=null) return true;
		if (this.Bp!=null) return true;
		if (this.Suvr!=null) return true;
		if (this.Numvoxels!=null) return true;
		if (this.Intercept!=null) return true;
		if (this.Rsquared!=null) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
