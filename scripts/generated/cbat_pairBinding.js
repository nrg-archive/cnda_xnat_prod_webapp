/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cbat_pairBinding(){
this.xsiType="cbat:pairBinding";

	this.getSchemaElementName=function(){
		return "pairBinding";
	}

	this.getFullSchemaElementName=function(){
		return "cbat:pairBinding";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Intactt1_accuracy=null;


	function getIntactt1_accuracy() {
		return this.Intactt1_accuracy;
	}
	this.getIntactt1_accuracy=getIntactt1_accuracy;


	function setIntactt1_accuracy(v){
		this.Intactt1_accuracy=v;
	}
	this.setIntactt1_accuracy=setIntactt1_accuracy;

	this.Intactt2_accuracy=null;


	function getIntactt2_accuracy() {
		return this.Intactt2_accuracy;
	}
	this.getIntactt2_accuracy=getIntactt2_accuracy;


	function setIntactt2_accuracy(v){
		this.Intactt2_accuracy=v;
	}
	this.setIntactt2_accuracy=setIntactt2_accuracy;

	this.Intactt3_accuracy=null;


	function getIntactt3_accuracy() {
		return this.Intactt3_accuracy;
	}
	this.getIntactt3_accuracy=getIntactt3_accuracy;


	function setIntactt3_accuracy(v){
		this.Intactt3_accuracy=v;
	}
	this.setIntactt3_accuracy=setIntactt3_accuracy;

	this.Intactt4_accuracy=null;


	function getIntactt4_accuracy() {
		return this.Intactt4_accuracy;
	}
	this.getIntactt4_accuracy=getIntactt4_accuracy;


	function setIntactt4_accuracy(v){
		this.Intactt4_accuracy=v;
	}
	this.setIntactt4_accuracy=setIntactt4_accuracy;

	this.Intactt5_accuracy=null;


	function getIntactt5_accuracy() {
		return this.Intactt5_accuracy;
	}
	this.getIntactt5_accuracy=getIntactt5_accuracy;


	function setIntactt5_accuracy(v){
		this.Intactt5_accuracy=v;
	}
	this.setIntactt5_accuracy=setIntactt5_accuracy;

	this.Intactt6_accuracy=null;


	function getIntactt6_accuracy() {
		return this.Intactt6_accuracy;
	}
	this.getIntactt6_accuracy=getIntactt6_accuracy;


	function setIntactt6_accuracy(v){
		this.Intactt6_accuracy=v;
	}
	this.setIntactt6_accuracy=setIntactt6_accuracy;

	this.Intactt7_accuracy=null;


	function getIntactt7_accuracy() {
		return this.Intactt7_accuracy;
	}
	this.getIntactt7_accuracy=getIntactt7_accuracy;


	function setIntactt7_accuracy(v){
		this.Intactt7_accuracy=v;
	}
	this.setIntactt7_accuracy=setIntactt7_accuracy;

	this.Intactt8_accuracy=null;


	function getIntactt8_accuracy() {
		return this.Intactt8_accuracy;
	}
	this.getIntactt8_accuracy=getIntactt8_accuracy;


	function setIntactt8_accuracy(v){
		this.Intactt8_accuracy=v;
	}
	this.setIntactt8_accuracy=setIntactt8_accuracy;

	this.Intactt9_accuracy=null;


	function getIntactt9_accuracy() {
		return this.Intactt9_accuracy;
	}
	this.getIntactt9_accuracy=getIntactt9_accuracy;


	function setIntactt9_accuracy(v){
		this.Intactt9_accuracy=v;
	}
	this.setIntactt9_accuracy=setIntactt9_accuracy;

	this.Intactt10_accuracy=null;


	function getIntactt10_accuracy() {
		return this.Intactt10_accuracy;
	}
	this.getIntactt10_accuracy=getIntactt10_accuracy;


	function setIntactt10_accuracy(v){
		this.Intactt10_accuracy=v;
	}
	this.setIntactt10_accuracy=setIntactt10_accuracy;

	this.Intactt11_accuracy=null;


	function getIntactt11_accuracy() {
		return this.Intactt11_accuracy;
	}
	this.getIntactt11_accuracy=getIntactt11_accuracy;


	function setIntactt11_accuracy(v){
		this.Intactt11_accuracy=v;
	}
	this.setIntactt11_accuracy=setIntactt11_accuracy;

	this.Intactt12_accuracy=null;


	function getIntactt12_accuracy() {
		return this.Intactt12_accuracy;
	}
	this.getIntactt12_accuracy=getIntactt12_accuracy;


	function setIntactt12_accuracy(v){
		this.Intactt12_accuracy=v;
	}
	this.setIntactt12_accuracy=setIntactt12_accuracy;

	this.Mixedt1_accuracy=null;


	function getMixedt1_accuracy() {
		return this.Mixedt1_accuracy;
	}
	this.getMixedt1_accuracy=getMixedt1_accuracy;


	function setMixedt1_accuracy(v){
		this.Mixedt1_accuracy=v;
	}
	this.setMixedt1_accuracy=setMixedt1_accuracy;

	this.Mixedt2_accuracy=null;


	function getMixedt2_accuracy() {
		return this.Mixedt2_accuracy;
	}
	this.getMixedt2_accuracy=getMixedt2_accuracy;


	function setMixedt2_accuracy(v){
		this.Mixedt2_accuracy=v;
	}
	this.setMixedt2_accuracy=setMixedt2_accuracy;

	this.Mixedt3_accuracy=null;


	function getMixedt3_accuracy() {
		return this.Mixedt3_accuracy;
	}
	this.getMixedt3_accuracy=getMixedt3_accuracy;


	function setMixedt3_accuracy(v){
		this.Mixedt3_accuracy=v;
	}
	this.setMixedt3_accuracy=setMixedt3_accuracy;

	this.Mixedt4_accuracy=null;


	function getMixedt4_accuracy() {
		return this.Mixedt4_accuracy;
	}
	this.getMixedt4_accuracy=getMixedt4_accuracy;


	function setMixedt4_accuracy(v){
		this.Mixedt4_accuracy=v;
	}
	this.setMixedt4_accuracy=setMixedt4_accuracy;

	this.Mixedt5_accuracy=null;


	function getMixedt5_accuracy() {
		return this.Mixedt5_accuracy;
	}
	this.getMixedt5_accuracy=getMixedt5_accuracy;


	function setMixedt5_accuracy(v){
		this.Mixedt5_accuracy=v;
	}
	this.setMixedt5_accuracy=setMixedt5_accuracy;

	this.Mixedt6_accuracy=null;


	function getMixedt6_accuracy() {
		return this.Mixedt6_accuracy;
	}
	this.getMixedt6_accuracy=getMixedt6_accuracy;


	function setMixedt6_accuracy(v){
		this.Mixedt6_accuracy=v;
	}
	this.setMixedt6_accuracy=setMixedt6_accuracy;

	this.Mixedt7_accuracy=null;


	function getMixedt7_accuracy() {
		return this.Mixedt7_accuracy;
	}
	this.getMixedt7_accuracy=getMixedt7_accuracy;


	function setMixedt7_accuracy(v){
		this.Mixedt7_accuracy=v;
	}
	this.setMixedt7_accuracy=setMixedt7_accuracy;

	this.Mixedt8_accuracy=null;


	function getMixedt8_accuracy() {
		return this.Mixedt8_accuracy;
	}
	this.getMixedt8_accuracy=getMixedt8_accuracy;


	function setMixedt8_accuracy(v){
		this.Mixedt8_accuracy=v;
	}
	this.setMixedt8_accuracy=setMixedt8_accuracy;

	this.Mixedt9_accuracy=null;


	function getMixedt9_accuracy() {
		return this.Mixedt9_accuracy;
	}
	this.getMixedt9_accuracy=getMixedt9_accuracy;


	function setMixedt9_accuracy(v){
		this.Mixedt9_accuracy=v;
	}
	this.setMixedt9_accuracy=setMixedt9_accuracy;

	this.Mixedt10_accuracy=null;


	function getMixedt10_accuracy() {
		return this.Mixedt10_accuracy;
	}
	this.getMixedt10_accuracy=getMixedt10_accuracy;


	function setMixedt10_accuracy(v){
		this.Mixedt10_accuracy=v;
	}
	this.setMixedt10_accuracy=setMixedt10_accuracy;

	this.Mixedt11_accuracy=null;


	function getMixedt11_accuracy() {
		return this.Mixedt11_accuracy;
	}
	this.getMixedt11_accuracy=getMixedt11_accuracy;


	function setMixedt11_accuracy(v){
		this.Mixedt11_accuracy=v;
	}
	this.setMixedt11_accuracy=setMixedt11_accuracy;

	this.Mixedt12_accuracy=null;


	function getMixedt12_accuracy() {
		return this.Mixedt12_accuracy;
	}
	this.getMixedt12_accuracy=getMixedt12_accuracy;


	function setMixedt12_accuracy(v){
		this.Mixedt12_accuracy=v;
	}
	this.setMixedt12_accuracy=setMixedt12_accuracy;

	this.Newt1_accuracy=null;


	function getNewt1_accuracy() {
		return this.Newt1_accuracy;
	}
	this.getNewt1_accuracy=getNewt1_accuracy;


	function setNewt1_accuracy(v){
		this.Newt1_accuracy=v;
	}
	this.setNewt1_accuracy=setNewt1_accuracy;

	this.Newt2_accuracy=null;


	function getNewt2_accuracy() {
		return this.Newt2_accuracy;
	}
	this.getNewt2_accuracy=getNewt2_accuracy;


	function setNewt2_accuracy(v){
		this.Newt2_accuracy=v;
	}
	this.setNewt2_accuracy=setNewt2_accuracy;

	this.Newt3_accuracy=null;


	function getNewt3_accuracy() {
		return this.Newt3_accuracy;
	}
	this.getNewt3_accuracy=getNewt3_accuracy;


	function setNewt3_accuracy(v){
		this.Newt3_accuracy=v;
	}
	this.setNewt3_accuracy=setNewt3_accuracy;

	this.Newt4_accuracy=null;


	function getNewt4_accuracy() {
		return this.Newt4_accuracy;
	}
	this.getNewt4_accuracy=getNewt4_accuracy;


	function setNewt4_accuracy(v){
		this.Newt4_accuracy=v;
	}
	this.setNewt4_accuracy=setNewt4_accuracy;

	this.Newt5_accuracy=null;


	function getNewt5_accuracy() {
		return this.Newt5_accuracy;
	}
	this.getNewt5_accuracy=getNewt5_accuracy;


	function setNewt5_accuracy(v){
		this.Newt5_accuracy=v;
	}
	this.setNewt5_accuracy=setNewt5_accuracy;

	this.Newt6_accuracy=null;


	function getNewt6_accuracy() {
		return this.Newt6_accuracy;
	}
	this.getNewt6_accuracy=getNewt6_accuracy;


	function setNewt6_accuracy(v){
		this.Newt6_accuracy=v;
	}
	this.setNewt6_accuracy=setNewt6_accuracy;

	this.Newt7_accuracy=null;


	function getNewt7_accuracy() {
		return this.Newt7_accuracy;
	}
	this.getNewt7_accuracy=getNewt7_accuracy;


	function setNewt7_accuracy(v){
		this.Newt7_accuracy=v;
	}
	this.setNewt7_accuracy=setNewt7_accuracy;

	this.Newt8_accuracy=null;


	function getNewt8_accuracy() {
		return this.Newt8_accuracy;
	}
	this.getNewt8_accuracy=getNewt8_accuracy;


	function setNewt8_accuracy(v){
		this.Newt8_accuracy=v;
	}
	this.setNewt8_accuracy=setNewt8_accuracy;

	this.Newt9_accuracy=null;


	function getNewt9_accuracy() {
		return this.Newt9_accuracy;
	}
	this.getNewt9_accuracy=getNewt9_accuracy;


	function setNewt9_accuracy(v){
		this.Newt9_accuracy=v;
	}
	this.setNewt9_accuracy=setNewt9_accuracy;

	this.Newt10_accuracy=null;


	function getNewt10_accuracy() {
		return this.Newt10_accuracy;
	}
	this.getNewt10_accuracy=getNewt10_accuracy;


	function setNewt10_accuracy(v){
		this.Newt10_accuracy=v;
	}
	this.setNewt10_accuracy=setNewt10_accuracy;

	this.Newt11_accuracy=null;


	function getNewt11_accuracy() {
		return this.Newt11_accuracy;
	}
	this.getNewt11_accuracy=getNewt11_accuracy;


	function setNewt11_accuracy(v){
		this.Newt11_accuracy=v;
	}
	this.setNewt11_accuracy=setNewt11_accuracy;

	this.Newt12_accuracy=null;


	function getNewt12_accuracy() {
		return this.Newt12_accuracy;
	}
	this.getNewt12_accuracy=getNewt12_accuracy;


	function setNewt12_accuracy(v){
		this.Newt12_accuracy=v;
	}
	this.setNewt12_accuracy=setNewt12_accuracy;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="IntactT1/accuracy"){
				return this.Intactt1_accuracy ;
			} else 
			if(xmlPath=="IntactT2/accuracy"){
				return this.Intactt2_accuracy ;
			} else 
			if(xmlPath=="IntactT3/accuracy"){
				return this.Intactt3_accuracy ;
			} else 
			if(xmlPath=="IntactT4/accuracy"){
				return this.Intactt4_accuracy ;
			} else 
			if(xmlPath=="IntactT5/accuracy"){
				return this.Intactt5_accuracy ;
			} else 
			if(xmlPath=="IntactT6/accuracy"){
				return this.Intactt6_accuracy ;
			} else 
			if(xmlPath=="IntactT7/accuracy"){
				return this.Intactt7_accuracy ;
			} else 
			if(xmlPath=="IntactT8/accuracy"){
				return this.Intactt8_accuracy ;
			} else 
			if(xmlPath=="IntactT9/accuracy"){
				return this.Intactt9_accuracy ;
			} else 
			if(xmlPath=="IntactT10/accuracy"){
				return this.Intactt10_accuracy ;
			} else 
			if(xmlPath=="IntactT11/accuracy"){
				return this.Intactt11_accuracy ;
			} else 
			if(xmlPath=="IntactT12/accuracy"){
				return this.Intactt12_accuracy ;
			} else 
			if(xmlPath=="MixedT1/accuracy"){
				return this.Mixedt1_accuracy ;
			} else 
			if(xmlPath=="MixedT2/accuracy"){
				return this.Mixedt2_accuracy ;
			} else 
			if(xmlPath=="MixedT3/accuracy"){
				return this.Mixedt3_accuracy ;
			} else 
			if(xmlPath=="MixedT4/accuracy"){
				return this.Mixedt4_accuracy ;
			} else 
			if(xmlPath=="MixedT5/accuracy"){
				return this.Mixedt5_accuracy ;
			} else 
			if(xmlPath=="MixedT6/accuracy"){
				return this.Mixedt6_accuracy ;
			} else 
			if(xmlPath=="MixedT7/accuracy"){
				return this.Mixedt7_accuracy ;
			} else 
			if(xmlPath=="MixedT8/accuracy"){
				return this.Mixedt8_accuracy ;
			} else 
			if(xmlPath=="MixedT9/accuracy"){
				return this.Mixedt9_accuracy ;
			} else 
			if(xmlPath=="MixedT10/accuracy"){
				return this.Mixedt10_accuracy ;
			} else 
			if(xmlPath=="MixedT11/accuracy"){
				return this.Mixedt11_accuracy ;
			} else 
			if(xmlPath=="MixedT12/accuracy"){
				return this.Mixedt12_accuracy ;
			} else 
			if(xmlPath=="NewT1/accuracy"){
				return this.Newt1_accuracy ;
			} else 
			if(xmlPath=="NewT2/accuracy"){
				return this.Newt2_accuracy ;
			} else 
			if(xmlPath=="NewT3/accuracy"){
				return this.Newt3_accuracy ;
			} else 
			if(xmlPath=="NewT4/accuracy"){
				return this.Newt4_accuracy ;
			} else 
			if(xmlPath=="NewT5/accuracy"){
				return this.Newt5_accuracy ;
			} else 
			if(xmlPath=="NewT6/accuracy"){
				return this.Newt6_accuracy ;
			} else 
			if(xmlPath=="NewT7/accuracy"){
				return this.Newt7_accuracy ;
			} else 
			if(xmlPath=="NewT8/accuracy"){
				return this.Newt8_accuracy ;
			} else 
			if(xmlPath=="NewT9/accuracy"){
				return this.Newt9_accuracy ;
			} else 
			if(xmlPath=="NewT10/accuracy"){
				return this.Newt10_accuracy ;
			} else 
			if(xmlPath=="NewT11/accuracy"){
				return this.Newt11_accuracy ;
			} else 
			if(xmlPath=="NewT12/accuracy"){
				return this.Newt12_accuracy ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="IntactT1/accuracy"){
				this.Intactt1_accuracy=value;
			} else 
			if(xmlPath=="IntactT2/accuracy"){
				this.Intactt2_accuracy=value;
			} else 
			if(xmlPath=="IntactT3/accuracy"){
				this.Intactt3_accuracy=value;
			} else 
			if(xmlPath=="IntactT4/accuracy"){
				this.Intactt4_accuracy=value;
			} else 
			if(xmlPath=="IntactT5/accuracy"){
				this.Intactt5_accuracy=value;
			} else 
			if(xmlPath=="IntactT6/accuracy"){
				this.Intactt6_accuracy=value;
			} else 
			if(xmlPath=="IntactT7/accuracy"){
				this.Intactt7_accuracy=value;
			} else 
			if(xmlPath=="IntactT8/accuracy"){
				this.Intactt8_accuracy=value;
			} else 
			if(xmlPath=="IntactT9/accuracy"){
				this.Intactt9_accuracy=value;
			} else 
			if(xmlPath=="IntactT10/accuracy"){
				this.Intactt10_accuracy=value;
			} else 
			if(xmlPath=="IntactT11/accuracy"){
				this.Intactt11_accuracy=value;
			} else 
			if(xmlPath=="IntactT12/accuracy"){
				this.Intactt12_accuracy=value;
			} else 
			if(xmlPath=="MixedT1/accuracy"){
				this.Mixedt1_accuracy=value;
			} else 
			if(xmlPath=="MixedT2/accuracy"){
				this.Mixedt2_accuracy=value;
			} else 
			if(xmlPath=="MixedT3/accuracy"){
				this.Mixedt3_accuracy=value;
			} else 
			if(xmlPath=="MixedT4/accuracy"){
				this.Mixedt4_accuracy=value;
			} else 
			if(xmlPath=="MixedT5/accuracy"){
				this.Mixedt5_accuracy=value;
			} else 
			if(xmlPath=="MixedT6/accuracy"){
				this.Mixedt6_accuracy=value;
			} else 
			if(xmlPath=="MixedT7/accuracy"){
				this.Mixedt7_accuracy=value;
			} else 
			if(xmlPath=="MixedT8/accuracy"){
				this.Mixedt8_accuracy=value;
			} else 
			if(xmlPath=="MixedT9/accuracy"){
				this.Mixedt9_accuracy=value;
			} else 
			if(xmlPath=="MixedT10/accuracy"){
				this.Mixedt10_accuracy=value;
			} else 
			if(xmlPath=="MixedT11/accuracy"){
				this.Mixedt11_accuracy=value;
			} else 
			if(xmlPath=="MixedT12/accuracy"){
				this.Mixedt12_accuracy=value;
			} else 
			if(xmlPath=="NewT1/accuracy"){
				this.Newt1_accuracy=value;
			} else 
			if(xmlPath=="NewT2/accuracy"){
				this.Newt2_accuracy=value;
			} else 
			if(xmlPath=="NewT3/accuracy"){
				this.Newt3_accuracy=value;
			} else 
			if(xmlPath=="NewT4/accuracy"){
				this.Newt4_accuracy=value;
			} else 
			if(xmlPath=="NewT5/accuracy"){
				this.Newt5_accuracy=value;
			} else 
			if(xmlPath=="NewT6/accuracy"){
				this.Newt6_accuracy=value;
			} else 
			if(xmlPath=="NewT7/accuracy"){
				this.Newt7_accuracy=value;
			} else 
			if(xmlPath=="NewT8/accuracy"){
				this.Newt8_accuracy=value;
			} else 
			if(xmlPath=="NewT9/accuracy"){
				this.Newt9_accuracy=value;
			} else 
			if(xmlPath=="NewT10/accuracy"){
				this.Newt10_accuracy=value;
			} else 
			if(xmlPath=="NewT11/accuracy"){
				this.Newt11_accuracy=value;
			} else 
			if(xmlPath=="NewT12/accuracy"){
				this.Newt12_accuracy=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="IntactT1/accuracy"){
			return "field_data";
		}else if (xmlPath=="IntactT2/accuracy"){
			return "field_data";
		}else if (xmlPath=="IntactT3/accuracy"){
			return "field_data";
		}else if (xmlPath=="IntactT4/accuracy"){
			return "field_data";
		}else if (xmlPath=="IntactT5/accuracy"){
			return "field_data";
		}else if (xmlPath=="IntactT6/accuracy"){
			return "field_data";
		}else if (xmlPath=="IntactT7/accuracy"){
			return "field_data";
		}else if (xmlPath=="IntactT8/accuracy"){
			return "field_data";
		}else if (xmlPath=="IntactT9/accuracy"){
			return "field_data";
		}else if (xmlPath=="IntactT10/accuracy"){
			return "field_data";
		}else if (xmlPath=="IntactT11/accuracy"){
			return "field_data";
		}else if (xmlPath=="IntactT12/accuracy"){
			return "field_data";
		}else if (xmlPath=="MixedT1/accuracy"){
			return "field_data";
		}else if (xmlPath=="MixedT2/accuracy"){
			return "field_data";
		}else if (xmlPath=="MixedT3/accuracy"){
			return "field_data";
		}else if (xmlPath=="MixedT4/accuracy"){
			return "field_data";
		}else if (xmlPath=="MixedT5/accuracy"){
			return "field_data";
		}else if (xmlPath=="MixedT6/accuracy"){
			return "field_data";
		}else if (xmlPath=="MixedT7/accuracy"){
			return "field_data";
		}else if (xmlPath=="MixedT8/accuracy"){
			return "field_data";
		}else if (xmlPath=="MixedT9/accuracy"){
			return "field_data";
		}else if (xmlPath=="MixedT10/accuracy"){
			return "field_data";
		}else if (xmlPath=="MixedT11/accuracy"){
			return "field_data";
		}else if (xmlPath=="MixedT12/accuracy"){
			return "field_data";
		}else if (xmlPath=="NewT1/accuracy"){
			return "field_data";
		}else if (xmlPath=="NewT2/accuracy"){
			return "field_data";
		}else if (xmlPath=="NewT3/accuracy"){
			return "field_data";
		}else if (xmlPath=="NewT4/accuracy"){
			return "field_data";
		}else if (xmlPath=="NewT5/accuracy"){
			return "field_data";
		}else if (xmlPath=="NewT6/accuracy"){
			return "field_data";
		}else if (xmlPath=="NewT7/accuracy"){
			return "field_data";
		}else if (xmlPath=="NewT8/accuracy"){
			return "field_data";
		}else if (xmlPath=="NewT9/accuracy"){
			return "field_data";
		}else if (xmlPath=="NewT10/accuracy"){
			return "field_data";
		}else if (xmlPath=="NewT11/accuracy"){
			return "field_data";
		}else if (xmlPath=="NewT12/accuracy"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cbat:PairBinding";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cbat:PairBinding>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
			var child0=0;
			var att0=0;
			if(this.Intactt1_accuracy!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<cbat:IntactT1";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Intactt1_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Intactt1_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:IntactT1>";
			}
			}

			var child1=0;
			var att1=0;
			if(this.Intactt2_accuracy!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<cbat:IntactT2";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Intactt2_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Intactt2_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:IntactT2>";
			}
			}

			var child2=0;
			var att2=0;
			if(this.Intactt3_accuracy!=null)
			child2++;
			if(child2>0 || att2>0){
				xmlTxt+="\n<cbat:IntactT3";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Intactt3_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Intactt3_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:IntactT3>";
			}
			}

			var child3=0;
			var att3=0;
			if(this.Intactt4_accuracy!=null)
			child3++;
			if(child3>0 || att3>0){
				xmlTxt+="\n<cbat:IntactT4";
			if(child3==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Intactt4_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Intactt4_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:IntactT4>";
			}
			}

			var child4=0;
			var att4=0;
			if(this.Intactt5_accuracy!=null)
			child4++;
			if(child4>0 || att4>0){
				xmlTxt+="\n<cbat:IntactT5";
			if(child4==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Intactt5_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Intactt5_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:IntactT5>";
			}
			}

			var child5=0;
			var att5=0;
			if(this.Intactt6_accuracy!=null)
			child5++;
			if(child5>0 || att5>0){
				xmlTxt+="\n<cbat:IntactT6";
			if(child5==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Intactt6_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Intactt6_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:IntactT6>";
			}
			}

			var child6=0;
			var att6=0;
			if(this.Intactt7_accuracy!=null)
			child6++;
			if(child6>0 || att6>0){
				xmlTxt+="\n<cbat:IntactT7";
			if(child6==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Intactt7_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Intactt7_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:IntactT7>";
			}
			}

			var child7=0;
			var att7=0;
			if(this.Intactt8_accuracy!=null)
			child7++;
			if(child7>0 || att7>0){
				xmlTxt+="\n<cbat:IntactT8";
			if(child7==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Intactt8_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Intactt8_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:IntactT8>";
			}
			}

			var child8=0;
			var att8=0;
			if(this.Intactt9_accuracy!=null)
			child8++;
			if(child8>0 || att8>0){
				xmlTxt+="\n<cbat:IntactT9";
			if(child8==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Intactt9_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Intactt9_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:IntactT9>";
			}
			}

			var child9=0;
			var att9=0;
			if(this.Intactt10_accuracy!=null)
			child9++;
			if(child9>0 || att9>0){
				xmlTxt+="\n<cbat:IntactT10";
			if(child9==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Intactt10_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Intactt10_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:IntactT10>";
			}
			}

			var child10=0;
			var att10=0;
			if(this.Intactt11_accuracy!=null)
			child10++;
			if(child10>0 || att10>0){
				xmlTxt+="\n<cbat:IntactT11";
			if(child10==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Intactt11_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Intactt11_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:IntactT11>";
			}
			}

			var child11=0;
			var att11=0;
			if(this.Intactt12_accuracy!=null)
			child11++;
			if(child11>0 || att11>0){
				xmlTxt+="\n<cbat:IntactT12";
			if(child11==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Intactt12_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Intactt12_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:IntactT12>";
			}
			}

			var child12=0;
			var att12=0;
			if(this.Mixedt1_accuracy!=null)
			child12++;
			if(child12>0 || att12>0){
				xmlTxt+="\n<cbat:MixedT1";
			if(child12==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Mixedt1_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Mixedt1_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:MixedT1>";
			}
			}

			var child13=0;
			var att13=0;
			if(this.Mixedt2_accuracy!=null)
			child13++;
			if(child13>0 || att13>0){
				xmlTxt+="\n<cbat:MixedT2";
			if(child13==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Mixedt2_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Mixedt2_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:MixedT2>";
			}
			}

			var child14=0;
			var att14=0;
			if(this.Mixedt3_accuracy!=null)
			child14++;
			if(child14>0 || att14>0){
				xmlTxt+="\n<cbat:MixedT3";
			if(child14==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Mixedt3_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Mixedt3_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:MixedT3>";
			}
			}

			var child15=0;
			var att15=0;
			if(this.Mixedt4_accuracy!=null)
			child15++;
			if(child15>0 || att15>0){
				xmlTxt+="\n<cbat:MixedT4";
			if(child15==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Mixedt4_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Mixedt4_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:MixedT4>";
			}
			}

			var child16=0;
			var att16=0;
			if(this.Mixedt5_accuracy!=null)
			child16++;
			if(child16>0 || att16>0){
				xmlTxt+="\n<cbat:MixedT5";
			if(child16==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Mixedt5_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Mixedt5_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:MixedT5>";
			}
			}

			var child17=0;
			var att17=0;
			if(this.Mixedt6_accuracy!=null)
			child17++;
			if(child17>0 || att17>0){
				xmlTxt+="\n<cbat:MixedT6";
			if(child17==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Mixedt6_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Mixedt6_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:MixedT6>";
			}
			}

			var child18=0;
			var att18=0;
			if(this.Mixedt7_accuracy!=null)
			child18++;
			if(child18>0 || att18>0){
				xmlTxt+="\n<cbat:MixedT7";
			if(child18==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Mixedt7_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Mixedt7_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:MixedT7>";
			}
			}

			var child19=0;
			var att19=0;
			if(this.Mixedt8_accuracy!=null)
			child19++;
			if(child19>0 || att19>0){
				xmlTxt+="\n<cbat:MixedT8";
			if(child19==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Mixedt8_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Mixedt8_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:MixedT8>";
			}
			}

			var child20=0;
			var att20=0;
			if(this.Mixedt9_accuracy!=null)
			child20++;
			if(child20>0 || att20>0){
				xmlTxt+="\n<cbat:MixedT9";
			if(child20==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Mixedt9_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Mixedt9_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:MixedT9>";
			}
			}

			var child21=0;
			var att21=0;
			if(this.Mixedt10_accuracy!=null)
			child21++;
			if(child21>0 || att21>0){
				xmlTxt+="\n<cbat:MixedT10";
			if(child21==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Mixedt10_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Mixedt10_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:MixedT10>";
			}
			}

			var child22=0;
			var att22=0;
			if(this.Mixedt11_accuracy!=null)
			child22++;
			if(child22>0 || att22>0){
				xmlTxt+="\n<cbat:MixedT11";
			if(child22==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Mixedt11_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Mixedt11_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:MixedT11>";
			}
			}

			var child23=0;
			var att23=0;
			if(this.Mixedt12_accuracy!=null)
			child23++;
			if(child23>0 || att23>0){
				xmlTxt+="\n<cbat:MixedT12";
			if(child23==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Mixedt12_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Mixedt12_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:MixedT12>";
			}
			}

			var child24=0;
			var att24=0;
			if(this.Newt1_accuracy!=null)
			child24++;
			if(child24>0 || att24>0){
				xmlTxt+="\n<cbat:NewT1";
			if(child24==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Newt1_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Newt1_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:NewT1>";
			}
			}

			var child25=0;
			var att25=0;
			if(this.Newt2_accuracy!=null)
			child25++;
			if(child25>0 || att25>0){
				xmlTxt+="\n<cbat:NewT2";
			if(child25==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Newt2_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Newt2_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:NewT2>";
			}
			}

			var child26=0;
			var att26=0;
			if(this.Newt3_accuracy!=null)
			child26++;
			if(child26>0 || att26>0){
				xmlTxt+="\n<cbat:NewT3";
			if(child26==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Newt3_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Newt3_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:NewT3>";
			}
			}

			var child27=0;
			var att27=0;
			if(this.Newt4_accuracy!=null)
			child27++;
			if(child27>0 || att27>0){
				xmlTxt+="\n<cbat:NewT4";
			if(child27==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Newt4_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Newt4_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:NewT4>";
			}
			}

			var child28=0;
			var att28=0;
			if(this.Newt5_accuracy!=null)
			child28++;
			if(child28>0 || att28>0){
				xmlTxt+="\n<cbat:NewT5";
			if(child28==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Newt5_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Newt5_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:NewT5>";
			}
			}

			var child29=0;
			var att29=0;
			if(this.Newt6_accuracy!=null)
			child29++;
			if(child29>0 || att29>0){
				xmlTxt+="\n<cbat:NewT6";
			if(child29==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Newt6_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Newt6_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:NewT6>";
			}
			}

			var child30=0;
			var att30=0;
			if(this.Newt7_accuracy!=null)
			child30++;
			if(child30>0 || att30>0){
				xmlTxt+="\n<cbat:NewT7";
			if(child30==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Newt7_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Newt7_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:NewT7>";
			}
			}

			var child31=0;
			var att31=0;
			if(this.Newt8_accuracy!=null)
			child31++;
			if(child31>0 || att31>0){
				xmlTxt+="\n<cbat:NewT8";
			if(child31==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Newt8_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Newt8_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:NewT8>";
			}
			}

			var child32=0;
			var att32=0;
			if(this.Newt9_accuracy!=null)
			child32++;
			if(child32>0 || att32>0){
				xmlTxt+="\n<cbat:NewT9";
			if(child32==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Newt9_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Newt9_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:NewT9>";
			}
			}

			var child33=0;
			var att33=0;
			if(this.Newt10_accuracy!=null)
			child33++;
			if(child33>0 || att33>0){
				xmlTxt+="\n<cbat:NewT10";
			if(child33==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Newt10_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Newt10_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:NewT10>";
			}
			}

			var child34=0;
			var att34=0;
			if(this.Newt11_accuracy!=null)
			child34++;
			if(child34>0 || att34>0){
				xmlTxt+="\n<cbat:NewT11";
			if(child34==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Newt11_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Newt11_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:NewT11>";
			}
			}

			var child35=0;
			var att35=0;
			if(this.Newt12_accuracy!=null)
			child35++;
			if(child35>0 || att35>0){
				xmlTxt+="\n<cbat:NewT12";
			if(child35==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Newt12_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Newt12_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:NewT12>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
			if(this.Intactt1_accuracy!=null) return true;
			if(this.Intactt2_accuracy!=null) return true;
			if(this.Intactt3_accuracy!=null) return true;
			if(this.Intactt4_accuracy!=null) return true;
			if(this.Intactt5_accuracy!=null) return true;
			if(this.Intactt6_accuracy!=null) return true;
			if(this.Intactt7_accuracy!=null) return true;
			if(this.Intactt8_accuracy!=null) return true;
			if(this.Intactt9_accuracy!=null) return true;
			if(this.Intactt10_accuracy!=null) return true;
			if(this.Intactt11_accuracy!=null) return true;
			if(this.Intactt12_accuracy!=null) return true;
			if(this.Mixedt1_accuracy!=null) return true;
			if(this.Mixedt2_accuracy!=null) return true;
			if(this.Mixedt3_accuracy!=null) return true;
			if(this.Mixedt4_accuracy!=null) return true;
			if(this.Mixedt5_accuracy!=null) return true;
			if(this.Mixedt6_accuracy!=null) return true;
			if(this.Mixedt7_accuracy!=null) return true;
			if(this.Mixedt8_accuracy!=null) return true;
			if(this.Mixedt9_accuracy!=null) return true;
			if(this.Mixedt10_accuracy!=null) return true;
			if(this.Mixedt11_accuracy!=null) return true;
			if(this.Mixedt12_accuracy!=null) return true;
			if(this.Newt1_accuracy!=null) return true;
			if(this.Newt2_accuracy!=null) return true;
			if(this.Newt3_accuracy!=null) return true;
			if(this.Newt4_accuracy!=null) return true;
			if(this.Newt5_accuracy!=null) return true;
			if(this.Newt6_accuracy!=null) return true;
			if(this.Newt7_accuracy!=null) return true;
			if(this.Newt8_accuracy!=null) return true;
			if(this.Newt9_accuracy!=null) return true;
			if(this.Newt10_accuracy!=null) return true;
			if(this.Newt11_accuracy!=null) return true;
			if(this.Newt12_accuracy!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
