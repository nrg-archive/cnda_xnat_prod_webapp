/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function dian_plasmaData(){
this.xsiType="dian:plasmaData";

	this.getSchemaElementName=function(){
		return "plasmaData";
	}

	this.getFullSchemaElementName=function(){
		return "dian:plasmaData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Bldcollsess=null;


	function getBldcollsess() {
		return this.Bldcollsess;
	}
	this.getBldcollsess=getBldcollsess;


	function setBldcollsess(v){
		this.Bldcollsess=v;
	}
	this.setBldcollsess=setBldcollsess;

	this.Ab40=null;


	function getAb40() {
		return this.Ab40;
	}
	this.getAb40=getAb40;


	function setAb40(v){
		this.Ab40=v;
	}
	this.setAb40=setAb40;

	this.Ab40units=null;


	function getAb40units() {
		return this.Ab40units;
	}
	this.getAb40units=getAb40units;


	function setAb40units(v){
		this.Ab40units=v;
	}
	this.setAb40units=setAb40units;

	this.Ab40lot=null;


	function getAb40lot() {
		return this.Ab40lot;
	}
	this.getAb40lot=getAb40lot;


	function setAb40lot(v){
		this.Ab40lot=v;
	}
	this.setAb40lot=setAb40lot;

	this.Ab40plate=null;


	function getAb40plate() {
		return this.Ab40plate;
	}
	this.getAb40plate=getAb40plate;


	function setAb40plate(v){
		this.Ab40plate=v;
	}
	this.setAb40plate=setAb40plate;

	this.Ab40assaydate=null;


	function getAb40assaydate() {
		return this.Ab40assaydate;
	}
	this.getAb40assaydate=getAb40assaydate;


	function setAb40assaydate(v){
		this.Ab40assaydate=v;
	}
	this.setAb40assaydate=setAb40assaydate;

	this.Ab42=null;


	function getAb42() {
		return this.Ab42;
	}
	this.getAb42=getAb42;


	function setAb42(v){
		this.Ab42=v;
	}
	this.setAb42=setAb42;

	this.Ab42units=null;


	function getAb42units() {
		return this.Ab42units;
	}
	this.getAb42units=getAb42units;


	function setAb42units(v){
		this.Ab42units=v;
	}
	this.setAb42units=setAb42units;

	this.Ab42lot=null;


	function getAb42lot() {
		return this.Ab42lot;
	}
	this.getAb42lot=getAb42lot;


	function setAb42lot(v){
		this.Ab42lot=v;
	}
	this.setAb42lot=setAb42lot;

	this.Ab42plate=null;


	function getAb42plate() {
		return this.Ab42plate;
	}
	this.getAb42plate=getAb42plate;


	function setAb42plate(v){
		this.Ab42plate=v;
	}
	this.setAb42plate=setAb42plate;

	this.Ab42assaydate=null;


	function getAb42assaydate() {
		return this.Ab42assaydate;
	}
	this.getAb42assaydate=getAb42assaydate;


	function setAb42assaydate(v){
		this.Ab42assaydate=v;
	}
	this.setAb42assaydate=setAb42assaydate;

	this.Abx40=null;


	function getAbx40() {
		return this.Abx40;
	}
	this.getAbx40=getAbx40;


	function setAbx40(v){
		this.Abx40=v;
	}
	this.setAbx40=setAbx40;

	this.Abx40units=null;


	function getAbx40units() {
		return this.Abx40units;
	}
	this.getAbx40units=getAbx40units;


	function setAbx40units(v){
		this.Abx40units=v;
	}
	this.setAbx40units=setAbx40units;

	this.Abx40lot=null;


	function getAbx40lot() {
		return this.Abx40lot;
	}
	this.getAbx40lot=getAbx40lot;


	function setAbx40lot(v){
		this.Abx40lot=v;
	}
	this.setAbx40lot=setAbx40lot;

	this.Abx40plate=null;


	function getAbx40plate() {
		return this.Abx40plate;
	}
	this.getAbx40plate=getAbx40plate;


	function setAbx40plate(v){
		this.Abx40plate=v;
	}
	this.setAbx40plate=setAbx40plate;

	this.Abx40assaydate=null;


	function getAbx40assaydate() {
		return this.Abx40assaydate;
	}
	this.getAbx40assaydate=getAbx40assaydate;


	function setAbx40assaydate(v){
		this.Abx40assaydate=v;
	}
	this.setAbx40assaydate=setAbx40assaydate;

	this.Abx42=null;


	function getAbx42() {
		return this.Abx42;
	}
	this.getAbx42=getAbx42;


	function setAbx42(v){
		this.Abx42=v;
	}
	this.setAbx42=setAbx42;

	this.Abx42units=null;


	function getAbx42units() {
		return this.Abx42units;
	}
	this.getAbx42units=getAbx42units;


	function setAbx42units(v){
		this.Abx42units=v;
	}
	this.setAbx42units=setAbx42units;

	this.Abx42lot=null;


	function getAbx42lot() {
		return this.Abx42lot;
	}
	this.getAbx42lot=getAbx42lot;


	function setAbx42lot(v){
		this.Abx42lot=v;
	}
	this.setAbx42lot=setAbx42lot;

	this.Abx42plate=null;


	function getAbx42plate() {
		return this.Abx42plate;
	}
	this.getAbx42plate=getAbx42plate;


	function setAbx42plate(v){
		this.Abx42plate=v;
	}
	this.setAbx42plate=setAbx42plate;

	this.Abx42assaydate=null;


	function getAbx42assaydate() {
		return this.Abx42assaydate;
	}
	this.getAbx42assaydate=getAbx42assaydate;


	function setAbx42assaydate(v){
		this.Abx42assaydate=v;
	}
	this.setAbx42assaydate=setAbx42assaydate;

	this.PlasmaAb40PlusQcfails=null;


	function getPlasmaAb40PlusQcfails() {
		return this.PlasmaAb40PlusQcfails;
	}
	this.getPlasmaAb40PlusQcfails=getPlasmaAb40PlusQcfails;


	function setPlasmaAb40PlusQcfails(v){
		this.PlasmaAb40PlusQcfails=v;
	}
	this.setPlasmaAb40PlusQcfails=setPlasmaAb40PlusQcfails;

	this.PlasmaAb40Comments=null;


	function getPlasmaAb40Comments() {
		return this.PlasmaAb40Comments;
	}
	this.getPlasmaAb40Comments=getPlasmaAb40Comments;


	function setPlasmaAb40Comments(v){
		this.PlasmaAb40Comments=v;
	}
	this.setPlasmaAb40Comments=setPlasmaAb40Comments;

	this.PlasmaAb42PlusQcfails=null;


	function getPlasmaAb42PlusQcfails() {
		return this.PlasmaAb42PlusQcfails;
	}
	this.getPlasmaAb42PlusQcfails=getPlasmaAb42PlusQcfails;


	function setPlasmaAb42PlusQcfails(v){
		this.PlasmaAb42PlusQcfails=v;
	}
	this.setPlasmaAb42PlusQcfails=setPlasmaAb42PlusQcfails;

	this.PlasmaAb42Comments=null;


	function getPlasmaAb42Comments() {
		return this.PlasmaAb42Comments;
	}
	this.getPlasmaAb42Comments=getPlasmaAb42Comments;


	function setPlasmaAb42Comments(v){
		this.PlasmaAb42Comments=v;
	}
	this.setPlasmaAb42Comments=setPlasmaAb42Comments;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="bldCollSess"){
				return this.Bldcollsess ;
			} else 
			if(xmlPath=="Ab40"){
				return this.Ab40 ;
			} else 
			if(xmlPath=="Ab40Units"){
				return this.Ab40units ;
			} else 
			if(xmlPath=="Ab40Lot"){
				return this.Ab40lot ;
			} else 
			if(xmlPath=="Ab40Plate"){
				return this.Ab40plate ;
			} else 
			if(xmlPath=="Ab40AssayDate"){
				return this.Ab40assaydate ;
			} else 
			if(xmlPath=="Ab42"){
				return this.Ab42 ;
			} else 
			if(xmlPath=="Ab42Units"){
				return this.Ab42units ;
			} else 
			if(xmlPath=="Ab42Lot"){
				return this.Ab42lot ;
			} else 
			if(xmlPath=="Ab42Plate"){
				return this.Ab42plate ;
			} else 
			if(xmlPath=="Ab42AssayDate"){
				return this.Ab42assaydate ;
			} else 
			if(xmlPath=="Abx40"){
				return this.Abx40 ;
			} else 
			if(xmlPath=="Abx40Units"){
				return this.Abx40units ;
			} else 
			if(xmlPath=="Abx40Lot"){
				return this.Abx40lot ;
			} else 
			if(xmlPath=="Abx40Plate"){
				return this.Abx40plate ;
			} else 
			if(xmlPath=="Abx40AssayDate"){
				return this.Abx40assaydate ;
			} else 
			if(xmlPath=="Abx42"){
				return this.Abx42 ;
			} else 
			if(xmlPath=="Abx42Units"){
				return this.Abx42units ;
			} else 
			if(xmlPath=="Abx42Lot"){
				return this.Abx42lot ;
			} else 
			if(xmlPath=="Abx42Plate"){
				return this.Abx42plate ;
			} else 
			if(xmlPath=="Abx42AssayDate"){
				return this.Abx42assaydate ;
			} else 
			if(xmlPath=="PLASMA_AB40_plus_QCFAILS"){
				return this.PlasmaAb40PlusQcfails ;
			} else 
			if(xmlPath=="PLASMA_AB40_COMMENTS"){
				return this.PlasmaAb40Comments ;
			} else 
			if(xmlPath=="PLASMA_AB42_plus_QCFAILS"){
				return this.PlasmaAb42PlusQcfails ;
			} else 
			if(xmlPath=="PLASMA_AB42_COMMENTS"){
				return this.PlasmaAb42Comments ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="bldCollSess"){
				this.Bldcollsess=value;
			} else 
			if(xmlPath=="Ab40"){
				this.Ab40=value;
			} else 
			if(xmlPath=="Ab40Units"){
				this.Ab40units=value;
			} else 
			if(xmlPath=="Ab40Lot"){
				this.Ab40lot=value;
			} else 
			if(xmlPath=="Ab40Plate"){
				this.Ab40plate=value;
			} else 
			if(xmlPath=="Ab40AssayDate"){
				this.Ab40assaydate=value;
			} else 
			if(xmlPath=="Ab42"){
				this.Ab42=value;
			} else 
			if(xmlPath=="Ab42Units"){
				this.Ab42units=value;
			} else 
			if(xmlPath=="Ab42Lot"){
				this.Ab42lot=value;
			} else 
			if(xmlPath=="Ab42Plate"){
				this.Ab42plate=value;
			} else 
			if(xmlPath=="Ab42AssayDate"){
				this.Ab42assaydate=value;
			} else 
			if(xmlPath=="Abx40"){
				this.Abx40=value;
			} else 
			if(xmlPath=="Abx40Units"){
				this.Abx40units=value;
			} else 
			if(xmlPath=="Abx40Lot"){
				this.Abx40lot=value;
			} else 
			if(xmlPath=="Abx40Plate"){
				this.Abx40plate=value;
			} else 
			if(xmlPath=="Abx40AssayDate"){
				this.Abx40assaydate=value;
			} else 
			if(xmlPath=="Abx42"){
				this.Abx42=value;
			} else 
			if(xmlPath=="Abx42Units"){
				this.Abx42units=value;
			} else 
			if(xmlPath=="Abx42Lot"){
				this.Abx42lot=value;
			} else 
			if(xmlPath=="Abx42Plate"){
				this.Abx42plate=value;
			} else 
			if(xmlPath=="Abx42AssayDate"){
				this.Abx42assaydate=value;
			} else 
			if(xmlPath=="PLASMA_AB40_plus_QCFAILS"){
				this.PlasmaAb40PlusQcfails=value;
			} else 
			if(xmlPath=="PLASMA_AB40_COMMENTS"){
				this.PlasmaAb40Comments=value;
			} else 
			if(xmlPath=="PLASMA_AB42_plus_QCFAILS"){
				this.PlasmaAb42PlusQcfails=value;
			} else 
			if(xmlPath=="PLASMA_AB42_COMMENTS"){
				this.PlasmaAb42Comments=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="bldCollSess"){
			return "field_data";
		}else if (xmlPath=="Ab40"){
			return "field_data";
		}else if (xmlPath=="Ab40Units"){
			return "field_data";
		}else if (xmlPath=="Ab40Lot"){
			return "field_data";
		}else if (xmlPath=="Ab40Plate"){
			return "field_data";
		}else if (xmlPath=="Ab40AssayDate"){
			return "field_data";
		}else if (xmlPath=="Ab42"){
			return "field_data";
		}else if (xmlPath=="Ab42Units"){
			return "field_data";
		}else if (xmlPath=="Ab42Lot"){
			return "field_data";
		}else if (xmlPath=="Ab42Plate"){
			return "field_data";
		}else if (xmlPath=="Ab42AssayDate"){
			return "field_data";
		}else if (xmlPath=="Abx40"){
			return "field_data";
		}else if (xmlPath=="Abx40Units"){
			return "field_data";
		}else if (xmlPath=="Abx40Lot"){
			return "field_data";
		}else if (xmlPath=="Abx40Plate"){
			return "field_data";
		}else if (xmlPath=="Abx40AssayDate"){
			return "field_data";
		}else if (xmlPath=="Abx42"){
			return "field_data";
		}else if (xmlPath=="Abx42Units"){
			return "field_data";
		}else if (xmlPath=="Abx42Lot"){
			return "field_data";
		}else if (xmlPath=="Abx42Plate"){
			return "field_data";
		}else if (xmlPath=="Abx42AssayDate"){
			return "field_data";
		}else if (xmlPath=="PLASMA_AB40_plus_QCFAILS"){
			return "field_data";
		}else if (xmlPath=="PLASMA_AB40_COMMENTS"){
			return "field_data";
		}else if (xmlPath=="PLASMA_AB42_plus_QCFAILS"){
			return "field_data";
		}else if (xmlPath=="PLASMA_AB42_COMMENTS"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<dian:PLASMA";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</dian:PLASMA>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Bldcollsess!=null){
			xmlTxt+="\n<dian:bldCollSess";
			xmlTxt+=">";
			xmlTxt+=this.Bldcollsess.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:bldCollSess>";
		}
		if (this.Ab40!=null){
			xmlTxt+="\n<dian:Ab40";
			xmlTxt+=">";
			xmlTxt+=this.Ab40;
			xmlTxt+="</dian:Ab40>";
		}
		if (this.Ab40units!=null){
			xmlTxt+="\n<dian:Ab40Units";
			xmlTxt+=">";
			xmlTxt+=this.Ab40units.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:Ab40Units>";
		}
		if (this.Ab40lot!=null){
			xmlTxt+="\n<dian:Ab40Lot";
			xmlTxt+=">";
			xmlTxt+=this.Ab40lot.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:Ab40Lot>";
		}
		if (this.Ab40plate!=null){
			xmlTxt+="\n<dian:Ab40Plate";
			xmlTxt+=">";
			xmlTxt+=this.Ab40plate.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:Ab40Plate>";
		}
		if (this.Ab40assaydate!=null){
			xmlTxt+="\n<dian:Ab40AssayDate";
			xmlTxt+=">";
			xmlTxt+=this.Ab40assaydate;
			xmlTxt+="</dian:Ab40AssayDate>";
		}
		if (this.Ab42!=null){
			xmlTxt+="\n<dian:Ab42";
			xmlTxt+=">";
			xmlTxt+=this.Ab42;
			xmlTxt+="</dian:Ab42>";
		}
		if (this.Ab42units!=null){
			xmlTxt+="\n<dian:Ab42Units";
			xmlTxt+=">";
			xmlTxt+=this.Ab42units.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:Ab42Units>";
		}
		if (this.Ab42lot!=null){
			xmlTxt+="\n<dian:Ab42Lot";
			xmlTxt+=">";
			xmlTxt+=this.Ab42lot.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:Ab42Lot>";
		}
		if (this.Ab42plate!=null){
			xmlTxt+="\n<dian:Ab42Plate";
			xmlTxt+=">";
			xmlTxt+=this.Ab42plate.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:Ab42Plate>";
		}
		if (this.Ab42assaydate!=null){
			xmlTxt+="\n<dian:Ab42AssayDate";
			xmlTxt+=">";
			xmlTxt+=this.Ab42assaydate;
			xmlTxt+="</dian:Ab42AssayDate>";
		}
		if (this.Abx40!=null){
			xmlTxt+="\n<dian:Abx40";
			xmlTxt+=">";
			xmlTxt+=this.Abx40;
			xmlTxt+="</dian:Abx40>";
		}
		if (this.Abx40units!=null){
			xmlTxt+="\n<dian:Abx40Units";
			xmlTxt+=">";
			xmlTxt+=this.Abx40units.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:Abx40Units>";
		}
		if (this.Abx40lot!=null){
			xmlTxt+="\n<dian:Abx40Lot";
			xmlTxt+=">";
			xmlTxt+=this.Abx40lot.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:Abx40Lot>";
		}
		if (this.Abx40plate!=null){
			xmlTxt+="\n<dian:Abx40Plate";
			xmlTxt+=">";
			xmlTxt+=this.Abx40plate.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:Abx40Plate>";
		}
		if (this.Abx40assaydate!=null){
			xmlTxt+="\n<dian:Abx40AssayDate";
			xmlTxt+=">";
			xmlTxt+=this.Abx40assaydate;
			xmlTxt+="</dian:Abx40AssayDate>";
		}
		if (this.Abx42!=null){
			xmlTxt+="\n<dian:Abx42";
			xmlTxt+=">";
			xmlTxt+=this.Abx42;
			xmlTxt+="</dian:Abx42>";
		}
		if (this.Abx42units!=null){
			xmlTxt+="\n<dian:Abx42Units";
			xmlTxt+=">";
			xmlTxt+=this.Abx42units.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:Abx42Units>";
		}
		if (this.Abx42lot!=null){
			xmlTxt+="\n<dian:Abx42Lot";
			xmlTxt+=">";
			xmlTxt+=this.Abx42lot.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:Abx42Lot>";
		}
		if (this.Abx42plate!=null){
			xmlTxt+="\n<dian:Abx42Plate";
			xmlTxt+=">";
			xmlTxt+=this.Abx42plate.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:Abx42Plate>";
		}
		if (this.Abx42assaydate!=null){
			xmlTxt+="\n<dian:Abx42AssayDate";
			xmlTxt+=">";
			xmlTxt+=this.Abx42assaydate;
			xmlTxt+="</dian:Abx42AssayDate>";
		}
		if (this.PlasmaAb40PlusQcfails!=null){
			xmlTxt+="\n<dian:PLASMA_AB40_plus_QCFAILS";
			xmlTxt+=">";
			xmlTxt+=this.PlasmaAb40PlusQcfails;
			xmlTxt+="</dian:PLASMA_AB40_plus_QCFAILS>";
		}
		if (this.PlasmaAb40Comments!=null){
			xmlTxt+="\n<dian:PLASMA_AB40_COMMENTS";
			xmlTxt+=">";
			xmlTxt+=this.PlasmaAb40Comments.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:PLASMA_AB40_COMMENTS>";
		}
		if (this.PlasmaAb42PlusQcfails!=null){
			xmlTxt+="\n<dian:PLASMA_AB42_plus_QCFAILS";
			xmlTxt+=">";
			xmlTxt+=this.PlasmaAb42PlusQcfails;
			xmlTxt+="</dian:PLASMA_AB42_plus_QCFAILS>";
		}
		if (this.PlasmaAb42Comments!=null){
			xmlTxt+="\n<dian:PLASMA_AB42_COMMENTS";
			xmlTxt+=">";
			xmlTxt+=this.PlasmaAb42Comments.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:PLASMA_AB42_COMMENTS>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Bldcollsess!=null) return true;
		if (this.Ab40!=null) return true;
		if (this.Ab40units!=null) return true;
		if (this.Ab40lot!=null) return true;
		if (this.Ab40plate!=null) return true;
		if (this.Ab40assaydate!=null) return true;
		if (this.Ab42!=null) return true;
		if (this.Ab42units!=null) return true;
		if (this.Ab42lot!=null) return true;
		if (this.Ab42plate!=null) return true;
		if (this.Ab42assaydate!=null) return true;
		if (this.Abx40!=null) return true;
		if (this.Abx40units!=null) return true;
		if (this.Abx40lot!=null) return true;
		if (this.Abx40plate!=null) return true;
		if (this.Abx40assaydate!=null) return true;
		if (this.Abx42!=null) return true;
		if (this.Abx42units!=null) return true;
		if (this.Abx42lot!=null) return true;
		if (this.Abx42plate!=null) return true;
		if (this.Abx42assaydate!=null) return true;
		if (this.PlasmaAb40PlusQcfails!=null) return true;
		if (this.PlasmaAb40Comments!=null) return true;
		if (this.PlasmaAb42PlusQcfails!=null) return true;
		if (this.PlasmaAb42Comments!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
