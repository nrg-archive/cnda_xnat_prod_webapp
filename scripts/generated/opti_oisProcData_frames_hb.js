/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function opti_oisProcData_frames_hb(){
this.xsiType="opti:oisProcData_frames_hb";

	this.getSchemaElementName=function(){
		return "oisProcData_frames_hb";
	}

	this.getFullSchemaElementName=function(){
		return "opti:oisProcData_frames_hb";
	}

	this.FramesHb=null;


	function getFramesHb() {
		return this.FramesHb;
	}
	this.getFramesHb=getFramesHb;


	function setFramesHb(v){
		this.FramesHb=v;
	}
	this.setFramesHb=setFramesHb;

	this.Scanid=null;


	function getScanid() {
		return this.Scanid;
	}
	this.getScanid=getScanid;


	function setScanid(v){
		this.Scanid=v;
	}
	this.setScanid=setScanid;

	this.OptiOisprocdataFramesHbId=null;


	function getOptiOisprocdataFramesHbId() {
		return this.OptiOisprocdataFramesHbId;
	}
	this.getOptiOisprocdataFramesHbId=getOptiOisprocdataFramesHbId;


	function setOptiOisprocdataFramesHbId(v){
		this.OptiOisprocdataFramesHbId=v;
	}
	this.setOptiOisprocdataFramesHbId=setOptiOisprocdataFramesHbId;

	this.frames_hbs_frames_hb_opti_oisPr_id_fk=null;


	this.getframes_hbs_frames_hb_opti_oisPr_id=function() {
		return this.frames_hbs_frames_hb_opti_oisPr_id_fk;
	}


	this.setframes_hbs_frames_hb_opti_oisPr_id=function(v){
		this.frames_hbs_frames_hb_opti_oisPr_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="frames_hb"){
				return this.FramesHb ;
			} else 
			if(xmlPath=="scanid"){
				return this.Scanid ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="opti_oisProcData_frames_hb_id"){
				return this.OptiOisprocdataFramesHbId ;
			} else 
			if(xmlPath=="frames_hbs_frames_hb_opti_oisPr_id"){
				return this.frames_hbs_frames_hb_opti_oisPr_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="frames_hb"){
				this.FramesHb=value;
			} else 
			if(xmlPath=="scanid"){
				this.Scanid=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="opti_oisProcData_frames_hb_id"){
				this.OptiOisprocdataFramesHbId=value;
			} else 
			if(xmlPath=="frames_hbs_frames_hb_opti_oisPr_id"){
				this.frames_hbs_frames_hb_opti_oisPr_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="frames_hb"){
			return "field_data";
		}else if (xmlPath=="scanid"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<opti:oisProcData_frames_hb";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</opti:oisProcData_frames_hb>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.OptiOisprocdataFramesHbId!=null){
				if(hiddenCount++>0)str+=",";
				str+="opti_oisProcData_frames_hb_id=\"" + this.OptiOisprocdataFramesHbId + "\"";
			}
			if(this.frames_hbs_frames_hb_opti_oisPr_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="frames_hbs_frames_hb_opti_oisPr_id=\"" + this.frames_hbs_frames_hb_opti_oisPr_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.Scanid!=null)
			attTxt+=" scanid=\"" +this.Scanid +"\"";
		else attTxt+=" scanid=\"\"";//REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.FramesHb!=null){
			xmlTxt+=this.FramesHb;
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.OptiOisprocdataFramesHbId!=null) return true;
			if (this.frames_hbs_frames_hb_opti_oisPr_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.FramesHb!=null) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
