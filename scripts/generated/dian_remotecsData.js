/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function dian_remotecsData(){
this.xsiType="dian:remotecsData";

	this.getSchemaElementName=function(){
		return "remotecsData";
	}

	this.getFullSchemaElementName=function(){
		return "dian:remotecsData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Ad8mem=null;


	function getAd8mem() {
		return this.Ad8mem;
	}
	this.getAd8mem=getAd8mem;


	function setAd8mem(v){
		this.Ad8mem=v;
	}
	this.setAd8mem=setAd8mem;

	this.Ad8jud=null;


	function getAd8jud() {
		return this.Ad8jud;
	}
	this.getAd8jud=getAd8jud;


	function setAd8jud(v){
		this.Ad8jud=v;
	}
	this.setAd8jud=setAd8jud;

	this.Ad8hob=null;


	function getAd8hob() {
		return this.Ad8hob;
	}
	this.getAd8hob=getAd8hob;


	function setAd8hob(v){
		this.Ad8hob=v;
	}
	this.setAd8hob=setAd8hob;

	this.Ad8rep=null;


	function getAd8rep() {
		return this.Ad8rep;
	}
	this.getAd8rep=getAd8rep;


	function setAd8rep(v){
		this.Ad8rep=v;
	}
	this.setAd8rep=setAd8rep;

	this.Ad8tool=null;


	function getAd8tool() {
		return this.Ad8tool;
	}
	this.getAd8tool=getAd8tool;


	function setAd8tool(v){
		this.Ad8tool=v;
	}
	this.setAd8tool=setAd8tool;

	this.Ad8for=null;


	function getAd8for() {
		return this.Ad8for;
	}
	this.getAd8for=getAd8for;


	function setAd8for(v){
		this.Ad8for=v;
	}
	this.setAd8for=setAd8for;

	this.Ad8fin=null;


	function getAd8fin() {
		return this.Ad8fin;
	}
	this.getAd8fin=getAd8fin;


	function setAd8fin(v){
		this.Ad8fin=v;
	}
	this.setAd8fin=setAd8fin;

	this.Ad8appt=null;


	function getAd8appt() {
		return this.Ad8appt;
	}
	this.getAd8appt=getAd8appt;


	function setAd8appt(v){
		this.Ad8appt=v;
	}
	this.setAd8appt=setAd8appt;

	this.Famhist=null;


	function getFamhist() {
		return this.Famhist;
	}
	this.getFamhist=getFamhist;


	function setFamhist(v){
		this.Famhist=v;
	}
	this.setFamhist=setFamhist;

	this.Medcond=null;


	function getMedcond() {
		return this.Medcond;
	}
	this.getMedcond=getMedcond;


	function setMedcond(v){
		this.Medcond=v;
	}
	this.setMedcond=setMedcond;

	this.Medspec=null;


	function getMedspec() {
		return this.Medspec;
	}
	this.getMedspec=getMedspec;


	function setMedspec(v){
		this.Medspec=v;
	}
	this.setMedspec=setMedspec;

	this.Memory=null;


	function getMemory() {
		return this.Memory;
	}
	this.getMemory=getMemory;


	function setMemory(v){
		this.Memory=v;
	}
	this.setMemory=setMemory;

	this.Adl=null;


	function getAdl() {
		return this.Adl;
	}
	this.getAdl=getAdl;


	function setAdl(v){
		this.Adl=v;
	}
	this.setAdl=setAdl;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="AD8MEM"){
				return this.Ad8mem ;
			} else 
			if(xmlPath=="AD8JUD"){
				return this.Ad8jud ;
			} else 
			if(xmlPath=="AD8HOB"){
				return this.Ad8hob ;
			} else 
			if(xmlPath=="AD8REP"){
				return this.Ad8rep ;
			} else 
			if(xmlPath=="AD8TOOL"){
				return this.Ad8tool ;
			} else 
			if(xmlPath=="AD8FOR"){
				return this.Ad8for ;
			} else 
			if(xmlPath=="AD8FIN"){
				return this.Ad8fin ;
			} else 
			if(xmlPath=="AD8APPT"){
				return this.Ad8appt ;
			} else 
			if(xmlPath=="FAMHIST"){
				return this.Famhist ;
			} else 
			if(xmlPath=="MEDCOND"){
				return this.Medcond ;
			} else 
			if(xmlPath=="MEDSPEC"){
				return this.Medspec ;
			} else 
			if(xmlPath=="MEMORY"){
				return this.Memory ;
			} else 
			if(xmlPath=="ADL"){
				return this.Adl ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="AD8MEM"){
				this.Ad8mem=value;
			} else 
			if(xmlPath=="AD8JUD"){
				this.Ad8jud=value;
			} else 
			if(xmlPath=="AD8HOB"){
				this.Ad8hob=value;
			} else 
			if(xmlPath=="AD8REP"){
				this.Ad8rep=value;
			} else 
			if(xmlPath=="AD8TOOL"){
				this.Ad8tool=value;
			} else 
			if(xmlPath=="AD8FOR"){
				this.Ad8for=value;
			} else 
			if(xmlPath=="AD8FIN"){
				this.Ad8fin=value;
			} else 
			if(xmlPath=="AD8APPT"){
				this.Ad8appt=value;
			} else 
			if(xmlPath=="FAMHIST"){
				this.Famhist=value;
			} else 
			if(xmlPath=="MEDCOND"){
				this.Medcond=value;
			} else 
			if(xmlPath=="MEDSPEC"){
				this.Medspec=value;
			} else 
			if(xmlPath=="MEMORY"){
				this.Memory=value;
			} else 
			if(xmlPath=="ADL"){
				this.Adl=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="AD8MEM"){
			return "field_data";
		}else if (xmlPath=="AD8JUD"){
			return "field_data";
		}else if (xmlPath=="AD8HOB"){
			return "field_data";
		}else if (xmlPath=="AD8REP"){
			return "field_data";
		}else if (xmlPath=="AD8TOOL"){
			return "field_data";
		}else if (xmlPath=="AD8FOR"){
			return "field_data";
		}else if (xmlPath=="AD8FIN"){
			return "field_data";
		}else if (xmlPath=="AD8APPT"){
			return "field_data";
		}else if (xmlPath=="FAMHIST"){
			return "field_data";
		}else if (xmlPath=="MEDCOND"){
			return "field_data";
		}else if (xmlPath=="MEDSPEC"){
			return "field_data";
		}else if (xmlPath=="MEMORY"){
			return "field_data";
		}else if (xmlPath=="ADL"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<dian:REMOTECS";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</dian:REMOTECS>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Ad8mem!=null){
			xmlTxt+="\n<dian:AD8MEM";
			xmlTxt+=">";
			xmlTxt+=this.Ad8mem;
			xmlTxt+="</dian:AD8MEM>";
		}
		if (this.Ad8jud!=null){
			xmlTxt+="\n<dian:AD8JUD";
			xmlTxt+=">";
			xmlTxt+=this.Ad8jud;
			xmlTxt+="</dian:AD8JUD>";
		}
		if (this.Ad8hob!=null){
			xmlTxt+="\n<dian:AD8HOB";
			xmlTxt+=">";
			xmlTxt+=this.Ad8hob;
			xmlTxt+="</dian:AD8HOB>";
		}
		if (this.Ad8rep!=null){
			xmlTxt+="\n<dian:AD8REP";
			xmlTxt+=">";
			xmlTxt+=this.Ad8rep;
			xmlTxt+="</dian:AD8REP>";
		}
		if (this.Ad8tool!=null){
			xmlTxt+="\n<dian:AD8TOOL";
			xmlTxt+=">";
			xmlTxt+=this.Ad8tool;
			xmlTxt+="</dian:AD8TOOL>";
		}
		if (this.Ad8for!=null){
			xmlTxt+="\n<dian:AD8FOR";
			xmlTxt+=">";
			xmlTxt+=this.Ad8for;
			xmlTxt+="</dian:AD8FOR>";
		}
		if (this.Ad8fin!=null){
			xmlTxt+="\n<dian:AD8FIN";
			xmlTxt+=">";
			xmlTxt+=this.Ad8fin;
			xmlTxt+="</dian:AD8FIN>";
		}
		if (this.Ad8appt!=null){
			xmlTxt+="\n<dian:AD8APPT";
			xmlTxt+=">";
			xmlTxt+=this.Ad8appt;
			xmlTxt+="</dian:AD8APPT>";
		}
		if (this.Famhist!=null){
			xmlTxt+="\n<dian:FAMHIST";
			xmlTxt+=">";
			xmlTxt+=this.Famhist;
			xmlTxt+="</dian:FAMHIST>";
		}
		if (this.Medcond!=null){
			xmlTxt+="\n<dian:MEDCOND";
			xmlTxt+=">";
			xmlTxt+=this.Medcond;
			xmlTxt+="</dian:MEDCOND>";
		}
		if (this.Medspec!=null){
			xmlTxt+="\n<dian:MEDSPEC";
			xmlTxt+=">";
			xmlTxt+=this.Medspec.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:MEDSPEC>";
		}
		if (this.Memory!=null){
			xmlTxt+="\n<dian:MEMORY";
			xmlTxt+=">";
			xmlTxt+=this.Memory;
			xmlTxt+="</dian:MEMORY>";
		}
		if (this.Adl!=null){
			xmlTxt+="\n<dian:ADL";
			xmlTxt+=">";
			xmlTxt+=this.Adl;
			xmlTxt+="</dian:ADL>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Ad8mem!=null) return true;
		if (this.Ad8jud!=null) return true;
		if (this.Ad8hob!=null) return true;
		if (this.Ad8rep!=null) return true;
		if (this.Ad8tool!=null) return true;
		if (this.Ad8for!=null) return true;
		if (this.Ad8fin!=null) return true;
		if (this.Ad8appt!=null) return true;
		if (this.Famhist!=null) return true;
		if (this.Medcond!=null) return true;
		if (this.Medspec!=null) return true;
		if (this.Memory!=null) return true;
		if (this.Adl!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
