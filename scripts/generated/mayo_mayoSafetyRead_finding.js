/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function mayo_mayoSafetyRead_finding(){
this.xsiType="mayo:mayoSafetyRead_finding";

	this.getSchemaElementName=function(){
		return "mayoSafetyRead_finding";
	}

	this.getFullSchemaElementName=function(){
		return "mayo:mayoSafetyRead_finding";
	}

	this.Visit=null;


	function getVisit() {
		return this.Visit;
	}
	this.getVisit=getVisit;


	function setVisit(v){
		this.Visit=v;
	}
	this.setVisit=setVisit;

	this.Visittime=null;


	function getVisittime() {
		return this.Visittime;
	}
	this.getVisittime=getVisittime;


	function setVisittime(v){
		this.Visittime=v;
	}
	this.setVisittime=setVisittime;

	this.Findingtype=null;


	function getFindingtype() {
		return this.Findingtype;
	}
	this.getFindingtype=getFindingtype;


	function setFindingtype(v){
		this.Findingtype=v;
	}
	this.setFindingtype=setFindingtype;

	this.Change=null;


	function getChange() {
		return this.Change;
	}
	this.getChange=getChange;


	function setChange(v){
		this.Change=v;
	}
	this.setChange=setChange;


	this.isChange=function(defaultValue) {
		if(this.Change==null)return defaultValue;
		if(this.Change=="1" || this.Change==true)return true;
		return false;
	}

	this.Definitecount=null;


	function getDefinitecount() {
		return this.Definitecount;
	}
	this.getDefinitecount=getDefinitecount;


	function setDefinitecount(v){
		this.Definitecount=v;
	}
	this.setDefinitecount=setDefinitecount;

	this.Definitemagnitude=null;


	function getDefinitemagnitude() {
		return this.Definitemagnitude;
	}
	this.getDefinitemagnitude=getDefinitemagnitude;


	function setDefinitemagnitude(v){
		this.Definitemagnitude=v;
	}
	this.setDefinitemagnitude=setDefinitemagnitude;

	this.Possiblecount=null;


	function getPossiblecount() {
		return this.Possiblecount;
	}
	this.getPossiblecount=getPossiblecount;


	function setPossiblecount(v){
		this.Possiblecount=v;
	}
	this.setPossiblecount=setPossiblecount;

	this.Possiblemagnitude=null;


	function getPossiblemagnitude() {
		return this.Possiblemagnitude;
	}
	this.getPossiblemagnitude=getPossiblemagnitude;


	function setPossiblemagnitude(v){
		this.Possiblemagnitude=v;
	}
	this.setPossiblemagnitude=setPossiblemagnitude;

	this.Noimage=null;


	function getNoimage() {
		return this.Noimage;
	}
	this.getNoimage=getNoimage;


	function setNoimage(v){
		this.Noimage=v;
	}
	this.setNoimage=setNoimage;


	this.isNoimage=function(defaultValue) {
		if(this.Noimage==null)return defaultValue;
		if(this.Noimage=="1" || this.Noimage==true)return true;
		return false;
	}

	this.Failedimage=null;


	function getFailedimage() {
		return this.Failedimage;
	}
	this.getFailedimage=getFailedimage;


	function setFailedimage(v){
		this.Failedimage=v;
	}
	this.setFailedimage=setFailedimage;


	this.isFailedimage=function(defaultValue) {
		if(this.Failedimage==null)return defaultValue;
		if(this.Failedimage=="1" || this.Failedimage==true)return true;
		return false;
	}

	this.Seenotes=null;


	function getSeenotes() {
		return this.Seenotes;
	}
	this.getSeenotes=getSeenotes;


	function setSeenotes(v){
		this.Seenotes=v;
	}
	this.setSeenotes=setSeenotes;


	this.isSeenotes=function(defaultValue) {
		if(this.Seenotes==null)return defaultValue;
		if(this.Seenotes=="1" || this.Seenotes==true)return true;
		return false;
	}

	this.MayoMayosafetyreadFindingId=null;


	function getMayoMayosafetyreadFindingId() {
		return this.MayoMayosafetyreadFindingId;
	}
	this.getMayoMayosafetyreadFindingId=getMayoMayosafetyreadFindingId;


	function setMayoMayosafetyreadFindingId(v){
		this.MayoMayosafetyreadFindingId=v;
	}
	this.setMayoMayosafetyreadFindingId=setMayoMayosafetyreadFindingId;

	this.findings_finding_mayo_mayoSafet_id_fk=null;


	this.getfindings_finding_mayo_mayoSafet_id=function() {
		return this.findings_finding_mayo_mayoSafet_id_fk;
	}


	this.setfindings_finding_mayo_mayoSafet_id=function(v){
		this.findings_finding_mayo_mayoSafet_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="visit"){
				return this.Visit ;
			} else 
			if(xmlPath=="visitTime"){
				return this.Visittime ;
			} else 
			if(xmlPath=="findingType"){
				return this.Findingtype ;
			} else 
			if(xmlPath=="change"){
				return this.Change ;
			} else 
			if(xmlPath=="definiteCount"){
				return this.Definitecount ;
			} else 
			if(xmlPath=="definiteMagnitude"){
				return this.Definitemagnitude ;
			} else 
			if(xmlPath=="possibleCount"){
				return this.Possiblecount ;
			} else 
			if(xmlPath=="possibleMagnitude"){
				return this.Possiblemagnitude ;
			} else 
			if(xmlPath=="noImage"){
				return this.Noimage ;
			} else 
			if(xmlPath=="failedImage"){
				return this.Failedimage ;
			} else 
			if(xmlPath=="seeNotes"){
				return this.Seenotes ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="mayo_mayoSafetyRead_finding_id"){
				return this.MayoMayosafetyreadFindingId ;
			} else 
			if(xmlPath=="findings_finding_mayo_mayoSafet_id"){
				return this.findings_finding_mayo_mayoSafet_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="visit"){
				this.Visit=value;
			} else 
			if(xmlPath=="visitTime"){
				this.Visittime=value;
			} else 
			if(xmlPath=="findingType"){
				this.Findingtype=value;
			} else 
			if(xmlPath=="change"){
				this.Change=value;
			} else 
			if(xmlPath=="definiteCount"){
				this.Definitecount=value;
			} else 
			if(xmlPath=="definiteMagnitude"){
				this.Definitemagnitude=value;
			} else 
			if(xmlPath=="possibleCount"){
				this.Possiblecount=value;
			} else 
			if(xmlPath=="possibleMagnitude"){
				this.Possiblemagnitude=value;
			} else 
			if(xmlPath=="noImage"){
				this.Noimage=value;
			} else 
			if(xmlPath=="failedImage"){
				this.Failedimage=value;
			} else 
			if(xmlPath=="seeNotes"){
				this.Seenotes=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="mayo_mayoSafetyRead_finding_id"){
				this.MayoMayosafetyreadFindingId=value;
			} else 
			if(xmlPath=="findings_finding_mayo_mayoSafet_id"){
				this.findings_finding_mayo_mayoSafet_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="visit"){
			return "field_data";
		}else if (xmlPath=="visitTime"){
			return "field_data";
		}else if (xmlPath=="findingType"){
			return "field_data";
		}else if (xmlPath=="change"){
			return "field_data";
		}else if (xmlPath=="definiteCount"){
			return "field_data";
		}else if (xmlPath=="definiteMagnitude"){
			return "field_data";
		}else if (xmlPath=="possibleCount"){
			return "field_data";
		}else if (xmlPath=="possibleMagnitude"){
			return "field_data";
		}else if (xmlPath=="noImage"){
			return "field_data";
		}else if (xmlPath=="failedImage"){
			return "field_data";
		}else if (xmlPath=="seeNotes"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<mayo:mayoSafetyRead_finding";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</mayo:mayoSafetyRead_finding>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.MayoMayosafetyreadFindingId!=null){
				if(hiddenCount++>0)str+=",";
				str+="mayo_mayoSafetyRead_finding_id=\"" + this.MayoMayosafetyreadFindingId + "\"";
			}
			if(this.findings_finding_mayo_mayoSafet_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="findings_finding_mayo_mayoSafet_id=\"" + this.findings_finding_mayo_mayoSafet_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Visit!=null){
			xmlTxt+="\n<mayo:visit";
			xmlTxt+=">";
			xmlTxt+=this.Visit;
			xmlTxt+="</mayo:visit>";
		}
		if (this.Visittime!=null){
			xmlTxt+="\n<mayo:visitTime";
			xmlTxt+=">";
			xmlTxt+=this.Visittime;
			xmlTxt+="</mayo:visitTime>";
		}
		if (this.Findingtype!=null){
			xmlTxt+="\n<mayo:findingType";
			xmlTxt+=">";
			xmlTxt+=this.Findingtype.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</mayo:findingType>";
		}
		if (this.Change!=null){
			xmlTxt+="\n<mayo:change";
			xmlTxt+=">";
			xmlTxt+=this.Change;
			xmlTxt+="</mayo:change>";
		}
		if (this.Definitecount!=null){
			xmlTxt+="\n<mayo:definiteCount";
			xmlTxt+=">";
			xmlTxt+=this.Definitecount;
			xmlTxt+="</mayo:definiteCount>";
		}
		if (this.Definitemagnitude!=null){
			xmlTxt+="\n<mayo:definiteMagnitude";
			xmlTxt+=">";
			xmlTxt+=this.Definitemagnitude;
			xmlTxt+="</mayo:definiteMagnitude>";
		}
		if (this.Possiblecount!=null){
			xmlTxt+="\n<mayo:possibleCount";
			xmlTxt+=">";
			xmlTxt+=this.Possiblecount;
			xmlTxt+="</mayo:possibleCount>";
		}
		if (this.Possiblemagnitude!=null){
			xmlTxt+="\n<mayo:possibleMagnitude";
			xmlTxt+=">";
			xmlTxt+=this.Possiblemagnitude;
			xmlTxt+="</mayo:possibleMagnitude>";
		}
		if (this.Noimage!=null){
			xmlTxt+="\n<mayo:noImage";
			xmlTxt+=">";
			xmlTxt+=this.Noimage;
			xmlTxt+="</mayo:noImage>";
		}
		if (this.Failedimage!=null){
			xmlTxt+="\n<mayo:failedImage";
			xmlTxt+=">";
			xmlTxt+=this.Failedimage;
			xmlTxt+="</mayo:failedImage>";
		}
		if (this.Seenotes!=null){
			xmlTxt+="\n<mayo:seeNotes";
			xmlTxt+=">";
			xmlTxt+=this.Seenotes;
			xmlTxt+="</mayo:seeNotes>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.MayoMayosafetyreadFindingId!=null) return true;
			if (this.findings_finding_mayo_mayoSafet_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Visit!=null) return true;
		if (this.Visittime!=null) return true;
		if (this.Findingtype!=null) return true;
		if (this.Change!=null) return true;
		if (this.Definitecount!=null) return true;
		if (this.Definitemagnitude!=null) return true;
		if (this.Possiblecount!=null) return true;
		if (this.Possiblemagnitude!=null) return true;
		if (this.Noimage!=null) return true;
		if (this.Failedimage!=null) return true;
		if (this.Seenotes!=null) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
