/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:04 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function uds_a3sbfmhstData(){
this.xsiType="uds:a3sbfmhstData";

	this.getSchemaElementName=function(){
		return "a3sbfmhstData";
	}

	this.getFullSchemaElementName=function(){
		return "uds:a3sbfmhstData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Examdate=null;


	function getExamdate() {
		return this.Examdate;
	}
	this.getExamdate=getExamdate;


	function setExamdate(v){
		this.Examdate=v;
	}
	this.setExamdate=setExamdate;

	this.A3chg=null;


	function getA3chg() {
		return this.A3chg;
	}
	this.getA3chg=getA3chg;


	function setA3chg(v){
		this.A3chg=v;
	}
	this.setA3chg=setA3chg;

	this.Parchg=null;


	function getParchg() {
		return this.Parchg;
	}
	this.getParchg=getParchg;


	function setParchg(v){
		this.Parchg=v;
	}
	this.setParchg=setParchg;

	this.Familyid=null;


	function getFamilyid() {
		return this.Familyid;
	}
	this.getFamilyid=getFamilyid;


	function setFamilyid(v){
		this.Familyid=v;
	}
	this.setFamilyid=setFamilyid;

	this.Gmomid=null;


	function getGmomid() {
		return this.Gmomid;
	}
	this.getGmomid=getGmomid;


	function setGmomid(v){
		this.Gmomid=v;
	}
	this.setGmomid=setGmomid;

	this.Gmomyob=null;


	function getGmomyob() {
		return this.Gmomyob;
	}
	this.getGmomyob=getGmomyob;


	function setGmomyob(v){
		this.Gmomyob=v;
	}
	this.setGmomyob=setGmomyob;

	this.Gmomliv=null;


	function getGmomliv() {
		return this.Gmomliv;
	}
	this.getGmomliv=getGmomliv;


	function setGmomliv(v){
		this.Gmomliv=v;
	}
	this.setGmomliv=setGmomliv;

	this.Gmomyod=null;


	function getGmomyod() {
		return this.Gmomyod;
	}
	this.getGmomyod=getGmomyod;


	function setGmomyod(v){
		this.Gmomyod=v;
	}
	this.setGmomyod=setGmomyod;

	this.Gmomdem=null;


	function getGmomdem() {
		return this.Gmomdem;
	}
	this.getGmomdem=getGmomdem;


	function setGmomdem(v){
		this.Gmomdem=v;
	}
	this.setGmomdem=setGmomdem;

	this.Gmomonset=null;


	function getGmomonset() {
		return this.Gmomonset;
	}
	this.getGmomonset=getGmomonset;


	function setGmomonset(v){
		this.Gmomonset=v;
	}
	this.setGmomonset=setGmomonset;

	this.Gmomauto=null;


	function getGmomauto() {
		return this.Gmomauto;
	}
	this.getGmomauto=getGmomauto;


	function setGmomauto(v){
		this.Gmomauto=v;
	}
	this.setGmomauto=setGmomauto;

	this.Gdadid=null;


	function getGdadid() {
		return this.Gdadid;
	}
	this.getGdadid=getGdadid;


	function setGdadid(v){
		this.Gdadid=v;
	}
	this.setGdadid=setGdadid;

	this.Gdadyob=null;


	function getGdadyob() {
		return this.Gdadyob;
	}
	this.getGdadyob=getGdadyob;


	function setGdadyob(v){
		this.Gdadyob=v;
	}
	this.setGdadyob=setGdadyob;

	this.Gdadliv=null;


	function getGdadliv() {
		return this.Gdadliv;
	}
	this.getGdadliv=getGdadliv;


	function setGdadliv(v){
		this.Gdadliv=v;
	}
	this.setGdadliv=setGdadliv;

	this.Gdadyod=null;


	function getGdadyod() {
		return this.Gdadyod;
	}
	this.getGdadyod=getGdadyod;


	function setGdadyod(v){
		this.Gdadyod=v;
	}
	this.setGdadyod=setGdadyod;

	this.Gdaddem=null;


	function getGdaddem() {
		return this.Gdaddem;
	}
	this.getGdaddem=getGdaddem;


	function setGdaddem(v){
		this.Gdaddem=v;
	}
	this.setGdaddem=setGdaddem;

	this.Gdadonset=null;


	function getGdadonset() {
		return this.Gdadonset;
	}
	this.getGdadonset=getGdadonset;


	function setGdadonset(v){
		this.Gdadonset=v;
	}
	this.setGdadonset=setGdadonset;

	this.Gdadauto=null;


	function getGdadauto() {
		return this.Gdadauto;
	}
	this.getGdadauto=getGdadauto;


	function setGdadauto(v){
		this.Gdadauto=v;
	}
	this.setGdadauto=setGdadauto;

	this.Momid=null;


	function getMomid() {
		return this.Momid;
	}
	this.getMomid=getMomid;


	function setMomid(v){
		this.Momid=v;
	}
	this.setMomid=setMomid;

	this.Momyob=null;


	function getMomyob() {
		return this.Momyob;
	}
	this.getMomyob=getMomyob;


	function setMomyob(v){
		this.Momyob=v;
	}
	this.setMomyob=setMomyob;

	this.Momliv=null;


	function getMomliv() {
		return this.Momliv;
	}
	this.getMomliv=getMomliv;


	function setMomliv(v){
		this.Momliv=v;
	}
	this.setMomliv=setMomliv;

	this.Momyod=null;


	function getMomyod() {
		return this.Momyod;
	}
	this.getMomyod=getMomyod;


	function setMomyod(v){
		this.Momyod=v;
	}
	this.setMomyod=setMomyod;

	this.Momdem=null;


	function getMomdem() {
		return this.Momdem;
	}
	this.getMomdem=getMomdem;


	function setMomdem(v){
		this.Momdem=v;
	}
	this.setMomdem=setMomdem;

	this.Momonset=null;


	function getMomonset() {
		return this.Momonset;
	}
	this.getMomonset=getMomonset;


	function setMomonset(v){
		this.Momonset=v;
	}
	this.setMomonset=setMomonset;

	this.Momauto=null;


	function getMomauto() {
		return this.Momauto;
	}
	this.getMomauto=getMomauto;


	function setMomauto(v){
		this.Momauto=v;
	}
	this.setMomauto=setMomauto;

	this.Dadid=null;


	function getDadid() {
		return this.Dadid;
	}
	this.getDadid=getDadid;


	function setDadid(v){
		this.Dadid=v;
	}
	this.setDadid=setDadid;

	this.Dadyob=null;


	function getDadyob() {
		return this.Dadyob;
	}
	this.getDadyob=getDadyob;


	function setDadyob(v){
		this.Dadyob=v;
	}
	this.setDadyob=setDadyob;

	this.Dadliv=null;


	function getDadliv() {
		return this.Dadliv;
	}
	this.getDadliv=getDadliv;


	function setDadliv(v){
		this.Dadliv=v;
	}
	this.setDadliv=setDadliv;

	this.Dadyod=null;


	function getDadyod() {
		return this.Dadyod;
	}
	this.getDadyod=getDadyod;


	function setDadyod(v){
		this.Dadyod=v;
	}
	this.setDadyod=setDadyod;

	this.Daddem=null;


	function getDaddem() {
		return this.Daddem;
	}
	this.getDaddem=getDaddem;


	function setDaddem(v){
		this.Daddem=v;
	}
	this.setDaddem=setDaddem;

	this.Dadonset=null;


	function getDadonset() {
		return this.Dadonset;
	}
	this.getDadonset=getDadonset;


	function setDadonset(v){
		this.Dadonset=v;
	}
	this.setDadonset=setDadonset;

	this.Dadauto=null;


	function getDadauto() {
		return this.Dadauto;
	}
	this.getDadauto=getDadauto;


	function setDadauto(v){
		this.Dadauto=v;
	}
	this.setDadauto=setDadauto;

	this.Sibchg=null;


	function getSibchg() {
		return this.Sibchg;
	}
	this.getSibchg=getSibchg;


	function setSibchg(v){
		this.Sibchg=v;
	}
	this.setSibchg=setSibchg;

	this.Twin=null;


	function getTwin() {
		return this.Twin;
	}
	this.getTwin=getTwin;


	function setTwin(v){
		this.Twin=v;
	}
	this.setTwin=setTwin;

	this.Twintype=null;


	function getTwintype() {
		return this.Twintype;
	}
	this.getTwintype=getTwintype;


	function setTwintype(v){
		this.Twintype=v;
	}
	this.setTwintype=setTwintype;

	this.Sibs=null;


	function getSibs() {
		return this.Sibs;
	}
	this.getSibs=getSibs;


	function setSibs(v){
		this.Sibs=v;
	}
	this.setSibs=setSibs;

	this.Sib1id=null;


	function getSib1id() {
		return this.Sib1id;
	}
	this.getSib1id=getSib1id;


	function setSib1id(v){
		this.Sib1id=v;
	}
	this.setSib1id=setSib1id;

	this.Sib1yob=null;


	function getSib1yob() {
		return this.Sib1yob;
	}
	this.getSib1yob=getSib1yob;


	function setSib1yob(v){
		this.Sib1yob=v;
	}
	this.setSib1yob=setSib1yob;

	this.Sib1liv=null;


	function getSib1liv() {
		return this.Sib1liv;
	}
	this.getSib1liv=getSib1liv;


	function setSib1liv(v){
		this.Sib1liv=v;
	}
	this.setSib1liv=setSib1liv;

	this.Sib1yod=null;


	function getSib1yod() {
		return this.Sib1yod;
	}
	this.getSib1yod=getSib1yod;


	function setSib1yod(v){
		this.Sib1yod=v;
	}
	this.setSib1yod=setSib1yod;

	this.Sib1dem=null;


	function getSib1dem() {
		return this.Sib1dem;
	}
	this.getSib1dem=getSib1dem;


	function setSib1dem(v){
		this.Sib1dem=v;
	}
	this.setSib1dem=setSib1dem;

	this.Sib1ons=null;


	function getSib1ons() {
		return this.Sib1ons;
	}
	this.getSib1ons=getSib1ons;


	function setSib1ons(v){
		this.Sib1ons=v;
	}
	this.setSib1ons=setSib1ons;

	this.Sib1auto=null;


	function getSib1auto() {
		return this.Sib1auto;
	}
	this.getSib1auto=getSib1auto;


	function setSib1auto(v){
		this.Sib1auto=v;
	}
	this.setSib1auto=setSib1auto;

	this.Sib1sex=null;


	function getSib1sex() {
		return this.Sib1sex;
	}
	this.getSib1sex=getSib1sex;


	function setSib1sex(v){
		this.Sib1sex=v;
	}
	this.setSib1sex=setSib1sex;

	this.Sib2id=null;


	function getSib2id() {
		return this.Sib2id;
	}
	this.getSib2id=getSib2id;


	function setSib2id(v){
		this.Sib2id=v;
	}
	this.setSib2id=setSib2id;

	this.Sib2yob=null;


	function getSib2yob() {
		return this.Sib2yob;
	}
	this.getSib2yob=getSib2yob;


	function setSib2yob(v){
		this.Sib2yob=v;
	}
	this.setSib2yob=setSib2yob;

	this.Sib2liv=null;


	function getSib2liv() {
		return this.Sib2liv;
	}
	this.getSib2liv=getSib2liv;


	function setSib2liv(v){
		this.Sib2liv=v;
	}
	this.setSib2liv=setSib2liv;

	this.Sib2yod=null;


	function getSib2yod() {
		return this.Sib2yod;
	}
	this.getSib2yod=getSib2yod;


	function setSib2yod(v){
		this.Sib2yod=v;
	}
	this.setSib2yod=setSib2yod;

	this.Sib2dem=null;


	function getSib2dem() {
		return this.Sib2dem;
	}
	this.getSib2dem=getSib2dem;


	function setSib2dem(v){
		this.Sib2dem=v;
	}
	this.setSib2dem=setSib2dem;

	this.Sib2ons=null;


	function getSib2ons() {
		return this.Sib2ons;
	}
	this.getSib2ons=getSib2ons;


	function setSib2ons(v){
		this.Sib2ons=v;
	}
	this.setSib2ons=setSib2ons;

	this.Sib2auto=null;


	function getSib2auto() {
		return this.Sib2auto;
	}
	this.getSib2auto=getSib2auto;


	function setSib2auto(v){
		this.Sib2auto=v;
	}
	this.setSib2auto=setSib2auto;

	this.Sib2sex=null;


	function getSib2sex() {
		return this.Sib2sex;
	}
	this.getSib2sex=getSib2sex;


	function setSib2sex(v){
		this.Sib2sex=v;
	}
	this.setSib2sex=setSib2sex;

	this.Sib3id=null;


	function getSib3id() {
		return this.Sib3id;
	}
	this.getSib3id=getSib3id;


	function setSib3id(v){
		this.Sib3id=v;
	}
	this.setSib3id=setSib3id;

	this.Sib3yob=null;


	function getSib3yob() {
		return this.Sib3yob;
	}
	this.getSib3yob=getSib3yob;


	function setSib3yob(v){
		this.Sib3yob=v;
	}
	this.setSib3yob=setSib3yob;

	this.Sib3liv=null;


	function getSib3liv() {
		return this.Sib3liv;
	}
	this.getSib3liv=getSib3liv;


	function setSib3liv(v){
		this.Sib3liv=v;
	}
	this.setSib3liv=setSib3liv;

	this.Sib3yod=null;


	function getSib3yod() {
		return this.Sib3yod;
	}
	this.getSib3yod=getSib3yod;


	function setSib3yod(v){
		this.Sib3yod=v;
	}
	this.setSib3yod=setSib3yod;

	this.Sib3dem=null;


	function getSib3dem() {
		return this.Sib3dem;
	}
	this.getSib3dem=getSib3dem;


	function setSib3dem(v){
		this.Sib3dem=v;
	}
	this.setSib3dem=setSib3dem;

	this.Sib3ons=null;


	function getSib3ons() {
		return this.Sib3ons;
	}
	this.getSib3ons=getSib3ons;


	function setSib3ons(v){
		this.Sib3ons=v;
	}
	this.setSib3ons=setSib3ons;

	this.Sib3auto=null;


	function getSib3auto() {
		return this.Sib3auto;
	}
	this.getSib3auto=getSib3auto;


	function setSib3auto(v){
		this.Sib3auto=v;
	}
	this.setSib3auto=setSib3auto;

	this.Sib3sex=null;


	function getSib3sex() {
		return this.Sib3sex;
	}
	this.getSib3sex=getSib3sex;


	function setSib3sex(v){
		this.Sib3sex=v;
	}
	this.setSib3sex=setSib3sex;

	this.Sib4id=null;


	function getSib4id() {
		return this.Sib4id;
	}
	this.getSib4id=getSib4id;


	function setSib4id(v){
		this.Sib4id=v;
	}
	this.setSib4id=setSib4id;

	this.Sib4yob=null;


	function getSib4yob() {
		return this.Sib4yob;
	}
	this.getSib4yob=getSib4yob;


	function setSib4yob(v){
		this.Sib4yob=v;
	}
	this.setSib4yob=setSib4yob;

	this.Sib4liv=null;


	function getSib4liv() {
		return this.Sib4liv;
	}
	this.getSib4liv=getSib4liv;


	function setSib4liv(v){
		this.Sib4liv=v;
	}
	this.setSib4liv=setSib4liv;

	this.Sib4yod=null;


	function getSib4yod() {
		return this.Sib4yod;
	}
	this.getSib4yod=getSib4yod;


	function setSib4yod(v){
		this.Sib4yod=v;
	}
	this.setSib4yod=setSib4yod;

	this.Sib4dem=null;


	function getSib4dem() {
		return this.Sib4dem;
	}
	this.getSib4dem=getSib4dem;


	function setSib4dem(v){
		this.Sib4dem=v;
	}
	this.setSib4dem=setSib4dem;

	this.Sib4ons=null;


	function getSib4ons() {
		return this.Sib4ons;
	}
	this.getSib4ons=getSib4ons;


	function setSib4ons(v){
		this.Sib4ons=v;
	}
	this.setSib4ons=setSib4ons;

	this.Sib4auto=null;


	function getSib4auto() {
		return this.Sib4auto;
	}
	this.getSib4auto=getSib4auto;


	function setSib4auto(v){
		this.Sib4auto=v;
	}
	this.setSib4auto=setSib4auto;

	this.Sib4sex=null;


	function getSib4sex() {
		return this.Sib4sex;
	}
	this.getSib4sex=getSib4sex;


	function setSib4sex(v){
		this.Sib4sex=v;
	}
	this.setSib4sex=setSib4sex;

	this.Sib5id=null;


	function getSib5id() {
		return this.Sib5id;
	}
	this.getSib5id=getSib5id;


	function setSib5id(v){
		this.Sib5id=v;
	}
	this.setSib5id=setSib5id;

	this.Sib5yob=null;


	function getSib5yob() {
		return this.Sib5yob;
	}
	this.getSib5yob=getSib5yob;


	function setSib5yob(v){
		this.Sib5yob=v;
	}
	this.setSib5yob=setSib5yob;

	this.Sib5liv=null;


	function getSib5liv() {
		return this.Sib5liv;
	}
	this.getSib5liv=getSib5liv;


	function setSib5liv(v){
		this.Sib5liv=v;
	}
	this.setSib5liv=setSib5liv;

	this.Sib5yod=null;


	function getSib5yod() {
		return this.Sib5yod;
	}
	this.getSib5yod=getSib5yod;


	function setSib5yod(v){
		this.Sib5yod=v;
	}
	this.setSib5yod=setSib5yod;

	this.Sib5dem=null;


	function getSib5dem() {
		return this.Sib5dem;
	}
	this.getSib5dem=getSib5dem;


	function setSib5dem(v){
		this.Sib5dem=v;
	}
	this.setSib5dem=setSib5dem;

	this.Sib5ons=null;


	function getSib5ons() {
		return this.Sib5ons;
	}
	this.getSib5ons=getSib5ons;


	function setSib5ons(v){
		this.Sib5ons=v;
	}
	this.setSib5ons=setSib5ons;

	this.Sib5auto=null;


	function getSib5auto() {
		return this.Sib5auto;
	}
	this.getSib5auto=getSib5auto;


	function setSib5auto(v){
		this.Sib5auto=v;
	}
	this.setSib5auto=setSib5auto;

	this.Sib5sex=null;


	function getSib5sex() {
		return this.Sib5sex;
	}
	this.getSib5sex=getSib5sex;


	function setSib5sex(v){
		this.Sib5sex=v;
	}
	this.setSib5sex=setSib5sex;

	this.Sib6id=null;


	function getSib6id() {
		return this.Sib6id;
	}
	this.getSib6id=getSib6id;


	function setSib6id(v){
		this.Sib6id=v;
	}
	this.setSib6id=setSib6id;

	this.Sib6yob=null;


	function getSib6yob() {
		return this.Sib6yob;
	}
	this.getSib6yob=getSib6yob;


	function setSib6yob(v){
		this.Sib6yob=v;
	}
	this.setSib6yob=setSib6yob;

	this.Sib6liv=null;


	function getSib6liv() {
		return this.Sib6liv;
	}
	this.getSib6liv=getSib6liv;


	function setSib6liv(v){
		this.Sib6liv=v;
	}
	this.setSib6liv=setSib6liv;

	this.Sib6yod=null;


	function getSib6yod() {
		return this.Sib6yod;
	}
	this.getSib6yod=getSib6yod;


	function setSib6yod(v){
		this.Sib6yod=v;
	}
	this.setSib6yod=setSib6yod;

	this.Sib6dem=null;


	function getSib6dem() {
		return this.Sib6dem;
	}
	this.getSib6dem=getSib6dem;


	function setSib6dem(v){
		this.Sib6dem=v;
	}
	this.setSib6dem=setSib6dem;

	this.Sib6ons=null;


	function getSib6ons() {
		return this.Sib6ons;
	}
	this.getSib6ons=getSib6ons;


	function setSib6ons(v){
		this.Sib6ons=v;
	}
	this.setSib6ons=setSib6ons;

	this.Sib6auto=null;


	function getSib6auto() {
		return this.Sib6auto;
	}
	this.getSib6auto=getSib6auto;


	function setSib6auto(v){
		this.Sib6auto=v;
	}
	this.setSib6auto=setSib6auto;

	this.Sib6sex=null;


	function getSib6sex() {
		return this.Sib6sex;
	}
	this.getSib6sex=getSib6sex;


	function setSib6sex(v){
		this.Sib6sex=v;
	}
	this.setSib6sex=setSib6sex;

	this.Sib7id=null;


	function getSib7id() {
		return this.Sib7id;
	}
	this.getSib7id=getSib7id;


	function setSib7id(v){
		this.Sib7id=v;
	}
	this.setSib7id=setSib7id;

	this.Sib7yob=null;


	function getSib7yob() {
		return this.Sib7yob;
	}
	this.getSib7yob=getSib7yob;


	function setSib7yob(v){
		this.Sib7yob=v;
	}
	this.setSib7yob=setSib7yob;

	this.Sib7liv=null;


	function getSib7liv() {
		return this.Sib7liv;
	}
	this.getSib7liv=getSib7liv;


	function setSib7liv(v){
		this.Sib7liv=v;
	}
	this.setSib7liv=setSib7liv;

	this.Sib7yod=null;


	function getSib7yod() {
		return this.Sib7yod;
	}
	this.getSib7yod=getSib7yod;


	function setSib7yod(v){
		this.Sib7yod=v;
	}
	this.setSib7yod=setSib7yod;

	this.Sib7dem=null;


	function getSib7dem() {
		return this.Sib7dem;
	}
	this.getSib7dem=getSib7dem;


	function setSib7dem(v){
		this.Sib7dem=v;
	}
	this.setSib7dem=setSib7dem;

	this.Sib7ons=null;


	function getSib7ons() {
		return this.Sib7ons;
	}
	this.getSib7ons=getSib7ons;


	function setSib7ons(v){
		this.Sib7ons=v;
	}
	this.setSib7ons=setSib7ons;

	this.Sib7auto=null;


	function getSib7auto() {
		return this.Sib7auto;
	}
	this.getSib7auto=getSib7auto;


	function setSib7auto(v){
		this.Sib7auto=v;
	}
	this.setSib7auto=setSib7auto;

	this.Sib7sex=null;


	function getSib7sex() {
		return this.Sib7sex;
	}
	this.getSib7sex=getSib7sex;


	function setSib7sex(v){
		this.Sib7sex=v;
	}
	this.setSib7sex=setSib7sex;

	this.Sib8id=null;


	function getSib8id() {
		return this.Sib8id;
	}
	this.getSib8id=getSib8id;


	function setSib8id(v){
		this.Sib8id=v;
	}
	this.setSib8id=setSib8id;

	this.Sib8yob=null;


	function getSib8yob() {
		return this.Sib8yob;
	}
	this.getSib8yob=getSib8yob;


	function setSib8yob(v){
		this.Sib8yob=v;
	}
	this.setSib8yob=setSib8yob;

	this.Sib8liv=null;


	function getSib8liv() {
		return this.Sib8liv;
	}
	this.getSib8liv=getSib8liv;


	function setSib8liv(v){
		this.Sib8liv=v;
	}
	this.setSib8liv=setSib8liv;

	this.Sib8yod=null;


	function getSib8yod() {
		return this.Sib8yod;
	}
	this.getSib8yod=getSib8yod;


	function setSib8yod(v){
		this.Sib8yod=v;
	}
	this.setSib8yod=setSib8yod;

	this.Sib8dem=null;


	function getSib8dem() {
		return this.Sib8dem;
	}
	this.getSib8dem=getSib8dem;


	function setSib8dem(v){
		this.Sib8dem=v;
	}
	this.setSib8dem=setSib8dem;

	this.Sib8ons=null;


	function getSib8ons() {
		return this.Sib8ons;
	}
	this.getSib8ons=getSib8ons;


	function setSib8ons(v){
		this.Sib8ons=v;
	}
	this.setSib8ons=setSib8ons;

	this.Sib8auto=null;


	function getSib8auto() {
		return this.Sib8auto;
	}
	this.getSib8auto=getSib8auto;


	function setSib8auto(v){
		this.Sib8auto=v;
	}
	this.setSib8auto=setSib8auto;

	this.Sib8sex=null;


	function getSib8sex() {
		return this.Sib8sex;
	}
	this.getSib8sex=getSib8sex;


	function setSib8sex(v){
		this.Sib8sex=v;
	}
	this.setSib8sex=setSib8sex;

	this.Sib9id=null;


	function getSib9id() {
		return this.Sib9id;
	}
	this.getSib9id=getSib9id;


	function setSib9id(v){
		this.Sib9id=v;
	}
	this.setSib9id=setSib9id;

	this.Sib9yob=null;


	function getSib9yob() {
		return this.Sib9yob;
	}
	this.getSib9yob=getSib9yob;


	function setSib9yob(v){
		this.Sib9yob=v;
	}
	this.setSib9yob=setSib9yob;

	this.Sib9liv=null;


	function getSib9liv() {
		return this.Sib9liv;
	}
	this.getSib9liv=getSib9liv;


	function setSib9liv(v){
		this.Sib9liv=v;
	}
	this.setSib9liv=setSib9liv;

	this.Sib9yod=null;


	function getSib9yod() {
		return this.Sib9yod;
	}
	this.getSib9yod=getSib9yod;


	function setSib9yod(v){
		this.Sib9yod=v;
	}
	this.setSib9yod=setSib9yod;

	this.Sib9dem=null;


	function getSib9dem() {
		return this.Sib9dem;
	}
	this.getSib9dem=getSib9dem;


	function setSib9dem(v){
		this.Sib9dem=v;
	}
	this.setSib9dem=setSib9dem;

	this.Sib9ons=null;


	function getSib9ons() {
		return this.Sib9ons;
	}
	this.getSib9ons=getSib9ons;


	function setSib9ons(v){
		this.Sib9ons=v;
	}
	this.setSib9ons=setSib9ons;

	this.Sib9auto=null;


	function getSib9auto() {
		return this.Sib9auto;
	}
	this.getSib9auto=getSib9auto;


	function setSib9auto(v){
		this.Sib9auto=v;
	}
	this.setSib9auto=setSib9auto;

	this.Sib9sex=null;


	function getSib9sex() {
		return this.Sib9sex;
	}
	this.getSib9sex=getSib9sex;


	function setSib9sex(v){
		this.Sib9sex=v;
	}
	this.setSib9sex=setSib9sex;

	this.Sib10id=null;


	function getSib10id() {
		return this.Sib10id;
	}
	this.getSib10id=getSib10id;


	function setSib10id(v){
		this.Sib10id=v;
	}
	this.setSib10id=setSib10id;

	this.Sib10yob=null;


	function getSib10yob() {
		return this.Sib10yob;
	}
	this.getSib10yob=getSib10yob;


	function setSib10yob(v){
		this.Sib10yob=v;
	}
	this.setSib10yob=setSib10yob;

	this.Sib10liv=null;


	function getSib10liv() {
		return this.Sib10liv;
	}
	this.getSib10liv=getSib10liv;


	function setSib10liv(v){
		this.Sib10liv=v;
	}
	this.setSib10liv=setSib10liv;

	this.Sib10yod=null;


	function getSib10yod() {
		return this.Sib10yod;
	}
	this.getSib10yod=getSib10yod;


	function setSib10yod(v){
		this.Sib10yod=v;
	}
	this.setSib10yod=setSib10yod;

	this.Sib10dem=null;


	function getSib10dem() {
		return this.Sib10dem;
	}
	this.getSib10dem=getSib10dem;


	function setSib10dem(v){
		this.Sib10dem=v;
	}
	this.setSib10dem=setSib10dem;

	this.Sib10ons=null;


	function getSib10ons() {
		return this.Sib10ons;
	}
	this.getSib10ons=getSib10ons;


	function setSib10ons(v){
		this.Sib10ons=v;
	}
	this.setSib10ons=setSib10ons;

	this.Sib10auto=null;


	function getSib10auto() {
		return this.Sib10auto;
	}
	this.getSib10auto=getSib10auto;


	function setSib10auto(v){
		this.Sib10auto=v;
	}
	this.setSib10auto=setSib10auto;

	this.Sib10sex=null;


	function getSib10sex() {
		return this.Sib10sex;
	}
	this.getSib10sex=getSib10sex;


	function setSib10sex(v){
		this.Sib10sex=v;
	}
	this.setSib10sex=setSib10sex;

	this.Sib11id=null;


	function getSib11id() {
		return this.Sib11id;
	}
	this.getSib11id=getSib11id;


	function setSib11id(v){
		this.Sib11id=v;
	}
	this.setSib11id=setSib11id;

	this.Sib11yob=null;


	function getSib11yob() {
		return this.Sib11yob;
	}
	this.getSib11yob=getSib11yob;


	function setSib11yob(v){
		this.Sib11yob=v;
	}
	this.setSib11yob=setSib11yob;

	this.Sib11liv=null;


	function getSib11liv() {
		return this.Sib11liv;
	}
	this.getSib11liv=getSib11liv;


	function setSib11liv(v){
		this.Sib11liv=v;
	}
	this.setSib11liv=setSib11liv;

	this.Sib11yod=null;


	function getSib11yod() {
		return this.Sib11yod;
	}
	this.getSib11yod=getSib11yod;


	function setSib11yod(v){
		this.Sib11yod=v;
	}
	this.setSib11yod=setSib11yod;

	this.Sib11dem=null;


	function getSib11dem() {
		return this.Sib11dem;
	}
	this.getSib11dem=getSib11dem;


	function setSib11dem(v){
		this.Sib11dem=v;
	}
	this.setSib11dem=setSib11dem;

	this.Sib11ons=null;


	function getSib11ons() {
		return this.Sib11ons;
	}
	this.getSib11ons=getSib11ons;


	function setSib11ons(v){
		this.Sib11ons=v;
	}
	this.setSib11ons=setSib11ons;

	this.Sib11auto=null;


	function getSib11auto() {
		return this.Sib11auto;
	}
	this.getSib11auto=getSib11auto;


	function setSib11auto(v){
		this.Sib11auto=v;
	}
	this.setSib11auto=setSib11auto;

	this.Sib11sex=null;


	function getSib11sex() {
		return this.Sib11sex;
	}
	this.getSib11sex=getSib11sex;


	function setSib11sex(v){
		this.Sib11sex=v;
	}
	this.setSib11sex=setSib11sex;

	this.Sib12id=null;


	function getSib12id() {
		return this.Sib12id;
	}
	this.getSib12id=getSib12id;


	function setSib12id(v){
		this.Sib12id=v;
	}
	this.setSib12id=setSib12id;

	this.Sib12yob=null;


	function getSib12yob() {
		return this.Sib12yob;
	}
	this.getSib12yob=getSib12yob;


	function setSib12yob(v){
		this.Sib12yob=v;
	}
	this.setSib12yob=setSib12yob;

	this.Sib12liv=null;


	function getSib12liv() {
		return this.Sib12liv;
	}
	this.getSib12liv=getSib12liv;


	function setSib12liv(v){
		this.Sib12liv=v;
	}
	this.setSib12liv=setSib12liv;

	this.Sib12yod=null;


	function getSib12yod() {
		return this.Sib12yod;
	}
	this.getSib12yod=getSib12yod;


	function setSib12yod(v){
		this.Sib12yod=v;
	}
	this.setSib12yod=setSib12yod;

	this.Sib12dem=null;


	function getSib12dem() {
		return this.Sib12dem;
	}
	this.getSib12dem=getSib12dem;


	function setSib12dem(v){
		this.Sib12dem=v;
	}
	this.setSib12dem=setSib12dem;

	this.Sib12ons=null;


	function getSib12ons() {
		return this.Sib12ons;
	}
	this.getSib12ons=getSib12ons;


	function setSib12ons(v){
		this.Sib12ons=v;
	}
	this.setSib12ons=setSib12ons;

	this.Sib12auto=null;


	function getSib12auto() {
		return this.Sib12auto;
	}
	this.getSib12auto=getSib12auto;


	function setSib12auto(v){
		this.Sib12auto=v;
	}
	this.setSib12auto=setSib12auto;

	this.Sib12sex=null;


	function getSib12sex() {
		return this.Sib12sex;
	}
	this.getSib12sex=getSib12sex;


	function setSib12sex(v){
		this.Sib12sex=v;
	}
	this.setSib12sex=setSib12sex;

	this.Sib13id=null;


	function getSib13id() {
		return this.Sib13id;
	}
	this.getSib13id=getSib13id;


	function setSib13id(v){
		this.Sib13id=v;
	}
	this.setSib13id=setSib13id;

	this.Sib13yob=null;


	function getSib13yob() {
		return this.Sib13yob;
	}
	this.getSib13yob=getSib13yob;


	function setSib13yob(v){
		this.Sib13yob=v;
	}
	this.setSib13yob=setSib13yob;

	this.Sib13liv=null;


	function getSib13liv() {
		return this.Sib13liv;
	}
	this.getSib13liv=getSib13liv;


	function setSib13liv(v){
		this.Sib13liv=v;
	}
	this.setSib13liv=setSib13liv;

	this.Sib13yod=null;


	function getSib13yod() {
		return this.Sib13yod;
	}
	this.getSib13yod=getSib13yod;


	function setSib13yod(v){
		this.Sib13yod=v;
	}
	this.setSib13yod=setSib13yod;

	this.Sib13dem=null;


	function getSib13dem() {
		return this.Sib13dem;
	}
	this.getSib13dem=getSib13dem;


	function setSib13dem(v){
		this.Sib13dem=v;
	}
	this.setSib13dem=setSib13dem;

	this.Sib13ons=null;


	function getSib13ons() {
		return this.Sib13ons;
	}
	this.getSib13ons=getSib13ons;


	function setSib13ons(v){
		this.Sib13ons=v;
	}
	this.setSib13ons=setSib13ons;

	this.Sib13auto=null;


	function getSib13auto() {
		return this.Sib13auto;
	}
	this.getSib13auto=getSib13auto;


	function setSib13auto(v){
		this.Sib13auto=v;
	}
	this.setSib13auto=setSib13auto;

	this.Sib13sex=null;


	function getSib13sex() {
		return this.Sib13sex;
	}
	this.getSib13sex=getSib13sex;


	function setSib13sex(v){
		this.Sib13sex=v;
	}
	this.setSib13sex=setSib13sex;

	this.Sib14id=null;


	function getSib14id() {
		return this.Sib14id;
	}
	this.getSib14id=getSib14id;


	function setSib14id(v){
		this.Sib14id=v;
	}
	this.setSib14id=setSib14id;

	this.Sib14yob=null;


	function getSib14yob() {
		return this.Sib14yob;
	}
	this.getSib14yob=getSib14yob;


	function setSib14yob(v){
		this.Sib14yob=v;
	}
	this.setSib14yob=setSib14yob;

	this.Sib14liv=null;


	function getSib14liv() {
		return this.Sib14liv;
	}
	this.getSib14liv=getSib14liv;


	function setSib14liv(v){
		this.Sib14liv=v;
	}
	this.setSib14liv=setSib14liv;

	this.Sib14yod=null;


	function getSib14yod() {
		return this.Sib14yod;
	}
	this.getSib14yod=getSib14yod;


	function setSib14yod(v){
		this.Sib14yod=v;
	}
	this.setSib14yod=setSib14yod;

	this.Sib14dem=null;


	function getSib14dem() {
		return this.Sib14dem;
	}
	this.getSib14dem=getSib14dem;


	function setSib14dem(v){
		this.Sib14dem=v;
	}
	this.setSib14dem=setSib14dem;

	this.Sib14ons=null;


	function getSib14ons() {
		return this.Sib14ons;
	}
	this.getSib14ons=getSib14ons;


	function setSib14ons(v){
		this.Sib14ons=v;
	}
	this.setSib14ons=setSib14ons;

	this.Sib14auto=null;


	function getSib14auto() {
		return this.Sib14auto;
	}
	this.getSib14auto=getSib14auto;


	function setSib14auto(v){
		this.Sib14auto=v;
	}
	this.setSib14auto=setSib14auto;

	this.Sib14sex=null;


	function getSib14sex() {
		return this.Sib14sex;
	}
	this.getSib14sex=getSib14sex;


	function setSib14sex(v){
		this.Sib14sex=v;
	}
	this.setSib14sex=setSib14sex;

	this.Sib15id=null;


	function getSib15id() {
		return this.Sib15id;
	}
	this.getSib15id=getSib15id;


	function setSib15id(v){
		this.Sib15id=v;
	}
	this.setSib15id=setSib15id;

	this.Sib15yob=null;


	function getSib15yob() {
		return this.Sib15yob;
	}
	this.getSib15yob=getSib15yob;


	function setSib15yob(v){
		this.Sib15yob=v;
	}
	this.setSib15yob=setSib15yob;

	this.Sib15liv=null;


	function getSib15liv() {
		return this.Sib15liv;
	}
	this.getSib15liv=getSib15liv;


	function setSib15liv(v){
		this.Sib15liv=v;
	}
	this.setSib15liv=setSib15liv;

	this.Sib15yod=null;


	function getSib15yod() {
		return this.Sib15yod;
	}
	this.getSib15yod=getSib15yod;


	function setSib15yod(v){
		this.Sib15yod=v;
	}
	this.setSib15yod=setSib15yod;

	this.Sib15dem=null;


	function getSib15dem() {
		return this.Sib15dem;
	}
	this.getSib15dem=getSib15dem;


	function setSib15dem(v){
		this.Sib15dem=v;
	}
	this.setSib15dem=setSib15dem;

	this.Sib15ons=null;


	function getSib15ons() {
		return this.Sib15ons;
	}
	this.getSib15ons=getSib15ons;


	function setSib15ons(v){
		this.Sib15ons=v;
	}
	this.setSib15ons=setSib15ons;

	this.Sib155auto=null;


	function getSib155auto() {
		return this.Sib155auto;
	}
	this.getSib155auto=getSib155auto;


	function setSib155auto(v){
		this.Sib155auto=v;
	}
	this.setSib155auto=setSib155auto;

	this.Sib155sex=null;


	function getSib155sex() {
		return this.Sib155sex;
	}
	this.getSib155sex=getSib155sex;


	function setSib155sex(v){
		this.Sib155sex=v;
	}
	this.setSib155sex=setSib155sex;

	this.Sib16id=null;


	function getSib16id() {
		return this.Sib16id;
	}
	this.getSib16id=getSib16id;


	function setSib16id(v){
		this.Sib16id=v;
	}
	this.setSib16id=setSib16id;

	this.Sib16yob=null;


	function getSib16yob() {
		return this.Sib16yob;
	}
	this.getSib16yob=getSib16yob;


	function setSib16yob(v){
		this.Sib16yob=v;
	}
	this.setSib16yob=setSib16yob;

	this.Sib16liv=null;


	function getSib16liv() {
		return this.Sib16liv;
	}
	this.getSib16liv=getSib16liv;


	function setSib16liv(v){
		this.Sib16liv=v;
	}
	this.setSib16liv=setSib16liv;

	this.Sib16yod=null;


	function getSib16yod() {
		return this.Sib16yod;
	}
	this.getSib16yod=getSib16yod;


	function setSib16yod(v){
		this.Sib16yod=v;
	}
	this.setSib16yod=setSib16yod;

	this.Sib16dem=null;


	function getSib16dem() {
		return this.Sib16dem;
	}
	this.getSib16dem=getSib16dem;


	function setSib16dem(v){
		this.Sib16dem=v;
	}
	this.setSib16dem=setSib16dem;

	this.Sib16ons=null;


	function getSib16ons() {
		return this.Sib16ons;
	}
	this.getSib16ons=getSib16ons;


	function setSib16ons(v){
		this.Sib16ons=v;
	}
	this.setSib16ons=setSib16ons;

	this.Sib16auto=null;


	function getSib16auto() {
		return this.Sib16auto;
	}
	this.getSib16auto=getSib16auto;


	function setSib16auto(v){
		this.Sib16auto=v;
	}
	this.setSib16auto=setSib16auto;

	this.Sib16sex=null;


	function getSib16sex() {
		return this.Sib16sex;
	}
	this.getSib16sex=getSib16sex;


	function setSib16sex(v){
		this.Sib16sex=v;
	}
	this.setSib16sex=setSib16sex;

	this.Sib17id=null;


	function getSib17id() {
		return this.Sib17id;
	}
	this.getSib17id=getSib17id;


	function setSib17id(v){
		this.Sib17id=v;
	}
	this.setSib17id=setSib17id;

	this.Sib17yob=null;


	function getSib17yob() {
		return this.Sib17yob;
	}
	this.getSib17yob=getSib17yob;


	function setSib17yob(v){
		this.Sib17yob=v;
	}
	this.setSib17yob=setSib17yob;

	this.Sib17liv=null;


	function getSib17liv() {
		return this.Sib17liv;
	}
	this.getSib17liv=getSib17liv;


	function setSib17liv(v){
		this.Sib17liv=v;
	}
	this.setSib17liv=setSib17liv;

	this.Sib17yod=null;


	function getSib17yod() {
		return this.Sib17yod;
	}
	this.getSib17yod=getSib17yod;


	function setSib17yod(v){
		this.Sib17yod=v;
	}
	this.setSib17yod=setSib17yod;

	this.Sib17dem=null;


	function getSib17dem() {
		return this.Sib17dem;
	}
	this.getSib17dem=getSib17dem;


	function setSib17dem(v){
		this.Sib17dem=v;
	}
	this.setSib17dem=setSib17dem;

	this.Sib17ons=null;


	function getSib17ons() {
		return this.Sib17ons;
	}
	this.getSib17ons=getSib17ons;


	function setSib17ons(v){
		this.Sib17ons=v;
	}
	this.setSib17ons=setSib17ons;

	this.Sib17auto=null;


	function getSib17auto() {
		return this.Sib17auto;
	}
	this.getSib17auto=getSib17auto;


	function setSib17auto(v){
		this.Sib17auto=v;
	}
	this.setSib17auto=setSib17auto;

	this.Sib17sex=null;


	function getSib17sex() {
		return this.Sib17sex;
	}
	this.getSib17sex=getSib17sex;


	function setSib17sex(v){
		this.Sib17sex=v;
	}
	this.setSib17sex=setSib17sex;

	this.Sib18id=null;


	function getSib18id() {
		return this.Sib18id;
	}
	this.getSib18id=getSib18id;


	function setSib18id(v){
		this.Sib18id=v;
	}
	this.setSib18id=setSib18id;

	this.Sib18yob=null;


	function getSib18yob() {
		return this.Sib18yob;
	}
	this.getSib18yob=getSib18yob;


	function setSib18yob(v){
		this.Sib18yob=v;
	}
	this.setSib18yob=setSib18yob;

	this.Sib18liv=null;


	function getSib18liv() {
		return this.Sib18liv;
	}
	this.getSib18liv=getSib18liv;


	function setSib18liv(v){
		this.Sib18liv=v;
	}
	this.setSib18liv=setSib18liv;

	this.Sib18yod=null;


	function getSib18yod() {
		return this.Sib18yod;
	}
	this.getSib18yod=getSib18yod;


	function setSib18yod(v){
		this.Sib18yod=v;
	}
	this.setSib18yod=setSib18yod;

	this.Sib18dem=null;


	function getSib18dem() {
		return this.Sib18dem;
	}
	this.getSib18dem=getSib18dem;


	function setSib18dem(v){
		this.Sib18dem=v;
	}
	this.setSib18dem=setSib18dem;

	this.Sib18ons=null;


	function getSib18ons() {
		return this.Sib18ons;
	}
	this.getSib18ons=getSib18ons;


	function setSib18ons(v){
		this.Sib18ons=v;
	}
	this.setSib18ons=setSib18ons;

	this.Sib18auto=null;


	function getSib18auto() {
		return this.Sib18auto;
	}
	this.getSib18auto=getSib18auto;


	function setSib18auto(v){
		this.Sib18auto=v;
	}
	this.setSib18auto=setSib18auto;

	this.Sib18sex=null;


	function getSib18sex() {
		return this.Sib18sex;
	}
	this.getSib18sex=getSib18sex;


	function setSib18sex(v){
		this.Sib18sex=v;
	}
	this.setSib18sex=setSib18sex;

	this.Sib19id=null;


	function getSib19id() {
		return this.Sib19id;
	}
	this.getSib19id=getSib19id;


	function setSib19id(v){
		this.Sib19id=v;
	}
	this.setSib19id=setSib19id;

	this.Sib19yob=null;


	function getSib19yob() {
		return this.Sib19yob;
	}
	this.getSib19yob=getSib19yob;


	function setSib19yob(v){
		this.Sib19yob=v;
	}
	this.setSib19yob=setSib19yob;

	this.Sib19liv=null;


	function getSib19liv() {
		return this.Sib19liv;
	}
	this.getSib19liv=getSib19liv;


	function setSib19liv(v){
		this.Sib19liv=v;
	}
	this.setSib19liv=setSib19liv;

	this.Sib19yod=null;


	function getSib19yod() {
		return this.Sib19yod;
	}
	this.getSib19yod=getSib19yod;


	function setSib19yod(v){
		this.Sib19yod=v;
	}
	this.setSib19yod=setSib19yod;

	this.Sib19dem=null;


	function getSib19dem() {
		return this.Sib19dem;
	}
	this.getSib19dem=getSib19dem;


	function setSib19dem(v){
		this.Sib19dem=v;
	}
	this.setSib19dem=setSib19dem;

	this.Sib19ons=null;


	function getSib19ons() {
		return this.Sib19ons;
	}
	this.getSib19ons=getSib19ons;


	function setSib19ons(v){
		this.Sib19ons=v;
	}
	this.setSib19ons=setSib19ons;

	this.Sib19auto=null;


	function getSib19auto() {
		return this.Sib19auto;
	}
	this.getSib19auto=getSib19auto;


	function setSib19auto(v){
		this.Sib19auto=v;
	}
	this.setSib19auto=setSib19auto;

	this.Sib19sex=null;


	function getSib19sex() {
		return this.Sib19sex;
	}
	this.getSib19sex=getSib19sex;


	function setSib19sex(v){
		this.Sib19sex=v;
	}
	this.setSib19sex=setSib19sex;

	this.Sib20id=null;


	function getSib20id() {
		return this.Sib20id;
	}
	this.getSib20id=getSib20id;


	function setSib20id(v){
		this.Sib20id=v;
	}
	this.setSib20id=setSib20id;

	this.Sib20yob=null;


	function getSib20yob() {
		return this.Sib20yob;
	}
	this.getSib20yob=getSib20yob;


	function setSib20yob(v){
		this.Sib20yob=v;
	}
	this.setSib20yob=setSib20yob;

	this.Sib20liv=null;


	function getSib20liv() {
		return this.Sib20liv;
	}
	this.getSib20liv=getSib20liv;


	function setSib20liv(v){
		this.Sib20liv=v;
	}
	this.setSib20liv=setSib20liv;

	this.Sib20yod=null;


	function getSib20yod() {
		return this.Sib20yod;
	}
	this.getSib20yod=getSib20yod;


	function setSib20yod(v){
		this.Sib20yod=v;
	}
	this.setSib20yod=setSib20yod;

	this.Sib20dem=null;


	function getSib20dem() {
		return this.Sib20dem;
	}
	this.getSib20dem=getSib20dem;


	function setSib20dem(v){
		this.Sib20dem=v;
	}
	this.setSib20dem=setSib20dem;

	this.Sib20ons=null;


	function getSib20ons() {
		return this.Sib20ons;
	}
	this.getSib20ons=getSib20ons;


	function setSib20ons(v){
		this.Sib20ons=v;
	}
	this.setSib20ons=setSib20ons;

	this.Sib20auto=null;


	function getSib20auto() {
		return this.Sib20auto;
	}
	this.getSib20auto=getSib20auto;


	function setSib20auto(v){
		this.Sib20auto=v;
	}
	this.setSib20auto=setSib20auto;

	this.Sib20sex=null;


	function getSib20sex() {
		return this.Sib20sex;
	}
	this.getSib20sex=getSib20sex;


	function setSib20sex(v){
		this.Sib20sex=v;
	}
	this.setSib20sex=setSib20sex;

	this.Kidchg=null;


	function getKidchg() {
		return this.Kidchg;
	}
	this.getKidchg=getKidchg;


	function setKidchg(v){
		this.Kidchg=v;
	}
	this.setKidchg=setKidchg;

	this.Kids=null;


	function getKids() {
		return this.Kids;
	}
	this.getKids=getKids;


	function setKids(v){
		this.Kids=v;
	}
	this.setKids=setKids;

	this.Kid1id=null;


	function getKid1id() {
		return this.Kid1id;
	}
	this.getKid1id=getKid1id;


	function setKid1id(v){
		this.Kid1id=v;
	}
	this.setKid1id=setKid1id;

	this.Kid1yob=null;


	function getKid1yob() {
		return this.Kid1yob;
	}
	this.getKid1yob=getKid1yob;


	function setKid1yob(v){
		this.Kid1yob=v;
	}
	this.setKid1yob=setKid1yob;

	this.Kid1liv=null;


	function getKid1liv() {
		return this.Kid1liv;
	}
	this.getKid1liv=getKid1liv;


	function setKid1liv(v){
		this.Kid1liv=v;
	}
	this.setKid1liv=setKid1liv;

	this.Kid1yod=null;


	function getKid1yod() {
		return this.Kid1yod;
	}
	this.getKid1yod=getKid1yod;


	function setKid1yod(v){
		this.Kid1yod=v;
	}
	this.setKid1yod=setKid1yod;

	this.Kid1dem=null;


	function getKid1dem() {
		return this.Kid1dem;
	}
	this.getKid1dem=getKid1dem;


	function setKid1dem(v){
		this.Kid1dem=v;
	}
	this.setKid1dem=setKid1dem;

	this.Kid1ons=null;


	function getKid1ons() {
		return this.Kid1ons;
	}
	this.getKid1ons=getKid1ons;


	function setKid1ons(v){
		this.Kid1ons=v;
	}
	this.setKid1ons=setKid1ons;

	this.Kid1auto=null;


	function getKid1auto() {
		return this.Kid1auto;
	}
	this.getKid1auto=getKid1auto;


	function setKid1auto(v){
		this.Kid1auto=v;
	}
	this.setKid1auto=setKid1auto;

	this.Kid1sex=null;


	function getKid1sex() {
		return this.Kid1sex;
	}
	this.getKid1sex=getKid1sex;


	function setKid1sex(v){
		this.Kid1sex=v;
	}
	this.setKid1sex=setKid1sex;

	this.Kid2id=null;


	function getKid2id() {
		return this.Kid2id;
	}
	this.getKid2id=getKid2id;


	function setKid2id(v){
		this.Kid2id=v;
	}
	this.setKid2id=setKid2id;

	this.Kid2yob=null;


	function getKid2yob() {
		return this.Kid2yob;
	}
	this.getKid2yob=getKid2yob;


	function setKid2yob(v){
		this.Kid2yob=v;
	}
	this.setKid2yob=setKid2yob;

	this.Kid2liv=null;


	function getKid2liv() {
		return this.Kid2liv;
	}
	this.getKid2liv=getKid2liv;


	function setKid2liv(v){
		this.Kid2liv=v;
	}
	this.setKid2liv=setKid2liv;

	this.Kid2yod=null;


	function getKid2yod() {
		return this.Kid2yod;
	}
	this.getKid2yod=getKid2yod;


	function setKid2yod(v){
		this.Kid2yod=v;
	}
	this.setKid2yod=setKid2yod;

	this.Kid2dem=null;


	function getKid2dem() {
		return this.Kid2dem;
	}
	this.getKid2dem=getKid2dem;


	function setKid2dem(v){
		this.Kid2dem=v;
	}
	this.setKid2dem=setKid2dem;

	this.Kid2ons=null;


	function getKid2ons() {
		return this.Kid2ons;
	}
	this.getKid2ons=getKid2ons;


	function setKid2ons(v){
		this.Kid2ons=v;
	}
	this.setKid2ons=setKid2ons;

	this.Kid2auto=null;


	function getKid2auto() {
		return this.Kid2auto;
	}
	this.getKid2auto=getKid2auto;


	function setKid2auto(v){
		this.Kid2auto=v;
	}
	this.setKid2auto=setKid2auto;

	this.Kid2sex=null;


	function getKid2sex() {
		return this.Kid2sex;
	}
	this.getKid2sex=getKid2sex;


	function setKid2sex(v){
		this.Kid2sex=v;
	}
	this.setKid2sex=setKid2sex;

	this.Kid3id=null;


	function getKid3id() {
		return this.Kid3id;
	}
	this.getKid3id=getKid3id;


	function setKid3id(v){
		this.Kid3id=v;
	}
	this.setKid3id=setKid3id;

	this.Kid3yob=null;


	function getKid3yob() {
		return this.Kid3yob;
	}
	this.getKid3yob=getKid3yob;


	function setKid3yob(v){
		this.Kid3yob=v;
	}
	this.setKid3yob=setKid3yob;

	this.Kid3liv=null;


	function getKid3liv() {
		return this.Kid3liv;
	}
	this.getKid3liv=getKid3liv;


	function setKid3liv(v){
		this.Kid3liv=v;
	}
	this.setKid3liv=setKid3liv;

	this.Kid3yod=null;


	function getKid3yod() {
		return this.Kid3yod;
	}
	this.getKid3yod=getKid3yod;


	function setKid3yod(v){
		this.Kid3yod=v;
	}
	this.setKid3yod=setKid3yod;

	this.Kid3dem=null;


	function getKid3dem() {
		return this.Kid3dem;
	}
	this.getKid3dem=getKid3dem;


	function setKid3dem(v){
		this.Kid3dem=v;
	}
	this.setKid3dem=setKid3dem;

	this.Kid3ons=null;


	function getKid3ons() {
		return this.Kid3ons;
	}
	this.getKid3ons=getKid3ons;


	function setKid3ons(v){
		this.Kid3ons=v;
	}
	this.setKid3ons=setKid3ons;

	this.Kid3auto=null;


	function getKid3auto() {
		return this.Kid3auto;
	}
	this.getKid3auto=getKid3auto;


	function setKid3auto(v){
		this.Kid3auto=v;
	}
	this.setKid3auto=setKid3auto;

	this.Kid3sex=null;


	function getKid3sex() {
		return this.Kid3sex;
	}
	this.getKid3sex=getKid3sex;


	function setKid3sex(v){
		this.Kid3sex=v;
	}
	this.setKid3sex=setKid3sex;

	this.Kid4id=null;


	function getKid4id() {
		return this.Kid4id;
	}
	this.getKid4id=getKid4id;


	function setKid4id(v){
		this.Kid4id=v;
	}
	this.setKid4id=setKid4id;

	this.Kid4yob=null;


	function getKid4yob() {
		return this.Kid4yob;
	}
	this.getKid4yob=getKid4yob;


	function setKid4yob(v){
		this.Kid4yob=v;
	}
	this.setKid4yob=setKid4yob;

	this.Kid4liv=null;


	function getKid4liv() {
		return this.Kid4liv;
	}
	this.getKid4liv=getKid4liv;


	function setKid4liv(v){
		this.Kid4liv=v;
	}
	this.setKid4liv=setKid4liv;

	this.Kid4yod=null;


	function getKid4yod() {
		return this.Kid4yod;
	}
	this.getKid4yod=getKid4yod;


	function setKid4yod(v){
		this.Kid4yod=v;
	}
	this.setKid4yod=setKid4yod;

	this.Kid4dem=null;


	function getKid4dem() {
		return this.Kid4dem;
	}
	this.getKid4dem=getKid4dem;


	function setKid4dem(v){
		this.Kid4dem=v;
	}
	this.setKid4dem=setKid4dem;

	this.Kid4ons=null;


	function getKid4ons() {
		return this.Kid4ons;
	}
	this.getKid4ons=getKid4ons;


	function setKid4ons(v){
		this.Kid4ons=v;
	}
	this.setKid4ons=setKid4ons;

	this.Kid4auto=null;


	function getKid4auto() {
		return this.Kid4auto;
	}
	this.getKid4auto=getKid4auto;


	function setKid4auto(v){
		this.Kid4auto=v;
	}
	this.setKid4auto=setKid4auto;

	this.Kid4sex=null;


	function getKid4sex() {
		return this.Kid4sex;
	}
	this.getKid4sex=getKid4sex;


	function setKid4sex(v){
		this.Kid4sex=v;
	}
	this.setKid4sex=setKid4sex;

	this.Kid5id=null;


	function getKid5id() {
		return this.Kid5id;
	}
	this.getKid5id=getKid5id;


	function setKid5id(v){
		this.Kid5id=v;
	}
	this.setKid5id=setKid5id;

	this.Kid5yob=null;


	function getKid5yob() {
		return this.Kid5yob;
	}
	this.getKid5yob=getKid5yob;


	function setKid5yob(v){
		this.Kid5yob=v;
	}
	this.setKid5yob=setKid5yob;

	this.Kid5liv=null;


	function getKid5liv() {
		return this.Kid5liv;
	}
	this.getKid5liv=getKid5liv;


	function setKid5liv(v){
		this.Kid5liv=v;
	}
	this.setKid5liv=setKid5liv;

	this.Kid5yod=null;


	function getKid5yod() {
		return this.Kid5yod;
	}
	this.getKid5yod=getKid5yod;


	function setKid5yod(v){
		this.Kid5yod=v;
	}
	this.setKid5yod=setKid5yod;

	this.Kid5dem=null;


	function getKid5dem() {
		return this.Kid5dem;
	}
	this.getKid5dem=getKid5dem;


	function setKid5dem(v){
		this.Kid5dem=v;
	}
	this.setKid5dem=setKid5dem;

	this.Kid5ons=null;


	function getKid5ons() {
		return this.Kid5ons;
	}
	this.getKid5ons=getKid5ons;


	function setKid5ons(v){
		this.Kid5ons=v;
	}
	this.setKid5ons=setKid5ons;

	this.Kid5auto=null;


	function getKid5auto() {
		return this.Kid5auto;
	}
	this.getKid5auto=getKid5auto;


	function setKid5auto(v){
		this.Kid5auto=v;
	}
	this.setKid5auto=setKid5auto;

	this.Kid5sex=null;


	function getKid5sex() {
		return this.Kid5sex;
	}
	this.getKid5sex=getKid5sex;


	function setKid5sex(v){
		this.Kid5sex=v;
	}
	this.setKid5sex=setKid5sex;

	this.Kid6id=null;


	function getKid6id() {
		return this.Kid6id;
	}
	this.getKid6id=getKid6id;


	function setKid6id(v){
		this.Kid6id=v;
	}
	this.setKid6id=setKid6id;

	this.Kid6yob=null;


	function getKid6yob() {
		return this.Kid6yob;
	}
	this.getKid6yob=getKid6yob;


	function setKid6yob(v){
		this.Kid6yob=v;
	}
	this.setKid6yob=setKid6yob;

	this.Kid6liv=null;


	function getKid6liv() {
		return this.Kid6liv;
	}
	this.getKid6liv=getKid6liv;


	function setKid6liv(v){
		this.Kid6liv=v;
	}
	this.setKid6liv=setKid6liv;

	this.Kid6yod=null;


	function getKid6yod() {
		return this.Kid6yod;
	}
	this.getKid6yod=getKid6yod;


	function setKid6yod(v){
		this.Kid6yod=v;
	}
	this.setKid6yod=setKid6yod;

	this.Kid6dem=null;


	function getKid6dem() {
		return this.Kid6dem;
	}
	this.getKid6dem=getKid6dem;


	function setKid6dem(v){
		this.Kid6dem=v;
	}
	this.setKid6dem=setKid6dem;

	this.Kid6ons=null;


	function getKid6ons() {
		return this.Kid6ons;
	}
	this.getKid6ons=getKid6ons;


	function setKid6ons(v){
		this.Kid6ons=v;
	}
	this.setKid6ons=setKid6ons;

	this.Kid6auto=null;


	function getKid6auto() {
		return this.Kid6auto;
	}
	this.getKid6auto=getKid6auto;


	function setKid6auto(v){
		this.Kid6auto=v;
	}
	this.setKid6auto=setKid6auto;

	this.Kid6sex=null;


	function getKid6sex() {
		return this.Kid6sex;
	}
	this.getKid6sex=getKid6sex;


	function setKid6sex(v){
		this.Kid6sex=v;
	}
	this.setKid6sex=setKid6sex;

	this.Kid7id=null;


	function getKid7id() {
		return this.Kid7id;
	}
	this.getKid7id=getKid7id;


	function setKid7id(v){
		this.Kid7id=v;
	}
	this.setKid7id=setKid7id;

	this.Kid7yob=null;


	function getKid7yob() {
		return this.Kid7yob;
	}
	this.getKid7yob=getKid7yob;


	function setKid7yob(v){
		this.Kid7yob=v;
	}
	this.setKid7yob=setKid7yob;

	this.Kid7liv=null;


	function getKid7liv() {
		return this.Kid7liv;
	}
	this.getKid7liv=getKid7liv;


	function setKid7liv(v){
		this.Kid7liv=v;
	}
	this.setKid7liv=setKid7liv;

	this.Kid7yod=null;


	function getKid7yod() {
		return this.Kid7yod;
	}
	this.getKid7yod=getKid7yod;


	function setKid7yod(v){
		this.Kid7yod=v;
	}
	this.setKid7yod=setKid7yod;

	this.Kid7dem=null;


	function getKid7dem() {
		return this.Kid7dem;
	}
	this.getKid7dem=getKid7dem;


	function setKid7dem(v){
		this.Kid7dem=v;
	}
	this.setKid7dem=setKid7dem;

	this.Kid7ons=null;


	function getKid7ons() {
		return this.Kid7ons;
	}
	this.getKid7ons=getKid7ons;


	function setKid7ons(v){
		this.Kid7ons=v;
	}
	this.setKid7ons=setKid7ons;

	this.Kid7auto=null;


	function getKid7auto() {
		return this.Kid7auto;
	}
	this.getKid7auto=getKid7auto;


	function setKid7auto(v){
		this.Kid7auto=v;
	}
	this.setKid7auto=setKid7auto;

	this.Kid7sex=null;


	function getKid7sex() {
		return this.Kid7sex;
	}
	this.getKid7sex=getKid7sex;


	function setKid7sex(v){
		this.Kid7sex=v;
	}
	this.setKid7sex=setKid7sex;

	this.Kid8id=null;


	function getKid8id() {
		return this.Kid8id;
	}
	this.getKid8id=getKid8id;


	function setKid8id(v){
		this.Kid8id=v;
	}
	this.setKid8id=setKid8id;

	this.Kid8yob=null;


	function getKid8yob() {
		return this.Kid8yob;
	}
	this.getKid8yob=getKid8yob;


	function setKid8yob(v){
		this.Kid8yob=v;
	}
	this.setKid8yob=setKid8yob;

	this.Kid8liv=null;


	function getKid8liv() {
		return this.Kid8liv;
	}
	this.getKid8liv=getKid8liv;


	function setKid8liv(v){
		this.Kid8liv=v;
	}
	this.setKid8liv=setKid8liv;

	this.Kid8yod=null;


	function getKid8yod() {
		return this.Kid8yod;
	}
	this.getKid8yod=getKid8yod;


	function setKid8yod(v){
		this.Kid8yod=v;
	}
	this.setKid8yod=setKid8yod;

	this.Kid8dem=null;


	function getKid8dem() {
		return this.Kid8dem;
	}
	this.getKid8dem=getKid8dem;


	function setKid8dem(v){
		this.Kid8dem=v;
	}
	this.setKid8dem=setKid8dem;

	this.Kid8ons=null;


	function getKid8ons() {
		return this.Kid8ons;
	}
	this.getKid8ons=getKid8ons;


	function setKid8ons(v){
		this.Kid8ons=v;
	}
	this.setKid8ons=setKid8ons;

	this.Kid8auto=null;


	function getKid8auto() {
		return this.Kid8auto;
	}
	this.getKid8auto=getKid8auto;


	function setKid8auto(v){
		this.Kid8auto=v;
	}
	this.setKid8auto=setKid8auto;

	this.Kid8sex=null;


	function getKid8sex() {
		return this.Kid8sex;
	}
	this.getKid8sex=getKid8sex;


	function setKid8sex(v){
		this.Kid8sex=v;
	}
	this.setKid8sex=setKid8sex;

	this.Kid9id=null;


	function getKid9id() {
		return this.Kid9id;
	}
	this.getKid9id=getKid9id;


	function setKid9id(v){
		this.Kid9id=v;
	}
	this.setKid9id=setKid9id;

	this.Kid9yob=null;


	function getKid9yob() {
		return this.Kid9yob;
	}
	this.getKid9yob=getKid9yob;


	function setKid9yob(v){
		this.Kid9yob=v;
	}
	this.setKid9yob=setKid9yob;

	this.Kid9liv=null;


	function getKid9liv() {
		return this.Kid9liv;
	}
	this.getKid9liv=getKid9liv;


	function setKid9liv(v){
		this.Kid9liv=v;
	}
	this.setKid9liv=setKid9liv;

	this.Kid9yod=null;


	function getKid9yod() {
		return this.Kid9yod;
	}
	this.getKid9yod=getKid9yod;


	function setKid9yod(v){
		this.Kid9yod=v;
	}
	this.setKid9yod=setKid9yod;

	this.Kid9dem=null;


	function getKid9dem() {
		return this.Kid9dem;
	}
	this.getKid9dem=getKid9dem;


	function setKid9dem(v){
		this.Kid9dem=v;
	}
	this.setKid9dem=setKid9dem;

	this.Kid9ons=null;


	function getKid9ons() {
		return this.Kid9ons;
	}
	this.getKid9ons=getKid9ons;


	function setKid9ons(v){
		this.Kid9ons=v;
	}
	this.setKid9ons=setKid9ons;

	this.Kid9auto=null;


	function getKid9auto() {
		return this.Kid9auto;
	}
	this.getKid9auto=getKid9auto;


	function setKid9auto(v){
		this.Kid9auto=v;
	}
	this.setKid9auto=setKid9auto;

	this.Kid9sex=null;


	function getKid9sex() {
		return this.Kid9sex;
	}
	this.getKid9sex=getKid9sex;


	function setKid9sex(v){
		this.Kid9sex=v;
	}
	this.setKid9sex=setKid9sex;

	this.Kid10id=null;


	function getKid10id() {
		return this.Kid10id;
	}
	this.getKid10id=getKid10id;


	function setKid10id(v){
		this.Kid10id=v;
	}
	this.setKid10id=setKid10id;

	this.Kid10yob=null;


	function getKid10yob() {
		return this.Kid10yob;
	}
	this.getKid10yob=getKid10yob;


	function setKid10yob(v){
		this.Kid10yob=v;
	}
	this.setKid10yob=setKid10yob;

	this.Kid10liv=null;


	function getKid10liv() {
		return this.Kid10liv;
	}
	this.getKid10liv=getKid10liv;


	function setKid10liv(v){
		this.Kid10liv=v;
	}
	this.setKid10liv=setKid10liv;

	this.Kid10yod=null;


	function getKid10yod() {
		return this.Kid10yod;
	}
	this.getKid10yod=getKid10yod;


	function setKid10yod(v){
		this.Kid10yod=v;
	}
	this.setKid10yod=setKid10yod;

	this.Kid10dem=null;


	function getKid10dem() {
		return this.Kid10dem;
	}
	this.getKid10dem=getKid10dem;


	function setKid10dem(v){
		this.Kid10dem=v;
	}
	this.setKid10dem=setKid10dem;

	this.Kid10ons=null;


	function getKid10ons() {
		return this.Kid10ons;
	}
	this.getKid10ons=getKid10ons;


	function setKid10ons(v){
		this.Kid10ons=v;
	}
	this.setKid10ons=setKid10ons;

	this.Kid10auto=null;


	function getKid10auto() {
		return this.Kid10auto;
	}
	this.getKid10auto=getKid10auto;


	function setKid10auto(v){
		this.Kid10auto=v;
	}
	this.setKid10auto=setKid10auto;

	this.Kid10sex=null;


	function getKid10sex() {
		return this.Kid10sex;
	}
	this.getKid10sex=getKid10sex;


	function setKid10sex(v){
		this.Kid10sex=v;
	}
	this.setKid10sex=setKid10sex;

	this.Kid11id=null;


	function getKid11id() {
		return this.Kid11id;
	}
	this.getKid11id=getKid11id;


	function setKid11id(v){
		this.Kid11id=v;
	}
	this.setKid11id=setKid11id;

	this.Kid11yob=null;


	function getKid11yob() {
		return this.Kid11yob;
	}
	this.getKid11yob=getKid11yob;


	function setKid11yob(v){
		this.Kid11yob=v;
	}
	this.setKid11yob=setKid11yob;

	this.Kid11liv=null;


	function getKid11liv() {
		return this.Kid11liv;
	}
	this.getKid11liv=getKid11liv;


	function setKid11liv(v){
		this.Kid11liv=v;
	}
	this.setKid11liv=setKid11liv;

	this.Kid11yod=null;


	function getKid11yod() {
		return this.Kid11yod;
	}
	this.getKid11yod=getKid11yod;


	function setKid11yod(v){
		this.Kid11yod=v;
	}
	this.setKid11yod=setKid11yod;

	this.Kid11dem=null;


	function getKid11dem() {
		return this.Kid11dem;
	}
	this.getKid11dem=getKid11dem;


	function setKid11dem(v){
		this.Kid11dem=v;
	}
	this.setKid11dem=setKid11dem;

	this.Kid11ons=null;


	function getKid11ons() {
		return this.Kid11ons;
	}
	this.getKid11ons=getKid11ons;


	function setKid11ons(v){
		this.Kid11ons=v;
	}
	this.setKid11ons=setKid11ons;

	this.Kid11auto=null;


	function getKid11auto() {
		return this.Kid11auto;
	}
	this.getKid11auto=getKid11auto;


	function setKid11auto(v){
		this.Kid11auto=v;
	}
	this.setKid11auto=setKid11auto;

	this.Kid11sex=null;


	function getKid11sex() {
		return this.Kid11sex;
	}
	this.getKid11sex=getKid11sex;


	function setKid11sex(v){
		this.Kid11sex=v;
	}
	this.setKid11sex=setKid11sex;

	this.Kid12id=null;


	function getKid12id() {
		return this.Kid12id;
	}
	this.getKid12id=getKid12id;


	function setKid12id(v){
		this.Kid12id=v;
	}
	this.setKid12id=setKid12id;

	this.Kid12yob=null;


	function getKid12yob() {
		return this.Kid12yob;
	}
	this.getKid12yob=getKid12yob;


	function setKid12yob(v){
		this.Kid12yob=v;
	}
	this.setKid12yob=setKid12yob;

	this.Kid12liv=null;


	function getKid12liv() {
		return this.Kid12liv;
	}
	this.getKid12liv=getKid12liv;


	function setKid12liv(v){
		this.Kid12liv=v;
	}
	this.setKid12liv=setKid12liv;

	this.Kid12yod=null;


	function getKid12yod() {
		return this.Kid12yod;
	}
	this.getKid12yod=getKid12yod;


	function setKid12yod(v){
		this.Kid12yod=v;
	}
	this.setKid12yod=setKid12yod;

	this.Kid12dem=null;


	function getKid12dem() {
		return this.Kid12dem;
	}
	this.getKid12dem=getKid12dem;


	function setKid12dem(v){
		this.Kid12dem=v;
	}
	this.setKid12dem=setKid12dem;

	this.Kid12ons=null;


	function getKid12ons() {
		return this.Kid12ons;
	}
	this.getKid12ons=getKid12ons;


	function setKid12ons(v){
		this.Kid12ons=v;
	}
	this.setKid12ons=setKid12ons;

	this.Kid12auto=null;


	function getKid12auto() {
		return this.Kid12auto;
	}
	this.getKid12auto=getKid12auto;


	function setKid12auto(v){
		this.Kid12auto=v;
	}
	this.setKid12auto=setKid12auto;

	this.Kid12sex=null;


	function getKid12sex() {
		return this.Kid12sex;
	}
	this.getKid12sex=getKid12sex;


	function setKid12sex(v){
		this.Kid12sex=v;
	}
	this.setKid12sex=setKid12sex;

	this.Kid13id=null;


	function getKid13id() {
		return this.Kid13id;
	}
	this.getKid13id=getKid13id;


	function setKid13id(v){
		this.Kid13id=v;
	}
	this.setKid13id=setKid13id;

	this.Kid13yob=null;


	function getKid13yob() {
		return this.Kid13yob;
	}
	this.getKid13yob=getKid13yob;


	function setKid13yob(v){
		this.Kid13yob=v;
	}
	this.setKid13yob=setKid13yob;

	this.Kid13liv=null;


	function getKid13liv() {
		return this.Kid13liv;
	}
	this.getKid13liv=getKid13liv;


	function setKid13liv(v){
		this.Kid13liv=v;
	}
	this.setKid13liv=setKid13liv;

	this.Kid13yod=null;


	function getKid13yod() {
		return this.Kid13yod;
	}
	this.getKid13yod=getKid13yod;


	function setKid13yod(v){
		this.Kid13yod=v;
	}
	this.setKid13yod=setKid13yod;

	this.Kid13dem=null;


	function getKid13dem() {
		return this.Kid13dem;
	}
	this.getKid13dem=getKid13dem;


	function setKid13dem(v){
		this.Kid13dem=v;
	}
	this.setKid13dem=setKid13dem;

	this.Kid13ons=null;


	function getKid13ons() {
		return this.Kid13ons;
	}
	this.getKid13ons=getKid13ons;


	function setKid13ons(v){
		this.Kid13ons=v;
	}
	this.setKid13ons=setKid13ons;

	this.Kid13auto=null;


	function getKid13auto() {
		return this.Kid13auto;
	}
	this.getKid13auto=getKid13auto;


	function setKid13auto(v){
		this.Kid13auto=v;
	}
	this.setKid13auto=setKid13auto;

	this.Kid13sex=null;


	function getKid13sex() {
		return this.Kid13sex;
	}
	this.getKid13sex=getKid13sex;


	function setKid13sex(v){
		this.Kid13sex=v;
	}
	this.setKid13sex=setKid13sex;

	this.Kid14id=null;


	function getKid14id() {
		return this.Kid14id;
	}
	this.getKid14id=getKid14id;


	function setKid14id(v){
		this.Kid14id=v;
	}
	this.setKid14id=setKid14id;

	this.Kid14yob=null;


	function getKid14yob() {
		return this.Kid14yob;
	}
	this.getKid14yob=getKid14yob;


	function setKid14yob(v){
		this.Kid14yob=v;
	}
	this.setKid14yob=setKid14yob;

	this.Kid14liv=null;


	function getKid14liv() {
		return this.Kid14liv;
	}
	this.getKid14liv=getKid14liv;


	function setKid14liv(v){
		this.Kid14liv=v;
	}
	this.setKid14liv=setKid14liv;

	this.Kid14yod=null;


	function getKid14yod() {
		return this.Kid14yod;
	}
	this.getKid14yod=getKid14yod;


	function setKid14yod(v){
		this.Kid14yod=v;
	}
	this.setKid14yod=setKid14yod;

	this.Kid14dem=null;


	function getKid14dem() {
		return this.Kid14dem;
	}
	this.getKid14dem=getKid14dem;


	function setKid14dem(v){
		this.Kid14dem=v;
	}
	this.setKid14dem=setKid14dem;

	this.Kid14ons=null;


	function getKid14ons() {
		return this.Kid14ons;
	}
	this.getKid14ons=getKid14ons;


	function setKid14ons(v){
		this.Kid14ons=v;
	}
	this.setKid14ons=setKid14ons;

	this.Kid14auto=null;


	function getKid14auto() {
		return this.Kid14auto;
	}
	this.getKid14auto=getKid14auto;


	function setKid14auto(v){
		this.Kid14auto=v;
	}
	this.setKid14auto=setKid14auto;

	this.Kid14sex=null;


	function getKid14sex() {
		return this.Kid14sex;
	}
	this.getKid14sex=getKid14sex;


	function setKid14sex(v){
		this.Kid14sex=v;
	}
	this.setKid14sex=setKid14sex;

	this.Kid15id=null;


	function getKid15id() {
		return this.Kid15id;
	}
	this.getKid15id=getKid15id;


	function setKid15id(v){
		this.Kid15id=v;
	}
	this.setKid15id=setKid15id;

	this.Kid15yob=null;


	function getKid15yob() {
		return this.Kid15yob;
	}
	this.getKid15yob=getKid15yob;


	function setKid15yob(v){
		this.Kid15yob=v;
	}
	this.setKid15yob=setKid15yob;

	this.Kid15liv=null;


	function getKid15liv() {
		return this.Kid15liv;
	}
	this.getKid15liv=getKid15liv;


	function setKid15liv(v){
		this.Kid15liv=v;
	}
	this.setKid15liv=setKid15liv;

	this.Kid15yod=null;


	function getKid15yod() {
		return this.Kid15yod;
	}
	this.getKid15yod=getKid15yod;


	function setKid15yod(v){
		this.Kid15yod=v;
	}
	this.setKid15yod=setKid15yod;

	this.Kid15dem=null;


	function getKid15dem() {
		return this.Kid15dem;
	}
	this.getKid15dem=getKid15dem;


	function setKid15dem(v){
		this.Kid15dem=v;
	}
	this.setKid15dem=setKid15dem;

	this.Kid15ons=null;


	function getKid15ons() {
		return this.Kid15ons;
	}
	this.getKid15ons=getKid15ons;


	function setKid15ons(v){
		this.Kid15ons=v;
	}
	this.setKid15ons=setKid15ons;

	this.Kid15auto=null;


	function getKid15auto() {
		return this.Kid15auto;
	}
	this.getKid15auto=getKid15auto;


	function setKid15auto(v){
		this.Kid15auto=v;
	}
	this.setKid15auto=setKid15auto;

	this.Kid15sex=null;


	function getKid15sex() {
		return this.Kid15sex;
	}
	this.getKid15sex=getKid15sex;


	function setKid15sex(v){
		this.Kid15sex=v;
	}
	this.setKid15sex=setKid15sex;

	this.Relchg=null;


	function getRelchg() {
		return this.Relchg;
	}
	this.getRelchg=getRelchg;


	function setRelchg(v){
		this.Relchg=v;
	}
	this.setRelchg=setRelchg;

	this.Relsdem=null;


	function getRelsdem() {
		return this.Relsdem;
	}
	this.getRelsdem=getRelsdem;


	function setRelsdem(v){
		this.Relsdem=v;
	}
	this.setRelsdem=setRelsdem;

	this.Rel1id=null;


	function getRel1id() {
		return this.Rel1id;
	}
	this.getRel1id=getRel1id;


	function setRel1id(v){
		this.Rel1id=v;
	}
	this.setRel1id=setRel1id;

	this.Rel1yob=null;


	function getRel1yob() {
		return this.Rel1yob;
	}
	this.getRel1yob=getRel1yob;


	function setRel1yob(v){
		this.Rel1yob=v;
	}
	this.setRel1yob=setRel1yob;

	this.Rel1liv=null;


	function getRel1liv() {
		return this.Rel1liv;
	}
	this.getRel1liv=getRel1liv;


	function setRel1liv(v){
		this.Rel1liv=v;
	}
	this.setRel1liv=setRel1liv;

	this.Rel1yod=null;


	function getRel1yod() {
		return this.Rel1yod;
	}
	this.getRel1yod=getRel1yod;


	function setRel1yod(v){
		this.Rel1yod=v;
	}
	this.setRel1yod=setRel1yod;

	this.Rel1ons=null;


	function getRel1ons() {
		return this.Rel1ons;
	}
	this.getRel1ons=getRel1ons;


	function setRel1ons(v){
		this.Rel1ons=v;
	}
	this.setRel1ons=setRel1ons;

	this.Rel2id=null;


	function getRel2id() {
		return this.Rel2id;
	}
	this.getRel2id=getRel2id;


	function setRel2id(v){
		this.Rel2id=v;
	}
	this.setRel2id=setRel2id;

	this.Rel2yob=null;


	function getRel2yob() {
		return this.Rel2yob;
	}
	this.getRel2yob=getRel2yob;


	function setRel2yob(v){
		this.Rel2yob=v;
	}
	this.setRel2yob=setRel2yob;

	this.Rel2liv=null;


	function getRel2liv() {
		return this.Rel2liv;
	}
	this.getRel2liv=getRel2liv;


	function setRel2liv(v){
		this.Rel2liv=v;
	}
	this.setRel2liv=setRel2liv;

	this.Rel2yod=null;


	function getRel2yod() {
		return this.Rel2yod;
	}
	this.getRel2yod=getRel2yod;


	function setRel2yod(v){
		this.Rel2yod=v;
	}
	this.setRel2yod=setRel2yod;

	this.Rel2ons=null;


	function getRel2ons() {
		return this.Rel2ons;
	}
	this.getRel2ons=getRel2ons;


	function setRel2ons(v){
		this.Rel2ons=v;
	}
	this.setRel2ons=setRel2ons;

	this.Rel3id=null;


	function getRel3id() {
		return this.Rel3id;
	}
	this.getRel3id=getRel3id;


	function setRel3id(v){
		this.Rel3id=v;
	}
	this.setRel3id=setRel3id;

	this.Rel3yob=null;


	function getRel3yob() {
		return this.Rel3yob;
	}
	this.getRel3yob=getRel3yob;


	function setRel3yob(v){
		this.Rel3yob=v;
	}
	this.setRel3yob=setRel3yob;

	this.Rel3liv=null;


	function getRel3liv() {
		return this.Rel3liv;
	}
	this.getRel3liv=getRel3liv;


	function setRel3liv(v){
		this.Rel3liv=v;
	}
	this.setRel3liv=setRel3liv;

	this.Rel3yod=null;


	function getRel3yod() {
		return this.Rel3yod;
	}
	this.getRel3yod=getRel3yod;


	function setRel3yod(v){
		this.Rel3yod=v;
	}
	this.setRel3yod=setRel3yod;

	this.Rel3ons=null;


	function getRel3ons() {
		return this.Rel3ons;
	}
	this.getRel3ons=getRel3ons;


	function setRel3ons(v){
		this.Rel3ons=v;
	}
	this.setRel3ons=setRel3ons;

	this.Rel4id=null;


	function getRel4id() {
		return this.Rel4id;
	}
	this.getRel4id=getRel4id;


	function setRel4id(v){
		this.Rel4id=v;
	}
	this.setRel4id=setRel4id;

	this.Rel4yob=null;


	function getRel4yob() {
		return this.Rel4yob;
	}
	this.getRel4yob=getRel4yob;


	function setRel4yob(v){
		this.Rel4yob=v;
	}
	this.setRel4yob=setRel4yob;

	this.Rel4liv=null;


	function getRel4liv() {
		return this.Rel4liv;
	}
	this.getRel4liv=getRel4liv;


	function setRel4liv(v){
		this.Rel4liv=v;
	}
	this.setRel4liv=setRel4liv;

	this.Rel4yod=null;


	function getRel4yod() {
		return this.Rel4yod;
	}
	this.getRel4yod=getRel4yod;


	function setRel4yod(v){
		this.Rel4yod=v;
	}
	this.setRel4yod=setRel4yod;

	this.Rel4ons=null;


	function getRel4ons() {
		return this.Rel4ons;
	}
	this.getRel4ons=getRel4ons;


	function setRel4ons(v){
		this.Rel4ons=v;
	}
	this.setRel4ons=setRel4ons;

	this.Rel5id=null;


	function getRel5id() {
		return this.Rel5id;
	}
	this.getRel5id=getRel5id;


	function setRel5id(v){
		this.Rel5id=v;
	}
	this.setRel5id=setRel5id;

	this.Rel5yob=null;


	function getRel5yob() {
		return this.Rel5yob;
	}
	this.getRel5yob=getRel5yob;


	function setRel5yob(v){
		this.Rel5yob=v;
	}
	this.setRel5yob=setRel5yob;

	this.Rel5liv=null;


	function getRel5liv() {
		return this.Rel5liv;
	}
	this.getRel5liv=getRel5liv;


	function setRel5liv(v){
		this.Rel5liv=v;
	}
	this.setRel5liv=setRel5liv;

	this.Rel5yod=null;


	function getRel5yod() {
		return this.Rel5yod;
	}
	this.getRel5yod=getRel5yod;


	function setRel5yod(v){
		this.Rel5yod=v;
	}
	this.setRel5yod=setRel5yod;

	this.Rel5ons=null;


	function getRel5ons() {
		return this.Rel5ons;
	}
	this.getRel5ons=getRel5ons;


	function setRel5ons(v){
		this.Rel5ons=v;
	}
	this.setRel5ons=setRel5ons;

	this.Rel6id=null;


	function getRel6id() {
		return this.Rel6id;
	}
	this.getRel6id=getRel6id;


	function setRel6id(v){
		this.Rel6id=v;
	}
	this.setRel6id=setRel6id;

	this.Rel6yob=null;


	function getRel6yob() {
		return this.Rel6yob;
	}
	this.getRel6yob=getRel6yob;


	function setRel6yob(v){
		this.Rel6yob=v;
	}
	this.setRel6yob=setRel6yob;

	this.Rel6liv=null;


	function getRel6liv() {
		return this.Rel6liv;
	}
	this.getRel6liv=getRel6liv;


	function setRel6liv(v){
		this.Rel6liv=v;
	}
	this.setRel6liv=setRel6liv;

	this.Rel6yod=null;


	function getRel6yod() {
		return this.Rel6yod;
	}
	this.getRel6yod=getRel6yod;


	function setRel6yod(v){
		this.Rel6yod=v;
	}
	this.setRel6yod=setRel6yod;

	this.Rel6ons=null;


	function getRel6ons() {
		return this.Rel6ons;
	}
	this.getRel6ons=getRel6ons;


	function setRel6ons(v){
		this.Rel6ons=v;
	}
	this.setRel6ons=setRel6ons;

	this.Rel7id=null;


	function getRel7id() {
		return this.Rel7id;
	}
	this.getRel7id=getRel7id;


	function setRel7id(v){
		this.Rel7id=v;
	}
	this.setRel7id=setRel7id;

	this.Rel7yob=null;


	function getRel7yob() {
		return this.Rel7yob;
	}
	this.getRel7yob=getRel7yob;


	function setRel7yob(v){
		this.Rel7yob=v;
	}
	this.setRel7yob=setRel7yob;

	this.Rel7liv=null;


	function getRel7liv() {
		return this.Rel7liv;
	}
	this.getRel7liv=getRel7liv;


	function setRel7liv(v){
		this.Rel7liv=v;
	}
	this.setRel7liv=setRel7liv;

	this.Rel7yod=null;


	function getRel7yod() {
		return this.Rel7yod;
	}
	this.getRel7yod=getRel7yod;


	function setRel7yod(v){
		this.Rel7yod=v;
	}
	this.setRel7yod=setRel7yod;

	this.Rel7ons=null;


	function getRel7ons() {
		return this.Rel7ons;
	}
	this.getRel7ons=getRel7ons;


	function setRel7ons(v){
		this.Rel7ons=v;
	}
	this.setRel7ons=setRel7ons;

	this.Rel8id=null;


	function getRel8id() {
		return this.Rel8id;
	}
	this.getRel8id=getRel8id;


	function setRel8id(v){
		this.Rel8id=v;
	}
	this.setRel8id=setRel8id;

	this.Rel8yob=null;


	function getRel8yob() {
		return this.Rel8yob;
	}
	this.getRel8yob=getRel8yob;


	function setRel8yob(v){
		this.Rel8yob=v;
	}
	this.setRel8yob=setRel8yob;

	this.Rel8liv=null;


	function getRel8liv() {
		return this.Rel8liv;
	}
	this.getRel8liv=getRel8liv;


	function setRel8liv(v){
		this.Rel8liv=v;
	}
	this.setRel8liv=setRel8liv;

	this.Rel8yod=null;


	function getRel8yod() {
		return this.Rel8yod;
	}
	this.getRel8yod=getRel8yod;


	function setRel8yod(v){
		this.Rel8yod=v;
	}
	this.setRel8yod=setRel8yod;

	this.Rel8ons=null;


	function getRel8ons() {
		return this.Rel8ons;
	}
	this.getRel8ons=getRel8ons;


	function setRel8ons(v){
		this.Rel8ons=v;
	}
	this.setRel8ons=setRel8ons;

	this.Rel9id=null;


	function getRel9id() {
		return this.Rel9id;
	}
	this.getRel9id=getRel9id;


	function setRel9id(v){
		this.Rel9id=v;
	}
	this.setRel9id=setRel9id;

	this.Rel9yob=null;


	function getRel9yob() {
		return this.Rel9yob;
	}
	this.getRel9yob=getRel9yob;


	function setRel9yob(v){
		this.Rel9yob=v;
	}
	this.setRel9yob=setRel9yob;

	this.Rel9liv=null;


	function getRel9liv() {
		return this.Rel9liv;
	}
	this.getRel9liv=getRel9liv;


	function setRel9liv(v){
		this.Rel9liv=v;
	}
	this.setRel9liv=setRel9liv;

	this.Rel9yod=null;


	function getRel9yod() {
		return this.Rel9yod;
	}
	this.getRel9yod=getRel9yod;


	function setRel9yod(v){
		this.Rel9yod=v;
	}
	this.setRel9yod=setRel9yod;

	this.Rel9ons=null;


	function getRel9ons() {
		return this.Rel9ons;
	}
	this.getRel9ons=getRel9ons;


	function setRel9ons(v){
		this.Rel9ons=v;
	}
	this.setRel9ons=setRel9ons;

	this.Rel10id=null;


	function getRel10id() {
		return this.Rel10id;
	}
	this.getRel10id=getRel10id;


	function setRel10id(v){
		this.Rel10id=v;
	}
	this.setRel10id=setRel10id;

	this.Rel10yob=null;


	function getRel10yob() {
		return this.Rel10yob;
	}
	this.getRel10yob=getRel10yob;


	function setRel10yob(v){
		this.Rel10yob=v;
	}
	this.setRel10yob=setRel10yob;

	this.Rel10liv=null;


	function getRel10liv() {
		return this.Rel10liv;
	}
	this.getRel10liv=getRel10liv;


	function setRel10liv(v){
		this.Rel10liv=v;
	}
	this.setRel10liv=setRel10liv;

	this.Rel10yod=null;


	function getRel10yod() {
		return this.Rel10yod;
	}
	this.getRel10yod=getRel10yod;


	function setRel10yod(v){
		this.Rel10yod=v;
	}
	this.setRel10yod=setRel10yod;

	this.Rel10ons=null;


	function getRel10ons() {
		return this.Rel10ons;
	}
	this.getRel10ons=getRel10ons;


	function setRel10ons(v){
		this.Rel10ons=v;
	}
	this.setRel10ons=setRel10ons;

	this.Rel11id=null;


	function getRel11id() {
		return this.Rel11id;
	}
	this.getRel11id=getRel11id;


	function setRel11id(v){
		this.Rel11id=v;
	}
	this.setRel11id=setRel11id;

	this.Rel11yob=null;


	function getRel11yob() {
		return this.Rel11yob;
	}
	this.getRel11yob=getRel11yob;


	function setRel11yob(v){
		this.Rel11yob=v;
	}
	this.setRel11yob=setRel11yob;

	this.Rel11liv=null;


	function getRel11liv() {
		return this.Rel11liv;
	}
	this.getRel11liv=getRel11liv;


	function setRel11liv(v){
		this.Rel11liv=v;
	}
	this.setRel11liv=setRel11liv;

	this.Rel11yod=null;


	function getRel11yod() {
		return this.Rel11yod;
	}
	this.getRel11yod=getRel11yod;


	function setRel11yod(v){
		this.Rel11yod=v;
	}
	this.setRel11yod=setRel11yod;

	this.Rel11ons=null;


	function getRel11ons() {
		return this.Rel11ons;
	}
	this.getRel11ons=getRel11ons;


	function setRel11ons(v){
		this.Rel11ons=v;
	}
	this.setRel11ons=setRel11ons;

	this.Rel12id=null;


	function getRel12id() {
		return this.Rel12id;
	}
	this.getRel12id=getRel12id;


	function setRel12id(v){
		this.Rel12id=v;
	}
	this.setRel12id=setRel12id;

	this.Rel12yob=null;


	function getRel12yob() {
		return this.Rel12yob;
	}
	this.getRel12yob=getRel12yob;


	function setRel12yob(v){
		this.Rel12yob=v;
	}
	this.setRel12yob=setRel12yob;

	this.Rel12liv=null;


	function getRel12liv() {
		return this.Rel12liv;
	}
	this.getRel12liv=getRel12liv;


	function setRel12liv(v){
		this.Rel12liv=v;
	}
	this.setRel12liv=setRel12liv;

	this.Rel12yod=null;


	function getRel12yod() {
		return this.Rel12yod;
	}
	this.getRel12yod=getRel12yod;


	function setRel12yod(v){
		this.Rel12yod=v;
	}
	this.setRel12yod=setRel12yod;

	this.Rel12ons=null;


	function getRel12ons() {
		return this.Rel12ons;
	}
	this.getRel12ons=getRel12ons;


	function setRel12ons(v){
		this.Rel12ons=v;
	}
	this.setRel12ons=setRel12ons;

	this.Rel13id=null;


	function getRel13id() {
		return this.Rel13id;
	}
	this.getRel13id=getRel13id;


	function setRel13id(v){
		this.Rel13id=v;
	}
	this.setRel13id=setRel13id;

	this.Rel13yob=null;


	function getRel13yob() {
		return this.Rel13yob;
	}
	this.getRel13yob=getRel13yob;


	function setRel13yob(v){
		this.Rel13yob=v;
	}
	this.setRel13yob=setRel13yob;

	this.Rel13liv=null;


	function getRel13liv() {
		return this.Rel13liv;
	}
	this.getRel13liv=getRel13liv;


	function setRel13liv(v){
		this.Rel13liv=v;
	}
	this.setRel13liv=setRel13liv;

	this.Rel13yod=null;


	function getRel13yod() {
		return this.Rel13yod;
	}
	this.getRel13yod=getRel13yod;


	function setRel13yod(v){
		this.Rel13yod=v;
	}
	this.setRel13yod=setRel13yod;

	this.Rel13ons=null;


	function getRel13ons() {
		return this.Rel13ons;
	}
	this.getRel13ons=getRel13ons;


	function setRel13ons(v){
		this.Rel13ons=v;
	}
	this.setRel13ons=setRel13ons;

	this.Rel14id=null;


	function getRel14id() {
		return this.Rel14id;
	}
	this.getRel14id=getRel14id;


	function setRel14id(v){
		this.Rel14id=v;
	}
	this.setRel14id=setRel14id;

	this.Rel14yob=null;


	function getRel14yob() {
		return this.Rel14yob;
	}
	this.getRel14yob=getRel14yob;


	function setRel14yob(v){
		this.Rel14yob=v;
	}
	this.setRel14yob=setRel14yob;

	this.Rel14liv=null;


	function getRel14liv() {
		return this.Rel14liv;
	}
	this.getRel14liv=getRel14liv;


	function setRel14liv(v){
		this.Rel14liv=v;
	}
	this.setRel14liv=setRel14liv;

	this.Rel14yod=null;


	function getRel14yod() {
		return this.Rel14yod;
	}
	this.getRel14yod=getRel14yod;


	function setRel14yod(v){
		this.Rel14yod=v;
	}
	this.setRel14yod=setRel14yod;

	this.Rel14ons=null;


	function getRel14ons() {
		return this.Rel14ons;
	}
	this.getRel14ons=getRel14ons;


	function setRel14ons(v){
		this.Rel14ons=v;
	}
	this.setRel14ons=setRel14ons;

	this.Rel15id=null;


	function getRel15id() {
		return this.Rel15id;
	}
	this.getRel15id=getRel15id;


	function setRel15id(v){
		this.Rel15id=v;
	}
	this.setRel15id=setRel15id;

	this.Rel15yob=null;


	function getRel15yob() {
		return this.Rel15yob;
	}
	this.getRel15yob=getRel15yob;


	function setRel15yob(v){
		this.Rel15yob=v;
	}
	this.setRel15yob=setRel15yob;

	this.Rel15liv=null;


	function getRel15liv() {
		return this.Rel15liv;
	}
	this.getRel15liv=getRel15liv;


	function setRel15liv(v){
		this.Rel15liv=v;
	}
	this.setRel15liv=setRel15liv;

	this.Rel15yod=null;


	function getRel15yod() {
		return this.Rel15yod;
	}
	this.getRel15yod=getRel15yod;


	function setRel15yod(v){
		this.Rel15yod=v;
	}
	this.setRel15yod=setRel15yod;

	this.Rel15ons=null;


	function getRel15ons() {
		return this.Rel15ons;
	}
	this.getRel15ons=getRel15ons;


	function setRel15ons(v){
		this.Rel15ons=v;
	}
	this.setRel15ons=setRel15ons;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="EXAMDATE"){
				return this.Examdate ;
			} else 
			if(xmlPath=="A3CHG"){
				return this.A3chg ;
			} else 
			if(xmlPath=="PARCHG"){
				return this.Parchg ;
			} else 
			if(xmlPath=="FAMILYID"){
				return this.Familyid ;
			} else 
			if(xmlPath=="GMOMID"){
				return this.Gmomid ;
			} else 
			if(xmlPath=="GMOMYOB"){
				return this.Gmomyob ;
			} else 
			if(xmlPath=="GMOMLIV"){
				return this.Gmomliv ;
			} else 
			if(xmlPath=="GMOMYOD"){
				return this.Gmomyod ;
			} else 
			if(xmlPath=="GMOMDEM"){
				return this.Gmomdem ;
			} else 
			if(xmlPath=="GMOMONSET"){
				return this.Gmomonset ;
			} else 
			if(xmlPath=="GMOMAUTO"){
				return this.Gmomauto ;
			} else 
			if(xmlPath=="GDADID"){
				return this.Gdadid ;
			} else 
			if(xmlPath=="GDADYOB"){
				return this.Gdadyob ;
			} else 
			if(xmlPath=="GDADLIV"){
				return this.Gdadliv ;
			} else 
			if(xmlPath=="GDADYOD"){
				return this.Gdadyod ;
			} else 
			if(xmlPath=="GDADDEM"){
				return this.Gdaddem ;
			} else 
			if(xmlPath=="GDADONSET"){
				return this.Gdadonset ;
			} else 
			if(xmlPath=="GDADAUTO"){
				return this.Gdadauto ;
			} else 
			if(xmlPath=="MOMID"){
				return this.Momid ;
			} else 
			if(xmlPath=="MOMYOB"){
				return this.Momyob ;
			} else 
			if(xmlPath=="MOMLIV"){
				return this.Momliv ;
			} else 
			if(xmlPath=="MOMYOD"){
				return this.Momyod ;
			} else 
			if(xmlPath=="MOMDEM"){
				return this.Momdem ;
			} else 
			if(xmlPath=="MOMONSET"){
				return this.Momonset ;
			} else 
			if(xmlPath=="MOMAUTO"){
				return this.Momauto ;
			} else 
			if(xmlPath=="DADID"){
				return this.Dadid ;
			} else 
			if(xmlPath=="DADYOB"){
				return this.Dadyob ;
			} else 
			if(xmlPath=="DADLIV"){
				return this.Dadliv ;
			} else 
			if(xmlPath=="DADYOD"){
				return this.Dadyod ;
			} else 
			if(xmlPath=="DADDEM"){
				return this.Daddem ;
			} else 
			if(xmlPath=="DADONSET"){
				return this.Dadonset ;
			} else 
			if(xmlPath=="DADAUTO"){
				return this.Dadauto ;
			} else 
			if(xmlPath=="SIBCHG"){
				return this.Sibchg ;
			} else 
			if(xmlPath=="TWIN"){
				return this.Twin ;
			} else 
			if(xmlPath=="TWINTYPE"){
				return this.Twintype ;
			} else 
			if(xmlPath=="SIBS"){
				return this.Sibs ;
			} else 
			if(xmlPath=="SIB1ID"){
				return this.Sib1id ;
			} else 
			if(xmlPath=="SIB1YOB"){
				return this.Sib1yob ;
			} else 
			if(xmlPath=="SIB1LIV"){
				return this.Sib1liv ;
			} else 
			if(xmlPath=="SIB1YOD"){
				return this.Sib1yod ;
			} else 
			if(xmlPath=="SIB1DEM"){
				return this.Sib1dem ;
			} else 
			if(xmlPath=="SIB1ONS"){
				return this.Sib1ons ;
			} else 
			if(xmlPath=="SIB1AUTO"){
				return this.Sib1auto ;
			} else 
			if(xmlPath=="SIB1SEX"){
				return this.Sib1sex ;
			} else 
			if(xmlPath=="SIB2ID"){
				return this.Sib2id ;
			} else 
			if(xmlPath=="SIB2YOB"){
				return this.Sib2yob ;
			} else 
			if(xmlPath=="SIB2LIV"){
				return this.Sib2liv ;
			} else 
			if(xmlPath=="SIB2YOD"){
				return this.Sib2yod ;
			} else 
			if(xmlPath=="SIB2DEM"){
				return this.Sib2dem ;
			} else 
			if(xmlPath=="SIB2ONS"){
				return this.Sib2ons ;
			} else 
			if(xmlPath=="SIB2AUTO"){
				return this.Sib2auto ;
			} else 
			if(xmlPath=="SIB2SEX"){
				return this.Sib2sex ;
			} else 
			if(xmlPath=="SIB3ID"){
				return this.Sib3id ;
			} else 
			if(xmlPath=="SIB3YOB"){
				return this.Sib3yob ;
			} else 
			if(xmlPath=="SIB3LIV"){
				return this.Sib3liv ;
			} else 
			if(xmlPath=="SIB3YOD"){
				return this.Sib3yod ;
			} else 
			if(xmlPath=="SIB3DEM"){
				return this.Sib3dem ;
			} else 
			if(xmlPath=="SIB3ONS"){
				return this.Sib3ons ;
			} else 
			if(xmlPath=="SIB3AUTO"){
				return this.Sib3auto ;
			} else 
			if(xmlPath=="SIB3SEX"){
				return this.Sib3sex ;
			} else 
			if(xmlPath=="SIB4ID"){
				return this.Sib4id ;
			} else 
			if(xmlPath=="SIB4YOB"){
				return this.Sib4yob ;
			} else 
			if(xmlPath=="SIB4LIV"){
				return this.Sib4liv ;
			} else 
			if(xmlPath=="SIB4YOD"){
				return this.Sib4yod ;
			} else 
			if(xmlPath=="SIB4DEM"){
				return this.Sib4dem ;
			} else 
			if(xmlPath=="SIB4ONS"){
				return this.Sib4ons ;
			} else 
			if(xmlPath=="SIB4AUTO"){
				return this.Sib4auto ;
			} else 
			if(xmlPath=="SIB4SEX"){
				return this.Sib4sex ;
			} else 
			if(xmlPath=="SIB5ID"){
				return this.Sib5id ;
			} else 
			if(xmlPath=="SIB5YOB"){
				return this.Sib5yob ;
			} else 
			if(xmlPath=="SIB5LIV"){
				return this.Sib5liv ;
			} else 
			if(xmlPath=="SIB5YOD"){
				return this.Sib5yod ;
			} else 
			if(xmlPath=="SIB5DEM"){
				return this.Sib5dem ;
			} else 
			if(xmlPath=="SIB5ONS"){
				return this.Sib5ons ;
			} else 
			if(xmlPath=="SIB5AUTO"){
				return this.Sib5auto ;
			} else 
			if(xmlPath=="SIB5SEX"){
				return this.Sib5sex ;
			} else 
			if(xmlPath=="SIB6ID"){
				return this.Sib6id ;
			} else 
			if(xmlPath=="SIB6YOB"){
				return this.Sib6yob ;
			} else 
			if(xmlPath=="SIB6LIV"){
				return this.Sib6liv ;
			} else 
			if(xmlPath=="SIB6YOD"){
				return this.Sib6yod ;
			} else 
			if(xmlPath=="SIB6DEM"){
				return this.Sib6dem ;
			} else 
			if(xmlPath=="SIB6ONS"){
				return this.Sib6ons ;
			} else 
			if(xmlPath=="SIB6AUTO"){
				return this.Sib6auto ;
			} else 
			if(xmlPath=="SIB6SEX"){
				return this.Sib6sex ;
			} else 
			if(xmlPath=="SIB7ID"){
				return this.Sib7id ;
			} else 
			if(xmlPath=="SIB7YOB"){
				return this.Sib7yob ;
			} else 
			if(xmlPath=="SIB7LIV"){
				return this.Sib7liv ;
			} else 
			if(xmlPath=="SIB7YOD"){
				return this.Sib7yod ;
			} else 
			if(xmlPath=="SIB7DEM"){
				return this.Sib7dem ;
			} else 
			if(xmlPath=="SIB7ONS"){
				return this.Sib7ons ;
			} else 
			if(xmlPath=="SIB7AUTO"){
				return this.Sib7auto ;
			} else 
			if(xmlPath=="SIB7SEX"){
				return this.Sib7sex ;
			} else 
			if(xmlPath=="SIB8ID"){
				return this.Sib8id ;
			} else 
			if(xmlPath=="SIB8YOB"){
				return this.Sib8yob ;
			} else 
			if(xmlPath=="SIB8LIV"){
				return this.Sib8liv ;
			} else 
			if(xmlPath=="SIB8YOD"){
				return this.Sib8yod ;
			} else 
			if(xmlPath=="SIB8DEM"){
				return this.Sib8dem ;
			} else 
			if(xmlPath=="SIB8ONS"){
				return this.Sib8ons ;
			} else 
			if(xmlPath=="SIB8AUTO"){
				return this.Sib8auto ;
			} else 
			if(xmlPath=="SIB8SEX"){
				return this.Sib8sex ;
			} else 
			if(xmlPath=="SIB9ID"){
				return this.Sib9id ;
			} else 
			if(xmlPath=="SIB9YOB"){
				return this.Sib9yob ;
			} else 
			if(xmlPath=="SIB9LIV"){
				return this.Sib9liv ;
			} else 
			if(xmlPath=="SIB9YOD"){
				return this.Sib9yod ;
			} else 
			if(xmlPath=="SIB9DEM"){
				return this.Sib9dem ;
			} else 
			if(xmlPath=="SIB9ONS"){
				return this.Sib9ons ;
			} else 
			if(xmlPath=="SIB9AUTO"){
				return this.Sib9auto ;
			} else 
			if(xmlPath=="SIB9SEX"){
				return this.Sib9sex ;
			} else 
			if(xmlPath=="SIB10ID"){
				return this.Sib10id ;
			} else 
			if(xmlPath=="SIB10YOB"){
				return this.Sib10yob ;
			} else 
			if(xmlPath=="SIB10LIV"){
				return this.Sib10liv ;
			} else 
			if(xmlPath=="SIB10YOD"){
				return this.Sib10yod ;
			} else 
			if(xmlPath=="SIB10DEM"){
				return this.Sib10dem ;
			} else 
			if(xmlPath=="SIB10ONS"){
				return this.Sib10ons ;
			} else 
			if(xmlPath=="SIB10AUTO"){
				return this.Sib10auto ;
			} else 
			if(xmlPath=="SIB10SEX"){
				return this.Sib10sex ;
			} else 
			if(xmlPath=="SIB11ID"){
				return this.Sib11id ;
			} else 
			if(xmlPath=="SIB11YOB"){
				return this.Sib11yob ;
			} else 
			if(xmlPath=="SIB11LIV"){
				return this.Sib11liv ;
			} else 
			if(xmlPath=="SIB11YOD"){
				return this.Sib11yod ;
			} else 
			if(xmlPath=="SIB11DEM"){
				return this.Sib11dem ;
			} else 
			if(xmlPath=="SIB11ONS"){
				return this.Sib11ons ;
			} else 
			if(xmlPath=="SIB11AUTO"){
				return this.Sib11auto ;
			} else 
			if(xmlPath=="SIB11SEX"){
				return this.Sib11sex ;
			} else 
			if(xmlPath=="SIB12ID"){
				return this.Sib12id ;
			} else 
			if(xmlPath=="SIB12YOB"){
				return this.Sib12yob ;
			} else 
			if(xmlPath=="SIB12LIV"){
				return this.Sib12liv ;
			} else 
			if(xmlPath=="SIB12YOD"){
				return this.Sib12yod ;
			} else 
			if(xmlPath=="SIB12DEM"){
				return this.Sib12dem ;
			} else 
			if(xmlPath=="SIB12ONS"){
				return this.Sib12ons ;
			} else 
			if(xmlPath=="SIB12AUTO"){
				return this.Sib12auto ;
			} else 
			if(xmlPath=="SIB12SEX"){
				return this.Sib12sex ;
			} else 
			if(xmlPath=="SIB13ID"){
				return this.Sib13id ;
			} else 
			if(xmlPath=="SIB13YOB"){
				return this.Sib13yob ;
			} else 
			if(xmlPath=="SIB13LIV"){
				return this.Sib13liv ;
			} else 
			if(xmlPath=="SIB13YOD"){
				return this.Sib13yod ;
			} else 
			if(xmlPath=="SIB13DEM"){
				return this.Sib13dem ;
			} else 
			if(xmlPath=="SIB13ONS"){
				return this.Sib13ons ;
			} else 
			if(xmlPath=="SIB13AUTO"){
				return this.Sib13auto ;
			} else 
			if(xmlPath=="SIB13SEX"){
				return this.Sib13sex ;
			} else 
			if(xmlPath=="SIB14ID"){
				return this.Sib14id ;
			} else 
			if(xmlPath=="SIB14YOB"){
				return this.Sib14yob ;
			} else 
			if(xmlPath=="SIB14LIV"){
				return this.Sib14liv ;
			} else 
			if(xmlPath=="SIB14YOD"){
				return this.Sib14yod ;
			} else 
			if(xmlPath=="SIB14DEM"){
				return this.Sib14dem ;
			} else 
			if(xmlPath=="SIB14ONS"){
				return this.Sib14ons ;
			} else 
			if(xmlPath=="SIB14AUTO"){
				return this.Sib14auto ;
			} else 
			if(xmlPath=="SIB14SEX"){
				return this.Sib14sex ;
			} else 
			if(xmlPath=="SIB15ID"){
				return this.Sib15id ;
			} else 
			if(xmlPath=="SIB15YOB"){
				return this.Sib15yob ;
			} else 
			if(xmlPath=="SIB15LIV"){
				return this.Sib15liv ;
			} else 
			if(xmlPath=="SIB15YOD"){
				return this.Sib15yod ;
			} else 
			if(xmlPath=="SIB15DEM"){
				return this.Sib15dem ;
			} else 
			if(xmlPath=="SIB15ONS"){
				return this.Sib15ons ;
			} else 
			if(xmlPath=="SIB155AUTO"){
				return this.Sib155auto ;
			} else 
			if(xmlPath=="SIB155SEX"){
				return this.Sib155sex ;
			} else 
			if(xmlPath=="SIB16ID"){
				return this.Sib16id ;
			} else 
			if(xmlPath=="SIB16YOB"){
				return this.Sib16yob ;
			} else 
			if(xmlPath=="SIB16LIV"){
				return this.Sib16liv ;
			} else 
			if(xmlPath=="SIB16YOD"){
				return this.Sib16yod ;
			} else 
			if(xmlPath=="SIB16DEM"){
				return this.Sib16dem ;
			} else 
			if(xmlPath=="SIB16ONS"){
				return this.Sib16ons ;
			} else 
			if(xmlPath=="SIB16AUTO"){
				return this.Sib16auto ;
			} else 
			if(xmlPath=="SIB16SEX"){
				return this.Sib16sex ;
			} else 
			if(xmlPath=="SIB17ID"){
				return this.Sib17id ;
			} else 
			if(xmlPath=="SIB17YOB"){
				return this.Sib17yob ;
			} else 
			if(xmlPath=="SIB17LIV"){
				return this.Sib17liv ;
			} else 
			if(xmlPath=="SIB17YOD"){
				return this.Sib17yod ;
			} else 
			if(xmlPath=="SIB17DEM"){
				return this.Sib17dem ;
			} else 
			if(xmlPath=="SIB17ONS"){
				return this.Sib17ons ;
			} else 
			if(xmlPath=="SIB17AUTO"){
				return this.Sib17auto ;
			} else 
			if(xmlPath=="SIB17SEX"){
				return this.Sib17sex ;
			} else 
			if(xmlPath=="SIB18ID"){
				return this.Sib18id ;
			} else 
			if(xmlPath=="SIB18YOB"){
				return this.Sib18yob ;
			} else 
			if(xmlPath=="SIB18LIV"){
				return this.Sib18liv ;
			} else 
			if(xmlPath=="SIB18YOD"){
				return this.Sib18yod ;
			} else 
			if(xmlPath=="SIB18DEM"){
				return this.Sib18dem ;
			} else 
			if(xmlPath=="SIB18ONS"){
				return this.Sib18ons ;
			} else 
			if(xmlPath=="SIB18AUTO"){
				return this.Sib18auto ;
			} else 
			if(xmlPath=="SIB18SEX"){
				return this.Sib18sex ;
			} else 
			if(xmlPath=="SIB19ID"){
				return this.Sib19id ;
			} else 
			if(xmlPath=="SIB19YOB"){
				return this.Sib19yob ;
			} else 
			if(xmlPath=="SIB19LIV"){
				return this.Sib19liv ;
			} else 
			if(xmlPath=="SIB19YOD"){
				return this.Sib19yod ;
			} else 
			if(xmlPath=="SIB19DEM"){
				return this.Sib19dem ;
			} else 
			if(xmlPath=="SIB19ONS"){
				return this.Sib19ons ;
			} else 
			if(xmlPath=="SIB19AUTO"){
				return this.Sib19auto ;
			} else 
			if(xmlPath=="SIB19SEX"){
				return this.Sib19sex ;
			} else 
			if(xmlPath=="SIB20ID"){
				return this.Sib20id ;
			} else 
			if(xmlPath=="SIB20YOB"){
				return this.Sib20yob ;
			} else 
			if(xmlPath=="SIB20LIV"){
				return this.Sib20liv ;
			} else 
			if(xmlPath=="SIB20YOD"){
				return this.Sib20yod ;
			} else 
			if(xmlPath=="SIB20DEM"){
				return this.Sib20dem ;
			} else 
			if(xmlPath=="SIB20ONS"){
				return this.Sib20ons ;
			} else 
			if(xmlPath=="SIB20AUTO"){
				return this.Sib20auto ;
			} else 
			if(xmlPath=="SIB20SEX"){
				return this.Sib20sex ;
			} else 
			if(xmlPath=="KIDCHG"){
				return this.Kidchg ;
			} else 
			if(xmlPath=="KIDS"){
				return this.Kids ;
			} else 
			if(xmlPath=="KID1ID"){
				return this.Kid1id ;
			} else 
			if(xmlPath=="KID1YOB"){
				return this.Kid1yob ;
			} else 
			if(xmlPath=="KID1LIV"){
				return this.Kid1liv ;
			} else 
			if(xmlPath=="KID1YOD"){
				return this.Kid1yod ;
			} else 
			if(xmlPath=="KID1DEM"){
				return this.Kid1dem ;
			} else 
			if(xmlPath=="KID1ONS"){
				return this.Kid1ons ;
			} else 
			if(xmlPath=="KID1AUTO"){
				return this.Kid1auto ;
			} else 
			if(xmlPath=="KID1SEX"){
				return this.Kid1sex ;
			} else 
			if(xmlPath=="KID2ID"){
				return this.Kid2id ;
			} else 
			if(xmlPath=="KID2YOB"){
				return this.Kid2yob ;
			} else 
			if(xmlPath=="KID2LIV"){
				return this.Kid2liv ;
			} else 
			if(xmlPath=="KID2YOD"){
				return this.Kid2yod ;
			} else 
			if(xmlPath=="KID2DEM"){
				return this.Kid2dem ;
			} else 
			if(xmlPath=="KID2ONS"){
				return this.Kid2ons ;
			} else 
			if(xmlPath=="KID2AUTO"){
				return this.Kid2auto ;
			} else 
			if(xmlPath=="KID2SEX"){
				return this.Kid2sex ;
			} else 
			if(xmlPath=="KID3ID"){
				return this.Kid3id ;
			} else 
			if(xmlPath=="KID3YOB"){
				return this.Kid3yob ;
			} else 
			if(xmlPath=="KID3LIV"){
				return this.Kid3liv ;
			} else 
			if(xmlPath=="KID3YOD"){
				return this.Kid3yod ;
			} else 
			if(xmlPath=="KID3DEM"){
				return this.Kid3dem ;
			} else 
			if(xmlPath=="KID3ONS"){
				return this.Kid3ons ;
			} else 
			if(xmlPath=="KID3AUTO"){
				return this.Kid3auto ;
			} else 
			if(xmlPath=="KID3SEX"){
				return this.Kid3sex ;
			} else 
			if(xmlPath=="KID4ID"){
				return this.Kid4id ;
			} else 
			if(xmlPath=="KID4YOB"){
				return this.Kid4yob ;
			} else 
			if(xmlPath=="KID4LIV"){
				return this.Kid4liv ;
			} else 
			if(xmlPath=="KID4YOD"){
				return this.Kid4yod ;
			} else 
			if(xmlPath=="KID4DEM"){
				return this.Kid4dem ;
			} else 
			if(xmlPath=="KID4ONS"){
				return this.Kid4ons ;
			} else 
			if(xmlPath=="KID4AUTO"){
				return this.Kid4auto ;
			} else 
			if(xmlPath=="KID4SEX"){
				return this.Kid4sex ;
			} else 
			if(xmlPath=="KID5ID"){
				return this.Kid5id ;
			} else 
			if(xmlPath=="KID5YOB"){
				return this.Kid5yob ;
			} else 
			if(xmlPath=="KID5LIV"){
				return this.Kid5liv ;
			} else 
			if(xmlPath=="KID5YOD"){
				return this.Kid5yod ;
			} else 
			if(xmlPath=="KID5DEM"){
				return this.Kid5dem ;
			} else 
			if(xmlPath=="KID5ONS"){
				return this.Kid5ons ;
			} else 
			if(xmlPath=="KID5AUTO"){
				return this.Kid5auto ;
			} else 
			if(xmlPath=="KID5SEX"){
				return this.Kid5sex ;
			} else 
			if(xmlPath=="KID6ID"){
				return this.Kid6id ;
			} else 
			if(xmlPath=="KID6YOB"){
				return this.Kid6yob ;
			} else 
			if(xmlPath=="KID6LIV"){
				return this.Kid6liv ;
			} else 
			if(xmlPath=="KID6YOD"){
				return this.Kid6yod ;
			} else 
			if(xmlPath=="KID6DEM"){
				return this.Kid6dem ;
			} else 
			if(xmlPath=="KID6ONS"){
				return this.Kid6ons ;
			} else 
			if(xmlPath=="KID6AUTO"){
				return this.Kid6auto ;
			} else 
			if(xmlPath=="KID6SEX"){
				return this.Kid6sex ;
			} else 
			if(xmlPath=="KID7ID"){
				return this.Kid7id ;
			} else 
			if(xmlPath=="KID7YOB"){
				return this.Kid7yob ;
			} else 
			if(xmlPath=="KID7LIV"){
				return this.Kid7liv ;
			} else 
			if(xmlPath=="KID7YOD"){
				return this.Kid7yod ;
			} else 
			if(xmlPath=="KID7DEM"){
				return this.Kid7dem ;
			} else 
			if(xmlPath=="KID7ONS"){
				return this.Kid7ons ;
			} else 
			if(xmlPath=="KID7AUTO"){
				return this.Kid7auto ;
			} else 
			if(xmlPath=="KID7SEX"){
				return this.Kid7sex ;
			} else 
			if(xmlPath=="KID8ID"){
				return this.Kid8id ;
			} else 
			if(xmlPath=="KID8YOB"){
				return this.Kid8yob ;
			} else 
			if(xmlPath=="KID8LIV"){
				return this.Kid8liv ;
			} else 
			if(xmlPath=="KID8YOD"){
				return this.Kid8yod ;
			} else 
			if(xmlPath=="KID8DEM"){
				return this.Kid8dem ;
			} else 
			if(xmlPath=="KID8ONS"){
				return this.Kid8ons ;
			} else 
			if(xmlPath=="KID8AUTO"){
				return this.Kid8auto ;
			} else 
			if(xmlPath=="KID8SEX"){
				return this.Kid8sex ;
			} else 
			if(xmlPath=="KID9ID"){
				return this.Kid9id ;
			} else 
			if(xmlPath=="KID9YOB"){
				return this.Kid9yob ;
			} else 
			if(xmlPath=="KID9LIV"){
				return this.Kid9liv ;
			} else 
			if(xmlPath=="KID9YOD"){
				return this.Kid9yod ;
			} else 
			if(xmlPath=="KID9DEM"){
				return this.Kid9dem ;
			} else 
			if(xmlPath=="KID9ONS"){
				return this.Kid9ons ;
			} else 
			if(xmlPath=="KID9AUTO"){
				return this.Kid9auto ;
			} else 
			if(xmlPath=="KID9SEX"){
				return this.Kid9sex ;
			} else 
			if(xmlPath=="KID10ID"){
				return this.Kid10id ;
			} else 
			if(xmlPath=="KID10YOB"){
				return this.Kid10yob ;
			} else 
			if(xmlPath=="KID10LIV"){
				return this.Kid10liv ;
			} else 
			if(xmlPath=="KID10YOD"){
				return this.Kid10yod ;
			} else 
			if(xmlPath=="KID10DEM"){
				return this.Kid10dem ;
			} else 
			if(xmlPath=="KID10ONS"){
				return this.Kid10ons ;
			} else 
			if(xmlPath=="KID10AUTO"){
				return this.Kid10auto ;
			} else 
			if(xmlPath=="KID10SEX"){
				return this.Kid10sex ;
			} else 
			if(xmlPath=="KID11ID"){
				return this.Kid11id ;
			} else 
			if(xmlPath=="KID11YOB"){
				return this.Kid11yob ;
			} else 
			if(xmlPath=="KID11LIV"){
				return this.Kid11liv ;
			} else 
			if(xmlPath=="KID11YOD"){
				return this.Kid11yod ;
			} else 
			if(xmlPath=="KID11DEM"){
				return this.Kid11dem ;
			} else 
			if(xmlPath=="KID11ONS"){
				return this.Kid11ons ;
			} else 
			if(xmlPath=="KID11AUTO"){
				return this.Kid11auto ;
			} else 
			if(xmlPath=="KID11SEX"){
				return this.Kid11sex ;
			} else 
			if(xmlPath=="KID12ID"){
				return this.Kid12id ;
			} else 
			if(xmlPath=="KID12YOB"){
				return this.Kid12yob ;
			} else 
			if(xmlPath=="KID12LIV"){
				return this.Kid12liv ;
			} else 
			if(xmlPath=="KID12YOD"){
				return this.Kid12yod ;
			} else 
			if(xmlPath=="KID12DEM"){
				return this.Kid12dem ;
			} else 
			if(xmlPath=="KID12ONS"){
				return this.Kid12ons ;
			} else 
			if(xmlPath=="KID12AUTO"){
				return this.Kid12auto ;
			} else 
			if(xmlPath=="KID12SEX"){
				return this.Kid12sex ;
			} else 
			if(xmlPath=="KID13ID"){
				return this.Kid13id ;
			} else 
			if(xmlPath=="KID13YOB"){
				return this.Kid13yob ;
			} else 
			if(xmlPath=="KID13LIV"){
				return this.Kid13liv ;
			} else 
			if(xmlPath=="KID13YOD"){
				return this.Kid13yod ;
			} else 
			if(xmlPath=="KID13DEM"){
				return this.Kid13dem ;
			} else 
			if(xmlPath=="KID13ONS"){
				return this.Kid13ons ;
			} else 
			if(xmlPath=="KID13AUTO"){
				return this.Kid13auto ;
			} else 
			if(xmlPath=="KID13SEX"){
				return this.Kid13sex ;
			} else 
			if(xmlPath=="KID14ID"){
				return this.Kid14id ;
			} else 
			if(xmlPath=="KID14YOB"){
				return this.Kid14yob ;
			} else 
			if(xmlPath=="KID14LIV"){
				return this.Kid14liv ;
			} else 
			if(xmlPath=="KID14YOD"){
				return this.Kid14yod ;
			} else 
			if(xmlPath=="KID14DEM"){
				return this.Kid14dem ;
			} else 
			if(xmlPath=="KID14ONS"){
				return this.Kid14ons ;
			} else 
			if(xmlPath=="KID14AUTO"){
				return this.Kid14auto ;
			} else 
			if(xmlPath=="KID14SEX"){
				return this.Kid14sex ;
			} else 
			if(xmlPath=="KID15ID"){
				return this.Kid15id ;
			} else 
			if(xmlPath=="KID15YOB"){
				return this.Kid15yob ;
			} else 
			if(xmlPath=="KID15LIV"){
				return this.Kid15liv ;
			} else 
			if(xmlPath=="KID15YOD"){
				return this.Kid15yod ;
			} else 
			if(xmlPath=="KID15DEM"){
				return this.Kid15dem ;
			} else 
			if(xmlPath=="KID15ONS"){
				return this.Kid15ons ;
			} else 
			if(xmlPath=="KID15AUTO"){
				return this.Kid15auto ;
			} else 
			if(xmlPath=="KID15SEX"){
				return this.Kid15sex ;
			} else 
			if(xmlPath=="RELCHG"){
				return this.Relchg ;
			} else 
			if(xmlPath=="RELSDEM"){
				return this.Relsdem ;
			} else 
			if(xmlPath=="REL1ID"){
				return this.Rel1id ;
			} else 
			if(xmlPath=="REL1YOB"){
				return this.Rel1yob ;
			} else 
			if(xmlPath=="REL1LIV"){
				return this.Rel1liv ;
			} else 
			if(xmlPath=="REL1YOD"){
				return this.Rel1yod ;
			} else 
			if(xmlPath=="REL1ONS"){
				return this.Rel1ons ;
			} else 
			if(xmlPath=="REL2ID"){
				return this.Rel2id ;
			} else 
			if(xmlPath=="REL2YOB"){
				return this.Rel2yob ;
			} else 
			if(xmlPath=="REL2LIV"){
				return this.Rel2liv ;
			} else 
			if(xmlPath=="REL2YOD"){
				return this.Rel2yod ;
			} else 
			if(xmlPath=="REL2ONS"){
				return this.Rel2ons ;
			} else 
			if(xmlPath=="REL3ID"){
				return this.Rel3id ;
			} else 
			if(xmlPath=="REL3YOB"){
				return this.Rel3yob ;
			} else 
			if(xmlPath=="REL3LIV"){
				return this.Rel3liv ;
			} else 
			if(xmlPath=="REL3YOD"){
				return this.Rel3yod ;
			} else 
			if(xmlPath=="REL3ONS"){
				return this.Rel3ons ;
			} else 
			if(xmlPath=="REL4ID"){
				return this.Rel4id ;
			} else 
			if(xmlPath=="REL4YOB"){
				return this.Rel4yob ;
			} else 
			if(xmlPath=="REL4LIV"){
				return this.Rel4liv ;
			} else 
			if(xmlPath=="REL4YOD"){
				return this.Rel4yod ;
			} else 
			if(xmlPath=="REL4ONS"){
				return this.Rel4ons ;
			} else 
			if(xmlPath=="REL5ID"){
				return this.Rel5id ;
			} else 
			if(xmlPath=="REL5YOB"){
				return this.Rel5yob ;
			} else 
			if(xmlPath=="REL5LIV"){
				return this.Rel5liv ;
			} else 
			if(xmlPath=="REL5YOD"){
				return this.Rel5yod ;
			} else 
			if(xmlPath=="REL5ONS"){
				return this.Rel5ons ;
			} else 
			if(xmlPath=="REL6ID"){
				return this.Rel6id ;
			} else 
			if(xmlPath=="REL6YOB"){
				return this.Rel6yob ;
			} else 
			if(xmlPath=="REL6LIV"){
				return this.Rel6liv ;
			} else 
			if(xmlPath=="REL6YOD"){
				return this.Rel6yod ;
			} else 
			if(xmlPath=="REL6ONS"){
				return this.Rel6ons ;
			} else 
			if(xmlPath=="REL7ID"){
				return this.Rel7id ;
			} else 
			if(xmlPath=="REL7YOB"){
				return this.Rel7yob ;
			} else 
			if(xmlPath=="REL7LIV"){
				return this.Rel7liv ;
			} else 
			if(xmlPath=="REL7YOD"){
				return this.Rel7yod ;
			} else 
			if(xmlPath=="REL7ONS"){
				return this.Rel7ons ;
			} else 
			if(xmlPath=="REL8ID"){
				return this.Rel8id ;
			} else 
			if(xmlPath=="REL8YOB"){
				return this.Rel8yob ;
			} else 
			if(xmlPath=="REL8LIV"){
				return this.Rel8liv ;
			} else 
			if(xmlPath=="REL8YOD"){
				return this.Rel8yod ;
			} else 
			if(xmlPath=="REL8ONS"){
				return this.Rel8ons ;
			} else 
			if(xmlPath=="REL9ID"){
				return this.Rel9id ;
			} else 
			if(xmlPath=="REL9YOB"){
				return this.Rel9yob ;
			} else 
			if(xmlPath=="REL9LIV"){
				return this.Rel9liv ;
			} else 
			if(xmlPath=="REL9YOD"){
				return this.Rel9yod ;
			} else 
			if(xmlPath=="REL9ONS"){
				return this.Rel9ons ;
			} else 
			if(xmlPath=="REL10ID"){
				return this.Rel10id ;
			} else 
			if(xmlPath=="REL10YOB"){
				return this.Rel10yob ;
			} else 
			if(xmlPath=="REL10LIV"){
				return this.Rel10liv ;
			} else 
			if(xmlPath=="REL10YOD"){
				return this.Rel10yod ;
			} else 
			if(xmlPath=="REL10ONS"){
				return this.Rel10ons ;
			} else 
			if(xmlPath=="REL11ID"){
				return this.Rel11id ;
			} else 
			if(xmlPath=="REL11YOB"){
				return this.Rel11yob ;
			} else 
			if(xmlPath=="REL11LIV"){
				return this.Rel11liv ;
			} else 
			if(xmlPath=="REL11YOD"){
				return this.Rel11yod ;
			} else 
			if(xmlPath=="REL11ONS"){
				return this.Rel11ons ;
			} else 
			if(xmlPath=="REL12ID"){
				return this.Rel12id ;
			} else 
			if(xmlPath=="REL12YOB"){
				return this.Rel12yob ;
			} else 
			if(xmlPath=="REL12LIV"){
				return this.Rel12liv ;
			} else 
			if(xmlPath=="REL12YOD"){
				return this.Rel12yod ;
			} else 
			if(xmlPath=="REL12ONS"){
				return this.Rel12ons ;
			} else 
			if(xmlPath=="REL13ID"){
				return this.Rel13id ;
			} else 
			if(xmlPath=="REL13YOB"){
				return this.Rel13yob ;
			} else 
			if(xmlPath=="REL13LIV"){
				return this.Rel13liv ;
			} else 
			if(xmlPath=="REL13YOD"){
				return this.Rel13yod ;
			} else 
			if(xmlPath=="REL13ONS"){
				return this.Rel13ons ;
			} else 
			if(xmlPath=="REL14ID"){
				return this.Rel14id ;
			} else 
			if(xmlPath=="REL14YOB"){
				return this.Rel14yob ;
			} else 
			if(xmlPath=="REL14LIV"){
				return this.Rel14liv ;
			} else 
			if(xmlPath=="REL14YOD"){
				return this.Rel14yod ;
			} else 
			if(xmlPath=="REL14ONS"){
				return this.Rel14ons ;
			} else 
			if(xmlPath=="REL15ID"){
				return this.Rel15id ;
			} else 
			if(xmlPath=="REL15YOB"){
				return this.Rel15yob ;
			} else 
			if(xmlPath=="REL15LIV"){
				return this.Rel15liv ;
			} else 
			if(xmlPath=="REL15YOD"){
				return this.Rel15yod ;
			} else 
			if(xmlPath=="REL15ONS"){
				return this.Rel15ons ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="EXAMDATE"){
				this.Examdate=value;
			} else 
			if(xmlPath=="A3CHG"){
				this.A3chg=value;
			} else 
			if(xmlPath=="PARCHG"){
				this.Parchg=value;
			} else 
			if(xmlPath=="FAMILYID"){
				this.Familyid=value;
			} else 
			if(xmlPath=="GMOMID"){
				this.Gmomid=value;
			} else 
			if(xmlPath=="GMOMYOB"){
				this.Gmomyob=value;
			} else 
			if(xmlPath=="GMOMLIV"){
				this.Gmomliv=value;
			} else 
			if(xmlPath=="GMOMYOD"){
				this.Gmomyod=value;
			} else 
			if(xmlPath=="GMOMDEM"){
				this.Gmomdem=value;
			} else 
			if(xmlPath=="GMOMONSET"){
				this.Gmomonset=value;
			} else 
			if(xmlPath=="GMOMAUTO"){
				this.Gmomauto=value;
			} else 
			if(xmlPath=="GDADID"){
				this.Gdadid=value;
			} else 
			if(xmlPath=="GDADYOB"){
				this.Gdadyob=value;
			} else 
			if(xmlPath=="GDADLIV"){
				this.Gdadliv=value;
			} else 
			if(xmlPath=="GDADYOD"){
				this.Gdadyod=value;
			} else 
			if(xmlPath=="GDADDEM"){
				this.Gdaddem=value;
			} else 
			if(xmlPath=="GDADONSET"){
				this.Gdadonset=value;
			} else 
			if(xmlPath=="GDADAUTO"){
				this.Gdadauto=value;
			} else 
			if(xmlPath=="MOMID"){
				this.Momid=value;
			} else 
			if(xmlPath=="MOMYOB"){
				this.Momyob=value;
			} else 
			if(xmlPath=="MOMLIV"){
				this.Momliv=value;
			} else 
			if(xmlPath=="MOMYOD"){
				this.Momyod=value;
			} else 
			if(xmlPath=="MOMDEM"){
				this.Momdem=value;
			} else 
			if(xmlPath=="MOMONSET"){
				this.Momonset=value;
			} else 
			if(xmlPath=="MOMAUTO"){
				this.Momauto=value;
			} else 
			if(xmlPath=="DADID"){
				this.Dadid=value;
			} else 
			if(xmlPath=="DADYOB"){
				this.Dadyob=value;
			} else 
			if(xmlPath=="DADLIV"){
				this.Dadliv=value;
			} else 
			if(xmlPath=="DADYOD"){
				this.Dadyod=value;
			} else 
			if(xmlPath=="DADDEM"){
				this.Daddem=value;
			} else 
			if(xmlPath=="DADONSET"){
				this.Dadonset=value;
			} else 
			if(xmlPath=="DADAUTO"){
				this.Dadauto=value;
			} else 
			if(xmlPath=="SIBCHG"){
				this.Sibchg=value;
			} else 
			if(xmlPath=="TWIN"){
				this.Twin=value;
			} else 
			if(xmlPath=="TWINTYPE"){
				this.Twintype=value;
			} else 
			if(xmlPath=="SIBS"){
				this.Sibs=value;
			} else 
			if(xmlPath=="SIB1ID"){
				this.Sib1id=value;
			} else 
			if(xmlPath=="SIB1YOB"){
				this.Sib1yob=value;
			} else 
			if(xmlPath=="SIB1LIV"){
				this.Sib1liv=value;
			} else 
			if(xmlPath=="SIB1YOD"){
				this.Sib1yod=value;
			} else 
			if(xmlPath=="SIB1DEM"){
				this.Sib1dem=value;
			} else 
			if(xmlPath=="SIB1ONS"){
				this.Sib1ons=value;
			} else 
			if(xmlPath=="SIB1AUTO"){
				this.Sib1auto=value;
			} else 
			if(xmlPath=="SIB1SEX"){
				this.Sib1sex=value;
			} else 
			if(xmlPath=="SIB2ID"){
				this.Sib2id=value;
			} else 
			if(xmlPath=="SIB2YOB"){
				this.Sib2yob=value;
			} else 
			if(xmlPath=="SIB2LIV"){
				this.Sib2liv=value;
			} else 
			if(xmlPath=="SIB2YOD"){
				this.Sib2yod=value;
			} else 
			if(xmlPath=="SIB2DEM"){
				this.Sib2dem=value;
			} else 
			if(xmlPath=="SIB2ONS"){
				this.Sib2ons=value;
			} else 
			if(xmlPath=="SIB2AUTO"){
				this.Sib2auto=value;
			} else 
			if(xmlPath=="SIB2SEX"){
				this.Sib2sex=value;
			} else 
			if(xmlPath=="SIB3ID"){
				this.Sib3id=value;
			} else 
			if(xmlPath=="SIB3YOB"){
				this.Sib3yob=value;
			} else 
			if(xmlPath=="SIB3LIV"){
				this.Sib3liv=value;
			} else 
			if(xmlPath=="SIB3YOD"){
				this.Sib3yod=value;
			} else 
			if(xmlPath=="SIB3DEM"){
				this.Sib3dem=value;
			} else 
			if(xmlPath=="SIB3ONS"){
				this.Sib3ons=value;
			} else 
			if(xmlPath=="SIB3AUTO"){
				this.Sib3auto=value;
			} else 
			if(xmlPath=="SIB3SEX"){
				this.Sib3sex=value;
			} else 
			if(xmlPath=="SIB4ID"){
				this.Sib4id=value;
			} else 
			if(xmlPath=="SIB4YOB"){
				this.Sib4yob=value;
			} else 
			if(xmlPath=="SIB4LIV"){
				this.Sib4liv=value;
			} else 
			if(xmlPath=="SIB4YOD"){
				this.Sib4yod=value;
			} else 
			if(xmlPath=="SIB4DEM"){
				this.Sib4dem=value;
			} else 
			if(xmlPath=="SIB4ONS"){
				this.Sib4ons=value;
			} else 
			if(xmlPath=="SIB4AUTO"){
				this.Sib4auto=value;
			} else 
			if(xmlPath=="SIB4SEX"){
				this.Sib4sex=value;
			} else 
			if(xmlPath=="SIB5ID"){
				this.Sib5id=value;
			} else 
			if(xmlPath=="SIB5YOB"){
				this.Sib5yob=value;
			} else 
			if(xmlPath=="SIB5LIV"){
				this.Sib5liv=value;
			} else 
			if(xmlPath=="SIB5YOD"){
				this.Sib5yod=value;
			} else 
			if(xmlPath=="SIB5DEM"){
				this.Sib5dem=value;
			} else 
			if(xmlPath=="SIB5ONS"){
				this.Sib5ons=value;
			} else 
			if(xmlPath=="SIB5AUTO"){
				this.Sib5auto=value;
			} else 
			if(xmlPath=="SIB5SEX"){
				this.Sib5sex=value;
			} else 
			if(xmlPath=="SIB6ID"){
				this.Sib6id=value;
			} else 
			if(xmlPath=="SIB6YOB"){
				this.Sib6yob=value;
			} else 
			if(xmlPath=="SIB6LIV"){
				this.Sib6liv=value;
			} else 
			if(xmlPath=="SIB6YOD"){
				this.Sib6yod=value;
			} else 
			if(xmlPath=="SIB6DEM"){
				this.Sib6dem=value;
			} else 
			if(xmlPath=="SIB6ONS"){
				this.Sib6ons=value;
			} else 
			if(xmlPath=="SIB6AUTO"){
				this.Sib6auto=value;
			} else 
			if(xmlPath=="SIB6SEX"){
				this.Sib6sex=value;
			} else 
			if(xmlPath=="SIB7ID"){
				this.Sib7id=value;
			} else 
			if(xmlPath=="SIB7YOB"){
				this.Sib7yob=value;
			} else 
			if(xmlPath=="SIB7LIV"){
				this.Sib7liv=value;
			} else 
			if(xmlPath=="SIB7YOD"){
				this.Sib7yod=value;
			} else 
			if(xmlPath=="SIB7DEM"){
				this.Sib7dem=value;
			} else 
			if(xmlPath=="SIB7ONS"){
				this.Sib7ons=value;
			} else 
			if(xmlPath=="SIB7AUTO"){
				this.Sib7auto=value;
			} else 
			if(xmlPath=="SIB7SEX"){
				this.Sib7sex=value;
			} else 
			if(xmlPath=="SIB8ID"){
				this.Sib8id=value;
			} else 
			if(xmlPath=="SIB8YOB"){
				this.Sib8yob=value;
			} else 
			if(xmlPath=="SIB8LIV"){
				this.Sib8liv=value;
			} else 
			if(xmlPath=="SIB8YOD"){
				this.Sib8yod=value;
			} else 
			if(xmlPath=="SIB8DEM"){
				this.Sib8dem=value;
			} else 
			if(xmlPath=="SIB8ONS"){
				this.Sib8ons=value;
			} else 
			if(xmlPath=="SIB8AUTO"){
				this.Sib8auto=value;
			} else 
			if(xmlPath=="SIB8SEX"){
				this.Sib8sex=value;
			} else 
			if(xmlPath=="SIB9ID"){
				this.Sib9id=value;
			} else 
			if(xmlPath=="SIB9YOB"){
				this.Sib9yob=value;
			} else 
			if(xmlPath=="SIB9LIV"){
				this.Sib9liv=value;
			} else 
			if(xmlPath=="SIB9YOD"){
				this.Sib9yod=value;
			} else 
			if(xmlPath=="SIB9DEM"){
				this.Sib9dem=value;
			} else 
			if(xmlPath=="SIB9ONS"){
				this.Sib9ons=value;
			} else 
			if(xmlPath=="SIB9AUTO"){
				this.Sib9auto=value;
			} else 
			if(xmlPath=="SIB9SEX"){
				this.Sib9sex=value;
			} else 
			if(xmlPath=="SIB10ID"){
				this.Sib10id=value;
			} else 
			if(xmlPath=="SIB10YOB"){
				this.Sib10yob=value;
			} else 
			if(xmlPath=="SIB10LIV"){
				this.Sib10liv=value;
			} else 
			if(xmlPath=="SIB10YOD"){
				this.Sib10yod=value;
			} else 
			if(xmlPath=="SIB10DEM"){
				this.Sib10dem=value;
			} else 
			if(xmlPath=="SIB10ONS"){
				this.Sib10ons=value;
			} else 
			if(xmlPath=="SIB10AUTO"){
				this.Sib10auto=value;
			} else 
			if(xmlPath=="SIB10SEX"){
				this.Sib10sex=value;
			} else 
			if(xmlPath=="SIB11ID"){
				this.Sib11id=value;
			} else 
			if(xmlPath=="SIB11YOB"){
				this.Sib11yob=value;
			} else 
			if(xmlPath=="SIB11LIV"){
				this.Sib11liv=value;
			} else 
			if(xmlPath=="SIB11YOD"){
				this.Sib11yod=value;
			} else 
			if(xmlPath=="SIB11DEM"){
				this.Sib11dem=value;
			} else 
			if(xmlPath=="SIB11ONS"){
				this.Sib11ons=value;
			} else 
			if(xmlPath=="SIB11AUTO"){
				this.Sib11auto=value;
			} else 
			if(xmlPath=="SIB11SEX"){
				this.Sib11sex=value;
			} else 
			if(xmlPath=="SIB12ID"){
				this.Sib12id=value;
			} else 
			if(xmlPath=="SIB12YOB"){
				this.Sib12yob=value;
			} else 
			if(xmlPath=="SIB12LIV"){
				this.Sib12liv=value;
			} else 
			if(xmlPath=="SIB12YOD"){
				this.Sib12yod=value;
			} else 
			if(xmlPath=="SIB12DEM"){
				this.Sib12dem=value;
			} else 
			if(xmlPath=="SIB12ONS"){
				this.Sib12ons=value;
			} else 
			if(xmlPath=="SIB12AUTO"){
				this.Sib12auto=value;
			} else 
			if(xmlPath=="SIB12SEX"){
				this.Sib12sex=value;
			} else 
			if(xmlPath=="SIB13ID"){
				this.Sib13id=value;
			} else 
			if(xmlPath=="SIB13YOB"){
				this.Sib13yob=value;
			} else 
			if(xmlPath=="SIB13LIV"){
				this.Sib13liv=value;
			} else 
			if(xmlPath=="SIB13YOD"){
				this.Sib13yod=value;
			} else 
			if(xmlPath=="SIB13DEM"){
				this.Sib13dem=value;
			} else 
			if(xmlPath=="SIB13ONS"){
				this.Sib13ons=value;
			} else 
			if(xmlPath=="SIB13AUTO"){
				this.Sib13auto=value;
			} else 
			if(xmlPath=="SIB13SEX"){
				this.Sib13sex=value;
			} else 
			if(xmlPath=="SIB14ID"){
				this.Sib14id=value;
			} else 
			if(xmlPath=="SIB14YOB"){
				this.Sib14yob=value;
			} else 
			if(xmlPath=="SIB14LIV"){
				this.Sib14liv=value;
			} else 
			if(xmlPath=="SIB14YOD"){
				this.Sib14yod=value;
			} else 
			if(xmlPath=="SIB14DEM"){
				this.Sib14dem=value;
			} else 
			if(xmlPath=="SIB14ONS"){
				this.Sib14ons=value;
			} else 
			if(xmlPath=="SIB14AUTO"){
				this.Sib14auto=value;
			} else 
			if(xmlPath=="SIB14SEX"){
				this.Sib14sex=value;
			} else 
			if(xmlPath=="SIB15ID"){
				this.Sib15id=value;
			} else 
			if(xmlPath=="SIB15YOB"){
				this.Sib15yob=value;
			} else 
			if(xmlPath=="SIB15LIV"){
				this.Sib15liv=value;
			} else 
			if(xmlPath=="SIB15YOD"){
				this.Sib15yod=value;
			} else 
			if(xmlPath=="SIB15DEM"){
				this.Sib15dem=value;
			} else 
			if(xmlPath=="SIB15ONS"){
				this.Sib15ons=value;
			} else 
			if(xmlPath=="SIB155AUTO"){
				this.Sib155auto=value;
			} else 
			if(xmlPath=="SIB155SEX"){
				this.Sib155sex=value;
			} else 
			if(xmlPath=="SIB16ID"){
				this.Sib16id=value;
			} else 
			if(xmlPath=="SIB16YOB"){
				this.Sib16yob=value;
			} else 
			if(xmlPath=="SIB16LIV"){
				this.Sib16liv=value;
			} else 
			if(xmlPath=="SIB16YOD"){
				this.Sib16yod=value;
			} else 
			if(xmlPath=="SIB16DEM"){
				this.Sib16dem=value;
			} else 
			if(xmlPath=="SIB16ONS"){
				this.Sib16ons=value;
			} else 
			if(xmlPath=="SIB16AUTO"){
				this.Sib16auto=value;
			} else 
			if(xmlPath=="SIB16SEX"){
				this.Sib16sex=value;
			} else 
			if(xmlPath=="SIB17ID"){
				this.Sib17id=value;
			} else 
			if(xmlPath=="SIB17YOB"){
				this.Sib17yob=value;
			} else 
			if(xmlPath=="SIB17LIV"){
				this.Sib17liv=value;
			} else 
			if(xmlPath=="SIB17YOD"){
				this.Sib17yod=value;
			} else 
			if(xmlPath=="SIB17DEM"){
				this.Sib17dem=value;
			} else 
			if(xmlPath=="SIB17ONS"){
				this.Sib17ons=value;
			} else 
			if(xmlPath=="SIB17AUTO"){
				this.Sib17auto=value;
			} else 
			if(xmlPath=="SIB17SEX"){
				this.Sib17sex=value;
			} else 
			if(xmlPath=="SIB18ID"){
				this.Sib18id=value;
			} else 
			if(xmlPath=="SIB18YOB"){
				this.Sib18yob=value;
			} else 
			if(xmlPath=="SIB18LIV"){
				this.Sib18liv=value;
			} else 
			if(xmlPath=="SIB18YOD"){
				this.Sib18yod=value;
			} else 
			if(xmlPath=="SIB18DEM"){
				this.Sib18dem=value;
			} else 
			if(xmlPath=="SIB18ONS"){
				this.Sib18ons=value;
			} else 
			if(xmlPath=="SIB18AUTO"){
				this.Sib18auto=value;
			} else 
			if(xmlPath=="SIB18SEX"){
				this.Sib18sex=value;
			} else 
			if(xmlPath=="SIB19ID"){
				this.Sib19id=value;
			} else 
			if(xmlPath=="SIB19YOB"){
				this.Sib19yob=value;
			} else 
			if(xmlPath=="SIB19LIV"){
				this.Sib19liv=value;
			} else 
			if(xmlPath=="SIB19YOD"){
				this.Sib19yod=value;
			} else 
			if(xmlPath=="SIB19DEM"){
				this.Sib19dem=value;
			} else 
			if(xmlPath=="SIB19ONS"){
				this.Sib19ons=value;
			} else 
			if(xmlPath=="SIB19AUTO"){
				this.Sib19auto=value;
			} else 
			if(xmlPath=="SIB19SEX"){
				this.Sib19sex=value;
			} else 
			if(xmlPath=="SIB20ID"){
				this.Sib20id=value;
			} else 
			if(xmlPath=="SIB20YOB"){
				this.Sib20yob=value;
			} else 
			if(xmlPath=="SIB20LIV"){
				this.Sib20liv=value;
			} else 
			if(xmlPath=="SIB20YOD"){
				this.Sib20yod=value;
			} else 
			if(xmlPath=="SIB20DEM"){
				this.Sib20dem=value;
			} else 
			if(xmlPath=="SIB20ONS"){
				this.Sib20ons=value;
			} else 
			if(xmlPath=="SIB20AUTO"){
				this.Sib20auto=value;
			} else 
			if(xmlPath=="SIB20SEX"){
				this.Sib20sex=value;
			} else 
			if(xmlPath=="KIDCHG"){
				this.Kidchg=value;
			} else 
			if(xmlPath=="KIDS"){
				this.Kids=value;
			} else 
			if(xmlPath=="KID1ID"){
				this.Kid1id=value;
			} else 
			if(xmlPath=="KID1YOB"){
				this.Kid1yob=value;
			} else 
			if(xmlPath=="KID1LIV"){
				this.Kid1liv=value;
			} else 
			if(xmlPath=="KID1YOD"){
				this.Kid1yod=value;
			} else 
			if(xmlPath=="KID1DEM"){
				this.Kid1dem=value;
			} else 
			if(xmlPath=="KID1ONS"){
				this.Kid1ons=value;
			} else 
			if(xmlPath=="KID1AUTO"){
				this.Kid1auto=value;
			} else 
			if(xmlPath=="KID1SEX"){
				this.Kid1sex=value;
			} else 
			if(xmlPath=="KID2ID"){
				this.Kid2id=value;
			} else 
			if(xmlPath=="KID2YOB"){
				this.Kid2yob=value;
			} else 
			if(xmlPath=="KID2LIV"){
				this.Kid2liv=value;
			} else 
			if(xmlPath=="KID2YOD"){
				this.Kid2yod=value;
			} else 
			if(xmlPath=="KID2DEM"){
				this.Kid2dem=value;
			} else 
			if(xmlPath=="KID2ONS"){
				this.Kid2ons=value;
			} else 
			if(xmlPath=="KID2AUTO"){
				this.Kid2auto=value;
			} else 
			if(xmlPath=="KID2SEX"){
				this.Kid2sex=value;
			} else 
			if(xmlPath=="KID3ID"){
				this.Kid3id=value;
			} else 
			if(xmlPath=="KID3YOB"){
				this.Kid3yob=value;
			} else 
			if(xmlPath=="KID3LIV"){
				this.Kid3liv=value;
			} else 
			if(xmlPath=="KID3YOD"){
				this.Kid3yod=value;
			} else 
			if(xmlPath=="KID3DEM"){
				this.Kid3dem=value;
			} else 
			if(xmlPath=="KID3ONS"){
				this.Kid3ons=value;
			} else 
			if(xmlPath=="KID3AUTO"){
				this.Kid3auto=value;
			} else 
			if(xmlPath=="KID3SEX"){
				this.Kid3sex=value;
			} else 
			if(xmlPath=="KID4ID"){
				this.Kid4id=value;
			} else 
			if(xmlPath=="KID4YOB"){
				this.Kid4yob=value;
			} else 
			if(xmlPath=="KID4LIV"){
				this.Kid4liv=value;
			} else 
			if(xmlPath=="KID4YOD"){
				this.Kid4yod=value;
			} else 
			if(xmlPath=="KID4DEM"){
				this.Kid4dem=value;
			} else 
			if(xmlPath=="KID4ONS"){
				this.Kid4ons=value;
			} else 
			if(xmlPath=="KID4AUTO"){
				this.Kid4auto=value;
			} else 
			if(xmlPath=="KID4SEX"){
				this.Kid4sex=value;
			} else 
			if(xmlPath=="KID5ID"){
				this.Kid5id=value;
			} else 
			if(xmlPath=="KID5YOB"){
				this.Kid5yob=value;
			} else 
			if(xmlPath=="KID5LIV"){
				this.Kid5liv=value;
			} else 
			if(xmlPath=="KID5YOD"){
				this.Kid5yod=value;
			} else 
			if(xmlPath=="KID5DEM"){
				this.Kid5dem=value;
			} else 
			if(xmlPath=="KID5ONS"){
				this.Kid5ons=value;
			} else 
			if(xmlPath=="KID5AUTO"){
				this.Kid5auto=value;
			} else 
			if(xmlPath=="KID5SEX"){
				this.Kid5sex=value;
			} else 
			if(xmlPath=="KID6ID"){
				this.Kid6id=value;
			} else 
			if(xmlPath=="KID6YOB"){
				this.Kid6yob=value;
			} else 
			if(xmlPath=="KID6LIV"){
				this.Kid6liv=value;
			} else 
			if(xmlPath=="KID6YOD"){
				this.Kid6yod=value;
			} else 
			if(xmlPath=="KID6DEM"){
				this.Kid6dem=value;
			} else 
			if(xmlPath=="KID6ONS"){
				this.Kid6ons=value;
			} else 
			if(xmlPath=="KID6AUTO"){
				this.Kid6auto=value;
			} else 
			if(xmlPath=="KID6SEX"){
				this.Kid6sex=value;
			} else 
			if(xmlPath=="KID7ID"){
				this.Kid7id=value;
			} else 
			if(xmlPath=="KID7YOB"){
				this.Kid7yob=value;
			} else 
			if(xmlPath=="KID7LIV"){
				this.Kid7liv=value;
			} else 
			if(xmlPath=="KID7YOD"){
				this.Kid7yod=value;
			} else 
			if(xmlPath=="KID7DEM"){
				this.Kid7dem=value;
			} else 
			if(xmlPath=="KID7ONS"){
				this.Kid7ons=value;
			} else 
			if(xmlPath=="KID7AUTO"){
				this.Kid7auto=value;
			} else 
			if(xmlPath=="KID7SEX"){
				this.Kid7sex=value;
			} else 
			if(xmlPath=="KID8ID"){
				this.Kid8id=value;
			} else 
			if(xmlPath=="KID8YOB"){
				this.Kid8yob=value;
			} else 
			if(xmlPath=="KID8LIV"){
				this.Kid8liv=value;
			} else 
			if(xmlPath=="KID8YOD"){
				this.Kid8yod=value;
			} else 
			if(xmlPath=="KID8DEM"){
				this.Kid8dem=value;
			} else 
			if(xmlPath=="KID8ONS"){
				this.Kid8ons=value;
			} else 
			if(xmlPath=="KID8AUTO"){
				this.Kid8auto=value;
			} else 
			if(xmlPath=="KID8SEX"){
				this.Kid8sex=value;
			} else 
			if(xmlPath=="KID9ID"){
				this.Kid9id=value;
			} else 
			if(xmlPath=="KID9YOB"){
				this.Kid9yob=value;
			} else 
			if(xmlPath=="KID9LIV"){
				this.Kid9liv=value;
			} else 
			if(xmlPath=="KID9YOD"){
				this.Kid9yod=value;
			} else 
			if(xmlPath=="KID9DEM"){
				this.Kid9dem=value;
			} else 
			if(xmlPath=="KID9ONS"){
				this.Kid9ons=value;
			} else 
			if(xmlPath=="KID9AUTO"){
				this.Kid9auto=value;
			} else 
			if(xmlPath=="KID9SEX"){
				this.Kid9sex=value;
			} else 
			if(xmlPath=="KID10ID"){
				this.Kid10id=value;
			} else 
			if(xmlPath=="KID10YOB"){
				this.Kid10yob=value;
			} else 
			if(xmlPath=="KID10LIV"){
				this.Kid10liv=value;
			} else 
			if(xmlPath=="KID10YOD"){
				this.Kid10yod=value;
			} else 
			if(xmlPath=="KID10DEM"){
				this.Kid10dem=value;
			} else 
			if(xmlPath=="KID10ONS"){
				this.Kid10ons=value;
			} else 
			if(xmlPath=="KID10AUTO"){
				this.Kid10auto=value;
			} else 
			if(xmlPath=="KID10SEX"){
				this.Kid10sex=value;
			} else 
			if(xmlPath=="KID11ID"){
				this.Kid11id=value;
			} else 
			if(xmlPath=="KID11YOB"){
				this.Kid11yob=value;
			} else 
			if(xmlPath=="KID11LIV"){
				this.Kid11liv=value;
			} else 
			if(xmlPath=="KID11YOD"){
				this.Kid11yod=value;
			} else 
			if(xmlPath=="KID11DEM"){
				this.Kid11dem=value;
			} else 
			if(xmlPath=="KID11ONS"){
				this.Kid11ons=value;
			} else 
			if(xmlPath=="KID11AUTO"){
				this.Kid11auto=value;
			} else 
			if(xmlPath=="KID11SEX"){
				this.Kid11sex=value;
			} else 
			if(xmlPath=="KID12ID"){
				this.Kid12id=value;
			} else 
			if(xmlPath=="KID12YOB"){
				this.Kid12yob=value;
			} else 
			if(xmlPath=="KID12LIV"){
				this.Kid12liv=value;
			} else 
			if(xmlPath=="KID12YOD"){
				this.Kid12yod=value;
			} else 
			if(xmlPath=="KID12DEM"){
				this.Kid12dem=value;
			} else 
			if(xmlPath=="KID12ONS"){
				this.Kid12ons=value;
			} else 
			if(xmlPath=="KID12AUTO"){
				this.Kid12auto=value;
			} else 
			if(xmlPath=="KID12SEX"){
				this.Kid12sex=value;
			} else 
			if(xmlPath=="KID13ID"){
				this.Kid13id=value;
			} else 
			if(xmlPath=="KID13YOB"){
				this.Kid13yob=value;
			} else 
			if(xmlPath=="KID13LIV"){
				this.Kid13liv=value;
			} else 
			if(xmlPath=="KID13YOD"){
				this.Kid13yod=value;
			} else 
			if(xmlPath=="KID13DEM"){
				this.Kid13dem=value;
			} else 
			if(xmlPath=="KID13ONS"){
				this.Kid13ons=value;
			} else 
			if(xmlPath=="KID13AUTO"){
				this.Kid13auto=value;
			} else 
			if(xmlPath=="KID13SEX"){
				this.Kid13sex=value;
			} else 
			if(xmlPath=="KID14ID"){
				this.Kid14id=value;
			} else 
			if(xmlPath=="KID14YOB"){
				this.Kid14yob=value;
			} else 
			if(xmlPath=="KID14LIV"){
				this.Kid14liv=value;
			} else 
			if(xmlPath=="KID14YOD"){
				this.Kid14yod=value;
			} else 
			if(xmlPath=="KID14DEM"){
				this.Kid14dem=value;
			} else 
			if(xmlPath=="KID14ONS"){
				this.Kid14ons=value;
			} else 
			if(xmlPath=="KID14AUTO"){
				this.Kid14auto=value;
			} else 
			if(xmlPath=="KID14SEX"){
				this.Kid14sex=value;
			} else 
			if(xmlPath=="KID15ID"){
				this.Kid15id=value;
			} else 
			if(xmlPath=="KID15YOB"){
				this.Kid15yob=value;
			} else 
			if(xmlPath=="KID15LIV"){
				this.Kid15liv=value;
			} else 
			if(xmlPath=="KID15YOD"){
				this.Kid15yod=value;
			} else 
			if(xmlPath=="KID15DEM"){
				this.Kid15dem=value;
			} else 
			if(xmlPath=="KID15ONS"){
				this.Kid15ons=value;
			} else 
			if(xmlPath=="KID15AUTO"){
				this.Kid15auto=value;
			} else 
			if(xmlPath=="KID15SEX"){
				this.Kid15sex=value;
			} else 
			if(xmlPath=="RELCHG"){
				this.Relchg=value;
			} else 
			if(xmlPath=="RELSDEM"){
				this.Relsdem=value;
			} else 
			if(xmlPath=="REL1ID"){
				this.Rel1id=value;
			} else 
			if(xmlPath=="REL1YOB"){
				this.Rel1yob=value;
			} else 
			if(xmlPath=="REL1LIV"){
				this.Rel1liv=value;
			} else 
			if(xmlPath=="REL1YOD"){
				this.Rel1yod=value;
			} else 
			if(xmlPath=="REL1ONS"){
				this.Rel1ons=value;
			} else 
			if(xmlPath=="REL2ID"){
				this.Rel2id=value;
			} else 
			if(xmlPath=="REL2YOB"){
				this.Rel2yob=value;
			} else 
			if(xmlPath=="REL2LIV"){
				this.Rel2liv=value;
			} else 
			if(xmlPath=="REL2YOD"){
				this.Rel2yod=value;
			} else 
			if(xmlPath=="REL2ONS"){
				this.Rel2ons=value;
			} else 
			if(xmlPath=="REL3ID"){
				this.Rel3id=value;
			} else 
			if(xmlPath=="REL3YOB"){
				this.Rel3yob=value;
			} else 
			if(xmlPath=="REL3LIV"){
				this.Rel3liv=value;
			} else 
			if(xmlPath=="REL3YOD"){
				this.Rel3yod=value;
			} else 
			if(xmlPath=="REL3ONS"){
				this.Rel3ons=value;
			} else 
			if(xmlPath=="REL4ID"){
				this.Rel4id=value;
			} else 
			if(xmlPath=="REL4YOB"){
				this.Rel4yob=value;
			} else 
			if(xmlPath=="REL4LIV"){
				this.Rel4liv=value;
			} else 
			if(xmlPath=="REL4YOD"){
				this.Rel4yod=value;
			} else 
			if(xmlPath=="REL4ONS"){
				this.Rel4ons=value;
			} else 
			if(xmlPath=="REL5ID"){
				this.Rel5id=value;
			} else 
			if(xmlPath=="REL5YOB"){
				this.Rel5yob=value;
			} else 
			if(xmlPath=="REL5LIV"){
				this.Rel5liv=value;
			} else 
			if(xmlPath=="REL5YOD"){
				this.Rel5yod=value;
			} else 
			if(xmlPath=="REL5ONS"){
				this.Rel5ons=value;
			} else 
			if(xmlPath=="REL6ID"){
				this.Rel6id=value;
			} else 
			if(xmlPath=="REL6YOB"){
				this.Rel6yob=value;
			} else 
			if(xmlPath=="REL6LIV"){
				this.Rel6liv=value;
			} else 
			if(xmlPath=="REL6YOD"){
				this.Rel6yod=value;
			} else 
			if(xmlPath=="REL6ONS"){
				this.Rel6ons=value;
			} else 
			if(xmlPath=="REL7ID"){
				this.Rel7id=value;
			} else 
			if(xmlPath=="REL7YOB"){
				this.Rel7yob=value;
			} else 
			if(xmlPath=="REL7LIV"){
				this.Rel7liv=value;
			} else 
			if(xmlPath=="REL7YOD"){
				this.Rel7yod=value;
			} else 
			if(xmlPath=="REL7ONS"){
				this.Rel7ons=value;
			} else 
			if(xmlPath=="REL8ID"){
				this.Rel8id=value;
			} else 
			if(xmlPath=="REL8YOB"){
				this.Rel8yob=value;
			} else 
			if(xmlPath=="REL8LIV"){
				this.Rel8liv=value;
			} else 
			if(xmlPath=="REL8YOD"){
				this.Rel8yod=value;
			} else 
			if(xmlPath=="REL8ONS"){
				this.Rel8ons=value;
			} else 
			if(xmlPath=="REL9ID"){
				this.Rel9id=value;
			} else 
			if(xmlPath=="REL9YOB"){
				this.Rel9yob=value;
			} else 
			if(xmlPath=="REL9LIV"){
				this.Rel9liv=value;
			} else 
			if(xmlPath=="REL9YOD"){
				this.Rel9yod=value;
			} else 
			if(xmlPath=="REL9ONS"){
				this.Rel9ons=value;
			} else 
			if(xmlPath=="REL10ID"){
				this.Rel10id=value;
			} else 
			if(xmlPath=="REL10YOB"){
				this.Rel10yob=value;
			} else 
			if(xmlPath=="REL10LIV"){
				this.Rel10liv=value;
			} else 
			if(xmlPath=="REL10YOD"){
				this.Rel10yod=value;
			} else 
			if(xmlPath=="REL10ONS"){
				this.Rel10ons=value;
			} else 
			if(xmlPath=="REL11ID"){
				this.Rel11id=value;
			} else 
			if(xmlPath=="REL11YOB"){
				this.Rel11yob=value;
			} else 
			if(xmlPath=="REL11LIV"){
				this.Rel11liv=value;
			} else 
			if(xmlPath=="REL11YOD"){
				this.Rel11yod=value;
			} else 
			if(xmlPath=="REL11ONS"){
				this.Rel11ons=value;
			} else 
			if(xmlPath=="REL12ID"){
				this.Rel12id=value;
			} else 
			if(xmlPath=="REL12YOB"){
				this.Rel12yob=value;
			} else 
			if(xmlPath=="REL12LIV"){
				this.Rel12liv=value;
			} else 
			if(xmlPath=="REL12YOD"){
				this.Rel12yod=value;
			} else 
			if(xmlPath=="REL12ONS"){
				this.Rel12ons=value;
			} else 
			if(xmlPath=="REL13ID"){
				this.Rel13id=value;
			} else 
			if(xmlPath=="REL13YOB"){
				this.Rel13yob=value;
			} else 
			if(xmlPath=="REL13LIV"){
				this.Rel13liv=value;
			} else 
			if(xmlPath=="REL13YOD"){
				this.Rel13yod=value;
			} else 
			if(xmlPath=="REL13ONS"){
				this.Rel13ons=value;
			} else 
			if(xmlPath=="REL14ID"){
				this.Rel14id=value;
			} else 
			if(xmlPath=="REL14YOB"){
				this.Rel14yob=value;
			} else 
			if(xmlPath=="REL14LIV"){
				this.Rel14liv=value;
			} else 
			if(xmlPath=="REL14YOD"){
				this.Rel14yod=value;
			} else 
			if(xmlPath=="REL14ONS"){
				this.Rel14ons=value;
			} else 
			if(xmlPath=="REL15ID"){
				this.Rel15id=value;
			} else 
			if(xmlPath=="REL15YOB"){
				this.Rel15yob=value;
			} else 
			if(xmlPath=="REL15LIV"){
				this.Rel15liv=value;
			} else 
			if(xmlPath=="REL15YOD"){
				this.Rel15yod=value;
			} else 
			if(xmlPath=="REL15ONS"){
				this.Rel15ons=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="EXAMDATE"){
			return "field_data";
		}else if (xmlPath=="A3CHG"){
			return "field_data";
		}else if (xmlPath=="PARCHG"){
			return "field_data";
		}else if (xmlPath=="FAMILYID"){
			return "field_data";
		}else if (xmlPath=="GMOMID"){
			return "field_data";
		}else if (xmlPath=="GMOMYOB"){
			return "field_data";
		}else if (xmlPath=="GMOMLIV"){
			return "field_data";
		}else if (xmlPath=="GMOMYOD"){
			return "field_data";
		}else if (xmlPath=="GMOMDEM"){
			return "field_data";
		}else if (xmlPath=="GMOMONSET"){
			return "field_data";
		}else if (xmlPath=="GMOMAUTO"){
			return "field_data";
		}else if (xmlPath=="GDADID"){
			return "field_data";
		}else if (xmlPath=="GDADYOB"){
			return "field_data";
		}else if (xmlPath=="GDADLIV"){
			return "field_data";
		}else if (xmlPath=="GDADYOD"){
			return "field_data";
		}else if (xmlPath=="GDADDEM"){
			return "field_data";
		}else if (xmlPath=="GDADONSET"){
			return "field_data";
		}else if (xmlPath=="GDADAUTO"){
			return "field_data";
		}else if (xmlPath=="MOMID"){
			return "field_data";
		}else if (xmlPath=="MOMYOB"){
			return "field_data";
		}else if (xmlPath=="MOMLIV"){
			return "field_data";
		}else if (xmlPath=="MOMYOD"){
			return "field_data";
		}else if (xmlPath=="MOMDEM"){
			return "field_data";
		}else if (xmlPath=="MOMONSET"){
			return "field_data";
		}else if (xmlPath=="MOMAUTO"){
			return "field_data";
		}else if (xmlPath=="DADID"){
			return "field_data";
		}else if (xmlPath=="DADYOB"){
			return "field_data";
		}else if (xmlPath=="DADLIV"){
			return "field_data";
		}else if (xmlPath=="DADYOD"){
			return "field_data";
		}else if (xmlPath=="DADDEM"){
			return "field_data";
		}else if (xmlPath=="DADONSET"){
			return "field_data";
		}else if (xmlPath=="DADAUTO"){
			return "field_data";
		}else if (xmlPath=="SIBCHG"){
			return "field_data";
		}else if (xmlPath=="TWIN"){
			return "field_data";
		}else if (xmlPath=="TWINTYPE"){
			return "field_data";
		}else if (xmlPath=="SIBS"){
			return "field_data";
		}else if (xmlPath=="SIB1ID"){
			return "field_data";
		}else if (xmlPath=="SIB1YOB"){
			return "field_data";
		}else if (xmlPath=="SIB1LIV"){
			return "field_data";
		}else if (xmlPath=="SIB1YOD"){
			return "field_data";
		}else if (xmlPath=="SIB1DEM"){
			return "field_data";
		}else if (xmlPath=="SIB1ONS"){
			return "field_data";
		}else if (xmlPath=="SIB1AUTO"){
			return "field_data";
		}else if (xmlPath=="SIB1SEX"){
			return "field_data";
		}else if (xmlPath=="SIB2ID"){
			return "field_data";
		}else if (xmlPath=="SIB2YOB"){
			return "field_data";
		}else if (xmlPath=="SIB2LIV"){
			return "field_data";
		}else if (xmlPath=="SIB2YOD"){
			return "field_data";
		}else if (xmlPath=="SIB2DEM"){
			return "field_data";
		}else if (xmlPath=="SIB2ONS"){
			return "field_data";
		}else if (xmlPath=="SIB2AUTO"){
			return "field_data";
		}else if (xmlPath=="SIB2SEX"){
			return "field_data";
		}else if (xmlPath=="SIB3ID"){
			return "field_data";
		}else if (xmlPath=="SIB3YOB"){
			return "field_data";
		}else if (xmlPath=="SIB3LIV"){
			return "field_data";
		}else if (xmlPath=="SIB3YOD"){
			return "field_data";
		}else if (xmlPath=="SIB3DEM"){
			return "field_data";
		}else if (xmlPath=="SIB3ONS"){
			return "field_data";
		}else if (xmlPath=="SIB3AUTO"){
			return "field_data";
		}else if (xmlPath=="SIB3SEX"){
			return "field_data";
		}else if (xmlPath=="SIB4ID"){
			return "field_data";
		}else if (xmlPath=="SIB4YOB"){
			return "field_data";
		}else if (xmlPath=="SIB4LIV"){
			return "field_data";
		}else if (xmlPath=="SIB4YOD"){
			return "field_data";
		}else if (xmlPath=="SIB4DEM"){
			return "field_data";
		}else if (xmlPath=="SIB4ONS"){
			return "field_data";
		}else if (xmlPath=="SIB4AUTO"){
			return "field_data";
		}else if (xmlPath=="SIB4SEX"){
			return "field_data";
		}else if (xmlPath=="SIB5ID"){
			return "field_data";
		}else if (xmlPath=="SIB5YOB"){
			return "field_data";
		}else if (xmlPath=="SIB5LIV"){
			return "field_data";
		}else if (xmlPath=="SIB5YOD"){
			return "field_data";
		}else if (xmlPath=="SIB5DEM"){
			return "field_data";
		}else if (xmlPath=="SIB5ONS"){
			return "field_data";
		}else if (xmlPath=="SIB5AUTO"){
			return "field_data";
		}else if (xmlPath=="SIB5SEX"){
			return "field_data";
		}else if (xmlPath=="SIB6ID"){
			return "field_data";
		}else if (xmlPath=="SIB6YOB"){
			return "field_data";
		}else if (xmlPath=="SIB6LIV"){
			return "field_data";
		}else if (xmlPath=="SIB6YOD"){
			return "field_data";
		}else if (xmlPath=="SIB6DEM"){
			return "field_data";
		}else if (xmlPath=="SIB6ONS"){
			return "field_data";
		}else if (xmlPath=="SIB6AUTO"){
			return "field_data";
		}else if (xmlPath=="SIB6SEX"){
			return "field_data";
		}else if (xmlPath=="SIB7ID"){
			return "field_data";
		}else if (xmlPath=="SIB7YOB"){
			return "field_data";
		}else if (xmlPath=="SIB7LIV"){
			return "field_data";
		}else if (xmlPath=="SIB7YOD"){
			return "field_data";
		}else if (xmlPath=="SIB7DEM"){
			return "field_data";
		}else if (xmlPath=="SIB7ONS"){
			return "field_data";
		}else if (xmlPath=="SIB7AUTO"){
			return "field_data";
		}else if (xmlPath=="SIB7SEX"){
			return "field_data";
		}else if (xmlPath=="SIB8ID"){
			return "field_data";
		}else if (xmlPath=="SIB8YOB"){
			return "field_data";
		}else if (xmlPath=="SIB8LIV"){
			return "field_data";
		}else if (xmlPath=="SIB8YOD"){
			return "field_data";
		}else if (xmlPath=="SIB8DEM"){
			return "field_data";
		}else if (xmlPath=="SIB8ONS"){
			return "field_data";
		}else if (xmlPath=="SIB8AUTO"){
			return "field_data";
		}else if (xmlPath=="SIB8SEX"){
			return "field_data";
		}else if (xmlPath=="SIB9ID"){
			return "field_data";
		}else if (xmlPath=="SIB9YOB"){
			return "field_data";
		}else if (xmlPath=="SIB9LIV"){
			return "field_data";
		}else if (xmlPath=="SIB9YOD"){
			return "field_data";
		}else if (xmlPath=="SIB9DEM"){
			return "field_data";
		}else if (xmlPath=="SIB9ONS"){
			return "field_data";
		}else if (xmlPath=="SIB9AUTO"){
			return "field_data";
		}else if (xmlPath=="SIB9SEX"){
			return "field_data";
		}else if (xmlPath=="SIB10ID"){
			return "field_data";
		}else if (xmlPath=="SIB10YOB"){
			return "field_data";
		}else if (xmlPath=="SIB10LIV"){
			return "field_data";
		}else if (xmlPath=="SIB10YOD"){
			return "field_data";
		}else if (xmlPath=="SIB10DEM"){
			return "field_data";
		}else if (xmlPath=="SIB10ONS"){
			return "field_data";
		}else if (xmlPath=="SIB10AUTO"){
			return "field_data";
		}else if (xmlPath=="SIB10SEX"){
			return "field_data";
		}else if (xmlPath=="SIB11ID"){
			return "field_data";
		}else if (xmlPath=="SIB11YOB"){
			return "field_data";
		}else if (xmlPath=="SIB11LIV"){
			return "field_data";
		}else if (xmlPath=="SIB11YOD"){
			return "field_data";
		}else if (xmlPath=="SIB11DEM"){
			return "field_data";
		}else if (xmlPath=="SIB11ONS"){
			return "field_data";
		}else if (xmlPath=="SIB11AUTO"){
			return "field_data";
		}else if (xmlPath=="SIB11SEX"){
			return "field_data";
		}else if (xmlPath=="SIB12ID"){
			return "field_data";
		}else if (xmlPath=="SIB12YOB"){
			return "field_data";
		}else if (xmlPath=="SIB12LIV"){
			return "field_data";
		}else if (xmlPath=="SIB12YOD"){
			return "field_data";
		}else if (xmlPath=="SIB12DEM"){
			return "field_data";
		}else if (xmlPath=="SIB12ONS"){
			return "field_data";
		}else if (xmlPath=="SIB12AUTO"){
			return "field_data";
		}else if (xmlPath=="SIB12SEX"){
			return "field_data";
		}else if (xmlPath=="SIB13ID"){
			return "field_data";
		}else if (xmlPath=="SIB13YOB"){
			return "field_data";
		}else if (xmlPath=="SIB13LIV"){
			return "field_data";
		}else if (xmlPath=="SIB13YOD"){
			return "field_data";
		}else if (xmlPath=="SIB13DEM"){
			return "field_data";
		}else if (xmlPath=="SIB13ONS"){
			return "field_data";
		}else if (xmlPath=="SIB13AUTO"){
			return "field_data";
		}else if (xmlPath=="SIB13SEX"){
			return "field_data";
		}else if (xmlPath=="SIB14ID"){
			return "field_data";
		}else if (xmlPath=="SIB14YOB"){
			return "field_data";
		}else if (xmlPath=="SIB14LIV"){
			return "field_data";
		}else if (xmlPath=="SIB14YOD"){
			return "field_data";
		}else if (xmlPath=="SIB14DEM"){
			return "field_data";
		}else if (xmlPath=="SIB14ONS"){
			return "field_data";
		}else if (xmlPath=="SIB14AUTO"){
			return "field_data";
		}else if (xmlPath=="SIB14SEX"){
			return "field_data";
		}else if (xmlPath=="SIB15ID"){
			return "field_data";
		}else if (xmlPath=="SIB15YOB"){
			return "field_data";
		}else if (xmlPath=="SIB15LIV"){
			return "field_data";
		}else if (xmlPath=="SIB15YOD"){
			return "field_data";
		}else if (xmlPath=="SIB15DEM"){
			return "field_data";
		}else if (xmlPath=="SIB15ONS"){
			return "field_data";
		}else if (xmlPath=="SIB155AUTO"){
			return "field_data";
		}else if (xmlPath=="SIB155SEX"){
			return "field_data";
		}else if (xmlPath=="SIB16ID"){
			return "field_data";
		}else if (xmlPath=="SIB16YOB"){
			return "field_data";
		}else if (xmlPath=="SIB16LIV"){
			return "field_data";
		}else if (xmlPath=="SIB16YOD"){
			return "field_data";
		}else if (xmlPath=="SIB16DEM"){
			return "field_data";
		}else if (xmlPath=="SIB16ONS"){
			return "field_data";
		}else if (xmlPath=="SIB16AUTO"){
			return "field_data";
		}else if (xmlPath=="SIB16SEX"){
			return "field_data";
		}else if (xmlPath=="SIB17ID"){
			return "field_data";
		}else if (xmlPath=="SIB17YOB"){
			return "field_data";
		}else if (xmlPath=="SIB17LIV"){
			return "field_data";
		}else if (xmlPath=="SIB17YOD"){
			return "field_data";
		}else if (xmlPath=="SIB17DEM"){
			return "field_data";
		}else if (xmlPath=="SIB17ONS"){
			return "field_data";
		}else if (xmlPath=="SIB17AUTO"){
			return "field_data";
		}else if (xmlPath=="SIB17SEX"){
			return "field_data";
		}else if (xmlPath=="SIB18ID"){
			return "field_data";
		}else if (xmlPath=="SIB18YOB"){
			return "field_data";
		}else if (xmlPath=="SIB18LIV"){
			return "field_data";
		}else if (xmlPath=="SIB18YOD"){
			return "field_data";
		}else if (xmlPath=="SIB18DEM"){
			return "field_data";
		}else if (xmlPath=="SIB18ONS"){
			return "field_data";
		}else if (xmlPath=="SIB18AUTO"){
			return "field_data";
		}else if (xmlPath=="SIB18SEX"){
			return "field_data";
		}else if (xmlPath=="SIB19ID"){
			return "field_data";
		}else if (xmlPath=="SIB19YOB"){
			return "field_data";
		}else if (xmlPath=="SIB19LIV"){
			return "field_data";
		}else if (xmlPath=="SIB19YOD"){
			return "field_data";
		}else if (xmlPath=="SIB19DEM"){
			return "field_data";
		}else if (xmlPath=="SIB19ONS"){
			return "field_data";
		}else if (xmlPath=="SIB19AUTO"){
			return "field_data";
		}else if (xmlPath=="SIB19SEX"){
			return "field_data";
		}else if (xmlPath=="SIB20ID"){
			return "field_data";
		}else if (xmlPath=="SIB20YOB"){
			return "field_data";
		}else if (xmlPath=="SIB20LIV"){
			return "field_data";
		}else if (xmlPath=="SIB20YOD"){
			return "field_data";
		}else if (xmlPath=="SIB20DEM"){
			return "field_data";
		}else if (xmlPath=="SIB20ONS"){
			return "field_data";
		}else if (xmlPath=="SIB20AUTO"){
			return "field_data";
		}else if (xmlPath=="SIB20SEX"){
			return "field_data";
		}else if (xmlPath=="KIDCHG"){
			return "field_data";
		}else if (xmlPath=="KIDS"){
			return "field_data";
		}else if (xmlPath=="KID1ID"){
			return "field_data";
		}else if (xmlPath=="KID1YOB"){
			return "field_data";
		}else if (xmlPath=="KID1LIV"){
			return "field_data";
		}else if (xmlPath=="KID1YOD"){
			return "field_data";
		}else if (xmlPath=="KID1DEM"){
			return "field_data";
		}else if (xmlPath=="KID1ONS"){
			return "field_data";
		}else if (xmlPath=="KID1AUTO"){
			return "field_data";
		}else if (xmlPath=="KID1SEX"){
			return "field_data";
		}else if (xmlPath=="KID2ID"){
			return "field_data";
		}else if (xmlPath=="KID2YOB"){
			return "field_data";
		}else if (xmlPath=="KID2LIV"){
			return "field_data";
		}else if (xmlPath=="KID2YOD"){
			return "field_data";
		}else if (xmlPath=="KID2DEM"){
			return "field_data";
		}else if (xmlPath=="KID2ONS"){
			return "field_data";
		}else if (xmlPath=="KID2AUTO"){
			return "field_data";
		}else if (xmlPath=="KID2SEX"){
			return "field_data";
		}else if (xmlPath=="KID3ID"){
			return "field_data";
		}else if (xmlPath=="KID3YOB"){
			return "field_data";
		}else if (xmlPath=="KID3LIV"){
			return "field_data";
		}else if (xmlPath=="KID3YOD"){
			return "field_data";
		}else if (xmlPath=="KID3DEM"){
			return "field_data";
		}else if (xmlPath=="KID3ONS"){
			return "field_data";
		}else if (xmlPath=="KID3AUTO"){
			return "field_data";
		}else if (xmlPath=="KID3SEX"){
			return "field_data";
		}else if (xmlPath=="KID4ID"){
			return "field_data";
		}else if (xmlPath=="KID4YOB"){
			return "field_data";
		}else if (xmlPath=="KID4LIV"){
			return "field_data";
		}else if (xmlPath=="KID4YOD"){
			return "field_data";
		}else if (xmlPath=="KID4DEM"){
			return "field_data";
		}else if (xmlPath=="KID4ONS"){
			return "field_data";
		}else if (xmlPath=="KID4AUTO"){
			return "field_data";
		}else if (xmlPath=="KID4SEX"){
			return "field_data";
		}else if (xmlPath=="KID5ID"){
			return "field_data";
		}else if (xmlPath=="KID5YOB"){
			return "field_data";
		}else if (xmlPath=="KID5LIV"){
			return "field_data";
		}else if (xmlPath=="KID5YOD"){
			return "field_data";
		}else if (xmlPath=="KID5DEM"){
			return "field_data";
		}else if (xmlPath=="KID5ONS"){
			return "field_data";
		}else if (xmlPath=="KID5AUTO"){
			return "field_data";
		}else if (xmlPath=="KID5SEX"){
			return "field_data";
		}else if (xmlPath=="KID6ID"){
			return "field_data";
		}else if (xmlPath=="KID6YOB"){
			return "field_data";
		}else if (xmlPath=="KID6LIV"){
			return "field_data";
		}else if (xmlPath=="KID6YOD"){
			return "field_data";
		}else if (xmlPath=="KID6DEM"){
			return "field_data";
		}else if (xmlPath=="KID6ONS"){
			return "field_data";
		}else if (xmlPath=="KID6AUTO"){
			return "field_data";
		}else if (xmlPath=="KID6SEX"){
			return "field_data";
		}else if (xmlPath=="KID7ID"){
			return "field_data";
		}else if (xmlPath=="KID7YOB"){
			return "field_data";
		}else if (xmlPath=="KID7LIV"){
			return "field_data";
		}else if (xmlPath=="KID7YOD"){
			return "field_data";
		}else if (xmlPath=="KID7DEM"){
			return "field_data";
		}else if (xmlPath=="KID7ONS"){
			return "field_data";
		}else if (xmlPath=="KID7AUTO"){
			return "field_data";
		}else if (xmlPath=="KID7SEX"){
			return "field_data";
		}else if (xmlPath=="KID8ID"){
			return "field_data";
		}else if (xmlPath=="KID8YOB"){
			return "field_data";
		}else if (xmlPath=="KID8LIV"){
			return "field_data";
		}else if (xmlPath=="KID8YOD"){
			return "field_data";
		}else if (xmlPath=="KID8DEM"){
			return "field_data";
		}else if (xmlPath=="KID8ONS"){
			return "field_data";
		}else if (xmlPath=="KID8AUTO"){
			return "field_data";
		}else if (xmlPath=="KID8SEX"){
			return "field_data";
		}else if (xmlPath=="KID9ID"){
			return "field_data";
		}else if (xmlPath=="KID9YOB"){
			return "field_data";
		}else if (xmlPath=="KID9LIV"){
			return "field_data";
		}else if (xmlPath=="KID9YOD"){
			return "field_data";
		}else if (xmlPath=="KID9DEM"){
			return "field_data";
		}else if (xmlPath=="KID9ONS"){
			return "field_data";
		}else if (xmlPath=="KID9AUTO"){
			return "field_data";
		}else if (xmlPath=="KID9SEX"){
			return "field_data";
		}else if (xmlPath=="KID10ID"){
			return "field_data";
		}else if (xmlPath=="KID10YOB"){
			return "field_data";
		}else if (xmlPath=="KID10LIV"){
			return "field_data";
		}else if (xmlPath=="KID10YOD"){
			return "field_data";
		}else if (xmlPath=="KID10DEM"){
			return "field_data";
		}else if (xmlPath=="KID10ONS"){
			return "field_data";
		}else if (xmlPath=="KID10AUTO"){
			return "field_data";
		}else if (xmlPath=="KID10SEX"){
			return "field_data";
		}else if (xmlPath=="KID11ID"){
			return "field_data";
		}else if (xmlPath=="KID11YOB"){
			return "field_data";
		}else if (xmlPath=="KID11LIV"){
			return "field_data";
		}else if (xmlPath=="KID11YOD"){
			return "field_data";
		}else if (xmlPath=="KID11DEM"){
			return "field_data";
		}else if (xmlPath=="KID11ONS"){
			return "field_data";
		}else if (xmlPath=="KID11AUTO"){
			return "field_data";
		}else if (xmlPath=="KID11SEX"){
			return "field_data";
		}else if (xmlPath=="KID12ID"){
			return "field_data";
		}else if (xmlPath=="KID12YOB"){
			return "field_data";
		}else if (xmlPath=="KID12LIV"){
			return "field_data";
		}else if (xmlPath=="KID12YOD"){
			return "field_data";
		}else if (xmlPath=="KID12DEM"){
			return "field_data";
		}else if (xmlPath=="KID12ONS"){
			return "field_data";
		}else if (xmlPath=="KID12AUTO"){
			return "field_data";
		}else if (xmlPath=="KID12SEX"){
			return "field_data";
		}else if (xmlPath=="KID13ID"){
			return "field_data";
		}else if (xmlPath=="KID13YOB"){
			return "field_data";
		}else if (xmlPath=="KID13LIV"){
			return "field_data";
		}else if (xmlPath=="KID13YOD"){
			return "field_data";
		}else if (xmlPath=="KID13DEM"){
			return "field_data";
		}else if (xmlPath=="KID13ONS"){
			return "field_data";
		}else if (xmlPath=="KID13AUTO"){
			return "field_data";
		}else if (xmlPath=="KID13SEX"){
			return "field_data";
		}else if (xmlPath=="KID14ID"){
			return "field_data";
		}else if (xmlPath=="KID14YOB"){
			return "field_data";
		}else if (xmlPath=="KID14LIV"){
			return "field_data";
		}else if (xmlPath=="KID14YOD"){
			return "field_data";
		}else if (xmlPath=="KID14DEM"){
			return "field_data";
		}else if (xmlPath=="KID14ONS"){
			return "field_data";
		}else if (xmlPath=="KID14AUTO"){
			return "field_data";
		}else if (xmlPath=="KID14SEX"){
			return "field_data";
		}else if (xmlPath=="KID15ID"){
			return "field_data";
		}else if (xmlPath=="KID15YOB"){
			return "field_data";
		}else if (xmlPath=="KID15LIV"){
			return "field_data";
		}else if (xmlPath=="KID15YOD"){
			return "field_data";
		}else if (xmlPath=="KID15DEM"){
			return "field_data";
		}else if (xmlPath=="KID15ONS"){
			return "field_data";
		}else if (xmlPath=="KID15AUTO"){
			return "field_data";
		}else if (xmlPath=="KID15SEX"){
			return "field_data";
		}else if (xmlPath=="RELCHG"){
			return "field_data";
		}else if (xmlPath=="RELSDEM"){
			return "field_data";
		}else if (xmlPath=="REL1ID"){
			return "field_data";
		}else if (xmlPath=="REL1YOB"){
			return "field_data";
		}else if (xmlPath=="REL1LIV"){
			return "field_data";
		}else if (xmlPath=="REL1YOD"){
			return "field_data";
		}else if (xmlPath=="REL1ONS"){
			return "field_data";
		}else if (xmlPath=="REL2ID"){
			return "field_data";
		}else if (xmlPath=="REL2YOB"){
			return "field_data";
		}else if (xmlPath=="REL2LIV"){
			return "field_data";
		}else if (xmlPath=="REL2YOD"){
			return "field_data";
		}else if (xmlPath=="REL2ONS"){
			return "field_data";
		}else if (xmlPath=="REL3ID"){
			return "field_data";
		}else if (xmlPath=="REL3YOB"){
			return "field_data";
		}else if (xmlPath=="REL3LIV"){
			return "field_data";
		}else if (xmlPath=="REL3YOD"){
			return "field_data";
		}else if (xmlPath=="REL3ONS"){
			return "field_data";
		}else if (xmlPath=="REL4ID"){
			return "field_data";
		}else if (xmlPath=="REL4YOB"){
			return "field_data";
		}else if (xmlPath=="REL4LIV"){
			return "field_data";
		}else if (xmlPath=="REL4YOD"){
			return "field_data";
		}else if (xmlPath=="REL4ONS"){
			return "field_data";
		}else if (xmlPath=="REL5ID"){
			return "field_data";
		}else if (xmlPath=="REL5YOB"){
			return "field_data";
		}else if (xmlPath=="REL5LIV"){
			return "field_data";
		}else if (xmlPath=="REL5YOD"){
			return "field_data";
		}else if (xmlPath=="REL5ONS"){
			return "field_data";
		}else if (xmlPath=="REL6ID"){
			return "field_data";
		}else if (xmlPath=="REL6YOB"){
			return "field_data";
		}else if (xmlPath=="REL6LIV"){
			return "field_data";
		}else if (xmlPath=="REL6YOD"){
			return "field_data";
		}else if (xmlPath=="REL6ONS"){
			return "field_data";
		}else if (xmlPath=="REL7ID"){
			return "field_data";
		}else if (xmlPath=="REL7YOB"){
			return "field_data";
		}else if (xmlPath=="REL7LIV"){
			return "field_data";
		}else if (xmlPath=="REL7YOD"){
			return "field_data";
		}else if (xmlPath=="REL7ONS"){
			return "field_data";
		}else if (xmlPath=="REL8ID"){
			return "field_data";
		}else if (xmlPath=="REL8YOB"){
			return "field_data";
		}else if (xmlPath=="REL8LIV"){
			return "field_data";
		}else if (xmlPath=="REL8YOD"){
			return "field_data";
		}else if (xmlPath=="REL8ONS"){
			return "field_data";
		}else if (xmlPath=="REL9ID"){
			return "field_data";
		}else if (xmlPath=="REL9YOB"){
			return "field_data";
		}else if (xmlPath=="REL9LIV"){
			return "field_data";
		}else if (xmlPath=="REL9YOD"){
			return "field_data";
		}else if (xmlPath=="REL9ONS"){
			return "field_data";
		}else if (xmlPath=="REL10ID"){
			return "field_data";
		}else if (xmlPath=="REL10YOB"){
			return "field_data";
		}else if (xmlPath=="REL10LIV"){
			return "field_data";
		}else if (xmlPath=="REL10YOD"){
			return "field_data";
		}else if (xmlPath=="REL10ONS"){
			return "field_data";
		}else if (xmlPath=="REL11ID"){
			return "field_data";
		}else if (xmlPath=="REL11YOB"){
			return "field_data";
		}else if (xmlPath=="REL11LIV"){
			return "field_data";
		}else if (xmlPath=="REL11YOD"){
			return "field_data";
		}else if (xmlPath=="REL11ONS"){
			return "field_data";
		}else if (xmlPath=="REL12ID"){
			return "field_data";
		}else if (xmlPath=="REL12YOB"){
			return "field_data";
		}else if (xmlPath=="REL12LIV"){
			return "field_data";
		}else if (xmlPath=="REL12YOD"){
			return "field_data";
		}else if (xmlPath=="REL12ONS"){
			return "field_data";
		}else if (xmlPath=="REL13ID"){
			return "field_data";
		}else if (xmlPath=="REL13YOB"){
			return "field_data";
		}else if (xmlPath=="REL13LIV"){
			return "field_data";
		}else if (xmlPath=="REL13YOD"){
			return "field_data";
		}else if (xmlPath=="REL13ONS"){
			return "field_data";
		}else if (xmlPath=="REL14ID"){
			return "field_data";
		}else if (xmlPath=="REL14YOB"){
			return "field_data";
		}else if (xmlPath=="REL14LIV"){
			return "field_data";
		}else if (xmlPath=="REL14YOD"){
			return "field_data";
		}else if (xmlPath=="REL14ONS"){
			return "field_data";
		}else if (xmlPath=="REL15ID"){
			return "field_data";
		}else if (xmlPath=="REL15YOB"){
			return "field_data";
		}else if (xmlPath=="REL15LIV"){
			return "field_data";
		}else if (xmlPath=="REL15YOD"){
			return "field_data";
		}else if (xmlPath=="REL15ONS"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<uds:A3SBFMHST";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</uds:A3SBFMHST>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Examdate!=null){
			xmlTxt+="\n<uds:EXAMDATE";
			xmlTxt+=">";
			xmlTxt+=this.Examdate;
			xmlTxt+="</uds:EXAMDATE>";
		}
		if (this.A3chg!=null){
			xmlTxt+="\n<uds:A3CHG";
			xmlTxt+=">";
			xmlTxt+=this.A3chg;
			xmlTxt+="</uds:A3CHG>";
		}
		if (this.Parchg!=null){
			xmlTxt+="\n<uds:PARCHG";
			xmlTxt+=">";
			xmlTxt+=this.Parchg;
			xmlTxt+="</uds:PARCHG>";
		}
		if (this.Familyid!=null){
			xmlTxt+="\n<uds:FAMILYID";
			xmlTxt+=">";
			xmlTxt+=this.Familyid;
			xmlTxt+="</uds:FAMILYID>";
		}
		if (this.Gmomid!=null){
			xmlTxt+="\n<uds:GMOMID";
			xmlTxt+=">";
			xmlTxt+=this.Gmomid.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:GMOMID>";
		}
		if (this.Gmomyob!=null){
			xmlTxt+="\n<uds:GMOMYOB";
			xmlTxt+=">";
			xmlTxt+=this.Gmomyob;
			xmlTxt+="</uds:GMOMYOB>";
		}
		if (this.Gmomliv!=null){
			xmlTxt+="\n<uds:GMOMLIV";
			xmlTxt+=">";
			xmlTxt+=this.Gmomliv;
			xmlTxt+="</uds:GMOMLIV>";
		}
		if (this.Gmomyod!=null){
			xmlTxt+="\n<uds:GMOMYOD";
			xmlTxt+=">";
			xmlTxt+=this.Gmomyod;
			xmlTxt+="</uds:GMOMYOD>";
		}
		if (this.Gmomdem!=null){
			xmlTxt+="\n<uds:GMOMDEM";
			xmlTxt+=">";
			xmlTxt+=this.Gmomdem;
			xmlTxt+="</uds:GMOMDEM>";
		}
		if (this.Gmomonset!=null){
			xmlTxt+="\n<uds:GMOMONSET";
			xmlTxt+=">";
			xmlTxt+=this.Gmomonset.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:GMOMONSET>";
		}
		if (this.Gmomauto!=null){
			xmlTxt+="\n<uds:GMOMAUTO";
			xmlTxt+=">";
			xmlTxt+=this.Gmomauto;
			xmlTxt+="</uds:GMOMAUTO>";
		}
		if (this.Gdadid!=null){
			xmlTxt+="\n<uds:GDADID";
			xmlTxt+=">";
			xmlTxt+=this.Gdadid.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:GDADID>";
		}
		if (this.Gdadyob!=null){
			xmlTxt+="\n<uds:GDADYOB";
			xmlTxt+=">";
			xmlTxt+=this.Gdadyob;
			xmlTxt+="</uds:GDADYOB>";
		}
		if (this.Gdadliv!=null){
			xmlTxt+="\n<uds:GDADLIV";
			xmlTxt+=">";
			xmlTxt+=this.Gdadliv;
			xmlTxt+="</uds:GDADLIV>";
		}
		if (this.Gdadyod!=null){
			xmlTxt+="\n<uds:GDADYOD";
			xmlTxt+=">";
			xmlTxt+=this.Gdadyod;
			xmlTxt+="</uds:GDADYOD>";
		}
		if (this.Gdaddem!=null){
			xmlTxt+="\n<uds:GDADDEM";
			xmlTxt+=">";
			xmlTxt+=this.Gdaddem;
			xmlTxt+="</uds:GDADDEM>";
		}
		if (this.Gdadonset!=null){
			xmlTxt+="\n<uds:GDADONSET";
			xmlTxt+=">";
			xmlTxt+=this.Gdadonset.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:GDADONSET>";
		}
		if (this.Gdadauto!=null){
			xmlTxt+="\n<uds:GDADAUTO";
			xmlTxt+=">";
			xmlTxt+=this.Gdadauto;
			xmlTxt+="</uds:GDADAUTO>";
		}
		if (this.Momid!=null){
			xmlTxt+="\n<uds:MOMID";
			xmlTxt+=">";
			xmlTxt+=this.Momid.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:MOMID>";
		}
		if (this.Momyob!=null){
			xmlTxt+="\n<uds:MOMYOB";
			xmlTxt+=">";
			xmlTxt+=this.Momyob;
			xmlTxt+="</uds:MOMYOB>";
		}
		if (this.Momliv!=null){
			xmlTxt+="\n<uds:MOMLIV";
			xmlTxt+=">";
			xmlTxt+=this.Momliv;
			xmlTxt+="</uds:MOMLIV>";
		}
		if (this.Momyod!=null){
			xmlTxt+="\n<uds:MOMYOD";
			xmlTxt+=">";
			xmlTxt+=this.Momyod;
			xmlTxt+="</uds:MOMYOD>";
		}
		if (this.Momdem!=null){
			xmlTxt+="\n<uds:MOMDEM";
			xmlTxt+=">";
			xmlTxt+=this.Momdem;
			xmlTxt+="</uds:MOMDEM>";
		}
		if (this.Momonset!=null){
			xmlTxt+="\n<uds:MOMONSET";
			xmlTxt+=">";
			xmlTxt+=this.Momonset.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:MOMONSET>";
		}
		if (this.Momauto!=null){
			xmlTxt+="\n<uds:MOMAUTO";
			xmlTxt+=">";
			xmlTxt+=this.Momauto;
			xmlTxt+="</uds:MOMAUTO>";
		}
		if (this.Dadid!=null){
			xmlTxt+="\n<uds:DADID";
			xmlTxt+=">";
			xmlTxt+=this.Dadid.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:DADID>";
		}
		if (this.Dadyob!=null){
			xmlTxt+="\n<uds:DADYOB";
			xmlTxt+=">";
			xmlTxt+=this.Dadyob;
			xmlTxt+="</uds:DADYOB>";
		}
		if (this.Dadliv!=null){
			xmlTxt+="\n<uds:DADLIV";
			xmlTxt+=">";
			xmlTxt+=this.Dadliv;
			xmlTxt+="</uds:DADLIV>";
		}
		if (this.Dadyod!=null){
			xmlTxt+="\n<uds:DADYOD";
			xmlTxt+=">";
			xmlTxt+=this.Dadyod;
			xmlTxt+="</uds:DADYOD>";
		}
		if (this.Daddem!=null){
			xmlTxt+="\n<uds:DADDEM";
			xmlTxt+=">";
			xmlTxt+=this.Daddem;
			xmlTxt+="</uds:DADDEM>";
		}
		if (this.Dadonset!=null){
			xmlTxt+="\n<uds:DADONSET";
			xmlTxt+=">";
			xmlTxt+=this.Dadonset.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:DADONSET>";
		}
		if (this.Dadauto!=null){
			xmlTxt+="\n<uds:DADAUTO";
			xmlTxt+=">";
			xmlTxt+=this.Dadauto;
			xmlTxt+="</uds:DADAUTO>";
		}
		if (this.Sibchg!=null){
			xmlTxt+="\n<uds:SIBCHG";
			xmlTxt+=">";
			xmlTxt+=this.Sibchg;
			xmlTxt+="</uds:SIBCHG>";
		}
		if (this.Twin!=null){
			xmlTxt+="\n<uds:TWIN";
			xmlTxt+=">";
			xmlTxt+=this.Twin;
			xmlTxt+="</uds:TWIN>";
		}
		if (this.Twintype!=null){
			xmlTxt+="\n<uds:TWINTYPE";
			xmlTxt+=">";
			xmlTxt+=this.Twintype;
			xmlTxt+="</uds:TWINTYPE>";
		}
		if (this.Sibs!=null){
			xmlTxt+="\n<uds:SIBS";
			xmlTxt+=">";
			xmlTxt+=this.Sibs;
			xmlTxt+="</uds:SIBS>";
		}
		if (this.Sib1id!=null){
			xmlTxt+="\n<uds:SIB1ID";
			xmlTxt+=">";
			xmlTxt+=this.Sib1id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:SIB1ID>";
		}
		if (this.Sib1yob!=null){
			xmlTxt+="\n<uds:SIB1YOB";
			xmlTxt+=">";
			xmlTxt+=this.Sib1yob;
			xmlTxt+="</uds:SIB1YOB>";
		}
		if (this.Sib1liv!=null){
			xmlTxt+="\n<uds:SIB1LIV";
			xmlTxt+=">";
			xmlTxt+=this.Sib1liv;
			xmlTxt+="</uds:SIB1LIV>";
		}
		if (this.Sib1yod!=null){
			xmlTxt+="\n<uds:SIB1YOD";
			xmlTxt+=">";
			xmlTxt+=this.Sib1yod;
			xmlTxt+="</uds:SIB1YOD>";
		}
		if (this.Sib1dem!=null){
			xmlTxt+="\n<uds:SIB1DEM";
			xmlTxt+=">";
			xmlTxt+=this.Sib1dem;
			xmlTxt+="</uds:SIB1DEM>";
		}
		if (this.Sib1ons!=null){
			xmlTxt+="\n<uds:SIB1ONS";
			xmlTxt+=">";
			xmlTxt+=this.Sib1ons;
			xmlTxt+="</uds:SIB1ONS>";
		}
		if (this.Sib1auto!=null){
			xmlTxt+="\n<uds:SIB1AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Sib1auto;
			xmlTxt+="</uds:SIB1AUTO>";
		}
		if (this.Sib1sex!=null){
			xmlTxt+="\n<uds:SIB1SEX";
			xmlTxt+=">";
			xmlTxt+=this.Sib1sex;
			xmlTxt+="</uds:SIB1SEX>";
		}
		if (this.Sib2id!=null){
			xmlTxt+="\n<uds:SIB2ID";
			xmlTxt+=">";
			xmlTxt+=this.Sib2id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:SIB2ID>";
		}
		if (this.Sib2yob!=null){
			xmlTxt+="\n<uds:SIB2YOB";
			xmlTxt+=">";
			xmlTxt+=this.Sib2yob;
			xmlTxt+="</uds:SIB2YOB>";
		}
		if (this.Sib2liv!=null){
			xmlTxt+="\n<uds:SIB2LIV";
			xmlTxt+=">";
			xmlTxt+=this.Sib2liv;
			xmlTxt+="</uds:SIB2LIV>";
		}
		if (this.Sib2yod!=null){
			xmlTxt+="\n<uds:SIB2YOD";
			xmlTxt+=">";
			xmlTxt+=this.Sib2yod;
			xmlTxt+="</uds:SIB2YOD>";
		}
		if (this.Sib2dem!=null){
			xmlTxt+="\n<uds:SIB2DEM";
			xmlTxt+=">";
			xmlTxt+=this.Sib2dem;
			xmlTxt+="</uds:SIB2DEM>";
		}
		if (this.Sib2ons!=null){
			xmlTxt+="\n<uds:SIB2ONS";
			xmlTxt+=">";
			xmlTxt+=this.Sib2ons;
			xmlTxt+="</uds:SIB2ONS>";
		}
		if (this.Sib2auto!=null){
			xmlTxt+="\n<uds:SIB2AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Sib2auto;
			xmlTxt+="</uds:SIB2AUTO>";
		}
		if (this.Sib2sex!=null){
			xmlTxt+="\n<uds:SIB2SEX";
			xmlTxt+=">";
			xmlTxt+=this.Sib2sex;
			xmlTxt+="</uds:SIB2SEX>";
		}
		if (this.Sib3id!=null){
			xmlTxt+="\n<uds:SIB3ID";
			xmlTxt+=">";
			xmlTxt+=this.Sib3id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:SIB3ID>";
		}
		if (this.Sib3yob!=null){
			xmlTxt+="\n<uds:SIB3YOB";
			xmlTxt+=">";
			xmlTxt+=this.Sib3yob;
			xmlTxt+="</uds:SIB3YOB>";
		}
		if (this.Sib3liv!=null){
			xmlTxt+="\n<uds:SIB3LIV";
			xmlTxt+=">";
			xmlTxt+=this.Sib3liv;
			xmlTxt+="</uds:SIB3LIV>";
		}
		if (this.Sib3yod!=null){
			xmlTxt+="\n<uds:SIB3YOD";
			xmlTxt+=">";
			xmlTxt+=this.Sib3yod;
			xmlTxt+="</uds:SIB3YOD>";
		}
		if (this.Sib3dem!=null){
			xmlTxt+="\n<uds:SIB3DEM";
			xmlTxt+=">";
			xmlTxt+=this.Sib3dem;
			xmlTxt+="</uds:SIB3DEM>";
		}
		if (this.Sib3ons!=null){
			xmlTxt+="\n<uds:SIB3ONS";
			xmlTxt+=">";
			xmlTxt+=this.Sib3ons;
			xmlTxt+="</uds:SIB3ONS>";
		}
		if (this.Sib3auto!=null){
			xmlTxt+="\n<uds:SIB3AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Sib3auto;
			xmlTxt+="</uds:SIB3AUTO>";
		}
		if (this.Sib3sex!=null){
			xmlTxt+="\n<uds:SIB3SEX";
			xmlTxt+=">";
			xmlTxt+=this.Sib3sex;
			xmlTxt+="</uds:SIB3SEX>";
		}
		if (this.Sib4id!=null){
			xmlTxt+="\n<uds:SIB4ID";
			xmlTxt+=">";
			xmlTxt+=this.Sib4id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:SIB4ID>";
		}
		if (this.Sib4yob!=null){
			xmlTxt+="\n<uds:SIB4YOB";
			xmlTxt+=">";
			xmlTxt+=this.Sib4yob;
			xmlTxt+="</uds:SIB4YOB>";
		}
		if (this.Sib4liv!=null){
			xmlTxt+="\n<uds:SIB4LIV";
			xmlTxt+=">";
			xmlTxt+=this.Sib4liv;
			xmlTxt+="</uds:SIB4LIV>";
		}
		if (this.Sib4yod!=null){
			xmlTxt+="\n<uds:SIB4YOD";
			xmlTxt+=">";
			xmlTxt+=this.Sib4yod;
			xmlTxt+="</uds:SIB4YOD>";
		}
		if (this.Sib4dem!=null){
			xmlTxt+="\n<uds:SIB4DEM";
			xmlTxt+=">";
			xmlTxt+=this.Sib4dem;
			xmlTxt+="</uds:SIB4DEM>";
		}
		if (this.Sib4ons!=null){
			xmlTxt+="\n<uds:SIB4ONS";
			xmlTxt+=">";
			xmlTxt+=this.Sib4ons;
			xmlTxt+="</uds:SIB4ONS>";
		}
		if (this.Sib4auto!=null){
			xmlTxt+="\n<uds:SIB4AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Sib4auto;
			xmlTxt+="</uds:SIB4AUTO>";
		}
		if (this.Sib4sex!=null){
			xmlTxt+="\n<uds:SIB4SEX";
			xmlTxt+=">";
			xmlTxt+=this.Sib4sex;
			xmlTxt+="</uds:SIB4SEX>";
		}
		if (this.Sib5id!=null){
			xmlTxt+="\n<uds:SIB5ID";
			xmlTxt+=">";
			xmlTxt+=this.Sib5id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:SIB5ID>";
		}
		if (this.Sib5yob!=null){
			xmlTxt+="\n<uds:SIB5YOB";
			xmlTxt+=">";
			xmlTxt+=this.Sib5yob;
			xmlTxt+="</uds:SIB5YOB>";
		}
		if (this.Sib5liv!=null){
			xmlTxt+="\n<uds:SIB5LIV";
			xmlTxt+=">";
			xmlTxt+=this.Sib5liv;
			xmlTxt+="</uds:SIB5LIV>";
		}
		if (this.Sib5yod!=null){
			xmlTxt+="\n<uds:SIB5YOD";
			xmlTxt+=">";
			xmlTxt+=this.Sib5yod;
			xmlTxt+="</uds:SIB5YOD>";
		}
		if (this.Sib5dem!=null){
			xmlTxt+="\n<uds:SIB5DEM";
			xmlTxt+=">";
			xmlTxt+=this.Sib5dem;
			xmlTxt+="</uds:SIB5DEM>";
		}
		if (this.Sib5ons!=null){
			xmlTxt+="\n<uds:SIB5ONS";
			xmlTxt+=">";
			xmlTxt+=this.Sib5ons;
			xmlTxt+="</uds:SIB5ONS>";
		}
		if (this.Sib5auto!=null){
			xmlTxt+="\n<uds:SIB5AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Sib5auto;
			xmlTxt+="</uds:SIB5AUTO>";
		}
		if (this.Sib5sex!=null){
			xmlTxt+="\n<uds:SIB5SEX";
			xmlTxt+=">";
			xmlTxt+=this.Sib5sex;
			xmlTxt+="</uds:SIB5SEX>";
		}
		if (this.Sib6id!=null){
			xmlTxt+="\n<uds:SIB6ID";
			xmlTxt+=">";
			xmlTxt+=this.Sib6id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:SIB6ID>";
		}
		if (this.Sib6yob!=null){
			xmlTxt+="\n<uds:SIB6YOB";
			xmlTxt+=">";
			xmlTxt+=this.Sib6yob;
			xmlTxt+="</uds:SIB6YOB>";
		}
		if (this.Sib6liv!=null){
			xmlTxt+="\n<uds:SIB6LIV";
			xmlTxt+=">";
			xmlTxt+=this.Sib6liv;
			xmlTxt+="</uds:SIB6LIV>";
		}
		if (this.Sib6yod!=null){
			xmlTxt+="\n<uds:SIB6YOD";
			xmlTxt+=">";
			xmlTxt+=this.Sib6yod;
			xmlTxt+="</uds:SIB6YOD>";
		}
		if (this.Sib6dem!=null){
			xmlTxt+="\n<uds:SIB6DEM";
			xmlTxt+=">";
			xmlTxt+=this.Sib6dem;
			xmlTxt+="</uds:SIB6DEM>";
		}
		if (this.Sib6ons!=null){
			xmlTxt+="\n<uds:SIB6ONS";
			xmlTxt+=">";
			xmlTxt+=this.Sib6ons;
			xmlTxt+="</uds:SIB6ONS>";
		}
		if (this.Sib6auto!=null){
			xmlTxt+="\n<uds:SIB6AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Sib6auto;
			xmlTxt+="</uds:SIB6AUTO>";
		}
		if (this.Sib6sex!=null){
			xmlTxt+="\n<uds:SIB6SEX";
			xmlTxt+=">";
			xmlTxt+=this.Sib6sex;
			xmlTxt+="</uds:SIB6SEX>";
		}
		if (this.Sib7id!=null){
			xmlTxt+="\n<uds:SIB7ID";
			xmlTxt+=">";
			xmlTxt+=this.Sib7id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:SIB7ID>";
		}
		if (this.Sib7yob!=null){
			xmlTxt+="\n<uds:SIB7YOB";
			xmlTxt+=">";
			xmlTxt+=this.Sib7yob;
			xmlTxt+="</uds:SIB7YOB>";
		}
		if (this.Sib7liv!=null){
			xmlTxt+="\n<uds:SIB7LIV";
			xmlTxt+=">";
			xmlTxt+=this.Sib7liv;
			xmlTxt+="</uds:SIB7LIV>";
		}
		if (this.Sib7yod!=null){
			xmlTxt+="\n<uds:SIB7YOD";
			xmlTxt+=">";
			xmlTxt+=this.Sib7yod;
			xmlTxt+="</uds:SIB7YOD>";
		}
		if (this.Sib7dem!=null){
			xmlTxt+="\n<uds:SIB7DEM";
			xmlTxt+=">";
			xmlTxt+=this.Sib7dem;
			xmlTxt+="</uds:SIB7DEM>";
		}
		if (this.Sib7ons!=null){
			xmlTxt+="\n<uds:SIB7ONS";
			xmlTxt+=">";
			xmlTxt+=this.Sib7ons;
			xmlTxt+="</uds:SIB7ONS>";
		}
		if (this.Sib7auto!=null){
			xmlTxt+="\n<uds:SIB7AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Sib7auto;
			xmlTxt+="</uds:SIB7AUTO>";
		}
		if (this.Sib7sex!=null){
			xmlTxt+="\n<uds:SIB7SEX";
			xmlTxt+=">";
			xmlTxt+=this.Sib7sex;
			xmlTxt+="</uds:SIB7SEX>";
		}
		if (this.Sib8id!=null){
			xmlTxt+="\n<uds:SIB8ID";
			xmlTxt+=">";
			xmlTxt+=this.Sib8id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:SIB8ID>";
		}
		if (this.Sib8yob!=null){
			xmlTxt+="\n<uds:SIB8YOB";
			xmlTxt+=">";
			xmlTxt+=this.Sib8yob;
			xmlTxt+="</uds:SIB8YOB>";
		}
		if (this.Sib8liv!=null){
			xmlTxt+="\n<uds:SIB8LIV";
			xmlTxt+=">";
			xmlTxt+=this.Sib8liv;
			xmlTxt+="</uds:SIB8LIV>";
		}
		if (this.Sib8yod!=null){
			xmlTxt+="\n<uds:SIB8YOD";
			xmlTxt+=">";
			xmlTxt+=this.Sib8yod;
			xmlTxt+="</uds:SIB8YOD>";
		}
		if (this.Sib8dem!=null){
			xmlTxt+="\n<uds:SIB8DEM";
			xmlTxt+=">";
			xmlTxt+=this.Sib8dem;
			xmlTxt+="</uds:SIB8DEM>";
		}
		if (this.Sib8ons!=null){
			xmlTxt+="\n<uds:SIB8ONS";
			xmlTxt+=">";
			xmlTxt+=this.Sib8ons;
			xmlTxt+="</uds:SIB8ONS>";
		}
		if (this.Sib8auto!=null){
			xmlTxt+="\n<uds:SIB8AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Sib8auto;
			xmlTxt+="</uds:SIB8AUTO>";
		}
		if (this.Sib8sex!=null){
			xmlTxt+="\n<uds:SIB8SEX";
			xmlTxt+=">";
			xmlTxt+=this.Sib8sex;
			xmlTxt+="</uds:SIB8SEX>";
		}
		if (this.Sib9id!=null){
			xmlTxt+="\n<uds:SIB9ID";
			xmlTxt+=">";
			xmlTxt+=this.Sib9id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:SIB9ID>";
		}
		if (this.Sib9yob!=null){
			xmlTxt+="\n<uds:SIB9YOB";
			xmlTxt+=">";
			xmlTxt+=this.Sib9yob;
			xmlTxt+="</uds:SIB9YOB>";
		}
		if (this.Sib9liv!=null){
			xmlTxt+="\n<uds:SIB9LIV";
			xmlTxt+=">";
			xmlTxt+=this.Sib9liv;
			xmlTxt+="</uds:SIB9LIV>";
		}
		if (this.Sib9yod!=null){
			xmlTxt+="\n<uds:SIB9YOD";
			xmlTxt+=">";
			xmlTxt+=this.Sib9yod;
			xmlTxt+="</uds:SIB9YOD>";
		}
		if (this.Sib9dem!=null){
			xmlTxt+="\n<uds:SIB9DEM";
			xmlTxt+=">";
			xmlTxt+=this.Sib9dem;
			xmlTxt+="</uds:SIB9DEM>";
		}
		if (this.Sib9ons!=null){
			xmlTxt+="\n<uds:SIB9ONS";
			xmlTxt+=">";
			xmlTxt+=this.Sib9ons;
			xmlTxt+="</uds:SIB9ONS>";
		}
		if (this.Sib9auto!=null){
			xmlTxt+="\n<uds:SIB9AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Sib9auto;
			xmlTxt+="</uds:SIB9AUTO>";
		}
		if (this.Sib9sex!=null){
			xmlTxt+="\n<uds:SIB9SEX";
			xmlTxt+=">";
			xmlTxt+=this.Sib9sex;
			xmlTxt+="</uds:SIB9SEX>";
		}
		if (this.Sib10id!=null){
			xmlTxt+="\n<uds:SIB10ID";
			xmlTxt+=">";
			xmlTxt+=this.Sib10id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:SIB10ID>";
		}
		if (this.Sib10yob!=null){
			xmlTxt+="\n<uds:SIB10YOB";
			xmlTxt+=">";
			xmlTxt+=this.Sib10yob;
			xmlTxt+="</uds:SIB10YOB>";
		}
		if (this.Sib10liv!=null){
			xmlTxt+="\n<uds:SIB10LIV";
			xmlTxt+=">";
			xmlTxt+=this.Sib10liv;
			xmlTxt+="</uds:SIB10LIV>";
		}
		if (this.Sib10yod!=null){
			xmlTxt+="\n<uds:SIB10YOD";
			xmlTxt+=">";
			xmlTxt+=this.Sib10yod;
			xmlTxt+="</uds:SIB10YOD>";
		}
		if (this.Sib10dem!=null){
			xmlTxt+="\n<uds:SIB10DEM";
			xmlTxt+=">";
			xmlTxt+=this.Sib10dem;
			xmlTxt+="</uds:SIB10DEM>";
		}
		if (this.Sib10ons!=null){
			xmlTxt+="\n<uds:SIB10ONS";
			xmlTxt+=">";
			xmlTxt+=this.Sib10ons;
			xmlTxt+="</uds:SIB10ONS>";
		}
		if (this.Sib10auto!=null){
			xmlTxt+="\n<uds:SIB10AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Sib10auto;
			xmlTxt+="</uds:SIB10AUTO>";
		}
		if (this.Sib10sex!=null){
			xmlTxt+="\n<uds:SIB10SEX";
			xmlTxt+=">";
			xmlTxt+=this.Sib10sex;
			xmlTxt+="</uds:SIB10SEX>";
		}
		if (this.Sib11id!=null){
			xmlTxt+="\n<uds:SIB11ID";
			xmlTxt+=">";
			xmlTxt+=this.Sib11id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:SIB11ID>";
		}
		if (this.Sib11yob!=null){
			xmlTxt+="\n<uds:SIB11YOB";
			xmlTxt+=">";
			xmlTxt+=this.Sib11yob;
			xmlTxt+="</uds:SIB11YOB>";
		}
		if (this.Sib11liv!=null){
			xmlTxt+="\n<uds:SIB11LIV";
			xmlTxt+=">";
			xmlTxt+=this.Sib11liv;
			xmlTxt+="</uds:SIB11LIV>";
		}
		if (this.Sib11yod!=null){
			xmlTxt+="\n<uds:SIB11YOD";
			xmlTxt+=">";
			xmlTxt+=this.Sib11yod;
			xmlTxt+="</uds:SIB11YOD>";
		}
		if (this.Sib11dem!=null){
			xmlTxt+="\n<uds:SIB11DEM";
			xmlTxt+=">";
			xmlTxt+=this.Sib11dem;
			xmlTxt+="</uds:SIB11DEM>";
		}
		if (this.Sib11ons!=null){
			xmlTxt+="\n<uds:SIB11ONS";
			xmlTxt+=">";
			xmlTxt+=this.Sib11ons;
			xmlTxt+="</uds:SIB11ONS>";
		}
		if (this.Sib11auto!=null){
			xmlTxt+="\n<uds:SIB11AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Sib11auto;
			xmlTxt+="</uds:SIB11AUTO>";
		}
		if (this.Sib11sex!=null){
			xmlTxt+="\n<uds:SIB11SEX";
			xmlTxt+=">";
			xmlTxt+=this.Sib11sex;
			xmlTxt+="</uds:SIB11SEX>";
		}
		if (this.Sib12id!=null){
			xmlTxt+="\n<uds:SIB12ID";
			xmlTxt+=">";
			xmlTxt+=this.Sib12id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:SIB12ID>";
		}
		if (this.Sib12yob!=null){
			xmlTxt+="\n<uds:SIB12YOB";
			xmlTxt+=">";
			xmlTxt+=this.Sib12yob;
			xmlTxt+="</uds:SIB12YOB>";
		}
		if (this.Sib12liv!=null){
			xmlTxt+="\n<uds:SIB12LIV";
			xmlTxt+=">";
			xmlTxt+=this.Sib12liv;
			xmlTxt+="</uds:SIB12LIV>";
		}
		if (this.Sib12yod!=null){
			xmlTxt+="\n<uds:SIB12YOD";
			xmlTxt+=">";
			xmlTxt+=this.Sib12yod;
			xmlTxt+="</uds:SIB12YOD>";
		}
		if (this.Sib12dem!=null){
			xmlTxt+="\n<uds:SIB12DEM";
			xmlTxt+=">";
			xmlTxt+=this.Sib12dem;
			xmlTxt+="</uds:SIB12DEM>";
		}
		if (this.Sib12ons!=null){
			xmlTxt+="\n<uds:SIB12ONS";
			xmlTxt+=">";
			xmlTxt+=this.Sib12ons;
			xmlTxt+="</uds:SIB12ONS>";
		}
		if (this.Sib12auto!=null){
			xmlTxt+="\n<uds:SIB12AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Sib12auto;
			xmlTxt+="</uds:SIB12AUTO>";
		}
		if (this.Sib12sex!=null){
			xmlTxt+="\n<uds:SIB12SEX";
			xmlTxt+=">";
			xmlTxt+=this.Sib12sex;
			xmlTxt+="</uds:SIB12SEX>";
		}
		if (this.Sib13id!=null){
			xmlTxt+="\n<uds:SIB13ID";
			xmlTxt+=">";
			xmlTxt+=this.Sib13id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:SIB13ID>";
		}
		if (this.Sib13yob!=null){
			xmlTxt+="\n<uds:SIB13YOB";
			xmlTxt+=">";
			xmlTxt+=this.Sib13yob;
			xmlTxt+="</uds:SIB13YOB>";
		}
		if (this.Sib13liv!=null){
			xmlTxt+="\n<uds:SIB13LIV";
			xmlTxt+=">";
			xmlTxt+=this.Sib13liv;
			xmlTxt+="</uds:SIB13LIV>";
		}
		if (this.Sib13yod!=null){
			xmlTxt+="\n<uds:SIB13YOD";
			xmlTxt+=">";
			xmlTxt+=this.Sib13yod;
			xmlTxt+="</uds:SIB13YOD>";
		}
		if (this.Sib13dem!=null){
			xmlTxt+="\n<uds:SIB13DEM";
			xmlTxt+=">";
			xmlTxt+=this.Sib13dem;
			xmlTxt+="</uds:SIB13DEM>";
		}
		if (this.Sib13ons!=null){
			xmlTxt+="\n<uds:SIB13ONS";
			xmlTxt+=">";
			xmlTxt+=this.Sib13ons;
			xmlTxt+="</uds:SIB13ONS>";
		}
		if (this.Sib13auto!=null){
			xmlTxt+="\n<uds:SIB13AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Sib13auto;
			xmlTxt+="</uds:SIB13AUTO>";
		}
		if (this.Sib13sex!=null){
			xmlTxt+="\n<uds:SIB13SEX";
			xmlTxt+=">";
			xmlTxt+=this.Sib13sex;
			xmlTxt+="</uds:SIB13SEX>";
		}
		if (this.Sib14id!=null){
			xmlTxt+="\n<uds:SIB14ID";
			xmlTxt+=">";
			xmlTxt+=this.Sib14id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:SIB14ID>";
		}
		if (this.Sib14yob!=null){
			xmlTxt+="\n<uds:SIB14YOB";
			xmlTxt+=">";
			xmlTxt+=this.Sib14yob;
			xmlTxt+="</uds:SIB14YOB>";
		}
		if (this.Sib14liv!=null){
			xmlTxt+="\n<uds:SIB14LIV";
			xmlTxt+=">";
			xmlTxt+=this.Sib14liv;
			xmlTxt+="</uds:SIB14LIV>";
		}
		if (this.Sib14yod!=null){
			xmlTxt+="\n<uds:SIB14YOD";
			xmlTxt+=">";
			xmlTxt+=this.Sib14yod;
			xmlTxt+="</uds:SIB14YOD>";
		}
		if (this.Sib14dem!=null){
			xmlTxt+="\n<uds:SIB14DEM";
			xmlTxt+=">";
			xmlTxt+=this.Sib14dem;
			xmlTxt+="</uds:SIB14DEM>";
		}
		if (this.Sib14ons!=null){
			xmlTxt+="\n<uds:SIB14ONS";
			xmlTxt+=">";
			xmlTxt+=this.Sib14ons;
			xmlTxt+="</uds:SIB14ONS>";
		}
		if (this.Sib14auto!=null){
			xmlTxt+="\n<uds:SIB14AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Sib14auto;
			xmlTxt+="</uds:SIB14AUTO>";
		}
		if (this.Sib14sex!=null){
			xmlTxt+="\n<uds:SIB14SEX";
			xmlTxt+=">";
			xmlTxt+=this.Sib14sex;
			xmlTxt+="</uds:SIB14SEX>";
		}
		if (this.Sib15id!=null){
			xmlTxt+="\n<uds:SIB15ID";
			xmlTxt+=">";
			xmlTxt+=this.Sib15id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:SIB15ID>";
		}
		if (this.Sib15yob!=null){
			xmlTxt+="\n<uds:SIB15YOB";
			xmlTxt+=">";
			xmlTxt+=this.Sib15yob;
			xmlTxt+="</uds:SIB15YOB>";
		}
		if (this.Sib15liv!=null){
			xmlTxt+="\n<uds:SIB15LIV";
			xmlTxt+=">";
			xmlTxt+=this.Sib15liv;
			xmlTxt+="</uds:SIB15LIV>";
		}
		if (this.Sib15yod!=null){
			xmlTxt+="\n<uds:SIB15YOD";
			xmlTxt+=">";
			xmlTxt+=this.Sib15yod;
			xmlTxt+="</uds:SIB15YOD>";
		}
		if (this.Sib15dem!=null){
			xmlTxt+="\n<uds:SIB15DEM";
			xmlTxt+=">";
			xmlTxt+=this.Sib15dem;
			xmlTxt+="</uds:SIB15DEM>";
		}
		if (this.Sib15ons!=null){
			xmlTxt+="\n<uds:SIB15ONS";
			xmlTxt+=">";
			xmlTxt+=this.Sib15ons;
			xmlTxt+="</uds:SIB15ONS>";
		}
		if (this.Sib155auto!=null){
			xmlTxt+="\n<uds:SIB155AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Sib155auto;
			xmlTxt+="</uds:SIB155AUTO>";
		}
		if (this.Sib155sex!=null){
			xmlTxt+="\n<uds:SIB155SEX";
			xmlTxt+=">";
			xmlTxt+=this.Sib155sex;
			xmlTxt+="</uds:SIB155SEX>";
		}
		if (this.Sib16id!=null){
			xmlTxt+="\n<uds:SIB16ID";
			xmlTxt+=">";
			xmlTxt+=this.Sib16id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:SIB16ID>";
		}
		if (this.Sib16yob!=null){
			xmlTxt+="\n<uds:SIB16YOB";
			xmlTxt+=">";
			xmlTxt+=this.Sib16yob;
			xmlTxt+="</uds:SIB16YOB>";
		}
		if (this.Sib16liv!=null){
			xmlTxt+="\n<uds:SIB16LIV";
			xmlTxt+=">";
			xmlTxt+=this.Sib16liv;
			xmlTxt+="</uds:SIB16LIV>";
		}
		if (this.Sib16yod!=null){
			xmlTxt+="\n<uds:SIB16YOD";
			xmlTxt+=">";
			xmlTxt+=this.Sib16yod;
			xmlTxt+="</uds:SIB16YOD>";
		}
		if (this.Sib16dem!=null){
			xmlTxt+="\n<uds:SIB16DEM";
			xmlTxt+=">";
			xmlTxt+=this.Sib16dem;
			xmlTxt+="</uds:SIB16DEM>";
		}
		if (this.Sib16ons!=null){
			xmlTxt+="\n<uds:SIB16ONS";
			xmlTxt+=">";
			xmlTxt+=this.Sib16ons;
			xmlTxt+="</uds:SIB16ONS>";
		}
		if (this.Sib16auto!=null){
			xmlTxt+="\n<uds:SIB16AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Sib16auto;
			xmlTxt+="</uds:SIB16AUTO>";
		}
		if (this.Sib16sex!=null){
			xmlTxt+="\n<uds:SIB16SEX";
			xmlTxt+=">";
			xmlTxt+=this.Sib16sex;
			xmlTxt+="</uds:SIB16SEX>";
		}
		if (this.Sib17id!=null){
			xmlTxt+="\n<uds:SIB17ID";
			xmlTxt+=">";
			xmlTxt+=this.Sib17id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:SIB17ID>";
		}
		if (this.Sib17yob!=null){
			xmlTxt+="\n<uds:SIB17YOB";
			xmlTxt+=">";
			xmlTxt+=this.Sib17yob;
			xmlTxt+="</uds:SIB17YOB>";
		}
		if (this.Sib17liv!=null){
			xmlTxt+="\n<uds:SIB17LIV";
			xmlTxt+=">";
			xmlTxt+=this.Sib17liv;
			xmlTxt+="</uds:SIB17LIV>";
		}
		if (this.Sib17yod!=null){
			xmlTxt+="\n<uds:SIB17YOD";
			xmlTxt+=">";
			xmlTxt+=this.Sib17yod;
			xmlTxt+="</uds:SIB17YOD>";
		}
		if (this.Sib17dem!=null){
			xmlTxt+="\n<uds:SIB17DEM";
			xmlTxt+=">";
			xmlTxt+=this.Sib17dem;
			xmlTxt+="</uds:SIB17DEM>";
		}
		if (this.Sib17ons!=null){
			xmlTxt+="\n<uds:SIB17ONS";
			xmlTxt+=">";
			xmlTxt+=this.Sib17ons;
			xmlTxt+="</uds:SIB17ONS>";
		}
		if (this.Sib17auto!=null){
			xmlTxt+="\n<uds:SIB17AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Sib17auto;
			xmlTxt+="</uds:SIB17AUTO>";
		}
		if (this.Sib17sex!=null){
			xmlTxt+="\n<uds:SIB17SEX";
			xmlTxt+=">";
			xmlTxt+=this.Sib17sex;
			xmlTxt+="</uds:SIB17SEX>";
		}
		if (this.Sib18id!=null){
			xmlTxt+="\n<uds:SIB18ID";
			xmlTxt+=">";
			xmlTxt+=this.Sib18id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:SIB18ID>";
		}
		if (this.Sib18yob!=null){
			xmlTxt+="\n<uds:SIB18YOB";
			xmlTxt+=">";
			xmlTxt+=this.Sib18yob;
			xmlTxt+="</uds:SIB18YOB>";
		}
		if (this.Sib18liv!=null){
			xmlTxt+="\n<uds:SIB18LIV";
			xmlTxt+=">";
			xmlTxt+=this.Sib18liv;
			xmlTxt+="</uds:SIB18LIV>";
		}
		if (this.Sib18yod!=null){
			xmlTxt+="\n<uds:SIB18YOD";
			xmlTxt+=">";
			xmlTxt+=this.Sib18yod;
			xmlTxt+="</uds:SIB18YOD>";
		}
		if (this.Sib18dem!=null){
			xmlTxt+="\n<uds:SIB18DEM";
			xmlTxt+=">";
			xmlTxt+=this.Sib18dem;
			xmlTxt+="</uds:SIB18DEM>";
		}
		if (this.Sib18ons!=null){
			xmlTxt+="\n<uds:SIB18ONS";
			xmlTxt+=">";
			xmlTxt+=this.Sib18ons;
			xmlTxt+="</uds:SIB18ONS>";
		}
		if (this.Sib18auto!=null){
			xmlTxt+="\n<uds:SIB18AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Sib18auto;
			xmlTxt+="</uds:SIB18AUTO>";
		}
		if (this.Sib18sex!=null){
			xmlTxt+="\n<uds:SIB18SEX";
			xmlTxt+=">";
			xmlTxt+=this.Sib18sex;
			xmlTxt+="</uds:SIB18SEX>";
		}
		if (this.Sib19id!=null){
			xmlTxt+="\n<uds:SIB19ID";
			xmlTxt+=">";
			xmlTxt+=this.Sib19id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:SIB19ID>";
		}
		if (this.Sib19yob!=null){
			xmlTxt+="\n<uds:SIB19YOB";
			xmlTxt+=">";
			xmlTxt+=this.Sib19yob;
			xmlTxt+="</uds:SIB19YOB>";
		}
		if (this.Sib19liv!=null){
			xmlTxt+="\n<uds:SIB19LIV";
			xmlTxt+=">";
			xmlTxt+=this.Sib19liv;
			xmlTxt+="</uds:SIB19LIV>";
		}
		if (this.Sib19yod!=null){
			xmlTxt+="\n<uds:SIB19YOD";
			xmlTxt+=">";
			xmlTxt+=this.Sib19yod;
			xmlTxt+="</uds:SIB19YOD>";
		}
		if (this.Sib19dem!=null){
			xmlTxt+="\n<uds:SIB19DEM";
			xmlTxt+=">";
			xmlTxt+=this.Sib19dem;
			xmlTxt+="</uds:SIB19DEM>";
		}
		if (this.Sib19ons!=null){
			xmlTxt+="\n<uds:SIB19ONS";
			xmlTxt+=">";
			xmlTxt+=this.Sib19ons;
			xmlTxt+="</uds:SIB19ONS>";
		}
		if (this.Sib19auto!=null){
			xmlTxt+="\n<uds:SIB19AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Sib19auto;
			xmlTxt+="</uds:SIB19AUTO>";
		}
		if (this.Sib19sex!=null){
			xmlTxt+="\n<uds:SIB19SEX";
			xmlTxt+=">";
			xmlTxt+=this.Sib19sex;
			xmlTxt+="</uds:SIB19SEX>";
		}
		if (this.Sib20id!=null){
			xmlTxt+="\n<uds:SIB20ID";
			xmlTxt+=">";
			xmlTxt+=this.Sib20id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:SIB20ID>";
		}
		if (this.Sib20yob!=null){
			xmlTxt+="\n<uds:SIB20YOB";
			xmlTxt+=">";
			xmlTxt+=this.Sib20yob;
			xmlTxt+="</uds:SIB20YOB>";
		}
		if (this.Sib20liv!=null){
			xmlTxt+="\n<uds:SIB20LIV";
			xmlTxt+=">";
			xmlTxt+=this.Sib20liv;
			xmlTxt+="</uds:SIB20LIV>";
		}
		if (this.Sib20yod!=null){
			xmlTxt+="\n<uds:SIB20YOD";
			xmlTxt+=">";
			xmlTxt+=this.Sib20yod;
			xmlTxt+="</uds:SIB20YOD>";
		}
		if (this.Sib20dem!=null){
			xmlTxt+="\n<uds:SIB20DEM";
			xmlTxt+=">";
			xmlTxt+=this.Sib20dem;
			xmlTxt+="</uds:SIB20DEM>";
		}
		if (this.Sib20ons!=null){
			xmlTxt+="\n<uds:SIB20ONS";
			xmlTxt+=">";
			xmlTxt+=this.Sib20ons;
			xmlTxt+="</uds:SIB20ONS>";
		}
		if (this.Sib20auto!=null){
			xmlTxt+="\n<uds:SIB20AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Sib20auto;
			xmlTxt+="</uds:SIB20AUTO>";
		}
		if (this.Sib20sex!=null){
			xmlTxt+="\n<uds:SIB20SEX";
			xmlTxt+=">";
			xmlTxt+=this.Sib20sex;
			xmlTxt+="</uds:SIB20SEX>";
		}
		if (this.Kidchg!=null){
			xmlTxt+="\n<uds:KIDCHG";
			xmlTxt+=">";
			xmlTxt+=this.Kidchg;
			xmlTxt+="</uds:KIDCHG>";
		}
		if (this.Kids!=null){
			xmlTxt+="\n<uds:KIDS";
			xmlTxt+=">";
			xmlTxt+=this.Kids;
			xmlTxt+="</uds:KIDS>";
		}
		if (this.Kid1id!=null){
			xmlTxt+="\n<uds:KID1ID";
			xmlTxt+=">";
			xmlTxt+=this.Kid1id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:KID1ID>";
		}
		if (this.Kid1yob!=null){
			xmlTxt+="\n<uds:KID1YOB";
			xmlTxt+=">";
			xmlTxt+=this.Kid1yob;
			xmlTxt+="</uds:KID1YOB>";
		}
		if (this.Kid1liv!=null){
			xmlTxt+="\n<uds:KID1LIV";
			xmlTxt+=">";
			xmlTxt+=this.Kid1liv;
			xmlTxt+="</uds:KID1LIV>";
		}
		if (this.Kid1yod!=null){
			xmlTxt+="\n<uds:KID1YOD";
			xmlTxt+=">";
			xmlTxt+=this.Kid1yod;
			xmlTxt+="</uds:KID1YOD>";
		}
		if (this.Kid1dem!=null){
			xmlTxt+="\n<uds:KID1DEM";
			xmlTxt+=">";
			xmlTxt+=this.Kid1dem;
			xmlTxt+="</uds:KID1DEM>";
		}
		if (this.Kid1ons!=null){
			xmlTxt+="\n<uds:KID1ONS";
			xmlTxt+=">";
			xmlTxt+=this.Kid1ons;
			xmlTxt+="</uds:KID1ONS>";
		}
		if (this.Kid1auto!=null){
			xmlTxt+="\n<uds:KID1AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Kid1auto;
			xmlTxt+="</uds:KID1AUTO>";
		}
		if (this.Kid1sex!=null){
			xmlTxt+="\n<uds:KID1SEX";
			xmlTxt+=">";
			xmlTxt+=this.Kid1sex;
			xmlTxt+="</uds:KID1SEX>";
		}
		if (this.Kid2id!=null){
			xmlTxt+="\n<uds:KID2ID";
			xmlTxt+=">";
			xmlTxt+=this.Kid2id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:KID2ID>";
		}
		if (this.Kid2yob!=null){
			xmlTxt+="\n<uds:KID2YOB";
			xmlTxt+=">";
			xmlTxt+=this.Kid2yob;
			xmlTxt+="</uds:KID2YOB>";
		}
		if (this.Kid2liv!=null){
			xmlTxt+="\n<uds:KID2LIV";
			xmlTxt+=">";
			xmlTxt+=this.Kid2liv;
			xmlTxt+="</uds:KID2LIV>";
		}
		if (this.Kid2yod!=null){
			xmlTxt+="\n<uds:KID2YOD";
			xmlTxt+=">";
			xmlTxt+=this.Kid2yod;
			xmlTxt+="</uds:KID2YOD>";
		}
		if (this.Kid2dem!=null){
			xmlTxt+="\n<uds:KID2DEM";
			xmlTxt+=">";
			xmlTxt+=this.Kid2dem;
			xmlTxt+="</uds:KID2DEM>";
		}
		if (this.Kid2ons!=null){
			xmlTxt+="\n<uds:KID2ONS";
			xmlTxt+=">";
			xmlTxt+=this.Kid2ons;
			xmlTxt+="</uds:KID2ONS>";
		}
		if (this.Kid2auto!=null){
			xmlTxt+="\n<uds:KID2AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Kid2auto;
			xmlTxt+="</uds:KID2AUTO>";
		}
		if (this.Kid2sex!=null){
			xmlTxt+="\n<uds:KID2SEX";
			xmlTxt+=">";
			xmlTxt+=this.Kid2sex;
			xmlTxt+="</uds:KID2SEX>";
		}
		if (this.Kid3id!=null){
			xmlTxt+="\n<uds:KID3ID";
			xmlTxt+=">";
			xmlTxt+=this.Kid3id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:KID3ID>";
		}
		if (this.Kid3yob!=null){
			xmlTxt+="\n<uds:KID3YOB";
			xmlTxt+=">";
			xmlTxt+=this.Kid3yob;
			xmlTxt+="</uds:KID3YOB>";
		}
		if (this.Kid3liv!=null){
			xmlTxt+="\n<uds:KID3LIV";
			xmlTxt+=">";
			xmlTxt+=this.Kid3liv;
			xmlTxt+="</uds:KID3LIV>";
		}
		if (this.Kid3yod!=null){
			xmlTxt+="\n<uds:KID3YOD";
			xmlTxt+=">";
			xmlTxt+=this.Kid3yod;
			xmlTxt+="</uds:KID3YOD>";
		}
		if (this.Kid3dem!=null){
			xmlTxt+="\n<uds:KID3DEM";
			xmlTxt+=">";
			xmlTxt+=this.Kid3dem;
			xmlTxt+="</uds:KID3DEM>";
		}
		if (this.Kid3ons!=null){
			xmlTxt+="\n<uds:KID3ONS";
			xmlTxt+=">";
			xmlTxt+=this.Kid3ons;
			xmlTxt+="</uds:KID3ONS>";
		}
		if (this.Kid3auto!=null){
			xmlTxt+="\n<uds:KID3AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Kid3auto;
			xmlTxt+="</uds:KID3AUTO>";
		}
		if (this.Kid3sex!=null){
			xmlTxt+="\n<uds:KID3SEX";
			xmlTxt+=">";
			xmlTxt+=this.Kid3sex;
			xmlTxt+="</uds:KID3SEX>";
		}
		if (this.Kid4id!=null){
			xmlTxt+="\n<uds:KID4ID";
			xmlTxt+=">";
			xmlTxt+=this.Kid4id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:KID4ID>";
		}
		if (this.Kid4yob!=null){
			xmlTxt+="\n<uds:KID4YOB";
			xmlTxt+=">";
			xmlTxt+=this.Kid4yob;
			xmlTxt+="</uds:KID4YOB>";
		}
		if (this.Kid4liv!=null){
			xmlTxt+="\n<uds:KID4LIV";
			xmlTxt+=">";
			xmlTxt+=this.Kid4liv;
			xmlTxt+="</uds:KID4LIV>";
		}
		if (this.Kid4yod!=null){
			xmlTxt+="\n<uds:KID4YOD";
			xmlTxt+=">";
			xmlTxt+=this.Kid4yod;
			xmlTxt+="</uds:KID4YOD>";
		}
		if (this.Kid4dem!=null){
			xmlTxt+="\n<uds:KID4DEM";
			xmlTxt+=">";
			xmlTxt+=this.Kid4dem;
			xmlTxt+="</uds:KID4DEM>";
		}
		if (this.Kid4ons!=null){
			xmlTxt+="\n<uds:KID4ONS";
			xmlTxt+=">";
			xmlTxt+=this.Kid4ons;
			xmlTxt+="</uds:KID4ONS>";
		}
		if (this.Kid4auto!=null){
			xmlTxt+="\n<uds:KID4AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Kid4auto;
			xmlTxt+="</uds:KID4AUTO>";
		}
		if (this.Kid4sex!=null){
			xmlTxt+="\n<uds:KID4SEX";
			xmlTxt+=">";
			xmlTxt+=this.Kid4sex;
			xmlTxt+="</uds:KID4SEX>";
		}
		if (this.Kid5id!=null){
			xmlTxt+="\n<uds:KID5ID";
			xmlTxt+=">";
			xmlTxt+=this.Kid5id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:KID5ID>";
		}
		if (this.Kid5yob!=null){
			xmlTxt+="\n<uds:KID5YOB";
			xmlTxt+=">";
			xmlTxt+=this.Kid5yob;
			xmlTxt+="</uds:KID5YOB>";
		}
		if (this.Kid5liv!=null){
			xmlTxt+="\n<uds:KID5LIV";
			xmlTxt+=">";
			xmlTxt+=this.Kid5liv;
			xmlTxt+="</uds:KID5LIV>";
		}
		if (this.Kid5yod!=null){
			xmlTxt+="\n<uds:KID5YOD";
			xmlTxt+=">";
			xmlTxt+=this.Kid5yod;
			xmlTxt+="</uds:KID5YOD>";
		}
		if (this.Kid5dem!=null){
			xmlTxt+="\n<uds:KID5DEM";
			xmlTxt+=">";
			xmlTxt+=this.Kid5dem;
			xmlTxt+="</uds:KID5DEM>";
		}
		if (this.Kid5ons!=null){
			xmlTxt+="\n<uds:KID5ONS";
			xmlTxt+=">";
			xmlTxt+=this.Kid5ons;
			xmlTxt+="</uds:KID5ONS>";
		}
		if (this.Kid5auto!=null){
			xmlTxt+="\n<uds:KID5AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Kid5auto;
			xmlTxt+="</uds:KID5AUTO>";
		}
		if (this.Kid5sex!=null){
			xmlTxt+="\n<uds:KID5SEX";
			xmlTxt+=">";
			xmlTxt+=this.Kid5sex;
			xmlTxt+="</uds:KID5SEX>";
		}
		if (this.Kid6id!=null){
			xmlTxt+="\n<uds:KID6ID";
			xmlTxt+=">";
			xmlTxt+=this.Kid6id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:KID6ID>";
		}
		if (this.Kid6yob!=null){
			xmlTxt+="\n<uds:KID6YOB";
			xmlTxt+=">";
			xmlTxt+=this.Kid6yob;
			xmlTxt+="</uds:KID6YOB>";
		}
		if (this.Kid6liv!=null){
			xmlTxt+="\n<uds:KID6LIV";
			xmlTxt+=">";
			xmlTxt+=this.Kid6liv;
			xmlTxt+="</uds:KID6LIV>";
		}
		if (this.Kid6yod!=null){
			xmlTxt+="\n<uds:KID6YOD";
			xmlTxt+=">";
			xmlTxt+=this.Kid6yod;
			xmlTxt+="</uds:KID6YOD>";
		}
		if (this.Kid6dem!=null){
			xmlTxt+="\n<uds:KID6DEM";
			xmlTxt+=">";
			xmlTxt+=this.Kid6dem;
			xmlTxt+="</uds:KID6DEM>";
		}
		if (this.Kid6ons!=null){
			xmlTxt+="\n<uds:KID6ONS";
			xmlTxt+=">";
			xmlTxt+=this.Kid6ons;
			xmlTxt+="</uds:KID6ONS>";
		}
		if (this.Kid6auto!=null){
			xmlTxt+="\n<uds:KID6AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Kid6auto;
			xmlTxt+="</uds:KID6AUTO>";
		}
		if (this.Kid6sex!=null){
			xmlTxt+="\n<uds:KID6SEX";
			xmlTxt+=">";
			xmlTxt+=this.Kid6sex;
			xmlTxt+="</uds:KID6SEX>";
		}
		if (this.Kid7id!=null){
			xmlTxt+="\n<uds:KID7ID";
			xmlTxt+=">";
			xmlTxt+=this.Kid7id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:KID7ID>";
		}
		if (this.Kid7yob!=null){
			xmlTxt+="\n<uds:KID7YOB";
			xmlTxt+=">";
			xmlTxt+=this.Kid7yob;
			xmlTxt+="</uds:KID7YOB>";
		}
		if (this.Kid7liv!=null){
			xmlTxt+="\n<uds:KID7LIV";
			xmlTxt+=">";
			xmlTxt+=this.Kid7liv;
			xmlTxt+="</uds:KID7LIV>";
		}
		if (this.Kid7yod!=null){
			xmlTxt+="\n<uds:KID7YOD";
			xmlTxt+=">";
			xmlTxt+=this.Kid7yod;
			xmlTxt+="</uds:KID7YOD>";
		}
		if (this.Kid7dem!=null){
			xmlTxt+="\n<uds:KID7DEM";
			xmlTxt+=">";
			xmlTxt+=this.Kid7dem;
			xmlTxt+="</uds:KID7DEM>";
		}
		if (this.Kid7ons!=null){
			xmlTxt+="\n<uds:KID7ONS";
			xmlTxt+=">";
			xmlTxt+=this.Kid7ons;
			xmlTxt+="</uds:KID7ONS>";
		}
		if (this.Kid7auto!=null){
			xmlTxt+="\n<uds:KID7AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Kid7auto;
			xmlTxt+="</uds:KID7AUTO>";
		}
		if (this.Kid7sex!=null){
			xmlTxt+="\n<uds:KID7SEX";
			xmlTxt+=">";
			xmlTxt+=this.Kid7sex;
			xmlTxt+="</uds:KID7SEX>";
		}
		if (this.Kid8id!=null){
			xmlTxt+="\n<uds:KID8ID";
			xmlTxt+=">";
			xmlTxt+=this.Kid8id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:KID8ID>";
		}
		if (this.Kid8yob!=null){
			xmlTxt+="\n<uds:KID8YOB";
			xmlTxt+=">";
			xmlTxt+=this.Kid8yob;
			xmlTxt+="</uds:KID8YOB>";
		}
		if (this.Kid8liv!=null){
			xmlTxt+="\n<uds:KID8LIV";
			xmlTxt+=">";
			xmlTxt+=this.Kid8liv;
			xmlTxt+="</uds:KID8LIV>";
		}
		if (this.Kid8yod!=null){
			xmlTxt+="\n<uds:KID8YOD";
			xmlTxt+=">";
			xmlTxt+=this.Kid8yod;
			xmlTxt+="</uds:KID8YOD>";
		}
		if (this.Kid8dem!=null){
			xmlTxt+="\n<uds:KID8DEM";
			xmlTxt+=">";
			xmlTxt+=this.Kid8dem;
			xmlTxt+="</uds:KID8DEM>";
		}
		if (this.Kid8ons!=null){
			xmlTxt+="\n<uds:KID8ONS";
			xmlTxt+=">";
			xmlTxt+=this.Kid8ons;
			xmlTxt+="</uds:KID8ONS>";
		}
		if (this.Kid8auto!=null){
			xmlTxt+="\n<uds:KID8AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Kid8auto;
			xmlTxt+="</uds:KID8AUTO>";
		}
		if (this.Kid8sex!=null){
			xmlTxt+="\n<uds:KID8SEX";
			xmlTxt+=">";
			xmlTxt+=this.Kid8sex;
			xmlTxt+="</uds:KID8SEX>";
		}
		if (this.Kid9id!=null){
			xmlTxt+="\n<uds:KID9ID";
			xmlTxt+=">";
			xmlTxt+=this.Kid9id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:KID9ID>";
		}
		if (this.Kid9yob!=null){
			xmlTxt+="\n<uds:KID9YOB";
			xmlTxt+=">";
			xmlTxt+=this.Kid9yob;
			xmlTxt+="</uds:KID9YOB>";
		}
		if (this.Kid9liv!=null){
			xmlTxt+="\n<uds:KID9LIV";
			xmlTxt+=">";
			xmlTxt+=this.Kid9liv;
			xmlTxt+="</uds:KID9LIV>";
		}
		if (this.Kid9yod!=null){
			xmlTxt+="\n<uds:KID9YOD";
			xmlTxt+=">";
			xmlTxt+=this.Kid9yod;
			xmlTxt+="</uds:KID9YOD>";
		}
		if (this.Kid9dem!=null){
			xmlTxt+="\n<uds:KID9DEM";
			xmlTxt+=">";
			xmlTxt+=this.Kid9dem;
			xmlTxt+="</uds:KID9DEM>";
		}
		if (this.Kid9ons!=null){
			xmlTxt+="\n<uds:KID9ONS";
			xmlTxt+=">";
			xmlTxt+=this.Kid9ons;
			xmlTxt+="</uds:KID9ONS>";
		}
		if (this.Kid9auto!=null){
			xmlTxt+="\n<uds:KID9AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Kid9auto;
			xmlTxt+="</uds:KID9AUTO>";
		}
		if (this.Kid9sex!=null){
			xmlTxt+="\n<uds:KID9SEX";
			xmlTxt+=">";
			xmlTxt+=this.Kid9sex;
			xmlTxt+="</uds:KID9SEX>";
		}
		if (this.Kid10id!=null){
			xmlTxt+="\n<uds:KID10ID";
			xmlTxt+=">";
			xmlTxt+=this.Kid10id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:KID10ID>";
		}
		if (this.Kid10yob!=null){
			xmlTxt+="\n<uds:KID10YOB";
			xmlTxt+=">";
			xmlTxt+=this.Kid10yob;
			xmlTxt+="</uds:KID10YOB>";
		}
		if (this.Kid10liv!=null){
			xmlTxt+="\n<uds:KID10LIV";
			xmlTxt+=">";
			xmlTxt+=this.Kid10liv;
			xmlTxt+="</uds:KID10LIV>";
		}
		if (this.Kid10yod!=null){
			xmlTxt+="\n<uds:KID10YOD";
			xmlTxt+=">";
			xmlTxt+=this.Kid10yod;
			xmlTxt+="</uds:KID10YOD>";
		}
		if (this.Kid10dem!=null){
			xmlTxt+="\n<uds:KID10DEM";
			xmlTxt+=">";
			xmlTxt+=this.Kid10dem;
			xmlTxt+="</uds:KID10DEM>";
		}
		if (this.Kid10ons!=null){
			xmlTxt+="\n<uds:KID10ONS";
			xmlTxt+=">";
			xmlTxt+=this.Kid10ons;
			xmlTxt+="</uds:KID10ONS>";
		}
		if (this.Kid10auto!=null){
			xmlTxt+="\n<uds:KID10AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Kid10auto;
			xmlTxt+="</uds:KID10AUTO>";
		}
		if (this.Kid10sex!=null){
			xmlTxt+="\n<uds:KID10SEX";
			xmlTxt+=">";
			xmlTxt+=this.Kid10sex;
			xmlTxt+="</uds:KID10SEX>";
		}
		if (this.Kid11id!=null){
			xmlTxt+="\n<uds:KID11ID";
			xmlTxt+=">";
			xmlTxt+=this.Kid11id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:KID11ID>";
		}
		if (this.Kid11yob!=null){
			xmlTxt+="\n<uds:KID11YOB";
			xmlTxt+=">";
			xmlTxt+=this.Kid11yob;
			xmlTxt+="</uds:KID11YOB>";
		}
		if (this.Kid11liv!=null){
			xmlTxt+="\n<uds:KID11LIV";
			xmlTxt+=">";
			xmlTxt+=this.Kid11liv;
			xmlTxt+="</uds:KID11LIV>";
		}
		if (this.Kid11yod!=null){
			xmlTxt+="\n<uds:KID11YOD";
			xmlTxt+=">";
			xmlTxt+=this.Kid11yod;
			xmlTxt+="</uds:KID11YOD>";
		}
		if (this.Kid11dem!=null){
			xmlTxt+="\n<uds:KID11DEM";
			xmlTxt+=">";
			xmlTxt+=this.Kid11dem;
			xmlTxt+="</uds:KID11DEM>";
		}
		if (this.Kid11ons!=null){
			xmlTxt+="\n<uds:KID11ONS";
			xmlTxt+=">";
			xmlTxt+=this.Kid11ons;
			xmlTxt+="</uds:KID11ONS>";
		}
		if (this.Kid11auto!=null){
			xmlTxt+="\n<uds:KID11AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Kid11auto;
			xmlTxt+="</uds:KID11AUTO>";
		}
		if (this.Kid11sex!=null){
			xmlTxt+="\n<uds:KID11SEX";
			xmlTxt+=">";
			xmlTxt+=this.Kid11sex;
			xmlTxt+="</uds:KID11SEX>";
		}
		if (this.Kid12id!=null){
			xmlTxt+="\n<uds:KID12ID";
			xmlTxt+=">";
			xmlTxt+=this.Kid12id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:KID12ID>";
		}
		if (this.Kid12yob!=null){
			xmlTxt+="\n<uds:KID12YOB";
			xmlTxt+=">";
			xmlTxt+=this.Kid12yob;
			xmlTxt+="</uds:KID12YOB>";
		}
		if (this.Kid12liv!=null){
			xmlTxt+="\n<uds:KID12LIV";
			xmlTxt+=">";
			xmlTxt+=this.Kid12liv;
			xmlTxt+="</uds:KID12LIV>";
		}
		if (this.Kid12yod!=null){
			xmlTxt+="\n<uds:KID12YOD";
			xmlTxt+=">";
			xmlTxt+=this.Kid12yod;
			xmlTxt+="</uds:KID12YOD>";
		}
		if (this.Kid12dem!=null){
			xmlTxt+="\n<uds:KID12DEM";
			xmlTxt+=">";
			xmlTxt+=this.Kid12dem;
			xmlTxt+="</uds:KID12DEM>";
		}
		if (this.Kid12ons!=null){
			xmlTxt+="\n<uds:KID12ONS";
			xmlTxt+=">";
			xmlTxt+=this.Kid12ons;
			xmlTxt+="</uds:KID12ONS>";
		}
		if (this.Kid12auto!=null){
			xmlTxt+="\n<uds:KID12AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Kid12auto;
			xmlTxt+="</uds:KID12AUTO>";
		}
		if (this.Kid12sex!=null){
			xmlTxt+="\n<uds:KID12SEX";
			xmlTxt+=">";
			xmlTxt+=this.Kid12sex;
			xmlTxt+="</uds:KID12SEX>";
		}
		if (this.Kid13id!=null){
			xmlTxt+="\n<uds:KID13ID";
			xmlTxt+=">";
			xmlTxt+=this.Kid13id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:KID13ID>";
		}
		if (this.Kid13yob!=null){
			xmlTxt+="\n<uds:KID13YOB";
			xmlTxt+=">";
			xmlTxt+=this.Kid13yob;
			xmlTxt+="</uds:KID13YOB>";
		}
		if (this.Kid13liv!=null){
			xmlTxt+="\n<uds:KID13LIV";
			xmlTxt+=">";
			xmlTxt+=this.Kid13liv;
			xmlTxt+="</uds:KID13LIV>";
		}
		if (this.Kid13yod!=null){
			xmlTxt+="\n<uds:KID13YOD";
			xmlTxt+=">";
			xmlTxt+=this.Kid13yod;
			xmlTxt+="</uds:KID13YOD>";
		}
		if (this.Kid13dem!=null){
			xmlTxt+="\n<uds:KID13DEM";
			xmlTxt+=">";
			xmlTxt+=this.Kid13dem;
			xmlTxt+="</uds:KID13DEM>";
		}
		if (this.Kid13ons!=null){
			xmlTxt+="\n<uds:KID13ONS";
			xmlTxt+=">";
			xmlTxt+=this.Kid13ons;
			xmlTxt+="</uds:KID13ONS>";
		}
		if (this.Kid13auto!=null){
			xmlTxt+="\n<uds:KID13AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Kid13auto;
			xmlTxt+="</uds:KID13AUTO>";
		}
		if (this.Kid13sex!=null){
			xmlTxt+="\n<uds:KID13SEX";
			xmlTxt+=">";
			xmlTxt+=this.Kid13sex;
			xmlTxt+="</uds:KID13SEX>";
		}
		if (this.Kid14id!=null){
			xmlTxt+="\n<uds:KID14ID";
			xmlTxt+=">";
			xmlTxt+=this.Kid14id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:KID14ID>";
		}
		if (this.Kid14yob!=null){
			xmlTxt+="\n<uds:KID14YOB";
			xmlTxt+=">";
			xmlTxt+=this.Kid14yob;
			xmlTxt+="</uds:KID14YOB>";
		}
		if (this.Kid14liv!=null){
			xmlTxt+="\n<uds:KID14LIV";
			xmlTxt+=">";
			xmlTxt+=this.Kid14liv;
			xmlTxt+="</uds:KID14LIV>";
		}
		if (this.Kid14yod!=null){
			xmlTxt+="\n<uds:KID14YOD";
			xmlTxt+=">";
			xmlTxt+=this.Kid14yod;
			xmlTxt+="</uds:KID14YOD>";
		}
		if (this.Kid14dem!=null){
			xmlTxt+="\n<uds:KID14DEM";
			xmlTxt+=">";
			xmlTxt+=this.Kid14dem;
			xmlTxt+="</uds:KID14DEM>";
		}
		if (this.Kid14ons!=null){
			xmlTxt+="\n<uds:KID14ONS";
			xmlTxt+=">";
			xmlTxt+=this.Kid14ons;
			xmlTxt+="</uds:KID14ONS>";
		}
		if (this.Kid14auto!=null){
			xmlTxt+="\n<uds:KID14AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Kid14auto;
			xmlTxt+="</uds:KID14AUTO>";
		}
		if (this.Kid14sex!=null){
			xmlTxt+="\n<uds:KID14SEX";
			xmlTxt+=">";
			xmlTxt+=this.Kid14sex;
			xmlTxt+="</uds:KID14SEX>";
		}
		if (this.Kid15id!=null){
			xmlTxt+="\n<uds:KID15ID";
			xmlTxt+=">";
			xmlTxt+=this.Kid15id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:KID15ID>";
		}
		if (this.Kid15yob!=null){
			xmlTxt+="\n<uds:KID15YOB";
			xmlTxt+=">";
			xmlTxt+=this.Kid15yob;
			xmlTxt+="</uds:KID15YOB>";
		}
		if (this.Kid15liv!=null){
			xmlTxt+="\n<uds:KID15LIV";
			xmlTxt+=">";
			xmlTxt+=this.Kid15liv;
			xmlTxt+="</uds:KID15LIV>";
		}
		if (this.Kid15yod!=null){
			xmlTxt+="\n<uds:KID15YOD";
			xmlTxt+=">";
			xmlTxt+=this.Kid15yod;
			xmlTxt+="</uds:KID15YOD>";
		}
		if (this.Kid15dem!=null){
			xmlTxt+="\n<uds:KID15DEM";
			xmlTxt+=">";
			xmlTxt+=this.Kid15dem;
			xmlTxt+="</uds:KID15DEM>";
		}
		if (this.Kid15ons!=null){
			xmlTxt+="\n<uds:KID15ONS";
			xmlTxt+=">";
			xmlTxt+=this.Kid15ons;
			xmlTxt+="</uds:KID15ONS>";
		}
		if (this.Kid15auto!=null){
			xmlTxt+="\n<uds:KID15AUTO";
			xmlTxt+=">";
			xmlTxt+=this.Kid15auto;
			xmlTxt+="</uds:KID15AUTO>";
		}
		if (this.Kid15sex!=null){
			xmlTxt+="\n<uds:KID15SEX";
			xmlTxt+=">";
			xmlTxt+=this.Kid15sex;
			xmlTxt+="</uds:KID15SEX>";
		}
		if (this.Relchg!=null){
			xmlTxt+="\n<uds:RELCHG";
			xmlTxt+=">";
			xmlTxt+=this.Relchg;
			xmlTxt+="</uds:RELCHG>";
		}
		if (this.Relsdem!=null){
			xmlTxt+="\n<uds:RELSDEM";
			xmlTxt+=">";
			xmlTxt+=this.Relsdem;
			xmlTxt+="</uds:RELSDEM>";
		}
		if (this.Rel1id!=null){
			xmlTxt+="\n<uds:REL1ID";
			xmlTxt+=">";
			xmlTxt+=this.Rel1id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:REL1ID>";
		}
		if (this.Rel1yob!=null){
			xmlTxt+="\n<uds:REL1YOB";
			xmlTxt+=">";
			xmlTxt+=this.Rel1yob;
			xmlTxt+="</uds:REL1YOB>";
		}
		if (this.Rel1liv!=null){
			xmlTxt+="\n<uds:REL1LIV";
			xmlTxt+=">";
			xmlTxt+=this.Rel1liv;
			xmlTxt+="</uds:REL1LIV>";
		}
		if (this.Rel1yod!=null){
			xmlTxt+="\n<uds:REL1YOD";
			xmlTxt+=">";
			xmlTxt+=this.Rel1yod;
			xmlTxt+="</uds:REL1YOD>";
		}
		if (this.Rel1ons!=null){
			xmlTxt+="\n<uds:REL1ONS";
			xmlTxt+=">";
			xmlTxt+=this.Rel1ons;
			xmlTxt+="</uds:REL1ONS>";
		}
		if (this.Rel2id!=null){
			xmlTxt+="\n<uds:REL2ID";
			xmlTxt+=">";
			xmlTxt+=this.Rel2id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:REL2ID>";
		}
		if (this.Rel2yob!=null){
			xmlTxt+="\n<uds:REL2YOB";
			xmlTxt+=">";
			xmlTxt+=this.Rel2yob;
			xmlTxt+="</uds:REL2YOB>";
		}
		if (this.Rel2liv!=null){
			xmlTxt+="\n<uds:REL2LIV";
			xmlTxt+=">";
			xmlTxt+=this.Rel2liv;
			xmlTxt+="</uds:REL2LIV>";
		}
		if (this.Rel2yod!=null){
			xmlTxt+="\n<uds:REL2YOD";
			xmlTxt+=">";
			xmlTxt+=this.Rel2yod;
			xmlTxt+="</uds:REL2YOD>";
		}
		if (this.Rel2ons!=null){
			xmlTxt+="\n<uds:REL2ONS";
			xmlTxt+=">";
			xmlTxt+=this.Rel2ons;
			xmlTxt+="</uds:REL2ONS>";
		}
		if (this.Rel3id!=null){
			xmlTxt+="\n<uds:REL3ID";
			xmlTxt+=">";
			xmlTxt+=this.Rel3id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:REL3ID>";
		}
		if (this.Rel3yob!=null){
			xmlTxt+="\n<uds:REL3YOB";
			xmlTxt+=">";
			xmlTxt+=this.Rel3yob;
			xmlTxt+="</uds:REL3YOB>";
		}
		if (this.Rel3liv!=null){
			xmlTxt+="\n<uds:REL3LIV";
			xmlTxt+=">";
			xmlTxt+=this.Rel3liv;
			xmlTxt+="</uds:REL3LIV>";
		}
		if (this.Rel3yod!=null){
			xmlTxt+="\n<uds:REL3YOD";
			xmlTxt+=">";
			xmlTxt+=this.Rel3yod;
			xmlTxt+="</uds:REL3YOD>";
		}
		if (this.Rel3ons!=null){
			xmlTxt+="\n<uds:REL3ONS";
			xmlTxt+=">";
			xmlTxt+=this.Rel3ons;
			xmlTxt+="</uds:REL3ONS>";
		}
		if (this.Rel4id!=null){
			xmlTxt+="\n<uds:REL4ID";
			xmlTxt+=">";
			xmlTxt+=this.Rel4id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:REL4ID>";
		}
		if (this.Rel4yob!=null){
			xmlTxt+="\n<uds:REL4YOB";
			xmlTxt+=">";
			xmlTxt+=this.Rel4yob;
			xmlTxt+="</uds:REL4YOB>";
		}
		if (this.Rel4liv!=null){
			xmlTxt+="\n<uds:REL4LIV";
			xmlTxt+=">";
			xmlTxt+=this.Rel4liv;
			xmlTxt+="</uds:REL4LIV>";
		}
		if (this.Rel4yod!=null){
			xmlTxt+="\n<uds:REL4YOD";
			xmlTxt+=">";
			xmlTxt+=this.Rel4yod;
			xmlTxt+="</uds:REL4YOD>";
		}
		if (this.Rel4ons!=null){
			xmlTxt+="\n<uds:REL4ONS";
			xmlTxt+=">";
			xmlTxt+=this.Rel4ons;
			xmlTxt+="</uds:REL4ONS>";
		}
		if (this.Rel5id!=null){
			xmlTxt+="\n<uds:REL5ID";
			xmlTxt+=">";
			xmlTxt+=this.Rel5id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:REL5ID>";
		}
		if (this.Rel5yob!=null){
			xmlTxt+="\n<uds:REL5YOB";
			xmlTxt+=">";
			xmlTxt+=this.Rel5yob;
			xmlTxt+="</uds:REL5YOB>";
		}
		if (this.Rel5liv!=null){
			xmlTxt+="\n<uds:REL5LIV";
			xmlTxt+=">";
			xmlTxt+=this.Rel5liv;
			xmlTxt+="</uds:REL5LIV>";
		}
		if (this.Rel5yod!=null){
			xmlTxt+="\n<uds:REL5YOD";
			xmlTxt+=">";
			xmlTxt+=this.Rel5yod;
			xmlTxt+="</uds:REL5YOD>";
		}
		if (this.Rel5ons!=null){
			xmlTxt+="\n<uds:REL5ONS";
			xmlTxt+=">";
			xmlTxt+=this.Rel5ons;
			xmlTxt+="</uds:REL5ONS>";
		}
		if (this.Rel6id!=null){
			xmlTxt+="\n<uds:REL6ID";
			xmlTxt+=">";
			xmlTxt+=this.Rel6id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:REL6ID>";
		}
		if (this.Rel6yob!=null){
			xmlTxt+="\n<uds:REL6YOB";
			xmlTxt+=">";
			xmlTxt+=this.Rel6yob;
			xmlTxt+="</uds:REL6YOB>";
		}
		if (this.Rel6liv!=null){
			xmlTxt+="\n<uds:REL6LIV";
			xmlTxt+=">";
			xmlTxt+=this.Rel6liv;
			xmlTxt+="</uds:REL6LIV>";
		}
		if (this.Rel6yod!=null){
			xmlTxt+="\n<uds:REL6YOD";
			xmlTxt+=">";
			xmlTxt+=this.Rel6yod;
			xmlTxt+="</uds:REL6YOD>";
		}
		if (this.Rel6ons!=null){
			xmlTxt+="\n<uds:REL6ONS";
			xmlTxt+=">";
			xmlTxt+=this.Rel6ons;
			xmlTxt+="</uds:REL6ONS>";
		}
		if (this.Rel7id!=null){
			xmlTxt+="\n<uds:REL7ID";
			xmlTxt+=">";
			xmlTxt+=this.Rel7id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:REL7ID>";
		}
		if (this.Rel7yob!=null){
			xmlTxt+="\n<uds:REL7YOB";
			xmlTxt+=">";
			xmlTxt+=this.Rel7yob;
			xmlTxt+="</uds:REL7YOB>";
		}
		if (this.Rel7liv!=null){
			xmlTxt+="\n<uds:REL7LIV";
			xmlTxt+=">";
			xmlTxt+=this.Rel7liv;
			xmlTxt+="</uds:REL7LIV>";
		}
		if (this.Rel7yod!=null){
			xmlTxt+="\n<uds:REL7YOD";
			xmlTxt+=">";
			xmlTxt+=this.Rel7yod;
			xmlTxt+="</uds:REL7YOD>";
		}
		if (this.Rel7ons!=null){
			xmlTxt+="\n<uds:REL7ONS";
			xmlTxt+=">";
			xmlTxt+=this.Rel7ons;
			xmlTxt+="</uds:REL7ONS>";
		}
		if (this.Rel8id!=null){
			xmlTxt+="\n<uds:REL8ID";
			xmlTxt+=">";
			xmlTxt+=this.Rel8id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:REL8ID>";
		}
		if (this.Rel8yob!=null){
			xmlTxt+="\n<uds:REL8YOB";
			xmlTxt+=">";
			xmlTxt+=this.Rel8yob;
			xmlTxt+="</uds:REL8YOB>";
		}
		if (this.Rel8liv!=null){
			xmlTxt+="\n<uds:REL8LIV";
			xmlTxt+=">";
			xmlTxt+=this.Rel8liv;
			xmlTxt+="</uds:REL8LIV>";
		}
		if (this.Rel8yod!=null){
			xmlTxt+="\n<uds:REL8YOD";
			xmlTxt+=">";
			xmlTxt+=this.Rel8yod;
			xmlTxt+="</uds:REL8YOD>";
		}
		if (this.Rel8ons!=null){
			xmlTxt+="\n<uds:REL8ONS";
			xmlTxt+=">";
			xmlTxt+=this.Rel8ons;
			xmlTxt+="</uds:REL8ONS>";
		}
		if (this.Rel9id!=null){
			xmlTxt+="\n<uds:REL9ID";
			xmlTxt+=">";
			xmlTxt+=this.Rel9id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:REL9ID>";
		}
		if (this.Rel9yob!=null){
			xmlTxt+="\n<uds:REL9YOB";
			xmlTxt+=">";
			xmlTxt+=this.Rel9yob;
			xmlTxt+="</uds:REL9YOB>";
		}
		if (this.Rel9liv!=null){
			xmlTxt+="\n<uds:REL9LIV";
			xmlTxt+=">";
			xmlTxt+=this.Rel9liv;
			xmlTxt+="</uds:REL9LIV>";
		}
		if (this.Rel9yod!=null){
			xmlTxt+="\n<uds:REL9YOD";
			xmlTxt+=">";
			xmlTxt+=this.Rel9yod;
			xmlTxt+="</uds:REL9YOD>";
		}
		if (this.Rel9ons!=null){
			xmlTxt+="\n<uds:REL9ONS";
			xmlTxt+=">";
			xmlTxt+=this.Rel9ons;
			xmlTxt+="</uds:REL9ONS>";
		}
		if (this.Rel10id!=null){
			xmlTxt+="\n<uds:REL10ID";
			xmlTxt+=">";
			xmlTxt+=this.Rel10id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:REL10ID>";
		}
		if (this.Rel10yob!=null){
			xmlTxt+="\n<uds:REL10YOB";
			xmlTxt+=">";
			xmlTxt+=this.Rel10yob;
			xmlTxt+="</uds:REL10YOB>";
		}
		if (this.Rel10liv!=null){
			xmlTxt+="\n<uds:REL10LIV";
			xmlTxt+=">";
			xmlTxt+=this.Rel10liv;
			xmlTxt+="</uds:REL10LIV>";
		}
		if (this.Rel10yod!=null){
			xmlTxt+="\n<uds:REL10YOD";
			xmlTxt+=">";
			xmlTxt+=this.Rel10yod;
			xmlTxt+="</uds:REL10YOD>";
		}
		if (this.Rel10ons!=null){
			xmlTxt+="\n<uds:REL10ONS";
			xmlTxt+=">";
			xmlTxt+=this.Rel10ons;
			xmlTxt+="</uds:REL10ONS>";
		}
		if (this.Rel11id!=null){
			xmlTxt+="\n<uds:REL11ID";
			xmlTxt+=">";
			xmlTxt+=this.Rel11id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:REL11ID>";
		}
		if (this.Rel11yob!=null){
			xmlTxt+="\n<uds:REL11YOB";
			xmlTxt+=">";
			xmlTxt+=this.Rel11yob;
			xmlTxt+="</uds:REL11YOB>";
		}
		if (this.Rel11liv!=null){
			xmlTxt+="\n<uds:REL11LIV";
			xmlTxt+=">";
			xmlTxt+=this.Rel11liv;
			xmlTxt+="</uds:REL11LIV>";
		}
		if (this.Rel11yod!=null){
			xmlTxt+="\n<uds:REL11YOD";
			xmlTxt+=">";
			xmlTxt+=this.Rel11yod;
			xmlTxt+="</uds:REL11YOD>";
		}
		if (this.Rel11ons!=null){
			xmlTxt+="\n<uds:REL11ONS";
			xmlTxt+=">";
			xmlTxt+=this.Rel11ons;
			xmlTxt+="</uds:REL11ONS>";
		}
		if (this.Rel12id!=null){
			xmlTxt+="\n<uds:REL12ID";
			xmlTxt+=">";
			xmlTxt+=this.Rel12id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:REL12ID>";
		}
		if (this.Rel12yob!=null){
			xmlTxt+="\n<uds:REL12YOB";
			xmlTxt+=">";
			xmlTxt+=this.Rel12yob;
			xmlTxt+="</uds:REL12YOB>";
		}
		if (this.Rel12liv!=null){
			xmlTxt+="\n<uds:REL12LIV";
			xmlTxt+=">";
			xmlTxt+=this.Rel12liv;
			xmlTxt+="</uds:REL12LIV>";
		}
		if (this.Rel12yod!=null){
			xmlTxt+="\n<uds:REL12YOD";
			xmlTxt+=">";
			xmlTxt+=this.Rel12yod;
			xmlTxt+="</uds:REL12YOD>";
		}
		if (this.Rel12ons!=null){
			xmlTxt+="\n<uds:REL12ONS";
			xmlTxt+=">";
			xmlTxt+=this.Rel12ons;
			xmlTxt+="</uds:REL12ONS>";
		}
		if (this.Rel13id!=null){
			xmlTxt+="\n<uds:REL13ID";
			xmlTxt+=">";
			xmlTxt+=this.Rel13id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:REL13ID>";
		}
		if (this.Rel13yob!=null){
			xmlTxt+="\n<uds:REL13YOB";
			xmlTxt+=">";
			xmlTxt+=this.Rel13yob;
			xmlTxt+="</uds:REL13YOB>";
		}
		if (this.Rel13liv!=null){
			xmlTxt+="\n<uds:REL13LIV";
			xmlTxt+=">";
			xmlTxt+=this.Rel13liv;
			xmlTxt+="</uds:REL13LIV>";
		}
		if (this.Rel13yod!=null){
			xmlTxt+="\n<uds:REL13YOD";
			xmlTxt+=">";
			xmlTxt+=this.Rel13yod;
			xmlTxt+="</uds:REL13YOD>";
		}
		if (this.Rel13ons!=null){
			xmlTxt+="\n<uds:REL13ONS";
			xmlTxt+=">";
			xmlTxt+=this.Rel13ons;
			xmlTxt+="</uds:REL13ONS>";
		}
		if (this.Rel14id!=null){
			xmlTxt+="\n<uds:REL14ID";
			xmlTxt+=">";
			xmlTxt+=this.Rel14id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:REL14ID>";
		}
		if (this.Rel14yob!=null){
			xmlTxt+="\n<uds:REL14YOB";
			xmlTxt+=">";
			xmlTxt+=this.Rel14yob;
			xmlTxt+="</uds:REL14YOB>";
		}
		if (this.Rel14liv!=null){
			xmlTxt+="\n<uds:REL14LIV";
			xmlTxt+=">";
			xmlTxt+=this.Rel14liv;
			xmlTxt+="</uds:REL14LIV>";
		}
		if (this.Rel14yod!=null){
			xmlTxt+="\n<uds:REL14YOD";
			xmlTxt+=">";
			xmlTxt+=this.Rel14yod;
			xmlTxt+="</uds:REL14YOD>";
		}
		if (this.Rel14ons!=null){
			xmlTxt+="\n<uds:REL14ONS";
			xmlTxt+=">";
			xmlTxt+=this.Rel14ons;
			xmlTxt+="</uds:REL14ONS>";
		}
		if (this.Rel15id!=null){
			xmlTxt+="\n<uds:REL15ID";
			xmlTxt+=">";
			xmlTxt+=this.Rel15id.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:REL15ID>";
		}
		if (this.Rel15yob!=null){
			xmlTxt+="\n<uds:REL15YOB";
			xmlTxt+=">";
			xmlTxt+=this.Rel15yob;
			xmlTxt+="</uds:REL15YOB>";
		}
		if (this.Rel15liv!=null){
			xmlTxt+="\n<uds:REL15LIV";
			xmlTxt+=">";
			xmlTxt+=this.Rel15liv;
			xmlTxt+="</uds:REL15LIV>";
		}
		if (this.Rel15yod!=null){
			xmlTxt+="\n<uds:REL15YOD";
			xmlTxt+=">";
			xmlTxt+=this.Rel15yod;
			xmlTxt+="</uds:REL15YOD>";
		}
		if (this.Rel15ons!=null){
			xmlTxt+="\n<uds:REL15ONS";
			xmlTxt+=">";
			xmlTxt+=this.Rel15ons;
			xmlTxt+="</uds:REL15ONS>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Examdate!=null) return true;
		if (this.A3chg!=null) return true;
		if (this.Parchg!=null) return true;
		if (this.Familyid!=null) return true;
		if (this.Gmomid!=null) return true;
		if (this.Gmomyob!=null) return true;
		if (this.Gmomliv!=null) return true;
		if (this.Gmomyod!=null) return true;
		if (this.Gmomdem!=null) return true;
		if (this.Gmomonset!=null) return true;
		if (this.Gmomauto!=null) return true;
		if (this.Gdadid!=null) return true;
		if (this.Gdadyob!=null) return true;
		if (this.Gdadliv!=null) return true;
		if (this.Gdadyod!=null) return true;
		if (this.Gdaddem!=null) return true;
		if (this.Gdadonset!=null) return true;
		if (this.Gdadauto!=null) return true;
		if (this.Momid!=null) return true;
		if (this.Momyob!=null) return true;
		if (this.Momliv!=null) return true;
		if (this.Momyod!=null) return true;
		if (this.Momdem!=null) return true;
		if (this.Momonset!=null) return true;
		if (this.Momauto!=null) return true;
		if (this.Dadid!=null) return true;
		if (this.Dadyob!=null) return true;
		if (this.Dadliv!=null) return true;
		if (this.Dadyod!=null) return true;
		if (this.Daddem!=null) return true;
		if (this.Dadonset!=null) return true;
		if (this.Dadauto!=null) return true;
		if (this.Sibchg!=null) return true;
		if (this.Twin!=null) return true;
		if (this.Twintype!=null) return true;
		if (this.Sibs!=null) return true;
		if (this.Sib1id!=null) return true;
		if (this.Sib1yob!=null) return true;
		if (this.Sib1liv!=null) return true;
		if (this.Sib1yod!=null) return true;
		if (this.Sib1dem!=null) return true;
		if (this.Sib1ons!=null) return true;
		if (this.Sib1auto!=null) return true;
		if (this.Sib1sex!=null) return true;
		if (this.Sib2id!=null) return true;
		if (this.Sib2yob!=null) return true;
		if (this.Sib2liv!=null) return true;
		if (this.Sib2yod!=null) return true;
		if (this.Sib2dem!=null) return true;
		if (this.Sib2ons!=null) return true;
		if (this.Sib2auto!=null) return true;
		if (this.Sib2sex!=null) return true;
		if (this.Sib3id!=null) return true;
		if (this.Sib3yob!=null) return true;
		if (this.Sib3liv!=null) return true;
		if (this.Sib3yod!=null) return true;
		if (this.Sib3dem!=null) return true;
		if (this.Sib3ons!=null) return true;
		if (this.Sib3auto!=null) return true;
		if (this.Sib3sex!=null) return true;
		if (this.Sib4id!=null) return true;
		if (this.Sib4yob!=null) return true;
		if (this.Sib4liv!=null) return true;
		if (this.Sib4yod!=null) return true;
		if (this.Sib4dem!=null) return true;
		if (this.Sib4ons!=null) return true;
		if (this.Sib4auto!=null) return true;
		if (this.Sib4sex!=null) return true;
		if (this.Sib5id!=null) return true;
		if (this.Sib5yob!=null) return true;
		if (this.Sib5liv!=null) return true;
		if (this.Sib5yod!=null) return true;
		if (this.Sib5dem!=null) return true;
		if (this.Sib5ons!=null) return true;
		if (this.Sib5auto!=null) return true;
		if (this.Sib5sex!=null) return true;
		if (this.Sib6id!=null) return true;
		if (this.Sib6yob!=null) return true;
		if (this.Sib6liv!=null) return true;
		if (this.Sib6yod!=null) return true;
		if (this.Sib6dem!=null) return true;
		if (this.Sib6ons!=null) return true;
		if (this.Sib6auto!=null) return true;
		if (this.Sib6sex!=null) return true;
		if (this.Sib7id!=null) return true;
		if (this.Sib7yob!=null) return true;
		if (this.Sib7liv!=null) return true;
		if (this.Sib7yod!=null) return true;
		if (this.Sib7dem!=null) return true;
		if (this.Sib7ons!=null) return true;
		if (this.Sib7auto!=null) return true;
		if (this.Sib7sex!=null) return true;
		if (this.Sib8id!=null) return true;
		if (this.Sib8yob!=null) return true;
		if (this.Sib8liv!=null) return true;
		if (this.Sib8yod!=null) return true;
		if (this.Sib8dem!=null) return true;
		if (this.Sib8ons!=null) return true;
		if (this.Sib8auto!=null) return true;
		if (this.Sib8sex!=null) return true;
		if (this.Sib9id!=null) return true;
		if (this.Sib9yob!=null) return true;
		if (this.Sib9liv!=null) return true;
		if (this.Sib9yod!=null) return true;
		if (this.Sib9dem!=null) return true;
		if (this.Sib9ons!=null) return true;
		if (this.Sib9auto!=null) return true;
		if (this.Sib9sex!=null) return true;
		if (this.Sib10id!=null) return true;
		if (this.Sib10yob!=null) return true;
		if (this.Sib10liv!=null) return true;
		if (this.Sib10yod!=null) return true;
		if (this.Sib10dem!=null) return true;
		if (this.Sib10ons!=null) return true;
		if (this.Sib10auto!=null) return true;
		if (this.Sib10sex!=null) return true;
		if (this.Sib11id!=null) return true;
		if (this.Sib11yob!=null) return true;
		if (this.Sib11liv!=null) return true;
		if (this.Sib11yod!=null) return true;
		if (this.Sib11dem!=null) return true;
		if (this.Sib11ons!=null) return true;
		if (this.Sib11auto!=null) return true;
		if (this.Sib11sex!=null) return true;
		if (this.Sib12id!=null) return true;
		if (this.Sib12yob!=null) return true;
		if (this.Sib12liv!=null) return true;
		if (this.Sib12yod!=null) return true;
		if (this.Sib12dem!=null) return true;
		if (this.Sib12ons!=null) return true;
		if (this.Sib12auto!=null) return true;
		if (this.Sib12sex!=null) return true;
		if (this.Sib13id!=null) return true;
		if (this.Sib13yob!=null) return true;
		if (this.Sib13liv!=null) return true;
		if (this.Sib13yod!=null) return true;
		if (this.Sib13dem!=null) return true;
		if (this.Sib13ons!=null) return true;
		if (this.Sib13auto!=null) return true;
		if (this.Sib13sex!=null) return true;
		if (this.Sib14id!=null) return true;
		if (this.Sib14yob!=null) return true;
		if (this.Sib14liv!=null) return true;
		if (this.Sib14yod!=null) return true;
		if (this.Sib14dem!=null) return true;
		if (this.Sib14ons!=null) return true;
		if (this.Sib14auto!=null) return true;
		if (this.Sib14sex!=null) return true;
		if (this.Sib15id!=null) return true;
		if (this.Sib15yob!=null) return true;
		if (this.Sib15liv!=null) return true;
		if (this.Sib15yod!=null) return true;
		if (this.Sib15dem!=null) return true;
		if (this.Sib15ons!=null) return true;
		if (this.Sib155auto!=null) return true;
		if (this.Sib155sex!=null) return true;
		if (this.Sib16id!=null) return true;
		if (this.Sib16yob!=null) return true;
		if (this.Sib16liv!=null) return true;
		if (this.Sib16yod!=null) return true;
		if (this.Sib16dem!=null) return true;
		if (this.Sib16ons!=null) return true;
		if (this.Sib16auto!=null) return true;
		if (this.Sib16sex!=null) return true;
		if (this.Sib17id!=null) return true;
		if (this.Sib17yob!=null) return true;
		if (this.Sib17liv!=null) return true;
		if (this.Sib17yod!=null) return true;
		if (this.Sib17dem!=null) return true;
		if (this.Sib17ons!=null) return true;
		if (this.Sib17auto!=null) return true;
		if (this.Sib17sex!=null) return true;
		if (this.Sib18id!=null) return true;
		if (this.Sib18yob!=null) return true;
		if (this.Sib18liv!=null) return true;
		if (this.Sib18yod!=null) return true;
		if (this.Sib18dem!=null) return true;
		if (this.Sib18ons!=null) return true;
		if (this.Sib18auto!=null) return true;
		if (this.Sib18sex!=null) return true;
		if (this.Sib19id!=null) return true;
		if (this.Sib19yob!=null) return true;
		if (this.Sib19liv!=null) return true;
		if (this.Sib19yod!=null) return true;
		if (this.Sib19dem!=null) return true;
		if (this.Sib19ons!=null) return true;
		if (this.Sib19auto!=null) return true;
		if (this.Sib19sex!=null) return true;
		if (this.Sib20id!=null) return true;
		if (this.Sib20yob!=null) return true;
		if (this.Sib20liv!=null) return true;
		if (this.Sib20yod!=null) return true;
		if (this.Sib20dem!=null) return true;
		if (this.Sib20ons!=null) return true;
		if (this.Sib20auto!=null) return true;
		if (this.Sib20sex!=null) return true;
		if (this.Kidchg!=null) return true;
		if (this.Kids!=null) return true;
		if (this.Kid1id!=null) return true;
		if (this.Kid1yob!=null) return true;
		if (this.Kid1liv!=null) return true;
		if (this.Kid1yod!=null) return true;
		if (this.Kid1dem!=null) return true;
		if (this.Kid1ons!=null) return true;
		if (this.Kid1auto!=null) return true;
		if (this.Kid1sex!=null) return true;
		if (this.Kid2id!=null) return true;
		if (this.Kid2yob!=null) return true;
		if (this.Kid2liv!=null) return true;
		if (this.Kid2yod!=null) return true;
		if (this.Kid2dem!=null) return true;
		if (this.Kid2ons!=null) return true;
		if (this.Kid2auto!=null) return true;
		if (this.Kid2sex!=null) return true;
		if (this.Kid3id!=null) return true;
		if (this.Kid3yob!=null) return true;
		if (this.Kid3liv!=null) return true;
		if (this.Kid3yod!=null) return true;
		if (this.Kid3dem!=null) return true;
		if (this.Kid3ons!=null) return true;
		if (this.Kid3auto!=null) return true;
		if (this.Kid3sex!=null) return true;
		if (this.Kid4id!=null) return true;
		if (this.Kid4yob!=null) return true;
		if (this.Kid4liv!=null) return true;
		if (this.Kid4yod!=null) return true;
		if (this.Kid4dem!=null) return true;
		if (this.Kid4ons!=null) return true;
		if (this.Kid4auto!=null) return true;
		if (this.Kid4sex!=null) return true;
		if (this.Kid5id!=null) return true;
		if (this.Kid5yob!=null) return true;
		if (this.Kid5liv!=null) return true;
		if (this.Kid5yod!=null) return true;
		if (this.Kid5dem!=null) return true;
		if (this.Kid5ons!=null) return true;
		if (this.Kid5auto!=null) return true;
		if (this.Kid5sex!=null) return true;
		if (this.Kid6id!=null) return true;
		if (this.Kid6yob!=null) return true;
		if (this.Kid6liv!=null) return true;
		if (this.Kid6yod!=null) return true;
		if (this.Kid6dem!=null) return true;
		if (this.Kid6ons!=null) return true;
		if (this.Kid6auto!=null) return true;
		if (this.Kid6sex!=null) return true;
		if (this.Kid7id!=null) return true;
		if (this.Kid7yob!=null) return true;
		if (this.Kid7liv!=null) return true;
		if (this.Kid7yod!=null) return true;
		if (this.Kid7dem!=null) return true;
		if (this.Kid7ons!=null) return true;
		if (this.Kid7auto!=null) return true;
		if (this.Kid7sex!=null) return true;
		if (this.Kid8id!=null) return true;
		if (this.Kid8yob!=null) return true;
		if (this.Kid8liv!=null) return true;
		if (this.Kid8yod!=null) return true;
		if (this.Kid8dem!=null) return true;
		if (this.Kid8ons!=null) return true;
		if (this.Kid8auto!=null) return true;
		if (this.Kid8sex!=null) return true;
		if (this.Kid9id!=null) return true;
		if (this.Kid9yob!=null) return true;
		if (this.Kid9liv!=null) return true;
		if (this.Kid9yod!=null) return true;
		if (this.Kid9dem!=null) return true;
		if (this.Kid9ons!=null) return true;
		if (this.Kid9auto!=null) return true;
		if (this.Kid9sex!=null) return true;
		if (this.Kid10id!=null) return true;
		if (this.Kid10yob!=null) return true;
		if (this.Kid10liv!=null) return true;
		if (this.Kid10yod!=null) return true;
		if (this.Kid10dem!=null) return true;
		if (this.Kid10ons!=null) return true;
		if (this.Kid10auto!=null) return true;
		if (this.Kid10sex!=null) return true;
		if (this.Kid11id!=null) return true;
		if (this.Kid11yob!=null) return true;
		if (this.Kid11liv!=null) return true;
		if (this.Kid11yod!=null) return true;
		if (this.Kid11dem!=null) return true;
		if (this.Kid11ons!=null) return true;
		if (this.Kid11auto!=null) return true;
		if (this.Kid11sex!=null) return true;
		if (this.Kid12id!=null) return true;
		if (this.Kid12yob!=null) return true;
		if (this.Kid12liv!=null) return true;
		if (this.Kid12yod!=null) return true;
		if (this.Kid12dem!=null) return true;
		if (this.Kid12ons!=null) return true;
		if (this.Kid12auto!=null) return true;
		if (this.Kid12sex!=null) return true;
		if (this.Kid13id!=null) return true;
		if (this.Kid13yob!=null) return true;
		if (this.Kid13liv!=null) return true;
		if (this.Kid13yod!=null) return true;
		if (this.Kid13dem!=null) return true;
		if (this.Kid13ons!=null) return true;
		if (this.Kid13auto!=null) return true;
		if (this.Kid13sex!=null) return true;
		if (this.Kid14id!=null) return true;
		if (this.Kid14yob!=null) return true;
		if (this.Kid14liv!=null) return true;
		if (this.Kid14yod!=null) return true;
		if (this.Kid14dem!=null) return true;
		if (this.Kid14ons!=null) return true;
		if (this.Kid14auto!=null) return true;
		if (this.Kid14sex!=null) return true;
		if (this.Kid15id!=null) return true;
		if (this.Kid15yob!=null) return true;
		if (this.Kid15liv!=null) return true;
		if (this.Kid15yod!=null) return true;
		if (this.Kid15dem!=null) return true;
		if (this.Kid15ons!=null) return true;
		if (this.Kid15auto!=null) return true;
		if (this.Kid15sex!=null) return true;
		if (this.Relchg!=null) return true;
		if (this.Relsdem!=null) return true;
		if (this.Rel1id!=null) return true;
		if (this.Rel1yob!=null) return true;
		if (this.Rel1liv!=null) return true;
		if (this.Rel1yod!=null) return true;
		if (this.Rel1ons!=null) return true;
		if (this.Rel2id!=null) return true;
		if (this.Rel2yob!=null) return true;
		if (this.Rel2liv!=null) return true;
		if (this.Rel2yod!=null) return true;
		if (this.Rel2ons!=null) return true;
		if (this.Rel3id!=null) return true;
		if (this.Rel3yob!=null) return true;
		if (this.Rel3liv!=null) return true;
		if (this.Rel3yod!=null) return true;
		if (this.Rel3ons!=null) return true;
		if (this.Rel4id!=null) return true;
		if (this.Rel4yob!=null) return true;
		if (this.Rel4liv!=null) return true;
		if (this.Rel4yod!=null) return true;
		if (this.Rel4ons!=null) return true;
		if (this.Rel5id!=null) return true;
		if (this.Rel5yob!=null) return true;
		if (this.Rel5liv!=null) return true;
		if (this.Rel5yod!=null) return true;
		if (this.Rel5ons!=null) return true;
		if (this.Rel6id!=null) return true;
		if (this.Rel6yob!=null) return true;
		if (this.Rel6liv!=null) return true;
		if (this.Rel6yod!=null) return true;
		if (this.Rel6ons!=null) return true;
		if (this.Rel7id!=null) return true;
		if (this.Rel7yob!=null) return true;
		if (this.Rel7liv!=null) return true;
		if (this.Rel7yod!=null) return true;
		if (this.Rel7ons!=null) return true;
		if (this.Rel8id!=null) return true;
		if (this.Rel8yob!=null) return true;
		if (this.Rel8liv!=null) return true;
		if (this.Rel8yod!=null) return true;
		if (this.Rel8ons!=null) return true;
		if (this.Rel9id!=null) return true;
		if (this.Rel9yob!=null) return true;
		if (this.Rel9liv!=null) return true;
		if (this.Rel9yod!=null) return true;
		if (this.Rel9ons!=null) return true;
		if (this.Rel10id!=null) return true;
		if (this.Rel10yob!=null) return true;
		if (this.Rel10liv!=null) return true;
		if (this.Rel10yod!=null) return true;
		if (this.Rel10ons!=null) return true;
		if (this.Rel11id!=null) return true;
		if (this.Rel11yob!=null) return true;
		if (this.Rel11liv!=null) return true;
		if (this.Rel11yod!=null) return true;
		if (this.Rel11ons!=null) return true;
		if (this.Rel12id!=null) return true;
		if (this.Rel12yob!=null) return true;
		if (this.Rel12liv!=null) return true;
		if (this.Rel12yod!=null) return true;
		if (this.Rel12ons!=null) return true;
		if (this.Rel13id!=null) return true;
		if (this.Rel13yob!=null) return true;
		if (this.Rel13liv!=null) return true;
		if (this.Rel13yod!=null) return true;
		if (this.Rel13ons!=null) return true;
		if (this.Rel14id!=null) return true;
		if (this.Rel14yob!=null) return true;
		if (this.Rel14liv!=null) return true;
		if (this.Rel14yod!=null) return true;
		if (this.Rel14ons!=null) return true;
		if (this.Rel15id!=null) return true;
		if (this.Rel15yob!=null) return true;
		if (this.Rel15liv!=null) return true;
		if (this.Rel15yod!=null) return true;
		if (this.Rel15ons!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
