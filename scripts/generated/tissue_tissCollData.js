/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function tissue_tissCollData(){
this.xsiType="tissue:tissCollData";

	this.getSchemaElementName=function(){
		return "tissCollData";
	}

	this.getFullSchemaElementName=function(){
		return "tissue:tissCollData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Tisstype=null;


	function getTisstype() {
		return this.Tisstype;
	}
	this.getTisstype=getTisstype;


	function setTisstype(v){
		this.Tisstype=v;
	}
	this.setTisstype=setTisstype;

	this.Collector=null;


	function getCollector() {
		return this.Collector;
	}
	this.getCollector=getCollector;


	function setCollector(v){
		this.Collector=v;
	}
	this.setCollector=setCollector;

	this.Method=null;


	function getMethod() {
		return this.Method;
	}
	this.getMethod=getMethod;


	function setMethod(v){
		this.Method=v;
	}
	this.setMethod=setMethod;

	this.Complicsbool=null;


	function getComplicsbool() {
		return this.Complicsbool;
	}
	this.getComplicsbool=getComplicsbool;


	function setComplicsbool(v){
		this.Complicsbool=v;
	}
	this.setComplicsbool=setComplicsbool;


	this.isComplicsbool=function(defaultValue) {
		if(this.Complicsbool==null)return defaultValue;
		if(this.Complicsbool=="1" || this.Complicsbool==true)return true;
		return false;
	}

	this.Complicsdetails=null;


	function getComplicsdetails() {
		return this.Complicsdetails;
	}
	this.getComplicsdetails=getComplicsdetails;


	function setComplicsdetails(v){
		this.Complicsdetails=v;
	}
	this.setComplicsdetails=setComplicsdetails;

	this.Altlabel1=null;


	function getAltlabel1() {
		return this.Altlabel1;
	}
	this.getAltlabel1=getAltlabel1;


	function setAltlabel1(v){
		this.Altlabel1=v;
	}
	this.setAltlabel1=setAltlabel1;

	this.Altlabel1ref=null;


	function getAltlabel1ref() {
		return this.Altlabel1ref;
	}
	this.getAltlabel1ref=getAltlabel1ref;


	function setAltlabel1ref(v){
		this.Altlabel1ref=v;
	}
	this.setAltlabel1ref=setAltlabel1ref;

	this.Altlabel2=null;


	function getAltlabel2() {
		return this.Altlabel2;
	}
	this.getAltlabel2=getAltlabel2;


	function setAltlabel2(v){
		this.Altlabel2=v;
	}
	this.setAltlabel2=setAltlabel2;

	this.Altlabel2ref=null;


	function getAltlabel2ref() {
		return this.Altlabel2ref;
	}
	this.getAltlabel2ref=getAltlabel2ref;


	function setAltlabel2ref(v){
		this.Altlabel2ref=v;
	}
	this.setAltlabel2ref=setAltlabel2ref;

	this.Altlabel3=null;


	function getAltlabel3() {
		return this.Altlabel3;
	}
	this.getAltlabel3=getAltlabel3;


	function setAltlabel3(v){
		this.Altlabel3=v;
	}
	this.setAltlabel3=setAltlabel3;

	this.Altlabel3ref=null;


	function getAltlabel3ref() {
		return this.Altlabel3ref;
	}
	this.getAltlabel3ref=getAltlabel3ref;


	function setAltlabel3ref(v){
		this.Altlabel3ref=v;
	}
	this.setAltlabel3ref=setAltlabel3ref;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="tissType"){
				return this.Tisstype ;
			} else 
			if(xmlPath=="collector"){
				return this.Collector ;
			} else 
			if(xmlPath=="method"){
				return this.Method ;
			} else 
			if(xmlPath=="complicsBool"){
				return this.Complicsbool ;
			} else 
			if(xmlPath=="complicsDetails"){
				return this.Complicsdetails ;
			} else 
			if(xmlPath=="altLabel1"){
				return this.Altlabel1 ;
			} else 
			if(xmlPath=="altLabel1Ref"){
				return this.Altlabel1ref ;
			} else 
			if(xmlPath=="altLabel2"){
				return this.Altlabel2 ;
			} else 
			if(xmlPath=="altLabel2Ref"){
				return this.Altlabel2ref ;
			} else 
			if(xmlPath=="altLabel3"){
				return this.Altlabel3 ;
			} else 
			if(xmlPath=="altLabel3Ref"){
				return this.Altlabel3ref ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="tissType"){
				this.Tisstype=value;
			} else 
			if(xmlPath=="collector"){
				this.Collector=value;
			} else 
			if(xmlPath=="method"){
				this.Method=value;
			} else 
			if(xmlPath=="complicsBool"){
				this.Complicsbool=value;
			} else 
			if(xmlPath=="complicsDetails"){
				this.Complicsdetails=value;
			} else 
			if(xmlPath=="altLabel1"){
				this.Altlabel1=value;
			} else 
			if(xmlPath=="altLabel1Ref"){
				this.Altlabel1ref=value;
			} else 
			if(xmlPath=="altLabel2"){
				this.Altlabel2=value;
			} else 
			if(xmlPath=="altLabel2Ref"){
				this.Altlabel2ref=value;
			} else 
			if(xmlPath=="altLabel3"){
				this.Altlabel3=value;
			} else 
			if(xmlPath=="altLabel3Ref"){
				this.Altlabel3ref=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="tissType"){
			return "field_data";
		}else if (xmlPath=="collector"){
			return "field_data";
		}else if (xmlPath=="method"){
			return "field_data";
		}else if (xmlPath=="complicsBool"){
			return "field_data";
		}else if (xmlPath=="complicsDetails"){
			return "field_LONG_DATA";
		}else if (xmlPath=="altLabel1"){
			return "field_data";
		}else if (xmlPath=="altLabel1Ref"){
			return "field_data";
		}else if (xmlPath=="altLabel2"){
			return "field_data";
		}else if (xmlPath=="altLabel2Ref"){
			return "field_data";
		}else if (xmlPath=="altLabel3"){
			return "field_data";
		}else if (xmlPath=="altLabel3Ref"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<tissue:tissCollData";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</tissue:tissCollData>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Tisstype!=null){
			xmlTxt+="\n<tissue:tissType";
			xmlTxt+=">";
			xmlTxt+=this.Tisstype.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tissue:tissType>";
		}
		if (this.Collector!=null){
			xmlTxt+="\n<tissue:collector";
			xmlTxt+=">";
			xmlTxt+=this.Collector.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tissue:collector>";
		}
		if (this.Method!=null){
			xmlTxt+="\n<tissue:method";
			xmlTxt+=">";
			xmlTxt+=this.Method.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tissue:method>";
		}
		if (this.Complicsbool!=null){
			xmlTxt+="\n<tissue:complicsBool";
			xmlTxt+=">";
			xmlTxt+=this.Complicsbool;
			xmlTxt+="</tissue:complicsBool>";
		}
		if (this.Complicsdetails!=null){
			xmlTxt+="\n<tissue:complicsDetails";
			xmlTxt+=">";
			xmlTxt+=this.Complicsdetails.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tissue:complicsDetails>";
		}
		if (this.Altlabel1!=null){
			xmlTxt+="\n<tissue:altLabel1";
			xmlTxt+=">";
			xmlTxt+=this.Altlabel1.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tissue:altLabel1>";
		}
		if (this.Altlabel1ref!=null){
			xmlTxt+="\n<tissue:altLabel1Ref";
			xmlTxt+=">";
			xmlTxt+=this.Altlabel1ref.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tissue:altLabel1Ref>";
		}
		if (this.Altlabel2!=null){
			xmlTxt+="\n<tissue:altLabel2";
			xmlTxt+=">";
			xmlTxt+=this.Altlabel2.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tissue:altLabel2>";
		}
		if (this.Altlabel2ref!=null){
			xmlTxt+="\n<tissue:altLabel2Ref";
			xmlTxt+=">";
			xmlTxt+=this.Altlabel2ref.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tissue:altLabel2Ref>";
		}
		if (this.Altlabel3!=null){
			xmlTxt+="\n<tissue:altLabel3";
			xmlTxt+=">";
			xmlTxt+=this.Altlabel3.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tissue:altLabel3>";
		}
		if (this.Altlabel3ref!=null){
			xmlTxt+="\n<tissue:altLabel3Ref";
			xmlTxt+=">";
			xmlTxt+=this.Altlabel3ref.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tissue:altLabel3Ref>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Tisstype!=null) return true;
		if (this.Collector!=null) return true;
		if (this.Method!=null) return true;
		if (this.Complicsbool!=null) return true;
		if (this.Complicsdetails!=null) return true;
		if (this.Altlabel1!=null) return true;
		if (this.Altlabel1ref!=null) return true;
		if (this.Altlabel2!=null) return true;
		if (this.Altlabel2ref!=null) return true;
		if (this.Altlabel3!=null) return true;
		if (this.Altlabel3ref!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
