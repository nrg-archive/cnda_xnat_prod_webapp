/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:04 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function condr_brainPathData(){
this.xsiType="condr:brainPathData";

	this.getSchemaElementName=function(){
		return "brainPathData";
	}

	this.getFullSchemaElementName=function(){
		return "condr:brainPathData";
	}
this.extension=dynamicJSLoad('tissue_labResultData','generated/tissue_labResultData.js');

	this.Tumortype=null;


	function getTumortype() {
		return this.Tumortype;
	}
	this.getTumortype=getTumortype;


	function setTumortype(v){
		this.Tumortype=v;
	}
	this.setTumortype=setTumortype;

	this.Primarytumortype=null;


	function getPrimarytumortype() {
		return this.Primarytumortype;
	}
	this.getPrimarytumortype=getPrimarytumortype;


	function setPrimarytumortype(v){
		this.Primarytumortype=v;
	}
	this.setPrimarytumortype=setPrimarytumortype;

	this.Primarywhograde=null;


	function getPrimarywhograde() {
		return this.Primarywhograde;
	}
	this.getPrimarywhograde=getPrimarywhograde;


	function setPrimarywhograde(v){
		this.Primarywhograde=v;
	}
	this.setPrimarywhograde=setPrimarywhograde;

	this.Metastatorig=null;


	function getMetastatorig() {
		return this.Metastatorig;
	}
	this.getMetastatorig=getMetastatorig;


	function setMetastatorig(v){
		this.Metastatorig=v;
	}
	this.setMetastatorig=setMetastatorig;

	this.Onep19qdeletions=null;


	function getOnep19qdeletions() {
		return this.Onep19qdeletions;
	}
	this.getOnep19qdeletions=getOnep19qdeletions;


	function setOnep19qdeletions(v){
		this.Onep19qdeletions=v;
	}
	this.setOnep19qdeletions=setOnep19qdeletions;

	this.Mgmtpromoterstatus=null;


	function getMgmtpromoterstatus() {
		return this.Mgmtpromoterstatus;
	}
	this.getMgmtpromoterstatus=getMgmtpromoterstatus;


	function setMgmtpromoterstatus(v){
		this.Mgmtpromoterstatus=v;
	}
	this.setMgmtpromoterstatus=setMgmtpromoterstatus;

	this.Mib1index=null;


	function getMib1index() {
		return this.Mib1index;
	}
	this.getMib1index=getMib1index;


	function setMib1index(v){
		this.Mib1index=v;
	}
	this.setMib1index=setMib1index;

	this.Mib1indexnotreported=null;


	function getMib1indexnotreported() {
		return this.Mib1indexnotreported;
	}
	this.getMib1indexnotreported=getMib1indexnotreported;


	function setMib1indexnotreported(v){
		this.Mib1indexnotreported=v;
	}
	this.setMib1indexnotreported=setMib1indexnotreported;


	this.isMib1indexnotreported=function(defaultValue) {
		if(this.Mib1indexnotreported==null)return defaultValue;
		if(this.Mib1indexnotreported=="1" || this.Mib1indexnotreported==true)return true;
		return false;
	}

	this.Pten=null;


	function getPten() {
		return this.Pten;
	}
	this.getPten=getPten;


	function setPten(v){
		this.Pten=v;
	}
	this.setPten=setPten;

	this.P53=null;


	function getP53() {
		return this.P53;
	}
	this.getP53=getP53;


	function setP53(v){
		this.P53=v;
	}
	this.setP53=setP53;

	this.Othfeat=null;


	function getOthfeat() {
		return this.Othfeat;
	}
	this.getOthfeat=getOthfeat;


	function setOthfeat(v){
		this.Othfeat=v;
	}
	this.setOthfeat=setOthfeat;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="labResultData"){
				return this.Labresultdata ;
			} else 
			if(xmlPath.startsWith("labResultData")){
				xmlPath=xmlPath.substring(13);
				if(xmlPath=="")return this.Labresultdata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Labresultdata!=undefined)return this.Labresultdata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="tumorType"){
				return this.Tumortype ;
			} else 
			if(xmlPath=="primaryTumorType"){
				return this.Primarytumortype ;
			} else 
			if(xmlPath=="primaryWHOGrade"){
				return this.Primarywhograde ;
			} else 
			if(xmlPath=="metastatOrig"){
				return this.Metastatorig ;
			} else 
			if(xmlPath=="onep19qDeletions"){
				return this.Onep19qdeletions ;
			} else 
			if(xmlPath=="mgmtPromoterStatus"){
				return this.Mgmtpromoterstatus ;
			} else 
			if(xmlPath=="mib1Index"){
				return this.Mib1index ;
			} else 
			if(xmlPath=="mib1IndexNotReported"){
				return this.Mib1indexnotreported ;
			} else 
			if(xmlPath=="pten"){
				return this.Pten ;
			} else 
			if(xmlPath=="p53"){
				return this.P53 ;
			} else 
			if(xmlPath=="othFeat"){
				return this.Othfeat ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="labResultData"){
				this.Labresultdata=value;
			} else 
			if(xmlPath.startsWith("labResultData")){
				xmlPath=xmlPath.substring(13);
				if(xmlPath=="")return this.Labresultdata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Labresultdata!=undefined){
					this.Labresultdata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Labresultdata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Labresultdata= instanciateObject("tissue:labResultData");//omUtils.js
						}
						if(options && options.where)this.Labresultdata.setProperty(options.where.field,options.where.value);
						this.Labresultdata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="tumorType"){
				this.Tumortype=value;
			} else 
			if(xmlPath=="primaryTumorType"){
				this.Primarytumortype=value;
			} else 
			if(xmlPath=="primaryWHOGrade"){
				this.Primarywhograde=value;
			} else 
			if(xmlPath=="metastatOrig"){
				this.Metastatorig=value;
			} else 
			if(xmlPath=="onep19qDeletions"){
				this.Onep19qdeletions=value;
			} else 
			if(xmlPath=="mgmtPromoterStatus"){
				this.Mgmtpromoterstatus=value;
			} else 
			if(xmlPath=="mib1Index"){
				this.Mib1index=value;
			} else 
			if(xmlPath=="mib1IndexNotReported"){
				this.Mib1indexnotreported=value;
			} else 
			if(xmlPath=="pten"){
				this.Pten=value;
			} else 
			if(xmlPath=="p53"){
				this.P53=value;
			} else 
			if(xmlPath=="othFeat"){
				this.Othfeat=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="tumorType"){
			return "field_data";
		}else if (xmlPath=="primaryTumorType"){
			return "field_data";
		}else if (xmlPath=="primaryWHOGrade"){
			return "field_data";
		}else if (xmlPath=="metastatOrig"){
			return "field_data";
		}else if (xmlPath=="onep19qDeletions"){
			return "field_data";
		}else if (xmlPath=="mgmtPromoterStatus"){
			return "field_data";
		}else if (xmlPath=="mib1Index"){
			return "field_data";
		}else if (xmlPath=="mib1IndexNotReported"){
			return "field_data";
		}else if (xmlPath=="pten"){
			return "field_data";
		}else if (xmlPath=="p53"){
			return "field_data";
		}else if (xmlPath=="othFeat"){
			return "field_LONG_DATA";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<condr:BrainPathology";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</condr:BrainPathology>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Tumortype!=null){
			xmlTxt+="\n<condr:tumorType";
			xmlTxt+=">";
			xmlTxt+=this.Tumortype.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:tumorType>";
		}
		if (this.Primarytumortype!=null){
			xmlTxt+="\n<condr:primaryTumorType";
			xmlTxt+=">";
			xmlTxt+=this.Primarytumortype.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:primaryTumorType>";
		}
		if (this.Primarywhograde!=null){
			xmlTxt+="\n<condr:primaryWHOGrade";
			xmlTxt+=">";
			xmlTxt+=this.Primarywhograde.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:primaryWHOGrade>";
		}
		if (this.Metastatorig!=null){
			xmlTxt+="\n<condr:metastatOrig";
			xmlTxt+=">";
			xmlTxt+=this.Metastatorig.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:metastatOrig>";
		}
		if (this.Onep19qdeletions!=null){
			xmlTxt+="\n<condr:onep19qDeletions";
			xmlTxt+=">";
			xmlTxt+=this.Onep19qdeletions.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:onep19qDeletions>";
		}
		if (this.Mgmtpromoterstatus!=null){
			xmlTxt+="\n<condr:mgmtPromoterStatus";
			xmlTxt+=">";
			xmlTxt+=this.Mgmtpromoterstatus.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:mgmtPromoterStatus>";
		}
		if (this.Mib1index!=null){
			xmlTxt+="\n<condr:mib1Index";
			xmlTxt+=">";
			xmlTxt+=this.Mib1index;
			xmlTxt+="</condr:mib1Index>";
		}
		if (this.Mib1indexnotreported!=null){
			xmlTxt+="\n<condr:mib1IndexNotReported";
			xmlTxt+=">";
			xmlTxt+=this.Mib1indexnotreported;
			xmlTxt+="</condr:mib1IndexNotReported>";
		}
		if (this.Pten!=null){
			xmlTxt+="\n<condr:pten";
			xmlTxt+=">";
			xmlTxt+=this.Pten.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:pten>";
		}
		if (this.P53!=null){
			xmlTxt+="\n<condr:p53";
			xmlTxt+=">";
			xmlTxt+=this.P53.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:p53>";
		}
		if (this.Othfeat!=null){
			xmlTxt+="\n<condr:othFeat";
			xmlTxt+=">";
			xmlTxt+=this.Othfeat.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:othFeat>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Tumortype!=null) return true;
		if (this.Primarytumortype!=null) return true;
		if (this.Primarywhograde!=null) return true;
		if (this.Metastatorig!=null) return true;
		if (this.Onep19qdeletions!=null) return true;
		if (this.Mgmtpromoterstatus!=null) return true;
		if (this.Mib1index!=null) return true;
		if (this.Mib1indexnotreported!=null) return true;
		if (this.Pten!=null) return true;
		if (this.P53!=null) return true;
		if (this.Othfeat!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
