/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:04 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function nihSS_nihStrokeScaleData(){
this.xsiType="nihSS:nihStrokeScaleData";

	this.getSchemaElementName=function(){
		return "nihStrokeScaleData";
	}

	this.getFullSchemaElementName=function(){
		return "nihSS:nihStrokeScaleData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Q1a=null;


	function getQ1a() {
		return this.Q1a;
	}
	this.getQ1a=getQ1a;


	function setQ1a(v){
		this.Q1a=v;
	}
	this.setQ1a=setQ1a;

	this.Q1b=null;


	function getQ1b() {
		return this.Q1b;
	}
	this.getQ1b=getQ1b;


	function setQ1b(v){
		this.Q1b=v;
	}
	this.setQ1b=setQ1b;

	this.Q1c=null;


	function getQ1c() {
		return this.Q1c;
	}
	this.getQ1c=getQ1c;


	function setQ1c(v){
		this.Q1c=v;
	}
	this.setQ1c=setQ1c;

	this.Q2=null;


	function getQ2() {
		return this.Q2;
	}
	this.getQ2=getQ2;


	function setQ2(v){
		this.Q2=v;
	}
	this.setQ2=setQ2;

	this.Q3=null;


	function getQ3() {
		return this.Q3;
	}
	this.getQ3=getQ3;


	function setQ3(v){
		this.Q3=v;
	}
	this.setQ3=setQ3;

	this.Q4=null;


	function getQ4() {
		return this.Q4;
	}
	this.getQ4=getQ4;


	function setQ4(v){
		this.Q4=v;
	}
	this.setQ4=setQ4;

	this.Q5a=null;


	function getQ5a() {
		return this.Q5a;
	}
	this.getQ5a=getQ5a;


	function setQ5a(v){
		this.Q5a=v;
	}
	this.setQ5a=setQ5a;

	this.Q5anote=null;


	function getQ5anote() {
		return this.Q5anote;
	}
	this.getQ5anote=getQ5anote;


	function setQ5anote(v){
		this.Q5anote=v;
	}
	this.setQ5anote=setQ5anote;

	this.Q5b=null;


	function getQ5b() {
		return this.Q5b;
	}
	this.getQ5b=getQ5b;


	function setQ5b(v){
		this.Q5b=v;
	}
	this.setQ5b=setQ5b;

	this.Q5bnote=null;


	function getQ5bnote() {
		return this.Q5bnote;
	}
	this.getQ5bnote=getQ5bnote;


	function setQ5bnote(v){
		this.Q5bnote=v;
	}
	this.setQ5bnote=setQ5bnote;

	this.Q6a=null;


	function getQ6a() {
		return this.Q6a;
	}
	this.getQ6a=getQ6a;


	function setQ6a(v){
		this.Q6a=v;
	}
	this.setQ6a=setQ6a;

	this.Q6anote=null;


	function getQ6anote() {
		return this.Q6anote;
	}
	this.getQ6anote=getQ6anote;


	function setQ6anote(v){
		this.Q6anote=v;
	}
	this.setQ6anote=setQ6anote;

	this.Q6b=null;


	function getQ6b() {
		return this.Q6b;
	}
	this.getQ6b=getQ6b;


	function setQ6b(v){
		this.Q6b=v;
	}
	this.setQ6b=setQ6b;

	this.Q6bnote=null;


	function getQ6bnote() {
		return this.Q6bnote;
	}
	this.getQ6bnote=getQ6bnote;


	function setQ6bnote(v){
		this.Q6bnote=v;
	}
	this.setQ6bnote=setQ6bnote;

	this.Q7=null;


	function getQ7() {
		return this.Q7;
	}
	this.getQ7=getQ7;


	function setQ7(v){
		this.Q7=v;
	}
	this.setQ7=setQ7;

	this.Q7note=null;


	function getQ7note() {
		return this.Q7note;
	}
	this.getQ7note=getQ7note;


	function setQ7note(v){
		this.Q7note=v;
	}
	this.setQ7note=setQ7note;

	this.Q8=null;


	function getQ8() {
		return this.Q8;
	}
	this.getQ8=getQ8;


	function setQ8(v){
		this.Q8=v;
	}
	this.setQ8=setQ8;

	this.Q9=null;


	function getQ9() {
		return this.Q9;
	}
	this.getQ9=getQ9;


	function setQ9(v){
		this.Q9=v;
	}
	this.setQ9=setQ9;

	this.Q10=null;


	function getQ10() {
		return this.Q10;
	}
	this.getQ10=getQ10;


	function setQ10(v){
		this.Q10=v;
	}
	this.setQ10=setQ10;

	this.Q10note=null;


	function getQ10note() {
		return this.Q10note;
	}
	this.getQ10note=getQ10note;


	function setQ10note(v){
		this.Q10note=v;
	}
	this.setQ10note=setQ10note;

	this.Q11=null;


	function getQ11() {
		return this.Q11;
	}
	this.getQ11=getQ11;


	function setQ11(v){
		this.Q11=v;
	}
	this.setQ11=setQ11;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="Q1a"){
				return this.Q1a ;
			} else 
			if(xmlPath=="Q1b"){
				return this.Q1b ;
			} else 
			if(xmlPath=="Q1c"){
				return this.Q1c ;
			} else 
			if(xmlPath=="Q2"){
				return this.Q2 ;
			} else 
			if(xmlPath=="Q3"){
				return this.Q3 ;
			} else 
			if(xmlPath=="Q4"){
				return this.Q4 ;
			} else 
			if(xmlPath=="Q5a"){
				return this.Q5a ;
			} else 
			if(xmlPath=="Q5aNote"){
				return this.Q5anote ;
			} else 
			if(xmlPath=="Q5b"){
				return this.Q5b ;
			} else 
			if(xmlPath=="Q5bNote"){
				return this.Q5bnote ;
			} else 
			if(xmlPath=="Q6a"){
				return this.Q6a ;
			} else 
			if(xmlPath=="Q6aNote"){
				return this.Q6anote ;
			} else 
			if(xmlPath=="Q6b"){
				return this.Q6b ;
			} else 
			if(xmlPath=="Q6bNote"){
				return this.Q6bnote ;
			} else 
			if(xmlPath=="Q7"){
				return this.Q7 ;
			} else 
			if(xmlPath=="Q7Note"){
				return this.Q7note ;
			} else 
			if(xmlPath=="Q8"){
				return this.Q8 ;
			} else 
			if(xmlPath=="Q9"){
				return this.Q9 ;
			} else 
			if(xmlPath=="Q10"){
				return this.Q10 ;
			} else 
			if(xmlPath=="Q10Note"){
				return this.Q10note ;
			} else 
			if(xmlPath=="Q11"){
				return this.Q11 ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="Q1a"){
				this.Q1a=value;
			} else 
			if(xmlPath=="Q1b"){
				this.Q1b=value;
			} else 
			if(xmlPath=="Q1c"){
				this.Q1c=value;
			} else 
			if(xmlPath=="Q2"){
				this.Q2=value;
			} else 
			if(xmlPath=="Q3"){
				this.Q3=value;
			} else 
			if(xmlPath=="Q4"){
				this.Q4=value;
			} else 
			if(xmlPath=="Q5a"){
				this.Q5a=value;
			} else 
			if(xmlPath=="Q5aNote"){
				this.Q5anote=value;
			} else 
			if(xmlPath=="Q5b"){
				this.Q5b=value;
			} else 
			if(xmlPath=="Q5bNote"){
				this.Q5bnote=value;
			} else 
			if(xmlPath=="Q6a"){
				this.Q6a=value;
			} else 
			if(xmlPath=="Q6aNote"){
				this.Q6anote=value;
			} else 
			if(xmlPath=="Q6b"){
				this.Q6b=value;
			} else 
			if(xmlPath=="Q6bNote"){
				this.Q6bnote=value;
			} else 
			if(xmlPath=="Q7"){
				this.Q7=value;
			} else 
			if(xmlPath=="Q7Note"){
				this.Q7note=value;
			} else 
			if(xmlPath=="Q8"){
				this.Q8=value;
			} else 
			if(xmlPath=="Q9"){
				this.Q9=value;
			} else 
			if(xmlPath=="Q10"){
				this.Q10=value;
			} else 
			if(xmlPath=="Q10Note"){
				this.Q10note=value;
			} else 
			if(xmlPath=="Q11"){
				this.Q11=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="Q1a"){
			return "field_data";
		}else if (xmlPath=="Q1b"){
			return "field_data";
		}else if (xmlPath=="Q1c"){
			return "field_data";
		}else if (xmlPath=="Q2"){
			return "field_data";
		}else if (xmlPath=="Q3"){
			return "field_data";
		}else if (xmlPath=="Q4"){
			return "field_data";
		}else if (xmlPath=="Q5a"){
			return "field_data";
		}else if (xmlPath=="Q5aNote"){
			return "field_LONG_DATA";
		}else if (xmlPath=="Q5b"){
			return "field_data";
		}else if (xmlPath=="Q5bNote"){
			return "field_LONG_DATA";
		}else if (xmlPath=="Q6a"){
			return "field_data";
		}else if (xmlPath=="Q6aNote"){
			return "field_LONG_DATA";
		}else if (xmlPath=="Q6b"){
			return "field_data";
		}else if (xmlPath=="Q6bNote"){
			return "field_LONG_DATA";
		}else if (xmlPath=="Q7"){
			return "field_data";
		}else if (xmlPath=="Q7Note"){
			return "field_LONG_DATA";
		}else if (xmlPath=="Q8"){
			return "field_data";
		}else if (xmlPath=="Q9"){
			return "field_data";
		}else if (xmlPath=="Q10"){
			return "field_data";
		}else if (xmlPath=="Q10Note"){
			return "field_LONG_DATA";
		}else if (xmlPath=="Q11"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<nihSS:NIHStrokeScale";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</nihSS:NIHStrokeScale>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Q1a!=null){
			xmlTxt+="\n<nihSS:Q1a";
			xmlTxt+=">";
			xmlTxt+=this.Q1a;
			xmlTxt+="</nihSS:Q1a>";
		}
		if (this.Q1b!=null){
			xmlTxt+="\n<nihSS:Q1b";
			xmlTxt+=">";
			xmlTxt+=this.Q1b;
			xmlTxt+="</nihSS:Q1b>";
		}
		if (this.Q1c!=null){
			xmlTxt+="\n<nihSS:Q1c";
			xmlTxt+=">";
			xmlTxt+=this.Q1c;
			xmlTxt+="</nihSS:Q1c>";
		}
		if (this.Q2!=null){
			xmlTxt+="\n<nihSS:Q2";
			xmlTxt+=">";
			xmlTxt+=this.Q2;
			xmlTxt+="</nihSS:Q2>";
		}
		if (this.Q3!=null){
			xmlTxt+="\n<nihSS:Q3";
			xmlTxt+=">";
			xmlTxt+=this.Q3;
			xmlTxt+="</nihSS:Q3>";
		}
		if (this.Q4!=null){
			xmlTxt+="\n<nihSS:Q4";
			xmlTxt+=">";
			xmlTxt+=this.Q4;
			xmlTxt+="</nihSS:Q4>";
		}
		if (this.Q5a!=null){
			xmlTxt+="\n<nihSS:Q5a";
			xmlTxt+=">";
			xmlTxt+=this.Q5a;
			xmlTxt+="</nihSS:Q5a>";
		}
		if (this.Q5anote!=null){
			xmlTxt+="\n<nihSS:Q5aNote";
			xmlTxt+=">";
			xmlTxt+=this.Q5anote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</nihSS:Q5aNote>";
		}
		else{
			xmlTxt+="\n<nihSS:Q5aNote";
			xmlTxt+="/>";
		}

		if (this.Q5b!=null){
			xmlTxt+="\n<nihSS:Q5b";
			xmlTxt+=">";
			xmlTxt+=this.Q5b;
			xmlTxt+="</nihSS:Q5b>";
		}
		if (this.Q5bnote!=null){
			xmlTxt+="\n<nihSS:Q5bNote";
			xmlTxt+=">";
			xmlTxt+=this.Q5bnote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</nihSS:Q5bNote>";
		}
		else{
			xmlTxt+="\n<nihSS:Q5bNote";
			xmlTxt+="/>";
		}

		if (this.Q6a!=null){
			xmlTxt+="\n<nihSS:Q6a";
			xmlTxt+=">";
			xmlTxt+=this.Q6a;
			xmlTxt+="</nihSS:Q6a>";
		}
		if (this.Q6anote!=null){
			xmlTxt+="\n<nihSS:Q6aNote";
			xmlTxt+=">";
			xmlTxt+=this.Q6anote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</nihSS:Q6aNote>";
		}
		else{
			xmlTxt+="\n<nihSS:Q6aNote";
			xmlTxt+="/>";
		}

		if (this.Q6b!=null){
			xmlTxt+="\n<nihSS:Q6b";
			xmlTxt+=">";
			xmlTxt+=this.Q6b;
			xmlTxt+="</nihSS:Q6b>";
		}
		if (this.Q6bnote!=null){
			xmlTxt+="\n<nihSS:Q6bNote";
			xmlTxt+=">";
			xmlTxt+=this.Q6bnote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</nihSS:Q6bNote>";
		}
		else{
			xmlTxt+="\n<nihSS:Q6bNote";
			xmlTxt+="/>";
		}

		if (this.Q7!=null){
			xmlTxt+="\n<nihSS:Q7";
			xmlTxt+=">";
			xmlTxt+=this.Q7;
			xmlTxt+="</nihSS:Q7>";
		}
		if (this.Q7note!=null){
			xmlTxt+="\n<nihSS:Q7Note";
			xmlTxt+=">";
			xmlTxt+=this.Q7note.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</nihSS:Q7Note>";
		}
		else{
			xmlTxt+="\n<nihSS:Q7Note";
			xmlTxt+="/>";
		}

		if (this.Q8!=null){
			xmlTxt+="\n<nihSS:Q8";
			xmlTxt+=">";
			xmlTxt+=this.Q8;
			xmlTxt+="</nihSS:Q8>";
		}
		if (this.Q9!=null){
			xmlTxt+="\n<nihSS:Q9";
			xmlTxt+=">";
			xmlTxt+=this.Q9;
			xmlTxt+="</nihSS:Q9>";
		}
		if (this.Q10!=null){
			xmlTxt+="\n<nihSS:Q10";
			xmlTxt+=">";
			xmlTxt+=this.Q10;
			xmlTxt+="</nihSS:Q10>";
		}
		if (this.Q10note!=null){
			xmlTxt+="\n<nihSS:Q10Note";
			xmlTxt+=">";
			xmlTxt+=this.Q10note.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</nihSS:Q10Note>";
		}
		else{
			xmlTxt+="\n<nihSS:Q10Note";
			xmlTxt+="/>";
		}

		if (this.Q11!=null){
			xmlTxt+="\n<nihSS:Q11";
			xmlTxt+=">";
			xmlTxt+=this.Q11;
			xmlTxt+="</nihSS:Q11>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Q1a!=null) return true;
		if (this.Q1b!=null) return true;
		if (this.Q1c!=null) return true;
		if (this.Q2!=null) return true;
		if (this.Q3!=null) return true;
		if (this.Q4!=null) return true;
		if (this.Q5a!=null) return true;
		if (this.Q5anote!=null) return true;
		return true;//REQUIRED Q5aNote
	}
}
