/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function dian_localcsfData(){
this.xsiType="dian:localcsfData";

	this.getSchemaElementName=function(){
		return "localcsfData";
	}

	this.getFullSchemaElementName=function(){
		return "dian:localcsfData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Lpdate=null;


	function getLpdate() {
		return this.Lpdate;
	}
	this.getLpdate=getLpdate;


	function setLpdate(v){
		this.Lpdate=v;
	}
	this.setLpdate=setLpdate;

	this.Lptime=null;


	function getLptime() {
		return this.Lptime;
	}
	this.getLptime=getLptime;


	function setLptime(v){
		this.Lptime=v;
	}
	this.setLptime=setLptime;

	this.Whitect=null;


	function getWhitect() {
		return this.Whitect;
	}
	this.getWhitect=getWhitect;


	function setWhitect(v){
		this.Whitect=v;
	}
	this.setWhitect=setWhitect;

	this.Whiteunit=null;


	function getWhiteunit() {
		return this.Whiteunit;
	}
	this.getWhiteunit=getWhiteunit;


	function setWhiteunit(v){
		this.Whiteunit=v;
	}
	this.setWhiteunit=setWhiteunit;

	this.Redct=null;


	function getRedct() {
		return this.Redct;
	}
	this.getRedct=getRedct;


	function setRedct(v){
		this.Redct=v;
	}
	this.setRedct=setRedct;

	this.Redunit=null;


	function getRedunit() {
		return this.Redunit;
	}
	this.getRedunit=getRedunit;


	function setRedunit(v){
		this.Redunit=v;
	}
	this.setRedunit=setRedunit;

	this.Protein=null;


	function getProtein() {
		return this.Protein;
	}
	this.getProtein=getProtein;


	function setProtein(v){
		this.Protein=v;
	}
	this.setProtein=setProtein;

	this.Protunit=null;


	function getProtunit() {
		return this.Protunit;
	}
	this.getProtunit=getProtunit;


	function setProtunit(v){
		this.Protunit=v;
	}
	this.setProtunit=setProtunit;

	this.Glucose=null;


	function getGlucose() {
		return this.Glucose;
	}
	this.getGlucose=getGlucose;


	function setGlucose(v){
		this.Glucose=v;
	}
	this.setGlucose=setGlucose;

	this.Glucunit=null;


	function getGlucunit() {
		return this.Glucunit;
	}
	this.getGlucunit=getGlucunit;


	function setGlucunit(v){
		this.Glucunit=v;
	}
	this.setGlucunit=setGlucunit;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="LPDATE"){
				return this.Lpdate ;
			} else 
			if(xmlPath=="LPTIME"){
				return this.Lptime ;
			} else 
			if(xmlPath=="WHITECT"){
				return this.Whitect ;
			} else 
			if(xmlPath=="WHITEUNIT"){
				return this.Whiteunit ;
			} else 
			if(xmlPath=="REDCT"){
				return this.Redct ;
			} else 
			if(xmlPath=="REDUNIT"){
				return this.Redunit ;
			} else 
			if(xmlPath=="PROTEIN"){
				return this.Protein ;
			} else 
			if(xmlPath=="PROTUNIT"){
				return this.Protunit ;
			} else 
			if(xmlPath=="GLUCOSE"){
				return this.Glucose ;
			} else 
			if(xmlPath=="GLUCUNIT"){
				return this.Glucunit ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="LPDATE"){
				this.Lpdate=value;
			} else 
			if(xmlPath=="LPTIME"){
				this.Lptime=value;
			} else 
			if(xmlPath=="WHITECT"){
				this.Whitect=value;
			} else 
			if(xmlPath=="WHITEUNIT"){
				this.Whiteunit=value;
			} else 
			if(xmlPath=="REDCT"){
				this.Redct=value;
			} else 
			if(xmlPath=="REDUNIT"){
				this.Redunit=value;
			} else 
			if(xmlPath=="PROTEIN"){
				this.Protein=value;
			} else 
			if(xmlPath=="PROTUNIT"){
				this.Protunit=value;
			} else 
			if(xmlPath=="GLUCOSE"){
				this.Glucose=value;
			} else 
			if(xmlPath=="GLUCUNIT"){
				this.Glucunit=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="LPDATE"){
			return "field_data";
		}else if (xmlPath=="LPTIME"){
			return "field_data";
		}else if (xmlPath=="WHITECT"){
			return "field_data";
		}else if (xmlPath=="WHITEUNIT"){
			return "field_data";
		}else if (xmlPath=="REDCT"){
			return "field_data";
		}else if (xmlPath=="REDUNIT"){
			return "field_data";
		}else if (xmlPath=="PROTEIN"){
			return "field_data";
		}else if (xmlPath=="PROTUNIT"){
			return "field_data";
		}else if (xmlPath=="GLUCOSE"){
			return "field_data";
		}else if (xmlPath=="GLUCUNIT"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<dian:LOCALCSF";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</dian:LOCALCSF>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Lpdate!=null){
			xmlTxt+="\n<dian:LPDATE";
			xmlTxt+=">";
			xmlTxt+=this.Lpdate;
			xmlTxt+="</dian:LPDATE>";
		}
		if (this.Lptime!=null){
			xmlTxt+="\n<dian:LPTIME";
			xmlTxt+=">";
			xmlTxt+=this.Lptime;
			xmlTxt+="</dian:LPTIME>";
		}
		if (this.Whitect!=null){
			xmlTxt+="\n<dian:WHITECT";
			xmlTxt+=">";
			xmlTxt+=this.Whitect;
			xmlTxt+="</dian:WHITECT>";
		}
		if (this.Whiteunit!=null){
			xmlTxt+="\n<dian:WHITEUNIT";
			xmlTxt+=">";
			xmlTxt+=this.Whiteunit.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:WHITEUNIT>";
		}
		if (this.Redct!=null){
			xmlTxt+="\n<dian:REDCT";
			xmlTxt+=">";
			xmlTxt+=this.Redct;
			xmlTxt+="</dian:REDCT>";
		}
		if (this.Redunit!=null){
			xmlTxt+="\n<dian:REDUNIT";
			xmlTxt+=">";
			xmlTxt+=this.Redunit.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:REDUNIT>";
		}
		if (this.Protein!=null){
			xmlTxt+="\n<dian:PROTEIN";
			xmlTxt+=">";
			xmlTxt+=this.Protein;
			xmlTxt+="</dian:PROTEIN>";
		}
		if (this.Protunit!=null){
			xmlTxt+="\n<dian:PROTUNIT";
			xmlTxt+=">";
			xmlTxt+=this.Protunit.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:PROTUNIT>";
		}
		if (this.Glucose!=null){
			xmlTxt+="\n<dian:GLUCOSE";
			xmlTxt+=">";
			xmlTxt+=this.Glucose;
			xmlTxt+="</dian:GLUCOSE>";
		}
		if (this.Glucunit!=null){
			xmlTxt+="\n<dian:GLUCUNIT";
			xmlTxt+=">";
			xmlTxt+=this.Glucunit.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:GLUCUNIT>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Lpdate!=null) return true;
		if (this.Lptime!=null) return true;
		if (this.Whitect!=null) return true;
		if (this.Whiteunit!=null) return true;
		if (this.Redct!=null) return true;
		if (this.Redunit!=null) return true;
		if (this.Protein!=null) return true;
		if (this.Protunit!=null) return true;
		if (this.Glucose!=null) return true;
		if (this.Glucunit!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
