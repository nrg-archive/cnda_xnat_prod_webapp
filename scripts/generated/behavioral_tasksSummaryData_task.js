/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function behavioral_tasksSummaryData_task(){
this.xsiType="behavioral:tasksSummaryData_task";

	this.getSchemaElementName=function(){
		return "tasksSummaryData_task";
	}

	this.getFullSchemaElementName=function(){
		return "behavioral:tasksSummaryData_task";
	}
	this.Run =new Array();

	function getRun() {
		return this.Run;
	}
	this.getRun=getRun;


	function addRun(v){
		this.Run.push(v);
	}
	this.addRun=addRun;
	this.Statisticsacrossruns =null;
	function getStatisticsacrossruns() {
		return this.Statisticsacrossruns;
	}
	this.getStatisticsacrossruns=getStatisticsacrossruns;


	function setStatisticsacrossruns(v){
		this.Statisticsacrossruns =v;
	}
	this.setStatisticsacrossruns=setStatisticsacrossruns;

	this.Statisticsacrossruns_StatisticsacrossrunsBehavioralStatisticsId=null;


	function getStatisticsacrossruns_StatisticsacrossrunsBehavioralStatisticsId(){
		return this.Statisticsacrossruns_StatisticsacrossrunsBehavioralStatisticsId;
	}
	this.getStatisticsacrossruns_StatisticsacrossrunsBehavioralStatisticsId=getStatisticsacrossruns_StatisticsacrossrunsBehavioralStatisticsId;


	function setStatisticsacrossruns_StatisticsacrossrunsBehavioralStatisticsId(v){
		this.Statisticsacrossruns_StatisticsacrossrunsBehavioralStatisticsId=v;
	}
	this.setStatisticsacrossruns_StatisticsacrossrunsBehavioralStatisticsId=setStatisticsacrossruns_StatisticsacrossrunsBehavioralStatisticsId;

	this.Name=null;


	function getName() {
		return this.Name;
	}
	this.getName=getName;


	function setName(v){
		this.Name=v;
	}
	this.setName=setName;

	this.BehavioralTaskssummarydataTaskId=null;


	function getBehavioralTaskssummarydataTaskId() {
		return this.BehavioralTaskssummarydataTaskId;
	}
	this.getBehavioralTaskssummarydataTaskId=getBehavioralTaskssummarydataTaskId;


	function setBehavioralTaskssummarydataTaskId(v){
		this.BehavioralTaskssummarydataTaskId=v;
	}
	this.setBehavioralTaskssummarydataTaskId=setBehavioralTaskssummarydataTaskId;

	this.tasks_task_behavioral_tasksSumm_id_fk=null;


	this.gettasks_task_behavioral_tasksSumm_id=function() {
		return this.tasks_task_behavioral_tasksSumm_id_fk;
	}


	this.settasks_task_behavioral_tasksSumm_id=function(v){
		this.tasks_task_behavioral_tasksSumm_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="run"){
				return this.Run ;
			} else 
			if(xmlPath.startsWith("run")){
				xmlPath=xmlPath.substring(3);
				if(xmlPath=="")return this.Run ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Run.length;whereCount++){

					var tempValue=this.Run[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Run[whereCount]);

					}

				}
				}else{

				whereArray=this.Run;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="StatisticsAcrossRuns"){
				return this.Statisticsacrossruns ;
			} else 
			if(xmlPath.startsWith("StatisticsAcrossRuns")){
				xmlPath=xmlPath.substring(20);
				if(xmlPath=="")return this.Statisticsacrossruns ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Statisticsacrossruns!=undefined)return this.Statisticsacrossruns.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="name"){
				return this.Name ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="behavioral_tasksSummaryData_task_id"){
				return this.BehavioralTaskssummarydataTaskId ;
			} else 
			if(xmlPath=="tasks_task_behavioral_tasksSumm_id"){
				return this.tasks_task_behavioral_tasksSumm_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="run"){
				this.Run=value;
			} else 
			if(xmlPath.startsWith("run")){
				xmlPath=xmlPath.substring(3);
				if(xmlPath=="")return this.Run ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Run.length;whereCount++){

					var tempValue=this.Run[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Run[whereCount]);

					}

				}
				}else{

				whereArray=this.Run;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("behavioral:tasksSummaryData_task_run");//omUtils.js
					}
					this.addRun(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="StatisticsAcrossRuns"){
				this.Statisticsacrossruns=value;
			} else 
			if(xmlPath.startsWith("StatisticsAcrossRuns")){
				xmlPath=xmlPath.substring(20);
				if(xmlPath=="")return this.Statisticsacrossruns ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Statisticsacrossruns!=undefined){
					this.Statisticsacrossruns.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Statisticsacrossruns= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Statisticsacrossruns= instanciateObject("behavioral:statistics");//omUtils.js
						}
						if(options && options.where)this.Statisticsacrossruns.setProperty(options.where.field,options.where.value);
						this.Statisticsacrossruns.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="name"){
				this.Name=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="behavioral_tasksSummaryData_task_id"){
				this.BehavioralTaskssummarydataTaskId=value;
			} else 
			if(xmlPath=="tasks_task_behavioral_tasksSumm_id"){
				this.tasks_task_behavioral_tasksSumm_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="run"){
			this.addRun(v);
		}else if (xmlPath=="StatisticsAcrossRuns"){
			this.setStatisticsacrossruns(v);
		}
		else{
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="run"){
			return "http://nrg.wustl.edu/behavioral:tasksSummaryData_task_run";
		}else if (xmlPath=="StatisticsAcrossRuns"){
			return "http://nrg.wustl.edu/behavioral:statistics";
		}
		else{
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="run"){
			return "field_multi_reference";
		}else if (xmlPath=="StatisticsAcrossRuns"){
			return "field_single_reference";
		}else if (xmlPath=="name"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<behavioral:tasksSummaryData_task";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</behavioral:tasksSummaryData_task>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.BehavioralTaskssummarydataTaskId!=null){
				if(hiddenCount++>0)str+=",";
				str+="behavioral_tasksSummaryData_task_id=\"" + this.BehavioralTaskssummarydataTaskId + "\"";
			}
			if(this.tasks_task_behavioral_tasksSumm_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="tasks_task_behavioral_tasksSumm_id=\"" + this.tasks_task_behavioral_tasksSumm_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.Name!=null)
			attTxt+=" name=\"" +this.Name +"\"";
		else attTxt+=" name=\"\"";//REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		for(var RunCOUNT=0;RunCOUNT<this.Run.length;RunCOUNT++){
			xmlTxt +="\n<behavioral:run";
			xmlTxt +=this.Run[RunCOUNT].getXMLAtts();
			if(this.Run[RunCOUNT].xsiType!="behavioral:tasksSummaryData_task_run"){
				xmlTxt+=" xsi:type=\"" + this.Run[RunCOUNT].xsiType + "\"";
			}
			if (this.Run[RunCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Run[RunCOUNT].getXMLBody(preventComments);
					xmlTxt+="</behavioral:run>";
			}else {xmlTxt+="/>";}
		}
		if (this.Statisticsacrossruns!=null){
			xmlTxt+="\n<behavioral:StatisticsAcrossRuns";
			xmlTxt+=this.Statisticsacrossruns.getXMLAtts();
			if(this.Statisticsacrossruns.xsiType!="behavioral:statistics"){
				xmlTxt+=" xsi:type=\"" + this.Statisticsacrossruns.xsiType + "\"";
			}
			if (this.Statisticsacrossruns.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Statisticsacrossruns.getXMLBody(preventComments);
				xmlTxt+="</behavioral:StatisticsAcrossRuns>";
			}else {xmlTxt+="/>";}
		}
		//NOT REQUIRED

		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.BehavioralTaskssummarydataTaskId!=null) return true;
			if (this.tasks_task_behavioral_tasksSumm_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if(this.Run.length>0) return true;
		if (this.Statisticsacrossruns!=null){
			if (this.Statisticsacrossruns.hasXMLBodyContent()) return true;
		}
		//NOT REQUIRED

		if(this.hasXMLComments())return true;
		return false;
	}
}
