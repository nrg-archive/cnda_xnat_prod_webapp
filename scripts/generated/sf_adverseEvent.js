/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function sf_adverseEvent(){
this.xsiType="sf:adverseEvent";

	this.getSchemaElementName=function(){
		return "adverseEvent";
	}

	this.getFullSchemaElementName=function(){
		return "sf:adverseEvent";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Eventdescription=null;


	function getEventdescription() {
		return this.Eventdescription;
	}
	this.getEventdescription=getEventdescription;


	function setEventdescription(v){
		this.Eventdescription=v;
	}
	this.setEventdescription=setEventdescription;

	this.Severity=null;


	function getSeverity() {
		return this.Severity;
	}
	this.getSeverity=getSeverity;


	function setSeverity(v){
		this.Severity=v;
	}
	this.setSeverity=setSeverity;

	this.Attribution=null;


	function getAttribution() {
		return this.Attribution;
	}
	this.getAttribution=getAttribution;


	function setAttribution(v){
		this.Attribution=v;
	}
	this.setAttribution=setAttribution;

	this.Attributionnotes=null;


	function getAttributionnotes() {
		return this.Attributionnotes;
	}
	this.getAttributionnotes=getAttributionnotes;


	function setAttributionnotes(v){
		this.Attributionnotes=v;
	}
	this.setAttributionnotes=setAttributionnotes;

	this.Expected=null;


	function getExpected() {
		return this.Expected;
	}
	this.getExpected=getExpected;


	function setExpected(v){
		this.Expected=v;
	}
	this.setExpected=setExpected;


	this.isExpected=function(defaultValue) {
		if(this.Expected==null)return defaultValue;
		if(this.Expected=="1" || this.Expected==true)return true;
		return false;
	}

	this.Onsetdate=null;


	function getOnsetdate() {
		return this.Onsetdate;
	}
	this.getOnsetdate=getOnsetdate;


	function setOnsetdate(v){
		this.Onsetdate=v;
	}
	this.setOnsetdate=setOnsetdate;

	this.Resolutiondate=null;


	function getResolutiondate() {
		return this.Resolutiondate;
	}
	this.getResolutiondate=getResolutiondate;


	function setResolutiondate(v){
		this.Resolutiondate=v;
	}
	this.setResolutiondate=setResolutiondate;

	this.Ongoing=null;


	function getOngoing() {
		return this.Ongoing;
	}
	this.getOngoing=getOngoing;


	function setOngoing(v){
		this.Ongoing=v;
	}
	this.setOngoing=setOngoing;


	this.isOngoing=function(defaultValue) {
		if(this.Ongoing==null)return defaultValue;
		if(this.Ongoing=="1" || this.Ongoing==true)return true;
		return false;
	}

	this.Actiontaken=null;


	function getActiontaken() {
		return this.Actiontaken;
	}
	this.getActiontaken=getActiontaken;


	function setActiontaken(v){
		this.Actiontaken=v;
	}
	this.setActiontaken=setActiontaken;

	this.Outcome=null;


	function getOutcome() {
		return this.Outcome;
	}
	this.getOutcome=getOutcome;


	function setOutcome(v){
		this.Outcome=v;
	}
	this.setOutcome=setOutcome;

	this.Serious=null;


	function getSerious() {
		return this.Serious;
	}
	this.getSerious=getSerious;


	function setSerious(v){
		this.Serious=v;
	}
	this.setSerious=setSerious;


	this.isSerious=function(defaultValue) {
		if(this.Serious==null)return defaultValue;
		if(this.Serious=="1" || this.Serious==true)return true;
		return false;
	}

	this.Seriousreason=null;


	function getSeriousreason() {
		return this.Seriousreason;
	}
	this.getSeriousreason=getSeriousreason;


	function setSeriousreason(v){
		this.Seriousreason=v;
	}
	this.setSeriousreason=setSeriousreason;

	this.Adverseeventnotes=null;


	function getAdverseeventnotes() {
		return this.Adverseeventnotes;
	}
	this.getAdverseeventnotes=getAdverseeventnotes;


	function setAdverseeventnotes(v){
		this.Adverseeventnotes=v;
	}
	this.setAdverseeventnotes=setAdverseeventnotes;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="eventDescription"){
				return this.Eventdescription ;
			} else 
			if(xmlPath=="severity"){
				return this.Severity ;
			} else 
			if(xmlPath=="attribution"){
				return this.Attribution ;
			} else 
			if(xmlPath=="attributionNotes"){
				return this.Attributionnotes ;
			} else 
			if(xmlPath=="expected"){
				return this.Expected ;
			} else 
			if(xmlPath=="onsetDate"){
				return this.Onsetdate ;
			} else 
			if(xmlPath=="resolutionDate"){
				return this.Resolutiondate ;
			} else 
			if(xmlPath=="ongoing"){
				return this.Ongoing ;
			} else 
			if(xmlPath=="actionTaken"){
				return this.Actiontaken ;
			} else 
			if(xmlPath=="outcome"){
				return this.Outcome ;
			} else 
			if(xmlPath=="serious"){
				return this.Serious ;
			} else 
			if(xmlPath=="seriousReason"){
				return this.Seriousreason ;
			} else 
			if(xmlPath=="adverseEventNotes"){
				return this.Adverseeventnotes ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="eventDescription"){
				this.Eventdescription=value;
			} else 
			if(xmlPath=="severity"){
				this.Severity=value;
			} else 
			if(xmlPath=="attribution"){
				this.Attribution=value;
			} else 
			if(xmlPath=="attributionNotes"){
				this.Attributionnotes=value;
			} else 
			if(xmlPath=="expected"){
				this.Expected=value;
			} else 
			if(xmlPath=="onsetDate"){
				this.Onsetdate=value;
			} else 
			if(xmlPath=="resolutionDate"){
				this.Resolutiondate=value;
			} else 
			if(xmlPath=="ongoing"){
				this.Ongoing=value;
			} else 
			if(xmlPath=="actionTaken"){
				this.Actiontaken=value;
			} else 
			if(xmlPath=="outcome"){
				this.Outcome=value;
			} else 
			if(xmlPath=="serious"){
				this.Serious=value;
			} else 
			if(xmlPath=="seriousReason"){
				this.Seriousreason=value;
			} else 
			if(xmlPath=="adverseEventNotes"){
				this.Adverseeventnotes=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="eventDescription"){
			return "field_data";
		}else if (xmlPath=="severity"){
			return "field_data";
		}else if (xmlPath=="attribution"){
			return "field_data";
		}else if (xmlPath=="attributionNotes"){
			return "field_LONG_DATA";
		}else if (xmlPath=="expected"){
			return "field_data";
		}else if (xmlPath=="onsetDate"){
			return "field_data";
		}else if (xmlPath=="resolutionDate"){
			return "field_data";
		}else if (xmlPath=="ongoing"){
			return "field_data";
		}else if (xmlPath=="actionTaken"){
			return "field_data";
		}else if (xmlPath=="outcome"){
			return "field_data";
		}else if (xmlPath=="serious"){
			return "field_data";
		}else if (xmlPath=="seriousReason"){
			return "field_data";
		}else if (xmlPath=="adverseEventNotes"){
			return "field_LONG_DATA";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<sf:AdverseEvent";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</sf:AdverseEvent>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Eventdescription!=null){
			xmlTxt+="\n<sf:eventDescription";
			xmlTxt+=">";
			xmlTxt+=this.Eventdescription.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:eventDescription>";
		}
		if (this.Severity!=null){
			xmlTxt+="\n<sf:severity";
			xmlTxt+=">";
			xmlTxt+=this.Severity.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:severity>";
		}
		if (this.Attribution!=null){
			xmlTxt+="\n<sf:attribution";
			xmlTxt+=">";
			xmlTxt+=this.Attribution.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:attribution>";
		}
		if (this.Attributionnotes!=null){
			xmlTxt+="\n<sf:attributionNotes";
			xmlTxt+=">";
			xmlTxt+=this.Attributionnotes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:attributionNotes>";
		}
		if (this.Expected!=null){
			xmlTxt+="\n<sf:expected";
			xmlTxt+=">";
			xmlTxt+=this.Expected;
			xmlTxt+="</sf:expected>";
		}
		if (this.Onsetdate!=null){
			xmlTxt+="\n<sf:onsetDate";
			xmlTxt+=">";
			xmlTxt+=this.Onsetdate;
			xmlTxt+="</sf:onsetDate>";
		}
		if (this.Resolutiondate!=null){
			xmlTxt+="\n<sf:resolutionDate";
			xmlTxt+=">";
			xmlTxt+=this.Resolutiondate;
			xmlTxt+="</sf:resolutionDate>";
		}
		if (this.Ongoing!=null){
			xmlTxt+="\n<sf:ongoing";
			xmlTxt+=">";
			xmlTxt+=this.Ongoing;
			xmlTxt+="</sf:ongoing>";
		}
		if (this.Actiontaken!=null){
			xmlTxt+="\n<sf:actionTaken";
			xmlTxt+=">";
			xmlTxt+=this.Actiontaken.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:actionTaken>";
		}
		if (this.Outcome!=null){
			xmlTxt+="\n<sf:outcome";
			xmlTxt+=">";
			xmlTxt+=this.Outcome.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:outcome>";
		}
		if (this.Serious!=null){
			xmlTxt+="\n<sf:serious";
			xmlTxt+=">";
			xmlTxt+=this.Serious;
			xmlTxt+="</sf:serious>";
		}
		if (this.Seriousreason!=null){
			xmlTxt+="\n<sf:seriousReason";
			xmlTxt+=">";
			xmlTxt+=this.Seriousreason.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:seriousReason>";
		}
		if (this.Adverseeventnotes!=null){
			xmlTxt+="\n<sf:adverseEventNotes";
			xmlTxt+=">";
			xmlTxt+=this.Adverseeventnotes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:adverseEventNotes>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Eventdescription!=null) return true;
		if (this.Severity!=null) return true;
		if (this.Attribution!=null) return true;
		if (this.Attributionnotes!=null) return true;
		if (this.Expected!=null) return true;
		if (this.Onsetdate!=null) return true;
		if (this.Resolutiondate!=null) return true;
		if (this.Ongoing!=null) return true;
		if (this.Actiontaken!=null) return true;
		if (this.Outcome!=null) return true;
		if (this.Serious!=null) return true;
		if (this.Seriousreason!=null) return true;
		if (this.Adverseeventnotes!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
