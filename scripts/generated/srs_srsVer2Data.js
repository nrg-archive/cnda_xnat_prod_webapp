/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function srs_srsVer2Data(){
this.xsiType="srs:srsVer2Data";

	this.getSchemaElementName=function(){
		return "srsVer2Data";
	}

	this.getFullSchemaElementName=function(){
		return "srs:srsVer2Data";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.TotalRaw=null;


	function getTotalRaw() {
		return this.TotalRaw;
	}
	this.getTotalRaw=getTotalRaw;


	function setTotalRaw(v){
		this.TotalRaw=v;
	}
	this.setTotalRaw=setTotalRaw;

	this.TotalT=null;


	function getTotalT() {
		return this.TotalT;
	}
	this.getTotalT=getTotalT;


	function setTotalT(v){
		this.TotalT=v;
	}
	this.setTotalT=setTotalT;

	this.TotalNote=null;


	function getTotalNote() {
		return this.TotalNote;
	}
	this.getTotalNote=getTotalNote;


	function setTotalNote(v){
		this.TotalNote=v;
	}
	this.setTotalNote=setTotalNote;

	this.Testtype=null;


	function getTesttype() {
		return this.Testtype;
	}
	this.getTesttype=getTesttype;


	function setTesttype(v){
		this.Testtype=v;
	}
	this.setTesttype=setTesttype;

	this.Scoreform_filledoutbyrelat=null;


	function getScoreform_filledoutbyrelat() {
		return this.Scoreform_filledoutbyrelat;
	}
	this.getScoreform_filledoutbyrelat=getScoreform_filledoutbyrelat;


	function setScoreform_filledoutbyrelat(v){
		this.Scoreform_filledoutbyrelat=v;
	}
	this.setScoreform_filledoutbyrelat=setScoreform_filledoutbyrelat;

	this.Scoreform_sectiona_question1=null;


	function getScoreform_sectiona_question1() {
		return this.Scoreform_sectiona_question1;
	}
	this.getScoreform_sectiona_question1=getScoreform_sectiona_question1;


	function setScoreform_sectiona_question1(v){
		this.Scoreform_sectiona_question1=v;
	}
	this.setScoreform_sectiona_question1=setScoreform_sectiona_question1;

	this.Scoreform_sectiona_question2=null;


	function getScoreform_sectiona_question2() {
		return this.Scoreform_sectiona_question2;
	}
	this.getScoreform_sectiona_question2=getScoreform_sectiona_question2;


	function setScoreform_sectiona_question2(v){
		this.Scoreform_sectiona_question2=v;
	}
	this.setScoreform_sectiona_question2=setScoreform_sectiona_question2;

	this.Scoreform_sectiona_question3=null;


	function getScoreform_sectiona_question3() {
		return this.Scoreform_sectiona_question3;
	}
	this.getScoreform_sectiona_question3=getScoreform_sectiona_question3;


	function setScoreform_sectiona_question3(v){
		this.Scoreform_sectiona_question3=v;
	}
	this.setScoreform_sectiona_question3=setScoreform_sectiona_question3;

	this.Scoreform_sectiona_question4=null;


	function getScoreform_sectiona_question4() {
		return this.Scoreform_sectiona_question4;
	}
	this.getScoreform_sectiona_question4=getScoreform_sectiona_question4;


	function setScoreform_sectiona_question4(v){
		this.Scoreform_sectiona_question4=v;
	}
	this.setScoreform_sectiona_question4=setScoreform_sectiona_question4;

	this.Scoreform_sectiona_question5=null;


	function getScoreform_sectiona_question5() {
		return this.Scoreform_sectiona_question5;
	}
	this.getScoreform_sectiona_question5=getScoreform_sectiona_question5;


	function setScoreform_sectiona_question5(v){
		this.Scoreform_sectiona_question5=v;
	}
	this.setScoreform_sectiona_question5=setScoreform_sectiona_question5;

	this.Scoreform_sectiona_question6=null;


	function getScoreform_sectiona_question6() {
		return this.Scoreform_sectiona_question6;
	}
	this.getScoreform_sectiona_question6=getScoreform_sectiona_question6;


	function setScoreform_sectiona_question6(v){
		this.Scoreform_sectiona_question6=v;
	}
	this.setScoreform_sectiona_question6=setScoreform_sectiona_question6;

	this.Scoreform_sectiona_question7=null;


	function getScoreform_sectiona_question7() {
		return this.Scoreform_sectiona_question7;
	}
	this.getScoreform_sectiona_question7=getScoreform_sectiona_question7;


	function setScoreform_sectiona_question7(v){
		this.Scoreform_sectiona_question7=v;
	}
	this.setScoreform_sectiona_question7=setScoreform_sectiona_question7;

	this.Scoreform_sectiona_question8=null;


	function getScoreform_sectiona_question8() {
		return this.Scoreform_sectiona_question8;
	}
	this.getScoreform_sectiona_question8=getScoreform_sectiona_question8;


	function setScoreform_sectiona_question8(v){
		this.Scoreform_sectiona_question8=v;
	}
	this.setScoreform_sectiona_question8=setScoreform_sectiona_question8;

	this.Scoreform_sectiona_question9=null;


	function getScoreform_sectiona_question9() {
		return this.Scoreform_sectiona_question9;
	}
	this.getScoreform_sectiona_question9=getScoreform_sectiona_question9;


	function setScoreform_sectiona_question9(v){
		this.Scoreform_sectiona_question9=v;
	}
	this.setScoreform_sectiona_question9=setScoreform_sectiona_question9;

	this.Scoreform_sectiona_question10=null;


	function getScoreform_sectiona_question10() {
		return this.Scoreform_sectiona_question10;
	}
	this.getScoreform_sectiona_question10=getScoreform_sectiona_question10;


	function setScoreform_sectiona_question10(v){
		this.Scoreform_sectiona_question10=v;
	}
	this.setScoreform_sectiona_question10=setScoreform_sectiona_question10;

	this.Scoreform_sectiona_question11=null;


	function getScoreform_sectiona_question11() {
		return this.Scoreform_sectiona_question11;
	}
	this.getScoreform_sectiona_question11=getScoreform_sectiona_question11;


	function setScoreform_sectiona_question11(v){
		this.Scoreform_sectiona_question11=v;
	}
	this.setScoreform_sectiona_question11=setScoreform_sectiona_question11;

	this.Scoreform_sectiona_question12=null;


	function getScoreform_sectiona_question12() {
		return this.Scoreform_sectiona_question12;
	}
	this.getScoreform_sectiona_question12=getScoreform_sectiona_question12;


	function setScoreform_sectiona_question12(v){
		this.Scoreform_sectiona_question12=v;
	}
	this.setScoreform_sectiona_question12=setScoreform_sectiona_question12;

	this.Scoreform_sectionb_question13=null;


	function getScoreform_sectionb_question13() {
		return this.Scoreform_sectionb_question13;
	}
	this.getScoreform_sectionb_question13=getScoreform_sectionb_question13;


	function setScoreform_sectionb_question13(v){
		this.Scoreform_sectionb_question13=v;
	}
	this.setScoreform_sectionb_question13=setScoreform_sectionb_question13;

	this.Scoreform_sectionb_question14=null;


	function getScoreform_sectionb_question14() {
		return this.Scoreform_sectionb_question14;
	}
	this.getScoreform_sectionb_question14=getScoreform_sectionb_question14;


	function setScoreform_sectionb_question14(v){
		this.Scoreform_sectionb_question14=v;
	}
	this.setScoreform_sectionb_question14=setScoreform_sectionb_question14;

	this.Scoreform_sectionb_question15=null;


	function getScoreform_sectionb_question15() {
		return this.Scoreform_sectionb_question15;
	}
	this.getScoreform_sectionb_question15=getScoreform_sectionb_question15;


	function setScoreform_sectionb_question15(v){
		this.Scoreform_sectionb_question15=v;
	}
	this.setScoreform_sectionb_question15=setScoreform_sectionb_question15;

	this.Scoreform_sectionb_question16=null;


	function getScoreform_sectionb_question16() {
		return this.Scoreform_sectionb_question16;
	}
	this.getScoreform_sectionb_question16=getScoreform_sectionb_question16;


	function setScoreform_sectionb_question16(v){
		this.Scoreform_sectionb_question16=v;
	}
	this.setScoreform_sectionb_question16=setScoreform_sectionb_question16;

	this.Scoreform_sectionb_question17=null;


	function getScoreform_sectionb_question17() {
		return this.Scoreform_sectionb_question17;
	}
	this.getScoreform_sectionb_question17=getScoreform_sectionb_question17;


	function setScoreform_sectionb_question17(v){
		this.Scoreform_sectionb_question17=v;
	}
	this.setScoreform_sectionb_question17=setScoreform_sectionb_question17;

	this.Scoreform_sectionb_question18=null;


	function getScoreform_sectionb_question18() {
		return this.Scoreform_sectionb_question18;
	}
	this.getScoreform_sectionb_question18=getScoreform_sectionb_question18;


	function setScoreform_sectionb_question18(v){
		this.Scoreform_sectionb_question18=v;
	}
	this.setScoreform_sectionb_question18=setScoreform_sectionb_question18;

	this.Scoreform_sectionb_question19=null;


	function getScoreform_sectionb_question19() {
		return this.Scoreform_sectionb_question19;
	}
	this.getScoreform_sectionb_question19=getScoreform_sectionb_question19;


	function setScoreform_sectionb_question19(v){
		this.Scoreform_sectionb_question19=v;
	}
	this.setScoreform_sectionb_question19=setScoreform_sectionb_question19;

	this.Scoreform_sectionb_question20=null;


	function getScoreform_sectionb_question20() {
		return this.Scoreform_sectionb_question20;
	}
	this.getScoreform_sectionb_question20=getScoreform_sectionb_question20;


	function setScoreform_sectionb_question20(v){
		this.Scoreform_sectionb_question20=v;
	}
	this.setScoreform_sectionb_question20=setScoreform_sectionb_question20;

	this.Scoreform_sectionc_question21=null;


	function getScoreform_sectionc_question21() {
		return this.Scoreform_sectionc_question21;
	}
	this.getScoreform_sectionc_question21=getScoreform_sectionc_question21;


	function setScoreform_sectionc_question21(v){
		this.Scoreform_sectionc_question21=v;
	}
	this.setScoreform_sectionc_question21=setScoreform_sectionc_question21;

	this.Scoreform_sectionc_question22=null;


	function getScoreform_sectionc_question22() {
		return this.Scoreform_sectionc_question22;
	}
	this.getScoreform_sectionc_question22=getScoreform_sectionc_question22;


	function setScoreform_sectionc_question22(v){
		this.Scoreform_sectionc_question22=v;
	}
	this.setScoreform_sectionc_question22=setScoreform_sectionc_question22;

	this.Scoreform_sectionc_question23=null;


	function getScoreform_sectionc_question23() {
		return this.Scoreform_sectionc_question23;
	}
	this.getScoreform_sectionc_question23=getScoreform_sectionc_question23;


	function setScoreform_sectionc_question23(v){
		this.Scoreform_sectionc_question23=v;
	}
	this.setScoreform_sectionc_question23=setScoreform_sectionc_question23;

	this.Scoreform_sectionc_question24=null;


	function getScoreform_sectionc_question24() {
		return this.Scoreform_sectionc_question24;
	}
	this.getScoreform_sectionc_question24=getScoreform_sectionc_question24;


	function setScoreform_sectionc_question24(v){
		this.Scoreform_sectionc_question24=v;
	}
	this.setScoreform_sectionc_question24=setScoreform_sectionc_question24;

	this.Scoreform_sectionc_question25=null;


	function getScoreform_sectionc_question25() {
		return this.Scoreform_sectionc_question25;
	}
	this.getScoreform_sectionc_question25=getScoreform_sectionc_question25;


	function setScoreform_sectionc_question25(v){
		this.Scoreform_sectionc_question25=v;
	}
	this.setScoreform_sectionc_question25=setScoreform_sectionc_question25;

	this.Scoreform_sectionc_question26=null;


	function getScoreform_sectionc_question26() {
		return this.Scoreform_sectionc_question26;
	}
	this.getScoreform_sectionc_question26=getScoreform_sectionc_question26;


	function setScoreform_sectionc_question26(v){
		this.Scoreform_sectionc_question26=v;
	}
	this.setScoreform_sectionc_question26=setScoreform_sectionc_question26;

	this.Scoreform_sectionc_question27=null;


	function getScoreform_sectionc_question27() {
		return this.Scoreform_sectionc_question27;
	}
	this.getScoreform_sectionc_question27=getScoreform_sectionc_question27;


	function setScoreform_sectionc_question27(v){
		this.Scoreform_sectionc_question27=v;
	}
	this.setScoreform_sectionc_question27=setScoreform_sectionc_question27;

	this.Scoreform_sectionc_question28=null;


	function getScoreform_sectionc_question28() {
		return this.Scoreform_sectionc_question28;
	}
	this.getScoreform_sectionc_question28=getScoreform_sectionc_question28;


	function setScoreform_sectionc_question28(v){
		this.Scoreform_sectionc_question28=v;
	}
	this.setScoreform_sectionc_question28=setScoreform_sectionc_question28;

	this.Scoreform_sectionc_question29=null;


	function getScoreform_sectionc_question29() {
		return this.Scoreform_sectionc_question29;
	}
	this.getScoreform_sectionc_question29=getScoreform_sectionc_question29;


	function setScoreform_sectionc_question29(v){
		this.Scoreform_sectionc_question29=v;
	}
	this.setScoreform_sectionc_question29=setScoreform_sectionc_question29;

	this.Scoreform_sectionc_question30=null;


	function getScoreform_sectionc_question30() {
		return this.Scoreform_sectionc_question30;
	}
	this.getScoreform_sectionc_question30=getScoreform_sectionc_question30;


	function setScoreform_sectionc_question30(v){
		this.Scoreform_sectionc_question30=v;
	}
	this.setScoreform_sectionc_question30=setScoreform_sectionc_question30;

	this.Scoreform_sectiond_question31=null;


	function getScoreform_sectiond_question31() {
		return this.Scoreform_sectiond_question31;
	}
	this.getScoreform_sectiond_question31=getScoreform_sectiond_question31;


	function setScoreform_sectiond_question31(v){
		this.Scoreform_sectiond_question31=v;
	}
	this.setScoreform_sectiond_question31=setScoreform_sectiond_question31;

	this.Scoreform_sectiond_question32=null;


	function getScoreform_sectiond_question32() {
		return this.Scoreform_sectiond_question32;
	}
	this.getScoreform_sectiond_question32=getScoreform_sectiond_question32;


	function setScoreform_sectiond_question32(v){
		this.Scoreform_sectiond_question32=v;
	}
	this.setScoreform_sectiond_question32=setScoreform_sectiond_question32;

	this.Scoreform_sectiond_question33=null;


	function getScoreform_sectiond_question33() {
		return this.Scoreform_sectiond_question33;
	}
	this.getScoreform_sectiond_question33=getScoreform_sectiond_question33;


	function setScoreform_sectiond_question33(v){
		this.Scoreform_sectiond_question33=v;
	}
	this.setScoreform_sectiond_question33=setScoreform_sectiond_question33;

	this.Scoreform_sectiond_question34=null;


	function getScoreform_sectiond_question34() {
		return this.Scoreform_sectiond_question34;
	}
	this.getScoreform_sectiond_question34=getScoreform_sectiond_question34;


	function setScoreform_sectiond_question34(v){
		this.Scoreform_sectiond_question34=v;
	}
	this.setScoreform_sectiond_question34=setScoreform_sectiond_question34;

	this.Scoreform_sectiond_question35=null;


	function getScoreform_sectiond_question35() {
		return this.Scoreform_sectiond_question35;
	}
	this.getScoreform_sectiond_question35=getScoreform_sectiond_question35;


	function setScoreform_sectiond_question35(v){
		this.Scoreform_sectiond_question35=v;
	}
	this.setScoreform_sectiond_question35=setScoreform_sectiond_question35;

	this.Scoreform_sectiond_question36=null;


	function getScoreform_sectiond_question36() {
		return this.Scoreform_sectiond_question36;
	}
	this.getScoreform_sectiond_question36=getScoreform_sectiond_question36;


	function setScoreform_sectiond_question36(v){
		this.Scoreform_sectiond_question36=v;
	}
	this.setScoreform_sectiond_question36=setScoreform_sectiond_question36;

	this.Scoreform_sectiond_question37=null;


	function getScoreform_sectiond_question37() {
		return this.Scoreform_sectiond_question37;
	}
	this.getScoreform_sectiond_question37=getScoreform_sectiond_question37;


	function setScoreform_sectiond_question37(v){
		this.Scoreform_sectiond_question37=v;
	}
	this.setScoreform_sectiond_question37=setScoreform_sectiond_question37;

	this.Scoreform_sectiond_question38=null;


	function getScoreform_sectiond_question38() {
		return this.Scoreform_sectiond_question38;
	}
	this.getScoreform_sectiond_question38=getScoreform_sectiond_question38;


	function setScoreform_sectiond_question38(v){
		this.Scoreform_sectiond_question38=v;
	}
	this.setScoreform_sectiond_question38=setScoreform_sectiond_question38;

	this.Scoreform_sectiond_question39=null;


	function getScoreform_sectiond_question39() {
		return this.Scoreform_sectiond_question39;
	}
	this.getScoreform_sectiond_question39=getScoreform_sectiond_question39;


	function setScoreform_sectiond_question39(v){
		this.Scoreform_sectiond_question39=v;
	}
	this.setScoreform_sectiond_question39=setScoreform_sectiond_question39;

	this.Scoreform_sectione_question40=null;


	function getScoreform_sectione_question40() {
		return this.Scoreform_sectione_question40;
	}
	this.getScoreform_sectione_question40=getScoreform_sectione_question40;


	function setScoreform_sectione_question40(v){
		this.Scoreform_sectione_question40=v;
	}
	this.setScoreform_sectione_question40=setScoreform_sectione_question40;

	this.Scoreform_sectione_question41=null;


	function getScoreform_sectione_question41() {
		return this.Scoreform_sectione_question41;
	}
	this.getScoreform_sectione_question41=getScoreform_sectione_question41;


	function setScoreform_sectione_question41(v){
		this.Scoreform_sectione_question41=v;
	}
	this.setScoreform_sectione_question41=setScoreform_sectione_question41;

	this.Scoreform_sectione_question42=null;


	function getScoreform_sectione_question42() {
		return this.Scoreform_sectione_question42;
	}
	this.getScoreform_sectione_question42=getScoreform_sectione_question42;


	function setScoreform_sectione_question42(v){
		this.Scoreform_sectione_question42=v;
	}
	this.setScoreform_sectione_question42=setScoreform_sectione_question42;

	this.Scoreform_sectione_question43=null;


	function getScoreform_sectione_question43() {
		return this.Scoreform_sectione_question43;
	}
	this.getScoreform_sectione_question43=getScoreform_sectione_question43;


	function setScoreform_sectione_question43(v){
		this.Scoreform_sectione_question43=v;
	}
	this.setScoreform_sectione_question43=setScoreform_sectione_question43;

	this.Scoreform_sectione_question44=null;


	function getScoreform_sectione_question44() {
		return this.Scoreform_sectione_question44;
	}
	this.getScoreform_sectione_question44=getScoreform_sectione_question44;


	function setScoreform_sectione_question44(v){
		this.Scoreform_sectione_question44=v;
	}
	this.setScoreform_sectione_question44=setScoreform_sectione_question44;

	this.Scoreform_sectione_question45=null;


	function getScoreform_sectione_question45() {
		return this.Scoreform_sectione_question45;
	}
	this.getScoreform_sectione_question45=getScoreform_sectione_question45;


	function setScoreform_sectione_question45(v){
		this.Scoreform_sectione_question45=v;
	}
	this.setScoreform_sectione_question45=setScoreform_sectione_question45;

	this.Scoreform_sectione_question46=null;


	function getScoreform_sectione_question46() {
		return this.Scoreform_sectione_question46;
	}
	this.getScoreform_sectione_question46=getScoreform_sectione_question46;


	function setScoreform_sectione_question46(v){
		this.Scoreform_sectione_question46=v;
	}
	this.setScoreform_sectione_question46=setScoreform_sectione_question46;

	this.Scoreform_sectione_question47=null;


	function getScoreform_sectione_question47() {
		return this.Scoreform_sectione_question47;
	}
	this.getScoreform_sectione_question47=getScoreform_sectione_question47;


	function setScoreform_sectione_question47(v){
		this.Scoreform_sectione_question47=v;
	}
	this.setScoreform_sectione_question47=setScoreform_sectione_question47;

	this.Scoreform_sectione_question48=null;


	function getScoreform_sectione_question48() {
		return this.Scoreform_sectione_question48;
	}
	this.getScoreform_sectione_question48=getScoreform_sectione_question48;


	function setScoreform_sectione_question48(v){
		this.Scoreform_sectione_question48=v;
	}
	this.setScoreform_sectione_question48=setScoreform_sectione_question48;

	this.Scoreform_sectione_question49=null;


	function getScoreform_sectione_question49() {
		return this.Scoreform_sectione_question49;
	}
	this.getScoreform_sectione_question49=getScoreform_sectione_question49;


	function setScoreform_sectione_question49(v){
		this.Scoreform_sectione_question49=v;
	}
	this.setScoreform_sectione_question49=setScoreform_sectione_question49;

	this.Scoreform_sectionf_question50=null;


	function getScoreform_sectionf_question50() {
		return this.Scoreform_sectionf_question50;
	}
	this.getScoreform_sectionf_question50=getScoreform_sectionf_question50;


	function setScoreform_sectionf_question50(v){
		this.Scoreform_sectionf_question50=v;
	}
	this.setScoreform_sectionf_question50=setScoreform_sectionf_question50;

	this.Scoreform_sectionf_question51=null;


	function getScoreform_sectionf_question51() {
		return this.Scoreform_sectionf_question51;
	}
	this.getScoreform_sectionf_question51=getScoreform_sectionf_question51;


	function setScoreform_sectionf_question51(v){
		this.Scoreform_sectionf_question51=v;
	}
	this.setScoreform_sectionf_question51=setScoreform_sectionf_question51;

	this.Scoreform_sectionf_question52=null;


	function getScoreform_sectionf_question52() {
		return this.Scoreform_sectionf_question52;
	}
	this.getScoreform_sectionf_question52=getScoreform_sectionf_question52;


	function setScoreform_sectionf_question52(v){
		this.Scoreform_sectionf_question52=v;
	}
	this.setScoreform_sectionf_question52=setScoreform_sectionf_question52;

	this.Scoreform_sectionf_question53=null;


	function getScoreform_sectionf_question53() {
		return this.Scoreform_sectionf_question53;
	}
	this.getScoreform_sectionf_question53=getScoreform_sectionf_question53;


	function setScoreform_sectionf_question53(v){
		this.Scoreform_sectionf_question53=v;
	}
	this.setScoreform_sectionf_question53=setScoreform_sectionf_question53;

	this.Scoreform_sectionf_question54=null;


	function getScoreform_sectionf_question54() {
		return this.Scoreform_sectionf_question54;
	}
	this.getScoreform_sectionf_question54=getScoreform_sectionf_question54;


	function setScoreform_sectionf_question54(v){
		this.Scoreform_sectionf_question54=v;
	}
	this.setScoreform_sectionf_question54=setScoreform_sectionf_question54;

	this.Scoreform_sectionf_question55=null;


	function getScoreform_sectionf_question55() {
		return this.Scoreform_sectionf_question55;
	}
	this.getScoreform_sectionf_question55=getScoreform_sectionf_question55;


	function setScoreform_sectionf_question55(v){
		this.Scoreform_sectionf_question55=v;
	}
	this.setScoreform_sectionf_question55=setScoreform_sectionf_question55;

	this.Scoreform_sectionf_question56=null;


	function getScoreform_sectionf_question56() {
		return this.Scoreform_sectionf_question56;
	}
	this.getScoreform_sectionf_question56=getScoreform_sectionf_question56;


	function setScoreform_sectionf_question56(v){
		this.Scoreform_sectionf_question56=v;
	}
	this.setScoreform_sectionf_question56=setScoreform_sectionf_question56;

	this.Scoreform_sectionf_question57=null;


	function getScoreform_sectionf_question57() {
		return this.Scoreform_sectionf_question57;
	}
	this.getScoreform_sectionf_question57=getScoreform_sectionf_question57;


	function setScoreform_sectionf_question57(v){
		this.Scoreform_sectionf_question57=v;
	}
	this.setScoreform_sectionf_question57=setScoreform_sectionf_question57;

	this.Scoreform_sectionf_question58=null;


	function getScoreform_sectionf_question58() {
		return this.Scoreform_sectionf_question58;
	}
	this.getScoreform_sectionf_question58=getScoreform_sectionf_question58;


	function setScoreform_sectionf_question58(v){
		this.Scoreform_sectionf_question58=v;
	}
	this.setScoreform_sectionf_question58=setScoreform_sectionf_question58;

	this.Scoreform_sectionf_question59=null;


	function getScoreform_sectionf_question59() {
		return this.Scoreform_sectionf_question59;
	}
	this.getScoreform_sectionf_question59=getScoreform_sectionf_question59;


	function setScoreform_sectionf_question59(v){
		this.Scoreform_sectionf_question59=v;
	}
	this.setScoreform_sectionf_question59=setScoreform_sectionf_question59;

	this.Scoreform_sectiong_question60=null;


	function getScoreform_sectiong_question60() {
		return this.Scoreform_sectiong_question60;
	}
	this.getScoreform_sectiong_question60=getScoreform_sectiong_question60;


	function setScoreform_sectiong_question60(v){
		this.Scoreform_sectiong_question60=v;
	}
	this.setScoreform_sectiong_question60=setScoreform_sectiong_question60;

	this.Scoreform_sectiong_question61=null;


	function getScoreform_sectiong_question61() {
		return this.Scoreform_sectiong_question61;
	}
	this.getScoreform_sectiong_question61=getScoreform_sectiong_question61;


	function setScoreform_sectiong_question61(v){
		this.Scoreform_sectiong_question61=v;
	}
	this.setScoreform_sectiong_question61=setScoreform_sectiong_question61;

	this.Scoreform_sectiong_question62=null;


	function getScoreform_sectiong_question62() {
		return this.Scoreform_sectiong_question62;
	}
	this.getScoreform_sectiong_question62=getScoreform_sectiong_question62;


	function setScoreform_sectiong_question62(v){
		this.Scoreform_sectiong_question62=v;
	}
	this.setScoreform_sectiong_question62=setScoreform_sectiong_question62;

	this.Scoreform_sectiong_question63=null;


	function getScoreform_sectiong_question63() {
		return this.Scoreform_sectiong_question63;
	}
	this.getScoreform_sectiong_question63=getScoreform_sectiong_question63;


	function setScoreform_sectiong_question63(v){
		this.Scoreform_sectiong_question63=v;
	}
	this.setScoreform_sectiong_question63=setScoreform_sectiong_question63;

	this.Scoreform_sectiong_question64=null;


	function getScoreform_sectiong_question64() {
		return this.Scoreform_sectiong_question64;
	}
	this.getScoreform_sectiong_question64=getScoreform_sectiong_question64;


	function setScoreform_sectiong_question64(v){
		this.Scoreform_sectiong_question64=v;
	}
	this.setScoreform_sectiong_question64=setScoreform_sectiong_question64;

	this.Scoreform_sectiong_question65=null;


	function getScoreform_sectiong_question65() {
		return this.Scoreform_sectiong_question65;
	}
	this.getScoreform_sectiong_question65=getScoreform_sectiong_question65;


	function setScoreform_sectiong_question65(v){
		this.Scoreform_sectiong_question65=v;
	}
	this.setScoreform_sectiong_question65=setScoreform_sectiong_question65;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="total_raw"){
				return this.TotalRaw ;
			} else 
			if(xmlPath=="total_t"){
				return this.TotalT ;
			} else 
			if(xmlPath=="total_note"){
				return this.TotalNote ;
			} else 
			if(xmlPath=="testType"){
				return this.Testtype ;
			} else 
			if(xmlPath=="ScoreForm/filledOutByRelat"){
				return this.Scoreform_filledoutbyrelat ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question1"){
				return this.Scoreform_sectiona_question1 ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question2"){
				return this.Scoreform_sectiona_question2 ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question3"){
				return this.Scoreform_sectiona_question3 ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question4"){
				return this.Scoreform_sectiona_question4 ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question5"){
				return this.Scoreform_sectiona_question5 ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question6"){
				return this.Scoreform_sectiona_question6 ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question7"){
				return this.Scoreform_sectiona_question7 ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question8"){
				return this.Scoreform_sectiona_question8 ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question9"){
				return this.Scoreform_sectiona_question9 ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question10"){
				return this.Scoreform_sectiona_question10 ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question11"){
				return this.Scoreform_sectiona_question11 ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question12"){
				return this.Scoreform_sectiona_question12 ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/question13"){
				return this.Scoreform_sectionb_question13 ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/question14"){
				return this.Scoreform_sectionb_question14 ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/question15"){
				return this.Scoreform_sectionb_question15 ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/question16"){
				return this.Scoreform_sectionb_question16 ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/question17"){
				return this.Scoreform_sectionb_question17 ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/question18"){
				return this.Scoreform_sectionb_question18 ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/question19"){
				return this.Scoreform_sectionb_question19 ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/question20"){
				return this.Scoreform_sectionb_question20 ;
			} else 
			if(xmlPath=="ScoreForm/sectionC/question21"){
				return this.Scoreform_sectionc_question21 ;
			} else 
			if(xmlPath=="ScoreForm/sectionC/question22"){
				return this.Scoreform_sectionc_question22 ;
			} else 
			if(xmlPath=="ScoreForm/sectionC/question23"){
				return this.Scoreform_sectionc_question23 ;
			} else 
			if(xmlPath=="ScoreForm/sectionC/question24"){
				return this.Scoreform_sectionc_question24 ;
			} else 
			if(xmlPath=="ScoreForm/sectionC/question25"){
				return this.Scoreform_sectionc_question25 ;
			} else 
			if(xmlPath=="ScoreForm/sectionC/question26"){
				return this.Scoreform_sectionc_question26 ;
			} else 
			if(xmlPath=="ScoreForm/sectionC/question27"){
				return this.Scoreform_sectionc_question27 ;
			} else 
			if(xmlPath=="ScoreForm/sectionC/question28"){
				return this.Scoreform_sectionc_question28 ;
			} else 
			if(xmlPath=="ScoreForm/sectionC/question29"){
				return this.Scoreform_sectionc_question29 ;
			} else 
			if(xmlPath=="ScoreForm/sectionC/question30"){
				return this.Scoreform_sectionc_question30 ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/question31"){
				return this.Scoreform_sectiond_question31 ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/question32"){
				return this.Scoreform_sectiond_question32 ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/question33"){
				return this.Scoreform_sectiond_question33 ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/question34"){
				return this.Scoreform_sectiond_question34 ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/question35"){
				return this.Scoreform_sectiond_question35 ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/question36"){
				return this.Scoreform_sectiond_question36 ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/question37"){
				return this.Scoreform_sectiond_question37 ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/question38"){
				return this.Scoreform_sectiond_question38 ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/question39"){
				return this.Scoreform_sectiond_question39 ;
			} else 
			if(xmlPath=="ScoreForm/sectionE/question40"){
				return this.Scoreform_sectione_question40 ;
			} else 
			if(xmlPath=="ScoreForm/sectionE/question41"){
				return this.Scoreform_sectione_question41 ;
			} else 
			if(xmlPath=="ScoreForm/sectionE/question42"){
				return this.Scoreform_sectione_question42 ;
			} else 
			if(xmlPath=="ScoreForm/sectionE/question43"){
				return this.Scoreform_sectione_question43 ;
			} else 
			if(xmlPath=="ScoreForm/sectionE/question44"){
				return this.Scoreform_sectione_question44 ;
			} else 
			if(xmlPath=="ScoreForm/sectionE/question45"){
				return this.Scoreform_sectione_question45 ;
			} else 
			if(xmlPath=="ScoreForm/sectionE/question46"){
				return this.Scoreform_sectione_question46 ;
			} else 
			if(xmlPath=="ScoreForm/sectionE/question47"){
				return this.Scoreform_sectione_question47 ;
			} else 
			if(xmlPath=="ScoreForm/sectionE/question48"){
				return this.Scoreform_sectione_question48 ;
			} else 
			if(xmlPath=="ScoreForm/sectionE/question49"){
				return this.Scoreform_sectione_question49 ;
			} else 
			if(xmlPath=="ScoreForm/sectionF/question50"){
				return this.Scoreform_sectionf_question50 ;
			} else 
			if(xmlPath=="ScoreForm/sectionF/question51"){
				return this.Scoreform_sectionf_question51 ;
			} else 
			if(xmlPath=="ScoreForm/sectionF/question52"){
				return this.Scoreform_sectionf_question52 ;
			} else 
			if(xmlPath=="ScoreForm/sectionF/question53"){
				return this.Scoreform_sectionf_question53 ;
			} else 
			if(xmlPath=="ScoreForm/sectionF/question54"){
				return this.Scoreform_sectionf_question54 ;
			} else 
			if(xmlPath=="ScoreForm/sectionF/question55"){
				return this.Scoreform_sectionf_question55 ;
			} else 
			if(xmlPath=="ScoreForm/sectionF/question56"){
				return this.Scoreform_sectionf_question56 ;
			} else 
			if(xmlPath=="ScoreForm/sectionF/question57"){
				return this.Scoreform_sectionf_question57 ;
			} else 
			if(xmlPath=="ScoreForm/sectionF/question58"){
				return this.Scoreform_sectionf_question58 ;
			} else 
			if(xmlPath=="ScoreForm/sectionF/question59"){
				return this.Scoreform_sectionf_question59 ;
			} else 
			if(xmlPath=="ScoreForm/sectionG/question60"){
				return this.Scoreform_sectiong_question60 ;
			} else 
			if(xmlPath=="ScoreForm/sectionG/question61"){
				return this.Scoreform_sectiong_question61 ;
			} else 
			if(xmlPath=="ScoreForm/sectionG/question62"){
				return this.Scoreform_sectiong_question62 ;
			} else 
			if(xmlPath=="ScoreForm/sectionG/question63"){
				return this.Scoreform_sectiong_question63 ;
			} else 
			if(xmlPath=="ScoreForm/sectionG/question64"){
				return this.Scoreform_sectiong_question64 ;
			} else 
			if(xmlPath=="ScoreForm/sectionG/question65"){
				return this.Scoreform_sectiong_question65 ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="total_raw"){
				this.TotalRaw=value;
			} else 
			if(xmlPath=="total_t"){
				this.TotalT=value;
			} else 
			if(xmlPath=="total_note"){
				this.TotalNote=value;
			} else 
			if(xmlPath=="testType"){
				this.Testtype=value;
			} else 
			if(xmlPath=="ScoreForm/filledOutByRelat"){
				this.Scoreform_filledoutbyrelat=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question1"){
				this.Scoreform_sectiona_question1=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question2"){
				this.Scoreform_sectiona_question2=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question3"){
				this.Scoreform_sectiona_question3=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question4"){
				this.Scoreform_sectiona_question4=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question5"){
				this.Scoreform_sectiona_question5=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question6"){
				this.Scoreform_sectiona_question6=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question7"){
				this.Scoreform_sectiona_question7=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question8"){
				this.Scoreform_sectiona_question8=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question9"){
				this.Scoreform_sectiona_question9=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question10"){
				this.Scoreform_sectiona_question10=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question11"){
				this.Scoreform_sectiona_question11=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/question12"){
				this.Scoreform_sectiona_question12=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/question13"){
				this.Scoreform_sectionb_question13=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/question14"){
				this.Scoreform_sectionb_question14=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/question15"){
				this.Scoreform_sectionb_question15=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/question16"){
				this.Scoreform_sectionb_question16=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/question17"){
				this.Scoreform_sectionb_question17=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/question18"){
				this.Scoreform_sectionb_question18=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/question19"){
				this.Scoreform_sectionb_question19=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/question20"){
				this.Scoreform_sectionb_question20=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC/question21"){
				this.Scoreform_sectionc_question21=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC/question22"){
				this.Scoreform_sectionc_question22=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC/question23"){
				this.Scoreform_sectionc_question23=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC/question24"){
				this.Scoreform_sectionc_question24=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC/question25"){
				this.Scoreform_sectionc_question25=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC/question26"){
				this.Scoreform_sectionc_question26=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC/question27"){
				this.Scoreform_sectionc_question27=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC/question28"){
				this.Scoreform_sectionc_question28=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC/question29"){
				this.Scoreform_sectionc_question29=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC/question30"){
				this.Scoreform_sectionc_question30=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/question31"){
				this.Scoreform_sectiond_question31=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/question32"){
				this.Scoreform_sectiond_question32=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/question33"){
				this.Scoreform_sectiond_question33=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/question34"){
				this.Scoreform_sectiond_question34=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/question35"){
				this.Scoreform_sectiond_question35=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/question36"){
				this.Scoreform_sectiond_question36=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/question37"){
				this.Scoreform_sectiond_question37=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/question38"){
				this.Scoreform_sectiond_question38=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/question39"){
				this.Scoreform_sectiond_question39=value;
			} else 
			if(xmlPath=="ScoreForm/sectionE/question40"){
				this.Scoreform_sectione_question40=value;
			} else 
			if(xmlPath=="ScoreForm/sectionE/question41"){
				this.Scoreform_sectione_question41=value;
			} else 
			if(xmlPath=="ScoreForm/sectionE/question42"){
				this.Scoreform_sectione_question42=value;
			} else 
			if(xmlPath=="ScoreForm/sectionE/question43"){
				this.Scoreform_sectione_question43=value;
			} else 
			if(xmlPath=="ScoreForm/sectionE/question44"){
				this.Scoreform_sectione_question44=value;
			} else 
			if(xmlPath=="ScoreForm/sectionE/question45"){
				this.Scoreform_sectione_question45=value;
			} else 
			if(xmlPath=="ScoreForm/sectionE/question46"){
				this.Scoreform_sectione_question46=value;
			} else 
			if(xmlPath=="ScoreForm/sectionE/question47"){
				this.Scoreform_sectione_question47=value;
			} else 
			if(xmlPath=="ScoreForm/sectionE/question48"){
				this.Scoreform_sectione_question48=value;
			} else 
			if(xmlPath=="ScoreForm/sectionE/question49"){
				this.Scoreform_sectione_question49=value;
			} else 
			if(xmlPath=="ScoreForm/sectionF/question50"){
				this.Scoreform_sectionf_question50=value;
			} else 
			if(xmlPath=="ScoreForm/sectionF/question51"){
				this.Scoreform_sectionf_question51=value;
			} else 
			if(xmlPath=="ScoreForm/sectionF/question52"){
				this.Scoreform_sectionf_question52=value;
			} else 
			if(xmlPath=="ScoreForm/sectionF/question53"){
				this.Scoreform_sectionf_question53=value;
			} else 
			if(xmlPath=="ScoreForm/sectionF/question54"){
				this.Scoreform_sectionf_question54=value;
			} else 
			if(xmlPath=="ScoreForm/sectionF/question55"){
				this.Scoreform_sectionf_question55=value;
			} else 
			if(xmlPath=="ScoreForm/sectionF/question56"){
				this.Scoreform_sectionf_question56=value;
			} else 
			if(xmlPath=="ScoreForm/sectionF/question57"){
				this.Scoreform_sectionf_question57=value;
			} else 
			if(xmlPath=="ScoreForm/sectionF/question58"){
				this.Scoreform_sectionf_question58=value;
			} else 
			if(xmlPath=="ScoreForm/sectionF/question59"){
				this.Scoreform_sectionf_question59=value;
			} else 
			if(xmlPath=="ScoreForm/sectionG/question60"){
				this.Scoreform_sectiong_question60=value;
			} else 
			if(xmlPath=="ScoreForm/sectionG/question61"){
				this.Scoreform_sectiong_question61=value;
			} else 
			if(xmlPath=="ScoreForm/sectionG/question62"){
				this.Scoreform_sectiong_question62=value;
			} else 
			if(xmlPath=="ScoreForm/sectionG/question63"){
				this.Scoreform_sectiong_question63=value;
			} else 
			if(xmlPath=="ScoreForm/sectionG/question64"){
				this.Scoreform_sectiong_question64=value;
			} else 
			if(xmlPath=="ScoreForm/sectionG/question65"){
				this.Scoreform_sectiong_question65=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="total_raw"){
			return "field_data";
		}else if (xmlPath=="total_t"){
			return "field_data";
		}else if (xmlPath=="total_note"){
			return "field_data";
		}else if (xmlPath=="testType"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/filledOutByRelat"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/question1"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/question2"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/question3"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/question4"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/question5"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/question6"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/question7"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/question8"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/question9"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/question10"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/question11"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/question12"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/question13"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/question14"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/question15"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/question16"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/question17"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/question18"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/question19"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/question20"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC/question21"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC/question22"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC/question23"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC/question24"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC/question25"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC/question26"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC/question27"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC/question28"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC/question29"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC/question30"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/question31"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/question32"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/question33"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/question34"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/question35"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/question36"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/question37"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/question38"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/question39"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionE/question40"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionE/question41"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionE/question42"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionE/question43"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionE/question44"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionE/question45"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionE/question46"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionE/question47"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionE/question48"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionE/question49"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionF/question50"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionF/question51"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionF/question52"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionF/question53"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionF/question54"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionF/question55"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionF/question56"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionF/question57"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionF/question58"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionF/question59"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionG/question60"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionG/question61"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionG/question62"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionG/question63"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionG/question64"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionG/question65"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<srs:SRSVer2";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</srs:SRSVer2>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.TotalRaw!=null){
			xmlTxt+="\n<srs:total_raw";
			xmlTxt+=">";
			xmlTxt+=this.TotalRaw;
			xmlTxt+="</srs:total_raw>";
		}
		if (this.TotalT!=null){
			xmlTxt+="\n<srs:total_t";
			xmlTxt+=">";
			xmlTxt+=this.TotalT;
			xmlTxt+="</srs:total_t>";
		}
		if (this.TotalNote!=null){
			xmlTxt+="\n<srs:total_note";
			xmlTxt+=">";
			xmlTxt+=this.TotalNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</srs:total_note>";
		}
		if (this.Testtype!=null){
			xmlTxt+="\n<srs:testType";
			xmlTxt+=">";
			xmlTxt+=this.Testtype.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</srs:testType>";
		}
			var child0=0;
			var att0=0;
			if(this.Scoreform_sectionc_question29!=null)
			child0++;
			if(this.Scoreform_sectionc_question28!=null)
			child0++;
			if(this.Scoreform_sectionc_question27!=null)
			child0++;
			if(this.Scoreform_sectionc_question26!=null)
			child0++;
			if(this.Scoreform_sectionc_question25!=null)
			child0++;
			if(this.Scoreform_sectionc_question24!=null)
			child0++;
			if(this.Scoreform_sectionc_question23!=null)
			child0++;
			if(this.Scoreform_sectionc_question22!=null)
			child0++;
			if(this.Scoreform_sectionc_question21!=null)
			child0++;
			if(this.Scoreform_sectiona_question9!=null)
			child0++;
			if(this.Scoreform_filledoutbyrelat!=null)
			child0++;
			if(this.Scoreform_sectiona_question8!=null)
			child0++;
			if(this.Scoreform_sectiona_question7!=null)
			child0++;
			if(this.Scoreform_sectiona_question6!=null)
			child0++;
			if(this.Scoreform_sectiona_question5!=null)
			child0++;
			if(this.Scoreform_sectiona_question4!=null)
			child0++;
			if(this.Scoreform_sectiona_question3!=null)
			child0++;
			if(this.Scoreform_sectiond_question39!=null)
			child0++;
			if(this.Scoreform_sectiona_question2!=null)
			child0++;
			if(this.Scoreform_sectiond_question38!=null)
			child0++;
			if(this.Scoreform_sectiond_question37!=null)
			child0++;
			if(this.Scoreform_sectiona_question1!=null)
			child0++;
			if(this.Scoreform_sectiond_question36!=null)
			child0++;
			if(this.Scoreform_sectiond_question35!=null)
			child0++;
			if(this.Scoreform_sectiond_question34!=null)
			child0++;
			if(this.Scoreform_sectiond_question33!=null)
			child0++;
			if(this.Scoreform_sectiond_question32!=null)
			child0++;
			if(this.Scoreform_sectiond_question31!=null)
			child0++;
			if(this.Scoreform_sectiona_question12!=null)
			child0++;
			if(this.Scoreform_sectiona_question11!=null)
			child0++;
			if(this.Scoreform_sectiona_question10!=null)
			child0++;
			if(this.Scoreform_sectione_question49!=null)
			child0++;
			if(this.Scoreform_sectione_question48!=null)
			child0++;
			if(this.Scoreform_sectione_question47!=null)
			child0++;
			if(this.Scoreform_sectione_question46!=null)
			child0++;
			if(this.Scoreform_sectione_question45!=null)
			child0++;
			if(this.Scoreform_sectione_question44!=null)
			child0++;
			if(this.Scoreform_sectione_question43!=null)
			child0++;
			if(this.Scoreform_sectione_question42!=null)
			child0++;
			if(this.Scoreform_sectione_question41!=null)
			child0++;
			if(this.Scoreform_sectione_question40!=null)
			child0++;
			if(this.Scoreform_sectionb_question20!=null)
			child0++;
			if(this.Scoreform_sectionf_question59!=null)
			child0++;
			if(this.Scoreform_sectionf_question58!=null)
			child0++;
			if(this.Scoreform_sectionf_question57!=null)
			child0++;
			if(this.Scoreform_sectionf_question56!=null)
			child0++;
			if(this.Scoreform_sectionf_question55!=null)
			child0++;
			if(this.Scoreform_sectionf_question54!=null)
			child0++;
			if(this.Scoreform_sectionf_question53!=null)
			child0++;
			if(this.Scoreform_sectionf_question52!=null)
			child0++;
			if(this.Scoreform_sectionf_question51!=null)
			child0++;
			if(this.Scoreform_sectionf_question50!=null)
			child0++;
			if(this.Scoreform_sectionc_question30!=null)
			child0++;
			if(this.Scoreform_sectionb_question19!=null)
			child0++;
			if(this.Scoreform_sectionb_question18!=null)
			child0++;
			if(this.Scoreform_sectionb_question17!=null)
			child0++;
			if(this.Scoreform_sectionb_question16!=null)
			child0++;
			if(this.Scoreform_sectionb_question15!=null)
			child0++;
			if(this.Scoreform_sectionb_question14!=null)
			child0++;
			if(this.Scoreform_sectionb_question13!=null)
			child0++;
			if(this.Scoreform_sectiong_question65!=null)
			child0++;
			if(this.Scoreform_sectiong_question64!=null)
			child0++;
			if(this.Scoreform_sectiong_question63!=null)
			child0++;
			if(this.Scoreform_sectiong_question62!=null)
			child0++;
			if(this.Scoreform_sectiong_question61!=null)
			child0++;
			if(this.Scoreform_sectiong_question60!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<srs:ScoreForm";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_filledoutbyrelat!=null){
			xmlTxt+="\n<srs:filledOutByRelat";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_filledoutbyrelat.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</srs:filledOutByRelat>";
		}
			var child1=0;
			var att1=0;
			if(this.Scoreform_sectiona_question12!=null)
			child1++;
			if(this.Scoreform_sectiona_question11!=null)
			child1++;
			if(this.Scoreform_sectiona_question10!=null)
			child1++;
			if(this.Scoreform_sectiona_question9!=null)
			child1++;
			if(this.Scoreform_sectiona_question8!=null)
			child1++;
			if(this.Scoreform_sectiona_question7!=null)
			child1++;
			if(this.Scoreform_sectiona_question6!=null)
			child1++;
			if(this.Scoreform_sectiona_question5!=null)
			child1++;
			if(this.Scoreform_sectiona_question4!=null)
			child1++;
			if(this.Scoreform_sectiona_question3!=null)
			child1++;
			if(this.Scoreform_sectiona_question2!=null)
			child1++;
			if(this.Scoreform_sectiona_question1!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<srs:sectionA";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiona_question1!=null){
			xmlTxt+="\n<srs:question1";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_question1;
			xmlTxt+="</srs:question1>";
		}
		if (this.Scoreform_sectiona_question2!=null){
			xmlTxt+="\n<srs:question2";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_question2;
			xmlTxt+="</srs:question2>";
		}
		if (this.Scoreform_sectiona_question3!=null){
			xmlTxt+="\n<srs:question3";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_question3;
			xmlTxt+="</srs:question3>";
		}
		if (this.Scoreform_sectiona_question4!=null){
			xmlTxt+="\n<srs:question4";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_question4;
			xmlTxt+="</srs:question4>";
		}
		if (this.Scoreform_sectiona_question5!=null){
			xmlTxt+="\n<srs:question5";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_question5;
			xmlTxt+="</srs:question5>";
		}
		if (this.Scoreform_sectiona_question6!=null){
			xmlTxt+="\n<srs:question6";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_question6;
			xmlTxt+="</srs:question6>";
		}
		if (this.Scoreform_sectiona_question7!=null){
			xmlTxt+="\n<srs:question7";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_question7;
			xmlTxt+="</srs:question7>";
		}
		if (this.Scoreform_sectiona_question8!=null){
			xmlTxt+="\n<srs:question8";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_question8;
			xmlTxt+="</srs:question8>";
		}
		if (this.Scoreform_sectiona_question9!=null){
			xmlTxt+="\n<srs:question9";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_question9;
			xmlTxt+="</srs:question9>";
		}
		if (this.Scoreform_sectiona_question10!=null){
			xmlTxt+="\n<srs:question10";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_question10;
			xmlTxt+="</srs:question10>";
		}
		if (this.Scoreform_sectiona_question11!=null){
			xmlTxt+="\n<srs:question11";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_question11;
			xmlTxt+="</srs:question11>";
		}
		if (this.Scoreform_sectiona_question12!=null){
			xmlTxt+="\n<srs:question12";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_question12;
			xmlTxt+="</srs:question12>";
		}
				xmlTxt+="\n</srs:sectionA>";
			}
			}

			var child2=0;
			var att2=0;
			if(this.Scoreform_sectionb_question16!=null)
			child2++;
			if(this.Scoreform_sectionb_question15!=null)
			child2++;
			if(this.Scoreform_sectionb_question14!=null)
			child2++;
			if(this.Scoreform_sectionb_question13!=null)
			child2++;
			if(this.Scoreform_sectionb_question20!=null)
			child2++;
			if(this.Scoreform_sectionb_question19!=null)
			child2++;
			if(this.Scoreform_sectionb_question18!=null)
			child2++;
			if(this.Scoreform_sectionb_question17!=null)
			child2++;
			if(child2>0 || att2>0){
				xmlTxt+="\n<srs:sectionB";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionb_question13!=null){
			xmlTxt+="\n<srs:question13";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_question13;
			xmlTxt+="</srs:question13>";
		}
		if (this.Scoreform_sectionb_question14!=null){
			xmlTxt+="\n<srs:question14";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_question14;
			xmlTxt+="</srs:question14>";
		}
		if (this.Scoreform_sectionb_question15!=null){
			xmlTxt+="\n<srs:question15";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_question15;
			xmlTxt+="</srs:question15>";
		}
		if (this.Scoreform_sectionb_question16!=null){
			xmlTxt+="\n<srs:question16";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_question16;
			xmlTxt+="</srs:question16>";
		}
		if (this.Scoreform_sectionb_question17!=null){
			xmlTxt+="\n<srs:question17";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_question17;
			xmlTxt+="</srs:question17>";
		}
		if (this.Scoreform_sectionb_question18!=null){
			xmlTxt+="\n<srs:question18";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_question18;
			xmlTxt+="</srs:question18>";
		}
		if (this.Scoreform_sectionb_question19!=null){
			xmlTxt+="\n<srs:question19";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_question19;
			xmlTxt+="</srs:question19>";
		}
		if (this.Scoreform_sectionb_question20!=null){
			xmlTxt+="\n<srs:question20";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_question20;
			xmlTxt+="</srs:question20>";
		}
				xmlTxt+="\n</srs:sectionB>";
			}
			}

			var child3=0;
			var att3=0;
			if(this.Scoreform_sectionc_question29!=null)
			child3++;
			if(this.Scoreform_sectionc_question30!=null)
			child3++;
			if(this.Scoreform_sectionc_question28!=null)
			child3++;
			if(this.Scoreform_sectionc_question27!=null)
			child3++;
			if(this.Scoreform_sectionc_question26!=null)
			child3++;
			if(this.Scoreform_sectionc_question25!=null)
			child3++;
			if(this.Scoreform_sectionc_question24!=null)
			child3++;
			if(this.Scoreform_sectionc_question23!=null)
			child3++;
			if(this.Scoreform_sectionc_question22!=null)
			child3++;
			if(this.Scoreform_sectionc_question21!=null)
			child3++;
			if(child3>0 || att3>0){
				xmlTxt+="\n<srs:sectionC";
			if(child3==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionc_question21!=null){
			xmlTxt+="\n<srs:question21";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc_question21;
			xmlTxt+="</srs:question21>";
		}
		if (this.Scoreform_sectionc_question22!=null){
			xmlTxt+="\n<srs:question22";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc_question22;
			xmlTxt+="</srs:question22>";
		}
		if (this.Scoreform_sectionc_question23!=null){
			xmlTxt+="\n<srs:question23";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc_question23;
			xmlTxt+="</srs:question23>";
		}
		if (this.Scoreform_sectionc_question24!=null){
			xmlTxt+="\n<srs:question24";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc_question24;
			xmlTxt+="</srs:question24>";
		}
		if (this.Scoreform_sectionc_question25!=null){
			xmlTxt+="\n<srs:question25";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc_question25;
			xmlTxt+="</srs:question25>";
		}
		if (this.Scoreform_sectionc_question26!=null){
			xmlTxt+="\n<srs:question26";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc_question26;
			xmlTxt+="</srs:question26>";
		}
		if (this.Scoreform_sectionc_question27!=null){
			xmlTxt+="\n<srs:question27";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc_question27;
			xmlTxt+="</srs:question27>";
		}
		if (this.Scoreform_sectionc_question28!=null){
			xmlTxt+="\n<srs:question28";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc_question28;
			xmlTxt+="</srs:question28>";
		}
		if (this.Scoreform_sectionc_question29!=null){
			xmlTxt+="\n<srs:question29";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc_question29;
			xmlTxt+="</srs:question29>";
		}
		if (this.Scoreform_sectionc_question30!=null){
			xmlTxt+="\n<srs:question30";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc_question30;
			xmlTxt+="</srs:question30>";
		}
				xmlTxt+="\n</srs:sectionC>";
			}
			}

			var child4=0;
			var att4=0;
			if(this.Scoreform_sectiond_question32!=null)
			child4++;
			if(this.Scoreform_sectiond_question31!=null)
			child4++;
			if(this.Scoreform_sectiond_question39!=null)
			child4++;
			if(this.Scoreform_sectiond_question38!=null)
			child4++;
			if(this.Scoreform_sectiond_question37!=null)
			child4++;
			if(this.Scoreform_sectiond_question36!=null)
			child4++;
			if(this.Scoreform_sectiond_question35!=null)
			child4++;
			if(this.Scoreform_sectiond_question34!=null)
			child4++;
			if(this.Scoreform_sectiond_question33!=null)
			child4++;
			if(child4>0 || att4>0){
				xmlTxt+="\n<srs:sectionD";
			if(child4==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiond_question31!=null){
			xmlTxt+="\n<srs:question31";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_question31;
			xmlTxt+="</srs:question31>";
		}
		if (this.Scoreform_sectiond_question32!=null){
			xmlTxt+="\n<srs:question32";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_question32;
			xmlTxt+="</srs:question32>";
		}
		if (this.Scoreform_sectiond_question33!=null){
			xmlTxt+="\n<srs:question33";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_question33;
			xmlTxt+="</srs:question33>";
		}
		if (this.Scoreform_sectiond_question34!=null){
			xmlTxt+="\n<srs:question34";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_question34;
			xmlTxt+="</srs:question34>";
		}
		if (this.Scoreform_sectiond_question35!=null){
			xmlTxt+="\n<srs:question35";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_question35;
			xmlTxt+="</srs:question35>";
		}
		if (this.Scoreform_sectiond_question36!=null){
			xmlTxt+="\n<srs:question36";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_question36;
			xmlTxt+="</srs:question36>";
		}
		if (this.Scoreform_sectiond_question37!=null){
			xmlTxt+="\n<srs:question37";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_question37;
			xmlTxt+="</srs:question37>";
		}
		if (this.Scoreform_sectiond_question38!=null){
			xmlTxt+="\n<srs:question38";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_question38;
			xmlTxt+="</srs:question38>";
		}
		if (this.Scoreform_sectiond_question39!=null){
			xmlTxt+="\n<srs:question39";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_question39;
			xmlTxt+="</srs:question39>";
		}
				xmlTxt+="\n</srs:sectionD>";
			}
			}

			var child5=0;
			var att5=0;
			if(this.Scoreform_sectione_question49!=null)
			child5++;
			if(this.Scoreform_sectione_question48!=null)
			child5++;
			if(this.Scoreform_sectione_question47!=null)
			child5++;
			if(this.Scoreform_sectione_question46!=null)
			child5++;
			if(this.Scoreform_sectione_question45!=null)
			child5++;
			if(this.Scoreform_sectione_question44!=null)
			child5++;
			if(this.Scoreform_sectione_question43!=null)
			child5++;
			if(this.Scoreform_sectione_question42!=null)
			child5++;
			if(this.Scoreform_sectione_question41!=null)
			child5++;
			if(this.Scoreform_sectione_question40!=null)
			child5++;
			if(child5>0 || att5>0){
				xmlTxt+="\n<srs:sectionE";
			if(child5==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectione_question40!=null){
			xmlTxt+="\n<srs:question40";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectione_question40;
			xmlTxt+="</srs:question40>";
		}
		if (this.Scoreform_sectione_question41!=null){
			xmlTxt+="\n<srs:question41";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectione_question41;
			xmlTxt+="</srs:question41>";
		}
		if (this.Scoreform_sectione_question42!=null){
			xmlTxt+="\n<srs:question42";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectione_question42;
			xmlTxt+="</srs:question42>";
		}
		if (this.Scoreform_sectione_question43!=null){
			xmlTxt+="\n<srs:question43";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectione_question43;
			xmlTxt+="</srs:question43>";
		}
		if (this.Scoreform_sectione_question44!=null){
			xmlTxt+="\n<srs:question44";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectione_question44;
			xmlTxt+="</srs:question44>";
		}
		if (this.Scoreform_sectione_question45!=null){
			xmlTxt+="\n<srs:question45";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectione_question45;
			xmlTxt+="</srs:question45>";
		}
		if (this.Scoreform_sectione_question46!=null){
			xmlTxt+="\n<srs:question46";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectione_question46;
			xmlTxt+="</srs:question46>";
		}
		if (this.Scoreform_sectione_question47!=null){
			xmlTxt+="\n<srs:question47";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectione_question47;
			xmlTxt+="</srs:question47>";
		}
		if (this.Scoreform_sectione_question48!=null){
			xmlTxt+="\n<srs:question48";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectione_question48;
			xmlTxt+="</srs:question48>";
		}
		if (this.Scoreform_sectione_question49!=null){
			xmlTxt+="\n<srs:question49";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectione_question49;
			xmlTxt+="</srs:question49>";
		}
				xmlTxt+="\n</srs:sectionE>";
			}
			}

			var child6=0;
			var att6=0;
			if(this.Scoreform_sectionf_question53!=null)
			child6++;
			if(this.Scoreform_sectionf_question52!=null)
			child6++;
			if(this.Scoreform_sectionf_question51!=null)
			child6++;
			if(this.Scoreform_sectionf_question50!=null)
			child6++;
			if(this.Scoreform_sectionf_question59!=null)
			child6++;
			if(this.Scoreform_sectionf_question58!=null)
			child6++;
			if(this.Scoreform_sectionf_question57!=null)
			child6++;
			if(this.Scoreform_sectionf_question56!=null)
			child6++;
			if(this.Scoreform_sectionf_question55!=null)
			child6++;
			if(this.Scoreform_sectionf_question54!=null)
			child6++;
			if(child6>0 || att6>0){
				xmlTxt+="\n<srs:sectionF";
			if(child6==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionf_question50!=null){
			xmlTxt+="\n<srs:question50";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionf_question50;
			xmlTxt+="</srs:question50>";
		}
		if (this.Scoreform_sectionf_question51!=null){
			xmlTxt+="\n<srs:question51";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionf_question51;
			xmlTxt+="</srs:question51>";
		}
		if (this.Scoreform_sectionf_question52!=null){
			xmlTxt+="\n<srs:question52";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionf_question52;
			xmlTxt+="</srs:question52>";
		}
		if (this.Scoreform_sectionf_question53!=null){
			xmlTxt+="\n<srs:question53";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionf_question53;
			xmlTxt+="</srs:question53>";
		}
		if (this.Scoreform_sectionf_question54!=null){
			xmlTxt+="\n<srs:question54";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionf_question54;
			xmlTxt+="</srs:question54>";
		}
		if (this.Scoreform_sectionf_question55!=null){
			xmlTxt+="\n<srs:question55";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionf_question55;
			xmlTxt+="</srs:question55>";
		}
		if (this.Scoreform_sectionf_question56!=null){
			xmlTxt+="\n<srs:question56";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionf_question56;
			xmlTxt+="</srs:question56>";
		}
		if (this.Scoreform_sectionf_question57!=null){
			xmlTxt+="\n<srs:question57";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionf_question57;
			xmlTxt+="</srs:question57>";
		}
		if (this.Scoreform_sectionf_question58!=null){
			xmlTxt+="\n<srs:question58";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionf_question58;
			xmlTxt+="</srs:question58>";
		}
		if (this.Scoreform_sectionf_question59!=null){
			xmlTxt+="\n<srs:question59";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionf_question59;
			xmlTxt+="</srs:question59>";
		}
				xmlTxt+="\n</srs:sectionF>";
			}
			}

			var child7=0;
			var att7=0;
			if(this.Scoreform_sectiong_question63!=null)
			child7++;
			if(this.Scoreform_sectiong_question62!=null)
			child7++;
			if(this.Scoreform_sectiong_question61!=null)
			child7++;
			if(this.Scoreform_sectiong_question60!=null)
			child7++;
			if(this.Scoreform_sectiong_question65!=null)
			child7++;
			if(this.Scoreform_sectiong_question64!=null)
			child7++;
			if(child7>0 || att7>0){
				xmlTxt+="\n<srs:sectionG";
			if(child7==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiong_question60!=null){
			xmlTxt+="\n<srs:question60";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiong_question60;
			xmlTxt+="</srs:question60>";
		}
		if (this.Scoreform_sectiong_question61!=null){
			xmlTxt+="\n<srs:question61";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiong_question61;
			xmlTxt+="</srs:question61>";
		}
		if (this.Scoreform_sectiong_question62!=null){
			xmlTxt+="\n<srs:question62";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiong_question62;
			xmlTxt+="</srs:question62>";
		}
		if (this.Scoreform_sectiong_question63!=null){
			xmlTxt+="\n<srs:question63";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiong_question63;
			xmlTxt+="</srs:question63>";
		}
		if (this.Scoreform_sectiong_question64!=null){
			xmlTxt+="\n<srs:question64";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiong_question64;
			xmlTxt+="</srs:question64>";
		}
		if (this.Scoreform_sectiong_question65!=null){
			xmlTxt+="\n<srs:question65";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiong_question65;
			xmlTxt+="</srs:question65>";
		}
				xmlTxt+="\n</srs:sectionG>";
			}
			}

				xmlTxt+="\n</srs:ScoreForm>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.TotalRaw!=null) return true;
		if (this.TotalT!=null) return true;
		if (this.TotalNote!=null) return true;
		if (this.Testtype!=null) return true;
			if(this.Scoreform_sectionc_question29!=null) return true;
			if(this.Scoreform_sectionc_question28!=null) return true;
			if(this.Scoreform_sectionc_question27!=null) return true;
			if(this.Scoreform_sectionc_question26!=null) return true;
			if(this.Scoreform_sectionc_question25!=null) return true;
			if(this.Scoreform_sectionc_question24!=null) return true;
			if(this.Scoreform_sectionc_question23!=null) return true;
			if(this.Scoreform_sectionc_question22!=null) return true;
			if(this.Scoreform_sectionc_question21!=null) return true;
			if(this.Scoreform_sectiona_question9!=null) return true;
			if(this.Scoreform_filledoutbyrelat!=null) return true;
			if(this.Scoreform_sectiona_question8!=null) return true;
			if(this.Scoreform_sectiona_question7!=null) return true;
			if(this.Scoreform_sectiona_question6!=null) return true;
			if(this.Scoreform_sectiona_question5!=null) return true;
			if(this.Scoreform_sectiona_question4!=null) return true;
			if(this.Scoreform_sectiona_question3!=null) return true;
			if(this.Scoreform_sectiond_question39!=null) return true;
			if(this.Scoreform_sectiona_question2!=null) return true;
			if(this.Scoreform_sectiond_question38!=null) return true;
			if(this.Scoreform_sectiond_question37!=null) return true;
			if(this.Scoreform_sectiona_question1!=null) return true;
			if(this.Scoreform_sectiond_question36!=null) return true;
			if(this.Scoreform_sectiond_question35!=null) return true;
			if(this.Scoreform_sectiond_question34!=null) return true;
			if(this.Scoreform_sectiond_question33!=null) return true;
			if(this.Scoreform_sectiond_question32!=null) return true;
			if(this.Scoreform_sectiond_question31!=null) return true;
			if(this.Scoreform_sectiona_question12!=null) return true;
			if(this.Scoreform_sectiona_question11!=null) return true;
			if(this.Scoreform_sectiona_question10!=null) return true;
			if(this.Scoreform_sectione_question49!=null) return true;
			if(this.Scoreform_sectione_question48!=null) return true;
			if(this.Scoreform_sectione_question47!=null) return true;
			if(this.Scoreform_sectione_question46!=null) return true;
			if(this.Scoreform_sectione_question45!=null) return true;
			if(this.Scoreform_sectione_question44!=null) return true;
			if(this.Scoreform_sectione_question43!=null) return true;
			if(this.Scoreform_sectione_question42!=null) return true;
			if(this.Scoreform_sectione_question41!=null) return true;
			if(this.Scoreform_sectione_question40!=null) return true;
			if(this.Scoreform_sectionb_question20!=null) return true;
			if(this.Scoreform_sectionf_question59!=null) return true;
			if(this.Scoreform_sectionf_question58!=null) return true;
			if(this.Scoreform_sectionf_question57!=null) return true;
			if(this.Scoreform_sectionf_question56!=null) return true;
			if(this.Scoreform_sectionf_question55!=null) return true;
			if(this.Scoreform_sectionf_question54!=null) return true;
			if(this.Scoreform_sectionf_question53!=null) return true;
			if(this.Scoreform_sectionf_question52!=null) return true;
			if(this.Scoreform_sectionf_question51!=null) return true;
			if(this.Scoreform_sectionf_question50!=null) return true;
			if(this.Scoreform_sectionc_question30!=null) return true;
			if(this.Scoreform_sectionb_question19!=null) return true;
			if(this.Scoreform_sectionb_question18!=null) return true;
			if(this.Scoreform_sectionb_question17!=null) return true;
			if(this.Scoreform_sectionb_question16!=null) return true;
			if(this.Scoreform_sectionb_question15!=null) return true;
			if(this.Scoreform_sectionb_question14!=null) return true;
			if(this.Scoreform_sectionb_question13!=null) return true;
			if(this.Scoreform_sectiong_question65!=null) return true;
			if(this.Scoreform_sectiong_question64!=null) return true;
			if(this.Scoreform_sectiong_question63!=null) return true;
			if(this.Scoreform_sectiong_question62!=null) return true;
			if(this.Scoreform_sectiong_question61!=null) return true;
			if(this.Scoreform_sectiong_question60!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
