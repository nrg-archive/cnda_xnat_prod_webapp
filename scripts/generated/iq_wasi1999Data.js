/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function iq_wasi1999Data(){
this.xsiType="iq:wasi1999Data";

	this.getSchemaElementName=function(){
		return "wasi1999Data";
	}

	this.getFullSchemaElementName=function(){
		return "iq:wasi1999Data";
	}
this.extension=dynamicJSLoad('iq_abstractIQTest','generated/iq_abstractIQTest.js');

	this.WasiVraw=null;


	function getWasiVraw() {
		return this.WasiVraw;
	}
	this.getWasiVraw=getWasiVraw;


	function setWasiVraw(v){
		this.WasiVraw=v;
	}
	this.setWasiVraw=setWasiVraw;

	this.WasiVt=null;


	function getWasiVt() {
		return this.WasiVt;
	}
	this.getWasiVt=getWasiVt;


	function setWasiVt(v){
		this.WasiVt=v;
	}
	this.setWasiVt=setWasiVt;

	this.WasiVnote=null;


	function getWasiVnote() {
		return this.WasiVnote;
	}
	this.getWasiVnote=getWasiVnote;


	function setWasiVnote(v){
		this.WasiVnote=v;
	}
	this.setWasiVnote=setWasiVnote;

	this.WasiBraw=null;


	function getWasiBraw() {
		return this.WasiBraw;
	}
	this.getWasiBraw=getWasiBraw;


	function setWasiBraw(v){
		this.WasiBraw=v;
	}
	this.setWasiBraw=setWasiBraw;

	this.WasiBt=null;


	function getWasiBt() {
		return this.WasiBt;
	}
	this.getWasiBt=getWasiBt;


	function setWasiBt(v){
		this.WasiBt=v;
	}
	this.setWasiBt=setWasiBt;

	this.WasiBnote=null;


	function getWasiBnote() {
		return this.WasiBnote;
	}
	this.getWasiBnote=getWasiBnote;


	function setWasiBnote(v){
		this.WasiBnote=v;
	}
	this.setWasiBnote=setWasiBnote;

	this.WasiSraw=null;


	function getWasiSraw() {
		return this.WasiSraw;
	}
	this.getWasiSraw=getWasiSraw;


	function setWasiSraw(v){
		this.WasiSraw=v;
	}
	this.setWasiSraw=setWasiSraw;

	this.WasiSt=null;


	function getWasiSt() {
		return this.WasiSt;
	}
	this.getWasiSt=getWasiSt;


	function setWasiSt(v){
		this.WasiSt=v;
	}
	this.setWasiSt=setWasiSt;

	this.WasiSnote=null;


	function getWasiSnote() {
		return this.WasiSnote;
	}
	this.getWasiSnote=getWasiSnote;


	function setWasiSnote(v){
		this.WasiSnote=v;
	}
	this.setWasiSnote=setWasiSnote;

	this.WasiMraw=null;


	function getWasiMraw() {
		return this.WasiMraw;
	}
	this.getWasiMraw=getWasiMraw;


	function setWasiMraw(v){
		this.WasiMraw=v;
	}
	this.setWasiMraw=setWasiMraw;

	this.WasiMt=null;


	function getWasiMt() {
		return this.WasiMt;
	}
	this.getWasiMt=getWasiMt;


	function setWasiMt(v){
		this.WasiMt=v;
	}
	this.setWasiMt=setWasiMt;

	this.WasiMnote=null;


	function getWasiMnote() {
		return this.WasiMnote;
	}
	this.getWasiMnote=getWasiMnote;


	function setWasiMnote(v){
		this.WasiMnote=v;
	}
	this.setWasiMnote=setWasiMnote;

	this.Scoreform_section1_vocab1_response=null;


	function getScoreform_section1_vocab1_response() {
		return this.Scoreform_section1_vocab1_response;
	}
	this.getScoreform_section1_vocab1_response=getScoreform_section1_vocab1_response;


	function setScoreform_section1_vocab1_response(v){
		this.Scoreform_section1_vocab1_response=v;
	}
	this.setScoreform_section1_vocab1_response=setScoreform_section1_vocab1_response;

	this.Scoreform_section1_vocab1_score=null;


	function getScoreform_section1_vocab1_score() {
		return this.Scoreform_section1_vocab1_score;
	}
	this.getScoreform_section1_vocab1_score=getScoreform_section1_vocab1_score;


	function setScoreform_section1_vocab1_score(v){
		this.Scoreform_section1_vocab1_score=v;
	}
	this.setScoreform_section1_vocab1_score=setScoreform_section1_vocab1_score;

	this.Scoreform_section1_vocab2_response=null;


	function getScoreform_section1_vocab2_response() {
		return this.Scoreform_section1_vocab2_response;
	}
	this.getScoreform_section1_vocab2_response=getScoreform_section1_vocab2_response;


	function setScoreform_section1_vocab2_response(v){
		this.Scoreform_section1_vocab2_response=v;
	}
	this.setScoreform_section1_vocab2_response=setScoreform_section1_vocab2_response;

	this.Scoreform_section1_vocab2_score=null;


	function getScoreform_section1_vocab2_score() {
		return this.Scoreform_section1_vocab2_score;
	}
	this.getScoreform_section1_vocab2_score=getScoreform_section1_vocab2_score;


	function setScoreform_section1_vocab2_score(v){
		this.Scoreform_section1_vocab2_score=v;
	}
	this.setScoreform_section1_vocab2_score=setScoreform_section1_vocab2_score;

	this.Scoreform_section1_vocab3_response=null;


	function getScoreform_section1_vocab3_response() {
		return this.Scoreform_section1_vocab3_response;
	}
	this.getScoreform_section1_vocab3_response=getScoreform_section1_vocab3_response;


	function setScoreform_section1_vocab3_response(v){
		this.Scoreform_section1_vocab3_response=v;
	}
	this.setScoreform_section1_vocab3_response=setScoreform_section1_vocab3_response;

	this.Scoreform_section1_vocab3_score=null;


	function getScoreform_section1_vocab3_score() {
		return this.Scoreform_section1_vocab3_score;
	}
	this.getScoreform_section1_vocab3_score=getScoreform_section1_vocab3_score;


	function setScoreform_section1_vocab3_score(v){
		this.Scoreform_section1_vocab3_score=v;
	}
	this.setScoreform_section1_vocab3_score=setScoreform_section1_vocab3_score;

	this.Scoreform_section1_vocab4_response=null;


	function getScoreform_section1_vocab4_response() {
		return this.Scoreform_section1_vocab4_response;
	}
	this.getScoreform_section1_vocab4_response=getScoreform_section1_vocab4_response;


	function setScoreform_section1_vocab4_response(v){
		this.Scoreform_section1_vocab4_response=v;
	}
	this.setScoreform_section1_vocab4_response=setScoreform_section1_vocab4_response;

	this.Scoreform_section1_vocab4_score=null;


	function getScoreform_section1_vocab4_score() {
		return this.Scoreform_section1_vocab4_score;
	}
	this.getScoreform_section1_vocab4_score=getScoreform_section1_vocab4_score;


	function setScoreform_section1_vocab4_score(v){
		this.Scoreform_section1_vocab4_score=v;
	}
	this.setScoreform_section1_vocab4_score=setScoreform_section1_vocab4_score;

	this.Scoreform_section1_vocab5_response=null;


	function getScoreform_section1_vocab5_response() {
		return this.Scoreform_section1_vocab5_response;
	}
	this.getScoreform_section1_vocab5_response=getScoreform_section1_vocab5_response;


	function setScoreform_section1_vocab5_response(v){
		this.Scoreform_section1_vocab5_response=v;
	}
	this.setScoreform_section1_vocab5_response=setScoreform_section1_vocab5_response;

	this.Scoreform_section1_vocab5_score=null;


	function getScoreform_section1_vocab5_score() {
		return this.Scoreform_section1_vocab5_score;
	}
	this.getScoreform_section1_vocab5_score=getScoreform_section1_vocab5_score;


	function setScoreform_section1_vocab5_score(v){
		this.Scoreform_section1_vocab5_score=v;
	}
	this.setScoreform_section1_vocab5_score=setScoreform_section1_vocab5_score;

	this.Scoreform_section1_vocab6_response=null;


	function getScoreform_section1_vocab6_response() {
		return this.Scoreform_section1_vocab6_response;
	}
	this.getScoreform_section1_vocab6_response=getScoreform_section1_vocab6_response;


	function setScoreform_section1_vocab6_response(v){
		this.Scoreform_section1_vocab6_response=v;
	}
	this.setScoreform_section1_vocab6_response=setScoreform_section1_vocab6_response;

	this.Scoreform_section1_vocab6_score=null;


	function getScoreform_section1_vocab6_score() {
		return this.Scoreform_section1_vocab6_score;
	}
	this.getScoreform_section1_vocab6_score=getScoreform_section1_vocab6_score;


	function setScoreform_section1_vocab6_score(v){
		this.Scoreform_section1_vocab6_score=v;
	}
	this.setScoreform_section1_vocab6_score=setScoreform_section1_vocab6_score;

	this.Scoreform_section1_vocab7_response=null;


	function getScoreform_section1_vocab7_response() {
		return this.Scoreform_section1_vocab7_response;
	}
	this.getScoreform_section1_vocab7_response=getScoreform_section1_vocab7_response;


	function setScoreform_section1_vocab7_response(v){
		this.Scoreform_section1_vocab7_response=v;
	}
	this.setScoreform_section1_vocab7_response=setScoreform_section1_vocab7_response;

	this.Scoreform_section1_vocab7_score=null;


	function getScoreform_section1_vocab7_score() {
		return this.Scoreform_section1_vocab7_score;
	}
	this.getScoreform_section1_vocab7_score=getScoreform_section1_vocab7_score;


	function setScoreform_section1_vocab7_score(v){
		this.Scoreform_section1_vocab7_score=v;
	}
	this.setScoreform_section1_vocab7_score=setScoreform_section1_vocab7_score;

	this.Scoreform_section1_vocab8_response=null;


	function getScoreform_section1_vocab8_response() {
		return this.Scoreform_section1_vocab8_response;
	}
	this.getScoreform_section1_vocab8_response=getScoreform_section1_vocab8_response;


	function setScoreform_section1_vocab8_response(v){
		this.Scoreform_section1_vocab8_response=v;
	}
	this.setScoreform_section1_vocab8_response=setScoreform_section1_vocab8_response;

	this.Scoreform_section1_vocab8_score=null;


	function getScoreform_section1_vocab8_score() {
		return this.Scoreform_section1_vocab8_score;
	}
	this.getScoreform_section1_vocab8_score=getScoreform_section1_vocab8_score;


	function setScoreform_section1_vocab8_score(v){
		this.Scoreform_section1_vocab8_score=v;
	}
	this.setScoreform_section1_vocab8_score=setScoreform_section1_vocab8_score;

	this.Scoreform_section1_vocab9_response=null;


	function getScoreform_section1_vocab9_response() {
		return this.Scoreform_section1_vocab9_response;
	}
	this.getScoreform_section1_vocab9_response=getScoreform_section1_vocab9_response;


	function setScoreform_section1_vocab9_response(v){
		this.Scoreform_section1_vocab9_response=v;
	}
	this.setScoreform_section1_vocab9_response=setScoreform_section1_vocab9_response;

	this.Scoreform_section1_vocab9_score=null;


	function getScoreform_section1_vocab9_score() {
		return this.Scoreform_section1_vocab9_score;
	}
	this.getScoreform_section1_vocab9_score=getScoreform_section1_vocab9_score;


	function setScoreform_section1_vocab9_score(v){
		this.Scoreform_section1_vocab9_score=v;
	}
	this.setScoreform_section1_vocab9_score=setScoreform_section1_vocab9_score;

	this.Scoreform_section1_vocab10_response=null;


	function getScoreform_section1_vocab10_response() {
		return this.Scoreform_section1_vocab10_response;
	}
	this.getScoreform_section1_vocab10_response=getScoreform_section1_vocab10_response;


	function setScoreform_section1_vocab10_response(v){
		this.Scoreform_section1_vocab10_response=v;
	}
	this.setScoreform_section1_vocab10_response=setScoreform_section1_vocab10_response;

	this.Scoreform_section1_vocab10_score=null;


	function getScoreform_section1_vocab10_score() {
		return this.Scoreform_section1_vocab10_score;
	}
	this.getScoreform_section1_vocab10_score=getScoreform_section1_vocab10_score;


	function setScoreform_section1_vocab10_score(v){
		this.Scoreform_section1_vocab10_score=v;
	}
	this.setScoreform_section1_vocab10_score=setScoreform_section1_vocab10_score;

	this.Scoreform_section1_vocab11_response=null;


	function getScoreform_section1_vocab11_response() {
		return this.Scoreform_section1_vocab11_response;
	}
	this.getScoreform_section1_vocab11_response=getScoreform_section1_vocab11_response;


	function setScoreform_section1_vocab11_response(v){
		this.Scoreform_section1_vocab11_response=v;
	}
	this.setScoreform_section1_vocab11_response=setScoreform_section1_vocab11_response;

	this.Scoreform_section1_vocab11_score=null;


	function getScoreform_section1_vocab11_score() {
		return this.Scoreform_section1_vocab11_score;
	}
	this.getScoreform_section1_vocab11_score=getScoreform_section1_vocab11_score;


	function setScoreform_section1_vocab11_score(v){
		this.Scoreform_section1_vocab11_score=v;
	}
	this.setScoreform_section1_vocab11_score=setScoreform_section1_vocab11_score;

	this.Scoreform_section1_vocab12_response=null;


	function getScoreform_section1_vocab12_response() {
		return this.Scoreform_section1_vocab12_response;
	}
	this.getScoreform_section1_vocab12_response=getScoreform_section1_vocab12_response;


	function setScoreform_section1_vocab12_response(v){
		this.Scoreform_section1_vocab12_response=v;
	}
	this.setScoreform_section1_vocab12_response=setScoreform_section1_vocab12_response;

	this.Scoreform_section1_vocab12_score=null;


	function getScoreform_section1_vocab12_score() {
		return this.Scoreform_section1_vocab12_score;
	}
	this.getScoreform_section1_vocab12_score=getScoreform_section1_vocab12_score;


	function setScoreform_section1_vocab12_score(v){
		this.Scoreform_section1_vocab12_score=v;
	}
	this.setScoreform_section1_vocab12_score=setScoreform_section1_vocab12_score;

	this.Scoreform_section1_vocab13_response=null;


	function getScoreform_section1_vocab13_response() {
		return this.Scoreform_section1_vocab13_response;
	}
	this.getScoreform_section1_vocab13_response=getScoreform_section1_vocab13_response;


	function setScoreform_section1_vocab13_response(v){
		this.Scoreform_section1_vocab13_response=v;
	}
	this.setScoreform_section1_vocab13_response=setScoreform_section1_vocab13_response;

	this.Scoreform_section1_vocab13_score=null;


	function getScoreform_section1_vocab13_score() {
		return this.Scoreform_section1_vocab13_score;
	}
	this.getScoreform_section1_vocab13_score=getScoreform_section1_vocab13_score;


	function setScoreform_section1_vocab13_score(v){
		this.Scoreform_section1_vocab13_score=v;
	}
	this.setScoreform_section1_vocab13_score=setScoreform_section1_vocab13_score;

	this.Scoreform_section1_vocab14_response=null;


	function getScoreform_section1_vocab14_response() {
		return this.Scoreform_section1_vocab14_response;
	}
	this.getScoreform_section1_vocab14_response=getScoreform_section1_vocab14_response;


	function setScoreform_section1_vocab14_response(v){
		this.Scoreform_section1_vocab14_response=v;
	}
	this.setScoreform_section1_vocab14_response=setScoreform_section1_vocab14_response;

	this.Scoreform_section1_vocab14_score=null;


	function getScoreform_section1_vocab14_score() {
		return this.Scoreform_section1_vocab14_score;
	}
	this.getScoreform_section1_vocab14_score=getScoreform_section1_vocab14_score;


	function setScoreform_section1_vocab14_score(v){
		this.Scoreform_section1_vocab14_score=v;
	}
	this.setScoreform_section1_vocab14_score=setScoreform_section1_vocab14_score;

	this.Scoreform_section1_vocab15_response=null;


	function getScoreform_section1_vocab15_response() {
		return this.Scoreform_section1_vocab15_response;
	}
	this.getScoreform_section1_vocab15_response=getScoreform_section1_vocab15_response;


	function setScoreform_section1_vocab15_response(v){
		this.Scoreform_section1_vocab15_response=v;
	}
	this.setScoreform_section1_vocab15_response=setScoreform_section1_vocab15_response;

	this.Scoreform_section1_vocab15_score=null;


	function getScoreform_section1_vocab15_score() {
		return this.Scoreform_section1_vocab15_score;
	}
	this.getScoreform_section1_vocab15_score=getScoreform_section1_vocab15_score;


	function setScoreform_section1_vocab15_score(v){
		this.Scoreform_section1_vocab15_score=v;
	}
	this.setScoreform_section1_vocab15_score=setScoreform_section1_vocab15_score;

	this.Scoreform_section1_vocab16_response=null;


	function getScoreform_section1_vocab16_response() {
		return this.Scoreform_section1_vocab16_response;
	}
	this.getScoreform_section1_vocab16_response=getScoreform_section1_vocab16_response;


	function setScoreform_section1_vocab16_response(v){
		this.Scoreform_section1_vocab16_response=v;
	}
	this.setScoreform_section1_vocab16_response=setScoreform_section1_vocab16_response;

	this.Scoreform_section1_vocab16_score=null;


	function getScoreform_section1_vocab16_score() {
		return this.Scoreform_section1_vocab16_score;
	}
	this.getScoreform_section1_vocab16_score=getScoreform_section1_vocab16_score;


	function setScoreform_section1_vocab16_score(v){
		this.Scoreform_section1_vocab16_score=v;
	}
	this.setScoreform_section1_vocab16_score=setScoreform_section1_vocab16_score;

	this.Scoreform_section1_vocab17_response=null;


	function getScoreform_section1_vocab17_response() {
		return this.Scoreform_section1_vocab17_response;
	}
	this.getScoreform_section1_vocab17_response=getScoreform_section1_vocab17_response;


	function setScoreform_section1_vocab17_response(v){
		this.Scoreform_section1_vocab17_response=v;
	}
	this.setScoreform_section1_vocab17_response=setScoreform_section1_vocab17_response;

	this.Scoreform_section1_vocab17_score=null;


	function getScoreform_section1_vocab17_score() {
		return this.Scoreform_section1_vocab17_score;
	}
	this.getScoreform_section1_vocab17_score=getScoreform_section1_vocab17_score;


	function setScoreform_section1_vocab17_score(v){
		this.Scoreform_section1_vocab17_score=v;
	}
	this.setScoreform_section1_vocab17_score=setScoreform_section1_vocab17_score;

	this.Scoreform_section1_vocab18_response=null;


	function getScoreform_section1_vocab18_response() {
		return this.Scoreform_section1_vocab18_response;
	}
	this.getScoreform_section1_vocab18_response=getScoreform_section1_vocab18_response;


	function setScoreform_section1_vocab18_response(v){
		this.Scoreform_section1_vocab18_response=v;
	}
	this.setScoreform_section1_vocab18_response=setScoreform_section1_vocab18_response;

	this.Scoreform_section1_vocab18_score=null;


	function getScoreform_section1_vocab18_score() {
		return this.Scoreform_section1_vocab18_score;
	}
	this.getScoreform_section1_vocab18_score=getScoreform_section1_vocab18_score;


	function setScoreform_section1_vocab18_score(v){
		this.Scoreform_section1_vocab18_score=v;
	}
	this.setScoreform_section1_vocab18_score=setScoreform_section1_vocab18_score;

	this.Scoreform_section1_vocab19_response=null;


	function getScoreform_section1_vocab19_response() {
		return this.Scoreform_section1_vocab19_response;
	}
	this.getScoreform_section1_vocab19_response=getScoreform_section1_vocab19_response;


	function setScoreform_section1_vocab19_response(v){
		this.Scoreform_section1_vocab19_response=v;
	}
	this.setScoreform_section1_vocab19_response=setScoreform_section1_vocab19_response;

	this.Scoreform_section1_vocab19_score=null;


	function getScoreform_section1_vocab19_score() {
		return this.Scoreform_section1_vocab19_score;
	}
	this.getScoreform_section1_vocab19_score=getScoreform_section1_vocab19_score;


	function setScoreform_section1_vocab19_score(v){
		this.Scoreform_section1_vocab19_score=v;
	}
	this.setScoreform_section1_vocab19_score=setScoreform_section1_vocab19_score;

	this.Scoreform_section1_vocab20_response=null;


	function getScoreform_section1_vocab20_response() {
		return this.Scoreform_section1_vocab20_response;
	}
	this.getScoreform_section1_vocab20_response=getScoreform_section1_vocab20_response;


	function setScoreform_section1_vocab20_response(v){
		this.Scoreform_section1_vocab20_response=v;
	}
	this.setScoreform_section1_vocab20_response=setScoreform_section1_vocab20_response;

	this.Scoreform_section1_vocab20_score=null;


	function getScoreform_section1_vocab20_score() {
		return this.Scoreform_section1_vocab20_score;
	}
	this.getScoreform_section1_vocab20_score=getScoreform_section1_vocab20_score;


	function setScoreform_section1_vocab20_score(v){
		this.Scoreform_section1_vocab20_score=v;
	}
	this.setScoreform_section1_vocab20_score=setScoreform_section1_vocab20_score;

	this.Scoreform_section1_vocab21_response=null;


	function getScoreform_section1_vocab21_response() {
		return this.Scoreform_section1_vocab21_response;
	}
	this.getScoreform_section1_vocab21_response=getScoreform_section1_vocab21_response;


	function setScoreform_section1_vocab21_response(v){
		this.Scoreform_section1_vocab21_response=v;
	}
	this.setScoreform_section1_vocab21_response=setScoreform_section1_vocab21_response;

	this.Scoreform_section1_vocab21_score=null;


	function getScoreform_section1_vocab21_score() {
		return this.Scoreform_section1_vocab21_score;
	}
	this.getScoreform_section1_vocab21_score=getScoreform_section1_vocab21_score;


	function setScoreform_section1_vocab21_score(v){
		this.Scoreform_section1_vocab21_score=v;
	}
	this.setScoreform_section1_vocab21_score=setScoreform_section1_vocab21_score;

	this.Scoreform_section1_vocab22_response=null;


	function getScoreform_section1_vocab22_response() {
		return this.Scoreform_section1_vocab22_response;
	}
	this.getScoreform_section1_vocab22_response=getScoreform_section1_vocab22_response;


	function setScoreform_section1_vocab22_response(v){
		this.Scoreform_section1_vocab22_response=v;
	}
	this.setScoreform_section1_vocab22_response=setScoreform_section1_vocab22_response;

	this.Scoreform_section1_vocab22_score=null;


	function getScoreform_section1_vocab22_score() {
		return this.Scoreform_section1_vocab22_score;
	}
	this.getScoreform_section1_vocab22_score=getScoreform_section1_vocab22_score;


	function setScoreform_section1_vocab22_score(v){
		this.Scoreform_section1_vocab22_score=v;
	}
	this.setScoreform_section1_vocab22_score=setScoreform_section1_vocab22_score;

	this.Scoreform_section1_vocab23_response=null;


	function getScoreform_section1_vocab23_response() {
		return this.Scoreform_section1_vocab23_response;
	}
	this.getScoreform_section1_vocab23_response=getScoreform_section1_vocab23_response;


	function setScoreform_section1_vocab23_response(v){
		this.Scoreform_section1_vocab23_response=v;
	}
	this.setScoreform_section1_vocab23_response=setScoreform_section1_vocab23_response;

	this.Scoreform_section1_vocab23_score=null;


	function getScoreform_section1_vocab23_score() {
		return this.Scoreform_section1_vocab23_score;
	}
	this.getScoreform_section1_vocab23_score=getScoreform_section1_vocab23_score;


	function setScoreform_section1_vocab23_score(v){
		this.Scoreform_section1_vocab23_score=v;
	}
	this.setScoreform_section1_vocab23_score=setScoreform_section1_vocab23_score;

	this.Scoreform_section1_vocab24_response=null;


	function getScoreform_section1_vocab24_response() {
		return this.Scoreform_section1_vocab24_response;
	}
	this.getScoreform_section1_vocab24_response=getScoreform_section1_vocab24_response;


	function setScoreform_section1_vocab24_response(v){
		this.Scoreform_section1_vocab24_response=v;
	}
	this.setScoreform_section1_vocab24_response=setScoreform_section1_vocab24_response;

	this.Scoreform_section1_vocab24_score=null;


	function getScoreform_section1_vocab24_score() {
		return this.Scoreform_section1_vocab24_score;
	}
	this.getScoreform_section1_vocab24_score=getScoreform_section1_vocab24_score;


	function setScoreform_section1_vocab24_score(v){
		this.Scoreform_section1_vocab24_score=v;
	}
	this.setScoreform_section1_vocab24_score=setScoreform_section1_vocab24_score;

	this.Scoreform_section1_vocab25_response=null;


	function getScoreform_section1_vocab25_response() {
		return this.Scoreform_section1_vocab25_response;
	}
	this.getScoreform_section1_vocab25_response=getScoreform_section1_vocab25_response;


	function setScoreform_section1_vocab25_response(v){
		this.Scoreform_section1_vocab25_response=v;
	}
	this.setScoreform_section1_vocab25_response=setScoreform_section1_vocab25_response;

	this.Scoreform_section1_vocab25_score=null;


	function getScoreform_section1_vocab25_score() {
		return this.Scoreform_section1_vocab25_score;
	}
	this.getScoreform_section1_vocab25_score=getScoreform_section1_vocab25_score;


	function setScoreform_section1_vocab25_score(v){
		this.Scoreform_section1_vocab25_score=v;
	}
	this.setScoreform_section1_vocab25_score=setScoreform_section1_vocab25_score;

	this.Scoreform_section1_vocab26_response=null;


	function getScoreform_section1_vocab26_response() {
		return this.Scoreform_section1_vocab26_response;
	}
	this.getScoreform_section1_vocab26_response=getScoreform_section1_vocab26_response;


	function setScoreform_section1_vocab26_response(v){
		this.Scoreform_section1_vocab26_response=v;
	}
	this.setScoreform_section1_vocab26_response=setScoreform_section1_vocab26_response;

	this.Scoreform_section1_vocab26_score=null;


	function getScoreform_section1_vocab26_score() {
		return this.Scoreform_section1_vocab26_score;
	}
	this.getScoreform_section1_vocab26_score=getScoreform_section1_vocab26_score;


	function setScoreform_section1_vocab26_score(v){
		this.Scoreform_section1_vocab26_score=v;
	}
	this.setScoreform_section1_vocab26_score=setScoreform_section1_vocab26_score;

	this.Scoreform_section1_vocab27_response=null;


	function getScoreform_section1_vocab27_response() {
		return this.Scoreform_section1_vocab27_response;
	}
	this.getScoreform_section1_vocab27_response=getScoreform_section1_vocab27_response;


	function setScoreform_section1_vocab27_response(v){
		this.Scoreform_section1_vocab27_response=v;
	}
	this.setScoreform_section1_vocab27_response=setScoreform_section1_vocab27_response;

	this.Scoreform_section1_vocab27_score=null;


	function getScoreform_section1_vocab27_score() {
		return this.Scoreform_section1_vocab27_score;
	}
	this.getScoreform_section1_vocab27_score=getScoreform_section1_vocab27_score;


	function setScoreform_section1_vocab27_score(v){
		this.Scoreform_section1_vocab27_score=v;
	}
	this.setScoreform_section1_vocab27_score=setScoreform_section1_vocab27_score;

	this.Scoreform_section1_vocab28_response=null;


	function getScoreform_section1_vocab28_response() {
		return this.Scoreform_section1_vocab28_response;
	}
	this.getScoreform_section1_vocab28_response=getScoreform_section1_vocab28_response;


	function setScoreform_section1_vocab28_response(v){
		this.Scoreform_section1_vocab28_response=v;
	}
	this.setScoreform_section1_vocab28_response=setScoreform_section1_vocab28_response;

	this.Scoreform_section1_vocab28_score=null;


	function getScoreform_section1_vocab28_score() {
		return this.Scoreform_section1_vocab28_score;
	}
	this.getScoreform_section1_vocab28_score=getScoreform_section1_vocab28_score;


	function setScoreform_section1_vocab28_score(v){
		this.Scoreform_section1_vocab28_score=v;
	}
	this.setScoreform_section1_vocab28_score=setScoreform_section1_vocab28_score;

	this.Scoreform_section1_vocab29_response=null;


	function getScoreform_section1_vocab29_response() {
		return this.Scoreform_section1_vocab29_response;
	}
	this.getScoreform_section1_vocab29_response=getScoreform_section1_vocab29_response;


	function setScoreform_section1_vocab29_response(v){
		this.Scoreform_section1_vocab29_response=v;
	}
	this.setScoreform_section1_vocab29_response=setScoreform_section1_vocab29_response;

	this.Scoreform_section1_vocab29_score=null;


	function getScoreform_section1_vocab29_score() {
		return this.Scoreform_section1_vocab29_score;
	}
	this.getScoreform_section1_vocab29_score=getScoreform_section1_vocab29_score;


	function setScoreform_section1_vocab29_score(v){
		this.Scoreform_section1_vocab29_score=v;
	}
	this.setScoreform_section1_vocab29_score=setScoreform_section1_vocab29_score;

	this.Scoreform_section1_vocab30_response=null;


	function getScoreform_section1_vocab30_response() {
		return this.Scoreform_section1_vocab30_response;
	}
	this.getScoreform_section1_vocab30_response=getScoreform_section1_vocab30_response;


	function setScoreform_section1_vocab30_response(v){
		this.Scoreform_section1_vocab30_response=v;
	}
	this.setScoreform_section1_vocab30_response=setScoreform_section1_vocab30_response;

	this.Scoreform_section1_vocab30_score=null;


	function getScoreform_section1_vocab30_score() {
		return this.Scoreform_section1_vocab30_score;
	}
	this.getScoreform_section1_vocab30_score=getScoreform_section1_vocab30_score;


	function setScoreform_section1_vocab30_score(v){
		this.Scoreform_section1_vocab30_score=v;
	}
	this.setScoreform_section1_vocab30_score=setScoreform_section1_vocab30_score;

	this.Scoreform_section1_vocab31_response=null;


	function getScoreform_section1_vocab31_response() {
		return this.Scoreform_section1_vocab31_response;
	}
	this.getScoreform_section1_vocab31_response=getScoreform_section1_vocab31_response;


	function setScoreform_section1_vocab31_response(v){
		this.Scoreform_section1_vocab31_response=v;
	}
	this.setScoreform_section1_vocab31_response=setScoreform_section1_vocab31_response;

	this.Scoreform_section1_vocab31_score=null;


	function getScoreform_section1_vocab31_score() {
		return this.Scoreform_section1_vocab31_score;
	}
	this.getScoreform_section1_vocab31_score=getScoreform_section1_vocab31_score;


	function setScoreform_section1_vocab31_score(v){
		this.Scoreform_section1_vocab31_score=v;
	}
	this.setScoreform_section1_vocab31_score=setScoreform_section1_vocab31_score;

	this.Scoreform_section1_vocab32_response=null;


	function getScoreform_section1_vocab32_response() {
		return this.Scoreform_section1_vocab32_response;
	}
	this.getScoreform_section1_vocab32_response=getScoreform_section1_vocab32_response;


	function setScoreform_section1_vocab32_response(v){
		this.Scoreform_section1_vocab32_response=v;
	}
	this.setScoreform_section1_vocab32_response=setScoreform_section1_vocab32_response;

	this.Scoreform_section1_vocab32_score=null;


	function getScoreform_section1_vocab32_score() {
		return this.Scoreform_section1_vocab32_score;
	}
	this.getScoreform_section1_vocab32_score=getScoreform_section1_vocab32_score;


	function setScoreform_section1_vocab32_score(v){
		this.Scoreform_section1_vocab32_score=v;
	}
	this.setScoreform_section1_vocab32_score=setScoreform_section1_vocab32_score;

	this.Scoreform_section1_vocab33_response=null;


	function getScoreform_section1_vocab33_response() {
		return this.Scoreform_section1_vocab33_response;
	}
	this.getScoreform_section1_vocab33_response=getScoreform_section1_vocab33_response;


	function setScoreform_section1_vocab33_response(v){
		this.Scoreform_section1_vocab33_response=v;
	}
	this.setScoreform_section1_vocab33_response=setScoreform_section1_vocab33_response;

	this.Scoreform_section1_vocab33_score=null;


	function getScoreform_section1_vocab33_score() {
		return this.Scoreform_section1_vocab33_score;
	}
	this.getScoreform_section1_vocab33_score=getScoreform_section1_vocab33_score;


	function setScoreform_section1_vocab33_score(v){
		this.Scoreform_section1_vocab33_score=v;
	}
	this.setScoreform_section1_vocab33_score=setScoreform_section1_vocab33_score;

	this.Scoreform_section1_vocab34_response=null;


	function getScoreform_section1_vocab34_response() {
		return this.Scoreform_section1_vocab34_response;
	}
	this.getScoreform_section1_vocab34_response=getScoreform_section1_vocab34_response;


	function setScoreform_section1_vocab34_response(v){
		this.Scoreform_section1_vocab34_response=v;
	}
	this.setScoreform_section1_vocab34_response=setScoreform_section1_vocab34_response;

	this.Scoreform_section1_vocab34_score=null;


	function getScoreform_section1_vocab34_score() {
		return this.Scoreform_section1_vocab34_score;
	}
	this.getScoreform_section1_vocab34_score=getScoreform_section1_vocab34_score;


	function setScoreform_section1_vocab34_score(v){
		this.Scoreform_section1_vocab34_score=v;
	}
	this.setScoreform_section1_vocab34_score=setScoreform_section1_vocab34_score;

	this.Scoreform_section1_vocab35_response=null;


	function getScoreform_section1_vocab35_response() {
		return this.Scoreform_section1_vocab35_response;
	}
	this.getScoreform_section1_vocab35_response=getScoreform_section1_vocab35_response;


	function setScoreform_section1_vocab35_response(v){
		this.Scoreform_section1_vocab35_response=v;
	}
	this.setScoreform_section1_vocab35_response=setScoreform_section1_vocab35_response;

	this.Scoreform_section1_vocab35_score=null;


	function getScoreform_section1_vocab35_score() {
		return this.Scoreform_section1_vocab35_score;
	}
	this.getScoreform_section1_vocab35_score=getScoreform_section1_vocab35_score;


	function setScoreform_section1_vocab35_score(v){
		this.Scoreform_section1_vocab35_score=v;
	}
	this.setScoreform_section1_vocab35_score=setScoreform_section1_vocab35_score;

	this.Scoreform_section1_vocab36_response=null;


	function getScoreform_section1_vocab36_response() {
		return this.Scoreform_section1_vocab36_response;
	}
	this.getScoreform_section1_vocab36_response=getScoreform_section1_vocab36_response;


	function setScoreform_section1_vocab36_response(v){
		this.Scoreform_section1_vocab36_response=v;
	}
	this.setScoreform_section1_vocab36_response=setScoreform_section1_vocab36_response;

	this.Scoreform_section1_vocab36_score=null;


	function getScoreform_section1_vocab36_score() {
		return this.Scoreform_section1_vocab36_score;
	}
	this.getScoreform_section1_vocab36_score=getScoreform_section1_vocab36_score;


	function setScoreform_section1_vocab36_score(v){
		this.Scoreform_section1_vocab36_score=v;
	}
	this.setScoreform_section1_vocab36_score=setScoreform_section1_vocab36_score;

	this.Scoreform_section1_vocab37_response=null;


	function getScoreform_section1_vocab37_response() {
		return this.Scoreform_section1_vocab37_response;
	}
	this.getScoreform_section1_vocab37_response=getScoreform_section1_vocab37_response;


	function setScoreform_section1_vocab37_response(v){
		this.Scoreform_section1_vocab37_response=v;
	}
	this.setScoreform_section1_vocab37_response=setScoreform_section1_vocab37_response;

	this.Scoreform_section1_vocab37_score=null;


	function getScoreform_section1_vocab37_score() {
		return this.Scoreform_section1_vocab37_score;
	}
	this.getScoreform_section1_vocab37_score=getScoreform_section1_vocab37_score;


	function setScoreform_section1_vocab37_score(v){
		this.Scoreform_section1_vocab37_score=v;
	}
	this.setScoreform_section1_vocab37_score=setScoreform_section1_vocab37_score;

	this.Scoreform_section1_vocab38_response=null;


	function getScoreform_section1_vocab38_response() {
		return this.Scoreform_section1_vocab38_response;
	}
	this.getScoreform_section1_vocab38_response=getScoreform_section1_vocab38_response;


	function setScoreform_section1_vocab38_response(v){
		this.Scoreform_section1_vocab38_response=v;
	}
	this.setScoreform_section1_vocab38_response=setScoreform_section1_vocab38_response;

	this.Scoreform_section1_vocab38_score=null;


	function getScoreform_section1_vocab38_score() {
		return this.Scoreform_section1_vocab38_score;
	}
	this.getScoreform_section1_vocab38_score=getScoreform_section1_vocab38_score;


	function setScoreform_section1_vocab38_score(v){
		this.Scoreform_section1_vocab38_score=v;
	}
	this.setScoreform_section1_vocab38_score=setScoreform_section1_vocab38_score;

	this.Scoreform_section1_vocab39_response=null;


	function getScoreform_section1_vocab39_response() {
		return this.Scoreform_section1_vocab39_response;
	}
	this.getScoreform_section1_vocab39_response=getScoreform_section1_vocab39_response;


	function setScoreform_section1_vocab39_response(v){
		this.Scoreform_section1_vocab39_response=v;
	}
	this.setScoreform_section1_vocab39_response=setScoreform_section1_vocab39_response;

	this.Scoreform_section1_vocab39_score=null;


	function getScoreform_section1_vocab39_score() {
		return this.Scoreform_section1_vocab39_score;
	}
	this.getScoreform_section1_vocab39_score=getScoreform_section1_vocab39_score;


	function setScoreform_section1_vocab39_score(v){
		this.Scoreform_section1_vocab39_score=v;
	}
	this.setScoreform_section1_vocab39_score=setScoreform_section1_vocab39_score;

	this.Scoreform_section1_vocab40_response=null;


	function getScoreform_section1_vocab40_response() {
		return this.Scoreform_section1_vocab40_response;
	}
	this.getScoreform_section1_vocab40_response=getScoreform_section1_vocab40_response;


	function setScoreform_section1_vocab40_response(v){
		this.Scoreform_section1_vocab40_response=v;
	}
	this.setScoreform_section1_vocab40_response=setScoreform_section1_vocab40_response;

	this.Scoreform_section1_vocab40_score=null;


	function getScoreform_section1_vocab40_score() {
		return this.Scoreform_section1_vocab40_score;
	}
	this.getScoreform_section1_vocab40_score=getScoreform_section1_vocab40_score;


	function setScoreform_section1_vocab40_score(v){
		this.Scoreform_section1_vocab40_score=v;
	}
	this.setScoreform_section1_vocab40_score=setScoreform_section1_vocab40_score;

	this.Scoreform_section1_vocab41_response=null;


	function getScoreform_section1_vocab41_response() {
		return this.Scoreform_section1_vocab41_response;
	}
	this.getScoreform_section1_vocab41_response=getScoreform_section1_vocab41_response;


	function setScoreform_section1_vocab41_response(v){
		this.Scoreform_section1_vocab41_response=v;
	}
	this.setScoreform_section1_vocab41_response=setScoreform_section1_vocab41_response;

	this.Scoreform_section1_vocab41_score=null;


	function getScoreform_section1_vocab41_score() {
		return this.Scoreform_section1_vocab41_score;
	}
	this.getScoreform_section1_vocab41_score=getScoreform_section1_vocab41_score;


	function setScoreform_section1_vocab41_score(v){
		this.Scoreform_section1_vocab41_score=v;
	}
	this.setScoreform_section1_vocab41_score=setScoreform_section1_vocab41_score;

	this.Scoreform_section1_vocab42_response=null;


	function getScoreform_section1_vocab42_response() {
		return this.Scoreform_section1_vocab42_response;
	}
	this.getScoreform_section1_vocab42_response=getScoreform_section1_vocab42_response;


	function setScoreform_section1_vocab42_response(v){
		this.Scoreform_section1_vocab42_response=v;
	}
	this.setScoreform_section1_vocab42_response=setScoreform_section1_vocab42_response;

	this.Scoreform_section1_vocab42_score=null;


	function getScoreform_section1_vocab42_score() {
		return this.Scoreform_section1_vocab42_score;
	}
	this.getScoreform_section1_vocab42_score=getScoreform_section1_vocab42_score;


	function setScoreform_section1_vocab42_score(v){
		this.Scoreform_section1_vocab42_score=v;
	}
	this.setScoreform_section1_vocab42_score=setScoreform_section1_vocab42_score;

	this.Scoreform_section2_block1_time=null;


	function getScoreform_section2_block1_time() {
		return this.Scoreform_section2_block1_time;
	}
	this.getScoreform_section2_block1_time=getScoreform_section2_block1_time;


	function setScoreform_section2_block1_time(v){
		this.Scoreform_section2_block1_time=v;
	}
	this.setScoreform_section2_block1_time=setScoreform_section2_block1_time;

	this.Scoreform_section2_block1_correct=null;


	function getScoreform_section2_block1_correct() {
		return this.Scoreform_section2_block1_correct;
	}
	this.getScoreform_section2_block1_correct=getScoreform_section2_block1_correct;


	function setScoreform_section2_block1_correct(v){
		this.Scoreform_section2_block1_correct=v;
	}
	this.setScoreform_section2_block1_correct=setScoreform_section2_block1_correct;

	this.Scoreform_section2_block1_score=null;


	function getScoreform_section2_block1_score() {
		return this.Scoreform_section2_block1_score;
	}
	this.getScoreform_section2_block1_score=getScoreform_section2_block1_score;


	function setScoreform_section2_block1_score(v){
		this.Scoreform_section2_block1_score=v;
	}
	this.setScoreform_section2_block1_score=setScoreform_section2_block1_score;

	this.Scoreform_section2_block2_time=null;


	function getScoreform_section2_block2_time() {
		return this.Scoreform_section2_block2_time;
	}
	this.getScoreform_section2_block2_time=getScoreform_section2_block2_time;


	function setScoreform_section2_block2_time(v){
		this.Scoreform_section2_block2_time=v;
	}
	this.setScoreform_section2_block2_time=setScoreform_section2_block2_time;

	this.Scoreform_section2_block2_correct=null;


	function getScoreform_section2_block2_correct() {
		return this.Scoreform_section2_block2_correct;
	}
	this.getScoreform_section2_block2_correct=getScoreform_section2_block2_correct;


	function setScoreform_section2_block2_correct(v){
		this.Scoreform_section2_block2_correct=v;
	}
	this.setScoreform_section2_block2_correct=setScoreform_section2_block2_correct;

	this.Scoreform_section2_block2_score=null;


	function getScoreform_section2_block2_score() {
		return this.Scoreform_section2_block2_score;
	}
	this.getScoreform_section2_block2_score=getScoreform_section2_block2_score;


	function setScoreform_section2_block2_score(v){
		this.Scoreform_section2_block2_score=v;
	}
	this.setScoreform_section2_block2_score=setScoreform_section2_block2_score;

	this.Scoreform_section2_block3_time=null;


	function getScoreform_section2_block3_time() {
		return this.Scoreform_section2_block3_time;
	}
	this.getScoreform_section2_block3_time=getScoreform_section2_block3_time;


	function setScoreform_section2_block3_time(v){
		this.Scoreform_section2_block3_time=v;
	}
	this.setScoreform_section2_block3_time=setScoreform_section2_block3_time;

	this.Scoreform_section2_block3_correct=null;


	function getScoreform_section2_block3_correct() {
		return this.Scoreform_section2_block3_correct;
	}
	this.getScoreform_section2_block3_correct=getScoreform_section2_block3_correct;


	function setScoreform_section2_block3_correct(v){
		this.Scoreform_section2_block3_correct=v;
	}
	this.setScoreform_section2_block3_correct=setScoreform_section2_block3_correct;

	this.Scoreform_section2_block3_score=null;


	function getScoreform_section2_block3_score() {
		return this.Scoreform_section2_block3_score;
	}
	this.getScoreform_section2_block3_score=getScoreform_section2_block3_score;


	function setScoreform_section2_block3_score(v){
		this.Scoreform_section2_block3_score=v;
	}
	this.setScoreform_section2_block3_score=setScoreform_section2_block3_score;

	this.Scoreform_section2_block4_time=null;


	function getScoreform_section2_block4_time() {
		return this.Scoreform_section2_block4_time;
	}
	this.getScoreform_section2_block4_time=getScoreform_section2_block4_time;


	function setScoreform_section2_block4_time(v){
		this.Scoreform_section2_block4_time=v;
	}
	this.setScoreform_section2_block4_time=setScoreform_section2_block4_time;

	this.Scoreform_section2_block4_correct=null;


	function getScoreform_section2_block4_correct() {
		return this.Scoreform_section2_block4_correct;
	}
	this.getScoreform_section2_block4_correct=getScoreform_section2_block4_correct;


	function setScoreform_section2_block4_correct(v){
		this.Scoreform_section2_block4_correct=v;
	}
	this.setScoreform_section2_block4_correct=setScoreform_section2_block4_correct;

	this.Scoreform_section2_block4_score=null;


	function getScoreform_section2_block4_score() {
		return this.Scoreform_section2_block4_score;
	}
	this.getScoreform_section2_block4_score=getScoreform_section2_block4_score;


	function setScoreform_section2_block4_score(v){
		this.Scoreform_section2_block4_score=v;
	}
	this.setScoreform_section2_block4_score=setScoreform_section2_block4_score;

	this.Scoreform_section2_block5_time=null;


	function getScoreform_section2_block5_time() {
		return this.Scoreform_section2_block5_time;
	}
	this.getScoreform_section2_block5_time=getScoreform_section2_block5_time;


	function setScoreform_section2_block5_time(v){
		this.Scoreform_section2_block5_time=v;
	}
	this.setScoreform_section2_block5_time=setScoreform_section2_block5_time;

	this.Scoreform_section2_block5_correct=null;


	function getScoreform_section2_block5_correct() {
		return this.Scoreform_section2_block5_correct;
	}
	this.getScoreform_section2_block5_correct=getScoreform_section2_block5_correct;


	function setScoreform_section2_block5_correct(v){
		this.Scoreform_section2_block5_correct=v;
	}
	this.setScoreform_section2_block5_correct=setScoreform_section2_block5_correct;

	this.Scoreform_section2_block5_score=null;


	function getScoreform_section2_block5_score() {
		return this.Scoreform_section2_block5_score;
	}
	this.getScoreform_section2_block5_score=getScoreform_section2_block5_score;


	function setScoreform_section2_block5_score(v){
		this.Scoreform_section2_block5_score=v;
	}
	this.setScoreform_section2_block5_score=setScoreform_section2_block5_score;

	this.Scoreform_section2_block6_time=null;


	function getScoreform_section2_block6_time() {
		return this.Scoreform_section2_block6_time;
	}
	this.getScoreform_section2_block6_time=getScoreform_section2_block6_time;


	function setScoreform_section2_block6_time(v){
		this.Scoreform_section2_block6_time=v;
	}
	this.setScoreform_section2_block6_time=setScoreform_section2_block6_time;

	this.Scoreform_section2_block6_correct=null;


	function getScoreform_section2_block6_correct() {
		return this.Scoreform_section2_block6_correct;
	}
	this.getScoreform_section2_block6_correct=getScoreform_section2_block6_correct;


	function setScoreform_section2_block6_correct(v){
		this.Scoreform_section2_block6_correct=v;
	}
	this.setScoreform_section2_block6_correct=setScoreform_section2_block6_correct;

	this.Scoreform_section2_block6_score=null;


	function getScoreform_section2_block6_score() {
		return this.Scoreform_section2_block6_score;
	}
	this.getScoreform_section2_block6_score=getScoreform_section2_block6_score;


	function setScoreform_section2_block6_score(v){
		this.Scoreform_section2_block6_score=v;
	}
	this.setScoreform_section2_block6_score=setScoreform_section2_block6_score;

	this.Scoreform_section2_block7_time=null;


	function getScoreform_section2_block7_time() {
		return this.Scoreform_section2_block7_time;
	}
	this.getScoreform_section2_block7_time=getScoreform_section2_block7_time;


	function setScoreform_section2_block7_time(v){
		this.Scoreform_section2_block7_time=v;
	}
	this.setScoreform_section2_block7_time=setScoreform_section2_block7_time;

	this.Scoreform_section2_block7_correct=null;


	function getScoreform_section2_block7_correct() {
		return this.Scoreform_section2_block7_correct;
	}
	this.getScoreform_section2_block7_correct=getScoreform_section2_block7_correct;


	function setScoreform_section2_block7_correct(v){
		this.Scoreform_section2_block7_correct=v;
	}
	this.setScoreform_section2_block7_correct=setScoreform_section2_block7_correct;

	this.Scoreform_section2_block7_score=null;


	function getScoreform_section2_block7_score() {
		return this.Scoreform_section2_block7_score;
	}
	this.getScoreform_section2_block7_score=getScoreform_section2_block7_score;


	function setScoreform_section2_block7_score(v){
		this.Scoreform_section2_block7_score=v;
	}
	this.setScoreform_section2_block7_score=setScoreform_section2_block7_score;

	this.Scoreform_section2_block8_time=null;


	function getScoreform_section2_block8_time() {
		return this.Scoreform_section2_block8_time;
	}
	this.getScoreform_section2_block8_time=getScoreform_section2_block8_time;


	function setScoreform_section2_block8_time(v){
		this.Scoreform_section2_block8_time=v;
	}
	this.setScoreform_section2_block8_time=setScoreform_section2_block8_time;

	this.Scoreform_section2_block8_correct=null;


	function getScoreform_section2_block8_correct() {
		return this.Scoreform_section2_block8_correct;
	}
	this.getScoreform_section2_block8_correct=getScoreform_section2_block8_correct;


	function setScoreform_section2_block8_correct(v){
		this.Scoreform_section2_block8_correct=v;
	}
	this.setScoreform_section2_block8_correct=setScoreform_section2_block8_correct;

	this.Scoreform_section2_block8_score=null;


	function getScoreform_section2_block8_score() {
		return this.Scoreform_section2_block8_score;
	}
	this.getScoreform_section2_block8_score=getScoreform_section2_block8_score;


	function setScoreform_section2_block8_score(v){
		this.Scoreform_section2_block8_score=v;
	}
	this.setScoreform_section2_block8_score=setScoreform_section2_block8_score;

	this.Scoreform_section2_block9_time=null;


	function getScoreform_section2_block9_time() {
		return this.Scoreform_section2_block9_time;
	}
	this.getScoreform_section2_block9_time=getScoreform_section2_block9_time;


	function setScoreform_section2_block9_time(v){
		this.Scoreform_section2_block9_time=v;
	}
	this.setScoreform_section2_block9_time=setScoreform_section2_block9_time;

	this.Scoreform_section2_block9_correct=null;


	function getScoreform_section2_block9_correct() {
		return this.Scoreform_section2_block9_correct;
	}
	this.getScoreform_section2_block9_correct=getScoreform_section2_block9_correct;


	function setScoreform_section2_block9_correct(v){
		this.Scoreform_section2_block9_correct=v;
	}
	this.setScoreform_section2_block9_correct=setScoreform_section2_block9_correct;

	this.Scoreform_section2_block9_score=null;


	function getScoreform_section2_block9_score() {
		return this.Scoreform_section2_block9_score;
	}
	this.getScoreform_section2_block9_score=getScoreform_section2_block9_score;


	function setScoreform_section2_block9_score(v){
		this.Scoreform_section2_block9_score=v;
	}
	this.setScoreform_section2_block9_score=setScoreform_section2_block9_score;

	this.Scoreform_section2_block10_time=null;


	function getScoreform_section2_block10_time() {
		return this.Scoreform_section2_block10_time;
	}
	this.getScoreform_section2_block10_time=getScoreform_section2_block10_time;


	function setScoreform_section2_block10_time(v){
		this.Scoreform_section2_block10_time=v;
	}
	this.setScoreform_section2_block10_time=setScoreform_section2_block10_time;

	this.Scoreform_section2_block10_correct=null;


	function getScoreform_section2_block10_correct() {
		return this.Scoreform_section2_block10_correct;
	}
	this.getScoreform_section2_block10_correct=getScoreform_section2_block10_correct;


	function setScoreform_section2_block10_correct(v){
		this.Scoreform_section2_block10_correct=v;
	}
	this.setScoreform_section2_block10_correct=setScoreform_section2_block10_correct;

	this.Scoreform_section2_block10_score=null;


	function getScoreform_section2_block10_score() {
		return this.Scoreform_section2_block10_score;
	}
	this.getScoreform_section2_block10_score=getScoreform_section2_block10_score;


	function setScoreform_section2_block10_score(v){
		this.Scoreform_section2_block10_score=v;
	}
	this.setScoreform_section2_block10_score=setScoreform_section2_block10_score;

	this.Scoreform_section2_block11_time=null;


	function getScoreform_section2_block11_time() {
		return this.Scoreform_section2_block11_time;
	}
	this.getScoreform_section2_block11_time=getScoreform_section2_block11_time;


	function setScoreform_section2_block11_time(v){
		this.Scoreform_section2_block11_time=v;
	}
	this.setScoreform_section2_block11_time=setScoreform_section2_block11_time;

	this.Scoreform_section2_block11_correct=null;


	function getScoreform_section2_block11_correct() {
		return this.Scoreform_section2_block11_correct;
	}
	this.getScoreform_section2_block11_correct=getScoreform_section2_block11_correct;


	function setScoreform_section2_block11_correct(v){
		this.Scoreform_section2_block11_correct=v;
	}
	this.setScoreform_section2_block11_correct=setScoreform_section2_block11_correct;

	this.Scoreform_section2_block11_score=null;


	function getScoreform_section2_block11_score() {
		return this.Scoreform_section2_block11_score;
	}
	this.getScoreform_section2_block11_score=getScoreform_section2_block11_score;


	function setScoreform_section2_block11_score(v){
		this.Scoreform_section2_block11_score=v;
	}
	this.setScoreform_section2_block11_score=setScoreform_section2_block11_score;

	this.Scoreform_section2_block12_time=null;


	function getScoreform_section2_block12_time() {
		return this.Scoreform_section2_block12_time;
	}
	this.getScoreform_section2_block12_time=getScoreform_section2_block12_time;


	function setScoreform_section2_block12_time(v){
		this.Scoreform_section2_block12_time=v;
	}
	this.setScoreform_section2_block12_time=setScoreform_section2_block12_time;

	this.Scoreform_section2_block12_correct=null;


	function getScoreform_section2_block12_correct() {
		return this.Scoreform_section2_block12_correct;
	}
	this.getScoreform_section2_block12_correct=getScoreform_section2_block12_correct;


	function setScoreform_section2_block12_correct(v){
		this.Scoreform_section2_block12_correct=v;
	}
	this.setScoreform_section2_block12_correct=setScoreform_section2_block12_correct;

	this.Scoreform_section2_block12_score=null;


	function getScoreform_section2_block12_score() {
		return this.Scoreform_section2_block12_score;
	}
	this.getScoreform_section2_block12_score=getScoreform_section2_block12_score;


	function setScoreform_section2_block12_score(v){
		this.Scoreform_section2_block12_score=v;
	}
	this.setScoreform_section2_block12_score=setScoreform_section2_block12_score;

	this.Scoreform_section2_block13_time=null;


	function getScoreform_section2_block13_time() {
		return this.Scoreform_section2_block13_time;
	}
	this.getScoreform_section2_block13_time=getScoreform_section2_block13_time;


	function setScoreform_section2_block13_time(v){
		this.Scoreform_section2_block13_time=v;
	}
	this.setScoreform_section2_block13_time=setScoreform_section2_block13_time;

	this.Scoreform_section2_block13_correct=null;


	function getScoreform_section2_block13_correct() {
		return this.Scoreform_section2_block13_correct;
	}
	this.getScoreform_section2_block13_correct=getScoreform_section2_block13_correct;


	function setScoreform_section2_block13_correct(v){
		this.Scoreform_section2_block13_correct=v;
	}
	this.setScoreform_section2_block13_correct=setScoreform_section2_block13_correct;

	this.Scoreform_section2_block13_score=null;


	function getScoreform_section2_block13_score() {
		return this.Scoreform_section2_block13_score;
	}
	this.getScoreform_section2_block13_score=getScoreform_section2_block13_score;


	function setScoreform_section2_block13_score(v){
		this.Scoreform_section2_block13_score=v;
	}
	this.setScoreform_section2_block13_score=setScoreform_section2_block13_score;

	this.Scoreform_section3_similar1_response=null;


	function getScoreform_section3_similar1_response() {
		return this.Scoreform_section3_similar1_response;
	}
	this.getScoreform_section3_similar1_response=getScoreform_section3_similar1_response;


	function setScoreform_section3_similar1_response(v){
		this.Scoreform_section3_similar1_response=v;
	}
	this.setScoreform_section3_similar1_response=setScoreform_section3_similar1_response;

	this.Scoreform_section3_similar1_score=null;


	function getScoreform_section3_similar1_score() {
		return this.Scoreform_section3_similar1_score;
	}
	this.getScoreform_section3_similar1_score=getScoreform_section3_similar1_score;


	function setScoreform_section3_similar1_score(v){
		this.Scoreform_section3_similar1_score=v;
	}
	this.setScoreform_section3_similar1_score=setScoreform_section3_similar1_score;

	this.Scoreform_section3_similar2_response=null;


	function getScoreform_section3_similar2_response() {
		return this.Scoreform_section3_similar2_response;
	}
	this.getScoreform_section3_similar2_response=getScoreform_section3_similar2_response;


	function setScoreform_section3_similar2_response(v){
		this.Scoreform_section3_similar2_response=v;
	}
	this.setScoreform_section3_similar2_response=setScoreform_section3_similar2_response;

	this.Scoreform_section3_similar2_score=null;


	function getScoreform_section3_similar2_score() {
		return this.Scoreform_section3_similar2_score;
	}
	this.getScoreform_section3_similar2_score=getScoreform_section3_similar2_score;


	function setScoreform_section3_similar2_score(v){
		this.Scoreform_section3_similar2_score=v;
	}
	this.setScoreform_section3_similar2_score=setScoreform_section3_similar2_score;

	this.Scoreform_section3_similar3_response=null;


	function getScoreform_section3_similar3_response() {
		return this.Scoreform_section3_similar3_response;
	}
	this.getScoreform_section3_similar3_response=getScoreform_section3_similar3_response;


	function setScoreform_section3_similar3_response(v){
		this.Scoreform_section3_similar3_response=v;
	}
	this.setScoreform_section3_similar3_response=setScoreform_section3_similar3_response;

	this.Scoreform_section3_similar3_score=null;


	function getScoreform_section3_similar3_score() {
		return this.Scoreform_section3_similar3_score;
	}
	this.getScoreform_section3_similar3_score=getScoreform_section3_similar3_score;


	function setScoreform_section3_similar3_score(v){
		this.Scoreform_section3_similar3_score=v;
	}
	this.setScoreform_section3_similar3_score=setScoreform_section3_similar3_score;

	this.Scoreform_section3_similar4_response=null;


	function getScoreform_section3_similar4_response() {
		return this.Scoreform_section3_similar4_response;
	}
	this.getScoreform_section3_similar4_response=getScoreform_section3_similar4_response;


	function setScoreform_section3_similar4_response(v){
		this.Scoreform_section3_similar4_response=v;
	}
	this.setScoreform_section3_similar4_response=setScoreform_section3_similar4_response;

	this.Scoreform_section3_similar4_score=null;


	function getScoreform_section3_similar4_score() {
		return this.Scoreform_section3_similar4_score;
	}
	this.getScoreform_section3_similar4_score=getScoreform_section3_similar4_score;


	function setScoreform_section3_similar4_score(v){
		this.Scoreform_section3_similar4_score=v;
	}
	this.setScoreform_section3_similar4_score=setScoreform_section3_similar4_score;

	this.Scoreform_section3_similar5_response=null;


	function getScoreform_section3_similar5_response() {
		return this.Scoreform_section3_similar5_response;
	}
	this.getScoreform_section3_similar5_response=getScoreform_section3_similar5_response;


	function setScoreform_section3_similar5_response(v){
		this.Scoreform_section3_similar5_response=v;
	}
	this.setScoreform_section3_similar5_response=setScoreform_section3_similar5_response;

	this.Scoreform_section3_similar5_score=null;


	function getScoreform_section3_similar5_score() {
		return this.Scoreform_section3_similar5_score;
	}
	this.getScoreform_section3_similar5_score=getScoreform_section3_similar5_score;


	function setScoreform_section3_similar5_score(v){
		this.Scoreform_section3_similar5_score=v;
	}
	this.setScoreform_section3_similar5_score=setScoreform_section3_similar5_score;

	this.Scoreform_section3_similar6_response=null;


	function getScoreform_section3_similar6_response() {
		return this.Scoreform_section3_similar6_response;
	}
	this.getScoreform_section3_similar6_response=getScoreform_section3_similar6_response;


	function setScoreform_section3_similar6_response(v){
		this.Scoreform_section3_similar6_response=v;
	}
	this.setScoreform_section3_similar6_response=setScoreform_section3_similar6_response;

	this.Scoreform_section3_similar6_score=null;


	function getScoreform_section3_similar6_score() {
		return this.Scoreform_section3_similar6_score;
	}
	this.getScoreform_section3_similar6_score=getScoreform_section3_similar6_score;


	function setScoreform_section3_similar6_score(v){
		this.Scoreform_section3_similar6_score=v;
	}
	this.setScoreform_section3_similar6_score=setScoreform_section3_similar6_score;

	this.Scoreform_section3_similar7_response=null;


	function getScoreform_section3_similar7_response() {
		return this.Scoreform_section3_similar7_response;
	}
	this.getScoreform_section3_similar7_response=getScoreform_section3_similar7_response;


	function setScoreform_section3_similar7_response(v){
		this.Scoreform_section3_similar7_response=v;
	}
	this.setScoreform_section3_similar7_response=setScoreform_section3_similar7_response;

	this.Scoreform_section3_similar7_score=null;


	function getScoreform_section3_similar7_score() {
		return this.Scoreform_section3_similar7_score;
	}
	this.getScoreform_section3_similar7_score=getScoreform_section3_similar7_score;


	function setScoreform_section3_similar7_score(v){
		this.Scoreform_section3_similar7_score=v;
	}
	this.setScoreform_section3_similar7_score=setScoreform_section3_similar7_score;

	this.Scoreform_section3_similar8_response=null;


	function getScoreform_section3_similar8_response() {
		return this.Scoreform_section3_similar8_response;
	}
	this.getScoreform_section3_similar8_response=getScoreform_section3_similar8_response;


	function setScoreform_section3_similar8_response(v){
		this.Scoreform_section3_similar8_response=v;
	}
	this.setScoreform_section3_similar8_response=setScoreform_section3_similar8_response;

	this.Scoreform_section3_similar8_score=null;


	function getScoreform_section3_similar8_score() {
		return this.Scoreform_section3_similar8_score;
	}
	this.getScoreform_section3_similar8_score=getScoreform_section3_similar8_score;


	function setScoreform_section3_similar8_score(v){
		this.Scoreform_section3_similar8_score=v;
	}
	this.setScoreform_section3_similar8_score=setScoreform_section3_similar8_score;

	this.Scoreform_section3_similar9_response=null;


	function getScoreform_section3_similar9_response() {
		return this.Scoreform_section3_similar9_response;
	}
	this.getScoreform_section3_similar9_response=getScoreform_section3_similar9_response;


	function setScoreform_section3_similar9_response(v){
		this.Scoreform_section3_similar9_response=v;
	}
	this.setScoreform_section3_similar9_response=setScoreform_section3_similar9_response;

	this.Scoreform_section3_similar9_score=null;


	function getScoreform_section3_similar9_score() {
		return this.Scoreform_section3_similar9_score;
	}
	this.getScoreform_section3_similar9_score=getScoreform_section3_similar9_score;


	function setScoreform_section3_similar9_score(v){
		this.Scoreform_section3_similar9_score=v;
	}
	this.setScoreform_section3_similar9_score=setScoreform_section3_similar9_score;

	this.Scoreform_section3_similar10_response=null;


	function getScoreform_section3_similar10_response() {
		return this.Scoreform_section3_similar10_response;
	}
	this.getScoreform_section3_similar10_response=getScoreform_section3_similar10_response;


	function setScoreform_section3_similar10_response(v){
		this.Scoreform_section3_similar10_response=v;
	}
	this.setScoreform_section3_similar10_response=setScoreform_section3_similar10_response;

	this.Scoreform_section3_similar10_score=null;


	function getScoreform_section3_similar10_score() {
		return this.Scoreform_section3_similar10_score;
	}
	this.getScoreform_section3_similar10_score=getScoreform_section3_similar10_score;


	function setScoreform_section3_similar10_score(v){
		this.Scoreform_section3_similar10_score=v;
	}
	this.setScoreform_section3_similar10_score=setScoreform_section3_similar10_score;

	this.Scoreform_section3_similar11_response=null;


	function getScoreform_section3_similar11_response() {
		return this.Scoreform_section3_similar11_response;
	}
	this.getScoreform_section3_similar11_response=getScoreform_section3_similar11_response;


	function setScoreform_section3_similar11_response(v){
		this.Scoreform_section3_similar11_response=v;
	}
	this.setScoreform_section3_similar11_response=setScoreform_section3_similar11_response;

	this.Scoreform_section3_similar11_score=null;


	function getScoreform_section3_similar11_score() {
		return this.Scoreform_section3_similar11_score;
	}
	this.getScoreform_section3_similar11_score=getScoreform_section3_similar11_score;


	function setScoreform_section3_similar11_score(v){
		this.Scoreform_section3_similar11_score=v;
	}
	this.setScoreform_section3_similar11_score=setScoreform_section3_similar11_score;

	this.Scoreform_section3_similar12_response=null;


	function getScoreform_section3_similar12_response() {
		return this.Scoreform_section3_similar12_response;
	}
	this.getScoreform_section3_similar12_response=getScoreform_section3_similar12_response;


	function setScoreform_section3_similar12_response(v){
		this.Scoreform_section3_similar12_response=v;
	}
	this.setScoreform_section3_similar12_response=setScoreform_section3_similar12_response;

	this.Scoreform_section3_similar12_score=null;


	function getScoreform_section3_similar12_score() {
		return this.Scoreform_section3_similar12_score;
	}
	this.getScoreform_section3_similar12_score=getScoreform_section3_similar12_score;


	function setScoreform_section3_similar12_score(v){
		this.Scoreform_section3_similar12_score=v;
	}
	this.setScoreform_section3_similar12_score=setScoreform_section3_similar12_score;

	this.Scoreform_section3_similar13_response=null;


	function getScoreform_section3_similar13_response() {
		return this.Scoreform_section3_similar13_response;
	}
	this.getScoreform_section3_similar13_response=getScoreform_section3_similar13_response;


	function setScoreform_section3_similar13_response(v){
		this.Scoreform_section3_similar13_response=v;
	}
	this.setScoreform_section3_similar13_response=setScoreform_section3_similar13_response;

	this.Scoreform_section3_similar13_score=null;


	function getScoreform_section3_similar13_score() {
		return this.Scoreform_section3_similar13_score;
	}
	this.getScoreform_section3_similar13_score=getScoreform_section3_similar13_score;


	function setScoreform_section3_similar13_score(v){
		this.Scoreform_section3_similar13_score=v;
	}
	this.setScoreform_section3_similar13_score=setScoreform_section3_similar13_score;

	this.Scoreform_section3_similar14_response=null;


	function getScoreform_section3_similar14_response() {
		return this.Scoreform_section3_similar14_response;
	}
	this.getScoreform_section3_similar14_response=getScoreform_section3_similar14_response;


	function setScoreform_section3_similar14_response(v){
		this.Scoreform_section3_similar14_response=v;
	}
	this.setScoreform_section3_similar14_response=setScoreform_section3_similar14_response;

	this.Scoreform_section3_similar14_score=null;


	function getScoreform_section3_similar14_score() {
		return this.Scoreform_section3_similar14_score;
	}
	this.getScoreform_section3_similar14_score=getScoreform_section3_similar14_score;


	function setScoreform_section3_similar14_score(v){
		this.Scoreform_section3_similar14_score=v;
	}
	this.setScoreform_section3_similar14_score=setScoreform_section3_similar14_score;

	this.Scoreform_section3_similar15_response=null;


	function getScoreform_section3_similar15_response() {
		return this.Scoreform_section3_similar15_response;
	}
	this.getScoreform_section3_similar15_response=getScoreform_section3_similar15_response;


	function setScoreform_section3_similar15_response(v){
		this.Scoreform_section3_similar15_response=v;
	}
	this.setScoreform_section3_similar15_response=setScoreform_section3_similar15_response;

	this.Scoreform_section3_similar15_score=null;


	function getScoreform_section3_similar15_score() {
		return this.Scoreform_section3_similar15_score;
	}
	this.getScoreform_section3_similar15_score=getScoreform_section3_similar15_score;


	function setScoreform_section3_similar15_score(v){
		this.Scoreform_section3_similar15_score=v;
	}
	this.setScoreform_section3_similar15_score=setScoreform_section3_similar15_score;

	this.Scoreform_section3_similar16_response=null;


	function getScoreform_section3_similar16_response() {
		return this.Scoreform_section3_similar16_response;
	}
	this.getScoreform_section3_similar16_response=getScoreform_section3_similar16_response;


	function setScoreform_section3_similar16_response(v){
		this.Scoreform_section3_similar16_response=v;
	}
	this.setScoreform_section3_similar16_response=setScoreform_section3_similar16_response;

	this.Scoreform_section3_similar16_score=null;


	function getScoreform_section3_similar16_score() {
		return this.Scoreform_section3_similar16_score;
	}
	this.getScoreform_section3_similar16_score=getScoreform_section3_similar16_score;


	function setScoreform_section3_similar16_score(v){
		this.Scoreform_section3_similar16_score=v;
	}
	this.setScoreform_section3_similar16_score=setScoreform_section3_similar16_score;

	this.Scoreform_section3_similar17_response=null;


	function getScoreform_section3_similar17_response() {
		return this.Scoreform_section3_similar17_response;
	}
	this.getScoreform_section3_similar17_response=getScoreform_section3_similar17_response;


	function setScoreform_section3_similar17_response(v){
		this.Scoreform_section3_similar17_response=v;
	}
	this.setScoreform_section3_similar17_response=setScoreform_section3_similar17_response;

	this.Scoreform_section3_similar17_score=null;


	function getScoreform_section3_similar17_score() {
		return this.Scoreform_section3_similar17_score;
	}
	this.getScoreform_section3_similar17_score=getScoreform_section3_similar17_score;


	function setScoreform_section3_similar17_score(v){
		this.Scoreform_section3_similar17_score=v;
	}
	this.setScoreform_section3_similar17_score=setScoreform_section3_similar17_score;

	this.Scoreform_section3_similar18_response=null;


	function getScoreform_section3_similar18_response() {
		return this.Scoreform_section3_similar18_response;
	}
	this.getScoreform_section3_similar18_response=getScoreform_section3_similar18_response;


	function setScoreform_section3_similar18_response(v){
		this.Scoreform_section3_similar18_response=v;
	}
	this.setScoreform_section3_similar18_response=setScoreform_section3_similar18_response;

	this.Scoreform_section3_similar18_score=null;


	function getScoreform_section3_similar18_score() {
		return this.Scoreform_section3_similar18_score;
	}
	this.getScoreform_section3_similar18_score=getScoreform_section3_similar18_score;


	function setScoreform_section3_similar18_score(v){
		this.Scoreform_section3_similar18_score=v;
	}
	this.setScoreform_section3_similar18_score=setScoreform_section3_similar18_score;

	this.Scoreform_section3_similar19_response=null;


	function getScoreform_section3_similar19_response() {
		return this.Scoreform_section3_similar19_response;
	}
	this.getScoreform_section3_similar19_response=getScoreform_section3_similar19_response;


	function setScoreform_section3_similar19_response(v){
		this.Scoreform_section3_similar19_response=v;
	}
	this.setScoreform_section3_similar19_response=setScoreform_section3_similar19_response;

	this.Scoreform_section3_similar19_score=null;


	function getScoreform_section3_similar19_score() {
		return this.Scoreform_section3_similar19_score;
	}
	this.getScoreform_section3_similar19_score=getScoreform_section3_similar19_score;


	function setScoreform_section3_similar19_score(v){
		this.Scoreform_section3_similar19_score=v;
	}
	this.setScoreform_section3_similar19_score=setScoreform_section3_similar19_score;

	this.Scoreform_section3_similar20_response=null;


	function getScoreform_section3_similar20_response() {
		return this.Scoreform_section3_similar20_response;
	}
	this.getScoreform_section3_similar20_response=getScoreform_section3_similar20_response;


	function setScoreform_section3_similar20_response(v){
		this.Scoreform_section3_similar20_response=v;
	}
	this.setScoreform_section3_similar20_response=setScoreform_section3_similar20_response;

	this.Scoreform_section3_similar20_score=null;


	function getScoreform_section3_similar20_score() {
		return this.Scoreform_section3_similar20_score;
	}
	this.getScoreform_section3_similar20_score=getScoreform_section3_similar20_score;


	function setScoreform_section3_similar20_score(v){
		this.Scoreform_section3_similar20_score=v;
	}
	this.setScoreform_section3_similar20_score=setScoreform_section3_similar20_score;

	this.Scoreform_section3_similar21_response=null;


	function getScoreform_section3_similar21_response() {
		return this.Scoreform_section3_similar21_response;
	}
	this.getScoreform_section3_similar21_response=getScoreform_section3_similar21_response;


	function setScoreform_section3_similar21_response(v){
		this.Scoreform_section3_similar21_response=v;
	}
	this.setScoreform_section3_similar21_response=setScoreform_section3_similar21_response;

	this.Scoreform_section3_similar21_score=null;


	function getScoreform_section3_similar21_score() {
		return this.Scoreform_section3_similar21_score;
	}
	this.getScoreform_section3_similar21_score=getScoreform_section3_similar21_score;


	function setScoreform_section3_similar21_score(v){
		this.Scoreform_section3_similar21_score=v;
	}
	this.setScoreform_section3_similar21_score=setScoreform_section3_similar21_score;

	this.Scoreform_section3_similar22_response=null;


	function getScoreform_section3_similar22_response() {
		return this.Scoreform_section3_similar22_response;
	}
	this.getScoreform_section3_similar22_response=getScoreform_section3_similar22_response;


	function setScoreform_section3_similar22_response(v){
		this.Scoreform_section3_similar22_response=v;
	}
	this.setScoreform_section3_similar22_response=setScoreform_section3_similar22_response;

	this.Scoreform_section3_similar22_score=null;


	function getScoreform_section3_similar22_score() {
		return this.Scoreform_section3_similar22_score;
	}
	this.getScoreform_section3_similar22_score=getScoreform_section3_similar22_score;


	function setScoreform_section3_similar22_score(v){
		this.Scoreform_section3_similar22_score=v;
	}
	this.setScoreform_section3_similar22_score=setScoreform_section3_similar22_score;

	this.Scoreform_section3_similar23_response=null;


	function getScoreform_section3_similar23_response() {
		return this.Scoreform_section3_similar23_response;
	}
	this.getScoreform_section3_similar23_response=getScoreform_section3_similar23_response;


	function setScoreform_section3_similar23_response(v){
		this.Scoreform_section3_similar23_response=v;
	}
	this.setScoreform_section3_similar23_response=setScoreform_section3_similar23_response;

	this.Scoreform_section3_similar23_score=null;


	function getScoreform_section3_similar23_score() {
		return this.Scoreform_section3_similar23_score;
	}
	this.getScoreform_section3_similar23_score=getScoreform_section3_similar23_score;


	function setScoreform_section3_similar23_score(v){
		this.Scoreform_section3_similar23_score=v;
	}
	this.setScoreform_section3_similar23_score=setScoreform_section3_similar23_score;

	this.Scoreform_section3_similar24_response=null;


	function getScoreform_section3_similar24_response() {
		return this.Scoreform_section3_similar24_response;
	}
	this.getScoreform_section3_similar24_response=getScoreform_section3_similar24_response;


	function setScoreform_section3_similar24_response(v){
		this.Scoreform_section3_similar24_response=v;
	}
	this.setScoreform_section3_similar24_response=setScoreform_section3_similar24_response;

	this.Scoreform_section3_similar24_score=null;


	function getScoreform_section3_similar24_score() {
		return this.Scoreform_section3_similar24_score;
	}
	this.getScoreform_section3_similar24_score=getScoreform_section3_similar24_score;


	function setScoreform_section3_similar24_score(v){
		this.Scoreform_section3_similar24_score=v;
	}
	this.setScoreform_section3_similar24_score=setScoreform_section3_similar24_score;

	this.Scoreform_section3_similar25_response=null;


	function getScoreform_section3_similar25_response() {
		return this.Scoreform_section3_similar25_response;
	}
	this.getScoreform_section3_similar25_response=getScoreform_section3_similar25_response;


	function setScoreform_section3_similar25_response(v){
		this.Scoreform_section3_similar25_response=v;
	}
	this.setScoreform_section3_similar25_response=setScoreform_section3_similar25_response;

	this.Scoreform_section3_similar25_score=null;


	function getScoreform_section3_similar25_score() {
		return this.Scoreform_section3_similar25_score;
	}
	this.getScoreform_section3_similar25_score=getScoreform_section3_similar25_score;


	function setScoreform_section3_similar25_score(v){
		this.Scoreform_section3_similar25_score=v;
	}
	this.setScoreform_section3_similar25_score=setScoreform_section3_similar25_score;

	this.Scoreform_section3_similar26_response=null;


	function getScoreform_section3_similar26_response() {
		return this.Scoreform_section3_similar26_response;
	}
	this.getScoreform_section3_similar26_response=getScoreform_section3_similar26_response;


	function setScoreform_section3_similar26_response(v){
		this.Scoreform_section3_similar26_response=v;
	}
	this.setScoreform_section3_similar26_response=setScoreform_section3_similar26_response;

	this.Scoreform_section3_similar26_score=null;


	function getScoreform_section3_similar26_score() {
		return this.Scoreform_section3_similar26_score;
	}
	this.getScoreform_section3_similar26_score=getScoreform_section3_similar26_score;


	function setScoreform_section3_similar26_score(v){
		this.Scoreform_section3_similar26_score=v;
	}
	this.setScoreform_section3_similar26_score=setScoreform_section3_similar26_score;

	this.Scoreform_section4_matrixa_response=null;


	function getScoreform_section4_matrixa_response() {
		return this.Scoreform_section4_matrixa_response;
	}
	this.getScoreform_section4_matrixa_response=getScoreform_section4_matrixa_response;


	function setScoreform_section4_matrixa_response(v){
		this.Scoreform_section4_matrixa_response=v;
	}
	this.setScoreform_section4_matrixa_response=setScoreform_section4_matrixa_response;

	this.Scoreform_section4_matrixa_score=null;


	function getScoreform_section4_matrixa_score() {
		return this.Scoreform_section4_matrixa_score;
	}
	this.getScoreform_section4_matrixa_score=getScoreform_section4_matrixa_score;


	function setScoreform_section4_matrixa_score(v){
		this.Scoreform_section4_matrixa_score=v;
	}
	this.setScoreform_section4_matrixa_score=setScoreform_section4_matrixa_score;

	this.Scoreform_section4_matrixb_response=null;


	function getScoreform_section4_matrixb_response() {
		return this.Scoreform_section4_matrixb_response;
	}
	this.getScoreform_section4_matrixb_response=getScoreform_section4_matrixb_response;


	function setScoreform_section4_matrixb_response(v){
		this.Scoreform_section4_matrixb_response=v;
	}
	this.setScoreform_section4_matrixb_response=setScoreform_section4_matrixb_response;

	this.Scoreform_section4_matrixb_score=null;


	function getScoreform_section4_matrixb_score() {
		return this.Scoreform_section4_matrixb_score;
	}
	this.getScoreform_section4_matrixb_score=getScoreform_section4_matrixb_score;


	function setScoreform_section4_matrixb_score(v){
		this.Scoreform_section4_matrixb_score=v;
	}
	this.setScoreform_section4_matrixb_score=setScoreform_section4_matrixb_score;

	this.Scoreform_section4_matrix1_response=null;


	function getScoreform_section4_matrix1_response() {
		return this.Scoreform_section4_matrix1_response;
	}
	this.getScoreform_section4_matrix1_response=getScoreform_section4_matrix1_response;


	function setScoreform_section4_matrix1_response(v){
		this.Scoreform_section4_matrix1_response=v;
	}
	this.setScoreform_section4_matrix1_response=setScoreform_section4_matrix1_response;

	this.Scoreform_section4_matrix1_score=null;


	function getScoreform_section4_matrix1_score() {
		return this.Scoreform_section4_matrix1_score;
	}
	this.getScoreform_section4_matrix1_score=getScoreform_section4_matrix1_score;


	function setScoreform_section4_matrix1_score(v){
		this.Scoreform_section4_matrix1_score=v;
	}
	this.setScoreform_section4_matrix1_score=setScoreform_section4_matrix1_score;

	this.Scoreform_section4_matrix2_response=null;


	function getScoreform_section4_matrix2_response() {
		return this.Scoreform_section4_matrix2_response;
	}
	this.getScoreform_section4_matrix2_response=getScoreform_section4_matrix2_response;


	function setScoreform_section4_matrix2_response(v){
		this.Scoreform_section4_matrix2_response=v;
	}
	this.setScoreform_section4_matrix2_response=setScoreform_section4_matrix2_response;

	this.Scoreform_section4_matrix2_score=null;


	function getScoreform_section4_matrix2_score() {
		return this.Scoreform_section4_matrix2_score;
	}
	this.getScoreform_section4_matrix2_score=getScoreform_section4_matrix2_score;


	function setScoreform_section4_matrix2_score(v){
		this.Scoreform_section4_matrix2_score=v;
	}
	this.setScoreform_section4_matrix2_score=setScoreform_section4_matrix2_score;

	this.Scoreform_section4_matrix3_response=null;


	function getScoreform_section4_matrix3_response() {
		return this.Scoreform_section4_matrix3_response;
	}
	this.getScoreform_section4_matrix3_response=getScoreform_section4_matrix3_response;


	function setScoreform_section4_matrix3_response(v){
		this.Scoreform_section4_matrix3_response=v;
	}
	this.setScoreform_section4_matrix3_response=setScoreform_section4_matrix3_response;

	this.Scoreform_section4_matrix3_score=null;


	function getScoreform_section4_matrix3_score() {
		return this.Scoreform_section4_matrix3_score;
	}
	this.getScoreform_section4_matrix3_score=getScoreform_section4_matrix3_score;


	function setScoreform_section4_matrix3_score(v){
		this.Scoreform_section4_matrix3_score=v;
	}
	this.setScoreform_section4_matrix3_score=setScoreform_section4_matrix3_score;

	this.Scoreform_section4_matrix4_response=null;


	function getScoreform_section4_matrix4_response() {
		return this.Scoreform_section4_matrix4_response;
	}
	this.getScoreform_section4_matrix4_response=getScoreform_section4_matrix4_response;


	function setScoreform_section4_matrix4_response(v){
		this.Scoreform_section4_matrix4_response=v;
	}
	this.setScoreform_section4_matrix4_response=setScoreform_section4_matrix4_response;

	this.Scoreform_section4_matrix4_score=null;


	function getScoreform_section4_matrix4_score() {
		return this.Scoreform_section4_matrix4_score;
	}
	this.getScoreform_section4_matrix4_score=getScoreform_section4_matrix4_score;


	function setScoreform_section4_matrix4_score(v){
		this.Scoreform_section4_matrix4_score=v;
	}
	this.setScoreform_section4_matrix4_score=setScoreform_section4_matrix4_score;

	this.Scoreform_section4_matrix5_response=null;


	function getScoreform_section4_matrix5_response() {
		return this.Scoreform_section4_matrix5_response;
	}
	this.getScoreform_section4_matrix5_response=getScoreform_section4_matrix5_response;


	function setScoreform_section4_matrix5_response(v){
		this.Scoreform_section4_matrix5_response=v;
	}
	this.setScoreform_section4_matrix5_response=setScoreform_section4_matrix5_response;

	this.Scoreform_section4_matrix5_score=null;


	function getScoreform_section4_matrix5_score() {
		return this.Scoreform_section4_matrix5_score;
	}
	this.getScoreform_section4_matrix5_score=getScoreform_section4_matrix5_score;


	function setScoreform_section4_matrix5_score(v){
		this.Scoreform_section4_matrix5_score=v;
	}
	this.setScoreform_section4_matrix5_score=setScoreform_section4_matrix5_score;

	this.Scoreform_section4_matrix6_response=null;


	function getScoreform_section4_matrix6_response() {
		return this.Scoreform_section4_matrix6_response;
	}
	this.getScoreform_section4_matrix6_response=getScoreform_section4_matrix6_response;


	function setScoreform_section4_matrix6_response(v){
		this.Scoreform_section4_matrix6_response=v;
	}
	this.setScoreform_section4_matrix6_response=setScoreform_section4_matrix6_response;

	this.Scoreform_section4_matrix6_score=null;


	function getScoreform_section4_matrix6_score() {
		return this.Scoreform_section4_matrix6_score;
	}
	this.getScoreform_section4_matrix6_score=getScoreform_section4_matrix6_score;


	function setScoreform_section4_matrix6_score(v){
		this.Scoreform_section4_matrix6_score=v;
	}
	this.setScoreform_section4_matrix6_score=setScoreform_section4_matrix6_score;

	this.Scoreform_section4_matrix7_response=null;


	function getScoreform_section4_matrix7_response() {
		return this.Scoreform_section4_matrix7_response;
	}
	this.getScoreform_section4_matrix7_response=getScoreform_section4_matrix7_response;


	function setScoreform_section4_matrix7_response(v){
		this.Scoreform_section4_matrix7_response=v;
	}
	this.setScoreform_section4_matrix7_response=setScoreform_section4_matrix7_response;

	this.Scoreform_section4_matrix7_score=null;


	function getScoreform_section4_matrix7_score() {
		return this.Scoreform_section4_matrix7_score;
	}
	this.getScoreform_section4_matrix7_score=getScoreform_section4_matrix7_score;


	function setScoreform_section4_matrix7_score(v){
		this.Scoreform_section4_matrix7_score=v;
	}
	this.setScoreform_section4_matrix7_score=setScoreform_section4_matrix7_score;

	this.Scoreform_section4_matrix8_response=null;


	function getScoreform_section4_matrix8_response() {
		return this.Scoreform_section4_matrix8_response;
	}
	this.getScoreform_section4_matrix8_response=getScoreform_section4_matrix8_response;


	function setScoreform_section4_matrix8_response(v){
		this.Scoreform_section4_matrix8_response=v;
	}
	this.setScoreform_section4_matrix8_response=setScoreform_section4_matrix8_response;

	this.Scoreform_section4_matrix8_score=null;


	function getScoreform_section4_matrix8_score() {
		return this.Scoreform_section4_matrix8_score;
	}
	this.getScoreform_section4_matrix8_score=getScoreform_section4_matrix8_score;


	function setScoreform_section4_matrix8_score(v){
		this.Scoreform_section4_matrix8_score=v;
	}
	this.setScoreform_section4_matrix8_score=setScoreform_section4_matrix8_score;

	this.Scoreform_section4_matrix9_response=null;


	function getScoreform_section4_matrix9_response() {
		return this.Scoreform_section4_matrix9_response;
	}
	this.getScoreform_section4_matrix9_response=getScoreform_section4_matrix9_response;


	function setScoreform_section4_matrix9_response(v){
		this.Scoreform_section4_matrix9_response=v;
	}
	this.setScoreform_section4_matrix9_response=setScoreform_section4_matrix9_response;

	this.Scoreform_section4_matrix9_score=null;


	function getScoreform_section4_matrix9_score() {
		return this.Scoreform_section4_matrix9_score;
	}
	this.getScoreform_section4_matrix9_score=getScoreform_section4_matrix9_score;


	function setScoreform_section4_matrix9_score(v){
		this.Scoreform_section4_matrix9_score=v;
	}
	this.setScoreform_section4_matrix9_score=setScoreform_section4_matrix9_score;

	this.Scoreform_section4_matrix10_response=null;


	function getScoreform_section4_matrix10_response() {
		return this.Scoreform_section4_matrix10_response;
	}
	this.getScoreform_section4_matrix10_response=getScoreform_section4_matrix10_response;


	function setScoreform_section4_matrix10_response(v){
		this.Scoreform_section4_matrix10_response=v;
	}
	this.setScoreform_section4_matrix10_response=setScoreform_section4_matrix10_response;

	this.Scoreform_section4_matrix10_score=null;


	function getScoreform_section4_matrix10_score() {
		return this.Scoreform_section4_matrix10_score;
	}
	this.getScoreform_section4_matrix10_score=getScoreform_section4_matrix10_score;


	function setScoreform_section4_matrix10_score(v){
		this.Scoreform_section4_matrix10_score=v;
	}
	this.setScoreform_section4_matrix10_score=setScoreform_section4_matrix10_score;

	this.Scoreform_section4_matrix11_response=null;


	function getScoreform_section4_matrix11_response() {
		return this.Scoreform_section4_matrix11_response;
	}
	this.getScoreform_section4_matrix11_response=getScoreform_section4_matrix11_response;


	function setScoreform_section4_matrix11_response(v){
		this.Scoreform_section4_matrix11_response=v;
	}
	this.setScoreform_section4_matrix11_response=setScoreform_section4_matrix11_response;

	this.Scoreform_section4_matrix11_score=null;


	function getScoreform_section4_matrix11_score() {
		return this.Scoreform_section4_matrix11_score;
	}
	this.getScoreform_section4_matrix11_score=getScoreform_section4_matrix11_score;


	function setScoreform_section4_matrix11_score(v){
		this.Scoreform_section4_matrix11_score=v;
	}
	this.setScoreform_section4_matrix11_score=setScoreform_section4_matrix11_score;

	this.Scoreform_section4_matrix12_response=null;


	function getScoreform_section4_matrix12_response() {
		return this.Scoreform_section4_matrix12_response;
	}
	this.getScoreform_section4_matrix12_response=getScoreform_section4_matrix12_response;


	function setScoreform_section4_matrix12_response(v){
		this.Scoreform_section4_matrix12_response=v;
	}
	this.setScoreform_section4_matrix12_response=setScoreform_section4_matrix12_response;

	this.Scoreform_section4_matrix12_score=null;


	function getScoreform_section4_matrix12_score() {
		return this.Scoreform_section4_matrix12_score;
	}
	this.getScoreform_section4_matrix12_score=getScoreform_section4_matrix12_score;


	function setScoreform_section4_matrix12_score(v){
		this.Scoreform_section4_matrix12_score=v;
	}
	this.setScoreform_section4_matrix12_score=setScoreform_section4_matrix12_score;

	this.Scoreform_section4_matrix13_response=null;


	function getScoreform_section4_matrix13_response() {
		return this.Scoreform_section4_matrix13_response;
	}
	this.getScoreform_section4_matrix13_response=getScoreform_section4_matrix13_response;


	function setScoreform_section4_matrix13_response(v){
		this.Scoreform_section4_matrix13_response=v;
	}
	this.setScoreform_section4_matrix13_response=setScoreform_section4_matrix13_response;

	this.Scoreform_section4_matrix13_score=null;


	function getScoreform_section4_matrix13_score() {
		return this.Scoreform_section4_matrix13_score;
	}
	this.getScoreform_section4_matrix13_score=getScoreform_section4_matrix13_score;


	function setScoreform_section4_matrix13_score(v){
		this.Scoreform_section4_matrix13_score=v;
	}
	this.setScoreform_section4_matrix13_score=setScoreform_section4_matrix13_score;

	this.Scoreform_section4_matrix14_response=null;


	function getScoreform_section4_matrix14_response() {
		return this.Scoreform_section4_matrix14_response;
	}
	this.getScoreform_section4_matrix14_response=getScoreform_section4_matrix14_response;


	function setScoreform_section4_matrix14_response(v){
		this.Scoreform_section4_matrix14_response=v;
	}
	this.setScoreform_section4_matrix14_response=setScoreform_section4_matrix14_response;

	this.Scoreform_section4_matrix14_score=null;


	function getScoreform_section4_matrix14_score() {
		return this.Scoreform_section4_matrix14_score;
	}
	this.getScoreform_section4_matrix14_score=getScoreform_section4_matrix14_score;


	function setScoreform_section4_matrix14_score(v){
		this.Scoreform_section4_matrix14_score=v;
	}
	this.setScoreform_section4_matrix14_score=setScoreform_section4_matrix14_score;

	this.Scoreform_section4_matrix15_response=null;


	function getScoreform_section4_matrix15_response() {
		return this.Scoreform_section4_matrix15_response;
	}
	this.getScoreform_section4_matrix15_response=getScoreform_section4_matrix15_response;


	function setScoreform_section4_matrix15_response(v){
		this.Scoreform_section4_matrix15_response=v;
	}
	this.setScoreform_section4_matrix15_response=setScoreform_section4_matrix15_response;

	this.Scoreform_section4_matrix15_score=null;


	function getScoreform_section4_matrix15_score() {
		return this.Scoreform_section4_matrix15_score;
	}
	this.getScoreform_section4_matrix15_score=getScoreform_section4_matrix15_score;


	function setScoreform_section4_matrix15_score(v){
		this.Scoreform_section4_matrix15_score=v;
	}
	this.setScoreform_section4_matrix15_score=setScoreform_section4_matrix15_score;

	this.Scoreform_section4_matrix16_response=null;


	function getScoreform_section4_matrix16_response() {
		return this.Scoreform_section4_matrix16_response;
	}
	this.getScoreform_section4_matrix16_response=getScoreform_section4_matrix16_response;


	function setScoreform_section4_matrix16_response(v){
		this.Scoreform_section4_matrix16_response=v;
	}
	this.setScoreform_section4_matrix16_response=setScoreform_section4_matrix16_response;

	this.Scoreform_section4_matrix16_score=null;


	function getScoreform_section4_matrix16_score() {
		return this.Scoreform_section4_matrix16_score;
	}
	this.getScoreform_section4_matrix16_score=getScoreform_section4_matrix16_score;


	function setScoreform_section4_matrix16_score(v){
		this.Scoreform_section4_matrix16_score=v;
	}
	this.setScoreform_section4_matrix16_score=setScoreform_section4_matrix16_score;

	this.Scoreform_section4_matrix17_response=null;


	function getScoreform_section4_matrix17_response() {
		return this.Scoreform_section4_matrix17_response;
	}
	this.getScoreform_section4_matrix17_response=getScoreform_section4_matrix17_response;


	function setScoreform_section4_matrix17_response(v){
		this.Scoreform_section4_matrix17_response=v;
	}
	this.setScoreform_section4_matrix17_response=setScoreform_section4_matrix17_response;

	this.Scoreform_section4_matrix17_score=null;


	function getScoreform_section4_matrix17_score() {
		return this.Scoreform_section4_matrix17_score;
	}
	this.getScoreform_section4_matrix17_score=getScoreform_section4_matrix17_score;


	function setScoreform_section4_matrix17_score(v){
		this.Scoreform_section4_matrix17_score=v;
	}
	this.setScoreform_section4_matrix17_score=setScoreform_section4_matrix17_score;

	this.Scoreform_section4_matrix18_response=null;


	function getScoreform_section4_matrix18_response() {
		return this.Scoreform_section4_matrix18_response;
	}
	this.getScoreform_section4_matrix18_response=getScoreform_section4_matrix18_response;


	function setScoreform_section4_matrix18_response(v){
		this.Scoreform_section4_matrix18_response=v;
	}
	this.setScoreform_section4_matrix18_response=setScoreform_section4_matrix18_response;

	this.Scoreform_section4_matrix18_score=null;


	function getScoreform_section4_matrix18_score() {
		return this.Scoreform_section4_matrix18_score;
	}
	this.getScoreform_section4_matrix18_score=getScoreform_section4_matrix18_score;


	function setScoreform_section4_matrix18_score(v){
		this.Scoreform_section4_matrix18_score=v;
	}
	this.setScoreform_section4_matrix18_score=setScoreform_section4_matrix18_score;

	this.Scoreform_section4_matrix19_response=null;


	function getScoreform_section4_matrix19_response() {
		return this.Scoreform_section4_matrix19_response;
	}
	this.getScoreform_section4_matrix19_response=getScoreform_section4_matrix19_response;


	function setScoreform_section4_matrix19_response(v){
		this.Scoreform_section4_matrix19_response=v;
	}
	this.setScoreform_section4_matrix19_response=setScoreform_section4_matrix19_response;

	this.Scoreform_section4_matrix19_score=null;


	function getScoreform_section4_matrix19_score() {
		return this.Scoreform_section4_matrix19_score;
	}
	this.getScoreform_section4_matrix19_score=getScoreform_section4_matrix19_score;


	function setScoreform_section4_matrix19_score(v){
		this.Scoreform_section4_matrix19_score=v;
	}
	this.setScoreform_section4_matrix19_score=setScoreform_section4_matrix19_score;

	this.Scoreform_section4_matrix20_response=null;


	function getScoreform_section4_matrix20_response() {
		return this.Scoreform_section4_matrix20_response;
	}
	this.getScoreform_section4_matrix20_response=getScoreform_section4_matrix20_response;


	function setScoreform_section4_matrix20_response(v){
		this.Scoreform_section4_matrix20_response=v;
	}
	this.setScoreform_section4_matrix20_response=setScoreform_section4_matrix20_response;

	this.Scoreform_section4_matrix20_score=null;


	function getScoreform_section4_matrix20_score() {
		return this.Scoreform_section4_matrix20_score;
	}
	this.getScoreform_section4_matrix20_score=getScoreform_section4_matrix20_score;


	function setScoreform_section4_matrix20_score(v){
		this.Scoreform_section4_matrix20_score=v;
	}
	this.setScoreform_section4_matrix20_score=setScoreform_section4_matrix20_score;

	this.Scoreform_section4_matrix21_response=null;


	function getScoreform_section4_matrix21_response() {
		return this.Scoreform_section4_matrix21_response;
	}
	this.getScoreform_section4_matrix21_response=getScoreform_section4_matrix21_response;


	function setScoreform_section4_matrix21_response(v){
		this.Scoreform_section4_matrix21_response=v;
	}
	this.setScoreform_section4_matrix21_response=setScoreform_section4_matrix21_response;

	this.Scoreform_section4_matrix21_score=null;


	function getScoreform_section4_matrix21_score() {
		return this.Scoreform_section4_matrix21_score;
	}
	this.getScoreform_section4_matrix21_score=getScoreform_section4_matrix21_score;


	function setScoreform_section4_matrix21_score(v){
		this.Scoreform_section4_matrix21_score=v;
	}
	this.setScoreform_section4_matrix21_score=setScoreform_section4_matrix21_score;

	this.Scoreform_section4_matrix22_response=null;


	function getScoreform_section4_matrix22_response() {
		return this.Scoreform_section4_matrix22_response;
	}
	this.getScoreform_section4_matrix22_response=getScoreform_section4_matrix22_response;


	function setScoreform_section4_matrix22_response(v){
		this.Scoreform_section4_matrix22_response=v;
	}
	this.setScoreform_section4_matrix22_response=setScoreform_section4_matrix22_response;

	this.Scoreform_section4_matrix22_score=null;


	function getScoreform_section4_matrix22_score() {
		return this.Scoreform_section4_matrix22_score;
	}
	this.getScoreform_section4_matrix22_score=getScoreform_section4_matrix22_score;


	function setScoreform_section4_matrix22_score(v){
		this.Scoreform_section4_matrix22_score=v;
	}
	this.setScoreform_section4_matrix22_score=setScoreform_section4_matrix22_score;

	this.Scoreform_section4_matrix23_response=null;


	function getScoreform_section4_matrix23_response() {
		return this.Scoreform_section4_matrix23_response;
	}
	this.getScoreform_section4_matrix23_response=getScoreform_section4_matrix23_response;


	function setScoreform_section4_matrix23_response(v){
		this.Scoreform_section4_matrix23_response=v;
	}
	this.setScoreform_section4_matrix23_response=setScoreform_section4_matrix23_response;

	this.Scoreform_section4_matrix23_score=null;


	function getScoreform_section4_matrix23_score() {
		return this.Scoreform_section4_matrix23_score;
	}
	this.getScoreform_section4_matrix23_score=getScoreform_section4_matrix23_score;


	function setScoreform_section4_matrix23_score(v){
		this.Scoreform_section4_matrix23_score=v;
	}
	this.setScoreform_section4_matrix23_score=setScoreform_section4_matrix23_score;

	this.Scoreform_section4_matrix24_response=null;


	function getScoreform_section4_matrix24_response() {
		return this.Scoreform_section4_matrix24_response;
	}
	this.getScoreform_section4_matrix24_response=getScoreform_section4_matrix24_response;


	function setScoreform_section4_matrix24_response(v){
		this.Scoreform_section4_matrix24_response=v;
	}
	this.setScoreform_section4_matrix24_response=setScoreform_section4_matrix24_response;

	this.Scoreform_section4_matrix24_score=null;


	function getScoreform_section4_matrix24_score() {
		return this.Scoreform_section4_matrix24_score;
	}
	this.getScoreform_section4_matrix24_score=getScoreform_section4_matrix24_score;


	function setScoreform_section4_matrix24_score(v){
		this.Scoreform_section4_matrix24_score=v;
	}
	this.setScoreform_section4_matrix24_score=setScoreform_section4_matrix24_score;

	this.Scoreform_section4_matrix25_response=null;


	function getScoreform_section4_matrix25_response() {
		return this.Scoreform_section4_matrix25_response;
	}
	this.getScoreform_section4_matrix25_response=getScoreform_section4_matrix25_response;


	function setScoreform_section4_matrix25_response(v){
		this.Scoreform_section4_matrix25_response=v;
	}
	this.setScoreform_section4_matrix25_response=setScoreform_section4_matrix25_response;

	this.Scoreform_section4_matrix25_score=null;


	function getScoreform_section4_matrix25_score() {
		return this.Scoreform_section4_matrix25_score;
	}
	this.getScoreform_section4_matrix25_score=getScoreform_section4_matrix25_score;


	function setScoreform_section4_matrix25_score(v){
		this.Scoreform_section4_matrix25_score=v;
	}
	this.setScoreform_section4_matrix25_score=setScoreform_section4_matrix25_score;

	this.Scoreform_section4_matrix26_response=null;


	function getScoreform_section4_matrix26_response() {
		return this.Scoreform_section4_matrix26_response;
	}
	this.getScoreform_section4_matrix26_response=getScoreform_section4_matrix26_response;


	function setScoreform_section4_matrix26_response(v){
		this.Scoreform_section4_matrix26_response=v;
	}
	this.setScoreform_section4_matrix26_response=setScoreform_section4_matrix26_response;

	this.Scoreform_section4_matrix26_score=null;


	function getScoreform_section4_matrix26_score() {
		return this.Scoreform_section4_matrix26_score;
	}
	this.getScoreform_section4_matrix26_score=getScoreform_section4_matrix26_score;


	function setScoreform_section4_matrix26_score(v){
		this.Scoreform_section4_matrix26_score=v;
	}
	this.setScoreform_section4_matrix26_score=setScoreform_section4_matrix26_score;

	this.Scoreform_section4_matrix27_response=null;


	function getScoreform_section4_matrix27_response() {
		return this.Scoreform_section4_matrix27_response;
	}
	this.getScoreform_section4_matrix27_response=getScoreform_section4_matrix27_response;


	function setScoreform_section4_matrix27_response(v){
		this.Scoreform_section4_matrix27_response=v;
	}
	this.setScoreform_section4_matrix27_response=setScoreform_section4_matrix27_response;

	this.Scoreform_section4_matrix27_score=null;


	function getScoreform_section4_matrix27_score() {
		return this.Scoreform_section4_matrix27_score;
	}
	this.getScoreform_section4_matrix27_score=getScoreform_section4_matrix27_score;


	function setScoreform_section4_matrix27_score(v){
		this.Scoreform_section4_matrix27_score=v;
	}
	this.setScoreform_section4_matrix27_score=setScoreform_section4_matrix27_score;

	this.Scoreform_section4_matrix28_response=null;


	function getScoreform_section4_matrix28_response() {
		return this.Scoreform_section4_matrix28_response;
	}
	this.getScoreform_section4_matrix28_response=getScoreform_section4_matrix28_response;


	function setScoreform_section4_matrix28_response(v){
		this.Scoreform_section4_matrix28_response=v;
	}
	this.setScoreform_section4_matrix28_response=setScoreform_section4_matrix28_response;

	this.Scoreform_section4_matrix28_score=null;


	function getScoreform_section4_matrix28_score() {
		return this.Scoreform_section4_matrix28_score;
	}
	this.getScoreform_section4_matrix28_score=getScoreform_section4_matrix28_score;


	function setScoreform_section4_matrix28_score(v){
		this.Scoreform_section4_matrix28_score=v;
	}
	this.setScoreform_section4_matrix28_score=setScoreform_section4_matrix28_score;

	this.Scoreform_section4_matrix29_response=null;


	function getScoreform_section4_matrix29_response() {
		return this.Scoreform_section4_matrix29_response;
	}
	this.getScoreform_section4_matrix29_response=getScoreform_section4_matrix29_response;


	function setScoreform_section4_matrix29_response(v){
		this.Scoreform_section4_matrix29_response=v;
	}
	this.setScoreform_section4_matrix29_response=setScoreform_section4_matrix29_response;

	this.Scoreform_section4_matrix29_score=null;


	function getScoreform_section4_matrix29_score() {
		return this.Scoreform_section4_matrix29_score;
	}
	this.getScoreform_section4_matrix29_score=getScoreform_section4_matrix29_score;


	function setScoreform_section4_matrix29_score(v){
		this.Scoreform_section4_matrix29_score=v;
	}
	this.setScoreform_section4_matrix29_score=setScoreform_section4_matrix29_score;

	this.Scoreform_section4_matrix30_response=null;


	function getScoreform_section4_matrix30_response() {
		return this.Scoreform_section4_matrix30_response;
	}
	this.getScoreform_section4_matrix30_response=getScoreform_section4_matrix30_response;


	function setScoreform_section4_matrix30_response(v){
		this.Scoreform_section4_matrix30_response=v;
	}
	this.setScoreform_section4_matrix30_response=setScoreform_section4_matrix30_response;

	this.Scoreform_section4_matrix30_score=null;


	function getScoreform_section4_matrix30_score() {
		return this.Scoreform_section4_matrix30_score;
	}
	this.getScoreform_section4_matrix30_score=getScoreform_section4_matrix30_score;


	function setScoreform_section4_matrix30_score(v){
		this.Scoreform_section4_matrix30_score=v;
	}
	this.setScoreform_section4_matrix30_score=setScoreform_section4_matrix30_score;

	this.Scoreform_section4_matrix31_response=null;


	function getScoreform_section4_matrix31_response() {
		return this.Scoreform_section4_matrix31_response;
	}
	this.getScoreform_section4_matrix31_response=getScoreform_section4_matrix31_response;


	function setScoreform_section4_matrix31_response(v){
		this.Scoreform_section4_matrix31_response=v;
	}
	this.setScoreform_section4_matrix31_response=setScoreform_section4_matrix31_response;

	this.Scoreform_section4_matrix31_score=null;


	function getScoreform_section4_matrix31_score() {
		return this.Scoreform_section4_matrix31_score;
	}
	this.getScoreform_section4_matrix31_score=getScoreform_section4_matrix31_score;


	function setScoreform_section4_matrix31_score(v){
		this.Scoreform_section4_matrix31_score=v;
	}
	this.setScoreform_section4_matrix31_score=setScoreform_section4_matrix31_score;

	this.Scoreform_section4_matrix32_response=null;


	function getScoreform_section4_matrix32_response() {
		return this.Scoreform_section4_matrix32_response;
	}
	this.getScoreform_section4_matrix32_response=getScoreform_section4_matrix32_response;


	function setScoreform_section4_matrix32_response(v){
		this.Scoreform_section4_matrix32_response=v;
	}
	this.setScoreform_section4_matrix32_response=setScoreform_section4_matrix32_response;

	this.Scoreform_section4_matrix32_score=null;


	function getScoreform_section4_matrix32_score() {
		return this.Scoreform_section4_matrix32_score;
	}
	this.getScoreform_section4_matrix32_score=getScoreform_section4_matrix32_score;


	function setScoreform_section4_matrix32_score(v){
		this.Scoreform_section4_matrix32_score=v;
	}
	this.setScoreform_section4_matrix32_score=setScoreform_section4_matrix32_score;

	this.Scoreform_section4_matrix33_response=null;


	function getScoreform_section4_matrix33_response() {
		return this.Scoreform_section4_matrix33_response;
	}
	this.getScoreform_section4_matrix33_response=getScoreform_section4_matrix33_response;


	function setScoreform_section4_matrix33_response(v){
		this.Scoreform_section4_matrix33_response=v;
	}
	this.setScoreform_section4_matrix33_response=setScoreform_section4_matrix33_response;

	this.Scoreform_section4_matrix33_score=null;


	function getScoreform_section4_matrix33_score() {
		return this.Scoreform_section4_matrix33_score;
	}
	this.getScoreform_section4_matrix33_score=getScoreform_section4_matrix33_score;


	function setScoreform_section4_matrix33_score(v){
		this.Scoreform_section4_matrix33_score=v;
	}
	this.setScoreform_section4_matrix33_score=setScoreform_section4_matrix33_score;

	this.Scoreform_section4_matrix34_response=null;


	function getScoreform_section4_matrix34_response() {
		return this.Scoreform_section4_matrix34_response;
	}
	this.getScoreform_section4_matrix34_response=getScoreform_section4_matrix34_response;


	function setScoreform_section4_matrix34_response(v){
		this.Scoreform_section4_matrix34_response=v;
	}
	this.setScoreform_section4_matrix34_response=setScoreform_section4_matrix34_response;

	this.Scoreform_section4_matrix34_score=null;


	function getScoreform_section4_matrix34_score() {
		return this.Scoreform_section4_matrix34_score;
	}
	this.getScoreform_section4_matrix34_score=getScoreform_section4_matrix34_score;


	function setScoreform_section4_matrix34_score(v){
		this.Scoreform_section4_matrix34_score=v;
	}
	this.setScoreform_section4_matrix34_score=setScoreform_section4_matrix34_score;

	this.Scoreform_section4_matrix35_response=null;


	function getScoreform_section4_matrix35_response() {
		return this.Scoreform_section4_matrix35_response;
	}
	this.getScoreform_section4_matrix35_response=getScoreform_section4_matrix35_response;


	function setScoreform_section4_matrix35_response(v){
		this.Scoreform_section4_matrix35_response=v;
	}
	this.setScoreform_section4_matrix35_response=setScoreform_section4_matrix35_response;

	this.Scoreform_section4_matrix35_score=null;


	function getScoreform_section4_matrix35_score() {
		return this.Scoreform_section4_matrix35_score;
	}
	this.getScoreform_section4_matrix35_score=getScoreform_section4_matrix35_score;


	function setScoreform_section4_matrix35_score(v){
		this.Scoreform_section4_matrix35_score=v;
	}
	this.setScoreform_section4_matrix35_score=setScoreform_section4_matrix35_score;

	this.Scoreform_behavob_reasonrefer=null;


	function getScoreform_behavob_reasonrefer() {
		return this.Scoreform_behavob_reasonrefer;
	}
	this.getScoreform_behavob_reasonrefer=getScoreform_behavob_reasonrefer;


	function setScoreform_behavob_reasonrefer(v){
		this.Scoreform_behavob_reasonrefer=v;
	}
	this.setScoreform_behavob_reasonrefer=setScoreform_behavob_reasonrefer;

	this.Scoreform_behavob_attitude=null;


	function getScoreform_behavob_attitude() {
		return this.Scoreform_behavob_attitude;
	}
	this.getScoreform_behavob_attitude=getScoreform_behavob_attitude;


	function setScoreform_behavob_attitude(v){
		this.Scoreform_behavob_attitude=v;
	}
	this.setScoreform_behavob_attitude=setScoreform_behavob_attitude;

	this.Scoreform_behavob_physappear=null;


	function getScoreform_behavob_physappear() {
		return this.Scoreform_behavob_physappear;
	}
	this.getScoreform_behavob_physappear=getScoreform_behavob_physappear;


	function setScoreform_behavob_physappear(v){
		this.Scoreform_behavob_physappear=v;
	}
	this.setScoreform_behavob_physappear=setScoreform_behavob_physappear;

	this.Scoreform_behavob_attention=null;


	function getScoreform_behavob_attention() {
		return this.Scoreform_behavob_attention;
	}
	this.getScoreform_behavob_attention=getScoreform_behavob_attention;


	function setScoreform_behavob_attention(v){
		this.Scoreform_behavob_attention=v;
	}
	this.setScoreform_behavob_attention=setScoreform_behavob_attention;

	this.Scoreform_behavob_visaudmotor=null;


	function getScoreform_behavob_visaudmotor() {
		return this.Scoreform_behavob_visaudmotor;
	}
	this.getScoreform_behavob_visaudmotor=getScoreform_behavob_visaudmotor;


	function setScoreform_behavob_visaudmotor(v){
		this.Scoreform_behavob_visaudmotor=v;
	}
	this.setScoreform_behavob_visaudmotor=setScoreform_behavob_visaudmotor;

	this.Scoreform_behavob_lang=null;


	function getScoreform_behavob_lang() {
		return this.Scoreform_behavob_lang;
	}
	this.getScoreform_behavob_lang=getScoreform_behavob_lang;


	function setScoreform_behavob_lang(v){
		this.Scoreform_behavob_lang=v;
	}
	this.setScoreform_behavob_lang=setScoreform_behavob_lang;

	this.Scoreform_behavob_affectmood=null;


	function getScoreform_behavob_affectmood() {
		return this.Scoreform_behavob_affectmood;
	}
	this.getScoreform_behavob_affectmood=getScoreform_behavob_affectmood;


	function setScoreform_behavob_affectmood(v){
		this.Scoreform_behavob_affectmood=v;
	}
	this.setScoreform_behavob_affectmood=setScoreform_behavob_affectmood;

	this.Scoreform_behavob_unusualbehav=null;


	function getScoreform_behavob_unusualbehav() {
		return this.Scoreform_behavob_unusualbehav;
	}
	this.getScoreform_behavob_unusualbehav=getScoreform_behavob_unusualbehav;


	function setScoreform_behavob_unusualbehav(v){
		this.Scoreform_behavob_unusualbehav=v;
	}
	this.setScoreform_behavob_unusualbehav=setScoreform_behavob_unusualbehav;

	this.Scoreform_behavob_other=null;


	function getScoreform_behavob_other() {
		return this.Scoreform_behavob_other;
	}
	this.getScoreform_behavob_other=getScoreform_behavob_other;


	function setScoreform_behavob_other(v){
		this.Scoreform_behavob_other=v;
	}
	this.setScoreform_behavob_other=setScoreform_behavob_other;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="abstractIQTest"){
				return this.Abstractiqtest ;
			} else 
			if(xmlPath.startsWith("abstractIQTest")){
				xmlPath=xmlPath.substring(14);
				if(xmlPath=="")return this.Abstractiqtest ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Abstractiqtest!=undefined)return this.Abstractiqtest.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="wasi_v_raw"){
				return this.WasiVraw ;
			} else 
			if(xmlPath=="wasi_v_t"){
				return this.WasiVt ;
			} else 
			if(xmlPath=="wasi_v_note"){
				return this.WasiVnote ;
			} else 
			if(xmlPath=="wasi_b_raw"){
				return this.WasiBraw ;
			} else 
			if(xmlPath=="wasi_b_t"){
				return this.WasiBt ;
			} else 
			if(xmlPath=="wasi_b_note"){
				return this.WasiBnote ;
			} else 
			if(xmlPath=="wasi_s_raw"){
				return this.WasiSraw ;
			} else 
			if(xmlPath=="wasi_s_t"){
				return this.WasiSt ;
			} else 
			if(xmlPath=="wasi_s_note"){
				return this.WasiSnote ;
			} else 
			if(xmlPath=="wasi_m_raw"){
				return this.WasiMraw ;
			} else 
			if(xmlPath=="wasi_m_t"){
				return this.WasiMt ;
			} else 
			if(xmlPath=="wasi_m_note"){
				return this.WasiMnote ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab1/response"){
				return this.Scoreform_section1_vocab1_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab1/score"){
				return this.Scoreform_section1_vocab1_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab2/response"){
				return this.Scoreform_section1_vocab2_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab2/score"){
				return this.Scoreform_section1_vocab2_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab3/response"){
				return this.Scoreform_section1_vocab3_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab3/score"){
				return this.Scoreform_section1_vocab3_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab4/response"){
				return this.Scoreform_section1_vocab4_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab4/score"){
				return this.Scoreform_section1_vocab4_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab5/response"){
				return this.Scoreform_section1_vocab5_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab5/score"){
				return this.Scoreform_section1_vocab5_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab6/response"){
				return this.Scoreform_section1_vocab6_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab6/score"){
				return this.Scoreform_section1_vocab6_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab7/response"){
				return this.Scoreform_section1_vocab7_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab7/score"){
				return this.Scoreform_section1_vocab7_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab8/response"){
				return this.Scoreform_section1_vocab8_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab8/score"){
				return this.Scoreform_section1_vocab8_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab9/response"){
				return this.Scoreform_section1_vocab9_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab9/score"){
				return this.Scoreform_section1_vocab9_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab10/response"){
				return this.Scoreform_section1_vocab10_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab10/score"){
				return this.Scoreform_section1_vocab10_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab11/response"){
				return this.Scoreform_section1_vocab11_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab11/score"){
				return this.Scoreform_section1_vocab11_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab12/response"){
				return this.Scoreform_section1_vocab12_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab12/score"){
				return this.Scoreform_section1_vocab12_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab13/response"){
				return this.Scoreform_section1_vocab13_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab13/score"){
				return this.Scoreform_section1_vocab13_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab14/response"){
				return this.Scoreform_section1_vocab14_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab14/score"){
				return this.Scoreform_section1_vocab14_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab15/response"){
				return this.Scoreform_section1_vocab15_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab15/score"){
				return this.Scoreform_section1_vocab15_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab16/response"){
				return this.Scoreform_section1_vocab16_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab16/score"){
				return this.Scoreform_section1_vocab16_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab17/response"){
				return this.Scoreform_section1_vocab17_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab17/score"){
				return this.Scoreform_section1_vocab17_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab18/response"){
				return this.Scoreform_section1_vocab18_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab18/score"){
				return this.Scoreform_section1_vocab18_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab19/response"){
				return this.Scoreform_section1_vocab19_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab19/score"){
				return this.Scoreform_section1_vocab19_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab20/response"){
				return this.Scoreform_section1_vocab20_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab20/score"){
				return this.Scoreform_section1_vocab20_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab21/response"){
				return this.Scoreform_section1_vocab21_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab21/score"){
				return this.Scoreform_section1_vocab21_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab22/response"){
				return this.Scoreform_section1_vocab22_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab22/score"){
				return this.Scoreform_section1_vocab22_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab23/response"){
				return this.Scoreform_section1_vocab23_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab23/score"){
				return this.Scoreform_section1_vocab23_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab24/response"){
				return this.Scoreform_section1_vocab24_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab24/score"){
				return this.Scoreform_section1_vocab24_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab25/response"){
				return this.Scoreform_section1_vocab25_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab25/score"){
				return this.Scoreform_section1_vocab25_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab26/response"){
				return this.Scoreform_section1_vocab26_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab26/score"){
				return this.Scoreform_section1_vocab26_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab27/response"){
				return this.Scoreform_section1_vocab27_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab27/score"){
				return this.Scoreform_section1_vocab27_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab28/response"){
				return this.Scoreform_section1_vocab28_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab28/score"){
				return this.Scoreform_section1_vocab28_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab29/response"){
				return this.Scoreform_section1_vocab29_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab29/score"){
				return this.Scoreform_section1_vocab29_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab30/response"){
				return this.Scoreform_section1_vocab30_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab30/score"){
				return this.Scoreform_section1_vocab30_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab31/response"){
				return this.Scoreform_section1_vocab31_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab31/score"){
				return this.Scoreform_section1_vocab31_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab32/response"){
				return this.Scoreform_section1_vocab32_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab32/score"){
				return this.Scoreform_section1_vocab32_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab33/response"){
				return this.Scoreform_section1_vocab33_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab33/score"){
				return this.Scoreform_section1_vocab33_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab34/response"){
				return this.Scoreform_section1_vocab34_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab34/score"){
				return this.Scoreform_section1_vocab34_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab35/response"){
				return this.Scoreform_section1_vocab35_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab35/score"){
				return this.Scoreform_section1_vocab35_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab36/response"){
				return this.Scoreform_section1_vocab36_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab36/score"){
				return this.Scoreform_section1_vocab36_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab37/response"){
				return this.Scoreform_section1_vocab37_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab37/score"){
				return this.Scoreform_section1_vocab37_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab38/response"){
				return this.Scoreform_section1_vocab38_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab38/score"){
				return this.Scoreform_section1_vocab38_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab39/response"){
				return this.Scoreform_section1_vocab39_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab39/score"){
				return this.Scoreform_section1_vocab39_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab40/response"){
				return this.Scoreform_section1_vocab40_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab40/score"){
				return this.Scoreform_section1_vocab40_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab41/response"){
				return this.Scoreform_section1_vocab41_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab41/score"){
				return this.Scoreform_section1_vocab41_score ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab42/response"){
				return this.Scoreform_section1_vocab42_response ;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab42/score"){
				return this.Scoreform_section1_vocab42_score ;
			} else 
			if(xmlPath=="ScoreForm/section2/block1/time"){
				return this.Scoreform_section2_block1_time ;
			} else 
			if(xmlPath=="ScoreForm/section2/block1/correct"){
				return this.Scoreform_section2_block1_correct ;
			} else 
			if(xmlPath=="ScoreForm/section2/block1/score"){
				return this.Scoreform_section2_block1_score ;
			} else 
			if(xmlPath=="ScoreForm/section2/block2/time"){
				return this.Scoreform_section2_block2_time ;
			} else 
			if(xmlPath=="ScoreForm/section2/block2/correct"){
				return this.Scoreform_section2_block2_correct ;
			} else 
			if(xmlPath=="ScoreForm/section2/block2/score"){
				return this.Scoreform_section2_block2_score ;
			} else 
			if(xmlPath=="ScoreForm/section2/block3/time"){
				return this.Scoreform_section2_block3_time ;
			} else 
			if(xmlPath=="ScoreForm/section2/block3/correct"){
				return this.Scoreform_section2_block3_correct ;
			} else 
			if(xmlPath=="ScoreForm/section2/block3/score"){
				return this.Scoreform_section2_block3_score ;
			} else 
			if(xmlPath=="ScoreForm/section2/block4/time"){
				return this.Scoreform_section2_block4_time ;
			} else 
			if(xmlPath=="ScoreForm/section2/block4/correct"){
				return this.Scoreform_section2_block4_correct ;
			} else 
			if(xmlPath=="ScoreForm/section2/block4/score"){
				return this.Scoreform_section2_block4_score ;
			} else 
			if(xmlPath=="ScoreForm/section2/block5/time"){
				return this.Scoreform_section2_block5_time ;
			} else 
			if(xmlPath=="ScoreForm/section2/block5/correct"){
				return this.Scoreform_section2_block5_correct ;
			} else 
			if(xmlPath=="ScoreForm/section2/block5/score"){
				return this.Scoreform_section2_block5_score ;
			} else 
			if(xmlPath=="ScoreForm/section2/block6/time"){
				return this.Scoreform_section2_block6_time ;
			} else 
			if(xmlPath=="ScoreForm/section2/block6/correct"){
				return this.Scoreform_section2_block6_correct ;
			} else 
			if(xmlPath=="ScoreForm/section2/block6/score"){
				return this.Scoreform_section2_block6_score ;
			} else 
			if(xmlPath=="ScoreForm/section2/block7/time"){
				return this.Scoreform_section2_block7_time ;
			} else 
			if(xmlPath=="ScoreForm/section2/block7/correct"){
				return this.Scoreform_section2_block7_correct ;
			} else 
			if(xmlPath=="ScoreForm/section2/block7/score"){
				return this.Scoreform_section2_block7_score ;
			} else 
			if(xmlPath=="ScoreForm/section2/block8/time"){
				return this.Scoreform_section2_block8_time ;
			} else 
			if(xmlPath=="ScoreForm/section2/block8/correct"){
				return this.Scoreform_section2_block8_correct ;
			} else 
			if(xmlPath=="ScoreForm/section2/block8/score"){
				return this.Scoreform_section2_block8_score ;
			} else 
			if(xmlPath=="ScoreForm/section2/block9/time"){
				return this.Scoreform_section2_block9_time ;
			} else 
			if(xmlPath=="ScoreForm/section2/block9/correct"){
				return this.Scoreform_section2_block9_correct ;
			} else 
			if(xmlPath=="ScoreForm/section2/block9/score"){
				return this.Scoreform_section2_block9_score ;
			} else 
			if(xmlPath=="ScoreForm/section2/block10/time"){
				return this.Scoreform_section2_block10_time ;
			} else 
			if(xmlPath=="ScoreForm/section2/block10/correct"){
				return this.Scoreform_section2_block10_correct ;
			} else 
			if(xmlPath=="ScoreForm/section2/block10/score"){
				return this.Scoreform_section2_block10_score ;
			} else 
			if(xmlPath=="ScoreForm/section2/block11/time"){
				return this.Scoreform_section2_block11_time ;
			} else 
			if(xmlPath=="ScoreForm/section2/block11/correct"){
				return this.Scoreform_section2_block11_correct ;
			} else 
			if(xmlPath=="ScoreForm/section2/block11/score"){
				return this.Scoreform_section2_block11_score ;
			} else 
			if(xmlPath=="ScoreForm/section2/block12/time"){
				return this.Scoreform_section2_block12_time ;
			} else 
			if(xmlPath=="ScoreForm/section2/block12/correct"){
				return this.Scoreform_section2_block12_correct ;
			} else 
			if(xmlPath=="ScoreForm/section2/block12/score"){
				return this.Scoreform_section2_block12_score ;
			} else 
			if(xmlPath=="ScoreForm/section2/block13/time"){
				return this.Scoreform_section2_block13_time ;
			} else 
			if(xmlPath=="ScoreForm/section2/block13/correct"){
				return this.Scoreform_section2_block13_correct ;
			} else 
			if(xmlPath=="ScoreForm/section2/block13/score"){
				return this.Scoreform_section2_block13_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar1/response"){
				return this.Scoreform_section3_similar1_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar1/score"){
				return this.Scoreform_section3_similar1_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar2/response"){
				return this.Scoreform_section3_similar2_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar2/score"){
				return this.Scoreform_section3_similar2_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar3/response"){
				return this.Scoreform_section3_similar3_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar3/score"){
				return this.Scoreform_section3_similar3_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar4/response"){
				return this.Scoreform_section3_similar4_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar4/score"){
				return this.Scoreform_section3_similar4_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar5/response"){
				return this.Scoreform_section3_similar5_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar5/score"){
				return this.Scoreform_section3_similar5_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar6/response"){
				return this.Scoreform_section3_similar6_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar6/score"){
				return this.Scoreform_section3_similar6_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar7/response"){
				return this.Scoreform_section3_similar7_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar7/score"){
				return this.Scoreform_section3_similar7_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar8/response"){
				return this.Scoreform_section3_similar8_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar8/score"){
				return this.Scoreform_section3_similar8_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar9/response"){
				return this.Scoreform_section3_similar9_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar9/score"){
				return this.Scoreform_section3_similar9_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar10/response"){
				return this.Scoreform_section3_similar10_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar10/score"){
				return this.Scoreform_section3_similar10_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar11/response"){
				return this.Scoreform_section3_similar11_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar11/score"){
				return this.Scoreform_section3_similar11_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar12/response"){
				return this.Scoreform_section3_similar12_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar12/score"){
				return this.Scoreform_section3_similar12_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar13/response"){
				return this.Scoreform_section3_similar13_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar13/score"){
				return this.Scoreform_section3_similar13_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar14/response"){
				return this.Scoreform_section3_similar14_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar14/score"){
				return this.Scoreform_section3_similar14_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar15/response"){
				return this.Scoreform_section3_similar15_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar15/score"){
				return this.Scoreform_section3_similar15_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar16/response"){
				return this.Scoreform_section3_similar16_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar16/score"){
				return this.Scoreform_section3_similar16_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar17/response"){
				return this.Scoreform_section3_similar17_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar17/score"){
				return this.Scoreform_section3_similar17_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar18/response"){
				return this.Scoreform_section3_similar18_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar18/score"){
				return this.Scoreform_section3_similar18_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar19/response"){
				return this.Scoreform_section3_similar19_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar19/score"){
				return this.Scoreform_section3_similar19_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar20/response"){
				return this.Scoreform_section3_similar20_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar20/score"){
				return this.Scoreform_section3_similar20_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar21/response"){
				return this.Scoreform_section3_similar21_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar21/score"){
				return this.Scoreform_section3_similar21_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar22/response"){
				return this.Scoreform_section3_similar22_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar22/score"){
				return this.Scoreform_section3_similar22_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar23/response"){
				return this.Scoreform_section3_similar23_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar23/score"){
				return this.Scoreform_section3_similar23_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar24/response"){
				return this.Scoreform_section3_similar24_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar24/score"){
				return this.Scoreform_section3_similar24_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar25/response"){
				return this.Scoreform_section3_similar25_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar25/score"){
				return this.Scoreform_section3_similar25_score ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar26/response"){
				return this.Scoreform_section3_similar26_response ;
			} else 
			if(xmlPath=="ScoreForm/section3/similar26/score"){
				return this.Scoreform_section3_similar26_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrixA/response"){
				return this.Scoreform_section4_matrixa_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrixA/score"){
				return this.Scoreform_section4_matrixa_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrixB/response"){
				return this.Scoreform_section4_matrixb_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrixB/score"){
				return this.Scoreform_section4_matrixb_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix1/response"){
				return this.Scoreform_section4_matrix1_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix1/score"){
				return this.Scoreform_section4_matrix1_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix2/response"){
				return this.Scoreform_section4_matrix2_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix2/score"){
				return this.Scoreform_section4_matrix2_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix3/response"){
				return this.Scoreform_section4_matrix3_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix3/score"){
				return this.Scoreform_section4_matrix3_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix4/response"){
				return this.Scoreform_section4_matrix4_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix4/score"){
				return this.Scoreform_section4_matrix4_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix5/response"){
				return this.Scoreform_section4_matrix5_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix5/score"){
				return this.Scoreform_section4_matrix5_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix6/response"){
				return this.Scoreform_section4_matrix6_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix6/score"){
				return this.Scoreform_section4_matrix6_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix7/response"){
				return this.Scoreform_section4_matrix7_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix7/score"){
				return this.Scoreform_section4_matrix7_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix8/response"){
				return this.Scoreform_section4_matrix8_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix8/score"){
				return this.Scoreform_section4_matrix8_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix9/response"){
				return this.Scoreform_section4_matrix9_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix9/score"){
				return this.Scoreform_section4_matrix9_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix10/response"){
				return this.Scoreform_section4_matrix10_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix10/score"){
				return this.Scoreform_section4_matrix10_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix11/response"){
				return this.Scoreform_section4_matrix11_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix11/score"){
				return this.Scoreform_section4_matrix11_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix12/response"){
				return this.Scoreform_section4_matrix12_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix12/score"){
				return this.Scoreform_section4_matrix12_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix13/response"){
				return this.Scoreform_section4_matrix13_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix13/score"){
				return this.Scoreform_section4_matrix13_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix14/response"){
				return this.Scoreform_section4_matrix14_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix14/score"){
				return this.Scoreform_section4_matrix14_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix15/response"){
				return this.Scoreform_section4_matrix15_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix15/score"){
				return this.Scoreform_section4_matrix15_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix16/response"){
				return this.Scoreform_section4_matrix16_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix16/score"){
				return this.Scoreform_section4_matrix16_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix17/response"){
				return this.Scoreform_section4_matrix17_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix17/score"){
				return this.Scoreform_section4_matrix17_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix18/response"){
				return this.Scoreform_section4_matrix18_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix18/score"){
				return this.Scoreform_section4_matrix18_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix19/response"){
				return this.Scoreform_section4_matrix19_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix19/score"){
				return this.Scoreform_section4_matrix19_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix20/response"){
				return this.Scoreform_section4_matrix20_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix20/score"){
				return this.Scoreform_section4_matrix20_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix21/response"){
				return this.Scoreform_section4_matrix21_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix21/score"){
				return this.Scoreform_section4_matrix21_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix22/response"){
				return this.Scoreform_section4_matrix22_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix22/score"){
				return this.Scoreform_section4_matrix22_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix23/response"){
				return this.Scoreform_section4_matrix23_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix23/score"){
				return this.Scoreform_section4_matrix23_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix24/response"){
				return this.Scoreform_section4_matrix24_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix24/score"){
				return this.Scoreform_section4_matrix24_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix25/response"){
				return this.Scoreform_section4_matrix25_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix25/score"){
				return this.Scoreform_section4_matrix25_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix26/response"){
				return this.Scoreform_section4_matrix26_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix26/score"){
				return this.Scoreform_section4_matrix26_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix27/response"){
				return this.Scoreform_section4_matrix27_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix27/score"){
				return this.Scoreform_section4_matrix27_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix28/response"){
				return this.Scoreform_section4_matrix28_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix28/score"){
				return this.Scoreform_section4_matrix28_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix29/response"){
				return this.Scoreform_section4_matrix29_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix29/score"){
				return this.Scoreform_section4_matrix29_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix30/response"){
				return this.Scoreform_section4_matrix30_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix30/score"){
				return this.Scoreform_section4_matrix30_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix31/response"){
				return this.Scoreform_section4_matrix31_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix31/score"){
				return this.Scoreform_section4_matrix31_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix32/response"){
				return this.Scoreform_section4_matrix32_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix32/score"){
				return this.Scoreform_section4_matrix32_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix33/response"){
				return this.Scoreform_section4_matrix33_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix33/score"){
				return this.Scoreform_section4_matrix33_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix34/response"){
				return this.Scoreform_section4_matrix34_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix34/score"){
				return this.Scoreform_section4_matrix34_score ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix35/response"){
				return this.Scoreform_section4_matrix35_response ;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix35/score"){
				return this.Scoreform_section4_matrix35_score ;
			} else 
			if(xmlPath=="ScoreForm/behavOb/reasonRefer"){
				return this.Scoreform_behavob_reasonrefer ;
			} else 
			if(xmlPath=="ScoreForm/behavOb/attitude"){
				return this.Scoreform_behavob_attitude ;
			} else 
			if(xmlPath=="ScoreForm/behavOb/physAppear"){
				return this.Scoreform_behavob_physappear ;
			} else 
			if(xmlPath=="ScoreForm/behavOb/attention"){
				return this.Scoreform_behavob_attention ;
			} else 
			if(xmlPath=="ScoreForm/behavOb/visAudMotor"){
				return this.Scoreform_behavob_visaudmotor ;
			} else 
			if(xmlPath=="ScoreForm/behavOb/lang"){
				return this.Scoreform_behavob_lang ;
			} else 
			if(xmlPath=="ScoreForm/behavOb/affectMood"){
				return this.Scoreform_behavob_affectmood ;
			} else 
			if(xmlPath=="ScoreForm/behavOb/unusualBehav"){
				return this.Scoreform_behavob_unusualbehav ;
			} else 
			if(xmlPath=="ScoreForm/behavOb/other"){
				return this.Scoreform_behavob_other ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="abstractIQTest"){
				this.Abstractiqtest=value;
			} else 
			if(xmlPath.startsWith("abstractIQTest")){
				xmlPath=xmlPath.substring(14);
				if(xmlPath=="")return this.Abstractiqtest ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Abstractiqtest!=undefined){
					this.Abstractiqtest.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Abstractiqtest= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Abstractiqtest= instanciateObject("iq:abstractIQTest");//omUtils.js
						}
						if(options && options.where)this.Abstractiqtest.setProperty(options.where.field,options.where.value);
						this.Abstractiqtest.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="wasi_v_raw"){
				this.WasiVraw=value;
			} else 
			if(xmlPath=="wasi_v_t"){
				this.WasiVt=value;
			} else 
			if(xmlPath=="wasi_v_note"){
				this.WasiVnote=value;
			} else 
			if(xmlPath=="wasi_b_raw"){
				this.WasiBraw=value;
			} else 
			if(xmlPath=="wasi_b_t"){
				this.WasiBt=value;
			} else 
			if(xmlPath=="wasi_b_note"){
				this.WasiBnote=value;
			} else 
			if(xmlPath=="wasi_s_raw"){
				this.WasiSraw=value;
			} else 
			if(xmlPath=="wasi_s_t"){
				this.WasiSt=value;
			} else 
			if(xmlPath=="wasi_s_note"){
				this.WasiSnote=value;
			} else 
			if(xmlPath=="wasi_m_raw"){
				this.WasiMraw=value;
			} else 
			if(xmlPath=="wasi_m_t"){
				this.WasiMt=value;
			} else 
			if(xmlPath=="wasi_m_note"){
				this.WasiMnote=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab1/response"){
				this.Scoreform_section1_vocab1_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab1/score"){
				this.Scoreform_section1_vocab1_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab2/response"){
				this.Scoreform_section1_vocab2_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab2/score"){
				this.Scoreform_section1_vocab2_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab3/response"){
				this.Scoreform_section1_vocab3_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab3/score"){
				this.Scoreform_section1_vocab3_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab4/response"){
				this.Scoreform_section1_vocab4_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab4/score"){
				this.Scoreform_section1_vocab4_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab5/response"){
				this.Scoreform_section1_vocab5_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab5/score"){
				this.Scoreform_section1_vocab5_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab6/response"){
				this.Scoreform_section1_vocab6_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab6/score"){
				this.Scoreform_section1_vocab6_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab7/response"){
				this.Scoreform_section1_vocab7_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab7/score"){
				this.Scoreform_section1_vocab7_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab8/response"){
				this.Scoreform_section1_vocab8_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab8/score"){
				this.Scoreform_section1_vocab8_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab9/response"){
				this.Scoreform_section1_vocab9_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab9/score"){
				this.Scoreform_section1_vocab9_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab10/response"){
				this.Scoreform_section1_vocab10_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab10/score"){
				this.Scoreform_section1_vocab10_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab11/response"){
				this.Scoreform_section1_vocab11_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab11/score"){
				this.Scoreform_section1_vocab11_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab12/response"){
				this.Scoreform_section1_vocab12_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab12/score"){
				this.Scoreform_section1_vocab12_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab13/response"){
				this.Scoreform_section1_vocab13_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab13/score"){
				this.Scoreform_section1_vocab13_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab14/response"){
				this.Scoreform_section1_vocab14_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab14/score"){
				this.Scoreform_section1_vocab14_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab15/response"){
				this.Scoreform_section1_vocab15_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab15/score"){
				this.Scoreform_section1_vocab15_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab16/response"){
				this.Scoreform_section1_vocab16_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab16/score"){
				this.Scoreform_section1_vocab16_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab17/response"){
				this.Scoreform_section1_vocab17_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab17/score"){
				this.Scoreform_section1_vocab17_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab18/response"){
				this.Scoreform_section1_vocab18_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab18/score"){
				this.Scoreform_section1_vocab18_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab19/response"){
				this.Scoreform_section1_vocab19_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab19/score"){
				this.Scoreform_section1_vocab19_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab20/response"){
				this.Scoreform_section1_vocab20_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab20/score"){
				this.Scoreform_section1_vocab20_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab21/response"){
				this.Scoreform_section1_vocab21_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab21/score"){
				this.Scoreform_section1_vocab21_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab22/response"){
				this.Scoreform_section1_vocab22_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab22/score"){
				this.Scoreform_section1_vocab22_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab23/response"){
				this.Scoreform_section1_vocab23_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab23/score"){
				this.Scoreform_section1_vocab23_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab24/response"){
				this.Scoreform_section1_vocab24_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab24/score"){
				this.Scoreform_section1_vocab24_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab25/response"){
				this.Scoreform_section1_vocab25_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab25/score"){
				this.Scoreform_section1_vocab25_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab26/response"){
				this.Scoreform_section1_vocab26_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab26/score"){
				this.Scoreform_section1_vocab26_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab27/response"){
				this.Scoreform_section1_vocab27_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab27/score"){
				this.Scoreform_section1_vocab27_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab28/response"){
				this.Scoreform_section1_vocab28_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab28/score"){
				this.Scoreform_section1_vocab28_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab29/response"){
				this.Scoreform_section1_vocab29_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab29/score"){
				this.Scoreform_section1_vocab29_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab30/response"){
				this.Scoreform_section1_vocab30_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab30/score"){
				this.Scoreform_section1_vocab30_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab31/response"){
				this.Scoreform_section1_vocab31_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab31/score"){
				this.Scoreform_section1_vocab31_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab32/response"){
				this.Scoreform_section1_vocab32_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab32/score"){
				this.Scoreform_section1_vocab32_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab33/response"){
				this.Scoreform_section1_vocab33_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab33/score"){
				this.Scoreform_section1_vocab33_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab34/response"){
				this.Scoreform_section1_vocab34_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab34/score"){
				this.Scoreform_section1_vocab34_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab35/response"){
				this.Scoreform_section1_vocab35_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab35/score"){
				this.Scoreform_section1_vocab35_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab36/response"){
				this.Scoreform_section1_vocab36_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab36/score"){
				this.Scoreform_section1_vocab36_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab37/response"){
				this.Scoreform_section1_vocab37_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab37/score"){
				this.Scoreform_section1_vocab37_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab38/response"){
				this.Scoreform_section1_vocab38_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab38/score"){
				this.Scoreform_section1_vocab38_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab39/response"){
				this.Scoreform_section1_vocab39_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab39/score"){
				this.Scoreform_section1_vocab39_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab40/response"){
				this.Scoreform_section1_vocab40_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab40/score"){
				this.Scoreform_section1_vocab40_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab41/response"){
				this.Scoreform_section1_vocab41_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab41/score"){
				this.Scoreform_section1_vocab41_score=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab42/response"){
				this.Scoreform_section1_vocab42_response=value;
			} else 
			if(xmlPath=="ScoreForm/section1/vocab42/score"){
				this.Scoreform_section1_vocab42_score=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block1/time"){
				this.Scoreform_section2_block1_time=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block1/correct"){
				this.Scoreform_section2_block1_correct=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block1/score"){
				this.Scoreform_section2_block1_score=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block2/time"){
				this.Scoreform_section2_block2_time=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block2/correct"){
				this.Scoreform_section2_block2_correct=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block2/score"){
				this.Scoreform_section2_block2_score=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block3/time"){
				this.Scoreform_section2_block3_time=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block3/correct"){
				this.Scoreform_section2_block3_correct=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block3/score"){
				this.Scoreform_section2_block3_score=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block4/time"){
				this.Scoreform_section2_block4_time=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block4/correct"){
				this.Scoreform_section2_block4_correct=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block4/score"){
				this.Scoreform_section2_block4_score=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block5/time"){
				this.Scoreform_section2_block5_time=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block5/correct"){
				this.Scoreform_section2_block5_correct=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block5/score"){
				this.Scoreform_section2_block5_score=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block6/time"){
				this.Scoreform_section2_block6_time=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block6/correct"){
				this.Scoreform_section2_block6_correct=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block6/score"){
				this.Scoreform_section2_block6_score=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block7/time"){
				this.Scoreform_section2_block7_time=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block7/correct"){
				this.Scoreform_section2_block7_correct=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block7/score"){
				this.Scoreform_section2_block7_score=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block8/time"){
				this.Scoreform_section2_block8_time=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block8/correct"){
				this.Scoreform_section2_block8_correct=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block8/score"){
				this.Scoreform_section2_block8_score=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block9/time"){
				this.Scoreform_section2_block9_time=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block9/correct"){
				this.Scoreform_section2_block9_correct=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block9/score"){
				this.Scoreform_section2_block9_score=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block10/time"){
				this.Scoreform_section2_block10_time=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block10/correct"){
				this.Scoreform_section2_block10_correct=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block10/score"){
				this.Scoreform_section2_block10_score=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block11/time"){
				this.Scoreform_section2_block11_time=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block11/correct"){
				this.Scoreform_section2_block11_correct=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block11/score"){
				this.Scoreform_section2_block11_score=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block12/time"){
				this.Scoreform_section2_block12_time=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block12/correct"){
				this.Scoreform_section2_block12_correct=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block12/score"){
				this.Scoreform_section2_block12_score=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block13/time"){
				this.Scoreform_section2_block13_time=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block13/correct"){
				this.Scoreform_section2_block13_correct=value;
			} else 
			if(xmlPath=="ScoreForm/section2/block13/score"){
				this.Scoreform_section2_block13_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar1/response"){
				this.Scoreform_section3_similar1_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar1/score"){
				this.Scoreform_section3_similar1_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar2/response"){
				this.Scoreform_section3_similar2_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar2/score"){
				this.Scoreform_section3_similar2_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar3/response"){
				this.Scoreform_section3_similar3_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar3/score"){
				this.Scoreform_section3_similar3_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar4/response"){
				this.Scoreform_section3_similar4_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar4/score"){
				this.Scoreform_section3_similar4_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar5/response"){
				this.Scoreform_section3_similar5_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar5/score"){
				this.Scoreform_section3_similar5_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar6/response"){
				this.Scoreform_section3_similar6_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar6/score"){
				this.Scoreform_section3_similar6_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar7/response"){
				this.Scoreform_section3_similar7_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar7/score"){
				this.Scoreform_section3_similar7_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar8/response"){
				this.Scoreform_section3_similar8_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar8/score"){
				this.Scoreform_section3_similar8_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar9/response"){
				this.Scoreform_section3_similar9_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar9/score"){
				this.Scoreform_section3_similar9_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar10/response"){
				this.Scoreform_section3_similar10_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar10/score"){
				this.Scoreform_section3_similar10_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar11/response"){
				this.Scoreform_section3_similar11_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar11/score"){
				this.Scoreform_section3_similar11_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar12/response"){
				this.Scoreform_section3_similar12_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar12/score"){
				this.Scoreform_section3_similar12_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar13/response"){
				this.Scoreform_section3_similar13_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar13/score"){
				this.Scoreform_section3_similar13_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar14/response"){
				this.Scoreform_section3_similar14_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar14/score"){
				this.Scoreform_section3_similar14_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar15/response"){
				this.Scoreform_section3_similar15_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar15/score"){
				this.Scoreform_section3_similar15_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar16/response"){
				this.Scoreform_section3_similar16_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar16/score"){
				this.Scoreform_section3_similar16_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar17/response"){
				this.Scoreform_section3_similar17_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar17/score"){
				this.Scoreform_section3_similar17_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar18/response"){
				this.Scoreform_section3_similar18_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar18/score"){
				this.Scoreform_section3_similar18_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar19/response"){
				this.Scoreform_section3_similar19_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar19/score"){
				this.Scoreform_section3_similar19_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar20/response"){
				this.Scoreform_section3_similar20_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar20/score"){
				this.Scoreform_section3_similar20_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar21/response"){
				this.Scoreform_section3_similar21_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar21/score"){
				this.Scoreform_section3_similar21_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar22/response"){
				this.Scoreform_section3_similar22_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar22/score"){
				this.Scoreform_section3_similar22_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar23/response"){
				this.Scoreform_section3_similar23_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar23/score"){
				this.Scoreform_section3_similar23_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar24/response"){
				this.Scoreform_section3_similar24_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar24/score"){
				this.Scoreform_section3_similar24_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar25/response"){
				this.Scoreform_section3_similar25_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar25/score"){
				this.Scoreform_section3_similar25_score=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar26/response"){
				this.Scoreform_section3_similar26_response=value;
			} else 
			if(xmlPath=="ScoreForm/section3/similar26/score"){
				this.Scoreform_section3_similar26_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrixA/response"){
				this.Scoreform_section4_matrixa_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrixA/score"){
				this.Scoreform_section4_matrixa_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrixB/response"){
				this.Scoreform_section4_matrixb_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrixB/score"){
				this.Scoreform_section4_matrixb_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix1/response"){
				this.Scoreform_section4_matrix1_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix1/score"){
				this.Scoreform_section4_matrix1_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix2/response"){
				this.Scoreform_section4_matrix2_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix2/score"){
				this.Scoreform_section4_matrix2_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix3/response"){
				this.Scoreform_section4_matrix3_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix3/score"){
				this.Scoreform_section4_matrix3_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix4/response"){
				this.Scoreform_section4_matrix4_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix4/score"){
				this.Scoreform_section4_matrix4_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix5/response"){
				this.Scoreform_section4_matrix5_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix5/score"){
				this.Scoreform_section4_matrix5_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix6/response"){
				this.Scoreform_section4_matrix6_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix6/score"){
				this.Scoreform_section4_matrix6_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix7/response"){
				this.Scoreform_section4_matrix7_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix7/score"){
				this.Scoreform_section4_matrix7_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix8/response"){
				this.Scoreform_section4_matrix8_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix8/score"){
				this.Scoreform_section4_matrix8_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix9/response"){
				this.Scoreform_section4_matrix9_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix9/score"){
				this.Scoreform_section4_matrix9_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix10/response"){
				this.Scoreform_section4_matrix10_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix10/score"){
				this.Scoreform_section4_matrix10_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix11/response"){
				this.Scoreform_section4_matrix11_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix11/score"){
				this.Scoreform_section4_matrix11_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix12/response"){
				this.Scoreform_section4_matrix12_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix12/score"){
				this.Scoreform_section4_matrix12_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix13/response"){
				this.Scoreform_section4_matrix13_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix13/score"){
				this.Scoreform_section4_matrix13_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix14/response"){
				this.Scoreform_section4_matrix14_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix14/score"){
				this.Scoreform_section4_matrix14_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix15/response"){
				this.Scoreform_section4_matrix15_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix15/score"){
				this.Scoreform_section4_matrix15_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix16/response"){
				this.Scoreform_section4_matrix16_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix16/score"){
				this.Scoreform_section4_matrix16_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix17/response"){
				this.Scoreform_section4_matrix17_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix17/score"){
				this.Scoreform_section4_matrix17_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix18/response"){
				this.Scoreform_section4_matrix18_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix18/score"){
				this.Scoreform_section4_matrix18_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix19/response"){
				this.Scoreform_section4_matrix19_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix19/score"){
				this.Scoreform_section4_matrix19_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix20/response"){
				this.Scoreform_section4_matrix20_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix20/score"){
				this.Scoreform_section4_matrix20_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix21/response"){
				this.Scoreform_section4_matrix21_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix21/score"){
				this.Scoreform_section4_matrix21_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix22/response"){
				this.Scoreform_section4_matrix22_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix22/score"){
				this.Scoreform_section4_matrix22_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix23/response"){
				this.Scoreform_section4_matrix23_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix23/score"){
				this.Scoreform_section4_matrix23_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix24/response"){
				this.Scoreform_section4_matrix24_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix24/score"){
				this.Scoreform_section4_matrix24_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix25/response"){
				this.Scoreform_section4_matrix25_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix25/score"){
				this.Scoreform_section4_matrix25_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix26/response"){
				this.Scoreform_section4_matrix26_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix26/score"){
				this.Scoreform_section4_matrix26_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix27/response"){
				this.Scoreform_section4_matrix27_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix27/score"){
				this.Scoreform_section4_matrix27_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix28/response"){
				this.Scoreform_section4_matrix28_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix28/score"){
				this.Scoreform_section4_matrix28_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix29/response"){
				this.Scoreform_section4_matrix29_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix29/score"){
				this.Scoreform_section4_matrix29_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix30/response"){
				this.Scoreform_section4_matrix30_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix30/score"){
				this.Scoreform_section4_matrix30_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix31/response"){
				this.Scoreform_section4_matrix31_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix31/score"){
				this.Scoreform_section4_matrix31_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix32/response"){
				this.Scoreform_section4_matrix32_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix32/score"){
				this.Scoreform_section4_matrix32_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix33/response"){
				this.Scoreform_section4_matrix33_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix33/score"){
				this.Scoreform_section4_matrix33_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix34/response"){
				this.Scoreform_section4_matrix34_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix34/score"){
				this.Scoreform_section4_matrix34_score=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix35/response"){
				this.Scoreform_section4_matrix35_response=value;
			} else 
			if(xmlPath=="ScoreForm/section4/matrix35/score"){
				this.Scoreform_section4_matrix35_score=value;
			} else 
			if(xmlPath=="ScoreForm/behavOb/reasonRefer"){
				this.Scoreform_behavob_reasonrefer=value;
			} else 
			if(xmlPath=="ScoreForm/behavOb/attitude"){
				this.Scoreform_behavob_attitude=value;
			} else 
			if(xmlPath=="ScoreForm/behavOb/physAppear"){
				this.Scoreform_behavob_physappear=value;
			} else 
			if(xmlPath=="ScoreForm/behavOb/attention"){
				this.Scoreform_behavob_attention=value;
			} else 
			if(xmlPath=="ScoreForm/behavOb/visAudMotor"){
				this.Scoreform_behavob_visaudmotor=value;
			} else 
			if(xmlPath=="ScoreForm/behavOb/lang"){
				this.Scoreform_behavob_lang=value;
			} else 
			if(xmlPath=="ScoreForm/behavOb/affectMood"){
				this.Scoreform_behavob_affectmood=value;
			} else 
			if(xmlPath=="ScoreForm/behavOb/unusualBehav"){
				this.Scoreform_behavob_unusualbehav=value;
			} else 
			if(xmlPath=="ScoreForm/behavOb/other"){
				this.Scoreform_behavob_other=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="wasi_v_raw"){
			return "field_data";
		}else if (xmlPath=="wasi_v_t"){
			return "field_data";
		}else if (xmlPath=="wasi_v_note"){
			return "field_data";
		}else if (xmlPath=="wasi_b_raw"){
			return "field_data";
		}else if (xmlPath=="wasi_b_t"){
			return "field_data";
		}else if (xmlPath=="wasi_b_note"){
			return "field_data";
		}else if (xmlPath=="wasi_s_raw"){
			return "field_data";
		}else if (xmlPath=="wasi_s_t"){
			return "field_data";
		}else if (xmlPath=="wasi_s_note"){
			return "field_data";
		}else if (xmlPath=="wasi_m_raw"){
			return "field_data";
		}else if (xmlPath=="wasi_m_t"){
			return "field_data";
		}else if (xmlPath=="wasi_m_note"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab1/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab1/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab2/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab2/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab3/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab3/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab4/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab4/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab5/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab5/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab6/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab6/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab7/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab7/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab8/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab8/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab9/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab9/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab10/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab10/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab11/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab11/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab12/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab12/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab13/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab13/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab14/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab14/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab15/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab15/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab16/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab16/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab17/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab17/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab18/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab18/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab19/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab19/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab20/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab20/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab21/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab21/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab22/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab22/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab23/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab23/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab24/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab24/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab25/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab25/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab26/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab26/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab27/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab27/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab28/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab28/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab29/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab29/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab30/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab30/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab31/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab31/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab32/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab32/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab33/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab33/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab34/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab34/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab35/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab35/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab36/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab36/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab37/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab37/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab38/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab38/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab39/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab39/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab40/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab40/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab41/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab41/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section1/vocab42/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section1/vocab42/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block1/time"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block1/correct"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block1/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block2/time"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block2/correct"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block2/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block3/time"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block3/correct"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block3/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block4/time"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block4/correct"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block4/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block5/time"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block5/correct"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block5/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block6/time"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block6/correct"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block6/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block7/time"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block7/correct"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block7/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block8/time"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block8/correct"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block8/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block9/time"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block9/correct"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block9/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block10/time"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block10/correct"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block10/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block11/time"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block11/correct"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block11/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block12/time"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block12/correct"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block12/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block13/time"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block13/correct"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section2/block13/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar1/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar1/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar2/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar2/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar3/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar3/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar4/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar4/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar5/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section3/similar5/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar6/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section3/similar6/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar7/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section3/similar7/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar8/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section3/similar8/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar9/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section3/similar9/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar10/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section3/similar10/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar11/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section3/similar11/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar12/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section3/similar12/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar13/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section3/similar13/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar14/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section3/similar14/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar15/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section3/similar15/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar16/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section3/similar16/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar17/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section3/similar17/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar18/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section3/similar18/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar19/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section3/similar19/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar20/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section3/similar20/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar21/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section3/similar21/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar22/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section3/similar22/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar23/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section3/similar23/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar24/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section3/similar24/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar25/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section3/similar25/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section3/similar26/response"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/section3/similar26/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrixA/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrixA/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrixB/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrixB/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix1/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix1/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix2/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix2/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix3/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix3/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix4/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix4/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix5/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix5/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix6/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix6/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix7/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix7/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix8/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix8/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix9/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix9/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix10/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix10/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix11/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix11/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix12/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix12/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix13/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix13/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix14/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix14/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix15/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix15/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix16/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix16/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix17/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix17/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix18/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix18/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix19/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix19/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix20/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix20/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix21/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix21/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix22/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix22/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix23/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix23/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix24/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix24/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix25/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix25/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix26/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix26/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix27/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix27/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix28/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix28/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix29/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix29/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix30/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix30/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix31/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix31/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix32/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix32/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix33/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix33/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix34/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix34/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix35/response"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/section4/matrix35/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/behavOb/reasonRefer"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/behavOb/attitude"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/behavOb/physAppear"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/behavOb/attention"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/behavOb/visAudMotor"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/behavOb/lang"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/behavOb/affectMood"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/behavOb/unusualBehav"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/behavOb/other"){
			return "field_LONG_DATA";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<iq:wasi1999Data";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</iq:wasi1999Data>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.WasiVraw!=null){
			xmlTxt+="\n<iq:wasi_v_raw";
			xmlTxt+=">";
			xmlTxt+=this.WasiVraw;
			xmlTxt+="</iq:wasi_v_raw>";
		}
		if (this.WasiVt!=null){
			xmlTxt+="\n<iq:wasi_v_t";
			xmlTxt+=">";
			xmlTxt+=this.WasiVt;
			xmlTxt+="</iq:wasi_v_t>";
		}
		if (this.WasiVnote!=null){
			xmlTxt+="\n<iq:wasi_v_note";
			xmlTxt+=">";
			xmlTxt+=this.WasiVnote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:wasi_v_note>";
		}
		if (this.WasiBraw!=null){
			xmlTxt+="\n<iq:wasi_b_raw";
			xmlTxt+=">";
			xmlTxt+=this.WasiBraw;
			xmlTxt+="</iq:wasi_b_raw>";
		}
		if (this.WasiBt!=null){
			xmlTxt+="\n<iq:wasi_b_t";
			xmlTxt+=">";
			xmlTxt+=this.WasiBt;
			xmlTxt+="</iq:wasi_b_t>";
		}
		if (this.WasiBnote!=null){
			xmlTxt+="\n<iq:wasi_b_note";
			xmlTxt+=">";
			xmlTxt+=this.WasiBnote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:wasi_b_note>";
		}
		if (this.WasiSraw!=null){
			xmlTxt+="\n<iq:wasi_s_raw";
			xmlTxt+=">";
			xmlTxt+=this.WasiSraw;
			xmlTxt+="</iq:wasi_s_raw>";
		}
		if (this.WasiSt!=null){
			xmlTxt+="\n<iq:wasi_s_t";
			xmlTxt+=">";
			xmlTxt+=this.WasiSt;
			xmlTxt+="</iq:wasi_s_t>";
		}
		if (this.WasiSnote!=null){
			xmlTxt+="\n<iq:wasi_s_note";
			xmlTxt+=">";
			xmlTxt+=this.WasiSnote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:wasi_s_note>";
		}
		if (this.WasiMraw!=null){
			xmlTxt+="\n<iq:wasi_m_raw";
			xmlTxt+=">";
			xmlTxt+=this.WasiMraw;
			xmlTxt+="</iq:wasi_m_raw>";
		}
		if (this.WasiMt!=null){
			xmlTxt+="\n<iq:wasi_m_t";
			xmlTxt+=">";
			xmlTxt+=this.WasiMt;
			xmlTxt+="</iq:wasi_m_t>";
		}
		if (this.WasiMnote!=null){
			xmlTxt+="\n<iq:wasi_m_note";
			xmlTxt+=">";
			xmlTxt+=this.WasiMnote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:wasi_m_note>";
		}
			var child0=0;
			var att0=0;
			if(this.Scoreform_section1_vocab14_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab23_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab32_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab41_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab8_response!=null)
			child0++;
			if(this.Scoreform_section3_similar23_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab1_score!=null)
			child0++;
			if(this.Scoreform_section3_similar24_score!=null)
			child0++;
			if(this.Scoreform_section4_matrixa_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix9_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab2_score!=null)
			child0++;
			if(this.Scoreform_section2_block12_time!=null)
			child0++;
			if(this.Scoreform_section3_similar25_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix12_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix21_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix30_response!=null)
			child0++;
			if(this.Scoreform_behavob_lang!=null)
			child0++;
			if(this.Scoreform_behavob_reasonrefer!=null)
			child0++;
			if(this.Scoreform_section3_similar21_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix27_score!=null)
			child0++;
			if(this.Scoreform_section3_similar22_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix28_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab15_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab24_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab33_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab42_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab9_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix29_score!=null)
			child0++;
			if(this.Scoreform_section4_matrixb_response!=null)
			child0++;
			if(this.Scoreform_section3_similar1_response!=null)
			child0++;
			if(this.Scoreform_behavob_visaudmotor!=null)
			child0++;
			if(this.Scoreform_section1_vocab17_score!=null)
			child0++;
			if(this.Scoreform_section3_similar10_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix13_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix22_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix31_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix25_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab18_score!=null)
			child0++;
			if(this.Scoreform_section3_similar20_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix26_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab19_score!=null)
			child0++;
			if(this.Scoreform_behavob_unusualbehav!=null)
			child0++;
			if(this.Scoreform_section1_vocab41_score!=null)
			child0++;
			if(this.Scoreform_section2_block3_time!=null)
			child0++;
			if(this.Scoreform_section1_vocab42_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab16_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab25_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab34_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix22_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab15_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix23_score!=null)
			child0++;
			if(this.Scoreform_section2_block7_time!=null)
			child0++;
			if(this.Scoreform_section2_block7_correct!=null)
			child0++;
			if(this.Scoreform_section1_vocab16_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix24_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix1_response!=null)
			child0++;
			if(this.Scoreform_section3_similar2_response!=null)
			child0++;
			if(this.Scoreform_section3_similar11_response!=null)
			child0++;
			if(this.Scoreform_section3_similar20_response!=null)
			child0++;
			if(this.Scoreform_behavob_attention!=null)
			child0++;
			if(this.Scoreform_section4_matrix23_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix32_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix14_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab12_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab40_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix20_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab13_score!=null)
			child0++;
			if(this.Scoreform_section2_block1_correct!=null)
			child0++;
			if(this.Scoreform_section3_similar8_score!=null)
			child0++;
			if(this.Scoreform_behavob_other!=null)
			child0++;
			if(this.Scoreform_section4_matrix21_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab14_score!=null)
			child0++;
			if(this.Scoreform_section3_similar9_score!=null)
			child0++;
			if(this.Scoreform_section2_block4_correct!=null)
			child0++;
			if(this.Scoreform_section1_vocab26_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab35_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab1_response!=null)
			child0++;
			if(this.Scoreform_behavob_affectmood!=null)
			child0++;
			if(this.Scoreform_section1_vocab10_score!=null)
			child0++;
			if(this.Scoreform_section2_block12_correct!=null)
			child0++;
			if(this.Scoreform_section2_block13_time!=null)
			child0++;
			if(this.Scoreform_behavob_physappear!=null)
			child0++;
			if(this.Scoreform_section3_similar5_score!=null)
			child0++;
			if(this.Scoreform_section3_similar3_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab11_score!=null)
			child0++;
			if(this.Scoreform_section3_similar6_score!=null)
			child0++;
			if(this.Scoreform_section3_similar21_response!=null)
			child0++;
			if(this.Scoreform_section3_similar12_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix15_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix24_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix33_response!=null)
			child0++;
			if(this.Scoreform_section3_similar7_score!=null)
			child0++;
			if(this.Scoreform_behavob_attitude!=null)
			child0++;
			if(this.Scoreform_section3_similar3_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab17_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab27_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab36_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab2_response!=null)
			child0++;
			if(this.Scoreform_section3_similar4_score!=null)
			child0++;
			if(this.Scoreform_section2_block11_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix2_response!=null)
			child0++;
			if(this.Scoreform_section2_block12_score!=null)
			child0++;
			if(this.Scoreform_section3_similar4_response!=null)
			child0++;
			if(this.Scoreform_section3_similar22_response!=null)
			child0++;
			if(this.Scoreform_section3_similar13_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix25_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix34_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix16_response!=null)
			child0++;
			if(this.Scoreform_section2_block13_score!=null)
			child0++;
			if(this.Scoreform_section3_similar1_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix35_score!=null)
			child0++;
			if(this.Scoreform_section2_block4_time!=null)
			child0++;
			if(this.Scoreform_section1_vocab28_score!=null)
			child0++;
			if(this.Scoreform_section3_similar2_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab29_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab18_response!=null)
			child0++;
			if(this.Scoreform_section2_block10_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab37_response!=null)
			child0++;
			if(this.Scoreform_section2_block8_time!=null)
			child0++;
			if(this.Scoreform_section4_matrix9_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab25_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix33_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix3_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab26_score!=null)
			child0++;
			if(this.Scoreform_section3_similar5_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix34_score!=null)
			child0++;
			if(this.Scoreform_section3_similar14_response!=null)
			child0++;
			if(this.Scoreform_section3_similar23_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix26_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix35_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab27_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix6_score!=null)
			child0++;
			if(this.Scoreform_section2_block10_time!=null)
			child0++;
			if(this.Scoreform_section2_block5_correct!=null)
			child0++;
			if(this.Scoreform_section4_matrix7_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix30_score!=null)
			child0++;
			if(this.Scoreform_section4_matrixb_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab23_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix8_score!=null)
			child0++;
			if(this.Scoreform_section2_block8_correct!=null)
			child0++;
			if(this.Scoreform_section4_matrix31_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab19_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab28_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab3_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab24_score!=null)
			child0++;
			if(this.Scoreform_section2_block13_correct!=null)
			child0++;
			if(this.Scoreform_section1_vocab38_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix32_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix4_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix4_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab20_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix5_score!=null)
			child0++;
			if(this.Scoreform_section3_similar6_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix17_response!=null)
			child0++;
			if(this.Scoreform_section3_similar15_response!=null)
			child0++;
			if(this.Scoreform_section3_similar24_response!=null)
			child0++;
			if(this.Scoreform_section2_block2_correct!=null)
			child0++;
			if(this.Scoreform_section4_matrix27_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab21_score!=null)
			child0++;
			if(this.Scoreform_section4_matrixa_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab22_score!=null)
			child0++;
			if(this.Scoreform_section3_similar17_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix1_score!=null)
			child0++;
			if(this.Scoreform_section2_block10_correct!=null)
			child0++;
			if(this.Scoreform_section3_similar18_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix2_score!=null)
			child0++;
			if(this.Scoreform_section2_block8_score!=null)
			child0++;
			if(this.Scoreform_section3_similar19_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab29_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab10_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab4_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab39_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix3_score!=null)
			child0++;
			if(this.Scoreform_section2_block9_score!=null)
			child0++;
			if(this.Scoreform_section2_block1_time!=null)
			child0++;
			if(this.Scoreform_section4_matrix5_response!=null)
			child0++;
			if(this.Scoreform_section3_similar15_score!=null)
			child0++;
			if(this.Scoreform_section3_similar7_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix18_response!=null)
			child0++;
			if(this.Scoreform_section3_similar25_response!=null)
			child0++;
			if(this.Scoreform_section3_similar16_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix28_response!=null)
			child0++;
			if(this.Scoreform_section3_similar16_score!=null)
			child0++;
			if(this.Scoreform_section2_block5_time!=null)
			child0++;
			if(this.Scoreform_section2_block6_score!=null)
			child0++;
			if(this.Scoreform_section2_block7_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab38_score!=null)
			child0++;
			if(this.Scoreform_section2_block9_time!=null)
			child0++;
			if(this.Scoreform_section1_vocab39_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab11_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab20_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab5_response!=null)
			child0++;
			if(this.Scoreform_section3_similar13_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix19_score!=null)
			child0++;
			if(this.Scoreform_section2_block3_score!=null)
			child0++;
			if(this.Scoreform_section3_similar14_score!=null)
			child0++;
			if(this.Scoreform_section2_block4_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix6_response!=null)
			child0++;
			if(this.Scoreform_section3_similar8_response!=null)
			child0++;
			if(this.Scoreform_section2_block5_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix19_response!=null)
			child0++;
			if(this.Scoreform_section3_similar26_response!=null)
			child0++;
			if(this.Scoreform_section3_similar17_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab36_score!=null)
			child0++;
			if(this.Scoreform_section3_similar10_score!=null)
			child0++;
			if(this.Scoreform_section2_block11_time!=null)
			child0++;
			if(this.Scoreform_section1_vocab37_score!=null)
			child0++;
			if(this.Scoreform_section3_similar11_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix17_score!=null)
			child0++;
			if(this.Scoreform_section2_block1_score!=null)
			child0++;
			if(this.Scoreform_section3_similar12_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix18_score!=null)
			child0++;
			if(this.Scoreform_section2_block2_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab12_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab33_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab21_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab30_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab6_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab40_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab34_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix14_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab7_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab35_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix7_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix15_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab8_score!=null)
			child0++;
			if(this.Scoreform_section3_similar9_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix10_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix29_response!=null)
			child0++;
			if(this.Scoreform_section2_block6_correct!=null)
			child0++;
			if(this.Scoreform_section3_similar18_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix16_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab9_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab31_score!=null)
			child0++;
			if(this.Scoreform_section2_block9_correct!=null)
			child0++;
			if(this.Scoreform_section1_vocab32_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix12_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab5_score!=null)
			child0++;
			if(this.Scoreform_section2_block2_time!=null)
			child0++;
			if(this.Scoreform_section1_vocab13_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab22_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab31_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab7_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix13_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab6_score!=null)
			child0++;
			if(this.Scoreform_section2_block3_correct!=null)
			child0++;
			if(this.Scoreform_section2_block6_time!=null)
			child0++;
			if(this.Scoreform_section4_matrix8_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix11_response!=null)
			child0++;
			if(this.Scoreform_section4_matrix20_response!=null)
			child0++;
			if(this.Scoreform_section1_vocab30_score!=null)
			child0++;
			if(this.Scoreform_section3_similar19_response!=null)
			child0++;
			if(this.Scoreform_section2_block11_correct!=null)
			child0++;
			if(this.Scoreform_section4_matrix10_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab3_score!=null)
			child0++;
			if(this.Scoreform_section3_similar26_score!=null)
			child0++;
			if(this.Scoreform_section4_matrix11_score!=null)
			child0++;
			if(this.Scoreform_section1_vocab4_score!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<iq:ScoreForm";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child1=0;
			var att1=0;
			if(this.Scoreform_section1_vocab9_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab28_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab4_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab15_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab35_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab17_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab41_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab22_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab19_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab41_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab9_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab3_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab30_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab34_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab32_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab27_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab21_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab34_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab2_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab8_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab21_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab36_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab14_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab39_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab33_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab23_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab38_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab10_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab25_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab26_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab20_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab40_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab12_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab27_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab19_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab7_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab2_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab13_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab14_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab38_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab4_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab16_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab6_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab40_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab25_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab8_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab18_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab1_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab29_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab12_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab32_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab18_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab42_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab6_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab17_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab37_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab31_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab31_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab33_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab24_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab1_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab20_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab35_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab5_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab11_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab22_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab37_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab36_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab24_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab39_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab29_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab23_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab11_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab26_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab13_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab3_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab16_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab10_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab30_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab15_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab5_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab7_response!=null)
			child1++;
			if(this.Scoreform_section1_vocab28_score!=null)
			child1++;
			if(this.Scoreform_section1_vocab42_score!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<iq:section1";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child2=0;
			var att2=0;
			if(this.Scoreform_section1_vocab1_score!=null)
			child2++;
			if(this.Scoreform_section1_vocab1_response!=null)
			child2++;
			if(child2>0 || att2>0){
				xmlTxt+="\n<iq:vocab1";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab1_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab1_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab1_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab1_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab1>";
			}
			}

			var child3=0;
			var att3=0;
			if(this.Scoreform_section1_vocab2_score!=null)
			child3++;
			if(this.Scoreform_section1_vocab2_response!=null)
			child3++;
			if(child3>0 || att3>0){
				xmlTxt+="\n<iq:vocab2";
			if(child3==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab2_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab2_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab2_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab2_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab2>";
			}
			}

			var child4=0;
			var att4=0;
			if(this.Scoreform_section1_vocab3_score!=null)
			child4++;
			if(this.Scoreform_section1_vocab3_response!=null)
			child4++;
			if(child4>0 || att4>0){
				xmlTxt+="\n<iq:vocab3";
			if(child4==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab3_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab3_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab3_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab3_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab3>";
			}
			}

			var child5=0;
			var att5=0;
			if(this.Scoreform_section1_vocab4_score!=null)
			child5++;
			if(this.Scoreform_section1_vocab4_response!=null)
			child5++;
			if(child5>0 || att5>0){
				xmlTxt+="\n<iq:vocab4";
			if(child5==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab4_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab4_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab4_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab4_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab4>";
			}
			}

			var child6=0;
			var att6=0;
			if(this.Scoreform_section1_vocab5_score!=null)
			child6++;
			if(this.Scoreform_section1_vocab5_response!=null)
			child6++;
			if(child6>0 || att6>0){
				xmlTxt+="\n<iq:vocab5";
			if(child6==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab5_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab5_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab5_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab5_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab5>";
			}
			}

			var child7=0;
			var att7=0;
			if(this.Scoreform_section1_vocab6_score!=null)
			child7++;
			if(this.Scoreform_section1_vocab6_response!=null)
			child7++;
			if(child7>0 || att7>0){
				xmlTxt+="\n<iq:vocab6";
			if(child7==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab6_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab6_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab6_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab6_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab6>";
			}
			}

			var child8=0;
			var att8=0;
			if(this.Scoreform_section1_vocab7_response!=null)
			child8++;
			if(this.Scoreform_section1_vocab7_score!=null)
			child8++;
			if(child8>0 || att8>0){
				xmlTxt+="\n<iq:vocab7";
			if(child8==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab7_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab7_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab7_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab7_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab7>";
			}
			}

			var child9=0;
			var att9=0;
			if(this.Scoreform_section1_vocab8_response!=null)
			child9++;
			if(this.Scoreform_section1_vocab8_score!=null)
			child9++;
			if(child9>0 || att9>0){
				xmlTxt+="\n<iq:vocab8";
			if(child9==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab8_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab8_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab8_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab8_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab8>";
			}
			}

			var child10=0;
			var att10=0;
			if(this.Scoreform_section1_vocab9_score!=null)
			child10++;
			if(this.Scoreform_section1_vocab9_response!=null)
			child10++;
			if(child10>0 || att10>0){
				xmlTxt+="\n<iq:vocab9";
			if(child10==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab9_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab9_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab9_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab9_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab9>";
			}
			}

			var child11=0;
			var att11=0;
			if(this.Scoreform_section1_vocab10_response!=null)
			child11++;
			if(this.Scoreform_section1_vocab10_score!=null)
			child11++;
			if(child11>0 || att11>0){
				xmlTxt+="\n<iq:vocab10";
			if(child11==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab10_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab10_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab10_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab10_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab10>";
			}
			}

			var child12=0;
			var att12=0;
			if(this.Scoreform_section1_vocab11_score!=null)
			child12++;
			if(this.Scoreform_section1_vocab11_response!=null)
			child12++;
			if(child12>0 || att12>0){
				xmlTxt+="\n<iq:vocab11";
			if(child12==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab11_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab11_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab11_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab11_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab11>";
			}
			}

			var child13=0;
			var att13=0;
			if(this.Scoreform_section1_vocab12_response!=null)
			child13++;
			if(this.Scoreform_section1_vocab12_score!=null)
			child13++;
			if(child13>0 || att13>0){
				xmlTxt+="\n<iq:vocab12";
			if(child13==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab12_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab12_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab12_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab12_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab12>";
			}
			}

			var child14=0;
			var att14=0;
			if(this.Scoreform_section1_vocab13_response!=null)
			child14++;
			if(this.Scoreform_section1_vocab13_score!=null)
			child14++;
			if(child14>0 || att14>0){
				xmlTxt+="\n<iq:vocab13";
			if(child14==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab13_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab13_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab13_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab13_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab13>";
			}
			}

			var child15=0;
			var att15=0;
			if(this.Scoreform_section1_vocab14_response!=null)
			child15++;
			if(this.Scoreform_section1_vocab14_score!=null)
			child15++;
			if(child15>0 || att15>0){
				xmlTxt+="\n<iq:vocab14";
			if(child15==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab14_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab14_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab14_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab14_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab14>";
			}
			}

			var child16=0;
			var att16=0;
			if(this.Scoreform_section1_vocab15_score!=null)
			child16++;
			if(this.Scoreform_section1_vocab15_response!=null)
			child16++;
			if(child16>0 || att16>0){
				xmlTxt+="\n<iq:vocab15";
			if(child16==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab15_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab15_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab15_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab15_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab15>";
			}
			}

			var child17=0;
			var att17=0;
			if(this.Scoreform_section1_vocab16_score!=null)
			child17++;
			if(this.Scoreform_section1_vocab16_response!=null)
			child17++;
			if(child17>0 || att17>0){
				xmlTxt+="\n<iq:vocab16";
			if(child17==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab16_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab16_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab16_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab16_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab16>";
			}
			}

			var child18=0;
			var att18=0;
			if(this.Scoreform_section1_vocab17_response!=null)
			child18++;
			if(this.Scoreform_section1_vocab17_score!=null)
			child18++;
			if(child18>0 || att18>0){
				xmlTxt+="\n<iq:vocab17";
			if(child18==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab17_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab17_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab17_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab17_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab17>";
			}
			}

			var child19=0;
			var att19=0;
			if(this.Scoreform_section1_vocab18_response!=null)
			child19++;
			if(this.Scoreform_section1_vocab18_score!=null)
			child19++;
			if(child19>0 || att19>0){
				xmlTxt+="\n<iq:vocab18";
			if(child19==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab18_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab18_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab18_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab18_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab18>";
			}
			}

			var child20=0;
			var att20=0;
			if(this.Scoreform_section1_vocab19_score!=null)
			child20++;
			if(this.Scoreform_section1_vocab19_response!=null)
			child20++;
			if(child20>0 || att20>0){
				xmlTxt+="\n<iq:vocab19";
			if(child20==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab19_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab19_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab19_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab19_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab19>";
			}
			}

			var child21=0;
			var att21=0;
			if(this.Scoreform_section1_vocab20_response!=null)
			child21++;
			if(this.Scoreform_section1_vocab20_score!=null)
			child21++;
			if(child21>0 || att21>0){
				xmlTxt+="\n<iq:vocab20";
			if(child21==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab20_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab20_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab20_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab20_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab20>";
			}
			}

			var child22=0;
			var att22=0;
			if(this.Scoreform_section1_vocab21_score!=null)
			child22++;
			if(this.Scoreform_section1_vocab21_response!=null)
			child22++;
			if(child22>0 || att22>0){
				xmlTxt+="\n<iq:vocab21";
			if(child22==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab21_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab21_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab21_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab21_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab21>";
			}
			}

			var child23=0;
			var att23=0;
			if(this.Scoreform_section1_vocab22_score!=null)
			child23++;
			if(this.Scoreform_section1_vocab22_response!=null)
			child23++;
			if(child23>0 || att23>0){
				xmlTxt+="\n<iq:vocab22";
			if(child23==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab22_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab22_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab22_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab22_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab22>";
			}
			}

			var child24=0;
			var att24=0;
			if(this.Scoreform_section1_vocab23_response!=null)
			child24++;
			if(this.Scoreform_section1_vocab23_score!=null)
			child24++;
			if(child24>0 || att24>0){
				xmlTxt+="\n<iq:vocab23";
			if(child24==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab23_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab23_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab23_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab23_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab23>";
			}
			}

			var child25=0;
			var att25=0;
			if(this.Scoreform_section1_vocab24_response!=null)
			child25++;
			if(this.Scoreform_section1_vocab24_score!=null)
			child25++;
			if(child25>0 || att25>0){
				xmlTxt+="\n<iq:vocab24";
			if(child25==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab24_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab24_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab24_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab24_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab24>";
			}
			}

			var child26=0;
			var att26=0;
			if(this.Scoreform_section1_vocab25_score!=null)
			child26++;
			if(this.Scoreform_section1_vocab25_response!=null)
			child26++;
			if(child26>0 || att26>0){
				xmlTxt+="\n<iq:vocab25";
			if(child26==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab25_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab25_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab25_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab25_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab25>";
			}
			}

			var child27=0;
			var att27=0;
			if(this.Scoreform_section1_vocab26_score!=null)
			child27++;
			if(this.Scoreform_section1_vocab26_response!=null)
			child27++;
			if(child27>0 || att27>0){
				xmlTxt+="\n<iq:vocab26";
			if(child27==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab26_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab26_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab26_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab26_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab26>";
			}
			}

			var child28=0;
			var att28=0;
			if(this.Scoreform_section1_vocab27_score!=null)
			child28++;
			if(this.Scoreform_section1_vocab27_response!=null)
			child28++;
			if(child28>0 || att28>0){
				xmlTxt+="\n<iq:vocab27";
			if(child28==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab27_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab27_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab27_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab27_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab27>";
			}
			}

			var child29=0;
			var att29=0;
			if(this.Scoreform_section1_vocab28_response!=null)
			child29++;
			if(this.Scoreform_section1_vocab28_score!=null)
			child29++;
			if(child29>0 || att29>0){
				xmlTxt+="\n<iq:vocab28";
			if(child29==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab28_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab28_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab28_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab28_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab28>";
			}
			}

			var child30=0;
			var att30=0;
			if(this.Scoreform_section1_vocab29_score!=null)
			child30++;
			if(this.Scoreform_section1_vocab29_response!=null)
			child30++;
			if(child30>0 || att30>0){
				xmlTxt+="\n<iq:vocab29";
			if(child30==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab29_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab29_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab29_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab29_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab29>";
			}
			}

			var child31=0;
			var att31=0;
			if(this.Scoreform_section1_vocab30_response!=null)
			child31++;
			if(this.Scoreform_section1_vocab30_score!=null)
			child31++;
			if(child31>0 || att31>0){
				xmlTxt+="\n<iq:vocab30";
			if(child31==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab30_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab30_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab30_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab30_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab30>";
			}
			}

			var child32=0;
			var att32=0;
			if(this.Scoreform_section1_vocab31_score!=null)
			child32++;
			if(this.Scoreform_section1_vocab31_response!=null)
			child32++;
			if(child32>0 || att32>0){
				xmlTxt+="\n<iq:vocab31";
			if(child32==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab31_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab31_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab31_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab31_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab31>";
			}
			}

			var child33=0;
			var att33=0;
			if(this.Scoreform_section1_vocab32_score!=null)
			child33++;
			if(this.Scoreform_section1_vocab32_response!=null)
			child33++;
			if(child33>0 || att33>0){
				xmlTxt+="\n<iq:vocab32";
			if(child33==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab32_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab32_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab32_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab32_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab32>";
			}
			}

			var child34=0;
			var att34=0;
			if(this.Scoreform_section1_vocab33_score!=null)
			child34++;
			if(this.Scoreform_section1_vocab33_response!=null)
			child34++;
			if(child34>0 || att34>0){
				xmlTxt+="\n<iq:vocab33";
			if(child34==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab33_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab33_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab33_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab33_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab33>";
			}
			}

			var child35=0;
			var att35=0;
			if(this.Scoreform_section1_vocab34_response!=null)
			child35++;
			if(this.Scoreform_section1_vocab34_score!=null)
			child35++;
			if(child35>0 || att35>0){
				xmlTxt+="\n<iq:vocab34";
			if(child35==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab34_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab34_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab34_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab34_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab34>";
			}
			}

			var child36=0;
			var att36=0;
			if(this.Scoreform_section1_vocab35_score!=null)
			child36++;
			if(this.Scoreform_section1_vocab35_response!=null)
			child36++;
			if(child36>0 || att36>0){
				xmlTxt+="\n<iq:vocab35";
			if(child36==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab35_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab35_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab35_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab35_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab35>";
			}
			}

			var child37=0;
			var att37=0;
			if(this.Scoreform_section1_vocab36_score!=null)
			child37++;
			if(this.Scoreform_section1_vocab36_response!=null)
			child37++;
			if(child37>0 || att37>0){
				xmlTxt+="\n<iq:vocab36";
			if(child37==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab36_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab36_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab36_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab36_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab36>";
			}
			}

			var child38=0;
			var att38=0;
			if(this.Scoreform_section1_vocab37_response!=null)
			child38++;
			if(this.Scoreform_section1_vocab37_score!=null)
			child38++;
			if(child38>0 || att38>0){
				xmlTxt+="\n<iq:vocab37";
			if(child38==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab37_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab37_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab37_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab37_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab37>";
			}
			}

			var child39=0;
			var att39=0;
			if(this.Scoreform_section1_vocab38_response!=null)
			child39++;
			if(this.Scoreform_section1_vocab38_score!=null)
			child39++;
			if(child39>0 || att39>0){
				xmlTxt+="\n<iq:vocab38";
			if(child39==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab38_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab38_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab38_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab38_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab38>";
			}
			}

			var child40=0;
			var att40=0;
			if(this.Scoreform_section1_vocab39_score!=null)
			child40++;
			if(this.Scoreform_section1_vocab39_response!=null)
			child40++;
			if(child40>0 || att40>0){
				xmlTxt+="\n<iq:vocab39";
			if(child40==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab39_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab39_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab39_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab39_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab39>";
			}
			}

			var child41=0;
			var att41=0;
			if(this.Scoreform_section1_vocab40_score!=null)
			child41++;
			if(this.Scoreform_section1_vocab40_response!=null)
			child41++;
			if(child41>0 || att41>0){
				xmlTxt+="\n<iq:vocab40";
			if(child41==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab40_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab40_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab40_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab40_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab40>";
			}
			}

			var child42=0;
			var att42=0;
			if(this.Scoreform_section1_vocab41_score!=null)
			child42++;
			if(this.Scoreform_section1_vocab41_response!=null)
			child42++;
			if(child42>0 || att42>0){
				xmlTxt+="\n<iq:vocab41";
			if(child42==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab41_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab41_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab41_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab41_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab41>";
			}
			}

			var child43=0;
			var att43=0;
			if(this.Scoreform_section1_vocab42_score!=null)
			child43++;
			if(this.Scoreform_section1_vocab42_response!=null)
			child43++;
			if(child43>0 || att43>0){
				xmlTxt+="\n<iq:vocab42";
			if(child43==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section1_vocab42_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab42_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section1_vocab42_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section1_vocab42_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:vocab42>";
			}
			}

				xmlTxt+="\n</iq:section1>";
			}
			}

			var child44=0;
			var att44=0;
			if(this.Scoreform_section2_block1_time!=null)
			child44++;
			if(this.Scoreform_section2_block13_score!=null)
			child44++;
			if(this.Scoreform_section2_block10_score!=null)
			child44++;
			if(this.Scoreform_section2_block3_time!=null)
			child44++;
			if(this.Scoreform_section2_block12_score!=null)
			child44++;
			if(this.Scoreform_section2_block9_correct!=null)
			child44++;
			if(this.Scoreform_section2_block11_score!=null)
			child44++;
			if(this.Scoreform_section2_block5_time!=null)
			child44++;
			if(this.Scoreform_section2_block2_correct!=null)
			child44++;
			if(this.Scoreform_section2_block7_time!=null)
			child44++;
			if(this.Scoreform_section2_block10_time!=null)
			child44++;
			if(this.Scoreform_section2_block9_time!=null)
			child44++;
			if(this.Scoreform_section2_block12_time!=null)
			child44++;
			if(this.Scoreform_section2_block7_correct!=null)
			child44++;
			if(this.Scoreform_section2_block12_correct!=null)
			child44++;
			if(this.Scoreform_section2_block5_correct!=null)
			child44++;
			if(this.Scoreform_section2_block10_correct!=null)
			child44++;
			if(this.Scoreform_section2_block3_correct!=null)
			child44++;
			if(this.Scoreform_section2_block2_time!=null)
			child44++;
			if(this.Scoreform_section2_block4_time!=null)
			child44++;
			if(this.Scoreform_section2_block5_score!=null)
			child44++;
			if(this.Scoreform_section2_block2_score!=null)
			child44++;
			if(this.Scoreform_section2_block6_time!=null)
			child44++;
			if(this.Scoreform_section2_block4_score!=null)
			child44++;
			if(this.Scoreform_section2_block1_score!=null)
			child44++;
			if(this.Scoreform_section2_block7_score!=null)
			child44++;
			if(this.Scoreform_section2_block3_score!=null)
			child44++;
			if(this.Scoreform_section2_block9_score!=null)
			child44++;
			if(this.Scoreform_section2_block6_score!=null)
			child44++;
			if(this.Scoreform_section2_block8_score!=null)
			child44++;
			if(this.Scoreform_section2_block8_time!=null)
			child44++;
			if(this.Scoreform_section2_block11_time!=null)
			child44++;
			if(this.Scoreform_section2_block8_correct!=null)
			child44++;
			if(this.Scoreform_section2_block13_time!=null)
			child44++;
			if(this.Scoreform_section2_block1_correct!=null)
			child44++;
			if(this.Scoreform_section2_block13_correct!=null)
			child44++;
			if(this.Scoreform_section2_block6_correct!=null)
			child44++;
			if(this.Scoreform_section2_block11_correct!=null)
			child44++;
			if(this.Scoreform_section2_block4_correct!=null)
			child44++;
			if(child44>0 || att44>0){
				xmlTxt+="\n<iq:section2";
			if(child44==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child45=0;
			var att45=0;
			if(this.Scoreform_section2_block1_score!=null)
			child45++;
			if(this.Scoreform_section2_block1_time!=null)
			child45++;
			if(this.Scoreform_section2_block1_correct!=null)
			child45++;
			if(child45>0 || att45>0){
				xmlTxt+="\n<iq:block1";
			if(child45==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section2_block1_time!=null){
			xmlTxt+="\n<iq:time";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block1_time;
			xmlTxt+="</iq:time>";
		}
		if (this.Scoreform_section2_block1_correct!=null){
			xmlTxt+="\n<iq:correct";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block1_correct;
			xmlTxt+="</iq:correct>";
		}
		if (this.Scoreform_section2_block1_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block1_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:block1>";
			}
			}

			var child46=0;
			var att46=0;
			if(this.Scoreform_section2_block2_time!=null)
			child46++;
			if(this.Scoreform_section2_block2_score!=null)
			child46++;
			if(this.Scoreform_section2_block2_correct!=null)
			child46++;
			if(child46>0 || att46>0){
				xmlTxt+="\n<iq:block2";
			if(child46==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section2_block2_time!=null){
			xmlTxt+="\n<iq:time";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block2_time;
			xmlTxt+="</iq:time>";
		}
		if (this.Scoreform_section2_block2_correct!=null){
			xmlTxt+="\n<iq:correct";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block2_correct;
			xmlTxt+="</iq:correct>";
		}
		if (this.Scoreform_section2_block2_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block2_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:block2>";
			}
			}

			var child47=0;
			var att47=0;
			if(this.Scoreform_section2_block3_score!=null)
			child47++;
			if(this.Scoreform_section2_block3_correct!=null)
			child47++;
			if(this.Scoreform_section2_block3_time!=null)
			child47++;
			if(child47>0 || att47>0){
				xmlTxt+="\n<iq:block3";
			if(child47==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section2_block3_time!=null){
			xmlTxt+="\n<iq:time";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block3_time;
			xmlTxt+="</iq:time>";
		}
		if (this.Scoreform_section2_block3_correct!=null){
			xmlTxt+="\n<iq:correct";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block3_correct;
			xmlTxt+="</iq:correct>";
		}
		if (this.Scoreform_section2_block3_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block3_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:block3>";
			}
			}

			var child48=0;
			var att48=0;
			if(this.Scoreform_section2_block4_correct!=null)
			child48++;
			if(this.Scoreform_section2_block4_score!=null)
			child48++;
			if(this.Scoreform_section2_block4_time!=null)
			child48++;
			if(child48>0 || att48>0){
				xmlTxt+="\n<iq:block4";
			if(child48==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section2_block4_time!=null){
			xmlTxt+="\n<iq:time";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block4_time;
			xmlTxt+="</iq:time>";
		}
		if (this.Scoreform_section2_block4_correct!=null){
			xmlTxt+="\n<iq:correct";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block4_correct;
			xmlTxt+="</iq:correct>";
		}
		if (this.Scoreform_section2_block4_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block4_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:block4>";
			}
			}

			var child49=0;
			var att49=0;
			if(this.Scoreform_section2_block5_score!=null)
			child49++;
			if(this.Scoreform_section2_block5_correct!=null)
			child49++;
			if(this.Scoreform_section2_block5_time!=null)
			child49++;
			if(child49>0 || att49>0){
				xmlTxt+="\n<iq:block5";
			if(child49==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section2_block5_time!=null){
			xmlTxt+="\n<iq:time";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block5_time;
			xmlTxt+="</iq:time>";
		}
		if (this.Scoreform_section2_block5_correct!=null){
			xmlTxt+="\n<iq:correct";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block5_correct;
			xmlTxt+="</iq:correct>";
		}
		if (this.Scoreform_section2_block5_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block5_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:block5>";
			}
			}

			var child50=0;
			var att50=0;
			if(this.Scoreform_section2_block6_score!=null)
			child50++;
			if(this.Scoreform_section2_block6_correct!=null)
			child50++;
			if(this.Scoreform_section2_block6_time!=null)
			child50++;
			if(child50>0 || att50>0){
				xmlTxt+="\n<iq:block6";
			if(child50==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section2_block6_time!=null){
			xmlTxt+="\n<iq:time";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block6_time;
			xmlTxt+="</iq:time>";
		}
		if (this.Scoreform_section2_block6_correct!=null){
			xmlTxt+="\n<iq:correct";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block6_correct;
			xmlTxt+="</iq:correct>";
		}
		if (this.Scoreform_section2_block6_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block6_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:block6>";
			}
			}

			var child51=0;
			var att51=0;
			if(this.Scoreform_section2_block7_correct!=null)
			child51++;
			if(this.Scoreform_section2_block7_score!=null)
			child51++;
			if(this.Scoreform_section2_block7_time!=null)
			child51++;
			if(child51>0 || att51>0){
				xmlTxt+="\n<iq:block7";
			if(child51==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section2_block7_time!=null){
			xmlTxt+="\n<iq:time";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block7_time;
			xmlTxt+="</iq:time>";
		}
		if (this.Scoreform_section2_block7_correct!=null){
			xmlTxt+="\n<iq:correct";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block7_correct;
			xmlTxt+="</iq:correct>";
		}
		if (this.Scoreform_section2_block7_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block7_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:block7>";
			}
			}

			var child52=0;
			var att52=0;
			if(this.Scoreform_section2_block8_correct!=null)
			child52++;
			if(this.Scoreform_section2_block8_time!=null)
			child52++;
			if(this.Scoreform_section2_block8_score!=null)
			child52++;
			if(child52>0 || att52>0){
				xmlTxt+="\n<iq:block8";
			if(child52==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section2_block8_time!=null){
			xmlTxt+="\n<iq:time";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block8_time;
			xmlTxt+="</iq:time>";
		}
		if (this.Scoreform_section2_block8_correct!=null){
			xmlTxt+="\n<iq:correct";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block8_correct;
			xmlTxt+="</iq:correct>";
		}
		if (this.Scoreform_section2_block8_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block8_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:block8>";
			}
			}

			var child53=0;
			var att53=0;
			if(this.Scoreform_section2_block9_score!=null)
			child53++;
			if(this.Scoreform_section2_block9_correct!=null)
			child53++;
			if(this.Scoreform_section2_block9_time!=null)
			child53++;
			if(child53>0 || att53>0){
				xmlTxt+="\n<iq:block9";
			if(child53==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section2_block9_time!=null){
			xmlTxt+="\n<iq:time";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block9_time;
			xmlTxt+="</iq:time>";
		}
		if (this.Scoreform_section2_block9_correct!=null){
			xmlTxt+="\n<iq:correct";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block9_correct;
			xmlTxt+="</iq:correct>";
		}
		if (this.Scoreform_section2_block9_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block9_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:block9>";
			}
			}

			var child54=0;
			var att54=0;
			if(this.Scoreform_section2_block10_score!=null)
			child54++;
			if(this.Scoreform_section2_block10_correct!=null)
			child54++;
			if(this.Scoreform_section2_block10_time!=null)
			child54++;
			if(child54>0 || att54>0){
				xmlTxt+="\n<iq:block10";
			if(child54==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section2_block10_time!=null){
			xmlTxt+="\n<iq:time";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block10_time;
			xmlTxt+="</iq:time>";
		}
		if (this.Scoreform_section2_block10_correct!=null){
			xmlTxt+="\n<iq:correct";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block10_correct;
			xmlTxt+="</iq:correct>";
		}
		if (this.Scoreform_section2_block10_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block10_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:block10>";
			}
			}

			var child55=0;
			var att55=0;
			if(this.Scoreform_section2_block11_correct!=null)
			child55++;
			if(this.Scoreform_section2_block11_score!=null)
			child55++;
			if(this.Scoreform_section2_block11_time!=null)
			child55++;
			if(child55>0 || att55>0){
				xmlTxt+="\n<iq:block11";
			if(child55==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section2_block11_time!=null){
			xmlTxt+="\n<iq:time";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block11_time;
			xmlTxt+="</iq:time>";
		}
		if (this.Scoreform_section2_block11_correct!=null){
			xmlTxt+="\n<iq:correct";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block11_correct;
			xmlTxt+="</iq:correct>";
		}
		if (this.Scoreform_section2_block11_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block11_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:block11>";
			}
			}

			var child56=0;
			var att56=0;
			if(this.Scoreform_section2_block12_correct!=null)
			child56++;
			if(this.Scoreform_section2_block12_time!=null)
			child56++;
			if(this.Scoreform_section2_block12_score!=null)
			child56++;
			if(child56>0 || att56>0){
				xmlTxt+="\n<iq:block12";
			if(child56==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section2_block12_time!=null){
			xmlTxt+="\n<iq:time";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block12_time;
			xmlTxt+="</iq:time>";
		}
		if (this.Scoreform_section2_block12_correct!=null){
			xmlTxt+="\n<iq:correct";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block12_correct;
			xmlTxt+="</iq:correct>";
		}
		if (this.Scoreform_section2_block12_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block12_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:block12>";
			}
			}

			var child57=0;
			var att57=0;
			if(this.Scoreform_section2_block13_score!=null)
			child57++;
			if(this.Scoreform_section2_block13_correct!=null)
			child57++;
			if(this.Scoreform_section2_block13_time!=null)
			child57++;
			if(child57>0 || att57>0){
				xmlTxt+="\n<iq:block13";
			if(child57==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section2_block13_time!=null){
			xmlTxt+="\n<iq:time";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block13_time;
			xmlTxt+="</iq:time>";
		}
		if (this.Scoreform_section2_block13_correct!=null){
			xmlTxt+="\n<iq:correct";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block13_correct;
			xmlTxt+="</iq:correct>";
		}
		if (this.Scoreform_section2_block13_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section2_block13_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:block13>";
			}
			}

				xmlTxt+="\n</iq:section2>";
			}
			}

			var child58=0;
			var att58=0;
			if(this.Scoreform_section3_similar10_response!=null)
			child58++;
			if(this.Scoreform_section3_similar15_response!=null)
			child58++;
			if(this.Scoreform_section3_similar23_response!=null)
			child58++;
			if(this.Scoreform_section3_similar3_response!=null)
			child58++;
			if(this.Scoreform_section3_similar8_response!=null)
			child58++;
			if(this.Scoreform_section3_similar11_response!=null)
			child58++;
			if(this.Scoreform_section3_similar16_response!=null)
			child58++;
			if(this.Scoreform_section3_similar24_response!=null)
			child58++;
			if(this.Scoreform_section3_similar4_response!=null)
			child58++;
			if(this.Scoreform_section3_similar9_response!=null)
			child58++;
			if(this.Scoreform_section3_similar12_score!=null)
			child58++;
			if(this.Scoreform_section3_similar12_response!=null)
			child58++;
			if(this.Scoreform_section3_similar20_response!=null)
			child58++;
			if(this.Scoreform_section3_similar11_score!=null)
			child58++;
			if(this.Scoreform_section3_similar14_score!=null)
			child58++;
			if(this.Scoreform_section3_similar19_score!=null)
			child58++;
			if(this.Scoreform_section3_similar10_score!=null)
			child58++;
			if(this.Scoreform_section3_similar16_score!=null)
			child58++;
			if(this.Scoreform_section3_similar13_score!=null)
			child58++;
			if(this.Scoreform_section3_similar18_score!=null)
			child58++;
			if(this.Scoreform_section3_similar15_score!=null)
			child58++;
			if(this.Scoreform_section3_similar17_score!=null)
			child58++;
			if(this.Scoreform_section3_similar17_response!=null)
			child58++;
			if(this.Scoreform_section3_similar25_response!=null)
			child58++;
			if(this.Scoreform_section3_similar5_response!=null)
			child58++;
			if(this.Scoreform_section3_similar20_score!=null)
			child58++;
			if(this.Scoreform_section3_similar25_score!=null)
			child58++;
			if(this.Scoreform_section3_similar22_score!=null)
			child58++;
			if(this.Scoreform_section3_similar24_score!=null)
			child58++;
			if(this.Scoreform_section3_similar21_score!=null)
			child58++;
			if(this.Scoreform_section3_similar26_score!=null)
			child58++;
			if(this.Scoreform_section3_similar23_score!=null)
			child58++;
			if(this.Scoreform_section3_similar13_response!=null)
			child58++;
			if(this.Scoreform_section3_similar21_response!=null)
			child58++;
			if(this.Scoreform_section3_similar1_response!=null)
			child58++;
			if(this.Scoreform_section3_similar18_response!=null)
			child58++;
			if(this.Scoreform_section3_similar26_response!=null)
			child58++;
			if(this.Scoreform_section3_similar6_response!=null)
			child58++;
			if(this.Scoreform_section3_similar2_score!=null)
			child58++;
			if(this.Scoreform_section3_similar1_score!=null)
			child58++;
			if(this.Scoreform_section3_similar14_response!=null)
			child58++;
			if(this.Scoreform_section3_similar7_score!=null)
			child58++;
			if(this.Scoreform_section3_similar4_score!=null)
			child58++;
			if(this.Scoreform_section3_similar22_response!=null)
			child58++;
			if(this.Scoreform_section3_similar9_score!=null)
			child58++;
			if(this.Scoreform_section3_similar6_score!=null)
			child58++;
			if(this.Scoreform_section3_similar2_response!=null)
			child58++;
			if(this.Scoreform_section3_similar3_score!=null)
			child58++;
			if(this.Scoreform_section3_similar8_score!=null)
			child58++;
			if(this.Scoreform_section3_similar5_score!=null)
			child58++;
			if(this.Scoreform_section3_similar19_response!=null)
			child58++;
			if(this.Scoreform_section3_similar7_response!=null)
			child58++;
			if(child58>0 || att58>0){
				xmlTxt+="\n<iq:section3";
			if(child58==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child59=0;
			var att59=0;
			if(this.Scoreform_section3_similar1_score!=null)
			child59++;
			if(this.Scoreform_section3_similar1_response!=null)
			child59++;
			if(child59>0 || att59>0){
				xmlTxt+="\n<iq:similar1";
			if(child59==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar1_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar1_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar1_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar1_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar1>";
			}
			}

			var child60=0;
			var att60=0;
			if(this.Scoreform_section3_similar2_score!=null)
			child60++;
			if(this.Scoreform_section3_similar2_response!=null)
			child60++;
			if(child60>0 || att60>0){
				xmlTxt+="\n<iq:similar2";
			if(child60==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar2_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar2_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar2_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar2_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar2>";
			}
			}

			var child61=0;
			var att61=0;
			if(this.Scoreform_section3_similar3_response!=null)
			child61++;
			if(this.Scoreform_section3_similar3_score!=null)
			child61++;
			if(child61>0 || att61>0){
				xmlTxt+="\n<iq:similar3";
			if(child61==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar3_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar3_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar3_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar3_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar3>";
			}
			}

			var child62=0;
			var att62=0;
			if(this.Scoreform_section3_similar4_score!=null)
			child62++;
			if(this.Scoreform_section3_similar4_response!=null)
			child62++;
			if(child62>0 || att62>0){
				xmlTxt+="\n<iq:similar4";
			if(child62==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar4_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar4_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar4_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar4_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar4>";
			}
			}

			var child63=0;
			var att63=0;
			if(this.Scoreform_section3_similar5_response!=null)
			child63++;
			if(this.Scoreform_section3_similar5_score!=null)
			child63++;
			if(child63>0 || att63>0){
				xmlTxt+="\n<iq:similar5";
			if(child63==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar5_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar5_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar5_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar5_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar5>";
			}
			}

			var child64=0;
			var att64=0;
			if(this.Scoreform_section3_similar6_response!=null)
			child64++;
			if(this.Scoreform_section3_similar6_score!=null)
			child64++;
			if(child64>0 || att64>0){
				xmlTxt+="\n<iq:similar6";
			if(child64==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar6_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar6_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar6_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar6_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar6>";
			}
			}

			var child65=0;
			var att65=0;
			if(this.Scoreform_section3_similar7_response!=null)
			child65++;
			if(this.Scoreform_section3_similar7_score!=null)
			child65++;
			if(child65>0 || att65>0){
				xmlTxt+="\n<iq:similar7";
			if(child65==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar7_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar7_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar7_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar7_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar7>";
			}
			}

			var child66=0;
			var att66=0;
			if(this.Scoreform_section3_similar8_score!=null)
			child66++;
			if(this.Scoreform_section3_similar8_response!=null)
			child66++;
			if(child66>0 || att66>0){
				xmlTxt+="\n<iq:similar8";
			if(child66==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar8_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar8_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar8_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar8_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar8>";
			}
			}

			var child67=0;
			var att67=0;
			if(this.Scoreform_section3_similar9_score!=null)
			child67++;
			if(this.Scoreform_section3_similar9_response!=null)
			child67++;
			if(child67>0 || att67>0){
				xmlTxt+="\n<iq:similar9";
			if(child67==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar9_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar9_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar9_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar9_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar9>";
			}
			}

			var child68=0;
			var att68=0;
			if(this.Scoreform_section3_similar10_response!=null)
			child68++;
			if(this.Scoreform_section3_similar10_score!=null)
			child68++;
			if(child68>0 || att68>0){
				xmlTxt+="\n<iq:similar10";
			if(child68==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar10_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar10_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar10_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar10_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar10>";
			}
			}

			var child69=0;
			var att69=0;
			if(this.Scoreform_section3_similar11_score!=null)
			child69++;
			if(this.Scoreform_section3_similar11_response!=null)
			child69++;
			if(child69>0 || att69>0){
				xmlTxt+="\n<iq:similar11";
			if(child69==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar11_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar11_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar11_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar11_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar11>";
			}
			}

			var child70=0;
			var att70=0;
			if(this.Scoreform_section3_similar12_score!=null)
			child70++;
			if(this.Scoreform_section3_similar12_response!=null)
			child70++;
			if(child70>0 || att70>0){
				xmlTxt+="\n<iq:similar12";
			if(child70==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar12_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar12_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar12_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar12_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar12>";
			}
			}

			var child71=0;
			var att71=0;
			if(this.Scoreform_section3_similar13_score!=null)
			child71++;
			if(this.Scoreform_section3_similar13_response!=null)
			child71++;
			if(child71>0 || att71>0){
				xmlTxt+="\n<iq:similar13";
			if(child71==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar13_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar13_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar13_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar13_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar13>";
			}
			}

			var child72=0;
			var att72=0;
			if(this.Scoreform_section3_similar14_response!=null)
			child72++;
			if(this.Scoreform_section3_similar14_score!=null)
			child72++;
			if(child72>0 || att72>0){
				xmlTxt+="\n<iq:similar14";
			if(child72==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar14_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar14_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar14_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar14_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar14>";
			}
			}

			var child73=0;
			var att73=0;
			if(this.Scoreform_section3_similar15_score!=null)
			child73++;
			if(this.Scoreform_section3_similar15_response!=null)
			child73++;
			if(child73>0 || att73>0){
				xmlTxt+="\n<iq:similar15";
			if(child73==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar15_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar15_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar15_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar15_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar15>";
			}
			}

			var child74=0;
			var att74=0;
			if(this.Scoreform_section3_similar16_score!=null)
			child74++;
			if(this.Scoreform_section3_similar16_response!=null)
			child74++;
			if(child74>0 || att74>0){
				xmlTxt+="\n<iq:similar16";
			if(child74==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar16_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar16_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar16_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar16_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar16>";
			}
			}

			var child75=0;
			var att75=0;
			if(this.Scoreform_section3_similar17_score!=null)
			child75++;
			if(this.Scoreform_section3_similar17_response!=null)
			child75++;
			if(child75>0 || att75>0){
				xmlTxt+="\n<iq:similar17";
			if(child75==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar17_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar17_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar17_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar17_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar17>";
			}
			}

			var child76=0;
			var att76=0;
			if(this.Scoreform_section3_similar18_response!=null)
			child76++;
			if(this.Scoreform_section3_similar18_score!=null)
			child76++;
			if(child76>0 || att76>0){
				xmlTxt+="\n<iq:similar18";
			if(child76==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar18_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar18_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar18_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar18_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar18>";
			}
			}

			var child77=0;
			var att77=0;
			if(this.Scoreform_section3_similar19_score!=null)
			child77++;
			if(this.Scoreform_section3_similar19_response!=null)
			child77++;
			if(child77>0 || att77>0){
				xmlTxt+="\n<iq:similar19";
			if(child77==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar19_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar19_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar19_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar19_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar19>";
			}
			}

			var child78=0;
			var att78=0;
			if(this.Scoreform_section3_similar20_response!=null)
			child78++;
			if(this.Scoreform_section3_similar20_score!=null)
			child78++;
			if(child78>0 || att78>0){
				xmlTxt+="\n<iq:similar20";
			if(child78==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar20_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar20_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar20_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar20_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar20>";
			}
			}

			var child79=0;
			var att79=0;
			if(this.Scoreform_section3_similar21_score!=null)
			child79++;
			if(this.Scoreform_section3_similar21_response!=null)
			child79++;
			if(child79>0 || att79>0){
				xmlTxt+="\n<iq:similar21";
			if(child79==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar21_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar21_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar21_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar21_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar21>";
			}
			}

			var child80=0;
			var att80=0;
			if(this.Scoreform_section3_similar22_score!=null)
			child80++;
			if(this.Scoreform_section3_similar22_response!=null)
			child80++;
			if(child80>0 || att80>0){
				xmlTxt+="\n<iq:similar22";
			if(child80==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar22_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar22_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar22_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar22_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar22>";
			}
			}

			var child81=0;
			var att81=0;
			if(this.Scoreform_section3_similar23_score!=null)
			child81++;
			if(this.Scoreform_section3_similar23_response!=null)
			child81++;
			if(child81>0 || att81>0){
				xmlTxt+="\n<iq:similar23";
			if(child81==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar23_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar23_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar23_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar23_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar23>";
			}
			}

			var child82=0;
			var att82=0;
			if(this.Scoreform_section3_similar24_score!=null)
			child82++;
			if(this.Scoreform_section3_similar24_response!=null)
			child82++;
			if(child82>0 || att82>0){
				xmlTxt+="\n<iq:similar24";
			if(child82==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar24_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar24_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar24_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar24_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar24>";
			}
			}

			var child83=0;
			var att83=0;
			if(this.Scoreform_section3_similar25_response!=null)
			child83++;
			if(this.Scoreform_section3_similar25_score!=null)
			child83++;
			if(child83>0 || att83>0){
				xmlTxt+="\n<iq:similar25";
			if(child83==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar25_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar25_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar25_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar25_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar25>";
			}
			}

			var child84=0;
			var att84=0;
			if(this.Scoreform_section3_similar26_response!=null)
			child84++;
			if(this.Scoreform_section3_similar26_score!=null)
			child84++;
			if(child84>0 || att84>0){
				xmlTxt+="\n<iq:similar26";
			if(child84==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section3_similar26_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar26_response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section3_similar26_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section3_similar26_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:similar26>";
			}
			}

				xmlTxt+="\n</iq:section3>";
			}
			}

			var child85=0;
			var att85=0;
			if(this.Scoreform_section4_matrix4_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix19_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix14_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix3_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix33_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix6_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix16_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix8_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix26_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix20_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix29_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix13_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix8_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix2_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix18_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix25_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix18_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix12_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix7_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix32_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix31_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix1_score!=null)
			child85++;
			if(this.Scoreform_section4_matrixb_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix33_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix1_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix20_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix35_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix17_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix22_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix6_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix31_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix24_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix24_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix11_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix26_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix3_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix13_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix28_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix5_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix11_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix15_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix30_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix7_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix29_score!=null)
			child85++;
			if(this.Scoreform_section4_matrixb_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix23_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix9_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix16_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix10_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix5_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix35_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix17_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix28_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix22_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix19_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix15_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix30_response!=null)
			child85++;
			if(this.Scoreform_section4_matrixa_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix4_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix32_response!=null)
			child85++;
			if(this.Scoreform_section4_matrixa_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix27_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix34_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix21_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix14_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix9_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix34_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix23_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix10_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix25_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix2_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix21_score!=null)
			child85++;
			if(this.Scoreform_section4_matrix12_response!=null)
			child85++;
			if(this.Scoreform_section4_matrix27_response!=null)
			child85++;
			if(child85>0 || att85>0){
				xmlTxt+="\n<iq:section4";
			if(child85==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child86=0;
			var att86=0;
			if(this.Scoreform_section4_matrixa_score!=null)
			child86++;
			if(this.Scoreform_section4_matrixa_response!=null)
			child86++;
			if(child86>0 || att86>0){
				xmlTxt+="\n<iq:matrixA";
			if(child86==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrixa_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrixa_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrixa_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrixa_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrixA>";
			}
			}

			var child87=0;
			var att87=0;
			if(this.Scoreform_section4_matrixb_response!=null)
			child87++;
			if(this.Scoreform_section4_matrixb_score!=null)
			child87++;
			if(child87>0 || att87>0){
				xmlTxt+="\n<iq:matrixB";
			if(child87==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrixb_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrixb_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrixb_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrixb_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrixB>";
			}
			}

			var child88=0;
			var att88=0;
			if(this.Scoreform_section4_matrix1_score!=null)
			child88++;
			if(this.Scoreform_section4_matrix1_response!=null)
			child88++;
			if(child88>0 || att88>0){
				xmlTxt+="\n<iq:matrix1";
			if(child88==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix1_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix1_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix1_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix1_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix1>";
			}
			}

			var child89=0;
			var att89=0;
			if(this.Scoreform_section4_matrix2_score!=null)
			child89++;
			if(this.Scoreform_section4_matrix2_response!=null)
			child89++;
			if(child89>0 || att89>0){
				xmlTxt+="\n<iq:matrix2";
			if(child89==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix2_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix2_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix2_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix2_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix2>";
			}
			}

			var child90=0;
			var att90=0;
			if(this.Scoreform_section4_matrix3_score!=null)
			child90++;
			if(this.Scoreform_section4_matrix3_response!=null)
			child90++;
			if(child90>0 || att90>0){
				xmlTxt+="\n<iq:matrix3";
			if(child90==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix3_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix3_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix3_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix3_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix3>";
			}
			}

			var child91=0;
			var att91=0;
			if(this.Scoreform_section4_matrix4_score!=null)
			child91++;
			if(this.Scoreform_section4_matrix4_response!=null)
			child91++;
			if(child91>0 || att91>0){
				xmlTxt+="\n<iq:matrix4";
			if(child91==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix4_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix4_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix4_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix4_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix4>";
			}
			}

			var child92=0;
			var att92=0;
			if(this.Scoreform_section4_matrix5_response!=null)
			child92++;
			if(this.Scoreform_section4_matrix5_score!=null)
			child92++;
			if(child92>0 || att92>0){
				xmlTxt+="\n<iq:matrix5";
			if(child92==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix5_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix5_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix5_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix5_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix5>";
			}
			}

			var child93=0;
			var att93=0;
			if(this.Scoreform_section4_matrix6_response!=null)
			child93++;
			if(this.Scoreform_section4_matrix6_score!=null)
			child93++;
			if(child93>0 || att93>0){
				xmlTxt+="\n<iq:matrix6";
			if(child93==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix6_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix6_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix6_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix6_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix6>";
			}
			}

			var child94=0;
			var att94=0;
			if(this.Scoreform_section4_matrix7_response!=null)
			child94++;
			if(this.Scoreform_section4_matrix7_score!=null)
			child94++;
			if(child94>0 || att94>0){
				xmlTxt+="\n<iq:matrix7";
			if(child94==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix7_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix7_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix7_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix7_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix7>";
			}
			}

			var child95=0;
			var att95=0;
			if(this.Scoreform_section4_matrix8_response!=null)
			child95++;
			if(this.Scoreform_section4_matrix8_score!=null)
			child95++;
			if(child95>0 || att95>0){
				xmlTxt+="\n<iq:matrix8";
			if(child95==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix8_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix8_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix8_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix8_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix8>";
			}
			}

			var child96=0;
			var att96=0;
			if(this.Scoreform_section4_matrix9_score!=null)
			child96++;
			if(this.Scoreform_section4_matrix9_response!=null)
			child96++;
			if(child96>0 || att96>0){
				xmlTxt+="\n<iq:matrix9";
			if(child96==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix9_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix9_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix9_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix9_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix9>";
			}
			}

			var child97=0;
			var att97=0;
			if(this.Scoreform_section4_matrix10_score!=null)
			child97++;
			if(this.Scoreform_section4_matrix10_response!=null)
			child97++;
			if(child97>0 || att97>0){
				xmlTxt+="\n<iq:matrix10";
			if(child97==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix10_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix10_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix10_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix10_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix10>";
			}
			}

			var child98=0;
			var att98=0;
			if(this.Scoreform_section4_matrix11_score!=null)
			child98++;
			if(this.Scoreform_section4_matrix11_response!=null)
			child98++;
			if(child98>0 || att98>0){
				xmlTxt+="\n<iq:matrix11";
			if(child98==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix11_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix11_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix11_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix11_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix11>";
			}
			}

			var child99=0;
			var att99=0;
			if(this.Scoreform_section4_matrix12_response!=null)
			child99++;
			if(this.Scoreform_section4_matrix12_score!=null)
			child99++;
			if(child99>0 || att99>0){
				xmlTxt+="\n<iq:matrix12";
			if(child99==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix12_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix12_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix12_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix12_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix12>";
			}
			}

			var child100=0;
			var att100=0;
			if(this.Scoreform_section4_matrix13_response!=null)
			child100++;
			if(this.Scoreform_section4_matrix13_score!=null)
			child100++;
			if(child100>0 || att100>0){
				xmlTxt+="\n<iq:matrix13";
			if(child100==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix13_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix13_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix13_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix13_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix13>";
			}
			}

			var child101=0;
			var att101=0;
			if(this.Scoreform_section4_matrix14_score!=null)
			child101++;
			if(this.Scoreform_section4_matrix14_response!=null)
			child101++;
			if(child101>0 || att101>0){
				xmlTxt+="\n<iq:matrix14";
			if(child101==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix14_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix14_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix14_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix14_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix14>";
			}
			}

			var child102=0;
			var att102=0;
			if(this.Scoreform_section4_matrix15_score!=null)
			child102++;
			if(this.Scoreform_section4_matrix15_response!=null)
			child102++;
			if(child102>0 || att102>0){
				xmlTxt+="\n<iq:matrix15";
			if(child102==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix15_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix15_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix15_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix15_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix15>";
			}
			}

			var child103=0;
			var att103=0;
			if(this.Scoreform_section4_matrix16_score!=null)
			child103++;
			if(this.Scoreform_section4_matrix16_response!=null)
			child103++;
			if(child103>0 || att103>0){
				xmlTxt+="\n<iq:matrix16";
			if(child103==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix16_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix16_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix16_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix16_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix16>";
			}
			}

			var child104=0;
			var att104=0;
			if(this.Scoreform_section4_matrix17_score!=null)
			child104++;
			if(this.Scoreform_section4_matrix17_response!=null)
			child104++;
			if(child104>0 || att104>0){
				xmlTxt+="\n<iq:matrix17";
			if(child104==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix17_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix17_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix17_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix17_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix17>";
			}
			}

			var child105=0;
			var att105=0;
			if(this.Scoreform_section4_matrix18_score!=null)
			child105++;
			if(this.Scoreform_section4_matrix18_response!=null)
			child105++;
			if(child105>0 || att105>0){
				xmlTxt+="\n<iq:matrix18";
			if(child105==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix18_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix18_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix18_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix18_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix18>";
			}
			}

			var child106=0;
			var att106=0;
			if(this.Scoreform_section4_matrix19_score!=null)
			child106++;
			if(this.Scoreform_section4_matrix19_response!=null)
			child106++;
			if(child106>0 || att106>0){
				xmlTxt+="\n<iq:matrix19";
			if(child106==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix19_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix19_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix19_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix19_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix19>";
			}
			}

			var child107=0;
			var att107=0;
			if(this.Scoreform_section4_matrix20_score!=null)
			child107++;
			if(this.Scoreform_section4_matrix20_response!=null)
			child107++;
			if(child107>0 || att107>0){
				xmlTxt+="\n<iq:matrix20";
			if(child107==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix20_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix20_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix20_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix20_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix20>";
			}
			}

			var child108=0;
			var att108=0;
			if(this.Scoreform_section4_matrix21_score!=null)
			child108++;
			if(this.Scoreform_section4_matrix21_response!=null)
			child108++;
			if(child108>0 || att108>0){
				xmlTxt+="\n<iq:matrix21";
			if(child108==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix21_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix21_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix21_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix21_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix21>";
			}
			}

			var child109=0;
			var att109=0;
			if(this.Scoreform_section4_matrix22_score!=null)
			child109++;
			if(this.Scoreform_section4_matrix22_response!=null)
			child109++;
			if(child109>0 || att109>0){
				xmlTxt+="\n<iq:matrix22";
			if(child109==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix22_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix22_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix22_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix22_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix22>";
			}
			}

			var child110=0;
			var att110=0;
			if(this.Scoreform_section4_matrix23_response!=null)
			child110++;
			if(this.Scoreform_section4_matrix23_score!=null)
			child110++;
			if(child110>0 || att110>0){
				xmlTxt+="\n<iq:matrix23";
			if(child110==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix23_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix23_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix23_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix23_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix23>";
			}
			}

			var child111=0;
			var att111=0;
			if(this.Scoreform_section4_matrix24_score!=null)
			child111++;
			if(this.Scoreform_section4_matrix24_response!=null)
			child111++;
			if(child111>0 || att111>0){
				xmlTxt+="\n<iq:matrix24";
			if(child111==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix24_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix24_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix24_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix24_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix24>";
			}
			}

			var child112=0;
			var att112=0;
			if(this.Scoreform_section4_matrix25_response!=null)
			child112++;
			if(this.Scoreform_section4_matrix25_score!=null)
			child112++;
			if(child112>0 || att112>0){
				xmlTxt+="\n<iq:matrix25";
			if(child112==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix25_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix25_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix25_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix25_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix25>";
			}
			}

			var child113=0;
			var att113=0;
			if(this.Scoreform_section4_matrix26_response!=null)
			child113++;
			if(this.Scoreform_section4_matrix26_score!=null)
			child113++;
			if(child113>0 || att113>0){
				xmlTxt+="\n<iq:matrix26";
			if(child113==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix26_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix26_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix26_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix26_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix26>";
			}
			}

			var child114=0;
			var att114=0;
			if(this.Scoreform_section4_matrix27_score!=null)
			child114++;
			if(this.Scoreform_section4_matrix27_response!=null)
			child114++;
			if(child114>0 || att114>0){
				xmlTxt+="\n<iq:matrix27";
			if(child114==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix27_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix27_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix27_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix27_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix27>";
			}
			}

			var child115=0;
			var att115=0;
			if(this.Scoreform_section4_matrix28_score!=null)
			child115++;
			if(this.Scoreform_section4_matrix28_response!=null)
			child115++;
			if(child115>0 || att115>0){
				xmlTxt+="\n<iq:matrix28";
			if(child115==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix28_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix28_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix28_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix28_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix28>";
			}
			}

			var child116=0;
			var att116=0;
			if(this.Scoreform_section4_matrix29_response!=null)
			child116++;
			if(this.Scoreform_section4_matrix29_score!=null)
			child116++;
			if(child116>0 || att116>0){
				xmlTxt+="\n<iq:matrix29";
			if(child116==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix29_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix29_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix29_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix29_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix29>";
			}
			}

			var child117=0;
			var att117=0;
			if(this.Scoreform_section4_matrix30_score!=null)
			child117++;
			if(this.Scoreform_section4_matrix30_response!=null)
			child117++;
			if(child117>0 || att117>0){
				xmlTxt+="\n<iq:matrix30";
			if(child117==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix30_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix30_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix30_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix30_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix30>";
			}
			}

			var child118=0;
			var att118=0;
			if(this.Scoreform_section4_matrix31_score!=null)
			child118++;
			if(this.Scoreform_section4_matrix31_response!=null)
			child118++;
			if(child118>0 || att118>0){
				xmlTxt+="\n<iq:matrix31";
			if(child118==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix31_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix31_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix31_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix31_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix31>";
			}
			}

			var child119=0;
			var att119=0;
			if(this.Scoreform_section4_matrix32_response!=null)
			child119++;
			if(this.Scoreform_section4_matrix32_score!=null)
			child119++;
			if(child119>0 || att119>0){
				xmlTxt+="\n<iq:matrix32";
			if(child119==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix32_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix32_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix32_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix32_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix32>";
			}
			}

			var child120=0;
			var att120=0;
			if(this.Scoreform_section4_matrix33_response!=null)
			child120++;
			if(this.Scoreform_section4_matrix33_score!=null)
			child120++;
			if(child120>0 || att120>0){
				xmlTxt+="\n<iq:matrix33";
			if(child120==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix33_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix33_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix33_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix33_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix33>";
			}
			}

			var child121=0;
			var att121=0;
			if(this.Scoreform_section4_matrix34_score!=null)
			child121++;
			if(this.Scoreform_section4_matrix34_response!=null)
			child121++;
			if(child121>0 || att121>0){
				xmlTxt+="\n<iq:matrix34";
			if(child121==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix34_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix34_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix34_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix34_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix34>";
			}
			}

			var child122=0;
			var att122=0;
			if(this.Scoreform_section4_matrix35_response!=null)
			child122++;
			if(this.Scoreform_section4_matrix35_score!=null)
			child122++;
			if(child122>0 || att122>0){
				xmlTxt+="\n<iq:matrix35";
			if(child122==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_section4_matrix35_response!=null){
			xmlTxt+="\n<iq:response";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix35_response;
			xmlTxt+="</iq:response>";
		}
		if (this.Scoreform_section4_matrix35_score!=null){
			xmlTxt+="\n<iq:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_section4_matrix35_score;
			xmlTxt+="</iq:score>";
		}
				xmlTxt+="\n</iq:matrix35>";
			}
			}

				xmlTxt+="\n</iq:section4>";
			}
			}

			var child123=0;
			var att123=0;
			if(this.Scoreform_behavob_unusualbehav!=null)
			child123++;
			if(this.Scoreform_behavob_lang!=null)
			child123++;
			if(this.Scoreform_behavob_affectmood!=null)
			child123++;
			if(this.Scoreform_behavob_attitude!=null)
			child123++;
			if(this.Scoreform_behavob_physappear!=null)
			child123++;
			if(this.Scoreform_behavob_reasonrefer!=null)
			child123++;
			if(this.Scoreform_behavob_other!=null)
			child123++;
			if(this.Scoreform_behavob_attention!=null)
			child123++;
			if(this.Scoreform_behavob_visaudmotor!=null)
			child123++;
			if(child123>0 || att123>0){
				xmlTxt+="\n<iq:behavOb";
			if(child123==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_behavob_reasonrefer!=null){
			xmlTxt+="\n<iq:reasonRefer";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_behavob_reasonrefer.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:reasonRefer>";
		}
		if (this.Scoreform_behavob_attitude!=null){
			xmlTxt+="\n<iq:attitude";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_behavob_attitude.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:attitude>";
		}
		if (this.Scoreform_behavob_physappear!=null){
			xmlTxt+="\n<iq:physAppear";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_behavob_physappear.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:physAppear>";
		}
		if (this.Scoreform_behavob_attention!=null){
			xmlTxt+="\n<iq:attention";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_behavob_attention.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:attention>";
		}
		if (this.Scoreform_behavob_visaudmotor!=null){
			xmlTxt+="\n<iq:visAudMotor";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_behavob_visaudmotor.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:visAudMotor>";
		}
		if (this.Scoreform_behavob_lang!=null){
			xmlTxt+="\n<iq:lang";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_behavob_lang.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:lang>";
		}
		if (this.Scoreform_behavob_affectmood!=null){
			xmlTxt+="\n<iq:affectMood";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_behavob_affectmood.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:affectMood>";
		}
		if (this.Scoreform_behavob_unusualbehav!=null){
			xmlTxt+="\n<iq:unusualBehav";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_behavob_unusualbehav.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:unusualBehav>";
		}
		if (this.Scoreform_behavob_other!=null){
			xmlTxt+="\n<iq:other";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_behavob_other.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:other>";
		}
				xmlTxt+="\n</iq:behavOb>";
			}
			}

				xmlTxt+="\n</iq:ScoreForm>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.WasiVraw!=null) return true;
		if (this.WasiVt!=null) return true;
		if (this.WasiVnote!=null) return true;
		if (this.WasiBraw!=null) return true;
		if (this.WasiBt!=null) return true;
		if (this.WasiBnote!=null) return true;
		if (this.WasiSraw!=null) return true;
		if (this.WasiSt!=null) return true;
		if (this.WasiSnote!=null) return true;
		if (this.WasiMraw!=null) return true;
		if (this.WasiMt!=null) return true;
		if (this.WasiMnote!=null) return true;
			if(this.Scoreform_section1_vocab14_response!=null) return true;
			if(this.Scoreform_section1_vocab23_response!=null) return true;
			if(this.Scoreform_section1_vocab32_response!=null) return true;
			if(this.Scoreform_section1_vocab41_response!=null) return true;
			if(this.Scoreform_section1_vocab8_response!=null) return true;
			if(this.Scoreform_section3_similar23_score!=null) return true;
			if(this.Scoreform_section1_vocab1_score!=null) return true;
			if(this.Scoreform_section3_similar24_score!=null) return true;
			if(this.Scoreform_section4_matrixa_response!=null) return true;
			if(this.Scoreform_section4_matrix9_response!=null) return true;
			if(this.Scoreform_section1_vocab2_score!=null) return true;
			if(this.Scoreform_section2_block12_time!=null) return true;
			if(this.Scoreform_section3_similar25_score!=null) return true;
			if(this.Scoreform_section4_matrix12_response!=null) return true;
			if(this.Scoreform_section4_matrix21_response!=null) return true;
			if(this.Scoreform_section4_matrix30_response!=null) return true;
			if(this.Scoreform_behavob_lang!=null) return true;
			if(this.Scoreform_behavob_reasonrefer!=null) return true;
			if(this.Scoreform_section3_similar21_score!=null) return true;
			if(this.Scoreform_section4_matrix27_score!=null) return true;
			if(this.Scoreform_section3_similar22_score!=null) return true;
			if(this.Scoreform_section4_matrix28_score!=null) return true;
			if(this.Scoreform_section1_vocab15_response!=null) return true;
			if(this.Scoreform_section1_vocab24_response!=null) return true;
			if(this.Scoreform_section1_vocab33_response!=null) return true;
			if(this.Scoreform_section1_vocab42_response!=null) return true;
			if(this.Scoreform_section1_vocab9_response!=null) return true;
			if(this.Scoreform_section4_matrix29_score!=null) return true;
			if(this.Scoreform_section4_matrixb_response!=null) return true;
			if(this.Scoreform_section3_similar1_response!=null) return true;
			if(this.Scoreform_behavob_visaudmotor!=null) return true;
			if(this.Scoreform_section1_vocab17_score!=null) return true;
			if(this.Scoreform_section3_similar10_response!=null) return true;
			if(this.Scoreform_section4_matrix13_response!=null) return true;
			if(this.Scoreform_section4_matrix22_response!=null) return true;
			if(this.Scoreform_section4_matrix31_response!=null) return true;
			if(this.Scoreform_section4_matrix25_score!=null) return true;
			if(this.Scoreform_section1_vocab18_score!=null) return true;
			if(this.Scoreform_section3_similar20_score!=null) return true;
			if(this.Scoreform_section4_matrix26_score!=null) return true;
			if(this.Scoreform_section1_vocab19_score!=null) return true;
			if(this.Scoreform_behavob_unusualbehav!=null) return true;
			if(this.Scoreform_section1_vocab41_score!=null) return true;
			if(this.Scoreform_section2_block3_time!=null) return true;
			if(this.Scoreform_section1_vocab42_score!=null) return true;
			if(this.Scoreform_section1_vocab16_response!=null) return true;
			if(this.Scoreform_section1_vocab25_response!=null) return true;
			if(this.Scoreform_section1_vocab34_response!=null) return true;
			if(this.Scoreform_section4_matrix22_score!=null) return true;
			if(this.Scoreform_section1_vocab15_score!=null) return true;
			if(this.Scoreform_section4_matrix23_score!=null) return true;
			if(this.Scoreform_section2_block7_time!=null) return true;
			if(this.Scoreform_section2_block7_correct!=null) return true;
			if(this.Scoreform_section1_vocab16_score!=null) return true;
			if(this.Scoreform_section4_matrix24_score!=null) return true;
			if(this.Scoreform_section4_matrix1_response!=null) return true;
			if(this.Scoreform_section3_similar2_response!=null) return true;
			if(this.Scoreform_section3_similar11_response!=null) return true;
			if(this.Scoreform_section3_similar20_response!=null) return true;
			if(this.Scoreform_behavob_attention!=null) return true;
			if(this.Scoreform_section4_matrix23_response!=null) return true;
			if(this.Scoreform_section4_matrix32_response!=null) return true;
			if(this.Scoreform_section4_matrix14_response!=null) return true;
			if(this.Scoreform_section1_vocab12_score!=null) return true;
			if(this.Scoreform_section1_vocab40_score!=null) return true;
			if(this.Scoreform_section4_matrix20_score!=null) return true;
			if(this.Scoreform_section1_vocab13_score!=null) return true;
			if(this.Scoreform_section2_block1_correct!=null) return true;
			if(this.Scoreform_section3_similar8_score!=null) return true;
			if(this.Scoreform_behavob_other!=null) return true;
			if(this.Scoreform_section4_matrix21_score!=null) return true;
			if(this.Scoreform_section1_vocab14_score!=null) return true;
			if(this.Scoreform_section3_similar9_score!=null) return true;
			if(this.Scoreform_section2_block4_correct!=null) return true;
			if(this.Scoreform_section1_vocab26_response!=null) return true;
			if(this.Scoreform_section1_vocab35_response!=null) return true;
			if(this.Scoreform_section1_vocab1_response!=null) return true;
			if(this.Scoreform_behavob_affectmood!=null) return true;
			if(this.Scoreform_section1_vocab10_score!=null) return true;
			if(this.Scoreform_section2_block12_correct!=null) return true;
			if(this.Scoreform_section2_block13_time!=null) return true;
			if(this.Scoreform_behavob_physappear!=null) return true;
			if(this.Scoreform_section3_similar5_score!=null) return true;
			if(this.Scoreform_section3_similar3_response!=null) return true;
			if(this.Scoreform_section1_vocab11_score!=null) return true;
			if(this.Scoreform_section3_similar6_score!=null) return true;
			if(this.Scoreform_section3_similar21_response!=null) return true;
			if(this.Scoreform_section3_similar12_response!=null) return true;
			if(this.Scoreform_section4_matrix15_response!=null) return true;
			if(this.Scoreform_section4_matrix24_response!=null) return true;
			if(this.Scoreform_section4_matrix33_response!=null) return true;
			if(this.Scoreform_section3_similar7_score!=null) return true;
			if(this.Scoreform_behavob_attitude!=null) return true;
			if(this.Scoreform_section3_similar3_score!=null) return true;
			if(this.Scoreform_section1_vocab17_response!=null) return true;
			if(this.Scoreform_section1_vocab27_response!=null) return true;
			if(this.Scoreform_section1_vocab36_response!=null) return true;
			if(this.Scoreform_section1_vocab2_response!=null) return true;
			if(this.Scoreform_section3_similar4_score!=null) return true;
			if(this.Scoreform_section2_block11_score!=null) return true;
			if(this.Scoreform_section4_matrix2_response!=null) return true;
			if(this.Scoreform_section2_block12_score!=null) return true;
			if(this.Scoreform_section3_similar4_response!=null) return true;
			if(this.Scoreform_section3_similar22_response!=null) return true;
			if(this.Scoreform_section3_similar13_response!=null) return true;
			if(this.Scoreform_section4_matrix25_response!=null) return true;
			if(this.Scoreform_section4_matrix34_response!=null) return true;
			if(this.Scoreform_section4_matrix16_response!=null) return true;
			if(this.Scoreform_section2_block13_score!=null) return true;
			if(this.Scoreform_section3_similar1_score!=null) return true;
			if(this.Scoreform_section4_matrix35_score!=null) return true;
			if(this.Scoreform_section2_block4_time!=null) return true;
			if(this.Scoreform_section1_vocab28_score!=null) return true;
			if(this.Scoreform_section3_similar2_score!=null) return true;
			if(this.Scoreform_section1_vocab29_score!=null) return true;
			if(this.Scoreform_section1_vocab18_response!=null) return true;
			if(this.Scoreform_section2_block10_score!=null) return true;
			if(this.Scoreform_section1_vocab37_response!=null) return true;
			if(this.Scoreform_section2_block8_time!=null) return true;
			if(this.Scoreform_section4_matrix9_score!=null) return true;
			if(this.Scoreform_section1_vocab25_score!=null) return true;
			if(this.Scoreform_section4_matrix33_score!=null) return true;
			if(this.Scoreform_section4_matrix3_response!=null) return true;
			if(this.Scoreform_section1_vocab26_score!=null) return true;
			if(this.Scoreform_section3_similar5_response!=null) return true;
			if(this.Scoreform_section4_matrix34_score!=null) return true;
			if(this.Scoreform_section3_similar14_response!=null) return true;
			if(this.Scoreform_section3_similar23_response!=null) return true;
			if(this.Scoreform_section4_matrix26_response!=null) return true;
			if(this.Scoreform_section4_matrix35_response!=null) return true;
			if(this.Scoreform_section1_vocab27_score!=null) return true;
			if(this.Scoreform_section4_matrix6_score!=null) return true;
			if(this.Scoreform_section2_block10_time!=null) return true;
			if(this.Scoreform_section2_block5_correct!=null) return true;
			if(this.Scoreform_section4_matrix7_score!=null) return true;
			if(this.Scoreform_section4_matrix30_score!=null) return true;
			if(this.Scoreform_section4_matrixb_score!=null) return true;
			if(this.Scoreform_section1_vocab23_score!=null) return true;
			if(this.Scoreform_section4_matrix8_score!=null) return true;
			if(this.Scoreform_section2_block8_correct!=null) return true;
			if(this.Scoreform_section4_matrix31_score!=null) return true;
			if(this.Scoreform_section1_vocab19_response!=null) return true;
			if(this.Scoreform_section1_vocab28_response!=null) return true;
			if(this.Scoreform_section1_vocab3_response!=null) return true;
			if(this.Scoreform_section1_vocab24_score!=null) return true;
			if(this.Scoreform_section2_block13_correct!=null) return true;
			if(this.Scoreform_section1_vocab38_response!=null) return true;
			if(this.Scoreform_section4_matrix32_score!=null) return true;
			if(this.Scoreform_section4_matrix4_score!=null) return true;
			if(this.Scoreform_section4_matrix4_response!=null) return true;
			if(this.Scoreform_section1_vocab20_score!=null) return true;
			if(this.Scoreform_section4_matrix5_score!=null) return true;
			if(this.Scoreform_section3_similar6_response!=null) return true;
			if(this.Scoreform_section4_matrix17_response!=null) return true;
			if(this.Scoreform_section3_similar15_response!=null) return true;
			if(this.Scoreform_section3_similar24_response!=null) return true;
			if(this.Scoreform_section2_block2_correct!=null) return true;
			if(this.Scoreform_section4_matrix27_response!=null) return true;
			if(this.Scoreform_section1_vocab21_score!=null) return true;
			if(this.Scoreform_section4_matrixa_score!=null) return true;
			if(this.Scoreform_section1_vocab22_score!=null) return true;
			if(this.Scoreform_section3_similar17_score!=null) return true;
			if(this.Scoreform_section4_matrix1_score!=null) return true;
			if(this.Scoreform_section2_block10_correct!=null) return true;
			if(this.Scoreform_section3_similar18_score!=null) return true;
			if(this.Scoreform_section4_matrix2_score!=null) return true;
			if(this.Scoreform_section2_block8_score!=null) return true;
			if(this.Scoreform_section3_similar19_score!=null) return true;
			if(this.Scoreform_section1_vocab29_response!=null) return true;
			if(this.Scoreform_section1_vocab10_response!=null) return true;
			if(this.Scoreform_section1_vocab4_response!=null) return true;
			if(this.Scoreform_section1_vocab39_response!=null) return true;
			if(this.Scoreform_section4_matrix3_score!=null) return true;
			if(this.Scoreform_section2_block9_score!=null) return true;
			if(this.Scoreform_section2_block1_time!=null) return true;
			if(this.Scoreform_section4_matrix5_response!=null) return true;
			if(this.Scoreform_section3_similar15_score!=null) return true;
			if(this.Scoreform_section3_similar7_response!=null) return true;
			if(this.Scoreform_section4_matrix18_response!=null) return true;
			if(this.Scoreform_section3_similar25_response!=null) return true;
			if(this.Scoreform_section3_similar16_response!=null) return true;
			if(this.Scoreform_section4_matrix28_response!=null) return true;
			if(this.Scoreform_section3_similar16_score!=null) return true;
			if(this.Scoreform_section2_block5_time!=null) return true;
			if(this.Scoreform_section2_block6_score!=null) return true;
			if(this.Scoreform_section2_block7_score!=null) return true;
			if(this.Scoreform_section1_vocab38_score!=null) return true;
			if(this.Scoreform_section2_block9_time!=null) return true;
			if(this.Scoreform_section1_vocab39_score!=null) return true;
			if(this.Scoreform_section1_vocab11_response!=null) return true;
			if(this.Scoreform_section1_vocab20_response!=null) return true;
			if(this.Scoreform_section1_vocab5_response!=null) return true;
			if(this.Scoreform_section3_similar13_score!=null) return true;
			if(this.Scoreform_section4_matrix19_score!=null) return true;
			if(this.Scoreform_section2_block3_score!=null) return true;
			if(this.Scoreform_section3_similar14_score!=null) return true;
			if(this.Scoreform_section2_block4_score!=null) return true;
			if(this.Scoreform_section4_matrix6_response!=null) return true;
			if(this.Scoreform_section3_similar8_response!=null) return true;
			if(this.Scoreform_section2_block5_score!=null) return true;
			if(this.Scoreform_section4_matrix19_response!=null) return true;
			if(this.Scoreform_section3_similar26_response!=null) return true;
			if(this.Scoreform_section3_similar17_response!=null) return true;
			if(this.Scoreform_section1_vocab36_score!=null) return true;
			if(this.Scoreform_section3_similar10_score!=null) return true;
			if(this.Scoreform_section2_block11_time!=null) return true;
			if(this.Scoreform_section1_vocab37_score!=null) return true;
			if(this.Scoreform_section3_similar11_score!=null) return true;
			if(this.Scoreform_section4_matrix17_score!=null) return true;
			if(this.Scoreform_section2_block1_score!=null) return true;
			if(this.Scoreform_section3_similar12_score!=null) return true;
			if(this.Scoreform_section4_matrix18_score!=null) return true;
			if(this.Scoreform_section2_block2_score!=null) return true;
			if(this.Scoreform_section1_vocab12_response!=null) return true;
			if(this.Scoreform_section1_vocab33_score!=null) return true;
			if(this.Scoreform_section1_vocab21_response!=null) return true;
			if(this.Scoreform_section1_vocab30_response!=null) return true;
			if(this.Scoreform_section1_vocab6_response!=null) return true;
			if(this.Scoreform_section1_vocab40_response!=null) return true;
			if(this.Scoreform_section1_vocab34_score!=null) return true;
			if(this.Scoreform_section4_matrix14_score!=null) return true;
			if(this.Scoreform_section1_vocab7_score!=null) return true;
			if(this.Scoreform_section1_vocab35_score!=null) return true;
			if(this.Scoreform_section4_matrix7_response!=null) return true;
			if(this.Scoreform_section4_matrix15_score!=null) return true;
			if(this.Scoreform_section1_vocab8_score!=null) return true;
			if(this.Scoreform_section3_similar9_response!=null) return true;
			if(this.Scoreform_section4_matrix10_response!=null) return true;
			if(this.Scoreform_section4_matrix29_response!=null) return true;
			if(this.Scoreform_section2_block6_correct!=null) return true;
			if(this.Scoreform_section3_similar18_response!=null) return true;
			if(this.Scoreform_section4_matrix16_score!=null) return true;
			if(this.Scoreform_section1_vocab9_score!=null) return true;
			if(this.Scoreform_section1_vocab31_score!=null) return true;
			if(this.Scoreform_section2_block9_correct!=null) return true;
			if(this.Scoreform_section1_vocab32_score!=null) return true;
			if(this.Scoreform_section4_matrix12_score!=null) return true;
			if(this.Scoreform_section1_vocab5_score!=null) return true;
			if(this.Scoreform_section2_block2_time!=null) return true;
			if(this.Scoreform_section1_vocab13_response!=null) return true;
			if(this.Scoreform_section1_vocab22_response!=null) return true;
			if(this.Scoreform_section1_vocab31_response!=null) return true;
			if(this.Scoreform_section1_vocab7_response!=null) return true;
			if(this.Scoreform_section4_matrix13_score!=null) return true;
			if(this.Scoreform_section1_vocab6_score!=null) return true;
			if(this.Scoreform_section2_block3_correct!=null) return true;
			if(this.Scoreform_section2_block6_time!=null) return true;
			if(this.Scoreform_section4_matrix8_response!=null) return true;
			if(this.Scoreform_section4_matrix11_response!=null) return true;
			if(this.Scoreform_section4_matrix20_response!=null) return true;
			if(this.Scoreform_section1_vocab30_score!=null) return true;
			if(this.Scoreform_section3_similar19_response!=null) return true;
			if(this.Scoreform_section2_block11_correct!=null) return true;
			if(this.Scoreform_section4_matrix10_score!=null) return true;
			if(this.Scoreform_section1_vocab3_score!=null) return true;
			if(this.Scoreform_section3_similar26_score!=null) return true;
			if(this.Scoreform_section4_matrix11_score!=null) return true;
			if(this.Scoreform_section1_vocab4_score!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
