/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function dian_cdrsuppData(){
this.xsiType="dian:cdrsuppData";

	this.getSchemaElementName=function(){
		return "cdrsuppData";
	}

	this.getFullSchemaElementName=function(){
		return "dian:cdrsuppData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Amend2=null;


	function getAmend2() {
		return this.Amend2;
	}
	this.getAmend2=getAmend2;


	function setAmend2(v){
		this.Amend2=v;
	}
	this.setAmend2=setAmend2;

	this.Changes=null;


	function getChanges() {
		return this.Changes;
	}
	this.getChanges=getChanges;


	function setChanges(v){
		this.Changes=v;
	}
	this.setChanges=setChanges;

	this.Memory=null;


	function getMemory() {
		return this.Memory;
	}
	this.getMemory=getMemory;


	function setMemory(v){
		this.Memory=v;
	}
	this.setMemory=setMemory;

	this.Memorym=null;


	function getMemorym() {
		return this.Memorym;
	}
	this.getMemorym=getMemorym;


	function setMemorym(v){
		this.Memorym=v;
	}
	this.setMemorym=setMemorym;

	this.Judgment=null;


	function getJudgment() {
		return this.Judgment;
	}
	this.getJudgment=getJudgment;


	function setJudgment(v){
		this.Judgment=v;
	}
	this.setJudgment=setJudgment;

	this.Judgmentm=null;


	function getJudgmentm() {
		return this.Judgmentm;
	}
	this.getJudgmentm=getJudgmentm;


	function setJudgmentm(v){
		this.Judgmentm=v;
	}
	this.setJudgmentm=setJudgmentm;

	this.Language=null;


	function getLanguage() {
		return this.Language;
	}
	this.getLanguage=getLanguage;


	function setLanguage(v){
		this.Language=v;
	}
	this.setLanguage=setLanguage;

	this.Languagem=null;


	function getLanguagem() {
		return this.Languagem;
	}
	this.getLanguagem=getLanguagem;


	function setLanguagem(v){
		this.Languagem=v;
	}
	this.setLanguagem=setLanguagem;

	this.Visuospat=null;


	function getVisuospat() {
		return this.Visuospat;
	}
	this.getVisuospat=getVisuospat;


	function setVisuospat(v){
		this.Visuospat=v;
	}
	this.setVisuospat=setVisuospat;

	this.Visuospatm=null;


	function getVisuospatm() {
		return this.Visuospatm;
	}
	this.getVisuospatm=getVisuospatm;


	function setVisuospatm(v){
		this.Visuospatm=v;
	}
	this.setVisuospatm=setVisuospatm;

	this.Attention=null;


	function getAttention() {
		return this.Attention;
	}
	this.getAttention=getAttention;


	function setAttention(v){
		this.Attention=v;
	}
	this.setAttention=setAttention;

	this.Attentionm=null;


	function getAttentionm() {
		return this.Attentionm;
	}
	this.getAttentionm=getAttentionm;


	function setAttentionm(v){
		this.Attentionm=v;
	}
	this.setAttentionm=setAttentionm;

	this.Behavior=null;


	function getBehavior() {
		return this.Behavior;
	}
	this.getBehavior=getBehavior;


	function setBehavior(v){
		this.Behavior=v;
	}
	this.setBehavior=setBehavior;

	this.Behaviorm=null;


	function getBehaviorm() {
		return this.Behaviorm;
	}
	this.getBehaviorm=getBehaviorm;


	function setBehaviorm(v){
		this.Behaviorm=v;
	}
	this.setBehaviorm=setBehaviorm;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="AMEND2"){
				return this.Amend2 ;
			} else 
			if(xmlPath=="CHANGES"){
				return this.Changes ;
			} else 
			if(xmlPath=="MEMORY"){
				return this.Memory ;
			} else 
			if(xmlPath=="MEMORYM"){
				return this.Memorym ;
			} else 
			if(xmlPath=="JUDGMENT"){
				return this.Judgment ;
			} else 
			if(xmlPath=="JUDGMENTM"){
				return this.Judgmentm ;
			} else 
			if(xmlPath=="LANGUAGE"){
				return this.Language ;
			} else 
			if(xmlPath=="LANGUAGEM"){
				return this.Languagem ;
			} else 
			if(xmlPath=="VISUOSPAT"){
				return this.Visuospat ;
			} else 
			if(xmlPath=="VISUOSPATM"){
				return this.Visuospatm ;
			} else 
			if(xmlPath=="ATTENTION"){
				return this.Attention ;
			} else 
			if(xmlPath=="ATTENTIONM"){
				return this.Attentionm ;
			} else 
			if(xmlPath=="BEHAVIOR"){
				return this.Behavior ;
			} else 
			if(xmlPath=="BEHAVIORM"){
				return this.Behaviorm ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="AMEND2"){
				this.Amend2=value;
			} else 
			if(xmlPath=="CHANGES"){
				this.Changes=value;
			} else 
			if(xmlPath=="MEMORY"){
				this.Memory=value;
			} else 
			if(xmlPath=="MEMORYM"){
				this.Memorym=value;
			} else 
			if(xmlPath=="JUDGMENT"){
				this.Judgment=value;
			} else 
			if(xmlPath=="JUDGMENTM"){
				this.Judgmentm=value;
			} else 
			if(xmlPath=="LANGUAGE"){
				this.Language=value;
			} else 
			if(xmlPath=="LANGUAGEM"){
				this.Languagem=value;
			} else 
			if(xmlPath=="VISUOSPAT"){
				this.Visuospat=value;
			} else 
			if(xmlPath=="VISUOSPATM"){
				this.Visuospatm=value;
			} else 
			if(xmlPath=="ATTENTION"){
				this.Attention=value;
			} else 
			if(xmlPath=="ATTENTIONM"){
				this.Attentionm=value;
			} else 
			if(xmlPath=="BEHAVIOR"){
				this.Behavior=value;
			} else 
			if(xmlPath=="BEHAVIORM"){
				this.Behaviorm=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="AMEND2"){
			return "field_data";
		}else if (xmlPath=="CHANGES"){
			return "field_data";
		}else if (xmlPath=="MEMORY"){
			return "field_data";
		}else if (xmlPath=="MEMORYM"){
			return "field_data";
		}else if (xmlPath=="JUDGMENT"){
			return "field_data";
		}else if (xmlPath=="JUDGMENTM"){
			return "field_data";
		}else if (xmlPath=="LANGUAGE"){
			return "field_data";
		}else if (xmlPath=="LANGUAGEM"){
			return "field_data";
		}else if (xmlPath=="VISUOSPAT"){
			return "field_data";
		}else if (xmlPath=="VISUOSPATM"){
			return "field_data";
		}else if (xmlPath=="ATTENTION"){
			return "field_data";
		}else if (xmlPath=="ATTENTIONM"){
			return "field_data";
		}else if (xmlPath=="BEHAVIOR"){
			return "field_data";
		}else if (xmlPath=="BEHAVIORM"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<dian:CDRSUPP";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</dian:CDRSUPP>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Amend2!=null){
			xmlTxt+="\n<dian:AMEND2";
			xmlTxt+=">";
			xmlTxt+=this.Amend2;
			xmlTxt+="</dian:AMEND2>";
		}
		if (this.Changes!=null){
			xmlTxt+="\n<dian:CHANGES";
			xmlTxt+=">";
			xmlTxt+=this.Changes;
			xmlTxt+="</dian:CHANGES>";
		}
		if (this.Memory!=null){
			xmlTxt+="\n<dian:MEMORY";
			xmlTxt+=">";
			xmlTxt+=this.Memory;
			xmlTxt+="</dian:MEMORY>";
		}
		if (this.Memorym!=null){
			xmlTxt+="\n<dian:MEMORYM";
			xmlTxt+=">";
			xmlTxt+=this.Memorym;
			xmlTxt+="</dian:MEMORYM>";
		}
		if (this.Judgment!=null){
			xmlTxt+="\n<dian:JUDGMENT";
			xmlTxt+=">";
			xmlTxt+=this.Judgment;
			xmlTxt+="</dian:JUDGMENT>";
		}
		if (this.Judgmentm!=null){
			xmlTxt+="\n<dian:JUDGMENTM";
			xmlTxt+=">";
			xmlTxt+=this.Judgmentm;
			xmlTxt+="</dian:JUDGMENTM>";
		}
		if (this.Language!=null){
			xmlTxt+="\n<dian:LANGUAGE";
			xmlTxt+=">";
			xmlTxt+=this.Language;
			xmlTxt+="</dian:LANGUAGE>";
		}
		if (this.Languagem!=null){
			xmlTxt+="\n<dian:LANGUAGEM";
			xmlTxt+=">";
			xmlTxt+=this.Languagem;
			xmlTxt+="</dian:LANGUAGEM>";
		}
		if (this.Visuospat!=null){
			xmlTxt+="\n<dian:VISUOSPAT";
			xmlTxt+=">";
			xmlTxt+=this.Visuospat;
			xmlTxt+="</dian:VISUOSPAT>";
		}
		if (this.Visuospatm!=null){
			xmlTxt+="\n<dian:VISUOSPATM";
			xmlTxt+=">";
			xmlTxt+=this.Visuospatm;
			xmlTxt+="</dian:VISUOSPATM>";
		}
		if (this.Attention!=null){
			xmlTxt+="\n<dian:ATTENTION";
			xmlTxt+=">";
			xmlTxt+=this.Attention;
			xmlTxt+="</dian:ATTENTION>";
		}
		if (this.Attentionm!=null){
			xmlTxt+="\n<dian:ATTENTIONM";
			xmlTxt+=">";
			xmlTxt+=this.Attentionm;
			xmlTxt+="</dian:ATTENTIONM>";
		}
		if (this.Behavior!=null){
			xmlTxt+="\n<dian:BEHAVIOR";
			xmlTxt+=">";
			xmlTxt+=this.Behavior;
			xmlTxt+="</dian:BEHAVIOR>";
		}
		if (this.Behaviorm!=null){
			xmlTxt+="\n<dian:BEHAVIORM";
			xmlTxt+=">";
			xmlTxt+=this.Behaviorm;
			xmlTxt+="</dian:BEHAVIORM>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Amend2!=null) return true;
		if (this.Changes!=null) return true;
		if (this.Memory!=null) return true;
		if (this.Memorym!=null) return true;
		if (this.Judgment!=null) return true;
		if (this.Judgmentm!=null) return true;
		if (this.Language!=null) return true;
		if (this.Languagem!=null) return true;
		if (this.Visuospat!=null) return true;
		if (this.Visuospatm!=null) return true;
		if (this.Attention!=null) return true;
		if (this.Attentionm!=null) return true;
		if (this.Behavior!=null) return true;
		if (this.Behaviorm!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
