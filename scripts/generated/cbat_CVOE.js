/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cbat_CVOE(){
this.xsiType="cbat:CVOE";

	this.getSchemaElementName=function(){
		return "CVOE";
	}

	this.getFullSchemaElementName=function(){
		return "cbat:CVOE";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Cvt1_accuracy=null;


	function getCvt1_accuracy() {
		return this.Cvt1_accuracy;
	}
	this.getCvt1_accuracy=getCvt1_accuracy;


	function setCvt1_accuracy(v){
		this.Cvt1_accuracy=v;
	}
	this.setCvt1_accuracy=setCvt1_accuracy;

	this.Cvt1_responsetime=null;


	function getCvt1_responsetime() {
		return this.Cvt1_responsetime;
	}
	this.getCvt1_responsetime=getCvt1_responsetime;


	function setCvt1_responsetime(v){
		this.Cvt1_responsetime=v;
	}
	this.setCvt1_responsetime=setCvt1_responsetime;

	this.Cvt2_accuracy=null;


	function getCvt2_accuracy() {
		return this.Cvt2_accuracy;
	}
	this.getCvt2_accuracy=getCvt2_accuracy;


	function setCvt2_accuracy(v){
		this.Cvt2_accuracy=v;
	}
	this.setCvt2_accuracy=setCvt2_accuracy;

	this.Cvt2_responsetime=null;


	function getCvt2_responsetime() {
		return this.Cvt2_responsetime;
	}
	this.getCvt2_responsetime=getCvt2_responsetime;


	function setCvt2_responsetime(v){
		this.Cvt2_responsetime=v;
	}
	this.setCvt2_responsetime=setCvt2_responsetime;

	this.Cvt3_accuracy=null;


	function getCvt3_accuracy() {
		return this.Cvt3_accuracy;
	}
	this.getCvt3_accuracy=getCvt3_accuracy;


	function setCvt3_accuracy(v){
		this.Cvt3_accuracy=v;
	}
	this.setCvt3_accuracy=setCvt3_accuracy;

	this.Cvt3_responsetime=null;


	function getCvt3_responsetime() {
		return this.Cvt3_responsetime;
	}
	this.getCvt3_responsetime=getCvt3_responsetime;


	function setCvt3_responsetime(v){
		this.Cvt3_responsetime=v;
	}
	this.setCvt3_responsetime=setCvt3_responsetime;

	this.Cvt4_accuracy=null;


	function getCvt4_accuracy() {
		return this.Cvt4_accuracy;
	}
	this.getCvt4_accuracy=getCvt4_accuracy;


	function setCvt4_accuracy(v){
		this.Cvt4_accuracy=v;
	}
	this.setCvt4_accuracy=setCvt4_accuracy;

	this.Cvt4_responsetime=null;


	function getCvt4_responsetime() {
		return this.Cvt4_responsetime;
	}
	this.getCvt4_responsetime=getCvt4_responsetime;


	function setCvt4_responsetime(v){
		this.Cvt4_responsetime=v;
	}
	this.setCvt4_responsetime=setCvt4_responsetime;

	this.Cvt5_accuracy=null;


	function getCvt5_accuracy() {
		return this.Cvt5_accuracy;
	}
	this.getCvt5_accuracy=getCvt5_accuracy;


	function setCvt5_accuracy(v){
		this.Cvt5_accuracy=v;
	}
	this.setCvt5_accuracy=setCvt5_accuracy;

	this.Cvt5_responsetime=null;


	function getCvt5_responsetime() {
		return this.Cvt5_responsetime;
	}
	this.getCvt5_responsetime=getCvt5_responsetime;


	function setCvt5_responsetime(v){
		this.Cvt5_responsetime=v;
	}
	this.setCvt5_responsetime=setCvt5_responsetime;

	this.Cvt6_accuracy=null;


	function getCvt6_accuracy() {
		return this.Cvt6_accuracy;
	}
	this.getCvt6_accuracy=getCvt6_accuracy;


	function setCvt6_accuracy(v){
		this.Cvt6_accuracy=v;
	}
	this.setCvt6_accuracy=setCvt6_accuracy;

	this.Cvt6_responsetime=null;


	function getCvt6_responsetime() {
		return this.Cvt6_responsetime;
	}
	this.getCvt6_responsetime=getCvt6_responsetime;


	function setCvt6_responsetime(v){
		this.Cvt6_responsetime=v;
	}
	this.setCvt6_responsetime=setCvt6_responsetime;

	this.Cvt7_accuracy=null;


	function getCvt7_accuracy() {
		return this.Cvt7_accuracy;
	}
	this.getCvt7_accuracy=getCvt7_accuracy;


	function setCvt7_accuracy(v){
		this.Cvt7_accuracy=v;
	}
	this.setCvt7_accuracy=setCvt7_accuracy;

	this.Cvt7_responsetime=null;


	function getCvt7_responsetime() {
		return this.Cvt7_responsetime;
	}
	this.getCvt7_responsetime=getCvt7_responsetime;


	function setCvt7_responsetime(v){
		this.Cvt7_responsetime=v;
	}
	this.setCvt7_responsetime=setCvt7_responsetime;

	this.Cvt8_accuracy=null;


	function getCvt8_accuracy() {
		return this.Cvt8_accuracy;
	}
	this.getCvt8_accuracy=getCvt8_accuracy;


	function setCvt8_accuracy(v){
		this.Cvt8_accuracy=v;
	}
	this.setCvt8_accuracy=setCvt8_accuracy;

	this.Cvt8_responsetime=null;


	function getCvt8_responsetime() {
		return this.Cvt8_responsetime;
	}
	this.getCvt8_responsetime=getCvt8_responsetime;


	function setCvt8_responsetime(v){
		this.Cvt8_responsetime=v;
	}
	this.setCvt8_responsetime=setCvt8_responsetime;

	this.Cvt9_accuracy=null;


	function getCvt9_accuracy() {
		return this.Cvt9_accuracy;
	}
	this.getCvt9_accuracy=getCvt9_accuracy;


	function setCvt9_accuracy(v){
		this.Cvt9_accuracy=v;
	}
	this.setCvt9_accuracy=setCvt9_accuracy;

	this.Cvt9_responsetime=null;


	function getCvt9_responsetime() {
		return this.Cvt9_responsetime;
	}
	this.getCvt9_responsetime=getCvt9_responsetime;


	function setCvt9_responsetime(v){
		this.Cvt9_responsetime=v;
	}
	this.setCvt9_responsetime=setCvt9_responsetime;

	this.Cvt10_accuracy=null;


	function getCvt10_accuracy() {
		return this.Cvt10_accuracy;
	}
	this.getCvt10_accuracy=getCvt10_accuracy;


	function setCvt10_accuracy(v){
		this.Cvt10_accuracy=v;
	}
	this.setCvt10_accuracy=setCvt10_accuracy;

	this.Cvt10_responsetime=null;


	function getCvt10_responsetime() {
		return this.Cvt10_responsetime;
	}
	this.getCvt10_responsetime=getCvt10_responsetime;


	function setCvt10_responsetime(v){
		this.Cvt10_responsetime=v;
	}
	this.setCvt10_responsetime=setCvt10_responsetime;

	this.Cvt11_accuracy=null;


	function getCvt11_accuracy() {
		return this.Cvt11_accuracy;
	}
	this.getCvt11_accuracy=getCvt11_accuracy;


	function setCvt11_accuracy(v){
		this.Cvt11_accuracy=v;
	}
	this.setCvt11_accuracy=setCvt11_accuracy;

	this.Cvt11_responsetime=null;


	function getCvt11_responsetime() {
		return this.Cvt11_responsetime;
	}
	this.getCvt11_responsetime=getCvt11_responsetime;


	function setCvt11_responsetime(v){
		this.Cvt11_responsetime=v;
	}
	this.setCvt11_responsetime=setCvt11_responsetime;

	this.Cvt12_accuracy=null;


	function getCvt12_accuracy() {
		return this.Cvt12_accuracy;
	}
	this.getCvt12_accuracy=getCvt12_accuracy;


	function setCvt12_accuracy(v){
		this.Cvt12_accuracy=v;
	}
	this.setCvt12_accuracy=setCvt12_accuracy;

	this.Cvt12_responsetime=null;


	function getCvt12_responsetime() {
		return this.Cvt12_responsetime;
	}
	this.getCvt12_responsetime=getCvt12_responsetime;


	function setCvt12_responsetime(v){
		this.Cvt12_responsetime=v;
	}
	this.setCvt12_responsetime=setCvt12_responsetime;

	this.Cvt13_accuracy=null;


	function getCvt13_accuracy() {
		return this.Cvt13_accuracy;
	}
	this.getCvt13_accuracy=getCvt13_accuracy;


	function setCvt13_accuracy(v){
		this.Cvt13_accuracy=v;
	}
	this.setCvt13_accuracy=setCvt13_accuracy;

	this.Cvt13_responsetime=null;


	function getCvt13_responsetime() {
		return this.Cvt13_responsetime;
	}
	this.getCvt13_responsetime=getCvt13_responsetime;


	function setCvt13_responsetime(v){
		this.Cvt13_responsetime=v;
	}
	this.setCvt13_responsetime=setCvt13_responsetime;

	this.Cvt14_accuracy=null;


	function getCvt14_accuracy() {
		return this.Cvt14_accuracy;
	}
	this.getCvt14_accuracy=getCvt14_accuracy;


	function setCvt14_accuracy(v){
		this.Cvt14_accuracy=v;
	}
	this.setCvt14_accuracy=setCvt14_accuracy;

	this.Cvt14_responsetime=null;


	function getCvt14_responsetime() {
		return this.Cvt14_responsetime;
	}
	this.getCvt14_responsetime=getCvt14_responsetime;


	function setCvt14_responsetime(v){
		this.Cvt14_responsetime=v;
	}
	this.setCvt14_responsetime=setCvt14_responsetime;

	this.Cvt15_accuracy=null;


	function getCvt15_accuracy() {
		return this.Cvt15_accuracy;
	}
	this.getCvt15_accuracy=getCvt15_accuracy;


	function setCvt15_accuracy(v){
		this.Cvt15_accuracy=v;
	}
	this.setCvt15_accuracy=setCvt15_accuracy;

	this.Cvt15_responsetime=null;


	function getCvt15_responsetime() {
		return this.Cvt15_responsetime;
	}
	this.getCvt15_responsetime=getCvt15_responsetime;


	function setCvt15_responsetime(v){
		this.Cvt15_responsetime=v;
	}
	this.setCvt15_responsetime=setCvt15_responsetime;

	this.Cvt16_accuracy=null;


	function getCvt16_accuracy() {
		return this.Cvt16_accuracy;
	}
	this.getCvt16_accuracy=getCvt16_accuracy;


	function setCvt16_accuracy(v){
		this.Cvt16_accuracy=v;
	}
	this.setCvt16_accuracy=setCvt16_accuracy;

	this.Cvt16_responsetime=null;


	function getCvt16_responsetime() {
		return this.Cvt16_responsetime;
	}
	this.getCvt16_responsetime=getCvt16_responsetime;


	function setCvt16_responsetime(v){
		this.Cvt16_responsetime=v;
	}
	this.setCvt16_responsetime=setCvt16_responsetime;

	this.Cvt17_accuracy=null;


	function getCvt17_accuracy() {
		return this.Cvt17_accuracy;
	}
	this.getCvt17_accuracy=getCvt17_accuracy;


	function setCvt17_accuracy(v){
		this.Cvt17_accuracy=v;
	}
	this.setCvt17_accuracy=setCvt17_accuracy;

	this.Cvt17_responsetime=null;


	function getCvt17_responsetime() {
		return this.Cvt17_responsetime;
	}
	this.getCvt17_responsetime=getCvt17_responsetime;


	function setCvt17_responsetime(v){
		this.Cvt17_responsetime=v;
	}
	this.setCvt17_responsetime=setCvt17_responsetime;

	this.Cvt18_accuracy=null;


	function getCvt18_accuracy() {
		return this.Cvt18_accuracy;
	}
	this.getCvt18_accuracy=getCvt18_accuracy;


	function setCvt18_accuracy(v){
		this.Cvt18_accuracy=v;
	}
	this.setCvt18_accuracy=setCvt18_accuracy;

	this.Cvt18_responsetime=null;


	function getCvt18_responsetime() {
		return this.Cvt18_responsetime;
	}
	this.getCvt18_responsetime=getCvt18_responsetime;


	function setCvt18_responsetime(v){
		this.Cvt18_responsetime=v;
	}
	this.setCvt18_responsetime=setCvt18_responsetime;

	this.Cvt19_accuracy=null;


	function getCvt19_accuracy() {
		return this.Cvt19_accuracy;
	}
	this.getCvt19_accuracy=getCvt19_accuracy;


	function setCvt19_accuracy(v){
		this.Cvt19_accuracy=v;
	}
	this.setCvt19_accuracy=setCvt19_accuracy;

	this.Cvt19_responsetime=null;


	function getCvt19_responsetime() {
		return this.Cvt19_responsetime;
	}
	this.getCvt19_responsetime=getCvt19_responsetime;


	function setCvt19_responsetime(v){
		this.Cvt19_responsetime=v;
	}
	this.setCvt19_responsetime=setCvt19_responsetime;

	this.Cvt20_accuracy=null;


	function getCvt20_accuracy() {
		return this.Cvt20_accuracy;
	}
	this.getCvt20_accuracy=getCvt20_accuracy;


	function setCvt20_accuracy(v){
		this.Cvt20_accuracy=v;
	}
	this.setCvt20_accuracy=setCvt20_accuracy;

	this.Cvt20_responsetime=null;


	function getCvt20_responsetime() {
		return this.Cvt20_responsetime;
	}
	this.getCvt20_responsetime=getCvt20_responsetime;


	function setCvt20_responsetime(v){
		this.Cvt20_responsetime=v;
	}
	this.setCvt20_responsetime=setCvt20_responsetime;

	this.Cvt21_accuracy=null;


	function getCvt21_accuracy() {
		return this.Cvt21_accuracy;
	}
	this.getCvt21_accuracy=getCvt21_accuracy;


	function setCvt21_accuracy(v){
		this.Cvt21_accuracy=v;
	}
	this.setCvt21_accuracy=setCvt21_accuracy;

	this.Cvt21_responsetime=null;


	function getCvt21_responsetime() {
		return this.Cvt21_responsetime;
	}
	this.getCvt21_responsetime=getCvt21_responsetime;


	function setCvt21_responsetime(v){
		this.Cvt21_responsetime=v;
	}
	this.setCvt21_responsetime=setCvt21_responsetime;

	this.Cvt22_accuracy=null;


	function getCvt22_accuracy() {
		return this.Cvt22_accuracy;
	}
	this.getCvt22_accuracy=getCvt22_accuracy;


	function setCvt22_accuracy(v){
		this.Cvt22_accuracy=v;
	}
	this.setCvt22_accuracy=setCvt22_accuracy;

	this.Cvt22_responsetime=null;


	function getCvt22_responsetime() {
		return this.Cvt22_responsetime;
	}
	this.getCvt22_responsetime=getCvt22_responsetime;


	function setCvt22_responsetime(v){
		this.Cvt22_responsetime=v;
	}
	this.setCvt22_responsetime=setCvt22_responsetime;

	this.Cvt23_accuracy=null;


	function getCvt23_accuracy() {
		return this.Cvt23_accuracy;
	}
	this.getCvt23_accuracy=getCvt23_accuracy;


	function setCvt23_accuracy(v){
		this.Cvt23_accuracy=v;
	}
	this.setCvt23_accuracy=setCvt23_accuracy;

	this.Cvt23_responsetime=null;


	function getCvt23_responsetime() {
		return this.Cvt23_responsetime;
	}
	this.getCvt23_responsetime=getCvt23_responsetime;


	function setCvt23_responsetime(v){
		this.Cvt23_responsetime=v;
	}
	this.setCvt23_responsetime=setCvt23_responsetime;

	this.Cvt24_accuracy=null;


	function getCvt24_accuracy() {
		return this.Cvt24_accuracy;
	}
	this.getCvt24_accuracy=getCvt24_accuracy;


	function setCvt24_accuracy(v){
		this.Cvt24_accuracy=v;
	}
	this.setCvt24_accuracy=setCvt24_accuracy;

	this.Cvt24_responsetime=null;


	function getCvt24_responsetime() {
		return this.Cvt24_responsetime;
	}
	this.getCvt24_responsetime=getCvt24_responsetime;


	function setCvt24_responsetime(v){
		this.Cvt24_responsetime=v;
	}
	this.setCvt24_responsetime=setCvt24_responsetime;

	this.Cvt25_accuracy=null;


	function getCvt25_accuracy() {
		return this.Cvt25_accuracy;
	}
	this.getCvt25_accuracy=getCvt25_accuracy;


	function setCvt25_accuracy(v){
		this.Cvt25_accuracy=v;
	}
	this.setCvt25_accuracy=setCvt25_accuracy;

	this.Cvt25_responsetime=null;


	function getCvt25_responsetime() {
		return this.Cvt25_responsetime;
	}
	this.getCvt25_responsetime=getCvt25_responsetime;


	function setCvt25_responsetime(v){
		this.Cvt25_responsetime=v;
	}
	this.setCvt25_responsetime=setCvt25_responsetime;

	this.Cvt26_accuracy=null;


	function getCvt26_accuracy() {
		return this.Cvt26_accuracy;
	}
	this.getCvt26_accuracy=getCvt26_accuracy;


	function setCvt26_accuracy(v){
		this.Cvt26_accuracy=v;
	}
	this.setCvt26_accuracy=setCvt26_accuracy;

	this.Cvt26_responsetime=null;


	function getCvt26_responsetime() {
		return this.Cvt26_responsetime;
	}
	this.getCvt26_responsetime=getCvt26_responsetime;


	function setCvt26_responsetime(v){
		this.Cvt26_responsetime=v;
	}
	this.setCvt26_responsetime=setCvt26_responsetime;

	this.Cvt27_accuracy=null;


	function getCvt27_accuracy() {
		return this.Cvt27_accuracy;
	}
	this.getCvt27_accuracy=getCvt27_accuracy;


	function setCvt27_accuracy(v){
		this.Cvt27_accuracy=v;
	}
	this.setCvt27_accuracy=setCvt27_accuracy;

	this.Cvt27_responsetime=null;


	function getCvt27_responsetime() {
		return this.Cvt27_responsetime;
	}
	this.getCvt27_responsetime=getCvt27_responsetime;


	function setCvt27_responsetime(v){
		this.Cvt27_responsetime=v;
	}
	this.setCvt27_responsetime=setCvt27_responsetime;

	this.Cvt28_accuracy=null;


	function getCvt28_accuracy() {
		return this.Cvt28_accuracy;
	}
	this.getCvt28_accuracy=getCvt28_accuracy;


	function setCvt28_accuracy(v){
		this.Cvt28_accuracy=v;
	}
	this.setCvt28_accuracy=setCvt28_accuracy;

	this.Cvt28_responsetime=null;


	function getCvt28_responsetime() {
		return this.Cvt28_responsetime;
	}
	this.getCvt28_responsetime=getCvt28_responsetime;


	function setCvt28_responsetime(v){
		this.Cvt28_responsetime=v;
	}
	this.setCvt28_responsetime=setCvt28_responsetime;

	this.Cvt29_accuracy=null;


	function getCvt29_accuracy() {
		return this.Cvt29_accuracy;
	}
	this.getCvt29_accuracy=getCvt29_accuracy;


	function setCvt29_accuracy(v){
		this.Cvt29_accuracy=v;
	}
	this.setCvt29_accuracy=setCvt29_accuracy;

	this.Cvt29_responsetime=null;


	function getCvt29_responsetime() {
		return this.Cvt29_responsetime;
	}
	this.getCvt29_responsetime=getCvt29_responsetime;


	function setCvt29_responsetime(v){
		this.Cvt29_responsetime=v;
	}
	this.setCvt29_responsetime=setCvt29_responsetime;

	this.Cvt30_accuracy=null;


	function getCvt30_accuracy() {
		return this.Cvt30_accuracy;
	}
	this.getCvt30_accuracy=getCvt30_accuracy;


	function setCvt30_accuracy(v){
		this.Cvt30_accuracy=v;
	}
	this.setCvt30_accuracy=setCvt30_accuracy;

	this.Cvt30_responsetime=null;


	function getCvt30_responsetime() {
		return this.Cvt30_responsetime;
	}
	this.getCvt30_responsetime=getCvt30_responsetime;


	function setCvt30_responsetime(v){
		this.Cvt30_responsetime=v;
	}
	this.setCvt30_responsetime=setCvt30_responsetime;

	this.Cvt31_accuracy=null;


	function getCvt31_accuracy() {
		return this.Cvt31_accuracy;
	}
	this.getCvt31_accuracy=getCvt31_accuracy;


	function setCvt31_accuracy(v){
		this.Cvt31_accuracy=v;
	}
	this.setCvt31_accuracy=setCvt31_accuracy;

	this.Cvt31_responsetime=null;


	function getCvt31_responsetime() {
		return this.Cvt31_responsetime;
	}
	this.getCvt31_responsetime=getCvt31_responsetime;


	function setCvt31_responsetime(v){
		this.Cvt31_responsetime=v;
	}
	this.setCvt31_responsetime=setCvt31_responsetime;

	this.Cvt32_accuracy=null;


	function getCvt32_accuracy() {
		return this.Cvt32_accuracy;
	}
	this.getCvt32_accuracy=getCvt32_accuracy;


	function setCvt32_accuracy(v){
		this.Cvt32_accuracy=v;
	}
	this.setCvt32_accuracy=setCvt32_accuracy;

	this.Cvt32_responsetime=null;


	function getCvt32_responsetime() {
		return this.Cvt32_responsetime;
	}
	this.getCvt32_responsetime=getCvt32_responsetime;


	function setCvt32_responsetime(v){
		this.Cvt32_responsetime=v;
	}
	this.setCvt32_responsetime=setCvt32_responsetime;

	this.Cvt33_accuracy=null;


	function getCvt33_accuracy() {
		return this.Cvt33_accuracy;
	}
	this.getCvt33_accuracy=getCvt33_accuracy;


	function setCvt33_accuracy(v){
		this.Cvt33_accuracy=v;
	}
	this.setCvt33_accuracy=setCvt33_accuracy;

	this.Cvt33_responsetime=null;


	function getCvt33_responsetime() {
		return this.Cvt33_responsetime;
	}
	this.getCvt33_responsetime=getCvt33_responsetime;


	function setCvt33_responsetime(v){
		this.Cvt33_responsetime=v;
	}
	this.setCvt33_responsetime=setCvt33_responsetime;

	this.Cvt34_accuracy=null;


	function getCvt34_accuracy() {
		return this.Cvt34_accuracy;
	}
	this.getCvt34_accuracy=getCvt34_accuracy;


	function setCvt34_accuracy(v){
		this.Cvt34_accuracy=v;
	}
	this.setCvt34_accuracy=setCvt34_accuracy;

	this.Cvt34_responsetime=null;


	function getCvt34_responsetime() {
		return this.Cvt34_responsetime;
	}
	this.getCvt34_responsetime=getCvt34_responsetime;


	function setCvt34_responsetime(v){
		this.Cvt34_responsetime=v;
	}
	this.setCvt34_responsetime=setCvt34_responsetime;

	this.Cvt35_accuracy=null;


	function getCvt35_accuracy() {
		return this.Cvt35_accuracy;
	}
	this.getCvt35_accuracy=getCvt35_accuracy;


	function setCvt35_accuracy(v){
		this.Cvt35_accuracy=v;
	}
	this.setCvt35_accuracy=setCvt35_accuracy;

	this.Cvt35_responsetime=null;


	function getCvt35_responsetime() {
		return this.Cvt35_responsetime;
	}
	this.getCvt35_responsetime=getCvt35_responsetime;


	function setCvt35_responsetime(v){
		this.Cvt35_responsetime=v;
	}
	this.setCvt35_responsetime=setCvt35_responsetime;

	this.Cvt36_accuracy=null;


	function getCvt36_accuracy() {
		return this.Cvt36_accuracy;
	}
	this.getCvt36_accuracy=getCvt36_accuracy;


	function setCvt36_accuracy(v){
		this.Cvt36_accuracy=v;
	}
	this.setCvt36_accuracy=setCvt36_accuracy;

	this.Cvt36_responsetime=null;


	function getCvt36_responsetime() {
		return this.Cvt36_responsetime;
	}
	this.getCvt36_responsetime=getCvt36_responsetime;


	function setCvt36_responsetime(v){
		this.Cvt36_responsetime=v;
	}
	this.setCvt36_responsetime=setCvt36_responsetime;

	this.Cvt37_accuracy=null;


	function getCvt37_accuracy() {
		return this.Cvt37_accuracy;
	}
	this.getCvt37_accuracy=getCvt37_accuracy;


	function setCvt37_accuracy(v){
		this.Cvt37_accuracy=v;
	}
	this.setCvt37_accuracy=setCvt37_accuracy;

	this.Cvt37_responsetime=null;


	function getCvt37_responsetime() {
		return this.Cvt37_responsetime;
	}
	this.getCvt37_responsetime=getCvt37_responsetime;


	function setCvt37_responsetime(v){
		this.Cvt37_responsetime=v;
	}
	this.setCvt37_responsetime=setCvt37_responsetime;

	this.Cvt38_accuracy=null;


	function getCvt38_accuracy() {
		return this.Cvt38_accuracy;
	}
	this.getCvt38_accuracy=getCvt38_accuracy;


	function setCvt38_accuracy(v){
		this.Cvt38_accuracy=v;
	}
	this.setCvt38_accuracy=setCvt38_accuracy;

	this.Cvt38_responsetime=null;


	function getCvt38_responsetime() {
		return this.Cvt38_responsetime;
	}
	this.getCvt38_responsetime=getCvt38_responsetime;


	function setCvt38_responsetime(v){
		this.Cvt38_responsetime=v;
	}
	this.setCvt38_responsetime=setCvt38_responsetime;

	this.Cvt39_accuracy=null;


	function getCvt39_accuracy() {
		return this.Cvt39_accuracy;
	}
	this.getCvt39_accuracy=getCvt39_accuracy;


	function setCvt39_accuracy(v){
		this.Cvt39_accuracy=v;
	}
	this.setCvt39_accuracy=setCvt39_accuracy;

	this.Cvt39_responsetime=null;


	function getCvt39_responsetime() {
		return this.Cvt39_responsetime;
	}
	this.getCvt39_responsetime=getCvt39_responsetime;


	function setCvt39_responsetime(v){
		this.Cvt39_responsetime=v;
	}
	this.setCvt39_responsetime=setCvt39_responsetime;

	this.Cvt40_accuracy=null;


	function getCvt40_accuracy() {
		return this.Cvt40_accuracy;
	}
	this.getCvt40_accuracy=getCvt40_accuracy;


	function setCvt40_accuracy(v){
		this.Cvt40_accuracy=v;
	}
	this.setCvt40_accuracy=setCvt40_accuracy;

	this.Cvt40_responsetime=null;


	function getCvt40_responsetime() {
		return this.Cvt40_responsetime;
	}
	this.getCvt40_responsetime=getCvt40_responsetime;


	function setCvt40_responsetime(v){
		this.Cvt40_responsetime=v;
	}
	this.setCvt40_responsetime=setCvt40_responsetime;

	this.Oet1_accuracy=null;


	function getOet1_accuracy() {
		return this.Oet1_accuracy;
	}
	this.getOet1_accuracy=getOet1_accuracy;


	function setOet1_accuracy(v){
		this.Oet1_accuracy=v;
	}
	this.setOet1_accuracy=setOet1_accuracy;

	this.Oet1_responsetime=null;


	function getOet1_responsetime() {
		return this.Oet1_responsetime;
	}
	this.getOet1_responsetime=getOet1_responsetime;


	function setOet1_responsetime(v){
		this.Oet1_responsetime=v;
	}
	this.setOet1_responsetime=setOet1_responsetime;

	this.Oet2_accuracy=null;


	function getOet2_accuracy() {
		return this.Oet2_accuracy;
	}
	this.getOet2_accuracy=getOet2_accuracy;


	function setOet2_accuracy(v){
		this.Oet2_accuracy=v;
	}
	this.setOet2_accuracy=setOet2_accuracy;

	this.Oet2_responsetime=null;


	function getOet2_responsetime() {
		return this.Oet2_responsetime;
	}
	this.getOet2_responsetime=getOet2_responsetime;


	function setOet2_responsetime(v){
		this.Oet2_responsetime=v;
	}
	this.setOet2_responsetime=setOet2_responsetime;

	this.Oet3_accuracy=null;


	function getOet3_accuracy() {
		return this.Oet3_accuracy;
	}
	this.getOet3_accuracy=getOet3_accuracy;


	function setOet3_accuracy(v){
		this.Oet3_accuracy=v;
	}
	this.setOet3_accuracy=setOet3_accuracy;

	this.Oet3_responsetime=null;


	function getOet3_responsetime() {
		return this.Oet3_responsetime;
	}
	this.getOet3_responsetime=getOet3_responsetime;


	function setOet3_responsetime(v){
		this.Oet3_responsetime=v;
	}
	this.setOet3_responsetime=setOet3_responsetime;

	this.Oet4_accuracy=null;


	function getOet4_accuracy() {
		return this.Oet4_accuracy;
	}
	this.getOet4_accuracy=getOet4_accuracy;


	function setOet4_accuracy(v){
		this.Oet4_accuracy=v;
	}
	this.setOet4_accuracy=setOet4_accuracy;

	this.Oet4_responsetime=null;


	function getOet4_responsetime() {
		return this.Oet4_responsetime;
	}
	this.getOet4_responsetime=getOet4_responsetime;


	function setOet4_responsetime(v){
		this.Oet4_responsetime=v;
	}
	this.setOet4_responsetime=setOet4_responsetime;

	this.Oet5_accuracy=null;


	function getOet5_accuracy() {
		return this.Oet5_accuracy;
	}
	this.getOet5_accuracy=getOet5_accuracy;


	function setOet5_accuracy(v){
		this.Oet5_accuracy=v;
	}
	this.setOet5_accuracy=setOet5_accuracy;

	this.Oet5_responsetime=null;


	function getOet5_responsetime() {
		return this.Oet5_responsetime;
	}
	this.getOet5_responsetime=getOet5_responsetime;


	function setOet5_responsetime(v){
		this.Oet5_responsetime=v;
	}
	this.setOet5_responsetime=setOet5_responsetime;

	this.Oet6_accuracy=null;


	function getOet6_accuracy() {
		return this.Oet6_accuracy;
	}
	this.getOet6_accuracy=getOet6_accuracy;


	function setOet6_accuracy(v){
		this.Oet6_accuracy=v;
	}
	this.setOet6_accuracy=setOet6_accuracy;

	this.Oet6_responsetime=null;


	function getOet6_responsetime() {
		return this.Oet6_responsetime;
	}
	this.getOet6_responsetime=getOet6_responsetime;


	function setOet6_responsetime(v){
		this.Oet6_responsetime=v;
	}
	this.setOet6_responsetime=setOet6_responsetime;

	this.Oet7_accuracy=null;


	function getOet7_accuracy() {
		return this.Oet7_accuracy;
	}
	this.getOet7_accuracy=getOet7_accuracy;


	function setOet7_accuracy(v){
		this.Oet7_accuracy=v;
	}
	this.setOet7_accuracy=setOet7_accuracy;

	this.Oet7_responsetime=null;


	function getOet7_responsetime() {
		return this.Oet7_responsetime;
	}
	this.getOet7_responsetime=getOet7_responsetime;


	function setOet7_responsetime(v){
		this.Oet7_responsetime=v;
	}
	this.setOet7_responsetime=setOet7_responsetime;

	this.Oet8_accuracy=null;


	function getOet8_accuracy() {
		return this.Oet8_accuracy;
	}
	this.getOet8_accuracy=getOet8_accuracy;


	function setOet8_accuracy(v){
		this.Oet8_accuracy=v;
	}
	this.setOet8_accuracy=setOet8_accuracy;

	this.Oet8_responsetime=null;


	function getOet8_responsetime() {
		return this.Oet8_responsetime;
	}
	this.getOet8_responsetime=getOet8_responsetime;


	function setOet8_responsetime(v){
		this.Oet8_responsetime=v;
	}
	this.setOet8_responsetime=setOet8_responsetime;

	this.Oet9_accuracy=null;


	function getOet9_accuracy() {
		return this.Oet9_accuracy;
	}
	this.getOet9_accuracy=getOet9_accuracy;


	function setOet9_accuracy(v){
		this.Oet9_accuracy=v;
	}
	this.setOet9_accuracy=setOet9_accuracy;

	this.Oet9_responsetime=null;


	function getOet9_responsetime() {
		return this.Oet9_responsetime;
	}
	this.getOet9_responsetime=getOet9_responsetime;


	function setOet9_responsetime(v){
		this.Oet9_responsetime=v;
	}
	this.setOet9_responsetime=setOet9_responsetime;

	this.Oet10_accuracy=null;


	function getOet10_accuracy() {
		return this.Oet10_accuracy;
	}
	this.getOet10_accuracy=getOet10_accuracy;


	function setOet10_accuracy(v){
		this.Oet10_accuracy=v;
	}
	this.setOet10_accuracy=setOet10_accuracy;

	this.Oet10_responsetime=null;


	function getOet10_responsetime() {
		return this.Oet10_responsetime;
	}
	this.getOet10_responsetime=getOet10_responsetime;


	function setOet10_responsetime(v){
		this.Oet10_responsetime=v;
	}
	this.setOet10_responsetime=setOet10_responsetime;

	this.Oet11_accuracy=null;


	function getOet11_accuracy() {
		return this.Oet11_accuracy;
	}
	this.getOet11_accuracy=getOet11_accuracy;


	function setOet11_accuracy(v){
		this.Oet11_accuracy=v;
	}
	this.setOet11_accuracy=setOet11_accuracy;

	this.Oet11_responsetime=null;


	function getOet11_responsetime() {
		return this.Oet11_responsetime;
	}
	this.getOet11_responsetime=getOet11_responsetime;


	function setOet11_responsetime(v){
		this.Oet11_responsetime=v;
	}
	this.setOet11_responsetime=setOet11_responsetime;

	this.Oet12_accuracy=null;


	function getOet12_accuracy() {
		return this.Oet12_accuracy;
	}
	this.getOet12_accuracy=getOet12_accuracy;


	function setOet12_accuracy(v){
		this.Oet12_accuracy=v;
	}
	this.setOet12_accuracy=setOet12_accuracy;

	this.Oet12_responsetime=null;


	function getOet12_responsetime() {
		return this.Oet12_responsetime;
	}
	this.getOet12_responsetime=getOet12_responsetime;


	function setOet12_responsetime(v){
		this.Oet12_responsetime=v;
	}
	this.setOet12_responsetime=setOet12_responsetime;

	this.Oet13_accuracy=null;


	function getOet13_accuracy() {
		return this.Oet13_accuracy;
	}
	this.getOet13_accuracy=getOet13_accuracy;


	function setOet13_accuracy(v){
		this.Oet13_accuracy=v;
	}
	this.setOet13_accuracy=setOet13_accuracy;

	this.Oet13_responsetime=null;


	function getOet13_responsetime() {
		return this.Oet13_responsetime;
	}
	this.getOet13_responsetime=getOet13_responsetime;


	function setOet13_responsetime(v){
		this.Oet13_responsetime=v;
	}
	this.setOet13_responsetime=setOet13_responsetime;

	this.Oet14_accuracy=null;


	function getOet14_accuracy() {
		return this.Oet14_accuracy;
	}
	this.getOet14_accuracy=getOet14_accuracy;


	function setOet14_accuracy(v){
		this.Oet14_accuracy=v;
	}
	this.setOet14_accuracy=setOet14_accuracy;

	this.Oet14_responsetime=null;


	function getOet14_responsetime() {
		return this.Oet14_responsetime;
	}
	this.getOet14_responsetime=getOet14_responsetime;


	function setOet14_responsetime(v){
		this.Oet14_responsetime=v;
	}
	this.setOet14_responsetime=setOet14_responsetime;

	this.Oet15_accuracy=null;


	function getOet15_accuracy() {
		return this.Oet15_accuracy;
	}
	this.getOet15_accuracy=getOet15_accuracy;


	function setOet15_accuracy(v){
		this.Oet15_accuracy=v;
	}
	this.setOet15_accuracy=setOet15_accuracy;

	this.Oet15_responsetime=null;


	function getOet15_responsetime() {
		return this.Oet15_responsetime;
	}
	this.getOet15_responsetime=getOet15_responsetime;


	function setOet15_responsetime(v){
		this.Oet15_responsetime=v;
	}
	this.setOet15_responsetime=setOet15_responsetime;

	this.Oet16_accuracy=null;


	function getOet16_accuracy() {
		return this.Oet16_accuracy;
	}
	this.getOet16_accuracy=getOet16_accuracy;


	function setOet16_accuracy(v){
		this.Oet16_accuracy=v;
	}
	this.setOet16_accuracy=setOet16_accuracy;

	this.Oet16_responsetime=null;


	function getOet16_responsetime() {
		return this.Oet16_responsetime;
	}
	this.getOet16_responsetime=getOet16_responsetime;


	function setOet16_responsetime(v){
		this.Oet16_responsetime=v;
	}
	this.setOet16_responsetime=setOet16_responsetime;

	this.Oet17_accuracy=null;


	function getOet17_accuracy() {
		return this.Oet17_accuracy;
	}
	this.getOet17_accuracy=getOet17_accuracy;


	function setOet17_accuracy(v){
		this.Oet17_accuracy=v;
	}
	this.setOet17_accuracy=setOet17_accuracy;

	this.Oet17_responsetime=null;


	function getOet17_responsetime() {
		return this.Oet17_responsetime;
	}
	this.getOet17_responsetime=getOet17_responsetime;


	function setOet17_responsetime(v){
		this.Oet17_responsetime=v;
	}
	this.setOet17_responsetime=setOet17_responsetime;

	this.Oet18_accuracy=null;


	function getOet18_accuracy() {
		return this.Oet18_accuracy;
	}
	this.getOet18_accuracy=getOet18_accuracy;


	function setOet18_accuracy(v){
		this.Oet18_accuracy=v;
	}
	this.setOet18_accuracy=setOet18_accuracy;

	this.Oet18_responsetime=null;


	function getOet18_responsetime() {
		return this.Oet18_responsetime;
	}
	this.getOet18_responsetime=getOet18_responsetime;


	function setOet18_responsetime(v){
		this.Oet18_responsetime=v;
	}
	this.setOet18_responsetime=setOet18_responsetime;

	this.Oet19_accuracy=null;


	function getOet19_accuracy() {
		return this.Oet19_accuracy;
	}
	this.getOet19_accuracy=getOet19_accuracy;


	function setOet19_accuracy(v){
		this.Oet19_accuracy=v;
	}
	this.setOet19_accuracy=setOet19_accuracy;

	this.Oet19_responsetime=null;


	function getOet19_responsetime() {
		return this.Oet19_responsetime;
	}
	this.getOet19_responsetime=getOet19_responsetime;


	function setOet19_responsetime(v){
		this.Oet19_responsetime=v;
	}
	this.setOet19_responsetime=setOet19_responsetime;

	this.Oet20_accuracy=null;


	function getOet20_accuracy() {
		return this.Oet20_accuracy;
	}
	this.getOet20_accuracy=getOet20_accuracy;


	function setOet20_accuracy(v){
		this.Oet20_accuracy=v;
	}
	this.setOet20_accuracy=setOet20_accuracy;

	this.Oet20_responsetime=null;


	function getOet20_responsetime() {
		return this.Oet20_responsetime;
	}
	this.getOet20_responsetime=getOet20_responsetime;


	function setOet20_responsetime(v){
		this.Oet20_responsetime=v;
	}
	this.setOet20_responsetime=setOet20_responsetime;

	this.Oet21_accuracy=null;


	function getOet21_accuracy() {
		return this.Oet21_accuracy;
	}
	this.getOet21_accuracy=getOet21_accuracy;


	function setOet21_accuracy(v){
		this.Oet21_accuracy=v;
	}
	this.setOet21_accuracy=setOet21_accuracy;

	this.Oet21_responsetime=null;


	function getOet21_responsetime() {
		return this.Oet21_responsetime;
	}
	this.getOet21_responsetime=getOet21_responsetime;


	function setOet21_responsetime(v){
		this.Oet21_responsetime=v;
	}
	this.setOet21_responsetime=setOet21_responsetime;

	this.Oet22_accuracy=null;


	function getOet22_accuracy() {
		return this.Oet22_accuracy;
	}
	this.getOet22_accuracy=getOet22_accuracy;


	function setOet22_accuracy(v){
		this.Oet22_accuracy=v;
	}
	this.setOet22_accuracy=setOet22_accuracy;

	this.Oet22_responsetime=null;


	function getOet22_responsetime() {
		return this.Oet22_responsetime;
	}
	this.getOet22_responsetime=getOet22_responsetime;


	function setOet22_responsetime(v){
		this.Oet22_responsetime=v;
	}
	this.setOet22_responsetime=setOet22_responsetime;

	this.Oet23_accuracy=null;


	function getOet23_accuracy() {
		return this.Oet23_accuracy;
	}
	this.getOet23_accuracy=getOet23_accuracy;


	function setOet23_accuracy(v){
		this.Oet23_accuracy=v;
	}
	this.setOet23_accuracy=setOet23_accuracy;

	this.Oet23_responsetime=null;


	function getOet23_responsetime() {
		return this.Oet23_responsetime;
	}
	this.getOet23_responsetime=getOet23_responsetime;


	function setOet23_responsetime(v){
		this.Oet23_responsetime=v;
	}
	this.setOet23_responsetime=setOet23_responsetime;

	this.Oet24_accuracy=null;


	function getOet24_accuracy() {
		return this.Oet24_accuracy;
	}
	this.getOet24_accuracy=getOet24_accuracy;


	function setOet24_accuracy(v){
		this.Oet24_accuracy=v;
	}
	this.setOet24_accuracy=setOet24_accuracy;

	this.Oet24_responsetime=null;


	function getOet24_responsetime() {
		return this.Oet24_responsetime;
	}
	this.getOet24_responsetime=getOet24_responsetime;


	function setOet24_responsetime(v){
		this.Oet24_responsetime=v;
	}
	this.setOet24_responsetime=setOet24_responsetime;

	this.Oet25_accuracy=null;


	function getOet25_accuracy() {
		return this.Oet25_accuracy;
	}
	this.getOet25_accuracy=getOet25_accuracy;


	function setOet25_accuracy(v){
		this.Oet25_accuracy=v;
	}
	this.setOet25_accuracy=setOet25_accuracy;

	this.Oet25_responsetime=null;


	function getOet25_responsetime() {
		return this.Oet25_responsetime;
	}
	this.getOet25_responsetime=getOet25_responsetime;


	function setOet25_responsetime(v){
		this.Oet25_responsetime=v;
	}
	this.setOet25_responsetime=setOet25_responsetime;

	this.Oet26_accuracy=null;


	function getOet26_accuracy() {
		return this.Oet26_accuracy;
	}
	this.getOet26_accuracy=getOet26_accuracy;


	function setOet26_accuracy(v){
		this.Oet26_accuracy=v;
	}
	this.setOet26_accuracy=setOet26_accuracy;

	this.Oet26_responsetime=null;


	function getOet26_responsetime() {
		return this.Oet26_responsetime;
	}
	this.getOet26_responsetime=getOet26_responsetime;


	function setOet26_responsetime(v){
		this.Oet26_responsetime=v;
	}
	this.setOet26_responsetime=setOet26_responsetime;

	this.Oet27_accuracy=null;


	function getOet27_accuracy() {
		return this.Oet27_accuracy;
	}
	this.getOet27_accuracy=getOet27_accuracy;


	function setOet27_accuracy(v){
		this.Oet27_accuracy=v;
	}
	this.setOet27_accuracy=setOet27_accuracy;

	this.Oet27_responsetime=null;


	function getOet27_responsetime() {
		return this.Oet27_responsetime;
	}
	this.getOet27_responsetime=getOet27_responsetime;


	function setOet27_responsetime(v){
		this.Oet27_responsetime=v;
	}
	this.setOet27_responsetime=setOet27_responsetime;

	this.Oet28_accuracy=null;


	function getOet28_accuracy() {
		return this.Oet28_accuracy;
	}
	this.getOet28_accuracy=getOet28_accuracy;


	function setOet28_accuracy(v){
		this.Oet28_accuracy=v;
	}
	this.setOet28_accuracy=setOet28_accuracy;

	this.Oet28_responsetime=null;


	function getOet28_responsetime() {
		return this.Oet28_responsetime;
	}
	this.getOet28_responsetime=getOet28_responsetime;


	function setOet28_responsetime(v){
		this.Oet28_responsetime=v;
	}
	this.setOet28_responsetime=setOet28_responsetime;

	this.Oet29_accuracy=null;


	function getOet29_accuracy() {
		return this.Oet29_accuracy;
	}
	this.getOet29_accuracy=getOet29_accuracy;


	function setOet29_accuracy(v){
		this.Oet29_accuracy=v;
	}
	this.setOet29_accuracy=setOet29_accuracy;

	this.Oet29_responsetime=null;


	function getOet29_responsetime() {
		return this.Oet29_responsetime;
	}
	this.getOet29_responsetime=getOet29_responsetime;


	function setOet29_responsetime(v){
		this.Oet29_responsetime=v;
	}
	this.setOet29_responsetime=setOet29_responsetime;

	this.Oet30_accuracy=null;


	function getOet30_accuracy() {
		return this.Oet30_accuracy;
	}
	this.getOet30_accuracy=getOet30_accuracy;


	function setOet30_accuracy(v){
		this.Oet30_accuracy=v;
	}
	this.setOet30_accuracy=setOet30_accuracy;

	this.Oet30_responsetime=null;


	function getOet30_responsetime() {
		return this.Oet30_responsetime;
	}
	this.getOet30_responsetime=getOet30_responsetime;


	function setOet30_responsetime(v){
		this.Oet30_responsetime=v;
	}
	this.setOet30_responsetime=setOet30_responsetime;

	this.Oet31_accuracy=null;


	function getOet31_accuracy() {
		return this.Oet31_accuracy;
	}
	this.getOet31_accuracy=getOet31_accuracy;


	function setOet31_accuracy(v){
		this.Oet31_accuracy=v;
	}
	this.setOet31_accuracy=setOet31_accuracy;

	this.Oet31_responsetime=null;


	function getOet31_responsetime() {
		return this.Oet31_responsetime;
	}
	this.getOet31_responsetime=getOet31_responsetime;


	function setOet31_responsetime(v){
		this.Oet31_responsetime=v;
	}
	this.setOet31_responsetime=setOet31_responsetime;

	this.Oet32_accuracy=null;


	function getOet32_accuracy() {
		return this.Oet32_accuracy;
	}
	this.getOet32_accuracy=getOet32_accuracy;


	function setOet32_accuracy(v){
		this.Oet32_accuracy=v;
	}
	this.setOet32_accuracy=setOet32_accuracy;

	this.Oet32_responsetime=null;


	function getOet32_responsetime() {
		return this.Oet32_responsetime;
	}
	this.getOet32_responsetime=getOet32_responsetime;


	function setOet32_responsetime(v){
		this.Oet32_responsetime=v;
	}
	this.setOet32_responsetime=setOet32_responsetime;

	this.Oet33_accuracy=null;


	function getOet33_accuracy() {
		return this.Oet33_accuracy;
	}
	this.getOet33_accuracy=getOet33_accuracy;


	function setOet33_accuracy(v){
		this.Oet33_accuracy=v;
	}
	this.setOet33_accuracy=setOet33_accuracy;

	this.Oet33_responsetime=null;


	function getOet33_responsetime() {
		return this.Oet33_responsetime;
	}
	this.getOet33_responsetime=getOet33_responsetime;


	function setOet33_responsetime(v){
		this.Oet33_responsetime=v;
	}
	this.setOet33_responsetime=setOet33_responsetime;

	this.Oet34_accuracy=null;


	function getOet34_accuracy() {
		return this.Oet34_accuracy;
	}
	this.getOet34_accuracy=getOet34_accuracy;


	function setOet34_accuracy(v){
		this.Oet34_accuracy=v;
	}
	this.setOet34_accuracy=setOet34_accuracy;

	this.Oet34_responsetime=null;


	function getOet34_responsetime() {
		return this.Oet34_responsetime;
	}
	this.getOet34_responsetime=getOet34_responsetime;


	function setOet34_responsetime(v){
		this.Oet34_responsetime=v;
	}
	this.setOet34_responsetime=setOet34_responsetime;

	this.Oet35_accuracy=null;


	function getOet35_accuracy() {
		return this.Oet35_accuracy;
	}
	this.getOet35_accuracy=getOet35_accuracy;


	function setOet35_accuracy(v){
		this.Oet35_accuracy=v;
	}
	this.setOet35_accuracy=setOet35_accuracy;

	this.Oet35_responsetime=null;


	function getOet35_responsetime() {
		return this.Oet35_responsetime;
	}
	this.getOet35_responsetime=getOet35_responsetime;


	function setOet35_responsetime(v){
		this.Oet35_responsetime=v;
	}
	this.setOet35_responsetime=setOet35_responsetime;

	this.Oet36_accuracy=null;


	function getOet36_accuracy() {
		return this.Oet36_accuracy;
	}
	this.getOet36_accuracy=getOet36_accuracy;


	function setOet36_accuracy(v){
		this.Oet36_accuracy=v;
	}
	this.setOet36_accuracy=setOet36_accuracy;

	this.Oet36_responsetime=null;


	function getOet36_responsetime() {
		return this.Oet36_responsetime;
	}
	this.getOet36_responsetime=getOet36_responsetime;


	function setOet36_responsetime(v){
		this.Oet36_responsetime=v;
	}
	this.setOet36_responsetime=setOet36_responsetime;

	this.Oet37_accuracy=null;


	function getOet37_accuracy() {
		return this.Oet37_accuracy;
	}
	this.getOet37_accuracy=getOet37_accuracy;


	function setOet37_accuracy(v){
		this.Oet37_accuracy=v;
	}
	this.setOet37_accuracy=setOet37_accuracy;

	this.Oet37_responsetime=null;


	function getOet37_responsetime() {
		return this.Oet37_responsetime;
	}
	this.getOet37_responsetime=getOet37_responsetime;


	function setOet37_responsetime(v){
		this.Oet37_responsetime=v;
	}
	this.setOet37_responsetime=setOet37_responsetime;

	this.Oet38_accuracy=null;


	function getOet38_accuracy() {
		return this.Oet38_accuracy;
	}
	this.getOet38_accuracy=getOet38_accuracy;


	function setOet38_accuracy(v){
		this.Oet38_accuracy=v;
	}
	this.setOet38_accuracy=setOet38_accuracy;

	this.Oet38_responsetime=null;


	function getOet38_responsetime() {
		return this.Oet38_responsetime;
	}
	this.getOet38_responsetime=getOet38_responsetime;


	function setOet38_responsetime(v){
		this.Oet38_responsetime=v;
	}
	this.setOet38_responsetime=setOet38_responsetime;

	this.Oet39_accuracy=null;


	function getOet39_accuracy() {
		return this.Oet39_accuracy;
	}
	this.getOet39_accuracy=getOet39_accuracy;


	function setOet39_accuracy(v){
		this.Oet39_accuracy=v;
	}
	this.setOet39_accuracy=setOet39_accuracy;

	this.Oet39_responsetime=null;


	function getOet39_responsetime() {
		return this.Oet39_responsetime;
	}
	this.getOet39_responsetime=getOet39_responsetime;


	function setOet39_responsetime(v){
		this.Oet39_responsetime=v;
	}
	this.setOet39_responsetime=setOet39_responsetime;

	this.Oet40_accuracy=null;


	function getOet40_accuracy() {
		return this.Oet40_accuracy;
	}
	this.getOet40_accuracy=getOet40_accuracy;


	function setOet40_accuracy(v){
		this.Oet40_accuracy=v;
	}
	this.setOet40_accuracy=setOet40_accuracy;

	this.Oet40_responsetime=null;


	function getOet40_responsetime() {
		return this.Oet40_responsetime;
	}
	this.getOet40_responsetime=getOet40_responsetime;


	function setOet40_responsetime(v){
		this.Oet40_responsetime=v;
	}
	this.setOet40_responsetime=setOet40_responsetime;

	this.Cvoet1_accuracy=null;


	function getCvoet1_accuracy() {
		return this.Cvoet1_accuracy;
	}
	this.getCvoet1_accuracy=getCvoet1_accuracy;


	function setCvoet1_accuracy(v){
		this.Cvoet1_accuracy=v;
	}
	this.setCvoet1_accuracy=setCvoet1_accuracy;

	this.Cvoet1_responsetime=null;


	function getCvoet1_responsetime() {
		return this.Cvoet1_responsetime;
	}
	this.getCvoet1_responsetime=getCvoet1_responsetime;


	function setCvoet1_responsetime(v){
		this.Cvoet1_responsetime=v;
	}
	this.setCvoet1_responsetime=setCvoet1_responsetime;

	this.Cvoet2_accuracy=null;


	function getCvoet2_accuracy() {
		return this.Cvoet2_accuracy;
	}
	this.getCvoet2_accuracy=getCvoet2_accuracy;


	function setCvoet2_accuracy(v){
		this.Cvoet2_accuracy=v;
	}
	this.setCvoet2_accuracy=setCvoet2_accuracy;

	this.Cvoet2_responsetime=null;


	function getCvoet2_responsetime() {
		return this.Cvoet2_responsetime;
	}
	this.getCvoet2_responsetime=getCvoet2_responsetime;


	function setCvoet2_responsetime(v){
		this.Cvoet2_responsetime=v;
	}
	this.setCvoet2_responsetime=setCvoet2_responsetime;

	this.Cvoet3_accuracy=null;


	function getCvoet3_accuracy() {
		return this.Cvoet3_accuracy;
	}
	this.getCvoet3_accuracy=getCvoet3_accuracy;


	function setCvoet3_accuracy(v){
		this.Cvoet3_accuracy=v;
	}
	this.setCvoet3_accuracy=setCvoet3_accuracy;

	this.Cvoet3_responsetime=null;


	function getCvoet3_responsetime() {
		return this.Cvoet3_responsetime;
	}
	this.getCvoet3_responsetime=getCvoet3_responsetime;


	function setCvoet3_responsetime(v){
		this.Cvoet3_responsetime=v;
	}
	this.setCvoet3_responsetime=setCvoet3_responsetime;

	this.Cvoet4_accuracy=null;


	function getCvoet4_accuracy() {
		return this.Cvoet4_accuracy;
	}
	this.getCvoet4_accuracy=getCvoet4_accuracy;


	function setCvoet4_accuracy(v){
		this.Cvoet4_accuracy=v;
	}
	this.setCvoet4_accuracy=setCvoet4_accuracy;

	this.Cvoet4_responsetime=null;


	function getCvoet4_responsetime() {
		return this.Cvoet4_responsetime;
	}
	this.getCvoet4_responsetime=getCvoet4_responsetime;


	function setCvoet4_responsetime(v){
		this.Cvoet4_responsetime=v;
	}
	this.setCvoet4_responsetime=setCvoet4_responsetime;

	this.Cvoet5_accuracy=null;


	function getCvoet5_accuracy() {
		return this.Cvoet5_accuracy;
	}
	this.getCvoet5_accuracy=getCvoet5_accuracy;


	function setCvoet5_accuracy(v){
		this.Cvoet5_accuracy=v;
	}
	this.setCvoet5_accuracy=setCvoet5_accuracy;

	this.Cvoet5_responsetime=null;


	function getCvoet5_responsetime() {
		return this.Cvoet5_responsetime;
	}
	this.getCvoet5_responsetime=getCvoet5_responsetime;


	function setCvoet5_responsetime(v){
		this.Cvoet5_responsetime=v;
	}
	this.setCvoet5_responsetime=setCvoet5_responsetime;

	this.Cvoet6_accuracy=null;


	function getCvoet6_accuracy() {
		return this.Cvoet6_accuracy;
	}
	this.getCvoet6_accuracy=getCvoet6_accuracy;


	function setCvoet6_accuracy(v){
		this.Cvoet6_accuracy=v;
	}
	this.setCvoet6_accuracy=setCvoet6_accuracy;

	this.Cvoet6_responsetime=null;


	function getCvoet6_responsetime() {
		return this.Cvoet6_responsetime;
	}
	this.getCvoet6_responsetime=getCvoet6_responsetime;


	function setCvoet6_responsetime(v){
		this.Cvoet6_responsetime=v;
	}
	this.setCvoet6_responsetime=setCvoet6_responsetime;

	this.Cvoet7_accuracy=null;


	function getCvoet7_accuracy() {
		return this.Cvoet7_accuracy;
	}
	this.getCvoet7_accuracy=getCvoet7_accuracy;


	function setCvoet7_accuracy(v){
		this.Cvoet7_accuracy=v;
	}
	this.setCvoet7_accuracy=setCvoet7_accuracy;

	this.Cvoet7_responsetime=null;


	function getCvoet7_responsetime() {
		return this.Cvoet7_responsetime;
	}
	this.getCvoet7_responsetime=getCvoet7_responsetime;


	function setCvoet7_responsetime(v){
		this.Cvoet7_responsetime=v;
	}
	this.setCvoet7_responsetime=setCvoet7_responsetime;

	this.Cvoet8_accuracy=null;


	function getCvoet8_accuracy() {
		return this.Cvoet8_accuracy;
	}
	this.getCvoet8_accuracy=getCvoet8_accuracy;


	function setCvoet8_accuracy(v){
		this.Cvoet8_accuracy=v;
	}
	this.setCvoet8_accuracy=setCvoet8_accuracy;

	this.Cvoet8_responsetime=null;


	function getCvoet8_responsetime() {
		return this.Cvoet8_responsetime;
	}
	this.getCvoet8_responsetime=getCvoet8_responsetime;


	function setCvoet8_responsetime(v){
		this.Cvoet8_responsetime=v;
	}
	this.setCvoet8_responsetime=setCvoet8_responsetime;

	this.Cvoet9_accuracy=null;


	function getCvoet9_accuracy() {
		return this.Cvoet9_accuracy;
	}
	this.getCvoet9_accuracy=getCvoet9_accuracy;


	function setCvoet9_accuracy(v){
		this.Cvoet9_accuracy=v;
	}
	this.setCvoet9_accuracy=setCvoet9_accuracy;

	this.Cvoet9_responsetime=null;


	function getCvoet9_responsetime() {
		return this.Cvoet9_responsetime;
	}
	this.getCvoet9_responsetime=getCvoet9_responsetime;


	function setCvoet9_responsetime(v){
		this.Cvoet9_responsetime=v;
	}
	this.setCvoet9_responsetime=setCvoet9_responsetime;

	this.Cvoet10_accuracy=null;


	function getCvoet10_accuracy() {
		return this.Cvoet10_accuracy;
	}
	this.getCvoet10_accuracy=getCvoet10_accuracy;


	function setCvoet10_accuracy(v){
		this.Cvoet10_accuracy=v;
	}
	this.setCvoet10_accuracy=setCvoet10_accuracy;

	this.Cvoet10_responsetime=null;


	function getCvoet10_responsetime() {
		return this.Cvoet10_responsetime;
	}
	this.getCvoet10_responsetime=getCvoet10_responsetime;


	function setCvoet10_responsetime(v){
		this.Cvoet10_responsetime=v;
	}
	this.setCvoet10_responsetime=setCvoet10_responsetime;

	this.Cvoet11_accuracy=null;


	function getCvoet11_accuracy() {
		return this.Cvoet11_accuracy;
	}
	this.getCvoet11_accuracy=getCvoet11_accuracy;


	function setCvoet11_accuracy(v){
		this.Cvoet11_accuracy=v;
	}
	this.setCvoet11_accuracy=setCvoet11_accuracy;

	this.Cvoet11_responsetime=null;


	function getCvoet11_responsetime() {
		return this.Cvoet11_responsetime;
	}
	this.getCvoet11_responsetime=getCvoet11_responsetime;


	function setCvoet11_responsetime(v){
		this.Cvoet11_responsetime=v;
	}
	this.setCvoet11_responsetime=setCvoet11_responsetime;

	this.Cvoet12_accuracy=null;


	function getCvoet12_accuracy() {
		return this.Cvoet12_accuracy;
	}
	this.getCvoet12_accuracy=getCvoet12_accuracy;


	function setCvoet12_accuracy(v){
		this.Cvoet12_accuracy=v;
	}
	this.setCvoet12_accuracy=setCvoet12_accuracy;

	this.Cvoet12_responsetime=null;


	function getCvoet12_responsetime() {
		return this.Cvoet12_responsetime;
	}
	this.getCvoet12_responsetime=getCvoet12_responsetime;


	function setCvoet12_responsetime(v){
		this.Cvoet12_responsetime=v;
	}
	this.setCvoet12_responsetime=setCvoet12_responsetime;

	this.Cvoet13_accuracy=null;


	function getCvoet13_accuracy() {
		return this.Cvoet13_accuracy;
	}
	this.getCvoet13_accuracy=getCvoet13_accuracy;


	function setCvoet13_accuracy(v){
		this.Cvoet13_accuracy=v;
	}
	this.setCvoet13_accuracy=setCvoet13_accuracy;

	this.Cvoet13_responsetime=null;


	function getCvoet13_responsetime() {
		return this.Cvoet13_responsetime;
	}
	this.getCvoet13_responsetime=getCvoet13_responsetime;


	function setCvoet13_responsetime(v){
		this.Cvoet13_responsetime=v;
	}
	this.setCvoet13_responsetime=setCvoet13_responsetime;

	this.Cvoet14_accuracy=null;


	function getCvoet14_accuracy() {
		return this.Cvoet14_accuracy;
	}
	this.getCvoet14_accuracy=getCvoet14_accuracy;


	function setCvoet14_accuracy(v){
		this.Cvoet14_accuracy=v;
	}
	this.setCvoet14_accuracy=setCvoet14_accuracy;

	this.Cvoet14_responsetime=null;


	function getCvoet14_responsetime() {
		return this.Cvoet14_responsetime;
	}
	this.getCvoet14_responsetime=getCvoet14_responsetime;


	function setCvoet14_responsetime(v){
		this.Cvoet14_responsetime=v;
	}
	this.setCvoet14_responsetime=setCvoet14_responsetime;

	this.Cvoet15_accuracy=null;


	function getCvoet15_accuracy() {
		return this.Cvoet15_accuracy;
	}
	this.getCvoet15_accuracy=getCvoet15_accuracy;


	function setCvoet15_accuracy(v){
		this.Cvoet15_accuracy=v;
	}
	this.setCvoet15_accuracy=setCvoet15_accuracy;

	this.Cvoet15_responsetime=null;


	function getCvoet15_responsetime() {
		return this.Cvoet15_responsetime;
	}
	this.getCvoet15_responsetime=getCvoet15_responsetime;


	function setCvoet15_responsetime(v){
		this.Cvoet15_responsetime=v;
	}
	this.setCvoet15_responsetime=setCvoet15_responsetime;

	this.Cvoet16_accuracy=null;


	function getCvoet16_accuracy() {
		return this.Cvoet16_accuracy;
	}
	this.getCvoet16_accuracy=getCvoet16_accuracy;


	function setCvoet16_accuracy(v){
		this.Cvoet16_accuracy=v;
	}
	this.setCvoet16_accuracy=setCvoet16_accuracy;

	this.Cvoet16_responsetime=null;


	function getCvoet16_responsetime() {
		return this.Cvoet16_responsetime;
	}
	this.getCvoet16_responsetime=getCvoet16_responsetime;


	function setCvoet16_responsetime(v){
		this.Cvoet16_responsetime=v;
	}
	this.setCvoet16_responsetime=setCvoet16_responsetime;

	this.Cvoet17_accuracy=null;


	function getCvoet17_accuracy() {
		return this.Cvoet17_accuracy;
	}
	this.getCvoet17_accuracy=getCvoet17_accuracy;


	function setCvoet17_accuracy(v){
		this.Cvoet17_accuracy=v;
	}
	this.setCvoet17_accuracy=setCvoet17_accuracy;

	this.Cvoet17_responsetime=null;


	function getCvoet17_responsetime() {
		return this.Cvoet17_responsetime;
	}
	this.getCvoet17_responsetime=getCvoet17_responsetime;


	function setCvoet17_responsetime(v){
		this.Cvoet17_responsetime=v;
	}
	this.setCvoet17_responsetime=setCvoet17_responsetime;

	this.Cvoet18_accuracy=null;


	function getCvoet18_accuracy() {
		return this.Cvoet18_accuracy;
	}
	this.getCvoet18_accuracy=getCvoet18_accuracy;


	function setCvoet18_accuracy(v){
		this.Cvoet18_accuracy=v;
	}
	this.setCvoet18_accuracy=setCvoet18_accuracy;

	this.Cvoet18_responsetime=null;


	function getCvoet18_responsetime() {
		return this.Cvoet18_responsetime;
	}
	this.getCvoet18_responsetime=getCvoet18_responsetime;


	function setCvoet18_responsetime(v){
		this.Cvoet18_responsetime=v;
	}
	this.setCvoet18_responsetime=setCvoet18_responsetime;

	this.Cvoet19_accuracy=null;


	function getCvoet19_accuracy() {
		return this.Cvoet19_accuracy;
	}
	this.getCvoet19_accuracy=getCvoet19_accuracy;


	function setCvoet19_accuracy(v){
		this.Cvoet19_accuracy=v;
	}
	this.setCvoet19_accuracy=setCvoet19_accuracy;

	this.Cvoet19_responsetime=null;


	function getCvoet19_responsetime() {
		return this.Cvoet19_responsetime;
	}
	this.getCvoet19_responsetime=getCvoet19_responsetime;


	function setCvoet19_responsetime(v){
		this.Cvoet19_responsetime=v;
	}
	this.setCvoet19_responsetime=setCvoet19_responsetime;

	this.Cvoet20_accuracy=null;


	function getCvoet20_accuracy() {
		return this.Cvoet20_accuracy;
	}
	this.getCvoet20_accuracy=getCvoet20_accuracy;


	function setCvoet20_accuracy(v){
		this.Cvoet20_accuracy=v;
	}
	this.setCvoet20_accuracy=setCvoet20_accuracy;

	this.Cvoet20_responsetime=null;


	function getCvoet20_responsetime() {
		return this.Cvoet20_responsetime;
	}
	this.getCvoet20_responsetime=getCvoet20_responsetime;


	function setCvoet20_responsetime(v){
		this.Cvoet20_responsetime=v;
	}
	this.setCvoet20_responsetime=setCvoet20_responsetime;

	this.Cvoet21_accuracy=null;


	function getCvoet21_accuracy() {
		return this.Cvoet21_accuracy;
	}
	this.getCvoet21_accuracy=getCvoet21_accuracy;


	function setCvoet21_accuracy(v){
		this.Cvoet21_accuracy=v;
	}
	this.setCvoet21_accuracy=setCvoet21_accuracy;

	this.Cvoet21_responsetime=null;


	function getCvoet21_responsetime() {
		return this.Cvoet21_responsetime;
	}
	this.getCvoet21_responsetime=getCvoet21_responsetime;


	function setCvoet21_responsetime(v){
		this.Cvoet21_responsetime=v;
	}
	this.setCvoet21_responsetime=setCvoet21_responsetime;

	this.Cvoet22_accuracy=null;


	function getCvoet22_accuracy() {
		return this.Cvoet22_accuracy;
	}
	this.getCvoet22_accuracy=getCvoet22_accuracy;


	function setCvoet22_accuracy(v){
		this.Cvoet22_accuracy=v;
	}
	this.setCvoet22_accuracy=setCvoet22_accuracy;

	this.Cvoet22_responsetime=null;


	function getCvoet22_responsetime() {
		return this.Cvoet22_responsetime;
	}
	this.getCvoet22_responsetime=getCvoet22_responsetime;


	function setCvoet22_responsetime(v){
		this.Cvoet22_responsetime=v;
	}
	this.setCvoet22_responsetime=setCvoet22_responsetime;

	this.Cvoet23_accuracy=null;


	function getCvoet23_accuracy() {
		return this.Cvoet23_accuracy;
	}
	this.getCvoet23_accuracy=getCvoet23_accuracy;


	function setCvoet23_accuracy(v){
		this.Cvoet23_accuracy=v;
	}
	this.setCvoet23_accuracy=setCvoet23_accuracy;

	this.Cvoet23_responsetime=null;


	function getCvoet23_responsetime() {
		return this.Cvoet23_responsetime;
	}
	this.getCvoet23_responsetime=getCvoet23_responsetime;


	function setCvoet23_responsetime(v){
		this.Cvoet23_responsetime=v;
	}
	this.setCvoet23_responsetime=setCvoet23_responsetime;

	this.Cvoet24_accuracy=null;


	function getCvoet24_accuracy() {
		return this.Cvoet24_accuracy;
	}
	this.getCvoet24_accuracy=getCvoet24_accuracy;


	function setCvoet24_accuracy(v){
		this.Cvoet24_accuracy=v;
	}
	this.setCvoet24_accuracy=setCvoet24_accuracy;

	this.Cvoet24_responsetime=null;


	function getCvoet24_responsetime() {
		return this.Cvoet24_responsetime;
	}
	this.getCvoet24_responsetime=getCvoet24_responsetime;


	function setCvoet24_responsetime(v){
		this.Cvoet24_responsetime=v;
	}
	this.setCvoet24_responsetime=setCvoet24_responsetime;

	this.Cvoet25_accuracy=null;


	function getCvoet25_accuracy() {
		return this.Cvoet25_accuracy;
	}
	this.getCvoet25_accuracy=getCvoet25_accuracy;


	function setCvoet25_accuracy(v){
		this.Cvoet25_accuracy=v;
	}
	this.setCvoet25_accuracy=setCvoet25_accuracy;

	this.Cvoet25_responsetime=null;


	function getCvoet25_responsetime() {
		return this.Cvoet25_responsetime;
	}
	this.getCvoet25_responsetime=getCvoet25_responsetime;


	function setCvoet25_responsetime(v){
		this.Cvoet25_responsetime=v;
	}
	this.setCvoet25_responsetime=setCvoet25_responsetime;

	this.Cvoet26_accuracy=null;


	function getCvoet26_accuracy() {
		return this.Cvoet26_accuracy;
	}
	this.getCvoet26_accuracy=getCvoet26_accuracy;


	function setCvoet26_accuracy(v){
		this.Cvoet26_accuracy=v;
	}
	this.setCvoet26_accuracy=setCvoet26_accuracy;

	this.Cvoet26_responsetime=null;


	function getCvoet26_responsetime() {
		return this.Cvoet26_responsetime;
	}
	this.getCvoet26_responsetime=getCvoet26_responsetime;


	function setCvoet26_responsetime(v){
		this.Cvoet26_responsetime=v;
	}
	this.setCvoet26_responsetime=setCvoet26_responsetime;

	this.Cvoet27_accuracy=null;


	function getCvoet27_accuracy() {
		return this.Cvoet27_accuracy;
	}
	this.getCvoet27_accuracy=getCvoet27_accuracy;


	function setCvoet27_accuracy(v){
		this.Cvoet27_accuracy=v;
	}
	this.setCvoet27_accuracy=setCvoet27_accuracy;

	this.Cvoet27_responsetime=null;


	function getCvoet27_responsetime() {
		return this.Cvoet27_responsetime;
	}
	this.getCvoet27_responsetime=getCvoet27_responsetime;


	function setCvoet27_responsetime(v){
		this.Cvoet27_responsetime=v;
	}
	this.setCvoet27_responsetime=setCvoet27_responsetime;

	this.Cvoet28_accuracy=null;


	function getCvoet28_accuracy() {
		return this.Cvoet28_accuracy;
	}
	this.getCvoet28_accuracy=getCvoet28_accuracy;


	function setCvoet28_accuracy(v){
		this.Cvoet28_accuracy=v;
	}
	this.setCvoet28_accuracy=setCvoet28_accuracy;

	this.Cvoet28_responsetime=null;


	function getCvoet28_responsetime() {
		return this.Cvoet28_responsetime;
	}
	this.getCvoet28_responsetime=getCvoet28_responsetime;


	function setCvoet28_responsetime(v){
		this.Cvoet28_responsetime=v;
	}
	this.setCvoet28_responsetime=setCvoet28_responsetime;

	this.Cvoet29_accuracy=null;


	function getCvoet29_accuracy() {
		return this.Cvoet29_accuracy;
	}
	this.getCvoet29_accuracy=getCvoet29_accuracy;


	function setCvoet29_accuracy(v){
		this.Cvoet29_accuracy=v;
	}
	this.setCvoet29_accuracy=setCvoet29_accuracy;

	this.Cvoet29_responsetime=null;


	function getCvoet29_responsetime() {
		return this.Cvoet29_responsetime;
	}
	this.getCvoet29_responsetime=getCvoet29_responsetime;


	function setCvoet29_responsetime(v){
		this.Cvoet29_responsetime=v;
	}
	this.setCvoet29_responsetime=setCvoet29_responsetime;

	this.Cvoet30_accuracy=null;


	function getCvoet30_accuracy() {
		return this.Cvoet30_accuracy;
	}
	this.getCvoet30_accuracy=getCvoet30_accuracy;


	function setCvoet30_accuracy(v){
		this.Cvoet30_accuracy=v;
	}
	this.setCvoet30_accuracy=setCvoet30_accuracy;

	this.Cvoet30_responsetime=null;


	function getCvoet30_responsetime() {
		return this.Cvoet30_responsetime;
	}
	this.getCvoet30_responsetime=getCvoet30_responsetime;


	function setCvoet30_responsetime(v){
		this.Cvoet30_responsetime=v;
	}
	this.setCvoet30_responsetime=setCvoet30_responsetime;

	this.Cvoet31_accuracy=null;


	function getCvoet31_accuracy() {
		return this.Cvoet31_accuracy;
	}
	this.getCvoet31_accuracy=getCvoet31_accuracy;


	function setCvoet31_accuracy(v){
		this.Cvoet31_accuracy=v;
	}
	this.setCvoet31_accuracy=setCvoet31_accuracy;

	this.Cvoet31_responsetime=null;


	function getCvoet31_responsetime() {
		return this.Cvoet31_responsetime;
	}
	this.getCvoet31_responsetime=getCvoet31_responsetime;


	function setCvoet31_responsetime(v){
		this.Cvoet31_responsetime=v;
	}
	this.setCvoet31_responsetime=setCvoet31_responsetime;

	this.Cvoet32_accuracy=null;


	function getCvoet32_accuracy() {
		return this.Cvoet32_accuracy;
	}
	this.getCvoet32_accuracy=getCvoet32_accuracy;


	function setCvoet32_accuracy(v){
		this.Cvoet32_accuracy=v;
	}
	this.setCvoet32_accuracy=setCvoet32_accuracy;

	this.Cvoet32_responsetime=null;


	function getCvoet32_responsetime() {
		return this.Cvoet32_responsetime;
	}
	this.getCvoet32_responsetime=getCvoet32_responsetime;


	function setCvoet32_responsetime(v){
		this.Cvoet32_responsetime=v;
	}
	this.setCvoet32_responsetime=setCvoet32_responsetime;

	this.Cvoet33_accuracy=null;


	function getCvoet33_accuracy() {
		return this.Cvoet33_accuracy;
	}
	this.getCvoet33_accuracy=getCvoet33_accuracy;


	function setCvoet33_accuracy(v){
		this.Cvoet33_accuracy=v;
	}
	this.setCvoet33_accuracy=setCvoet33_accuracy;

	this.Cvoet33_responsetime=null;


	function getCvoet33_responsetime() {
		return this.Cvoet33_responsetime;
	}
	this.getCvoet33_responsetime=getCvoet33_responsetime;


	function setCvoet33_responsetime(v){
		this.Cvoet33_responsetime=v;
	}
	this.setCvoet33_responsetime=setCvoet33_responsetime;

	this.Cvoet34_accuracy=null;


	function getCvoet34_accuracy() {
		return this.Cvoet34_accuracy;
	}
	this.getCvoet34_accuracy=getCvoet34_accuracy;


	function setCvoet34_accuracy(v){
		this.Cvoet34_accuracy=v;
	}
	this.setCvoet34_accuracy=setCvoet34_accuracy;

	this.Cvoet34_responsetime=null;


	function getCvoet34_responsetime() {
		return this.Cvoet34_responsetime;
	}
	this.getCvoet34_responsetime=getCvoet34_responsetime;


	function setCvoet34_responsetime(v){
		this.Cvoet34_responsetime=v;
	}
	this.setCvoet34_responsetime=setCvoet34_responsetime;

	this.Cvoet35_accuracy=null;


	function getCvoet35_accuracy() {
		return this.Cvoet35_accuracy;
	}
	this.getCvoet35_accuracy=getCvoet35_accuracy;


	function setCvoet35_accuracy(v){
		this.Cvoet35_accuracy=v;
	}
	this.setCvoet35_accuracy=setCvoet35_accuracy;

	this.Cvoet35_responsetime=null;


	function getCvoet35_responsetime() {
		return this.Cvoet35_responsetime;
	}
	this.getCvoet35_responsetime=getCvoet35_responsetime;


	function setCvoet35_responsetime(v){
		this.Cvoet35_responsetime=v;
	}
	this.setCvoet35_responsetime=setCvoet35_responsetime;

	this.Cvoet36_accuracy=null;


	function getCvoet36_accuracy() {
		return this.Cvoet36_accuracy;
	}
	this.getCvoet36_accuracy=getCvoet36_accuracy;


	function setCvoet36_accuracy(v){
		this.Cvoet36_accuracy=v;
	}
	this.setCvoet36_accuracy=setCvoet36_accuracy;

	this.Cvoet36_responsetime=null;


	function getCvoet36_responsetime() {
		return this.Cvoet36_responsetime;
	}
	this.getCvoet36_responsetime=getCvoet36_responsetime;


	function setCvoet36_responsetime(v){
		this.Cvoet36_responsetime=v;
	}
	this.setCvoet36_responsetime=setCvoet36_responsetime;

	this.Cvoet37_accuracy=null;


	function getCvoet37_accuracy() {
		return this.Cvoet37_accuracy;
	}
	this.getCvoet37_accuracy=getCvoet37_accuracy;


	function setCvoet37_accuracy(v){
		this.Cvoet37_accuracy=v;
	}
	this.setCvoet37_accuracy=setCvoet37_accuracy;

	this.Cvoet37_responsetime=null;


	function getCvoet37_responsetime() {
		return this.Cvoet37_responsetime;
	}
	this.getCvoet37_responsetime=getCvoet37_responsetime;


	function setCvoet37_responsetime(v){
		this.Cvoet37_responsetime=v;
	}
	this.setCvoet37_responsetime=setCvoet37_responsetime;

	this.Cvoet38_accuracy=null;


	function getCvoet38_accuracy() {
		return this.Cvoet38_accuracy;
	}
	this.getCvoet38_accuracy=getCvoet38_accuracy;


	function setCvoet38_accuracy(v){
		this.Cvoet38_accuracy=v;
	}
	this.setCvoet38_accuracy=setCvoet38_accuracy;

	this.Cvoet38_responsetime=null;


	function getCvoet38_responsetime() {
		return this.Cvoet38_responsetime;
	}
	this.getCvoet38_responsetime=getCvoet38_responsetime;


	function setCvoet38_responsetime(v){
		this.Cvoet38_responsetime=v;
	}
	this.setCvoet38_responsetime=setCvoet38_responsetime;

	this.Cvoet39_accuracy=null;


	function getCvoet39_accuracy() {
		return this.Cvoet39_accuracy;
	}
	this.getCvoet39_accuracy=getCvoet39_accuracy;


	function setCvoet39_accuracy(v){
		this.Cvoet39_accuracy=v;
	}
	this.setCvoet39_accuracy=setCvoet39_accuracy;

	this.Cvoet39_responsetime=null;


	function getCvoet39_responsetime() {
		return this.Cvoet39_responsetime;
	}
	this.getCvoet39_responsetime=getCvoet39_responsetime;


	function setCvoet39_responsetime(v){
		this.Cvoet39_responsetime=v;
	}
	this.setCvoet39_responsetime=setCvoet39_responsetime;

	this.Cvoet40_accuracy=null;


	function getCvoet40_accuracy() {
		return this.Cvoet40_accuracy;
	}
	this.getCvoet40_accuracy=getCvoet40_accuracy;


	function setCvoet40_accuracy(v){
		this.Cvoet40_accuracy=v;
	}
	this.setCvoet40_accuracy=setCvoet40_accuracy;

	this.Cvoet40_responsetime=null;


	function getCvoet40_responsetime() {
		return this.Cvoet40_responsetime;
	}
	this.getCvoet40_responsetime=getCvoet40_responsetime;


	function setCvoet40_responsetime(v){
		this.Cvoet40_responsetime=v;
	}
	this.setCvoet40_responsetime=setCvoet40_responsetime;

	this.Cvoet41_accuracy=null;


	function getCvoet41_accuracy() {
		return this.Cvoet41_accuracy;
	}
	this.getCvoet41_accuracy=getCvoet41_accuracy;


	function setCvoet41_accuracy(v){
		this.Cvoet41_accuracy=v;
	}
	this.setCvoet41_accuracy=setCvoet41_accuracy;

	this.Cvoet41_responsetime=null;


	function getCvoet41_responsetime() {
		return this.Cvoet41_responsetime;
	}
	this.getCvoet41_responsetime=getCvoet41_responsetime;


	function setCvoet41_responsetime(v){
		this.Cvoet41_responsetime=v;
	}
	this.setCvoet41_responsetime=setCvoet41_responsetime;

	this.Cvoet42_accuracy=null;


	function getCvoet42_accuracy() {
		return this.Cvoet42_accuracy;
	}
	this.getCvoet42_accuracy=getCvoet42_accuracy;


	function setCvoet42_accuracy(v){
		this.Cvoet42_accuracy=v;
	}
	this.setCvoet42_accuracy=setCvoet42_accuracy;

	this.Cvoet42_responsetime=null;


	function getCvoet42_responsetime() {
		return this.Cvoet42_responsetime;
	}
	this.getCvoet42_responsetime=getCvoet42_responsetime;


	function setCvoet42_responsetime(v){
		this.Cvoet42_responsetime=v;
	}
	this.setCvoet42_responsetime=setCvoet42_responsetime;

	this.Cvoet43_accuracy=null;


	function getCvoet43_accuracy() {
		return this.Cvoet43_accuracy;
	}
	this.getCvoet43_accuracy=getCvoet43_accuracy;


	function setCvoet43_accuracy(v){
		this.Cvoet43_accuracy=v;
	}
	this.setCvoet43_accuracy=setCvoet43_accuracy;

	this.Cvoet43_responsetime=null;


	function getCvoet43_responsetime() {
		return this.Cvoet43_responsetime;
	}
	this.getCvoet43_responsetime=getCvoet43_responsetime;


	function setCvoet43_responsetime(v){
		this.Cvoet43_responsetime=v;
	}
	this.setCvoet43_responsetime=setCvoet43_responsetime;

	this.Cvoet44_accuracy=null;


	function getCvoet44_accuracy() {
		return this.Cvoet44_accuracy;
	}
	this.getCvoet44_accuracy=getCvoet44_accuracy;


	function setCvoet44_accuracy(v){
		this.Cvoet44_accuracy=v;
	}
	this.setCvoet44_accuracy=setCvoet44_accuracy;

	this.Cvoet44_responsetime=null;


	function getCvoet44_responsetime() {
		return this.Cvoet44_responsetime;
	}
	this.getCvoet44_responsetime=getCvoet44_responsetime;


	function setCvoet44_responsetime(v){
		this.Cvoet44_responsetime=v;
	}
	this.setCvoet44_responsetime=setCvoet44_responsetime;

	this.Cvoet45_accuracy=null;


	function getCvoet45_accuracy() {
		return this.Cvoet45_accuracy;
	}
	this.getCvoet45_accuracy=getCvoet45_accuracy;


	function setCvoet45_accuracy(v){
		this.Cvoet45_accuracy=v;
	}
	this.setCvoet45_accuracy=setCvoet45_accuracy;

	this.Cvoet45_responsetime=null;


	function getCvoet45_responsetime() {
		return this.Cvoet45_responsetime;
	}
	this.getCvoet45_responsetime=getCvoet45_responsetime;


	function setCvoet45_responsetime(v){
		this.Cvoet45_responsetime=v;
	}
	this.setCvoet45_responsetime=setCvoet45_responsetime;

	this.Cvoet46_accuracy=null;


	function getCvoet46_accuracy() {
		return this.Cvoet46_accuracy;
	}
	this.getCvoet46_accuracy=getCvoet46_accuracy;


	function setCvoet46_accuracy(v){
		this.Cvoet46_accuracy=v;
	}
	this.setCvoet46_accuracy=setCvoet46_accuracy;

	this.Cvoet46_responsetime=null;


	function getCvoet46_responsetime() {
		return this.Cvoet46_responsetime;
	}
	this.getCvoet46_responsetime=getCvoet46_responsetime;


	function setCvoet46_responsetime(v){
		this.Cvoet46_responsetime=v;
	}
	this.setCvoet46_responsetime=setCvoet46_responsetime;

	this.Cvoet47_accuracy=null;


	function getCvoet47_accuracy() {
		return this.Cvoet47_accuracy;
	}
	this.getCvoet47_accuracy=getCvoet47_accuracy;


	function setCvoet47_accuracy(v){
		this.Cvoet47_accuracy=v;
	}
	this.setCvoet47_accuracy=setCvoet47_accuracy;

	this.Cvoet47_responsetime=null;


	function getCvoet47_responsetime() {
		return this.Cvoet47_responsetime;
	}
	this.getCvoet47_responsetime=getCvoet47_responsetime;


	function setCvoet47_responsetime(v){
		this.Cvoet47_responsetime=v;
	}
	this.setCvoet47_responsetime=setCvoet47_responsetime;

	this.Cvoet48_accuracy=null;


	function getCvoet48_accuracy() {
		return this.Cvoet48_accuracy;
	}
	this.getCvoet48_accuracy=getCvoet48_accuracy;


	function setCvoet48_accuracy(v){
		this.Cvoet48_accuracy=v;
	}
	this.setCvoet48_accuracy=setCvoet48_accuracy;

	this.Cvoet48_responsetime=null;


	function getCvoet48_responsetime() {
		return this.Cvoet48_responsetime;
	}
	this.getCvoet48_responsetime=getCvoet48_responsetime;


	function setCvoet48_responsetime(v){
		this.Cvoet48_responsetime=v;
	}
	this.setCvoet48_responsetime=setCvoet48_responsetime;

	this.Cvoet49_accuracy=null;


	function getCvoet49_accuracy() {
		return this.Cvoet49_accuracy;
	}
	this.getCvoet49_accuracy=getCvoet49_accuracy;


	function setCvoet49_accuracy(v){
		this.Cvoet49_accuracy=v;
	}
	this.setCvoet49_accuracy=setCvoet49_accuracy;

	this.Cvoet49_responsetime=null;


	function getCvoet49_responsetime() {
		return this.Cvoet49_responsetime;
	}
	this.getCvoet49_responsetime=getCvoet49_responsetime;


	function setCvoet49_responsetime(v){
		this.Cvoet49_responsetime=v;
	}
	this.setCvoet49_responsetime=setCvoet49_responsetime;

	this.Cvoet50_accuracy=null;


	function getCvoet50_accuracy() {
		return this.Cvoet50_accuracy;
	}
	this.getCvoet50_accuracy=getCvoet50_accuracy;


	function setCvoet50_accuracy(v){
		this.Cvoet50_accuracy=v;
	}
	this.setCvoet50_accuracy=setCvoet50_accuracy;

	this.Cvoet50_responsetime=null;


	function getCvoet50_responsetime() {
		return this.Cvoet50_responsetime;
	}
	this.getCvoet50_responsetime=getCvoet50_responsetime;


	function setCvoet50_responsetime(v){
		this.Cvoet50_responsetime=v;
	}
	this.setCvoet50_responsetime=setCvoet50_responsetime;

	this.Cvoet51_accuracy=null;


	function getCvoet51_accuracy() {
		return this.Cvoet51_accuracy;
	}
	this.getCvoet51_accuracy=getCvoet51_accuracy;


	function setCvoet51_accuracy(v){
		this.Cvoet51_accuracy=v;
	}
	this.setCvoet51_accuracy=setCvoet51_accuracy;

	this.Cvoet51_responsetime=null;


	function getCvoet51_responsetime() {
		return this.Cvoet51_responsetime;
	}
	this.getCvoet51_responsetime=getCvoet51_responsetime;


	function setCvoet51_responsetime(v){
		this.Cvoet51_responsetime=v;
	}
	this.setCvoet51_responsetime=setCvoet51_responsetime;

	this.Cvoet52_accuracy=null;


	function getCvoet52_accuracy() {
		return this.Cvoet52_accuracy;
	}
	this.getCvoet52_accuracy=getCvoet52_accuracy;


	function setCvoet52_accuracy(v){
		this.Cvoet52_accuracy=v;
	}
	this.setCvoet52_accuracy=setCvoet52_accuracy;

	this.Cvoet52_responsetime=null;


	function getCvoet52_responsetime() {
		return this.Cvoet52_responsetime;
	}
	this.getCvoet52_responsetime=getCvoet52_responsetime;


	function setCvoet52_responsetime(v){
		this.Cvoet52_responsetime=v;
	}
	this.setCvoet52_responsetime=setCvoet52_responsetime;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="CVT1/accuracy"){
				return this.Cvt1_accuracy ;
			} else 
			if(xmlPath=="CVT1/responseTime"){
				return this.Cvt1_responsetime ;
			} else 
			if(xmlPath=="CVT2/accuracy"){
				return this.Cvt2_accuracy ;
			} else 
			if(xmlPath=="CVT2/responseTime"){
				return this.Cvt2_responsetime ;
			} else 
			if(xmlPath=="CVT3/accuracy"){
				return this.Cvt3_accuracy ;
			} else 
			if(xmlPath=="CVT3/responseTime"){
				return this.Cvt3_responsetime ;
			} else 
			if(xmlPath=="CVT4/accuracy"){
				return this.Cvt4_accuracy ;
			} else 
			if(xmlPath=="CVT4/responseTime"){
				return this.Cvt4_responsetime ;
			} else 
			if(xmlPath=="CVT5/accuracy"){
				return this.Cvt5_accuracy ;
			} else 
			if(xmlPath=="CVT5/responseTime"){
				return this.Cvt5_responsetime ;
			} else 
			if(xmlPath=="CVT6/accuracy"){
				return this.Cvt6_accuracy ;
			} else 
			if(xmlPath=="CVT6/responseTime"){
				return this.Cvt6_responsetime ;
			} else 
			if(xmlPath=="CVT7/accuracy"){
				return this.Cvt7_accuracy ;
			} else 
			if(xmlPath=="CVT7/responseTime"){
				return this.Cvt7_responsetime ;
			} else 
			if(xmlPath=="CVT8/accuracy"){
				return this.Cvt8_accuracy ;
			} else 
			if(xmlPath=="CVT8/responseTime"){
				return this.Cvt8_responsetime ;
			} else 
			if(xmlPath=="CVT9/accuracy"){
				return this.Cvt9_accuracy ;
			} else 
			if(xmlPath=="CVT9/responseTime"){
				return this.Cvt9_responsetime ;
			} else 
			if(xmlPath=="CVT10/accuracy"){
				return this.Cvt10_accuracy ;
			} else 
			if(xmlPath=="CVT10/responseTime"){
				return this.Cvt10_responsetime ;
			} else 
			if(xmlPath=="CVT11/accuracy"){
				return this.Cvt11_accuracy ;
			} else 
			if(xmlPath=="CVT11/responseTime"){
				return this.Cvt11_responsetime ;
			} else 
			if(xmlPath=="CVT12/accuracy"){
				return this.Cvt12_accuracy ;
			} else 
			if(xmlPath=="CVT12/responseTime"){
				return this.Cvt12_responsetime ;
			} else 
			if(xmlPath=="CVT13/accuracy"){
				return this.Cvt13_accuracy ;
			} else 
			if(xmlPath=="CVT13/responseTime"){
				return this.Cvt13_responsetime ;
			} else 
			if(xmlPath=="CVT14/accuracy"){
				return this.Cvt14_accuracy ;
			} else 
			if(xmlPath=="CVT14/responseTime"){
				return this.Cvt14_responsetime ;
			} else 
			if(xmlPath=="CVT15/accuracy"){
				return this.Cvt15_accuracy ;
			} else 
			if(xmlPath=="CVT15/responseTime"){
				return this.Cvt15_responsetime ;
			} else 
			if(xmlPath=="CVT16/accuracy"){
				return this.Cvt16_accuracy ;
			} else 
			if(xmlPath=="CVT16/responseTime"){
				return this.Cvt16_responsetime ;
			} else 
			if(xmlPath=="CVT17/accuracy"){
				return this.Cvt17_accuracy ;
			} else 
			if(xmlPath=="CVT17/responseTime"){
				return this.Cvt17_responsetime ;
			} else 
			if(xmlPath=="CVT18/accuracy"){
				return this.Cvt18_accuracy ;
			} else 
			if(xmlPath=="CVT18/responseTime"){
				return this.Cvt18_responsetime ;
			} else 
			if(xmlPath=="CVT19/accuracy"){
				return this.Cvt19_accuracy ;
			} else 
			if(xmlPath=="CVT19/responseTime"){
				return this.Cvt19_responsetime ;
			} else 
			if(xmlPath=="CVT20/accuracy"){
				return this.Cvt20_accuracy ;
			} else 
			if(xmlPath=="CVT20/responseTime"){
				return this.Cvt20_responsetime ;
			} else 
			if(xmlPath=="CVT21/accuracy"){
				return this.Cvt21_accuracy ;
			} else 
			if(xmlPath=="CVT21/responseTime"){
				return this.Cvt21_responsetime ;
			} else 
			if(xmlPath=="CVT22/accuracy"){
				return this.Cvt22_accuracy ;
			} else 
			if(xmlPath=="CVT22/responseTime"){
				return this.Cvt22_responsetime ;
			} else 
			if(xmlPath=="CVT23/accuracy"){
				return this.Cvt23_accuracy ;
			} else 
			if(xmlPath=="CVT23/responseTime"){
				return this.Cvt23_responsetime ;
			} else 
			if(xmlPath=="CVT24/accuracy"){
				return this.Cvt24_accuracy ;
			} else 
			if(xmlPath=="CVT24/responseTime"){
				return this.Cvt24_responsetime ;
			} else 
			if(xmlPath=="CVT25/accuracy"){
				return this.Cvt25_accuracy ;
			} else 
			if(xmlPath=="CVT25/responseTime"){
				return this.Cvt25_responsetime ;
			} else 
			if(xmlPath=="CVT26/accuracy"){
				return this.Cvt26_accuracy ;
			} else 
			if(xmlPath=="CVT26/responseTime"){
				return this.Cvt26_responsetime ;
			} else 
			if(xmlPath=="CVT27/accuracy"){
				return this.Cvt27_accuracy ;
			} else 
			if(xmlPath=="CVT27/responseTime"){
				return this.Cvt27_responsetime ;
			} else 
			if(xmlPath=="CVT28/accuracy"){
				return this.Cvt28_accuracy ;
			} else 
			if(xmlPath=="CVT28/responseTime"){
				return this.Cvt28_responsetime ;
			} else 
			if(xmlPath=="CVT29/accuracy"){
				return this.Cvt29_accuracy ;
			} else 
			if(xmlPath=="CVT29/responseTime"){
				return this.Cvt29_responsetime ;
			} else 
			if(xmlPath=="CVT30/accuracy"){
				return this.Cvt30_accuracy ;
			} else 
			if(xmlPath=="CVT30/responseTime"){
				return this.Cvt30_responsetime ;
			} else 
			if(xmlPath=="CVT31/accuracy"){
				return this.Cvt31_accuracy ;
			} else 
			if(xmlPath=="CVT31/responseTime"){
				return this.Cvt31_responsetime ;
			} else 
			if(xmlPath=="CVT32/accuracy"){
				return this.Cvt32_accuracy ;
			} else 
			if(xmlPath=="CVT32/responseTime"){
				return this.Cvt32_responsetime ;
			} else 
			if(xmlPath=="CVT33/accuracy"){
				return this.Cvt33_accuracy ;
			} else 
			if(xmlPath=="CVT33/responseTime"){
				return this.Cvt33_responsetime ;
			} else 
			if(xmlPath=="CVT34/accuracy"){
				return this.Cvt34_accuracy ;
			} else 
			if(xmlPath=="CVT34/responseTime"){
				return this.Cvt34_responsetime ;
			} else 
			if(xmlPath=="CVT35/accuracy"){
				return this.Cvt35_accuracy ;
			} else 
			if(xmlPath=="CVT35/responseTime"){
				return this.Cvt35_responsetime ;
			} else 
			if(xmlPath=="CVT36/accuracy"){
				return this.Cvt36_accuracy ;
			} else 
			if(xmlPath=="CVT36/responseTime"){
				return this.Cvt36_responsetime ;
			} else 
			if(xmlPath=="CVT37/accuracy"){
				return this.Cvt37_accuracy ;
			} else 
			if(xmlPath=="CVT37/responseTime"){
				return this.Cvt37_responsetime ;
			} else 
			if(xmlPath=="CVT38/accuracy"){
				return this.Cvt38_accuracy ;
			} else 
			if(xmlPath=="CVT38/responseTime"){
				return this.Cvt38_responsetime ;
			} else 
			if(xmlPath=="CVT39/accuracy"){
				return this.Cvt39_accuracy ;
			} else 
			if(xmlPath=="CVT39/responseTime"){
				return this.Cvt39_responsetime ;
			} else 
			if(xmlPath=="CVT40/accuracy"){
				return this.Cvt40_accuracy ;
			} else 
			if(xmlPath=="CVT40/responseTime"){
				return this.Cvt40_responsetime ;
			} else 
			if(xmlPath=="OET1/accuracy"){
				return this.Oet1_accuracy ;
			} else 
			if(xmlPath=="OET1/responseTime"){
				return this.Oet1_responsetime ;
			} else 
			if(xmlPath=="OET2/accuracy"){
				return this.Oet2_accuracy ;
			} else 
			if(xmlPath=="OET2/responseTime"){
				return this.Oet2_responsetime ;
			} else 
			if(xmlPath=="OET3/accuracy"){
				return this.Oet3_accuracy ;
			} else 
			if(xmlPath=="OET3/responseTime"){
				return this.Oet3_responsetime ;
			} else 
			if(xmlPath=="OET4/accuracy"){
				return this.Oet4_accuracy ;
			} else 
			if(xmlPath=="OET4/responseTime"){
				return this.Oet4_responsetime ;
			} else 
			if(xmlPath=="OET5/accuracy"){
				return this.Oet5_accuracy ;
			} else 
			if(xmlPath=="OET5/responseTime"){
				return this.Oet5_responsetime ;
			} else 
			if(xmlPath=="OET6/accuracy"){
				return this.Oet6_accuracy ;
			} else 
			if(xmlPath=="OET6/responseTime"){
				return this.Oet6_responsetime ;
			} else 
			if(xmlPath=="OET7/accuracy"){
				return this.Oet7_accuracy ;
			} else 
			if(xmlPath=="OET7/responseTime"){
				return this.Oet7_responsetime ;
			} else 
			if(xmlPath=="OET8/accuracy"){
				return this.Oet8_accuracy ;
			} else 
			if(xmlPath=="OET8/responseTime"){
				return this.Oet8_responsetime ;
			} else 
			if(xmlPath=="OET9/accuracy"){
				return this.Oet9_accuracy ;
			} else 
			if(xmlPath=="OET9/responseTime"){
				return this.Oet9_responsetime ;
			} else 
			if(xmlPath=="OET10/accuracy"){
				return this.Oet10_accuracy ;
			} else 
			if(xmlPath=="OET10/responseTime"){
				return this.Oet10_responsetime ;
			} else 
			if(xmlPath=="OET11/accuracy"){
				return this.Oet11_accuracy ;
			} else 
			if(xmlPath=="OET11/responseTime"){
				return this.Oet11_responsetime ;
			} else 
			if(xmlPath=="OET12/accuracy"){
				return this.Oet12_accuracy ;
			} else 
			if(xmlPath=="OET12/responseTime"){
				return this.Oet12_responsetime ;
			} else 
			if(xmlPath=="OET13/accuracy"){
				return this.Oet13_accuracy ;
			} else 
			if(xmlPath=="OET13/responseTime"){
				return this.Oet13_responsetime ;
			} else 
			if(xmlPath=="OET14/accuracy"){
				return this.Oet14_accuracy ;
			} else 
			if(xmlPath=="OET14/responseTime"){
				return this.Oet14_responsetime ;
			} else 
			if(xmlPath=="OET15/accuracy"){
				return this.Oet15_accuracy ;
			} else 
			if(xmlPath=="OET15/responseTime"){
				return this.Oet15_responsetime ;
			} else 
			if(xmlPath=="OET16/accuracy"){
				return this.Oet16_accuracy ;
			} else 
			if(xmlPath=="OET16/responseTime"){
				return this.Oet16_responsetime ;
			} else 
			if(xmlPath=="OET17/accuracy"){
				return this.Oet17_accuracy ;
			} else 
			if(xmlPath=="OET17/responseTime"){
				return this.Oet17_responsetime ;
			} else 
			if(xmlPath=="OET18/accuracy"){
				return this.Oet18_accuracy ;
			} else 
			if(xmlPath=="OET18/responseTime"){
				return this.Oet18_responsetime ;
			} else 
			if(xmlPath=="OET19/accuracy"){
				return this.Oet19_accuracy ;
			} else 
			if(xmlPath=="OET19/responseTime"){
				return this.Oet19_responsetime ;
			} else 
			if(xmlPath=="OET20/accuracy"){
				return this.Oet20_accuracy ;
			} else 
			if(xmlPath=="OET20/responseTime"){
				return this.Oet20_responsetime ;
			} else 
			if(xmlPath=="OET21/accuracy"){
				return this.Oet21_accuracy ;
			} else 
			if(xmlPath=="OET21/responseTime"){
				return this.Oet21_responsetime ;
			} else 
			if(xmlPath=="OET22/accuracy"){
				return this.Oet22_accuracy ;
			} else 
			if(xmlPath=="OET22/responseTime"){
				return this.Oet22_responsetime ;
			} else 
			if(xmlPath=="OET23/accuracy"){
				return this.Oet23_accuracy ;
			} else 
			if(xmlPath=="OET23/responseTime"){
				return this.Oet23_responsetime ;
			} else 
			if(xmlPath=="OET24/accuracy"){
				return this.Oet24_accuracy ;
			} else 
			if(xmlPath=="OET24/responseTime"){
				return this.Oet24_responsetime ;
			} else 
			if(xmlPath=="OET25/accuracy"){
				return this.Oet25_accuracy ;
			} else 
			if(xmlPath=="OET25/responseTime"){
				return this.Oet25_responsetime ;
			} else 
			if(xmlPath=="OET26/accuracy"){
				return this.Oet26_accuracy ;
			} else 
			if(xmlPath=="OET26/responseTime"){
				return this.Oet26_responsetime ;
			} else 
			if(xmlPath=="OET27/accuracy"){
				return this.Oet27_accuracy ;
			} else 
			if(xmlPath=="OET27/responseTime"){
				return this.Oet27_responsetime ;
			} else 
			if(xmlPath=="OET28/accuracy"){
				return this.Oet28_accuracy ;
			} else 
			if(xmlPath=="OET28/responseTime"){
				return this.Oet28_responsetime ;
			} else 
			if(xmlPath=="OET29/accuracy"){
				return this.Oet29_accuracy ;
			} else 
			if(xmlPath=="OET29/responseTime"){
				return this.Oet29_responsetime ;
			} else 
			if(xmlPath=="OET30/accuracy"){
				return this.Oet30_accuracy ;
			} else 
			if(xmlPath=="OET30/responseTime"){
				return this.Oet30_responsetime ;
			} else 
			if(xmlPath=="OET31/accuracy"){
				return this.Oet31_accuracy ;
			} else 
			if(xmlPath=="OET31/responseTime"){
				return this.Oet31_responsetime ;
			} else 
			if(xmlPath=="OET32/accuracy"){
				return this.Oet32_accuracy ;
			} else 
			if(xmlPath=="OET32/responseTime"){
				return this.Oet32_responsetime ;
			} else 
			if(xmlPath=="OET33/accuracy"){
				return this.Oet33_accuracy ;
			} else 
			if(xmlPath=="OET33/responseTime"){
				return this.Oet33_responsetime ;
			} else 
			if(xmlPath=="OET34/accuracy"){
				return this.Oet34_accuracy ;
			} else 
			if(xmlPath=="OET34/responseTime"){
				return this.Oet34_responsetime ;
			} else 
			if(xmlPath=="OET35/accuracy"){
				return this.Oet35_accuracy ;
			} else 
			if(xmlPath=="OET35/responseTime"){
				return this.Oet35_responsetime ;
			} else 
			if(xmlPath=="OET36/accuracy"){
				return this.Oet36_accuracy ;
			} else 
			if(xmlPath=="OET36/responseTime"){
				return this.Oet36_responsetime ;
			} else 
			if(xmlPath=="OET37/accuracy"){
				return this.Oet37_accuracy ;
			} else 
			if(xmlPath=="OET37/responseTime"){
				return this.Oet37_responsetime ;
			} else 
			if(xmlPath=="OET38/accuracy"){
				return this.Oet38_accuracy ;
			} else 
			if(xmlPath=="OET38/responseTime"){
				return this.Oet38_responsetime ;
			} else 
			if(xmlPath=="OET39/accuracy"){
				return this.Oet39_accuracy ;
			} else 
			if(xmlPath=="OET39/responseTime"){
				return this.Oet39_responsetime ;
			} else 
			if(xmlPath=="OET40/accuracy"){
				return this.Oet40_accuracy ;
			} else 
			if(xmlPath=="OET40/responseTime"){
				return this.Oet40_responsetime ;
			} else 
			if(xmlPath=="CVOET1/accuracy"){
				return this.Cvoet1_accuracy ;
			} else 
			if(xmlPath=="CVOET1/responseTime"){
				return this.Cvoet1_responsetime ;
			} else 
			if(xmlPath=="CVOET2/accuracy"){
				return this.Cvoet2_accuracy ;
			} else 
			if(xmlPath=="CVOET2/responseTime"){
				return this.Cvoet2_responsetime ;
			} else 
			if(xmlPath=="CVOET3/accuracy"){
				return this.Cvoet3_accuracy ;
			} else 
			if(xmlPath=="CVOET3/responseTime"){
				return this.Cvoet3_responsetime ;
			} else 
			if(xmlPath=="CVOET4/accuracy"){
				return this.Cvoet4_accuracy ;
			} else 
			if(xmlPath=="CVOET4/responseTime"){
				return this.Cvoet4_responsetime ;
			} else 
			if(xmlPath=="CVOET5/accuracy"){
				return this.Cvoet5_accuracy ;
			} else 
			if(xmlPath=="CVOET5/responseTime"){
				return this.Cvoet5_responsetime ;
			} else 
			if(xmlPath=="CVOET6/accuracy"){
				return this.Cvoet6_accuracy ;
			} else 
			if(xmlPath=="CVOET6/responseTime"){
				return this.Cvoet6_responsetime ;
			} else 
			if(xmlPath=="CVOET7/accuracy"){
				return this.Cvoet7_accuracy ;
			} else 
			if(xmlPath=="CVOET7/responseTime"){
				return this.Cvoet7_responsetime ;
			} else 
			if(xmlPath=="CVOET8/accuracy"){
				return this.Cvoet8_accuracy ;
			} else 
			if(xmlPath=="CVOET8/responseTime"){
				return this.Cvoet8_responsetime ;
			} else 
			if(xmlPath=="CVOET9/accuracy"){
				return this.Cvoet9_accuracy ;
			} else 
			if(xmlPath=="CVOET9/responseTime"){
				return this.Cvoet9_responsetime ;
			} else 
			if(xmlPath=="CVOET10/accuracy"){
				return this.Cvoet10_accuracy ;
			} else 
			if(xmlPath=="CVOET10/responseTime"){
				return this.Cvoet10_responsetime ;
			} else 
			if(xmlPath=="CVOET11/accuracy"){
				return this.Cvoet11_accuracy ;
			} else 
			if(xmlPath=="CVOET11/responseTime"){
				return this.Cvoet11_responsetime ;
			} else 
			if(xmlPath=="CVOET12/accuracy"){
				return this.Cvoet12_accuracy ;
			} else 
			if(xmlPath=="CVOET12/responseTime"){
				return this.Cvoet12_responsetime ;
			} else 
			if(xmlPath=="CVOET13/accuracy"){
				return this.Cvoet13_accuracy ;
			} else 
			if(xmlPath=="CVOET13/responseTime"){
				return this.Cvoet13_responsetime ;
			} else 
			if(xmlPath=="CVOET14/accuracy"){
				return this.Cvoet14_accuracy ;
			} else 
			if(xmlPath=="CVOET14/responseTime"){
				return this.Cvoet14_responsetime ;
			} else 
			if(xmlPath=="CVOET15/accuracy"){
				return this.Cvoet15_accuracy ;
			} else 
			if(xmlPath=="CVOET15/responseTime"){
				return this.Cvoet15_responsetime ;
			} else 
			if(xmlPath=="CVOET16/accuracy"){
				return this.Cvoet16_accuracy ;
			} else 
			if(xmlPath=="CVOET16/responseTime"){
				return this.Cvoet16_responsetime ;
			} else 
			if(xmlPath=="CVOET17/accuracy"){
				return this.Cvoet17_accuracy ;
			} else 
			if(xmlPath=="CVOET17/responseTime"){
				return this.Cvoet17_responsetime ;
			} else 
			if(xmlPath=="CVOET18/accuracy"){
				return this.Cvoet18_accuracy ;
			} else 
			if(xmlPath=="CVOET18/responseTime"){
				return this.Cvoet18_responsetime ;
			} else 
			if(xmlPath=="CVOET19/accuracy"){
				return this.Cvoet19_accuracy ;
			} else 
			if(xmlPath=="CVOET19/responseTime"){
				return this.Cvoet19_responsetime ;
			} else 
			if(xmlPath=="CVOET20/accuracy"){
				return this.Cvoet20_accuracy ;
			} else 
			if(xmlPath=="CVOET20/responseTime"){
				return this.Cvoet20_responsetime ;
			} else 
			if(xmlPath=="CVOET21/accuracy"){
				return this.Cvoet21_accuracy ;
			} else 
			if(xmlPath=="CVOET21/responseTime"){
				return this.Cvoet21_responsetime ;
			} else 
			if(xmlPath=="CVOET22/accuracy"){
				return this.Cvoet22_accuracy ;
			} else 
			if(xmlPath=="CVOET22/responseTime"){
				return this.Cvoet22_responsetime ;
			} else 
			if(xmlPath=="CVOET23/accuracy"){
				return this.Cvoet23_accuracy ;
			} else 
			if(xmlPath=="CVOET23/responseTime"){
				return this.Cvoet23_responsetime ;
			} else 
			if(xmlPath=="CVOET24/accuracy"){
				return this.Cvoet24_accuracy ;
			} else 
			if(xmlPath=="CVOET24/responseTime"){
				return this.Cvoet24_responsetime ;
			} else 
			if(xmlPath=="CVOET25/accuracy"){
				return this.Cvoet25_accuracy ;
			} else 
			if(xmlPath=="CVOET25/responseTime"){
				return this.Cvoet25_responsetime ;
			} else 
			if(xmlPath=="CVOET26/accuracy"){
				return this.Cvoet26_accuracy ;
			} else 
			if(xmlPath=="CVOET26/responseTime"){
				return this.Cvoet26_responsetime ;
			} else 
			if(xmlPath=="CVOET27/accuracy"){
				return this.Cvoet27_accuracy ;
			} else 
			if(xmlPath=="CVOET27/responseTime"){
				return this.Cvoet27_responsetime ;
			} else 
			if(xmlPath=="CVOET28/accuracy"){
				return this.Cvoet28_accuracy ;
			} else 
			if(xmlPath=="CVOET28/responseTime"){
				return this.Cvoet28_responsetime ;
			} else 
			if(xmlPath=="CVOET29/accuracy"){
				return this.Cvoet29_accuracy ;
			} else 
			if(xmlPath=="CVOET29/responseTime"){
				return this.Cvoet29_responsetime ;
			} else 
			if(xmlPath=="CVOET30/accuracy"){
				return this.Cvoet30_accuracy ;
			} else 
			if(xmlPath=="CVOET30/responseTime"){
				return this.Cvoet30_responsetime ;
			} else 
			if(xmlPath=="CVOET31/accuracy"){
				return this.Cvoet31_accuracy ;
			} else 
			if(xmlPath=="CVOET31/responseTime"){
				return this.Cvoet31_responsetime ;
			} else 
			if(xmlPath=="CVOET32/accuracy"){
				return this.Cvoet32_accuracy ;
			} else 
			if(xmlPath=="CVOET32/responseTime"){
				return this.Cvoet32_responsetime ;
			} else 
			if(xmlPath=="CVOET33/accuracy"){
				return this.Cvoet33_accuracy ;
			} else 
			if(xmlPath=="CVOET33/responseTime"){
				return this.Cvoet33_responsetime ;
			} else 
			if(xmlPath=="CVOET34/accuracy"){
				return this.Cvoet34_accuracy ;
			} else 
			if(xmlPath=="CVOET34/responseTime"){
				return this.Cvoet34_responsetime ;
			} else 
			if(xmlPath=="CVOET35/accuracy"){
				return this.Cvoet35_accuracy ;
			} else 
			if(xmlPath=="CVOET35/responseTime"){
				return this.Cvoet35_responsetime ;
			} else 
			if(xmlPath=="CVOET36/accuracy"){
				return this.Cvoet36_accuracy ;
			} else 
			if(xmlPath=="CVOET36/responseTime"){
				return this.Cvoet36_responsetime ;
			} else 
			if(xmlPath=="CVOET37/accuracy"){
				return this.Cvoet37_accuracy ;
			} else 
			if(xmlPath=="CVOET37/responseTime"){
				return this.Cvoet37_responsetime ;
			} else 
			if(xmlPath=="CVOET38/accuracy"){
				return this.Cvoet38_accuracy ;
			} else 
			if(xmlPath=="CVOET38/responseTime"){
				return this.Cvoet38_responsetime ;
			} else 
			if(xmlPath=="CVOET39/accuracy"){
				return this.Cvoet39_accuracy ;
			} else 
			if(xmlPath=="CVOET39/responseTime"){
				return this.Cvoet39_responsetime ;
			} else 
			if(xmlPath=="CVOET40/accuracy"){
				return this.Cvoet40_accuracy ;
			} else 
			if(xmlPath=="CVOET40/responseTime"){
				return this.Cvoet40_responsetime ;
			} else 
			if(xmlPath=="CVOET41/accuracy"){
				return this.Cvoet41_accuracy ;
			} else 
			if(xmlPath=="CVOET41/responseTime"){
				return this.Cvoet41_responsetime ;
			} else 
			if(xmlPath=="CVOET42/accuracy"){
				return this.Cvoet42_accuracy ;
			} else 
			if(xmlPath=="CVOET42/responseTime"){
				return this.Cvoet42_responsetime ;
			} else 
			if(xmlPath=="CVOET43/accuracy"){
				return this.Cvoet43_accuracy ;
			} else 
			if(xmlPath=="CVOET43/responseTime"){
				return this.Cvoet43_responsetime ;
			} else 
			if(xmlPath=="CVOET44/accuracy"){
				return this.Cvoet44_accuracy ;
			} else 
			if(xmlPath=="CVOET44/responseTime"){
				return this.Cvoet44_responsetime ;
			} else 
			if(xmlPath=="CVOET45/accuracy"){
				return this.Cvoet45_accuracy ;
			} else 
			if(xmlPath=="CVOET45/responseTime"){
				return this.Cvoet45_responsetime ;
			} else 
			if(xmlPath=="CVOET46/accuracy"){
				return this.Cvoet46_accuracy ;
			} else 
			if(xmlPath=="CVOET46/responseTime"){
				return this.Cvoet46_responsetime ;
			} else 
			if(xmlPath=="CVOET47/accuracy"){
				return this.Cvoet47_accuracy ;
			} else 
			if(xmlPath=="CVOET47/responseTime"){
				return this.Cvoet47_responsetime ;
			} else 
			if(xmlPath=="CVOET48/accuracy"){
				return this.Cvoet48_accuracy ;
			} else 
			if(xmlPath=="CVOET48/responseTime"){
				return this.Cvoet48_responsetime ;
			} else 
			if(xmlPath=="CVOET49/accuracy"){
				return this.Cvoet49_accuracy ;
			} else 
			if(xmlPath=="CVOET49/responseTime"){
				return this.Cvoet49_responsetime ;
			} else 
			if(xmlPath=="CVOET50/accuracy"){
				return this.Cvoet50_accuracy ;
			} else 
			if(xmlPath=="CVOET50/responseTime"){
				return this.Cvoet50_responsetime ;
			} else 
			if(xmlPath=="CVOET51/accuracy"){
				return this.Cvoet51_accuracy ;
			} else 
			if(xmlPath=="CVOET51/responseTime"){
				return this.Cvoet51_responsetime ;
			} else 
			if(xmlPath=="CVOET52/accuracy"){
				return this.Cvoet52_accuracy ;
			} else 
			if(xmlPath=="CVOET52/responseTime"){
				return this.Cvoet52_responsetime ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="CVT1/accuracy"){
				this.Cvt1_accuracy=value;
			} else 
			if(xmlPath=="CVT1/responseTime"){
				this.Cvt1_responsetime=value;
			} else 
			if(xmlPath=="CVT2/accuracy"){
				this.Cvt2_accuracy=value;
			} else 
			if(xmlPath=="CVT2/responseTime"){
				this.Cvt2_responsetime=value;
			} else 
			if(xmlPath=="CVT3/accuracy"){
				this.Cvt3_accuracy=value;
			} else 
			if(xmlPath=="CVT3/responseTime"){
				this.Cvt3_responsetime=value;
			} else 
			if(xmlPath=="CVT4/accuracy"){
				this.Cvt4_accuracy=value;
			} else 
			if(xmlPath=="CVT4/responseTime"){
				this.Cvt4_responsetime=value;
			} else 
			if(xmlPath=="CVT5/accuracy"){
				this.Cvt5_accuracy=value;
			} else 
			if(xmlPath=="CVT5/responseTime"){
				this.Cvt5_responsetime=value;
			} else 
			if(xmlPath=="CVT6/accuracy"){
				this.Cvt6_accuracy=value;
			} else 
			if(xmlPath=="CVT6/responseTime"){
				this.Cvt6_responsetime=value;
			} else 
			if(xmlPath=="CVT7/accuracy"){
				this.Cvt7_accuracy=value;
			} else 
			if(xmlPath=="CVT7/responseTime"){
				this.Cvt7_responsetime=value;
			} else 
			if(xmlPath=="CVT8/accuracy"){
				this.Cvt8_accuracy=value;
			} else 
			if(xmlPath=="CVT8/responseTime"){
				this.Cvt8_responsetime=value;
			} else 
			if(xmlPath=="CVT9/accuracy"){
				this.Cvt9_accuracy=value;
			} else 
			if(xmlPath=="CVT9/responseTime"){
				this.Cvt9_responsetime=value;
			} else 
			if(xmlPath=="CVT10/accuracy"){
				this.Cvt10_accuracy=value;
			} else 
			if(xmlPath=="CVT10/responseTime"){
				this.Cvt10_responsetime=value;
			} else 
			if(xmlPath=="CVT11/accuracy"){
				this.Cvt11_accuracy=value;
			} else 
			if(xmlPath=="CVT11/responseTime"){
				this.Cvt11_responsetime=value;
			} else 
			if(xmlPath=="CVT12/accuracy"){
				this.Cvt12_accuracy=value;
			} else 
			if(xmlPath=="CVT12/responseTime"){
				this.Cvt12_responsetime=value;
			} else 
			if(xmlPath=="CVT13/accuracy"){
				this.Cvt13_accuracy=value;
			} else 
			if(xmlPath=="CVT13/responseTime"){
				this.Cvt13_responsetime=value;
			} else 
			if(xmlPath=="CVT14/accuracy"){
				this.Cvt14_accuracy=value;
			} else 
			if(xmlPath=="CVT14/responseTime"){
				this.Cvt14_responsetime=value;
			} else 
			if(xmlPath=="CVT15/accuracy"){
				this.Cvt15_accuracy=value;
			} else 
			if(xmlPath=="CVT15/responseTime"){
				this.Cvt15_responsetime=value;
			} else 
			if(xmlPath=="CVT16/accuracy"){
				this.Cvt16_accuracy=value;
			} else 
			if(xmlPath=="CVT16/responseTime"){
				this.Cvt16_responsetime=value;
			} else 
			if(xmlPath=="CVT17/accuracy"){
				this.Cvt17_accuracy=value;
			} else 
			if(xmlPath=="CVT17/responseTime"){
				this.Cvt17_responsetime=value;
			} else 
			if(xmlPath=="CVT18/accuracy"){
				this.Cvt18_accuracy=value;
			} else 
			if(xmlPath=="CVT18/responseTime"){
				this.Cvt18_responsetime=value;
			} else 
			if(xmlPath=="CVT19/accuracy"){
				this.Cvt19_accuracy=value;
			} else 
			if(xmlPath=="CVT19/responseTime"){
				this.Cvt19_responsetime=value;
			} else 
			if(xmlPath=="CVT20/accuracy"){
				this.Cvt20_accuracy=value;
			} else 
			if(xmlPath=="CVT20/responseTime"){
				this.Cvt20_responsetime=value;
			} else 
			if(xmlPath=="CVT21/accuracy"){
				this.Cvt21_accuracy=value;
			} else 
			if(xmlPath=="CVT21/responseTime"){
				this.Cvt21_responsetime=value;
			} else 
			if(xmlPath=="CVT22/accuracy"){
				this.Cvt22_accuracy=value;
			} else 
			if(xmlPath=="CVT22/responseTime"){
				this.Cvt22_responsetime=value;
			} else 
			if(xmlPath=="CVT23/accuracy"){
				this.Cvt23_accuracy=value;
			} else 
			if(xmlPath=="CVT23/responseTime"){
				this.Cvt23_responsetime=value;
			} else 
			if(xmlPath=="CVT24/accuracy"){
				this.Cvt24_accuracy=value;
			} else 
			if(xmlPath=="CVT24/responseTime"){
				this.Cvt24_responsetime=value;
			} else 
			if(xmlPath=="CVT25/accuracy"){
				this.Cvt25_accuracy=value;
			} else 
			if(xmlPath=="CVT25/responseTime"){
				this.Cvt25_responsetime=value;
			} else 
			if(xmlPath=="CVT26/accuracy"){
				this.Cvt26_accuracy=value;
			} else 
			if(xmlPath=="CVT26/responseTime"){
				this.Cvt26_responsetime=value;
			} else 
			if(xmlPath=="CVT27/accuracy"){
				this.Cvt27_accuracy=value;
			} else 
			if(xmlPath=="CVT27/responseTime"){
				this.Cvt27_responsetime=value;
			} else 
			if(xmlPath=="CVT28/accuracy"){
				this.Cvt28_accuracy=value;
			} else 
			if(xmlPath=="CVT28/responseTime"){
				this.Cvt28_responsetime=value;
			} else 
			if(xmlPath=="CVT29/accuracy"){
				this.Cvt29_accuracy=value;
			} else 
			if(xmlPath=="CVT29/responseTime"){
				this.Cvt29_responsetime=value;
			} else 
			if(xmlPath=="CVT30/accuracy"){
				this.Cvt30_accuracy=value;
			} else 
			if(xmlPath=="CVT30/responseTime"){
				this.Cvt30_responsetime=value;
			} else 
			if(xmlPath=="CVT31/accuracy"){
				this.Cvt31_accuracy=value;
			} else 
			if(xmlPath=="CVT31/responseTime"){
				this.Cvt31_responsetime=value;
			} else 
			if(xmlPath=="CVT32/accuracy"){
				this.Cvt32_accuracy=value;
			} else 
			if(xmlPath=="CVT32/responseTime"){
				this.Cvt32_responsetime=value;
			} else 
			if(xmlPath=="CVT33/accuracy"){
				this.Cvt33_accuracy=value;
			} else 
			if(xmlPath=="CVT33/responseTime"){
				this.Cvt33_responsetime=value;
			} else 
			if(xmlPath=="CVT34/accuracy"){
				this.Cvt34_accuracy=value;
			} else 
			if(xmlPath=="CVT34/responseTime"){
				this.Cvt34_responsetime=value;
			} else 
			if(xmlPath=="CVT35/accuracy"){
				this.Cvt35_accuracy=value;
			} else 
			if(xmlPath=="CVT35/responseTime"){
				this.Cvt35_responsetime=value;
			} else 
			if(xmlPath=="CVT36/accuracy"){
				this.Cvt36_accuracy=value;
			} else 
			if(xmlPath=="CVT36/responseTime"){
				this.Cvt36_responsetime=value;
			} else 
			if(xmlPath=="CVT37/accuracy"){
				this.Cvt37_accuracy=value;
			} else 
			if(xmlPath=="CVT37/responseTime"){
				this.Cvt37_responsetime=value;
			} else 
			if(xmlPath=="CVT38/accuracy"){
				this.Cvt38_accuracy=value;
			} else 
			if(xmlPath=="CVT38/responseTime"){
				this.Cvt38_responsetime=value;
			} else 
			if(xmlPath=="CVT39/accuracy"){
				this.Cvt39_accuracy=value;
			} else 
			if(xmlPath=="CVT39/responseTime"){
				this.Cvt39_responsetime=value;
			} else 
			if(xmlPath=="CVT40/accuracy"){
				this.Cvt40_accuracy=value;
			} else 
			if(xmlPath=="CVT40/responseTime"){
				this.Cvt40_responsetime=value;
			} else 
			if(xmlPath=="OET1/accuracy"){
				this.Oet1_accuracy=value;
			} else 
			if(xmlPath=="OET1/responseTime"){
				this.Oet1_responsetime=value;
			} else 
			if(xmlPath=="OET2/accuracy"){
				this.Oet2_accuracy=value;
			} else 
			if(xmlPath=="OET2/responseTime"){
				this.Oet2_responsetime=value;
			} else 
			if(xmlPath=="OET3/accuracy"){
				this.Oet3_accuracy=value;
			} else 
			if(xmlPath=="OET3/responseTime"){
				this.Oet3_responsetime=value;
			} else 
			if(xmlPath=="OET4/accuracy"){
				this.Oet4_accuracy=value;
			} else 
			if(xmlPath=="OET4/responseTime"){
				this.Oet4_responsetime=value;
			} else 
			if(xmlPath=="OET5/accuracy"){
				this.Oet5_accuracy=value;
			} else 
			if(xmlPath=="OET5/responseTime"){
				this.Oet5_responsetime=value;
			} else 
			if(xmlPath=="OET6/accuracy"){
				this.Oet6_accuracy=value;
			} else 
			if(xmlPath=="OET6/responseTime"){
				this.Oet6_responsetime=value;
			} else 
			if(xmlPath=="OET7/accuracy"){
				this.Oet7_accuracy=value;
			} else 
			if(xmlPath=="OET7/responseTime"){
				this.Oet7_responsetime=value;
			} else 
			if(xmlPath=="OET8/accuracy"){
				this.Oet8_accuracy=value;
			} else 
			if(xmlPath=="OET8/responseTime"){
				this.Oet8_responsetime=value;
			} else 
			if(xmlPath=="OET9/accuracy"){
				this.Oet9_accuracy=value;
			} else 
			if(xmlPath=="OET9/responseTime"){
				this.Oet9_responsetime=value;
			} else 
			if(xmlPath=="OET10/accuracy"){
				this.Oet10_accuracy=value;
			} else 
			if(xmlPath=="OET10/responseTime"){
				this.Oet10_responsetime=value;
			} else 
			if(xmlPath=="OET11/accuracy"){
				this.Oet11_accuracy=value;
			} else 
			if(xmlPath=="OET11/responseTime"){
				this.Oet11_responsetime=value;
			} else 
			if(xmlPath=="OET12/accuracy"){
				this.Oet12_accuracy=value;
			} else 
			if(xmlPath=="OET12/responseTime"){
				this.Oet12_responsetime=value;
			} else 
			if(xmlPath=="OET13/accuracy"){
				this.Oet13_accuracy=value;
			} else 
			if(xmlPath=="OET13/responseTime"){
				this.Oet13_responsetime=value;
			} else 
			if(xmlPath=="OET14/accuracy"){
				this.Oet14_accuracy=value;
			} else 
			if(xmlPath=="OET14/responseTime"){
				this.Oet14_responsetime=value;
			} else 
			if(xmlPath=="OET15/accuracy"){
				this.Oet15_accuracy=value;
			} else 
			if(xmlPath=="OET15/responseTime"){
				this.Oet15_responsetime=value;
			} else 
			if(xmlPath=="OET16/accuracy"){
				this.Oet16_accuracy=value;
			} else 
			if(xmlPath=="OET16/responseTime"){
				this.Oet16_responsetime=value;
			} else 
			if(xmlPath=="OET17/accuracy"){
				this.Oet17_accuracy=value;
			} else 
			if(xmlPath=="OET17/responseTime"){
				this.Oet17_responsetime=value;
			} else 
			if(xmlPath=="OET18/accuracy"){
				this.Oet18_accuracy=value;
			} else 
			if(xmlPath=="OET18/responseTime"){
				this.Oet18_responsetime=value;
			} else 
			if(xmlPath=="OET19/accuracy"){
				this.Oet19_accuracy=value;
			} else 
			if(xmlPath=="OET19/responseTime"){
				this.Oet19_responsetime=value;
			} else 
			if(xmlPath=="OET20/accuracy"){
				this.Oet20_accuracy=value;
			} else 
			if(xmlPath=="OET20/responseTime"){
				this.Oet20_responsetime=value;
			} else 
			if(xmlPath=="OET21/accuracy"){
				this.Oet21_accuracy=value;
			} else 
			if(xmlPath=="OET21/responseTime"){
				this.Oet21_responsetime=value;
			} else 
			if(xmlPath=="OET22/accuracy"){
				this.Oet22_accuracy=value;
			} else 
			if(xmlPath=="OET22/responseTime"){
				this.Oet22_responsetime=value;
			} else 
			if(xmlPath=="OET23/accuracy"){
				this.Oet23_accuracy=value;
			} else 
			if(xmlPath=="OET23/responseTime"){
				this.Oet23_responsetime=value;
			} else 
			if(xmlPath=="OET24/accuracy"){
				this.Oet24_accuracy=value;
			} else 
			if(xmlPath=="OET24/responseTime"){
				this.Oet24_responsetime=value;
			} else 
			if(xmlPath=="OET25/accuracy"){
				this.Oet25_accuracy=value;
			} else 
			if(xmlPath=="OET25/responseTime"){
				this.Oet25_responsetime=value;
			} else 
			if(xmlPath=="OET26/accuracy"){
				this.Oet26_accuracy=value;
			} else 
			if(xmlPath=="OET26/responseTime"){
				this.Oet26_responsetime=value;
			} else 
			if(xmlPath=="OET27/accuracy"){
				this.Oet27_accuracy=value;
			} else 
			if(xmlPath=="OET27/responseTime"){
				this.Oet27_responsetime=value;
			} else 
			if(xmlPath=="OET28/accuracy"){
				this.Oet28_accuracy=value;
			} else 
			if(xmlPath=="OET28/responseTime"){
				this.Oet28_responsetime=value;
			} else 
			if(xmlPath=="OET29/accuracy"){
				this.Oet29_accuracy=value;
			} else 
			if(xmlPath=="OET29/responseTime"){
				this.Oet29_responsetime=value;
			} else 
			if(xmlPath=="OET30/accuracy"){
				this.Oet30_accuracy=value;
			} else 
			if(xmlPath=="OET30/responseTime"){
				this.Oet30_responsetime=value;
			} else 
			if(xmlPath=="OET31/accuracy"){
				this.Oet31_accuracy=value;
			} else 
			if(xmlPath=="OET31/responseTime"){
				this.Oet31_responsetime=value;
			} else 
			if(xmlPath=="OET32/accuracy"){
				this.Oet32_accuracy=value;
			} else 
			if(xmlPath=="OET32/responseTime"){
				this.Oet32_responsetime=value;
			} else 
			if(xmlPath=="OET33/accuracy"){
				this.Oet33_accuracy=value;
			} else 
			if(xmlPath=="OET33/responseTime"){
				this.Oet33_responsetime=value;
			} else 
			if(xmlPath=="OET34/accuracy"){
				this.Oet34_accuracy=value;
			} else 
			if(xmlPath=="OET34/responseTime"){
				this.Oet34_responsetime=value;
			} else 
			if(xmlPath=="OET35/accuracy"){
				this.Oet35_accuracy=value;
			} else 
			if(xmlPath=="OET35/responseTime"){
				this.Oet35_responsetime=value;
			} else 
			if(xmlPath=="OET36/accuracy"){
				this.Oet36_accuracy=value;
			} else 
			if(xmlPath=="OET36/responseTime"){
				this.Oet36_responsetime=value;
			} else 
			if(xmlPath=="OET37/accuracy"){
				this.Oet37_accuracy=value;
			} else 
			if(xmlPath=="OET37/responseTime"){
				this.Oet37_responsetime=value;
			} else 
			if(xmlPath=="OET38/accuracy"){
				this.Oet38_accuracy=value;
			} else 
			if(xmlPath=="OET38/responseTime"){
				this.Oet38_responsetime=value;
			} else 
			if(xmlPath=="OET39/accuracy"){
				this.Oet39_accuracy=value;
			} else 
			if(xmlPath=="OET39/responseTime"){
				this.Oet39_responsetime=value;
			} else 
			if(xmlPath=="OET40/accuracy"){
				this.Oet40_accuracy=value;
			} else 
			if(xmlPath=="OET40/responseTime"){
				this.Oet40_responsetime=value;
			} else 
			if(xmlPath=="CVOET1/accuracy"){
				this.Cvoet1_accuracy=value;
			} else 
			if(xmlPath=="CVOET1/responseTime"){
				this.Cvoet1_responsetime=value;
			} else 
			if(xmlPath=="CVOET2/accuracy"){
				this.Cvoet2_accuracy=value;
			} else 
			if(xmlPath=="CVOET2/responseTime"){
				this.Cvoet2_responsetime=value;
			} else 
			if(xmlPath=="CVOET3/accuracy"){
				this.Cvoet3_accuracy=value;
			} else 
			if(xmlPath=="CVOET3/responseTime"){
				this.Cvoet3_responsetime=value;
			} else 
			if(xmlPath=="CVOET4/accuracy"){
				this.Cvoet4_accuracy=value;
			} else 
			if(xmlPath=="CVOET4/responseTime"){
				this.Cvoet4_responsetime=value;
			} else 
			if(xmlPath=="CVOET5/accuracy"){
				this.Cvoet5_accuracy=value;
			} else 
			if(xmlPath=="CVOET5/responseTime"){
				this.Cvoet5_responsetime=value;
			} else 
			if(xmlPath=="CVOET6/accuracy"){
				this.Cvoet6_accuracy=value;
			} else 
			if(xmlPath=="CVOET6/responseTime"){
				this.Cvoet6_responsetime=value;
			} else 
			if(xmlPath=="CVOET7/accuracy"){
				this.Cvoet7_accuracy=value;
			} else 
			if(xmlPath=="CVOET7/responseTime"){
				this.Cvoet7_responsetime=value;
			} else 
			if(xmlPath=="CVOET8/accuracy"){
				this.Cvoet8_accuracy=value;
			} else 
			if(xmlPath=="CVOET8/responseTime"){
				this.Cvoet8_responsetime=value;
			} else 
			if(xmlPath=="CVOET9/accuracy"){
				this.Cvoet9_accuracy=value;
			} else 
			if(xmlPath=="CVOET9/responseTime"){
				this.Cvoet9_responsetime=value;
			} else 
			if(xmlPath=="CVOET10/accuracy"){
				this.Cvoet10_accuracy=value;
			} else 
			if(xmlPath=="CVOET10/responseTime"){
				this.Cvoet10_responsetime=value;
			} else 
			if(xmlPath=="CVOET11/accuracy"){
				this.Cvoet11_accuracy=value;
			} else 
			if(xmlPath=="CVOET11/responseTime"){
				this.Cvoet11_responsetime=value;
			} else 
			if(xmlPath=="CVOET12/accuracy"){
				this.Cvoet12_accuracy=value;
			} else 
			if(xmlPath=="CVOET12/responseTime"){
				this.Cvoet12_responsetime=value;
			} else 
			if(xmlPath=="CVOET13/accuracy"){
				this.Cvoet13_accuracy=value;
			} else 
			if(xmlPath=="CVOET13/responseTime"){
				this.Cvoet13_responsetime=value;
			} else 
			if(xmlPath=="CVOET14/accuracy"){
				this.Cvoet14_accuracy=value;
			} else 
			if(xmlPath=="CVOET14/responseTime"){
				this.Cvoet14_responsetime=value;
			} else 
			if(xmlPath=="CVOET15/accuracy"){
				this.Cvoet15_accuracy=value;
			} else 
			if(xmlPath=="CVOET15/responseTime"){
				this.Cvoet15_responsetime=value;
			} else 
			if(xmlPath=="CVOET16/accuracy"){
				this.Cvoet16_accuracy=value;
			} else 
			if(xmlPath=="CVOET16/responseTime"){
				this.Cvoet16_responsetime=value;
			} else 
			if(xmlPath=="CVOET17/accuracy"){
				this.Cvoet17_accuracy=value;
			} else 
			if(xmlPath=="CVOET17/responseTime"){
				this.Cvoet17_responsetime=value;
			} else 
			if(xmlPath=="CVOET18/accuracy"){
				this.Cvoet18_accuracy=value;
			} else 
			if(xmlPath=="CVOET18/responseTime"){
				this.Cvoet18_responsetime=value;
			} else 
			if(xmlPath=="CVOET19/accuracy"){
				this.Cvoet19_accuracy=value;
			} else 
			if(xmlPath=="CVOET19/responseTime"){
				this.Cvoet19_responsetime=value;
			} else 
			if(xmlPath=="CVOET20/accuracy"){
				this.Cvoet20_accuracy=value;
			} else 
			if(xmlPath=="CVOET20/responseTime"){
				this.Cvoet20_responsetime=value;
			} else 
			if(xmlPath=="CVOET21/accuracy"){
				this.Cvoet21_accuracy=value;
			} else 
			if(xmlPath=="CVOET21/responseTime"){
				this.Cvoet21_responsetime=value;
			} else 
			if(xmlPath=="CVOET22/accuracy"){
				this.Cvoet22_accuracy=value;
			} else 
			if(xmlPath=="CVOET22/responseTime"){
				this.Cvoet22_responsetime=value;
			} else 
			if(xmlPath=="CVOET23/accuracy"){
				this.Cvoet23_accuracy=value;
			} else 
			if(xmlPath=="CVOET23/responseTime"){
				this.Cvoet23_responsetime=value;
			} else 
			if(xmlPath=="CVOET24/accuracy"){
				this.Cvoet24_accuracy=value;
			} else 
			if(xmlPath=="CVOET24/responseTime"){
				this.Cvoet24_responsetime=value;
			} else 
			if(xmlPath=="CVOET25/accuracy"){
				this.Cvoet25_accuracy=value;
			} else 
			if(xmlPath=="CVOET25/responseTime"){
				this.Cvoet25_responsetime=value;
			} else 
			if(xmlPath=="CVOET26/accuracy"){
				this.Cvoet26_accuracy=value;
			} else 
			if(xmlPath=="CVOET26/responseTime"){
				this.Cvoet26_responsetime=value;
			} else 
			if(xmlPath=="CVOET27/accuracy"){
				this.Cvoet27_accuracy=value;
			} else 
			if(xmlPath=="CVOET27/responseTime"){
				this.Cvoet27_responsetime=value;
			} else 
			if(xmlPath=="CVOET28/accuracy"){
				this.Cvoet28_accuracy=value;
			} else 
			if(xmlPath=="CVOET28/responseTime"){
				this.Cvoet28_responsetime=value;
			} else 
			if(xmlPath=="CVOET29/accuracy"){
				this.Cvoet29_accuracy=value;
			} else 
			if(xmlPath=="CVOET29/responseTime"){
				this.Cvoet29_responsetime=value;
			} else 
			if(xmlPath=="CVOET30/accuracy"){
				this.Cvoet30_accuracy=value;
			} else 
			if(xmlPath=="CVOET30/responseTime"){
				this.Cvoet30_responsetime=value;
			} else 
			if(xmlPath=="CVOET31/accuracy"){
				this.Cvoet31_accuracy=value;
			} else 
			if(xmlPath=="CVOET31/responseTime"){
				this.Cvoet31_responsetime=value;
			} else 
			if(xmlPath=="CVOET32/accuracy"){
				this.Cvoet32_accuracy=value;
			} else 
			if(xmlPath=="CVOET32/responseTime"){
				this.Cvoet32_responsetime=value;
			} else 
			if(xmlPath=="CVOET33/accuracy"){
				this.Cvoet33_accuracy=value;
			} else 
			if(xmlPath=="CVOET33/responseTime"){
				this.Cvoet33_responsetime=value;
			} else 
			if(xmlPath=="CVOET34/accuracy"){
				this.Cvoet34_accuracy=value;
			} else 
			if(xmlPath=="CVOET34/responseTime"){
				this.Cvoet34_responsetime=value;
			} else 
			if(xmlPath=="CVOET35/accuracy"){
				this.Cvoet35_accuracy=value;
			} else 
			if(xmlPath=="CVOET35/responseTime"){
				this.Cvoet35_responsetime=value;
			} else 
			if(xmlPath=="CVOET36/accuracy"){
				this.Cvoet36_accuracy=value;
			} else 
			if(xmlPath=="CVOET36/responseTime"){
				this.Cvoet36_responsetime=value;
			} else 
			if(xmlPath=="CVOET37/accuracy"){
				this.Cvoet37_accuracy=value;
			} else 
			if(xmlPath=="CVOET37/responseTime"){
				this.Cvoet37_responsetime=value;
			} else 
			if(xmlPath=="CVOET38/accuracy"){
				this.Cvoet38_accuracy=value;
			} else 
			if(xmlPath=="CVOET38/responseTime"){
				this.Cvoet38_responsetime=value;
			} else 
			if(xmlPath=="CVOET39/accuracy"){
				this.Cvoet39_accuracy=value;
			} else 
			if(xmlPath=="CVOET39/responseTime"){
				this.Cvoet39_responsetime=value;
			} else 
			if(xmlPath=="CVOET40/accuracy"){
				this.Cvoet40_accuracy=value;
			} else 
			if(xmlPath=="CVOET40/responseTime"){
				this.Cvoet40_responsetime=value;
			} else 
			if(xmlPath=="CVOET41/accuracy"){
				this.Cvoet41_accuracy=value;
			} else 
			if(xmlPath=="CVOET41/responseTime"){
				this.Cvoet41_responsetime=value;
			} else 
			if(xmlPath=="CVOET42/accuracy"){
				this.Cvoet42_accuracy=value;
			} else 
			if(xmlPath=="CVOET42/responseTime"){
				this.Cvoet42_responsetime=value;
			} else 
			if(xmlPath=="CVOET43/accuracy"){
				this.Cvoet43_accuracy=value;
			} else 
			if(xmlPath=="CVOET43/responseTime"){
				this.Cvoet43_responsetime=value;
			} else 
			if(xmlPath=="CVOET44/accuracy"){
				this.Cvoet44_accuracy=value;
			} else 
			if(xmlPath=="CVOET44/responseTime"){
				this.Cvoet44_responsetime=value;
			} else 
			if(xmlPath=="CVOET45/accuracy"){
				this.Cvoet45_accuracy=value;
			} else 
			if(xmlPath=="CVOET45/responseTime"){
				this.Cvoet45_responsetime=value;
			} else 
			if(xmlPath=="CVOET46/accuracy"){
				this.Cvoet46_accuracy=value;
			} else 
			if(xmlPath=="CVOET46/responseTime"){
				this.Cvoet46_responsetime=value;
			} else 
			if(xmlPath=="CVOET47/accuracy"){
				this.Cvoet47_accuracy=value;
			} else 
			if(xmlPath=="CVOET47/responseTime"){
				this.Cvoet47_responsetime=value;
			} else 
			if(xmlPath=="CVOET48/accuracy"){
				this.Cvoet48_accuracy=value;
			} else 
			if(xmlPath=="CVOET48/responseTime"){
				this.Cvoet48_responsetime=value;
			} else 
			if(xmlPath=="CVOET49/accuracy"){
				this.Cvoet49_accuracy=value;
			} else 
			if(xmlPath=="CVOET49/responseTime"){
				this.Cvoet49_responsetime=value;
			} else 
			if(xmlPath=="CVOET50/accuracy"){
				this.Cvoet50_accuracy=value;
			} else 
			if(xmlPath=="CVOET50/responseTime"){
				this.Cvoet50_responsetime=value;
			} else 
			if(xmlPath=="CVOET51/accuracy"){
				this.Cvoet51_accuracy=value;
			} else 
			if(xmlPath=="CVOET51/responseTime"){
				this.Cvoet51_responsetime=value;
			} else 
			if(xmlPath=="CVOET52/accuracy"){
				this.Cvoet52_accuracy=value;
			} else 
			if(xmlPath=="CVOET52/responseTime"){
				this.Cvoet52_responsetime=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="CVT1/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT1/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT2/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT2/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT3/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT3/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT4/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT4/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT5/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT5/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT6/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT6/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT7/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT7/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT8/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT8/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT9/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT9/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT10/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT10/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT11/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT11/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT12/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT12/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT13/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT13/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT14/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT14/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT15/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT15/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT16/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT16/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT17/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT17/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT18/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT18/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT19/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT19/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT20/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT20/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT21/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT21/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT22/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT22/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT23/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT23/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT24/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT24/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT25/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT25/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT26/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT26/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT27/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT27/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT28/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT28/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT29/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT29/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT30/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT30/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT31/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT31/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT32/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT32/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT33/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT33/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT34/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT34/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT35/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT35/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT36/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT36/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT37/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT37/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT38/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT38/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT39/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT39/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVT40/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVT40/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET1/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET1/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET2/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET2/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET3/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET3/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET4/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET4/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET5/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET5/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET6/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET6/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET7/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET7/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET8/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET8/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET9/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET9/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET10/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET10/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET11/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET11/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET12/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET12/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET13/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET13/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET14/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET14/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET15/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET15/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET16/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET16/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET17/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET17/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET18/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET18/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET19/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET19/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET20/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET20/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET21/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET21/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET22/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET22/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET23/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET23/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET24/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET24/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET25/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET25/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET26/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET26/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET27/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET27/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET28/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET28/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET29/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET29/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET30/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET30/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET31/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET31/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET32/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET32/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET33/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET33/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET34/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET34/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET35/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET35/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET36/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET36/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET37/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET37/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET38/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET38/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET39/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET39/responseTime"){
			return "field_data";
		}else if (xmlPath=="OET40/accuracy"){
			return "field_data";
		}else if (xmlPath=="OET40/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET1/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET1/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET2/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET2/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET3/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET3/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET4/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET4/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET5/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET5/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET6/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET6/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET7/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET7/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET8/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET8/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET9/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET9/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET10/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET10/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET11/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET11/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET12/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET12/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET13/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET13/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET14/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET14/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET15/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET15/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET16/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET16/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET17/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET17/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET18/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET18/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET19/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET19/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET20/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET20/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET21/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET21/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET22/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET22/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET23/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET23/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET24/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET24/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET25/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET25/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET26/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET26/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET27/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET27/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET28/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET28/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET29/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET29/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET30/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET30/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET31/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET31/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET32/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET32/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET33/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET33/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET34/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET34/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET35/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET35/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET36/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET36/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET37/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET37/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET38/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET38/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET39/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET39/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET40/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET40/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET41/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET41/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET42/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET42/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET43/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET43/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET44/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET44/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET45/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET45/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET46/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET46/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET47/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET47/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET48/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET48/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET49/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET49/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET50/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET50/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET51/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET51/responseTime"){
			return "field_data";
		}else if (xmlPath=="CVOET52/accuracy"){
			return "field_data";
		}else if (xmlPath=="CVOET52/responseTime"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cbat:CVOE";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cbat:CVOE>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
			var child0=0;
			var att0=0;
			if(this.Cvt1_responsetime!=null)
			child0++;
			if(this.Cvt1_accuracy!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<cbat:CVT1";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt1_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt1_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt1_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt1_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT1>";
			}
			}

			var child1=0;
			var att1=0;
			if(this.Cvt2_responsetime!=null)
			child1++;
			if(this.Cvt2_accuracy!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<cbat:CVT2";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt2_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt2_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt2_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt2_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT2>";
			}
			}

			var child2=0;
			var att2=0;
			if(this.Cvt3_responsetime!=null)
			child2++;
			if(this.Cvt3_accuracy!=null)
			child2++;
			if(child2>0 || att2>0){
				xmlTxt+="\n<cbat:CVT3";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt3_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt3_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt3_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt3_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT3>";
			}
			}

			var child3=0;
			var att3=0;
			if(this.Cvt4_responsetime!=null)
			child3++;
			if(this.Cvt4_accuracy!=null)
			child3++;
			if(child3>0 || att3>0){
				xmlTxt+="\n<cbat:CVT4";
			if(child3==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt4_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt4_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt4_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt4_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT4>";
			}
			}

			var child4=0;
			var att4=0;
			if(this.Cvt5_accuracy!=null)
			child4++;
			if(this.Cvt5_responsetime!=null)
			child4++;
			if(child4>0 || att4>0){
				xmlTxt+="\n<cbat:CVT5";
			if(child4==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt5_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt5_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt5_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt5_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT5>";
			}
			}

			var child5=0;
			var att5=0;
			if(this.Cvt6_responsetime!=null)
			child5++;
			if(this.Cvt6_accuracy!=null)
			child5++;
			if(child5>0 || att5>0){
				xmlTxt+="\n<cbat:CVT6";
			if(child5==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt6_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt6_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt6_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt6_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT6>";
			}
			}

			var child6=0;
			var att6=0;
			if(this.Cvt7_accuracy!=null)
			child6++;
			if(this.Cvt7_responsetime!=null)
			child6++;
			if(child6>0 || att6>0){
				xmlTxt+="\n<cbat:CVT7";
			if(child6==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt7_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt7_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt7_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt7_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT7>";
			}
			}

			var child7=0;
			var att7=0;
			if(this.Cvt8_accuracy!=null)
			child7++;
			if(this.Cvt8_responsetime!=null)
			child7++;
			if(child7>0 || att7>0){
				xmlTxt+="\n<cbat:CVT8";
			if(child7==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt8_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt8_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt8_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt8_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT8>";
			}
			}

			var child8=0;
			var att8=0;
			if(this.Cvt9_responsetime!=null)
			child8++;
			if(this.Cvt9_accuracy!=null)
			child8++;
			if(child8>0 || att8>0){
				xmlTxt+="\n<cbat:CVT9";
			if(child8==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt9_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt9_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt9_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt9_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT9>";
			}
			}

			var child9=0;
			var att9=0;
			if(this.Cvt10_responsetime!=null)
			child9++;
			if(this.Cvt10_accuracy!=null)
			child9++;
			if(child9>0 || att9>0){
				xmlTxt+="\n<cbat:CVT10";
			if(child9==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt10_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt10_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt10_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt10_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT10>";
			}
			}

			var child10=0;
			var att10=0;
			if(this.Cvt11_responsetime!=null)
			child10++;
			if(this.Cvt11_accuracy!=null)
			child10++;
			if(child10>0 || att10>0){
				xmlTxt+="\n<cbat:CVT11";
			if(child10==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt11_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt11_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt11_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt11_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT11>";
			}
			}

			var child11=0;
			var att11=0;
			if(this.Cvt12_responsetime!=null)
			child11++;
			if(this.Cvt12_accuracy!=null)
			child11++;
			if(child11>0 || att11>0){
				xmlTxt+="\n<cbat:CVT12";
			if(child11==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt12_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt12_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt12_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt12_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT12>";
			}
			}

			var child12=0;
			var att12=0;
			if(this.Cvt13_responsetime!=null)
			child12++;
			if(this.Cvt13_accuracy!=null)
			child12++;
			if(child12>0 || att12>0){
				xmlTxt+="\n<cbat:CVT13";
			if(child12==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt13_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt13_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt13_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt13_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT13>";
			}
			}

			var child13=0;
			var att13=0;
			if(this.Cvt14_responsetime!=null)
			child13++;
			if(this.Cvt14_accuracy!=null)
			child13++;
			if(child13>0 || att13>0){
				xmlTxt+="\n<cbat:CVT14";
			if(child13==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt14_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt14_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt14_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt14_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT14>";
			}
			}

			var child14=0;
			var att14=0;
			if(this.Cvt15_responsetime!=null)
			child14++;
			if(this.Cvt15_accuracy!=null)
			child14++;
			if(child14>0 || att14>0){
				xmlTxt+="\n<cbat:CVT15";
			if(child14==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt15_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt15_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt15_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt15_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT15>";
			}
			}

			var child15=0;
			var att15=0;
			if(this.Cvt16_responsetime!=null)
			child15++;
			if(this.Cvt16_accuracy!=null)
			child15++;
			if(child15>0 || att15>0){
				xmlTxt+="\n<cbat:CVT16";
			if(child15==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt16_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt16_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt16_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt16_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT16>";
			}
			}

			var child16=0;
			var att16=0;
			if(this.Cvt17_accuracy!=null)
			child16++;
			if(this.Cvt17_responsetime!=null)
			child16++;
			if(child16>0 || att16>0){
				xmlTxt+="\n<cbat:CVT17";
			if(child16==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt17_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt17_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt17_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt17_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT17>";
			}
			}

			var child17=0;
			var att17=0;
			if(this.Cvt18_accuracy!=null)
			child17++;
			if(this.Cvt18_responsetime!=null)
			child17++;
			if(child17>0 || att17>0){
				xmlTxt+="\n<cbat:CVT18";
			if(child17==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt18_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt18_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt18_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt18_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT18>";
			}
			}

			var child18=0;
			var att18=0;
			if(this.Cvt19_accuracy!=null)
			child18++;
			if(this.Cvt19_responsetime!=null)
			child18++;
			if(child18>0 || att18>0){
				xmlTxt+="\n<cbat:CVT19";
			if(child18==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt19_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt19_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt19_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt19_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT19>";
			}
			}

			var child19=0;
			var att19=0;
			if(this.Cvt20_accuracy!=null)
			child19++;
			if(this.Cvt20_responsetime!=null)
			child19++;
			if(child19>0 || att19>0){
				xmlTxt+="\n<cbat:CVT20";
			if(child19==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt20_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt20_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt20_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt20_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT20>";
			}
			}

			var child20=0;
			var att20=0;
			if(this.Cvt21_accuracy!=null)
			child20++;
			if(this.Cvt21_responsetime!=null)
			child20++;
			if(child20>0 || att20>0){
				xmlTxt+="\n<cbat:CVT21";
			if(child20==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt21_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt21_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt21_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt21_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT21>";
			}
			}

			var child21=0;
			var att21=0;
			if(this.Cvt22_accuracy!=null)
			child21++;
			if(this.Cvt22_responsetime!=null)
			child21++;
			if(child21>0 || att21>0){
				xmlTxt+="\n<cbat:CVT22";
			if(child21==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt22_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt22_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt22_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt22_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT22>";
			}
			}

			var child22=0;
			var att22=0;
			if(this.Cvt23_accuracy!=null)
			child22++;
			if(this.Cvt23_responsetime!=null)
			child22++;
			if(child22>0 || att22>0){
				xmlTxt+="\n<cbat:CVT23";
			if(child22==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt23_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt23_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt23_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt23_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT23>";
			}
			}

			var child23=0;
			var att23=0;
			if(this.Cvt24_accuracy!=null)
			child23++;
			if(this.Cvt24_responsetime!=null)
			child23++;
			if(child23>0 || att23>0){
				xmlTxt+="\n<cbat:CVT24";
			if(child23==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt24_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt24_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt24_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt24_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT24>";
			}
			}

			var child24=0;
			var att24=0;
			if(this.Cvt25_responsetime!=null)
			child24++;
			if(this.Cvt25_accuracy!=null)
			child24++;
			if(child24>0 || att24>0){
				xmlTxt+="\n<cbat:CVT25";
			if(child24==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt25_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt25_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt25_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt25_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT25>";
			}
			}

			var child25=0;
			var att25=0;
			if(this.Cvt26_responsetime!=null)
			child25++;
			if(this.Cvt26_accuracy!=null)
			child25++;
			if(child25>0 || att25>0){
				xmlTxt+="\n<cbat:CVT26";
			if(child25==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt26_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt26_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt26_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt26_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT26>";
			}
			}

			var child26=0;
			var att26=0;
			if(this.Cvt27_responsetime!=null)
			child26++;
			if(this.Cvt27_accuracy!=null)
			child26++;
			if(child26>0 || att26>0){
				xmlTxt+="\n<cbat:CVT27";
			if(child26==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt27_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt27_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt27_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt27_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT27>";
			}
			}

			var child27=0;
			var att27=0;
			if(this.Cvt28_responsetime!=null)
			child27++;
			if(this.Cvt28_accuracy!=null)
			child27++;
			if(child27>0 || att27>0){
				xmlTxt+="\n<cbat:CVT28";
			if(child27==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt28_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt28_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt28_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt28_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT28>";
			}
			}

			var child28=0;
			var att28=0;
			if(this.Cvt29_responsetime!=null)
			child28++;
			if(this.Cvt29_accuracy!=null)
			child28++;
			if(child28>0 || att28>0){
				xmlTxt+="\n<cbat:CVT29";
			if(child28==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt29_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt29_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt29_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt29_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT29>";
			}
			}

			var child29=0;
			var att29=0;
			if(this.Cvt30_responsetime!=null)
			child29++;
			if(this.Cvt30_accuracy!=null)
			child29++;
			if(child29>0 || att29>0){
				xmlTxt+="\n<cbat:CVT30";
			if(child29==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt30_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt30_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt30_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt30_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT30>";
			}
			}

			var child30=0;
			var att30=0;
			if(this.Cvt31_responsetime!=null)
			child30++;
			if(this.Cvt31_accuracy!=null)
			child30++;
			if(child30>0 || att30>0){
				xmlTxt+="\n<cbat:CVT31";
			if(child30==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt31_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt31_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt31_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt31_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT31>";
			}
			}

			var child31=0;
			var att31=0;
			if(this.Cvt32_responsetime!=null)
			child31++;
			if(this.Cvt32_accuracy!=null)
			child31++;
			if(child31>0 || att31>0){
				xmlTxt+="\n<cbat:CVT32";
			if(child31==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt32_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt32_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt32_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt32_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT32>";
			}
			}

			var child32=0;
			var att32=0;
			if(this.Cvt33_responsetime!=null)
			child32++;
			if(this.Cvt33_accuracy!=null)
			child32++;
			if(child32>0 || att32>0){
				xmlTxt+="\n<cbat:CVT33";
			if(child32==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt33_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt33_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt33_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt33_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT33>";
			}
			}

			var child33=0;
			var att33=0;
			if(this.Cvt34_responsetime!=null)
			child33++;
			if(this.Cvt34_accuracy!=null)
			child33++;
			if(child33>0 || att33>0){
				xmlTxt+="\n<cbat:CVT34";
			if(child33==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt34_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt34_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt34_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt34_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT34>";
			}
			}

			var child34=0;
			var att34=0;
			if(this.Cvt35_responsetime!=null)
			child34++;
			if(this.Cvt35_accuracy!=null)
			child34++;
			if(child34>0 || att34>0){
				xmlTxt+="\n<cbat:CVT35";
			if(child34==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt35_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt35_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt35_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt35_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT35>";
			}
			}

			var child35=0;
			var att35=0;
			if(this.Cvt36_responsetime!=null)
			child35++;
			if(this.Cvt36_accuracy!=null)
			child35++;
			if(child35>0 || att35>0){
				xmlTxt+="\n<cbat:CVT36";
			if(child35==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt36_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt36_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt36_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt36_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT36>";
			}
			}

			var child36=0;
			var att36=0;
			if(this.Cvt37_accuracy!=null)
			child36++;
			if(this.Cvt37_responsetime!=null)
			child36++;
			if(child36>0 || att36>0){
				xmlTxt+="\n<cbat:CVT37";
			if(child36==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt37_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt37_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt37_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt37_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT37>";
			}
			}

			var child37=0;
			var att37=0;
			if(this.Cvt38_responsetime!=null)
			child37++;
			if(this.Cvt38_accuracy!=null)
			child37++;
			if(child37>0 || att37>0){
				xmlTxt+="\n<cbat:CVT38";
			if(child37==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt38_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt38_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt38_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt38_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT38>";
			}
			}

			var child38=0;
			var att38=0;
			if(this.Cvt39_responsetime!=null)
			child38++;
			if(this.Cvt39_accuracy!=null)
			child38++;
			if(child38>0 || att38>0){
				xmlTxt+="\n<cbat:CVT39";
			if(child38==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt39_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt39_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt39_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt39_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT39>";
			}
			}

			var child39=0;
			var att39=0;
			if(this.Cvt40_responsetime!=null)
			child39++;
			if(this.Cvt40_accuracy!=null)
			child39++;
			if(child39>0 || att39>0){
				xmlTxt+="\n<cbat:CVT40";
			if(child39==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvt40_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvt40_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvt40_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvt40_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVT40>";
			}
			}

			var child40=0;
			var att40=0;
			if(this.Oet1_accuracy!=null)
			child40++;
			if(this.Oet1_responsetime!=null)
			child40++;
			if(child40>0 || att40>0){
				xmlTxt+="\n<cbat:OET1";
			if(child40==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet1_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet1_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet1_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet1_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET1>";
			}
			}

			var child41=0;
			var att41=0;
			if(this.Oet2_accuracy!=null)
			child41++;
			if(this.Oet2_responsetime!=null)
			child41++;
			if(child41>0 || att41>0){
				xmlTxt+="\n<cbat:OET2";
			if(child41==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet2_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet2_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet2_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet2_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET2>";
			}
			}

			var child42=0;
			var att42=0;
			if(this.Oet3_responsetime!=null)
			child42++;
			if(this.Oet3_accuracy!=null)
			child42++;
			if(child42>0 || att42>0){
				xmlTxt+="\n<cbat:OET3";
			if(child42==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet3_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet3_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet3_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet3_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET3>";
			}
			}

			var child43=0;
			var att43=0;
			if(this.Oet4_responsetime!=null)
			child43++;
			if(this.Oet4_accuracy!=null)
			child43++;
			if(child43>0 || att43>0){
				xmlTxt+="\n<cbat:OET4";
			if(child43==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet4_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet4_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet4_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet4_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET4>";
			}
			}

			var child44=0;
			var att44=0;
			if(this.Oet5_responsetime!=null)
			child44++;
			if(this.Oet5_accuracy!=null)
			child44++;
			if(child44>0 || att44>0){
				xmlTxt+="\n<cbat:OET5";
			if(child44==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet5_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet5_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet5_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet5_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET5>";
			}
			}

			var child45=0;
			var att45=0;
			if(this.Oet6_accuracy!=null)
			child45++;
			if(this.Oet6_responsetime!=null)
			child45++;
			if(child45>0 || att45>0){
				xmlTxt+="\n<cbat:OET6";
			if(child45==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet6_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet6_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet6_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet6_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET6>";
			}
			}

			var child46=0;
			var att46=0;
			if(this.Oet7_accuracy!=null)
			child46++;
			if(this.Oet7_responsetime!=null)
			child46++;
			if(child46>0 || att46>0){
				xmlTxt+="\n<cbat:OET7";
			if(child46==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet7_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet7_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet7_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet7_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET7>";
			}
			}

			var child47=0;
			var att47=0;
			if(this.Oet8_accuracy!=null)
			child47++;
			if(this.Oet8_responsetime!=null)
			child47++;
			if(child47>0 || att47>0){
				xmlTxt+="\n<cbat:OET8";
			if(child47==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet8_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet8_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet8_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet8_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET8>";
			}
			}

			var child48=0;
			var att48=0;
			if(this.Oet9_accuracy!=null)
			child48++;
			if(this.Oet9_responsetime!=null)
			child48++;
			if(child48>0 || att48>0){
				xmlTxt+="\n<cbat:OET9";
			if(child48==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet9_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet9_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet9_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet9_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET9>";
			}
			}

			var child49=0;
			var att49=0;
			if(this.Oet10_accuracy!=null)
			child49++;
			if(this.Oet10_responsetime!=null)
			child49++;
			if(child49>0 || att49>0){
				xmlTxt+="\n<cbat:OET10";
			if(child49==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet10_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet10_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet10_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet10_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET10>";
			}
			}

			var child50=0;
			var att50=0;
			if(this.Oet11_accuracy!=null)
			child50++;
			if(this.Oet11_responsetime!=null)
			child50++;
			if(child50>0 || att50>0){
				xmlTxt+="\n<cbat:OET11";
			if(child50==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet11_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet11_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet11_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet11_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET11>";
			}
			}

			var child51=0;
			var att51=0;
			if(this.Oet12_accuracy!=null)
			child51++;
			if(this.Oet12_responsetime!=null)
			child51++;
			if(child51>0 || att51>0){
				xmlTxt+="\n<cbat:OET12";
			if(child51==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet12_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet12_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet12_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet12_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET12>";
			}
			}

			var child52=0;
			var att52=0;
			if(this.Oet13_accuracy!=null)
			child52++;
			if(this.Oet13_responsetime!=null)
			child52++;
			if(child52>0 || att52>0){
				xmlTxt+="\n<cbat:OET13";
			if(child52==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet13_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet13_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet13_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet13_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET13>";
			}
			}

			var child53=0;
			var att53=0;
			if(this.Oet14_accuracy!=null)
			child53++;
			if(this.Oet14_responsetime!=null)
			child53++;
			if(child53>0 || att53>0){
				xmlTxt+="\n<cbat:OET14";
			if(child53==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet14_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet14_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet14_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet14_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET14>";
			}
			}

			var child54=0;
			var att54=0;
			if(this.Oet15_accuracy!=null)
			child54++;
			if(this.Oet15_responsetime!=null)
			child54++;
			if(child54>0 || att54>0){
				xmlTxt+="\n<cbat:OET15";
			if(child54==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet15_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet15_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet15_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet15_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET15>";
			}
			}

			var child55=0;
			var att55=0;
			if(this.Oet16_accuracy!=null)
			child55++;
			if(this.Oet16_responsetime!=null)
			child55++;
			if(child55>0 || att55>0){
				xmlTxt+="\n<cbat:OET16";
			if(child55==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet16_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet16_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet16_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet16_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET16>";
			}
			}

			var child56=0;
			var att56=0;
			if(this.Oet17_accuracy!=null)
			child56++;
			if(this.Oet17_responsetime!=null)
			child56++;
			if(child56>0 || att56>0){
				xmlTxt+="\n<cbat:OET17";
			if(child56==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet17_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet17_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet17_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet17_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET17>";
			}
			}

			var child57=0;
			var att57=0;
			if(this.Oet18_accuracy!=null)
			child57++;
			if(this.Oet18_responsetime!=null)
			child57++;
			if(child57>0 || att57>0){
				xmlTxt+="\n<cbat:OET18";
			if(child57==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet18_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet18_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet18_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet18_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET18>";
			}
			}

			var child58=0;
			var att58=0;
			if(this.Oet19_accuracy!=null)
			child58++;
			if(this.Oet19_responsetime!=null)
			child58++;
			if(child58>0 || att58>0){
				xmlTxt+="\n<cbat:OET19";
			if(child58==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet19_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet19_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet19_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet19_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET19>";
			}
			}

			var child59=0;
			var att59=0;
			if(this.Oet20_accuracy!=null)
			child59++;
			if(this.Oet20_responsetime!=null)
			child59++;
			if(child59>0 || att59>0){
				xmlTxt+="\n<cbat:OET20";
			if(child59==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet20_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet20_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet20_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet20_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET20>";
			}
			}

			var child60=0;
			var att60=0;
			if(this.Oet21_accuracy!=null)
			child60++;
			if(this.Oet21_responsetime!=null)
			child60++;
			if(child60>0 || att60>0){
				xmlTxt+="\n<cbat:OET21";
			if(child60==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet21_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet21_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet21_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet21_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET21>";
			}
			}

			var child61=0;
			var att61=0;
			if(this.Oet22_accuracy!=null)
			child61++;
			if(this.Oet22_responsetime!=null)
			child61++;
			if(child61>0 || att61>0){
				xmlTxt+="\n<cbat:OET22";
			if(child61==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet22_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet22_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet22_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet22_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET22>";
			}
			}

			var child62=0;
			var att62=0;
			if(this.Oet23_accuracy!=null)
			child62++;
			if(this.Oet23_responsetime!=null)
			child62++;
			if(child62>0 || att62>0){
				xmlTxt+="\n<cbat:OET23";
			if(child62==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet23_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet23_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet23_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet23_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET23>";
			}
			}

			var child63=0;
			var att63=0;
			if(this.Oet24_responsetime!=null)
			child63++;
			if(this.Oet24_accuracy!=null)
			child63++;
			if(child63>0 || att63>0){
				xmlTxt+="\n<cbat:OET24";
			if(child63==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet24_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet24_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet24_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet24_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET24>";
			}
			}

			var child64=0;
			var att64=0;
			if(this.Oet25_responsetime!=null)
			child64++;
			if(this.Oet25_accuracy!=null)
			child64++;
			if(child64>0 || att64>0){
				xmlTxt+="\n<cbat:OET25";
			if(child64==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet25_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet25_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet25_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet25_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET25>";
			}
			}

			var child65=0;
			var att65=0;
			if(this.Oet26_responsetime!=null)
			child65++;
			if(this.Oet26_accuracy!=null)
			child65++;
			if(child65>0 || att65>0){
				xmlTxt+="\n<cbat:OET26";
			if(child65==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet26_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet26_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet26_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet26_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET26>";
			}
			}

			var child66=0;
			var att66=0;
			if(this.Oet27_responsetime!=null)
			child66++;
			if(this.Oet27_accuracy!=null)
			child66++;
			if(child66>0 || att66>0){
				xmlTxt+="\n<cbat:OET27";
			if(child66==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet27_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet27_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet27_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet27_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET27>";
			}
			}

			var child67=0;
			var att67=0;
			if(this.Oet28_responsetime!=null)
			child67++;
			if(this.Oet28_accuracy!=null)
			child67++;
			if(child67>0 || att67>0){
				xmlTxt+="\n<cbat:OET28";
			if(child67==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet28_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet28_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet28_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet28_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET28>";
			}
			}

			var child68=0;
			var att68=0;
			if(this.Oet29_responsetime!=null)
			child68++;
			if(this.Oet29_accuracy!=null)
			child68++;
			if(child68>0 || att68>0){
				xmlTxt+="\n<cbat:OET29";
			if(child68==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet29_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet29_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet29_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet29_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET29>";
			}
			}

			var child69=0;
			var att69=0;
			if(this.Oet30_responsetime!=null)
			child69++;
			if(this.Oet30_accuracy!=null)
			child69++;
			if(child69>0 || att69>0){
				xmlTxt+="\n<cbat:OET30";
			if(child69==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet30_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet30_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet30_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet30_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET30>";
			}
			}

			var child70=0;
			var att70=0;
			if(this.Oet31_responsetime!=null)
			child70++;
			if(this.Oet31_accuracy!=null)
			child70++;
			if(child70>0 || att70>0){
				xmlTxt+="\n<cbat:OET31";
			if(child70==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet31_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet31_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet31_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet31_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET31>";
			}
			}

			var child71=0;
			var att71=0;
			if(this.Oet32_responsetime!=null)
			child71++;
			if(this.Oet32_accuracy!=null)
			child71++;
			if(child71>0 || att71>0){
				xmlTxt+="\n<cbat:OET32";
			if(child71==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet32_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet32_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet32_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet32_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET32>";
			}
			}

			var child72=0;
			var att72=0;
			if(this.Oet33_responsetime!=null)
			child72++;
			if(this.Oet33_accuracy!=null)
			child72++;
			if(child72>0 || att72>0){
				xmlTxt+="\n<cbat:OET33";
			if(child72==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet33_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet33_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet33_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet33_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET33>";
			}
			}

			var child73=0;
			var att73=0;
			if(this.Oet34_responsetime!=null)
			child73++;
			if(this.Oet34_accuracy!=null)
			child73++;
			if(child73>0 || att73>0){
				xmlTxt+="\n<cbat:OET34";
			if(child73==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet34_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet34_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet34_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet34_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET34>";
			}
			}

			var child74=0;
			var att74=0;
			if(this.Oet35_accuracy!=null)
			child74++;
			if(this.Oet35_responsetime!=null)
			child74++;
			if(child74>0 || att74>0){
				xmlTxt+="\n<cbat:OET35";
			if(child74==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet35_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet35_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet35_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet35_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET35>";
			}
			}

			var child75=0;
			var att75=0;
			if(this.Oet36_accuracy!=null)
			child75++;
			if(this.Oet36_responsetime!=null)
			child75++;
			if(child75>0 || att75>0){
				xmlTxt+="\n<cbat:OET36";
			if(child75==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet36_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet36_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet36_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet36_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET36>";
			}
			}

			var child76=0;
			var att76=0;
			if(this.Oet37_responsetime!=null)
			child76++;
			if(this.Oet37_accuracy!=null)
			child76++;
			if(child76>0 || att76>0){
				xmlTxt+="\n<cbat:OET37";
			if(child76==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet37_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet37_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet37_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet37_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET37>";
			}
			}

			var child77=0;
			var att77=0;
			if(this.Oet38_responsetime!=null)
			child77++;
			if(this.Oet38_accuracy!=null)
			child77++;
			if(child77>0 || att77>0){
				xmlTxt+="\n<cbat:OET38";
			if(child77==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet38_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet38_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet38_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet38_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET38>";
			}
			}

			var child78=0;
			var att78=0;
			if(this.Oet39_responsetime!=null)
			child78++;
			if(this.Oet39_accuracy!=null)
			child78++;
			if(child78>0 || att78>0){
				xmlTxt+="\n<cbat:OET39";
			if(child78==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet39_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet39_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet39_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet39_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET39>";
			}
			}

			var child79=0;
			var att79=0;
			if(this.Oet40_responsetime!=null)
			child79++;
			if(this.Oet40_accuracy!=null)
			child79++;
			if(child79>0 || att79>0){
				xmlTxt+="\n<cbat:OET40";
			if(child79==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Oet40_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Oet40_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Oet40_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Oet40_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:OET40>";
			}
			}

			var child80=0;
			var att80=0;
			if(this.Cvoet1_accuracy!=null)
			child80++;
			if(this.Cvoet1_responsetime!=null)
			child80++;
			if(child80>0 || att80>0){
				xmlTxt+="\n<cbat:CVOET1";
			if(child80==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet1_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet1_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet1_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet1_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET1>";
			}
			}

			var child81=0;
			var att81=0;
			if(this.Cvoet2_accuracy!=null)
			child81++;
			if(this.Cvoet2_responsetime!=null)
			child81++;
			if(child81>0 || att81>0){
				xmlTxt+="\n<cbat:CVOET2";
			if(child81==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet2_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet2_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet2_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet2_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET2>";
			}
			}

			var child82=0;
			var att82=0;
			if(this.Cvoet3_accuracy!=null)
			child82++;
			if(this.Cvoet3_responsetime!=null)
			child82++;
			if(child82>0 || att82>0){
				xmlTxt+="\n<cbat:CVOET3";
			if(child82==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet3_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet3_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet3_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet3_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET3>";
			}
			}

			var child83=0;
			var att83=0;
			if(this.Cvoet4_responsetime!=null)
			child83++;
			if(this.Cvoet4_accuracy!=null)
			child83++;
			if(child83>0 || att83>0){
				xmlTxt+="\n<cbat:CVOET4";
			if(child83==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet4_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet4_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet4_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet4_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET4>";
			}
			}

			var child84=0;
			var att84=0;
			if(this.Cvoet5_responsetime!=null)
			child84++;
			if(this.Cvoet5_accuracy!=null)
			child84++;
			if(child84>0 || att84>0){
				xmlTxt+="\n<cbat:CVOET5";
			if(child84==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet5_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet5_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet5_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet5_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET5>";
			}
			}

			var child85=0;
			var att85=0;
			if(this.Cvoet6_responsetime!=null)
			child85++;
			if(this.Cvoet6_accuracy!=null)
			child85++;
			if(child85>0 || att85>0){
				xmlTxt+="\n<cbat:CVOET6";
			if(child85==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet6_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet6_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet6_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet6_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET6>";
			}
			}

			var child86=0;
			var att86=0;
			if(this.Cvoet7_responsetime!=null)
			child86++;
			if(this.Cvoet7_accuracy!=null)
			child86++;
			if(child86>0 || att86>0){
				xmlTxt+="\n<cbat:CVOET7";
			if(child86==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet7_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet7_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet7_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet7_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET7>";
			}
			}

			var child87=0;
			var att87=0;
			if(this.Cvoet8_responsetime!=null)
			child87++;
			if(this.Cvoet8_accuracy!=null)
			child87++;
			if(child87>0 || att87>0){
				xmlTxt+="\n<cbat:CVOET8";
			if(child87==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet8_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet8_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet8_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet8_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET8>";
			}
			}

			var child88=0;
			var att88=0;
			if(this.Cvoet9_responsetime!=null)
			child88++;
			if(this.Cvoet9_accuracy!=null)
			child88++;
			if(child88>0 || att88>0){
				xmlTxt+="\n<cbat:CVOET9";
			if(child88==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet9_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet9_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet9_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet9_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET9>";
			}
			}

			var child89=0;
			var att89=0;
			if(this.Cvoet10_accuracy!=null)
			child89++;
			if(this.Cvoet10_responsetime!=null)
			child89++;
			if(child89>0 || att89>0){
				xmlTxt+="\n<cbat:CVOET10";
			if(child89==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet10_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet10_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet10_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet10_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET10>";
			}
			}

			var child90=0;
			var att90=0;
			if(this.Cvoet11_accuracy!=null)
			child90++;
			if(this.Cvoet11_responsetime!=null)
			child90++;
			if(child90>0 || att90>0){
				xmlTxt+="\n<cbat:CVOET11";
			if(child90==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet11_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet11_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet11_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet11_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET11>";
			}
			}

			var child91=0;
			var att91=0;
			if(this.Cvoet12_accuracy!=null)
			child91++;
			if(this.Cvoet12_responsetime!=null)
			child91++;
			if(child91>0 || att91>0){
				xmlTxt+="\n<cbat:CVOET12";
			if(child91==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet12_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet12_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet12_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet12_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET12>";
			}
			}

			var child92=0;
			var att92=0;
			if(this.Cvoet13_accuracy!=null)
			child92++;
			if(this.Cvoet13_responsetime!=null)
			child92++;
			if(child92>0 || att92>0){
				xmlTxt+="\n<cbat:CVOET13";
			if(child92==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet13_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet13_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet13_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet13_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET13>";
			}
			}

			var child93=0;
			var att93=0;
			if(this.Cvoet14_accuracy!=null)
			child93++;
			if(this.Cvoet14_responsetime!=null)
			child93++;
			if(child93>0 || att93>0){
				xmlTxt+="\n<cbat:CVOET14";
			if(child93==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet14_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet14_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet14_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet14_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET14>";
			}
			}

			var child94=0;
			var att94=0;
			if(this.Cvoet15_responsetime!=null)
			child94++;
			if(this.Cvoet15_accuracy!=null)
			child94++;
			if(child94>0 || att94>0){
				xmlTxt+="\n<cbat:CVOET15";
			if(child94==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet15_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet15_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet15_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet15_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET15>";
			}
			}

			var child95=0;
			var att95=0;
			if(this.Cvoet16_responsetime!=null)
			child95++;
			if(this.Cvoet16_accuracy!=null)
			child95++;
			if(child95>0 || att95>0){
				xmlTxt+="\n<cbat:CVOET16";
			if(child95==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet16_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet16_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet16_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet16_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET16>";
			}
			}

			var child96=0;
			var att96=0;
			if(this.Cvoet17_responsetime!=null)
			child96++;
			if(this.Cvoet17_accuracy!=null)
			child96++;
			if(child96>0 || att96>0){
				xmlTxt+="\n<cbat:CVOET17";
			if(child96==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet17_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet17_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet17_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet17_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET17>";
			}
			}

			var child97=0;
			var att97=0;
			if(this.Cvoet18_responsetime!=null)
			child97++;
			if(this.Cvoet18_accuracy!=null)
			child97++;
			if(child97>0 || att97>0){
				xmlTxt+="\n<cbat:CVOET18";
			if(child97==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet18_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet18_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet18_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet18_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET18>";
			}
			}

			var child98=0;
			var att98=0;
			if(this.Cvoet19_responsetime!=null)
			child98++;
			if(this.Cvoet19_accuracy!=null)
			child98++;
			if(child98>0 || att98>0){
				xmlTxt+="\n<cbat:CVOET19";
			if(child98==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet19_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet19_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet19_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet19_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET19>";
			}
			}

			var child99=0;
			var att99=0;
			if(this.Cvoet20_accuracy!=null)
			child99++;
			if(this.Cvoet20_responsetime!=null)
			child99++;
			if(child99>0 || att99>0){
				xmlTxt+="\n<cbat:CVOET20";
			if(child99==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet20_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet20_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet20_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet20_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET20>";
			}
			}

			var child100=0;
			var att100=0;
			if(this.Cvoet21_responsetime!=null)
			child100++;
			if(this.Cvoet21_accuracy!=null)
			child100++;
			if(child100>0 || att100>0){
				xmlTxt+="\n<cbat:CVOET21";
			if(child100==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet21_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet21_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet21_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet21_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET21>";
			}
			}

			var child101=0;
			var att101=0;
			if(this.Cvoet22_responsetime!=null)
			child101++;
			if(this.Cvoet22_accuracy!=null)
			child101++;
			if(child101>0 || att101>0){
				xmlTxt+="\n<cbat:CVOET22";
			if(child101==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet22_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet22_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet22_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet22_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET22>";
			}
			}

			var child102=0;
			var att102=0;
			if(this.Cvoet23_responsetime!=null)
			child102++;
			if(this.Cvoet23_accuracy!=null)
			child102++;
			if(child102>0 || att102>0){
				xmlTxt+="\n<cbat:CVOET23";
			if(child102==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet23_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet23_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet23_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet23_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET23>";
			}
			}

			var child103=0;
			var att103=0;
			if(this.Cvoet24_responsetime!=null)
			child103++;
			if(this.Cvoet24_accuracy!=null)
			child103++;
			if(child103>0 || att103>0){
				xmlTxt+="\n<cbat:CVOET24";
			if(child103==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet24_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet24_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet24_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet24_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET24>";
			}
			}

			var child104=0;
			var att104=0;
			if(this.Cvoet25_responsetime!=null)
			child104++;
			if(this.Cvoet25_accuracy!=null)
			child104++;
			if(child104>0 || att104>0){
				xmlTxt+="\n<cbat:CVOET25";
			if(child104==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet25_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet25_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet25_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet25_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET25>";
			}
			}

			var child105=0;
			var att105=0;
			if(this.Cvoet26_responsetime!=null)
			child105++;
			if(this.Cvoet26_accuracy!=null)
			child105++;
			if(child105>0 || att105>0){
				xmlTxt+="\n<cbat:CVOET26";
			if(child105==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet26_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet26_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet26_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet26_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET26>";
			}
			}

			var child106=0;
			var att106=0;
			if(this.Cvoet27_responsetime!=null)
			child106++;
			if(this.Cvoet27_accuracy!=null)
			child106++;
			if(child106>0 || att106>0){
				xmlTxt+="\n<cbat:CVOET27";
			if(child106==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet27_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet27_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet27_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet27_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET27>";
			}
			}

			var child107=0;
			var att107=0;
			if(this.Cvoet28_responsetime!=null)
			child107++;
			if(this.Cvoet28_accuracy!=null)
			child107++;
			if(child107>0 || att107>0){
				xmlTxt+="\n<cbat:CVOET28";
			if(child107==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet28_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet28_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet28_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet28_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET28>";
			}
			}

			var child108=0;
			var att108=0;
			if(this.Cvoet29_responsetime!=null)
			child108++;
			if(this.Cvoet29_accuracy!=null)
			child108++;
			if(child108>0 || att108>0){
				xmlTxt+="\n<cbat:CVOET29";
			if(child108==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet29_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet29_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet29_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet29_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET29>";
			}
			}

			var child109=0;
			var att109=0;
			if(this.Cvoet30_responsetime!=null)
			child109++;
			if(this.Cvoet30_accuracy!=null)
			child109++;
			if(child109>0 || att109>0){
				xmlTxt+="\n<cbat:CVOET30";
			if(child109==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet30_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet30_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet30_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet30_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET30>";
			}
			}

			var child110=0;
			var att110=0;
			if(this.Cvoet31_responsetime!=null)
			child110++;
			if(this.Cvoet31_accuracy!=null)
			child110++;
			if(child110>0 || att110>0){
				xmlTxt+="\n<cbat:CVOET31";
			if(child110==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet31_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet31_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet31_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet31_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET31>";
			}
			}

			var child111=0;
			var att111=0;
			if(this.Cvoet32_responsetime!=null)
			child111++;
			if(this.Cvoet32_accuracy!=null)
			child111++;
			if(child111>0 || att111>0){
				xmlTxt+="\n<cbat:CVOET32";
			if(child111==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet32_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet32_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet32_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet32_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET32>";
			}
			}

			var child112=0;
			var att112=0;
			if(this.Cvoet33_accuracy!=null)
			child112++;
			if(this.Cvoet33_responsetime!=null)
			child112++;
			if(child112>0 || att112>0){
				xmlTxt+="\n<cbat:CVOET33";
			if(child112==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet33_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet33_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet33_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet33_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET33>";
			}
			}

			var child113=0;
			var att113=0;
			if(this.Cvoet34_responsetime!=null)
			child113++;
			if(this.Cvoet34_accuracy!=null)
			child113++;
			if(child113>0 || att113>0){
				xmlTxt+="\n<cbat:CVOET34";
			if(child113==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet34_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet34_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet34_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet34_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET34>";
			}
			}

			var child114=0;
			var att114=0;
			if(this.Cvoet35_accuracy!=null)
			child114++;
			if(this.Cvoet35_responsetime!=null)
			child114++;
			if(child114>0 || att114>0){
				xmlTxt+="\n<cbat:CVOET35";
			if(child114==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet35_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet35_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet35_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet35_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET35>";
			}
			}

			var child115=0;
			var att115=0;
			if(this.Cvoet36_accuracy!=null)
			child115++;
			if(this.Cvoet36_responsetime!=null)
			child115++;
			if(child115>0 || att115>0){
				xmlTxt+="\n<cbat:CVOET36";
			if(child115==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet36_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet36_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet36_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet36_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET36>";
			}
			}

			var child116=0;
			var att116=0;
			if(this.Cvoet37_accuracy!=null)
			child116++;
			if(this.Cvoet37_responsetime!=null)
			child116++;
			if(child116>0 || att116>0){
				xmlTxt+="\n<cbat:CVOET37";
			if(child116==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet37_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet37_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet37_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet37_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET37>";
			}
			}

			var child117=0;
			var att117=0;
			if(this.Cvoet38_accuracy!=null)
			child117++;
			if(this.Cvoet38_responsetime!=null)
			child117++;
			if(child117>0 || att117>0){
				xmlTxt+="\n<cbat:CVOET38";
			if(child117==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet38_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet38_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet38_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet38_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET38>";
			}
			}

			var child118=0;
			var att118=0;
			if(this.Cvoet39_accuracy!=null)
			child118++;
			if(this.Cvoet39_responsetime!=null)
			child118++;
			if(child118>0 || att118>0){
				xmlTxt+="\n<cbat:CVOET39";
			if(child118==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet39_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet39_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet39_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet39_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET39>";
			}
			}

			var child119=0;
			var att119=0;
			if(this.Cvoet40_accuracy!=null)
			child119++;
			if(this.Cvoet40_responsetime!=null)
			child119++;
			if(child119>0 || att119>0){
				xmlTxt+="\n<cbat:CVOET40";
			if(child119==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet40_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet40_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet40_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet40_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET40>";
			}
			}

			var child120=0;
			var att120=0;
			if(this.Cvoet41_accuracy!=null)
			child120++;
			if(this.Cvoet41_responsetime!=null)
			child120++;
			if(child120>0 || att120>0){
				xmlTxt+="\n<cbat:CVOET41";
			if(child120==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet41_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet41_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet41_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet41_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET41>";
			}
			}

			var child121=0;
			var att121=0;
			if(this.Cvoet42_accuracy!=null)
			child121++;
			if(this.Cvoet42_responsetime!=null)
			child121++;
			if(child121>0 || att121>0){
				xmlTxt+="\n<cbat:CVOET42";
			if(child121==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet42_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet42_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet42_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet42_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET42>";
			}
			}

			var child122=0;
			var att122=0;
			if(this.Cvoet43_accuracy!=null)
			child122++;
			if(this.Cvoet43_responsetime!=null)
			child122++;
			if(child122>0 || att122>0){
				xmlTxt+="\n<cbat:CVOET43";
			if(child122==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet43_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet43_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet43_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet43_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET43>";
			}
			}

			var child123=0;
			var att123=0;
			if(this.Cvoet44_accuracy!=null)
			child123++;
			if(this.Cvoet44_responsetime!=null)
			child123++;
			if(child123>0 || att123>0){
				xmlTxt+="\n<cbat:CVOET44";
			if(child123==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet44_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet44_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet44_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet44_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET44>";
			}
			}

			var child124=0;
			var att124=0;
			if(this.Cvoet45_accuracy!=null)
			child124++;
			if(this.Cvoet45_responsetime!=null)
			child124++;
			if(child124>0 || att124>0){
				xmlTxt+="\n<cbat:CVOET45";
			if(child124==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet45_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet45_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet45_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet45_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET45>";
			}
			}

			var child125=0;
			var att125=0;
			if(this.Cvoet46_accuracy!=null)
			child125++;
			if(this.Cvoet46_responsetime!=null)
			child125++;
			if(child125>0 || att125>0){
				xmlTxt+="\n<cbat:CVOET46";
			if(child125==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet46_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet46_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet46_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet46_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET46>";
			}
			}

			var child126=0;
			var att126=0;
			if(this.Cvoet47_responsetime!=null)
			child126++;
			if(this.Cvoet47_accuracy!=null)
			child126++;
			if(child126>0 || att126>0){
				xmlTxt+="\n<cbat:CVOET47";
			if(child126==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet47_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet47_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet47_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet47_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET47>";
			}
			}

			var child127=0;
			var att127=0;
			if(this.Cvoet48_responsetime!=null)
			child127++;
			if(this.Cvoet48_accuracy!=null)
			child127++;
			if(child127>0 || att127>0){
				xmlTxt+="\n<cbat:CVOET48";
			if(child127==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet48_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet48_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet48_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet48_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET48>";
			}
			}

			var child128=0;
			var att128=0;
			if(this.Cvoet49_responsetime!=null)
			child128++;
			if(this.Cvoet49_accuracy!=null)
			child128++;
			if(child128>0 || att128>0){
				xmlTxt+="\n<cbat:CVOET49";
			if(child128==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet49_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet49_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet49_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet49_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET49>";
			}
			}

			var child129=0;
			var att129=0;
			if(this.Cvoet50_accuracy!=null)
			child129++;
			if(this.Cvoet50_responsetime!=null)
			child129++;
			if(child129>0 || att129>0){
				xmlTxt+="\n<cbat:CVOET50";
			if(child129==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet50_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet50_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet50_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet50_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET50>";
			}
			}

			var child130=0;
			var att130=0;
			if(this.Cvoet51_responsetime!=null)
			child130++;
			if(this.Cvoet51_accuracy!=null)
			child130++;
			if(child130>0 || att130>0){
				xmlTxt+="\n<cbat:CVOET51";
			if(child130==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet51_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet51_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet51_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet51_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET51>";
			}
			}

			var child131=0;
			var att131=0;
			if(this.Cvoet52_responsetime!=null)
			child131++;
			if(this.Cvoet52_accuracy!=null)
			child131++;
			if(child131>0 || att131>0){
				xmlTxt+="\n<cbat:CVOET52";
			if(child131==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Cvoet52_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet52_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Cvoet52_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Cvoet52_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:CVOET52>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
			if(this.Cvt1_responsetime!=null) return true;
			if(this.Cvt1_accuracy!=null) return true;
			if(this.Cvt2_responsetime!=null) return true;
			if(this.Cvt2_accuracy!=null) return true;
			if(this.Cvt3_responsetime!=null) return true;
			if(this.Cvt3_accuracy!=null) return true;
			if(this.Cvt4_responsetime!=null) return true;
			if(this.Cvt4_accuracy!=null) return true;
			if(this.Cvt5_accuracy!=null) return true;
			if(this.Cvt5_responsetime!=null) return true;
			if(this.Cvt6_responsetime!=null) return true;
			if(this.Cvt6_accuracy!=null) return true;
			if(this.Cvt7_accuracy!=null) return true;
			if(this.Cvt7_responsetime!=null) return true;
			if(this.Cvt8_accuracy!=null) return true;
			if(this.Cvt8_responsetime!=null) return true;
			if(this.Cvt9_responsetime!=null) return true;
			if(this.Cvt9_accuracy!=null) return true;
			if(this.Cvt10_responsetime!=null) return true;
			if(this.Cvt10_accuracy!=null) return true;
			if(this.Cvt11_responsetime!=null) return true;
			if(this.Cvt11_accuracy!=null) return true;
			if(this.Cvt12_responsetime!=null) return true;
			if(this.Cvt12_accuracy!=null) return true;
			if(this.Cvt13_responsetime!=null) return true;
			if(this.Cvt13_accuracy!=null) return true;
			if(this.Cvt14_responsetime!=null) return true;
			if(this.Cvt14_accuracy!=null) return true;
			if(this.Cvt15_responsetime!=null) return true;
			if(this.Cvt15_accuracy!=null) return true;
			if(this.Cvt16_responsetime!=null) return true;
			if(this.Cvt16_accuracy!=null) return true;
			if(this.Cvt17_accuracy!=null) return true;
			if(this.Cvt17_responsetime!=null) return true;
			if(this.Cvt18_accuracy!=null) return true;
			if(this.Cvt18_responsetime!=null) return true;
			if(this.Cvt19_accuracy!=null) return true;
			if(this.Cvt19_responsetime!=null) return true;
			if(this.Cvt20_accuracy!=null) return true;
			if(this.Cvt20_responsetime!=null) return true;
			if(this.Cvt21_accuracy!=null) return true;
			if(this.Cvt21_responsetime!=null) return true;
			if(this.Cvt22_accuracy!=null) return true;
			if(this.Cvt22_responsetime!=null) return true;
			if(this.Cvt23_accuracy!=null) return true;
			if(this.Cvt23_responsetime!=null) return true;
			if(this.Cvt24_accuracy!=null) return true;
			if(this.Cvt24_responsetime!=null) return true;
			if(this.Cvt25_responsetime!=null) return true;
			if(this.Cvt25_accuracy!=null) return true;
			if(this.Cvt26_responsetime!=null) return true;
			if(this.Cvt26_accuracy!=null) return true;
			if(this.Cvt27_responsetime!=null) return true;
			if(this.Cvt27_accuracy!=null) return true;
			if(this.Cvt28_responsetime!=null) return true;
			if(this.Cvt28_accuracy!=null) return true;
			if(this.Cvt29_responsetime!=null) return true;
			if(this.Cvt29_accuracy!=null) return true;
			if(this.Cvt30_responsetime!=null) return true;
			if(this.Cvt30_accuracy!=null) return true;
			if(this.Cvt31_responsetime!=null) return true;
			if(this.Cvt31_accuracy!=null) return true;
			if(this.Cvt32_responsetime!=null) return true;
			if(this.Cvt32_accuracy!=null) return true;
			if(this.Cvt33_responsetime!=null) return true;
			if(this.Cvt33_accuracy!=null) return true;
			if(this.Cvt34_responsetime!=null) return true;
			if(this.Cvt34_accuracy!=null) return true;
			if(this.Cvt35_responsetime!=null) return true;
			if(this.Cvt35_accuracy!=null) return true;
			if(this.Cvt36_responsetime!=null) return true;
			if(this.Cvt36_accuracy!=null) return true;
			if(this.Cvt37_accuracy!=null) return true;
			if(this.Cvt37_responsetime!=null) return true;
			if(this.Cvt38_responsetime!=null) return true;
			if(this.Cvt38_accuracy!=null) return true;
			if(this.Cvt39_responsetime!=null) return true;
			if(this.Cvt39_accuracy!=null) return true;
			if(this.Cvt40_responsetime!=null) return true;
			if(this.Cvt40_accuracy!=null) return true;
			if(this.Oet1_accuracy!=null) return true;
			if(this.Oet1_responsetime!=null) return true;
			if(this.Oet2_accuracy!=null) return true;
			if(this.Oet2_responsetime!=null) return true;
			if(this.Oet3_responsetime!=null) return true;
			if(this.Oet3_accuracy!=null) return true;
			if(this.Oet4_responsetime!=null) return true;
			if(this.Oet4_accuracy!=null) return true;
			if(this.Oet5_responsetime!=null) return true;
			if(this.Oet5_accuracy!=null) return true;
			if(this.Oet6_accuracy!=null) return true;
			if(this.Oet6_responsetime!=null) return true;
			if(this.Oet7_accuracy!=null) return true;
			if(this.Oet7_responsetime!=null) return true;
			if(this.Oet8_accuracy!=null) return true;
			if(this.Oet8_responsetime!=null) return true;
			if(this.Oet9_accuracy!=null) return true;
			if(this.Oet9_responsetime!=null) return true;
			if(this.Oet10_accuracy!=null) return true;
			if(this.Oet10_responsetime!=null) return true;
			if(this.Oet11_accuracy!=null) return true;
			if(this.Oet11_responsetime!=null) return true;
			if(this.Oet12_accuracy!=null) return true;
			if(this.Oet12_responsetime!=null) return true;
			if(this.Oet13_accuracy!=null) return true;
			if(this.Oet13_responsetime!=null) return true;
			if(this.Oet14_accuracy!=null) return true;
			if(this.Oet14_responsetime!=null) return true;
			if(this.Oet15_accuracy!=null) return true;
			if(this.Oet15_responsetime!=null) return true;
			if(this.Oet16_accuracy!=null) return true;
			if(this.Oet16_responsetime!=null) return true;
			if(this.Oet17_accuracy!=null) return true;
			if(this.Oet17_responsetime!=null) return true;
			if(this.Oet18_accuracy!=null) return true;
			if(this.Oet18_responsetime!=null) return true;
			if(this.Oet19_accuracy!=null) return true;
			if(this.Oet19_responsetime!=null) return true;
			if(this.Oet20_accuracy!=null) return true;
			if(this.Oet20_responsetime!=null) return true;
			if(this.Oet21_accuracy!=null) return true;
			if(this.Oet21_responsetime!=null) return true;
			if(this.Oet22_accuracy!=null) return true;
			if(this.Oet22_responsetime!=null) return true;
			if(this.Oet23_accuracy!=null) return true;
			if(this.Oet23_responsetime!=null) return true;
			if(this.Oet24_responsetime!=null) return true;
			if(this.Oet24_accuracy!=null) return true;
			if(this.Oet25_responsetime!=null) return true;
			if(this.Oet25_accuracy!=null) return true;
			if(this.Oet26_responsetime!=null) return true;
			if(this.Oet26_accuracy!=null) return true;
			if(this.Oet27_responsetime!=null) return true;
			if(this.Oet27_accuracy!=null) return true;
			if(this.Oet28_responsetime!=null) return true;
			if(this.Oet28_accuracy!=null) return true;
			if(this.Oet29_responsetime!=null) return true;
			if(this.Oet29_accuracy!=null) return true;
			if(this.Oet30_responsetime!=null) return true;
			if(this.Oet30_accuracy!=null) return true;
			if(this.Oet31_responsetime!=null) return true;
			if(this.Oet31_accuracy!=null) return true;
			if(this.Oet32_responsetime!=null) return true;
			if(this.Oet32_accuracy!=null) return true;
			if(this.Oet33_responsetime!=null) return true;
			if(this.Oet33_accuracy!=null) return true;
			if(this.Oet34_responsetime!=null) return true;
			if(this.Oet34_accuracy!=null) return true;
			if(this.Oet35_accuracy!=null) return true;
			if(this.Oet35_responsetime!=null) return true;
			if(this.Oet36_accuracy!=null) return true;
			if(this.Oet36_responsetime!=null) return true;
			if(this.Oet37_responsetime!=null) return true;
			if(this.Oet37_accuracy!=null) return true;
			if(this.Oet38_responsetime!=null) return true;
			if(this.Oet38_accuracy!=null) return true;
			if(this.Oet39_responsetime!=null) return true;
			if(this.Oet39_accuracy!=null) return true;
			if(this.Oet40_responsetime!=null) return true;
			if(this.Oet40_accuracy!=null) return true;
			if(this.Cvoet1_accuracy!=null) return true;
			if(this.Cvoet1_responsetime!=null) return true;
			if(this.Cvoet2_accuracy!=null) return true;
			if(this.Cvoet2_responsetime!=null) return true;
			if(this.Cvoet3_accuracy!=null) return true;
			if(this.Cvoet3_responsetime!=null) return true;
			if(this.Cvoet4_responsetime!=null) return true;
			if(this.Cvoet4_accuracy!=null) return true;
			if(this.Cvoet5_responsetime!=null) return true;
			if(this.Cvoet5_accuracy!=null) return true;
			if(this.Cvoet6_responsetime!=null) return true;
			if(this.Cvoet6_accuracy!=null) return true;
			if(this.Cvoet7_responsetime!=null) return true;
			if(this.Cvoet7_accuracy!=null) return true;
			if(this.Cvoet8_responsetime!=null) return true;
			if(this.Cvoet8_accuracy!=null) return true;
			if(this.Cvoet9_responsetime!=null) return true;
			if(this.Cvoet9_accuracy!=null) return true;
			if(this.Cvoet10_accuracy!=null) return true;
			if(this.Cvoet10_responsetime!=null) return true;
			if(this.Cvoet11_accuracy!=null) return true;
			if(this.Cvoet11_responsetime!=null) return true;
			if(this.Cvoet12_accuracy!=null) return true;
			if(this.Cvoet12_responsetime!=null) return true;
			if(this.Cvoet13_accuracy!=null) return true;
			if(this.Cvoet13_responsetime!=null) return true;
			if(this.Cvoet14_accuracy!=null) return true;
			if(this.Cvoet14_responsetime!=null) return true;
			if(this.Cvoet15_responsetime!=null) return true;
			if(this.Cvoet15_accuracy!=null) return true;
			if(this.Cvoet16_responsetime!=null) return true;
			if(this.Cvoet16_accuracy!=null) return true;
			if(this.Cvoet17_responsetime!=null) return true;
			if(this.Cvoet17_accuracy!=null) return true;
			if(this.Cvoet18_responsetime!=null) return true;
			if(this.Cvoet18_accuracy!=null) return true;
			if(this.Cvoet19_responsetime!=null) return true;
			if(this.Cvoet19_accuracy!=null) return true;
			if(this.Cvoet20_accuracy!=null) return true;
			if(this.Cvoet20_responsetime!=null) return true;
			if(this.Cvoet21_responsetime!=null) return true;
			if(this.Cvoet21_accuracy!=null) return true;
			if(this.Cvoet22_responsetime!=null) return true;
			if(this.Cvoet22_accuracy!=null) return true;
			if(this.Cvoet23_responsetime!=null) return true;
			if(this.Cvoet23_accuracy!=null) return true;
			if(this.Cvoet24_responsetime!=null) return true;
			if(this.Cvoet24_accuracy!=null) return true;
			if(this.Cvoet25_responsetime!=null) return true;
			if(this.Cvoet25_accuracy!=null) return true;
			if(this.Cvoet26_responsetime!=null) return true;
			if(this.Cvoet26_accuracy!=null) return true;
			if(this.Cvoet27_responsetime!=null) return true;
			if(this.Cvoet27_accuracy!=null) return true;
			if(this.Cvoet28_responsetime!=null) return true;
			if(this.Cvoet28_accuracy!=null) return true;
			if(this.Cvoet29_responsetime!=null) return true;
			if(this.Cvoet29_accuracy!=null) return true;
			if(this.Cvoet30_responsetime!=null) return true;
			if(this.Cvoet30_accuracy!=null) return true;
			if(this.Cvoet31_responsetime!=null) return true;
			if(this.Cvoet31_accuracy!=null) return true;
			if(this.Cvoet32_responsetime!=null) return true;
			if(this.Cvoet32_accuracy!=null) return true;
			if(this.Cvoet33_accuracy!=null) return true;
			if(this.Cvoet33_responsetime!=null) return true;
			if(this.Cvoet34_responsetime!=null) return true;
			if(this.Cvoet34_accuracy!=null) return true;
			if(this.Cvoet35_accuracy!=null) return true;
			if(this.Cvoet35_responsetime!=null) return true;
			if(this.Cvoet36_accuracy!=null) return true;
			if(this.Cvoet36_responsetime!=null) return true;
			if(this.Cvoet37_accuracy!=null) return true;
			if(this.Cvoet37_responsetime!=null) return true;
			if(this.Cvoet38_accuracy!=null) return true;
			if(this.Cvoet38_responsetime!=null) return true;
			if(this.Cvoet39_accuracy!=null) return true;
			if(this.Cvoet39_responsetime!=null) return true;
			if(this.Cvoet40_accuracy!=null) return true;
			if(this.Cvoet40_responsetime!=null) return true;
			if(this.Cvoet41_accuracy!=null) return true;
			if(this.Cvoet41_responsetime!=null) return true;
			if(this.Cvoet42_accuracy!=null) return true;
			if(this.Cvoet42_responsetime!=null) return true;
			if(this.Cvoet43_accuracy!=null) return true;
			if(this.Cvoet43_responsetime!=null) return true;
			if(this.Cvoet44_accuracy!=null) return true;
			if(this.Cvoet44_responsetime!=null) return true;
			if(this.Cvoet45_accuracy!=null) return true;
			if(this.Cvoet45_responsetime!=null) return true;
			if(this.Cvoet46_accuracy!=null) return true;
			if(this.Cvoet46_responsetime!=null) return true;
			if(this.Cvoet47_responsetime!=null) return true;
			if(this.Cvoet47_accuracy!=null) return true;
			if(this.Cvoet48_responsetime!=null) return true;
			if(this.Cvoet48_accuracy!=null) return true;
			if(this.Cvoet49_responsetime!=null) return true;
			if(this.Cvoet49_accuracy!=null) return true;
			if(this.Cvoet50_accuracy!=null) return true;
			if(this.Cvoet50_responsetime!=null) return true;
			if(this.Cvoet51_responsetime!=null) return true;
			if(this.Cvoet51_accuracy!=null) return true;
			if(this.Cvoet52_responsetime!=null) return true;
			if(this.Cvoet52_accuracy!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
