/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function fs_longFSData(){
this.xsiType="fs:longFSData";

	this.getSchemaElementName=function(){
		return "longFSData";
	}

	this.getFullSchemaElementName=function(){
		return "fs:longFSData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.FsVersion=null;


	function getFsVersion() {
		return this.FsVersion;
	}
	this.getFsVersion=getFsVersion;


	function setFsVersion(v){
		this.FsVersion=v;
	}
	this.setFsVersion=setFsVersion;

	this.Measures_volumetric_icv=null;


	function getMeasures_volumetric_icv() {
		return this.Measures_volumetric_icv;
	}
	this.getMeasures_volumetric_icv=getMeasures_volumetric_icv;


	function setMeasures_volumetric_icv(v){
		this.Measures_volumetric_icv=v;
	}
	this.setMeasures_volumetric_icv=setMeasures_volumetric_icv;

	this.Measures_volumetric_lhcortexvol=null;


	function getMeasures_volumetric_lhcortexvol() {
		return this.Measures_volumetric_lhcortexvol;
	}
	this.getMeasures_volumetric_lhcortexvol=getMeasures_volumetric_lhcortexvol;


	function setMeasures_volumetric_lhcortexvol(v){
		this.Measures_volumetric_lhcortexvol=v;
	}
	this.setMeasures_volumetric_lhcortexvol=setMeasures_volumetric_lhcortexvol;

	this.Measures_volumetric_rhcortexvol=null;


	function getMeasures_volumetric_rhcortexvol() {
		return this.Measures_volumetric_rhcortexvol;
	}
	this.getMeasures_volumetric_rhcortexvol=getMeasures_volumetric_rhcortexvol;


	function setMeasures_volumetric_rhcortexvol(v){
		this.Measures_volumetric_rhcortexvol=v;
	}
	this.setMeasures_volumetric_rhcortexvol=setMeasures_volumetric_rhcortexvol;

	this.Measures_volumetric_cortexvol=null;


	function getMeasures_volumetric_cortexvol() {
		return this.Measures_volumetric_cortexvol;
	}
	this.getMeasures_volumetric_cortexvol=getMeasures_volumetric_cortexvol;


	function setMeasures_volumetric_cortexvol(v){
		this.Measures_volumetric_cortexvol=v;
	}
	this.setMeasures_volumetric_cortexvol=setMeasures_volumetric_cortexvol;

	this.Measures_volumetric_subcortgrayvol=null;


	function getMeasures_volumetric_subcortgrayvol() {
		return this.Measures_volumetric_subcortgrayvol;
	}
	this.getMeasures_volumetric_subcortgrayvol=getMeasures_volumetric_subcortgrayvol;


	function setMeasures_volumetric_subcortgrayvol(v){
		this.Measures_volumetric_subcortgrayvol=v;
	}
	this.setMeasures_volumetric_subcortgrayvol=setMeasures_volumetric_subcortgrayvol;

	this.Measures_volumetric_totalgrayvol=null;


	function getMeasures_volumetric_totalgrayvol() {
		return this.Measures_volumetric_totalgrayvol;
	}
	this.getMeasures_volumetric_totalgrayvol=getMeasures_volumetric_totalgrayvol;


	function setMeasures_volumetric_totalgrayvol(v){
		this.Measures_volumetric_totalgrayvol=v;
	}
	this.setMeasures_volumetric_totalgrayvol=setMeasures_volumetric_totalgrayvol;

	this.Measures_volumetric_supratentorialvol=null;


	function getMeasures_volumetric_supratentorialvol() {
		return this.Measures_volumetric_supratentorialvol;
	}
	this.getMeasures_volumetric_supratentorialvol=getMeasures_volumetric_supratentorialvol;


	function setMeasures_volumetric_supratentorialvol(v){
		this.Measures_volumetric_supratentorialvol=v;
	}
	this.setMeasures_volumetric_supratentorialvol=setMeasures_volumetric_supratentorialvol;

	this.Measures_volumetric_lhcorticalwhitemattervol=null;


	function getMeasures_volumetric_lhcorticalwhitemattervol() {
		return this.Measures_volumetric_lhcorticalwhitemattervol;
	}
	this.getMeasures_volumetric_lhcorticalwhitemattervol=getMeasures_volumetric_lhcorticalwhitemattervol;


	function setMeasures_volumetric_lhcorticalwhitemattervol(v){
		this.Measures_volumetric_lhcorticalwhitemattervol=v;
	}
	this.setMeasures_volumetric_lhcorticalwhitemattervol=setMeasures_volumetric_lhcorticalwhitemattervol;

	this.Measures_volumetric_rhcorticalwhitemattervol=null;


	function getMeasures_volumetric_rhcorticalwhitemattervol() {
		return this.Measures_volumetric_rhcorticalwhitemattervol;
	}
	this.getMeasures_volumetric_rhcorticalwhitemattervol=getMeasures_volumetric_rhcorticalwhitemattervol;


	function setMeasures_volumetric_rhcorticalwhitemattervol(v){
		this.Measures_volumetric_rhcorticalwhitemattervol=v;
	}
	this.setMeasures_volumetric_rhcorticalwhitemattervol=setMeasures_volumetric_rhcorticalwhitemattervol;

	this.Measures_volumetric_corticalwhitemattervol=null;


	function getMeasures_volumetric_corticalwhitemattervol() {
		return this.Measures_volumetric_corticalwhitemattervol;
	}
	this.getMeasures_volumetric_corticalwhitemattervol=getMeasures_volumetric_corticalwhitemattervol;


	function setMeasures_volumetric_corticalwhitemattervol(v){
		this.Measures_volumetric_corticalwhitemattervol=v;
	}
	this.setMeasures_volumetric_corticalwhitemattervol=setMeasures_volumetric_corticalwhitemattervol;
	this.Measures_volumetric_regions_region =new Array();

	function getMeasures_volumetric_regions_region() {
		return this.Measures_volumetric_regions_region;
	}
	this.getMeasures_volumetric_regions_region=getMeasures_volumetric_regions_region;


	function addMeasures_volumetric_regions_region(v){
		this.Measures_volumetric_regions_region.push(v);
	}
	this.addMeasures_volumetric_regions_region=addMeasures_volumetric_regions_region;
	this.Measures_surface_hemisphere =new Array();

	function getMeasures_surface_hemisphere() {
		return this.Measures_surface_hemisphere;
	}
	this.getMeasures_surface_hemisphere=getMeasures_surface_hemisphere;


	function addMeasures_surface_hemisphere(v){
		this.Measures_surface_hemisphere.push(v);
	}
	this.addMeasures_surface_hemisphere=addMeasures_surface_hemisphere;
	this.Timepoints_timepoint =new Array();

	function getTimepoints_timepoint() {
		return this.Timepoints_timepoint;
	}
	this.getTimepoints_timepoint=getTimepoints_timepoint;


	function addTimepoints_timepoint(v){
		this.Timepoints_timepoint.push(v);
	}
	this.addTimepoints_timepoint=addTimepoints_timepoint;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="fs_version"){
				return this.FsVersion ;
			} else 
			if(xmlPath=="measures/volumetric/ICV"){
				return this.Measures_volumetric_icv ;
			} else 
			if(xmlPath=="measures/volumetric/lhCortexVol"){
				return this.Measures_volumetric_lhcortexvol ;
			} else 
			if(xmlPath=="measures/volumetric/rhCortexVol"){
				return this.Measures_volumetric_rhcortexvol ;
			} else 
			if(xmlPath=="measures/volumetric/CortexVol"){
				return this.Measures_volumetric_cortexvol ;
			} else 
			if(xmlPath=="measures/volumetric/SubCortGrayVol"){
				return this.Measures_volumetric_subcortgrayvol ;
			} else 
			if(xmlPath=="measures/volumetric/TotalGrayVol"){
				return this.Measures_volumetric_totalgrayvol ;
			} else 
			if(xmlPath=="measures/volumetric/SupraTentorialVol"){
				return this.Measures_volumetric_supratentorialvol ;
			} else 
			if(xmlPath=="measures/volumetric/lhCorticalWhiteMatterVol"){
				return this.Measures_volumetric_lhcorticalwhitemattervol ;
			} else 
			if(xmlPath=="measures/volumetric/rhCorticalWhiteMatterVol"){
				return this.Measures_volumetric_rhcorticalwhitemattervol ;
			} else 
			if(xmlPath=="measures/volumetric/CorticalWhiteMatterVol"){
				return this.Measures_volumetric_corticalwhitemattervol ;
			} else 
			if(xmlPath=="measures/volumetric/regions/region"){
				return this.Measures_volumetric_regions_region ;
			} else 
			if(xmlPath.startsWith("measures/volumetric/regions/region")){
				xmlPath=xmlPath.substring(34);
				if(xmlPath=="")return this.Measures_volumetric_regions_region ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Measures_volumetric_regions_region.length;whereCount++){

					var tempValue=this.Measures_volumetric_regions_region[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Measures_volumetric_regions_region[whereCount]);

					}

				}
				}else{

				whereArray=this.Measures_volumetric_regions_region;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="measures/surface/hemisphere"){
				return this.Measures_surface_hemisphere ;
			} else 
			if(xmlPath.startsWith("measures/surface/hemisphere")){
				xmlPath=xmlPath.substring(27);
				if(xmlPath=="")return this.Measures_surface_hemisphere ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Measures_surface_hemisphere.length;whereCount++){

					var tempValue=this.Measures_surface_hemisphere[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Measures_surface_hemisphere[whereCount]);

					}

				}
				}else{

				whereArray=this.Measures_surface_hemisphere;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="timepoints/timepoint"){
				return this.Timepoints_timepoint ;
			} else 
			if(xmlPath.startsWith("timepoints/timepoint")){
				xmlPath=xmlPath.substring(20);
				if(xmlPath=="")return this.Timepoints_timepoint ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Timepoints_timepoint.length;whereCount++){

					var tempValue=this.Timepoints_timepoint[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Timepoints_timepoint[whereCount]);

					}

				}
				}else{

				whereArray=this.Timepoints_timepoint;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="fs_version"){
				this.FsVersion=value;
			} else 
			if(xmlPath=="measures/volumetric/ICV"){
				this.Measures_volumetric_icv=value;
			} else 
			if(xmlPath=="measures/volumetric/lhCortexVol"){
				this.Measures_volumetric_lhcortexvol=value;
			} else 
			if(xmlPath=="measures/volumetric/rhCortexVol"){
				this.Measures_volumetric_rhcortexvol=value;
			} else 
			if(xmlPath=="measures/volumetric/CortexVol"){
				this.Measures_volumetric_cortexvol=value;
			} else 
			if(xmlPath=="measures/volumetric/SubCortGrayVol"){
				this.Measures_volumetric_subcortgrayvol=value;
			} else 
			if(xmlPath=="measures/volumetric/TotalGrayVol"){
				this.Measures_volumetric_totalgrayvol=value;
			} else 
			if(xmlPath=="measures/volumetric/SupraTentorialVol"){
				this.Measures_volumetric_supratentorialvol=value;
			} else 
			if(xmlPath=="measures/volumetric/lhCorticalWhiteMatterVol"){
				this.Measures_volumetric_lhcorticalwhitemattervol=value;
			} else 
			if(xmlPath=="measures/volumetric/rhCorticalWhiteMatterVol"){
				this.Measures_volumetric_rhcorticalwhitemattervol=value;
			} else 
			if(xmlPath=="measures/volumetric/CorticalWhiteMatterVol"){
				this.Measures_volumetric_corticalwhitemattervol=value;
			} else 
			if(xmlPath=="measures/volumetric/regions/region"){
				this.Measures_volumetric_regions_region=value;
			} else 
			if(xmlPath.startsWith("measures/volumetric/regions/region")){
				xmlPath=xmlPath.substring(34);
				if(xmlPath=="")return this.Measures_volumetric_regions_region ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Measures_volumetric_regions_region.length;whereCount++){

					var tempValue=this.Measures_volumetric_regions_region[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Measures_volumetric_regions_region[whereCount]);

					}

				}
				}else{

				whereArray=this.Measures_volumetric_regions_region;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("fs:longFSData_region");//omUtils.js
					}
					this.addMeasures_volumetric_regions_region(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="measures/surface/hemisphere"){
				this.Measures_surface_hemisphere=value;
			} else 
			if(xmlPath.startsWith("measures/surface/hemisphere")){
				xmlPath=xmlPath.substring(27);
				if(xmlPath=="")return this.Measures_surface_hemisphere ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Measures_surface_hemisphere.length;whereCount++){

					var tempValue=this.Measures_surface_hemisphere[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Measures_surface_hemisphere[whereCount]);

					}

				}
				}else{

				whereArray=this.Measures_surface_hemisphere;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("fs:longFSData_hemisphere");//omUtils.js
					}
					this.addMeasures_surface_hemisphere(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="timepoints/timepoint"){
				this.Timepoints_timepoint=value;
			} else 
			if(xmlPath.startsWith("timepoints/timepoint")){
				xmlPath=xmlPath.substring(20);
				if(xmlPath=="")return this.Timepoints_timepoint ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Timepoints_timepoint.length;whereCount++){

					var tempValue=this.Timepoints_timepoint[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Timepoints_timepoint[whereCount]);

					}

				}
				}else{

				whereArray=this.Timepoints_timepoint;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("fs:longFSData_timepoint");//omUtils.js
					}
					this.addTimepoints_timepoint(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="measures/volumetric/regions/region"){
			this.addMeasures_volumetric_regions_region(v);
		}else if (xmlPath=="measures/surface/hemisphere"){
			this.addMeasures_surface_hemisphere(v);
		}else if (xmlPath=="timepoints/timepoint"){
			this.addTimepoints_timepoint(v);
		}
		else{
			this.extension.setReferenceField(xmlPath,v);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="measures/volumetric/regions/region"){
			return "http://nrg.wustl.edu/fs:longFSData_region";
		}else if (xmlPath=="measures/surface/hemisphere"){
			return "http://nrg.wustl.edu/fs:longFSData_hemisphere";
		}else if (xmlPath=="timepoints/timepoint"){
			return "http://nrg.wustl.edu/fs:longFSData_timepoint";
		}
		else{
			return this.extension.getReferenceFieldName(xmlPath);
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="fs_version"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/ICV"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/lhCortexVol"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/rhCortexVol"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/CortexVol"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/SubCortGrayVol"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/TotalGrayVol"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/SupraTentorialVol"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/lhCorticalWhiteMatterVol"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/rhCorticalWhiteMatterVol"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/CorticalWhiteMatterVol"){
			return "field_data";
		}else if (xmlPath=="measures/volumetric/regions/region"){
			return "field_multi_reference";
		}else if (xmlPath=="measures/surface/hemisphere"){
			return "field_multi_reference";
		}else if (xmlPath=="timepoints/timepoint"){
			return "field_multi_reference";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<fs:LongitudinalFS";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</fs:LongitudinalFS>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.FsVersion!=null){
			xmlTxt+="\n<fs:fs_version";
			xmlTxt+=">";
			xmlTxt+=this.FsVersion.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</fs:fs_version>";
		}
			var child0=0;
			var att0=0;
			child0+=this.Measures_volumetric_regions_region.length;
			if(this.Measures_volumetric_cortexvol!=null)
			child0++;
			if(this.Measures_volumetric_lhcorticalwhitemattervol!=null)
			child0++;
			if(this.Measures_volumetric_lhcortexvol!=null)
			child0++;
			if(this.Measures_volumetric_rhcorticalwhitemattervol!=null)
			child0++;
			if(this.Measures_volumetric_subcortgrayvol!=null)
			child0++;
			if(this.Measures_volumetric_totalgrayvol!=null)
			child0++;
			if(this.Measures_volumetric_rhcortexvol!=null)
			child0++;
			if(this.Measures_volumetric_corticalwhitemattervol!=null)
			child0++;
			if(this.Measures_volumetric_icv!=null)
			child0++;
			if(this.Measures_volumetric_supratentorialvol!=null)
			child0++;
			child0+=this.Measures_surface_hemisphere.length;
			if(child0>0 || att0>0){
				xmlTxt+="\n<fs:measures";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child1=0;
			var att1=0;
			child1+=this.Measures_volumetric_regions_region.length;
			if(this.Measures_volumetric_cortexvol!=null)
			child1++;
			if(this.Measures_volumetric_lhcorticalwhitemattervol!=null)
			child1++;
			if(this.Measures_volumetric_lhcortexvol!=null)
			child1++;
			if(this.Measures_volumetric_rhcorticalwhitemattervol!=null)
			child1++;
			if(this.Measures_volumetric_subcortgrayvol!=null)
			child1++;
			if(this.Measures_volumetric_totalgrayvol!=null)
			child1++;
			if(this.Measures_volumetric_corticalwhitemattervol!=null)
			child1++;
			if(this.Measures_volumetric_rhcortexvol!=null)
			child1++;
			if(this.Measures_volumetric_icv!=null)
			child1++;
			if(this.Measures_volumetric_supratentorialvol!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<fs:volumetric";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Measures_volumetric_icv!=null){
			xmlTxt+="\n<fs:ICV";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_icv;
			xmlTxt+="</fs:ICV>";
		}
		if (this.Measures_volumetric_lhcortexvol!=null){
			xmlTxt+="\n<fs:lhCortexVol";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_lhcortexvol;
			xmlTxt+="</fs:lhCortexVol>";
		}
		if (this.Measures_volumetric_rhcortexvol!=null){
			xmlTxt+="\n<fs:rhCortexVol";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_rhcortexvol;
			xmlTxt+="</fs:rhCortexVol>";
		}
		if (this.Measures_volumetric_cortexvol!=null){
			xmlTxt+="\n<fs:CortexVol";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_cortexvol;
			xmlTxt+="</fs:CortexVol>";
		}
		if (this.Measures_volumetric_subcortgrayvol!=null){
			xmlTxt+="\n<fs:SubCortGrayVol";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_subcortgrayvol;
			xmlTxt+="</fs:SubCortGrayVol>";
		}
		if (this.Measures_volumetric_totalgrayvol!=null){
			xmlTxt+="\n<fs:TotalGrayVol";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_totalgrayvol;
			xmlTxt+="</fs:TotalGrayVol>";
		}
		if (this.Measures_volumetric_supratentorialvol!=null){
			xmlTxt+="\n<fs:SupraTentorialVol";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_supratentorialvol;
			xmlTxt+="</fs:SupraTentorialVol>";
		}
		if (this.Measures_volumetric_lhcorticalwhitemattervol!=null){
			xmlTxt+="\n<fs:lhCorticalWhiteMatterVol";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_lhcorticalwhitemattervol;
			xmlTxt+="</fs:lhCorticalWhiteMatterVol>";
		}
		if (this.Measures_volumetric_rhcorticalwhitemattervol!=null){
			xmlTxt+="\n<fs:rhCorticalWhiteMatterVol";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_rhcorticalwhitemattervol;
			xmlTxt+="</fs:rhCorticalWhiteMatterVol>";
		}
		if (this.Measures_volumetric_corticalwhitemattervol!=null){
			xmlTxt+="\n<fs:CorticalWhiteMatterVol";
			xmlTxt+=">";
			xmlTxt+=this.Measures_volumetric_corticalwhitemattervol;
			xmlTxt+="</fs:CorticalWhiteMatterVol>";
		}
			var child2=0;
			var att2=0;
			child2+=this.Measures_volumetric_regions_region.length;
			if(child2>0 || att2>0){
				xmlTxt+="\n<fs:regions";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var Measures_volumetric_regions_regionCOUNT=0;Measures_volumetric_regions_regionCOUNT<this.Measures_volumetric_regions_region.length;Measures_volumetric_regions_regionCOUNT++){
			xmlTxt +="\n<fs:region";
			xmlTxt +=this.Measures_volumetric_regions_region[Measures_volumetric_regions_regionCOUNT].getXMLAtts();
			if(this.Measures_volumetric_regions_region[Measures_volumetric_regions_regionCOUNT].xsiType!="fs:longFSData_region"){
				xmlTxt+=" xsi:type=\"" + this.Measures_volumetric_regions_region[Measures_volumetric_regions_regionCOUNT].xsiType + "\"";
			}
			if (this.Measures_volumetric_regions_region[Measures_volumetric_regions_regionCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Measures_volumetric_regions_region[Measures_volumetric_regions_regionCOUNT].getXMLBody(preventComments);
					xmlTxt+="</fs:region>";
			}else {xmlTxt+="/>";}
		}
				xmlTxt+="\n</fs:regions>";
			}
			}

				xmlTxt+="\n</fs:volumetric>";
			}
			}

			var child3=0;
			var att3=0;
			child3+=this.Measures_surface_hemisphere.length;
			if(child3>0 || att3>0){
				xmlTxt+="\n<fs:surface";
			if(child3==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var Measures_surface_hemisphereCOUNT=0;Measures_surface_hemisphereCOUNT<this.Measures_surface_hemisphere.length;Measures_surface_hemisphereCOUNT++){
			xmlTxt +="\n<fs:hemisphere";
			xmlTxt +=this.Measures_surface_hemisphere[Measures_surface_hemisphereCOUNT].getXMLAtts();
			if(this.Measures_surface_hemisphere[Measures_surface_hemisphereCOUNT].xsiType!="fs:longFSData_hemisphere"){
				xmlTxt+=" xsi:type=\"" + this.Measures_surface_hemisphere[Measures_surface_hemisphereCOUNT].xsiType + "\"";
			}
			if (this.Measures_surface_hemisphere[Measures_surface_hemisphereCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Measures_surface_hemisphere[Measures_surface_hemisphereCOUNT].getXMLBody(preventComments);
					xmlTxt+="</fs:hemisphere>";
			}else {xmlTxt+="/>";}
		}
				xmlTxt+="\n</fs:surface>";
			}
			}

				xmlTxt+="\n</fs:measures>";
			}
			}

			var child4=0;
			var att4=0;
			child4+=this.Timepoints_timepoint.length;
			if(child4>0 || att4>0){
				xmlTxt+="\n<fs:timepoints";
			if(child4==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var Timepoints_timepointCOUNT=0;Timepoints_timepointCOUNT<this.Timepoints_timepoint.length;Timepoints_timepointCOUNT++){
			xmlTxt +="\n<fs:timepoint";
			xmlTxt +=this.Timepoints_timepoint[Timepoints_timepointCOUNT].getXMLAtts();
			if(this.Timepoints_timepoint[Timepoints_timepointCOUNT].xsiType!="fs:longFSData_timepoint"){
				xmlTxt+=" xsi:type=\"" + this.Timepoints_timepoint[Timepoints_timepointCOUNT].xsiType + "\"";
			}
			if (this.Timepoints_timepoint[Timepoints_timepointCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Timepoints_timepoint[Timepoints_timepointCOUNT].getXMLBody(preventComments);
					xmlTxt+="</fs:timepoint>";
			}else {xmlTxt+="/>";}
		}
				xmlTxt+="\n</fs:timepoints>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.FsVersion!=null) return true;
			if(this.Measures_volumetric_regions_region.length>0)return true;
			if(this.Measures_volumetric_cortexvol!=null) return true;
			if(this.Measures_volumetric_lhcorticalwhitemattervol!=null) return true;
			if(this.Measures_volumetric_lhcortexvol!=null) return true;
			if(this.Measures_volumetric_rhcorticalwhitemattervol!=null) return true;
			if(this.Measures_volumetric_subcortgrayvol!=null) return true;
			if(this.Measures_volumetric_totalgrayvol!=null) return true;
			if(this.Measures_volumetric_rhcortexvol!=null) return true;
			if(this.Measures_volumetric_corticalwhitemattervol!=null) return true;
			if(this.Measures_volumetric_icv!=null) return true;
			if(this.Measures_volumetric_supratentorialvol!=null) return true;
			if(this.Measures_surface_hemisphere.length>0)return true;
			if(this.Timepoints_timepoint.length>0)return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
