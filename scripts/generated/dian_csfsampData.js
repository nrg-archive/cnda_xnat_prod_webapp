/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function dian_csfsampData(){
this.xsiType="dian:csfsampData";

	this.getSchemaElementName=function(){
		return "csfsampData";
	}

	this.getFullSchemaElementName=function(){
		return "dian:csfsampData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Csfcoll=null;


	function getCsfcoll() {
		return this.Csfcoll;
	}
	this.getCsfcoll=getCsfcoll;


	function setCsfcoll(v){
		this.Csfcoll=v;
	}
	this.setCsfcoll=setCsfcoll;

	this.Csfinit=null;


	function getCsfinit() {
		return this.Csfinit;
	}
	this.getCsfinit=getCsfinit;


	function setCsfinit(v){
		this.Csfinit=v;
	}
	this.setCsfinit=setCsfinit;

	this.Csfdate=null;


	function getCsfdate() {
		return this.Csfdate;
	}
	this.getCsfdate=getCsfdate;


	function setCsfdate(v){
		this.Csfdate=v;
	}
	this.setCsfdate=setCsfdate;

	this.Csffast=null;


	function getCsffast() {
		return this.Csffast;
	}
	this.getCsffast=getCsffast;


	function setCsffast(v){
		this.Csffast=v;
	}
	this.setCsffast=setCsffast;

	this.Pretemp=null;


	function getPretemp() {
		return this.Pretemp;
	}
	this.getPretemp=getPretemp;


	function setPretemp(v){
		this.Pretemp=v;
	}
	this.setPretemp=setPretemp;

	this.Pretmpunt=null;


	function getPretmpunt() {
		return this.Pretmpunt;
	}
	this.getPretmpunt=getPretmpunt;


	function setPretmpunt(v){
		this.Pretmpunt=v;
	}
	this.setPretmpunt=setPretmpunt;

	this.Presys=null;


	function getPresys() {
		return this.Presys;
	}
	this.getPresys=getPresys;


	function setPresys(v){
		this.Presys=v;
	}
	this.setPresys=setPresys;

	this.Predia=null;


	function getPredia() {
		return this.Predia;
	}
	this.getPredia=getPredia;


	function setPredia(v){
		this.Predia=v;
	}
	this.setPredia=setPredia;

	this.Prepulse=null;


	function getPrepulse() {
		return this.Prepulse;
	}
	this.getPrepulse=getPrepulse;


	function setPrepulse(v){
		this.Prepulse=v;
	}
	this.setPrepulse=setPrepulse;

	this.Preresp=null;


	function getPreresp() {
		return this.Preresp;
	}
	this.getPreresp=getPreresp;


	function setPreresp(v){
		this.Preresp=v;
	}
	this.setPreresp=setPreresp;

	this.Needle=null;


	function getNeedle() {
		return this.Needle;
	}
	this.getNeedle=getNeedle;


	function setNeedle(v){
		this.Needle=v;
	}
	this.setNeedle=setNeedle;

	this.Needleoth=null;


	function getNeedleoth() {
		return this.Needleoth;
	}
	this.getNeedleoth=getNeedleoth;


	function setNeedleoth(v){
		this.Needleoth=v;
	}
	this.setNeedleoth=setNeedleoth;

	this.Method=null;


	function getMethod() {
		return this.Method;
	}
	this.getMethod=getMethod;


	function setMethod(v){
		this.Method=v;
	}
	this.setMethod=setMethod;

	this.Lpsite=null;


	function getLpsite() {
		return this.Lpsite;
	}
	this.getLpsite=getLpsite;


	function setLpsite(v){
		this.Lpsite=v;
	}
	this.setLpsite=setLpsite;

	this.Lpsiteoth=null;


	function getLpsiteoth() {
		return this.Lpsiteoth;
	}
	this.getLpsiteoth=getLpsiteoth;


	function setLpsiteoth(v){
		this.Lpsiteoth=v;
	}
	this.setLpsiteoth=setLpsiteoth;

	this.Position2=null;


	function getPosition2() {
		return this.Position2;
	}
	this.getPosition2=getPosition2;


	function setPosition2(v){
		this.Position2=v;
	}
	this.setPosition2=setPosition2;

	this.Tubecoll=null;


	function getTubecoll() {
		return this.Tubecoll;
	}
	this.getTubecoll=getTubecoll;


	function setTubecoll(v){
		this.Tubecoll=v;
	}
	this.setTubecoll=setTubecoll;

	this.Tubeship=null;


	function getTubeship() {
		return this.Tubeship;
	}
	this.getTubeship=getTubeship;


	function setTubeship(v){
		this.Tubeship=v;
	}
	this.setTubeship=setTubeship;

	this.Styrtime=null;


	function getStyrtime() {
		return this.Styrtime;
	}
	this.getStyrtime=getStyrtime;


	function setStyrtime(v){
		this.Styrtime=v;
	}
	this.setStyrtime=setStyrtime;

	this.Csftime=null;


	function getCsftime() {
		return this.Csftime;
	}
	this.getCsftime=getCsftime;


	function setCsftime(v){
		this.Csftime=v;
	}
	this.setCsftime=setCsftime;

	this.Csftrans=null;


	function getCsftrans() {
		return this.Csftrans;
	}
	this.getCsftrans=getCsftrans;


	function setCsftrans(v){
		this.Csftrans=v;
	}
	this.setCsftrans=setCsftrans;

	this.Volcoll=null;


	function getVolcoll() {
		return this.Volcoll;
	}
	this.getVolcoll=getVolcoll;


	function setVolcoll(v){
		this.Volcoll=v;
	}
	this.setVolcoll=setVolcoll;

	this.Voltrans=null;


	function getVoltrans() {
		return this.Voltrans;
	}
	this.getVoltrans=getVoltrans;


	function setVoltrans(v){
		this.Voltrans=v;
	}
	this.setVoltrans=setVoltrans;

	this.Loclab=null;


	function getLoclab() {
		return this.Loclab;
	}
	this.getLoclab=getLoclab;


	function setLoclab(v){
		this.Loclab=v;
	}
	this.setLoclab=setLoclab;

	this.Timefroz=null;


	function getTimefroz() {
		return this.Timefroz;
	}
	this.getTimefroz=getTimefroz;


	function setTimefroz(v){
		this.Timefroz=v;
	}
	this.setTimefroz=setTimefroz;

	this.Notes=null;


	function getNotes() {
		return this.Notes;
	}
	this.getNotes=getNotes;


	function setNotes(v){
		this.Notes=v;
	}
	this.setNotes=setNotes;

	this.Csfship=null;


	function getCsfship() {
		return this.Csfship;
	}
	this.getCsfship=getCsfship;


	function setCsfship(v){
		this.Csfship=v;
	}
	this.setCsfship=setCsfship;

	this.Csfshdt=null;


	function getCsfshdt() {
		return this.Csfshdt;
	}
	this.getCsfshdt=getCsfshdt;


	function setCsfshdt(v){
		this.Csfshdt=v;
	}
	this.setCsfshdt=setCsfshdt;

	this.Csfshnum=null;


	function getCsfshnum() {
		return this.Csfshnum;
	}
	this.getCsfshnum=getCsfshnum;


	function setCsfshnum(v){
		this.Csfshnum=v;
	}
	this.setCsfshnum=setCsfshnum;

	this.Csfdatervd=null;


	function getCsfdatervd() {
		return this.Csfdatervd;
	}
	this.getCsfdatervd=getCsfdatervd;


	function setCsfdatervd(v){
		this.Csfdatervd=v;
	}
	this.setCsfdatervd=setCsfdatervd;

	this.Csfvolrvd=null;


	function getCsfvolrvd() {
		return this.Csfvolrvd;
	}
	this.getCsfvolrvd=getCsfvolrvd;


	function setCsfvolrvd(v){
		this.Csfvolrvd=v;
	}
	this.setCsfvolrvd=setCsfvolrvd;

	this.Csfvialsrvd=null;


	function getCsfvialsrvd() {
		return this.Csfvialsrvd;
	}
	this.getCsfvialsrvd=getCsfvialsrvd;


	function setCsfvialsrvd(v){
		this.Csfvialsrvd=v;
	}
	this.setCsfvialsrvd=setCsfvialsrvd;

	this.Position3=null;


	function getPosition3() {
		return this.Position3;
	}
	this.getPosition3=getPosition3;


	function setPosition3(v){
		this.Position3=v;
	}
	this.setPosition3=setPosition3;

	this.Posttemp=null;


	function getPosttemp() {
		return this.Posttemp;
	}
	this.getPosttemp=getPosttemp;


	function setPosttemp(v){
		this.Posttemp=v;
	}
	this.setPosttemp=setPosttemp;

	this.Posttmpu=null;


	function getPosttmpu() {
		return this.Posttmpu;
	}
	this.getPosttmpu=getPosttmpu;


	function setPosttmpu(v){
		this.Posttmpu=v;
	}
	this.setPosttmpu=setPosttmpu;

	this.Postsys=null;


	function getPostsys() {
		return this.Postsys;
	}
	this.getPostsys=getPostsys;


	function setPostsys(v){
		this.Postsys=v;
	}
	this.setPostsys=setPostsys;

	this.Postdia=null;


	function getPostdia() {
		return this.Postdia;
	}
	this.getPostdia=getPostdia;


	function setPostdia(v){
		this.Postdia=v;
	}
	this.setPostdia=setPostdia;

	this.Postpulse=null;


	function getPostpulse() {
		return this.Postpulse;
	}
	this.getPostpulse=getPostpulse;


	function setPostpulse(v){
		this.Postpulse=v;
	}
	this.setPostpulse=setPostpulse;

	this.Postresp=null;


	function getPostresp() {
		return this.Postresp;
	}
	this.getPostresp=getPostresp;


	function setPostresp(v){
		this.Postresp=v;
	}
	this.setPostresp=setPostresp;

	this.Position4=null;


	function getPosition4() {
		return this.Position4;
	}
	this.getPosition4=getPosition4;


	function setPosition4(v){
		this.Position4=v;
	}
	this.setPosition4=setPosition4;

	this.Hrtemp=null;


	function getHrtemp() {
		return this.Hrtemp;
	}
	this.getHrtemp=getHrtemp;


	function setHrtemp(v){
		this.Hrtemp=v;
	}
	this.setHrtemp=setHrtemp;

	this.Hrtmpu=null;


	function getHrtmpu() {
		return this.Hrtmpu;
	}
	this.getHrtmpu=getHrtmpu;


	function setHrtmpu(v){
		this.Hrtmpu=v;
	}
	this.setHrtmpu=setHrtmpu;

	this.Hrsys=null;


	function getHrsys() {
		return this.Hrsys;
	}
	this.getHrsys=getHrsys;


	function setHrsys(v){
		this.Hrsys=v;
	}
	this.setHrsys=setHrsys;

	this.Hrdia=null;


	function getHrdia() {
		return this.Hrdia;
	}
	this.getHrdia=getHrdia;


	function setHrdia(v){
		this.Hrdia=v;
	}
	this.setHrdia=setHrdia;

	this.Hrpulse=null;


	function getHrpulse() {
		return this.Hrpulse;
	}
	this.getHrpulse=getHrpulse;


	function setHrpulse(v){
		this.Hrpulse=v;
	}
	this.setHrpulse=setHrpulse;

	this.Hrresp=null;


	function getHrresp() {
		return this.Hrresp;
	}
	this.getHrresp=getHrresp;


	function setHrresp(v){
		this.Hrresp=v;
	}
	this.setHrresp=setHrresp;

	this.Comp=null;


	function getComp() {
		return this.Comp;
	}
	this.getComp=getComp;


	function setComp(v){
		this.Comp=v;
	}
	this.setComp=setComp;

	this.Compoth=null;


	function getCompoth() {
		return this.Compoth;
	}
	this.getCompoth=getCompoth;


	function setCompoth(v){
		this.Compoth=v;
	}
	this.setCompoth=setCompoth;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="CSFCOLL"){
				return this.Csfcoll ;
			} else 
			if(xmlPath=="CSFINIT"){
				return this.Csfinit ;
			} else 
			if(xmlPath=="CSFDATE"){
				return this.Csfdate ;
			} else 
			if(xmlPath=="CSFFAST"){
				return this.Csffast ;
			} else 
			if(xmlPath=="PRETEMP"){
				return this.Pretemp ;
			} else 
			if(xmlPath=="PRETMPUNT"){
				return this.Pretmpunt ;
			} else 
			if(xmlPath=="PRESYS"){
				return this.Presys ;
			} else 
			if(xmlPath=="PREDIA"){
				return this.Predia ;
			} else 
			if(xmlPath=="PREPULSE"){
				return this.Prepulse ;
			} else 
			if(xmlPath=="PRERESP"){
				return this.Preresp ;
			} else 
			if(xmlPath=="NEEDLE"){
				return this.Needle ;
			} else 
			if(xmlPath=="NEEDLEOTH"){
				return this.Needleoth ;
			} else 
			if(xmlPath=="METHOD"){
				return this.Method ;
			} else 
			if(xmlPath=="LPSITE"){
				return this.Lpsite ;
			} else 
			if(xmlPath=="LPSITEOTH"){
				return this.Lpsiteoth ;
			} else 
			if(xmlPath=="POSITION2"){
				return this.Position2 ;
			} else 
			if(xmlPath=="TUBECOLL"){
				return this.Tubecoll ;
			} else 
			if(xmlPath=="TUBESHIP"){
				return this.Tubeship ;
			} else 
			if(xmlPath=="STYRTIME"){
				return this.Styrtime ;
			} else 
			if(xmlPath=="CSFTIME"){
				return this.Csftime ;
			} else 
			if(xmlPath=="CSFTRANS"){
				return this.Csftrans ;
			} else 
			if(xmlPath=="VOLCOLL"){
				return this.Volcoll ;
			} else 
			if(xmlPath=="VOLTRANS"){
				return this.Voltrans ;
			} else 
			if(xmlPath=="LOCLAB"){
				return this.Loclab ;
			} else 
			if(xmlPath=="TIMEFROZ"){
				return this.Timefroz ;
			} else 
			if(xmlPath=="NOTES"){
				return this.Notes ;
			} else 
			if(xmlPath=="CSFSHIP"){
				return this.Csfship ;
			} else 
			if(xmlPath=="CSFSHDT"){
				return this.Csfshdt ;
			} else 
			if(xmlPath=="CSFSHNUM"){
				return this.Csfshnum ;
			} else 
			if(xmlPath=="CSFDATERVD"){
				return this.Csfdatervd ;
			} else 
			if(xmlPath=="CSFVOLRVD"){
				return this.Csfvolrvd ;
			} else 
			if(xmlPath=="CSFVIALSRVD"){
				return this.Csfvialsrvd ;
			} else 
			if(xmlPath=="POSITION3"){
				return this.Position3 ;
			} else 
			if(xmlPath=="POSTTEMP"){
				return this.Posttemp ;
			} else 
			if(xmlPath=="POSTTMPU"){
				return this.Posttmpu ;
			} else 
			if(xmlPath=="POSTSYS"){
				return this.Postsys ;
			} else 
			if(xmlPath=="POSTDIA"){
				return this.Postdia ;
			} else 
			if(xmlPath=="POSTPULSE"){
				return this.Postpulse ;
			} else 
			if(xmlPath=="POSTRESP"){
				return this.Postresp ;
			} else 
			if(xmlPath=="POSITION4"){
				return this.Position4 ;
			} else 
			if(xmlPath=="HRTEMP"){
				return this.Hrtemp ;
			} else 
			if(xmlPath=="HRTMPU"){
				return this.Hrtmpu ;
			} else 
			if(xmlPath=="HRSYS"){
				return this.Hrsys ;
			} else 
			if(xmlPath=="HRDIA"){
				return this.Hrdia ;
			} else 
			if(xmlPath=="HRPULSE"){
				return this.Hrpulse ;
			} else 
			if(xmlPath=="HRRESP"){
				return this.Hrresp ;
			} else 
			if(xmlPath=="COMP"){
				return this.Comp ;
			} else 
			if(xmlPath=="COMPOTH"){
				return this.Compoth ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="CSFCOLL"){
				this.Csfcoll=value;
			} else 
			if(xmlPath=="CSFINIT"){
				this.Csfinit=value;
			} else 
			if(xmlPath=="CSFDATE"){
				this.Csfdate=value;
			} else 
			if(xmlPath=="CSFFAST"){
				this.Csffast=value;
			} else 
			if(xmlPath=="PRETEMP"){
				this.Pretemp=value;
			} else 
			if(xmlPath=="PRETMPUNT"){
				this.Pretmpunt=value;
			} else 
			if(xmlPath=="PRESYS"){
				this.Presys=value;
			} else 
			if(xmlPath=="PREDIA"){
				this.Predia=value;
			} else 
			if(xmlPath=="PREPULSE"){
				this.Prepulse=value;
			} else 
			if(xmlPath=="PRERESP"){
				this.Preresp=value;
			} else 
			if(xmlPath=="NEEDLE"){
				this.Needle=value;
			} else 
			if(xmlPath=="NEEDLEOTH"){
				this.Needleoth=value;
			} else 
			if(xmlPath=="METHOD"){
				this.Method=value;
			} else 
			if(xmlPath=="LPSITE"){
				this.Lpsite=value;
			} else 
			if(xmlPath=="LPSITEOTH"){
				this.Lpsiteoth=value;
			} else 
			if(xmlPath=="POSITION2"){
				this.Position2=value;
			} else 
			if(xmlPath=="TUBECOLL"){
				this.Tubecoll=value;
			} else 
			if(xmlPath=="TUBESHIP"){
				this.Tubeship=value;
			} else 
			if(xmlPath=="STYRTIME"){
				this.Styrtime=value;
			} else 
			if(xmlPath=="CSFTIME"){
				this.Csftime=value;
			} else 
			if(xmlPath=="CSFTRANS"){
				this.Csftrans=value;
			} else 
			if(xmlPath=="VOLCOLL"){
				this.Volcoll=value;
			} else 
			if(xmlPath=="VOLTRANS"){
				this.Voltrans=value;
			} else 
			if(xmlPath=="LOCLAB"){
				this.Loclab=value;
			} else 
			if(xmlPath=="TIMEFROZ"){
				this.Timefroz=value;
			} else 
			if(xmlPath=="NOTES"){
				this.Notes=value;
			} else 
			if(xmlPath=="CSFSHIP"){
				this.Csfship=value;
			} else 
			if(xmlPath=="CSFSHDT"){
				this.Csfshdt=value;
			} else 
			if(xmlPath=="CSFSHNUM"){
				this.Csfshnum=value;
			} else 
			if(xmlPath=="CSFDATERVD"){
				this.Csfdatervd=value;
			} else 
			if(xmlPath=="CSFVOLRVD"){
				this.Csfvolrvd=value;
			} else 
			if(xmlPath=="CSFVIALSRVD"){
				this.Csfvialsrvd=value;
			} else 
			if(xmlPath=="POSITION3"){
				this.Position3=value;
			} else 
			if(xmlPath=="POSTTEMP"){
				this.Posttemp=value;
			} else 
			if(xmlPath=="POSTTMPU"){
				this.Posttmpu=value;
			} else 
			if(xmlPath=="POSTSYS"){
				this.Postsys=value;
			} else 
			if(xmlPath=="POSTDIA"){
				this.Postdia=value;
			} else 
			if(xmlPath=="POSTPULSE"){
				this.Postpulse=value;
			} else 
			if(xmlPath=="POSTRESP"){
				this.Postresp=value;
			} else 
			if(xmlPath=="POSITION4"){
				this.Position4=value;
			} else 
			if(xmlPath=="HRTEMP"){
				this.Hrtemp=value;
			} else 
			if(xmlPath=="HRTMPU"){
				this.Hrtmpu=value;
			} else 
			if(xmlPath=="HRSYS"){
				this.Hrsys=value;
			} else 
			if(xmlPath=="HRDIA"){
				this.Hrdia=value;
			} else 
			if(xmlPath=="HRPULSE"){
				this.Hrpulse=value;
			} else 
			if(xmlPath=="HRRESP"){
				this.Hrresp=value;
			} else 
			if(xmlPath=="COMP"){
				this.Comp=value;
			} else 
			if(xmlPath=="COMPOTH"){
				this.Compoth=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="CSFCOLL"){
			return "field_data";
		}else if (xmlPath=="CSFINIT"){
			return "field_data";
		}else if (xmlPath=="CSFDATE"){
			return "field_data";
		}else if (xmlPath=="CSFFAST"){
			return "field_data";
		}else if (xmlPath=="PRETEMP"){
			return "field_data";
		}else if (xmlPath=="PRETMPUNT"){
			return "field_data";
		}else if (xmlPath=="PRESYS"){
			return "field_data";
		}else if (xmlPath=="PREDIA"){
			return "field_data";
		}else if (xmlPath=="PREPULSE"){
			return "field_data";
		}else if (xmlPath=="PRERESP"){
			return "field_data";
		}else if (xmlPath=="NEEDLE"){
			return "field_data";
		}else if (xmlPath=="NEEDLEOTH"){
			return "field_data";
		}else if (xmlPath=="METHOD"){
			return "field_data";
		}else if (xmlPath=="LPSITE"){
			return "field_data";
		}else if (xmlPath=="LPSITEOTH"){
			return "field_LONG_DATA";
		}else if (xmlPath=="POSITION2"){
			return "field_data";
		}else if (xmlPath=="TUBECOLL"){
			return "field_data";
		}else if (xmlPath=="TUBESHIP"){
			return "field_data";
		}else if (xmlPath=="STYRTIME"){
			return "field_data";
		}else if (xmlPath=="CSFTIME"){
			return "field_data";
		}else if (xmlPath=="CSFTRANS"){
			return "field_data";
		}else if (xmlPath=="VOLCOLL"){
			return "field_data";
		}else if (xmlPath=="VOLTRANS"){
			return "field_data";
		}else if (xmlPath=="LOCLAB"){
			return "field_data";
		}else if (xmlPath=="TIMEFROZ"){
			return "field_data";
		}else if (xmlPath=="NOTES"){
			return "field_LONG_DATA";
		}else if (xmlPath=="CSFSHIP"){
			return "field_data";
		}else if (xmlPath=="CSFSHDT"){
			return "field_data";
		}else if (xmlPath=="CSFSHNUM"){
			return "field_data";
		}else if (xmlPath=="CSFDATERVD"){
			return "field_data";
		}else if (xmlPath=="CSFVOLRVD"){
			return "field_data";
		}else if (xmlPath=="CSFVIALSRVD"){
			return "field_data";
		}else if (xmlPath=="POSITION3"){
			return "field_data";
		}else if (xmlPath=="POSTTEMP"){
			return "field_data";
		}else if (xmlPath=="POSTTMPU"){
			return "field_data";
		}else if (xmlPath=="POSTSYS"){
			return "field_data";
		}else if (xmlPath=="POSTDIA"){
			return "field_data";
		}else if (xmlPath=="POSTPULSE"){
			return "field_data";
		}else if (xmlPath=="POSTRESP"){
			return "field_data";
		}else if (xmlPath=="POSITION4"){
			return "field_data";
		}else if (xmlPath=="HRTEMP"){
			return "field_data";
		}else if (xmlPath=="HRTMPU"){
			return "field_data";
		}else if (xmlPath=="HRSYS"){
			return "field_data";
		}else if (xmlPath=="HRDIA"){
			return "field_data";
		}else if (xmlPath=="HRPULSE"){
			return "field_data";
		}else if (xmlPath=="HRRESP"){
			return "field_data";
		}else if (xmlPath=="COMP"){
			return "field_data";
		}else if (xmlPath=="COMPOTH"){
			return "field_LONG_DATA";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<dian:CSFSAMP";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</dian:CSFSAMP>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Csfcoll!=null){
			xmlTxt+="\n<dian:CSFCOLL";
			xmlTxt+=">";
			xmlTxt+=this.Csfcoll;
			xmlTxt+="</dian:CSFCOLL>";
		}
		if (this.Csfinit!=null){
			xmlTxt+="\n<dian:CSFINIT";
			xmlTxt+=">";
			xmlTxt+=this.Csfinit.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:CSFINIT>";
		}
		if (this.Csfdate!=null){
			xmlTxt+="\n<dian:CSFDATE";
			xmlTxt+=">";
			xmlTxt+=this.Csfdate;
			xmlTxt+="</dian:CSFDATE>";
		}
		if (this.Csffast!=null){
			xmlTxt+="\n<dian:CSFFAST";
			xmlTxt+=">";
			xmlTxt+=this.Csffast;
			xmlTxt+="</dian:CSFFAST>";
		}
		if (this.Pretemp!=null){
			xmlTxt+="\n<dian:PRETEMP";
			xmlTxt+=">";
			xmlTxt+=this.Pretemp;
			xmlTxt+="</dian:PRETEMP>";
		}
		if (this.Pretmpunt!=null){
			xmlTxt+="\n<dian:PRETMPUNT";
			xmlTxt+=">";
			xmlTxt+=this.Pretmpunt;
			xmlTxt+="</dian:PRETMPUNT>";
		}
		if (this.Presys!=null){
			xmlTxt+="\n<dian:PRESYS";
			xmlTxt+=">";
			xmlTxt+=this.Presys;
			xmlTxt+="</dian:PRESYS>";
		}
		if (this.Predia!=null){
			xmlTxt+="\n<dian:PREDIA";
			xmlTxt+=">";
			xmlTxt+=this.Predia;
			xmlTxt+="</dian:PREDIA>";
		}
		if (this.Prepulse!=null){
			xmlTxt+="\n<dian:PREPULSE";
			xmlTxt+=">";
			xmlTxt+=this.Prepulse;
			xmlTxt+="</dian:PREPULSE>";
		}
		if (this.Preresp!=null){
			xmlTxt+="\n<dian:PRERESP";
			xmlTxt+=">";
			xmlTxt+=this.Preresp;
			xmlTxt+="</dian:PRERESP>";
		}
		if (this.Needle!=null){
			xmlTxt+="\n<dian:NEEDLE";
			xmlTxt+=">";
			xmlTxt+=this.Needle;
			xmlTxt+="</dian:NEEDLE>";
		}
		if (this.Needleoth!=null){
			xmlTxt+="\n<dian:NEEDLEOTH";
			xmlTxt+=">";
			xmlTxt+=this.Needleoth.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:NEEDLEOTH>";
		}
		if (this.Method!=null){
			xmlTxt+="\n<dian:METHOD";
			xmlTxt+=">";
			xmlTxt+=this.Method;
			xmlTxt+="</dian:METHOD>";
		}
		if (this.Lpsite!=null){
			xmlTxt+="\n<dian:LPSITE";
			xmlTxt+=">";
			xmlTxt+=this.Lpsite;
			xmlTxt+="</dian:LPSITE>";
		}
		if (this.Lpsiteoth!=null){
			xmlTxt+="\n<dian:LPSITEOTH";
			xmlTxt+=">";
			xmlTxt+=this.Lpsiteoth.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:LPSITEOTH>";
		}
		else{
			xmlTxt+="\n<dian:LPSITEOTH";
			xmlTxt+="/>";
		}

		if (this.Position2!=null){
			xmlTxt+="\n<dian:POSITION2";
			xmlTxt+=">";
			xmlTxt+=this.Position2;
			xmlTxt+="</dian:POSITION2>";
		}
		if (this.Tubecoll!=null){
			xmlTxt+="\n<dian:TUBECOLL";
			xmlTxt+=">";
			xmlTxt+=this.Tubecoll;
			xmlTxt+="</dian:TUBECOLL>";
		}
		if (this.Tubeship!=null){
			xmlTxt+="\n<dian:TUBESHIP";
			xmlTxt+=">";
			xmlTxt+=this.Tubeship;
			xmlTxt+="</dian:TUBESHIP>";
		}
		if (this.Styrtime!=null){
			xmlTxt+="\n<dian:STYRTIME";
			xmlTxt+=">";
			xmlTxt+=this.Styrtime;
			xmlTxt+="</dian:STYRTIME>";
		}
		if (this.Csftime!=null){
			xmlTxt+="\n<dian:CSFTIME";
			xmlTxt+=">";
			xmlTxt+=this.Csftime;
			xmlTxt+="</dian:CSFTIME>";
		}
		if (this.Csftrans!=null){
			xmlTxt+="\n<dian:CSFTRANS";
			xmlTxt+=">";
			xmlTxt+=this.Csftrans.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:CSFTRANS>";
		}
		if (this.Volcoll!=null){
			xmlTxt+="\n<dian:VOLCOLL";
			xmlTxt+=">";
			xmlTxt+=this.Volcoll;
			xmlTxt+="</dian:VOLCOLL>";
		}
		if (this.Voltrans!=null){
			xmlTxt+="\n<dian:VOLTRANS";
			xmlTxt+=">";
			xmlTxt+=this.Voltrans;
			xmlTxt+="</dian:VOLTRANS>";
		}
		if (this.Loclab!=null){
			xmlTxt+="\n<dian:LOCLAB";
			xmlTxt+=">";
			xmlTxt+=this.Loclab;
			xmlTxt+="</dian:LOCLAB>";
		}
		if (this.Timefroz!=null){
			xmlTxt+="\n<dian:TIMEFROZ";
			xmlTxt+=">";
			xmlTxt+=this.Timefroz.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:TIMEFROZ>";
		}
		if (this.Notes!=null){
			xmlTxt+="\n<dian:NOTES";
			xmlTxt+=">";
			xmlTxt+=this.Notes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:NOTES>";
		}
		else{
			xmlTxt+="\n<dian:NOTES";
			xmlTxt+="/>";
		}

		if (this.Csfship!=null){
			xmlTxt+="\n<dian:CSFSHIP";
			xmlTxt+=">";
			xmlTxt+=this.Csfship;
			xmlTxt+="</dian:CSFSHIP>";
		}
		if (this.Csfshdt!=null){
			xmlTxt+="\n<dian:CSFSHDT";
			xmlTxt+=">";
			xmlTxt+=this.Csfshdt;
			xmlTxt+="</dian:CSFSHDT>";
		}
		if (this.Csfshnum!=null){
			xmlTxt+="\n<dian:CSFSHNUM";
			xmlTxt+=">";
			xmlTxt+=this.Csfshnum.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:CSFSHNUM>";
		}
		if (this.Csfdatervd!=null){
			xmlTxt+="\n<dian:CSFDATERVD";
			xmlTxt+=">";
			xmlTxt+=this.Csfdatervd;
			xmlTxt+="</dian:CSFDATERVD>";
		}
		if (this.Csfvolrvd!=null){
			xmlTxt+="\n<dian:CSFVOLRVD";
			xmlTxt+=">";
			xmlTxt+=this.Csfvolrvd;
			xmlTxt+="</dian:CSFVOLRVD>";
		}
		if (this.Csfvialsrvd!=null){
			xmlTxt+="\n<dian:CSFVIALSRVD";
			xmlTxt+=">";
			xmlTxt+=this.Csfvialsrvd;
			xmlTxt+="</dian:CSFVIALSRVD>";
		}
		if (this.Position3!=null){
			xmlTxt+="\n<dian:POSITION3";
			xmlTxt+=">";
			xmlTxt+=this.Position3;
			xmlTxt+="</dian:POSITION3>";
		}
		if (this.Posttemp!=null){
			xmlTxt+="\n<dian:POSTTEMP";
			xmlTxt+=">";
			xmlTxt+=this.Posttemp;
			xmlTxt+="</dian:POSTTEMP>";
		}
		if (this.Posttmpu!=null){
			xmlTxt+="\n<dian:POSTTMPU";
			xmlTxt+=">";
			xmlTxt+=this.Posttmpu;
			xmlTxt+="</dian:POSTTMPU>";
		}
		if (this.Postsys!=null){
			xmlTxt+="\n<dian:POSTSYS";
			xmlTxt+=">";
			xmlTxt+=this.Postsys;
			xmlTxt+="</dian:POSTSYS>";
		}
		if (this.Postdia!=null){
			xmlTxt+="\n<dian:POSTDIA";
			xmlTxt+=">";
			xmlTxt+=this.Postdia;
			xmlTxt+="</dian:POSTDIA>";
		}
		if (this.Postpulse!=null){
			xmlTxt+="\n<dian:POSTPULSE";
			xmlTxt+=">";
			xmlTxt+=this.Postpulse;
			xmlTxt+="</dian:POSTPULSE>";
		}
		if (this.Postresp!=null){
			xmlTxt+="\n<dian:POSTRESP";
			xmlTxt+=">";
			xmlTxt+=this.Postresp;
			xmlTxt+="</dian:POSTRESP>";
		}
		if (this.Position4!=null){
			xmlTxt+="\n<dian:POSITION4";
			xmlTxt+=">";
			xmlTxt+=this.Position4;
			xmlTxt+="</dian:POSITION4>";
		}
		if (this.Hrtemp!=null){
			xmlTxt+="\n<dian:HRTEMP";
			xmlTxt+=">";
			xmlTxt+=this.Hrtemp;
			xmlTxt+="</dian:HRTEMP>";
		}
		if (this.Hrtmpu!=null){
			xmlTxt+="\n<dian:HRTMPU";
			xmlTxt+=">";
			xmlTxt+=this.Hrtmpu;
			xmlTxt+="</dian:HRTMPU>";
		}
		if (this.Hrsys!=null){
			xmlTxt+="\n<dian:HRSYS";
			xmlTxt+=">";
			xmlTxt+=this.Hrsys;
			xmlTxt+="</dian:HRSYS>";
		}
		if (this.Hrdia!=null){
			xmlTxt+="\n<dian:HRDIA";
			xmlTxt+=">";
			xmlTxt+=this.Hrdia;
			xmlTxt+="</dian:HRDIA>";
		}
		if (this.Hrpulse!=null){
			xmlTxt+="\n<dian:HRPULSE";
			xmlTxt+=">";
			xmlTxt+=this.Hrpulse;
			xmlTxt+="</dian:HRPULSE>";
		}
		if (this.Hrresp!=null){
			xmlTxt+="\n<dian:HRRESP";
			xmlTxt+=">";
			xmlTxt+=this.Hrresp;
			xmlTxt+="</dian:HRRESP>";
		}
		if (this.Comp!=null){
			xmlTxt+="\n<dian:COMP";
			xmlTxt+=">";
			xmlTxt+=this.Comp;
			xmlTxt+="</dian:COMP>";
		}
		if (this.Compoth!=null){
			xmlTxt+="\n<dian:COMPOTH";
			xmlTxt+=">";
			xmlTxt+=this.Compoth.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:COMPOTH>";
		}
		else{
			xmlTxt+="\n<dian:COMPOTH";
			xmlTxt+="/>";
		}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Csfcoll!=null) return true;
		if (this.Csfinit!=null) return true;
		if (this.Csfdate!=null) return true;
		if (this.Csffast!=null) return true;
		if (this.Pretemp!=null) return true;
		if (this.Pretmpunt!=null) return true;
		if (this.Presys!=null) return true;
		if (this.Predia!=null) return true;
		if (this.Prepulse!=null) return true;
		if (this.Preresp!=null) return true;
		if (this.Needle!=null) return true;
		if (this.Needleoth!=null) return true;
		if (this.Method!=null) return true;
		if (this.Lpsite!=null) return true;
		if (this.Lpsiteoth!=null) return true;
		return true;//REQUIRED LPSITEOTH
	}
}
