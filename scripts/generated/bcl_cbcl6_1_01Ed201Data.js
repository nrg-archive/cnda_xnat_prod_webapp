/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function bcl_cbcl6_1_01Ed201Data(){
this.xsiType="bcl:cbcl6-1-01Ed201Data";

	this.getSchemaElementName=function(){
		return "cbcl6-1-01Ed201Data";
	}

	this.getFullSchemaElementName=function(){
		return "bcl:cbcl6-1-01Ed201Data";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.AnxdpRaw=null;


	function getAnxdpRaw() {
		return this.AnxdpRaw;
	}
	this.getAnxdpRaw=getAnxdpRaw;


	function setAnxdpRaw(v){
		this.AnxdpRaw=v;
	}
	this.setAnxdpRaw=setAnxdpRaw;

	this.AnxdpT=null;


	function getAnxdpT() {
		return this.AnxdpT;
	}
	this.getAnxdpT=getAnxdpT;


	function setAnxdpT(v){
		this.AnxdpT=v;
	}
	this.setAnxdpT=setAnxdpT;

	this.AnxdpNote=null;


	function getAnxdpNote() {
		return this.AnxdpNote;
	}
	this.getAnxdpNote=getAnxdpNote;


	function setAnxdpNote(v){
		this.AnxdpNote=v;
	}
	this.setAnxdpNote=setAnxdpNote;

	this.WthdpRaw=null;


	function getWthdpRaw() {
		return this.WthdpRaw;
	}
	this.getWthdpRaw=getWthdpRaw;


	function setWthdpRaw(v){
		this.WthdpRaw=v;
	}
	this.setWthdpRaw=setWthdpRaw;

	this.WthdpT=null;


	function getWthdpT() {
		return this.WthdpT;
	}
	this.getWthdpT=getWthdpT;


	function setWthdpT(v){
		this.WthdpT=v;
	}
	this.setWthdpT=setWthdpT;

	this.WthdpNote=null;


	function getWthdpNote() {
		return this.WthdpNote;
	}
	this.getWthdpNote=getWthdpNote;


	function setWthdpNote(v){
		this.WthdpNote=v;
	}
	this.setWthdpNote=setWthdpNote;

	this.SomRaw=null;


	function getSomRaw() {
		return this.SomRaw;
	}
	this.getSomRaw=getSomRaw;


	function setSomRaw(v){
		this.SomRaw=v;
	}
	this.setSomRaw=setSomRaw;

	this.SomT=null;


	function getSomT() {
		return this.SomT;
	}
	this.getSomT=getSomT;


	function setSomT(v){
		this.SomT=v;
	}
	this.setSomT=setSomT;

	this.SomNote=null;


	function getSomNote() {
		return this.SomNote;
	}
	this.getSomNote=getSomNote;


	function setSomNote(v){
		this.SomNote=v;
	}
	this.setSomNote=setSomNote;

	this.SocRaw=null;


	function getSocRaw() {
		return this.SocRaw;
	}
	this.getSocRaw=getSocRaw;


	function setSocRaw(v){
		this.SocRaw=v;
	}
	this.setSocRaw=setSocRaw;

	this.SocT=null;


	function getSocT() {
		return this.SocT;
	}
	this.getSocT=getSocT;


	function setSocT(v){
		this.SocT=v;
	}
	this.setSocT=setSocT;

	this.SocNote=null;


	function getSocNote() {
		return this.SocNote;
	}
	this.getSocNote=getSocNote;


	function setSocNote(v){
		this.SocNote=v;
	}
	this.setSocNote=setSocNote;

	this.ThoRaw=null;


	function getThoRaw() {
		return this.ThoRaw;
	}
	this.getThoRaw=getThoRaw;


	function setThoRaw(v){
		this.ThoRaw=v;
	}
	this.setThoRaw=setThoRaw;

	this.ThoT=null;


	function getThoT() {
		return this.ThoT;
	}
	this.getThoT=getThoT;


	function setThoT(v){
		this.ThoT=v;
	}
	this.setThoT=setThoT;

	this.ThoNote=null;


	function getThoNote() {
		return this.ThoNote;
	}
	this.getThoNote=getThoNote;


	function setThoNote(v){
		this.ThoNote=v;
	}
	this.setThoNote=setThoNote;

	this.AttRaw=null;


	function getAttRaw() {
		return this.AttRaw;
	}
	this.getAttRaw=getAttRaw;


	function setAttRaw(v){
		this.AttRaw=v;
	}
	this.setAttRaw=setAttRaw;

	this.AttT=null;


	function getAttT() {
		return this.AttT;
	}
	this.getAttT=getAttT;


	function setAttT(v){
		this.AttT=v;
	}
	this.setAttT=setAttT;

	this.AttNote=null;


	function getAttNote() {
		return this.AttNote;
	}
	this.getAttNote=getAttNote;


	function setAttNote(v){
		this.AttNote=v;
	}
	this.setAttNote=setAttNote;

	this.RuleRaw=null;


	function getRuleRaw() {
		return this.RuleRaw;
	}
	this.getRuleRaw=getRuleRaw;


	function setRuleRaw(v){
		this.RuleRaw=v;
	}
	this.setRuleRaw=setRuleRaw;

	this.RuleT=null;


	function getRuleT() {
		return this.RuleT;
	}
	this.getRuleT=getRuleT;


	function setRuleT(v){
		this.RuleT=v;
	}
	this.setRuleT=setRuleT;

	this.RuleNote=null;


	function getRuleNote() {
		return this.RuleNote;
	}
	this.getRuleNote=getRuleNote;


	function setRuleNote(v){
		this.RuleNote=v;
	}
	this.setRuleNote=setRuleNote;

	this.AggRaw=null;


	function getAggRaw() {
		return this.AggRaw;
	}
	this.getAggRaw=getAggRaw;


	function setAggRaw(v){
		this.AggRaw=v;
	}
	this.setAggRaw=setAggRaw;

	this.AggT=null;


	function getAggT() {
		return this.AggT;
	}
	this.getAggT=getAggT;


	function setAggT(v){
		this.AggT=v;
	}
	this.setAggT=setAggT;

	this.AggNote=null;


	function getAggNote() {
		return this.AggNote;
	}
	this.getAggNote=getAggNote;


	function setAggNote(v){
		this.AggNote=v;
	}
	this.setAggNote=setAggNote;

	this.Scoreform_resprltnsubj=null;


	function getScoreform_resprltnsubj() {
		return this.Scoreform_resprltnsubj;
	}
	this.getScoreform_resprltnsubj=getScoreform_resprltnsubj;


	function setScoreform_resprltnsubj(v){
		this.Scoreform_resprltnsubj=v;
	}
	this.setScoreform_resprltnsubj=setScoreform_resprltnsubj;

	this.Scoreform_respgender=null;


	function getScoreform_respgender() {
		return this.Scoreform_respgender;
	}
	this.getScoreform_respgender=getScoreform_respgender;


	function setScoreform_respgender(v){
		this.Scoreform_respgender=v;
	}
	this.setScoreform_respgender=setScoreform_respgender;

	this.Scoreform_fatherwork=null;


	function getScoreform_fatherwork() {
		return this.Scoreform_fatherwork;
	}
	this.getScoreform_fatherwork=getScoreform_fatherwork;


	function setScoreform_fatherwork(v){
		this.Scoreform_fatherwork=v;
	}
	this.setScoreform_fatherwork=setScoreform_fatherwork;

	this.Scoreform_motherwork=null;


	function getScoreform_motherwork() {
		return this.Scoreform_motherwork;
	}
	this.getScoreform_motherwork=getScoreform_motherwork;


	function setScoreform_motherwork(v){
		this.Scoreform_motherwork=v;
	}
	this.setScoreform_motherwork=setScoreform_motherwork;

	this.Scoreform_sectioni_sportstopica=null;


	function getScoreform_sectioni_sportstopica() {
		return this.Scoreform_sectioni_sportstopica;
	}
	this.getScoreform_sectioni_sportstopica=getScoreform_sectioni_sportstopica;


	function setScoreform_sectioni_sportstopica(v){
		this.Scoreform_sectioni_sportstopica=v;
	}
	this.setScoreform_sectioni_sportstopica=setScoreform_sectioni_sportstopica;

	this.Scoreform_sectioni_sportstopicb=null;


	function getScoreform_sectioni_sportstopicb() {
		return this.Scoreform_sectioni_sportstopicb;
	}
	this.getScoreform_sectioni_sportstopicb=getScoreform_sectioni_sportstopicb;


	function setScoreform_sectioni_sportstopicb(v){
		this.Scoreform_sectioni_sportstopicb=v;
	}
	this.setScoreform_sectioni_sportstopicb=setScoreform_sectioni_sportstopicb;

	this.Scoreform_sectioni_sportstopicc=null;


	function getScoreform_sectioni_sportstopicc() {
		return this.Scoreform_sectioni_sportstopicc;
	}
	this.getScoreform_sectioni_sportstopicc=getScoreform_sectioni_sportstopicc;


	function setScoreform_sectioni_sportstopicc(v){
		this.Scoreform_sectioni_sportstopicc=v;
	}
	this.setScoreform_sectioni_sportstopicc=setScoreform_sectioni_sportstopicc;

	this.Scoreform_sectioni_sportstimea=null;


	function getScoreform_sectioni_sportstimea() {
		return this.Scoreform_sectioni_sportstimea;
	}
	this.getScoreform_sectioni_sportstimea=getScoreform_sectioni_sportstimea;


	function setScoreform_sectioni_sportstimea(v){
		this.Scoreform_sectioni_sportstimea=v;
	}
	this.setScoreform_sectioni_sportstimea=setScoreform_sectioni_sportstimea;

	this.Scoreform_sectioni_sportstimeb=null;


	function getScoreform_sectioni_sportstimeb() {
		return this.Scoreform_sectioni_sportstimeb;
	}
	this.getScoreform_sectioni_sportstimeb=getScoreform_sectioni_sportstimeb;


	function setScoreform_sectioni_sportstimeb(v){
		this.Scoreform_sectioni_sportstimeb=v;
	}
	this.setScoreform_sectioni_sportstimeb=setScoreform_sectioni_sportstimeb;

	this.Scoreform_sectioni_sportstimec=null;


	function getScoreform_sectioni_sportstimec() {
		return this.Scoreform_sectioni_sportstimec;
	}
	this.getScoreform_sectioni_sportstimec=getScoreform_sectioni_sportstimec;


	function setScoreform_sectioni_sportstimec(v){
		this.Scoreform_sectioni_sportstimec=v;
	}
	this.setScoreform_sectioni_sportstimec=setScoreform_sectioni_sportstimec;

	this.Scoreform_sectioni_sportsdowella=null;


	function getScoreform_sectioni_sportsdowella() {
		return this.Scoreform_sectioni_sportsdowella;
	}
	this.getScoreform_sectioni_sportsdowella=getScoreform_sectioni_sportsdowella;


	function setScoreform_sectioni_sportsdowella(v){
		this.Scoreform_sectioni_sportsdowella=v;
	}
	this.setScoreform_sectioni_sportsdowella=setScoreform_sectioni_sportsdowella;

	this.Scoreform_sectioni_sportsdowellb=null;


	function getScoreform_sectioni_sportsdowellb() {
		return this.Scoreform_sectioni_sportsdowellb;
	}
	this.getScoreform_sectioni_sportsdowellb=getScoreform_sectioni_sportsdowellb;


	function setScoreform_sectioni_sportsdowellb(v){
		this.Scoreform_sectioni_sportsdowellb=v;
	}
	this.setScoreform_sectioni_sportsdowellb=setScoreform_sectioni_sportsdowellb;

	this.Scoreform_sectioni_sportsdowellc=null;


	function getScoreform_sectioni_sportsdowellc() {
		return this.Scoreform_sectioni_sportsdowellc;
	}
	this.getScoreform_sectioni_sportsdowellc=getScoreform_sectioni_sportsdowellc;


	function setScoreform_sectioni_sportsdowellc(v){
		this.Scoreform_sectioni_sportsdowellc=v;
	}
	this.setScoreform_sectioni_sportsdowellc=setScoreform_sectioni_sportsdowellc;

	this.Scoreform_sectionii_hobbiestopica=null;


	function getScoreform_sectionii_hobbiestopica() {
		return this.Scoreform_sectionii_hobbiestopica;
	}
	this.getScoreform_sectionii_hobbiestopica=getScoreform_sectionii_hobbiestopica;


	function setScoreform_sectionii_hobbiestopica(v){
		this.Scoreform_sectionii_hobbiestopica=v;
	}
	this.setScoreform_sectionii_hobbiestopica=setScoreform_sectionii_hobbiestopica;

	this.Scoreform_sectionii_hobbiestopicb=null;


	function getScoreform_sectionii_hobbiestopicb() {
		return this.Scoreform_sectionii_hobbiestopicb;
	}
	this.getScoreform_sectionii_hobbiestopicb=getScoreform_sectionii_hobbiestopicb;


	function setScoreform_sectionii_hobbiestopicb(v){
		this.Scoreform_sectionii_hobbiestopicb=v;
	}
	this.setScoreform_sectionii_hobbiestopicb=setScoreform_sectionii_hobbiestopicb;

	this.Scoreform_sectionii_hobbiestopicc=null;


	function getScoreform_sectionii_hobbiestopicc() {
		return this.Scoreform_sectionii_hobbiestopicc;
	}
	this.getScoreform_sectionii_hobbiestopicc=getScoreform_sectionii_hobbiestopicc;


	function setScoreform_sectionii_hobbiestopicc(v){
		this.Scoreform_sectionii_hobbiestopicc=v;
	}
	this.setScoreform_sectionii_hobbiestopicc=setScoreform_sectionii_hobbiestopicc;

	this.Scoreform_sectionii_hobbiestimea=null;


	function getScoreform_sectionii_hobbiestimea() {
		return this.Scoreform_sectionii_hobbiestimea;
	}
	this.getScoreform_sectionii_hobbiestimea=getScoreform_sectionii_hobbiestimea;


	function setScoreform_sectionii_hobbiestimea(v){
		this.Scoreform_sectionii_hobbiestimea=v;
	}
	this.setScoreform_sectionii_hobbiestimea=setScoreform_sectionii_hobbiestimea;

	this.Scoreform_sectionii_hobbiestimeb=null;


	function getScoreform_sectionii_hobbiestimeb() {
		return this.Scoreform_sectionii_hobbiestimeb;
	}
	this.getScoreform_sectionii_hobbiestimeb=getScoreform_sectionii_hobbiestimeb;


	function setScoreform_sectionii_hobbiestimeb(v){
		this.Scoreform_sectionii_hobbiestimeb=v;
	}
	this.setScoreform_sectionii_hobbiestimeb=setScoreform_sectionii_hobbiestimeb;

	this.Scoreform_sectionii_hobbiestimec=null;


	function getScoreform_sectionii_hobbiestimec() {
		return this.Scoreform_sectionii_hobbiestimec;
	}
	this.getScoreform_sectionii_hobbiestimec=getScoreform_sectionii_hobbiestimec;


	function setScoreform_sectionii_hobbiestimec(v){
		this.Scoreform_sectionii_hobbiestimec=v;
	}
	this.setScoreform_sectionii_hobbiestimec=setScoreform_sectionii_hobbiestimec;

	this.Scoreform_sectionii_hobbiesdowella=null;


	function getScoreform_sectionii_hobbiesdowella() {
		return this.Scoreform_sectionii_hobbiesdowella;
	}
	this.getScoreform_sectionii_hobbiesdowella=getScoreform_sectionii_hobbiesdowella;


	function setScoreform_sectionii_hobbiesdowella(v){
		this.Scoreform_sectionii_hobbiesdowella=v;
	}
	this.setScoreform_sectionii_hobbiesdowella=setScoreform_sectionii_hobbiesdowella;

	this.Scoreform_sectionii_hobbiesdowellb=null;


	function getScoreform_sectionii_hobbiesdowellb() {
		return this.Scoreform_sectionii_hobbiesdowellb;
	}
	this.getScoreform_sectionii_hobbiesdowellb=getScoreform_sectionii_hobbiesdowellb;


	function setScoreform_sectionii_hobbiesdowellb(v){
		this.Scoreform_sectionii_hobbiesdowellb=v;
	}
	this.setScoreform_sectionii_hobbiesdowellb=setScoreform_sectionii_hobbiesdowellb;

	this.Scoreform_sectionii_hobbiesdowellc=null;


	function getScoreform_sectionii_hobbiesdowellc() {
		return this.Scoreform_sectionii_hobbiesdowellc;
	}
	this.getScoreform_sectionii_hobbiesdowellc=getScoreform_sectionii_hobbiesdowellc;


	function setScoreform_sectionii_hobbiesdowellc(v){
		this.Scoreform_sectionii_hobbiesdowellc=v;
	}
	this.setScoreform_sectionii_hobbiesdowellc=setScoreform_sectionii_hobbiesdowellc;

	this.Scoreform_sectioniii_orgstopica=null;


	function getScoreform_sectioniii_orgstopica() {
		return this.Scoreform_sectioniii_orgstopica;
	}
	this.getScoreform_sectioniii_orgstopica=getScoreform_sectioniii_orgstopica;


	function setScoreform_sectioniii_orgstopica(v){
		this.Scoreform_sectioniii_orgstopica=v;
	}
	this.setScoreform_sectioniii_orgstopica=setScoreform_sectioniii_orgstopica;

	this.Scoreform_sectioniii_orgstopicb=null;


	function getScoreform_sectioniii_orgstopicb() {
		return this.Scoreform_sectioniii_orgstopicb;
	}
	this.getScoreform_sectioniii_orgstopicb=getScoreform_sectioniii_orgstopicb;


	function setScoreform_sectioniii_orgstopicb(v){
		this.Scoreform_sectioniii_orgstopicb=v;
	}
	this.setScoreform_sectioniii_orgstopicb=setScoreform_sectioniii_orgstopicb;

	this.Scoreform_sectioniii_orgstopicc=null;


	function getScoreform_sectioniii_orgstopicc() {
		return this.Scoreform_sectioniii_orgstopicc;
	}
	this.getScoreform_sectioniii_orgstopicc=getScoreform_sectioniii_orgstopicc;


	function setScoreform_sectioniii_orgstopicc(v){
		this.Scoreform_sectioniii_orgstopicc=v;
	}
	this.setScoreform_sectioniii_orgstopicc=setScoreform_sectioniii_orgstopicc;

	this.Scoreform_sectioniii_orgsactivea=null;


	function getScoreform_sectioniii_orgsactivea() {
		return this.Scoreform_sectioniii_orgsactivea;
	}
	this.getScoreform_sectioniii_orgsactivea=getScoreform_sectioniii_orgsactivea;


	function setScoreform_sectioniii_orgsactivea(v){
		this.Scoreform_sectioniii_orgsactivea=v;
	}
	this.setScoreform_sectioniii_orgsactivea=setScoreform_sectioniii_orgsactivea;

	this.Scoreform_sectioniii_orgsactiveb=null;


	function getScoreform_sectioniii_orgsactiveb() {
		return this.Scoreform_sectioniii_orgsactiveb;
	}
	this.getScoreform_sectioniii_orgsactiveb=getScoreform_sectioniii_orgsactiveb;


	function setScoreform_sectioniii_orgsactiveb(v){
		this.Scoreform_sectioniii_orgsactiveb=v;
	}
	this.setScoreform_sectioniii_orgsactiveb=setScoreform_sectioniii_orgsactiveb;

	this.Scoreform_sectioniii_orgsactivec=null;


	function getScoreform_sectioniii_orgsactivec() {
		return this.Scoreform_sectioniii_orgsactivec;
	}
	this.getScoreform_sectioniii_orgsactivec=getScoreform_sectioniii_orgsactivec;


	function setScoreform_sectioniii_orgsactivec(v){
		this.Scoreform_sectioniii_orgsactivec=v;
	}
	this.setScoreform_sectioniii_orgsactivec=setScoreform_sectioniii_orgsactivec;

	this.Scoreform_sectioniv_jobstopica=null;


	function getScoreform_sectioniv_jobstopica() {
		return this.Scoreform_sectioniv_jobstopica;
	}
	this.getScoreform_sectioniv_jobstopica=getScoreform_sectioniv_jobstopica;


	function setScoreform_sectioniv_jobstopica(v){
		this.Scoreform_sectioniv_jobstopica=v;
	}
	this.setScoreform_sectioniv_jobstopica=setScoreform_sectioniv_jobstopica;

	this.Scoreform_sectioniv_jobstopicb=null;


	function getScoreform_sectioniv_jobstopicb() {
		return this.Scoreform_sectioniv_jobstopicb;
	}
	this.getScoreform_sectioniv_jobstopicb=getScoreform_sectioniv_jobstopicb;


	function setScoreform_sectioniv_jobstopicb(v){
		this.Scoreform_sectioniv_jobstopicb=v;
	}
	this.setScoreform_sectioniv_jobstopicb=setScoreform_sectioniv_jobstopicb;

	this.Scoreform_sectioniv_jobstopicc=null;


	function getScoreform_sectioniv_jobstopicc() {
		return this.Scoreform_sectioniv_jobstopicc;
	}
	this.getScoreform_sectioniv_jobstopicc=getScoreform_sectioniv_jobstopicc;


	function setScoreform_sectioniv_jobstopicc(v){
		this.Scoreform_sectioniv_jobstopicc=v;
	}
	this.setScoreform_sectioniv_jobstopicc=setScoreform_sectioniv_jobstopicc;

	this.Scoreform_sectioniv_jobswella=null;


	function getScoreform_sectioniv_jobswella() {
		return this.Scoreform_sectioniv_jobswella;
	}
	this.getScoreform_sectioniv_jobswella=getScoreform_sectioniv_jobswella;


	function setScoreform_sectioniv_jobswella(v){
		this.Scoreform_sectioniv_jobswella=v;
	}
	this.setScoreform_sectioniv_jobswella=setScoreform_sectioniv_jobswella;

	this.Scoreform_sectioniv_jobswellb=null;


	function getScoreform_sectioniv_jobswellb() {
		return this.Scoreform_sectioniv_jobswellb;
	}
	this.getScoreform_sectioniv_jobswellb=getScoreform_sectioniv_jobswellb;


	function setScoreform_sectioniv_jobswellb(v){
		this.Scoreform_sectioniv_jobswellb=v;
	}
	this.setScoreform_sectioniv_jobswellb=setScoreform_sectioniv_jobswellb;

	this.Scoreform_sectioniv_jobswellc=null;


	function getScoreform_sectioniv_jobswellc() {
		return this.Scoreform_sectioniv_jobswellc;
	}
	this.getScoreform_sectioniv_jobswellc=getScoreform_sectioniv_jobswellc;


	function setScoreform_sectioniv_jobswellc(v){
		this.Scoreform_sectioniv_jobswellc=v;
	}
	this.setScoreform_sectioniv_jobswellc=setScoreform_sectioniv_jobswellc;

	this.Scoreform_sectionv_closefriends=null;


	function getScoreform_sectionv_closefriends() {
		return this.Scoreform_sectionv_closefriends;
	}
	this.getScoreform_sectionv_closefriends=getScoreform_sectionv_closefriends;


	function setScoreform_sectionv_closefriends(v){
		this.Scoreform_sectionv_closefriends=v;
	}
	this.setScoreform_sectionv_closefriends=setScoreform_sectionv_closefriends;

	this.Scoreform_sectionv_friendtimes=null;


	function getScoreform_sectionv_friendtimes() {
		return this.Scoreform_sectionv_friendtimes;
	}
	this.getScoreform_sectionv_friendtimes=getScoreform_sectionv_friendtimes;


	function setScoreform_sectionv_friendtimes(v){
		this.Scoreform_sectionv_friendtimes=v;
	}
	this.setScoreform_sectionv_friendtimes=setScoreform_sectionv_friendtimes;

	this.Scoreform_sectionvi_getalonga=null;


	function getScoreform_sectionvi_getalonga() {
		return this.Scoreform_sectionvi_getalonga;
	}
	this.getScoreform_sectionvi_getalonga=getScoreform_sectionvi_getalonga;


	function setScoreform_sectionvi_getalonga(v){
		this.Scoreform_sectionvi_getalonga=v;
	}
	this.setScoreform_sectionvi_getalonga=setScoreform_sectionvi_getalonga;

	this.Scoreform_sectionvi_getalongb=null;


	function getScoreform_sectionvi_getalongb() {
		return this.Scoreform_sectionvi_getalongb;
	}
	this.getScoreform_sectionvi_getalongb=getScoreform_sectionvi_getalongb;


	function setScoreform_sectionvi_getalongb(v){
		this.Scoreform_sectionvi_getalongb=v;
	}
	this.setScoreform_sectionvi_getalongb=setScoreform_sectionvi_getalongb;

	this.Scoreform_sectionvi_getalongc=null;


	function getScoreform_sectionvi_getalongc() {
		return this.Scoreform_sectionvi_getalongc;
	}
	this.getScoreform_sectionvi_getalongc=getScoreform_sectionvi_getalongc;


	function setScoreform_sectionvi_getalongc(v){
		this.Scoreform_sectionvi_getalongc=v;
	}
	this.setScoreform_sectionvi_getalongc=setScoreform_sectionvi_getalongc;

	this.Scoreform_sectionvi_getalongd=null;


	function getScoreform_sectionvi_getalongd() {
		return this.Scoreform_sectionvi_getalongd;
	}
	this.getScoreform_sectionvi_getalongd=getScoreform_sectionvi_getalongd;


	function setScoreform_sectionvi_getalongd(v){
		this.Scoreform_sectionvi_getalongd=v;
	}
	this.setScoreform_sectionvi_getalongd=setScoreform_sectionvi_getalongd;

	this.Scoreform_sectionvii_academperfnoschool=null;


	function getScoreform_sectionvii_academperfnoschool() {
		return this.Scoreform_sectionvii_academperfnoschool;
	}
	this.getScoreform_sectionvii_academperfnoschool=getScoreform_sectionvii_academperfnoschool;


	function setScoreform_sectionvii_academperfnoschool(v){
		this.Scoreform_sectionvii_academperfnoschool=v;
	}
	this.setScoreform_sectionvii_academperfnoschool=setScoreform_sectionvii_academperfnoschool;

	this.Scoreform_sectionvii_academperfa=null;


	function getScoreform_sectionvii_academperfa() {
		return this.Scoreform_sectionvii_academperfa;
	}
	this.getScoreform_sectionvii_academperfa=getScoreform_sectionvii_academperfa;


	function setScoreform_sectionvii_academperfa(v){
		this.Scoreform_sectionvii_academperfa=v;
	}
	this.setScoreform_sectionvii_academperfa=setScoreform_sectionvii_academperfa;

	this.Scoreform_sectionvii_academperfb=null;


	function getScoreform_sectionvii_academperfb() {
		return this.Scoreform_sectionvii_academperfb;
	}
	this.getScoreform_sectionvii_academperfb=getScoreform_sectionvii_academperfb;


	function setScoreform_sectionvii_academperfb(v){
		this.Scoreform_sectionvii_academperfb=v;
	}
	this.setScoreform_sectionvii_academperfb=setScoreform_sectionvii_academperfb;

	this.Scoreform_sectionvii_academperfc=null;


	function getScoreform_sectionvii_academperfc() {
		return this.Scoreform_sectionvii_academperfc;
	}
	this.getScoreform_sectionvii_academperfc=getScoreform_sectionvii_academperfc;


	function setScoreform_sectionvii_academperfc(v){
		this.Scoreform_sectionvii_academperfc=v;
	}
	this.setScoreform_sectionvii_academperfc=setScoreform_sectionvii_academperfc;

	this.Scoreform_sectionvii_academperfd=null;


	function getScoreform_sectionvii_academperfd() {
		return this.Scoreform_sectionvii_academperfd;
	}
	this.getScoreform_sectionvii_academperfd=getScoreform_sectionvii_academperfd;


	function setScoreform_sectionvii_academperfd(v){
		this.Scoreform_sectionvii_academperfd=v;
	}
	this.setScoreform_sectionvii_academperfd=setScoreform_sectionvii_academperfd;

	this.Scoreform_sectionvii_academperfetopic=null;


	function getScoreform_sectionvii_academperfetopic() {
		return this.Scoreform_sectionvii_academperfetopic;
	}
	this.getScoreform_sectionvii_academperfetopic=getScoreform_sectionvii_academperfetopic;


	function setScoreform_sectionvii_academperfetopic(v){
		this.Scoreform_sectionvii_academperfetopic=v;
	}
	this.setScoreform_sectionvii_academperfetopic=setScoreform_sectionvii_academperfetopic;

	this.Scoreform_sectionvii_academperfe=null;


	function getScoreform_sectionvii_academperfe() {
		return this.Scoreform_sectionvii_academperfe;
	}
	this.getScoreform_sectionvii_academperfe=getScoreform_sectionvii_academperfe;


	function setScoreform_sectionvii_academperfe(v){
		this.Scoreform_sectionvii_academperfe=v;
	}
	this.setScoreform_sectionvii_academperfe=setScoreform_sectionvii_academperfe;

	this.Scoreform_sectionvii_academperfftopic=null;


	function getScoreform_sectionvii_academperfftopic() {
		return this.Scoreform_sectionvii_academperfftopic;
	}
	this.getScoreform_sectionvii_academperfftopic=getScoreform_sectionvii_academperfftopic;


	function setScoreform_sectionvii_academperfftopic(v){
		this.Scoreform_sectionvii_academperfftopic=v;
	}
	this.setScoreform_sectionvii_academperfftopic=setScoreform_sectionvii_academperfftopic;

	this.Scoreform_sectionvii_academperff=null;


	function getScoreform_sectionvii_academperff() {
		return this.Scoreform_sectionvii_academperff;
	}
	this.getScoreform_sectionvii_academperff=getScoreform_sectionvii_academperff;


	function setScoreform_sectionvii_academperff(v){
		this.Scoreform_sectionvii_academperff=v;
	}
	this.setScoreform_sectionvii_academperff=setScoreform_sectionvii_academperff;

	this.Scoreform_sectionvii_academperfgtopic=null;


	function getScoreform_sectionvii_academperfgtopic() {
		return this.Scoreform_sectionvii_academperfgtopic;
	}
	this.getScoreform_sectionvii_academperfgtopic=getScoreform_sectionvii_academperfgtopic;


	function setScoreform_sectionvii_academperfgtopic(v){
		this.Scoreform_sectionvii_academperfgtopic=v;
	}
	this.setScoreform_sectionvii_academperfgtopic=setScoreform_sectionvii_academperfgtopic;

	this.Scoreform_sectionvii_academperfg=null;


	function getScoreform_sectionvii_academperfg() {
		return this.Scoreform_sectionvii_academperfg;
	}
	this.getScoreform_sectionvii_academperfg=getScoreform_sectionvii_academperfg;


	function setScoreform_sectionvii_academperfg(v){
		this.Scoreform_sectionvii_academperfg=v;
	}
	this.setScoreform_sectionvii_academperfg=setScoreform_sectionvii_academperfg;

	this.Scoreform_sectionvii_speced=null;


	function getScoreform_sectionvii_speced() {
		return this.Scoreform_sectionvii_speced;
	}
	this.getScoreform_sectionvii_speced=getScoreform_sectionvii_speced;


	function setScoreform_sectionvii_speced(v){
		this.Scoreform_sectionvii_speced=v;
	}
	this.setScoreform_sectionvii_speced=setScoreform_sectionvii_speced;

	this.Scoreform_sectionvii_specedkind=null;


	function getScoreform_sectionvii_specedkind() {
		return this.Scoreform_sectionvii_specedkind;
	}
	this.getScoreform_sectionvii_specedkind=getScoreform_sectionvii_specedkind;


	function setScoreform_sectionvii_specedkind(v){
		this.Scoreform_sectionvii_specedkind=v;
	}
	this.setScoreform_sectionvii_specedkind=setScoreform_sectionvii_specedkind;

	this.Scoreform_sectionvii_repeatgrade=null;


	function getScoreform_sectionvii_repeatgrade() {
		return this.Scoreform_sectionvii_repeatgrade;
	}
	this.getScoreform_sectionvii_repeatgrade=getScoreform_sectionvii_repeatgrade;


	function setScoreform_sectionvii_repeatgrade(v){
		this.Scoreform_sectionvii_repeatgrade=v;
	}
	this.setScoreform_sectionvii_repeatgrade=setScoreform_sectionvii_repeatgrade;

	this.Scoreform_sectionvii_repeatgradereason=null;


	function getScoreform_sectionvii_repeatgradereason() {
		return this.Scoreform_sectionvii_repeatgradereason;
	}
	this.getScoreform_sectionvii_repeatgradereason=getScoreform_sectionvii_repeatgradereason;


	function setScoreform_sectionvii_repeatgradereason(v){
		this.Scoreform_sectionvii_repeatgradereason=v;
	}
	this.setScoreform_sectionvii_repeatgradereason=setScoreform_sectionvii_repeatgradereason;

	this.Scoreform_sectionvii_schoolprob=null;


	function getScoreform_sectionvii_schoolprob() {
		return this.Scoreform_sectionvii_schoolprob;
	}
	this.getScoreform_sectionvii_schoolprob=getScoreform_sectionvii_schoolprob;


	function setScoreform_sectionvii_schoolprob(v){
		this.Scoreform_sectionvii_schoolprob=v;
	}
	this.setScoreform_sectionvii_schoolprob=setScoreform_sectionvii_schoolprob;

	this.Scoreform_sectionvii_schoolprobdesc=null;


	function getScoreform_sectionvii_schoolprobdesc() {
		return this.Scoreform_sectionvii_schoolprobdesc;
	}
	this.getScoreform_sectionvii_schoolprobdesc=getScoreform_sectionvii_schoolprobdesc;


	function setScoreform_sectionvii_schoolprobdesc(v){
		this.Scoreform_sectionvii_schoolprobdesc=v;
	}
	this.setScoreform_sectionvii_schoolprobdesc=setScoreform_sectionvii_schoolprobdesc;

	this.Scoreform_sectionvii_schoolprobstartdate=null;


	function getScoreform_sectionvii_schoolprobstartdate() {
		return this.Scoreform_sectionvii_schoolprobstartdate;
	}
	this.getScoreform_sectionvii_schoolprobstartdate=getScoreform_sectionvii_schoolprobstartdate;


	function setScoreform_sectionvii_schoolprobstartdate(v){
		this.Scoreform_sectionvii_schoolprobstartdate=v;
	}
	this.setScoreform_sectionvii_schoolprobstartdate=setScoreform_sectionvii_schoolprobstartdate;

	this.Scoreform_sectionvii_schoolprobend=null;


	function getScoreform_sectionvii_schoolprobend() {
		return this.Scoreform_sectionvii_schoolprobend;
	}
	this.getScoreform_sectionvii_schoolprobend=getScoreform_sectionvii_schoolprobend;


	function setScoreform_sectionvii_schoolprobend(v){
		this.Scoreform_sectionvii_schoolprobend=v;
	}
	this.setScoreform_sectionvii_schoolprobend=setScoreform_sectionvii_schoolprobend;

	this.Scoreform_sectionvii_schoolprobenddate=null;


	function getScoreform_sectionvii_schoolprobenddate() {
		return this.Scoreform_sectionvii_schoolprobenddate;
	}
	this.getScoreform_sectionvii_schoolprobenddate=getScoreform_sectionvii_schoolprobenddate;


	function setScoreform_sectionvii_schoolprobenddate(v){
		this.Scoreform_sectionvii_schoolprobenddate=v;
	}
	this.setScoreform_sectionvii_schoolprobenddate=setScoreform_sectionvii_schoolprobenddate;

	this.Scoreform_illdisable=null;


	function getScoreform_illdisable() {
		return this.Scoreform_illdisable;
	}
	this.getScoreform_illdisable=getScoreform_illdisable;


	function setScoreform_illdisable(v){
		this.Scoreform_illdisable=v;
	}
	this.setScoreform_illdisable=setScoreform_illdisable;

	this.Scoreform_illdisabledesc=null;


	function getScoreform_illdisabledesc() {
		return this.Scoreform_illdisabledesc;
	}
	this.getScoreform_illdisabledesc=getScoreform_illdisabledesc;


	function setScoreform_illdisabledesc(v){
		this.Scoreform_illdisabledesc=v;
	}
	this.setScoreform_illdisabledesc=setScoreform_illdisabledesc;

	this.Scoreform_concernmost=null;


	function getScoreform_concernmost() {
		return this.Scoreform_concernmost;
	}
	this.getScoreform_concernmost=getScoreform_concernmost;


	function setScoreform_concernmost(v){
		this.Scoreform_concernmost=v;
	}
	this.setScoreform_concernmost=setScoreform_concernmost;

	this.Scoreform_bestthings=null;


	function getScoreform_bestthings() {
		return this.Scoreform_bestthings;
	}
	this.getScoreform_bestthings=getScoreform_bestthings;


	function setScoreform_bestthings(v){
		this.Scoreform_bestthings=v;
	}
	this.setScoreform_bestthings=setScoreform_bestthings;

	this.Scoreform_descitemlist_question1=null;


	function getScoreform_descitemlist_question1() {
		return this.Scoreform_descitemlist_question1;
	}
	this.getScoreform_descitemlist_question1=getScoreform_descitemlist_question1;


	function setScoreform_descitemlist_question1(v){
		this.Scoreform_descitemlist_question1=v;
	}
	this.setScoreform_descitemlist_question1=setScoreform_descitemlist_question1;

	this.Scoreform_descitemlist_question2=null;


	function getScoreform_descitemlist_question2() {
		return this.Scoreform_descitemlist_question2;
	}
	this.getScoreform_descitemlist_question2=getScoreform_descitemlist_question2;


	function setScoreform_descitemlist_question2(v){
		this.Scoreform_descitemlist_question2=v;
	}
	this.setScoreform_descitemlist_question2=setScoreform_descitemlist_question2;

	this.Scoreform_descitemlist_question2desc=null;


	function getScoreform_descitemlist_question2desc() {
		return this.Scoreform_descitemlist_question2desc;
	}
	this.getScoreform_descitemlist_question2desc=getScoreform_descitemlist_question2desc;


	function setScoreform_descitemlist_question2desc(v){
		this.Scoreform_descitemlist_question2desc=v;
	}
	this.setScoreform_descitemlist_question2desc=setScoreform_descitemlist_question2desc;

	this.Scoreform_descitemlist_question3=null;


	function getScoreform_descitemlist_question3() {
		return this.Scoreform_descitemlist_question3;
	}
	this.getScoreform_descitemlist_question3=getScoreform_descitemlist_question3;


	function setScoreform_descitemlist_question3(v){
		this.Scoreform_descitemlist_question3=v;
	}
	this.setScoreform_descitemlist_question3=setScoreform_descitemlist_question3;

	this.Scoreform_descitemlist_question4=null;


	function getScoreform_descitemlist_question4() {
		return this.Scoreform_descitemlist_question4;
	}
	this.getScoreform_descitemlist_question4=getScoreform_descitemlist_question4;


	function setScoreform_descitemlist_question4(v){
		this.Scoreform_descitemlist_question4=v;
	}
	this.setScoreform_descitemlist_question4=setScoreform_descitemlist_question4;

	this.Scoreform_descitemlist_question5=null;


	function getScoreform_descitemlist_question5() {
		return this.Scoreform_descitemlist_question5;
	}
	this.getScoreform_descitemlist_question5=getScoreform_descitemlist_question5;


	function setScoreform_descitemlist_question5(v){
		this.Scoreform_descitemlist_question5=v;
	}
	this.setScoreform_descitemlist_question5=setScoreform_descitemlist_question5;

	this.Scoreform_descitemlist_question6=null;


	function getScoreform_descitemlist_question6() {
		return this.Scoreform_descitemlist_question6;
	}
	this.getScoreform_descitemlist_question6=getScoreform_descitemlist_question6;


	function setScoreform_descitemlist_question6(v){
		this.Scoreform_descitemlist_question6=v;
	}
	this.setScoreform_descitemlist_question6=setScoreform_descitemlist_question6;

	this.Scoreform_descitemlist_question7=null;


	function getScoreform_descitemlist_question7() {
		return this.Scoreform_descitemlist_question7;
	}
	this.getScoreform_descitemlist_question7=getScoreform_descitemlist_question7;


	function setScoreform_descitemlist_question7(v){
		this.Scoreform_descitemlist_question7=v;
	}
	this.setScoreform_descitemlist_question7=setScoreform_descitemlist_question7;

	this.Scoreform_descitemlist_question8=null;


	function getScoreform_descitemlist_question8() {
		return this.Scoreform_descitemlist_question8;
	}
	this.getScoreform_descitemlist_question8=getScoreform_descitemlist_question8;


	function setScoreform_descitemlist_question8(v){
		this.Scoreform_descitemlist_question8=v;
	}
	this.setScoreform_descitemlist_question8=setScoreform_descitemlist_question8;

	this.Scoreform_descitemlist_question9=null;


	function getScoreform_descitemlist_question9() {
		return this.Scoreform_descitemlist_question9;
	}
	this.getScoreform_descitemlist_question9=getScoreform_descitemlist_question9;


	function setScoreform_descitemlist_question9(v){
		this.Scoreform_descitemlist_question9=v;
	}
	this.setScoreform_descitemlist_question9=setScoreform_descitemlist_question9;

	this.Scoreform_descitemlist_question9desc=null;


	function getScoreform_descitemlist_question9desc() {
		return this.Scoreform_descitemlist_question9desc;
	}
	this.getScoreform_descitemlist_question9desc=getScoreform_descitemlist_question9desc;


	function setScoreform_descitemlist_question9desc(v){
		this.Scoreform_descitemlist_question9desc=v;
	}
	this.setScoreform_descitemlist_question9desc=setScoreform_descitemlist_question9desc;

	this.Scoreform_descitemlist_question10=null;


	function getScoreform_descitemlist_question10() {
		return this.Scoreform_descitemlist_question10;
	}
	this.getScoreform_descitemlist_question10=getScoreform_descitemlist_question10;


	function setScoreform_descitemlist_question10(v){
		this.Scoreform_descitemlist_question10=v;
	}
	this.setScoreform_descitemlist_question10=setScoreform_descitemlist_question10;

	this.Scoreform_descitemlist_question11=null;


	function getScoreform_descitemlist_question11() {
		return this.Scoreform_descitemlist_question11;
	}
	this.getScoreform_descitemlist_question11=getScoreform_descitemlist_question11;


	function setScoreform_descitemlist_question11(v){
		this.Scoreform_descitemlist_question11=v;
	}
	this.setScoreform_descitemlist_question11=setScoreform_descitemlist_question11;

	this.Scoreform_descitemlist_question12=null;


	function getScoreform_descitemlist_question12() {
		return this.Scoreform_descitemlist_question12;
	}
	this.getScoreform_descitemlist_question12=getScoreform_descitemlist_question12;


	function setScoreform_descitemlist_question12(v){
		this.Scoreform_descitemlist_question12=v;
	}
	this.setScoreform_descitemlist_question12=setScoreform_descitemlist_question12;

	this.Scoreform_descitemlist_question13=null;


	function getScoreform_descitemlist_question13() {
		return this.Scoreform_descitemlist_question13;
	}
	this.getScoreform_descitemlist_question13=getScoreform_descitemlist_question13;


	function setScoreform_descitemlist_question13(v){
		this.Scoreform_descitemlist_question13=v;
	}
	this.setScoreform_descitemlist_question13=setScoreform_descitemlist_question13;

	this.Scoreform_descitemlist_question14=null;


	function getScoreform_descitemlist_question14() {
		return this.Scoreform_descitemlist_question14;
	}
	this.getScoreform_descitemlist_question14=getScoreform_descitemlist_question14;


	function setScoreform_descitemlist_question14(v){
		this.Scoreform_descitemlist_question14=v;
	}
	this.setScoreform_descitemlist_question14=setScoreform_descitemlist_question14;

	this.Scoreform_descitemlist_question15=null;


	function getScoreform_descitemlist_question15() {
		return this.Scoreform_descitemlist_question15;
	}
	this.getScoreform_descitemlist_question15=getScoreform_descitemlist_question15;


	function setScoreform_descitemlist_question15(v){
		this.Scoreform_descitemlist_question15=v;
	}
	this.setScoreform_descitemlist_question15=setScoreform_descitemlist_question15;

	this.Scoreform_descitemlist_question16=null;


	function getScoreform_descitemlist_question16() {
		return this.Scoreform_descitemlist_question16;
	}
	this.getScoreform_descitemlist_question16=getScoreform_descitemlist_question16;


	function setScoreform_descitemlist_question16(v){
		this.Scoreform_descitemlist_question16=v;
	}
	this.setScoreform_descitemlist_question16=setScoreform_descitemlist_question16;

	this.Scoreform_descitemlist_question17=null;


	function getScoreform_descitemlist_question17() {
		return this.Scoreform_descitemlist_question17;
	}
	this.getScoreform_descitemlist_question17=getScoreform_descitemlist_question17;


	function setScoreform_descitemlist_question17(v){
		this.Scoreform_descitemlist_question17=v;
	}
	this.setScoreform_descitemlist_question17=setScoreform_descitemlist_question17;

	this.Scoreform_descitemlist_question18=null;


	function getScoreform_descitemlist_question18() {
		return this.Scoreform_descitemlist_question18;
	}
	this.getScoreform_descitemlist_question18=getScoreform_descitemlist_question18;


	function setScoreform_descitemlist_question18(v){
		this.Scoreform_descitemlist_question18=v;
	}
	this.setScoreform_descitemlist_question18=setScoreform_descitemlist_question18;

	this.Scoreform_descitemlist_question19=null;


	function getScoreform_descitemlist_question19() {
		return this.Scoreform_descitemlist_question19;
	}
	this.getScoreform_descitemlist_question19=getScoreform_descitemlist_question19;


	function setScoreform_descitemlist_question19(v){
		this.Scoreform_descitemlist_question19=v;
	}
	this.setScoreform_descitemlist_question19=setScoreform_descitemlist_question19;

	this.Scoreform_descitemlist_question20=null;


	function getScoreform_descitemlist_question20() {
		return this.Scoreform_descitemlist_question20;
	}
	this.getScoreform_descitemlist_question20=getScoreform_descitemlist_question20;


	function setScoreform_descitemlist_question20(v){
		this.Scoreform_descitemlist_question20=v;
	}
	this.setScoreform_descitemlist_question20=setScoreform_descitemlist_question20;

	this.Scoreform_descitemlist_question21=null;


	function getScoreform_descitemlist_question21() {
		return this.Scoreform_descitemlist_question21;
	}
	this.getScoreform_descitemlist_question21=getScoreform_descitemlist_question21;


	function setScoreform_descitemlist_question21(v){
		this.Scoreform_descitemlist_question21=v;
	}
	this.setScoreform_descitemlist_question21=setScoreform_descitemlist_question21;

	this.Scoreform_descitemlist_question22=null;


	function getScoreform_descitemlist_question22() {
		return this.Scoreform_descitemlist_question22;
	}
	this.getScoreform_descitemlist_question22=getScoreform_descitemlist_question22;


	function setScoreform_descitemlist_question22(v){
		this.Scoreform_descitemlist_question22=v;
	}
	this.setScoreform_descitemlist_question22=setScoreform_descitemlist_question22;

	this.Scoreform_descitemlist_question23=null;


	function getScoreform_descitemlist_question23() {
		return this.Scoreform_descitemlist_question23;
	}
	this.getScoreform_descitemlist_question23=getScoreform_descitemlist_question23;


	function setScoreform_descitemlist_question23(v){
		this.Scoreform_descitemlist_question23=v;
	}
	this.setScoreform_descitemlist_question23=setScoreform_descitemlist_question23;

	this.Scoreform_descitemlist_question24=null;


	function getScoreform_descitemlist_question24() {
		return this.Scoreform_descitemlist_question24;
	}
	this.getScoreform_descitemlist_question24=getScoreform_descitemlist_question24;


	function setScoreform_descitemlist_question24(v){
		this.Scoreform_descitemlist_question24=v;
	}
	this.setScoreform_descitemlist_question24=setScoreform_descitemlist_question24;

	this.Scoreform_descitemlist_question25=null;


	function getScoreform_descitemlist_question25() {
		return this.Scoreform_descitemlist_question25;
	}
	this.getScoreform_descitemlist_question25=getScoreform_descitemlist_question25;


	function setScoreform_descitemlist_question25(v){
		this.Scoreform_descitemlist_question25=v;
	}
	this.setScoreform_descitemlist_question25=setScoreform_descitemlist_question25;

	this.Scoreform_descitemlist_question26=null;


	function getScoreform_descitemlist_question26() {
		return this.Scoreform_descitemlist_question26;
	}
	this.getScoreform_descitemlist_question26=getScoreform_descitemlist_question26;


	function setScoreform_descitemlist_question26(v){
		this.Scoreform_descitemlist_question26=v;
	}
	this.setScoreform_descitemlist_question26=setScoreform_descitemlist_question26;

	this.Scoreform_descitemlist_question27=null;


	function getScoreform_descitemlist_question27() {
		return this.Scoreform_descitemlist_question27;
	}
	this.getScoreform_descitemlist_question27=getScoreform_descitemlist_question27;


	function setScoreform_descitemlist_question27(v){
		this.Scoreform_descitemlist_question27=v;
	}
	this.setScoreform_descitemlist_question27=setScoreform_descitemlist_question27;

	this.Scoreform_descitemlist_question28=null;


	function getScoreform_descitemlist_question28() {
		return this.Scoreform_descitemlist_question28;
	}
	this.getScoreform_descitemlist_question28=getScoreform_descitemlist_question28;


	function setScoreform_descitemlist_question28(v){
		this.Scoreform_descitemlist_question28=v;
	}
	this.setScoreform_descitemlist_question28=setScoreform_descitemlist_question28;

	this.Scoreform_descitemlist_question29=null;


	function getScoreform_descitemlist_question29() {
		return this.Scoreform_descitemlist_question29;
	}
	this.getScoreform_descitemlist_question29=getScoreform_descitemlist_question29;


	function setScoreform_descitemlist_question29(v){
		this.Scoreform_descitemlist_question29=v;
	}
	this.setScoreform_descitemlist_question29=setScoreform_descitemlist_question29;

	this.Scoreform_descitemlist_question29desc=null;


	function getScoreform_descitemlist_question29desc() {
		return this.Scoreform_descitemlist_question29desc;
	}
	this.getScoreform_descitemlist_question29desc=getScoreform_descitemlist_question29desc;


	function setScoreform_descitemlist_question29desc(v){
		this.Scoreform_descitemlist_question29desc=v;
	}
	this.setScoreform_descitemlist_question29desc=setScoreform_descitemlist_question29desc;

	this.Scoreform_descitemlist_question30=null;


	function getScoreform_descitemlist_question30() {
		return this.Scoreform_descitemlist_question30;
	}
	this.getScoreform_descitemlist_question30=getScoreform_descitemlist_question30;


	function setScoreform_descitemlist_question30(v){
		this.Scoreform_descitemlist_question30=v;
	}
	this.setScoreform_descitemlist_question30=setScoreform_descitemlist_question30;

	this.Scoreform_descitemlist_question31=null;


	function getScoreform_descitemlist_question31() {
		return this.Scoreform_descitemlist_question31;
	}
	this.getScoreform_descitemlist_question31=getScoreform_descitemlist_question31;


	function setScoreform_descitemlist_question31(v){
		this.Scoreform_descitemlist_question31=v;
	}
	this.setScoreform_descitemlist_question31=setScoreform_descitemlist_question31;

	this.Scoreform_descitemlist_question32=null;


	function getScoreform_descitemlist_question32() {
		return this.Scoreform_descitemlist_question32;
	}
	this.getScoreform_descitemlist_question32=getScoreform_descitemlist_question32;


	function setScoreform_descitemlist_question32(v){
		this.Scoreform_descitemlist_question32=v;
	}
	this.setScoreform_descitemlist_question32=setScoreform_descitemlist_question32;

	this.Scoreform_descitemlist_question33=null;


	function getScoreform_descitemlist_question33() {
		return this.Scoreform_descitemlist_question33;
	}
	this.getScoreform_descitemlist_question33=getScoreform_descitemlist_question33;


	function setScoreform_descitemlist_question33(v){
		this.Scoreform_descitemlist_question33=v;
	}
	this.setScoreform_descitemlist_question33=setScoreform_descitemlist_question33;

	this.Scoreform_descitemlist_question34=null;


	function getScoreform_descitemlist_question34() {
		return this.Scoreform_descitemlist_question34;
	}
	this.getScoreform_descitemlist_question34=getScoreform_descitemlist_question34;


	function setScoreform_descitemlist_question34(v){
		this.Scoreform_descitemlist_question34=v;
	}
	this.setScoreform_descitemlist_question34=setScoreform_descitemlist_question34;

	this.Scoreform_descitemlist_question35=null;


	function getScoreform_descitemlist_question35() {
		return this.Scoreform_descitemlist_question35;
	}
	this.getScoreform_descitemlist_question35=getScoreform_descitemlist_question35;


	function setScoreform_descitemlist_question35(v){
		this.Scoreform_descitemlist_question35=v;
	}
	this.setScoreform_descitemlist_question35=setScoreform_descitemlist_question35;

	this.Scoreform_descitemlist_question36=null;


	function getScoreform_descitemlist_question36() {
		return this.Scoreform_descitemlist_question36;
	}
	this.getScoreform_descitemlist_question36=getScoreform_descitemlist_question36;


	function setScoreform_descitemlist_question36(v){
		this.Scoreform_descitemlist_question36=v;
	}
	this.setScoreform_descitemlist_question36=setScoreform_descitemlist_question36;

	this.Scoreform_descitemlist_question37=null;


	function getScoreform_descitemlist_question37() {
		return this.Scoreform_descitemlist_question37;
	}
	this.getScoreform_descitemlist_question37=getScoreform_descitemlist_question37;


	function setScoreform_descitemlist_question37(v){
		this.Scoreform_descitemlist_question37=v;
	}
	this.setScoreform_descitemlist_question37=setScoreform_descitemlist_question37;

	this.Scoreform_descitemlist_question38=null;


	function getScoreform_descitemlist_question38() {
		return this.Scoreform_descitemlist_question38;
	}
	this.getScoreform_descitemlist_question38=getScoreform_descitemlist_question38;


	function setScoreform_descitemlist_question38(v){
		this.Scoreform_descitemlist_question38=v;
	}
	this.setScoreform_descitemlist_question38=setScoreform_descitemlist_question38;

	this.Scoreform_descitemlist_question39=null;


	function getScoreform_descitemlist_question39() {
		return this.Scoreform_descitemlist_question39;
	}
	this.getScoreform_descitemlist_question39=getScoreform_descitemlist_question39;


	function setScoreform_descitemlist_question39(v){
		this.Scoreform_descitemlist_question39=v;
	}
	this.setScoreform_descitemlist_question39=setScoreform_descitemlist_question39;

	this.Scoreform_descitemlist_question40=null;


	function getScoreform_descitemlist_question40() {
		return this.Scoreform_descitemlist_question40;
	}
	this.getScoreform_descitemlist_question40=getScoreform_descitemlist_question40;


	function setScoreform_descitemlist_question40(v){
		this.Scoreform_descitemlist_question40=v;
	}
	this.setScoreform_descitemlist_question40=setScoreform_descitemlist_question40;

	this.Scoreform_descitemlist_question40desc=null;


	function getScoreform_descitemlist_question40desc() {
		return this.Scoreform_descitemlist_question40desc;
	}
	this.getScoreform_descitemlist_question40desc=getScoreform_descitemlist_question40desc;


	function setScoreform_descitemlist_question40desc(v){
		this.Scoreform_descitemlist_question40desc=v;
	}
	this.setScoreform_descitemlist_question40desc=setScoreform_descitemlist_question40desc;

	this.Scoreform_descitemlist_question41=null;


	function getScoreform_descitemlist_question41() {
		return this.Scoreform_descitemlist_question41;
	}
	this.getScoreform_descitemlist_question41=getScoreform_descitemlist_question41;


	function setScoreform_descitemlist_question41(v){
		this.Scoreform_descitemlist_question41=v;
	}
	this.setScoreform_descitemlist_question41=setScoreform_descitemlist_question41;

	this.Scoreform_descitemlist_question42=null;


	function getScoreform_descitemlist_question42() {
		return this.Scoreform_descitemlist_question42;
	}
	this.getScoreform_descitemlist_question42=getScoreform_descitemlist_question42;


	function setScoreform_descitemlist_question42(v){
		this.Scoreform_descitemlist_question42=v;
	}
	this.setScoreform_descitemlist_question42=setScoreform_descitemlist_question42;

	this.Scoreform_descitemlist_question43=null;


	function getScoreform_descitemlist_question43() {
		return this.Scoreform_descitemlist_question43;
	}
	this.getScoreform_descitemlist_question43=getScoreform_descitemlist_question43;


	function setScoreform_descitemlist_question43(v){
		this.Scoreform_descitemlist_question43=v;
	}
	this.setScoreform_descitemlist_question43=setScoreform_descitemlist_question43;

	this.Scoreform_descitemlist_question44=null;


	function getScoreform_descitemlist_question44() {
		return this.Scoreform_descitemlist_question44;
	}
	this.getScoreform_descitemlist_question44=getScoreform_descitemlist_question44;


	function setScoreform_descitemlist_question44(v){
		this.Scoreform_descitemlist_question44=v;
	}
	this.setScoreform_descitemlist_question44=setScoreform_descitemlist_question44;

	this.Scoreform_descitemlist_question45=null;


	function getScoreform_descitemlist_question45() {
		return this.Scoreform_descitemlist_question45;
	}
	this.getScoreform_descitemlist_question45=getScoreform_descitemlist_question45;


	function setScoreform_descitemlist_question45(v){
		this.Scoreform_descitemlist_question45=v;
	}
	this.setScoreform_descitemlist_question45=setScoreform_descitemlist_question45;

	this.Scoreform_descitemlist_question46=null;


	function getScoreform_descitemlist_question46() {
		return this.Scoreform_descitemlist_question46;
	}
	this.getScoreform_descitemlist_question46=getScoreform_descitemlist_question46;


	function setScoreform_descitemlist_question46(v){
		this.Scoreform_descitemlist_question46=v;
	}
	this.setScoreform_descitemlist_question46=setScoreform_descitemlist_question46;

	this.Scoreform_descitemlist_question46desc=null;


	function getScoreform_descitemlist_question46desc() {
		return this.Scoreform_descitemlist_question46desc;
	}
	this.getScoreform_descitemlist_question46desc=getScoreform_descitemlist_question46desc;


	function setScoreform_descitemlist_question46desc(v){
		this.Scoreform_descitemlist_question46desc=v;
	}
	this.setScoreform_descitemlist_question46desc=setScoreform_descitemlist_question46desc;

	this.Scoreform_descitemlist_question47=null;


	function getScoreform_descitemlist_question47() {
		return this.Scoreform_descitemlist_question47;
	}
	this.getScoreform_descitemlist_question47=getScoreform_descitemlist_question47;


	function setScoreform_descitemlist_question47(v){
		this.Scoreform_descitemlist_question47=v;
	}
	this.setScoreform_descitemlist_question47=setScoreform_descitemlist_question47;

	this.Scoreform_descitemlist_question48=null;


	function getScoreform_descitemlist_question48() {
		return this.Scoreform_descitemlist_question48;
	}
	this.getScoreform_descitemlist_question48=getScoreform_descitemlist_question48;


	function setScoreform_descitemlist_question48(v){
		this.Scoreform_descitemlist_question48=v;
	}
	this.setScoreform_descitemlist_question48=setScoreform_descitemlist_question48;

	this.Scoreform_descitemlist_question49=null;


	function getScoreform_descitemlist_question49() {
		return this.Scoreform_descitemlist_question49;
	}
	this.getScoreform_descitemlist_question49=getScoreform_descitemlist_question49;


	function setScoreform_descitemlist_question49(v){
		this.Scoreform_descitemlist_question49=v;
	}
	this.setScoreform_descitemlist_question49=setScoreform_descitemlist_question49;

	this.Scoreform_descitemlist_question50=null;


	function getScoreform_descitemlist_question50() {
		return this.Scoreform_descitemlist_question50;
	}
	this.getScoreform_descitemlist_question50=getScoreform_descitemlist_question50;


	function setScoreform_descitemlist_question50(v){
		this.Scoreform_descitemlist_question50=v;
	}
	this.setScoreform_descitemlist_question50=setScoreform_descitemlist_question50;

	this.Scoreform_descitemlist_question51=null;


	function getScoreform_descitemlist_question51() {
		return this.Scoreform_descitemlist_question51;
	}
	this.getScoreform_descitemlist_question51=getScoreform_descitemlist_question51;


	function setScoreform_descitemlist_question51(v){
		this.Scoreform_descitemlist_question51=v;
	}
	this.setScoreform_descitemlist_question51=setScoreform_descitemlist_question51;

	this.Scoreform_descitemlist_question52=null;


	function getScoreform_descitemlist_question52() {
		return this.Scoreform_descitemlist_question52;
	}
	this.getScoreform_descitemlist_question52=getScoreform_descitemlist_question52;


	function setScoreform_descitemlist_question52(v){
		this.Scoreform_descitemlist_question52=v;
	}
	this.setScoreform_descitemlist_question52=setScoreform_descitemlist_question52;

	this.Scoreform_descitemlist_question53=null;


	function getScoreform_descitemlist_question53() {
		return this.Scoreform_descitemlist_question53;
	}
	this.getScoreform_descitemlist_question53=getScoreform_descitemlist_question53;


	function setScoreform_descitemlist_question53(v){
		this.Scoreform_descitemlist_question53=v;
	}
	this.setScoreform_descitemlist_question53=setScoreform_descitemlist_question53;

	this.Scoreform_descitemlist_question54=null;


	function getScoreform_descitemlist_question54() {
		return this.Scoreform_descitemlist_question54;
	}
	this.getScoreform_descitemlist_question54=getScoreform_descitemlist_question54;


	function setScoreform_descitemlist_question54(v){
		this.Scoreform_descitemlist_question54=v;
	}
	this.setScoreform_descitemlist_question54=setScoreform_descitemlist_question54;

	this.Scoreform_descitemlist_question55=null;


	function getScoreform_descitemlist_question55() {
		return this.Scoreform_descitemlist_question55;
	}
	this.getScoreform_descitemlist_question55=getScoreform_descitemlist_question55;


	function setScoreform_descitemlist_question55(v){
		this.Scoreform_descitemlist_question55=v;
	}
	this.setScoreform_descitemlist_question55=setScoreform_descitemlist_question55;

	this.Scoreform_descitemlist_question56a=null;


	function getScoreform_descitemlist_question56a() {
		return this.Scoreform_descitemlist_question56a;
	}
	this.getScoreform_descitemlist_question56a=getScoreform_descitemlist_question56a;


	function setScoreform_descitemlist_question56a(v){
		this.Scoreform_descitemlist_question56a=v;
	}
	this.setScoreform_descitemlist_question56a=setScoreform_descitemlist_question56a;

	this.Scoreform_descitemlist_question56b=null;


	function getScoreform_descitemlist_question56b() {
		return this.Scoreform_descitemlist_question56b;
	}
	this.getScoreform_descitemlist_question56b=getScoreform_descitemlist_question56b;


	function setScoreform_descitemlist_question56b(v){
		this.Scoreform_descitemlist_question56b=v;
	}
	this.setScoreform_descitemlist_question56b=setScoreform_descitemlist_question56b;

	this.Scoreform_descitemlist_question56c=null;


	function getScoreform_descitemlist_question56c() {
		return this.Scoreform_descitemlist_question56c;
	}
	this.getScoreform_descitemlist_question56c=getScoreform_descitemlist_question56c;


	function setScoreform_descitemlist_question56c(v){
		this.Scoreform_descitemlist_question56c=v;
	}
	this.setScoreform_descitemlist_question56c=setScoreform_descitemlist_question56c;

	this.Scoreform_descitemlist_question56d=null;


	function getScoreform_descitemlist_question56d() {
		return this.Scoreform_descitemlist_question56d;
	}
	this.getScoreform_descitemlist_question56d=getScoreform_descitemlist_question56d;


	function setScoreform_descitemlist_question56d(v){
		this.Scoreform_descitemlist_question56d=v;
	}
	this.setScoreform_descitemlist_question56d=setScoreform_descitemlist_question56d;

	this.Scoreform_descitemlist_question56ddesc=null;


	function getScoreform_descitemlist_question56ddesc() {
		return this.Scoreform_descitemlist_question56ddesc;
	}
	this.getScoreform_descitemlist_question56ddesc=getScoreform_descitemlist_question56ddesc;


	function setScoreform_descitemlist_question56ddesc(v){
		this.Scoreform_descitemlist_question56ddesc=v;
	}
	this.setScoreform_descitemlist_question56ddesc=setScoreform_descitemlist_question56ddesc;

	this.Scoreform_descitemlist_question56e=null;


	function getScoreform_descitemlist_question56e() {
		return this.Scoreform_descitemlist_question56e;
	}
	this.getScoreform_descitemlist_question56e=getScoreform_descitemlist_question56e;


	function setScoreform_descitemlist_question56e(v){
		this.Scoreform_descitemlist_question56e=v;
	}
	this.setScoreform_descitemlist_question56e=setScoreform_descitemlist_question56e;

	this.Scoreform_descitemlist_question56f=null;


	function getScoreform_descitemlist_question56f() {
		return this.Scoreform_descitemlist_question56f;
	}
	this.getScoreform_descitemlist_question56f=getScoreform_descitemlist_question56f;


	function setScoreform_descitemlist_question56f(v){
		this.Scoreform_descitemlist_question56f=v;
	}
	this.setScoreform_descitemlist_question56f=setScoreform_descitemlist_question56f;

	this.Scoreform_descitemlist_question56g=null;


	function getScoreform_descitemlist_question56g() {
		return this.Scoreform_descitemlist_question56g;
	}
	this.getScoreform_descitemlist_question56g=getScoreform_descitemlist_question56g;


	function setScoreform_descitemlist_question56g(v){
		this.Scoreform_descitemlist_question56g=v;
	}
	this.setScoreform_descitemlist_question56g=setScoreform_descitemlist_question56g;

	this.Scoreform_descitemlist_question56h=null;


	function getScoreform_descitemlist_question56h() {
		return this.Scoreform_descitemlist_question56h;
	}
	this.getScoreform_descitemlist_question56h=getScoreform_descitemlist_question56h;


	function setScoreform_descitemlist_question56h(v){
		this.Scoreform_descitemlist_question56h=v;
	}
	this.setScoreform_descitemlist_question56h=setScoreform_descitemlist_question56h;

	this.Scoreform_descitemlist_question57=null;


	function getScoreform_descitemlist_question57() {
		return this.Scoreform_descitemlist_question57;
	}
	this.getScoreform_descitemlist_question57=getScoreform_descitemlist_question57;


	function setScoreform_descitemlist_question57(v){
		this.Scoreform_descitemlist_question57=v;
	}
	this.setScoreform_descitemlist_question57=setScoreform_descitemlist_question57;

	this.Scoreform_descitemlist_question58=null;


	function getScoreform_descitemlist_question58() {
		return this.Scoreform_descitemlist_question58;
	}
	this.getScoreform_descitemlist_question58=getScoreform_descitemlist_question58;


	function setScoreform_descitemlist_question58(v){
		this.Scoreform_descitemlist_question58=v;
	}
	this.setScoreform_descitemlist_question58=setScoreform_descitemlist_question58;

	this.Scoreform_descitemlist_question58desc=null;


	function getScoreform_descitemlist_question58desc() {
		return this.Scoreform_descitemlist_question58desc;
	}
	this.getScoreform_descitemlist_question58desc=getScoreform_descitemlist_question58desc;


	function setScoreform_descitemlist_question58desc(v){
		this.Scoreform_descitemlist_question58desc=v;
	}
	this.setScoreform_descitemlist_question58desc=setScoreform_descitemlist_question58desc;

	this.Scoreform_descitemlist_question59=null;


	function getScoreform_descitemlist_question59() {
		return this.Scoreform_descitemlist_question59;
	}
	this.getScoreform_descitemlist_question59=getScoreform_descitemlist_question59;


	function setScoreform_descitemlist_question59(v){
		this.Scoreform_descitemlist_question59=v;
	}
	this.setScoreform_descitemlist_question59=setScoreform_descitemlist_question59;

	this.Scoreform_descitemlist_question60=null;


	function getScoreform_descitemlist_question60() {
		return this.Scoreform_descitemlist_question60;
	}
	this.getScoreform_descitemlist_question60=getScoreform_descitemlist_question60;


	function setScoreform_descitemlist_question60(v){
		this.Scoreform_descitemlist_question60=v;
	}
	this.setScoreform_descitemlist_question60=setScoreform_descitemlist_question60;

	this.Scoreform_descitemlist_question61=null;


	function getScoreform_descitemlist_question61() {
		return this.Scoreform_descitemlist_question61;
	}
	this.getScoreform_descitemlist_question61=getScoreform_descitemlist_question61;


	function setScoreform_descitemlist_question61(v){
		this.Scoreform_descitemlist_question61=v;
	}
	this.setScoreform_descitemlist_question61=setScoreform_descitemlist_question61;

	this.Scoreform_descitemlist_question62=null;


	function getScoreform_descitemlist_question62() {
		return this.Scoreform_descitemlist_question62;
	}
	this.getScoreform_descitemlist_question62=getScoreform_descitemlist_question62;


	function setScoreform_descitemlist_question62(v){
		this.Scoreform_descitemlist_question62=v;
	}
	this.setScoreform_descitemlist_question62=setScoreform_descitemlist_question62;

	this.Scoreform_descitemlist_question63=null;


	function getScoreform_descitemlist_question63() {
		return this.Scoreform_descitemlist_question63;
	}
	this.getScoreform_descitemlist_question63=getScoreform_descitemlist_question63;


	function setScoreform_descitemlist_question63(v){
		this.Scoreform_descitemlist_question63=v;
	}
	this.setScoreform_descitemlist_question63=setScoreform_descitemlist_question63;

	this.Scoreform_descitemlist_question64=null;


	function getScoreform_descitemlist_question64() {
		return this.Scoreform_descitemlist_question64;
	}
	this.getScoreform_descitemlist_question64=getScoreform_descitemlist_question64;


	function setScoreform_descitemlist_question64(v){
		this.Scoreform_descitemlist_question64=v;
	}
	this.setScoreform_descitemlist_question64=setScoreform_descitemlist_question64;

	this.Scoreform_descitemlist_question65=null;


	function getScoreform_descitemlist_question65() {
		return this.Scoreform_descitemlist_question65;
	}
	this.getScoreform_descitemlist_question65=getScoreform_descitemlist_question65;


	function setScoreform_descitemlist_question65(v){
		this.Scoreform_descitemlist_question65=v;
	}
	this.setScoreform_descitemlist_question65=setScoreform_descitemlist_question65;

	this.Scoreform_descitemlist_question66=null;


	function getScoreform_descitemlist_question66() {
		return this.Scoreform_descitemlist_question66;
	}
	this.getScoreform_descitemlist_question66=getScoreform_descitemlist_question66;


	function setScoreform_descitemlist_question66(v){
		this.Scoreform_descitemlist_question66=v;
	}
	this.setScoreform_descitemlist_question66=setScoreform_descitemlist_question66;

	this.Scoreform_descitemlist_question66desc=null;


	function getScoreform_descitemlist_question66desc() {
		return this.Scoreform_descitemlist_question66desc;
	}
	this.getScoreform_descitemlist_question66desc=getScoreform_descitemlist_question66desc;


	function setScoreform_descitemlist_question66desc(v){
		this.Scoreform_descitemlist_question66desc=v;
	}
	this.setScoreform_descitemlist_question66desc=setScoreform_descitemlist_question66desc;

	this.Scoreform_descitemlist_question67=null;


	function getScoreform_descitemlist_question67() {
		return this.Scoreform_descitemlist_question67;
	}
	this.getScoreform_descitemlist_question67=getScoreform_descitemlist_question67;


	function setScoreform_descitemlist_question67(v){
		this.Scoreform_descitemlist_question67=v;
	}
	this.setScoreform_descitemlist_question67=setScoreform_descitemlist_question67;

	this.Scoreform_descitemlist_question68=null;


	function getScoreform_descitemlist_question68() {
		return this.Scoreform_descitemlist_question68;
	}
	this.getScoreform_descitemlist_question68=getScoreform_descitemlist_question68;


	function setScoreform_descitemlist_question68(v){
		this.Scoreform_descitemlist_question68=v;
	}
	this.setScoreform_descitemlist_question68=setScoreform_descitemlist_question68;

	this.Scoreform_descitemlist_question69=null;


	function getScoreform_descitemlist_question69() {
		return this.Scoreform_descitemlist_question69;
	}
	this.getScoreform_descitemlist_question69=getScoreform_descitemlist_question69;


	function setScoreform_descitemlist_question69(v){
		this.Scoreform_descitemlist_question69=v;
	}
	this.setScoreform_descitemlist_question69=setScoreform_descitemlist_question69;

	this.Scoreform_descitemlist_question70=null;


	function getScoreform_descitemlist_question70() {
		return this.Scoreform_descitemlist_question70;
	}
	this.getScoreform_descitemlist_question70=getScoreform_descitemlist_question70;


	function setScoreform_descitemlist_question70(v){
		this.Scoreform_descitemlist_question70=v;
	}
	this.setScoreform_descitemlist_question70=setScoreform_descitemlist_question70;

	this.Scoreform_descitemlist_question70desc=null;


	function getScoreform_descitemlist_question70desc() {
		return this.Scoreform_descitemlist_question70desc;
	}
	this.getScoreform_descitemlist_question70desc=getScoreform_descitemlist_question70desc;


	function setScoreform_descitemlist_question70desc(v){
		this.Scoreform_descitemlist_question70desc=v;
	}
	this.setScoreform_descitemlist_question70desc=setScoreform_descitemlist_question70desc;

	this.Scoreform_descitemlist_question71=null;


	function getScoreform_descitemlist_question71() {
		return this.Scoreform_descitemlist_question71;
	}
	this.getScoreform_descitemlist_question71=getScoreform_descitemlist_question71;


	function setScoreform_descitemlist_question71(v){
		this.Scoreform_descitemlist_question71=v;
	}
	this.setScoreform_descitemlist_question71=setScoreform_descitemlist_question71;

	this.Scoreform_descitemlist_question72=null;


	function getScoreform_descitemlist_question72() {
		return this.Scoreform_descitemlist_question72;
	}
	this.getScoreform_descitemlist_question72=getScoreform_descitemlist_question72;


	function setScoreform_descitemlist_question72(v){
		this.Scoreform_descitemlist_question72=v;
	}
	this.setScoreform_descitemlist_question72=setScoreform_descitemlist_question72;

	this.Scoreform_descitemlist_question73=null;


	function getScoreform_descitemlist_question73() {
		return this.Scoreform_descitemlist_question73;
	}
	this.getScoreform_descitemlist_question73=getScoreform_descitemlist_question73;


	function setScoreform_descitemlist_question73(v){
		this.Scoreform_descitemlist_question73=v;
	}
	this.setScoreform_descitemlist_question73=setScoreform_descitemlist_question73;

	this.Scoreform_descitemlist_question73desc=null;


	function getScoreform_descitemlist_question73desc() {
		return this.Scoreform_descitemlist_question73desc;
	}
	this.getScoreform_descitemlist_question73desc=getScoreform_descitemlist_question73desc;


	function setScoreform_descitemlist_question73desc(v){
		this.Scoreform_descitemlist_question73desc=v;
	}
	this.setScoreform_descitemlist_question73desc=setScoreform_descitemlist_question73desc;

	this.Scoreform_descitemlist_question74=null;


	function getScoreform_descitemlist_question74() {
		return this.Scoreform_descitemlist_question74;
	}
	this.getScoreform_descitemlist_question74=getScoreform_descitemlist_question74;


	function setScoreform_descitemlist_question74(v){
		this.Scoreform_descitemlist_question74=v;
	}
	this.setScoreform_descitemlist_question74=setScoreform_descitemlist_question74;

	this.Scoreform_descitemlist_question75=null;


	function getScoreform_descitemlist_question75() {
		return this.Scoreform_descitemlist_question75;
	}
	this.getScoreform_descitemlist_question75=getScoreform_descitemlist_question75;


	function setScoreform_descitemlist_question75(v){
		this.Scoreform_descitemlist_question75=v;
	}
	this.setScoreform_descitemlist_question75=setScoreform_descitemlist_question75;

	this.Scoreform_descitemlist_question76=null;


	function getScoreform_descitemlist_question76() {
		return this.Scoreform_descitemlist_question76;
	}
	this.getScoreform_descitemlist_question76=getScoreform_descitemlist_question76;


	function setScoreform_descitemlist_question76(v){
		this.Scoreform_descitemlist_question76=v;
	}
	this.setScoreform_descitemlist_question76=setScoreform_descitemlist_question76;

	this.Scoreform_descitemlist_question77=null;


	function getScoreform_descitemlist_question77() {
		return this.Scoreform_descitemlist_question77;
	}
	this.getScoreform_descitemlist_question77=getScoreform_descitemlist_question77;


	function setScoreform_descitemlist_question77(v){
		this.Scoreform_descitemlist_question77=v;
	}
	this.setScoreform_descitemlist_question77=setScoreform_descitemlist_question77;

	this.Scoreform_descitemlist_question77desc=null;


	function getScoreform_descitemlist_question77desc() {
		return this.Scoreform_descitemlist_question77desc;
	}
	this.getScoreform_descitemlist_question77desc=getScoreform_descitemlist_question77desc;


	function setScoreform_descitemlist_question77desc(v){
		this.Scoreform_descitemlist_question77desc=v;
	}
	this.setScoreform_descitemlist_question77desc=setScoreform_descitemlist_question77desc;

	this.Scoreform_descitemlist_question78=null;


	function getScoreform_descitemlist_question78() {
		return this.Scoreform_descitemlist_question78;
	}
	this.getScoreform_descitemlist_question78=getScoreform_descitemlist_question78;


	function setScoreform_descitemlist_question78(v){
		this.Scoreform_descitemlist_question78=v;
	}
	this.setScoreform_descitemlist_question78=setScoreform_descitemlist_question78;

	this.Scoreform_descitemlist_question79=null;


	function getScoreform_descitemlist_question79() {
		return this.Scoreform_descitemlist_question79;
	}
	this.getScoreform_descitemlist_question79=getScoreform_descitemlist_question79;


	function setScoreform_descitemlist_question79(v){
		this.Scoreform_descitemlist_question79=v;
	}
	this.setScoreform_descitemlist_question79=setScoreform_descitemlist_question79;

	this.Scoreform_descitemlist_question79desc=null;


	function getScoreform_descitemlist_question79desc() {
		return this.Scoreform_descitemlist_question79desc;
	}
	this.getScoreform_descitemlist_question79desc=getScoreform_descitemlist_question79desc;


	function setScoreform_descitemlist_question79desc(v){
		this.Scoreform_descitemlist_question79desc=v;
	}
	this.setScoreform_descitemlist_question79desc=setScoreform_descitemlist_question79desc;

	this.Scoreform_descitemlist_question80=null;


	function getScoreform_descitemlist_question80() {
		return this.Scoreform_descitemlist_question80;
	}
	this.getScoreform_descitemlist_question80=getScoreform_descitemlist_question80;


	function setScoreform_descitemlist_question80(v){
		this.Scoreform_descitemlist_question80=v;
	}
	this.setScoreform_descitemlist_question80=setScoreform_descitemlist_question80;

	this.Scoreform_descitemlist_question81=null;


	function getScoreform_descitemlist_question81() {
		return this.Scoreform_descitemlist_question81;
	}
	this.getScoreform_descitemlist_question81=getScoreform_descitemlist_question81;


	function setScoreform_descitemlist_question81(v){
		this.Scoreform_descitemlist_question81=v;
	}
	this.setScoreform_descitemlist_question81=setScoreform_descitemlist_question81;

	this.Scoreform_descitemlist_question82=null;


	function getScoreform_descitemlist_question82() {
		return this.Scoreform_descitemlist_question82;
	}
	this.getScoreform_descitemlist_question82=getScoreform_descitemlist_question82;


	function setScoreform_descitemlist_question82(v){
		this.Scoreform_descitemlist_question82=v;
	}
	this.setScoreform_descitemlist_question82=setScoreform_descitemlist_question82;

	this.Scoreform_descitemlist_question83=null;


	function getScoreform_descitemlist_question83() {
		return this.Scoreform_descitemlist_question83;
	}
	this.getScoreform_descitemlist_question83=getScoreform_descitemlist_question83;


	function setScoreform_descitemlist_question83(v){
		this.Scoreform_descitemlist_question83=v;
	}
	this.setScoreform_descitemlist_question83=setScoreform_descitemlist_question83;

	this.Scoreform_descitemlist_question83desc=null;


	function getScoreform_descitemlist_question83desc() {
		return this.Scoreform_descitemlist_question83desc;
	}
	this.getScoreform_descitemlist_question83desc=getScoreform_descitemlist_question83desc;


	function setScoreform_descitemlist_question83desc(v){
		this.Scoreform_descitemlist_question83desc=v;
	}
	this.setScoreform_descitemlist_question83desc=setScoreform_descitemlist_question83desc;

	this.Scoreform_descitemlist_question84=null;


	function getScoreform_descitemlist_question84() {
		return this.Scoreform_descitemlist_question84;
	}
	this.getScoreform_descitemlist_question84=getScoreform_descitemlist_question84;


	function setScoreform_descitemlist_question84(v){
		this.Scoreform_descitemlist_question84=v;
	}
	this.setScoreform_descitemlist_question84=setScoreform_descitemlist_question84;

	this.Scoreform_descitemlist_question84desc=null;


	function getScoreform_descitemlist_question84desc() {
		return this.Scoreform_descitemlist_question84desc;
	}
	this.getScoreform_descitemlist_question84desc=getScoreform_descitemlist_question84desc;


	function setScoreform_descitemlist_question84desc(v){
		this.Scoreform_descitemlist_question84desc=v;
	}
	this.setScoreform_descitemlist_question84desc=setScoreform_descitemlist_question84desc;

	this.Scoreform_descitemlist_question85=null;


	function getScoreform_descitemlist_question85() {
		return this.Scoreform_descitemlist_question85;
	}
	this.getScoreform_descitemlist_question85=getScoreform_descitemlist_question85;


	function setScoreform_descitemlist_question85(v){
		this.Scoreform_descitemlist_question85=v;
	}
	this.setScoreform_descitemlist_question85=setScoreform_descitemlist_question85;

	this.Scoreform_descitemlist_question85desc=null;


	function getScoreform_descitemlist_question85desc() {
		return this.Scoreform_descitemlist_question85desc;
	}
	this.getScoreform_descitemlist_question85desc=getScoreform_descitemlist_question85desc;


	function setScoreform_descitemlist_question85desc(v){
		this.Scoreform_descitemlist_question85desc=v;
	}
	this.setScoreform_descitemlist_question85desc=setScoreform_descitemlist_question85desc;

	this.Scoreform_descitemlist_question86=null;


	function getScoreform_descitemlist_question86() {
		return this.Scoreform_descitemlist_question86;
	}
	this.getScoreform_descitemlist_question86=getScoreform_descitemlist_question86;


	function setScoreform_descitemlist_question86(v){
		this.Scoreform_descitemlist_question86=v;
	}
	this.setScoreform_descitemlist_question86=setScoreform_descitemlist_question86;

	this.Scoreform_descitemlist_question87=null;


	function getScoreform_descitemlist_question87() {
		return this.Scoreform_descitemlist_question87;
	}
	this.getScoreform_descitemlist_question87=getScoreform_descitemlist_question87;


	function setScoreform_descitemlist_question87(v){
		this.Scoreform_descitemlist_question87=v;
	}
	this.setScoreform_descitemlist_question87=setScoreform_descitemlist_question87;

	this.Scoreform_descitemlist_question88=null;


	function getScoreform_descitemlist_question88() {
		return this.Scoreform_descitemlist_question88;
	}
	this.getScoreform_descitemlist_question88=getScoreform_descitemlist_question88;


	function setScoreform_descitemlist_question88(v){
		this.Scoreform_descitemlist_question88=v;
	}
	this.setScoreform_descitemlist_question88=setScoreform_descitemlist_question88;

	this.Scoreform_descitemlist_question89=null;


	function getScoreform_descitemlist_question89() {
		return this.Scoreform_descitemlist_question89;
	}
	this.getScoreform_descitemlist_question89=getScoreform_descitemlist_question89;


	function setScoreform_descitemlist_question89(v){
		this.Scoreform_descitemlist_question89=v;
	}
	this.setScoreform_descitemlist_question89=setScoreform_descitemlist_question89;

	this.Scoreform_descitemlist_question90=null;


	function getScoreform_descitemlist_question90() {
		return this.Scoreform_descitemlist_question90;
	}
	this.getScoreform_descitemlist_question90=getScoreform_descitemlist_question90;


	function setScoreform_descitemlist_question90(v){
		this.Scoreform_descitemlist_question90=v;
	}
	this.setScoreform_descitemlist_question90=setScoreform_descitemlist_question90;

	this.Scoreform_descitemlist_question91=null;


	function getScoreform_descitemlist_question91() {
		return this.Scoreform_descitemlist_question91;
	}
	this.getScoreform_descitemlist_question91=getScoreform_descitemlist_question91;


	function setScoreform_descitemlist_question91(v){
		this.Scoreform_descitemlist_question91=v;
	}
	this.setScoreform_descitemlist_question91=setScoreform_descitemlist_question91;

	this.Scoreform_descitemlist_question92=null;


	function getScoreform_descitemlist_question92() {
		return this.Scoreform_descitemlist_question92;
	}
	this.getScoreform_descitemlist_question92=getScoreform_descitemlist_question92;


	function setScoreform_descitemlist_question92(v){
		this.Scoreform_descitemlist_question92=v;
	}
	this.setScoreform_descitemlist_question92=setScoreform_descitemlist_question92;

	this.Scoreform_descitemlist_question92desc=null;


	function getScoreform_descitemlist_question92desc() {
		return this.Scoreform_descitemlist_question92desc;
	}
	this.getScoreform_descitemlist_question92desc=getScoreform_descitemlist_question92desc;


	function setScoreform_descitemlist_question92desc(v){
		this.Scoreform_descitemlist_question92desc=v;
	}
	this.setScoreform_descitemlist_question92desc=setScoreform_descitemlist_question92desc;

	this.Scoreform_descitemlist_question93=null;


	function getScoreform_descitemlist_question93() {
		return this.Scoreform_descitemlist_question93;
	}
	this.getScoreform_descitemlist_question93=getScoreform_descitemlist_question93;


	function setScoreform_descitemlist_question93(v){
		this.Scoreform_descitemlist_question93=v;
	}
	this.setScoreform_descitemlist_question93=setScoreform_descitemlist_question93;

	this.Scoreform_descitemlist_question94=null;


	function getScoreform_descitemlist_question94() {
		return this.Scoreform_descitemlist_question94;
	}
	this.getScoreform_descitemlist_question94=getScoreform_descitemlist_question94;


	function setScoreform_descitemlist_question94(v){
		this.Scoreform_descitemlist_question94=v;
	}
	this.setScoreform_descitemlist_question94=setScoreform_descitemlist_question94;

	this.Scoreform_descitemlist_question95=null;


	function getScoreform_descitemlist_question95() {
		return this.Scoreform_descitemlist_question95;
	}
	this.getScoreform_descitemlist_question95=getScoreform_descitemlist_question95;


	function setScoreform_descitemlist_question95(v){
		this.Scoreform_descitemlist_question95=v;
	}
	this.setScoreform_descitemlist_question95=setScoreform_descitemlist_question95;

	this.Scoreform_descitemlist_question96=null;


	function getScoreform_descitemlist_question96() {
		return this.Scoreform_descitemlist_question96;
	}
	this.getScoreform_descitemlist_question96=getScoreform_descitemlist_question96;


	function setScoreform_descitemlist_question96(v){
		this.Scoreform_descitemlist_question96=v;
	}
	this.setScoreform_descitemlist_question96=setScoreform_descitemlist_question96;

	this.Scoreform_descitemlist_question97=null;


	function getScoreform_descitemlist_question97() {
		return this.Scoreform_descitemlist_question97;
	}
	this.getScoreform_descitemlist_question97=getScoreform_descitemlist_question97;


	function setScoreform_descitemlist_question97(v){
		this.Scoreform_descitemlist_question97=v;
	}
	this.setScoreform_descitemlist_question97=setScoreform_descitemlist_question97;

	this.Scoreform_descitemlist_question98=null;


	function getScoreform_descitemlist_question98() {
		return this.Scoreform_descitemlist_question98;
	}
	this.getScoreform_descitemlist_question98=getScoreform_descitemlist_question98;


	function setScoreform_descitemlist_question98(v){
		this.Scoreform_descitemlist_question98=v;
	}
	this.setScoreform_descitemlist_question98=setScoreform_descitemlist_question98;

	this.Scoreform_descitemlist_question99=null;


	function getScoreform_descitemlist_question99() {
		return this.Scoreform_descitemlist_question99;
	}
	this.getScoreform_descitemlist_question99=getScoreform_descitemlist_question99;


	function setScoreform_descitemlist_question99(v){
		this.Scoreform_descitemlist_question99=v;
	}
	this.setScoreform_descitemlist_question99=setScoreform_descitemlist_question99;

	this.Scoreform_descitemlist_question100=null;


	function getScoreform_descitemlist_question100() {
		return this.Scoreform_descitemlist_question100;
	}
	this.getScoreform_descitemlist_question100=getScoreform_descitemlist_question100;


	function setScoreform_descitemlist_question100(v){
		this.Scoreform_descitemlist_question100=v;
	}
	this.setScoreform_descitemlist_question100=setScoreform_descitemlist_question100;

	this.Scoreform_descitemlist_question100desc=null;


	function getScoreform_descitemlist_question100desc() {
		return this.Scoreform_descitemlist_question100desc;
	}
	this.getScoreform_descitemlist_question100desc=getScoreform_descitemlist_question100desc;


	function setScoreform_descitemlist_question100desc(v){
		this.Scoreform_descitemlist_question100desc=v;
	}
	this.setScoreform_descitemlist_question100desc=setScoreform_descitemlist_question100desc;

	this.Scoreform_descitemlist_question101=null;


	function getScoreform_descitemlist_question101() {
		return this.Scoreform_descitemlist_question101;
	}
	this.getScoreform_descitemlist_question101=getScoreform_descitemlist_question101;


	function setScoreform_descitemlist_question101(v){
		this.Scoreform_descitemlist_question101=v;
	}
	this.setScoreform_descitemlist_question101=setScoreform_descitemlist_question101;

	this.Scoreform_descitemlist_question102=null;


	function getScoreform_descitemlist_question102() {
		return this.Scoreform_descitemlist_question102;
	}
	this.getScoreform_descitemlist_question102=getScoreform_descitemlist_question102;


	function setScoreform_descitemlist_question102(v){
		this.Scoreform_descitemlist_question102=v;
	}
	this.setScoreform_descitemlist_question102=setScoreform_descitemlist_question102;

	this.Scoreform_descitemlist_question103=null;


	function getScoreform_descitemlist_question103() {
		return this.Scoreform_descitemlist_question103;
	}
	this.getScoreform_descitemlist_question103=getScoreform_descitemlist_question103;


	function setScoreform_descitemlist_question103(v){
		this.Scoreform_descitemlist_question103=v;
	}
	this.setScoreform_descitemlist_question103=setScoreform_descitemlist_question103;

	this.Scoreform_descitemlist_question104=null;


	function getScoreform_descitemlist_question104() {
		return this.Scoreform_descitemlist_question104;
	}
	this.getScoreform_descitemlist_question104=getScoreform_descitemlist_question104;


	function setScoreform_descitemlist_question104(v){
		this.Scoreform_descitemlist_question104=v;
	}
	this.setScoreform_descitemlist_question104=setScoreform_descitemlist_question104;

	this.Scoreform_descitemlist_question105=null;


	function getScoreform_descitemlist_question105() {
		return this.Scoreform_descitemlist_question105;
	}
	this.getScoreform_descitemlist_question105=getScoreform_descitemlist_question105;


	function setScoreform_descitemlist_question105(v){
		this.Scoreform_descitemlist_question105=v;
	}
	this.setScoreform_descitemlist_question105=setScoreform_descitemlist_question105;

	this.Scoreform_descitemlist_question105desc=null;


	function getScoreform_descitemlist_question105desc() {
		return this.Scoreform_descitemlist_question105desc;
	}
	this.getScoreform_descitemlist_question105desc=getScoreform_descitemlist_question105desc;


	function setScoreform_descitemlist_question105desc(v){
		this.Scoreform_descitemlist_question105desc=v;
	}
	this.setScoreform_descitemlist_question105desc=setScoreform_descitemlist_question105desc;

	this.Scoreform_descitemlist_question106=null;


	function getScoreform_descitemlist_question106() {
		return this.Scoreform_descitemlist_question106;
	}
	this.getScoreform_descitemlist_question106=getScoreform_descitemlist_question106;


	function setScoreform_descitemlist_question106(v){
		this.Scoreform_descitemlist_question106=v;
	}
	this.setScoreform_descitemlist_question106=setScoreform_descitemlist_question106;

	this.Scoreform_descitemlist_question107=null;


	function getScoreform_descitemlist_question107() {
		return this.Scoreform_descitemlist_question107;
	}
	this.getScoreform_descitemlist_question107=getScoreform_descitemlist_question107;


	function setScoreform_descitemlist_question107(v){
		this.Scoreform_descitemlist_question107=v;
	}
	this.setScoreform_descitemlist_question107=setScoreform_descitemlist_question107;

	this.Scoreform_descitemlist_question108=null;


	function getScoreform_descitemlist_question108() {
		return this.Scoreform_descitemlist_question108;
	}
	this.getScoreform_descitemlist_question108=getScoreform_descitemlist_question108;


	function setScoreform_descitemlist_question108(v){
		this.Scoreform_descitemlist_question108=v;
	}
	this.setScoreform_descitemlist_question108=setScoreform_descitemlist_question108;

	this.Scoreform_descitemlist_question109=null;


	function getScoreform_descitemlist_question109() {
		return this.Scoreform_descitemlist_question109;
	}
	this.getScoreform_descitemlist_question109=getScoreform_descitemlist_question109;


	function setScoreform_descitemlist_question109(v){
		this.Scoreform_descitemlist_question109=v;
	}
	this.setScoreform_descitemlist_question109=setScoreform_descitemlist_question109;

	this.Scoreform_descitemlist_question110=null;


	function getScoreform_descitemlist_question110() {
		return this.Scoreform_descitemlist_question110;
	}
	this.getScoreform_descitemlist_question110=getScoreform_descitemlist_question110;


	function setScoreform_descitemlist_question110(v){
		this.Scoreform_descitemlist_question110=v;
	}
	this.setScoreform_descitemlist_question110=setScoreform_descitemlist_question110;

	this.Scoreform_descitemlist_question111=null;


	function getScoreform_descitemlist_question111() {
		return this.Scoreform_descitemlist_question111;
	}
	this.getScoreform_descitemlist_question111=getScoreform_descitemlist_question111;


	function setScoreform_descitemlist_question111(v){
		this.Scoreform_descitemlist_question111=v;
	}
	this.setScoreform_descitemlist_question111=setScoreform_descitemlist_question111;

	this.Scoreform_descitemlist_question112=null;


	function getScoreform_descitemlist_question112() {
		return this.Scoreform_descitemlist_question112;
	}
	this.getScoreform_descitemlist_question112=getScoreform_descitemlist_question112;


	function setScoreform_descitemlist_question112(v){
		this.Scoreform_descitemlist_question112=v;
	}
	this.setScoreform_descitemlist_question112=setScoreform_descitemlist_question112;

	this.Scoreform_descitemlist_question113q1=null;


	function getScoreform_descitemlist_question113q1() {
		return this.Scoreform_descitemlist_question113q1;
	}
	this.getScoreform_descitemlist_question113q1=getScoreform_descitemlist_question113q1;


	function setScoreform_descitemlist_question113q1(v){
		this.Scoreform_descitemlist_question113q1=v;
	}
	this.setScoreform_descitemlist_question113q1=setScoreform_descitemlist_question113q1;

	this.Scoreform_descitemlist_question113a1=null;


	function getScoreform_descitemlist_question113a1() {
		return this.Scoreform_descitemlist_question113a1;
	}
	this.getScoreform_descitemlist_question113a1=getScoreform_descitemlist_question113a1;


	function setScoreform_descitemlist_question113a1(v){
		this.Scoreform_descitemlist_question113a1=v;
	}
	this.setScoreform_descitemlist_question113a1=setScoreform_descitemlist_question113a1;

	this.Scoreform_descitemlist_question113q2=null;


	function getScoreform_descitemlist_question113q2() {
		return this.Scoreform_descitemlist_question113q2;
	}
	this.getScoreform_descitemlist_question113q2=getScoreform_descitemlist_question113q2;


	function setScoreform_descitemlist_question113q2(v){
		this.Scoreform_descitemlist_question113q2=v;
	}
	this.setScoreform_descitemlist_question113q2=setScoreform_descitemlist_question113q2;

	this.Scoreform_descitemlist_question113a2=null;


	function getScoreform_descitemlist_question113a2() {
		return this.Scoreform_descitemlist_question113a2;
	}
	this.getScoreform_descitemlist_question113a2=getScoreform_descitemlist_question113a2;


	function setScoreform_descitemlist_question113a2(v){
		this.Scoreform_descitemlist_question113a2=v;
	}
	this.setScoreform_descitemlist_question113a2=setScoreform_descitemlist_question113a2;

	this.Scoreform_descitemlist_question113q3=null;


	function getScoreform_descitemlist_question113q3() {
		return this.Scoreform_descitemlist_question113q3;
	}
	this.getScoreform_descitemlist_question113q3=getScoreform_descitemlist_question113q3;


	function setScoreform_descitemlist_question113q3(v){
		this.Scoreform_descitemlist_question113q3=v;
	}
	this.setScoreform_descitemlist_question113q3=setScoreform_descitemlist_question113q3;

	this.Scoreform_descitemlist_question113a3=null;


	function getScoreform_descitemlist_question113a3() {
		return this.Scoreform_descitemlist_question113a3;
	}
	this.getScoreform_descitemlist_question113a3=getScoreform_descitemlist_question113a3;


	function setScoreform_descitemlist_question113a3(v){
		this.Scoreform_descitemlist_question113a3=v;
	}
	this.setScoreform_descitemlist_question113a3=setScoreform_descitemlist_question113a3;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="anxdp_raw"){
				return this.AnxdpRaw ;
			} else 
			if(xmlPath=="anxdp_t"){
				return this.AnxdpT ;
			} else 
			if(xmlPath=="anxdp_note"){
				return this.AnxdpNote ;
			} else 
			if(xmlPath=="wthdp_raw"){
				return this.WthdpRaw ;
			} else 
			if(xmlPath=="wthdp_t"){
				return this.WthdpT ;
			} else 
			if(xmlPath=="wthdp_note"){
				return this.WthdpNote ;
			} else 
			if(xmlPath=="som_raw"){
				return this.SomRaw ;
			} else 
			if(xmlPath=="som_t"){
				return this.SomT ;
			} else 
			if(xmlPath=="som_note"){
				return this.SomNote ;
			} else 
			if(xmlPath=="soc_raw"){
				return this.SocRaw ;
			} else 
			if(xmlPath=="soc_t"){
				return this.SocT ;
			} else 
			if(xmlPath=="soc_note"){
				return this.SocNote ;
			} else 
			if(xmlPath=="tho_raw"){
				return this.ThoRaw ;
			} else 
			if(xmlPath=="tho_t"){
				return this.ThoT ;
			} else 
			if(xmlPath=="tho_note"){
				return this.ThoNote ;
			} else 
			if(xmlPath=="att_raw"){
				return this.AttRaw ;
			} else 
			if(xmlPath=="att_t"){
				return this.AttT ;
			} else 
			if(xmlPath=="att_note"){
				return this.AttNote ;
			} else 
			if(xmlPath=="rule_raw"){
				return this.RuleRaw ;
			} else 
			if(xmlPath=="rule_t"){
				return this.RuleT ;
			} else 
			if(xmlPath=="rule_note"){
				return this.RuleNote ;
			} else 
			if(xmlPath=="agg_raw"){
				return this.AggRaw ;
			} else 
			if(xmlPath=="agg_t"){
				return this.AggT ;
			} else 
			if(xmlPath=="agg_note"){
				return this.AggNote ;
			} else 
			if(xmlPath=="ScoreForm/respRltnSubj"){
				return this.Scoreform_resprltnsubj ;
			} else 
			if(xmlPath=="ScoreForm/respGender"){
				return this.Scoreform_respgender ;
			} else 
			if(xmlPath=="ScoreForm/fatherWork"){
				return this.Scoreform_fatherwork ;
			} else 
			if(xmlPath=="ScoreForm/motherWork"){
				return this.Scoreform_motherwork ;
			} else 
			if(xmlPath=="ScoreForm/sectionI/sportsTopica"){
				return this.Scoreform_sectioni_sportstopica ;
			} else 
			if(xmlPath=="ScoreForm/sectionI/sportsTopicb"){
				return this.Scoreform_sectioni_sportstopicb ;
			} else 
			if(xmlPath=="ScoreForm/sectionI/sportsTopicc"){
				return this.Scoreform_sectioni_sportstopicc ;
			} else 
			if(xmlPath=="ScoreForm/sectionI/sportsTimea"){
				return this.Scoreform_sectioni_sportstimea ;
			} else 
			if(xmlPath=="ScoreForm/sectionI/sportsTimeb"){
				return this.Scoreform_sectioni_sportstimeb ;
			} else 
			if(xmlPath=="ScoreForm/sectionI/sportsTimec"){
				return this.Scoreform_sectioni_sportstimec ;
			} else 
			if(xmlPath=="ScoreForm/sectionI/sportsDoWella"){
				return this.Scoreform_sectioni_sportsdowella ;
			} else 
			if(xmlPath=="ScoreForm/sectionI/sportsDoWellb"){
				return this.Scoreform_sectioni_sportsdowellb ;
			} else 
			if(xmlPath=="ScoreForm/sectionI/sportsDoWellc"){
				return this.Scoreform_sectioni_sportsdowellc ;
			} else 
			if(xmlPath=="ScoreForm/sectionII/hobbiesTopica"){
				return this.Scoreform_sectionii_hobbiestopica ;
			} else 
			if(xmlPath=="ScoreForm/sectionII/hobbiesTopicb"){
				return this.Scoreform_sectionii_hobbiestopicb ;
			} else 
			if(xmlPath=="ScoreForm/sectionII/hobbiesTopicc"){
				return this.Scoreform_sectionii_hobbiestopicc ;
			} else 
			if(xmlPath=="ScoreForm/sectionII/hobbiesTimea"){
				return this.Scoreform_sectionii_hobbiestimea ;
			} else 
			if(xmlPath=="ScoreForm/sectionII/hobbiesTimeb"){
				return this.Scoreform_sectionii_hobbiestimeb ;
			} else 
			if(xmlPath=="ScoreForm/sectionII/hobbiesTimec"){
				return this.Scoreform_sectionii_hobbiestimec ;
			} else 
			if(xmlPath=="ScoreForm/sectionII/hobbiesDoWella"){
				return this.Scoreform_sectionii_hobbiesdowella ;
			} else 
			if(xmlPath=="ScoreForm/sectionII/hobbiesDoWellb"){
				return this.Scoreform_sectionii_hobbiesdowellb ;
			} else 
			if(xmlPath=="ScoreForm/sectionII/hobbiesDoWellc"){
				return this.Scoreform_sectionii_hobbiesdowellc ;
			} else 
			if(xmlPath=="ScoreForm/sectionIII/orgsTopica"){
				return this.Scoreform_sectioniii_orgstopica ;
			} else 
			if(xmlPath=="ScoreForm/sectionIII/orgsTopicb"){
				return this.Scoreform_sectioniii_orgstopicb ;
			} else 
			if(xmlPath=="ScoreForm/sectionIII/orgsTopicc"){
				return this.Scoreform_sectioniii_orgstopicc ;
			} else 
			if(xmlPath=="ScoreForm/sectionIII/orgsActivea"){
				return this.Scoreform_sectioniii_orgsactivea ;
			} else 
			if(xmlPath=="ScoreForm/sectionIII/orgsActiveb"){
				return this.Scoreform_sectioniii_orgsactiveb ;
			} else 
			if(xmlPath=="ScoreForm/sectionIII/orgsActivec"){
				return this.Scoreform_sectioniii_orgsactivec ;
			} else 
			if(xmlPath=="ScoreForm/sectionIV/jobsTopica"){
				return this.Scoreform_sectioniv_jobstopica ;
			} else 
			if(xmlPath=="ScoreForm/sectionIV/jobsTopicb"){
				return this.Scoreform_sectioniv_jobstopicb ;
			} else 
			if(xmlPath=="ScoreForm/sectionIV/jobsTopicc"){
				return this.Scoreform_sectioniv_jobstopicc ;
			} else 
			if(xmlPath=="ScoreForm/sectionIV/jobsWella"){
				return this.Scoreform_sectioniv_jobswella ;
			} else 
			if(xmlPath=="ScoreForm/sectionIV/jobsWellb"){
				return this.Scoreform_sectioniv_jobswellb ;
			} else 
			if(xmlPath=="ScoreForm/sectionIV/jobsWellc"){
				return this.Scoreform_sectioniv_jobswellc ;
			} else 
			if(xmlPath=="ScoreForm/sectionV/closeFriends"){
				return this.Scoreform_sectionv_closefriends ;
			} else 
			if(xmlPath=="ScoreForm/sectionV/friendTimes"){
				return this.Scoreform_sectionv_friendtimes ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/getAlonga"){
				return this.Scoreform_sectionvi_getalonga ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/getAlongb"){
				return this.Scoreform_sectionvi_getalongb ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/getAlongc"){
				return this.Scoreform_sectionvi_getalongc ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/getAlongd"){
				return this.Scoreform_sectionvi_getalongd ;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/academPerfNoSchool"){
				return this.Scoreform_sectionvii_academperfnoschool ;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/academPerfa"){
				return this.Scoreform_sectionvii_academperfa ;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/academPerfb"){
				return this.Scoreform_sectionvii_academperfb ;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/academPerfc"){
				return this.Scoreform_sectionvii_academperfc ;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/academPerfd"){
				return this.Scoreform_sectionvii_academperfd ;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/academPerfeTopic"){
				return this.Scoreform_sectionvii_academperfetopic ;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/academPerfe"){
				return this.Scoreform_sectionvii_academperfe ;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/academPerffTopic"){
				return this.Scoreform_sectionvii_academperfftopic ;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/academPerff"){
				return this.Scoreform_sectionvii_academperff ;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/academPerfgTopic"){
				return this.Scoreform_sectionvii_academperfgtopic ;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/academPerfg"){
				return this.Scoreform_sectionvii_academperfg ;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/specEd"){
				return this.Scoreform_sectionvii_speced ;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/specEdKind"){
				return this.Scoreform_sectionvii_specedkind ;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/repeatGrade"){
				return this.Scoreform_sectionvii_repeatgrade ;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/repeatGradeReason"){
				return this.Scoreform_sectionvii_repeatgradereason ;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/schoolProb"){
				return this.Scoreform_sectionvii_schoolprob ;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/schoolProbDesc"){
				return this.Scoreform_sectionvii_schoolprobdesc ;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/schoolProbStartDate"){
				return this.Scoreform_sectionvii_schoolprobstartdate ;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/schoolProbEnd"){
				return this.Scoreform_sectionvii_schoolprobend ;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/schoolProbEndDate"){
				return this.Scoreform_sectionvii_schoolprobenddate ;
			} else 
			if(xmlPath=="ScoreForm/illDisable"){
				return this.Scoreform_illdisable ;
			} else 
			if(xmlPath=="ScoreForm/illDisableDesc"){
				return this.Scoreform_illdisabledesc ;
			} else 
			if(xmlPath=="ScoreForm/concernMost"){
				return this.Scoreform_concernmost ;
			} else 
			if(xmlPath=="ScoreForm/bestThings"){
				return this.Scoreform_bestthings ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question1"){
				return this.Scoreform_descitemlist_question1 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question2"){
				return this.Scoreform_descitemlist_question2 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question2Desc"){
				return this.Scoreform_descitemlist_question2desc ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question3"){
				return this.Scoreform_descitemlist_question3 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question4"){
				return this.Scoreform_descitemlist_question4 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question5"){
				return this.Scoreform_descitemlist_question5 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question6"){
				return this.Scoreform_descitemlist_question6 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question7"){
				return this.Scoreform_descitemlist_question7 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question8"){
				return this.Scoreform_descitemlist_question8 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question9"){
				return this.Scoreform_descitemlist_question9 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question9Desc"){
				return this.Scoreform_descitemlist_question9desc ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question10"){
				return this.Scoreform_descitemlist_question10 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question11"){
				return this.Scoreform_descitemlist_question11 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question12"){
				return this.Scoreform_descitemlist_question12 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question13"){
				return this.Scoreform_descitemlist_question13 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question14"){
				return this.Scoreform_descitemlist_question14 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question15"){
				return this.Scoreform_descitemlist_question15 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question16"){
				return this.Scoreform_descitemlist_question16 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question17"){
				return this.Scoreform_descitemlist_question17 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question18"){
				return this.Scoreform_descitemlist_question18 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question19"){
				return this.Scoreform_descitemlist_question19 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question20"){
				return this.Scoreform_descitemlist_question20 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question21"){
				return this.Scoreform_descitemlist_question21 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question22"){
				return this.Scoreform_descitemlist_question22 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question23"){
				return this.Scoreform_descitemlist_question23 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question24"){
				return this.Scoreform_descitemlist_question24 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question25"){
				return this.Scoreform_descitemlist_question25 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question26"){
				return this.Scoreform_descitemlist_question26 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question27"){
				return this.Scoreform_descitemlist_question27 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question28"){
				return this.Scoreform_descitemlist_question28 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question29"){
				return this.Scoreform_descitemlist_question29 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question29Desc"){
				return this.Scoreform_descitemlist_question29desc ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question30"){
				return this.Scoreform_descitemlist_question30 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question31"){
				return this.Scoreform_descitemlist_question31 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question32"){
				return this.Scoreform_descitemlist_question32 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question33"){
				return this.Scoreform_descitemlist_question33 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question34"){
				return this.Scoreform_descitemlist_question34 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question35"){
				return this.Scoreform_descitemlist_question35 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question36"){
				return this.Scoreform_descitemlist_question36 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question37"){
				return this.Scoreform_descitemlist_question37 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question38"){
				return this.Scoreform_descitemlist_question38 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question39"){
				return this.Scoreform_descitemlist_question39 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question40"){
				return this.Scoreform_descitemlist_question40 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question40Desc"){
				return this.Scoreform_descitemlist_question40desc ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question41"){
				return this.Scoreform_descitemlist_question41 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question42"){
				return this.Scoreform_descitemlist_question42 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question43"){
				return this.Scoreform_descitemlist_question43 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question44"){
				return this.Scoreform_descitemlist_question44 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question45"){
				return this.Scoreform_descitemlist_question45 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question46"){
				return this.Scoreform_descitemlist_question46 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question46Desc"){
				return this.Scoreform_descitemlist_question46desc ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question47"){
				return this.Scoreform_descitemlist_question47 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question48"){
				return this.Scoreform_descitemlist_question48 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question49"){
				return this.Scoreform_descitemlist_question49 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question50"){
				return this.Scoreform_descitemlist_question50 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question51"){
				return this.Scoreform_descitemlist_question51 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question52"){
				return this.Scoreform_descitemlist_question52 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question53"){
				return this.Scoreform_descitemlist_question53 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question54"){
				return this.Scoreform_descitemlist_question54 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question55"){
				return this.Scoreform_descitemlist_question55 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question56a"){
				return this.Scoreform_descitemlist_question56a ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question56b"){
				return this.Scoreform_descitemlist_question56b ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question56c"){
				return this.Scoreform_descitemlist_question56c ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question56d"){
				return this.Scoreform_descitemlist_question56d ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question56dDesc"){
				return this.Scoreform_descitemlist_question56ddesc ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question56e"){
				return this.Scoreform_descitemlist_question56e ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question56f"){
				return this.Scoreform_descitemlist_question56f ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question56g"){
				return this.Scoreform_descitemlist_question56g ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question56h"){
				return this.Scoreform_descitemlist_question56h ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question57"){
				return this.Scoreform_descitemlist_question57 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question58"){
				return this.Scoreform_descitemlist_question58 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question58Desc"){
				return this.Scoreform_descitemlist_question58desc ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question59"){
				return this.Scoreform_descitemlist_question59 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question60"){
				return this.Scoreform_descitemlist_question60 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question61"){
				return this.Scoreform_descitemlist_question61 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question62"){
				return this.Scoreform_descitemlist_question62 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question63"){
				return this.Scoreform_descitemlist_question63 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question64"){
				return this.Scoreform_descitemlist_question64 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question65"){
				return this.Scoreform_descitemlist_question65 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question66"){
				return this.Scoreform_descitemlist_question66 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question66Desc"){
				return this.Scoreform_descitemlist_question66desc ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question67"){
				return this.Scoreform_descitemlist_question67 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question68"){
				return this.Scoreform_descitemlist_question68 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question69"){
				return this.Scoreform_descitemlist_question69 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question70"){
				return this.Scoreform_descitemlist_question70 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question70Desc"){
				return this.Scoreform_descitemlist_question70desc ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question71"){
				return this.Scoreform_descitemlist_question71 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question72"){
				return this.Scoreform_descitemlist_question72 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question73"){
				return this.Scoreform_descitemlist_question73 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question73Desc"){
				return this.Scoreform_descitemlist_question73desc ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question74"){
				return this.Scoreform_descitemlist_question74 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question75"){
				return this.Scoreform_descitemlist_question75 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question76"){
				return this.Scoreform_descitemlist_question76 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question77"){
				return this.Scoreform_descitemlist_question77 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question77Desc"){
				return this.Scoreform_descitemlist_question77desc ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question78"){
				return this.Scoreform_descitemlist_question78 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question79"){
				return this.Scoreform_descitemlist_question79 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question79Desc"){
				return this.Scoreform_descitemlist_question79desc ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question80"){
				return this.Scoreform_descitemlist_question80 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question81"){
				return this.Scoreform_descitemlist_question81 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question82"){
				return this.Scoreform_descitemlist_question82 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question83"){
				return this.Scoreform_descitemlist_question83 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question83Desc"){
				return this.Scoreform_descitemlist_question83desc ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question84"){
				return this.Scoreform_descitemlist_question84 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question84Desc"){
				return this.Scoreform_descitemlist_question84desc ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question85"){
				return this.Scoreform_descitemlist_question85 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question85Desc"){
				return this.Scoreform_descitemlist_question85desc ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question86"){
				return this.Scoreform_descitemlist_question86 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question87"){
				return this.Scoreform_descitemlist_question87 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question88"){
				return this.Scoreform_descitemlist_question88 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question89"){
				return this.Scoreform_descitemlist_question89 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question90"){
				return this.Scoreform_descitemlist_question90 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question91"){
				return this.Scoreform_descitemlist_question91 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question92"){
				return this.Scoreform_descitemlist_question92 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question92Desc"){
				return this.Scoreform_descitemlist_question92desc ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question93"){
				return this.Scoreform_descitemlist_question93 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question94"){
				return this.Scoreform_descitemlist_question94 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question95"){
				return this.Scoreform_descitemlist_question95 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question96"){
				return this.Scoreform_descitemlist_question96 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question97"){
				return this.Scoreform_descitemlist_question97 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question98"){
				return this.Scoreform_descitemlist_question98 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question99"){
				return this.Scoreform_descitemlist_question99 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question100"){
				return this.Scoreform_descitemlist_question100 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question100Desc"){
				return this.Scoreform_descitemlist_question100desc ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question101"){
				return this.Scoreform_descitemlist_question101 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question102"){
				return this.Scoreform_descitemlist_question102 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question103"){
				return this.Scoreform_descitemlist_question103 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question104"){
				return this.Scoreform_descitemlist_question104 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question105"){
				return this.Scoreform_descitemlist_question105 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question105Desc"){
				return this.Scoreform_descitemlist_question105desc ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question106"){
				return this.Scoreform_descitemlist_question106 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question107"){
				return this.Scoreform_descitemlist_question107 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question108"){
				return this.Scoreform_descitemlist_question108 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question109"){
				return this.Scoreform_descitemlist_question109 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question110"){
				return this.Scoreform_descitemlist_question110 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question111"){
				return this.Scoreform_descitemlist_question111 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question112"){
				return this.Scoreform_descitemlist_question112 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question113Q1"){
				return this.Scoreform_descitemlist_question113q1 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question113A1"){
				return this.Scoreform_descitemlist_question113a1 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question113Q2"){
				return this.Scoreform_descitemlist_question113q2 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question113A2"){
				return this.Scoreform_descitemlist_question113a2 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question113Q3"){
				return this.Scoreform_descitemlist_question113q3 ;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question113A3"){
				return this.Scoreform_descitemlist_question113a3 ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="anxdp_raw"){
				this.AnxdpRaw=value;
			} else 
			if(xmlPath=="anxdp_t"){
				this.AnxdpT=value;
			} else 
			if(xmlPath=="anxdp_note"){
				this.AnxdpNote=value;
			} else 
			if(xmlPath=="wthdp_raw"){
				this.WthdpRaw=value;
			} else 
			if(xmlPath=="wthdp_t"){
				this.WthdpT=value;
			} else 
			if(xmlPath=="wthdp_note"){
				this.WthdpNote=value;
			} else 
			if(xmlPath=="som_raw"){
				this.SomRaw=value;
			} else 
			if(xmlPath=="som_t"){
				this.SomT=value;
			} else 
			if(xmlPath=="som_note"){
				this.SomNote=value;
			} else 
			if(xmlPath=="soc_raw"){
				this.SocRaw=value;
			} else 
			if(xmlPath=="soc_t"){
				this.SocT=value;
			} else 
			if(xmlPath=="soc_note"){
				this.SocNote=value;
			} else 
			if(xmlPath=="tho_raw"){
				this.ThoRaw=value;
			} else 
			if(xmlPath=="tho_t"){
				this.ThoT=value;
			} else 
			if(xmlPath=="tho_note"){
				this.ThoNote=value;
			} else 
			if(xmlPath=="att_raw"){
				this.AttRaw=value;
			} else 
			if(xmlPath=="att_t"){
				this.AttT=value;
			} else 
			if(xmlPath=="att_note"){
				this.AttNote=value;
			} else 
			if(xmlPath=="rule_raw"){
				this.RuleRaw=value;
			} else 
			if(xmlPath=="rule_t"){
				this.RuleT=value;
			} else 
			if(xmlPath=="rule_note"){
				this.RuleNote=value;
			} else 
			if(xmlPath=="agg_raw"){
				this.AggRaw=value;
			} else 
			if(xmlPath=="agg_t"){
				this.AggT=value;
			} else 
			if(xmlPath=="agg_note"){
				this.AggNote=value;
			} else 
			if(xmlPath=="ScoreForm/respRltnSubj"){
				this.Scoreform_resprltnsubj=value;
			} else 
			if(xmlPath=="ScoreForm/respGender"){
				this.Scoreform_respgender=value;
			} else 
			if(xmlPath=="ScoreForm/fatherWork"){
				this.Scoreform_fatherwork=value;
			} else 
			if(xmlPath=="ScoreForm/motherWork"){
				this.Scoreform_motherwork=value;
			} else 
			if(xmlPath=="ScoreForm/sectionI/sportsTopica"){
				this.Scoreform_sectioni_sportstopica=value;
			} else 
			if(xmlPath=="ScoreForm/sectionI/sportsTopicb"){
				this.Scoreform_sectioni_sportstopicb=value;
			} else 
			if(xmlPath=="ScoreForm/sectionI/sportsTopicc"){
				this.Scoreform_sectioni_sportstopicc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionI/sportsTimea"){
				this.Scoreform_sectioni_sportstimea=value;
			} else 
			if(xmlPath=="ScoreForm/sectionI/sportsTimeb"){
				this.Scoreform_sectioni_sportstimeb=value;
			} else 
			if(xmlPath=="ScoreForm/sectionI/sportsTimec"){
				this.Scoreform_sectioni_sportstimec=value;
			} else 
			if(xmlPath=="ScoreForm/sectionI/sportsDoWella"){
				this.Scoreform_sectioni_sportsdowella=value;
			} else 
			if(xmlPath=="ScoreForm/sectionI/sportsDoWellb"){
				this.Scoreform_sectioni_sportsdowellb=value;
			} else 
			if(xmlPath=="ScoreForm/sectionI/sportsDoWellc"){
				this.Scoreform_sectioni_sportsdowellc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionII/hobbiesTopica"){
				this.Scoreform_sectionii_hobbiestopica=value;
			} else 
			if(xmlPath=="ScoreForm/sectionII/hobbiesTopicb"){
				this.Scoreform_sectionii_hobbiestopicb=value;
			} else 
			if(xmlPath=="ScoreForm/sectionII/hobbiesTopicc"){
				this.Scoreform_sectionii_hobbiestopicc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionII/hobbiesTimea"){
				this.Scoreform_sectionii_hobbiestimea=value;
			} else 
			if(xmlPath=="ScoreForm/sectionII/hobbiesTimeb"){
				this.Scoreform_sectionii_hobbiestimeb=value;
			} else 
			if(xmlPath=="ScoreForm/sectionII/hobbiesTimec"){
				this.Scoreform_sectionii_hobbiestimec=value;
			} else 
			if(xmlPath=="ScoreForm/sectionII/hobbiesDoWella"){
				this.Scoreform_sectionii_hobbiesdowella=value;
			} else 
			if(xmlPath=="ScoreForm/sectionII/hobbiesDoWellb"){
				this.Scoreform_sectionii_hobbiesdowellb=value;
			} else 
			if(xmlPath=="ScoreForm/sectionII/hobbiesDoWellc"){
				this.Scoreform_sectionii_hobbiesdowellc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionIII/orgsTopica"){
				this.Scoreform_sectioniii_orgstopica=value;
			} else 
			if(xmlPath=="ScoreForm/sectionIII/orgsTopicb"){
				this.Scoreform_sectioniii_orgstopicb=value;
			} else 
			if(xmlPath=="ScoreForm/sectionIII/orgsTopicc"){
				this.Scoreform_sectioniii_orgstopicc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionIII/orgsActivea"){
				this.Scoreform_sectioniii_orgsactivea=value;
			} else 
			if(xmlPath=="ScoreForm/sectionIII/orgsActiveb"){
				this.Scoreform_sectioniii_orgsactiveb=value;
			} else 
			if(xmlPath=="ScoreForm/sectionIII/orgsActivec"){
				this.Scoreform_sectioniii_orgsactivec=value;
			} else 
			if(xmlPath=="ScoreForm/sectionIV/jobsTopica"){
				this.Scoreform_sectioniv_jobstopica=value;
			} else 
			if(xmlPath=="ScoreForm/sectionIV/jobsTopicb"){
				this.Scoreform_sectioniv_jobstopicb=value;
			} else 
			if(xmlPath=="ScoreForm/sectionIV/jobsTopicc"){
				this.Scoreform_sectioniv_jobstopicc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionIV/jobsWella"){
				this.Scoreform_sectioniv_jobswella=value;
			} else 
			if(xmlPath=="ScoreForm/sectionIV/jobsWellb"){
				this.Scoreform_sectioniv_jobswellb=value;
			} else 
			if(xmlPath=="ScoreForm/sectionIV/jobsWellc"){
				this.Scoreform_sectioniv_jobswellc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionV/closeFriends"){
				this.Scoreform_sectionv_closefriends=value;
			} else 
			if(xmlPath=="ScoreForm/sectionV/friendTimes"){
				this.Scoreform_sectionv_friendtimes=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/getAlonga"){
				this.Scoreform_sectionvi_getalonga=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/getAlongb"){
				this.Scoreform_sectionvi_getalongb=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/getAlongc"){
				this.Scoreform_sectionvi_getalongc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/getAlongd"){
				this.Scoreform_sectionvi_getalongd=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/academPerfNoSchool"){
				this.Scoreform_sectionvii_academperfnoschool=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/academPerfa"){
				this.Scoreform_sectionvii_academperfa=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/academPerfb"){
				this.Scoreform_sectionvii_academperfb=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/academPerfc"){
				this.Scoreform_sectionvii_academperfc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/academPerfd"){
				this.Scoreform_sectionvii_academperfd=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/academPerfeTopic"){
				this.Scoreform_sectionvii_academperfetopic=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/academPerfe"){
				this.Scoreform_sectionvii_academperfe=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/academPerffTopic"){
				this.Scoreform_sectionvii_academperfftopic=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/academPerff"){
				this.Scoreform_sectionvii_academperff=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/academPerfgTopic"){
				this.Scoreform_sectionvii_academperfgtopic=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/academPerfg"){
				this.Scoreform_sectionvii_academperfg=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/specEd"){
				this.Scoreform_sectionvii_speced=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/specEdKind"){
				this.Scoreform_sectionvii_specedkind=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/repeatGrade"){
				this.Scoreform_sectionvii_repeatgrade=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/repeatGradeReason"){
				this.Scoreform_sectionvii_repeatgradereason=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/schoolProb"){
				this.Scoreform_sectionvii_schoolprob=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/schoolProbDesc"){
				this.Scoreform_sectionvii_schoolprobdesc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/schoolProbStartDate"){
				this.Scoreform_sectionvii_schoolprobstartdate=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/schoolProbEnd"){
				this.Scoreform_sectionvii_schoolprobend=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVII/schoolProbEndDate"){
				this.Scoreform_sectionvii_schoolprobenddate=value;
			} else 
			if(xmlPath=="ScoreForm/illDisable"){
				this.Scoreform_illdisable=value;
			} else 
			if(xmlPath=="ScoreForm/illDisableDesc"){
				this.Scoreform_illdisabledesc=value;
			} else 
			if(xmlPath=="ScoreForm/concernMost"){
				this.Scoreform_concernmost=value;
			} else 
			if(xmlPath=="ScoreForm/bestThings"){
				this.Scoreform_bestthings=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question1"){
				this.Scoreform_descitemlist_question1=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question2"){
				this.Scoreform_descitemlist_question2=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question2Desc"){
				this.Scoreform_descitemlist_question2desc=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question3"){
				this.Scoreform_descitemlist_question3=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question4"){
				this.Scoreform_descitemlist_question4=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question5"){
				this.Scoreform_descitemlist_question5=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question6"){
				this.Scoreform_descitemlist_question6=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question7"){
				this.Scoreform_descitemlist_question7=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question8"){
				this.Scoreform_descitemlist_question8=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question9"){
				this.Scoreform_descitemlist_question9=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question9Desc"){
				this.Scoreform_descitemlist_question9desc=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question10"){
				this.Scoreform_descitemlist_question10=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question11"){
				this.Scoreform_descitemlist_question11=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question12"){
				this.Scoreform_descitemlist_question12=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question13"){
				this.Scoreform_descitemlist_question13=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question14"){
				this.Scoreform_descitemlist_question14=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question15"){
				this.Scoreform_descitemlist_question15=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question16"){
				this.Scoreform_descitemlist_question16=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question17"){
				this.Scoreform_descitemlist_question17=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question18"){
				this.Scoreform_descitemlist_question18=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question19"){
				this.Scoreform_descitemlist_question19=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question20"){
				this.Scoreform_descitemlist_question20=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question21"){
				this.Scoreform_descitemlist_question21=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question22"){
				this.Scoreform_descitemlist_question22=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question23"){
				this.Scoreform_descitemlist_question23=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question24"){
				this.Scoreform_descitemlist_question24=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question25"){
				this.Scoreform_descitemlist_question25=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question26"){
				this.Scoreform_descitemlist_question26=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question27"){
				this.Scoreform_descitemlist_question27=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question28"){
				this.Scoreform_descitemlist_question28=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question29"){
				this.Scoreform_descitemlist_question29=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question29Desc"){
				this.Scoreform_descitemlist_question29desc=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question30"){
				this.Scoreform_descitemlist_question30=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question31"){
				this.Scoreform_descitemlist_question31=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question32"){
				this.Scoreform_descitemlist_question32=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question33"){
				this.Scoreform_descitemlist_question33=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question34"){
				this.Scoreform_descitemlist_question34=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question35"){
				this.Scoreform_descitemlist_question35=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question36"){
				this.Scoreform_descitemlist_question36=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question37"){
				this.Scoreform_descitemlist_question37=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question38"){
				this.Scoreform_descitemlist_question38=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question39"){
				this.Scoreform_descitemlist_question39=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question40"){
				this.Scoreform_descitemlist_question40=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question40Desc"){
				this.Scoreform_descitemlist_question40desc=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question41"){
				this.Scoreform_descitemlist_question41=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question42"){
				this.Scoreform_descitemlist_question42=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question43"){
				this.Scoreform_descitemlist_question43=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question44"){
				this.Scoreform_descitemlist_question44=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question45"){
				this.Scoreform_descitemlist_question45=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question46"){
				this.Scoreform_descitemlist_question46=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question46Desc"){
				this.Scoreform_descitemlist_question46desc=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question47"){
				this.Scoreform_descitemlist_question47=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question48"){
				this.Scoreform_descitemlist_question48=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question49"){
				this.Scoreform_descitemlist_question49=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question50"){
				this.Scoreform_descitemlist_question50=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question51"){
				this.Scoreform_descitemlist_question51=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question52"){
				this.Scoreform_descitemlist_question52=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question53"){
				this.Scoreform_descitemlist_question53=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question54"){
				this.Scoreform_descitemlist_question54=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question55"){
				this.Scoreform_descitemlist_question55=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question56a"){
				this.Scoreform_descitemlist_question56a=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question56b"){
				this.Scoreform_descitemlist_question56b=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question56c"){
				this.Scoreform_descitemlist_question56c=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question56d"){
				this.Scoreform_descitemlist_question56d=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question56dDesc"){
				this.Scoreform_descitemlist_question56ddesc=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question56e"){
				this.Scoreform_descitemlist_question56e=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question56f"){
				this.Scoreform_descitemlist_question56f=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question56g"){
				this.Scoreform_descitemlist_question56g=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question56h"){
				this.Scoreform_descitemlist_question56h=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question57"){
				this.Scoreform_descitemlist_question57=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question58"){
				this.Scoreform_descitemlist_question58=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question58Desc"){
				this.Scoreform_descitemlist_question58desc=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question59"){
				this.Scoreform_descitemlist_question59=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question60"){
				this.Scoreform_descitemlist_question60=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question61"){
				this.Scoreform_descitemlist_question61=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question62"){
				this.Scoreform_descitemlist_question62=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question63"){
				this.Scoreform_descitemlist_question63=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question64"){
				this.Scoreform_descitemlist_question64=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question65"){
				this.Scoreform_descitemlist_question65=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question66"){
				this.Scoreform_descitemlist_question66=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question66Desc"){
				this.Scoreform_descitemlist_question66desc=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question67"){
				this.Scoreform_descitemlist_question67=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question68"){
				this.Scoreform_descitemlist_question68=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question69"){
				this.Scoreform_descitemlist_question69=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question70"){
				this.Scoreform_descitemlist_question70=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question70Desc"){
				this.Scoreform_descitemlist_question70desc=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question71"){
				this.Scoreform_descitemlist_question71=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question72"){
				this.Scoreform_descitemlist_question72=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question73"){
				this.Scoreform_descitemlist_question73=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question73Desc"){
				this.Scoreform_descitemlist_question73desc=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question74"){
				this.Scoreform_descitemlist_question74=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question75"){
				this.Scoreform_descitemlist_question75=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question76"){
				this.Scoreform_descitemlist_question76=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question77"){
				this.Scoreform_descitemlist_question77=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question77Desc"){
				this.Scoreform_descitemlist_question77desc=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question78"){
				this.Scoreform_descitemlist_question78=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question79"){
				this.Scoreform_descitemlist_question79=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question79Desc"){
				this.Scoreform_descitemlist_question79desc=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question80"){
				this.Scoreform_descitemlist_question80=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question81"){
				this.Scoreform_descitemlist_question81=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question82"){
				this.Scoreform_descitemlist_question82=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question83"){
				this.Scoreform_descitemlist_question83=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question83Desc"){
				this.Scoreform_descitemlist_question83desc=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question84"){
				this.Scoreform_descitemlist_question84=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question84Desc"){
				this.Scoreform_descitemlist_question84desc=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question85"){
				this.Scoreform_descitemlist_question85=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question85Desc"){
				this.Scoreform_descitemlist_question85desc=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question86"){
				this.Scoreform_descitemlist_question86=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question87"){
				this.Scoreform_descitemlist_question87=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question88"){
				this.Scoreform_descitemlist_question88=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question89"){
				this.Scoreform_descitemlist_question89=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question90"){
				this.Scoreform_descitemlist_question90=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question91"){
				this.Scoreform_descitemlist_question91=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question92"){
				this.Scoreform_descitemlist_question92=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question92Desc"){
				this.Scoreform_descitemlist_question92desc=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question93"){
				this.Scoreform_descitemlist_question93=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question94"){
				this.Scoreform_descitemlist_question94=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question95"){
				this.Scoreform_descitemlist_question95=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question96"){
				this.Scoreform_descitemlist_question96=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question97"){
				this.Scoreform_descitemlist_question97=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question98"){
				this.Scoreform_descitemlist_question98=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question99"){
				this.Scoreform_descitemlist_question99=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question100"){
				this.Scoreform_descitemlist_question100=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question100Desc"){
				this.Scoreform_descitemlist_question100desc=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question101"){
				this.Scoreform_descitemlist_question101=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question102"){
				this.Scoreform_descitemlist_question102=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question103"){
				this.Scoreform_descitemlist_question103=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question104"){
				this.Scoreform_descitemlist_question104=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question105"){
				this.Scoreform_descitemlist_question105=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question105Desc"){
				this.Scoreform_descitemlist_question105desc=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question106"){
				this.Scoreform_descitemlist_question106=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question107"){
				this.Scoreform_descitemlist_question107=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question108"){
				this.Scoreform_descitemlist_question108=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question109"){
				this.Scoreform_descitemlist_question109=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question110"){
				this.Scoreform_descitemlist_question110=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question111"){
				this.Scoreform_descitemlist_question111=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question112"){
				this.Scoreform_descitemlist_question112=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question113Q1"){
				this.Scoreform_descitemlist_question113q1=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question113A1"){
				this.Scoreform_descitemlist_question113a1=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question113Q2"){
				this.Scoreform_descitemlist_question113q2=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question113A2"){
				this.Scoreform_descitemlist_question113a2=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question113Q3"){
				this.Scoreform_descitemlist_question113q3=value;
			} else 
			if(xmlPath=="ScoreForm/descItemList/question113A3"){
				this.Scoreform_descitemlist_question113a3=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="anxdp_raw"){
			return "field_data";
		}else if (xmlPath=="anxdp_t"){
			return "field_data";
		}else if (xmlPath=="anxdp_note"){
			return "field_data";
		}else if (xmlPath=="wthdp_raw"){
			return "field_data";
		}else if (xmlPath=="wthdp_t"){
			return "field_data";
		}else if (xmlPath=="wthdp_note"){
			return "field_data";
		}else if (xmlPath=="som_raw"){
			return "field_data";
		}else if (xmlPath=="som_t"){
			return "field_data";
		}else if (xmlPath=="som_note"){
			return "field_data";
		}else if (xmlPath=="soc_raw"){
			return "field_data";
		}else if (xmlPath=="soc_t"){
			return "field_data";
		}else if (xmlPath=="soc_note"){
			return "field_data";
		}else if (xmlPath=="tho_raw"){
			return "field_data";
		}else if (xmlPath=="tho_t"){
			return "field_data";
		}else if (xmlPath=="tho_note"){
			return "field_data";
		}else if (xmlPath=="att_raw"){
			return "field_data";
		}else if (xmlPath=="att_t"){
			return "field_data";
		}else if (xmlPath=="att_note"){
			return "field_data";
		}else if (xmlPath=="rule_raw"){
			return "field_data";
		}else if (xmlPath=="rule_t"){
			return "field_data";
		}else if (xmlPath=="rule_note"){
			return "field_data";
		}else if (xmlPath=="agg_raw"){
			return "field_data";
		}else if (xmlPath=="agg_t"){
			return "field_data";
		}else if (xmlPath=="agg_note"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/respRltnSubj"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/respGender"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/fatherWork"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/motherWork"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionI/sportsTopica"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionI/sportsTopicb"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionI/sportsTopicc"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionI/sportsTimea"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionI/sportsTimeb"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionI/sportsTimec"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionI/sportsDoWella"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionI/sportsDoWellb"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionI/sportsDoWellc"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionII/hobbiesTopica"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionII/hobbiesTopicb"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionII/hobbiesTopicc"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionII/hobbiesTimea"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionII/hobbiesTimeb"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionII/hobbiesTimec"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionII/hobbiesDoWella"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionII/hobbiesDoWellb"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionII/hobbiesDoWellc"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionIII/orgsTopica"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionIII/orgsTopicb"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionIII/orgsTopicc"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionIII/orgsActivea"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionIII/orgsActiveb"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionIII/orgsActivec"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionIV/jobsTopica"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionIV/jobsTopicb"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionIV/jobsTopicc"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionIV/jobsWella"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionIV/jobsWellb"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionIV/jobsWellc"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionV/closeFriends"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionV/friendTimes"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/getAlonga"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/getAlongb"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/getAlongc"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/getAlongd"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVII/academPerfNoSchool"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionVII/academPerfa"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVII/academPerfb"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVII/academPerfc"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVII/academPerfd"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVII/academPerfeTopic"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVII/academPerfe"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVII/academPerffTopic"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVII/academPerff"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVII/academPerfgTopic"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVII/academPerfg"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVII/specEd"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVII/specEdKind"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionVII/repeatGrade"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVII/repeatGradeReason"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionVII/schoolProb"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVII/schoolProbDesc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionVII/schoolProbStartDate"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVII/schoolProbEnd"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVII/schoolProbEndDate"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/illDisable"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/illDisableDesc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/concernMost"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/bestThings"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/descItemList/question1"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question2"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question2Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/descItemList/question3"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question4"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question5"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question6"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question7"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question8"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question9"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question9Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/descItemList/question10"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question11"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question12"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question13"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question14"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question15"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question16"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question17"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question18"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question19"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question20"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question21"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question22"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question23"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question24"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question25"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question26"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question27"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question28"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question29"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question29Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/descItemList/question30"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question31"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question32"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question33"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question34"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question35"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question36"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question37"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question38"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question39"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question40"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question40Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/descItemList/question41"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question42"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question43"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question44"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question45"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question46"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question46Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/descItemList/question47"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question48"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question49"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question50"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question51"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question52"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question53"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question54"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question55"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question56a"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question56b"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question56c"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question56d"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question56dDesc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/descItemList/question56e"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question56f"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question56g"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question56h"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/descItemList/question57"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question58"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question58Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/descItemList/question59"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question60"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question61"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question62"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question63"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question64"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question65"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question66"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question66Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/descItemList/question67"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question68"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question69"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question70"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question70Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/descItemList/question71"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question72"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question73"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question73Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/descItemList/question74"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question75"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question76"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question77"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question77Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/descItemList/question78"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question79"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question79Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/descItemList/question80"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question81"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question82"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question83"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question83Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/descItemList/question84"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question84Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/descItemList/question85"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question85Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/descItemList/question86"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question87"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question88"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question89"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question90"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question91"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question92"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question92Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/descItemList/question93"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question94"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question95"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question96"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question97"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question98"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question99"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question100"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question100Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/descItemList/question101"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question102"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question103"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question104"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question105"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question105Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/descItemList/question106"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question107"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question108"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question109"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question110"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question111"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question112"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question113Q1"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question113A1"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question113Q2"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question113A2"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question113Q3"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/descItemList/question113A3"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<bcl:CBCL6-1-01Ed201";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</bcl:CBCL6-1-01Ed201>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.AnxdpRaw!=null){
			xmlTxt+="\n<bcl:anxdp_raw";
			xmlTxt+=">";
			xmlTxt+=this.AnxdpRaw;
			xmlTxt+="</bcl:anxdp_raw>";
		}
		if (this.AnxdpT!=null){
			xmlTxt+="\n<bcl:anxdp_t";
			xmlTxt+=">";
			xmlTxt+=this.AnxdpT;
			xmlTxt+="</bcl:anxdp_t>";
		}
		if (this.AnxdpNote!=null){
			xmlTxt+="\n<bcl:anxdp_note";
			xmlTxt+=">";
			xmlTxt+=this.AnxdpNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:anxdp_note>";
		}
		if (this.WthdpRaw!=null){
			xmlTxt+="\n<bcl:wthdp_raw";
			xmlTxt+=">";
			xmlTxt+=this.WthdpRaw;
			xmlTxt+="</bcl:wthdp_raw>";
		}
		if (this.WthdpT!=null){
			xmlTxt+="\n<bcl:wthdp_t";
			xmlTxt+=">";
			xmlTxt+=this.WthdpT;
			xmlTxt+="</bcl:wthdp_t>";
		}
		if (this.WthdpNote!=null){
			xmlTxt+="\n<bcl:wthdp_note";
			xmlTxt+=">";
			xmlTxt+=this.WthdpNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:wthdp_note>";
		}
		if (this.SomRaw!=null){
			xmlTxt+="\n<bcl:som_raw";
			xmlTxt+=">";
			xmlTxt+=this.SomRaw;
			xmlTxt+="</bcl:som_raw>";
		}
		if (this.SomT!=null){
			xmlTxt+="\n<bcl:som_t";
			xmlTxt+=">";
			xmlTxt+=this.SomT;
			xmlTxt+="</bcl:som_t>";
		}
		if (this.SomNote!=null){
			xmlTxt+="\n<bcl:som_note";
			xmlTxt+=">";
			xmlTxt+=this.SomNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:som_note>";
		}
		if (this.SocRaw!=null){
			xmlTxt+="\n<bcl:soc_raw";
			xmlTxt+=">";
			xmlTxt+=this.SocRaw;
			xmlTxt+="</bcl:soc_raw>";
		}
		if (this.SocT!=null){
			xmlTxt+="\n<bcl:soc_t";
			xmlTxt+=">";
			xmlTxt+=this.SocT;
			xmlTxt+="</bcl:soc_t>";
		}
		if (this.SocNote!=null){
			xmlTxt+="\n<bcl:soc_note";
			xmlTxt+=">";
			xmlTxt+=this.SocNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:soc_note>";
		}
		if (this.ThoRaw!=null){
			xmlTxt+="\n<bcl:tho_raw";
			xmlTxt+=">";
			xmlTxt+=this.ThoRaw;
			xmlTxt+="</bcl:tho_raw>";
		}
		if (this.ThoT!=null){
			xmlTxt+="\n<bcl:tho_t";
			xmlTxt+=">";
			xmlTxt+=this.ThoT;
			xmlTxt+="</bcl:tho_t>";
		}
		if (this.ThoNote!=null){
			xmlTxt+="\n<bcl:tho_note";
			xmlTxt+=">";
			xmlTxt+=this.ThoNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:tho_note>";
		}
		if (this.AttRaw!=null){
			xmlTxt+="\n<bcl:att_raw";
			xmlTxt+=">";
			xmlTxt+=this.AttRaw;
			xmlTxt+="</bcl:att_raw>";
		}
		if (this.AttT!=null){
			xmlTxt+="\n<bcl:att_t";
			xmlTxt+=">";
			xmlTxt+=this.AttT;
			xmlTxt+="</bcl:att_t>";
		}
		if (this.AttNote!=null){
			xmlTxt+="\n<bcl:att_note";
			xmlTxt+=">";
			xmlTxt+=this.AttNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:att_note>";
		}
		if (this.RuleRaw!=null){
			xmlTxt+="\n<bcl:rule_raw";
			xmlTxt+=">";
			xmlTxt+=this.RuleRaw;
			xmlTxt+="</bcl:rule_raw>";
		}
		if (this.RuleT!=null){
			xmlTxt+="\n<bcl:rule_t";
			xmlTxt+=">";
			xmlTxt+=this.RuleT;
			xmlTxt+="</bcl:rule_t>";
		}
		if (this.RuleNote!=null){
			xmlTxt+="\n<bcl:rule_note";
			xmlTxt+=">";
			xmlTxt+=this.RuleNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:rule_note>";
		}
		if (this.AggRaw!=null){
			xmlTxt+="\n<bcl:agg_raw";
			xmlTxt+=">";
			xmlTxt+=this.AggRaw;
			xmlTxt+="</bcl:agg_raw>";
		}
		if (this.AggT!=null){
			xmlTxt+="\n<bcl:agg_t";
			xmlTxt+=">";
			xmlTxt+=this.AggT;
			xmlTxt+="</bcl:agg_t>";
		}
		if (this.AggNote!=null){
			xmlTxt+="\n<bcl:agg_note";
			xmlTxt+=">";
			xmlTxt+=this.AggNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:agg_note>";
		}
			var child0=0;
			var att0=0;
			if(this.Scoreform_descitemlist_question69!=null)
			child0++;
			if(this.Scoreform_descitemlist_question68!=null)
			child0++;
			if(this.Scoreform_sectionvii_schoolprobend!=null)
			child0++;
			if(this.Scoreform_descitemlist_question67!=null)
			child0++;
			if(this.Scoreform_descitemlist_question66!=null)
			child0++;
			if(this.Scoreform_descitemlist_question65!=null)
			child0++;
			if(this.Scoreform_descitemlist_question64!=null)
			child0++;
			if(this.Scoreform_descitemlist_question63!=null)
			child0++;
			if(this.Scoreform_sectionvii_repeatgradereason!=null)
			child0++;
			if(this.Scoreform_descitemlist_question62!=null)
			child0++;
			if(this.Scoreform_descitemlist_question61!=null)
			child0++;
			if(this.Scoreform_descitemlist_question112!=null)
			child0++;
			if(this.Scoreform_descitemlist_question60!=null)
			child0++;
			if(this.Scoreform_descitemlist_question111!=null)
			child0++;
			if(this.Scoreform_descitemlist_question110!=null)
			child0++;
			if(this.Scoreform_descitemlist_question59!=null)
			child0++;
			if(this.Scoreform_sectionii_hobbiestopicc!=null)
			child0++;
			if(this.Scoreform_descitemlist_question58!=null)
			child0++;
			if(this.Scoreform_descitemlist_question109!=null)
			child0++;
			if(this.Scoreform_sectionii_hobbiestopicb!=null)
			child0++;
			if(this.Scoreform_descitemlist_question57!=null)
			child0++;
			if(this.Scoreform_descitemlist_question108!=null)
			child0++;
			if(this.Scoreform_sectionii_hobbiestopica!=null)
			child0++;
			if(this.Scoreform_illdisabledesc!=null)
			child0++;
			if(this.Scoreform_descitemlist_question107!=null)
			child0++;
			if(this.Scoreform_descitemlist_question55!=null)
			child0++;
			if(this.Scoreform_descitemlist_question106!=null)
			child0++;
			if(this.Scoreform_descitemlist_question54!=null)
			child0++;
			if(this.Scoreform_descitemlist_question105!=null)
			child0++;
			if(this.Scoreform_descitemlist_question53!=null)
			child0++;
			if(this.Scoreform_descitemlist_question104!=null)
			child0++;
			if(this.Scoreform_descitemlist_question52!=null)
			child0++;
			if(this.Scoreform_descitemlist_question103!=null)
			child0++;
			if(this.Scoreform_descitemlist_question51!=null)
			child0++;
			if(this.Scoreform_descitemlist_question102!=null)
			child0++;
			if(this.Scoreform_descitemlist_question50!=null)
			child0++;
			if(this.Scoreform_descitemlist_question101!=null)
			child0++;
			if(this.Scoreform_descitemlist_question100!=null)
			child0++;
			if(this.Scoreform_sectionvi_getalongd!=null)
			child0++;
			if(this.Scoreform_sectioniii_orgstopicc!=null)
			child0++;
			if(this.Scoreform_sectionvi_getalongc!=null)
			child0++;
			if(this.Scoreform_sectioniii_orgstopicb!=null)
			child0++;
			if(this.Scoreform_sectionvi_getalongb!=null)
			child0++;
			if(this.Scoreform_sectioniii_orgstopica!=null)
			child0++;
			if(this.Scoreform_sectionvi_getalonga!=null)
			child0++;
			if(this.Scoreform_descitemlist_question49!=null)
			child0++;
			if(this.Scoreform_descitemlist_question48!=null)
			child0++;
			if(this.Scoreform_descitemlist_question47!=null)
			child0++;
			if(this.Scoreform_descitemlist_question46!=null)
			child0++;
			if(this.Scoreform_descitemlist_question45!=null)
			child0++;
			if(this.Scoreform_descitemlist_question44!=null)
			child0++;
			if(this.Scoreform_descitemlist_question43!=null)
			child0++;
			if(this.Scoreform_descitemlist_question42!=null)
			child0++;
			if(this.Scoreform_descitemlist_question41!=null)
			child0++;
			if(this.Scoreform_descitemlist_question40!=null)
			child0++;
			if(this.Scoreform_descitemlist_question85desc!=null)
			child0++;
			if(this.Scoreform_concernmost!=null)
			child0++;
			if(this.Scoreform_sectionvii_schoolprobstartdate!=null)
			child0++;
			if(this.Scoreform_illdisable!=null)
			child0++;
			if(this.Scoreform_sectioni_sportstopicc!=null)
			child0++;
			if(this.Scoreform_descitemlist_question39!=null)
			child0++;
			if(this.Scoreform_sectionv_friendtimes!=null)
			child0++;
			if(this.Scoreform_sectioni_sportstopicb!=null)
			child0++;
			if(this.Scoreform_sectioni_sportsdowellc!=null)
			child0++;
			if(this.Scoreform_descitemlist_question38!=null)
			child0++;
			if(this.Scoreform_sectioni_sportstopica!=null)
			child0++;
			if(this.Scoreform_sectioni_sportsdowellb!=null)
			child0++;
			if(this.Scoreform_descitemlist_question37!=null)
			child0++;
			if(this.Scoreform_sectioni_sportsdowella!=null)
			child0++;
			if(this.Scoreform_descitemlist_question36!=null)
			child0++;
			if(this.Scoreform_descitemlist_question35!=null)
			child0++;
			if(this.Scoreform_descitemlist_question34!=null)
			child0++;
			if(this.Scoreform_descitemlist_question33!=null)
			child0++;
			if(this.Scoreform_descitemlist_question32!=null)
			child0++;
			if(this.Scoreform_descitemlist_question31!=null)
			child0++;
			if(this.Scoreform_descitemlist_question30!=null)
			child0++;
			if(this.Scoreform_sectionvii_schoolprobdesc!=null)
			child0++;
			if(this.Scoreform_descitemlist_question92desc!=null)
			child0++;
			if(this.Scoreform_descitemlist_question29!=null)
			child0++;
			if(this.Scoreform_sectionvii_academperfetopic!=null)
			child0++;
			if(this.Scoreform_descitemlist_question28!=null)
			child0++;
			if(this.Scoreform_descitemlist_question27!=null)
			child0++;
			if(this.Scoreform_descitemlist_question105desc!=null)
			child0++;
			if(this.Scoreform_descitemlist_question26!=null)
			child0++;
			if(this.Scoreform_descitemlist_question25!=null)
			child0++;
			if(this.Scoreform_descitemlist_question24!=null)
			child0++;
			if(this.Scoreform_descitemlist_question23!=null)
			child0++;
			if(this.Scoreform_descitemlist_question22!=null)
			child0++;
			if(this.Scoreform_sectioniv_jobswellc!=null)
			child0++;
			if(this.Scoreform_descitemlist_question21!=null)
			child0++;
			if(this.Scoreform_sectioniv_jobswellb!=null)
			child0++;
			if(this.Scoreform_descitemlist_question20!=null)
			child0++;
			if(this.Scoreform_sectioniv_jobswella!=null)
			child0++;
			if(this.Scoreform_descitemlist_question70desc!=null)
			child0++;
			if(this.Scoreform_sectioniv_jobstopicc!=null)
			child0++;
			if(this.Scoreform_sectioniv_jobstopicb!=null)
			child0++;
			if(this.Scoreform_sectioniv_jobstopica!=null)
			child0++;
			if(this.Scoreform_descitemlist_question77desc!=null)
			child0++;
			if(this.Scoreform_sectionv_closefriends!=null)
			child0++;
			if(this.Scoreform_descitemlist_question66desc!=null)
			child0++;
			if(this.Scoreform_descitemlist_question19!=null)
			child0++;
			if(this.Scoreform_descitemlist_question18!=null)
			child0++;
			if(this.Scoreform_descitemlist_question17!=null)
			child0++;
			if(this.Scoreform_descitemlist_question16!=null)
			child0++;
			if(this.Scoreform_descitemlist_question15!=null)
			child0++;
			if(this.Scoreform_descitemlist_question14!=null)
			child0++;
			if(this.Scoreform_descitemlist_question13!=null)
			child0++;
			if(this.Scoreform_descitemlist_question12!=null)
			child0++;
			if(this.Scoreform_descitemlist_question11!=null)
			child0++;
			if(this.Scoreform_descitemlist_question10!=null)
			child0++;
			if(this.Scoreform_respgender!=null)
			child0++;
			if(this.Scoreform_sectionvii_schoolprobenddate!=null)
			child0++;
			if(this.Scoreform_descitemlist_question56h!=null)
			child0++;
			if(this.Scoreform_descitemlist_question56g!=null)
			child0++;
			if(this.Scoreform_descitemlist_question56f!=null)
			child0++;
			if(this.Scoreform_descitemlist_question56e!=null)
			child0++;
			if(this.Scoreform_descitemlist_question56d!=null)
			child0++;
			if(this.Scoreform_sectionvii_repeatgrade!=null)
			child0++;
			if(this.Scoreform_descitemlist_question56c!=null)
			child0++;
			if(this.Scoreform_descitemlist_question56b!=null)
			child0++;
			if(this.Scoreform_descitemlist_question56a!=null)
			child0++;
			if(this.Scoreform_descitemlist_question84desc!=null)
			child0++;
			if(this.Scoreform_sectionvii_specedkind!=null)
			child0++;
			if(this.Scoreform_descitemlist_question113q3!=null)
			child0++;
			if(this.Scoreform_descitemlist_question73desc!=null)
			child0++;
			if(this.Scoreform_descitemlist_question113q2!=null)
			child0++;
			if(this.Scoreform_descitemlist_question56ddesc!=null)
			child0++;
			if(this.Scoreform_descitemlist_question113q1!=null)
			child0++;
			if(this.Scoreform_descitemlist_question29desc!=null)
			child0++;
			if(this.Scoreform_descitemlist_question2desc!=null)
			child0++;
			if(this.Scoreform_fatherwork!=null)
			child0++;
			if(this.Scoreform_descitemlist_question9desc!=null)
			child0++;
			if(this.Scoreform_descitemlist_question40desc!=null)
			child0++;
			if(this.Scoreform_descitemlist_question58desc!=null)
			child0++;
			if(this.Scoreform_sectionvii_academperfftopic!=null)
			child0++;
			if(this.Scoreform_sectionii_hobbiestimec!=null)
			child0++;
			if(this.Scoreform_sectionvii_schoolprob!=null)
			child0++;
			if(this.Scoreform_sectionii_hobbiestimeb!=null)
			child0++;
			if(this.Scoreform_sectionii_hobbiestimea!=null)
			child0++;
			if(this.Scoreform_sectioniii_orgsactivec!=null)
			child0++;
			if(this.Scoreform_sectioniii_orgsactiveb!=null)
			child0++;
			if(this.Scoreform_sectioniii_orgsactivea!=null)
			child0++;
			if(this.Scoreform_sectionvii_academperfnoschool!=null)
			child0++;
			if(this.Scoreform_descitemlist_question9!=null)
			child0++;
			if(this.Scoreform_descitemlist_question8!=null)
			child0++;
			if(this.Scoreform_descitemlist_question7!=null)
			child0++;
			if(this.Scoreform_descitemlist_question6!=null)
			child0++;
			if(this.Scoreform_descitemlist_question5!=null)
			child0++;
			if(this.Scoreform_descitemlist_question4!=null)
			child0++;
			if(this.Scoreform_descitemlist_question3!=null)
			child0++;
			if(this.Scoreform_descitemlist_question2!=null)
			child0++;
			if(this.Scoreform_descitemlist_question1!=null)
			child0++;
			if(this.Scoreform_descitemlist_question100desc!=null)
			child0++;
			if(this.Scoreform_sectionvii_academperfg!=null)
			child0++;
			if(this.Scoreform_sectioni_sportstimec!=null)
			child0++;
			if(this.Scoreform_sectionvii_academperff!=null)
			child0++;
			if(this.Scoreform_sectioni_sportstimeb!=null)
			child0++;
			if(this.Scoreform_sectionvii_academperfe!=null)
			child0++;
			if(this.Scoreform_sectioni_sportstimea!=null)
			child0++;
			if(this.Scoreform_descitemlist_question99!=null)
			child0++;
			if(this.Scoreform_sectionvii_academperfd!=null)
			child0++;
			if(this.Scoreform_descitemlist_question98!=null)
			child0++;
			if(this.Scoreform_sectionvii_academperfc!=null)
			child0++;
			if(this.Scoreform_sectionii_hobbiesdowellc!=null)
			child0++;
			if(this.Scoreform_descitemlist_question97!=null)
			child0++;
			if(this.Scoreform_sectionvii_academperfb!=null)
			child0++;
			if(this.Scoreform_descitemlist_question83desc!=null)
			child0++;
			if(this.Scoreform_sectionii_hobbiesdowellb!=null)
			child0++;
			if(this.Scoreform_descitemlist_question96!=null)
			child0++;
			if(this.Scoreform_sectionvii_academperfa!=null)
			child0++;
			if(this.Scoreform_sectionii_hobbiesdowella!=null)
			child0++;
			if(this.Scoreform_descitemlist_question95!=null)
			child0++;
			if(this.Scoreform_descitemlist_question94!=null)
			child0++;
			if(this.Scoreform_descitemlist_question93!=null)
			child0++;
			if(this.Scoreform_descitemlist_question92!=null)
			child0++;
			if(this.Scoreform_descitemlist_question91!=null)
			child0++;
			if(this.Scoreform_descitemlist_question90!=null)
			child0++;
			if(this.Scoreform_descitemlist_question113a3!=null)
			child0++;
			if(this.Scoreform_descitemlist_question113a2!=null)
			child0++;
			if(this.Scoreform_descitemlist_question113a1!=null)
			child0++;
			if(this.Scoreform_descitemlist_question79desc!=null)
			child0++;
			if(this.Scoreform_descitemlist_question89!=null)
			child0++;
			if(this.Scoreform_descitemlist_question88!=null)
			child0++;
			if(this.Scoreform_descitemlist_question87!=null)
			child0++;
			if(this.Scoreform_sectionvii_academperfgtopic!=null)
			child0++;
			if(this.Scoreform_descitemlist_question86!=null)
			child0++;
			if(this.Scoreform_descitemlist_question85!=null)
			child0++;
			if(this.Scoreform_descitemlist_question84!=null)
			child0++;
			if(this.Scoreform_motherwork!=null)
			child0++;
			if(this.Scoreform_descitemlist_question83!=null)
			child0++;
			if(this.Scoreform_descitemlist_question82!=null)
			child0++;
			if(this.Scoreform_descitemlist_question81!=null)
			child0++;
			if(this.Scoreform_descitemlist_question80!=null)
			child0++;
			if(this.Scoreform_descitemlist_question46desc!=null)
			child0++;
			if(this.Scoreform_sectionvii_speced!=null)
			child0++;
			if(this.Scoreform_descitemlist_question79!=null)
			child0++;
			if(this.Scoreform_descitemlist_question78!=null)
			child0++;
			if(this.Scoreform_descitemlist_question77!=null)
			child0++;
			if(this.Scoreform_descitemlist_question76!=null)
			child0++;
			if(this.Scoreform_descitemlist_question75!=null)
			child0++;
			if(this.Scoreform_descitemlist_question74!=null)
			child0++;
			if(this.Scoreform_bestthings!=null)
			child0++;
			if(this.Scoreform_descitemlist_question73!=null)
			child0++;
			if(this.Scoreform_descitemlist_question72!=null)
			child0++;
			if(this.Scoreform_descitemlist_question71!=null)
			child0++;
			if(this.Scoreform_descitemlist_question70!=null)
			child0++;
			if(this.Scoreform_resprltnsubj!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<bcl:ScoreForm";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_resprltnsubj!=null){
			xmlTxt+="\n<bcl:respRltnSubj";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_resprltnsubj.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:respRltnSubj>";
		}
		if (this.Scoreform_respgender!=null){
			xmlTxt+="\n<bcl:respGender";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_respgender.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:respGender>";
		}
		if (this.Scoreform_fatherwork!=null){
			xmlTxt+="\n<bcl:fatherWork";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_fatherwork.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:fatherWork>";
		}
		if (this.Scoreform_motherwork!=null){
			xmlTxt+="\n<bcl:motherWork";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_motherwork.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:motherWork>";
		}
			var child1=0;
			var att1=0;
			if(this.Scoreform_sectioni_sportsdowellc!=null)
			child1++;
			if(this.Scoreform_sectioni_sportsdowellb!=null)
			child1++;
			if(this.Scoreform_sectioni_sportsdowella!=null)
			child1++;
			if(this.Scoreform_sectioni_sportstimec!=null)
			child1++;
			if(this.Scoreform_sectioni_sportstopicc!=null)
			child1++;
			if(this.Scoreform_sectioni_sportstimeb!=null)
			child1++;
			if(this.Scoreform_sectioni_sportstopicb!=null)
			child1++;
			if(this.Scoreform_sectioni_sportstimea!=null)
			child1++;
			if(this.Scoreform_sectioni_sportstopica!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<bcl:sectionI";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectioni_sportstopica!=null){
			xmlTxt+="\n<bcl:sportsTopica";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioni_sportstopica.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:sportsTopica>";
		}
		if (this.Scoreform_sectioni_sportstopicb!=null){
			xmlTxt+="\n<bcl:sportsTopicb";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioni_sportstopicb.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:sportsTopicb>";
		}
		if (this.Scoreform_sectioni_sportstopicc!=null){
			xmlTxt+="\n<bcl:sportsTopicc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioni_sportstopicc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:sportsTopicc>";
		}
		if (this.Scoreform_sectioni_sportstimea!=null){
			xmlTxt+="\n<bcl:sportsTimea";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioni_sportstimea;
			xmlTxt+="</bcl:sportsTimea>";
		}
		if (this.Scoreform_sectioni_sportstimeb!=null){
			xmlTxt+="\n<bcl:sportsTimeb";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioni_sportstimeb;
			xmlTxt+="</bcl:sportsTimeb>";
		}
		if (this.Scoreform_sectioni_sportstimec!=null){
			xmlTxt+="\n<bcl:sportsTimec";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioni_sportstimec;
			xmlTxt+="</bcl:sportsTimec>";
		}
		if (this.Scoreform_sectioni_sportsdowella!=null){
			xmlTxt+="\n<bcl:sportsDoWella";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioni_sportsdowella;
			xmlTxt+="</bcl:sportsDoWella>";
		}
		if (this.Scoreform_sectioni_sportsdowellb!=null){
			xmlTxt+="\n<bcl:sportsDoWellb";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioni_sportsdowellb;
			xmlTxt+="</bcl:sportsDoWellb>";
		}
		if (this.Scoreform_sectioni_sportsdowellc!=null){
			xmlTxt+="\n<bcl:sportsDoWellc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioni_sportsdowellc;
			xmlTxt+="</bcl:sportsDoWellc>";
		}
				xmlTxt+="\n</bcl:sectionI>";
			}
			}

			var child2=0;
			var att2=0;
			if(this.Scoreform_sectionii_hobbiestopicc!=null)
			child2++;
			if(this.Scoreform_sectionii_hobbiestopicb!=null)
			child2++;
			if(this.Scoreform_sectionii_hobbiestopica!=null)
			child2++;
			if(this.Scoreform_sectionii_hobbiesdowellc!=null)
			child2++;
			if(this.Scoreform_sectionii_hobbiesdowellb!=null)
			child2++;
			if(this.Scoreform_sectionii_hobbiestimec!=null)
			child2++;
			if(this.Scoreform_sectionii_hobbiesdowella!=null)
			child2++;
			if(this.Scoreform_sectionii_hobbiestimeb!=null)
			child2++;
			if(this.Scoreform_sectionii_hobbiestimea!=null)
			child2++;
			if(child2>0 || att2>0){
				xmlTxt+="\n<bcl:sectionII";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionii_hobbiestopica!=null){
			xmlTxt+="\n<bcl:hobbiesTopica";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionii_hobbiestopica.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:hobbiesTopica>";
		}
		if (this.Scoreform_sectionii_hobbiestopicb!=null){
			xmlTxt+="\n<bcl:hobbiesTopicb";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionii_hobbiestopicb.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:hobbiesTopicb>";
		}
		if (this.Scoreform_sectionii_hobbiestopicc!=null){
			xmlTxt+="\n<bcl:hobbiesTopicc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionii_hobbiestopicc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:hobbiesTopicc>";
		}
		if (this.Scoreform_sectionii_hobbiestimea!=null){
			xmlTxt+="\n<bcl:hobbiesTimea";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionii_hobbiestimea;
			xmlTxt+="</bcl:hobbiesTimea>";
		}
		if (this.Scoreform_sectionii_hobbiestimeb!=null){
			xmlTxt+="\n<bcl:hobbiesTimeb";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionii_hobbiestimeb;
			xmlTxt+="</bcl:hobbiesTimeb>";
		}
		if (this.Scoreform_sectionii_hobbiestimec!=null){
			xmlTxt+="\n<bcl:hobbiesTimec";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionii_hobbiestimec;
			xmlTxt+="</bcl:hobbiesTimec>";
		}
		if (this.Scoreform_sectionii_hobbiesdowella!=null){
			xmlTxt+="\n<bcl:hobbiesDoWella";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionii_hobbiesdowella;
			xmlTxt+="</bcl:hobbiesDoWella>";
		}
		if (this.Scoreform_sectionii_hobbiesdowellb!=null){
			xmlTxt+="\n<bcl:hobbiesDoWellb";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionii_hobbiesdowellb;
			xmlTxt+="</bcl:hobbiesDoWellb>";
		}
		if (this.Scoreform_sectionii_hobbiesdowellc!=null){
			xmlTxt+="\n<bcl:hobbiesDoWellc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionii_hobbiesdowellc;
			xmlTxt+="</bcl:hobbiesDoWellc>";
		}
				xmlTxt+="\n</bcl:sectionII>";
			}
			}

			var child3=0;
			var att3=0;
			if(this.Scoreform_sectioniii_orgsactivec!=null)
			child3++;
			if(this.Scoreform_sectioniii_orgsactiveb!=null)
			child3++;
			if(this.Scoreform_sectioniii_orgsactivea!=null)
			child3++;
			if(this.Scoreform_sectioniii_orgstopicc!=null)
			child3++;
			if(this.Scoreform_sectioniii_orgstopicb!=null)
			child3++;
			if(this.Scoreform_sectioniii_orgstopica!=null)
			child3++;
			if(child3>0 || att3>0){
				xmlTxt+="\n<bcl:sectionIII";
			if(child3==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectioniii_orgstopica!=null){
			xmlTxt+="\n<bcl:orgsTopica";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioniii_orgstopica.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:orgsTopica>";
		}
		if (this.Scoreform_sectioniii_orgstopicb!=null){
			xmlTxt+="\n<bcl:orgsTopicb";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioniii_orgstopicb.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:orgsTopicb>";
		}
		if (this.Scoreform_sectioniii_orgstopicc!=null){
			xmlTxt+="\n<bcl:orgsTopicc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioniii_orgstopicc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:orgsTopicc>";
		}
		if (this.Scoreform_sectioniii_orgsactivea!=null){
			xmlTxt+="\n<bcl:orgsActivea";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioniii_orgsactivea;
			xmlTxt+="</bcl:orgsActivea>";
		}
		if (this.Scoreform_sectioniii_orgsactiveb!=null){
			xmlTxt+="\n<bcl:orgsActiveb";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioniii_orgsactiveb;
			xmlTxt+="</bcl:orgsActiveb>";
		}
		if (this.Scoreform_sectioniii_orgsactivec!=null){
			xmlTxt+="\n<bcl:orgsActivec";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioniii_orgsactivec;
			xmlTxt+="</bcl:orgsActivec>";
		}
				xmlTxt+="\n</bcl:sectionIII>";
			}
			}

			var child4=0;
			var att4=0;
			if(this.Scoreform_sectioniv_jobswellc!=null)
			child4++;
			if(this.Scoreform_sectioniv_jobswellb!=null)
			child4++;
			if(this.Scoreform_sectioniv_jobswella!=null)
			child4++;
			if(this.Scoreform_sectioniv_jobstopicc!=null)
			child4++;
			if(this.Scoreform_sectioniv_jobstopicb!=null)
			child4++;
			if(this.Scoreform_sectioniv_jobstopica!=null)
			child4++;
			if(child4>0 || att4>0){
				xmlTxt+="\n<bcl:sectionIV";
			if(child4==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectioniv_jobstopica!=null){
			xmlTxt+="\n<bcl:jobsTopica";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioniv_jobstopica.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:jobsTopica>";
		}
		if (this.Scoreform_sectioniv_jobstopicb!=null){
			xmlTxt+="\n<bcl:jobsTopicb";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioniv_jobstopicb.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:jobsTopicb>";
		}
		if (this.Scoreform_sectioniv_jobstopicc!=null){
			xmlTxt+="\n<bcl:jobsTopicc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioniv_jobstopicc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:jobsTopicc>";
		}
		if (this.Scoreform_sectioniv_jobswella!=null){
			xmlTxt+="\n<bcl:jobsWella";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioniv_jobswella;
			xmlTxt+="</bcl:jobsWella>";
		}
		if (this.Scoreform_sectioniv_jobswellb!=null){
			xmlTxt+="\n<bcl:jobsWellb";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioniv_jobswellb;
			xmlTxt+="</bcl:jobsWellb>";
		}
		if (this.Scoreform_sectioniv_jobswellc!=null){
			xmlTxt+="\n<bcl:jobsWellc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioniv_jobswellc;
			xmlTxt+="</bcl:jobsWellc>";
		}
				xmlTxt+="\n</bcl:sectionIV>";
			}
			}

			var child5=0;
			var att5=0;
			if(this.Scoreform_sectionv_friendtimes!=null)
			child5++;
			if(this.Scoreform_sectionv_closefriends!=null)
			child5++;
			if(child5>0 || att5>0){
				xmlTxt+="\n<bcl:sectionV";
			if(child5==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionv_closefriends!=null){
			xmlTxt+="\n<bcl:closeFriends";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionv_closefriends;
			xmlTxt+="</bcl:closeFriends>";
		}
		if (this.Scoreform_sectionv_friendtimes!=null){
			xmlTxt+="\n<bcl:friendTimes";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionv_friendtimes;
			xmlTxt+="</bcl:friendTimes>";
		}
				xmlTxt+="\n</bcl:sectionV>";
			}
			}

			var child6=0;
			var att6=0;
			if(this.Scoreform_sectionvi_getalongd!=null)
			child6++;
			if(this.Scoreform_sectionvi_getalongc!=null)
			child6++;
			if(this.Scoreform_sectionvi_getalongb!=null)
			child6++;
			if(this.Scoreform_sectionvi_getalonga!=null)
			child6++;
			if(child6>0 || att6>0){
				xmlTxt+="\n<bcl:sectionVI";
			if(child6==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionvi_getalonga!=null){
			xmlTxt+="\n<bcl:getAlonga";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_getalonga;
			xmlTxt+="</bcl:getAlonga>";
		}
		if (this.Scoreform_sectionvi_getalongb!=null){
			xmlTxt+="\n<bcl:getAlongb";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_getalongb;
			xmlTxt+="</bcl:getAlongb>";
		}
		if (this.Scoreform_sectionvi_getalongc!=null){
			xmlTxt+="\n<bcl:getAlongc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_getalongc;
			xmlTxt+="</bcl:getAlongc>";
		}
		if (this.Scoreform_sectionvi_getalongd!=null){
			xmlTxt+="\n<bcl:getAlongd";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_getalongd;
			xmlTxt+="</bcl:getAlongd>";
		}
				xmlTxt+="\n</bcl:sectionVI>";
			}
			}

			var child7=0;
			var att7=0;
			if(this.Scoreform_sectionvii_repeatgradereason!=null)
			child7++;
			if(this.Scoreform_sectionvii_academperfftopic!=null)
			child7++;
			if(this.Scoreform_sectionvii_schoolprob!=null)
			child7++;
			if(this.Scoreform_sectionvii_academperfgtopic!=null)
			child7++;
			if(this.Scoreform_sectionvii_speced!=null)
			child7++;
			if(this.Scoreform_sectionvii_schoolprobdesc!=null)
			child7++;
			if(this.Scoreform_sectionvii_academperfg!=null)
			child7++;
			if(this.Scoreform_sectionvii_academperff!=null)
			child7++;
			if(this.Scoreform_sectionvii_academperfnoschool!=null)
			child7++;
			if(this.Scoreform_sectionvii_academperfe!=null)
			child7++;
			if(this.Scoreform_sectionvii_academperfd!=null)
			child7++;
			if(this.Scoreform_sectionvii_repeatgrade!=null)
			child7++;
			if(this.Scoreform_sectionvii_academperfc!=null)
			child7++;
			if(this.Scoreform_sectionvii_academperfb!=null)
			child7++;
			if(this.Scoreform_sectionvii_academperfa!=null)
			child7++;
			if(this.Scoreform_sectionvii_schoolprobstartdate!=null)
			child7++;
			if(this.Scoreform_sectionvii_schoolprobend!=null)
			child7++;
			if(this.Scoreform_sectionvii_specedkind!=null)
			child7++;
			if(this.Scoreform_sectionvii_schoolprobenddate!=null)
			child7++;
			if(this.Scoreform_sectionvii_academperfetopic!=null)
			child7++;
			if(child7>0 || att7>0){
				xmlTxt+="\n<bcl:sectionVII";
			if(child7==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionvii_academperfnoschool!=null){
			xmlTxt+="\n<bcl:academPerfNoSchool";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvii_academperfnoschool.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:academPerfNoSchool>";
		}
		if (this.Scoreform_sectionvii_academperfa!=null){
			xmlTxt+="\n<bcl:academPerfa";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvii_academperfa;
			xmlTxt+="</bcl:academPerfa>";
		}
		if (this.Scoreform_sectionvii_academperfb!=null){
			xmlTxt+="\n<bcl:academPerfb";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvii_academperfb;
			xmlTxt+="</bcl:academPerfb>";
		}
		if (this.Scoreform_sectionvii_academperfc!=null){
			xmlTxt+="\n<bcl:academPerfc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvii_academperfc;
			xmlTxt+="</bcl:academPerfc>";
		}
		if (this.Scoreform_sectionvii_academperfd!=null){
			xmlTxt+="\n<bcl:academPerfd";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvii_academperfd;
			xmlTxt+="</bcl:academPerfd>";
		}
		if (this.Scoreform_sectionvii_academperfetopic!=null){
			xmlTxt+="\n<bcl:academPerfeTopic";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvii_academperfetopic.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:academPerfeTopic>";
		}
		if (this.Scoreform_sectionvii_academperfe!=null){
			xmlTxt+="\n<bcl:academPerfe";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvii_academperfe;
			xmlTxt+="</bcl:academPerfe>";
		}
		if (this.Scoreform_sectionvii_academperfftopic!=null){
			xmlTxt+="\n<bcl:academPerffTopic";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvii_academperfftopic.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:academPerffTopic>";
		}
		if (this.Scoreform_sectionvii_academperff!=null){
			xmlTxt+="\n<bcl:academPerff";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvii_academperff;
			xmlTxt+="</bcl:academPerff>";
		}
		if (this.Scoreform_sectionvii_academperfgtopic!=null){
			xmlTxt+="\n<bcl:academPerfgTopic";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvii_academperfgtopic.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:academPerfgTopic>";
		}
		if (this.Scoreform_sectionvii_academperfg!=null){
			xmlTxt+="\n<bcl:academPerfg";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvii_academperfg;
			xmlTxt+="</bcl:academPerfg>";
		}
		if (this.Scoreform_sectionvii_speced!=null){
			xmlTxt+="\n<bcl:specEd";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvii_speced;
			xmlTxt+="</bcl:specEd>";
		}
		if (this.Scoreform_sectionvii_specedkind!=null){
			xmlTxt+="\n<bcl:specEdKind";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvii_specedkind.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:specEdKind>";
		}
		if (this.Scoreform_sectionvii_repeatgrade!=null){
			xmlTxt+="\n<bcl:repeatGrade";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvii_repeatgrade;
			xmlTxt+="</bcl:repeatGrade>";
		}
		if (this.Scoreform_sectionvii_repeatgradereason!=null){
			xmlTxt+="\n<bcl:repeatGradeReason";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvii_repeatgradereason.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:repeatGradeReason>";
		}
		if (this.Scoreform_sectionvii_schoolprob!=null){
			xmlTxt+="\n<bcl:schoolProb";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvii_schoolprob;
			xmlTxt+="</bcl:schoolProb>";
		}
		if (this.Scoreform_sectionvii_schoolprobdesc!=null){
			xmlTxt+="\n<bcl:schoolProbDesc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvii_schoolprobdesc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:schoolProbDesc>";
		}
		if (this.Scoreform_sectionvii_schoolprobstartdate!=null){
			xmlTxt+="\n<bcl:schoolProbStartDate";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvii_schoolprobstartdate;
			xmlTxt+="</bcl:schoolProbStartDate>";
		}
		if (this.Scoreform_sectionvii_schoolprobend!=null){
			xmlTxt+="\n<bcl:schoolProbEnd";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvii_schoolprobend;
			xmlTxt+="</bcl:schoolProbEnd>";
		}
		if (this.Scoreform_sectionvii_schoolprobenddate!=null){
			xmlTxt+="\n<bcl:schoolProbEndDate";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvii_schoolprobenddate;
			xmlTxt+="</bcl:schoolProbEndDate>";
		}
				xmlTxt+="\n</bcl:sectionVII>";
			}
			}

		if (this.Scoreform_illdisable!=null){
			xmlTxt+="\n<bcl:illDisable";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_illdisable;
			xmlTxt+="</bcl:illDisable>";
		}
		if (this.Scoreform_illdisabledesc!=null){
			xmlTxt+="\n<bcl:illDisableDesc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_illdisabledesc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:illDisableDesc>";
		}
		if (this.Scoreform_concernmost!=null){
			xmlTxt+="\n<bcl:concernMost";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_concernmost.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:concernMost>";
		}
		if (this.Scoreform_bestthings!=null){
			xmlTxt+="\n<bcl:bestThings";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_bestthings.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:bestThings>";
		}
			var child8=0;
			var att8=0;
			if(this.Scoreform_descitemlist_question39!=null)
			child8++;
			if(this.Scoreform_descitemlist_question38!=null)
			child8++;
			if(this.Scoreform_descitemlist_question37!=null)
			child8++;
			if(this.Scoreform_descitemlist_question36!=null)
			child8++;
			if(this.Scoreform_descitemlist_question35!=null)
			child8++;
			if(this.Scoreform_descitemlist_question99!=null)
			child8++;
			if(this.Scoreform_descitemlist_question34!=null)
			child8++;
			if(this.Scoreform_descitemlist_question98!=null)
			child8++;
			if(this.Scoreform_descitemlist_question33!=null)
			child8++;
			if(this.Scoreform_descitemlist_question97!=null)
			child8++;
			if(this.Scoreform_descitemlist_question32!=null)
			child8++;
			if(this.Scoreform_descitemlist_question96!=null)
			child8++;
			if(this.Scoreform_descitemlist_question31!=null)
			child8++;
			if(this.Scoreform_descitemlist_question95!=null)
			child8++;
			if(this.Scoreform_descitemlist_question30!=null)
			child8++;
			if(this.Scoreform_descitemlist_question94!=null)
			child8++;
			if(this.Scoreform_descitemlist_question93!=null)
			child8++;
			if(this.Scoreform_descitemlist_question92!=null)
			child8++;
			if(this.Scoreform_descitemlist_question91!=null)
			child8++;
			if(this.Scoreform_descitemlist_question90!=null)
			child8++;
			if(this.Scoreform_descitemlist_question92desc!=null)
			child8++;
			if(this.Scoreform_descitemlist_question56h!=null)
			child8++;
			if(this.Scoreform_descitemlist_question56g!=null)
			child8++;
			if(this.Scoreform_descitemlist_question56f!=null)
			child8++;
			if(this.Scoreform_descitemlist_question56e!=null)
			child8++;
			if(this.Scoreform_descitemlist_question56d!=null)
			child8++;
			if(this.Scoreform_descitemlist_question56c!=null)
			child8++;
			if(this.Scoreform_descitemlist_question56b!=null)
			child8++;
			if(this.Scoreform_descitemlist_question56a!=null)
			child8++;
			if(this.Scoreform_descitemlist_question70desc!=null)
			child8++;
			if(this.Scoreform_descitemlist_question29!=null)
			child8++;
			if(this.Scoreform_descitemlist_question28!=null)
			child8++;
			if(this.Scoreform_descitemlist_question27!=null)
			child8++;
			if(this.Scoreform_descitemlist_question26!=null)
			child8++;
			if(this.Scoreform_descitemlist_question25!=null)
			child8++;
			if(this.Scoreform_descitemlist_question89!=null)
			child8++;
			if(this.Scoreform_descitemlist_question24!=null)
			child8++;
			if(this.Scoreform_descitemlist_question88!=null)
			child8++;
			if(this.Scoreform_descitemlist_question23!=null)
			child8++;
			if(this.Scoreform_descitemlist_question87!=null)
			child8++;
			if(this.Scoreform_descitemlist_question22!=null)
			child8++;
			if(this.Scoreform_descitemlist_question86!=null)
			child8++;
			if(this.Scoreform_descitemlist_question21!=null)
			child8++;
			if(this.Scoreform_descitemlist_question85!=null)
			child8++;
			if(this.Scoreform_descitemlist_question20!=null)
			child8++;
			if(this.Scoreform_descitemlist_question84!=null)
			child8++;
			if(this.Scoreform_descitemlist_question83!=null)
			child8++;
			if(this.Scoreform_descitemlist_question82!=null)
			child8++;
			if(this.Scoreform_descitemlist_question81!=null)
			child8++;
			if(this.Scoreform_descitemlist_question80!=null)
			child8++;
			if(this.Scoreform_descitemlist_question56ddesc!=null)
			child8++;
			if(this.Scoreform_descitemlist_question19!=null)
			child8++;
			if(this.Scoreform_descitemlist_question112!=null)
			child8++;
			if(this.Scoreform_descitemlist_question18!=null)
			child8++;
			if(this.Scoreform_descitemlist_question111!=null)
			child8++;
			if(this.Scoreform_descitemlist_question58desc!=null)
			child8++;
			if(this.Scoreform_descitemlist_question17!=null)
			child8++;
			if(this.Scoreform_descitemlist_question110!=null)
			child8++;
			if(this.Scoreform_descitemlist_question16!=null)
			child8++;
			if(this.Scoreform_descitemlist_question15!=null)
			child8++;
			if(this.Scoreform_descitemlist_question79!=null)
			child8++;
			if(this.Scoreform_descitemlist_question14!=null)
			child8++;
			if(this.Scoreform_descitemlist_question78!=null)
			child8++;
			if(this.Scoreform_descitemlist_question13!=null)
			child8++;
			if(this.Scoreform_descitemlist_question77!=null)
			child8++;
			if(this.Scoreform_descitemlist_question12!=null)
			child8++;
			if(this.Scoreform_descitemlist_question76!=null)
			child8++;
			if(this.Scoreform_descitemlist_question11!=null)
			child8++;
			if(this.Scoreform_descitemlist_question85desc!=null)
			child8++;
			if(this.Scoreform_descitemlist_question75!=null)
			child8++;
			if(this.Scoreform_descitemlist_question10!=null)
			child8++;
			if(this.Scoreform_descitemlist_question74!=null)
			child8++;
			if(this.Scoreform_descitemlist_question73!=null)
			child8++;
			if(this.Scoreform_descitemlist_question72!=null)
			child8++;
			if(this.Scoreform_descitemlist_question71!=null)
			child8++;
			if(this.Scoreform_descitemlist_question9!=null)
			child8++;
			if(this.Scoreform_descitemlist_question70!=null)
			child8++;
			if(this.Scoreform_descitemlist_question8!=null)
			child8++;
			if(this.Scoreform_descitemlist_question7!=null)
			child8++;
			if(this.Scoreform_descitemlist_question6!=null)
			child8++;
			if(this.Scoreform_descitemlist_question5!=null)
			child8++;
			if(this.Scoreform_descitemlist_question4!=null)
			child8++;
			if(this.Scoreform_descitemlist_question3!=null)
			child8++;
			if(this.Scoreform_descitemlist_question2!=null)
			child8++;
			if(this.Scoreform_descitemlist_question1!=null)
			child8++;
			if(this.Scoreform_descitemlist_question113a3!=null)
			child8++;
			if(this.Scoreform_descitemlist_question109!=null)
			child8++;
			if(this.Scoreform_descitemlist_question113a2!=null)
			child8++;
			if(this.Scoreform_descitemlist_question108!=null)
			child8++;
			if(this.Scoreform_descitemlist_question79desc!=null)
			child8++;
			if(this.Scoreform_descitemlist_question113a1!=null)
			child8++;
			if(this.Scoreform_descitemlist_question107!=null)
			child8++;
			if(this.Scoreform_descitemlist_question106!=null)
			child8++;
			if(this.Scoreform_descitemlist_question105!=null)
			child8++;
			if(this.Scoreform_descitemlist_question104!=null)
			child8++;
			if(this.Scoreform_descitemlist_question103!=null)
			child8++;
			if(this.Scoreform_descitemlist_question102!=null)
			child8++;
			if(this.Scoreform_descitemlist_question101!=null)
			child8++;
			if(this.Scoreform_descitemlist_question100!=null)
			child8++;
			if(this.Scoreform_descitemlist_question69!=null)
			child8++;
			if(this.Scoreform_descitemlist_question68!=null)
			child8++;
			if(this.Scoreform_descitemlist_question67!=null)
			child8++;
			if(this.Scoreform_descitemlist_question66!=null)
			child8++;
			if(this.Scoreform_descitemlist_question65!=null)
			child8++;
			if(this.Scoreform_descitemlist_question64!=null)
			child8++;
			if(this.Scoreform_descitemlist_question9desc!=null)
			child8++;
			if(this.Scoreform_descitemlist_question63!=null)
			child8++;
			if(this.Scoreform_descitemlist_question62!=null)
			child8++;
			if(this.Scoreform_descitemlist_question61!=null)
			child8++;
			if(this.Scoreform_descitemlist_question46desc!=null)
			child8++;
			if(this.Scoreform_descitemlist_question84desc!=null)
			child8++;
			if(this.Scoreform_descitemlist_question60!=null)
			child8++;
			if(this.Scoreform_descitemlist_question73desc!=null)
			child8++;
			if(this.Scoreform_descitemlist_question59!=null)
			child8++;
			if(this.Scoreform_descitemlist_question29desc!=null)
			child8++;
			if(this.Scoreform_descitemlist_question58!=null)
			child8++;
			if(this.Scoreform_descitemlist_question40desc!=null)
			child8++;
			if(this.Scoreform_descitemlist_question57!=null)
			child8++;
			if(this.Scoreform_descitemlist_question55!=null)
			child8++;
			if(this.Scoreform_descitemlist_question54!=null)
			child8++;
			if(this.Scoreform_descitemlist_question53!=null)
			child8++;
			if(this.Scoreform_descitemlist_question52!=null)
			child8++;
			if(this.Scoreform_descitemlist_question51!=null)
			child8++;
			if(this.Scoreform_descitemlist_question50!=null)
			child8++;
			if(this.Scoreform_descitemlist_question83desc!=null)
			child8++;
			if(this.Scoreform_descitemlist_question77desc!=null)
			child8++;
			if(this.Scoreform_descitemlist_question49!=null)
			child8++;
			if(this.Scoreform_descitemlist_question48!=null)
			child8++;
			if(this.Scoreform_descitemlist_question47!=null)
			child8++;
			if(this.Scoreform_descitemlist_question46!=null)
			child8++;
			if(this.Scoreform_descitemlist_question113q3!=null)
			child8++;
			if(this.Scoreform_descitemlist_question2desc!=null)
			child8++;
			if(this.Scoreform_descitemlist_question45!=null)
			child8++;
			if(this.Scoreform_descitemlist_question113q2!=null)
			child8++;
			if(this.Scoreform_descitemlist_question44!=null)
			child8++;
			if(this.Scoreform_descitemlist_question113q1!=null)
			child8++;
			if(this.Scoreform_descitemlist_question100desc!=null)
			child8++;
			if(this.Scoreform_descitemlist_question66desc!=null)
			child8++;
			if(this.Scoreform_descitemlist_question43!=null)
			child8++;
			if(this.Scoreform_descitemlist_question42!=null)
			child8++;
			if(this.Scoreform_descitemlist_question41!=null)
			child8++;
			if(this.Scoreform_descitemlist_question40!=null)
			child8++;
			if(this.Scoreform_descitemlist_question105desc!=null)
			child8++;
			if(child8>0 || att8>0){
				xmlTxt+="\n<bcl:descItemList";
			if(child8==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_descitemlist_question1!=null){
			xmlTxt+="\n<bcl:question1";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question1;
			xmlTxt+="</bcl:question1>";
		}
		if (this.Scoreform_descitemlist_question2!=null){
			xmlTxt+="\n<bcl:question2";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question2;
			xmlTxt+="</bcl:question2>";
		}
		if (this.Scoreform_descitemlist_question2desc!=null){
			xmlTxt+="\n<bcl:question2Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question2desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question2Desc>";
		}
		if (this.Scoreform_descitemlist_question3!=null){
			xmlTxt+="\n<bcl:question3";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question3;
			xmlTxt+="</bcl:question3>";
		}
		if (this.Scoreform_descitemlist_question4!=null){
			xmlTxt+="\n<bcl:question4";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question4;
			xmlTxt+="</bcl:question4>";
		}
		if (this.Scoreform_descitemlist_question5!=null){
			xmlTxt+="\n<bcl:question5";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question5;
			xmlTxt+="</bcl:question5>";
		}
		if (this.Scoreform_descitemlist_question6!=null){
			xmlTxt+="\n<bcl:question6";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question6;
			xmlTxt+="</bcl:question6>";
		}
		if (this.Scoreform_descitemlist_question7!=null){
			xmlTxt+="\n<bcl:question7";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question7;
			xmlTxt+="</bcl:question7>";
		}
		if (this.Scoreform_descitemlist_question8!=null){
			xmlTxt+="\n<bcl:question8";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question8;
			xmlTxt+="</bcl:question8>";
		}
		if (this.Scoreform_descitemlist_question9!=null){
			xmlTxt+="\n<bcl:question9";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question9;
			xmlTxt+="</bcl:question9>";
		}
		if (this.Scoreform_descitemlist_question9desc!=null){
			xmlTxt+="\n<bcl:question9Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question9desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question9Desc>";
		}
		if (this.Scoreform_descitemlist_question10!=null){
			xmlTxt+="\n<bcl:question10";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question10;
			xmlTxt+="</bcl:question10>";
		}
		if (this.Scoreform_descitemlist_question11!=null){
			xmlTxt+="\n<bcl:question11";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question11;
			xmlTxt+="</bcl:question11>";
		}
		if (this.Scoreform_descitemlist_question12!=null){
			xmlTxt+="\n<bcl:question12";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question12;
			xmlTxt+="</bcl:question12>";
		}
		if (this.Scoreform_descitemlist_question13!=null){
			xmlTxt+="\n<bcl:question13";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question13;
			xmlTxt+="</bcl:question13>";
		}
		if (this.Scoreform_descitemlist_question14!=null){
			xmlTxt+="\n<bcl:question14";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question14;
			xmlTxt+="</bcl:question14>";
		}
		if (this.Scoreform_descitemlist_question15!=null){
			xmlTxt+="\n<bcl:question15";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question15;
			xmlTxt+="</bcl:question15>";
		}
		if (this.Scoreform_descitemlist_question16!=null){
			xmlTxt+="\n<bcl:question16";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question16;
			xmlTxt+="</bcl:question16>";
		}
		if (this.Scoreform_descitemlist_question17!=null){
			xmlTxt+="\n<bcl:question17";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question17;
			xmlTxt+="</bcl:question17>";
		}
		if (this.Scoreform_descitemlist_question18!=null){
			xmlTxt+="\n<bcl:question18";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question18;
			xmlTxt+="</bcl:question18>";
		}
		if (this.Scoreform_descitemlist_question19!=null){
			xmlTxt+="\n<bcl:question19";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question19;
			xmlTxt+="</bcl:question19>";
		}
		if (this.Scoreform_descitemlist_question20!=null){
			xmlTxt+="\n<bcl:question20";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question20;
			xmlTxt+="</bcl:question20>";
		}
		if (this.Scoreform_descitemlist_question21!=null){
			xmlTxt+="\n<bcl:question21";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question21;
			xmlTxt+="</bcl:question21>";
		}
		if (this.Scoreform_descitemlist_question22!=null){
			xmlTxt+="\n<bcl:question22";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question22;
			xmlTxt+="</bcl:question22>";
		}
		if (this.Scoreform_descitemlist_question23!=null){
			xmlTxt+="\n<bcl:question23";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question23;
			xmlTxt+="</bcl:question23>";
		}
		if (this.Scoreform_descitemlist_question24!=null){
			xmlTxt+="\n<bcl:question24";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question24;
			xmlTxt+="</bcl:question24>";
		}
		if (this.Scoreform_descitemlist_question25!=null){
			xmlTxt+="\n<bcl:question25";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question25;
			xmlTxt+="</bcl:question25>";
		}
		if (this.Scoreform_descitemlist_question26!=null){
			xmlTxt+="\n<bcl:question26";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question26;
			xmlTxt+="</bcl:question26>";
		}
		if (this.Scoreform_descitemlist_question27!=null){
			xmlTxt+="\n<bcl:question27";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question27;
			xmlTxt+="</bcl:question27>";
		}
		if (this.Scoreform_descitemlist_question28!=null){
			xmlTxt+="\n<bcl:question28";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question28;
			xmlTxt+="</bcl:question28>";
		}
		if (this.Scoreform_descitemlist_question29!=null){
			xmlTxt+="\n<bcl:question29";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question29;
			xmlTxt+="</bcl:question29>";
		}
		if (this.Scoreform_descitemlist_question29desc!=null){
			xmlTxt+="\n<bcl:question29Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question29desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question29Desc>";
		}
		if (this.Scoreform_descitemlist_question30!=null){
			xmlTxt+="\n<bcl:question30";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question30;
			xmlTxt+="</bcl:question30>";
		}
		if (this.Scoreform_descitemlist_question31!=null){
			xmlTxt+="\n<bcl:question31";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question31;
			xmlTxt+="</bcl:question31>";
		}
		if (this.Scoreform_descitemlist_question32!=null){
			xmlTxt+="\n<bcl:question32";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question32;
			xmlTxt+="</bcl:question32>";
		}
		if (this.Scoreform_descitemlist_question33!=null){
			xmlTxt+="\n<bcl:question33";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question33;
			xmlTxt+="</bcl:question33>";
		}
		if (this.Scoreform_descitemlist_question34!=null){
			xmlTxt+="\n<bcl:question34";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question34;
			xmlTxt+="</bcl:question34>";
		}
		if (this.Scoreform_descitemlist_question35!=null){
			xmlTxt+="\n<bcl:question35";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question35;
			xmlTxt+="</bcl:question35>";
		}
		if (this.Scoreform_descitemlist_question36!=null){
			xmlTxt+="\n<bcl:question36";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question36;
			xmlTxt+="</bcl:question36>";
		}
		if (this.Scoreform_descitemlist_question37!=null){
			xmlTxt+="\n<bcl:question37";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question37;
			xmlTxt+="</bcl:question37>";
		}
		if (this.Scoreform_descitemlist_question38!=null){
			xmlTxt+="\n<bcl:question38";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question38;
			xmlTxt+="</bcl:question38>";
		}
		if (this.Scoreform_descitemlist_question39!=null){
			xmlTxt+="\n<bcl:question39";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question39;
			xmlTxt+="</bcl:question39>";
		}
		if (this.Scoreform_descitemlist_question40!=null){
			xmlTxt+="\n<bcl:question40";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question40;
			xmlTxt+="</bcl:question40>";
		}
		if (this.Scoreform_descitemlist_question40desc!=null){
			xmlTxt+="\n<bcl:question40Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question40desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question40Desc>";
		}
		if (this.Scoreform_descitemlist_question41!=null){
			xmlTxt+="\n<bcl:question41";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question41;
			xmlTxt+="</bcl:question41>";
		}
		if (this.Scoreform_descitemlist_question42!=null){
			xmlTxt+="\n<bcl:question42";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question42;
			xmlTxt+="</bcl:question42>";
		}
		if (this.Scoreform_descitemlist_question43!=null){
			xmlTxt+="\n<bcl:question43";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question43;
			xmlTxt+="</bcl:question43>";
		}
		if (this.Scoreform_descitemlist_question44!=null){
			xmlTxt+="\n<bcl:question44";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question44;
			xmlTxt+="</bcl:question44>";
		}
		if (this.Scoreform_descitemlist_question45!=null){
			xmlTxt+="\n<bcl:question45";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question45;
			xmlTxt+="</bcl:question45>";
		}
		if (this.Scoreform_descitemlist_question46!=null){
			xmlTxt+="\n<bcl:question46";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question46;
			xmlTxt+="</bcl:question46>";
		}
		if (this.Scoreform_descitemlist_question46desc!=null){
			xmlTxt+="\n<bcl:question46Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question46desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question46Desc>";
		}
		if (this.Scoreform_descitemlist_question47!=null){
			xmlTxt+="\n<bcl:question47";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question47;
			xmlTxt+="</bcl:question47>";
		}
		if (this.Scoreform_descitemlist_question48!=null){
			xmlTxt+="\n<bcl:question48";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question48;
			xmlTxt+="</bcl:question48>";
		}
		if (this.Scoreform_descitemlist_question49!=null){
			xmlTxt+="\n<bcl:question49";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question49;
			xmlTxt+="</bcl:question49>";
		}
		if (this.Scoreform_descitemlist_question50!=null){
			xmlTxt+="\n<bcl:question50";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question50;
			xmlTxt+="</bcl:question50>";
		}
		if (this.Scoreform_descitemlist_question51!=null){
			xmlTxt+="\n<bcl:question51";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question51;
			xmlTxt+="</bcl:question51>";
		}
		if (this.Scoreform_descitemlist_question52!=null){
			xmlTxt+="\n<bcl:question52";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question52;
			xmlTxt+="</bcl:question52>";
		}
		if (this.Scoreform_descitemlist_question53!=null){
			xmlTxt+="\n<bcl:question53";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question53;
			xmlTxt+="</bcl:question53>";
		}
		if (this.Scoreform_descitemlist_question54!=null){
			xmlTxt+="\n<bcl:question54";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question54;
			xmlTxt+="</bcl:question54>";
		}
		if (this.Scoreform_descitemlist_question55!=null){
			xmlTxt+="\n<bcl:question55";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question55;
			xmlTxt+="</bcl:question55>";
		}
		if (this.Scoreform_descitemlist_question56a!=null){
			xmlTxt+="\n<bcl:question56a";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question56a;
			xmlTxt+="</bcl:question56a>";
		}
		if (this.Scoreform_descitemlist_question56b!=null){
			xmlTxt+="\n<bcl:question56b";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question56b;
			xmlTxt+="</bcl:question56b>";
		}
		if (this.Scoreform_descitemlist_question56c!=null){
			xmlTxt+="\n<bcl:question56c";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question56c;
			xmlTxt+="</bcl:question56c>";
		}
		if (this.Scoreform_descitemlist_question56d!=null){
			xmlTxt+="\n<bcl:question56d";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question56d;
			xmlTxt+="</bcl:question56d>";
		}
		if (this.Scoreform_descitemlist_question56ddesc!=null){
			xmlTxt+="\n<bcl:question56dDesc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question56ddesc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question56dDesc>";
		}
		if (this.Scoreform_descitemlist_question56e!=null){
			xmlTxt+="\n<bcl:question56e";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question56e;
			xmlTxt+="</bcl:question56e>";
		}
		if (this.Scoreform_descitemlist_question56f!=null){
			xmlTxt+="\n<bcl:question56f";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question56f;
			xmlTxt+="</bcl:question56f>";
		}
		if (this.Scoreform_descitemlist_question56g!=null){
			xmlTxt+="\n<bcl:question56g";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question56g;
			xmlTxt+="</bcl:question56g>";
		}
		if (this.Scoreform_descitemlist_question56h!=null){
			xmlTxt+="\n<bcl:question56h";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question56h.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question56h>";
		}
		if (this.Scoreform_descitemlist_question57!=null){
			xmlTxt+="\n<bcl:question57";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question57;
			xmlTxt+="</bcl:question57>";
		}
		if (this.Scoreform_descitemlist_question58!=null){
			xmlTxt+="\n<bcl:question58";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question58;
			xmlTxt+="</bcl:question58>";
		}
		if (this.Scoreform_descitemlist_question58desc!=null){
			xmlTxt+="\n<bcl:question58Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question58desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question58Desc>";
		}
		if (this.Scoreform_descitemlist_question59!=null){
			xmlTxt+="\n<bcl:question59";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question59;
			xmlTxt+="</bcl:question59>";
		}
		if (this.Scoreform_descitemlist_question60!=null){
			xmlTxt+="\n<bcl:question60";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question60;
			xmlTxt+="</bcl:question60>";
		}
		if (this.Scoreform_descitemlist_question61!=null){
			xmlTxt+="\n<bcl:question61";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question61;
			xmlTxt+="</bcl:question61>";
		}
		if (this.Scoreform_descitemlist_question62!=null){
			xmlTxt+="\n<bcl:question62";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question62;
			xmlTxt+="</bcl:question62>";
		}
		if (this.Scoreform_descitemlist_question63!=null){
			xmlTxt+="\n<bcl:question63";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question63;
			xmlTxt+="</bcl:question63>";
		}
		if (this.Scoreform_descitemlist_question64!=null){
			xmlTxt+="\n<bcl:question64";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question64;
			xmlTxt+="</bcl:question64>";
		}
		if (this.Scoreform_descitemlist_question65!=null){
			xmlTxt+="\n<bcl:question65";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question65;
			xmlTxt+="</bcl:question65>";
		}
		if (this.Scoreform_descitemlist_question66!=null){
			xmlTxt+="\n<bcl:question66";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question66;
			xmlTxt+="</bcl:question66>";
		}
		if (this.Scoreform_descitemlist_question66desc!=null){
			xmlTxt+="\n<bcl:question66Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question66desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question66Desc>";
		}
		if (this.Scoreform_descitemlist_question67!=null){
			xmlTxt+="\n<bcl:question67";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question67;
			xmlTxt+="</bcl:question67>";
		}
		if (this.Scoreform_descitemlist_question68!=null){
			xmlTxt+="\n<bcl:question68";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question68;
			xmlTxt+="</bcl:question68>";
		}
		if (this.Scoreform_descitemlist_question69!=null){
			xmlTxt+="\n<bcl:question69";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question69;
			xmlTxt+="</bcl:question69>";
		}
		if (this.Scoreform_descitemlist_question70!=null){
			xmlTxt+="\n<bcl:question70";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question70;
			xmlTxt+="</bcl:question70>";
		}
		if (this.Scoreform_descitemlist_question70desc!=null){
			xmlTxt+="\n<bcl:question70Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question70desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question70Desc>";
		}
		if (this.Scoreform_descitemlist_question71!=null){
			xmlTxt+="\n<bcl:question71";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question71;
			xmlTxt+="</bcl:question71>";
		}
		if (this.Scoreform_descitemlist_question72!=null){
			xmlTxt+="\n<bcl:question72";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question72;
			xmlTxt+="</bcl:question72>";
		}
		if (this.Scoreform_descitemlist_question73!=null){
			xmlTxt+="\n<bcl:question73";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question73;
			xmlTxt+="</bcl:question73>";
		}
		if (this.Scoreform_descitemlist_question73desc!=null){
			xmlTxt+="\n<bcl:question73Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question73desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question73Desc>";
		}
		if (this.Scoreform_descitemlist_question74!=null){
			xmlTxt+="\n<bcl:question74";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question74;
			xmlTxt+="</bcl:question74>";
		}
		if (this.Scoreform_descitemlist_question75!=null){
			xmlTxt+="\n<bcl:question75";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question75;
			xmlTxt+="</bcl:question75>";
		}
		if (this.Scoreform_descitemlist_question76!=null){
			xmlTxt+="\n<bcl:question76";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question76;
			xmlTxt+="</bcl:question76>";
		}
		if (this.Scoreform_descitemlist_question77!=null){
			xmlTxt+="\n<bcl:question77";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question77;
			xmlTxt+="</bcl:question77>";
		}
		if (this.Scoreform_descitemlist_question77desc!=null){
			xmlTxt+="\n<bcl:question77Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question77desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question77Desc>";
		}
		if (this.Scoreform_descitemlist_question78!=null){
			xmlTxt+="\n<bcl:question78";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question78;
			xmlTxt+="</bcl:question78>";
		}
		if (this.Scoreform_descitemlist_question79!=null){
			xmlTxt+="\n<bcl:question79";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question79;
			xmlTxt+="</bcl:question79>";
		}
		if (this.Scoreform_descitemlist_question79desc!=null){
			xmlTxt+="\n<bcl:question79Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question79desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question79Desc>";
		}
		if (this.Scoreform_descitemlist_question80!=null){
			xmlTxt+="\n<bcl:question80";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question80;
			xmlTxt+="</bcl:question80>";
		}
		if (this.Scoreform_descitemlist_question81!=null){
			xmlTxt+="\n<bcl:question81";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question81;
			xmlTxt+="</bcl:question81>";
		}
		if (this.Scoreform_descitemlist_question82!=null){
			xmlTxt+="\n<bcl:question82";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question82;
			xmlTxt+="</bcl:question82>";
		}
		if (this.Scoreform_descitemlist_question83!=null){
			xmlTxt+="\n<bcl:question83";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question83;
			xmlTxt+="</bcl:question83>";
		}
		if (this.Scoreform_descitemlist_question83desc!=null){
			xmlTxt+="\n<bcl:question83Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question83desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question83Desc>";
		}
		if (this.Scoreform_descitemlist_question84!=null){
			xmlTxt+="\n<bcl:question84";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question84;
			xmlTxt+="</bcl:question84>";
		}
		if (this.Scoreform_descitemlist_question84desc!=null){
			xmlTxt+="\n<bcl:question84Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question84desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question84Desc>";
		}
		if (this.Scoreform_descitemlist_question85!=null){
			xmlTxt+="\n<bcl:question85";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question85;
			xmlTxt+="</bcl:question85>";
		}
		if (this.Scoreform_descitemlist_question85desc!=null){
			xmlTxt+="\n<bcl:question85Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question85desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question85Desc>";
		}
		if (this.Scoreform_descitemlist_question86!=null){
			xmlTxt+="\n<bcl:question86";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question86;
			xmlTxt+="</bcl:question86>";
		}
		if (this.Scoreform_descitemlist_question87!=null){
			xmlTxt+="\n<bcl:question87";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question87;
			xmlTxt+="</bcl:question87>";
		}
		if (this.Scoreform_descitemlist_question88!=null){
			xmlTxt+="\n<bcl:question88";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question88;
			xmlTxt+="</bcl:question88>";
		}
		if (this.Scoreform_descitemlist_question89!=null){
			xmlTxt+="\n<bcl:question89";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question89;
			xmlTxt+="</bcl:question89>";
		}
		if (this.Scoreform_descitemlist_question90!=null){
			xmlTxt+="\n<bcl:question90";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question90;
			xmlTxt+="</bcl:question90>";
		}
		if (this.Scoreform_descitemlist_question91!=null){
			xmlTxt+="\n<bcl:question91";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question91;
			xmlTxt+="</bcl:question91>";
		}
		if (this.Scoreform_descitemlist_question92!=null){
			xmlTxt+="\n<bcl:question92";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question92;
			xmlTxt+="</bcl:question92>";
		}
		if (this.Scoreform_descitemlist_question92desc!=null){
			xmlTxt+="\n<bcl:question92Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question92desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question92Desc>";
		}
		if (this.Scoreform_descitemlist_question93!=null){
			xmlTxt+="\n<bcl:question93";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question93;
			xmlTxt+="</bcl:question93>";
		}
		if (this.Scoreform_descitemlist_question94!=null){
			xmlTxt+="\n<bcl:question94";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question94;
			xmlTxt+="</bcl:question94>";
		}
		if (this.Scoreform_descitemlist_question95!=null){
			xmlTxt+="\n<bcl:question95";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question95;
			xmlTxt+="</bcl:question95>";
		}
		if (this.Scoreform_descitemlist_question96!=null){
			xmlTxt+="\n<bcl:question96";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question96;
			xmlTxt+="</bcl:question96>";
		}
		if (this.Scoreform_descitemlist_question97!=null){
			xmlTxt+="\n<bcl:question97";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question97;
			xmlTxt+="</bcl:question97>";
		}
		if (this.Scoreform_descitemlist_question98!=null){
			xmlTxt+="\n<bcl:question98";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question98;
			xmlTxt+="</bcl:question98>";
		}
		if (this.Scoreform_descitemlist_question99!=null){
			xmlTxt+="\n<bcl:question99";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question99;
			xmlTxt+="</bcl:question99>";
		}
		if (this.Scoreform_descitemlist_question100!=null){
			xmlTxt+="\n<bcl:question100";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question100;
			xmlTxt+="</bcl:question100>";
		}
		if (this.Scoreform_descitemlist_question100desc!=null){
			xmlTxt+="\n<bcl:question100Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question100desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question100Desc>";
		}
		if (this.Scoreform_descitemlist_question101!=null){
			xmlTxt+="\n<bcl:question101";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question101;
			xmlTxt+="</bcl:question101>";
		}
		if (this.Scoreform_descitemlist_question102!=null){
			xmlTxt+="\n<bcl:question102";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question102;
			xmlTxt+="</bcl:question102>";
		}
		if (this.Scoreform_descitemlist_question103!=null){
			xmlTxt+="\n<bcl:question103";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question103;
			xmlTxt+="</bcl:question103>";
		}
		if (this.Scoreform_descitemlist_question104!=null){
			xmlTxt+="\n<bcl:question104";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question104;
			xmlTxt+="</bcl:question104>";
		}
		if (this.Scoreform_descitemlist_question105!=null){
			xmlTxt+="\n<bcl:question105";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question105;
			xmlTxt+="</bcl:question105>";
		}
		if (this.Scoreform_descitemlist_question105desc!=null){
			xmlTxt+="\n<bcl:question105Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question105desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question105Desc>";
		}
		if (this.Scoreform_descitemlist_question106!=null){
			xmlTxt+="\n<bcl:question106";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question106;
			xmlTxt+="</bcl:question106>";
		}
		if (this.Scoreform_descitemlist_question107!=null){
			xmlTxt+="\n<bcl:question107";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question107;
			xmlTxt+="</bcl:question107>";
		}
		if (this.Scoreform_descitemlist_question108!=null){
			xmlTxt+="\n<bcl:question108";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question108;
			xmlTxt+="</bcl:question108>";
		}
		if (this.Scoreform_descitemlist_question109!=null){
			xmlTxt+="\n<bcl:question109";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question109;
			xmlTxt+="</bcl:question109>";
		}
		if (this.Scoreform_descitemlist_question110!=null){
			xmlTxt+="\n<bcl:question110";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question110;
			xmlTxt+="</bcl:question110>";
		}
		if (this.Scoreform_descitemlist_question111!=null){
			xmlTxt+="\n<bcl:question111";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question111;
			xmlTxt+="</bcl:question111>";
		}
		if (this.Scoreform_descitemlist_question112!=null){
			xmlTxt+="\n<bcl:question112";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question112;
			xmlTxt+="</bcl:question112>";
		}
		if (this.Scoreform_descitemlist_question113q1!=null){
			xmlTxt+="\n<bcl:question113Q1";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question113q1.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question113Q1>";
		}
		if (this.Scoreform_descitemlist_question113a1!=null){
			xmlTxt+="\n<bcl:question113A1";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question113a1;
			xmlTxt+="</bcl:question113A1>";
		}
		if (this.Scoreform_descitemlist_question113q2!=null){
			xmlTxt+="\n<bcl:question113Q2";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question113q2.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question113Q2>";
		}
		if (this.Scoreform_descitemlist_question113a2!=null){
			xmlTxt+="\n<bcl:question113A2";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question113a2;
			xmlTxt+="</bcl:question113A2>";
		}
		if (this.Scoreform_descitemlist_question113q3!=null){
			xmlTxt+="\n<bcl:question113Q3";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question113q3.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question113Q3>";
		}
		if (this.Scoreform_descitemlist_question113a3!=null){
			xmlTxt+="\n<bcl:question113A3";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_descitemlist_question113a3;
			xmlTxt+="</bcl:question113A3>";
		}
				xmlTxt+="\n</bcl:descItemList>";
			}
			}

				xmlTxt+="\n</bcl:ScoreForm>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.AnxdpRaw!=null) return true;
		if (this.AnxdpT!=null) return true;
		if (this.AnxdpNote!=null) return true;
		if (this.WthdpRaw!=null) return true;
		if (this.WthdpT!=null) return true;
		if (this.WthdpNote!=null) return true;
		if (this.SomRaw!=null) return true;
		if (this.SomT!=null) return true;
		if (this.SomNote!=null) return true;
		if (this.SocRaw!=null) return true;
		if (this.SocT!=null) return true;
		if (this.SocNote!=null) return true;
		if (this.ThoRaw!=null) return true;
		if (this.ThoT!=null) return true;
		if (this.ThoNote!=null) return true;
		if (this.AttRaw!=null) return true;
		if (this.AttT!=null) return true;
		if (this.AttNote!=null) return true;
		if (this.RuleRaw!=null) return true;
		if (this.RuleT!=null) return true;
		if (this.RuleNote!=null) return true;
		if (this.AggRaw!=null) return true;
		if (this.AggT!=null) return true;
		if (this.AggNote!=null) return true;
			if(this.Scoreform_descitemlist_question69!=null) return true;
			if(this.Scoreform_descitemlist_question68!=null) return true;
			if(this.Scoreform_sectionvii_schoolprobend!=null) return true;
			if(this.Scoreform_descitemlist_question67!=null) return true;
			if(this.Scoreform_descitemlist_question66!=null) return true;
			if(this.Scoreform_descitemlist_question65!=null) return true;
			if(this.Scoreform_descitemlist_question64!=null) return true;
			if(this.Scoreform_descitemlist_question63!=null) return true;
			if(this.Scoreform_sectionvii_repeatgradereason!=null) return true;
			if(this.Scoreform_descitemlist_question62!=null) return true;
			if(this.Scoreform_descitemlist_question61!=null) return true;
			if(this.Scoreform_descitemlist_question112!=null) return true;
			if(this.Scoreform_descitemlist_question60!=null) return true;
			if(this.Scoreform_descitemlist_question111!=null) return true;
			if(this.Scoreform_descitemlist_question110!=null) return true;
			if(this.Scoreform_descitemlist_question59!=null) return true;
			if(this.Scoreform_sectionii_hobbiestopicc!=null) return true;
			if(this.Scoreform_descitemlist_question58!=null) return true;
			if(this.Scoreform_descitemlist_question109!=null) return true;
			if(this.Scoreform_sectionii_hobbiestopicb!=null) return true;
			if(this.Scoreform_descitemlist_question57!=null) return true;
			if(this.Scoreform_descitemlist_question108!=null) return true;
			if(this.Scoreform_sectionii_hobbiestopica!=null) return true;
			if(this.Scoreform_illdisabledesc!=null) return true;
			if(this.Scoreform_descitemlist_question107!=null) return true;
			if(this.Scoreform_descitemlist_question55!=null) return true;
			if(this.Scoreform_descitemlist_question106!=null) return true;
			if(this.Scoreform_descitemlist_question54!=null) return true;
			if(this.Scoreform_descitemlist_question105!=null) return true;
			if(this.Scoreform_descitemlist_question53!=null) return true;
			if(this.Scoreform_descitemlist_question104!=null) return true;
			if(this.Scoreform_descitemlist_question52!=null) return true;
			if(this.Scoreform_descitemlist_question103!=null) return true;
			if(this.Scoreform_descitemlist_question51!=null) return true;
			if(this.Scoreform_descitemlist_question102!=null) return true;
			if(this.Scoreform_descitemlist_question50!=null) return true;
			if(this.Scoreform_descitemlist_question101!=null) return true;
			if(this.Scoreform_descitemlist_question100!=null) return true;
			if(this.Scoreform_sectionvi_getalongd!=null) return true;
			if(this.Scoreform_sectioniii_orgstopicc!=null) return true;
			if(this.Scoreform_sectionvi_getalongc!=null) return true;
			if(this.Scoreform_sectioniii_orgstopicb!=null) return true;
			if(this.Scoreform_sectionvi_getalongb!=null) return true;
			if(this.Scoreform_sectioniii_orgstopica!=null) return true;
			if(this.Scoreform_sectionvi_getalonga!=null) return true;
			if(this.Scoreform_descitemlist_question49!=null) return true;
			if(this.Scoreform_descitemlist_question48!=null) return true;
			if(this.Scoreform_descitemlist_question47!=null) return true;
			if(this.Scoreform_descitemlist_question46!=null) return true;
			if(this.Scoreform_descitemlist_question45!=null) return true;
			if(this.Scoreform_descitemlist_question44!=null) return true;
			if(this.Scoreform_descitemlist_question43!=null) return true;
			if(this.Scoreform_descitemlist_question42!=null) return true;
			if(this.Scoreform_descitemlist_question41!=null) return true;
			if(this.Scoreform_descitemlist_question40!=null) return true;
			if(this.Scoreform_descitemlist_question85desc!=null) return true;
			if(this.Scoreform_concernmost!=null) return true;
			if(this.Scoreform_sectionvii_schoolprobstartdate!=null) return true;
			if(this.Scoreform_illdisable!=null) return true;
			if(this.Scoreform_sectioni_sportstopicc!=null) return true;
			if(this.Scoreform_descitemlist_question39!=null) return true;
			if(this.Scoreform_sectionv_friendtimes!=null) return true;
			if(this.Scoreform_sectioni_sportstopicb!=null) return true;
			if(this.Scoreform_sectioni_sportsdowellc!=null) return true;
			if(this.Scoreform_descitemlist_question38!=null) return true;
			if(this.Scoreform_sectioni_sportstopica!=null) return true;
			if(this.Scoreform_sectioni_sportsdowellb!=null) return true;
			if(this.Scoreform_descitemlist_question37!=null) return true;
			if(this.Scoreform_sectioni_sportsdowella!=null) return true;
			if(this.Scoreform_descitemlist_question36!=null) return true;
			if(this.Scoreform_descitemlist_question35!=null) return true;
			if(this.Scoreform_descitemlist_question34!=null) return true;
			if(this.Scoreform_descitemlist_question33!=null) return true;
			if(this.Scoreform_descitemlist_question32!=null) return true;
			if(this.Scoreform_descitemlist_question31!=null) return true;
			if(this.Scoreform_descitemlist_question30!=null) return true;
			if(this.Scoreform_sectionvii_schoolprobdesc!=null) return true;
			if(this.Scoreform_descitemlist_question92desc!=null) return true;
			if(this.Scoreform_descitemlist_question29!=null) return true;
			if(this.Scoreform_sectionvii_academperfetopic!=null) return true;
			if(this.Scoreform_descitemlist_question28!=null) return true;
			if(this.Scoreform_descitemlist_question27!=null) return true;
			if(this.Scoreform_descitemlist_question105desc!=null) return true;
			if(this.Scoreform_descitemlist_question26!=null) return true;
			if(this.Scoreform_descitemlist_question25!=null) return true;
			if(this.Scoreform_descitemlist_question24!=null) return true;
			if(this.Scoreform_descitemlist_question23!=null) return true;
			if(this.Scoreform_descitemlist_question22!=null) return true;
			if(this.Scoreform_sectioniv_jobswellc!=null) return true;
			if(this.Scoreform_descitemlist_question21!=null) return true;
			if(this.Scoreform_sectioniv_jobswellb!=null) return true;
			if(this.Scoreform_descitemlist_question20!=null) return true;
			if(this.Scoreform_sectioniv_jobswella!=null) return true;
			if(this.Scoreform_descitemlist_question70desc!=null) return true;
			if(this.Scoreform_sectioniv_jobstopicc!=null) return true;
			if(this.Scoreform_sectioniv_jobstopicb!=null) return true;
			if(this.Scoreform_sectioniv_jobstopica!=null) return true;
			if(this.Scoreform_descitemlist_question77desc!=null) return true;
			if(this.Scoreform_sectionv_closefriends!=null) return true;
			if(this.Scoreform_descitemlist_question66desc!=null) return true;
			if(this.Scoreform_descitemlist_question19!=null) return true;
			if(this.Scoreform_descitemlist_question18!=null) return true;
			if(this.Scoreform_descitemlist_question17!=null) return true;
			if(this.Scoreform_descitemlist_question16!=null) return true;
			if(this.Scoreform_descitemlist_question15!=null) return true;
			if(this.Scoreform_descitemlist_question14!=null) return true;
			if(this.Scoreform_descitemlist_question13!=null) return true;
			if(this.Scoreform_descitemlist_question12!=null) return true;
			if(this.Scoreform_descitemlist_question11!=null) return true;
			if(this.Scoreform_descitemlist_question10!=null) return true;
			if(this.Scoreform_respgender!=null) return true;
			if(this.Scoreform_sectionvii_schoolprobenddate!=null) return true;
			if(this.Scoreform_descitemlist_question56h!=null) return true;
			if(this.Scoreform_descitemlist_question56g!=null) return true;
			if(this.Scoreform_descitemlist_question56f!=null) return true;
			if(this.Scoreform_descitemlist_question56e!=null) return true;
			if(this.Scoreform_descitemlist_question56d!=null) return true;
			if(this.Scoreform_sectionvii_repeatgrade!=null) return true;
			if(this.Scoreform_descitemlist_question56c!=null) return true;
			if(this.Scoreform_descitemlist_question56b!=null) return true;
			if(this.Scoreform_descitemlist_question56a!=null) return true;
			if(this.Scoreform_descitemlist_question84desc!=null) return true;
			if(this.Scoreform_sectionvii_specedkind!=null) return true;
			if(this.Scoreform_descitemlist_question113q3!=null) return true;
			if(this.Scoreform_descitemlist_question73desc!=null) return true;
			if(this.Scoreform_descitemlist_question113q2!=null) return true;
			if(this.Scoreform_descitemlist_question56ddesc!=null) return true;
			if(this.Scoreform_descitemlist_question113q1!=null) return true;
			if(this.Scoreform_descitemlist_question29desc!=null) return true;
			if(this.Scoreform_descitemlist_question2desc!=null) return true;
			if(this.Scoreform_fatherwork!=null) return true;
			if(this.Scoreform_descitemlist_question9desc!=null) return true;
			if(this.Scoreform_descitemlist_question40desc!=null) return true;
			if(this.Scoreform_descitemlist_question58desc!=null) return true;
			if(this.Scoreform_sectionvii_academperfftopic!=null) return true;
			if(this.Scoreform_sectionii_hobbiestimec!=null) return true;
			if(this.Scoreform_sectionvii_schoolprob!=null) return true;
			if(this.Scoreform_sectionii_hobbiestimeb!=null) return true;
			if(this.Scoreform_sectionii_hobbiestimea!=null) return true;
			if(this.Scoreform_sectioniii_orgsactivec!=null) return true;
			if(this.Scoreform_sectioniii_orgsactiveb!=null) return true;
			if(this.Scoreform_sectioniii_orgsactivea!=null) return true;
			if(this.Scoreform_sectionvii_academperfnoschool!=null) return true;
			if(this.Scoreform_descitemlist_question9!=null) return true;
			if(this.Scoreform_descitemlist_question8!=null) return true;
			if(this.Scoreform_descitemlist_question7!=null) return true;
			if(this.Scoreform_descitemlist_question6!=null) return true;
			if(this.Scoreform_descitemlist_question5!=null) return true;
			if(this.Scoreform_descitemlist_question4!=null) return true;
			if(this.Scoreform_descitemlist_question3!=null) return true;
			if(this.Scoreform_descitemlist_question2!=null) return true;
			if(this.Scoreform_descitemlist_question1!=null) return true;
			if(this.Scoreform_descitemlist_question100desc!=null) return true;
			if(this.Scoreform_sectionvii_academperfg!=null) return true;
			if(this.Scoreform_sectioni_sportstimec!=null) return true;
			if(this.Scoreform_sectionvii_academperff!=null) return true;
			if(this.Scoreform_sectioni_sportstimeb!=null) return true;
			if(this.Scoreform_sectionvii_academperfe!=null) return true;
			if(this.Scoreform_sectioni_sportstimea!=null) return true;
			if(this.Scoreform_descitemlist_question99!=null) return true;
			if(this.Scoreform_sectionvii_academperfd!=null) return true;
			if(this.Scoreform_descitemlist_question98!=null) return true;
			if(this.Scoreform_sectionvii_academperfc!=null) return true;
			if(this.Scoreform_sectionii_hobbiesdowellc!=null) return true;
			if(this.Scoreform_descitemlist_question97!=null) return true;
			if(this.Scoreform_sectionvii_academperfb!=null) return true;
			if(this.Scoreform_descitemlist_question83desc!=null) return true;
			if(this.Scoreform_sectionii_hobbiesdowellb!=null) return true;
			if(this.Scoreform_descitemlist_question96!=null) return true;
			if(this.Scoreform_sectionvii_academperfa!=null) return true;
			if(this.Scoreform_sectionii_hobbiesdowella!=null) return true;
			if(this.Scoreform_descitemlist_question95!=null) return true;
			if(this.Scoreform_descitemlist_question94!=null) return true;
			if(this.Scoreform_descitemlist_question93!=null) return true;
			if(this.Scoreform_descitemlist_question92!=null) return true;
			if(this.Scoreform_descitemlist_question91!=null) return true;
			if(this.Scoreform_descitemlist_question90!=null) return true;
			if(this.Scoreform_descitemlist_question113a3!=null) return true;
			if(this.Scoreform_descitemlist_question113a2!=null) return true;
			if(this.Scoreform_descitemlist_question113a1!=null) return true;
			if(this.Scoreform_descitemlist_question79desc!=null) return true;
			if(this.Scoreform_descitemlist_question89!=null) return true;
			if(this.Scoreform_descitemlist_question88!=null) return true;
			if(this.Scoreform_descitemlist_question87!=null) return true;
			if(this.Scoreform_sectionvii_academperfgtopic!=null) return true;
			if(this.Scoreform_descitemlist_question86!=null) return true;
			if(this.Scoreform_descitemlist_question85!=null) return true;
			if(this.Scoreform_descitemlist_question84!=null) return true;
			if(this.Scoreform_motherwork!=null) return true;
			if(this.Scoreform_descitemlist_question83!=null) return true;
			if(this.Scoreform_descitemlist_question82!=null) return true;
			if(this.Scoreform_descitemlist_question81!=null) return true;
			if(this.Scoreform_descitemlist_question80!=null) return true;
			if(this.Scoreform_descitemlist_question46desc!=null) return true;
			if(this.Scoreform_sectionvii_speced!=null) return true;
			if(this.Scoreform_descitemlist_question79!=null) return true;
			if(this.Scoreform_descitemlist_question78!=null) return true;
			if(this.Scoreform_descitemlist_question77!=null) return true;
			if(this.Scoreform_descitemlist_question76!=null) return true;
			if(this.Scoreform_descitemlist_question75!=null) return true;
			if(this.Scoreform_descitemlist_question74!=null) return true;
			if(this.Scoreform_bestthings!=null) return true;
			if(this.Scoreform_descitemlist_question73!=null) return true;
			if(this.Scoreform_descitemlist_question72!=null) return true;
			if(this.Scoreform_descitemlist_question71!=null) return true;
			if(this.Scoreform_descitemlist_question70!=null) return true;
			if(this.Scoreform_resprltnsubj!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
