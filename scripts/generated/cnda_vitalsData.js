/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cnda_vitalsData(){
this.xsiType="cnda:vitalsData";

	this.getSchemaElementName=function(){
		return "vitalsData";
	}

	this.getFullSchemaElementName=function(){
		return "cnda:vitalsData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Height=null;


	function getHeight() {
		return this.Height;
	}
	this.getHeight=getHeight;


	function setHeight(v){
		this.Height=v;
	}
	this.setHeight=setHeight;

	this.Height_units=null;


	function getHeight_units() {
		return this.Height_units;
	}
	this.getHeight_units=getHeight_units;


	function setHeight_units(v){
		this.Height_units=v;
	}
	this.setHeight_units=setHeight_units;

	this.Weight=null;


	function getWeight() {
		return this.Weight;
	}
	this.getWeight=getWeight;


	function setWeight(v){
		this.Weight=v;
	}
	this.setWeight=setWeight;

	this.Weight_units=null;


	function getWeight_units() {
		return this.Weight_units;
	}
	this.getWeight_units=getWeight_units;


	function setWeight_units(v){
		this.Weight_units=v;
	}
	this.setWeight_units=setWeight_units;

	this.Respirations=null;


	function getRespirations() {
		return this.Respirations;
	}
	this.getRespirations=getRespirations;


	function setRespirations(v){
		this.Respirations=v;
	}
	this.setRespirations=setRespirations;
	this.Pulse =new Array();

	function getPulse() {
		return this.Pulse;
	}
	this.getPulse=getPulse;


	function addPulse(v){
		this.Pulse.push(v);
	}
	this.addPulse=addPulse;
	this.Bloodpressure =new Array();

	function getBloodpressure() {
		return this.Bloodpressure;
	}
	this.getBloodpressure=getBloodpressure;


	function addBloodpressure(v){
		this.Bloodpressure.push(v);
	}
	this.addBloodpressure=addBloodpressure;

	this.Saturationo2=null;


	function getSaturationo2() {
		return this.Saturationo2;
	}
	this.getSaturationo2=getSaturationo2;


	function setSaturationo2(v){
		this.Saturationo2=v;
	}
	this.setSaturationo2=setSaturationo2;

	this.Problem=null;


	function getProblem() {
		return this.Problem;
	}
	this.getProblem=getProblem;


	function setProblem(v){
		this.Problem=v;
	}
	this.setProblem=setProblem;


	this.isProblem=function(defaultValue) {
		if(this.Problem==null)return defaultValue;
		if(this.Problem=="1" || this.Problem==true)return true;
		return false;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="height"){
				return this.Height ;
			} else 
			if(xmlPath=="height/units"){
				return this.Height_units ;
			} else 
			if(xmlPath=="weight"){
				return this.Weight ;
			} else 
			if(xmlPath=="weight/units"){
				return this.Weight_units ;
			} else 
			if(xmlPath=="respirations"){
				return this.Respirations ;
			} else 
			if(xmlPath=="pulse"){
				return this.Pulse ;
			} else 
			if(xmlPath.startsWith("pulse")){
				xmlPath=xmlPath.substring(5);
				if(xmlPath=="")return this.Pulse ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Pulse.length;whereCount++){

					var tempValue=this.Pulse[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Pulse[whereCount]);

					}

				}
				}else{

				whereArray=this.Pulse;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="bloodPressure"){
				return this.Bloodpressure ;
			} else 
			if(xmlPath.startsWith("bloodPressure")){
				xmlPath=xmlPath.substring(13);
				if(xmlPath=="")return this.Bloodpressure ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Bloodpressure.length;whereCount++){

					var tempValue=this.Bloodpressure[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Bloodpressure[whereCount]);

					}

				}
				}else{

				whereArray=this.Bloodpressure;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="saturationO2"){
				return this.Saturationo2 ;
			} else 
			if(xmlPath=="problem"){
				return this.Problem ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="height"){
				this.Height=value;
			} else 
			if(xmlPath=="height/units"){
				this.Height_units=value;
			} else 
			if(xmlPath=="weight"){
				this.Weight=value;
			} else 
			if(xmlPath=="weight/units"){
				this.Weight_units=value;
			} else 
			if(xmlPath=="respirations"){
				this.Respirations=value;
			} else 
			if(xmlPath=="pulse"){
				this.Pulse=value;
			} else 
			if(xmlPath.startsWith("pulse")){
				xmlPath=xmlPath.substring(5);
				if(xmlPath=="")return this.Pulse ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Pulse.length;whereCount++){

					var tempValue=this.Pulse[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Pulse[whereCount]);

					}

				}
				}else{

				whereArray=this.Pulse;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("cnda:vitalsData_pulse");//omUtils.js
					}
					this.addPulse(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="bloodPressure"){
				this.Bloodpressure=value;
			} else 
			if(xmlPath.startsWith("bloodPressure")){
				xmlPath=xmlPath.substring(13);
				if(xmlPath=="")return this.Bloodpressure ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Bloodpressure.length;whereCount++){

					var tempValue=this.Bloodpressure[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Bloodpressure[whereCount]);

					}

				}
				}else{

				whereArray=this.Bloodpressure;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("cnda:vitalsData_bloodPressure");//omUtils.js
					}
					this.addBloodpressure(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="saturationO2"){
				this.Saturationo2=value;
			} else 
			if(xmlPath=="problem"){
				this.Problem=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="pulse"){
			this.addPulse(v);
		}else if (xmlPath=="bloodPressure"){
			this.addBloodpressure(v);
		}
		else{
			this.extension.setReferenceField(xmlPath,v);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="pulse"){
			return "http://nrg.wustl.edu/cnda:vitalsData_pulse";
		}else if (xmlPath=="bloodPressure"){
			return "http://nrg.wustl.edu/cnda:vitalsData_bloodPressure";
		}
		else{
			return this.extension.getReferenceFieldName(xmlPath);
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="height"){
			return "field_data";
		}else if (xmlPath=="height/units"){
			return "field_data";
		}else if (xmlPath=="weight"){
			return "field_data";
		}else if (xmlPath=="weight/units"){
			return "field_data";
		}else if (xmlPath=="respirations"){
			return "field_data";
		}else if (xmlPath=="pulse"){
			return "field_NO_CHILD";
		}else if (xmlPath=="bloodPressure"){
			return "field_multi_reference";
		}else if (xmlPath=="saturationO2"){
			return "field_data";
		}else if (xmlPath=="problem"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cnda:Vitals";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cnda:Vitals>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		var HeightATT = ""
		if (this.Height_units!=null)
			HeightATT+=" units=\"" + this.Height_units.replace(/>/g,"&gt;").replace(/</g,"&lt;") + "\"";
		if (this.Height!=null){
			xmlTxt+="\n<cnda:height";
			xmlTxt+=HeightATT;
			xmlTxt+=">";
			xmlTxt+=this.Height;
			xmlTxt+="</cnda:height>";
		}
		else if(HeightATT!=""){
			xmlTxt+="\n<cnda:height";
			xmlTxt+=HeightATT;
			xmlTxt+="/>";
		}

		var WeightATT = ""
		if (this.Weight_units!=null)
			WeightATT+=" units=\"" + this.Weight_units.replace(/>/g,"&gt;").replace(/</g,"&lt;") + "\"";
		if (this.Weight!=null){
			xmlTxt+="\n<cnda:weight";
			xmlTxt+=WeightATT;
			xmlTxt+=">";
			xmlTxt+=this.Weight;
			xmlTxt+="</cnda:weight>";
		}
		else if(WeightATT!=""){
			xmlTxt+="\n<cnda:weight";
			xmlTxt+=WeightATT;
			xmlTxt+="/>";
		}

		if (this.Respirations!=null){
			xmlTxt+="\n<cnda:respirations";
			xmlTxt+=">";
			xmlTxt+=this.Respirations;
			xmlTxt+="</cnda:respirations>";
		}
		for(var PulseCOUNT=0;PulseCOUNT<this.Pulse.length;PulseCOUNT++){
			xmlTxt +="\n<cnda:pulse";
			xmlTxt +=this.Pulse[PulseCOUNT].getXMLAtts();
			if(this.Pulse[PulseCOUNT].xsiType!="cnda:vitalsData_pulse"){
				xmlTxt+=" xsi:type=\"" + this.Pulse[PulseCOUNT].xsiType + "\"";
			}
			if (this.Pulse[PulseCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Pulse[PulseCOUNT].getXMLBody(preventComments);
					xmlTxt+="</cnda:pulse>";
			}else {xmlTxt+="/>";}
		}
		for(var BloodpressureCOUNT=0;BloodpressureCOUNT<this.Bloodpressure.length;BloodpressureCOUNT++){
			xmlTxt +="\n<cnda:bloodPressure";
			xmlTxt +=this.Bloodpressure[BloodpressureCOUNT].getXMLAtts();
			if(this.Bloodpressure[BloodpressureCOUNT].xsiType!="cnda:vitalsData_bloodPressure"){
				xmlTxt+=" xsi:type=\"" + this.Bloodpressure[BloodpressureCOUNT].xsiType + "\"";
			}
			if (this.Bloodpressure[BloodpressureCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Bloodpressure[BloodpressureCOUNT].getXMLBody(preventComments);
					xmlTxt+="</cnda:bloodPressure>";
			}else {xmlTxt+="/>";}
		}
		if (this.Saturationo2!=null){
			xmlTxt+="\n<cnda:saturationO2";
			xmlTxt+=">";
			xmlTxt+=this.Saturationo2;
			xmlTxt+="</cnda:saturationO2>";
		}
		if (this.Problem!=null){
			xmlTxt+="\n<cnda:problem";
			xmlTxt+=">";
			xmlTxt+=this.Problem;
			xmlTxt+="</cnda:problem>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Height_units!=null)
			return true;
		if (this.Height!=null) return true;
		if (this.Weight_units!=null)
			return true;
		if (this.Weight!=null) return true;
		if (this.Respirations!=null) return true;
		if(this.Pulse.length>0) return true;
		if(this.Bloodpressure.length>0) return true;
		if (this.Saturationo2!=null) return true;
		if (this.Problem!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
