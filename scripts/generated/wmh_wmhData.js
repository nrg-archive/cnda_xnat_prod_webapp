/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function wmh_wmhData(){
this.xsiType="wmh:wmhData";

	this.getSchemaElementName=function(){
		return "wmhData";
	}

	this.getFullSchemaElementName=function(){
		return "wmh:wmhData";
	}
this.extension=dynamicJSLoad('xnat_imageAssessorData','generated/xnat_imageAssessorData.js');

	this.Volume=null;


	function getVolume() {
		return this.Volume;
	}
	this.getVolume=getVolume;


	function setVolume(v){
		this.Volume=v;
	}
	this.setVolume=setVolume;

	this.Voxels=null;


	function getVoxels() {
		return this.Voxels;
	}
	this.getVoxels=getVoxels;


	function setVoxels(v){
		this.Voxels=v;
	}
	this.setVoxels=setVoxels;

	this.Inputs_t1Session=null;


	function getInputs_t1Session() {
		return this.Inputs_t1Session;
	}
	this.getInputs_t1Session=getInputs_t1Session;


	function setInputs_t1Session(v){
		this.Inputs_t1Session=v;
	}
	this.setInputs_t1Session=setInputs_t1Session;

	this.Inputs_t1Scan=null;


	function getInputs_t1Scan() {
		return this.Inputs_t1Scan;
	}
	this.getInputs_t1Scan=getInputs_t1Scan;


	function setInputs_t1Scan(v){
		this.Inputs_t1Scan=v;
	}
	this.setInputs_t1Scan=setInputs_t1Scan;

	this.Inputs_flairSession=null;


	function getInputs_flairSession() {
		return this.Inputs_flairSession;
	}
	this.getInputs_flairSession=getInputs_flairSession;


	function setInputs_flairSession(v){
		this.Inputs_flairSession=v;
	}
	this.setInputs_flairSession=setInputs_flairSession;

	this.Inputs_flairScan=null;


	function getInputs_flairScan() {
		return this.Inputs_flairScan;
	}
	this.getInputs_flairScan=getInputs_flairScan;


	function setInputs_flairScan(v){
		this.Inputs_flairScan=v;
	}
	this.setInputs_flairScan=setInputs_flairScan;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="imageAssessorData"){
				return this.Imageassessordata ;
			} else 
			if(xmlPath.startsWith("imageAssessorData")){
				xmlPath=xmlPath.substring(17);
				if(xmlPath=="")return this.Imageassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Imageassessordata!=undefined)return this.Imageassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="volume"){
				return this.Volume ;
			} else 
			if(xmlPath=="voxels"){
				return this.Voxels ;
			} else 
			if(xmlPath=="inputs/t1_session"){
				return this.Inputs_t1Session ;
			} else 
			if(xmlPath=="inputs/t1_scan"){
				return this.Inputs_t1Scan ;
			} else 
			if(xmlPath=="inputs/flair_session"){
				return this.Inputs_flairSession ;
			} else 
			if(xmlPath=="inputs/flair_scan"){
				return this.Inputs_flairScan ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="imageAssessorData"){
				this.Imageassessordata=value;
			} else 
			if(xmlPath.startsWith("imageAssessorData")){
				xmlPath=xmlPath.substring(17);
				if(xmlPath=="")return this.Imageassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Imageassessordata!=undefined){
					this.Imageassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Imageassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Imageassessordata= instanciateObject("xnat:imageAssessorData");//omUtils.js
						}
						if(options && options.where)this.Imageassessordata.setProperty(options.where.field,options.where.value);
						this.Imageassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="volume"){
				this.Volume=value;
			} else 
			if(xmlPath=="voxels"){
				this.Voxels=value;
			} else 
			if(xmlPath=="inputs/t1_session"){
				this.Inputs_t1Session=value;
			} else 
			if(xmlPath=="inputs/t1_scan"){
				this.Inputs_t1Scan=value;
			} else 
			if(xmlPath=="inputs/flair_session"){
				this.Inputs_flairSession=value;
			} else 
			if(xmlPath=="inputs/flair_scan"){
				this.Inputs_flairScan=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="volume"){
			return "field_data";
		}else if (xmlPath=="voxels"){
			return "field_data";
		}else if (xmlPath=="inputs/t1_session"){
			return "field_data";
		}else if (xmlPath=="inputs/t1_scan"){
			return "field_data";
		}else if (xmlPath=="inputs/flair_session"){
			return "field_data";
		}else if (xmlPath=="inputs/flair_scan"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<wmh:WMH";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</wmh:WMH>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Volume!=null){
			xmlTxt+="\n<wmh:volume";
			xmlTxt+=">";
			xmlTxt+=this.Volume;
			xmlTxt+="</wmh:volume>";
		}
		if (this.Voxels!=null){
			xmlTxt+="\n<wmh:voxels";
			xmlTxt+=">";
			xmlTxt+=this.Voxels;
			xmlTxt+="</wmh:voxels>";
		}
			var child0=0;
			var att0=0;
			if(this.Inputs_t1Session!=null)
			child0++;
			if(this.Inputs_flairSession!=null)
			child0++;
			if(this.Inputs_flairScan!=null)
			child0++;
			if(this.Inputs_t1Scan!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<wmh:inputs";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Inputs_t1Session!=null){
			xmlTxt+="\n<wmh:t1_session";
			xmlTxt+=">";
			xmlTxt+=this.Inputs_t1Session.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</wmh:t1_session>";
		}
		if (this.Inputs_t1Scan!=null){
			xmlTxt+="\n<wmh:t1_scan";
			xmlTxt+=">";
			xmlTxt+=this.Inputs_t1Scan.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</wmh:t1_scan>";
		}
		if (this.Inputs_flairSession!=null){
			xmlTxt+="\n<wmh:flair_session";
			xmlTxt+=">";
			xmlTxt+=this.Inputs_flairSession.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</wmh:flair_session>";
		}
		if (this.Inputs_flairScan!=null){
			xmlTxt+="\n<wmh:flair_scan";
			xmlTxt+=">";
			xmlTxt+=this.Inputs_flairScan.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</wmh:flair_scan>";
		}
				xmlTxt+="\n</wmh:inputs>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Volume!=null) return true;
		if (this.Voxels!=null) return true;
			if(this.Inputs_t1Session!=null) return true;
			if(this.Inputs_flairSession!=null) return true;
			if(this.Inputs_flairScan!=null) return true;
			if(this.Inputs_t1Scan!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
