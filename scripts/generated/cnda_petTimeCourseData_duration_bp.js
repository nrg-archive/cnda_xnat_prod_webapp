/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cnda_petTimeCourseData_duration_bp(){
this.xsiType="cnda:petTimeCourseData_duration_bp";

	this.getSchemaElementName=function(){
		return "petTimeCourseData_duration_bp";
	}

	this.getFullSchemaElementName=function(){
		return "cnda:petTimeCourseData_duration_bp";
	}

	this.Bp=null;


	function getBp() {
		return this.Bp;
	}
	this.getBp=getBp;


	function setBp(v){
		this.Bp=v;
	}
	this.setBp=setBp;

	this.Name=null;


	function getName() {
		return this.Name;
	}
	this.getName=getName;


	function setName(v){
		this.Name=v;
	}
	this.setName=setName;

	this.Correl=null;


	function getCorrel() {
		return this.Correl;
	}
	this.getCorrel=getCorrel;


	function setCorrel(v){
		this.Correl=v;
	}
	this.setCorrel=setCorrel;

	this.CndaPettimecoursedataDurationBpId=null;


	function getCndaPettimecoursedataDurationBpId() {
		return this.CndaPettimecoursedataDurationBpId;
	}
	this.getCndaPettimecoursedataDurationBpId=getCndaPettimecoursedataDurationBpId;


	function setCndaPettimecoursedataDurationBpId(v){
		this.CndaPettimecoursedataDurationBpId=v;
	}
	this.setCndaPettimecoursedataDurationBpId=setCndaPettimecoursedataDurationBpId;

	this.cnda_petTimeCourseData_duration_cnda_pettimecoursedata_duration_fk=null;


	this.getcnda_petTimeCourseData_duration_cnda_pettimecoursedata_duration=function() {
		return this.cnda_petTimeCourseData_duration_cnda_pettimecoursedata_duration_fk;
	}


	this.setcnda_petTimeCourseData_duration_cnda_pettimecoursedata_duration=function(v){
		this.cnda_petTimeCourseData_duration_cnda_pettimecoursedata_duration_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="bp"){
				return this.Bp ;
			} else 
			if(xmlPath=="name"){
				return this.Name ;
			} else 
			if(xmlPath=="correl"){
				return this.Correl ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="cnda_petTimeCourseData_duration_bp_id"){
				return this.CndaPettimecoursedataDurationBpId ;
			} else 
			if(xmlPath=="cnda_petTimeCourseData_duration_cnda_pettimecoursedata_duration"){
				return this.cnda_petTimeCourseData_duration_cnda_pettimecoursedata_duration_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="bp"){
				this.Bp=value;
			} else 
			if(xmlPath=="name"){
				this.Name=value;
			} else 
			if(xmlPath=="correl"){
				this.Correl=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="cnda_petTimeCourseData_duration_bp_id"){
				this.CndaPettimecoursedataDurationBpId=value;
			} else 
			if(xmlPath=="cnda_petTimeCourseData_duration_cnda_pettimecoursedata_duration"){
				this.cnda_petTimeCourseData_duration_cnda_pettimecoursedata_duration_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="bp"){
			return "field_data";
		}else if (xmlPath=="name"){
			return "field_data";
		}else if (xmlPath=="correl"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cnda:petTimeCourseData_duration_bp";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cnda:petTimeCourseData_duration_bp>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.CndaPettimecoursedataDurationBpId!=null){
				if(hiddenCount++>0)str+=",";
				str+="cnda_petTimeCourseData_duration_bp_id=\"" + this.CndaPettimecoursedataDurationBpId + "\"";
			}
			if(this.cnda_petTimeCourseData_duration_cnda_pettimecoursedata_duration_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="cnda_petTimeCourseData_duration_cnda_pettimecoursedata_duration=\"" + this.cnda_petTimeCourseData_duration_cnda_pettimecoursedata_duration_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.Name!=null)
			attTxt+=" name=\"" +this.Name +"\"";
		//NOT REQUIRED FIELD

		if (this.Correl!=null)
			attTxt+=" correl=\"" +this.Correl +"\"";
		//NOT REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Bp!=null){
			xmlTxt+=this.Bp;
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.CndaPettimecoursedataDurationBpId!=null) return true;
			if (this.cnda_petTimeCourseData_duration_cnda_pettimecoursedata_duration_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Bp!=null) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
