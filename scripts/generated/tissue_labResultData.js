/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function tissue_labResultData(){
this.xsiType="tissue:labResultData";

	this.getSchemaElementName=function(){
		return "labResultData";
	}

	this.getFullSchemaElementName=function(){
		return "tissue:labResultData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Dateresultsreport=null;


	function getDateresultsreport() {
		return this.Dateresultsreport;
	}
	this.getDateresultsreport=getDateresultsreport;


	function setDateresultsreport(v){
		this.Dateresultsreport=v;
	}
	this.setDateresultsreport=setDateresultsreport;

	this.Timeresultsreport=null;


	function getTimeresultsreport() {
		return this.Timeresultsreport;
	}
	this.getTimeresultsreport=getTimeresultsreport;


	function setTimeresultsreport(v){
		this.Timeresultsreport=v;
	}
	this.setTimeresultsreport=setTimeresultsreport;

	this.Nonnumericresults=null;


	function getNonnumericresults() {
		return this.Nonnumericresults;
	}
	this.getNonnumericresults=getNonnumericresults;


	function setNonnumericresults(v){
		this.Nonnumericresults=v;
	}
	this.setNonnumericresults=setNonnumericresults;

	this.Numericresults=null;


	function getNumericresults() {
		return this.Numericresults;
	}
	this.getNumericresults=getNumericresults;


	function setNumericresults(v){
		this.Numericresults=v;
	}
	this.setNumericresults=setNumericresults;

	this.Unitsnumericresults=null;


	function getUnitsnumericresults() {
		return this.Unitsnumericresults;
	}
	this.getUnitsnumericresults=getUnitsnumericresults;


	function setUnitsnumericresults(v){
		this.Unitsnumericresults=v;
	}
	this.setUnitsnumericresults=setUnitsnumericresults;

	this.Tisscollid=null;


	function getTisscollid() {
		return this.Tisscollid;
	}
	this.getTisscollid=getTisscollid;


	function setTisscollid(v){
		this.Tisscollid=v;
	}
	this.setTisscollid=setTisscollid;

	this.Specid=null;


	function getSpecid() {
		return this.Specid;
	}
	this.getSpecid=getSpecid;


	function setSpecid(v){
		this.Specid=v;
	}
	this.setSpecid=setSpecid;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="dateResultsReport"){
				return this.Dateresultsreport ;
			} else 
			if(xmlPath=="timeResultsReport"){
				return this.Timeresultsreport ;
			} else 
			if(xmlPath=="nonNumericResults"){
				return this.Nonnumericresults ;
			} else 
			if(xmlPath=="numericResults"){
				return this.Numericresults ;
			} else 
			if(xmlPath=="unitsNumericResults"){
				return this.Unitsnumericresults ;
			} else 
			if(xmlPath=="tissCollID"){
				return this.Tisscollid ;
			} else 
			if(xmlPath=="specID"){
				return this.Specid ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="dateResultsReport"){
				this.Dateresultsreport=value;
			} else 
			if(xmlPath=="timeResultsReport"){
				this.Timeresultsreport=value;
			} else 
			if(xmlPath=="nonNumericResults"){
				this.Nonnumericresults=value;
			} else 
			if(xmlPath=="numericResults"){
				this.Numericresults=value;
			} else 
			if(xmlPath=="unitsNumericResults"){
				this.Unitsnumericresults=value;
			} else 
			if(xmlPath=="tissCollID"){
				this.Tisscollid=value;
			} else 
			if(xmlPath=="specID"){
				this.Specid=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="dateResultsReport"){
			return "field_data";
		}else if (xmlPath=="timeResultsReport"){
			return "field_data";
		}else if (xmlPath=="nonNumericResults"){
			return "field_data";
		}else if (xmlPath=="numericResults"){
			return "field_data";
		}else if (xmlPath=="unitsNumericResults"){
			return "field_data";
		}else if (xmlPath=="tissCollID"){
			return "field_data";
		}else if (xmlPath=="specID"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<tissue:LabResult";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</tissue:LabResult>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		if (this.Tisscollid!=null)
			attTxt+=" tissCollID=\"" +this.Tisscollid +"\"";
		//NOT REQUIRED FIELD

		if (this.Specid!=null)
			attTxt+=" specID=\"" +this.Specid +"\"";
		//NOT REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Dateresultsreport!=null){
			xmlTxt+="\n<tissue:dateResultsReport";
			xmlTxt+=">";
			xmlTxt+=this.Dateresultsreport;
			xmlTxt+="</tissue:dateResultsReport>";
		}
		if (this.Timeresultsreport!=null){
			xmlTxt+="\n<tissue:timeResultsReport";
			xmlTxt+=">";
			xmlTxt+=this.Timeresultsreport;
			xmlTxt+="</tissue:timeResultsReport>";
		}
		if (this.Nonnumericresults!=null){
			xmlTxt+="\n<tissue:nonNumericResults";
			xmlTxt+=">";
			xmlTxt+=this.Nonnumericresults.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tissue:nonNumericResults>";
		}
		if (this.Numericresults!=null){
			xmlTxt+="\n<tissue:numericResults";
			xmlTxt+=">";
			xmlTxt+=this.Numericresults;
			xmlTxt+="</tissue:numericResults>";
		}
		if (this.Unitsnumericresults!=null){
			xmlTxt+="\n<tissue:unitsNumericResults";
			xmlTxt+=">";
			xmlTxt+=this.Unitsnumericresults;
			xmlTxt+="</tissue:unitsNumericResults>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Dateresultsreport!=null) return true;
		if (this.Timeresultsreport!=null) return true;
		if (this.Nonnumericresults!=null) return true;
		if (this.Numericresults!=null) return true;
		if (this.Unitsnumericresults!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
