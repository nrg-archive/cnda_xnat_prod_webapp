/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function xnat_addField(){
this.xsiType="xnat:addField";

	this.getSchemaElementName=function(){
		return "addField";
	}

	this.getFullSchemaElementName=function(){
		return "xnat:addField";
	}

	this.Addfield=null;


	function getAddfield() {
		return this.Addfield;
	}
	this.getAddfield=getAddfield;


	function setAddfield(v){
		this.Addfield=v;
	}
	this.setAddfield=setAddfield;

	this.Name=null;


	function getName() {
		return this.Name;
	}
	this.getName=getName;


	function setName(v){
		this.Name=v;
	}
	this.setName=setName;

	this.XnatAddfieldId=null;


	function getXnatAddfieldId() {
		return this.XnatAddfieldId;
	}
	this.getXnatAddfieldId=getXnatAddfieldId;


	function setXnatAddfieldId(v){
		this.XnatAddfieldId=v;
	}
	this.setXnatAddfieldId=setXnatAddfieldId;

	this.pet_fspetTimeCourseData_roi_pet_fspettimecoursedata_roi_id_fk=null;


	this.getpet_fspetTimeCourseData_roi_pet_fspettimecoursedata_roi_id=function() {
		return this.pet_fspetTimeCourseData_roi_pet_fspettimecoursedata_roi_id_fk;
	}


	this.setpet_fspetTimeCourseData_roi_pet_fspettimecoursedata_roi_id=function(v){
		this.pet_fspetTimeCourseData_roi_pet_fspettimecoursedata_roi_id_fk=v;
	}

	this.parameters_addparam_xnat_mrScan_xnat_imagescandata_id_fk=null;


	this.getparameters_addparam_xnat_mrScan_xnat_imagescandata_id=function() {
		return this.parameters_addparam_xnat_mrScan_xnat_imagescandata_id_fk;
	}


	this.setparameters_addparam_xnat_mrScan_xnat_imagescandata_id=function(v){
		this.parameters_addparam_xnat_mrScan_xnat_imagescandata_id_fk=v;
	}

	this.parameters_addparam_xnat_recons_xnat_reconstructedimagedata_id_fk=null;


	this.getparameters_addparam_xnat_recons_xnat_reconstructedimagedata_id=function() {
		return this.parameters_addparam_xnat_recons_xnat_reconstructedimagedata_id_fk;
	}


	this.setparameters_addparam_xnat_recons_xnat_reconstructedimagedata_id=function(v){
		this.parameters_addparam_xnat_recons_xnat_reconstructedimagedata_id_fk=v;
	}

	this.parameters_addparam_xnat_petSca_xnat_imagescandata_id_fk=null;


	this.getparameters_addparam_xnat_petSca_xnat_imagescandata_id=function() {
		return this.parameters_addparam_xnat_petSca_xnat_imagescandata_id_fk;
	}


	this.setparameters_addparam_xnat_petSca_xnat_imagescandata_id=function(v){
		this.parameters_addparam_xnat_petSca_xnat_imagescandata_id_fk=v;
	}

	this.parameters_addparam_xnat_imageA_id_fk=null;


	this.getparameters_addparam_xnat_imageA_id=function() {
		return this.parameters_addparam_xnat_imageA_id_fk;
	}


	this.setparameters_addparam_xnat_imageA_id=function(v){
		this.parameters_addparam_xnat_imageA_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="addField"){
				return this.Addfield ;
			} else 
			if(xmlPath=="name"){
				return this.Name ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="xnat_addField_id"){
				return this.XnatAddfieldId ;
			} else 
			if(xmlPath=="pet_fspetTimeCourseData_roi_pet_fspettimecoursedata_roi_id"){
				return this.pet_fspetTimeCourseData_roi_pet_fspettimecoursedata_roi_id_fk ;
			} else 
			if(xmlPath=="parameters_addparam_xnat_mrScan_xnat_imagescandata_id"){
				return this.parameters_addparam_xnat_mrScan_xnat_imagescandata_id_fk ;
			} else 
			if(xmlPath=="parameters_addparam_xnat_recons_xnat_reconstructedimagedata_id"){
				return this.parameters_addparam_xnat_recons_xnat_reconstructedimagedata_id_fk ;
			} else 
			if(xmlPath=="parameters_addparam_xnat_petSca_xnat_imagescandata_id"){
				return this.parameters_addparam_xnat_petSca_xnat_imagescandata_id_fk ;
			} else 
			if(xmlPath=="parameters_addparam_xnat_imageA_id"){
				return this.parameters_addparam_xnat_imageA_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="addField"){
				this.Addfield=value;
			} else 
			if(xmlPath=="name"){
				this.Name=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="xnat_addField_id"){
				this.XnatAddfieldId=value;
			} else 
			if(xmlPath=="pet_fspetTimeCourseData_roi_pet_fspettimecoursedata_roi_id"){
				this.pet_fspetTimeCourseData_roi_pet_fspettimecoursedata_roi_id_fk=value;
			} else 
			if(xmlPath=="parameters_addparam_xnat_mrScan_xnat_imagescandata_id"){
				this.parameters_addparam_xnat_mrScan_xnat_imagescandata_id_fk=value;
			} else 
			if(xmlPath=="parameters_addparam_xnat_recons_xnat_reconstructedimagedata_id"){
				this.parameters_addparam_xnat_recons_xnat_reconstructedimagedata_id_fk=value;
			} else 
			if(xmlPath=="parameters_addparam_xnat_petSca_xnat_imagescandata_id"){
				this.parameters_addparam_xnat_petSca_xnat_imagescandata_id_fk=value;
			} else 
			if(xmlPath=="parameters_addparam_xnat_imageA_id"){
				this.parameters_addparam_xnat_imageA_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="addField"){
			return "field_data";
		}else if (xmlPath=="name"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<xnat:addField";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</xnat:addField>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.XnatAddfieldId!=null){
				if(hiddenCount++>0)str+=",";
				str+="xnat_addField_id=\"" + this.XnatAddfieldId + "\"";
			}
			if(this.pet_fspetTimeCourseData_roi_pet_fspettimecoursedata_roi_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="pet_fspetTimeCourseData_roi_pet_fspettimecoursedata_roi_id=\"" + this.pet_fspetTimeCourseData_roi_pet_fspettimecoursedata_roi_id_fk + "\"";
			}
			if(this.parameters_addparam_xnat_mrScan_xnat_imagescandata_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="parameters_addparam_xnat_mrScan_xnat_imagescandata_id=\"" + this.parameters_addparam_xnat_mrScan_xnat_imagescandata_id_fk + "\"";
			}
			if(this.parameters_addparam_xnat_recons_xnat_reconstructedimagedata_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="parameters_addparam_xnat_recons_xnat_reconstructedimagedata_id=\"" + this.parameters_addparam_xnat_recons_xnat_reconstructedimagedata_id_fk + "\"";
			}
			if(this.parameters_addparam_xnat_petSca_xnat_imagescandata_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="parameters_addparam_xnat_petSca_xnat_imagescandata_id=\"" + this.parameters_addparam_xnat_petSca_xnat_imagescandata_id_fk + "\"";
			}
			if(this.parameters_addparam_xnat_imageA_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="parameters_addparam_xnat_imageA_id=\"" + this.parameters_addparam_xnat_imageA_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.Name!=null)
			attTxt+=" name=\"" +this.Name +"\"";
		//NOT REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Addfield!=null){
			xmlTxt+=this.Addfield.replace(/>/g,"&gt;").replace(/</g,"&lt;");
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.XnatAddfieldId!=null) return true;
			if (this.pet_fspetTimeCourseData_roi_pet_fspettimecoursedata_roi_id_fk!=null) return true;
			if (this.parameters_addparam_xnat_mrScan_xnat_imagescandata_id_fk!=null) return true;
			if (this.parameters_addparam_xnat_recons_xnat_reconstructedimagedata_id_fk!=null) return true;
			if (this.parameters_addparam_xnat_petSca_xnat_imagescandata_id_fk!=null) return true;
			if (this.parameters_addparam_xnat_imageA_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Addfield!=null) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
