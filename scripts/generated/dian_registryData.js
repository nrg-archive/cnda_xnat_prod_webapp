/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function dian_registryData(){
this.xsiType="dian:registryData";

	this.getSchemaElementName=function(){
		return "registryData";
	}

	this.getFullSchemaElementName=function(){
		return "dian:registryData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Site=null;


	function getSite() {
		return this.Site;
	}
	this.getSite=getSite;


	function setSite(v){
		this.Site=v;
	}
	this.setSite=setSite;

	this.Rgcondct=null;


	function getRgcondct() {
		return this.Rgcondct;
	}
	this.getRgcondct=getRgcondct;


	function setRgcondct(v){
		this.Rgcondct=v;
	}
	this.setRgcondct=setRgcondct;

	this.Rgpartial=null;


	function getRgpartial() {
		return this.Rgpartial;
	}
	this.getRgpartial=getRgpartial;


	function setRgpartial(v){
		this.Rgpartial=v;
	}
	this.setRgpartial=setRgpartial;

	this.Visdate=null;


	function getVisdate() {
		return this.Visdate;
	}
	this.getVisdate=getVisdate;


	function setVisdate(v){
		this.Visdate=v;
	}
	this.setVisdate=setVisdate;

	this.Csid1=null;


	function getCsid1() {
		return this.Csid1;
	}
	this.getCsid1=getCsid1;


	function setCsid1(v){
		this.Csid1=v;
	}
	this.setCsid1=setCsid1;

	this.Csid2=null;


	function getCsid2() {
		return this.Csid2;
	}
	this.getCsid2=getCsid2;


	function setCsid2(v){
		this.Csid2=v;
	}
	this.setCsid2=setCsid2;

	this.Csdets=null;


	function getCsdets() {
		return this.Csdets;
	}
	this.getCsdets=getCsdets;


	function setCsdets(v){
		this.Csdets=v;
	}
	this.setCsdets=setCsdets;

	this.Lang=null;


	function getLang() {
		return this.Lang;
	}
	this.getLang=getLang;


	function setLang(v){
		this.Lang=v;
	}
	this.setLang=setLang;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="SITE"){
				return this.Site ;
			} else 
			if(xmlPath=="RGCONDCT"){
				return this.Rgcondct ;
			} else 
			if(xmlPath=="RGPARTIAL"){
				return this.Rgpartial ;
			} else 
			if(xmlPath=="VISDATE"){
				return this.Visdate ;
			} else 
			if(xmlPath=="CSID1"){
				return this.Csid1 ;
			} else 
			if(xmlPath=="CSID2"){
				return this.Csid2 ;
			} else 
			if(xmlPath=="CSDETS"){
				return this.Csdets ;
			} else 
			if(xmlPath=="LANG"){
				return this.Lang ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="SITE"){
				this.Site=value;
			} else 
			if(xmlPath=="RGCONDCT"){
				this.Rgcondct=value;
			} else 
			if(xmlPath=="RGPARTIAL"){
				this.Rgpartial=value;
			} else 
			if(xmlPath=="VISDATE"){
				this.Visdate=value;
			} else 
			if(xmlPath=="CSID1"){
				this.Csid1=value;
			} else 
			if(xmlPath=="CSID2"){
				this.Csid2=value;
			} else 
			if(xmlPath=="CSDETS"){
				this.Csdets=value;
			} else 
			if(xmlPath=="LANG"){
				this.Lang=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="SITE"){
			return "field_data";
		}else if (xmlPath=="RGCONDCT"){
			return "field_data";
		}else if (xmlPath=="RGPARTIAL"){
			return "field_data";
		}else if (xmlPath=="VISDATE"){
			return "field_data";
		}else if (xmlPath=="CSID1"){
			return "field_data";
		}else if (xmlPath=="CSID2"){
			return "field_data";
		}else if (xmlPath=="CSDETS"){
			return "field_data";
		}else if (xmlPath=="LANG"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<dian:REGISTRY";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</dian:REGISTRY>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Site!=null){
			xmlTxt+="\n<dian:SITE";
			xmlTxt+=">";
			xmlTxt+=this.Site.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:SITE>";
		}
		if (this.Rgcondct!=null){
			xmlTxt+="\n<dian:RGCONDCT";
			xmlTxt+=">";
			xmlTxt+=this.Rgcondct;
			xmlTxt+="</dian:RGCONDCT>";
		}
		if (this.Rgpartial!=null){
			xmlTxt+="\n<dian:RGPARTIAL";
			xmlTxt+=">";
			xmlTxt+=this.Rgpartial;
			xmlTxt+="</dian:RGPARTIAL>";
		}
		if (this.Visdate!=null){
			xmlTxt+="\n<dian:VISDATE";
			xmlTxt+=">";
			xmlTxt+=this.Visdate;
			xmlTxt+="</dian:VISDATE>";
		}
		if (this.Csid1!=null){
			xmlTxt+="\n<dian:CSID1";
			xmlTxt+=">";
			xmlTxt+=this.Csid1.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:CSID1>";
		}
		if (this.Csid2!=null){
			xmlTxt+="\n<dian:CSID2";
			xmlTxt+=">";
			xmlTxt+=this.Csid2.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:CSID2>";
		}
		if (this.Csdets!=null){
			xmlTxt+="\n<dian:CSDETS";
			xmlTxt+=">";
			xmlTxt+=this.Csdets.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:CSDETS>";
		}
		if (this.Lang!=null){
			xmlTxt+="\n<dian:LANG";
			xmlTxt+=">";
			xmlTxt+=this.Lang;
			xmlTxt+="</dian:LANG>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Site!=null) return true;
		if (this.Rgcondct!=null) return true;
		if (this.Rgpartial!=null) return true;
		if (this.Visdate!=null) return true;
		if (this.Csid1!=null) return true;
		if (this.Csid2!=null) return true;
		if (this.Csdets!=null) return true;
		if (this.Lang!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
