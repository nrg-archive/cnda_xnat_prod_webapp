/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function ls2_lsHealthData(){
this.xsiType="ls2:lsHealthData";

	this.getSchemaElementName=function(){
		return "lsHealthData";
	}

	this.getFullSchemaElementName=function(){
		return "ls2:lsHealthData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Rating=null;


	function getRating() {
		return this.Rating;
	}
	this.getRating=getRating;


	function setRating(v){
		this.Rating=v;
	}
	this.setRating=setRating;

	this.Satisfaction=null;


	function getSatisfaction() {
		return this.Satisfaction;
	}
	this.getSatisfaction=getSatisfaction;


	function setSatisfaction(v){
		this.Satisfaction=v;
	}
	this.setSatisfaction=setSatisfaction;

	this.Interferes=null;


	function getInterferes() {
		return this.Interferes;
	}
	this.getInterferes=getInterferes;


	function setInterferes(v){
		this.Interferes=v;
	}
	this.setInterferes=setInterferes;
	this.Activitylimits_activity =new Array();

	function getActivitylimits_activity() {
		return this.Activitylimits_activity;
	}
	this.getActivitylimits_activity=getActivitylimits_activity;


	function addActivitylimits_activity(v){
		this.Activitylimits_activity.push(v);
	}
	this.addActivitylimits_activity=addActivitylimits_activity;

	this.Activitylimits_avelimit=null;


	function getActivitylimits_avelimit() {
		return this.Activitylimits_avelimit;
	}
	this.getActivitylimits_avelimit=getActivitylimits_avelimit;


	function setActivitylimits_avelimit(v){
		this.Activitylimits_avelimit=v;
	}
	this.setActivitylimits_avelimit=setActivitylimits_avelimit;

	this.Headinjury=null;


	function getHeadinjury() {
		return this.Headinjury;
	}
	this.getHeadinjury=getHeadinjury;


	function setHeadinjury(v){
		this.Headinjury=v;
	}
	this.setHeadinjury=setHeadinjury;

	this.Hypertension_past=null;


	function getHypertension_past() {
		return this.Hypertension_past;
	}
	this.getHypertension_past=getHypertension_past;


	function setHypertension_past(v){
		this.Hypertension_past=v;
	}
	this.setHypertension_past=setHypertension_past;

	this.Hypertension_now=null;


	function getHypertension_now() {
		return this.Hypertension_now;
	}
	this.getHypertension_now=getHypertension_now;


	function setHypertension_now(v){
		this.Hypertension_now=v;
	}
	this.setHypertension_now=setHypertension_now;

	this.Hypertension_medication=null;


	function getHypertension_medication() {
		return this.Hypertension_medication;
	}
	this.getHypertension_medication=getHypertension_medication;


	function setHypertension_medication(v){
		this.Hypertension_medication=v;
	}
	this.setHypertension_medication=setHypertension_medication;

	this.Postmenopausal=null;


	function getPostmenopausal() {
		return this.Postmenopausal;
	}
	this.getPostmenopausal=getPostmenopausal;


	function setPostmenopausal(v){
		this.Postmenopausal=v;
	}
	this.setPostmenopausal=setPostmenopausal;

	this.Estreplcther=null;


	function getEstreplcther() {
		return this.Estreplcther;
	}
	this.getEstreplcther=getEstreplcther;


	function setEstreplcther(v){
		this.Estreplcther=v;
	}
	this.setEstreplcther=setEstreplcther;
	this.Medications_medication =new Array();

	function getMedications_medication() {
		return this.Medications_medication;
	}
	this.getMedications_medication=getMedications_medication;


	function addMedications_medication(v){
		this.Medications_medication.push(v);
	}
	this.addMedications_medication=addMedications_medication;

	this.Medications_others=null;


	function getMedications_others() {
		return this.Medications_others;
	}
	this.getMedications_others=getMedications_others;


	function setMedications_others(v){
		this.Medications_others=v;
	}
	this.setMedications_others=setMedications_others;
	this.Illnesses_illness =new Array();

	function getIllnesses_illness() {
		return this.Illnesses_illness;
	}
	this.getIllnesses_illness=getIllnesses_illness;


	function addIllnesses_illness(v){
		this.Illnesses_illness.push(v);
	}
	this.addIllnesses_illness=addIllnesses_illness;

	this.Bonefrac=null;


	function getBonefrac() {
		return this.Bonefrac;
	}
	this.getBonefrac=getBonefrac;


	function setBonefrac(v){
		this.Bonefrac=v;
	}
	this.setBonefrac=setBonefrac;

	this.Surgery=null;


	function getSurgery() {
		return this.Surgery;
	}
	this.getSurgery=getSurgery;


	function setSurgery(v){
		this.Surgery=v;
	}
	this.setSurgery=setSurgery;

	this.Hospital=null;


	function getHospital() {
		return this.Hospital;
	}
	this.getHospital=getHospital;


	function setHospital(v){
		this.Hospital=v;
	}
	this.setHospital=setHospital;
	this.Substanceuse_substance =new Array();

	function getSubstanceuse_substance() {
		return this.Substanceuse_substance;
	}
	this.getSubstanceuse_substance=getSubstanceuse_substance;


	function addSubstanceuse_substance(v){
		this.Substanceuse_substance.push(v);
	}
	this.addSubstanceuse_substance=addSubstanceuse_substance;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="rating"){
				return this.Rating ;
			} else 
			if(xmlPath=="satisfaction"){
				return this.Satisfaction ;
			} else 
			if(xmlPath=="interferes"){
				return this.Interferes ;
			} else 
			if(xmlPath=="activityLimits/activity"){
				return this.Activitylimits_activity ;
			} else 
			if(xmlPath.startsWith("activityLimits/activity")){
				xmlPath=xmlPath.substring(23);
				if(xmlPath=="")return this.Activitylimits_activity ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Activitylimits_activity.length;whereCount++){

					var tempValue=this.Activitylimits_activity[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Activitylimits_activity[whereCount]);

					}

				}
				}else{

				whereArray=this.Activitylimits_activity;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="activityLimits/aveLimit"){
				return this.Activitylimits_avelimit ;
			} else 
			if(xmlPath=="headInjury"){
				return this.Headinjury ;
			} else 
			if(xmlPath=="hypertension/past"){
				return this.Hypertension_past ;
			} else 
			if(xmlPath=="hypertension/now"){
				return this.Hypertension_now ;
			} else 
			if(xmlPath=="hypertension/medication"){
				return this.Hypertension_medication ;
			} else 
			if(xmlPath=="postMenopausal"){
				return this.Postmenopausal ;
			} else 
			if(xmlPath=="estReplcTher"){
				return this.Estreplcther ;
			} else 
			if(xmlPath=="medications/medication"){
				return this.Medications_medication ;
			} else 
			if(xmlPath.startsWith("medications/medication")){
				xmlPath=xmlPath.substring(22);
				if(xmlPath=="")return this.Medications_medication ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Medications_medication.length;whereCount++){

					var tempValue=this.Medications_medication[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Medications_medication[whereCount]);

					}

				}
				}else{

				whereArray=this.Medications_medication;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="medications/others"){
				return this.Medications_others ;
			} else 
			if(xmlPath=="illnesses/illness"){
				return this.Illnesses_illness ;
			} else 
			if(xmlPath.startsWith("illnesses/illness")){
				xmlPath=xmlPath.substring(17);
				if(xmlPath=="")return this.Illnesses_illness ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Illnesses_illness.length;whereCount++){

					var tempValue=this.Illnesses_illness[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Illnesses_illness[whereCount]);

					}

				}
				}else{

				whereArray=this.Illnesses_illness;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="boneFrac"){
				return this.Bonefrac ;
			} else 
			if(xmlPath=="surgery"){
				return this.Surgery ;
			} else 
			if(xmlPath=="hospital"){
				return this.Hospital ;
			} else 
			if(xmlPath=="substanceUse/substance"){
				return this.Substanceuse_substance ;
			} else 
			if(xmlPath.startsWith("substanceUse/substance")){
				xmlPath=xmlPath.substring(22);
				if(xmlPath=="")return this.Substanceuse_substance ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Substanceuse_substance.length;whereCount++){

					var tempValue=this.Substanceuse_substance[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Substanceuse_substance[whereCount]);

					}

				}
				}else{

				whereArray=this.Substanceuse_substance;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="rating"){
				this.Rating=value;
			} else 
			if(xmlPath=="satisfaction"){
				this.Satisfaction=value;
			} else 
			if(xmlPath=="interferes"){
				this.Interferes=value;
			} else 
			if(xmlPath=="activityLimits/activity"){
				this.Activitylimits_activity=value;
			} else 
			if(xmlPath.startsWith("activityLimits/activity")){
				xmlPath=xmlPath.substring(23);
				if(xmlPath=="")return this.Activitylimits_activity ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Activitylimits_activity.length;whereCount++){

					var tempValue=this.Activitylimits_activity[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Activitylimits_activity[whereCount]);

					}

				}
				}else{

				whereArray=this.Activitylimits_activity;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("ls2:lsActivityLimit");//omUtils.js
					}
					this.addActivitylimits_activity(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="activityLimits/aveLimit"){
				this.Activitylimits_avelimit=value;
			} else 
			if(xmlPath=="headInjury"){
				this.Headinjury=value;
			} else 
			if(xmlPath=="hypertension/past"){
				this.Hypertension_past=value;
			} else 
			if(xmlPath=="hypertension/now"){
				this.Hypertension_now=value;
			} else 
			if(xmlPath=="hypertension/medication"){
				this.Hypertension_medication=value;
			} else 
			if(xmlPath=="postMenopausal"){
				this.Postmenopausal=value;
			} else 
			if(xmlPath=="estReplcTher"){
				this.Estreplcther=value;
			} else 
			if(xmlPath=="medications/medication"){
				this.Medications_medication=value;
			} else 
			if(xmlPath.startsWith("medications/medication")){
				xmlPath=xmlPath.substring(22);
				if(xmlPath=="")return this.Medications_medication ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Medications_medication.length;whereCount++){

					var tempValue=this.Medications_medication[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Medications_medication[whereCount]);

					}

				}
				}else{

				whereArray=this.Medications_medication;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("ls2:lsMedication");//omUtils.js
					}
					this.addMedications_medication(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="medications/others"){
				this.Medications_others=value;
			} else 
			if(xmlPath=="illnesses/illness"){
				this.Illnesses_illness=value;
			} else 
			if(xmlPath.startsWith("illnesses/illness")){
				xmlPath=xmlPath.substring(17);
				if(xmlPath=="")return this.Illnesses_illness ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Illnesses_illness.length;whereCount++){

					var tempValue=this.Illnesses_illness[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Illnesses_illness[whereCount]);

					}

				}
				}else{

				whereArray=this.Illnesses_illness;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("ls2:lsIllness");//omUtils.js
					}
					this.addIllnesses_illness(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="boneFrac"){
				this.Bonefrac=value;
			} else 
			if(xmlPath=="surgery"){
				this.Surgery=value;
			} else 
			if(xmlPath=="hospital"){
				this.Hospital=value;
			} else 
			if(xmlPath=="substanceUse/substance"){
				this.Substanceuse_substance=value;
			} else 
			if(xmlPath.startsWith("substanceUse/substance")){
				xmlPath=xmlPath.substring(22);
				if(xmlPath=="")return this.Substanceuse_substance ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Substanceuse_substance.length;whereCount++){

					var tempValue=this.Substanceuse_substance[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Substanceuse_substance[whereCount]);

					}

				}
				}else{

				whereArray=this.Substanceuse_substance;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("ls2:lsSubstance");//omUtils.js
					}
					this.addSubstanceuse_substance(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="activityLimits/activity"){
			this.addActivitylimits_activity(v);
		}else if (xmlPath=="medications/medication"){
			this.addMedications_medication(v);
		}else if (xmlPath=="illnesses/illness"){
			this.addIllnesses_illness(v);
		}else if (xmlPath=="substanceUse/substance"){
			this.addSubstanceuse_substance(v);
		}
		else{
			this.extension.setReferenceField(xmlPath,v);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="activityLimits/activity"){
			return "http://nrg.wustl.edu/ls2:lsActivityLimit";
		}else if (xmlPath=="medications/medication"){
			return "http://nrg.wustl.edu/ls2:lsMedication";
		}else if (xmlPath=="illnesses/illness"){
			return "http://nrg.wustl.edu/ls2:lsIllness";
		}else if (xmlPath=="substanceUse/substance"){
			return "http://nrg.wustl.edu/ls2:lsSubstance";
		}
		else{
			return this.extension.getReferenceFieldName(xmlPath);
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="rating"){
			return "field_data";
		}else if (xmlPath=="satisfaction"){
			return "field_data";
		}else if (xmlPath=="interferes"){
			return "field_data";
		}else if (xmlPath=="activityLimits/activity"){
			return "field_multi_reference";
		}else if (xmlPath=="activityLimits/aveLimit"){
			return "field_data";
		}else if (xmlPath=="headInjury"){
			return "field_data";
		}else if (xmlPath=="hypertension/past"){
			return "field_data";
		}else if (xmlPath=="hypertension/now"){
			return "field_data";
		}else if (xmlPath=="hypertension/medication"){
			return "field_data";
		}else if (xmlPath=="postMenopausal"){
			return "field_data";
		}else if (xmlPath=="estReplcTher"){
			return "field_data";
		}else if (xmlPath=="medications/medication"){
			return "field_NO_CHILD";
		}else if (xmlPath=="medications/others"){
			return "field_data";
		}else if (xmlPath=="illnesses/illness"){
			return "field_NO_CHILD";
		}else if (xmlPath=="boneFrac"){
			return "field_data";
		}else if (xmlPath=="surgery"){
			return "field_data";
		}else if (xmlPath=="hospital"){
			return "field_data";
		}else if (xmlPath=="substanceUse/substance"){
			return "field_multi_reference";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<ls2:LS_Health";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</ls2:LS_Health>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Rating!=null){
			xmlTxt+="\n<ls2:rating";
			xmlTxt+=">";
			xmlTxt+=this.Rating;
			xmlTxt+="</ls2:rating>";
		}
		if (this.Satisfaction!=null){
			xmlTxt+="\n<ls2:satisfaction";
			xmlTxt+=">";
			xmlTxt+=this.Satisfaction;
			xmlTxt+="</ls2:satisfaction>";
		}
		if (this.Interferes!=null){
			xmlTxt+="\n<ls2:interferes";
			xmlTxt+=">";
			xmlTxt+=this.Interferes;
			xmlTxt+="</ls2:interferes>";
		}
		var ActivitylimitsATT = ""
		if (this.Activitylimits_avelimit!=null)
			ActivitylimitsATT+=" aveLimit=\"" + this.Activitylimits_avelimit + "\"";
			var child0=0;
			var att0=0;
			if(this.Activitylimits_avelimit!=null)
			att0++;
			child0+=this.Activitylimits_activity.length;
			if(child0>0 || att0>0){
				xmlTxt+="\n<ls2:activityLimits";
				xmlTxt+=ActivitylimitsATT;
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var Activitylimits_activityCOUNT=0;Activitylimits_activityCOUNT<this.Activitylimits_activity.length;Activitylimits_activityCOUNT++){
			xmlTxt +="\n<ls2:activity";
			xmlTxt +=this.Activitylimits_activity[Activitylimits_activityCOUNT].getXMLAtts();
			if(this.Activitylimits_activity[Activitylimits_activityCOUNT].xsiType!="ls2:lsActivityLimit"){
				xmlTxt+=" xsi:type=\"" + this.Activitylimits_activity[Activitylimits_activityCOUNT].xsiType + "\"";
			}
			if (this.Activitylimits_activity[Activitylimits_activityCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Activitylimits_activity[Activitylimits_activityCOUNT].getXMLBody(preventComments);
					xmlTxt+="</ls2:activity>";
			}else {xmlTxt+="/>";}
		}
				xmlTxt+="\n</ls2:activityLimits>";
			}
			}

		if (this.Headinjury!=null){
			xmlTxt+="\n<ls2:headInjury";
			xmlTxt+=">";
			xmlTxt+=this.Headinjury;
			xmlTxt+="</ls2:headInjury>";
		}
			var child1=0;
			var att1=0;
			if(this.Hypertension_now!=null)
			child1++;
			if(this.Hypertension_past!=null)
			child1++;
			if(this.Hypertension_medication!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<ls2:hypertension";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Hypertension_past!=null){
			xmlTxt+="\n<ls2:past";
			xmlTxt+=">";
			xmlTxt+=this.Hypertension_past;
			xmlTxt+="</ls2:past>";
		}
		if (this.Hypertension_now!=null){
			xmlTxt+="\n<ls2:now";
			xmlTxt+=">";
			xmlTxt+=this.Hypertension_now;
			xmlTxt+="</ls2:now>";
		}
		if (this.Hypertension_medication!=null){
			xmlTxt+="\n<ls2:medication";
			xmlTxt+=">";
			xmlTxt+=this.Hypertension_medication;
			xmlTxt+="</ls2:medication>";
		}
				xmlTxt+="\n</ls2:hypertension>";
			}
			}

		if (this.Postmenopausal!=null){
			xmlTxt+="\n<ls2:postMenopausal";
			xmlTxt+=">";
			xmlTxt+=this.Postmenopausal;
			xmlTxt+="</ls2:postMenopausal>";
		}
		if (this.Estreplcther!=null){
			xmlTxt+="\n<ls2:estReplcTher";
			xmlTxt+=">";
			xmlTxt+=this.Estreplcther;
			xmlTxt+="</ls2:estReplcTher>";
		}
		var MedicationsATT = ""
		if (this.Medications_others!=null)
			MedicationsATT+=" others=\"" + this.Medications_others + "\"";
			var child2=0;
			var att2=0;
			if(this.Medications_others!=null)
			att2++;
			child2+=this.Medications_medication.length;
			if(child2>0 || att2>0){
				xmlTxt+="\n<ls2:medications";
				xmlTxt+=MedicationsATT;
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var Medications_medicationCOUNT=0;Medications_medicationCOUNT<this.Medications_medication.length;Medications_medicationCOUNT++){
			xmlTxt +="\n<ls2:medication";
			xmlTxt +=this.Medications_medication[Medications_medicationCOUNT].getXMLAtts();
			if(this.Medications_medication[Medications_medicationCOUNT].xsiType!="ls2:lsMedication"){
				xmlTxt+=" xsi:type=\"" + this.Medications_medication[Medications_medicationCOUNT].xsiType + "\"";
			}
			if (this.Medications_medication[Medications_medicationCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Medications_medication[Medications_medicationCOUNT].getXMLBody(preventComments);
					xmlTxt+="</ls2:medication>";
			}else {xmlTxt+="/>";}
		}
				xmlTxt+="\n</ls2:medications>";
			}
			}

			var child3=0;
			var att3=0;
			child3+=this.Illnesses_illness.length;
			if(child3>0 || att3>0){
				xmlTxt+="\n<ls2:illnesses";
			if(child3==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var Illnesses_illnessCOUNT=0;Illnesses_illnessCOUNT<this.Illnesses_illness.length;Illnesses_illnessCOUNT++){
			xmlTxt +="\n<ls2:illness";
			xmlTxt +=this.Illnesses_illness[Illnesses_illnessCOUNT].getXMLAtts();
			if(this.Illnesses_illness[Illnesses_illnessCOUNT].xsiType!="ls2:lsIllness"){
				xmlTxt+=" xsi:type=\"" + this.Illnesses_illness[Illnesses_illnessCOUNT].xsiType + "\"";
			}
			if (this.Illnesses_illness[Illnesses_illnessCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Illnesses_illness[Illnesses_illnessCOUNT].getXMLBody(preventComments);
					xmlTxt+="</ls2:illness>";
			}else {xmlTxt+="/>";}
		}
				xmlTxt+="\n</ls2:illnesses>";
			}
			}

		if (this.Bonefrac!=null){
			xmlTxt+="\n<ls2:boneFrac";
			xmlTxt+=">";
			xmlTxt+=this.Bonefrac;
			xmlTxt+="</ls2:boneFrac>";
		}
		if (this.Surgery!=null){
			xmlTxt+="\n<ls2:surgery";
			xmlTxt+=">";
			xmlTxt+=this.Surgery;
			xmlTxt+="</ls2:surgery>";
		}
		if (this.Hospital!=null){
			xmlTxt+="\n<ls2:hospital";
			xmlTxt+=">";
			xmlTxt+=this.Hospital;
			xmlTxt+="</ls2:hospital>";
		}
			var child4=0;
			var att4=0;
			child4+=this.Substanceuse_substance.length;
			if(child4>0 || att4>0){
				xmlTxt+="\n<ls2:substanceUse";
			if(child4==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var Substanceuse_substanceCOUNT=0;Substanceuse_substanceCOUNT<this.Substanceuse_substance.length;Substanceuse_substanceCOUNT++){
			xmlTxt +="\n<ls2:substance";
			xmlTxt +=this.Substanceuse_substance[Substanceuse_substanceCOUNT].getXMLAtts();
			if(this.Substanceuse_substance[Substanceuse_substanceCOUNT].xsiType!="ls2:lsSubstance"){
				xmlTxt+=" xsi:type=\"" + this.Substanceuse_substance[Substanceuse_substanceCOUNT].xsiType + "\"";
			}
			if (this.Substanceuse_substance[Substanceuse_substanceCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Substanceuse_substance[Substanceuse_substanceCOUNT].getXMLBody(preventComments);
					xmlTxt+="</ls2:substance>";
			}else {xmlTxt+="/>";}
		}
				xmlTxt+="\n</ls2:substanceUse>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Rating!=null) return true;
		if (this.Satisfaction!=null) return true;
		if (this.Interferes!=null) return true;
		if (this.Activitylimits_avelimit!=null)
			return true;
			if(this.Activitylimits_avelimit!=null) return true;
			if(this.Activitylimits_activity.length>0)return true;
		if (this.Headinjury!=null) return true;
			if(this.Hypertension_now!=null) return true;
			if(this.Hypertension_past!=null) return true;
			if(this.Hypertension_medication!=null) return true;
		if (this.Postmenopausal!=null) return true;
		if (this.Estreplcther!=null) return true;
		if (this.Medications_others!=null)
			return true;
			if(this.Medications_others!=null) return true;
			if(this.Medications_medication.length>0)return true;
			if(this.Illnesses_illness.length>0)return true;
		if (this.Bonefrac!=null) return true;
		if (this.Surgery!=null) return true;
		if (this.Hospital!=null) return true;
			if(this.Substanceuse_substance.length>0)return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
