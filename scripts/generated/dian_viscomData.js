/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function dian_viscomData(){
this.xsiType="dian:viscomData";

	this.getSchemaElementName=function(){
		return "viscomData";
	}

	this.getFullSchemaElementName=function(){
		return "dian:viscomData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Viscomm1=null;


	function getViscomm1() {
		return this.Viscomm1;
	}
	this.getViscomm1=getViscomm1;


	function setViscomm1(v){
		this.Viscomm1=v;
	}
	this.setViscomm1=setViscomm1;

	this.Viscomm2=null;


	function getViscomm2() {
		return this.Viscomm2;
	}
	this.getViscomm2=getViscomm2;


	function setViscomm2(v){
		this.Viscomm2=v;
	}
	this.setViscomm2=setViscomm2;

	this.Viscomm3=null;


	function getViscomm3() {
		return this.Viscomm3;
	}
	this.getViscomm3=getViscomm3;


	function setViscomm3(v){
		this.Viscomm3=v;
	}
	this.setViscomm3=setViscomm3;

	this.Viscomm4=null;


	function getViscomm4() {
		return this.Viscomm4;
	}
	this.getViscomm4=getViscomm4;


	function setViscomm4(v){
		this.Viscomm4=v;
	}
	this.setViscomm4=setViscomm4;

	this.Viscomm5=null;


	function getViscomm5() {
		return this.Viscomm5;
	}
	this.getViscomm5=getViscomm5;


	function setViscomm5(v){
		this.Viscomm5=v;
	}
	this.setViscomm5=setViscomm5;

	this.Viscomm6=null;


	function getViscomm6() {
		return this.Viscomm6;
	}
	this.getViscomm6=getViscomm6;


	function setViscomm6(v){
		this.Viscomm6=v;
	}
	this.setViscomm6=setViscomm6;

	this.Viscomm7=null;


	function getViscomm7() {
		return this.Viscomm7;
	}
	this.getViscomm7=getViscomm7;


	function setViscomm7(v){
		this.Viscomm7=v;
	}
	this.setViscomm7=setViscomm7;

	this.Viscomm8=null;


	function getViscomm8() {
		return this.Viscomm8;
	}
	this.getViscomm8=getViscomm8;


	function setViscomm8(v){
		this.Viscomm8=v;
	}
	this.setViscomm8=setViscomm8;

	this.Viscomm9=null;


	function getViscomm9() {
		return this.Viscomm9;
	}
	this.getViscomm9=getViscomm9;


	function setViscomm9(v){
		this.Viscomm9=v;
	}
	this.setViscomm9=setViscomm9;

	this.Viscomm10=null;


	function getViscomm10() {
		return this.Viscomm10;
	}
	this.getViscomm10=getViscomm10;


	function setViscomm10(v){
		this.Viscomm10=v;
	}
	this.setViscomm10=setViscomm10;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="VISCOMM1"){
				return this.Viscomm1 ;
			} else 
			if(xmlPath=="VISCOMM2"){
				return this.Viscomm2 ;
			} else 
			if(xmlPath=="VISCOMM3"){
				return this.Viscomm3 ;
			} else 
			if(xmlPath=="VISCOMM4"){
				return this.Viscomm4 ;
			} else 
			if(xmlPath=="VISCOMM5"){
				return this.Viscomm5 ;
			} else 
			if(xmlPath=="VISCOMM6"){
				return this.Viscomm6 ;
			} else 
			if(xmlPath=="VISCOMM7"){
				return this.Viscomm7 ;
			} else 
			if(xmlPath=="VISCOMM8"){
				return this.Viscomm8 ;
			} else 
			if(xmlPath=="VISCOMM9"){
				return this.Viscomm9 ;
			} else 
			if(xmlPath=="VISCOMM10"){
				return this.Viscomm10 ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="VISCOMM1"){
				this.Viscomm1=value;
			} else 
			if(xmlPath=="VISCOMM2"){
				this.Viscomm2=value;
			} else 
			if(xmlPath=="VISCOMM3"){
				this.Viscomm3=value;
			} else 
			if(xmlPath=="VISCOMM4"){
				this.Viscomm4=value;
			} else 
			if(xmlPath=="VISCOMM5"){
				this.Viscomm5=value;
			} else 
			if(xmlPath=="VISCOMM6"){
				this.Viscomm6=value;
			} else 
			if(xmlPath=="VISCOMM7"){
				this.Viscomm7=value;
			} else 
			if(xmlPath=="VISCOMM8"){
				this.Viscomm8=value;
			} else 
			if(xmlPath=="VISCOMM9"){
				this.Viscomm9=value;
			} else 
			if(xmlPath=="VISCOMM10"){
				this.Viscomm10=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="VISCOMM1"){
			return "field_data";
		}else if (xmlPath=="VISCOMM2"){
			return "field_data";
		}else if (xmlPath=="VISCOMM3"){
			return "field_data";
		}else if (xmlPath=="VISCOMM4"){
			return "field_data";
		}else if (xmlPath=="VISCOMM5"){
			return "field_data";
		}else if (xmlPath=="VISCOMM6"){
			return "field_data";
		}else if (xmlPath=="VISCOMM7"){
			return "field_data";
		}else if (xmlPath=="VISCOMM8"){
			return "field_data";
		}else if (xmlPath=="VISCOMM9"){
			return "field_data";
		}else if (xmlPath=="VISCOMM10"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<dian:VISCOM";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</dian:VISCOM>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Viscomm1!=null){
			xmlTxt+="\n<dian:VISCOMM1";
			xmlTxt+=">";
			xmlTxt+=this.Viscomm1.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:VISCOMM1>";
		}
		if (this.Viscomm2!=null){
			xmlTxt+="\n<dian:VISCOMM2";
			xmlTxt+=">";
			xmlTxt+=this.Viscomm2.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:VISCOMM2>";
		}
		if (this.Viscomm3!=null){
			xmlTxt+="\n<dian:VISCOMM3";
			xmlTxt+=">";
			xmlTxt+=this.Viscomm3.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:VISCOMM3>";
		}
		if (this.Viscomm4!=null){
			xmlTxt+="\n<dian:VISCOMM4";
			xmlTxt+=">";
			xmlTxt+=this.Viscomm4.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:VISCOMM4>";
		}
		if (this.Viscomm5!=null){
			xmlTxt+="\n<dian:VISCOMM5";
			xmlTxt+=">";
			xmlTxt+=this.Viscomm5.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:VISCOMM5>";
		}
		if (this.Viscomm6!=null){
			xmlTxt+="\n<dian:VISCOMM6";
			xmlTxt+=">";
			xmlTxt+=this.Viscomm6.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:VISCOMM6>";
		}
		if (this.Viscomm7!=null){
			xmlTxt+="\n<dian:VISCOMM7";
			xmlTxt+=">";
			xmlTxt+=this.Viscomm7.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:VISCOMM7>";
		}
		if (this.Viscomm8!=null){
			xmlTxt+="\n<dian:VISCOMM8";
			xmlTxt+=">";
			xmlTxt+=this.Viscomm8.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:VISCOMM8>";
		}
		if (this.Viscomm9!=null){
			xmlTxt+="\n<dian:VISCOMM9";
			xmlTxt+=">";
			xmlTxt+=this.Viscomm9.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:VISCOMM9>";
		}
		if (this.Viscomm10!=null){
			xmlTxt+="\n<dian:VISCOMM10";
			xmlTxt+=">";
			xmlTxt+=this.Viscomm10.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:VISCOMM10>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Viscomm1!=null) return true;
		if (this.Viscomm2!=null) return true;
		if (this.Viscomm3!=null) return true;
		if (this.Viscomm4!=null) return true;
		if (this.Viscomm5!=null) return true;
		if (this.Viscomm6!=null) return true;
		if (this.Viscomm7!=null) return true;
		if (this.Viscomm8!=null) return true;
		if (this.Viscomm9!=null) return true;
		if (this.Viscomm10!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
