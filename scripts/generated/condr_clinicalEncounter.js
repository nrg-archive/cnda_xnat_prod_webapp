/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:04 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function condr_clinicalEncounter(){
this.xsiType="condr:clinicalEncounter";

	this.getSchemaElementName=function(){
		return "clinicalEncounter";
	}

	this.getFullSchemaElementName=function(){
		return "condr:clinicalEncounter";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Karnofskyperformancestatus=null;


	function getKarnofskyperformancestatus() {
		return this.Karnofskyperformancestatus;
	}
	this.getKarnofskyperformancestatus=getKarnofskyperformancestatus;


	function setKarnofskyperformancestatus(v){
		this.Karnofskyperformancestatus=v;
	}
	this.setKarnofskyperformancestatus=setKarnofskyperformancestatus;

	this.Rpaclass=null;


	function getRpaclass() {
		return this.Rpaclass;
	}
	this.getRpaclass=getRpaclass;


	function setRpaclass(v){
		this.Rpaclass=v;
	}
	this.setRpaclass=setRpaclass;
	this.Newdeficits =null;
	function getNewdeficits() {
		return this.Newdeficits;
	}
	this.getNewdeficits=getNewdeficits;


	function setNewdeficits(v){
		this.Newdeficits =v;
	}
	this.setNewdeficits=setNewdeficits;

	this.Newdeficits_NewdeficitsSfDeficitsId=null;


	function getNewdeficits_NewdeficitsSfDeficitsId(){
		return this.Newdeficits_NewdeficitsSfDeficitsId;
	}
	this.getNewdeficits_NewdeficitsSfDeficitsId=getNewdeficits_NewdeficitsSfDeficitsId;


	function setNewdeficits_NewdeficitsSfDeficitsId(v){
		this.Newdeficits_NewdeficitsSfDeficitsId=v;
	}
	this.setNewdeficits_NewdeficitsSfDeficitsId=setNewdeficits_NewdeficitsSfDeficitsId;

	this.Clinicaldeterioration=null;


	function getClinicaldeterioration() {
		return this.Clinicaldeterioration;
	}
	this.getClinicaldeterioration=getClinicaldeterioration;


	function setClinicaldeterioration(v){
		this.Clinicaldeterioration=v;
	}
	this.setClinicaldeterioration=setClinicaldeterioration;


	this.isClinicaldeterioration=function(defaultValue) {
		if(this.Clinicaldeterioration==null)return defaultValue;
		if(this.Clinicaldeterioration=="1" || this.Clinicaldeterioration==true)return true;
		return false;
	}

	this.Steroids=null;


	function getSteroids() {
		return this.Steroids;
	}
	this.getSteroids=getSteroids;


	function setSteroids(v){
		this.Steroids=v;
	}
	this.setSteroids=setSteroids;


	this.isSteroids=function(defaultValue) {
		if(this.Steroids==null)return defaultValue;
		if(this.Steroids=="1" || this.Steroids==true)return true;
		return false;
	}

	this.Steroiddose=null;


	function getSteroiddose() {
		return this.Steroiddose;
	}
	this.getSteroiddose=getSteroiddose;


	function setSteroiddose(v){
		this.Steroiddose=v;
	}
	this.setSteroiddose=setSteroiddose;

	this.Steroiddoseincreasing=null;


	function getSteroiddoseincreasing() {
		return this.Steroiddoseincreasing;
	}
	this.getSteroiddoseincreasing=getSteroiddoseincreasing;


	function setSteroiddoseincreasing(v){
		this.Steroiddoseincreasing=v;
	}
	this.setSteroiddoseincreasing=setSteroiddoseincreasing;


	this.isSteroiddoseincreasing=function(defaultValue) {
		if(this.Steroiddoseincreasing==null)return defaultValue;
		if(this.Steroiddoseincreasing=="1" || this.Steroiddoseincreasing==true)return true;
		return false;
	}

	this.Progression=null;


	function getProgression() {
		return this.Progression;
	}
	this.getProgression=getProgression;


	function setProgression(v){
		this.Progression=v;
	}
	this.setProgression=setProgression;


	this.isProgression=function(defaultValue) {
		if(this.Progression==null)return defaultValue;
		if(this.Progression=="1" || this.Progression==true)return true;
		return false;
	}

	this.Pseudoprogression=null;


	function getPseudoprogression() {
		return this.Pseudoprogression;
	}
	this.getPseudoprogression=getPseudoprogression;


	function setPseudoprogression(v){
		this.Pseudoprogression=v;
	}
	this.setPseudoprogression=setPseudoprogression;


	this.isPseudoprogression=function(defaultValue) {
		if(this.Pseudoprogression==null)return defaultValue;
		if(this.Pseudoprogression=="1" || this.Pseudoprogression==true)return true;
		return false;
	}

	this.Response=null;


	function getResponse() {
		return this.Response;
	}
	this.getResponse=getResponse;


	function setResponse(v){
		this.Response=v;
	}
	this.setResponse=setResponse;

	this.Changeintreatment=null;


	function getChangeintreatment() {
		return this.Changeintreatment;
	}
	this.getChangeintreatment=getChangeintreatment;


	function setChangeintreatment(v){
		this.Changeintreatment=v;
	}
	this.setChangeintreatment=setChangeintreatment;


	this.isChangeintreatment=function(defaultValue) {
		if(this.Changeintreatment==null)return defaultValue;
		if(this.Changeintreatment=="1" || this.Changeintreatment==true)return true;
		return false;
	}

	this.Changeintreatmentto=null;


	function getChangeintreatmentto() {
		return this.Changeintreatmentto;
	}
	this.getChangeintreatmentto=getChangeintreatmentto;


	function setChangeintreatmentto(v){
		this.Changeintreatmentto=v;
	}
	this.setChangeintreatmentto=setChangeintreatmentto;

	this.Clinicalencounternotes=null;


	function getClinicalencounternotes() {
		return this.Clinicalencounternotes;
	}
	this.getClinicalencounternotes=getClinicalencounternotes;


	function setClinicalencounternotes(v){
		this.Clinicalencounternotes=v;
	}
	this.setClinicalencounternotes=setClinicalencounternotes;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="karnofskyPerformanceStatus"){
				return this.Karnofskyperformancestatus ;
			} else 
			if(xmlPath=="rpaClass"){
				return this.Rpaclass ;
			} else 
			if(xmlPath=="newDeficits"){
				return this.Newdeficits ;
			} else 
			if(xmlPath.startsWith("newDeficits")){
				xmlPath=xmlPath.substring(11);
				if(xmlPath=="")return this.Newdeficits ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Newdeficits!=undefined)return this.Newdeficits.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="clinicalDeterioration"){
				return this.Clinicaldeterioration ;
			} else 
			if(xmlPath=="steroids"){
				return this.Steroids ;
			} else 
			if(xmlPath=="steroidDose"){
				return this.Steroiddose ;
			} else 
			if(xmlPath=="steroidDoseIncreasing"){
				return this.Steroiddoseincreasing ;
			} else 
			if(xmlPath=="progression"){
				return this.Progression ;
			} else 
			if(xmlPath=="pseudoProgression"){
				return this.Pseudoprogression ;
			} else 
			if(xmlPath=="response"){
				return this.Response ;
			} else 
			if(xmlPath=="changeInTreatment"){
				return this.Changeintreatment ;
			} else 
			if(xmlPath=="changeInTreatmentTo"){
				return this.Changeintreatmentto ;
			} else 
			if(xmlPath=="clinicalEncounterNotes"){
				return this.Clinicalencounternotes ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="karnofskyPerformanceStatus"){
				this.Karnofskyperformancestatus=value;
			} else 
			if(xmlPath=="rpaClass"){
				this.Rpaclass=value;
			} else 
			if(xmlPath=="newDeficits"){
				this.Newdeficits=value;
			} else 
			if(xmlPath.startsWith("newDeficits")){
				xmlPath=xmlPath.substring(11);
				if(xmlPath=="")return this.Newdeficits ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Newdeficits!=undefined){
					this.Newdeficits.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Newdeficits= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Newdeficits= instanciateObject("sf:deficits");//omUtils.js
						}
						if(options && options.where)this.Newdeficits.setProperty(options.where.field,options.where.value);
						this.Newdeficits.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="clinicalDeterioration"){
				this.Clinicaldeterioration=value;
			} else 
			if(xmlPath=="steroids"){
				this.Steroids=value;
			} else 
			if(xmlPath=="steroidDose"){
				this.Steroiddose=value;
			} else 
			if(xmlPath=="steroidDoseIncreasing"){
				this.Steroiddoseincreasing=value;
			} else 
			if(xmlPath=="progression"){
				this.Progression=value;
			} else 
			if(xmlPath=="pseudoProgression"){
				this.Pseudoprogression=value;
			} else 
			if(xmlPath=="response"){
				this.Response=value;
			} else 
			if(xmlPath=="changeInTreatment"){
				this.Changeintreatment=value;
			} else 
			if(xmlPath=="changeInTreatmentTo"){
				this.Changeintreatmentto=value;
			} else 
			if(xmlPath=="clinicalEncounterNotes"){
				this.Clinicalencounternotes=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="newDeficits"){
			this.setNewdeficits(v);
		}
		else{
			this.extension.setReferenceField(xmlPath,v);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="newDeficits"){
			return "http://nrg.wustl.edu/sf:deficits";
		}
		else{
			return this.extension.getReferenceFieldName(xmlPath);
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="karnofskyPerformanceStatus"){
			return "field_data";
		}else if (xmlPath=="rpaClass"){
			return "field_data";
		}else if (xmlPath=="newDeficits"){
			return "field_single_reference";
		}else if (xmlPath=="clinicalDeterioration"){
			return "field_data";
		}else if (xmlPath=="steroids"){
			return "field_data";
		}else if (xmlPath=="steroidDose"){
			return "field_data";
		}else if (xmlPath=="steroidDoseIncreasing"){
			return "field_data";
		}else if (xmlPath=="progression"){
			return "field_data";
		}else if (xmlPath=="pseudoProgression"){
			return "field_data";
		}else if (xmlPath=="response"){
			return "field_data";
		}else if (xmlPath=="changeInTreatment"){
			return "field_data";
		}else if (xmlPath=="changeInTreatmentTo"){
			return "field_data";
		}else if (xmlPath=="clinicalEncounterNotes"){
			return "field_LONG_DATA";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<condr:ClinicalEncounter";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</condr:ClinicalEncounter>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Karnofskyperformancestatus!=null){
			xmlTxt+="\n<condr:karnofskyPerformanceStatus";
			xmlTxt+=">";
			xmlTxt+=this.Karnofskyperformancestatus;
			xmlTxt+="</condr:karnofskyPerformanceStatus>";
		}
		if (this.Rpaclass!=null){
			xmlTxt+="\n<condr:rpaClass";
			xmlTxt+=">";
			xmlTxt+=this.Rpaclass.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:rpaClass>";
		}
		if (this.Newdeficits!=null){
			xmlTxt+="\n<condr:newDeficits";
			xmlTxt+=this.Newdeficits.getXMLAtts();
			if(this.Newdeficits.xsiType!="sf:deficits"){
				xmlTxt+=" xsi:type=\"" + this.Newdeficits.xsiType + "\"";
			}
			if (this.Newdeficits.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Newdeficits.getXMLBody(preventComments);
				xmlTxt+="</condr:newDeficits>";
			}else {xmlTxt+="/>";}
		}
		//NOT REQUIRED

		if (this.Clinicaldeterioration!=null){
			xmlTxt+="\n<condr:clinicalDeterioration";
			xmlTxt+=">";
			xmlTxt+=this.Clinicaldeterioration;
			xmlTxt+="</condr:clinicalDeterioration>";
		}
		if (this.Steroids!=null){
			xmlTxt+="\n<condr:steroids";
			xmlTxt+=">";
			xmlTxt+=this.Steroids;
			xmlTxt+="</condr:steroids>";
		}
		if (this.Steroiddose!=null){
			xmlTxt+="\n<condr:steroidDose";
			xmlTxt+=">";
			xmlTxt+=this.Steroiddose.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:steroidDose>";
		}
		if (this.Steroiddoseincreasing!=null){
			xmlTxt+="\n<condr:steroidDoseIncreasing";
			xmlTxt+=">";
			xmlTxt+=this.Steroiddoseincreasing;
			xmlTxt+="</condr:steroidDoseIncreasing>";
		}
		if (this.Progression!=null){
			xmlTxt+="\n<condr:progression";
			xmlTxt+=">";
			xmlTxt+=this.Progression;
			xmlTxt+="</condr:progression>";
		}
		if (this.Pseudoprogression!=null){
			xmlTxt+="\n<condr:pseudoProgression";
			xmlTxt+=">";
			xmlTxt+=this.Pseudoprogression;
			xmlTxt+="</condr:pseudoProgression>";
		}
		if (this.Response!=null){
			xmlTxt+="\n<condr:response";
			xmlTxt+=">";
			xmlTxt+=this.Response.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:response>";
		}
		if (this.Changeintreatment!=null){
			xmlTxt+="\n<condr:changeInTreatment";
			xmlTxt+=">";
			xmlTxt+=this.Changeintreatment;
			xmlTxt+="</condr:changeInTreatment>";
		}
		if (this.Changeintreatmentto!=null){
			xmlTxt+="\n<condr:changeInTreatmentTo";
			xmlTxt+=">";
			xmlTxt+=this.Changeintreatmentto.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:changeInTreatmentTo>";
		}
		if (this.Clinicalencounternotes!=null){
			xmlTxt+="\n<condr:clinicalEncounterNotes";
			xmlTxt+=">";
			xmlTxt+=this.Clinicalencounternotes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:clinicalEncounterNotes>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Karnofskyperformancestatus!=null) return true;
		if (this.Rpaclass!=null) return true;
		if (this.Newdeficits!=null){
			if (this.Newdeficits.hasXMLBodyContent()) return true;
		}
		//NOT REQUIRED

		if (this.Clinicaldeterioration!=null) return true;
		if (this.Steroids!=null) return true;
		if (this.Steroiddose!=null) return true;
		if (this.Steroiddoseincreasing!=null) return true;
		if (this.Progression!=null) return true;
		if (this.Pseudoprogression!=null) return true;
		if (this.Response!=null) return true;
		if (this.Changeintreatment!=null) return true;
		if (this.Changeintreatmentto!=null) return true;
		if (this.Clinicalencounternotes!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
