/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:04 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function uds_b5behavasData(){
this.xsiType="uds:b5behavasData";

	this.getSchemaElementName=function(){
		return "b5behavasData";
	}

	this.getFullSchemaElementName=function(){
		return "uds:b5behavasData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Initials=null;


	function getInitials() {
		return this.Initials;
	}
	this.getInitials=getInitials;


	function setInitials(v){
		this.Initials=v;
	}
	this.setInitials=setInitials;

	this.Npiqinf=null;


	function getNpiqinf() {
		return this.Npiqinf;
	}
	this.getNpiqinf=getNpiqinf;


	function setNpiqinf(v){
		this.Npiqinf=v;
	}
	this.setNpiqinf=setNpiqinf;

	this.Npiqinfx=null;


	function getNpiqinfx() {
		return this.Npiqinfx;
	}
	this.getNpiqinfx=getNpiqinfx;


	function setNpiqinfx(v){
		this.Npiqinfx=v;
	}
	this.setNpiqinfx=setNpiqinfx;

	this.Del=null;


	function getDel() {
		return this.Del;
	}
	this.getDel=getDel;


	function setDel(v){
		this.Del=v;
	}
	this.setDel=setDel;

	this.Delsev=null;


	function getDelsev() {
		return this.Delsev;
	}
	this.getDelsev=getDelsev;


	function setDelsev(v){
		this.Delsev=v;
	}
	this.setDelsev=setDelsev;

	this.Hall=null;


	function getHall() {
		return this.Hall;
	}
	this.getHall=getHall;


	function setHall(v){
		this.Hall=v;
	}
	this.setHall=setHall;

	this.Hallsev=null;


	function getHallsev() {
		return this.Hallsev;
	}
	this.getHallsev=getHallsev;


	function setHallsev(v){
		this.Hallsev=v;
	}
	this.setHallsev=setHallsev;

	this.Agit=null;


	function getAgit() {
		return this.Agit;
	}
	this.getAgit=getAgit;


	function setAgit(v){
		this.Agit=v;
	}
	this.setAgit=setAgit;

	this.Agitsev=null;


	function getAgitsev() {
		return this.Agitsev;
	}
	this.getAgitsev=getAgitsev;


	function setAgitsev(v){
		this.Agitsev=v;
	}
	this.setAgitsev=setAgitsev;

	this.Depd=null;


	function getDepd() {
		return this.Depd;
	}
	this.getDepd=getDepd;


	function setDepd(v){
		this.Depd=v;
	}
	this.setDepd=setDepd;

	this.Depdsev=null;


	function getDepdsev() {
		return this.Depdsev;
	}
	this.getDepdsev=getDepdsev;


	function setDepdsev(v){
		this.Depdsev=v;
	}
	this.setDepdsev=setDepdsev;

	this.Anx=null;


	function getAnx() {
		return this.Anx;
	}
	this.getAnx=getAnx;


	function setAnx(v){
		this.Anx=v;
	}
	this.setAnx=setAnx;

	this.Anxsev=null;


	function getAnxsev() {
		return this.Anxsev;
	}
	this.getAnxsev=getAnxsev;


	function setAnxsev(v){
		this.Anxsev=v;
	}
	this.setAnxsev=setAnxsev;

	this.Elat=null;


	function getElat() {
		return this.Elat;
	}
	this.getElat=getElat;


	function setElat(v){
		this.Elat=v;
	}
	this.setElat=setElat;

	this.Elatsev=null;


	function getElatsev() {
		return this.Elatsev;
	}
	this.getElatsev=getElatsev;


	function setElatsev(v){
		this.Elatsev=v;
	}
	this.setElatsev=setElatsev;

	this.Apa=null;


	function getApa() {
		return this.Apa;
	}
	this.getApa=getApa;


	function setApa(v){
		this.Apa=v;
	}
	this.setApa=setApa;

	this.Apasev=null;


	function getApasev() {
		return this.Apasev;
	}
	this.getApasev=getApasev;


	function setApasev(v){
		this.Apasev=v;
	}
	this.setApasev=setApasev;

	this.Disn=null;


	function getDisn() {
		return this.Disn;
	}
	this.getDisn=getDisn;


	function setDisn(v){
		this.Disn=v;
	}
	this.setDisn=setDisn;

	this.Disnsev=null;


	function getDisnsev() {
		return this.Disnsev;
	}
	this.getDisnsev=getDisnsev;


	function setDisnsev(v){
		this.Disnsev=v;
	}
	this.setDisnsev=setDisnsev;

	this.Irr=null;


	function getIrr() {
		return this.Irr;
	}
	this.getIrr=getIrr;


	function setIrr(v){
		this.Irr=v;
	}
	this.setIrr=setIrr;

	this.Irrsev=null;


	function getIrrsev() {
		return this.Irrsev;
	}
	this.getIrrsev=getIrrsev;


	function setIrrsev(v){
		this.Irrsev=v;
	}
	this.setIrrsev=setIrrsev;

	this.Mot=null;


	function getMot() {
		return this.Mot;
	}
	this.getMot=getMot;


	function setMot(v){
		this.Mot=v;
	}
	this.setMot=setMot;

	this.Motsev=null;


	function getMotsev() {
		return this.Motsev;
	}
	this.getMotsev=getMotsev;


	function setMotsev(v){
		this.Motsev=v;
	}
	this.setMotsev=setMotsev;

	this.Nite=null;


	function getNite() {
		return this.Nite;
	}
	this.getNite=getNite;


	function setNite(v){
		this.Nite=v;
	}
	this.setNite=setNite;

	this.Nitesev=null;


	function getNitesev() {
		return this.Nitesev;
	}
	this.getNitesev=getNitesev;


	function setNitesev(v){
		this.Nitesev=v;
	}
	this.setNitesev=setNitesev;

	this.App=null;


	function getApp() {
		return this.App;
	}
	this.getApp=getApp;


	function setApp(v){
		this.App=v;
	}
	this.setApp=setApp;

	this.Appsev=null;


	function getAppsev() {
		return this.Appsev;
	}
	this.getAppsev=getAppsev;


	function setAppsev(v){
		this.Appsev=v;
	}
	this.setAppsev=setAppsev;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="INITIALS"){
				return this.Initials ;
			} else 
			if(xmlPath=="NPIQINF"){
				return this.Npiqinf ;
			} else 
			if(xmlPath=="NPIQINFX"){
				return this.Npiqinfx ;
			} else 
			if(xmlPath=="DEL"){
				return this.Del ;
			} else 
			if(xmlPath=="DELSEV"){
				return this.Delsev ;
			} else 
			if(xmlPath=="HALL"){
				return this.Hall ;
			} else 
			if(xmlPath=="HALLSEV"){
				return this.Hallsev ;
			} else 
			if(xmlPath=="AGIT"){
				return this.Agit ;
			} else 
			if(xmlPath=="AGITSEV"){
				return this.Agitsev ;
			} else 
			if(xmlPath=="DEPD"){
				return this.Depd ;
			} else 
			if(xmlPath=="DEPDSEV"){
				return this.Depdsev ;
			} else 
			if(xmlPath=="ANX"){
				return this.Anx ;
			} else 
			if(xmlPath=="ANXSEV"){
				return this.Anxsev ;
			} else 
			if(xmlPath=="ELAT"){
				return this.Elat ;
			} else 
			if(xmlPath=="ELATSEV"){
				return this.Elatsev ;
			} else 
			if(xmlPath=="APA"){
				return this.Apa ;
			} else 
			if(xmlPath=="APASEV"){
				return this.Apasev ;
			} else 
			if(xmlPath=="DISN"){
				return this.Disn ;
			} else 
			if(xmlPath=="DISNSEV"){
				return this.Disnsev ;
			} else 
			if(xmlPath=="IRR"){
				return this.Irr ;
			} else 
			if(xmlPath=="IRRSEV"){
				return this.Irrsev ;
			} else 
			if(xmlPath=="MOT"){
				return this.Mot ;
			} else 
			if(xmlPath=="MOTSEV"){
				return this.Motsev ;
			} else 
			if(xmlPath=="NITE"){
				return this.Nite ;
			} else 
			if(xmlPath=="NITESEV"){
				return this.Nitesev ;
			} else 
			if(xmlPath=="APP"){
				return this.App ;
			} else 
			if(xmlPath=="APPSEV"){
				return this.Appsev ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="INITIALS"){
				this.Initials=value;
			} else 
			if(xmlPath=="NPIQINF"){
				this.Npiqinf=value;
			} else 
			if(xmlPath=="NPIQINFX"){
				this.Npiqinfx=value;
			} else 
			if(xmlPath=="DEL"){
				this.Del=value;
			} else 
			if(xmlPath=="DELSEV"){
				this.Delsev=value;
			} else 
			if(xmlPath=="HALL"){
				this.Hall=value;
			} else 
			if(xmlPath=="HALLSEV"){
				this.Hallsev=value;
			} else 
			if(xmlPath=="AGIT"){
				this.Agit=value;
			} else 
			if(xmlPath=="AGITSEV"){
				this.Agitsev=value;
			} else 
			if(xmlPath=="DEPD"){
				this.Depd=value;
			} else 
			if(xmlPath=="DEPDSEV"){
				this.Depdsev=value;
			} else 
			if(xmlPath=="ANX"){
				this.Anx=value;
			} else 
			if(xmlPath=="ANXSEV"){
				this.Anxsev=value;
			} else 
			if(xmlPath=="ELAT"){
				this.Elat=value;
			} else 
			if(xmlPath=="ELATSEV"){
				this.Elatsev=value;
			} else 
			if(xmlPath=="APA"){
				this.Apa=value;
			} else 
			if(xmlPath=="APASEV"){
				this.Apasev=value;
			} else 
			if(xmlPath=="DISN"){
				this.Disn=value;
			} else 
			if(xmlPath=="DISNSEV"){
				this.Disnsev=value;
			} else 
			if(xmlPath=="IRR"){
				this.Irr=value;
			} else 
			if(xmlPath=="IRRSEV"){
				this.Irrsev=value;
			} else 
			if(xmlPath=="MOT"){
				this.Mot=value;
			} else 
			if(xmlPath=="MOTSEV"){
				this.Motsev=value;
			} else 
			if(xmlPath=="NITE"){
				this.Nite=value;
			} else 
			if(xmlPath=="NITESEV"){
				this.Nitesev=value;
			} else 
			if(xmlPath=="APP"){
				this.App=value;
			} else 
			if(xmlPath=="APPSEV"){
				this.Appsev=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="INITIALS"){
			return "field_data";
		}else if (xmlPath=="NPIQINF"){
			return "field_data";
		}else if (xmlPath=="NPIQINFX"){
			return "field_data";
		}else if (xmlPath=="DEL"){
			return "field_data";
		}else if (xmlPath=="DELSEV"){
			return "field_data";
		}else if (xmlPath=="HALL"){
			return "field_data";
		}else if (xmlPath=="HALLSEV"){
			return "field_data";
		}else if (xmlPath=="AGIT"){
			return "field_data";
		}else if (xmlPath=="AGITSEV"){
			return "field_data";
		}else if (xmlPath=="DEPD"){
			return "field_data";
		}else if (xmlPath=="DEPDSEV"){
			return "field_data";
		}else if (xmlPath=="ANX"){
			return "field_data";
		}else if (xmlPath=="ANXSEV"){
			return "field_data";
		}else if (xmlPath=="ELAT"){
			return "field_data";
		}else if (xmlPath=="ELATSEV"){
			return "field_data";
		}else if (xmlPath=="APA"){
			return "field_data";
		}else if (xmlPath=="APASEV"){
			return "field_data";
		}else if (xmlPath=="DISN"){
			return "field_data";
		}else if (xmlPath=="DISNSEV"){
			return "field_data";
		}else if (xmlPath=="IRR"){
			return "field_data";
		}else if (xmlPath=="IRRSEV"){
			return "field_data";
		}else if (xmlPath=="MOT"){
			return "field_data";
		}else if (xmlPath=="MOTSEV"){
			return "field_data";
		}else if (xmlPath=="NITE"){
			return "field_data";
		}else if (xmlPath=="NITESEV"){
			return "field_data";
		}else if (xmlPath=="APP"){
			return "field_data";
		}else if (xmlPath=="APPSEV"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<uds:B5BEHAVAS";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</uds:B5BEHAVAS>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Initials!=null){
			xmlTxt+="\n<uds:INITIALS";
			xmlTxt+=">";
			xmlTxt+=this.Initials.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:INITIALS>";
		}
		if (this.Npiqinf!=null){
			xmlTxt+="\n<uds:NPIQINF";
			xmlTxt+=">";
			xmlTxt+=this.Npiqinf;
			xmlTxt+="</uds:NPIQINF>";
		}
		if (this.Npiqinfx!=null){
			xmlTxt+="\n<uds:NPIQINFX";
			xmlTxt+=">";
			xmlTxt+=this.Npiqinfx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:NPIQINFX>";
		}
		if (this.Del!=null){
			xmlTxt+="\n<uds:DEL";
			xmlTxt+=">";
			xmlTxt+=this.Del;
			xmlTxt+="</uds:DEL>";
		}
		if (this.Delsev!=null){
			xmlTxt+="\n<uds:DELSEV";
			xmlTxt+=">";
			xmlTxt+=this.Delsev;
			xmlTxt+="</uds:DELSEV>";
		}
		if (this.Hall!=null){
			xmlTxt+="\n<uds:HALL";
			xmlTxt+=">";
			xmlTxt+=this.Hall;
			xmlTxt+="</uds:HALL>";
		}
		if (this.Hallsev!=null){
			xmlTxt+="\n<uds:HALLSEV";
			xmlTxt+=">";
			xmlTxt+=this.Hallsev;
			xmlTxt+="</uds:HALLSEV>";
		}
		if (this.Agit!=null){
			xmlTxt+="\n<uds:AGIT";
			xmlTxt+=">";
			xmlTxt+=this.Agit;
			xmlTxt+="</uds:AGIT>";
		}
		if (this.Agitsev!=null){
			xmlTxt+="\n<uds:AGITSEV";
			xmlTxt+=">";
			xmlTxt+=this.Agitsev;
			xmlTxt+="</uds:AGITSEV>";
		}
		if (this.Depd!=null){
			xmlTxt+="\n<uds:DEPD";
			xmlTxt+=">";
			xmlTxt+=this.Depd;
			xmlTxt+="</uds:DEPD>";
		}
		if (this.Depdsev!=null){
			xmlTxt+="\n<uds:DEPDSEV";
			xmlTxt+=">";
			xmlTxt+=this.Depdsev;
			xmlTxt+="</uds:DEPDSEV>";
		}
		if (this.Anx!=null){
			xmlTxt+="\n<uds:ANX";
			xmlTxt+=">";
			xmlTxt+=this.Anx;
			xmlTxt+="</uds:ANX>";
		}
		if (this.Anxsev!=null){
			xmlTxt+="\n<uds:ANXSEV";
			xmlTxt+=">";
			xmlTxt+=this.Anxsev;
			xmlTxt+="</uds:ANXSEV>";
		}
		if (this.Elat!=null){
			xmlTxt+="\n<uds:ELAT";
			xmlTxt+=">";
			xmlTxt+=this.Elat;
			xmlTxt+="</uds:ELAT>";
		}
		if (this.Elatsev!=null){
			xmlTxt+="\n<uds:ELATSEV";
			xmlTxt+=">";
			xmlTxt+=this.Elatsev;
			xmlTxt+="</uds:ELATSEV>";
		}
		if (this.Apa!=null){
			xmlTxt+="\n<uds:APA";
			xmlTxt+=">";
			xmlTxt+=this.Apa;
			xmlTxt+="</uds:APA>";
		}
		if (this.Apasev!=null){
			xmlTxt+="\n<uds:APASEV";
			xmlTxt+=">";
			xmlTxt+=this.Apasev;
			xmlTxt+="</uds:APASEV>";
		}
		if (this.Disn!=null){
			xmlTxt+="\n<uds:DISN";
			xmlTxt+=">";
			xmlTxt+=this.Disn;
			xmlTxt+="</uds:DISN>";
		}
		if (this.Disnsev!=null){
			xmlTxt+="\n<uds:DISNSEV";
			xmlTxt+=">";
			xmlTxt+=this.Disnsev;
			xmlTxt+="</uds:DISNSEV>";
		}
		if (this.Irr!=null){
			xmlTxt+="\n<uds:IRR";
			xmlTxt+=">";
			xmlTxt+=this.Irr;
			xmlTxt+="</uds:IRR>";
		}
		if (this.Irrsev!=null){
			xmlTxt+="\n<uds:IRRSEV";
			xmlTxt+=">";
			xmlTxt+=this.Irrsev;
			xmlTxt+="</uds:IRRSEV>";
		}
		if (this.Mot!=null){
			xmlTxt+="\n<uds:MOT";
			xmlTxt+=">";
			xmlTxt+=this.Mot;
			xmlTxt+="</uds:MOT>";
		}
		if (this.Motsev!=null){
			xmlTxt+="\n<uds:MOTSEV";
			xmlTxt+=">";
			xmlTxt+=this.Motsev;
			xmlTxt+="</uds:MOTSEV>";
		}
		if (this.Nite!=null){
			xmlTxt+="\n<uds:NITE";
			xmlTxt+=">";
			xmlTxt+=this.Nite;
			xmlTxt+="</uds:NITE>";
		}
		if (this.Nitesev!=null){
			xmlTxt+="\n<uds:NITESEV";
			xmlTxt+=">";
			xmlTxt+=this.Nitesev;
			xmlTxt+="</uds:NITESEV>";
		}
		if (this.App!=null){
			xmlTxt+="\n<uds:APP";
			xmlTxt+=">";
			xmlTxt+=this.App;
			xmlTxt+="</uds:APP>";
		}
		if (this.Appsev!=null){
			xmlTxt+="\n<uds:APPSEV";
			xmlTxt+=">";
			xmlTxt+=this.Appsev;
			xmlTxt+="</uds:APPSEV>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Initials!=null) return true;
		if (this.Npiqinf!=null) return true;
		if (this.Npiqinfx!=null) return true;
		if (this.Del!=null) return true;
		if (this.Delsev!=null) return true;
		if (this.Hall!=null) return true;
		if (this.Hallsev!=null) return true;
		if (this.Agit!=null) return true;
		if (this.Agitsev!=null) return true;
		if (this.Depd!=null) return true;
		if (this.Depdsev!=null) return true;
		if (this.Anx!=null) return true;
		if (this.Anxsev!=null) return true;
		if (this.Elat!=null) return true;
		if (this.Elatsev!=null) return true;
		if (this.Apa!=null) return true;
		if (this.Apasev!=null) return true;
		if (this.Disn!=null) return true;
		if (this.Disnsev!=null) return true;
		if (this.Irr!=null) return true;
		if (this.Irrsev!=null) return true;
		if (this.Mot!=null) return true;
		if (this.Motsev!=null) return true;
		if (this.Nite!=null) return true;
		if (this.Nitesev!=null) return true;
		if (this.App!=null) return true;
		if (this.Appsev!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
