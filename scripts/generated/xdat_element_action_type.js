/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:04 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function xdat_element_action_type(){
this.xsiType="xdat:element_action_type";

	this.getSchemaElementName=function(){
		return "element_action_type";
	}

	this.getFullSchemaElementName=function(){
		return "xdat:element_action_type";
	}

	this.ElementActionName=null;


	function getElementActionName() {
		return this.ElementActionName;
	}
	this.getElementActionName=getElementActionName;


	function setElementActionName(v){
		this.ElementActionName=v;
	}
	this.setElementActionName=setElementActionName;

	this.DisplayName=null;


	function getDisplayName() {
		return this.DisplayName;
	}
	this.getDisplayName=getDisplayName;


	function setDisplayName(v){
		this.DisplayName=v;
	}
	this.setDisplayName=setDisplayName;

	this.Sequence=null;


	function getSequence() {
		return this.Sequence;
	}
	this.getSequence=getSequence;


	function setSequence(v){
		this.Sequence=v;
	}
	this.setSequence=setSequence;

	this.Image=null;


	function getImage() {
		return this.Image;
	}
	this.getImage=getImage;


	function setImage(v){
		this.Image=v;
	}
	this.setImage=setImage;

	this.Popup=null;


	function getPopup() {
		return this.Popup;
	}
	this.getPopup=getPopup;


	function setPopup(v){
		this.Popup=v;
	}
	this.setPopup=setPopup;

	this.Secureaccess=null;


	function getSecureaccess() {
		return this.Secureaccess;
	}
	this.getSecureaccess=getSecureaccess;


	function setSecureaccess(v){
		this.Secureaccess=v;
	}
	this.setSecureaccess=setSecureaccess;

	this.Securefeature=null;


	function getSecurefeature() {
		return this.Securefeature;
	}
	this.getSecurefeature=getSecurefeature;


	function setSecurefeature(v){
		this.Securefeature=v;
	}
	this.setSecurefeature=setSecurefeature;

	this.Parameterstring=null;


	function getParameterstring() {
		return this.Parameterstring;
	}
	this.getParameterstring=getParameterstring;


	function setParameterstring(v){
		this.Parameterstring=v;
	}
	this.setParameterstring=setParameterstring;

	this.Grouping=null;


	function getGrouping() {
		return this.Grouping;
	}
	this.getGrouping=getGrouping;


	function setGrouping(v){
		this.Grouping=v;
	}
	this.setGrouping=setGrouping;

	this.XdatElementActionTypeId=null;


	function getXdatElementActionTypeId() {
		return this.XdatElementActionTypeId;
	}
	this.getXdatElementActionTypeId=getXdatElementActionTypeId;


	function setXdatElementActionTypeId(v){
		this.XdatElementActionTypeId=v;
	}
	this.setXdatElementActionTypeId=setXdatElementActionTypeId;

	this.element_actions_element_action__element_name_fk=null;


	this.getelement_actions_element_action__element_name=function() {
		return this.element_actions_element_action__element_name_fk;
	}


	this.setelement_actions_element_action__element_name=function(v){
		this.element_actions_element_action__element_name_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="element_action_name"){
				return this.ElementActionName ;
			} else 
			if(xmlPath=="display_name"){
				return this.DisplayName ;
			} else 
			if(xmlPath=="sequence"){
				return this.Sequence ;
			} else 
			if(xmlPath=="image"){
				return this.Image ;
			} else 
			if(xmlPath=="popup"){
				return this.Popup ;
			} else 
			if(xmlPath=="secureAccess"){
				return this.Secureaccess ;
			} else 
			if(xmlPath=="secureFeature"){
				return this.Securefeature ;
			} else 
			if(xmlPath=="parameterString"){
				return this.Parameterstring ;
			} else 
			if(xmlPath=="grouping"){
				return this.Grouping ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="xdat_element_action_type_id"){
				return this.XdatElementActionTypeId ;
			} else 
			if(xmlPath=="element_actions_element_action__element_name"){
				return this.element_actions_element_action__element_name_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="element_action_name"){
				this.ElementActionName=value;
			} else 
			if(xmlPath=="display_name"){
				this.DisplayName=value;
			} else 
			if(xmlPath=="sequence"){
				this.Sequence=value;
			} else 
			if(xmlPath=="image"){
				this.Image=value;
			} else 
			if(xmlPath=="popup"){
				this.Popup=value;
			} else 
			if(xmlPath=="secureAccess"){
				this.Secureaccess=value;
			} else 
			if(xmlPath=="secureFeature"){
				this.Securefeature=value;
			} else 
			if(xmlPath=="parameterString"){
				this.Parameterstring=value;
			} else 
			if(xmlPath=="grouping"){
				this.Grouping=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="xdat_element_action_type_id"){
				this.XdatElementActionTypeId=value;
			} else 
			if(xmlPath=="element_actions_element_action__element_name"){
				this.element_actions_element_action__element_name_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="element_action_name"){
			return "field_data";
		}else if (xmlPath=="display_name"){
			return "field_data";
		}else if (xmlPath=="sequence"){
			return "field_data";
		}else if (xmlPath=="image"){
			return "field_data";
		}else if (xmlPath=="popup"){
			return "field_data";
		}else if (xmlPath=="secureAccess"){
			return "field_data";
		}else if (xmlPath=="secureFeature"){
			return "field_data";
		}else if (xmlPath=="parameterString"){
			return "field_data";
		}else if (xmlPath=="grouping"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<xdat:element_action_type";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</xdat:element_action_type>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.XdatElementActionTypeId!=null){
				if(hiddenCount++>0)str+=",";
				str+="xdat_element_action_type_id=\"" + this.XdatElementActionTypeId + "\"";
			}
			if(this.element_actions_element_action__element_name_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="element_actions_element_action__element_name=\"" + this.element_actions_element_action__element_name_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.ElementActionName!=null)
			attTxt+=" element_action_name=\"" +this.ElementActionName +"\"";
		else attTxt+=" element_action_name=\"\"";//REQUIRED FIELD

		if (this.DisplayName!=null)
			attTxt+=" display_name=\"" +this.DisplayName +"\"";
		else attTxt+=" display_name=\"\"";//REQUIRED FIELD

		if (this.Sequence!=null)
			attTxt+=" sequence=\"" +this.Sequence +"\"";
		//NOT REQUIRED FIELD

		if (this.Image!=null)
			attTxt+=" image=\"" +this.Image +"\"";
		//NOT REQUIRED FIELD

		if (this.Popup!=null)
			attTxt+=" popup=\"" +this.Popup +"\"";
		//NOT REQUIRED FIELD

		if (this.Secureaccess!=null)
			attTxt+=" secureAccess=\"" +this.Secureaccess +"\"";
		//NOT REQUIRED FIELD

		if (this.Securefeature!=null)
			attTxt+=" secureFeature=\"" +this.Securefeature +"\"";
		//NOT REQUIRED FIELD

		if (this.Parameterstring!=null)
			attTxt+=" parameterString=\"" +this.Parameterstring +"\"";
		//NOT REQUIRED FIELD

		if (this.Grouping!=null)
			attTxt+=" grouping=\"" +this.Grouping +"\"";
		//NOT REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.XdatElementActionTypeId!=null) return true;
			if (this.element_actions_element_action__element_name_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if(this.hasXMLComments())return true;
		return false;
	}
}
