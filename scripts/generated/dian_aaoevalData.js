/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function dian_aaoevalData(){
this.xsiType="dian:aaoevalData";

	this.getSchemaElementName=function(){
		return "aaoevalData";
	}

	this.getFullSchemaElementName=function(){
		return "dian:aaoevalData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Evaldate=null;


	function getEvaldate() {
		return this.Evaldate;
	}
	this.getEvaldate=getEvaldate;


	function setEvaldate(v){
		this.Evaldate=v;
	}
	this.setEvaldate=setEvaldate;

	this.Informid=null;


	function getInformid() {
		return this.Informid;
	}
	this.getInformid=getInformid;


	function setInformid(v){
		this.Informid=v;
	}
	this.setInformid=setInformid;

	this.Proxy=null;


	function getProxy() {
		return this.Proxy;
	}
	this.getProxy=getProxy;


	function setProxy(v){
		this.Proxy=v;
	}
	this.setProxy=setProxy;

	this.Parentid=null;


	function getParentid() {
		return this.Parentid;
	}
	this.getParentid=getParentid;


	function setParentid(v){
		this.Parentid=v;
	}
	this.setParentid=setParentid;

	this.Normalyr=null;


	function getNormalyr() {
		return this.Normalyr;
	}
	this.getNormalyr=getNormalyr;


	function setNormalyr(v){
		this.Normalyr=v;
	}
	this.setNormalyr=setNormalyr;

	this.Symptomyr=null;


	function getSymptomyr() {
		return this.Symptomyr;
	}
	this.getSymptomyr=getSymptomyr;


	function setSymptomyr(v){
		this.Symptomyr=v;
	}
	this.setSymptomyr=setSymptomyr;

	this.Memcogyr=null;


	function getMemcogyr() {
		return this.Memcogyr;
	}
	this.getMemcogyr=getMemcogyr;


	function setMemcogyr(v){
		this.Memcogyr=v;
	}
	this.setMemcogyr=setMemcogyr;

	this.Behavyr=null;


	function getBehavyr() {
		return this.Behavyr;
	}
	this.getBehavyr=getBehavyr;


	function setBehavyr(v){
		this.Behavyr=v;
	}
	this.setBehavyr=setBehavyr;

	this.Motoryr=null;


	function getMotoryr() {
		return this.Motoryr;
	}
	this.getMotoryr=getMotoryr;


	function setMotoryr(v){
		this.Motoryr=v;
	}
	this.setMotoryr=setMotoryr;

	this.Alzyr=null;


	function getAlzyr() {
		return this.Alzyr;
	}
	this.getAlzyr=getAlzyr;


	function setAlzyr(v){
		this.Alzyr=v;
	}
	this.setAlzyr=setAlzyr;

	this.Deathyr=null;


	function getDeathyr() {
		return this.Deathyr;
	}
	this.getDeathyr=getDeathyr;


	function setDeathyr(v){
		this.Deathyr=v;
	}
	this.setDeathyr=setDeathyr;

	this.Deathcs=null;


	function getDeathcs() {
		return this.Deathcs;
	}
	this.getDeathcs=getDeathcs;


	function setDeathcs(v){
		this.Deathcs=v;
	}
	this.setDeathcs=setDeathcs;

	this.Autopsy=null;


	function getAutopsy() {
		return this.Autopsy;
	}
	this.getAutopsy=getAutopsy;


	function setAutopsy(v){
		this.Autopsy=v;
	}
	this.setAutopsy=setAutopsy;

	this.Deathcdr=null;


	function getDeathcdr() {
		return this.Deathcdr;
	}
	this.getDeathcdr=getDeathcdr;


	function setDeathcdr(v){
		this.Deathcdr=v;
	}
	this.setDeathcdr=setDeathcdr;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="EVALDATE"){
				return this.Evaldate ;
			} else 
			if(xmlPath=="INFORMID"){
				return this.Informid ;
			} else 
			if(xmlPath=="PROXY"){
				return this.Proxy ;
			} else 
			if(xmlPath=="PARENTID"){
				return this.Parentid ;
			} else 
			if(xmlPath=="NORMALYR"){
				return this.Normalyr ;
			} else 
			if(xmlPath=="SYMPTOMYR"){
				return this.Symptomyr ;
			} else 
			if(xmlPath=="MEMCOGYR"){
				return this.Memcogyr ;
			} else 
			if(xmlPath=="BEHAVYR"){
				return this.Behavyr ;
			} else 
			if(xmlPath=="MOTORYR"){
				return this.Motoryr ;
			} else 
			if(xmlPath=="ALZYR"){
				return this.Alzyr ;
			} else 
			if(xmlPath=="DEATHYR"){
				return this.Deathyr ;
			} else 
			if(xmlPath=="DEATHCS"){
				return this.Deathcs ;
			} else 
			if(xmlPath=="AUTOPSY"){
				return this.Autopsy ;
			} else 
			if(xmlPath=="DEATHCDR"){
				return this.Deathcdr ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="EVALDATE"){
				this.Evaldate=value;
			} else 
			if(xmlPath=="INFORMID"){
				this.Informid=value;
			} else 
			if(xmlPath=="PROXY"){
				this.Proxy=value;
			} else 
			if(xmlPath=="PARENTID"){
				this.Parentid=value;
			} else 
			if(xmlPath=="NORMALYR"){
				this.Normalyr=value;
			} else 
			if(xmlPath=="SYMPTOMYR"){
				this.Symptomyr=value;
			} else 
			if(xmlPath=="MEMCOGYR"){
				this.Memcogyr=value;
			} else 
			if(xmlPath=="BEHAVYR"){
				this.Behavyr=value;
			} else 
			if(xmlPath=="MOTORYR"){
				this.Motoryr=value;
			} else 
			if(xmlPath=="ALZYR"){
				this.Alzyr=value;
			} else 
			if(xmlPath=="DEATHYR"){
				this.Deathyr=value;
			} else 
			if(xmlPath=="DEATHCS"){
				this.Deathcs=value;
			} else 
			if(xmlPath=="AUTOPSY"){
				this.Autopsy=value;
			} else 
			if(xmlPath=="DEATHCDR"){
				this.Deathcdr=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="EVALDATE"){
			return "field_data";
		}else if (xmlPath=="INFORMID"){
			return "field_data";
		}else if (xmlPath=="PROXY"){
			return "field_data";
		}else if (xmlPath=="PARENTID"){
			return "field_data";
		}else if (xmlPath=="NORMALYR"){
			return "field_data";
		}else if (xmlPath=="SYMPTOMYR"){
			return "field_data";
		}else if (xmlPath=="MEMCOGYR"){
			return "field_data";
		}else if (xmlPath=="BEHAVYR"){
			return "field_data";
		}else if (xmlPath=="MOTORYR"){
			return "field_data";
		}else if (xmlPath=="ALZYR"){
			return "field_data";
		}else if (xmlPath=="DEATHYR"){
			return "field_data";
		}else if (xmlPath=="DEATHCS"){
			return "field_LONG_DATA";
		}else if (xmlPath=="AUTOPSY"){
			return "field_data";
		}else if (xmlPath=="DEATHCDR"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<dian:AAOEVAL";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</dian:AAOEVAL>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Evaldate!=null){
			xmlTxt+="\n<dian:EVALDATE";
			xmlTxt+=">";
			xmlTxt+=this.Evaldate;
			xmlTxt+="</dian:EVALDATE>";
		}
		if (this.Informid!=null){
			xmlTxt+="\n<dian:INFORMID";
			xmlTxt+=">";
			xmlTxt+=this.Informid.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:INFORMID>";
		}
		if (this.Proxy!=null){
			xmlTxt+="\n<dian:PROXY";
			xmlTxt+=">";
			xmlTxt+=this.Proxy.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:PROXY>";
		}
		if (this.Parentid!=null){
			xmlTxt+="\n<dian:PARENTID";
			xmlTxt+=">";
			xmlTxt+=this.Parentid.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:PARENTID>";
		}
		if (this.Normalyr!=null){
			xmlTxt+="\n<dian:NORMALYR";
			xmlTxt+=">";
			xmlTxt+=this.Normalyr;
			xmlTxt+="</dian:NORMALYR>";
		}
		if (this.Symptomyr!=null){
			xmlTxt+="\n<dian:SYMPTOMYR";
			xmlTxt+=">";
			xmlTxt+=this.Symptomyr;
			xmlTxt+="</dian:SYMPTOMYR>";
		}
		if (this.Memcogyr!=null){
			xmlTxt+="\n<dian:MEMCOGYR";
			xmlTxt+=">";
			xmlTxt+=this.Memcogyr;
			xmlTxt+="</dian:MEMCOGYR>";
		}
		if (this.Behavyr!=null){
			xmlTxt+="\n<dian:BEHAVYR";
			xmlTxt+=">";
			xmlTxt+=this.Behavyr;
			xmlTxt+="</dian:BEHAVYR>";
		}
		if (this.Motoryr!=null){
			xmlTxt+="\n<dian:MOTORYR";
			xmlTxt+=">";
			xmlTxt+=this.Motoryr;
			xmlTxt+="</dian:MOTORYR>";
		}
		if (this.Alzyr!=null){
			xmlTxt+="\n<dian:ALZYR";
			xmlTxt+=">";
			xmlTxt+=this.Alzyr;
			xmlTxt+="</dian:ALZYR>";
		}
		if (this.Deathyr!=null){
			xmlTxt+="\n<dian:DEATHYR";
			xmlTxt+=">";
			xmlTxt+=this.Deathyr;
			xmlTxt+="</dian:DEATHYR>";
		}
		if (this.Deathcs!=null){
			xmlTxt+="\n<dian:DEATHCS";
			xmlTxt+=">";
			xmlTxt+=this.Deathcs.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:DEATHCS>";
		}
		else{
			xmlTxt+="\n<dian:DEATHCS";
			xmlTxt+="/>";
		}

		if (this.Autopsy!=null){
			xmlTxt+="\n<dian:AUTOPSY";
			xmlTxt+=">";
			xmlTxt+=this.Autopsy;
			xmlTxt+="</dian:AUTOPSY>";
		}
		if (this.Deathcdr!=null){
			xmlTxt+="\n<dian:DEATHCDR";
			xmlTxt+=">";
			xmlTxt+=this.Deathcdr;
			xmlTxt+="</dian:DEATHCDR>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Evaldate!=null) return true;
		if (this.Informid!=null) return true;
		if (this.Proxy!=null) return true;
		if (this.Parentid!=null) return true;
		if (this.Normalyr!=null) return true;
		if (this.Symptomyr!=null) return true;
		if (this.Memcogyr!=null) return true;
		if (this.Behavyr!=null) return true;
		if (this.Motoryr!=null) return true;
		if (this.Alzyr!=null) return true;
		if (this.Deathyr!=null) return true;
		if (this.Deathcs!=null) return true;
		return true;//REQUIRED DEATHCS
	}
}
