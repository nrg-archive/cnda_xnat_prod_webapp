/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function opti_oisProcData(){
this.xsiType="opti:oisProcData";

	this.getSchemaElementName=function(){
		return "oisProcData";
	}

	this.getFullSchemaElementName=function(){
		return "opti:oisProcData";
	}
this.extension=dynamicJSLoad('xnat_imageAssessorData','generated/xnat_imageAssessorData.js');

	this.Inputparams_rsSamplingrate=null;


	function getInputparams_rsSamplingrate() {
		return this.Inputparams_rsSamplingrate;
	}
	this.getInputparams_rsSamplingrate=getInputparams_rsSamplingrate;


	function setInputparams_rsSamplingrate(v){
		this.Inputparams_rsSamplingrate=v;
	}
	this.setInputparams_rsSamplingrate=setInputparams_rsSamplingrate;

	this.Inputparams_rsLowpass=null;


	function getInputparams_rsLowpass() {
		return this.Inputparams_rsLowpass;
	}
	this.getInputparams_rsLowpass=getInputparams_rsLowpass;


	function setInputparams_rsLowpass(v){
		this.Inputparams_rsLowpass=v;
	}
	this.setInputparams_rsLowpass=setInputparams_rsLowpass;

	this.Inputparams_rsHighpass=null;


	function getInputparams_rsHighpass() {
		return this.Inputparams_rsHighpass;
	}
	this.getInputparams_rsHighpass=getInputparams_rsHighpass;


	function setInputparams_rsHighpass(v){
		this.Inputparams_rsHighpass=v;
	}
	this.setInputparams_rsHighpass=setInputparams_rsHighpass;

	this.Inputparams_stimSamplingrate=null;


	function getInputparams_stimSamplingrate() {
		return this.Inputparams_stimSamplingrate;
	}
	this.getInputparams_stimSamplingrate=getInputparams_stimSamplingrate;


	function setInputparams_stimSamplingrate(v){
		this.Inputparams_stimSamplingrate=v;
	}
	this.setInputparams_stimSamplingrate=setInputparams_stimSamplingrate;

	this.Inputparams_stimLowpass=null;


	function getInputparams_stimLowpass() {
		return this.Inputparams_stimLowpass;
	}
	this.getInputparams_stimLowpass=getInputparams_stimLowpass;


	function setInputparams_stimLowpass(v){
		this.Inputparams_stimLowpass=v;
	}
	this.setInputparams_stimLowpass=setInputparams_stimLowpass;

	this.Inputparams_stimHighpass=null;


	function getInputparams_stimHighpass() {
		return this.Inputparams_stimHighpass;
	}
	this.getInputparams_stimHighpass=getInputparams_stimHighpass;


	function setInputparams_stimHighpass(v){
		this.Inputparams_stimHighpass=v;
	}
	this.setInputparams_stimHighpass=setInputparams_stimHighpass;

	this.Inputparams_stimBlocksize=null;


	function getInputparams_stimBlocksize() {
		return this.Inputparams_stimBlocksize;
	}
	this.getInputparams_stimBlocksize=getInputparams_stimBlocksize;


	function setInputparams_stimBlocksize(v){
		this.Inputparams_stimBlocksize=v;
	}
	this.setInputparams_stimBlocksize=setInputparams_stimBlocksize;

	this.Inputparams_stimBaseline=null;


	function getInputparams_stimBaseline() {
		return this.Inputparams_stimBaseline;
	}
	this.getInputparams_stimBaseline=getInputparams_stimBaseline;


	function setInputparams_stimBaseline(v){
		this.Inputparams_stimBaseline=v;
	}
	this.setInputparams_stimBaseline=setInputparams_stimBaseline;

	this.Inputparams_stimDuration=null;


	function getInputparams_stimDuration() {
		return this.Inputparams_stimDuration;
	}
	this.getInputparams_stimDuration=getInputparams_stimDuration;


	function setInputparams_stimDuration(v){
		this.Inputparams_stimDuration=v;
	}
	this.setInputparams_stimDuration=setInputparams_stimDuration;
	this.FramesHbs_framesHb =new Array();

	function getFramesHbs_framesHb() {
		return this.FramesHbs_framesHb;
	}
	this.getFramesHbs_framesHb=getFramesHbs_framesHb;


	function addFramesHbs_framesHb(v){
		this.FramesHbs_framesHb.push(v);
	}
	this.addFramesHbs_framesHb=addFramesHbs_framesHb;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="imageAssessorData"){
				return this.Imageassessordata ;
			} else 
			if(xmlPath.startsWith("imageAssessorData")){
				xmlPath=xmlPath.substring(17);
				if(xmlPath=="")return this.Imageassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Imageassessordata!=undefined)return this.Imageassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="inputParams/rs_samplingrate"){
				return this.Inputparams_rsSamplingrate ;
			} else 
			if(xmlPath=="inputParams/rs_lowpass"){
				return this.Inputparams_rsLowpass ;
			} else 
			if(xmlPath=="inputParams/rs_highpass"){
				return this.Inputparams_rsHighpass ;
			} else 
			if(xmlPath=="inputParams/stim_samplingrate"){
				return this.Inputparams_stimSamplingrate ;
			} else 
			if(xmlPath=="inputParams/stim_lowpass"){
				return this.Inputparams_stimLowpass ;
			} else 
			if(xmlPath=="inputParams/stim_highpass"){
				return this.Inputparams_stimHighpass ;
			} else 
			if(xmlPath=="inputParams/stim_blocksize"){
				return this.Inputparams_stimBlocksize ;
			} else 
			if(xmlPath=="inputParams/stim_baseline"){
				return this.Inputparams_stimBaseline ;
			} else 
			if(xmlPath=="inputParams/stim_duration"){
				return this.Inputparams_stimDuration ;
			} else 
			if(xmlPath=="frames_hbs/frames_hb"){
				return this.FramesHbs_framesHb ;
			} else 
			if(xmlPath.startsWith("frames_hbs/frames_hb")){
				xmlPath=xmlPath.substring(20);
				if(xmlPath=="")return this.FramesHbs_framesHb ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.FramesHbs_framesHb.length;whereCount++){

					var tempValue=this.FramesHbs_framesHb[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.FramesHbs_framesHb[whereCount]);

					}

				}
				}else{

				whereArray=this.FramesHbs_framesHb;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="imageAssessorData"){
				this.Imageassessordata=value;
			} else 
			if(xmlPath.startsWith("imageAssessorData")){
				xmlPath=xmlPath.substring(17);
				if(xmlPath=="")return this.Imageassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Imageassessordata!=undefined){
					this.Imageassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Imageassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Imageassessordata= instanciateObject("xnat:imageAssessorData");//omUtils.js
						}
						if(options && options.where)this.Imageassessordata.setProperty(options.where.field,options.where.value);
						this.Imageassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="inputParams/rs_samplingrate"){
				this.Inputparams_rsSamplingrate=value;
			} else 
			if(xmlPath=="inputParams/rs_lowpass"){
				this.Inputparams_rsLowpass=value;
			} else 
			if(xmlPath=="inputParams/rs_highpass"){
				this.Inputparams_rsHighpass=value;
			} else 
			if(xmlPath=="inputParams/stim_samplingrate"){
				this.Inputparams_stimSamplingrate=value;
			} else 
			if(xmlPath=="inputParams/stim_lowpass"){
				this.Inputparams_stimLowpass=value;
			} else 
			if(xmlPath=="inputParams/stim_highpass"){
				this.Inputparams_stimHighpass=value;
			} else 
			if(xmlPath=="inputParams/stim_blocksize"){
				this.Inputparams_stimBlocksize=value;
			} else 
			if(xmlPath=="inputParams/stim_baseline"){
				this.Inputparams_stimBaseline=value;
			} else 
			if(xmlPath=="inputParams/stim_duration"){
				this.Inputparams_stimDuration=value;
			} else 
			if(xmlPath=="frames_hbs/frames_hb"){
				this.FramesHbs_framesHb=value;
			} else 
			if(xmlPath.startsWith("frames_hbs/frames_hb")){
				xmlPath=xmlPath.substring(20);
				if(xmlPath=="")return this.FramesHbs_framesHb ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.FramesHbs_framesHb.length;whereCount++){

					var tempValue=this.FramesHbs_framesHb[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.FramesHbs_framesHb[whereCount]);

					}

				}
				}else{

				whereArray=this.FramesHbs_framesHb;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("opti:oisProcData_frames_hb");//omUtils.js
					}
					this.addFramesHbs_framesHb(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="frames_hbs/frames_hb"){
			this.addFramesHbs_framesHb(v);
		}
		else{
			this.extension.setReferenceField(xmlPath,v);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="frames_hbs/frames_hb"){
			return "http://nrg.wustl.edu/opti:oisProcData_frames_hb";
		}
		else{
			return this.extension.getReferenceFieldName(xmlPath);
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="inputParams/rs_samplingrate"){
			return "field_data";
		}else if (xmlPath=="inputParams/rs_lowpass"){
			return "field_data";
		}else if (xmlPath=="inputParams/rs_highpass"){
			return "field_data";
		}else if (xmlPath=="inputParams/stim_samplingrate"){
			return "field_data";
		}else if (xmlPath=="inputParams/stim_lowpass"){
			return "field_data";
		}else if (xmlPath=="inputParams/stim_highpass"){
			return "field_data";
		}else if (xmlPath=="inputParams/stim_blocksize"){
			return "field_data";
		}else if (xmlPath=="inputParams/stim_baseline"){
			return "field_data";
		}else if (xmlPath=="inputParams/stim_duration"){
			return "field_data";
		}else if (xmlPath=="frames_hbs/frames_hb"){
			return "field_NO_CHILD";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<opti:OISProcessed";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</opti:OISProcessed>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
			var child0=0;
			var att0=0;
			if(this.Inputparams_rsSamplingrate!=null)
			child0++;
			if(this.Inputparams_stimDuration!=null)
			child0++;
			if(this.Inputparams_stimSamplingrate!=null)
			child0++;
			if(this.Inputparams_rsHighpass!=null)
			child0++;
			if(this.Inputparams_stimBaseline!=null)
			child0++;
			if(this.Inputparams_stimLowpass!=null)
			child0++;
			if(this.Inputparams_stimHighpass!=null)
			child0++;
			if(this.Inputparams_stimBlocksize!=null)
			child0++;
			if(this.Inputparams_rsLowpass!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<opti:inputParams";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Inputparams_rsSamplingrate!=null){
			xmlTxt+="\n<opti:rs_samplingrate";
			xmlTxt+=">";
			xmlTxt+=this.Inputparams_rsSamplingrate;
			xmlTxt+="</opti:rs_samplingrate>";
		}
		if (this.Inputparams_rsLowpass!=null){
			xmlTxt+="\n<opti:rs_lowpass";
			xmlTxt+=">";
			xmlTxt+=this.Inputparams_rsLowpass;
			xmlTxt+="</opti:rs_lowpass>";
		}
		if (this.Inputparams_rsHighpass!=null){
			xmlTxt+="\n<opti:rs_highpass";
			xmlTxt+=">";
			xmlTxt+=this.Inputparams_rsHighpass;
			xmlTxt+="</opti:rs_highpass>";
		}
		if (this.Inputparams_stimSamplingrate!=null){
			xmlTxt+="\n<opti:stim_samplingrate";
			xmlTxt+=">";
			xmlTxt+=this.Inputparams_stimSamplingrate;
			xmlTxt+="</opti:stim_samplingrate>";
		}
		if (this.Inputparams_stimLowpass!=null){
			xmlTxt+="\n<opti:stim_lowpass";
			xmlTxt+=">";
			xmlTxt+=this.Inputparams_stimLowpass;
			xmlTxt+="</opti:stim_lowpass>";
		}
		if (this.Inputparams_stimHighpass!=null){
			xmlTxt+="\n<opti:stim_highpass";
			xmlTxt+=">";
			xmlTxt+=this.Inputparams_stimHighpass;
			xmlTxt+="</opti:stim_highpass>";
		}
		if (this.Inputparams_stimBlocksize!=null){
			xmlTxt+="\n<opti:stim_blocksize";
			xmlTxt+=">";
			xmlTxt+=this.Inputparams_stimBlocksize;
			xmlTxt+="</opti:stim_blocksize>";
		}
		if (this.Inputparams_stimBaseline!=null){
			xmlTxt+="\n<opti:stim_baseline";
			xmlTxt+=">";
			xmlTxt+=this.Inputparams_stimBaseline;
			xmlTxt+="</opti:stim_baseline>";
		}
		if (this.Inputparams_stimDuration!=null){
			xmlTxt+="\n<opti:stim_duration";
			xmlTxt+=">";
			xmlTxt+=this.Inputparams_stimDuration;
			xmlTxt+="</opti:stim_duration>";
		}
				xmlTxt+="\n</opti:inputParams>";
			}
			}

			var child1=0;
			var att1=0;
			child1+=this.FramesHbs_framesHb.length;
			if(child1>0 || att1>0){
				xmlTxt+="\n<opti:frames_hbs";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var FramesHbs_framesHbCOUNT=0;FramesHbs_framesHbCOUNT<this.FramesHbs_framesHb.length;FramesHbs_framesHbCOUNT++){
			xmlTxt +="\n<opti:frames_hb";
			xmlTxt +=this.FramesHbs_framesHb[FramesHbs_framesHbCOUNT].getXMLAtts();
			if(this.FramesHbs_framesHb[FramesHbs_framesHbCOUNT].xsiType!="opti:oisProcData_frames_hb"){
				xmlTxt+=" xsi:type=\"" + this.FramesHbs_framesHb[FramesHbs_framesHbCOUNT].xsiType + "\"";
			}
			if (this.FramesHbs_framesHb[FramesHbs_framesHbCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.FramesHbs_framesHb[FramesHbs_framesHbCOUNT].getXMLBody(preventComments);
					xmlTxt+="</opti:frames_hb>";
			}else {xmlTxt+="/>";}
		}
				xmlTxt+="\n</opti:frames_hbs>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
			if(this.Inputparams_rsSamplingrate!=null) return true;
			if(this.Inputparams_stimDuration!=null) return true;
			if(this.Inputparams_stimSamplingrate!=null) return true;
			if(this.Inputparams_rsHighpass!=null) return true;
			if(this.Inputparams_stimBaseline!=null) return true;
			if(this.Inputparams_stimLowpass!=null) return true;
			if(this.Inputparams_stimHighpass!=null) return true;
			if(this.Inputparams_stimBlocksize!=null) return true;
			if(this.Inputparams_rsLowpass!=null) return true;
			if(this.FramesHbs_framesHb.length>0)return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
