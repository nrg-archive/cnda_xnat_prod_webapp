/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:04 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function uds_b3updrsData(){
this.xsiType="uds:b3updrsData";

	this.getSchemaElementName=function(){
		return "b3updrsData";
	}

	this.getFullSchemaElementName=function(){
		return "uds:b3updrsData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Initials=null;


	function getInitials() {
		return this.Initials;
	}
	this.getInitials=getInitials;


	function setInitials(v){
		this.Initials=v;
	}
	this.setInitials=setInitials;

	this.Pdnormal=null;


	function getPdnormal() {
		return this.Pdnormal;
	}
	this.getPdnormal=getPdnormal;


	function setPdnormal(v){
		this.Pdnormal=v;
	}
	this.setPdnormal=setPdnormal;

	this.Speech=null;


	function getSpeech() {
		return this.Speech;
	}
	this.getSpeech=getSpeech;


	function setSpeech(v){
		this.Speech=v;
	}
	this.setSpeech=setSpeech;

	this.Speechx=null;


	function getSpeechx() {
		return this.Speechx;
	}
	this.getSpeechx=getSpeechx;


	function setSpeechx(v){
		this.Speechx=v;
	}
	this.setSpeechx=setSpeechx;

	this.Facexp=null;


	function getFacexp() {
		return this.Facexp;
	}
	this.getFacexp=getFacexp;


	function setFacexp(v){
		this.Facexp=v;
	}
	this.setFacexp=setFacexp;

	this.Facexpx=null;


	function getFacexpx() {
		return this.Facexpx;
	}
	this.getFacexpx=getFacexpx;


	function setFacexpx(v){
		this.Facexpx=v;
	}
	this.setFacexpx=setFacexpx;

	this.Trestfac=null;


	function getTrestfac() {
		return this.Trestfac;
	}
	this.getTrestfac=getTrestfac;


	function setTrestfac(v){
		this.Trestfac=v;
	}
	this.setTrestfac=setTrestfac;

	this.Trestfax=null;


	function getTrestfax() {
		return this.Trestfax;
	}
	this.getTrestfax=getTrestfax;


	function setTrestfax(v){
		this.Trestfax=v;
	}
	this.setTrestfax=setTrestfax;

	this.Trestrhd=null;


	function getTrestrhd() {
		return this.Trestrhd;
	}
	this.getTrestrhd=getTrestrhd;


	function setTrestrhd(v){
		this.Trestrhd=v;
	}
	this.setTrestrhd=setTrestrhd;

	this.Trestrhx=null;


	function getTrestrhx() {
		return this.Trestrhx;
	}
	this.getTrestrhx=getTrestrhx;


	function setTrestrhx(v){
		this.Trestrhx=v;
	}
	this.setTrestrhx=setTrestrhx;

	this.Trestlhd=null;


	function getTrestlhd() {
		return this.Trestlhd;
	}
	this.getTrestlhd=getTrestlhd;


	function setTrestlhd(v){
		this.Trestlhd=v;
	}
	this.setTrestlhd=setTrestlhd;

	this.Trestlhx=null;


	function getTrestlhx() {
		return this.Trestlhx;
	}
	this.getTrestlhx=getTrestlhx;


	function setTrestlhx(v){
		this.Trestlhx=v;
	}
	this.setTrestlhx=setTrestlhx;

	this.Trestrft=null;


	function getTrestrft() {
		return this.Trestrft;
	}
	this.getTrestrft=getTrestrft;


	function setTrestrft(v){
		this.Trestrft=v;
	}
	this.setTrestrft=setTrestrft;

	this.Trestrfx=null;


	function getTrestrfx() {
		return this.Trestrfx;
	}
	this.getTrestrfx=getTrestrfx;


	function setTrestrfx(v){
		this.Trestrfx=v;
	}
	this.setTrestrfx=setTrestrfx;

	this.Trestlft=null;


	function getTrestlft() {
		return this.Trestlft;
	}
	this.getTrestlft=getTrestlft;


	function setTrestlft(v){
		this.Trestlft=v;
	}
	this.setTrestlft=setTrestlft;

	this.Trestlfx=null;


	function getTrestlfx() {
		return this.Trestlfx;
	}
	this.getTrestlfx=getTrestlfx;


	function setTrestlfx(v){
		this.Trestlfx=v;
	}
	this.setTrestlfx=setTrestlfx;

	this.Tractrhd=null;


	function getTractrhd() {
		return this.Tractrhd;
	}
	this.getTractrhd=getTractrhd;


	function setTractrhd(v){
		this.Tractrhd=v;
	}
	this.setTractrhd=setTractrhd;

	this.Tractrhx=null;


	function getTractrhx() {
		return this.Tractrhx;
	}
	this.getTractrhx=getTractrhx;


	function setTractrhx(v){
		this.Tractrhx=v;
	}
	this.setTractrhx=setTractrhx;

	this.Tractlhd=null;


	function getTractlhd() {
		return this.Tractlhd;
	}
	this.getTractlhd=getTractlhd;


	function setTractlhd(v){
		this.Tractlhd=v;
	}
	this.setTractlhd=setTractlhd;

	this.Tractlhx=null;


	function getTractlhx() {
		return this.Tractlhx;
	}
	this.getTractlhx=getTractlhx;


	function setTractlhx(v){
		this.Tractlhx=v;
	}
	this.setTractlhx=setTractlhx;

	this.Rigdneck=null;


	function getRigdneck() {
		return this.Rigdneck;
	}
	this.getRigdneck=getRigdneck;


	function setRigdneck(v){
		this.Rigdneck=v;
	}
	this.setRigdneck=setRigdneck;

	this.Rigdnex=null;


	function getRigdnex() {
		return this.Rigdnex;
	}
	this.getRigdnex=getRigdnex;


	function setRigdnex(v){
		this.Rigdnex=v;
	}
	this.setRigdnex=setRigdnex;

	this.Rigduprt=null;


	function getRigduprt() {
		return this.Rigduprt;
	}
	this.getRigduprt=getRigduprt;


	function setRigduprt(v){
		this.Rigduprt=v;
	}
	this.setRigduprt=setRigduprt;

	this.Rigduprx=null;


	function getRigduprx() {
		return this.Rigduprx;
	}
	this.getRigduprx=getRigduprx;


	function setRigduprx(v){
		this.Rigduprx=v;
	}
	this.setRigduprx=setRigduprx;

	this.Rigduplf=null;


	function getRigduplf() {
		return this.Rigduplf;
	}
	this.getRigduplf=getRigduplf;


	function setRigduplf(v){
		this.Rigduplf=v;
	}
	this.setRigduplf=setRigduplf;

	this.Rigduplx=null;


	function getRigduplx() {
		return this.Rigduplx;
	}
	this.getRigduplx=getRigduplx;


	function setRigduplx(v){
		this.Rigduplx=v;
	}
	this.setRigduplx=setRigduplx;

	this.Rigdlort=null;


	function getRigdlort() {
		return this.Rigdlort;
	}
	this.getRigdlort=getRigdlort;


	function setRigdlort(v){
		this.Rigdlort=v;
	}
	this.setRigdlort=setRigdlort;

	this.Rigdlorx=null;


	function getRigdlorx() {
		return this.Rigdlorx;
	}
	this.getRigdlorx=getRigdlorx;


	function setRigdlorx(v){
		this.Rigdlorx=v;
	}
	this.setRigdlorx=setRigdlorx;

	this.Rigdlolf=null;


	function getRigdlolf() {
		return this.Rigdlolf;
	}
	this.getRigdlolf=getRigdlolf;


	function setRigdlolf(v){
		this.Rigdlolf=v;
	}
	this.setRigdlolf=setRigdlolf;

	this.Rigdlolx=null;


	function getRigdlolx() {
		return this.Rigdlolx;
	}
	this.getRigdlolx=getRigdlolx;


	function setRigdlolx(v){
		this.Rigdlolx=v;
	}
	this.setRigdlolx=setRigdlolx;

	this.Tapsrt=null;


	function getTapsrt() {
		return this.Tapsrt;
	}
	this.getTapsrt=getTapsrt;


	function setTapsrt(v){
		this.Tapsrt=v;
	}
	this.setTapsrt=setTapsrt;

	this.Tapsrtx=null;


	function getTapsrtx() {
		return this.Tapsrtx;
	}
	this.getTapsrtx=getTapsrtx;


	function setTapsrtx(v){
		this.Tapsrtx=v;
	}
	this.setTapsrtx=setTapsrtx;

	this.Tapslf=null;


	function getTapslf() {
		return this.Tapslf;
	}
	this.getTapslf=getTapslf;


	function setTapslf(v){
		this.Tapslf=v;
	}
	this.setTapslf=setTapslf;

	this.Tapslfx=null;


	function getTapslfx() {
		return this.Tapslfx;
	}
	this.getTapslfx=getTapslfx;


	function setTapslfx(v){
		this.Tapslfx=v;
	}
	this.setTapslfx=setTapslfx;

	this.Handmovr=null;


	function getHandmovr() {
		return this.Handmovr;
	}
	this.getHandmovr=getHandmovr;


	function setHandmovr(v){
		this.Handmovr=v;
	}
	this.setHandmovr=setHandmovr;

	this.Handmvrx=null;


	function getHandmvrx() {
		return this.Handmvrx;
	}
	this.getHandmvrx=getHandmvrx;


	function setHandmvrx(v){
		this.Handmvrx=v;
	}
	this.setHandmvrx=setHandmvrx;

	this.Handmovl=null;


	function getHandmovl() {
		return this.Handmovl;
	}
	this.getHandmovl=getHandmovl;


	function setHandmovl(v){
		this.Handmovl=v;
	}
	this.setHandmovl=setHandmovl;

	this.Handmvlx=null;


	function getHandmvlx() {
		return this.Handmvlx;
	}
	this.getHandmvlx=getHandmvlx;


	function setHandmvlx(v){
		this.Handmvlx=v;
	}
	this.setHandmvlx=setHandmvlx;

	this.Handaltr=null;


	function getHandaltr() {
		return this.Handaltr;
	}
	this.getHandaltr=getHandaltr;


	function setHandaltr(v){
		this.Handaltr=v;
	}
	this.setHandaltr=setHandaltr;

	this.Handatrx=null;


	function getHandatrx() {
		return this.Handatrx;
	}
	this.getHandatrx=getHandatrx;


	function setHandatrx(v){
		this.Handatrx=v;
	}
	this.setHandatrx=setHandatrx;

	this.Handaltl=null;


	function getHandaltl() {
		return this.Handaltl;
	}
	this.getHandaltl=getHandaltl;


	function setHandaltl(v){
		this.Handaltl=v;
	}
	this.setHandaltl=setHandaltl;

	this.Handatlx=null;


	function getHandatlx() {
		return this.Handatlx;
	}
	this.getHandatlx=getHandatlx;


	function setHandatlx(v){
		this.Handatlx=v;
	}
	this.setHandatlx=setHandatlx;

	this.Legrt=null;


	function getLegrt() {
		return this.Legrt;
	}
	this.getLegrt=getLegrt;


	function setLegrt(v){
		this.Legrt=v;
	}
	this.setLegrt=setLegrt;

	this.Legrtx=null;


	function getLegrtx() {
		return this.Legrtx;
	}
	this.getLegrtx=getLegrtx;


	function setLegrtx(v){
		this.Legrtx=v;
	}
	this.setLegrtx=setLegrtx;

	this.Leglf=null;


	function getLeglf() {
		return this.Leglf;
	}
	this.getLeglf=getLeglf;


	function setLeglf(v){
		this.Leglf=v;
	}
	this.setLeglf=setLeglf;

	this.Leglfx=null;


	function getLeglfx() {
		return this.Leglfx;
	}
	this.getLeglfx=getLeglfx;


	function setLeglfx(v){
		this.Leglfx=v;
	}
	this.setLeglfx=setLeglfx;

	this.Arising=null;


	function getArising() {
		return this.Arising;
	}
	this.getArising=getArising;


	function setArising(v){
		this.Arising=v;
	}
	this.setArising=setArising;

	this.Arisingx=null;


	function getArisingx() {
		return this.Arisingx;
	}
	this.getArisingx=getArisingx;


	function setArisingx(v){
		this.Arisingx=v;
	}
	this.setArisingx=setArisingx;

	this.Posture=null;


	function getPosture() {
		return this.Posture;
	}
	this.getPosture=getPosture;


	function setPosture(v){
		this.Posture=v;
	}
	this.setPosture=setPosture;

	this.Posturex=null;


	function getPosturex() {
		return this.Posturex;
	}
	this.getPosturex=getPosturex;


	function setPosturex(v){
		this.Posturex=v;
	}
	this.setPosturex=setPosturex;

	this.Gait=null;


	function getGait() {
		return this.Gait;
	}
	this.getGait=getGait;


	function setGait(v){
		this.Gait=v;
	}
	this.setGait=setGait;

	this.Gaitx=null;


	function getGaitx() {
		return this.Gaitx;
	}
	this.getGaitx=getGaitx;


	function setGaitx(v){
		this.Gaitx=v;
	}
	this.setGaitx=setGaitx;

	this.Posstab=null;


	function getPosstab() {
		return this.Posstab;
	}
	this.getPosstab=getPosstab;


	function setPosstab(v){
		this.Posstab=v;
	}
	this.setPosstab=setPosstab;

	this.Posstabx=null;


	function getPosstabx() {
		return this.Posstabx;
	}
	this.getPosstabx=getPosstabx;


	function setPosstabx(v){
		this.Posstabx=v;
	}
	this.setPosstabx=setPosstabx;

	this.Bradykin=null;


	function getBradykin() {
		return this.Bradykin;
	}
	this.getBradykin=getBradykin;


	function setBradykin(v){
		this.Bradykin=v;
	}
	this.setBradykin=setBradykin;

	this.Bradykix=null;


	function getBradykix() {
		return this.Bradykix;
	}
	this.getBradykix=getBradykix;


	function setBradykix(v){
		this.Bradykix=v;
	}
	this.setBradykix=setBradykix;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="INITIALS"){
				return this.Initials ;
			} else 
			if(xmlPath=="PDNORMAL"){
				return this.Pdnormal ;
			} else 
			if(xmlPath=="SPEECH"){
				return this.Speech ;
			} else 
			if(xmlPath=="SPEECHX"){
				return this.Speechx ;
			} else 
			if(xmlPath=="FACEXP"){
				return this.Facexp ;
			} else 
			if(xmlPath=="FACEXPX"){
				return this.Facexpx ;
			} else 
			if(xmlPath=="TRESTFAC"){
				return this.Trestfac ;
			} else 
			if(xmlPath=="TRESTFAX"){
				return this.Trestfax ;
			} else 
			if(xmlPath=="TRESTRHD"){
				return this.Trestrhd ;
			} else 
			if(xmlPath=="TRESTRHX"){
				return this.Trestrhx ;
			} else 
			if(xmlPath=="TRESTLHD"){
				return this.Trestlhd ;
			} else 
			if(xmlPath=="TRESTLHX"){
				return this.Trestlhx ;
			} else 
			if(xmlPath=="TRESTRFT"){
				return this.Trestrft ;
			} else 
			if(xmlPath=="TRESTRFX"){
				return this.Trestrfx ;
			} else 
			if(xmlPath=="TRESTLFT"){
				return this.Trestlft ;
			} else 
			if(xmlPath=="TRESTLFX"){
				return this.Trestlfx ;
			} else 
			if(xmlPath=="TRACTRHD"){
				return this.Tractrhd ;
			} else 
			if(xmlPath=="TRACTRHX"){
				return this.Tractrhx ;
			} else 
			if(xmlPath=="TRACTLHD"){
				return this.Tractlhd ;
			} else 
			if(xmlPath=="TRACTLHX"){
				return this.Tractlhx ;
			} else 
			if(xmlPath=="RIGDNECK"){
				return this.Rigdneck ;
			} else 
			if(xmlPath=="RIGDNEX"){
				return this.Rigdnex ;
			} else 
			if(xmlPath=="RIGDUPRT"){
				return this.Rigduprt ;
			} else 
			if(xmlPath=="RIGDUPRX"){
				return this.Rigduprx ;
			} else 
			if(xmlPath=="RIGDUPLF"){
				return this.Rigduplf ;
			} else 
			if(xmlPath=="RIGDUPLX"){
				return this.Rigduplx ;
			} else 
			if(xmlPath=="RIGDLORT"){
				return this.Rigdlort ;
			} else 
			if(xmlPath=="RIGDLORX"){
				return this.Rigdlorx ;
			} else 
			if(xmlPath=="RIGDLOLF"){
				return this.Rigdlolf ;
			} else 
			if(xmlPath=="RIGDLOLX"){
				return this.Rigdlolx ;
			} else 
			if(xmlPath=="TAPSRT"){
				return this.Tapsrt ;
			} else 
			if(xmlPath=="TAPSRTX"){
				return this.Tapsrtx ;
			} else 
			if(xmlPath=="TAPSLF"){
				return this.Tapslf ;
			} else 
			if(xmlPath=="TAPSLFX"){
				return this.Tapslfx ;
			} else 
			if(xmlPath=="HANDMOVR"){
				return this.Handmovr ;
			} else 
			if(xmlPath=="HANDMVRX"){
				return this.Handmvrx ;
			} else 
			if(xmlPath=="HANDMOVL"){
				return this.Handmovl ;
			} else 
			if(xmlPath=="HANDMVLX"){
				return this.Handmvlx ;
			} else 
			if(xmlPath=="HANDALTR"){
				return this.Handaltr ;
			} else 
			if(xmlPath=="HANDATRX"){
				return this.Handatrx ;
			} else 
			if(xmlPath=="HANDALTL"){
				return this.Handaltl ;
			} else 
			if(xmlPath=="HANDATLX"){
				return this.Handatlx ;
			} else 
			if(xmlPath=="LEGRT"){
				return this.Legrt ;
			} else 
			if(xmlPath=="LEGRTX"){
				return this.Legrtx ;
			} else 
			if(xmlPath=="LEGLF"){
				return this.Leglf ;
			} else 
			if(xmlPath=="LEGLFX"){
				return this.Leglfx ;
			} else 
			if(xmlPath=="ARISING"){
				return this.Arising ;
			} else 
			if(xmlPath=="ARISINGX"){
				return this.Arisingx ;
			} else 
			if(xmlPath=="POSTURE"){
				return this.Posture ;
			} else 
			if(xmlPath=="POSTUREX"){
				return this.Posturex ;
			} else 
			if(xmlPath=="GAIT"){
				return this.Gait ;
			} else 
			if(xmlPath=="GAITX"){
				return this.Gaitx ;
			} else 
			if(xmlPath=="POSSTAB"){
				return this.Posstab ;
			} else 
			if(xmlPath=="POSSTABX"){
				return this.Posstabx ;
			} else 
			if(xmlPath=="BRADYKIN"){
				return this.Bradykin ;
			} else 
			if(xmlPath=="BRADYKIX"){
				return this.Bradykix ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="INITIALS"){
				this.Initials=value;
			} else 
			if(xmlPath=="PDNORMAL"){
				this.Pdnormal=value;
			} else 
			if(xmlPath=="SPEECH"){
				this.Speech=value;
			} else 
			if(xmlPath=="SPEECHX"){
				this.Speechx=value;
			} else 
			if(xmlPath=="FACEXP"){
				this.Facexp=value;
			} else 
			if(xmlPath=="FACEXPX"){
				this.Facexpx=value;
			} else 
			if(xmlPath=="TRESTFAC"){
				this.Trestfac=value;
			} else 
			if(xmlPath=="TRESTFAX"){
				this.Trestfax=value;
			} else 
			if(xmlPath=="TRESTRHD"){
				this.Trestrhd=value;
			} else 
			if(xmlPath=="TRESTRHX"){
				this.Trestrhx=value;
			} else 
			if(xmlPath=="TRESTLHD"){
				this.Trestlhd=value;
			} else 
			if(xmlPath=="TRESTLHX"){
				this.Trestlhx=value;
			} else 
			if(xmlPath=="TRESTRFT"){
				this.Trestrft=value;
			} else 
			if(xmlPath=="TRESTRFX"){
				this.Trestrfx=value;
			} else 
			if(xmlPath=="TRESTLFT"){
				this.Trestlft=value;
			} else 
			if(xmlPath=="TRESTLFX"){
				this.Trestlfx=value;
			} else 
			if(xmlPath=="TRACTRHD"){
				this.Tractrhd=value;
			} else 
			if(xmlPath=="TRACTRHX"){
				this.Tractrhx=value;
			} else 
			if(xmlPath=="TRACTLHD"){
				this.Tractlhd=value;
			} else 
			if(xmlPath=="TRACTLHX"){
				this.Tractlhx=value;
			} else 
			if(xmlPath=="RIGDNECK"){
				this.Rigdneck=value;
			} else 
			if(xmlPath=="RIGDNEX"){
				this.Rigdnex=value;
			} else 
			if(xmlPath=="RIGDUPRT"){
				this.Rigduprt=value;
			} else 
			if(xmlPath=="RIGDUPRX"){
				this.Rigduprx=value;
			} else 
			if(xmlPath=="RIGDUPLF"){
				this.Rigduplf=value;
			} else 
			if(xmlPath=="RIGDUPLX"){
				this.Rigduplx=value;
			} else 
			if(xmlPath=="RIGDLORT"){
				this.Rigdlort=value;
			} else 
			if(xmlPath=="RIGDLORX"){
				this.Rigdlorx=value;
			} else 
			if(xmlPath=="RIGDLOLF"){
				this.Rigdlolf=value;
			} else 
			if(xmlPath=="RIGDLOLX"){
				this.Rigdlolx=value;
			} else 
			if(xmlPath=="TAPSRT"){
				this.Tapsrt=value;
			} else 
			if(xmlPath=="TAPSRTX"){
				this.Tapsrtx=value;
			} else 
			if(xmlPath=="TAPSLF"){
				this.Tapslf=value;
			} else 
			if(xmlPath=="TAPSLFX"){
				this.Tapslfx=value;
			} else 
			if(xmlPath=="HANDMOVR"){
				this.Handmovr=value;
			} else 
			if(xmlPath=="HANDMVRX"){
				this.Handmvrx=value;
			} else 
			if(xmlPath=="HANDMOVL"){
				this.Handmovl=value;
			} else 
			if(xmlPath=="HANDMVLX"){
				this.Handmvlx=value;
			} else 
			if(xmlPath=="HANDALTR"){
				this.Handaltr=value;
			} else 
			if(xmlPath=="HANDATRX"){
				this.Handatrx=value;
			} else 
			if(xmlPath=="HANDALTL"){
				this.Handaltl=value;
			} else 
			if(xmlPath=="HANDATLX"){
				this.Handatlx=value;
			} else 
			if(xmlPath=="LEGRT"){
				this.Legrt=value;
			} else 
			if(xmlPath=="LEGRTX"){
				this.Legrtx=value;
			} else 
			if(xmlPath=="LEGLF"){
				this.Leglf=value;
			} else 
			if(xmlPath=="LEGLFX"){
				this.Leglfx=value;
			} else 
			if(xmlPath=="ARISING"){
				this.Arising=value;
			} else 
			if(xmlPath=="ARISINGX"){
				this.Arisingx=value;
			} else 
			if(xmlPath=="POSTURE"){
				this.Posture=value;
			} else 
			if(xmlPath=="POSTUREX"){
				this.Posturex=value;
			} else 
			if(xmlPath=="GAIT"){
				this.Gait=value;
			} else 
			if(xmlPath=="GAITX"){
				this.Gaitx=value;
			} else 
			if(xmlPath=="POSSTAB"){
				this.Posstab=value;
			} else 
			if(xmlPath=="POSSTABX"){
				this.Posstabx=value;
			} else 
			if(xmlPath=="BRADYKIN"){
				this.Bradykin=value;
			} else 
			if(xmlPath=="BRADYKIX"){
				this.Bradykix=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="INITIALS"){
			return "field_data";
		}else if (xmlPath=="PDNORMAL"){
			return "field_data";
		}else if (xmlPath=="SPEECH"){
			return "field_data";
		}else if (xmlPath=="SPEECHX"){
			return "field_data";
		}else if (xmlPath=="FACEXP"){
			return "field_data";
		}else if (xmlPath=="FACEXPX"){
			return "field_data";
		}else if (xmlPath=="TRESTFAC"){
			return "field_data";
		}else if (xmlPath=="TRESTFAX"){
			return "field_data";
		}else if (xmlPath=="TRESTRHD"){
			return "field_data";
		}else if (xmlPath=="TRESTRHX"){
			return "field_data";
		}else if (xmlPath=="TRESTLHD"){
			return "field_data";
		}else if (xmlPath=="TRESTLHX"){
			return "field_data";
		}else if (xmlPath=="TRESTRFT"){
			return "field_data";
		}else if (xmlPath=="TRESTRFX"){
			return "field_data";
		}else if (xmlPath=="TRESTLFT"){
			return "field_data";
		}else if (xmlPath=="TRESTLFX"){
			return "field_data";
		}else if (xmlPath=="TRACTRHD"){
			return "field_data";
		}else if (xmlPath=="TRACTRHX"){
			return "field_data";
		}else if (xmlPath=="TRACTLHD"){
			return "field_data";
		}else if (xmlPath=="TRACTLHX"){
			return "field_data";
		}else if (xmlPath=="RIGDNECK"){
			return "field_data";
		}else if (xmlPath=="RIGDNEX"){
			return "field_data";
		}else if (xmlPath=="RIGDUPRT"){
			return "field_data";
		}else if (xmlPath=="RIGDUPRX"){
			return "field_data";
		}else if (xmlPath=="RIGDUPLF"){
			return "field_data";
		}else if (xmlPath=="RIGDUPLX"){
			return "field_data";
		}else if (xmlPath=="RIGDLORT"){
			return "field_data";
		}else if (xmlPath=="RIGDLORX"){
			return "field_data";
		}else if (xmlPath=="RIGDLOLF"){
			return "field_data";
		}else if (xmlPath=="RIGDLOLX"){
			return "field_data";
		}else if (xmlPath=="TAPSRT"){
			return "field_data";
		}else if (xmlPath=="TAPSRTX"){
			return "field_data";
		}else if (xmlPath=="TAPSLF"){
			return "field_data";
		}else if (xmlPath=="TAPSLFX"){
			return "field_data";
		}else if (xmlPath=="HANDMOVR"){
			return "field_data";
		}else if (xmlPath=="HANDMVRX"){
			return "field_data";
		}else if (xmlPath=="HANDMOVL"){
			return "field_data";
		}else if (xmlPath=="HANDMVLX"){
			return "field_data";
		}else if (xmlPath=="HANDALTR"){
			return "field_data";
		}else if (xmlPath=="HANDATRX"){
			return "field_data";
		}else if (xmlPath=="HANDALTL"){
			return "field_data";
		}else if (xmlPath=="HANDATLX"){
			return "field_data";
		}else if (xmlPath=="LEGRT"){
			return "field_data";
		}else if (xmlPath=="LEGRTX"){
			return "field_data";
		}else if (xmlPath=="LEGLF"){
			return "field_data";
		}else if (xmlPath=="LEGLFX"){
			return "field_data";
		}else if (xmlPath=="ARISING"){
			return "field_data";
		}else if (xmlPath=="ARISINGX"){
			return "field_data";
		}else if (xmlPath=="POSTURE"){
			return "field_data";
		}else if (xmlPath=="POSTUREX"){
			return "field_data";
		}else if (xmlPath=="GAIT"){
			return "field_data";
		}else if (xmlPath=="GAITX"){
			return "field_data";
		}else if (xmlPath=="POSSTAB"){
			return "field_data";
		}else if (xmlPath=="POSSTABX"){
			return "field_data";
		}else if (xmlPath=="BRADYKIN"){
			return "field_data";
		}else if (xmlPath=="BRADYKIX"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<uds:B3UPDRS";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</uds:B3UPDRS>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Initials!=null){
			xmlTxt+="\n<uds:INITIALS";
			xmlTxt+=">";
			xmlTxt+=this.Initials.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:INITIALS>";
		}
		if (this.Pdnormal!=null){
			xmlTxt+="\n<uds:PDNORMAL";
			xmlTxt+=">";
			xmlTxt+=this.Pdnormal;
			xmlTxt+="</uds:PDNORMAL>";
		}
		if (this.Speech!=null){
			xmlTxt+="\n<uds:SPEECH";
			xmlTxt+=">";
			xmlTxt+=this.Speech;
			xmlTxt+="</uds:SPEECH>";
		}
		if (this.Speechx!=null){
			xmlTxt+="\n<uds:SPEECHX";
			xmlTxt+=">";
			xmlTxt+=this.Speechx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:SPEECHX>";
		}
		if (this.Facexp!=null){
			xmlTxt+="\n<uds:FACEXP";
			xmlTxt+=">";
			xmlTxt+=this.Facexp;
			xmlTxt+="</uds:FACEXP>";
		}
		if (this.Facexpx!=null){
			xmlTxt+="\n<uds:FACEXPX";
			xmlTxt+=">";
			xmlTxt+=this.Facexpx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:FACEXPX>";
		}
		if (this.Trestfac!=null){
			xmlTxt+="\n<uds:TRESTFAC";
			xmlTxt+=">";
			xmlTxt+=this.Trestfac;
			xmlTxt+="</uds:TRESTFAC>";
		}
		if (this.Trestfax!=null){
			xmlTxt+="\n<uds:TRESTFAX";
			xmlTxt+=">";
			xmlTxt+=this.Trestfax.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:TRESTFAX>";
		}
		if (this.Trestrhd!=null){
			xmlTxt+="\n<uds:TRESTRHD";
			xmlTxt+=">";
			xmlTxt+=this.Trestrhd;
			xmlTxt+="</uds:TRESTRHD>";
		}
		if (this.Trestrhx!=null){
			xmlTxt+="\n<uds:TRESTRHX";
			xmlTxt+=">";
			xmlTxt+=this.Trestrhx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:TRESTRHX>";
		}
		if (this.Trestlhd!=null){
			xmlTxt+="\n<uds:TRESTLHD";
			xmlTxt+=">";
			xmlTxt+=this.Trestlhd;
			xmlTxt+="</uds:TRESTLHD>";
		}
		if (this.Trestlhx!=null){
			xmlTxt+="\n<uds:TRESTLHX";
			xmlTxt+=">";
			xmlTxt+=this.Trestlhx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:TRESTLHX>";
		}
		if (this.Trestrft!=null){
			xmlTxt+="\n<uds:TRESTRFT";
			xmlTxt+=">";
			xmlTxt+=this.Trestrft;
			xmlTxt+="</uds:TRESTRFT>";
		}
		if (this.Trestrfx!=null){
			xmlTxt+="\n<uds:TRESTRFX";
			xmlTxt+=">";
			xmlTxt+=this.Trestrfx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:TRESTRFX>";
		}
		if (this.Trestlft!=null){
			xmlTxt+="\n<uds:TRESTLFT";
			xmlTxt+=">";
			xmlTxt+=this.Trestlft;
			xmlTxt+="</uds:TRESTLFT>";
		}
		if (this.Trestlfx!=null){
			xmlTxt+="\n<uds:TRESTLFX";
			xmlTxt+=">";
			xmlTxt+=this.Trestlfx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:TRESTLFX>";
		}
		if (this.Tractrhd!=null){
			xmlTxt+="\n<uds:TRACTRHD";
			xmlTxt+=">";
			xmlTxt+=this.Tractrhd;
			xmlTxt+="</uds:TRACTRHD>";
		}
		if (this.Tractrhx!=null){
			xmlTxt+="\n<uds:TRACTRHX";
			xmlTxt+=">";
			xmlTxt+=this.Tractrhx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:TRACTRHX>";
		}
		if (this.Tractlhd!=null){
			xmlTxt+="\n<uds:TRACTLHD";
			xmlTxt+=">";
			xmlTxt+=this.Tractlhd;
			xmlTxt+="</uds:TRACTLHD>";
		}
		if (this.Tractlhx!=null){
			xmlTxt+="\n<uds:TRACTLHX";
			xmlTxt+=">";
			xmlTxt+=this.Tractlhx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:TRACTLHX>";
		}
		if (this.Rigdneck!=null){
			xmlTxt+="\n<uds:RIGDNECK";
			xmlTxt+=">";
			xmlTxt+=this.Rigdneck;
			xmlTxt+="</uds:RIGDNECK>";
		}
		if (this.Rigdnex!=null){
			xmlTxt+="\n<uds:RIGDNEX";
			xmlTxt+=">";
			xmlTxt+=this.Rigdnex.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:RIGDNEX>";
		}
		if (this.Rigduprt!=null){
			xmlTxt+="\n<uds:RIGDUPRT";
			xmlTxt+=">";
			xmlTxt+=this.Rigduprt;
			xmlTxt+="</uds:RIGDUPRT>";
		}
		if (this.Rigduprx!=null){
			xmlTxt+="\n<uds:RIGDUPRX";
			xmlTxt+=">";
			xmlTxt+=this.Rigduprx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:RIGDUPRX>";
		}
		if (this.Rigduplf!=null){
			xmlTxt+="\n<uds:RIGDUPLF";
			xmlTxt+=">";
			xmlTxt+=this.Rigduplf;
			xmlTxt+="</uds:RIGDUPLF>";
		}
		if (this.Rigduplx!=null){
			xmlTxt+="\n<uds:RIGDUPLX";
			xmlTxt+=">";
			xmlTxt+=this.Rigduplx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:RIGDUPLX>";
		}
		if (this.Rigdlort!=null){
			xmlTxt+="\n<uds:RIGDLORT";
			xmlTxt+=">";
			xmlTxt+=this.Rigdlort;
			xmlTxt+="</uds:RIGDLORT>";
		}
		if (this.Rigdlorx!=null){
			xmlTxt+="\n<uds:RIGDLORX";
			xmlTxt+=">";
			xmlTxt+=this.Rigdlorx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:RIGDLORX>";
		}
		if (this.Rigdlolf!=null){
			xmlTxt+="\n<uds:RIGDLOLF";
			xmlTxt+=">";
			xmlTxt+=this.Rigdlolf;
			xmlTxt+="</uds:RIGDLOLF>";
		}
		if (this.Rigdlolx!=null){
			xmlTxt+="\n<uds:RIGDLOLX";
			xmlTxt+=">";
			xmlTxt+=this.Rigdlolx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:RIGDLOLX>";
		}
		if (this.Tapsrt!=null){
			xmlTxt+="\n<uds:TAPSRT";
			xmlTxt+=">";
			xmlTxt+=this.Tapsrt;
			xmlTxt+="</uds:TAPSRT>";
		}
		if (this.Tapsrtx!=null){
			xmlTxt+="\n<uds:TAPSRTX";
			xmlTxt+=">";
			xmlTxt+=this.Tapsrtx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:TAPSRTX>";
		}
		if (this.Tapslf!=null){
			xmlTxt+="\n<uds:TAPSLF";
			xmlTxt+=">";
			xmlTxt+=this.Tapslf;
			xmlTxt+="</uds:TAPSLF>";
		}
		if (this.Tapslfx!=null){
			xmlTxt+="\n<uds:TAPSLFX";
			xmlTxt+=">";
			xmlTxt+=this.Tapslfx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:TAPSLFX>";
		}
		if (this.Handmovr!=null){
			xmlTxt+="\n<uds:HANDMOVR";
			xmlTxt+=">";
			xmlTxt+=this.Handmovr;
			xmlTxt+="</uds:HANDMOVR>";
		}
		if (this.Handmvrx!=null){
			xmlTxt+="\n<uds:HANDMVRX";
			xmlTxt+=">";
			xmlTxt+=this.Handmvrx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:HANDMVRX>";
		}
		if (this.Handmovl!=null){
			xmlTxt+="\n<uds:HANDMOVL";
			xmlTxt+=">";
			xmlTxt+=this.Handmovl;
			xmlTxt+="</uds:HANDMOVL>";
		}
		if (this.Handmvlx!=null){
			xmlTxt+="\n<uds:HANDMVLX";
			xmlTxt+=">";
			xmlTxt+=this.Handmvlx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:HANDMVLX>";
		}
		if (this.Handaltr!=null){
			xmlTxt+="\n<uds:HANDALTR";
			xmlTxt+=">";
			xmlTxt+=this.Handaltr;
			xmlTxt+="</uds:HANDALTR>";
		}
		if (this.Handatrx!=null){
			xmlTxt+="\n<uds:HANDATRX";
			xmlTxt+=">";
			xmlTxt+=this.Handatrx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:HANDATRX>";
		}
		if (this.Handaltl!=null){
			xmlTxt+="\n<uds:HANDALTL";
			xmlTxt+=">";
			xmlTxt+=this.Handaltl;
			xmlTxt+="</uds:HANDALTL>";
		}
		if (this.Handatlx!=null){
			xmlTxt+="\n<uds:HANDATLX";
			xmlTxt+=">";
			xmlTxt+=this.Handatlx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:HANDATLX>";
		}
		if (this.Legrt!=null){
			xmlTxt+="\n<uds:LEGRT";
			xmlTxt+=">";
			xmlTxt+=this.Legrt;
			xmlTxt+="</uds:LEGRT>";
		}
		if (this.Legrtx!=null){
			xmlTxt+="\n<uds:LEGRTX";
			xmlTxt+=">";
			xmlTxt+=this.Legrtx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:LEGRTX>";
		}
		if (this.Leglf!=null){
			xmlTxt+="\n<uds:LEGLF";
			xmlTxt+=">";
			xmlTxt+=this.Leglf;
			xmlTxt+="</uds:LEGLF>";
		}
		if (this.Leglfx!=null){
			xmlTxt+="\n<uds:LEGLFX";
			xmlTxt+=">";
			xmlTxt+=this.Leglfx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:LEGLFX>";
		}
		if (this.Arising!=null){
			xmlTxt+="\n<uds:ARISING";
			xmlTxt+=">";
			xmlTxt+=this.Arising;
			xmlTxt+="</uds:ARISING>";
		}
		if (this.Arisingx!=null){
			xmlTxt+="\n<uds:ARISINGX";
			xmlTxt+=">";
			xmlTxt+=this.Arisingx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:ARISINGX>";
		}
		if (this.Posture!=null){
			xmlTxt+="\n<uds:POSTURE";
			xmlTxt+=">";
			xmlTxt+=this.Posture;
			xmlTxt+="</uds:POSTURE>";
		}
		if (this.Posturex!=null){
			xmlTxt+="\n<uds:POSTUREX";
			xmlTxt+=">";
			xmlTxt+=this.Posturex.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:POSTUREX>";
		}
		if (this.Gait!=null){
			xmlTxt+="\n<uds:GAIT";
			xmlTxt+=">";
			xmlTxt+=this.Gait;
			xmlTxt+="</uds:GAIT>";
		}
		if (this.Gaitx!=null){
			xmlTxt+="\n<uds:GAITX";
			xmlTxt+=">";
			xmlTxt+=this.Gaitx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:GAITX>";
		}
		if (this.Posstab!=null){
			xmlTxt+="\n<uds:POSSTAB";
			xmlTxt+=">";
			xmlTxt+=this.Posstab;
			xmlTxt+="</uds:POSSTAB>";
		}
		if (this.Posstabx!=null){
			xmlTxt+="\n<uds:POSSTABX";
			xmlTxt+=">";
			xmlTxt+=this.Posstabx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:POSSTABX>";
		}
		if (this.Bradykin!=null){
			xmlTxt+="\n<uds:BRADYKIN";
			xmlTxt+=">";
			xmlTxt+=this.Bradykin;
			xmlTxt+="</uds:BRADYKIN>";
		}
		if (this.Bradykix!=null){
			xmlTxt+="\n<uds:BRADYKIX";
			xmlTxt+=">";
			xmlTxt+=this.Bradykix.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:BRADYKIX>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Initials!=null) return true;
		if (this.Pdnormal!=null) return true;
		if (this.Speech!=null) return true;
		if (this.Speechx!=null) return true;
		if (this.Facexp!=null) return true;
		if (this.Facexpx!=null) return true;
		if (this.Trestfac!=null) return true;
		if (this.Trestfax!=null) return true;
		if (this.Trestrhd!=null) return true;
		if (this.Trestrhx!=null) return true;
		if (this.Trestlhd!=null) return true;
		if (this.Trestlhx!=null) return true;
		if (this.Trestrft!=null) return true;
		if (this.Trestrfx!=null) return true;
		if (this.Trestlft!=null) return true;
		if (this.Trestlfx!=null) return true;
		if (this.Tractrhd!=null) return true;
		if (this.Tractrhx!=null) return true;
		if (this.Tractlhd!=null) return true;
		if (this.Tractlhx!=null) return true;
		if (this.Rigdneck!=null) return true;
		if (this.Rigdnex!=null) return true;
		if (this.Rigduprt!=null) return true;
		if (this.Rigduprx!=null) return true;
		if (this.Rigduplf!=null) return true;
		if (this.Rigduplx!=null) return true;
		if (this.Rigdlort!=null) return true;
		if (this.Rigdlorx!=null) return true;
		if (this.Rigdlolf!=null) return true;
		if (this.Rigdlolx!=null) return true;
		if (this.Tapsrt!=null) return true;
		if (this.Tapsrtx!=null) return true;
		if (this.Tapslf!=null) return true;
		if (this.Tapslfx!=null) return true;
		if (this.Handmovr!=null) return true;
		if (this.Handmvrx!=null) return true;
		if (this.Handmovl!=null) return true;
		if (this.Handmvlx!=null) return true;
		if (this.Handaltr!=null) return true;
		if (this.Handatrx!=null) return true;
		if (this.Handaltl!=null) return true;
		if (this.Handatlx!=null) return true;
		if (this.Legrt!=null) return true;
		if (this.Legrtx!=null) return true;
		if (this.Leglf!=null) return true;
		if (this.Leglfx!=null) return true;
		if (this.Arising!=null) return true;
		if (this.Arisingx!=null) return true;
		if (this.Posture!=null) return true;
		if (this.Posturex!=null) return true;
		if (this.Gait!=null) return true;
		if (this.Gaitx!=null) return true;
		if (this.Posstab!=null) return true;
		if (this.Posstabx!=null) return true;
		if (this.Bradykin!=null) return true;
		if (this.Bradykix!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
