/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function behavioral_tasksSummaryData_task_run(){
this.xsiType="behavioral:tasksSummaryData_task_run";

	this.getSchemaElementName=function(){
		return "tasksSummaryData_task_run";
	}

	this.getFullSchemaElementName=function(){
		return "behavioral:tasksSummaryData_task_run";
	}
	this.Statistics =null;
	function getStatistics() {
		return this.Statistics;
	}
	this.getStatistics=getStatistics;


	function setStatistics(v){
		this.Statistics =v;
	}
	this.setStatistics=setStatistics;

	this.Statistics_StatisticsBehavioralStatisticsId=null;


	function getStatistics_StatisticsBehavioralStatisticsId(){
		return this.Statistics_StatisticsBehavioralStatisticsId;
	}
	this.getStatistics_StatisticsBehavioralStatisticsId=getStatistics_StatisticsBehavioralStatisticsId;


	function setStatistics_StatisticsBehavioralStatisticsId(v){
		this.Statistics_StatisticsBehavioralStatisticsId=v;
	}
	this.setStatistics_StatisticsBehavioralStatisticsId=setStatistics_StatisticsBehavioralStatisticsId;

	this.Number=null;


	function getNumber() {
		return this.Number;
	}
	this.getNumber=getNumber;


	function setNumber(v){
		this.Number=v;
	}
	this.setNumber=setNumber;

	this.BehavioralTaskssummarydataTaskRunId=null;


	function getBehavioralTaskssummarydataTaskRunId() {
		return this.BehavioralTaskssummarydataTaskRunId;
	}
	this.getBehavioralTaskssummarydataTaskRunId=getBehavioralTaskssummarydataTaskRunId;


	function setBehavioralTaskssummarydataTaskRunId(v){
		this.BehavioralTaskssummarydataTaskRunId=v;
	}
	this.setBehavioralTaskssummarydataTaskRunId=setBehavioralTaskssummarydataTaskRunId;

	this.behavioral_tasksSummaryData_tas_behavioral_taskssummarydata_tas_fk=null;


	this.getbehavioral_tasksSummaryData_tas_behavioral_taskssummarydata_tas=function() {
		return this.behavioral_tasksSummaryData_tas_behavioral_taskssummarydata_tas_fk;
	}


	this.setbehavioral_tasksSummaryData_tas_behavioral_taskssummarydata_tas=function(v){
		this.behavioral_tasksSummaryData_tas_behavioral_taskssummarydata_tas_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="statistics"){
				return this.Statistics ;
			} else 
			if(xmlPath.startsWith("statistics")){
				xmlPath=xmlPath.substring(10);
				if(xmlPath=="")return this.Statistics ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Statistics!=undefined)return this.Statistics.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="number"){
				return this.Number ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="behavioral_tasksSummaryData_task_run_id"){
				return this.BehavioralTaskssummarydataTaskRunId ;
			} else 
			if(xmlPath=="behavioral_tasksSummaryData_tas_behavioral_taskssummarydata_tas"){
				return this.behavioral_tasksSummaryData_tas_behavioral_taskssummarydata_tas_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="statistics"){
				this.Statistics=value;
			} else 
			if(xmlPath.startsWith("statistics")){
				xmlPath=xmlPath.substring(10);
				if(xmlPath=="")return this.Statistics ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Statistics!=undefined){
					this.Statistics.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Statistics= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Statistics= instanciateObject("behavioral:statistics");//omUtils.js
						}
						if(options && options.where)this.Statistics.setProperty(options.where.field,options.where.value);
						this.Statistics.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="number"){
				this.Number=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="behavioral_tasksSummaryData_task_run_id"){
				this.BehavioralTaskssummarydataTaskRunId=value;
			} else 
			if(xmlPath=="behavioral_tasksSummaryData_tas_behavioral_taskssummarydata_tas"){
				this.behavioral_tasksSummaryData_tas_behavioral_taskssummarydata_tas_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="statistics"){
			this.setStatistics(v);
		}
		else{
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="statistics"){
			return "http://nrg.wustl.edu/behavioral:statistics";
		}
		else{
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="statistics"){
			return "field_single_reference";
		}else if (xmlPath=="number"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<behavioral:tasksSummaryData_task_run";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</behavioral:tasksSummaryData_task_run>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.BehavioralTaskssummarydataTaskRunId!=null){
				if(hiddenCount++>0)str+=",";
				str+="behavioral_tasksSummaryData_task_run_id=\"" + this.BehavioralTaskssummarydataTaskRunId + "\"";
			}
			if(this.behavioral_tasksSummaryData_tas_behavioral_taskssummarydata_tas_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="behavioral_tasksSummaryData_tas_behavioral_taskssummarydata_tas=\"" + this.behavioral_tasksSummaryData_tas_behavioral_taskssummarydata_tas_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.Number!=null)
			attTxt+=" number=\"" +this.Number +"\"";
		else attTxt+=" number=\"\"";//REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Statistics!=null){
			xmlTxt+="\n<behavioral:statistics";
			xmlTxt+=this.Statistics.getXMLAtts();
			if(this.Statistics.xsiType!="behavioral:statistics"){
				xmlTxt+=" xsi:type=\"" + this.Statistics.xsiType + "\"";
			}
			if (this.Statistics.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Statistics.getXMLBody(preventComments);
				xmlTxt+="</behavioral:statistics>";
			}else {xmlTxt+="/>";}
		}
		//NOT REQUIRED

		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.BehavioralTaskssummarydataTaskRunId!=null) return true;
			if (this.behavioral_tasksSummaryData_tas_behavioral_taskssummarydata_tas_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Statistics!=null){
			if (this.Statistics.hasXMLBodyContent()) return true;
		}
		//NOT REQUIRED

		if(this.hasXMLComments())return true;
		return false;
	}
}
