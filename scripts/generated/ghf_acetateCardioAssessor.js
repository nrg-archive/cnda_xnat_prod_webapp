/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function ghf_acetateCardioAssessor(){
this.xsiType="ghf:acetateCardioAssessor";

	this.getSchemaElementName=function(){
		return "acetateCardioAssessor";
	}

	this.getFullSchemaElementName=function(){
		return "ghf:acetateCardioAssessor";
	}
this.extension=dynamicJSLoad('xnat_imageAssessorData','generated/xnat_imageAssessorData.js');

	this.Pvscorrectfracs_fbb=null;


	function getPvscorrectfracs_fbb() {
		return this.Pvscorrectfracs_fbb;
	}
	this.getPvscorrectfracs_fbb=getPvscorrectfracs_fbb;


	function setPvscorrectfracs_fbb(v){
		this.Pvscorrectfracs_fbb=v;
	}
	this.setPvscorrectfracs_fbb=setPvscorrectfracs_fbb;

	this.Pvscorrectfracs_fbm=null;


	function getPvscorrectfracs_fbm() {
		return this.Pvscorrectfracs_fbm;
	}
	this.getPvscorrectfracs_fbm=getPvscorrectfracs_fbm;


	function setPvscorrectfracs_fbm(v){
		this.Pvscorrectfracs_fbm=v;
	}
	this.setPvscorrectfracs_fbm=setPvscorrectfracs_fbm;

	this.Pvscorrectfracs_fmb=null;


	function getPvscorrectfracs_fmb() {
		return this.Pvscorrectfracs_fmb;
	}
	this.getPvscorrectfracs_fmb=getPvscorrectfracs_fmb;


	function setPvscorrectfracs_fmb(v){
		this.Pvscorrectfracs_fmb=v;
	}
	this.setPvscorrectfracs_fmb=setPvscorrectfracs_fmb;

	this.Pvscorrectfracs_fmm=null;


	function getPvscorrectfracs_fmm() {
		return this.Pvscorrectfracs_fmm;
	}
	this.getPvscorrectfracs_fmm=getPvscorrectfracs_fmm;


	function setPvscorrectfracs_fmm(v){
		this.Pvscorrectfracs_fmm=v;
	}
	this.setPvscorrectfracs_fmm=setPvscorrectfracs_fmm;

	this.Km_roi1=null;


	function getKm_roi1() {
		return this.Km_roi1;
	}
	this.getKm_roi1=getKm_roi1;


	function setKm_roi1(v){
		this.Km_roi1=v;
	}
	this.setKm_roi1=setKm_roi1;

	this.Km_roi2=null;


	function getKm_roi2() {
		return this.Km_roi2;
	}
	this.getKm_roi2=getKm_roi2;


	function setKm_roi2(v){
		this.Km_roi2=v;
	}
	this.setKm_roi2=setKm_roi2;

	this.Km_roi3=null;


	function getKm_roi3() {
		return this.Km_roi3;
	}
	this.getKm_roi3=getKm_roi3;


	function setKm_roi3(v){
		this.Km_roi3=v;
	}
	this.setKm_roi3=setKm_roi3;

	this.Baselineplasma_gluc=null;


	function getBaselineplasma_gluc() {
		return this.Baselineplasma_gluc;
	}
	this.getBaselineplasma_gluc=getBaselineplasma_gluc;


	function setBaselineplasma_gluc(v){
		this.Baselineplasma_gluc=v;
	}
	this.setBaselineplasma_gluc=setBaselineplasma_gluc;

	this.Baselineplasma_ffa=null;


	function getBaselineplasma_ffa() {
		return this.Baselineplasma_ffa;
	}
	this.getBaselineplasma_ffa=getBaselineplasma_ffa;


	function setBaselineplasma_ffa(v){
		this.Baselineplasma_ffa=v;
	}
	this.setBaselineplasma_ffa=setBaselineplasma_ffa;

	this.Fifteenminplasma_gluc=null;


	function getFifteenminplasma_gluc() {
		return this.Fifteenminplasma_gluc;
	}
	this.getFifteenminplasma_gluc=getFifteenminplasma_gluc;


	function setFifteenminplasma_gluc(v){
		this.Fifteenminplasma_gluc=v;
	}
	this.setFifteenminplasma_gluc=setFifteenminplasma_gluc;

	this.Fifteenminplasma_ffa=null;


	function getFifteenminplasma_ffa() {
		return this.Fifteenminplasma_ffa;
	}
	this.getFifteenminplasma_ffa=getFifteenminplasma_ffa;


	function setFifteenminplasma_ffa(v){
		this.Fifteenminplasma_ffa=v;
	}
	this.setFifteenminplasma_ffa=setFifteenminplasma_ffa;

	this.Baselineheart_sbp=null;


	function getBaselineheart_sbp() {
		return this.Baselineheart_sbp;
	}
	this.getBaselineheart_sbp=getBaselineheart_sbp;


	function setBaselineheart_sbp(v){
		this.Baselineheart_sbp=v;
	}
	this.setBaselineheart_sbp=setBaselineheart_sbp;

	this.Baselineheart_dbp=null;


	function getBaselineheart_dbp() {
		return this.Baselineheart_dbp;
	}
	this.getBaselineheart_dbp=getBaselineheart_dbp;


	function setBaselineheart_dbp(v){
		this.Baselineheart_dbp=v;
	}
	this.setBaselineheart_dbp=setBaselineheart_dbp;

	this.Baselineheart_hr=null;


	function getBaselineheart_hr() {
		return this.Baselineheart_hr;
	}
	this.getBaselineheart_hr=getBaselineheart_hr;


	function setBaselineheart_hr(v){
		this.Baselineheart_hr=v;
	}
	this.setBaselineheart_hr=setBaselineheart_hr;

	this.Fifteenminheart_sbp=null;


	function getFifteenminheart_sbp() {
		return this.Fifteenminheart_sbp;
	}
	this.getFifteenminheart_sbp=getFifteenminheart_sbp;


	function setFifteenminheart_sbp(v){
		this.Fifteenminheart_sbp=v;
	}
	this.setFifteenminheart_sbp=setFifteenminheart_sbp;

	this.Fifteenminheart_dbp=null;


	function getFifteenminheart_dbp() {
		return this.Fifteenminheart_dbp;
	}
	this.getFifteenminheart_dbp=getFifteenminheart_dbp;


	function setFifteenminheart_dbp(v){
		this.Fifteenminheart_dbp=v;
	}
	this.setFifteenminheart_dbp=setFifteenminheart_dbp;

	this.Fifteenminheart_hr=null;


	function getFifteenminheart_hr() {
		return this.Fifteenminheart_hr;
	}
	this.getFifteenminheart_hr=getFifteenminheart_hr;


	function setFifteenminheart_hr(v){
		this.Fifteenminheart_hr=v;
	}
	this.setFifteenminheart_hr=setFifteenminheart_hr;

	this.Thirtyminheart_sbp=null;


	function getThirtyminheart_sbp() {
		return this.Thirtyminheart_sbp;
	}
	this.getThirtyminheart_sbp=getThirtyminheart_sbp;


	function setThirtyminheart_sbp(v){
		this.Thirtyminheart_sbp=v;
	}
	this.setThirtyminheart_sbp=setThirtyminheart_sbp;

	this.Thirtyminheart_dbp=null;


	function getThirtyminheart_dbp() {
		return this.Thirtyminheart_dbp;
	}
	this.getThirtyminheart_dbp=getThirtyminheart_dbp;


	function setThirtyminheart_dbp(v){
		this.Thirtyminheart_dbp=v;
	}
	this.setThirtyminheart_dbp=setThirtyminheart_dbp;

	this.Thirtyminheart_hr=null;


	function getThirtyminheart_hr() {
		return this.Thirtyminheart_hr;
	}
	this.getThirtyminheart_hr=getThirtyminheart_hr;


	function setThirtyminheart_hr(v){
		this.Thirtyminheart_hr=v;
	}
	this.setThirtyminheart_hr=setThirtyminheart_hr;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="imageAssessorData"){
				return this.Imageassessordata ;
			} else 
			if(xmlPath.startsWith("imageAssessorData")){
				xmlPath=xmlPath.substring(17);
				if(xmlPath=="")return this.Imageassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Imageassessordata!=undefined)return this.Imageassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="pvsCorrectFracs/fbb"){
				return this.Pvscorrectfracs_fbb ;
			} else 
			if(xmlPath=="pvsCorrectFracs/fbm"){
				return this.Pvscorrectfracs_fbm ;
			} else 
			if(xmlPath=="pvsCorrectFracs/fmb"){
				return this.Pvscorrectfracs_fmb ;
			} else 
			if(xmlPath=="pvsCorrectFracs/fmm"){
				return this.Pvscorrectfracs_fmm ;
			} else 
			if(xmlPath=="km/roi1"){
				return this.Km_roi1 ;
			} else 
			if(xmlPath=="km/roi2"){
				return this.Km_roi2 ;
			} else 
			if(xmlPath=="km/roi3"){
				return this.Km_roi3 ;
			} else 
			if(xmlPath=="baselinePlasma/gluc"){
				return this.Baselineplasma_gluc ;
			} else 
			if(xmlPath=="baselinePlasma/ffa"){
				return this.Baselineplasma_ffa ;
			} else 
			if(xmlPath=="fifteenMinPlasma/gluc"){
				return this.Fifteenminplasma_gluc ;
			} else 
			if(xmlPath=="fifteenMinPlasma/ffa"){
				return this.Fifteenminplasma_ffa ;
			} else 
			if(xmlPath=="baselineHeart/sbp"){
				return this.Baselineheart_sbp ;
			} else 
			if(xmlPath=="baselineHeart/dbp"){
				return this.Baselineheart_dbp ;
			} else 
			if(xmlPath=="baselineHeart/hr"){
				return this.Baselineheart_hr ;
			} else 
			if(xmlPath=="fifteenMinHeart/sbp"){
				return this.Fifteenminheart_sbp ;
			} else 
			if(xmlPath=="fifteenMinHeart/dbp"){
				return this.Fifteenminheart_dbp ;
			} else 
			if(xmlPath=="fifteenMinHeart/hr"){
				return this.Fifteenminheart_hr ;
			} else 
			if(xmlPath=="thirtyMinHeart/sbp"){
				return this.Thirtyminheart_sbp ;
			} else 
			if(xmlPath=="thirtyMinHeart/dbp"){
				return this.Thirtyminheart_dbp ;
			} else 
			if(xmlPath=="thirtyMinHeart/hr"){
				return this.Thirtyminheart_hr ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="imageAssessorData"){
				this.Imageassessordata=value;
			} else 
			if(xmlPath.startsWith("imageAssessorData")){
				xmlPath=xmlPath.substring(17);
				if(xmlPath=="")return this.Imageassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Imageassessordata!=undefined){
					this.Imageassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Imageassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Imageassessordata= instanciateObject("xnat:imageAssessorData");//omUtils.js
						}
						if(options && options.where)this.Imageassessordata.setProperty(options.where.field,options.where.value);
						this.Imageassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="pvsCorrectFracs/fbb"){
				this.Pvscorrectfracs_fbb=value;
			} else 
			if(xmlPath=="pvsCorrectFracs/fbm"){
				this.Pvscorrectfracs_fbm=value;
			} else 
			if(xmlPath=="pvsCorrectFracs/fmb"){
				this.Pvscorrectfracs_fmb=value;
			} else 
			if(xmlPath=="pvsCorrectFracs/fmm"){
				this.Pvscorrectfracs_fmm=value;
			} else 
			if(xmlPath=="km/roi1"){
				this.Km_roi1=value;
			} else 
			if(xmlPath=="km/roi2"){
				this.Km_roi2=value;
			} else 
			if(xmlPath=="km/roi3"){
				this.Km_roi3=value;
			} else 
			if(xmlPath=="baselinePlasma/gluc"){
				this.Baselineplasma_gluc=value;
			} else 
			if(xmlPath=="baselinePlasma/ffa"){
				this.Baselineplasma_ffa=value;
			} else 
			if(xmlPath=="fifteenMinPlasma/gluc"){
				this.Fifteenminplasma_gluc=value;
			} else 
			if(xmlPath=="fifteenMinPlasma/ffa"){
				this.Fifteenminplasma_ffa=value;
			} else 
			if(xmlPath=="baselineHeart/sbp"){
				this.Baselineheart_sbp=value;
			} else 
			if(xmlPath=="baselineHeart/dbp"){
				this.Baselineheart_dbp=value;
			} else 
			if(xmlPath=="baselineHeart/hr"){
				this.Baselineheart_hr=value;
			} else 
			if(xmlPath=="fifteenMinHeart/sbp"){
				this.Fifteenminheart_sbp=value;
			} else 
			if(xmlPath=="fifteenMinHeart/dbp"){
				this.Fifteenminheart_dbp=value;
			} else 
			if(xmlPath=="fifteenMinHeart/hr"){
				this.Fifteenminheart_hr=value;
			} else 
			if(xmlPath=="thirtyMinHeart/sbp"){
				this.Thirtyminheart_sbp=value;
			} else 
			if(xmlPath=="thirtyMinHeart/dbp"){
				this.Thirtyminheart_dbp=value;
			} else 
			if(xmlPath=="thirtyMinHeart/hr"){
				this.Thirtyminheart_hr=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="pvsCorrectFracs/fbb"){
			return "field_data";
		}else if (xmlPath=="pvsCorrectFracs/fbm"){
			return "field_data";
		}else if (xmlPath=="pvsCorrectFracs/fmb"){
			return "field_data";
		}else if (xmlPath=="pvsCorrectFracs/fmm"){
			return "field_data";
		}else if (xmlPath=="km/roi1"){
			return "field_data";
		}else if (xmlPath=="km/roi2"){
			return "field_data";
		}else if (xmlPath=="km/roi3"){
			return "field_data";
		}else if (xmlPath=="baselinePlasma/gluc"){
			return "field_data";
		}else if (xmlPath=="baselinePlasma/ffa"){
			return "field_data";
		}else if (xmlPath=="fifteenMinPlasma/gluc"){
			return "field_data";
		}else if (xmlPath=="fifteenMinPlasma/ffa"){
			return "field_data";
		}else if (xmlPath=="baselineHeart/sbp"){
			return "field_data";
		}else if (xmlPath=="baselineHeart/dbp"){
			return "field_data";
		}else if (xmlPath=="baselineHeart/hr"){
			return "field_data";
		}else if (xmlPath=="fifteenMinHeart/sbp"){
			return "field_data";
		}else if (xmlPath=="fifteenMinHeart/dbp"){
			return "field_data";
		}else if (xmlPath=="fifteenMinHeart/hr"){
			return "field_data";
		}else if (xmlPath=="thirtyMinHeart/sbp"){
			return "field_data";
		}else if (xmlPath=="thirtyMinHeart/dbp"){
			return "field_data";
		}else if (xmlPath=="thirtyMinHeart/hr"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<ghf:AcetateCardioAssessor";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</ghf:AcetateCardioAssessor>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
			var child0=0;
			var att0=0;
			if(this.Pvscorrectfracs_fmm!=null)
			child0++;
			if(this.Pvscorrectfracs_fmb!=null)
			child0++;
			if(this.Pvscorrectfracs_fbm!=null)
			child0++;
			if(this.Pvscorrectfracs_fbb!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<ghf:pvsCorrectFracs";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Pvscorrectfracs_fbb!=null){
			xmlTxt+="\n<ghf:fbb";
			xmlTxt+=">";
			xmlTxt+=this.Pvscorrectfracs_fbb;
			xmlTxt+="</ghf:fbb>";
		}
		if (this.Pvscorrectfracs_fbm!=null){
			xmlTxt+="\n<ghf:fbm";
			xmlTxt+=">";
			xmlTxt+=this.Pvscorrectfracs_fbm;
			xmlTxt+="</ghf:fbm>";
		}
		if (this.Pvscorrectfracs_fmb!=null){
			xmlTxt+="\n<ghf:fmb";
			xmlTxt+=">";
			xmlTxt+=this.Pvscorrectfracs_fmb;
			xmlTxt+="</ghf:fmb>";
		}
		if (this.Pvscorrectfracs_fmm!=null){
			xmlTxt+="\n<ghf:fmm";
			xmlTxt+=">";
			xmlTxt+=this.Pvscorrectfracs_fmm;
			xmlTxt+="</ghf:fmm>";
		}
				xmlTxt+="\n</ghf:pvsCorrectFracs>";
			}
			}

			var child1=0;
			var att1=0;
			if(this.Km_roi3!=null)
			child1++;
			if(this.Km_roi2!=null)
			child1++;
			if(this.Km_roi1!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<ghf:km";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Km_roi1!=null){
			xmlTxt+="\n<ghf:roi1";
			xmlTxt+=">";
			xmlTxt+=this.Km_roi1;
			xmlTxt+="</ghf:roi1>";
		}
		if (this.Km_roi2!=null){
			xmlTxt+="\n<ghf:roi2";
			xmlTxt+=">";
			xmlTxt+=this.Km_roi2;
			xmlTxt+="</ghf:roi2>";
		}
		if (this.Km_roi3!=null){
			xmlTxt+="\n<ghf:roi3";
			xmlTxt+=">";
			xmlTxt+=this.Km_roi3;
			xmlTxt+="</ghf:roi3>";
		}
				xmlTxt+="\n</ghf:km>";
			}
			}

			var child2=0;
			var att2=0;
			if(this.Baselineplasma_gluc!=null)
			child2++;
			if(this.Baselineplasma_ffa!=null)
			child2++;
			if(child2>0 || att2>0){
				xmlTxt+="\n<ghf:baselinePlasma";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Baselineplasma_gluc!=null){
			xmlTxt+="\n<ghf:gluc";
			xmlTxt+=">";
			xmlTxt+=this.Baselineplasma_gluc;
			xmlTxt+="</ghf:gluc>";
		}
		if (this.Baselineplasma_ffa!=null){
			xmlTxt+="\n<ghf:ffa";
			xmlTxt+=">";
			xmlTxt+=this.Baselineplasma_ffa;
			xmlTxt+="</ghf:ffa>";
		}
				xmlTxt+="\n</ghf:baselinePlasma>";
			}
			}

			var child3=0;
			var att3=0;
			if(this.Fifteenminplasma_gluc!=null)
			child3++;
			if(this.Fifteenminplasma_ffa!=null)
			child3++;
			if(child3>0 || att3>0){
				xmlTxt+="\n<ghf:fifteenMinPlasma";
			if(child3==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Fifteenminplasma_gluc!=null){
			xmlTxt+="\n<ghf:gluc";
			xmlTxt+=">";
			xmlTxt+=this.Fifteenminplasma_gluc;
			xmlTxt+="</ghf:gluc>";
		}
		if (this.Fifteenminplasma_ffa!=null){
			xmlTxt+="\n<ghf:ffa";
			xmlTxt+=">";
			xmlTxt+=this.Fifteenminplasma_ffa;
			xmlTxt+="</ghf:ffa>";
		}
				xmlTxt+="\n</ghf:fifteenMinPlasma>";
			}
			}

			var child4=0;
			var att4=0;
			if(this.Baselineheart_dbp!=null)
			child4++;
			if(this.Baselineheart_hr!=null)
			child4++;
			if(this.Baselineheart_sbp!=null)
			child4++;
			if(child4>0 || att4>0){
				xmlTxt+="\n<ghf:baselineHeart";
			if(child4==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Baselineheart_sbp!=null){
			xmlTxt+="\n<ghf:sbp";
			xmlTxt+=">";
			xmlTxt+=this.Baselineheart_sbp;
			xmlTxt+="</ghf:sbp>";
		}
		if (this.Baselineheart_dbp!=null){
			xmlTxt+="\n<ghf:dbp";
			xmlTxt+=">";
			xmlTxt+=this.Baselineheart_dbp;
			xmlTxt+="</ghf:dbp>";
		}
		if (this.Baselineheart_hr!=null){
			xmlTxt+="\n<ghf:hr";
			xmlTxt+=">";
			xmlTxt+=this.Baselineheart_hr;
			xmlTxt+="</ghf:hr>";
		}
				xmlTxt+="\n</ghf:baselineHeart>";
			}
			}

			var child5=0;
			var att5=0;
			if(this.Fifteenminheart_dbp!=null)
			child5++;
			if(this.Fifteenminheart_hr!=null)
			child5++;
			if(this.Fifteenminheart_sbp!=null)
			child5++;
			if(child5>0 || att5>0){
				xmlTxt+="\n<ghf:fifteenMinHeart";
			if(child5==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Fifteenminheart_sbp!=null){
			xmlTxt+="\n<ghf:sbp";
			xmlTxt+=">";
			xmlTxt+=this.Fifteenminheart_sbp;
			xmlTxt+="</ghf:sbp>";
		}
		if (this.Fifteenminheart_dbp!=null){
			xmlTxt+="\n<ghf:dbp";
			xmlTxt+=">";
			xmlTxt+=this.Fifteenminheart_dbp;
			xmlTxt+="</ghf:dbp>";
		}
		if (this.Fifteenminheart_hr!=null){
			xmlTxt+="\n<ghf:hr";
			xmlTxt+=">";
			xmlTxt+=this.Fifteenminheart_hr;
			xmlTxt+="</ghf:hr>";
		}
				xmlTxt+="\n</ghf:fifteenMinHeart>";
			}
			}

			var child6=0;
			var att6=0;
			if(this.Thirtyminheart_sbp!=null)
			child6++;
			if(this.Thirtyminheart_dbp!=null)
			child6++;
			if(this.Thirtyminheart_hr!=null)
			child6++;
			if(child6>0 || att6>0){
				xmlTxt+="\n<ghf:thirtyMinHeart";
			if(child6==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Thirtyminheart_sbp!=null){
			xmlTxt+="\n<ghf:sbp";
			xmlTxt+=">";
			xmlTxt+=this.Thirtyminheart_sbp;
			xmlTxt+="</ghf:sbp>";
		}
		if (this.Thirtyminheart_dbp!=null){
			xmlTxt+="\n<ghf:dbp";
			xmlTxt+=">";
			xmlTxt+=this.Thirtyminheart_dbp;
			xmlTxt+="</ghf:dbp>";
		}
		if (this.Thirtyminheart_hr!=null){
			xmlTxt+="\n<ghf:hr";
			xmlTxt+=">";
			xmlTxt+=this.Thirtyminheart_hr;
			xmlTxt+="</ghf:hr>";
		}
				xmlTxt+="\n</ghf:thirtyMinHeart>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
			if(this.Pvscorrectfracs_fmm!=null) return true;
			if(this.Pvscorrectfracs_fmb!=null) return true;
			if(this.Pvscorrectfracs_fbm!=null) return true;
			if(this.Pvscorrectfracs_fbb!=null) return true;
			if(this.Km_roi3!=null) return true;
			if(this.Km_roi2!=null) return true;
			if(this.Km_roi1!=null) return true;
			if(this.Baselineplasma_gluc!=null) return true;
			if(this.Baselineplasma_ffa!=null) return true;
			if(this.Fifteenminplasma_gluc!=null) return true;
			if(this.Fifteenminplasma_ffa!=null) return true;
			if(this.Baselineheart_dbp!=null) return true;
			if(this.Baselineheart_hr!=null) return true;
			if(this.Baselineheart_sbp!=null) return true;
			if(this.Fifteenminheart_dbp!=null) return true;
			if(this.Fifteenminheart_hr!=null) return true;
			if(this.Fifteenminheart_sbp!=null) return true;
			if(this.Thirtyminheart_sbp!=null) return true;
			if(this.Thirtyminheart_dbp!=null) return true;
			if(this.Thirtyminheart_hr!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
