/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:04 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function uds_b9clinjdgData(){
this.xsiType="uds:b9clinjdgData";

	this.getSchemaElementName=function(){
		return "b9clinjdgData";
	}

	this.getFullSchemaElementName=function(){
		return "uds:b9clinjdgData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.B9chg=null;


	function getB9chg() {
		return this.B9chg;
	}
	this.getB9chg=getB9chg;


	function setB9chg(v){
		this.B9chg=v;
	}
	this.setB9chg=setB9chg;

	this.Decsub=null;


	function getDecsub() {
		return this.Decsub;
	}
	this.getDecsub=getDecsub;


	function setDecsub(v){
		this.Decsub=v;
	}
	this.setDecsub=setDecsub;

	this.Decin=null;


	function getDecin() {
		return this.Decin;
	}
	this.getDecin=getDecin;


	function setDecin(v){
		this.Decin=v;
	}
	this.setDecin=setDecin;

	this.Decclin=null;


	function getDecclin() {
		return this.Decclin;
	}
	this.getDecclin=getDecclin;


	function setDecclin(v){
		this.Decclin=v;
	}
	this.setDecclin=setDecclin;

	this.Decage=null;


	function getDecage() {
		return this.Decage;
	}
	this.getDecage=getDecage;


	function setDecage(v){
		this.Decage=v;
	}
	this.setDecage=setDecage;

	this.Cogmem=null;


	function getCogmem() {
		return this.Cogmem;
	}
	this.getCogmem=getCogmem;


	function setCogmem(v){
		this.Cogmem=v;
	}
	this.setCogmem=setCogmem;

	this.Cogjudg=null;


	function getCogjudg() {
		return this.Cogjudg;
	}
	this.getCogjudg=getCogjudg;


	function setCogjudg(v){
		this.Cogjudg=v;
	}
	this.setCogjudg=setCogjudg;

	this.Coglang=null;


	function getCoglang() {
		return this.Coglang;
	}
	this.getCoglang=getCoglang;


	function setCoglang(v){
		this.Coglang=v;
	}
	this.setCoglang=setCoglang;

	this.Cogvis=null;


	function getCogvis() {
		return this.Cogvis;
	}
	this.getCogvis=getCogvis;


	function setCogvis(v){
		this.Cogvis=v;
	}
	this.setCogvis=setCogvis;

	this.Cogattn=null;


	function getCogattn() {
		return this.Cogattn;
	}
	this.getCogattn=getCogattn;


	function setCogattn(v){
		this.Cogattn=v;
	}
	this.setCogattn=setCogattn;

	this.Cogfluc=null;


	function getCogfluc() {
		return this.Cogfluc;
	}
	this.getCogfluc=getCogfluc;


	function setCogfluc(v){
		this.Cogfluc=v;
	}
	this.setCogfluc=setCogfluc;

	this.Cogothr=null;


	function getCogothr() {
		return this.Cogothr;
	}
	this.getCogothr=getCogothr;


	function setCogothr(v){
		this.Cogothr=v;
	}
	this.setCogothr=setCogothr;

	this.Cogothrx=null;


	function getCogothrx() {
		return this.Cogothrx;
	}
	this.getCogothrx=getCogothrx;


	function setCogothrx(v){
		this.Cogothrx=v;
	}
	this.setCogothrx=setCogothrx;

	this.Cogfrst=null;


	function getCogfrst() {
		return this.Cogfrst;
	}
	this.getCogfrst=getCogfrst;


	function setCogfrst(v){
		this.Cogfrst=v;
	}
	this.setCogfrst=setCogfrst;

	this.Cogfrstx=null;


	function getCogfrstx() {
		return this.Cogfrstx;
	}
	this.getCogfrstx=getCogfrstx;


	function setCogfrstx(v){
		this.Cogfrstx=v;
	}
	this.setCogfrstx=setCogfrstx;

	this.Cogmode=null;


	function getCogmode() {
		return this.Cogmode;
	}
	this.getCogmode=getCogmode;


	function setCogmode(v){
		this.Cogmode=v;
	}
	this.setCogmode=setCogmode;

	this.Cogmodex=null;


	function getCogmodex() {
		return this.Cogmodex;
	}
	this.getCogmodex=getCogmodex;


	function setCogmodex(v){
		this.Cogmodex=v;
	}
	this.setCogmodex=setCogmodex;

	this.Beapathy=null;


	function getBeapathy() {
		return this.Beapathy;
	}
	this.getBeapathy=getBeapathy;


	function setBeapathy(v){
		this.Beapathy=v;
	}
	this.setBeapathy=setBeapathy;

	this.Bedep=null;


	function getBedep() {
		return this.Bedep;
	}
	this.getBedep=getBedep;


	function setBedep(v){
		this.Bedep=v;
	}
	this.setBedep=setBedep;

	this.Bevhall=null;


	function getBevhall() {
		return this.Bevhall;
	}
	this.getBevhall=getBevhall;


	function setBevhall(v){
		this.Bevhall=v;
	}
	this.setBevhall=setBevhall;

	this.Bevwell=null;


	function getBevwell() {
		return this.Bevwell;
	}
	this.getBevwell=getBevwell;


	function setBevwell(v){
		this.Bevwell=v;
	}
	this.setBevwell=setBevwell;

	this.Beahall=null;


	function getBeahall() {
		return this.Beahall;
	}
	this.getBeahall=getBeahall;


	function setBeahall(v){
		this.Beahall=v;
	}
	this.setBeahall=setBeahall;

	this.Bedel=null;


	function getBedel() {
		return this.Bedel;
	}
	this.getBedel=getBedel;


	function setBedel(v){
		this.Bedel=v;
	}
	this.setBedel=setBedel;

	this.Bedisin=null;


	function getBedisin() {
		return this.Bedisin;
	}
	this.getBedisin=getBedisin;


	function setBedisin(v){
		this.Bedisin=v;
	}
	this.setBedisin=setBedisin;

	this.Beirrit=null;


	function getBeirrit() {
		return this.Beirrit;
	}
	this.getBeirrit=getBeirrit;


	function setBeirrit(v){
		this.Beirrit=v;
	}
	this.setBeirrit=setBeirrit;

	this.Beagit=null;


	function getBeagit() {
		return this.Beagit;
	}
	this.getBeagit=getBeagit;


	function setBeagit(v){
		this.Beagit=v;
	}
	this.setBeagit=setBeagit;

	this.Beperch=null;


	function getBeperch() {
		return this.Beperch;
	}
	this.getBeperch=getBeperch;


	function setBeperch(v){
		this.Beperch=v;
	}
	this.setBeperch=setBeperch;

	this.Berem=null;


	function getBerem() {
		return this.Berem;
	}
	this.getBerem=getBerem;


	function setBerem(v){
		this.Berem=v;
	}
	this.setBerem=setBerem;

	this.Beothr=null;


	function getBeothr() {
		return this.Beothr;
	}
	this.getBeothr=getBeothr;


	function setBeothr(v){
		this.Beothr=v;
	}
	this.setBeothr=setBeothr;

	this.Beothrx=null;


	function getBeothrx() {
		return this.Beothrx;
	}
	this.getBeothrx=getBeothrx;


	function setBeothrx(v){
		this.Beothrx=v;
	}
	this.setBeothrx=setBeothrx;

	this.Befrst=null;


	function getBefrst() {
		return this.Befrst;
	}
	this.getBefrst=getBefrst;


	function setBefrst(v){
		this.Befrst=v;
	}
	this.setBefrst=setBefrst;

	this.Befrstx=null;


	function getBefrstx() {
		return this.Befrstx;
	}
	this.getBefrstx=getBefrstx;


	function setBefrstx(v){
		this.Befrstx=v;
	}
	this.setBefrstx=setBefrstx;

	this.Bemode=null;


	function getBemode() {
		return this.Bemode;
	}
	this.getBemode=getBemode;


	function setBemode(v){
		this.Bemode=v;
	}
	this.setBemode=setBemode;

	this.Bemodex=null;


	function getBemodex() {
		return this.Bemodex;
	}
	this.getBemodex=getBemodex;


	function setBemodex(v){
		this.Bemodex=v;
	}
	this.setBemodex=setBemodex;

	this.Mogait=null;


	function getMogait() {
		return this.Mogait;
	}
	this.getMogait=getMogait;


	function setMogait(v){
		this.Mogait=v;
	}
	this.setMogait=setMogait;

	this.Mofalls=null;


	function getMofalls() {
		return this.Mofalls;
	}
	this.getMofalls=getMofalls;


	function setMofalls(v){
		this.Mofalls=v;
	}
	this.setMofalls=setMofalls;

	this.Motrem=null;


	function getMotrem() {
		return this.Motrem;
	}
	this.getMotrem=getMotrem;


	function setMotrem(v){
		this.Motrem=v;
	}
	this.setMotrem=setMotrem;

	this.Moslow=null;


	function getMoslow() {
		return this.Moslow;
	}
	this.getMoslow=getMoslow;


	function setMoslow(v){
		this.Moslow=v;
	}
	this.setMoslow=setMoslow;

	this.Mofrst=null;


	function getMofrst() {
		return this.Mofrst;
	}
	this.getMofrst=getMofrst;


	function setMofrst(v){
		this.Mofrst=v;
	}
	this.setMofrst=setMofrst;

	this.Momode=null;


	function getMomode() {
		return this.Momode;
	}
	this.getMomode=getMomode;


	function setMomode(v){
		this.Momode=v;
	}
	this.setMomode=setMomode;

	this.Momodex=null;


	function getMomodex() {
		return this.Momodex;
	}
	this.getMomodex=getMomodex;


	function setMomodex(v){
		this.Momodex=v;
	}
	this.setMomodex=setMomodex;

	this.Momopark=null;


	function getMomopark() {
		return this.Momopark;
	}
	this.getMomopark=getMomopark;


	function setMomopark(v){
		this.Momopark=v;
	}
	this.setMomopark=setMomopark;

	this.Course=null;


	function getCourse() {
		return this.Course;
	}
	this.getCourse=getCourse;


	function setCourse(v){
		this.Course=v;
	}
	this.setCourse=setCourse;

	this.Frstchg=null;


	function getFrstchg() {
		return this.Frstchg;
	}
	this.getFrstchg=getFrstchg;


	function setFrstchg(v){
		this.Frstchg=v;
	}
	this.setFrstchg=setFrstchg;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="B9CHG"){
				return this.B9chg ;
			} else 
			if(xmlPath=="DECSUB"){
				return this.Decsub ;
			} else 
			if(xmlPath=="DECIN"){
				return this.Decin ;
			} else 
			if(xmlPath=="DECCLIN"){
				return this.Decclin ;
			} else 
			if(xmlPath=="DECAGE"){
				return this.Decage ;
			} else 
			if(xmlPath=="COGMEM"){
				return this.Cogmem ;
			} else 
			if(xmlPath=="COGJUDG"){
				return this.Cogjudg ;
			} else 
			if(xmlPath=="COGLANG"){
				return this.Coglang ;
			} else 
			if(xmlPath=="COGVIS"){
				return this.Cogvis ;
			} else 
			if(xmlPath=="COGATTN"){
				return this.Cogattn ;
			} else 
			if(xmlPath=="COGFLUC"){
				return this.Cogfluc ;
			} else 
			if(xmlPath=="COGOTHR"){
				return this.Cogothr ;
			} else 
			if(xmlPath=="COGOTHRX"){
				return this.Cogothrx ;
			} else 
			if(xmlPath=="COGFRST"){
				return this.Cogfrst ;
			} else 
			if(xmlPath=="COGFRSTX"){
				return this.Cogfrstx ;
			} else 
			if(xmlPath=="COGMODE"){
				return this.Cogmode ;
			} else 
			if(xmlPath=="COGMODEX"){
				return this.Cogmodex ;
			} else 
			if(xmlPath=="BEAPATHY"){
				return this.Beapathy ;
			} else 
			if(xmlPath=="BEDEP"){
				return this.Bedep ;
			} else 
			if(xmlPath=="BEVHALL"){
				return this.Bevhall ;
			} else 
			if(xmlPath=="BEVWELL"){
				return this.Bevwell ;
			} else 
			if(xmlPath=="BEAHALL"){
				return this.Beahall ;
			} else 
			if(xmlPath=="BEDEL"){
				return this.Bedel ;
			} else 
			if(xmlPath=="BEDISIN"){
				return this.Bedisin ;
			} else 
			if(xmlPath=="BEIRRIT"){
				return this.Beirrit ;
			} else 
			if(xmlPath=="BEAGIT"){
				return this.Beagit ;
			} else 
			if(xmlPath=="BEPERCH"){
				return this.Beperch ;
			} else 
			if(xmlPath=="BEREM"){
				return this.Berem ;
			} else 
			if(xmlPath=="BEOTHR"){
				return this.Beothr ;
			} else 
			if(xmlPath=="BEOTHRX"){
				return this.Beothrx ;
			} else 
			if(xmlPath=="BEFRST"){
				return this.Befrst ;
			} else 
			if(xmlPath=="BEFRSTX"){
				return this.Befrstx ;
			} else 
			if(xmlPath=="BEMODE"){
				return this.Bemode ;
			} else 
			if(xmlPath=="BEMODEX"){
				return this.Bemodex ;
			} else 
			if(xmlPath=="MOGAIT"){
				return this.Mogait ;
			} else 
			if(xmlPath=="MOFALLS"){
				return this.Mofalls ;
			} else 
			if(xmlPath=="MOTREM"){
				return this.Motrem ;
			} else 
			if(xmlPath=="MOSLOW"){
				return this.Moslow ;
			} else 
			if(xmlPath=="MOFRST"){
				return this.Mofrst ;
			} else 
			if(xmlPath=="MOMODE"){
				return this.Momode ;
			} else 
			if(xmlPath=="MOMODEX"){
				return this.Momodex ;
			} else 
			if(xmlPath=="MOMOPARK"){
				return this.Momopark ;
			} else 
			if(xmlPath=="COURSE"){
				return this.Course ;
			} else 
			if(xmlPath=="FRSTCHG"){
				return this.Frstchg ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="B9CHG"){
				this.B9chg=value;
			} else 
			if(xmlPath=="DECSUB"){
				this.Decsub=value;
			} else 
			if(xmlPath=="DECIN"){
				this.Decin=value;
			} else 
			if(xmlPath=="DECCLIN"){
				this.Decclin=value;
			} else 
			if(xmlPath=="DECAGE"){
				this.Decage=value;
			} else 
			if(xmlPath=="COGMEM"){
				this.Cogmem=value;
			} else 
			if(xmlPath=="COGJUDG"){
				this.Cogjudg=value;
			} else 
			if(xmlPath=="COGLANG"){
				this.Coglang=value;
			} else 
			if(xmlPath=="COGVIS"){
				this.Cogvis=value;
			} else 
			if(xmlPath=="COGATTN"){
				this.Cogattn=value;
			} else 
			if(xmlPath=="COGFLUC"){
				this.Cogfluc=value;
			} else 
			if(xmlPath=="COGOTHR"){
				this.Cogothr=value;
			} else 
			if(xmlPath=="COGOTHRX"){
				this.Cogothrx=value;
			} else 
			if(xmlPath=="COGFRST"){
				this.Cogfrst=value;
			} else 
			if(xmlPath=="COGFRSTX"){
				this.Cogfrstx=value;
			} else 
			if(xmlPath=="COGMODE"){
				this.Cogmode=value;
			} else 
			if(xmlPath=="COGMODEX"){
				this.Cogmodex=value;
			} else 
			if(xmlPath=="BEAPATHY"){
				this.Beapathy=value;
			} else 
			if(xmlPath=="BEDEP"){
				this.Bedep=value;
			} else 
			if(xmlPath=="BEVHALL"){
				this.Bevhall=value;
			} else 
			if(xmlPath=="BEVWELL"){
				this.Bevwell=value;
			} else 
			if(xmlPath=="BEAHALL"){
				this.Beahall=value;
			} else 
			if(xmlPath=="BEDEL"){
				this.Bedel=value;
			} else 
			if(xmlPath=="BEDISIN"){
				this.Bedisin=value;
			} else 
			if(xmlPath=="BEIRRIT"){
				this.Beirrit=value;
			} else 
			if(xmlPath=="BEAGIT"){
				this.Beagit=value;
			} else 
			if(xmlPath=="BEPERCH"){
				this.Beperch=value;
			} else 
			if(xmlPath=="BEREM"){
				this.Berem=value;
			} else 
			if(xmlPath=="BEOTHR"){
				this.Beothr=value;
			} else 
			if(xmlPath=="BEOTHRX"){
				this.Beothrx=value;
			} else 
			if(xmlPath=="BEFRST"){
				this.Befrst=value;
			} else 
			if(xmlPath=="BEFRSTX"){
				this.Befrstx=value;
			} else 
			if(xmlPath=="BEMODE"){
				this.Bemode=value;
			} else 
			if(xmlPath=="BEMODEX"){
				this.Bemodex=value;
			} else 
			if(xmlPath=="MOGAIT"){
				this.Mogait=value;
			} else 
			if(xmlPath=="MOFALLS"){
				this.Mofalls=value;
			} else 
			if(xmlPath=="MOTREM"){
				this.Motrem=value;
			} else 
			if(xmlPath=="MOSLOW"){
				this.Moslow=value;
			} else 
			if(xmlPath=="MOFRST"){
				this.Mofrst=value;
			} else 
			if(xmlPath=="MOMODE"){
				this.Momode=value;
			} else 
			if(xmlPath=="MOMODEX"){
				this.Momodex=value;
			} else 
			if(xmlPath=="MOMOPARK"){
				this.Momopark=value;
			} else 
			if(xmlPath=="COURSE"){
				this.Course=value;
			} else 
			if(xmlPath=="FRSTCHG"){
				this.Frstchg=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="B9CHG"){
			return "field_data";
		}else if (xmlPath=="DECSUB"){
			return "field_data";
		}else if (xmlPath=="DECIN"){
			return "field_data";
		}else if (xmlPath=="DECCLIN"){
			return "field_data";
		}else if (xmlPath=="DECAGE"){
			return "field_data";
		}else if (xmlPath=="COGMEM"){
			return "field_data";
		}else if (xmlPath=="COGJUDG"){
			return "field_data";
		}else if (xmlPath=="COGLANG"){
			return "field_data";
		}else if (xmlPath=="COGVIS"){
			return "field_data";
		}else if (xmlPath=="COGATTN"){
			return "field_data";
		}else if (xmlPath=="COGFLUC"){
			return "field_data";
		}else if (xmlPath=="COGOTHR"){
			return "field_data";
		}else if (xmlPath=="COGOTHRX"){
			return "field_data";
		}else if (xmlPath=="COGFRST"){
			return "field_data";
		}else if (xmlPath=="COGFRSTX"){
			return "field_data";
		}else if (xmlPath=="COGMODE"){
			return "field_data";
		}else if (xmlPath=="COGMODEX"){
			return "field_data";
		}else if (xmlPath=="BEAPATHY"){
			return "field_data";
		}else if (xmlPath=="BEDEP"){
			return "field_data";
		}else if (xmlPath=="BEVHALL"){
			return "field_data";
		}else if (xmlPath=="BEVWELL"){
			return "field_data";
		}else if (xmlPath=="BEAHALL"){
			return "field_data";
		}else if (xmlPath=="BEDEL"){
			return "field_data";
		}else if (xmlPath=="BEDISIN"){
			return "field_data";
		}else if (xmlPath=="BEIRRIT"){
			return "field_data";
		}else if (xmlPath=="BEAGIT"){
			return "field_data";
		}else if (xmlPath=="BEPERCH"){
			return "field_data";
		}else if (xmlPath=="BEREM"){
			return "field_data";
		}else if (xmlPath=="BEOTHR"){
			return "field_data";
		}else if (xmlPath=="BEOTHRX"){
			return "field_data";
		}else if (xmlPath=="BEFRST"){
			return "field_data";
		}else if (xmlPath=="BEFRSTX"){
			return "field_data";
		}else if (xmlPath=="BEMODE"){
			return "field_data";
		}else if (xmlPath=="BEMODEX"){
			return "field_data";
		}else if (xmlPath=="MOGAIT"){
			return "field_data";
		}else if (xmlPath=="MOFALLS"){
			return "field_data";
		}else if (xmlPath=="MOTREM"){
			return "field_data";
		}else if (xmlPath=="MOSLOW"){
			return "field_data";
		}else if (xmlPath=="MOFRST"){
			return "field_data";
		}else if (xmlPath=="MOMODE"){
			return "field_data";
		}else if (xmlPath=="MOMODEX"){
			return "field_data";
		}else if (xmlPath=="MOMOPARK"){
			return "field_data";
		}else if (xmlPath=="COURSE"){
			return "field_data";
		}else if (xmlPath=="FRSTCHG"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<uds:B9CLINJDG";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</uds:B9CLINJDG>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.B9chg!=null){
			xmlTxt+="\n<uds:B9CHG";
			xmlTxt+=">";
			xmlTxt+=this.B9chg;
			xmlTxt+="</uds:B9CHG>";
		}
		if (this.Decsub!=null){
			xmlTxt+="\n<uds:DECSUB";
			xmlTxt+=">";
			xmlTxt+=this.Decsub;
			xmlTxt+="</uds:DECSUB>";
		}
		if (this.Decin!=null){
			xmlTxt+="\n<uds:DECIN";
			xmlTxt+=">";
			xmlTxt+=this.Decin;
			xmlTxt+="</uds:DECIN>";
		}
		if (this.Decclin!=null){
			xmlTxt+="\n<uds:DECCLIN";
			xmlTxt+=">";
			xmlTxt+=this.Decclin;
			xmlTxt+="</uds:DECCLIN>";
		}
		if (this.Decage!=null){
			xmlTxt+="\n<uds:DECAGE";
			xmlTxt+=">";
			xmlTxt+=this.Decage;
			xmlTxt+="</uds:DECAGE>";
		}
		if (this.Cogmem!=null){
			xmlTxt+="\n<uds:COGMEM";
			xmlTxt+=">";
			xmlTxt+=this.Cogmem;
			xmlTxt+="</uds:COGMEM>";
		}
		if (this.Cogjudg!=null){
			xmlTxt+="\n<uds:COGJUDG";
			xmlTxt+=">";
			xmlTxt+=this.Cogjudg;
			xmlTxt+="</uds:COGJUDG>";
		}
		if (this.Coglang!=null){
			xmlTxt+="\n<uds:COGLANG";
			xmlTxt+=">";
			xmlTxt+=this.Coglang;
			xmlTxt+="</uds:COGLANG>";
		}
		if (this.Cogvis!=null){
			xmlTxt+="\n<uds:COGVIS";
			xmlTxt+=">";
			xmlTxt+=this.Cogvis;
			xmlTxt+="</uds:COGVIS>";
		}
		if (this.Cogattn!=null){
			xmlTxt+="\n<uds:COGATTN";
			xmlTxt+=">";
			xmlTxt+=this.Cogattn;
			xmlTxt+="</uds:COGATTN>";
		}
		if (this.Cogfluc!=null){
			xmlTxt+="\n<uds:COGFLUC";
			xmlTxt+=">";
			xmlTxt+=this.Cogfluc;
			xmlTxt+="</uds:COGFLUC>";
		}
		if (this.Cogothr!=null){
			xmlTxt+="\n<uds:COGOTHR";
			xmlTxt+=">";
			xmlTxt+=this.Cogothr;
			xmlTxt+="</uds:COGOTHR>";
		}
		if (this.Cogothrx!=null){
			xmlTxt+="\n<uds:COGOTHRX";
			xmlTxt+=">";
			xmlTxt+=this.Cogothrx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:COGOTHRX>";
		}
		if (this.Cogfrst!=null){
			xmlTxt+="\n<uds:COGFRST";
			xmlTxt+=">";
			xmlTxt+=this.Cogfrst;
			xmlTxt+="</uds:COGFRST>";
		}
		if (this.Cogfrstx!=null){
			xmlTxt+="\n<uds:COGFRSTX";
			xmlTxt+=">";
			xmlTxt+=this.Cogfrstx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:COGFRSTX>";
		}
		if (this.Cogmode!=null){
			xmlTxt+="\n<uds:COGMODE";
			xmlTxt+=">";
			xmlTxt+=this.Cogmode;
			xmlTxt+="</uds:COGMODE>";
		}
		if (this.Cogmodex!=null){
			xmlTxt+="\n<uds:COGMODEX";
			xmlTxt+=">";
			xmlTxt+=this.Cogmodex.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:COGMODEX>";
		}
		if (this.Beapathy!=null){
			xmlTxt+="\n<uds:BEAPATHY";
			xmlTxt+=">";
			xmlTxt+=this.Beapathy;
			xmlTxt+="</uds:BEAPATHY>";
		}
		if (this.Bedep!=null){
			xmlTxt+="\n<uds:BEDEP";
			xmlTxt+=">";
			xmlTxt+=this.Bedep;
			xmlTxt+="</uds:BEDEP>";
		}
		if (this.Bevhall!=null){
			xmlTxt+="\n<uds:BEVHALL";
			xmlTxt+=">";
			xmlTxt+=this.Bevhall;
			xmlTxt+="</uds:BEVHALL>";
		}
		if (this.Bevwell!=null){
			xmlTxt+="\n<uds:BEVWELL";
			xmlTxt+=">";
			xmlTxt+=this.Bevwell;
			xmlTxt+="</uds:BEVWELL>";
		}
		if (this.Beahall!=null){
			xmlTxt+="\n<uds:BEAHALL";
			xmlTxt+=">";
			xmlTxt+=this.Beahall;
			xmlTxt+="</uds:BEAHALL>";
		}
		if (this.Bedel!=null){
			xmlTxt+="\n<uds:BEDEL";
			xmlTxt+=">";
			xmlTxt+=this.Bedel;
			xmlTxt+="</uds:BEDEL>";
		}
		if (this.Bedisin!=null){
			xmlTxt+="\n<uds:BEDISIN";
			xmlTxt+=">";
			xmlTxt+=this.Bedisin;
			xmlTxt+="</uds:BEDISIN>";
		}
		if (this.Beirrit!=null){
			xmlTxt+="\n<uds:BEIRRIT";
			xmlTxt+=">";
			xmlTxt+=this.Beirrit;
			xmlTxt+="</uds:BEIRRIT>";
		}
		if (this.Beagit!=null){
			xmlTxt+="\n<uds:BEAGIT";
			xmlTxt+=">";
			xmlTxt+=this.Beagit;
			xmlTxt+="</uds:BEAGIT>";
		}
		if (this.Beperch!=null){
			xmlTxt+="\n<uds:BEPERCH";
			xmlTxt+=">";
			xmlTxt+=this.Beperch;
			xmlTxt+="</uds:BEPERCH>";
		}
		if (this.Berem!=null){
			xmlTxt+="\n<uds:BEREM";
			xmlTxt+=">";
			xmlTxt+=this.Berem;
			xmlTxt+="</uds:BEREM>";
		}
		if (this.Beothr!=null){
			xmlTxt+="\n<uds:BEOTHR";
			xmlTxt+=">";
			xmlTxt+=this.Beothr;
			xmlTxt+="</uds:BEOTHR>";
		}
		if (this.Beothrx!=null){
			xmlTxt+="\n<uds:BEOTHRX";
			xmlTxt+=">";
			xmlTxt+=this.Beothrx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:BEOTHRX>";
		}
		if (this.Befrst!=null){
			xmlTxt+="\n<uds:BEFRST";
			xmlTxt+=">";
			xmlTxt+=this.Befrst;
			xmlTxt+="</uds:BEFRST>";
		}
		if (this.Befrstx!=null){
			xmlTxt+="\n<uds:BEFRSTX";
			xmlTxt+=">";
			xmlTxt+=this.Befrstx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:BEFRSTX>";
		}
		if (this.Bemode!=null){
			xmlTxt+="\n<uds:BEMODE";
			xmlTxt+=">";
			xmlTxt+=this.Bemode;
			xmlTxt+="</uds:BEMODE>";
		}
		if (this.Bemodex!=null){
			xmlTxt+="\n<uds:BEMODEX";
			xmlTxt+=">";
			xmlTxt+=this.Bemodex.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:BEMODEX>";
		}
		if (this.Mogait!=null){
			xmlTxt+="\n<uds:MOGAIT";
			xmlTxt+=">";
			xmlTxt+=this.Mogait;
			xmlTxt+="</uds:MOGAIT>";
		}
		if (this.Mofalls!=null){
			xmlTxt+="\n<uds:MOFALLS";
			xmlTxt+=">";
			xmlTxt+=this.Mofalls;
			xmlTxt+="</uds:MOFALLS>";
		}
		if (this.Motrem!=null){
			xmlTxt+="\n<uds:MOTREM";
			xmlTxt+=">";
			xmlTxt+=this.Motrem;
			xmlTxt+="</uds:MOTREM>";
		}
		if (this.Moslow!=null){
			xmlTxt+="\n<uds:MOSLOW";
			xmlTxt+=">";
			xmlTxt+=this.Moslow;
			xmlTxt+="</uds:MOSLOW>";
		}
		if (this.Mofrst!=null){
			xmlTxt+="\n<uds:MOFRST";
			xmlTxt+=">";
			xmlTxt+=this.Mofrst;
			xmlTxt+="</uds:MOFRST>";
		}
		if (this.Momode!=null){
			xmlTxt+="\n<uds:MOMODE";
			xmlTxt+=">";
			xmlTxt+=this.Momode;
			xmlTxt+="</uds:MOMODE>";
		}
		if (this.Momodex!=null){
			xmlTxt+="\n<uds:MOMODEX";
			xmlTxt+=">";
			xmlTxt+=this.Momodex.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:MOMODEX>";
		}
		if (this.Momopark!=null){
			xmlTxt+="\n<uds:MOMOPARK";
			xmlTxt+=">";
			xmlTxt+=this.Momopark;
			xmlTxt+="</uds:MOMOPARK>";
		}
		if (this.Course!=null){
			xmlTxt+="\n<uds:COURSE";
			xmlTxt+=">";
			xmlTxt+=this.Course;
			xmlTxt+="</uds:COURSE>";
		}
		if (this.Frstchg!=null){
			xmlTxt+="\n<uds:FRSTCHG";
			xmlTxt+=">";
			xmlTxt+=this.Frstchg;
			xmlTxt+="</uds:FRSTCHG>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.B9chg!=null) return true;
		if (this.Decsub!=null) return true;
		if (this.Decin!=null) return true;
		if (this.Decclin!=null) return true;
		if (this.Decage!=null) return true;
		if (this.Cogmem!=null) return true;
		if (this.Cogjudg!=null) return true;
		if (this.Coglang!=null) return true;
		if (this.Cogvis!=null) return true;
		if (this.Cogattn!=null) return true;
		if (this.Cogfluc!=null) return true;
		if (this.Cogothr!=null) return true;
		if (this.Cogothrx!=null) return true;
		if (this.Cogfrst!=null) return true;
		if (this.Cogfrstx!=null) return true;
		if (this.Cogmode!=null) return true;
		if (this.Cogmodex!=null) return true;
		if (this.Beapathy!=null) return true;
		if (this.Bedep!=null) return true;
		if (this.Bevhall!=null) return true;
		if (this.Bevwell!=null) return true;
		if (this.Beahall!=null) return true;
		if (this.Bedel!=null) return true;
		if (this.Bedisin!=null) return true;
		if (this.Beirrit!=null) return true;
		if (this.Beagit!=null) return true;
		if (this.Beperch!=null) return true;
		if (this.Berem!=null) return true;
		if (this.Beothr!=null) return true;
		if (this.Beothrx!=null) return true;
		if (this.Befrst!=null) return true;
		if (this.Befrstx!=null) return true;
		if (this.Bemode!=null) return true;
		if (this.Bemodex!=null) return true;
		if (this.Mogait!=null) return true;
		if (this.Mofalls!=null) return true;
		if (this.Motrem!=null) return true;
		if (this.Moslow!=null) return true;
		if (this.Mofrst!=null) return true;
		if (this.Momode!=null) return true;
		if (this.Momodex!=null) return true;
		if (this.Momopark!=null) return true;
		if (this.Course!=null) return true;
		if (this.Frstchg!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
