/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function dian_mintData(){
this.xsiType="dian:mintData";

	this.getSchemaElementName=function(){
		return "mintData";
	}

	this.getFullSchemaElementName=function(){
		return "dian:mintData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Done=null;


	function getDone() {
		return this.Done;
	}
	this.getDone=getDone;


	function setDone(v){
		this.Done=v;
	}
	this.setDone=setDone;

	this.Ndreason=null;


	function getNdreason() {
		return this.Ndreason;
	}
	this.getNdreason=getNdreason;


	function setNdreason(v){
		this.Ndreason=v;
	}
	this.setNdreason=setNdreason;

	this.Mtq1uncue=null;


	function getMtq1uncue() {
		return this.Mtq1uncue;
	}
	this.getMtq1uncue=getMtq1uncue;


	function setMtq1uncue(v){
		this.Mtq1uncue=v;
	}
	this.setMtq1uncue=setMtq1uncue;

	this.Mtq1msip=null;


	function getMtq1msip() {
		return this.Mtq1msip;
	}
	this.getMtq1msip=getMtq1msip;


	function setMtq1msip(v){
		this.Mtq1msip=v;
	}
	this.setMtq1msip=setMtq1msip;

	this.Mtq1sem=null;


	function getMtq1sem() {
		return this.Mtq1sem;
	}
	this.getMtq1sem=getMtq1sem;


	function setMtq1sem(v){
		this.Mtq1sem=v;
	}
	this.setMtq1sem=setMtq1sem;

	this.Mtq1phon=null;


	function getMtq1phon() {
		return this.Mtq1phon;
	}
	this.getMtq1phon=getMtq1phon;


	function setMtq1phon(v){
		this.Mtq1phon=v;
	}
	this.setMtq1phon=setMtq1phon;

	this.Mtq2uncue=null;


	function getMtq2uncue() {
		return this.Mtq2uncue;
	}
	this.getMtq2uncue=getMtq2uncue;


	function setMtq2uncue(v){
		this.Mtq2uncue=v;
	}
	this.setMtq2uncue=setMtq2uncue;

	this.Mtq2msip=null;


	function getMtq2msip() {
		return this.Mtq2msip;
	}
	this.getMtq2msip=getMtq2msip;


	function setMtq2msip(v){
		this.Mtq2msip=v;
	}
	this.setMtq2msip=setMtq2msip;

	this.Mtq2sem=null;


	function getMtq2sem() {
		return this.Mtq2sem;
	}
	this.getMtq2sem=getMtq2sem;


	function setMtq2sem(v){
		this.Mtq2sem=v;
	}
	this.setMtq2sem=setMtq2sem;

	this.Mtq2phon=null;


	function getMtq2phon() {
		return this.Mtq2phon;
	}
	this.getMtq2phon=getMtq2phon;


	function setMtq2phon(v){
		this.Mtq2phon=v;
	}
	this.setMtq2phon=setMtq2phon;

	this.Mtq3uncue=null;


	function getMtq3uncue() {
		return this.Mtq3uncue;
	}
	this.getMtq3uncue=getMtq3uncue;


	function setMtq3uncue(v){
		this.Mtq3uncue=v;
	}
	this.setMtq3uncue=setMtq3uncue;

	this.Mtq3msip=null;


	function getMtq3msip() {
		return this.Mtq3msip;
	}
	this.getMtq3msip=getMtq3msip;


	function setMtq3msip(v){
		this.Mtq3msip=v;
	}
	this.setMtq3msip=setMtq3msip;

	this.Mtq3sem=null;


	function getMtq3sem() {
		return this.Mtq3sem;
	}
	this.getMtq3sem=getMtq3sem;


	function setMtq3sem(v){
		this.Mtq3sem=v;
	}
	this.setMtq3sem=setMtq3sem;

	this.Mtq3phon=null;


	function getMtq3phon() {
		return this.Mtq3phon;
	}
	this.getMtq3phon=getMtq3phon;


	function setMtq3phon(v){
		this.Mtq3phon=v;
	}
	this.setMtq3phon=setMtq3phon;

	this.Mtq4uncue=null;


	function getMtq4uncue() {
		return this.Mtq4uncue;
	}
	this.getMtq4uncue=getMtq4uncue;


	function setMtq4uncue(v){
		this.Mtq4uncue=v;
	}
	this.setMtq4uncue=setMtq4uncue;

	this.Mtq4msip=null;


	function getMtq4msip() {
		return this.Mtq4msip;
	}
	this.getMtq4msip=getMtq4msip;


	function setMtq4msip(v){
		this.Mtq4msip=v;
	}
	this.setMtq4msip=setMtq4msip;

	this.Mtq4sem=null;


	function getMtq4sem() {
		return this.Mtq4sem;
	}
	this.getMtq4sem=getMtq4sem;


	function setMtq4sem(v){
		this.Mtq4sem=v;
	}
	this.setMtq4sem=setMtq4sem;

	this.Mtq4phon=null;


	function getMtq4phon() {
		return this.Mtq4phon;
	}
	this.getMtq4phon=getMtq4phon;


	function setMtq4phon(v){
		this.Mtq4phon=v;
	}
	this.setMtq4phon=setMtq4phon;

	this.Mtq5uncue=null;


	function getMtq5uncue() {
		return this.Mtq5uncue;
	}
	this.getMtq5uncue=getMtq5uncue;


	function setMtq5uncue(v){
		this.Mtq5uncue=v;
	}
	this.setMtq5uncue=setMtq5uncue;

	this.Mtq5msip=null;


	function getMtq5msip() {
		return this.Mtq5msip;
	}
	this.getMtq5msip=getMtq5msip;


	function setMtq5msip(v){
		this.Mtq5msip=v;
	}
	this.setMtq5msip=setMtq5msip;

	this.Mtq5sem=null;


	function getMtq5sem() {
		return this.Mtq5sem;
	}
	this.getMtq5sem=getMtq5sem;


	function setMtq5sem(v){
		this.Mtq5sem=v;
	}
	this.setMtq5sem=setMtq5sem;

	this.Mtq5phon=null;


	function getMtq5phon() {
		return this.Mtq5phon;
	}
	this.getMtq5phon=getMtq5phon;


	function setMtq5phon(v){
		this.Mtq5phon=v;
	}
	this.setMtq5phon=setMtq5phon;

	this.Mtq6uncue=null;


	function getMtq6uncue() {
		return this.Mtq6uncue;
	}
	this.getMtq6uncue=getMtq6uncue;


	function setMtq6uncue(v){
		this.Mtq6uncue=v;
	}
	this.setMtq6uncue=setMtq6uncue;

	this.Mtq6msip=null;


	function getMtq6msip() {
		return this.Mtq6msip;
	}
	this.getMtq6msip=getMtq6msip;


	function setMtq6msip(v){
		this.Mtq6msip=v;
	}
	this.setMtq6msip=setMtq6msip;

	this.Mtq6sem=null;


	function getMtq6sem() {
		return this.Mtq6sem;
	}
	this.getMtq6sem=getMtq6sem;


	function setMtq6sem(v){
		this.Mtq6sem=v;
	}
	this.setMtq6sem=setMtq6sem;

	this.Mtq6phon=null;


	function getMtq6phon() {
		return this.Mtq6phon;
	}
	this.getMtq6phon=getMtq6phon;


	function setMtq6phon(v){
		this.Mtq6phon=v;
	}
	this.setMtq6phon=setMtq6phon;

	this.Mtq7uncue=null;


	function getMtq7uncue() {
		return this.Mtq7uncue;
	}
	this.getMtq7uncue=getMtq7uncue;


	function setMtq7uncue(v){
		this.Mtq7uncue=v;
	}
	this.setMtq7uncue=setMtq7uncue;

	this.Mtq7msip=null;


	function getMtq7msip() {
		return this.Mtq7msip;
	}
	this.getMtq7msip=getMtq7msip;


	function setMtq7msip(v){
		this.Mtq7msip=v;
	}
	this.setMtq7msip=setMtq7msip;

	this.Mtq7sem=null;


	function getMtq7sem() {
		return this.Mtq7sem;
	}
	this.getMtq7sem=getMtq7sem;


	function setMtq7sem(v){
		this.Mtq7sem=v;
	}
	this.setMtq7sem=setMtq7sem;

	this.Mtq7phon=null;


	function getMtq7phon() {
		return this.Mtq7phon;
	}
	this.getMtq7phon=getMtq7phon;


	function setMtq7phon(v){
		this.Mtq7phon=v;
	}
	this.setMtq7phon=setMtq7phon;

	this.Mtq8uncue=null;


	function getMtq8uncue() {
		return this.Mtq8uncue;
	}
	this.getMtq8uncue=getMtq8uncue;


	function setMtq8uncue(v){
		this.Mtq8uncue=v;
	}
	this.setMtq8uncue=setMtq8uncue;

	this.Mtq8msip=null;


	function getMtq8msip() {
		return this.Mtq8msip;
	}
	this.getMtq8msip=getMtq8msip;


	function setMtq8msip(v){
		this.Mtq8msip=v;
	}
	this.setMtq8msip=setMtq8msip;

	this.Mtq8sem=null;


	function getMtq8sem() {
		return this.Mtq8sem;
	}
	this.getMtq8sem=getMtq8sem;


	function setMtq8sem(v){
		this.Mtq8sem=v;
	}
	this.setMtq8sem=setMtq8sem;

	this.Mtq8phon=null;


	function getMtq8phon() {
		return this.Mtq8phon;
	}
	this.getMtq8phon=getMtq8phon;


	function setMtq8phon(v){
		this.Mtq8phon=v;
	}
	this.setMtq8phon=setMtq8phon;

	this.Mtq9uncue=null;


	function getMtq9uncue() {
		return this.Mtq9uncue;
	}
	this.getMtq9uncue=getMtq9uncue;


	function setMtq9uncue(v){
		this.Mtq9uncue=v;
	}
	this.setMtq9uncue=setMtq9uncue;

	this.Mtq9msip=null;


	function getMtq9msip() {
		return this.Mtq9msip;
	}
	this.getMtq9msip=getMtq9msip;


	function setMtq9msip(v){
		this.Mtq9msip=v;
	}
	this.setMtq9msip=setMtq9msip;

	this.Mtq9sem=null;


	function getMtq9sem() {
		return this.Mtq9sem;
	}
	this.getMtq9sem=getMtq9sem;


	function setMtq9sem(v){
		this.Mtq9sem=v;
	}
	this.setMtq9sem=setMtq9sem;

	this.Mtq9phon=null;


	function getMtq9phon() {
		return this.Mtq9phon;
	}
	this.getMtq9phon=getMtq9phon;


	function setMtq9phon(v){
		this.Mtq9phon=v;
	}
	this.setMtq9phon=setMtq9phon;

	this.Mtq10uncue=null;


	function getMtq10uncue() {
		return this.Mtq10uncue;
	}
	this.getMtq10uncue=getMtq10uncue;


	function setMtq10uncue(v){
		this.Mtq10uncue=v;
	}
	this.setMtq10uncue=setMtq10uncue;

	this.Mtq10msip=null;


	function getMtq10msip() {
		return this.Mtq10msip;
	}
	this.getMtq10msip=getMtq10msip;


	function setMtq10msip(v){
		this.Mtq10msip=v;
	}
	this.setMtq10msip=setMtq10msip;

	this.Mtq10sem=null;


	function getMtq10sem() {
		return this.Mtq10sem;
	}
	this.getMtq10sem=getMtq10sem;


	function setMtq10sem(v){
		this.Mtq10sem=v;
	}
	this.setMtq10sem=setMtq10sem;

	this.Mtq10phon=null;


	function getMtq10phon() {
		return this.Mtq10phon;
	}
	this.getMtq10phon=getMtq10phon;


	function setMtq10phon(v){
		this.Mtq10phon=v;
	}
	this.setMtq10phon=setMtq10phon;

	this.Mtq11uncue=null;


	function getMtq11uncue() {
		return this.Mtq11uncue;
	}
	this.getMtq11uncue=getMtq11uncue;


	function setMtq11uncue(v){
		this.Mtq11uncue=v;
	}
	this.setMtq11uncue=setMtq11uncue;

	this.Mtq11msip=null;


	function getMtq11msip() {
		return this.Mtq11msip;
	}
	this.getMtq11msip=getMtq11msip;


	function setMtq11msip(v){
		this.Mtq11msip=v;
	}
	this.setMtq11msip=setMtq11msip;

	this.Mtq11sem=null;


	function getMtq11sem() {
		return this.Mtq11sem;
	}
	this.getMtq11sem=getMtq11sem;


	function setMtq11sem(v){
		this.Mtq11sem=v;
	}
	this.setMtq11sem=setMtq11sem;

	this.Mtq11phon=null;


	function getMtq11phon() {
		return this.Mtq11phon;
	}
	this.getMtq11phon=getMtq11phon;


	function setMtq11phon(v){
		this.Mtq11phon=v;
	}
	this.setMtq11phon=setMtq11phon;

	this.Mtq12uncue=null;


	function getMtq12uncue() {
		return this.Mtq12uncue;
	}
	this.getMtq12uncue=getMtq12uncue;


	function setMtq12uncue(v){
		this.Mtq12uncue=v;
	}
	this.setMtq12uncue=setMtq12uncue;

	this.Mtq12msip=null;


	function getMtq12msip() {
		return this.Mtq12msip;
	}
	this.getMtq12msip=getMtq12msip;


	function setMtq12msip(v){
		this.Mtq12msip=v;
	}
	this.setMtq12msip=setMtq12msip;

	this.Mtq12sem=null;


	function getMtq12sem() {
		return this.Mtq12sem;
	}
	this.getMtq12sem=getMtq12sem;


	function setMtq12sem(v){
		this.Mtq12sem=v;
	}
	this.setMtq12sem=setMtq12sem;

	this.Mtq12phon=null;


	function getMtq12phon() {
		return this.Mtq12phon;
	}
	this.getMtq12phon=getMtq12phon;


	function setMtq12phon(v){
		this.Mtq12phon=v;
	}
	this.setMtq12phon=setMtq12phon;

	this.Mtq13uncue=null;


	function getMtq13uncue() {
		return this.Mtq13uncue;
	}
	this.getMtq13uncue=getMtq13uncue;


	function setMtq13uncue(v){
		this.Mtq13uncue=v;
	}
	this.setMtq13uncue=setMtq13uncue;

	this.Mtq13msip=null;


	function getMtq13msip() {
		return this.Mtq13msip;
	}
	this.getMtq13msip=getMtq13msip;


	function setMtq13msip(v){
		this.Mtq13msip=v;
	}
	this.setMtq13msip=setMtq13msip;

	this.Mtq13sem=null;


	function getMtq13sem() {
		return this.Mtq13sem;
	}
	this.getMtq13sem=getMtq13sem;


	function setMtq13sem(v){
		this.Mtq13sem=v;
	}
	this.setMtq13sem=setMtq13sem;

	this.Mtq13phon=null;


	function getMtq13phon() {
		return this.Mtq13phon;
	}
	this.getMtq13phon=getMtq13phon;


	function setMtq13phon(v){
		this.Mtq13phon=v;
	}
	this.setMtq13phon=setMtq13phon;

	this.Mtq14uncue=null;


	function getMtq14uncue() {
		return this.Mtq14uncue;
	}
	this.getMtq14uncue=getMtq14uncue;


	function setMtq14uncue(v){
		this.Mtq14uncue=v;
	}
	this.setMtq14uncue=setMtq14uncue;

	this.Mtq14msip=null;


	function getMtq14msip() {
		return this.Mtq14msip;
	}
	this.getMtq14msip=getMtq14msip;


	function setMtq14msip(v){
		this.Mtq14msip=v;
	}
	this.setMtq14msip=setMtq14msip;

	this.Mtq14sem=null;


	function getMtq14sem() {
		return this.Mtq14sem;
	}
	this.getMtq14sem=getMtq14sem;


	function setMtq14sem(v){
		this.Mtq14sem=v;
	}
	this.setMtq14sem=setMtq14sem;

	this.Mtq14phon=null;


	function getMtq14phon() {
		return this.Mtq14phon;
	}
	this.getMtq14phon=getMtq14phon;


	function setMtq14phon(v){
		this.Mtq14phon=v;
	}
	this.setMtq14phon=setMtq14phon;

	this.Mtq15uncue=null;


	function getMtq15uncue() {
		return this.Mtq15uncue;
	}
	this.getMtq15uncue=getMtq15uncue;


	function setMtq15uncue(v){
		this.Mtq15uncue=v;
	}
	this.setMtq15uncue=setMtq15uncue;

	this.Mtq15msip=null;


	function getMtq15msip() {
		return this.Mtq15msip;
	}
	this.getMtq15msip=getMtq15msip;


	function setMtq15msip(v){
		this.Mtq15msip=v;
	}
	this.setMtq15msip=setMtq15msip;

	this.Mtq15sem=null;


	function getMtq15sem() {
		return this.Mtq15sem;
	}
	this.getMtq15sem=getMtq15sem;


	function setMtq15sem(v){
		this.Mtq15sem=v;
	}
	this.setMtq15sem=setMtq15sem;

	this.Mtq15phon=null;


	function getMtq15phon() {
		return this.Mtq15phon;
	}
	this.getMtq15phon=getMtq15phon;


	function setMtq15phon(v){
		this.Mtq15phon=v;
	}
	this.setMtq15phon=setMtq15phon;

	this.Mtq16uncue=null;


	function getMtq16uncue() {
		return this.Mtq16uncue;
	}
	this.getMtq16uncue=getMtq16uncue;


	function setMtq16uncue(v){
		this.Mtq16uncue=v;
	}
	this.setMtq16uncue=setMtq16uncue;

	this.Mtq16msip=null;


	function getMtq16msip() {
		return this.Mtq16msip;
	}
	this.getMtq16msip=getMtq16msip;


	function setMtq16msip(v){
		this.Mtq16msip=v;
	}
	this.setMtq16msip=setMtq16msip;

	this.Mtq16sem=null;


	function getMtq16sem() {
		return this.Mtq16sem;
	}
	this.getMtq16sem=getMtq16sem;


	function setMtq16sem(v){
		this.Mtq16sem=v;
	}
	this.setMtq16sem=setMtq16sem;

	this.Mtq16phon=null;


	function getMtq16phon() {
		return this.Mtq16phon;
	}
	this.getMtq16phon=getMtq16phon;


	function setMtq16phon(v){
		this.Mtq16phon=v;
	}
	this.setMtq16phon=setMtq16phon;

	this.Mtq17uncue=null;


	function getMtq17uncue() {
		return this.Mtq17uncue;
	}
	this.getMtq17uncue=getMtq17uncue;


	function setMtq17uncue(v){
		this.Mtq17uncue=v;
	}
	this.setMtq17uncue=setMtq17uncue;

	this.Mtq17msip=null;


	function getMtq17msip() {
		return this.Mtq17msip;
	}
	this.getMtq17msip=getMtq17msip;


	function setMtq17msip(v){
		this.Mtq17msip=v;
	}
	this.setMtq17msip=setMtq17msip;

	this.Mtq17sem=null;


	function getMtq17sem() {
		return this.Mtq17sem;
	}
	this.getMtq17sem=getMtq17sem;


	function setMtq17sem(v){
		this.Mtq17sem=v;
	}
	this.setMtq17sem=setMtq17sem;

	this.Mtq17phon=null;


	function getMtq17phon() {
		return this.Mtq17phon;
	}
	this.getMtq17phon=getMtq17phon;


	function setMtq17phon(v){
		this.Mtq17phon=v;
	}
	this.setMtq17phon=setMtq17phon;

	this.Mtq18uncue=null;


	function getMtq18uncue() {
		return this.Mtq18uncue;
	}
	this.getMtq18uncue=getMtq18uncue;


	function setMtq18uncue(v){
		this.Mtq18uncue=v;
	}
	this.setMtq18uncue=setMtq18uncue;

	this.Mtq18msip=null;


	function getMtq18msip() {
		return this.Mtq18msip;
	}
	this.getMtq18msip=getMtq18msip;


	function setMtq18msip(v){
		this.Mtq18msip=v;
	}
	this.setMtq18msip=setMtq18msip;

	this.Mtq18sem=null;


	function getMtq18sem() {
		return this.Mtq18sem;
	}
	this.getMtq18sem=getMtq18sem;


	function setMtq18sem(v){
		this.Mtq18sem=v;
	}
	this.setMtq18sem=setMtq18sem;

	this.Mtq18phon=null;


	function getMtq18phon() {
		return this.Mtq18phon;
	}
	this.getMtq18phon=getMtq18phon;


	function setMtq18phon(v){
		this.Mtq18phon=v;
	}
	this.setMtq18phon=setMtq18phon;

	this.Mtq19uncue=null;


	function getMtq19uncue() {
		return this.Mtq19uncue;
	}
	this.getMtq19uncue=getMtq19uncue;


	function setMtq19uncue(v){
		this.Mtq19uncue=v;
	}
	this.setMtq19uncue=setMtq19uncue;

	this.Mtq19msip=null;


	function getMtq19msip() {
		return this.Mtq19msip;
	}
	this.getMtq19msip=getMtq19msip;


	function setMtq19msip(v){
		this.Mtq19msip=v;
	}
	this.setMtq19msip=setMtq19msip;

	this.Mtq19sem=null;


	function getMtq19sem() {
		return this.Mtq19sem;
	}
	this.getMtq19sem=getMtq19sem;


	function setMtq19sem(v){
		this.Mtq19sem=v;
	}
	this.setMtq19sem=setMtq19sem;

	this.Mtq19phon=null;


	function getMtq19phon() {
		return this.Mtq19phon;
	}
	this.getMtq19phon=getMtq19phon;


	function setMtq19phon(v){
		this.Mtq19phon=v;
	}
	this.setMtq19phon=setMtq19phon;

	this.Mtq20uncue=null;


	function getMtq20uncue() {
		return this.Mtq20uncue;
	}
	this.getMtq20uncue=getMtq20uncue;


	function setMtq20uncue(v){
		this.Mtq20uncue=v;
	}
	this.setMtq20uncue=setMtq20uncue;

	this.Mtq20msip=null;


	function getMtq20msip() {
		return this.Mtq20msip;
	}
	this.getMtq20msip=getMtq20msip;


	function setMtq20msip(v){
		this.Mtq20msip=v;
	}
	this.setMtq20msip=setMtq20msip;

	this.Mtq20sem=null;


	function getMtq20sem() {
		return this.Mtq20sem;
	}
	this.getMtq20sem=getMtq20sem;


	function setMtq20sem(v){
		this.Mtq20sem=v;
	}
	this.setMtq20sem=setMtq20sem;

	this.Mtq20phon=null;


	function getMtq20phon() {
		return this.Mtq20phon;
	}
	this.getMtq20phon=getMtq20phon;


	function setMtq20phon(v){
		this.Mtq20phon=v;
	}
	this.setMtq20phon=setMtq20phon;

	this.Mtq21uncue=null;


	function getMtq21uncue() {
		return this.Mtq21uncue;
	}
	this.getMtq21uncue=getMtq21uncue;


	function setMtq21uncue(v){
		this.Mtq21uncue=v;
	}
	this.setMtq21uncue=setMtq21uncue;

	this.Mtq21msip=null;


	function getMtq21msip() {
		return this.Mtq21msip;
	}
	this.getMtq21msip=getMtq21msip;


	function setMtq21msip(v){
		this.Mtq21msip=v;
	}
	this.setMtq21msip=setMtq21msip;

	this.Mtq21sem=null;


	function getMtq21sem() {
		return this.Mtq21sem;
	}
	this.getMtq21sem=getMtq21sem;


	function setMtq21sem(v){
		this.Mtq21sem=v;
	}
	this.setMtq21sem=setMtq21sem;

	this.Mtq21phon=null;


	function getMtq21phon() {
		return this.Mtq21phon;
	}
	this.getMtq21phon=getMtq21phon;


	function setMtq21phon(v){
		this.Mtq21phon=v;
	}
	this.setMtq21phon=setMtq21phon;

	this.Mtq22uncue=null;


	function getMtq22uncue() {
		return this.Mtq22uncue;
	}
	this.getMtq22uncue=getMtq22uncue;


	function setMtq22uncue(v){
		this.Mtq22uncue=v;
	}
	this.setMtq22uncue=setMtq22uncue;

	this.Mtq22msip=null;


	function getMtq22msip() {
		return this.Mtq22msip;
	}
	this.getMtq22msip=getMtq22msip;


	function setMtq22msip(v){
		this.Mtq22msip=v;
	}
	this.setMtq22msip=setMtq22msip;

	this.Mtq22sem=null;


	function getMtq22sem() {
		return this.Mtq22sem;
	}
	this.getMtq22sem=getMtq22sem;


	function setMtq22sem(v){
		this.Mtq22sem=v;
	}
	this.setMtq22sem=setMtq22sem;

	this.Mtq22phon=null;


	function getMtq22phon() {
		return this.Mtq22phon;
	}
	this.getMtq22phon=getMtq22phon;


	function setMtq22phon(v){
		this.Mtq22phon=v;
	}
	this.setMtq22phon=setMtq22phon;

	this.Mtq23uncue=null;


	function getMtq23uncue() {
		return this.Mtq23uncue;
	}
	this.getMtq23uncue=getMtq23uncue;


	function setMtq23uncue(v){
		this.Mtq23uncue=v;
	}
	this.setMtq23uncue=setMtq23uncue;

	this.Mtq23msip=null;


	function getMtq23msip() {
		return this.Mtq23msip;
	}
	this.getMtq23msip=getMtq23msip;


	function setMtq23msip(v){
		this.Mtq23msip=v;
	}
	this.setMtq23msip=setMtq23msip;

	this.Mtq23sem=null;


	function getMtq23sem() {
		return this.Mtq23sem;
	}
	this.getMtq23sem=getMtq23sem;


	function setMtq23sem(v){
		this.Mtq23sem=v;
	}
	this.setMtq23sem=setMtq23sem;

	this.Mtq23phon=null;


	function getMtq23phon() {
		return this.Mtq23phon;
	}
	this.getMtq23phon=getMtq23phon;


	function setMtq23phon(v){
		this.Mtq23phon=v;
	}
	this.setMtq23phon=setMtq23phon;

	this.Mtq24uncue=null;


	function getMtq24uncue() {
		return this.Mtq24uncue;
	}
	this.getMtq24uncue=getMtq24uncue;


	function setMtq24uncue(v){
		this.Mtq24uncue=v;
	}
	this.setMtq24uncue=setMtq24uncue;

	this.Mtq24msip=null;


	function getMtq24msip() {
		return this.Mtq24msip;
	}
	this.getMtq24msip=getMtq24msip;


	function setMtq24msip(v){
		this.Mtq24msip=v;
	}
	this.setMtq24msip=setMtq24msip;

	this.Mtq24sem=null;


	function getMtq24sem() {
		return this.Mtq24sem;
	}
	this.getMtq24sem=getMtq24sem;


	function setMtq24sem(v){
		this.Mtq24sem=v;
	}
	this.setMtq24sem=setMtq24sem;

	this.Mtq24phon=null;


	function getMtq24phon() {
		return this.Mtq24phon;
	}
	this.getMtq24phon=getMtq24phon;


	function setMtq24phon(v){
		this.Mtq24phon=v;
	}
	this.setMtq24phon=setMtq24phon;

	this.Mtq25uncue=null;


	function getMtq25uncue() {
		return this.Mtq25uncue;
	}
	this.getMtq25uncue=getMtq25uncue;


	function setMtq25uncue(v){
		this.Mtq25uncue=v;
	}
	this.setMtq25uncue=setMtq25uncue;

	this.Mtq25msip=null;


	function getMtq25msip() {
		return this.Mtq25msip;
	}
	this.getMtq25msip=getMtq25msip;


	function setMtq25msip(v){
		this.Mtq25msip=v;
	}
	this.setMtq25msip=setMtq25msip;

	this.Mtq25sem=null;


	function getMtq25sem() {
		return this.Mtq25sem;
	}
	this.getMtq25sem=getMtq25sem;


	function setMtq25sem(v){
		this.Mtq25sem=v;
	}
	this.setMtq25sem=setMtq25sem;

	this.Mtq25phon=null;


	function getMtq25phon() {
		return this.Mtq25phon;
	}
	this.getMtq25phon=getMtq25phon;


	function setMtq25phon(v){
		this.Mtq25phon=v;
	}
	this.setMtq25phon=setMtq25phon;

	this.Mtq26uncue=null;


	function getMtq26uncue() {
		return this.Mtq26uncue;
	}
	this.getMtq26uncue=getMtq26uncue;


	function setMtq26uncue(v){
		this.Mtq26uncue=v;
	}
	this.setMtq26uncue=setMtq26uncue;

	this.Mtq26msip=null;


	function getMtq26msip() {
		return this.Mtq26msip;
	}
	this.getMtq26msip=getMtq26msip;


	function setMtq26msip(v){
		this.Mtq26msip=v;
	}
	this.setMtq26msip=setMtq26msip;

	this.Mtq26sem=null;


	function getMtq26sem() {
		return this.Mtq26sem;
	}
	this.getMtq26sem=getMtq26sem;


	function setMtq26sem(v){
		this.Mtq26sem=v;
	}
	this.setMtq26sem=setMtq26sem;

	this.Mtq26phon=null;


	function getMtq26phon() {
		return this.Mtq26phon;
	}
	this.getMtq26phon=getMtq26phon;


	function setMtq26phon(v){
		this.Mtq26phon=v;
	}
	this.setMtq26phon=setMtq26phon;

	this.Mtq27uncue=null;


	function getMtq27uncue() {
		return this.Mtq27uncue;
	}
	this.getMtq27uncue=getMtq27uncue;


	function setMtq27uncue(v){
		this.Mtq27uncue=v;
	}
	this.setMtq27uncue=setMtq27uncue;

	this.Mtq27msip=null;


	function getMtq27msip() {
		return this.Mtq27msip;
	}
	this.getMtq27msip=getMtq27msip;


	function setMtq27msip(v){
		this.Mtq27msip=v;
	}
	this.setMtq27msip=setMtq27msip;

	this.Mtq27sem=null;


	function getMtq27sem() {
		return this.Mtq27sem;
	}
	this.getMtq27sem=getMtq27sem;


	function setMtq27sem(v){
		this.Mtq27sem=v;
	}
	this.setMtq27sem=setMtq27sem;

	this.Mtq27phon=null;


	function getMtq27phon() {
		return this.Mtq27phon;
	}
	this.getMtq27phon=getMtq27phon;


	function setMtq27phon(v){
		this.Mtq27phon=v;
	}
	this.setMtq27phon=setMtq27phon;

	this.Mtq28uncue=null;


	function getMtq28uncue() {
		return this.Mtq28uncue;
	}
	this.getMtq28uncue=getMtq28uncue;


	function setMtq28uncue(v){
		this.Mtq28uncue=v;
	}
	this.setMtq28uncue=setMtq28uncue;

	this.Mtq28msip=null;


	function getMtq28msip() {
		return this.Mtq28msip;
	}
	this.getMtq28msip=getMtq28msip;


	function setMtq28msip(v){
		this.Mtq28msip=v;
	}
	this.setMtq28msip=setMtq28msip;

	this.Mtq28sem=null;


	function getMtq28sem() {
		return this.Mtq28sem;
	}
	this.getMtq28sem=getMtq28sem;


	function setMtq28sem(v){
		this.Mtq28sem=v;
	}
	this.setMtq28sem=setMtq28sem;

	this.Mtq28phon=null;


	function getMtq28phon() {
		return this.Mtq28phon;
	}
	this.getMtq28phon=getMtq28phon;


	function setMtq28phon(v){
		this.Mtq28phon=v;
	}
	this.setMtq28phon=setMtq28phon;

	this.Mtq29uncue=null;


	function getMtq29uncue() {
		return this.Mtq29uncue;
	}
	this.getMtq29uncue=getMtq29uncue;


	function setMtq29uncue(v){
		this.Mtq29uncue=v;
	}
	this.setMtq29uncue=setMtq29uncue;

	this.Mtq29msip=null;


	function getMtq29msip() {
		return this.Mtq29msip;
	}
	this.getMtq29msip=getMtq29msip;


	function setMtq29msip(v){
		this.Mtq29msip=v;
	}
	this.setMtq29msip=setMtq29msip;

	this.Mtq29sem=null;


	function getMtq29sem() {
		return this.Mtq29sem;
	}
	this.getMtq29sem=getMtq29sem;


	function setMtq29sem(v){
		this.Mtq29sem=v;
	}
	this.setMtq29sem=setMtq29sem;

	this.Mtq29phon=null;


	function getMtq29phon() {
		return this.Mtq29phon;
	}
	this.getMtq29phon=getMtq29phon;


	function setMtq29phon(v){
		this.Mtq29phon=v;
	}
	this.setMtq29phon=setMtq29phon;

	this.Mtq30uncue=null;


	function getMtq30uncue() {
		return this.Mtq30uncue;
	}
	this.getMtq30uncue=getMtq30uncue;


	function setMtq30uncue(v){
		this.Mtq30uncue=v;
	}
	this.setMtq30uncue=setMtq30uncue;

	this.Mtq30msip=null;


	function getMtq30msip() {
		return this.Mtq30msip;
	}
	this.getMtq30msip=getMtq30msip;


	function setMtq30msip(v){
		this.Mtq30msip=v;
	}
	this.setMtq30msip=setMtq30msip;

	this.Mtq30sem=null;


	function getMtq30sem() {
		return this.Mtq30sem;
	}
	this.getMtq30sem=getMtq30sem;


	function setMtq30sem(v){
		this.Mtq30sem=v;
	}
	this.setMtq30sem=setMtq30sem;

	this.Mtq30phon=null;


	function getMtq30phon() {
		return this.Mtq30phon;
	}
	this.getMtq30phon=getMtq30phon;


	function setMtq30phon(v){
		this.Mtq30phon=v;
	}
	this.setMtq30phon=setMtq30phon;

	this.Mtq31uncue=null;


	function getMtq31uncue() {
		return this.Mtq31uncue;
	}
	this.getMtq31uncue=getMtq31uncue;


	function setMtq31uncue(v){
		this.Mtq31uncue=v;
	}
	this.setMtq31uncue=setMtq31uncue;

	this.Mtq31msip=null;


	function getMtq31msip() {
		return this.Mtq31msip;
	}
	this.getMtq31msip=getMtq31msip;


	function setMtq31msip(v){
		this.Mtq31msip=v;
	}
	this.setMtq31msip=setMtq31msip;

	this.Mtq31sem=null;


	function getMtq31sem() {
		return this.Mtq31sem;
	}
	this.getMtq31sem=getMtq31sem;


	function setMtq31sem(v){
		this.Mtq31sem=v;
	}
	this.setMtq31sem=setMtq31sem;

	this.Mtq31phon=null;


	function getMtq31phon() {
		return this.Mtq31phon;
	}
	this.getMtq31phon=getMtq31phon;


	function setMtq31phon(v){
		this.Mtq31phon=v;
	}
	this.setMtq31phon=setMtq31phon;

	this.Mtq32uncue=null;


	function getMtq32uncue() {
		return this.Mtq32uncue;
	}
	this.getMtq32uncue=getMtq32uncue;


	function setMtq32uncue(v){
		this.Mtq32uncue=v;
	}
	this.setMtq32uncue=setMtq32uncue;

	this.Mtq32msip=null;


	function getMtq32msip() {
		return this.Mtq32msip;
	}
	this.getMtq32msip=getMtq32msip;


	function setMtq32msip(v){
		this.Mtq32msip=v;
	}
	this.setMtq32msip=setMtq32msip;

	this.Mtq32sem=null;


	function getMtq32sem() {
		return this.Mtq32sem;
	}
	this.getMtq32sem=getMtq32sem;


	function setMtq32sem(v){
		this.Mtq32sem=v;
	}
	this.setMtq32sem=setMtq32sem;

	this.Mtq32phon=null;


	function getMtq32phon() {
		return this.Mtq32phon;
	}
	this.getMtq32phon=getMtq32phon;


	function setMtq32phon(v){
		this.Mtq32phon=v;
	}
	this.setMtq32phon=setMtq32phon;

	this.Mttotal=null;


	function getMttotal() {
		return this.Mttotal;
	}
	this.getMttotal=getMttotal;


	function setMttotal(v){
		this.Mttotal=v;
	}
	this.setMttotal=setMttotal;

	this.Mtcomm=null;


	function getMtcomm() {
		return this.Mtcomm;
	}
	this.getMtcomm=getMtcomm;


	function setMtcomm(v){
		this.Mtcomm=v;
	}
	this.setMtcomm=setMtcomm;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="DONE"){
				return this.Done ;
			} else 
			if(xmlPath=="NDREASON"){
				return this.Ndreason ;
			} else 
			if(xmlPath=="MTQ1UNCUE"){
				return this.Mtq1uncue ;
			} else 
			if(xmlPath=="MTQ1MSIP"){
				return this.Mtq1msip ;
			} else 
			if(xmlPath=="MTQ1SEM"){
				return this.Mtq1sem ;
			} else 
			if(xmlPath=="MTQ1PHON"){
				return this.Mtq1phon ;
			} else 
			if(xmlPath=="MTQ2UNCUE"){
				return this.Mtq2uncue ;
			} else 
			if(xmlPath=="MTQ2MSIP"){
				return this.Mtq2msip ;
			} else 
			if(xmlPath=="MTQ2SEM"){
				return this.Mtq2sem ;
			} else 
			if(xmlPath=="MTQ2PHON"){
				return this.Mtq2phon ;
			} else 
			if(xmlPath=="MTQ3UNCUE"){
				return this.Mtq3uncue ;
			} else 
			if(xmlPath=="MTQ3MSIP"){
				return this.Mtq3msip ;
			} else 
			if(xmlPath=="MTQ3SEM"){
				return this.Mtq3sem ;
			} else 
			if(xmlPath=="MTQ3PHON"){
				return this.Mtq3phon ;
			} else 
			if(xmlPath=="MTQ4UNCUE"){
				return this.Mtq4uncue ;
			} else 
			if(xmlPath=="MTQ4MSIP"){
				return this.Mtq4msip ;
			} else 
			if(xmlPath=="MTQ4SEM"){
				return this.Mtq4sem ;
			} else 
			if(xmlPath=="MTQ4PHON"){
				return this.Mtq4phon ;
			} else 
			if(xmlPath=="MTQ5UNCUE"){
				return this.Mtq5uncue ;
			} else 
			if(xmlPath=="MTQ5MSIP"){
				return this.Mtq5msip ;
			} else 
			if(xmlPath=="MTQ5SEM"){
				return this.Mtq5sem ;
			} else 
			if(xmlPath=="MTQ5PHON"){
				return this.Mtq5phon ;
			} else 
			if(xmlPath=="MTQ6UNCUE"){
				return this.Mtq6uncue ;
			} else 
			if(xmlPath=="MTQ6MSIP"){
				return this.Mtq6msip ;
			} else 
			if(xmlPath=="MTQ6SEM"){
				return this.Mtq6sem ;
			} else 
			if(xmlPath=="MTQ6PHON"){
				return this.Mtq6phon ;
			} else 
			if(xmlPath=="MTQ7UNCUE"){
				return this.Mtq7uncue ;
			} else 
			if(xmlPath=="MTQ7MSIP"){
				return this.Mtq7msip ;
			} else 
			if(xmlPath=="MTQ7SEM"){
				return this.Mtq7sem ;
			} else 
			if(xmlPath=="MTQ7PHON"){
				return this.Mtq7phon ;
			} else 
			if(xmlPath=="MTQ8UNCUE"){
				return this.Mtq8uncue ;
			} else 
			if(xmlPath=="MTQ8MSIP"){
				return this.Mtq8msip ;
			} else 
			if(xmlPath=="MTQ8SEM"){
				return this.Mtq8sem ;
			} else 
			if(xmlPath=="MTQ8PHON"){
				return this.Mtq8phon ;
			} else 
			if(xmlPath=="MTQ9UNCUE"){
				return this.Mtq9uncue ;
			} else 
			if(xmlPath=="MTQ9MSIP"){
				return this.Mtq9msip ;
			} else 
			if(xmlPath=="MTQ9SEM"){
				return this.Mtq9sem ;
			} else 
			if(xmlPath=="MTQ9PHON"){
				return this.Mtq9phon ;
			} else 
			if(xmlPath=="MTQ10UNCUE"){
				return this.Mtq10uncue ;
			} else 
			if(xmlPath=="MTQ10MSIP"){
				return this.Mtq10msip ;
			} else 
			if(xmlPath=="MTQ10SEM"){
				return this.Mtq10sem ;
			} else 
			if(xmlPath=="MTQ10PHON"){
				return this.Mtq10phon ;
			} else 
			if(xmlPath=="MTQ11UNCUE"){
				return this.Mtq11uncue ;
			} else 
			if(xmlPath=="MTQ11MSIP"){
				return this.Mtq11msip ;
			} else 
			if(xmlPath=="MTQ11SEM"){
				return this.Mtq11sem ;
			} else 
			if(xmlPath=="MTQ11PHON"){
				return this.Mtq11phon ;
			} else 
			if(xmlPath=="MTQ12UNCUE"){
				return this.Mtq12uncue ;
			} else 
			if(xmlPath=="MTQ12MSIP"){
				return this.Mtq12msip ;
			} else 
			if(xmlPath=="MTQ12SEM"){
				return this.Mtq12sem ;
			} else 
			if(xmlPath=="MTQ12PHON"){
				return this.Mtq12phon ;
			} else 
			if(xmlPath=="MTQ13UNCUE"){
				return this.Mtq13uncue ;
			} else 
			if(xmlPath=="MTQ13MSIP"){
				return this.Mtq13msip ;
			} else 
			if(xmlPath=="MTQ13SEM"){
				return this.Mtq13sem ;
			} else 
			if(xmlPath=="MTQ13PHON"){
				return this.Mtq13phon ;
			} else 
			if(xmlPath=="MTQ14UNCUE"){
				return this.Mtq14uncue ;
			} else 
			if(xmlPath=="MTQ14MSIP"){
				return this.Mtq14msip ;
			} else 
			if(xmlPath=="MTQ14SEM"){
				return this.Mtq14sem ;
			} else 
			if(xmlPath=="MTQ14PHON"){
				return this.Mtq14phon ;
			} else 
			if(xmlPath=="MTQ15UNCUE"){
				return this.Mtq15uncue ;
			} else 
			if(xmlPath=="MTQ15MSIP"){
				return this.Mtq15msip ;
			} else 
			if(xmlPath=="MTQ15SEM"){
				return this.Mtq15sem ;
			} else 
			if(xmlPath=="MTQ15PHON"){
				return this.Mtq15phon ;
			} else 
			if(xmlPath=="MTQ16UNCUE"){
				return this.Mtq16uncue ;
			} else 
			if(xmlPath=="MTQ16MSIP"){
				return this.Mtq16msip ;
			} else 
			if(xmlPath=="MTQ16SEM"){
				return this.Mtq16sem ;
			} else 
			if(xmlPath=="MTQ16PHON"){
				return this.Mtq16phon ;
			} else 
			if(xmlPath=="MTQ17UNCUE"){
				return this.Mtq17uncue ;
			} else 
			if(xmlPath=="MTQ17MSIP"){
				return this.Mtq17msip ;
			} else 
			if(xmlPath=="MTQ17SEM"){
				return this.Mtq17sem ;
			} else 
			if(xmlPath=="MTQ17PHON"){
				return this.Mtq17phon ;
			} else 
			if(xmlPath=="MTQ18UNCUE"){
				return this.Mtq18uncue ;
			} else 
			if(xmlPath=="MTQ18MSIP"){
				return this.Mtq18msip ;
			} else 
			if(xmlPath=="MTQ18SEM"){
				return this.Mtq18sem ;
			} else 
			if(xmlPath=="MTQ18PHON"){
				return this.Mtq18phon ;
			} else 
			if(xmlPath=="MTQ19UNCUE"){
				return this.Mtq19uncue ;
			} else 
			if(xmlPath=="MTQ19MSIP"){
				return this.Mtq19msip ;
			} else 
			if(xmlPath=="MTQ19SEM"){
				return this.Mtq19sem ;
			} else 
			if(xmlPath=="MTQ19PHON"){
				return this.Mtq19phon ;
			} else 
			if(xmlPath=="MTQ20UNCUE"){
				return this.Mtq20uncue ;
			} else 
			if(xmlPath=="MTQ20MSIP"){
				return this.Mtq20msip ;
			} else 
			if(xmlPath=="MTQ20SEM"){
				return this.Mtq20sem ;
			} else 
			if(xmlPath=="MTQ20PHON"){
				return this.Mtq20phon ;
			} else 
			if(xmlPath=="MTQ21UNCUE"){
				return this.Mtq21uncue ;
			} else 
			if(xmlPath=="MTQ21MSIP"){
				return this.Mtq21msip ;
			} else 
			if(xmlPath=="MTQ21SEM"){
				return this.Mtq21sem ;
			} else 
			if(xmlPath=="MTQ21PHON"){
				return this.Mtq21phon ;
			} else 
			if(xmlPath=="MTQ22UNCUE"){
				return this.Mtq22uncue ;
			} else 
			if(xmlPath=="MTQ22MSIP"){
				return this.Mtq22msip ;
			} else 
			if(xmlPath=="MTQ22SEM"){
				return this.Mtq22sem ;
			} else 
			if(xmlPath=="MTQ22PHON"){
				return this.Mtq22phon ;
			} else 
			if(xmlPath=="MTQ23UNCUE"){
				return this.Mtq23uncue ;
			} else 
			if(xmlPath=="MTQ23MSIP"){
				return this.Mtq23msip ;
			} else 
			if(xmlPath=="MTQ23SEM"){
				return this.Mtq23sem ;
			} else 
			if(xmlPath=="MTQ23PHON"){
				return this.Mtq23phon ;
			} else 
			if(xmlPath=="MTQ24UNCUE"){
				return this.Mtq24uncue ;
			} else 
			if(xmlPath=="MTQ24MSIP"){
				return this.Mtq24msip ;
			} else 
			if(xmlPath=="MTQ24SEM"){
				return this.Mtq24sem ;
			} else 
			if(xmlPath=="MTQ24PHON"){
				return this.Mtq24phon ;
			} else 
			if(xmlPath=="MTQ25UNCUE"){
				return this.Mtq25uncue ;
			} else 
			if(xmlPath=="MTQ25MSIP"){
				return this.Mtq25msip ;
			} else 
			if(xmlPath=="MTQ25SEM"){
				return this.Mtq25sem ;
			} else 
			if(xmlPath=="MTQ25PHON"){
				return this.Mtq25phon ;
			} else 
			if(xmlPath=="MTQ26UNCUE"){
				return this.Mtq26uncue ;
			} else 
			if(xmlPath=="MTQ26MSIP"){
				return this.Mtq26msip ;
			} else 
			if(xmlPath=="MTQ26SEM"){
				return this.Mtq26sem ;
			} else 
			if(xmlPath=="MTQ26PHON"){
				return this.Mtq26phon ;
			} else 
			if(xmlPath=="MTQ27UNCUE"){
				return this.Mtq27uncue ;
			} else 
			if(xmlPath=="MTQ27MSIP"){
				return this.Mtq27msip ;
			} else 
			if(xmlPath=="MTQ27SEM"){
				return this.Mtq27sem ;
			} else 
			if(xmlPath=="MTQ27PHON"){
				return this.Mtq27phon ;
			} else 
			if(xmlPath=="MTQ28UNCUE"){
				return this.Mtq28uncue ;
			} else 
			if(xmlPath=="MTQ28MSIP"){
				return this.Mtq28msip ;
			} else 
			if(xmlPath=="MTQ28SEM"){
				return this.Mtq28sem ;
			} else 
			if(xmlPath=="MTQ28PHON"){
				return this.Mtq28phon ;
			} else 
			if(xmlPath=="MTQ29UNCUE"){
				return this.Mtq29uncue ;
			} else 
			if(xmlPath=="MTQ29MSIP"){
				return this.Mtq29msip ;
			} else 
			if(xmlPath=="MTQ29SEM"){
				return this.Mtq29sem ;
			} else 
			if(xmlPath=="MTQ29PHON"){
				return this.Mtq29phon ;
			} else 
			if(xmlPath=="MTQ30UNCUE"){
				return this.Mtq30uncue ;
			} else 
			if(xmlPath=="MTQ30MSIP"){
				return this.Mtq30msip ;
			} else 
			if(xmlPath=="MTQ30SEM"){
				return this.Mtq30sem ;
			} else 
			if(xmlPath=="MTQ30PHON"){
				return this.Mtq30phon ;
			} else 
			if(xmlPath=="MTQ31UNCUE"){
				return this.Mtq31uncue ;
			} else 
			if(xmlPath=="MTQ31MSIP"){
				return this.Mtq31msip ;
			} else 
			if(xmlPath=="MTQ31SEM"){
				return this.Mtq31sem ;
			} else 
			if(xmlPath=="MTQ31PHON"){
				return this.Mtq31phon ;
			} else 
			if(xmlPath=="MTQ32UNCUE"){
				return this.Mtq32uncue ;
			} else 
			if(xmlPath=="MTQ32MSIP"){
				return this.Mtq32msip ;
			} else 
			if(xmlPath=="MTQ32SEM"){
				return this.Mtq32sem ;
			} else 
			if(xmlPath=="MTQ32PHON"){
				return this.Mtq32phon ;
			} else 
			if(xmlPath=="MTTOTAL"){
				return this.Mttotal ;
			} else 
			if(xmlPath=="MTCOMM"){
				return this.Mtcomm ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="DONE"){
				this.Done=value;
			} else 
			if(xmlPath=="NDREASON"){
				this.Ndreason=value;
			} else 
			if(xmlPath=="MTQ1UNCUE"){
				this.Mtq1uncue=value;
			} else 
			if(xmlPath=="MTQ1MSIP"){
				this.Mtq1msip=value;
			} else 
			if(xmlPath=="MTQ1SEM"){
				this.Mtq1sem=value;
			} else 
			if(xmlPath=="MTQ1PHON"){
				this.Mtq1phon=value;
			} else 
			if(xmlPath=="MTQ2UNCUE"){
				this.Mtq2uncue=value;
			} else 
			if(xmlPath=="MTQ2MSIP"){
				this.Mtq2msip=value;
			} else 
			if(xmlPath=="MTQ2SEM"){
				this.Mtq2sem=value;
			} else 
			if(xmlPath=="MTQ2PHON"){
				this.Mtq2phon=value;
			} else 
			if(xmlPath=="MTQ3UNCUE"){
				this.Mtq3uncue=value;
			} else 
			if(xmlPath=="MTQ3MSIP"){
				this.Mtq3msip=value;
			} else 
			if(xmlPath=="MTQ3SEM"){
				this.Mtq3sem=value;
			} else 
			if(xmlPath=="MTQ3PHON"){
				this.Mtq3phon=value;
			} else 
			if(xmlPath=="MTQ4UNCUE"){
				this.Mtq4uncue=value;
			} else 
			if(xmlPath=="MTQ4MSIP"){
				this.Mtq4msip=value;
			} else 
			if(xmlPath=="MTQ4SEM"){
				this.Mtq4sem=value;
			} else 
			if(xmlPath=="MTQ4PHON"){
				this.Mtq4phon=value;
			} else 
			if(xmlPath=="MTQ5UNCUE"){
				this.Mtq5uncue=value;
			} else 
			if(xmlPath=="MTQ5MSIP"){
				this.Mtq5msip=value;
			} else 
			if(xmlPath=="MTQ5SEM"){
				this.Mtq5sem=value;
			} else 
			if(xmlPath=="MTQ5PHON"){
				this.Mtq5phon=value;
			} else 
			if(xmlPath=="MTQ6UNCUE"){
				this.Mtq6uncue=value;
			} else 
			if(xmlPath=="MTQ6MSIP"){
				this.Mtq6msip=value;
			} else 
			if(xmlPath=="MTQ6SEM"){
				this.Mtq6sem=value;
			} else 
			if(xmlPath=="MTQ6PHON"){
				this.Mtq6phon=value;
			} else 
			if(xmlPath=="MTQ7UNCUE"){
				this.Mtq7uncue=value;
			} else 
			if(xmlPath=="MTQ7MSIP"){
				this.Mtq7msip=value;
			} else 
			if(xmlPath=="MTQ7SEM"){
				this.Mtq7sem=value;
			} else 
			if(xmlPath=="MTQ7PHON"){
				this.Mtq7phon=value;
			} else 
			if(xmlPath=="MTQ8UNCUE"){
				this.Mtq8uncue=value;
			} else 
			if(xmlPath=="MTQ8MSIP"){
				this.Mtq8msip=value;
			} else 
			if(xmlPath=="MTQ8SEM"){
				this.Mtq8sem=value;
			} else 
			if(xmlPath=="MTQ8PHON"){
				this.Mtq8phon=value;
			} else 
			if(xmlPath=="MTQ9UNCUE"){
				this.Mtq9uncue=value;
			} else 
			if(xmlPath=="MTQ9MSIP"){
				this.Mtq9msip=value;
			} else 
			if(xmlPath=="MTQ9SEM"){
				this.Mtq9sem=value;
			} else 
			if(xmlPath=="MTQ9PHON"){
				this.Mtq9phon=value;
			} else 
			if(xmlPath=="MTQ10UNCUE"){
				this.Mtq10uncue=value;
			} else 
			if(xmlPath=="MTQ10MSIP"){
				this.Mtq10msip=value;
			} else 
			if(xmlPath=="MTQ10SEM"){
				this.Mtq10sem=value;
			} else 
			if(xmlPath=="MTQ10PHON"){
				this.Mtq10phon=value;
			} else 
			if(xmlPath=="MTQ11UNCUE"){
				this.Mtq11uncue=value;
			} else 
			if(xmlPath=="MTQ11MSIP"){
				this.Mtq11msip=value;
			} else 
			if(xmlPath=="MTQ11SEM"){
				this.Mtq11sem=value;
			} else 
			if(xmlPath=="MTQ11PHON"){
				this.Mtq11phon=value;
			} else 
			if(xmlPath=="MTQ12UNCUE"){
				this.Mtq12uncue=value;
			} else 
			if(xmlPath=="MTQ12MSIP"){
				this.Mtq12msip=value;
			} else 
			if(xmlPath=="MTQ12SEM"){
				this.Mtq12sem=value;
			} else 
			if(xmlPath=="MTQ12PHON"){
				this.Mtq12phon=value;
			} else 
			if(xmlPath=="MTQ13UNCUE"){
				this.Mtq13uncue=value;
			} else 
			if(xmlPath=="MTQ13MSIP"){
				this.Mtq13msip=value;
			} else 
			if(xmlPath=="MTQ13SEM"){
				this.Mtq13sem=value;
			} else 
			if(xmlPath=="MTQ13PHON"){
				this.Mtq13phon=value;
			} else 
			if(xmlPath=="MTQ14UNCUE"){
				this.Mtq14uncue=value;
			} else 
			if(xmlPath=="MTQ14MSIP"){
				this.Mtq14msip=value;
			} else 
			if(xmlPath=="MTQ14SEM"){
				this.Mtq14sem=value;
			} else 
			if(xmlPath=="MTQ14PHON"){
				this.Mtq14phon=value;
			} else 
			if(xmlPath=="MTQ15UNCUE"){
				this.Mtq15uncue=value;
			} else 
			if(xmlPath=="MTQ15MSIP"){
				this.Mtq15msip=value;
			} else 
			if(xmlPath=="MTQ15SEM"){
				this.Mtq15sem=value;
			} else 
			if(xmlPath=="MTQ15PHON"){
				this.Mtq15phon=value;
			} else 
			if(xmlPath=="MTQ16UNCUE"){
				this.Mtq16uncue=value;
			} else 
			if(xmlPath=="MTQ16MSIP"){
				this.Mtq16msip=value;
			} else 
			if(xmlPath=="MTQ16SEM"){
				this.Mtq16sem=value;
			} else 
			if(xmlPath=="MTQ16PHON"){
				this.Mtq16phon=value;
			} else 
			if(xmlPath=="MTQ17UNCUE"){
				this.Mtq17uncue=value;
			} else 
			if(xmlPath=="MTQ17MSIP"){
				this.Mtq17msip=value;
			} else 
			if(xmlPath=="MTQ17SEM"){
				this.Mtq17sem=value;
			} else 
			if(xmlPath=="MTQ17PHON"){
				this.Mtq17phon=value;
			} else 
			if(xmlPath=="MTQ18UNCUE"){
				this.Mtq18uncue=value;
			} else 
			if(xmlPath=="MTQ18MSIP"){
				this.Mtq18msip=value;
			} else 
			if(xmlPath=="MTQ18SEM"){
				this.Mtq18sem=value;
			} else 
			if(xmlPath=="MTQ18PHON"){
				this.Mtq18phon=value;
			} else 
			if(xmlPath=="MTQ19UNCUE"){
				this.Mtq19uncue=value;
			} else 
			if(xmlPath=="MTQ19MSIP"){
				this.Mtq19msip=value;
			} else 
			if(xmlPath=="MTQ19SEM"){
				this.Mtq19sem=value;
			} else 
			if(xmlPath=="MTQ19PHON"){
				this.Mtq19phon=value;
			} else 
			if(xmlPath=="MTQ20UNCUE"){
				this.Mtq20uncue=value;
			} else 
			if(xmlPath=="MTQ20MSIP"){
				this.Mtq20msip=value;
			} else 
			if(xmlPath=="MTQ20SEM"){
				this.Mtq20sem=value;
			} else 
			if(xmlPath=="MTQ20PHON"){
				this.Mtq20phon=value;
			} else 
			if(xmlPath=="MTQ21UNCUE"){
				this.Mtq21uncue=value;
			} else 
			if(xmlPath=="MTQ21MSIP"){
				this.Mtq21msip=value;
			} else 
			if(xmlPath=="MTQ21SEM"){
				this.Mtq21sem=value;
			} else 
			if(xmlPath=="MTQ21PHON"){
				this.Mtq21phon=value;
			} else 
			if(xmlPath=="MTQ22UNCUE"){
				this.Mtq22uncue=value;
			} else 
			if(xmlPath=="MTQ22MSIP"){
				this.Mtq22msip=value;
			} else 
			if(xmlPath=="MTQ22SEM"){
				this.Mtq22sem=value;
			} else 
			if(xmlPath=="MTQ22PHON"){
				this.Mtq22phon=value;
			} else 
			if(xmlPath=="MTQ23UNCUE"){
				this.Mtq23uncue=value;
			} else 
			if(xmlPath=="MTQ23MSIP"){
				this.Mtq23msip=value;
			} else 
			if(xmlPath=="MTQ23SEM"){
				this.Mtq23sem=value;
			} else 
			if(xmlPath=="MTQ23PHON"){
				this.Mtq23phon=value;
			} else 
			if(xmlPath=="MTQ24UNCUE"){
				this.Mtq24uncue=value;
			} else 
			if(xmlPath=="MTQ24MSIP"){
				this.Mtq24msip=value;
			} else 
			if(xmlPath=="MTQ24SEM"){
				this.Mtq24sem=value;
			} else 
			if(xmlPath=="MTQ24PHON"){
				this.Mtq24phon=value;
			} else 
			if(xmlPath=="MTQ25UNCUE"){
				this.Mtq25uncue=value;
			} else 
			if(xmlPath=="MTQ25MSIP"){
				this.Mtq25msip=value;
			} else 
			if(xmlPath=="MTQ25SEM"){
				this.Mtq25sem=value;
			} else 
			if(xmlPath=="MTQ25PHON"){
				this.Mtq25phon=value;
			} else 
			if(xmlPath=="MTQ26UNCUE"){
				this.Mtq26uncue=value;
			} else 
			if(xmlPath=="MTQ26MSIP"){
				this.Mtq26msip=value;
			} else 
			if(xmlPath=="MTQ26SEM"){
				this.Mtq26sem=value;
			} else 
			if(xmlPath=="MTQ26PHON"){
				this.Mtq26phon=value;
			} else 
			if(xmlPath=="MTQ27UNCUE"){
				this.Mtq27uncue=value;
			} else 
			if(xmlPath=="MTQ27MSIP"){
				this.Mtq27msip=value;
			} else 
			if(xmlPath=="MTQ27SEM"){
				this.Mtq27sem=value;
			} else 
			if(xmlPath=="MTQ27PHON"){
				this.Mtq27phon=value;
			} else 
			if(xmlPath=="MTQ28UNCUE"){
				this.Mtq28uncue=value;
			} else 
			if(xmlPath=="MTQ28MSIP"){
				this.Mtq28msip=value;
			} else 
			if(xmlPath=="MTQ28SEM"){
				this.Mtq28sem=value;
			} else 
			if(xmlPath=="MTQ28PHON"){
				this.Mtq28phon=value;
			} else 
			if(xmlPath=="MTQ29UNCUE"){
				this.Mtq29uncue=value;
			} else 
			if(xmlPath=="MTQ29MSIP"){
				this.Mtq29msip=value;
			} else 
			if(xmlPath=="MTQ29SEM"){
				this.Mtq29sem=value;
			} else 
			if(xmlPath=="MTQ29PHON"){
				this.Mtq29phon=value;
			} else 
			if(xmlPath=="MTQ30UNCUE"){
				this.Mtq30uncue=value;
			} else 
			if(xmlPath=="MTQ30MSIP"){
				this.Mtq30msip=value;
			} else 
			if(xmlPath=="MTQ30SEM"){
				this.Mtq30sem=value;
			} else 
			if(xmlPath=="MTQ30PHON"){
				this.Mtq30phon=value;
			} else 
			if(xmlPath=="MTQ31UNCUE"){
				this.Mtq31uncue=value;
			} else 
			if(xmlPath=="MTQ31MSIP"){
				this.Mtq31msip=value;
			} else 
			if(xmlPath=="MTQ31SEM"){
				this.Mtq31sem=value;
			} else 
			if(xmlPath=="MTQ31PHON"){
				this.Mtq31phon=value;
			} else 
			if(xmlPath=="MTQ32UNCUE"){
				this.Mtq32uncue=value;
			} else 
			if(xmlPath=="MTQ32MSIP"){
				this.Mtq32msip=value;
			} else 
			if(xmlPath=="MTQ32SEM"){
				this.Mtq32sem=value;
			} else 
			if(xmlPath=="MTQ32PHON"){
				this.Mtq32phon=value;
			} else 
			if(xmlPath=="MTTOTAL"){
				this.Mttotal=value;
			} else 
			if(xmlPath=="MTCOMM"){
				this.Mtcomm=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="DONE"){
			return "field_data";
		}else if (xmlPath=="NDREASON"){
			return "field_data";
		}else if (xmlPath=="MTQ1UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ1MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ1SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ1PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ2UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ2MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ2SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ2PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ3UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ3MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ3SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ3PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ4UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ4MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ4SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ4PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ5UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ5MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ5SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ5PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ6UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ6MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ6SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ6PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ7UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ7MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ7SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ7PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ8UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ8MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ8SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ8PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ9UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ9MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ9SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ9PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ10UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ10MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ10SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ10PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ11UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ11MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ11SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ11PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ12UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ12MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ12SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ12PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ13UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ13MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ13SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ13PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ14UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ14MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ14SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ14PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ15UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ15MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ15SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ15PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ16UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ16MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ16SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ16PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ17UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ17MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ17SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ17PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ18UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ18MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ18SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ18PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ19UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ19MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ19SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ19PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ20UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ20MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ20SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ20PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ21UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ21MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ21SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ21PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ22UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ22MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ22SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ22PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ23UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ23MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ23SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ23PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ24UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ24MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ24SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ24PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ25UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ25MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ25SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ25PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ26UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ26MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ26SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ26PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ27UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ27MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ27SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ27PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ28UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ28MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ28SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ28PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ29UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ29MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ29SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ29PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ30UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ30MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ30SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ30PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ31UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ31MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ31SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ31PHON"){
			return "field_data";
		}else if (xmlPath=="MTQ32UNCUE"){
			return "field_data";
		}else if (xmlPath=="MTQ32MSIP"){
			return "field_data";
		}else if (xmlPath=="MTQ32SEM"){
			return "field_data";
		}else if (xmlPath=="MTQ32PHON"){
			return "field_data";
		}else if (xmlPath=="MTTOTAL"){
			return "field_data";
		}else if (xmlPath=="MTCOMM"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<dian:MINT";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</dian:MINT>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Done!=null){
			xmlTxt+="\n<dian:DONE";
			xmlTxt+=">";
			xmlTxt+=this.Done;
			xmlTxt+="</dian:DONE>";
		}
		if (this.Ndreason!=null){
			xmlTxt+="\n<dian:NDREASON";
			xmlTxt+=">";
			xmlTxt+=this.Ndreason;
			xmlTxt+="</dian:NDREASON>";
		}
		if (this.Mtq1uncue!=null){
			xmlTxt+="\n<dian:MTQ1UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq1uncue;
			xmlTxt+="</dian:MTQ1UNCUE>";
		}
		if (this.Mtq1msip!=null){
			xmlTxt+="\n<dian:MTQ1MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq1msip;
			xmlTxt+="</dian:MTQ1MSIP>";
		}
		if (this.Mtq1sem!=null){
			xmlTxt+="\n<dian:MTQ1SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq1sem;
			xmlTxt+="</dian:MTQ1SEM>";
		}
		if (this.Mtq1phon!=null){
			xmlTxt+="\n<dian:MTQ1PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq1phon;
			xmlTxt+="</dian:MTQ1PHON>";
		}
		if (this.Mtq2uncue!=null){
			xmlTxt+="\n<dian:MTQ2UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq2uncue;
			xmlTxt+="</dian:MTQ2UNCUE>";
		}
		if (this.Mtq2msip!=null){
			xmlTxt+="\n<dian:MTQ2MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq2msip;
			xmlTxt+="</dian:MTQ2MSIP>";
		}
		if (this.Mtq2sem!=null){
			xmlTxt+="\n<dian:MTQ2SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq2sem;
			xmlTxt+="</dian:MTQ2SEM>";
		}
		if (this.Mtq2phon!=null){
			xmlTxt+="\n<dian:MTQ2PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq2phon;
			xmlTxt+="</dian:MTQ2PHON>";
		}
		if (this.Mtq3uncue!=null){
			xmlTxt+="\n<dian:MTQ3UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq3uncue;
			xmlTxt+="</dian:MTQ3UNCUE>";
		}
		if (this.Mtq3msip!=null){
			xmlTxt+="\n<dian:MTQ3MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq3msip;
			xmlTxt+="</dian:MTQ3MSIP>";
		}
		if (this.Mtq3sem!=null){
			xmlTxt+="\n<dian:MTQ3SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq3sem;
			xmlTxt+="</dian:MTQ3SEM>";
		}
		if (this.Mtq3phon!=null){
			xmlTxt+="\n<dian:MTQ3PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq3phon;
			xmlTxt+="</dian:MTQ3PHON>";
		}
		if (this.Mtq4uncue!=null){
			xmlTxt+="\n<dian:MTQ4UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq4uncue;
			xmlTxt+="</dian:MTQ4UNCUE>";
		}
		if (this.Mtq4msip!=null){
			xmlTxt+="\n<dian:MTQ4MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq4msip;
			xmlTxt+="</dian:MTQ4MSIP>";
		}
		if (this.Mtq4sem!=null){
			xmlTxt+="\n<dian:MTQ4SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq4sem;
			xmlTxt+="</dian:MTQ4SEM>";
		}
		if (this.Mtq4phon!=null){
			xmlTxt+="\n<dian:MTQ4PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq4phon;
			xmlTxt+="</dian:MTQ4PHON>";
		}
		if (this.Mtq5uncue!=null){
			xmlTxt+="\n<dian:MTQ5UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq5uncue;
			xmlTxt+="</dian:MTQ5UNCUE>";
		}
		if (this.Mtq5msip!=null){
			xmlTxt+="\n<dian:MTQ5MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq5msip;
			xmlTxt+="</dian:MTQ5MSIP>";
		}
		if (this.Mtq5sem!=null){
			xmlTxt+="\n<dian:MTQ5SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq5sem;
			xmlTxt+="</dian:MTQ5SEM>";
		}
		if (this.Mtq5phon!=null){
			xmlTxt+="\n<dian:MTQ5PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq5phon;
			xmlTxt+="</dian:MTQ5PHON>";
		}
		if (this.Mtq6uncue!=null){
			xmlTxt+="\n<dian:MTQ6UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq6uncue;
			xmlTxt+="</dian:MTQ6UNCUE>";
		}
		if (this.Mtq6msip!=null){
			xmlTxt+="\n<dian:MTQ6MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq6msip;
			xmlTxt+="</dian:MTQ6MSIP>";
		}
		if (this.Mtq6sem!=null){
			xmlTxt+="\n<dian:MTQ6SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq6sem;
			xmlTxt+="</dian:MTQ6SEM>";
		}
		if (this.Mtq6phon!=null){
			xmlTxt+="\n<dian:MTQ6PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq6phon;
			xmlTxt+="</dian:MTQ6PHON>";
		}
		if (this.Mtq7uncue!=null){
			xmlTxt+="\n<dian:MTQ7UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq7uncue;
			xmlTxt+="</dian:MTQ7UNCUE>";
		}
		if (this.Mtq7msip!=null){
			xmlTxt+="\n<dian:MTQ7MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq7msip;
			xmlTxt+="</dian:MTQ7MSIP>";
		}
		if (this.Mtq7sem!=null){
			xmlTxt+="\n<dian:MTQ7SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq7sem;
			xmlTxt+="</dian:MTQ7SEM>";
		}
		if (this.Mtq7phon!=null){
			xmlTxt+="\n<dian:MTQ7PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq7phon;
			xmlTxt+="</dian:MTQ7PHON>";
		}
		if (this.Mtq8uncue!=null){
			xmlTxt+="\n<dian:MTQ8UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq8uncue;
			xmlTxt+="</dian:MTQ8UNCUE>";
		}
		if (this.Mtq8msip!=null){
			xmlTxt+="\n<dian:MTQ8MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq8msip;
			xmlTxt+="</dian:MTQ8MSIP>";
		}
		if (this.Mtq8sem!=null){
			xmlTxt+="\n<dian:MTQ8SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq8sem;
			xmlTxt+="</dian:MTQ8SEM>";
		}
		if (this.Mtq8phon!=null){
			xmlTxt+="\n<dian:MTQ8PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq8phon;
			xmlTxt+="</dian:MTQ8PHON>";
		}
		if (this.Mtq9uncue!=null){
			xmlTxt+="\n<dian:MTQ9UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq9uncue;
			xmlTxt+="</dian:MTQ9UNCUE>";
		}
		if (this.Mtq9msip!=null){
			xmlTxt+="\n<dian:MTQ9MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq9msip;
			xmlTxt+="</dian:MTQ9MSIP>";
		}
		if (this.Mtq9sem!=null){
			xmlTxt+="\n<dian:MTQ9SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq9sem;
			xmlTxt+="</dian:MTQ9SEM>";
		}
		if (this.Mtq9phon!=null){
			xmlTxt+="\n<dian:MTQ9PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq9phon;
			xmlTxt+="</dian:MTQ9PHON>";
		}
		if (this.Mtq10uncue!=null){
			xmlTxt+="\n<dian:MTQ10UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq10uncue;
			xmlTxt+="</dian:MTQ10UNCUE>";
		}
		if (this.Mtq10msip!=null){
			xmlTxt+="\n<dian:MTQ10MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq10msip;
			xmlTxt+="</dian:MTQ10MSIP>";
		}
		if (this.Mtq10sem!=null){
			xmlTxt+="\n<dian:MTQ10SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq10sem;
			xmlTxt+="</dian:MTQ10SEM>";
		}
		if (this.Mtq10phon!=null){
			xmlTxt+="\n<dian:MTQ10PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq10phon;
			xmlTxt+="</dian:MTQ10PHON>";
		}
		if (this.Mtq11uncue!=null){
			xmlTxt+="\n<dian:MTQ11UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq11uncue;
			xmlTxt+="</dian:MTQ11UNCUE>";
		}
		if (this.Mtq11msip!=null){
			xmlTxt+="\n<dian:MTQ11MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq11msip;
			xmlTxt+="</dian:MTQ11MSIP>";
		}
		if (this.Mtq11sem!=null){
			xmlTxt+="\n<dian:MTQ11SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq11sem;
			xmlTxt+="</dian:MTQ11SEM>";
		}
		if (this.Mtq11phon!=null){
			xmlTxt+="\n<dian:MTQ11PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq11phon;
			xmlTxt+="</dian:MTQ11PHON>";
		}
		if (this.Mtq12uncue!=null){
			xmlTxt+="\n<dian:MTQ12UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq12uncue;
			xmlTxt+="</dian:MTQ12UNCUE>";
		}
		if (this.Mtq12msip!=null){
			xmlTxt+="\n<dian:MTQ12MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq12msip;
			xmlTxt+="</dian:MTQ12MSIP>";
		}
		if (this.Mtq12sem!=null){
			xmlTxt+="\n<dian:MTQ12SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq12sem;
			xmlTxt+="</dian:MTQ12SEM>";
		}
		if (this.Mtq12phon!=null){
			xmlTxt+="\n<dian:MTQ12PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq12phon;
			xmlTxt+="</dian:MTQ12PHON>";
		}
		if (this.Mtq13uncue!=null){
			xmlTxt+="\n<dian:MTQ13UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq13uncue;
			xmlTxt+="</dian:MTQ13UNCUE>";
		}
		if (this.Mtq13msip!=null){
			xmlTxt+="\n<dian:MTQ13MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq13msip;
			xmlTxt+="</dian:MTQ13MSIP>";
		}
		if (this.Mtq13sem!=null){
			xmlTxt+="\n<dian:MTQ13SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq13sem;
			xmlTxt+="</dian:MTQ13SEM>";
		}
		if (this.Mtq13phon!=null){
			xmlTxt+="\n<dian:MTQ13PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq13phon;
			xmlTxt+="</dian:MTQ13PHON>";
		}
		if (this.Mtq14uncue!=null){
			xmlTxt+="\n<dian:MTQ14UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq14uncue;
			xmlTxt+="</dian:MTQ14UNCUE>";
		}
		if (this.Mtq14msip!=null){
			xmlTxt+="\n<dian:MTQ14MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq14msip;
			xmlTxt+="</dian:MTQ14MSIP>";
		}
		if (this.Mtq14sem!=null){
			xmlTxt+="\n<dian:MTQ14SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq14sem;
			xmlTxt+="</dian:MTQ14SEM>";
		}
		if (this.Mtq14phon!=null){
			xmlTxt+="\n<dian:MTQ14PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq14phon;
			xmlTxt+="</dian:MTQ14PHON>";
		}
		if (this.Mtq15uncue!=null){
			xmlTxt+="\n<dian:MTQ15UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq15uncue;
			xmlTxt+="</dian:MTQ15UNCUE>";
		}
		if (this.Mtq15msip!=null){
			xmlTxt+="\n<dian:MTQ15MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq15msip;
			xmlTxt+="</dian:MTQ15MSIP>";
		}
		if (this.Mtq15sem!=null){
			xmlTxt+="\n<dian:MTQ15SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq15sem;
			xmlTxt+="</dian:MTQ15SEM>";
		}
		if (this.Mtq15phon!=null){
			xmlTxt+="\n<dian:MTQ15PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq15phon;
			xmlTxt+="</dian:MTQ15PHON>";
		}
		if (this.Mtq16uncue!=null){
			xmlTxt+="\n<dian:MTQ16UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq16uncue;
			xmlTxt+="</dian:MTQ16UNCUE>";
		}
		if (this.Mtq16msip!=null){
			xmlTxt+="\n<dian:MTQ16MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq16msip;
			xmlTxt+="</dian:MTQ16MSIP>";
		}
		if (this.Mtq16sem!=null){
			xmlTxt+="\n<dian:MTQ16SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq16sem;
			xmlTxt+="</dian:MTQ16SEM>";
		}
		if (this.Mtq16phon!=null){
			xmlTxt+="\n<dian:MTQ16PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq16phon;
			xmlTxt+="</dian:MTQ16PHON>";
		}
		if (this.Mtq17uncue!=null){
			xmlTxt+="\n<dian:MTQ17UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq17uncue;
			xmlTxt+="</dian:MTQ17UNCUE>";
		}
		if (this.Mtq17msip!=null){
			xmlTxt+="\n<dian:MTQ17MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq17msip;
			xmlTxt+="</dian:MTQ17MSIP>";
		}
		if (this.Mtq17sem!=null){
			xmlTxt+="\n<dian:MTQ17SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq17sem;
			xmlTxt+="</dian:MTQ17SEM>";
		}
		if (this.Mtq17phon!=null){
			xmlTxt+="\n<dian:MTQ17PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq17phon;
			xmlTxt+="</dian:MTQ17PHON>";
		}
		if (this.Mtq18uncue!=null){
			xmlTxt+="\n<dian:MTQ18UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq18uncue;
			xmlTxt+="</dian:MTQ18UNCUE>";
		}
		if (this.Mtq18msip!=null){
			xmlTxt+="\n<dian:MTQ18MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq18msip;
			xmlTxt+="</dian:MTQ18MSIP>";
		}
		if (this.Mtq18sem!=null){
			xmlTxt+="\n<dian:MTQ18SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq18sem;
			xmlTxt+="</dian:MTQ18SEM>";
		}
		if (this.Mtq18phon!=null){
			xmlTxt+="\n<dian:MTQ18PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq18phon;
			xmlTxt+="</dian:MTQ18PHON>";
		}
		if (this.Mtq19uncue!=null){
			xmlTxt+="\n<dian:MTQ19UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq19uncue;
			xmlTxt+="</dian:MTQ19UNCUE>";
		}
		if (this.Mtq19msip!=null){
			xmlTxt+="\n<dian:MTQ19MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq19msip;
			xmlTxt+="</dian:MTQ19MSIP>";
		}
		if (this.Mtq19sem!=null){
			xmlTxt+="\n<dian:MTQ19SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq19sem;
			xmlTxt+="</dian:MTQ19SEM>";
		}
		if (this.Mtq19phon!=null){
			xmlTxt+="\n<dian:MTQ19PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq19phon;
			xmlTxt+="</dian:MTQ19PHON>";
		}
		if (this.Mtq20uncue!=null){
			xmlTxt+="\n<dian:MTQ20UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq20uncue;
			xmlTxt+="</dian:MTQ20UNCUE>";
		}
		if (this.Mtq20msip!=null){
			xmlTxt+="\n<dian:MTQ20MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq20msip;
			xmlTxt+="</dian:MTQ20MSIP>";
		}
		if (this.Mtq20sem!=null){
			xmlTxt+="\n<dian:MTQ20SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq20sem;
			xmlTxt+="</dian:MTQ20SEM>";
		}
		if (this.Mtq20phon!=null){
			xmlTxt+="\n<dian:MTQ20PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq20phon;
			xmlTxt+="</dian:MTQ20PHON>";
		}
		if (this.Mtq21uncue!=null){
			xmlTxt+="\n<dian:MTQ21UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq21uncue;
			xmlTxt+="</dian:MTQ21UNCUE>";
		}
		if (this.Mtq21msip!=null){
			xmlTxt+="\n<dian:MTQ21MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq21msip;
			xmlTxt+="</dian:MTQ21MSIP>";
		}
		if (this.Mtq21sem!=null){
			xmlTxt+="\n<dian:MTQ21SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq21sem;
			xmlTxt+="</dian:MTQ21SEM>";
		}
		if (this.Mtq21phon!=null){
			xmlTxt+="\n<dian:MTQ21PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq21phon;
			xmlTxt+="</dian:MTQ21PHON>";
		}
		if (this.Mtq22uncue!=null){
			xmlTxt+="\n<dian:MTQ22UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq22uncue;
			xmlTxt+="</dian:MTQ22UNCUE>";
		}
		if (this.Mtq22msip!=null){
			xmlTxt+="\n<dian:MTQ22MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq22msip;
			xmlTxt+="</dian:MTQ22MSIP>";
		}
		if (this.Mtq22sem!=null){
			xmlTxt+="\n<dian:MTQ22SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq22sem;
			xmlTxt+="</dian:MTQ22SEM>";
		}
		if (this.Mtq22phon!=null){
			xmlTxt+="\n<dian:MTQ22PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq22phon;
			xmlTxt+="</dian:MTQ22PHON>";
		}
		if (this.Mtq23uncue!=null){
			xmlTxt+="\n<dian:MTQ23UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq23uncue;
			xmlTxt+="</dian:MTQ23UNCUE>";
		}
		if (this.Mtq23msip!=null){
			xmlTxt+="\n<dian:MTQ23MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq23msip;
			xmlTxt+="</dian:MTQ23MSIP>";
		}
		if (this.Mtq23sem!=null){
			xmlTxt+="\n<dian:MTQ23SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq23sem;
			xmlTxt+="</dian:MTQ23SEM>";
		}
		if (this.Mtq23phon!=null){
			xmlTxt+="\n<dian:MTQ23PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq23phon;
			xmlTxt+="</dian:MTQ23PHON>";
		}
		if (this.Mtq24uncue!=null){
			xmlTxt+="\n<dian:MTQ24UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq24uncue;
			xmlTxt+="</dian:MTQ24UNCUE>";
		}
		if (this.Mtq24msip!=null){
			xmlTxt+="\n<dian:MTQ24MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq24msip;
			xmlTxt+="</dian:MTQ24MSIP>";
		}
		if (this.Mtq24sem!=null){
			xmlTxt+="\n<dian:MTQ24SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq24sem;
			xmlTxt+="</dian:MTQ24SEM>";
		}
		if (this.Mtq24phon!=null){
			xmlTxt+="\n<dian:MTQ24PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq24phon;
			xmlTxt+="</dian:MTQ24PHON>";
		}
		if (this.Mtq25uncue!=null){
			xmlTxt+="\n<dian:MTQ25UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq25uncue;
			xmlTxt+="</dian:MTQ25UNCUE>";
		}
		if (this.Mtq25msip!=null){
			xmlTxt+="\n<dian:MTQ25MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq25msip;
			xmlTxt+="</dian:MTQ25MSIP>";
		}
		if (this.Mtq25sem!=null){
			xmlTxt+="\n<dian:MTQ25SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq25sem;
			xmlTxt+="</dian:MTQ25SEM>";
		}
		if (this.Mtq25phon!=null){
			xmlTxt+="\n<dian:MTQ25PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq25phon;
			xmlTxt+="</dian:MTQ25PHON>";
		}
		if (this.Mtq26uncue!=null){
			xmlTxt+="\n<dian:MTQ26UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq26uncue;
			xmlTxt+="</dian:MTQ26UNCUE>";
		}
		if (this.Mtq26msip!=null){
			xmlTxt+="\n<dian:MTQ26MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq26msip;
			xmlTxt+="</dian:MTQ26MSIP>";
		}
		if (this.Mtq26sem!=null){
			xmlTxt+="\n<dian:MTQ26SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq26sem;
			xmlTxt+="</dian:MTQ26SEM>";
		}
		if (this.Mtq26phon!=null){
			xmlTxt+="\n<dian:MTQ26PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq26phon;
			xmlTxt+="</dian:MTQ26PHON>";
		}
		if (this.Mtq27uncue!=null){
			xmlTxt+="\n<dian:MTQ27UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq27uncue;
			xmlTxt+="</dian:MTQ27UNCUE>";
		}
		if (this.Mtq27msip!=null){
			xmlTxt+="\n<dian:MTQ27MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq27msip;
			xmlTxt+="</dian:MTQ27MSIP>";
		}
		if (this.Mtq27sem!=null){
			xmlTxt+="\n<dian:MTQ27SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq27sem;
			xmlTxt+="</dian:MTQ27SEM>";
		}
		if (this.Mtq27phon!=null){
			xmlTxt+="\n<dian:MTQ27PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq27phon;
			xmlTxt+="</dian:MTQ27PHON>";
		}
		if (this.Mtq28uncue!=null){
			xmlTxt+="\n<dian:MTQ28UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq28uncue;
			xmlTxt+="</dian:MTQ28UNCUE>";
		}
		if (this.Mtq28msip!=null){
			xmlTxt+="\n<dian:MTQ28MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq28msip;
			xmlTxt+="</dian:MTQ28MSIP>";
		}
		if (this.Mtq28sem!=null){
			xmlTxt+="\n<dian:MTQ28SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq28sem;
			xmlTxt+="</dian:MTQ28SEM>";
		}
		if (this.Mtq28phon!=null){
			xmlTxt+="\n<dian:MTQ28PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq28phon;
			xmlTxt+="</dian:MTQ28PHON>";
		}
		if (this.Mtq29uncue!=null){
			xmlTxt+="\n<dian:MTQ29UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq29uncue;
			xmlTxt+="</dian:MTQ29UNCUE>";
		}
		if (this.Mtq29msip!=null){
			xmlTxt+="\n<dian:MTQ29MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq29msip;
			xmlTxt+="</dian:MTQ29MSIP>";
		}
		if (this.Mtq29sem!=null){
			xmlTxt+="\n<dian:MTQ29SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq29sem;
			xmlTxt+="</dian:MTQ29SEM>";
		}
		if (this.Mtq29phon!=null){
			xmlTxt+="\n<dian:MTQ29PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq29phon;
			xmlTxt+="</dian:MTQ29PHON>";
		}
		if (this.Mtq30uncue!=null){
			xmlTxt+="\n<dian:MTQ30UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq30uncue;
			xmlTxt+="</dian:MTQ30UNCUE>";
		}
		if (this.Mtq30msip!=null){
			xmlTxt+="\n<dian:MTQ30MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq30msip;
			xmlTxt+="</dian:MTQ30MSIP>";
		}
		if (this.Mtq30sem!=null){
			xmlTxt+="\n<dian:MTQ30SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq30sem;
			xmlTxt+="</dian:MTQ30SEM>";
		}
		if (this.Mtq30phon!=null){
			xmlTxt+="\n<dian:MTQ30PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq30phon;
			xmlTxt+="</dian:MTQ30PHON>";
		}
		if (this.Mtq31uncue!=null){
			xmlTxt+="\n<dian:MTQ31UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq31uncue;
			xmlTxt+="</dian:MTQ31UNCUE>";
		}
		if (this.Mtq31msip!=null){
			xmlTxt+="\n<dian:MTQ31MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq31msip;
			xmlTxt+="</dian:MTQ31MSIP>";
		}
		if (this.Mtq31sem!=null){
			xmlTxt+="\n<dian:MTQ31SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq31sem;
			xmlTxt+="</dian:MTQ31SEM>";
		}
		if (this.Mtq31phon!=null){
			xmlTxt+="\n<dian:MTQ31PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq31phon;
			xmlTxt+="</dian:MTQ31PHON>";
		}
		if (this.Mtq32uncue!=null){
			xmlTxt+="\n<dian:MTQ32UNCUE";
			xmlTxt+=">";
			xmlTxt+=this.Mtq32uncue;
			xmlTxt+="</dian:MTQ32UNCUE>";
		}
		if (this.Mtq32msip!=null){
			xmlTxt+="\n<dian:MTQ32MSIP";
			xmlTxt+=">";
			xmlTxt+=this.Mtq32msip;
			xmlTxt+="</dian:MTQ32MSIP>";
		}
		if (this.Mtq32sem!=null){
			xmlTxt+="\n<dian:MTQ32SEM";
			xmlTxt+=">";
			xmlTxt+=this.Mtq32sem;
			xmlTxt+="</dian:MTQ32SEM>";
		}
		if (this.Mtq32phon!=null){
			xmlTxt+="\n<dian:MTQ32PHON";
			xmlTxt+=">";
			xmlTxt+=this.Mtq32phon;
			xmlTxt+="</dian:MTQ32PHON>";
		}
		if (this.Mttotal!=null){
			xmlTxt+="\n<dian:MTTOTAL";
			xmlTxt+=">";
			xmlTxt+=this.Mttotal;
			xmlTxt+="</dian:MTTOTAL>";
		}
		if (this.Mtcomm!=null){
			xmlTxt+="\n<dian:MTCOMM";
			xmlTxt+=">";
			xmlTxt+=this.Mtcomm.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:MTCOMM>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Done!=null) return true;
		if (this.Ndreason!=null) return true;
		if (this.Mtq1uncue!=null) return true;
		if (this.Mtq1msip!=null) return true;
		if (this.Mtq1sem!=null) return true;
		if (this.Mtq1phon!=null) return true;
		if (this.Mtq2uncue!=null) return true;
		if (this.Mtq2msip!=null) return true;
		if (this.Mtq2sem!=null) return true;
		if (this.Mtq2phon!=null) return true;
		if (this.Mtq3uncue!=null) return true;
		if (this.Mtq3msip!=null) return true;
		if (this.Mtq3sem!=null) return true;
		if (this.Mtq3phon!=null) return true;
		if (this.Mtq4uncue!=null) return true;
		if (this.Mtq4msip!=null) return true;
		if (this.Mtq4sem!=null) return true;
		if (this.Mtq4phon!=null) return true;
		if (this.Mtq5uncue!=null) return true;
		if (this.Mtq5msip!=null) return true;
		if (this.Mtq5sem!=null) return true;
		if (this.Mtq5phon!=null) return true;
		if (this.Mtq6uncue!=null) return true;
		if (this.Mtq6msip!=null) return true;
		if (this.Mtq6sem!=null) return true;
		if (this.Mtq6phon!=null) return true;
		if (this.Mtq7uncue!=null) return true;
		if (this.Mtq7msip!=null) return true;
		if (this.Mtq7sem!=null) return true;
		if (this.Mtq7phon!=null) return true;
		if (this.Mtq8uncue!=null) return true;
		if (this.Mtq8msip!=null) return true;
		if (this.Mtq8sem!=null) return true;
		if (this.Mtq8phon!=null) return true;
		if (this.Mtq9uncue!=null) return true;
		if (this.Mtq9msip!=null) return true;
		if (this.Mtq9sem!=null) return true;
		if (this.Mtq9phon!=null) return true;
		if (this.Mtq10uncue!=null) return true;
		if (this.Mtq10msip!=null) return true;
		if (this.Mtq10sem!=null) return true;
		if (this.Mtq10phon!=null) return true;
		if (this.Mtq11uncue!=null) return true;
		if (this.Mtq11msip!=null) return true;
		if (this.Mtq11sem!=null) return true;
		if (this.Mtq11phon!=null) return true;
		if (this.Mtq12uncue!=null) return true;
		if (this.Mtq12msip!=null) return true;
		if (this.Mtq12sem!=null) return true;
		if (this.Mtq12phon!=null) return true;
		if (this.Mtq13uncue!=null) return true;
		if (this.Mtq13msip!=null) return true;
		if (this.Mtq13sem!=null) return true;
		if (this.Mtq13phon!=null) return true;
		if (this.Mtq14uncue!=null) return true;
		if (this.Mtq14msip!=null) return true;
		if (this.Mtq14sem!=null) return true;
		if (this.Mtq14phon!=null) return true;
		if (this.Mtq15uncue!=null) return true;
		if (this.Mtq15msip!=null) return true;
		if (this.Mtq15sem!=null) return true;
		if (this.Mtq15phon!=null) return true;
		if (this.Mtq16uncue!=null) return true;
		if (this.Mtq16msip!=null) return true;
		if (this.Mtq16sem!=null) return true;
		if (this.Mtq16phon!=null) return true;
		if (this.Mtq17uncue!=null) return true;
		if (this.Mtq17msip!=null) return true;
		if (this.Mtq17sem!=null) return true;
		if (this.Mtq17phon!=null) return true;
		if (this.Mtq18uncue!=null) return true;
		if (this.Mtq18msip!=null) return true;
		if (this.Mtq18sem!=null) return true;
		if (this.Mtq18phon!=null) return true;
		if (this.Mtq19uncue!=null) return true;
		if (this.Mtq19msip!=null) return true;
		if (this.Mtq19sem!=null) return true;
		if (this.Mtq19phon!=null) return true;
		if (this.Mtq20uncue!=null) return true;
		if (this.Mtq20msip!=null) return true;
		if (this.Mtq20sem!=null) return true;
		if (this.Mtq20phon!=null) return true;
		if (this.Mtq21uncue!=null) return true;
		if (this.Mtq21msip!=null) return true;
		if (this.Mtq21sem!=null) return true;
		if (this.Mtq21phon!=null) return true;
		if (this.Mtq22uncue!=null) return true;
		if (this.Mtq22msip!=null) return true;
		if (this.Mtq22sem!=null) return true;
		if (this.Mtq22phon!=null) return true;
		if (this.Mtq23uncue!=null) return true;
		if (this.Mtq23msip!=null) return true;
		if (this.Mtq23sem!=null) return true;
		if (this.Mtq23phon!=null) return true;
		if (this.Mtq24uncue!=null) return true;
		if (this.Mtq24msip!=null) return true;
		if (this.Mtq24sem!=null) return true;
		if (this.Mtq24phon!=null) return true;
		if (this.Mtq25uncue!=null) return true;
		if (this.Mtq25msip!=null) return true;
		if (this.Mtq25sem!=null) return true;
		if (this.Mtq25phon!=null) return true;
		if (this.Mtq26uncue!=null) return true;
		if (this.Mtq26msip!=null) return true;
		if (this.Mtq26sem!=null) return true;
		if (this.Mtq26phon!=null) return true;
		if (this.Mtq27uncue!=null) return true;
		if (this.Mtq27msip!=null) return true;
		if (this.Mtq27sem!=null) return true;
		if (this.Mtq27phon!=null) return true;
		if (this.Mtq28uncue!=null) return true;
		if (this.Mtq28msip!=null) return true;
		if (this.Mtq28sem!=null) return true;
		if (this.Mtq28phon!=null) return true;
		if (this.Mtq29uncue!=null) return true;
		if (this.Mtq29msip!=null) return true;
		if (this.Mtq29sem!=null) return true;
		if (this.Mtq29phon!=null) return true;
		if (this.Mtq30uncue!=null) return true;
		if (this.Mtq30msip!=null) return true;
		if (this.Mtq30sem!=null) return true;
		if (this.Mtq30phon!=null) return true;
		if (this.Mtq31uncue!=null) return true;
		if (this.Mtq31msip!=null) return true;
		if (this.Mtq31sem!=null) return true;
		if (this.Mtq31phon!=null) return true;
		if (this.Mtq32uncue!=null) return true;
		if (this.Mtq32msip!=null) return true;
		if (this.Mtq32sem!=null) return true;
		if (this.Mtq32phon!=null) return true;
		if (this.Mttotal!=null) return true;
		if (this.Mtcomm!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
