/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function condr_mets_metsClinEncCollection(){
this.xsiType="condr_mets:metsClinEncCollection";

	this.getSchemaElementName=function(){
		return "metsClinEncCollection";
	}

	this.getFullSchemaElementName=function(){
		return "condr_mets:metsClinEncCollection";
	}
this.extension=dynamicJSLoad('cnda_ext_saCollection','generated/cnda_ext_saCollection.js');

	this.Encdate=null;


	function getEncdate() {
		return this.Encdate;
	}
	this.getEncdate=getEncdate;


	function setEncdate(v){
		this.Encdate=v;
	}
	this.setEncdate=setEncdate;

	this.Karperfstatus=null;


	function getKarperfstatus() {
		return this.Karperfstatus;
	}
	this.getKarperfstatus=getKarperfstatus;


	function setKarperfstatus(v){
		this.Karperfstatus=v;
	}
	this.setKarperfstatus=setKarperfstatus;

	this.Steroiddose=null;


	function getSteroiddose() {
		return this.Steroiddose;
	}
	this.getSteroiddose=getSteroiddose;


	function setSteroiddose(v){
		this.Steroiddose=v;
	}
	this.setSteroiddose=setSteroiddose;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="saCollection"){
				return this.Sacollection ;
			} else 
			if(xmlPath.startsWith("saCollection")){
				xmlPath=xmlPath.substring(12);
				if(xmlPath=="")return this.Sacollection ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Sacollection!=undefined)return this.Sacollection.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="encDate"){
				return this.Encdate ;
			} else 
			if(xmlPath=="karPerfStatus"){
				return this.Karperfstatus ;
			} else 
			if(xmlPath=="steroidDose"){
				return this.Steroiddose ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="saCollection"){
				this.Sacollection=value;
			} else 
			if(xmlPath.startsWith("saCollection")){
				xmlPath=xmlPath.substring(12);
				if(xmlPath=="")return this.Sacollection ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Sacollection!=undefined){
					this.Sacollection.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Sacollection= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Sacollection= instanciateObject("cnda_ext:saCollection");//omUtils.js
						}
						if(options && options.where)this.Sacollection.setProperty(options.where.field,options.where.value);
						this.Sacollection.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="encDate"){
				this.Encdate=value;
			} else 
			if(xmlPath=="karPerfStatus"){
				this.Karperfstatus=value;
			} else 
			if(xmlPath=="steroidDose"){
				this.Steroiddose=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="encDate"){
			return "field_data";
		}else if (xmlPath=="karPerfStatus"){
			return "field_data";
		}else if (xmlPath=="steroidDose"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<condr_mets:MetsClinEncColl";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</condr_mets:MetsClinEncColl>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Encdate!=null){
			xmlTxt+="\n<condr_mets:encDate";
			xmlTxt+=">";
			xmlTxt+=this.Encdate;
			xmlTxt+="</condr_mets:encDate>";
		}
		if (this.Karperfstatus!=null){
			xmlTxt+="\n<condr_mets:karPerfStatus";
			xmlTxt+=">";
			xmlTxt+=this.Karperfstatus;
			xmlTxt+="</condr_mets:karPerfStatus>";
		}
		if (this.Steroiddose!=null){
			xmlTxt+="\n<condr_mets:steroidDose";
			xmlTxt+=">";
			xmlTxt+=this.Steroiddose;
			xmlTxt+="</condr_mets:steroidDose>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Encdate!=null) return true;
		if (this.Karperfstatus!=null) return true;
		if (this.Steroiddose!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
