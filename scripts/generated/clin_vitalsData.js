/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function clin_vitalsData(){
this.xsiType="clin:vitalsData";

	this.getSchemaElementName=function(){
		return "vitalsData";
	}

	this.getFullSchemaElementName=function(){
		return "clin:vitalsData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Vsweight=null;


	function getVsweight() {
		return this.Vsweight;
	}
	this.getVsweight=getVsweight;


	function setVsweight(v){
		this.Vsweight=v;
	}
	this.setVsweight=setVsweight;

	this.Vswtunit=null;


	function getVswtunit() {
		return this.Vswtunit;
	}
	this.getVswtunit=getVswtunit;


	function setVswtunit(v){
		this.Vswtunit=v;
	}
	this.setVswtunit=setVswtunit;

	this.Vsheight=null;


	function getVsheight() {
		return this.Vsheight;
	}
	this.getVsheight=getVsheight;


	function setVsheight(v){
		this.Vsheight=v;
	}
	this.setVsheight=setVsheight;

	this.Vshtunit=null;


	function getVshtunit() {
		return this.Vshtunit;
	}
	this.getVshtunit=getVshtunit;


	function setVshtunit(v){
		this.Vshtunit=v;
	}
	this.setVshtunit=setVshtunit;

	this.Vsbpsys=null;


	function getVsbpsys() {
		return this.Vsbpsys;
	}
	this.getVsbpsys=getVsbpsys;


	function setVsbpsys(v){
		this.Vsbpsys=v;
	}
	this.setVsbpsys=setVsbpsys;

	this.Vsbpdia=null;


	function getVsbpdia() {
		return this.Vsbpdia;
	}
	this.getVsbpdia=getVsbpdia;


	function setVsbpdia(v){
		this.Vsbpdia=v;
	}
	this.setVsbpdia=setVsbpdia;

	this.Vspulse=null;


	function getVspulse() {
		return this.Vspulse;
	}
	this.getVspulse=getVspulse;


	function setVspulse(v){
		this.Vspulse=v;
	}
	this.setVspulse=setVspulse;

	this.Vsresp=null;


	function getVsresp() {
		return this.Vsresp;
	}
	this.getVsresp=getVsresp;


	function setVsresp(v){
		this.Vsresp=v;
	}
	this.setVsresp=setVsresp;

	this.Vstemp=null;


	function getVstemp() {
		return this.Vstemp;
	}
	this.getVstemp=getVstemp;


	function setVstemp(v){
		this.Vstemp=v;
	}
	this.setVstemp=setVstemp;

	this.Vstmpunt=null;


	function getVstmpunt() {
		return this.Vstmpunt;
	}
	this.getVstmpunt=getVstmpunt;


	function setVstmpunt(v){
		this.Vstmpunt=v;
	}
	this.setVstmpunt=setVstmpunt;

	this.Vstmpsrc=null;


	function getVstmpsrc() {
		return this.Vstmpsrc;
	}
	this.getVstmpsrc=getVstmpsrc;


	function setVstmpsrc(v){
		this.Vstmpsrc=v;
	}
	this.setVstmpsrc=setVstmpsrc;

	this.Vscomm=null;


	function getVscomm() {
		return this.Vscomm;
	}
	this.getVscomm=getVscomm;


	function setVscomm(v){
		this.Vscomm=v;
	}
	this.setVscomm=setVscomm;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="VSWEIGHT"){
				return this.Vsweight ;
			} else 
			if(xmlPath=="VSWTUNIT"){
				return this.Vswtunit ;
			} else 
			if(xmlPath=="VSHEIGHT"){
				return this.Vsheight ;
			} else 
			if(xmlPath=="VSHTUNIT"){
				return this.Vshtunit ;
			} else 
			if(xmlPath=="VSBPSYS"){
				return this.Vsbpsys ;
			} else 
			if(xmlPath=="VSBPDIA"){
				return this.Vsbpdia ;
			} else 
			if(xmlPath=="VSPULSE"){
				return this.Vspulse ;
			} else 
			if(xmlPath=="VSRESP"){
				return this.Vsresp ;
			} else 
			if(xmlPath=="VSTEMP"){
				return this.Vstemp ;
			} else 
			if(xmlPath=="VSTMPUNT"){
				return this.Vstmpunt ;
			} else 
			if(xmlPath=="VSTMPSRC"){
				return this.Vstmpsrc ;
			} else 
			if(xmlPath=="VSCOMM"){
				return this.Vscomm ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="VSWEIGHT"){
				this.Vsweight=value;
			} else 
			if(xmlPath=="VSWTUNIT"){
				this.Vswtunit=value;
			} else 
			if(xmlPath=="VSHEIGHT"){
				this.Vsheight=value;
			} else 
			if(xmlPath=="VSHTUNIT"){
				this.Vshtunit=value;
			} else 
			if(xmlPath=="VSBPSYS"){
				this.Vsbpsys=value;
			} else 
			if(xmlPath=="VSBPDIA"){
				this.Vsbpdia=value;
			} else 
			if(xmlPath=="VSPULSE"){
				this.Vspulse=value;
			} else 
			if(xmlPath=="VSRESP"){
				this.Vsresp=value;
			} else 
			if(xmlPath=="VSTEMP"){
				this.Vstemp=value;
			} else 
			if(xmlPath=="VSTMPUNT"){
				this.Vstmpunt=value;
			} else 
			if(xmlPath=="VSTMPSRC"){
				this.Vstmpsrc=value;
			} else 
			if(xmlPath=="VSCOMM"){
				this.Vscomm=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="VSWEIGHT"){
			return "field_data";
		}else if (xmlPath=="VSWTUNIT"){
			return "field_data";
		}else if (xmlPath=="VSHEIGHT"){
			return "field_data";
		}else if (xmlPath=="VSHTUNIT"){
			return "field_data";
		}else if (xmlPath=="VSBPSYS"){
			return "field_data";
		}else if (xmlPath=="VSBPDIA"){
			return "field_data";
		}else if (xmlPath=="VSPULSE"){
			return "field_data";
		}else if (xmlPath=="VSRESP"){
			return "field_data";
		}else if (xmlPath=="VSTEMP"){
			return "field_data";
		}else if (xmlPath=="VSTMPUNT"){
			return "field_data";
		}else if (xmlPath=="VSTMPSRC"){
			return "field_data";
		}else if (xmlPath=="VSCOMM"){
			return "field_LONG_DATA";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<clin:VITALS";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</clin:VITALS>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Vsweight!=null){
			xmlTxt+="\n<clin:VSWEIGHT";
			xmlTxt+=">";
			xmlTxt+=this.Vsweight;
			xmlTxt+="</clin:VSWEIGHT>";
		}
		if (this.Vswtunit!=null){
			xmlTxt+="\n<clin:VSWTUNIT";
			xmlTxt+=">";
			xmlTxt+=this.Vswtunit;
			xmlTxt+="</clin:VSWTUNIT>";
		}
		if (this.Vsheight!=null){
			xmlTxt+="\n<clin:VSHEIGHT";
			xmlTxt+=">";
			xmlTxt+=this.Vsheight;
			xmlTxt+="</clin:VSHEIGHT>";
		}
		if (this.Vshtunit!=null){
			xmlTxt+="\n<clin:VSHTUNIT";
			xmlTxt+=">";
			xmlTxt+=this.Vshtunit;
			xmlTxt+="</clin:VSHTUNIT>";
		}
		if (this.Vsbpsys!=null){
			xmlTxt+="\n<clin:VSBPSYS";
			xmlTxt+=">";
			xmlTxt+=this.Vsbpsys;
			xmlTxt+="</clin:VSBPSYS>";
		}
		if (this.Vsbpdia!=null){
			xmlTxt+="\n<clin:VSBPDIA";
			xmlTxt+=">";
			xmlTxt+=this.Vsbpdia;
			xmlTxt+="</clin:VSBPDIA>";
		}
		if (this.Vspulse!=null){
			xmlTxt+="\n<clin:VSPULSE";
			xmlTxt+=">";
			xmlTxt+=this.Vspulse;
			xmlTxt+="</clin:VSPULSE>";
		}
		if (this.Vsresp!=null){
			xmlTxt+="\n<clin:VSRESP";
			xmlTxt+=">";
			xmlTxt+=this.Vsresp;
			xmlTxt+="</clin:VSRESP>";
		}
		if (this.Vstemp!=null){
			xmlTxt+="\n<clin:VSTEMP";
			xmlTxt+=">";
			xmlTxt+=this.Vstemp;
			xmlTxt+="</clin:VSTEMP>";
		}
		if (this.Vstmpunt!=null){
			xmlTxt+="\n<clin:VSTMPUNT";
			xmlTxt+=">";
			xmlTxt+=this.Vstmpunt;
			xmlTxt+="</clin:VSTMPUNT>";
		}
		if (this.Vstmpsrc!=null){
			xmlTxt+="\n<clin:VSTMPSRC";
			xmlTxt+=">";
			xmlTxt+=this.Vstmpsrc;
			xmlTxt+="</clin:VSTMPSRC>";
		}
		if (this.Vscomm!=null){
			xmlTxt+="\n<clin:VSCOMM";
			xmlTxt+=">";
			xmlTxt+=this.Vscomm.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:VSCOMM>";
		}
		else{
			xmlTxt+="\n<clin:VSCOMM";
			xmlTxt+="/>";
		}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Vsweight!=null) return true;
		if (this.Vswtunit!=null) return true;
		if (this.Vsheight!=null) return true;
		if (this.Vshtunit!=null) return true;
		if (this.Vsbpsys!=null) return true;
		if (this.Vsbpdia!=null) return true;
		if (this.Vspulse!=null) return true;
		if (this.Vsresp!=null) return true;
		if (this.Vstemp!=null) return true;
		if (this.Vstmpunt!=null) return true;
		if (this.Vstmpsrc!=null) return true;
		if (this.Vscomm!=null) return true;
		return true;//REQUIRED VSCOMM
	}
}
