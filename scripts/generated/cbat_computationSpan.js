/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cbat_computationSpan(){
this.xsiType="cbat:computationSpan";

	this.getSchemaElementName=function(){
		return "computationSpan";
	}

	this.getFullSchemaElementName=function(){
		return "cbat:computationSpan";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.T1_accuracy=null;


	function getT1_accuracy() {
		return this.T1_accuracy;
	}
	this.getT1_accuracy=getT1_accuracy;


	function setT1_accuracy(v){
		this.T1_accuracy=v;
	}
	this.setT1_accuracy=setT1_accuracy;

	this.T2_accuracy=null;


	function getT2_accuracy() {
		return this.T2_accuracy;
	}
	this.getT2_accuracy=getT2_accuracy;


	function setT2_accuracy(v){
		this.T2_accuracy=v;
	}
	this.setT2_accuracy=setT2_accuracy;

	this.T3_accuracy=null;


	function getT3_accuracy() {
		return this.T3_accuracy;
	}
	this.getT3_accuracy=getT3_accuracy;


	function setT3_accuracy(v){
		this.T3_accuracy=v;
	}
	this.setT3_accuracy=setT3_accuracy;

	this.T4_accuracy=null;


	function getT4_accuracy() {
		return this.T4_accuracy;
	}
	this.getT4_accuracy=getT4_accuracy;


	function setT4_accuracy(v){
		this.T4_accuracy=v;
	}
	this.setT4_accuracy=setT4_accuracy;

	this.T5_accuracy=null;


	function getT5_accuracy() {
		return this.T5_accuracy;
	}
	this.getT5_accuracy=getT5_accuracy;


	function setT5_accuracy(v){
		this.T5_accuracy=v;
	}
	this.setT5_accuracy=setT5_accuracy;

	this.T6_accuracy=null;


	function getT6_accuracy() {
		return this.T6_accuracy;
	}
	this.getT6_accuracy=getT6_accuracy;


	function setT6_accuracy(v){
		this.T6_accuracy=v;
	}
	this.setT6_accuracy=setT6_accuracy;

	this.T7_accuracy=null;


	function getT7_accuracy() {
		return this.T7_accuracy;
	}
	this.getT7_accuracy=getT7_accuracy;


	function setT7_accuracy(v){
		this.T7_accuracy=v;
	}
	this.setT7_accuracy=setT7_accuracy;

	this.T8_accuracy=null;


	function getT8_accuracy() {
		return this.T8_accuracy;
	}
	this.getT8_accuracy=getT8_accuracy;


	function setT8_accuracy(v){
		this.T8_accuracy=v;
	}
	this.setT8_accuracy=setT8_accuracy;

	this.T9_accuracy=null;


	function getT9_accuracy() {
		return this.T9_accuracy;
	}
	this.getT9_accuracy=getT9_accuracy;


	function setT9_accuracy(v){
		this.T9_accuracy=v;
	}
	this.setT9_accuracy=setT9_accuracy;

	this.T10_accuracy=null;


	function getT10_accuracy() {
		return this.T10_accuracy;
	}
	this.getT10_accuracy=getT10_accuracy;


	function setT10_accuracy(v){
		this.T10_accuracy=v;
	}
	this.setT10_accuracy=setT10_accuracy;

	this.T11_accuracy=null;


	function getT11_accuracy() {
		return this.T11_accuracy;
	}
	this.getT11_accuracy=getT11_accuracy;


	function setT11_accuracy(v){
		this.T11_accuracy=v;
	}
	this.setT11_accuracy=setT11_accuracy;

	this.T12_accuracy=null;


	function getT12_accuracy() {
		return this.T12_accuracy;
	}
	this.getT12_accuracy=getT12_accuracy;


	function setT12_accuracy(v){
		this.T12_accuracy=v;
	}
	this.setT12_accuracy=setT12_accuracy;

	this.T13_accuracy=null;


	function getT13_accuracy() {
		return this.T13_accuracy;
	}
	this.getT13_accuracy=getT13_accuracy;


	function setT13_accuracy(v){
		this.T13_accuracy=v;
	}
	this.setT13_accuracy=setT13_accuracy;

	this.T14_accuracy=null;


	function getT14_accuracy() {
		return this.T14_accuracy;
	}
	this.getT14_accuracy=getT14_accuracy;


	function setT14_accuracy(v){
		this.T14_accuracy=v;
	}
	this.setT14_accuracy=setT14_accuracy;

	this.T15_accuracy=null;


	function getT15_accuracy() {
		return this.T15_accuracy;
	}
	this.getT15_accuracy=getT15_accuracy;


	function setT15_accuracy(v){
		this.T15_accuracy=v;
	}
	this.setT15_accuracy=setT15_accuracy;

	this.T16_accuracy=null;


	function getT16_accuracy() {
		return this.T16_accuracy;
	}
	this.getT16_accuracy=getT16_accuracy;


	function setT16_accuracy(v){
		this.T16_accuracy=v;
	}
	this.setT16_accuracy=setT16_accuracy;

	this.T17_accuracy=null;


	function getT17_accuracy() {
		return this.T17_accuracy;
	}
	this.getT17_accuracy=getT17_accuracy;


	function setT17_accuracy(v){
		this.T17_accuracy=v;
	}
	this.setT17_accuracy=setT17_accuracy;

	this.T18_accuracy=null;


	function getT18_accuracy() {
		return this.T18_accuracy;
	}
	this.getT18_accuracy=getT18_accuracy;


	function setT18_accuracy(v){
		this.T18_accuracy=v;
	}
	this.setT18_accuracy=setT18_accuracy;

	this.T19_accuracy=null;


	function getT19_accuracy() {
		return this.T19_accuracy;
	}
	this.getT19_accuracy=getT19_accuracy;


	function setT19_accuracy(v){
		this.T19_accuracy=v;
	}
	this.setT19_accuracy=setT19_accuracy;

	this.T20_accuracy=null;


	function getT20_accuracy() {
		return this.T20_accuracy;
	}
	this.getT20_accuracy=getT20_accuracy;


	function setT20_accuracy(v){
		this.T20_accuracy=v;
	}
	this.setT20_accuracy=setT20_accuracy;

	this.T21_accuracy=null;


	function getT21_accuracy() {
		return this.T21_accuracy;
	}
	this.getT21_accuracy=getT21_accuracy;


	function setT21_accuracy(v){
		this.T21_accuracy=v;
	}
	this.setT21_accuracy=setT21_accuracy;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="T1/accuracy"){
				return this.T1_accuracy ;
			} else 
			if(xmlPath=="T2/accuracy"){
				return this.T2_accuracy ;
			} else 
			if(xmlPath=="T3/accuracy"){
				return this.T3_accuracy ;
			} else 
			if(xmlPath=="T4/accuracy"){
				return this.T4_accuracy ;
			} else 
			if(xmlPath=="T5/accuracy"){
				return this.T5_accuracy ;
			} else 
			if(xmlPath=="T6/accuracy"){
				return this.T6_accuracy ;
			} else 
			if(xmlPath=="T7/accuracy"){
				return this.T7_accuracy ;
			} else 
			if(xmlPath=="T8/accuracy"){
				return this.T8_accuracy ;
			} else 
			if(xmlPath=="T9/accuracy"){
				return this.T9_accuracy ;
			} else 
			if(xmlPath=="T10/accuracy"){
				return this.T10_accuracy ;
			} else 
			if(xmlPath=="T11/accuracy"){
				return this.T11_accuracy ;
			} else 
			if(xmlPath=="T12/accuracy"){
				return this.T12_accuracy ;
			} else 
			if(xmlPath=="T13/accuracy"){
				return this.T13_accuracy ;
			} else 
			if(xmlPath=="T14/accuracy"){
				return this.T14_accuracy ;
			} else 
			if(xmlPath=="T15/accuracy"){
				return this.T15_accuracy ;
			} else 
			if(xmlPath=="T16/accuracy"){
				return this.T16_accuracy ;
			} else 
			if(xmlPath=="T17/accuracy"){
				return this.T17_accuracy ;
			} else 
			if(xmlPath=="T18/accuracy"){
				return this.T18_accuracy ;
			} else 
			if(xmlPath=="T19/accuracy"){
				return this.T19_accuracy ;
			} else 
			if(xmlPath=="T20/accuracy"){
				return this.T20_accuracy ;
			} else 
			if(xmlPath=="T21/accuracy"){
				return this.T21_accuracy ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="T1/accuracy"){
				this.T1_accuracy=value;
			} else 
			if(xmlPath=="T2/accuracy"){
				this.T2_accuracy=value;
			} else 
			if(xmlPath=="T3/accuracy"){
				this.T3_accuracy=value;
			} else 
			if(xmlPath=="T4/accuracy"){
				this.T4_accuracy=value;
			} else 
			if(xmlPath=="T5/accuracy"){
				this.T5_accuracy=value;
			} else 
			if(xmlPath=="T6/accuracy"){
				this.T6_accuracy=value;
			} else 
			if(xmlPath=="T7/accuracy"){
				this.T7_accuracy=value;
			} else 
			if(xmlPath=="T8/accuracy"){
				this.T8_accuracy=value;
			} else 
			if(xmlPath=="T9/accuracy"){
				this.T9_accuracy=value;
			} else 
			if(xmlPath=="T10/accuracy"){
				this.T10_accuracy=value;
			} else 
			if(xmlPath=="T11/accuracy"){
				this.T11_accuracy=value;
			} else 
			if(xmlPath=="T12/accuracy"){
				this.T12_accuracy=value;
			} else 
			if(xmlPath=="T13/accuracy"){
				this.T13_accuracy=value;
			} else 
			if(xmlPath=="T14/accuracy"){
				this.T14_accuracy=value;
			} else 
			if(xmlPath=="T15/accuracy"){
				this.T15_accuracy=value;
			} else 
			if(xmlPath=="T16/accuracy"){
				this.T16_accuracy=value;
			} else 
			if(xmlPath=="T17/accuracy"){
				this.T17_accuracy=value;
			} else 
			if(xmlPath=="T18/accuracy"){
				this.T18_accuracy=value;
			} else 
			if(xmlPath=="T19/accuracy"){
				this.T19_accuracy=value;
			} else 
			if(xmlPath=="T20/accuracy"){
				this.T20_accuracy=value;
			} else 
			if(xmlPath=="T21/accuracy"){
				this.T21_accuracy=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="T1/accuracy"){
			return "field_data";
		}else if (xmlPath=="T2/accuracy"){
			return "field_data";
		}else if (xmlPath=="T3/accuracy"){
			return "field_data";
		}else if (xmlPath=="T4/accuracy"){
			return "field_data";
		}else if (xmlPath=="T5/accuracy"){
			return "field_data";
		}else if (xmlPath=="T6/accuracy"){
			return "field_data";
		}else if (xmlPath=="T7/accuracy"){
			return "field_data";
		}else if (xmlPath=="T8/accuracy"){
			return "field_data";
		}else if (xmlPath=="T9/accuracy"){
			return "field_data";
		}else if (xmlPath=="T10/accuracy"){
			return "field_data";
		}else if (xmlPath=="T11/accuracy"){
			return "field_data";
		}else if (xmlPath=="T12/accuracy"){
			return "field_data";
		}else if (xmlPath=="T13/accuracy"){
			return "field_data";
		}else if (xmlPath=="T14/accuracy"){
			return "field_data";
		}else if (xmlPath=="T15/accuracy"){
			return "field_data";
		}else if (xmlPath=="T16/accuracy"){
			return "field_data";
		}else if (xmlPath=="T17/accuracy"){
			return "field_data";
		}else if (xmlPath=="T18/accuracy"){
			return "field_data";
		}else if (xmlPath=="T19/accuracy"){
			return "field_data";
		}else if (xmlPath=="T20/accuracy"){
			return "field_data";
		}else if (xmlPath=="T21/accuracy"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cbat:ComputationSpan";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cbat:ComputationSpan>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
			var child0=0;
			var att0=0;
			if(this.T1_accuracy!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<cbat:T1";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T1_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T1_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:T1>";
			}
			}

			var child1=0;
			var att1=0;
			if(this.T2_accuracy!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<cbat:T2";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T2_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T2_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:T2>";
			}
			}

			var child2=0;
			var att2=0;
			if(this.T3_accuracy!=null)
			child2++;
			if(child2>0 || att2>0){
				xmlTxt+="\n<cbat:T3";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T3_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T3_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:T3>";
			}
			}

			var child3=0;
			var att3=0;
			if(this.T4_accuracy!=null)
			child3++;
			if(child3>0 || att3>0){
				xmlTxt+="\n<cbat:T4";
			if(child3==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T4_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T4_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:T4>";
			}
			}

			var child4=0;
			var att4=0;
			if(this.T5_accuracy!=null)
			child4++;
			if(child4>0 || att4>0){
				xmlTxt+="\n<cbat:T5";
			if(child4==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T5_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T5_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:T5>";
			}
			}

			var child5=0;
			var att5=0;
			if(this.T6_accuracy!=null)
			child5++;
			if(child5>0 || att5>0){
				xmlTxt+="\n<cbat:T6";
			if(child5==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T6_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T6_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:T6>";
			}
			}

			var child6=0;
			var att6=0;
			if(this.T7_accuracy!=null)
			child6++;
			if(child6>0 || att6>0){
				xmlTxt+="\n<cbat:T7";
			if(child6==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T7_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T7_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:T7>";
			}
			}

			var child7=0;
			var att7=0;
			if(this.T8_accuracy!=null)
			child7++;
			if(child7>0 || att7>0){
				xmlTxt+="\n<cbat:T8";
			if(child7==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T8_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T8_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:T8>";
			}
			}

			var child8=0;
			var att8=0;
			if(this.T9_accuracy!=null)
			child8++;
			if(child8>0 || att8>0){
				xmlTxt+="\n<cbat:T9";
			if(child8==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T9_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T9_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:T9>";
			}
			}

			var child9=0;
			var att9=0;
			if(this.T10_accuracy!=null)
			child9++;
			if(child9>0 || att9>0){
				xmlTxt+="\n<cbat:T10";
			if(child9==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T10_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T10_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:T10>";
			}
			}

			var child10=0;
			var att10=0;
			if(this.T11_accuracy!=null)
			child10++;
			if(child10>0 || att10>0){
				xmlTxt+="\n<cbat:T11";
			if(child10==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T11_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T11_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:T11>";
			}
			}

			var child11=0;
			var att11=0;
			if(this.T12_accuracy!=null)
			child11++;
			if(child11>0 || att11>0){
				xmlTxt+="\n<cbat:T12";
			if(child11==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T12_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T12_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:T12>";
			}
			}

			var child12=0;
			var att12=0;
			if(this.T13_accuracy!=null)
			child12++;
			if(child12>0 || att12>0){
				xmlTxt+="\n<cbat:T13";
			if(child12==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T13_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T13_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:T13>";
			}
			}

			var child13=0;
			var att13=0;
			if(this.T14_accuracy!=null)
			child13++;
			if(child13>0 || att13>0){
				xmlTxt+="\n<cbat:T14";
			if(child13==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T14_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T14_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:T14>";
			}
			}

			var child14=0;
			var att14=0;
			if(this.T15_accuracy!=null)
			child14++;
			if(child14>0 || att14>0){
				xmlTxt+="\n<cbat:T15";
			if(child14==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T15_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T15_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:T15>";
			}
			}

			var child15=0;
			var att15=0;
			if(this.T16_accuracy!=null)
			child15++;
			if(child15>0 || att15>0){
				xmlTxt+="\n<cbat:T16";
			if(child15==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T16_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T16_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:T16>";
			}
			}

			var child16=0;
			var att16=0;
			if(this.T17_accuracy!=null)
			child16++;
			if(child16>0 || att16>0){
				xmlTxt+="\n<cbat:T17";
			if(child16==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T17_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T17_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:T17>";
			}
			}

			var child17=0;
			var att17=0;
			if(this.T18_accuracy!=null)
			child17++;
			if(child17>0 || att17>0){
				xmlTxt+="\n<cbat:T18";
			if(child17==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T18_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T18_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:T18>";
			}
			}

			var child18=0;
			var att18=0;
			if(this.T19_accuracy!=null)
			child18++;
			if(child18>0 || att18>0){
				xmlTxt+="\n<cbat:T19";
			if(child18==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T19_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T19_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:T19>";
			}
			}

			var child19=0;
			var att19=0;
			if(this.T20_accuracy!=null)
			child19++;
			if(child19>0 || att19>0){
				xmlTxt+="\n<cbat:T20";
			if(child19==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T20_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T20_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:T20>";
			}
			}

			var child20=0;
			var att20=0;
			if(this.T21_accuracy!=null)
			child20++;
			if(child20>0 || att20>0){
				xmlTxt+="\n<cbat:T21";
			if(child20==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T21_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T21_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
				xmlTxt+="\n</cbat:T21>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
			if(this.T1_accuracy!=null) return true;
			if(this.T2_accuracy!=null) return true;
			if(this.T3_accuracy!=null) return true;
			if(this.T4_accuracy!=null) return true;
			if(this.T5_accuracy!=null) return true;
			if(this.T6_accuracy!=null) return true;
			if(this.T7_accuracy!=null) return true;
			if(this.T8_accuracy!=null) return true;
			if(this.T9_accuracy!=null) return true;
			if(this.T10_accuracy!=null) return true;
			if(this.T11_accuracy!=null) return true;
			if(this.T12_accuracy!=null) return true;
			if(this.T13_accuracy!=null) return true;
			if(this.T14_accuracy!=null) return true;
			if(this.T15_accuracy!=null) return true;
			if(this.T16_accuracy!=null) return true;
			if(this.T17_accuracy!=null) return true;
			if(this.T18_accuracy!=null) return true;
			if(this.T19_accuracy!=null) return true;
			if(this.T20_accuracy!=null) return true;
			if(this.T21_accuracy!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
