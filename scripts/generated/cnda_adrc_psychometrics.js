/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cnda_adrc_psychometrics(){
this.xsiType="cnda:adrc_psychometrics";

	this.getSchemaElementName=function(){
		return "adrc_psychometrics";
	}

	this.getFullSchemaElementName=function(){
		return "cnda:adrc_psychometrics";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Bentonrecall=null;


	function getBentonrecall() {
		return this.Bentonrecall;
	}
	this.getBentonrecall=getBentonrecall;


	function setBentonrecall(v){
		this.Bentonrecall=v;
	}
	this.setBentonrecall=setBentonrecall;

	this.Bnt=null;


	function getBnt() {
		return this.Bnt;
	}
	this.getBnt=getBnt;


	function setBnt(v){
		this.Bnt=v;
	}
	this.setBnt=setBnt;

	this.Animals=null;


	function getAnimals() {
		return this.Animals;
	}
	this.getAnimals=getAnimals;


	function setAnimals(v){
		this.Animals=v;
	}
	this.setAnimals=setAnimals;

	this.Veg=null;


	function getVeg() {
		return this.Veg;
	}
	this.getVeg=getVeg;


	function setVeg(v){
		this.Veg=v;
	}
	this.setVeg=setVeg;

	this.Crossingoff=null;


	function getCrossingoff() {
		return this.Crossingoff;
	}
	this.getCrossingoff=getCrossingoff;


	function setCrossingoff(v){
		this.Crossingoff=v;
	}
	this.setCrossingoff=setCrossingoff;

	this.Srtfree=null;


	function getSrtfree() {
		return this.Srtfree;
	}
	this.getSrtfree=getSrtfree;


	function setSrtfree(v){
		this.Srtfree=v;
	}
	this.setSrtfree=setSrtfree;

	this.Tma=null;


	function getTma() {
		return this.Tma;
	}
	this.getTma=getTma;


	function setTma(v){
		this.Tma=v;
	}
	this.setTma=setTma;

	this.Tmb=null;


	function getTmb() {
		return this.Tmb;
	}
	this.getTmb=getTmb;


	function setTmb(v){
		this.Tmb=v;
	}
	this.setTmb=setTmb;

	this.Info=null;


	function getInfo() {
		return this.Info;
	}
	this.getInfo=getInfo;


	function setInfo(v){
		this.Info=v;
	}
	this.setInfo=setInfo;

	this.BlockI=null;


	function getBlockI() {
		return this.BlockI;
	}
	this.getBlockI=getBlockI;


	function setBlockI(v){
		this.BlockI=v;
	}
	this.setBlockI=setBlockI;

	this.Digsym=null;


	function getDigsym() {
		return this.Digsym;
	}
	this.getDigsym=getDigsym;


	function setDigsym(v){
		this.Digsym=v;
	}
	this.setDigsym=setDigsym;

	this.Sim=null;


	function getSim() {
		return this.Sim;
	}
	this.getSim=getSim;


	function setSim(v){
		this.Sim=v;
	}
	this.setSim=setSim;

	this.Mentcont=null;


	function getMentcont() {
		return this.Mentcont;
	}
	this.getMentcont=getMentcont;


	function setMentcont(v){
		this.Mentcont=v;
	}
	this.setMentcont=setMentcont;

	this.Logmemimmed=null;


	function getLogmemimmed() {
		return this.Logmemimmed;
	}
	this.getLogmemimmed=getLogmemimmed;


	function setLogmemimmed(v){
		this.Logmemimmed=v;
	}
	this.setLogmemimmed=setLogmemimmed;

	this.Asscmem=null;


	function getAsscmem() {
		return this.Asscmem;
	}
	this.getAsscmem=getAsscmem;


	function setAsscmem(v){
		this.Asscmem=v;
	}
	this.setAsscmem=setAsscmem;

	this.Digfor=null;


	function getDigfor() {
		return this.Digfor;
	}
	this.getDigfor=getDigfor;


	function setDigfor(v){
		this.Digfor=v;
	}
	this.setDigfor=setDigfor;

	this.Digback=null;


	function getDigback() {
		return this.Digback;
	}
	this.getDigback=getDigback;


	function setDigback(v){
		this.Digback=v;
	}
	this.setDigback=setDigback;

	this.Wordflu=null;


	function getWordflu() {
		return this.Wordflu;
	}
	this.getWordflu=getWordflu;


	function setWordflu(v){
		this.Wordflu=v;
	}
	this.setWordflu=setWordflu;

	this.Gencomp=null;


	function getGencomp() {
		return this.Gencomp;
	}
	this.getGencomp=getGencomp;


	function setGencomp(v){
		this.Gencomp=v;
	}
	this.setGencomp=setGencomp;

	this.Visuospa=null;


	function getVisuospa() {
		return this.Visuospa;
	}
	this.getVisuospa=getVisuospa;


	function setVisuospa(v){
		this.Visuospa=v;
	}
	this.setVisuospa=setVisuospa;

	this.Exec=null;


	function getExec() {
		return this.Exec;
	}
	this.getExec=getExec;


	function setExec(v){
		this.Exec=v;
	}
	this.setExec=setExec;

	this.Trigrams=null;


	function getTrigrams() {
		return this.Trigrams;
	}
	this.getTrigrams=getTrigrams;


	function setTrigrams(v){
		this.Trigrams=v;
	}
	this.setTrigrams=setTrigrams;

	this.Line=null;


	function getLine() {
		return this.Line;
	}
	this.getLine=getLine;


	function setLine(v){
		this.Line=v;
	}
	this.setLine=setLine;

	this.Trailb=null;


	function getTrailb() {
		return this.Trailb;
	}
	this.getTrailb=getTrailb;


	function setTrailb(v){
		this.Trailb=v;
	}
	this.setTrailb=setTrailb;

	this.BlockIii=null;


	function getBlockIii() {
		return this.BlockIii;
	}
	this.getBlockIii=getBlockIii;


	function setBlockIii(v){
		this.BlockIii=v;
	}
	this.setBlockIii=setBlockIii;

	this.Inform=null;


	function getInform() {
		return this.Inform;
	}
	this.getInform=getInform;


	function setInform(v){
		this.Inform=v;
	}
	this.setInform=setInform;

	this.Lettnum=null;


	function getLettnum() {
		return this.Lettnum;
	}
	this.getLettnum=getLettnum;


	function setLettnum(v){
		this.Lettnum=v;
	}
	this.setLettnum=setLettnum;

	this.Logmem=null;


	function getLogmem() {
		return this.Logmem;
	}
	this.getLogmem=getLogmem;


	function setLogmem(v){
		this.Logmem=v;
	}
	this.setLogmem=setLogmem;

	this.Lmdelay=null;


	function getLmdelay() {
		return this.Lmdelay;
	}
	this.getLmdelay=getLmdelay;


	function setLmdelay(v){
		this.Lmdelay=v;
	}
	this.setLmdelay=setLmdelay;

	this.Pairs=null;


	function getPairs() {
		return this.Pairs;
	}
	this.getPairs=getPairs;


	function setPairs(v){
		this.Pairs=v;
	}
	this.setPairs=setPairs;

	this.Spatial=null;


	function getSpatial() {
		return this.Spatial;
	}
	this.getSpatial=getSpatial;


	function setSpatial(v){
		this.Spatial=v;
	}
	this.setSpatial=setSpatial;

	this.Logimem=null;


	function getLogimem() {
		return this.Logimem;
	}
	this.getLogimem=getLogimem;


	function setLogimem(v){
		this.Logimem=v;
	}
	this.setLogimem=setLogimem;

	this.Memunits=null;


	function getMemunits() {
		return this.Memunits;
	}
	this.getMemunits=getMemunits;


	function setMemunits(v){
		this.Memunits=v;
	}
	this.setMemunits=setMemunits;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="BentonRecall"){
				return this.Bentonrecall ;
			} else 
			if(xmlPath=="BNT"){
				return this.Bnt ;
			} else 
			if(xmlPath=="animals"){
				return this.Animals ;
			} else 
			if(xmlPath=="veg"){
				return this.Veg ;
			} else 
			if(xmlPath=="crossingOff"){
				return this.Crossingoff ;
			} else 
			if(xmlPath=="SRTFree"){
				return this.Srtfree ;
			} else 
			if(xmlPath=="tma"){
				return this.Tma ;
			} else 
			if(xmlPath=="tmb"){
				return this.Tmb ;
			} else 
			if(xmlPath=="info"){
				return this.Info ;
			} else 
			if(xmlPath=="block_I"){
				return this.BlockI ;
			} else 
			if(xmlPath=="digsym"){
				return this.Digsym ;
			} else 
			if(xmlPath=="sim"){
				return this.Sim ;
			} else 
			if(xmlPath=="mentcont"){
				return this.Mentcont ;
			} else 
			if(xmlPath=="logMemImmed"){
				return this.Logmemimmed ;
			} else 
			if(xmlPath=="asscMem"){
				return this.Asscmem ;
			} else 
			if(xmlPath=="digfor"){
				return this.Digfor ;
			} else 
			if(xmlPath=="digback"){
				return this.Digback ;
			} else 
			if(xmlPath=="wordflu"){
				return this.Wordflu ;
			} else 
			if(xmlPath=="gencomp"){
				return this.Gencomp ;
			} else 
			if(xmlPath=="visuospa"){
				return this.Visuospa ;
			} else 
			if(xmlPath=="exec"){
				return this.Exec ;
			} else 
			if(xmlPath=="trigrams"){
				return this.Trigrams ;
			} else 
			if(xmlPath=="line"){
				return this.Line ;
			} else 
			if(xmlPath=="trailb"){
				return this.Trailb ;
			} else 
			if(xmlPath=="block_III"){
				return this.BlockIii ;
			} else 
			if(xmlPath=="inform"){
				return this.Inform ;
			} else 
			if(xmlPath=="lettnum"){
				return this.Lettnum ;
			} else 
			if(xmlPath=="logmem"){
				return this.Logmem ;
			} else 
			if(xmlPath=="lmdelay"){
				return this.Lmdelay ;
			} else 
			if(xmlPath=="pairs"){
				return this.Pairs ;
			} else 
			if(xmlPath=="spatial"){
				return this.Spatial ;
			} else 
			if(xmlPath=="logimem"){
				return this.Logimem ;
			} else 
			if(xmlPath=="memunits"){
				return this.Memunits ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="BentonRecall"){
				this.Bentonrecall=value;
			} else 
			if(xmlPath=="BNT"){
				this.Bnt=value;
			} else 
			if(xmlPath=="animals"){
				this.Animals=value;
			} else 
			if(xmlPath=="veg"){
				this.Veg=value;
			} else 
			if(xmlPath=="crossingOff"){
				this.Crossingoff=value;
			} else 
			if(xmlPath=="SRTFree"){
				this.Srtfree=value;
			} else 
			if(xmlPath=="tma"){
				this.Tma=value;
			} else 
			if(xmlPath=="tmb"){
				this.Tmb=value;
			} else 
			if(xmlPath=="info"){
				this.Info=value;
			} else 
			if(xmlPath=="block_I"){
				this.BlockI=value;
			} else 
			if(xmlPath=="digsym"){
				this.Digsym=value;
			} else 
			if(xmlPath=="sim"){
				this.Sim=value;
			} else 
			if(xmlPath=="mentcont"){
				this.Mentcont=value;
			} else 
			if(xmlPath=="logMemImmed"){
				this.Logmemimmed=value;
			} else 
			if(xmlPath=="asscMem"){
				this.Asscmem=value;
			} else 
			if(xmlPath=="digfor"){
				this.Digfor=value;
			} else 
			if(xmlPath=="digback"){
				this.Digback=value;
			} else 
			if(xmlPath=="wordflu"){
				this.Wordflu=value;
			} else 
			if(xmlPath=="gencomp"){
				this.Gencomp=value;
			} else 
			if(xmlPath=="visuospa"){
				this.Visuospa=value;
			} else 
			if(xmlPath=="exec"){
				this.Exec=value;
			} else 
			if(xmlPath=="trigrams"){
				this.Trigrams=value;
			} else 
			if(xmlPath=="line"){
				this.Line=value;
			} else 
			if(xmlPath=="trailb"){
				this.Trailb=value;
			} else 
			if(xmlPath=="block_III"){
				this.BlockIii=value;
			} else 
			if(xmlPath=="inform"){
				this.Inform=value;
			} else 
			if(xmlPath=="lettnum"){
				this.Lettnum=value;
			} else 
			if(xmlPath=="logmem"){
				this.Logmem=value;
			} else 
			if(xmlPath=="lmdelay"){
				this.Lmdelay=value;
			} else 
			if(xmlPath=="pairs"){
				this.Pairs=value;
			} else 
			if(xmlPath=="spatial"){
				this.Spatial=value;
			} else 
			if(xmlPath=="logimem"){
				this.Logimem=value;
			} else 
			if(xmlPath=="memunits"){
				this.Memunits=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="BentonRecall"){
			return "field_data";
		}else if (xmlPath=="BNT"){
			return "field_data";
		}else if (xmlPath=="animals"){
			return "field_data";
		}else if (xmlPath=="veg"){
			return "field_data";
		}else if (xmlPath=="crossingOff"){
			return "field_data";
		}else if (xmlPath=="SRTFree"){
			return "field_data";
		}else if (xmlPath=="tma"){
			return "field_data";
		}else if (xmlPath=="tmb"){
			return "field_data";
		}else if (xmlPath=="info"){
			return "field_data";
		}else if (xmlPath=="block_I"){
			return "field_data";
		}else if (xmlPath=="digsym"){
			return "field_data";
		}else if (xmlPath=="sim"){
			return "field_data";
		}else if (xmlPath=="mentcont"){
			return "field_data";
		}else if (xmlPath=="logMemImmed"){
			return "field_data";
		}else if (xmlPath=="asscMem"){
			return "field_data";
		}else if (xmlPath=="digfor"){
			return "field_data";
		}else if (xmlPath=="digback"){
			return "field_data";
		}else if (xmlPath=="wordflu"){
			return "field_data";
		}else if (xmlPath=="gencomp"){
			return "field_data";
		}else if (xmlPath=="visuospa"){
			return "field_data";
		}else if (xmlPath=="exec"){
			return "field_data";
		}else if (xmlPath=="trigrams"){
			return "field_data";
		}else if (xmlPath=="line"){
			return "field_data";
		}else if (xmlPath=="trailb"){
			return "field_data";
		}else if (xmlPath=="block_III"){
			return "field_data";
		}else if (xmlPath=="inform"){
			return "field_data";
		}else if (xmlPath=="lettnum"){
			return "field_data";
		}else if (xmlPath=="logmem"){
			return "field_data";
		}else if (xmlPath=="lmdelay"){
			return "field_data";
		}else if (xmlPath=="pairs"){
			return "field_data";
		}else if (xmlPath=="spatial"){
			return "field_data";
		}else if (xmlPath=="logimem"){
			return "field_data";
		}else if (xmlPath=="memunits"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cnda:ADRCPsychometrics";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cnda:ADRCPsychometrics>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Bentonrecall!=null){
			xmlTxt+="\n<cnda:BentonRecall";
			xmlTxt+=">";
			xmlTxt+=this.Bentonrecall;
			xmlTxt+="</cnda:BentonRecall>";
		}
		if (this.Bnt!=null){
			xmlTxt+="\n<cnda:BNT";
			xmlTxt+=">";
			xmlTxt+=this.Bnt;
			xmlTxt+="</cnda:BNT>";
		}
		if (this.Animals!=null){
			xmlTxt+="\n<cnda:animals";
			xmlTxt+=">";
			xmlTxt+=this.Animals;
			xmlTxt+="</cnda:animals>";
		}
		if (this.Veg!=null){
			xmlTxt+="\n<cnda:veg";
			xmlTxt+=">";
			xmlTxt+=this.Veg;
			xmlTxt+="</cnda:veg>";
		}
		if (this.Crossingoff!=null){
			xmlTxt+="\n<cnda:crossingOff";
			xmlTxt+=">";
			xmlTxt+=this.Crossingoff;
			xmlTxt+="</cnda:crossingOff>";
		}
		if (this.Srtfree!=null){
			xmlTxt+="\n<cnda:SRTFree";
			xmlTxt+=">";
			xmlTxt+=this.Srtfree;
			xmlTxt+="</cnda:SRTFree>";
		}
		if (this.Tma!=null){
			xmlTxt+="\n<cnda:tma";
			xmlTxt+=">";
			xmlTxt+=this.Tma;
			xmlTxt+="</cnda:tma>";
		}
		if (this.Tmb!=null){
			xmlTxt+="\n<cnda:tmb";
			xmlTxt+=">";
			xmlTxt+=this.Tmb;
			xmlTxt+="</cnda:tmb>";
		}
		if (this.Info!=null){
			xmlTxt+="\n<cnda:info";
			xmlTxt+=">";
			xmlTxt+=this.Info;
			xmlTxt+="</cnda:info>";
		}
		if (this.BlockI!=null){
			xmlTxt+="\n<cnda:block_I";
			xmlTxt+=">";
			xmlTxt+=this.BlockI;
			xmlTxt+="</cnda:block_I>";
		}
		if (this.Digsym!=null){
			xmlTxt+="\n<cnda:digsym";
			xmlTxt+=">";
			xmlTxt+=this.Digsym;
			xmlTxt+="</cnda:digsym>";
		}
		if (this.Sim!=null){
			xmlTxt+="\n<cnda:sim";
			xmlTxt+=">";
			xmlTxt+=this.Sim;
			xmlTxt+="</cnda:sim>";
		}
		if (this.Mentcont!=null){
			xmlTxt+="\n<cnda:mentcont";
			xmlTxt+=">";
			xmlTxt+=this.Mentcont;
			xmlTxt+="</cnda:mentcont>";
		}
		if (this.Logmemimmed!=null){
			xmlTxt+="\n<cnda:logMemImmed";
			xmlTxt+=">";
			xmlTxt+=this.Logmemimmed;
			xmlTxt+="</cnda:logMemImmed>";
		}
		if (this.Asscmem!=null){
			xmlTxt+="\n<cnda:asscMem";
			xmlTxt+=">";
			xmlTxt+=this.Asscmem;
			xmlTxt+="</cnda:asscMem>";
		}
		if (this.Digfor!=null){
			xmlTxt+="\n<cnda:digfor";
			xmlTxt+=">";
			xmlTxt+=this.Digfor;
			xmlTxt+="</cnda:digfor>";
		}
		if (this.Digback!=null){
			xmlTxt+="\n<cnda:digback";
			xmlTxt+=">";
			xmlTxt+=this.Digback;
			xmlTxt+="</cnda:digback>";
		}
		if (this.Wordflu!=null){
			xmlTxt+="\n<cnda:wordflu";
			xmlTxt+=">";
			xmlTxt+=this.Wordflu;
			xmlTxt+="</cnda:wordflu>";
		}
		if (this.Gencomp!=null){
			xmlTxt+="\n<cnda:gencomp";
			xmlTxt+=">";
			xmlTxt+=this.Gencomp;
			xmlTxt+="</cnda:gencomp>";
		}
		if (this.Visuospa!=null){
			xmlTxt+="\n<cnda:visuospa";
			xmlTxt+=">";
			xmlTxt+=this.Visuospa;
			xmlTxt+="</cnda:visuospa>";
		}
		if (this.Exec!=null){
			xmlTxt+="\n<cnda:exec";
			xmlTxt+=">";
			xmlTxt+=this.Exec;
			xmlTxt+="</cnda:exec>";
		}
		if (this.Trigrams!=null){
			xmlTxt+="\n<cnda:trigrams";
			xmlTxt+=">";
			xmlTxt+=this.Trigrams;
			xmlTxt+="</cnda:trigrams>";
		}
		if (this.Line!=null){
			xmlTxt+="\n<cnda:line";
			xmlTxt+=">";
			xmlTxt+=this.Line;
			xmlTxt+="</cnda:line>";
		}
		if (this.Trailb!=null){
			xmlTxt+="\n<cnda:trailb";
			xmlTxt+=">";
			xmlTxt+=this.Trailb;
			xmlTxt+="</cnda:trailb>";
		}
		if (this.BlockIii!=null){
			xmlTxt+="\n<cnda:block_III";
			xmlTxt+=">";
			xmlTxt+=this.BlockIii;
			xmlTxt+="</cnda:block_III>";
		}
		if (this.Inform!=null){
			xmlTxt+="\n<cnda:inform";
			xmlTxt+=">";
			xmlTxt+=this.Inform;
			xmlTxt+="</cnda:inform>";
		}
		if (this.Lettnum!=null){
			xmlTxt+="\n<cnda:lettnum";
			xmlTxt+=">";
			xmlTxt+=this.Lettnum;
			xmlTxt+="</cnda:lettnum>";
		}
		if (this.Logmem!=null){
			xmlTxt+="\n<cnda:logmem";
			xmlTxt+=">";
			xmlTxt+=this.Logmem;
			xmlTxt+="</cnda:logmem>";
		}
		if (this.Lmdelay!=null){
			xmlTxt+="\n<cnda:lmdelay";
			xmlTxt+=">";
			xmlTxt+=this.Lmdelay;
			xmlTxt+="</cnda:lmdelay>";
		}
		if (this.Pairs!=null){
			xmlTxt+="\n<cnda:pairs";
			xmlTxt+=">";
			xmlTxt+=this.Pairs;
			xmlTxt+="</cnda:pairs>";
		}
		if (this.Spatial!=null){
			xmlTxt+="\n<cnda:spatial";
			xmlTxt+=">";
			xmlTxt+=this.Spatial;
			xmlTxt+="</cnda:spatial>";
		}
		if (this.Logimem!=null){
			xmlTxt+="\n<cnda:logimem";
			xmlTxt+=">";
			xmlTxt+=this.Logimem;
			xmlTxt+="</cnda:logimem>";
		}
		if (this.Memunits!=null){
			xmlTxt+="\n<cnda:memunits";
			xmlTxt+=">";
			xmlTxt+=this.Memunits;
			xmlTxt+="</cnda:memunits>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Bentonrecall!=null) return true;
		if (this.Bnt!=null) return true;
		if (this.Animals!=null) return true;
		if (this.Veg!=null) return true;
		if (this.Crossingoff!=null) return true;
		if (this.Srtfree!=null) return true;
		if (this.Tma!=null) return true;
		if (this.Tmb!=null) return true;
		if (this.Info!=null) return true;
		if (this.BlockI!=null) return true;
		if (this.Digsym!=null) return true;
		if (this.Sim!=null) return true;
		if (this.Mentcont!=null) return true;
		if (this.Logmemimmed!=null) return true;
		if (this.Asscmem!=null) return true;
		if (this.Digfor!=null) return true;
		if (this.Digback!=null) return true;
		if (this.Wordflu!=null) return true;
		if (this.Gencomp!=null) return true;
		if (this.Visuospa!=null) return true;
		if (this.Exec!=null) return true;
		if (this.Trigrams!=null) return true;
		if (this.Line!=null) return true;
		if (this.Trailb!=null) return true;
		if (this.BlockIii!=null) return true;
		if (this.Inform!=null) return true;
		if (this.Lettnum!=null) return true;
		if (this.Logmem!=null) return true;
		if (this.Lmdelay!=null) return true;
		if (this.Pairs!=null) return true;
		if (this.Spatial!=null) return true;
		if (this.Logimem!=null) return true;
		if (this.Memunits!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
