/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function ls2_lsDemographicData(){
this.xsiType="ls2:lsDemographicData";

	this.getSchemaElementName=function(){
		return "lsDemographicData";
	}

	this.getFullSchemaElementName=function(){
		return "ls2:lsDemographicData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Educationlevel=null;


	function getEducationlevel() {
		return this.Educationlevel;
	}
	this.getEducationlevel=getEducationlevel;


	function setEducationlevel(v){
		this.Educationlevel=v;
	}
	this.setEducationlevel=setEducationlevel;

	this.Maritalstatus=null;


	function getMaritalstatus() {
		return this.Maritalstatus;
	}
	this.getMaritalstatus=getMaritalstatus;


	function setMaritalstatus(v){
		this.Maritalstatus=v;
	}
	this.setMaritalstatus=setMaritalstatus;

	this.Maritalstatuso=null;


	function getMaritalstatuso() {
		return this.Maritalstatuso;
	}
	this.getMaritalstatuso=getMaritalstatuso;


	function setMaritalstatuso(v){
		this.Maritalstatuso=v;
	}
	this.setMaritalstatuso=setMaritalstatuso;

	this.Housing=null;


	function getHousing() {
		return this.Housing;
	}
	this.getHousing=getHousing;


	function setHousing(v){
		this.Housing=v;
	}
	this.setHousing=setHousing;

	this.Housingo=null;


	function getHousingo() {
		return this.Housingo;
	}
	this.getHousingo=getHousingo;


	function setHousingo(v){
		this.Housingo=v;
	}
	this.setHousingo=setHousingo;

	this.Livealone=null;


	function getLivealone() {
		return this.Livealone;
	}
	this.getLivealone=getLivealone;


	function setLivealone(v){
		this.Livealone=v;
	}
	this.setLivealone=setLivealone;

	this.Engnow=null;


	function getEngnow() {
		return this.Engnow;
	}
	this.getEngnow=getEngnow;


	function setEngnow(v){
		this.Engnow=v;
	}
	this.setEngnow=setEngnow;

	this.Engnowo=null;


	function getEngnowo() {
		return this.Engnowo;
	}
	this.getEngnowo=getEngnowo;


	function setEngnowo(v){
		this.Engnowo=v;
	}
	this.setEngnowo=setEngnowo;

	this.Engfirst=null;


	function getEngfirst() {
		return this.Engfirst;
	}
	this.getEngfirst=getEngfirst;


	function setEngfirst(v){
		this.Engfirst=v;
	}
	this.setEngfirst=setEngfirst;

	this.Engfirsto=null;


	function getEngfirsto() {
		return this.Engfirsto;
	}
	this.getEngfirsto=getEngfirsto;


	function setEngfirsto(v){
		this.Engfirsto=v;
	}
	this.setEngfirsto=setEngfirsto;

	this.Wrkft=null;


	function getWrkft() {
		return this.Wrkft;
	}
	this.getWrkft=getWrkft;


	function setWrkft(v){
		this.Wrkft=v;
	}
	this.setWrkft=setWrkft;

	this.Wrkpt=null;


	function getWrkpt() {
		return this.Wrkpt;
	}
	this.getWrkpt=getWrkpt;


	function setWrkpt(v){
		this.Wrkpt=v;
	}
	this.setWrkpt=setWrkpt;

	this.Wrkstu=null;


	function getWrkstu() {
		return this.Wrkstu;
	}
	this.getWrkstu=getWrkstu;


	function setWrkstu(v){
		this.Wrkstu=v;
	}
	this.setWrkstu=setWrkstu;

	this.Wrkhome=null;


	function getWrkhome() {
		return this.Wrkhome;
	}
	this.getWrkhome=getWrkhome;


	function setWrkhome(v){
		this.Wrkhome=v;
	}
	this.setWrkhome=setWrkhome;

	this.Wrkret=null;


	function getWrkret() {
		return this.Wrkret;
	}
	this.getWrkret=getWrkret;


	function setWrkret(v){
		this.Wrkret=v;
	}
	this.setWrkret=setWrkret;

	this.Wrkvol=null;


	function getWrkvol() {
		return this.Wrkvol;
	}
	this.getWrkvol=getWrkvol;


	function setWrkvol(v){
		this.Wrkvol=v;
	}
	this.setWrkvol=setWrkvol;

	this.Wrkseek=null;


	function getWrkseek() {
		return this.Wrkseek;
	}
	this.getWrkseek=getWrkseek;


	function setWrkseek(v){
		this.Wrkseek=v;
	}
	this.setWrkseek=setWrkseek;

	this.Wrkleave=null;


	function getWrkleave() {
		return this.Wrkleave;
	}
	this.getWrkleave=getWrkleave;


	function setWrkleave(v){
		this.Wrkleave=v;
	}
	this.setWrkleave=setWrkleave;

	this.Wrkoth=null;


	function getWrkoth() {
		return this.Wrkoth;
	}
	this.getWrkoth=getWrkoth;


	function setWrkoth(v){
		this.Wrkoth=v;
	}
	this.setWrkoth=setWrkoth;

	this.Wrkstat=null;


	function getWrkstat() {
		return this.Wrkstat;
	}
	this.getWrkstat=getWrkstat;


	function setWrkstat(v){
		this.Wrkstat=v;
	}
	this.setWrkstat=setWrkstat;

	this.Currocc=null;


	function getCurrocc() {
		return this.Currocc;
	}
	this.getCurrocc=getCurrocc;


	function setCurrocc(v){
		this.Currocc=v;
	}
	this.setCurrocc=setCurrocc;

	this.Retocc=null;


	function getRetocc() {
		return this.Retocc;
	}
	this.getRetocc=getRetocc;


	function setRetocc(v){
		this.Retocc=v;
	}
	this.setRetocc=setRetocc;

	this.Retyr=null;


	function getRetyr() {
		return this.Retyr;
	}
	this.getRetyr=getRetyr;


	function setRetyr(v){
		this.Retyr=v;
	}
	this.setRetyr=setRetyr;

	this.Height=null;


	function getHeight() {
		return this.Height;
	}
	this.getHeight=getHeight;


	function setHeight(v){
		this.Height=v;
	}
	this.setHeight=setHeight;

	this.Weight=null;


	function getWeight() {
		return this.Weight;
	}
	this.getWeight=getWeight;


	function setWeight(v){
		this.Weight=v;
	}
	this.setWeight=setWeight;

	this.Hand=null;


	function getHand() {
		return this.Hand;
	}
	this.getHand=getHand;


	function setHand(v){
		this.Hand=v;
	}
	this.setHand=setHand;

	this.Ethnic=null;


	function getEthnic() {
		return this.Ethnic;
	}
	this.getEthnic=getEthnic;


	function setEthnic(v){
		this.Ethnic=v;
	}
	this.setEthnic=setEthnic;

	this.Race1=null;


	function getRace1() {
		return this.Race1;
	}
	this.getRace1=getRace1;


	function setRace1(v){
		this.Race1=v;
	}
	this.setRace1=setRace1;

	this.Race2=null;


	function getRace2() {
		return this.Race2;
	}
	this.getRace2=getRace2;


	function setRace2(v){
		this.Race2=v;
	}
	this.setRace2=setRace2;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="educationLevel"){
				return this.Educationlevel ;
			} else 
			if(xmlPath=="maritalStatus"){
				return this.Maritalstatus ;
			} else 
			if(xmlPath=="maritalStatusO"){
				return this.Maritalstatuso ;
			} else 
			if(xmlPath=="housing"){
				return this.Housing ;
			} else 
			if(xmlPath=="housingO"){
				return this.Housingo ;
			} else 
			if(xmlPath=="liveAlone"){
				return this.Livealone ;
			} else 
			if(xmlPath=="engNow"){
				return this.Engnow ;
			} else 
			if(xmlPath=="engNowO"){
				return this.Engnowo ;
			} else 
			if(xmlPath=="engFirst"){
				return this.Engfirst ;
			} else 
			if(xmlPath=="engFirstO"){
				return this.Engfirsto ;
			} else 
			if(xmlPath=="wrkFt"){
				return this.Wrkft ;
			} else 
			if(xmlPath=="wrkPt"){
				return this.Wrkpt ;
			} else 
			if(xmlPath=="wrkStu"){
				return this.Wrkstu ;
			} else 
			if(xmlPath=="wrkHome"){
				return this.Wrkhome ;
			} else 
			if(xmlPath=="wrkRet"){
				return this.Wrkret ;
			} else 
			if(xmlPath=="wrkVol"){
				return this.Wrkvol ;
			} else 
			if(xmlPath=="wrkSeek"){
				return this.Wrkseek ;
			} else 
			if(xmlPath=="wrkLeave"){
				return this.Wrkleave ;
			} else 
			if(xmlPath=="wrkOth"){
				return this.Wrkoth ;
			} else 
			if(xmlPath=="wrkStat"){
				return this.Wrkstat ;
			} else 
			if(xmlPath=="currOcc"){
				return this.Currocc ;
			} else 
			if(xmlPath=="retOcc"){
				return this.Retocc ;
			} else 
			if(xmlPath=="retYr"){
				return this.Retyr ;
			} else 
			if(xmlPath=="height"){
				return this.Height ;
			} else 
			if(xmlPath=="weight"){
				return this.Weight ;
			} else 
			if(xmlPath=="hand"){
				return this.Hand ;
			} else 
			if(xmlPath=="ethnic"){
				return this.Ethnic ;
			} else 
			if(xmlPath=="race1"){
				return this.Race1 ;
			} else 
			if(xmlPath=="race2"){
				return this.Race2 ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="educationLevel"){
				this.Educationlevel=value;
			} else 
			if(xmlPath=="maritalStatus"){
				this.Maritalstatus=value;
			} else 
			if(xmlPath=="maritalStatusO"){
				this.Maritalstatuso=value;
			} else 
			if(xmlPath=="housing"){
				this.Housing=value;
			} else 
			if(xmlPath=="housingO"){
				this.Housingo=value;
			} else 
			if(xmlPath=="liveAlone"){
				this.Livealone=value;
			} else 
			if(xmlPath=="engNow"){
				this.Engnow=value;
			} else 
			if(xmlPath=="engNowO"){
				this.Engnowo=value;
			} else 
			if(xmlPath=="engFirst"){
				this.Engfirst=value;
			} else 
			if(xmlPath=="engFirstO"){
				this.Engfirsto=value;
			} else 
			if(xmlPath=="wrkFt"){
				this.Wrkft=value;
			} else 
			if(xmlPath=="wrkPt"){
				this.Wrkpt=value;
			} else 
			if(xmlPath=="wrkStu"){
				this.Wrkstu=value;
			} else 
			if(xmlPath=="wrkHome"){
				this.Wrkhome=value;
			} else 
			if(xmlPath=="wrkRet"){
				this.Wrkret=value;
			} else 
			if(xmlPath=="wrkVol"){
				this.Wrkvol=value;
			} else 
			if(xmlPath=="wrkSeek"){
				this.Wrkseek=value;
			} else 
			if(xmlPath=="wrkLeave"){
				this.Wrkleave=value;
			} else 
			if(xmlPath=="wrkOth"){
				this.Wrkoth=value;
			} else 
			if(xmlPath=="wrkStat"){
				this.Wrkstat=value;
			} else 
			if(xmlPath=="currOcc"){
				this.Currocc=value;
			} else 
			if(xmlPath=="retOcc"){
				this.Retocc=value;
			} else 
			if(xmlPath=="retYr"){
				this.Retyr=value;
			} else 
			if(xmlPath=="height"){
				this.Height=value;
			} else 
			if(xmlPath=="weight"){
				this.Weight=value;
			} else 
			if(xmlPath=="hand"){
				this.Hand=value;
			} else 
			if(xmlPath=="ethnic"){
				this.Ethnic=value;
			} else 
			if(xmlPath=="race1"){
				this.Race1=value;
			} else 
			if(xmlPath=="race2"){
				this.Race2=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="educationLevel"){
			return "field_data";
		}else if (xmlPath=="maritalStatus"){
			return "field_data";
		}else if (xmlPath=="maritalStatusO"){
			return "field_data";
		}else if (xmlPath=="housing"){
			return "field_data";
		}else if (xmlPath=="housingO"){
			return "field_data";
		}else if (xmlPath=="liveAlone"){
			return "field_data";
		}else if (xmlPath=="engNow"){
			return "field_data";
		}else if (xmlPath=="engNowO"){
			return "field_data";
		}else if (xmlPath=="engFirst"){
			return "field_data";
		}else if (xmlPath=="engFirstO"){
			return "field_data";
		}else if (xmlPath=="wrkFt"){
			return "field_data";
		}else if (xmlPath=="wrkPt"){
			return "field_data";
		}else if (xmlPath=="wrkStu"){
			return "field_data";
		}else if (xmlPath=="wrkHome"){
			return "field_data";
		}else if (xmlPath=="wrkRet"){
			return "field_data";
		}else if (xmlPath=="wrkVol"){
			return "field_data";
		}else if (xmlPath=="wrkSeek"){
			return "field_data";
		}else if (xmlPath=="wrkLeave"){
			return "field_data";
		}else if (xmlPath=="wrkOth"){
			return "field_data";
		}else if (xmlPath=="wrkStat"){
			return "field_data";
		}else if (xmlPath=="currOcc"){
			return "field_data";
		}else if (xmlPath=="retOcc"){
			return "field_data";
		}else if (xmlPath=="retYr"){
			return "field_data";
		}else if (xmlPath=="height"){
			return "field_data";
		}else if (xmlPath=="weight"){
			return "field_data";
		}else if (xmlPath=="hand"){
			return "field_data";
		}else if (xmlPath=="ethnic"){
			return "field_data";
		}else if (xmlPath=="race1"){
			return "field_data";
		}else if (xmlPath=="race2"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<ls2:LS_Demog";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</ls2:LS_Demog>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Educationlevel!=null){
			xmlTxt+="\n<ls2:educationLevel";
			xmlTxt+=">";
			xmlTxt+=this.Educationlevel;
			xmlTxt+="</ls2:educationLevel>";
		}
		if (this.Maritalstatus!=null){
			xmlTxt+="\n<ls2:maritalStatus";
			xmlTxt+=">";
			xmlTxt+=this.Maritalstatus;
			xmlTxt+="</ls2:maritalStatus>";
		}
		if (this.Maritalstatuso!=null){
			xmlTxt+="\n<ls2:maritalStatusO";
			xmlTxt+=">";
			xmlTxt+=this.Maritalstatuso.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ls2:maritalStatusO>";
		}
		if (this.Housing!=null){
			xmlTxt+="\n<ls2:housing";
			xmlTxt+=">";
			xmlTxt+=this.Housing;
			xmlTxt+="</ls2:housing>";
		}
		if (this.Housingo!=null){
			xmlTxt+="\n<ls2:housingO";
			xmlTxt+=">";
			xmlTxt+=this.Housingo.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ls2:housingO>";
		}
		if (this.Livealone!=null){
			xmlTxt+="\n<ls2:liveAlone";
			xmlTxt+=">";
			xmlTxt+=this.Livealone;
			xmlTxt+="</ls2:liveAlone>";
		}
		if (this.Engnow!=null){
			xmlTxt+="\n<ls2:engNow";
			xmlTxt+=">";
			xmlTxt+=this.Engnow;
			xmlTxt+="</ls2:engNow>";
		}
		if (this.Engnowo!=null){
			xmlTxt+="\n<ls2:engNowO";
			xmlTxt+=">";
			xmlTxt+=this.Engnowo.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ls2:engNowO>";
		}
		if (this.Engfirst!=null){
			xmlTxt+="\n<ls2:engFirst";
			xmlTxt+=">";
			xmlTxt+=this.Engfirst;
			xmlTxt+="</ls2:engFirst>";
		}
		if (this.Engfirsto!=null){
			xmlTxt+="\n<ls2:engFirstO";
			xmlTxt+=">";
			xmlTxt+=this.Engfirsto.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ls2:engFirstO>";
		}
		if (this.Wrkft!=null){
			xmlTxt+="\n<ls2:wrkFt";
			xmlTxt+=">";
			xmlTxt+=this.Wrkft;
			xmlTxt+="</ls2:wrkFt>";
		}
		if (this.Wrkpt!=null){
			xmlTxt+="\n<ls2:wrkPt";
			xmlTxt+=">";
			xmlTxt+=this.Wrkpt;
			xmlTxt+="</ls2:wrkPt>";
		}
		if (this.Wrkstu!=null){
			xmlTxt+="\n<ls2:wrkStu";
			xmlTxt+=">";
			xmlTxt+=this.Wrkstu;
			xmlTxt+="</ls2:wrkStu>";
		}
		if (this.Wrkhome!=null){
			xmlTxt+="\n<ls2:wrkHome";
			xmlTxt+=">";
			xmlTxt+=this.Wrkhome;
			xmlTxt+="</ls2:wrkHome>";
		}
		if (this.Wrkret!=null){
			xmlTxt+="\n<ls2:wrkRet";
			xmlTxt+=">";
			xmlTxt+=this.Wrkret;
			xmlTxt+="</ls2:wrkRet>";
		}
		if (this.Wrkvol!=null){
			xmlTxt+="\n<ls2:wrkVol";
			xmlTxt+=">";
			xmlTxt+=this.Wrkvol;
			xmlTxt+="</ls2:wrkVol>";
		}
		if (this.Wrkseek!=null){
			xmlTxt+="\n<ls2:wrkSeek";
			xmlTxt+=">";
			xmlTxt+=this.Wrkseek;
			xmlTxt+="</ls2:wrkSeek>";
		}
		if (this.Wrkleave!=null){
			xmlTxt+="\n<ls2:wrkLeave";
			xmlTxt+=">";
			xmlTxt+=this.Wrkleave;
			xmlTxt+="</ls2:wrkLeave>";
		}
		if (this.Wrkoth!=null){
			xmlTxt+="\n<ls2:wrkOth";
			xmlTxt+=">";
			xmlTxt+=this.Wrkoth;
			xmlTxt+="</ls2:wrkOth>";
		}
		if (this.Wrkstat!=null){
			xmlTxt+="\n<ls2:wrkStat";
			xmlTxt+=">";
			xmlTxt+=this.Wrkstat;
			xmlTxt+="</ls2:wrkStat>";
		}
		if (this.Currocc!=null){
			xmlTxt+="\n<ls2:currOcc";
			xmlTxt+=">";
			xmlTxt+=this.Currocc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ls2:currOcc>";
		}
		if (this.Retocc!=null){
			xmlTxt+="\n<ls2:retOcc";
			xmlTxt+=">";
			xmlTxt+=this.Retocc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ls2:retOcc>";
		}
		if (this.Retyr!=null){
			xmlTxt+="\n<ls2:retYr";
			xmlTxt+=">";
			xmlTxt+=this.Retyr;
			xmlTxt+="</ls2:retYr>";
		}
		if (this.Height!=null){
			xmlTxt+="\n<ls2:height";
			xmlTxt+=">";
			xmlTxt+=this.Height;
			xmlTxt+="</ls2:height>";
		}
		if (this.Weight!=null){
			xmlTxt+="\n<ls2:weight";
			xmlTxt+=">";
			xmlTxt+=this.Weight;
			xmlTxt+="</ls2:weight>";
		}
		if (this.Hand!=null){
			xmlTxt+="\n<ls2:hand";
			xmlTxt+=">";
			xmlTxt+=this.Hand;
			xmlTxt+="</ls2:hand>";
		}
		if (this.Ethnic!=null){
			xmlTxt+="\n<ls2:ethnic";
			xmlTxt+=">";
			xmlTxt+=this.Ethnic;
			xmlTxt+="</ls2:ethnic>";
		}
		if (this.Race1!=null){
			xmlTxt+="\n<ls2:race1";
			xmlTxt+=">";
			xmlTxt+=this.Race1;
			xmlTxt+="</ls2:race1>";
		}
		if (this.Race2!=null){
			xmlTxt+="\n<ls2:race2";
			xmlTxt+=">";
			xmlTxt+=this.Race2;
			xmlTxt+="</ls2:race2>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Educationlevel!=null) return true;
		if (this.Maritalstatus!=null) return true;
		if (this.Maritalstatuso!=null) return true;
		if (this.Housing!=null) return true;
		if (this.Housingo!=null) return true;
		if (this.Livealone!=null) return true;
		if (this.Engnow!=null) return true;
		if (this.Engnowo!=null) return true;
		if (this.Engfirst!=null) return true;
		if (this.Engfirsto!=null) return true;
		if (this.Wrkft!=null) return true;
		if (this.Wrkpt!=null) return true;
		if (this.Wrkstu!=null) return true;
		if (this.Wrkhome!=null) return true;
		if (this.Wrkret!=null) return true;
		if (this.Wrkvol!=null) return true;
		if (this.Wrkseek!=null) return true;
		if (this.Wrkleave!=null) return true;
		if (this.Wrkoth!=null) return true;
		if (this.Wrkstat!=null) return true;
		if (this.Currocc!=null) return true;
		if (this.Retocc!=null) return true;
		if (this.Retyr!=null) return true;
		if (this.Height!=null) return true;
		if (this.Weight!=null) return true;
		if (this.Hand!=null) return true;
		if (this.Ethnic!=null) return true;
		if (this.Race1!=null) return true;
		if (this.Race2!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
