/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cbat_simon(){
this.xsiType="cbat:simon";

	this.getSchemaElementName=function(){
		return "simon";
	}

	this.getFullSchemaElementName=function(){
		return "cbat:simon";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.T1_accuracy=null;


	function getT1_accuracy() {
		return this.T1_accuracy;
	}
	this.getT1_accuracy=getT1_accuracy;


	function setT1_accuracy(v){
		this.T1_accuracy=v;
	}
	this.setT1_accuracy=setT1_accuracy;

	this.T1_responsetime=null;


	function getT1_responsetime() {
		return this.T1_responsetime;
	}
	this.getT1_responsetime=getT1_responsetime;


	function setT1_responsetime(v){
		this.T1_responsetime=v;
	}
	this.setT1_responsetime=setT1_responsetime;

	this.T1_congruency=null;


	function getT1_congruency() {
		return this.T1_congruency;
	}
	this.getT1_congruency=getT1_congruency;


	function setT1_congruency(v){
		this.T1_congruency=v;
	}
	this.setT1_congruency=setT1_congruency;

	this.T2_accuracy=null;


	function getT2_accuracy() {
		return this.T2_accuracy;
	}
	this.getT2_accuracy=getT2_accuracy;


	function setT2_accuracy(v){
		this.T2_accuracy=v;
	}
	this.setT2_accuracy=setT2_accuracy;

	this.T2_responsetime=null;


	function getT2_responsetime() {
		return this.T2_responsetime;
	}
	this.getT2_responsetime=getT2_responsetime;


	function setT2_responsetime(v){
		this.T2_responsetime=v;
	}
	this.setT2_responsetime=setT2_responsetime;

	this.T2_congruency=null;


	function getT2_congruency() {
		return this.T2_congruency;
	}
	this.getT2_congruency=getT2_congruency;


	function setT2_congruency(v){
		this.T2_congruency=v;
	}
	this.setT2_congruency=setT2_congruency;

	this.T3_accuracy=null;


	function getT3_accuracy() {
		return this.T3_accuracy;
	}
	this.getT3_accuracy=getT3_accuracy;


	function setT3_accuracy(v){
		this.T3_accuracy=v;
	}
	this.setT3_accuracy=setT3_accuracy;

	this.T3_responsetime=null;


	function getT3_responsetime() {
		return this.T3_responsetime;
	}
	this.getT3_responsetime=getT3_responsetime;


	function setT3_responsetime(v){
		this.T3_responsetime=v;
	}
	this.setT3_responsetime=setT3_responsetime;

	this.T3_congruency=null;


	function getT3_congruency() {
		return this.T3_congruency;
	}
	this.getT3_congruency=getT3_congruency;


	function setT3_congruency(v){
		this.T3_congruency=v;
	}
	this.setT3_congruency=setT3_congruency;

	this.T4_accuracy=null;


	function getT4_accuracy() {
		return this.T4_accuracy;
	}
	this.getT4_accuracy=getT4_accuracy;


	function setT4_accuracy(v){
		this.T4_accuracy=v;
	}
	this.setT4_accuracy=setT4_accuracy;

	this.T4_responsetime=null;


	function getT4_responsetime() {
		return this.T4_responsetime;
	}
	this.getT4_responsetime=getT4_responsetime;


	function setT4_responsetime(v){
		this.T4_responsetime=v;
	}
	this.setT4_responsetime=setT4_responsetime;

	this.T4_congruency=null;


	function getT4_congruency() {
		return this.T4_congruency;
	}
	this.getT4_congruency=getT4_congruency;


	function setT4_congruency(v){
		this.T4_congruency=v;
	}
	this.setT4_congruency=setT4_congruency;

	this.T5_accuracy=null;


	function getT5_accuracy() {
		return this.T5_accuracy;
	}
	this.getT5_accuracy=getT5_accuracy;


	function setT5_accuracy(v){
		this.T5_accuracy=v;
	}
	this.setT5_accuracy=setT5_accuracy;

	this.T5_responsetime=null;


	function getT5_responsetime() {
		return this.T5_responsetime;
	}
	this.getT5_responsetime=getT5_responsetime;


	function setT5_responsetime(v){
		this.T5_responsetime=v;
	}
	this.setT5_responsetime=setT5_responsetime;

	this.T5_congruency=null;


	function getT5_congruency() {
		return this.T5_congruency;
	}
	this.getT5_congruency=getT5_congruency;


	function setT5_congruency(v){
		this.T5_congruency=v;
	}
	this.setT5_congruency=setT5_congruency;

	this.T6_accuracy=null;


	function getT6_accuracy() {
		return this.T6_accuracy;
	}
	this.getT6_accuracy=getT6_accuracy;


	function setT6_accuracy(v){
		this.T6_accuracy=v;
	}
	this.setT6_accuracy=setT6_accuracy;

	this.T6_responsetime=null;


	function getT6_responsetime() {
		return this.T6_responsetime;
	}
	this.getT6_responsetime=getT6_responsetime;


	function setT6_responsetime(v){
		this.T6_responsetime=v;
	}
	this.setT6_responsetime=setT6_responsetime;

	this.T6_congruency=null;


	function getT6_congruency() {
		return this.T6_congruency;
	}
	this.getT6_congruency=getT6_congruency;


	function setT6_congruency(v){
		this.T6_congruency=v;
	}
	this.setT6_congruency=setT6_congruency;

	this.T7_accuracy=null;


	function getT7_accuracy() {
		return this.T7_accuracy;
	}
	this.getT7_accuracy=getT7_accuracy;


	function setT7_accuracy(v){
		this.T7_accuracy=v;
	}
	this.setT7_accuracy=setT7_accuracy;

	this.T7_responsetime=null;


	function getT7_responsetime() {
		return this.T7_responsetime;
	}
	this.getT7_responsetime=getT7_responsetime;


	function setT7_responsetime(v){
		this.T7_responsetime=v;
	}
	this.setT7_responsetime=setT7_responsetime;

	this.T7_congruency=null;


	function getT7_congruency() {
		return this.T7_congruency;
	}
	this.getT7_congruency=getT7_congruency;


	function setT7_congruency(v){
		this.T7_congruency=v;
	}
	this.setT7_congruency=setT7_congruency;

	this.T8_accuracy=null;


	function getT8_accuracy() {
		return this.T8_accuracy;
	}
	this.getT8_accuracy=getT8_accuracy;


	function setT8_accuracy(v){
		this.T8_accuracy=v;
	}
	this.setT8_accuracy=setT8_accuracy;

	this.T8_responsetime=null;


	function getT8_responsetime() {
		return this.T8_responsetime;
	}
	this.getT8_responsetime=getT8_responsetime;


	function setT8_responsetime(v){
		this.T8_responsetime=v;
	}
	this.setT8_responsetime=setT8_responsetime;

	this.T8_congruency=null;


	function getT8_congruency() {
		return this.T8_congruency;
	}
	this.getT8_congruency=getT8_congruency;


	function setT8_congruency(v){
		this.T8_congruency=v;
	}
	this.setT8_congruency=setT8_congruency;

	this.T9_accuracy=null;


	function getT9_accuracy() {
		return this.T9_accuracy;
	}
	this.getT9_accuracy=getT9_accuracy;


	function setT9_accuracy(v){
		this.T9_accuracy=v;
	}
	this.setT9_accuracy=setT9_accuracy;

	this.T9_responsetime=null;


	function getT9_responsetime() {
		return this.T9_responsetime;
	}
	this.getT9_responsetime=getT9_responsetime;


	function setT9_responsetime(v){
		this.T9_responsetime=v;
	}
	this.setT9_responsetime=setT9_responsetime;

	this.T9_congruency=null;


	function getT9_congruency() {
		return this.T9_congruency;
	}
	this.getT9_congruency=getT9_congruency;


	function setT9_congruency(v){
		this.T9_congruency=v;
	}
	this.setT9_congruency=setT9_congruency;

	this.T10_accuracy=null;


	function getT10_accuracy() {
		return this.T10_accuracy;
	}
	this.getT10_accuracy=getT10_accuracy;


	function setT10_accuracy(v){
		this.T10_accuracy=v;
	}
	this.setT10_accuracy=setT10_accuracy;

	this.T10_responsetime=null;


	function getT10_responsetime() {
		return this.T10_responsetime;
	}
	this.getT10_responsetime=getT10_responsetime;


	function setT10_responsetime(v){
		this.T10_responsetime=v;
	}
	this.setT10_responsetime=setT10_responsetime;

	this.T10_congruency=null;


	function getT10_congruency() {
		return this.T10_congruency;
	}
	this.getT10_congruency=getT10_congruency;


	function setT10_congruency(v){
		this.T10_congruency=v;
	}
	this.setT10_congruency=setT10_congruency;

	this.T11_accuracy=null;


	function getT11_accuracy() {
		return this.T11_accuracy;
	}
	this.getT11_accuracy=getT11_accuracy;


	function setT11_accuracy(v){
		this.T11_accuracy=v;
	}
	this.setT11_accuracy=setT11_accuracy;

	this.T11_responsetime=null;


	function getT11_responsetime() {
		return this.T11_responsetime;
	}
	this.getT11_responsetime=getT11_responsetime;


	function setT11_responsetime(v){
		this.T11_responsetime=v;
	}
	this.setT11_responsetime=setT11_responsetime;

	this.T11_congruency=null;


	function getT11_congruency() {
		return this.T11_congruency;
	}
	this.getT11_congruency=getT11_congruency;


	function setT11_congruency(v){
		this.T11_congruency=v;
	}
	this.setT11_congruency=setT11_congruency;

	this.T12_accuracy=null;


	function getT12_accuracy() {
		return this.T12_accuracy;
	}
	this.getT12_accuracy=getT12_accuracy;


	function setT12_accuracy(v){
		this.T12_accuracy=v;
	}
	this.setT12_accuracy=setT12_accuracy;

	this.T12_responsetime=null;


	function getT12_responsetime() {
		return this.T12_responsetime;
	}
	this.getT12_responsetime=getT12_responsetime;


	function setT12_responsetime(v){
		this.T12_responsetime=v;
	}
	this.setT12_responsetime=setT12_responsetime;

	this.T12_congruency=null;


	function getT12_congruency() {
		return this.T12_congruency;
	}
	this.getT12_congruency=getT12_congruency;


	function setT12_congruency(v){
		this.T12_congruency=v;
	}
	this.setT12_congruency=setT12_congruency;

	this.T13_accuracy=null;


	function getT13_accuracy() {
		return this.T13_accuracy;
	}
	this.getT13_accuracy=getT13_accuracy;


	function setT13_accuracy(v){
		this.T13_accuracy=v;
	}
	this.setT13_accuracy=setT13_accuracy;

	this.T13_responsetime=null;


	function getT13_responsetime() {
		return this.T13_responsetime;
	}
	this.getT13_responsetime=getT13_responsetime;


	function setT13_responsetime(v){
		this.T13_responsetime=v;
	}
	this.setT13_responsetime=setT13_responsetime;

	this.T13_congruency=null;


	function getT13_congruency() {
		return this.T13_congruency;
	}
	this.getT13_congruency=getT13_congruency;


	function setT13_congruency(v){
		this.T13_congruency=v;
	}
	this.setT13_congruency=setT13_congruency;

	this.T14_accuracy=null;


	function getT14_accuracy() {
		return this.T14_accuracy;
	}
	this.getT14_accuracy=getT14_accuracy;


	function setT14_accuracy(v){
		this.T14_accuracy=v;
	}
	this.setT14_accuracy=setT14_accuracy;

	this.T14_responsetime=null;


	function getT14_responsetime() {
		return this.T14_responsetime;
	}
	this.getT14_responsetime=getT14_responsetime;


	function setT14_responsetime(v){
		this.T14_responsetime=v;
	}
	this.setT14_responsetime=setT14_responsetime;

	this.T14_congruency=null;


	function getT14_congruency() {
		return this.T14_congruency;
	}
	this.getT14_congruency=getT14_congruency;


	function setT14_congruency(v){
		this.T14_congruency=v;
	}
	this.setT14_congruency=setT14_congruency;

	this.T15_accuracy=null;


	function getT15_accuracy() {
		return this.T15_accuracy;
	}
	this.getT15_accuracy=getT15_accuracy;


	function setT15_accuracy(v){
		this.T15_accuracy=v;
	}
	this.setT15_accuracy=setT15_accuracy;

	this.T15_responsetime=null;


	function getT15_responsetime() {
		return this.T15_responsetime;
	}
	this.getT15_responsetime=getT15_responsetime;


	function setT15_responsetime(v){
		this.T15_responsetime=v;
	}
	this.setT15_responsetime=setT15_responsetime;

	this.T15_congruency=null;


	function getT15_congruency() {
		return this.T15_congruency;
	}
	this.getT15_congruency=getT15_congruency;


	function setT15_congruency(v){
		this.T15_congruency=v;
	}
	this.setT15_congruency=setT15_congruency;

	this.T16_accuracy=null;


	function getT16_accuracy() {
		return this.T16_accuracy;
	}
	this.getT16_accuracy=getT16_accuracy;


	function setT16_accuracy(v){
		this.T16_accuracy=v;
	}
	this.setT16_accuracy=setT16_accuracy;

	this.T16_responsetime=null;


	function getT16_responsetime() {
		return this.T16_responsetime;
	}
	this.getT16_responsetime=getT16_responsetime;


	function setT16_responsetime(v){
		this.T16_responsetime=v;
	}
	this.setT16_responsetime=setT16_responsetime;

	this.T16_congruency=null;


	function getT16_congruency() {
		return this.T16_congruency;
	}
	this.getT16_congruency=getT16_congruency;


	function setT16_congruency(v){
		this.T16_congruency=v;
	}
	this.setT16_congruency=setT16_congruency;

	this.T17_accuracy=null;


	function getT17_accuracy() {
		return this.T17_accuracy;
	}
	this.getT17_accuracy=getT17_accuracy;


	function setT17_accuracy(v){
		this.T17_accuracy=v;
	}
	this.setT17_accuracy=setT17_accuracy;

	this.T17_responsetime=null;


	function getT17_responsetime() {
		return this.T17_responsetime;
	}
	this.getT17_responsetime=getT17_responsetime;


	function setT17_responsetime(v){
		this.T17_responsetime=v;
	}
	this.setT17_responsetime=setT17_responsetime;

	this.T17_congruency=null;


	function getT17_congruency() {
		return this.T17_congruency;
	}
	this.getT17_congruency=getT17_congruency;


	function setT17_congruency(v){
		this.T17_congruency=v;
	}
	this.setT17_congruency=setT17_congruency;

	this.T18_accuracy=null;


	function getT18_accuracy() {
		return this.T18_accuracy;
	}
	this.getT18_accuracy=getT18_accuracy;


	function setT18_accuracy(v){
		this.T18_accuracy=v;
	}
	this.setT18_accuracy=setT18_accuracy;

	this.T18_responsetime=null;


	function getT18_responsetime() {
		return this.T18_responsetime;
	}
	this.getT18_responsetime=getT18_responsetime;


	function setT18_responsetime(v){
		this.T18_responsetime=v;
	}
	this.setT18_responsetime=setT18_responsetime;

	this.T18_congruency=null;


	function getT18_congruency() {
		return this.T18_congruency;
	}
	this.getT18_congruency=getT18_congruency;


	function setT18_congruency(v){
		this.T18_congruency=v;
	}
	this.setT18_congruency=setT18_congruency;

	this.T19_accuracy=null;


	function getT19_accuracy() {
		return this.T19_accuracy;
	}
	this.getT19_accuracy=getT19_accuracy;


	function setT19_accuracy(v){
		this.T19_accuracy=v;
	}
	this.setT19_accuracy=setT19_accuracy;

	this.T19_responsetime=null;


	function getT19_responsetime() {
		return this.T19_responsetime;
	}
	this.getT19_responsetime=getT19_responsetime;


	function setT19_responsetime(v){
		this.T19_responsetime=v;
	}
	this.setT19_responsetime=setT19_responsetime;

	this.T19_congruency=null;


	function getT19_congruency() {
		return this.T19_congruency;
	}
	this.getT19_congruency=getT19_congruency;


	function setT19_congruency(v){
		this.T19_congruency=v;
	}
	this.setT19_congruency=setT19_congruency;

	this.T20_accuracy=null;


	function getT20_accuracy() {
		return this.T20_accuracy;
	}
	this.getT20_accuracy=getT20_accuracy;


	function setT20_accuracy(v){
		this.T20_accuracy=v;
	}
	this.setT20_accuracy=setT20_accuracy;

	this.T20_responsetime=null;


	function getT20_responsetime() {
		return this.T20_responsetime;
	}
	this.getT20_responsetime=getT20_responsetime;


	function setT20_responsetime(v){
		this.T20_responsetime=v;
	}
	this.setT20_responsetime=setT20_responsetime;

	this.T20_congruency=null;


	function getT20_congruency() {
		return this.T20_congruency;
	}
	this.getT20_congruency=getT20_congruency;


	function setT20_congruency(v){
		this.T20_congruency=v;
	}
	this.setT20_congruency=setT20_congruency;

	this.T21_accuracy=null;


	function getT21_accuracy() {
		return this.T21_accuracy;
	}
	this.getT21_accuracy=getT21_accuracy;


	function setT21_accuracy(v){
		this.T21_accuracy=v;
	}
	this.setT21_accuracy=setT21_accuracy;

	this.T21_responsetime=null;


	function getT21_responsetime() {
		return this.T21_responsetime;
	}
	this.getT21_responsetime=getT21_responsetime;


	function setT21_responsetime(v){
		this.T21_responsetime=v;
	}
	this.setT21_responsetime=setT21_responsetime;

	this.T21_congruency=null;


	function getT21_congruency() {
		return this.T21_congruency;
	}
	this.getT21_congruency=getT21_congruency;


	function setT21_congruency(v){
		this.T21_congruency=v;
	}
	this.setT21_congruency=setT21_congruency;

	this.T22_accuracy=null;


	function getT22_accuracy() {
		return this.T22_accuracy;
	}
	this.getT22_accuracy=getT22_accuracy;


	function setT22_accuracy(v){
		this.T22_accuracy=v;
	}
	this.setT22_accuracy=setT22_accuracy;

	this.T22_responsetime=null;


	function getT22_responsetime() {
		return this.T22_responsetime;
	}
	this.getT22_responsetime=getT22_responsetime;


	function setT22_responsetime(v){
		this.T22_responsetime=v;
	}
	this.setT22_responsetime=setT22_responsetime;

	this.T22_congruency=null;


	function getT22_congruency() {
		return this.T22_congruency;
	}
	this.getT22_congruency=getT22_congruency;


	function setT22_congruency(v){
		this.T22_congruency=v;
	}
	this.setT22_congruency=setT22_congruency;

	this.T23_accuracy=null;


	function getT23_accuracy() {
		return this.T23_accuracy;
	}
	this.getT23_accuracy=getT23_accuracy;


	function setT23_accuracy(v){
		this.T23_accuracy=v;
	}
	this.setT23_accuracy=setT23_accuracy;

	this.T23_responsetime=null;


	function getT23_responsetime() {
		return this.T23_responsetime;
	}
	this.getT23_responsetime=getT23_responsetime;


	function setT23_responsetime(v){
		this.T23_responsetime=v;
	}
	this.setT23_responsetime=setT23_responsetime;

	this.T23_congruency=null;


	function getT23_congruency() {
		return this.T23_congruency;
	}
	this.getT23_congruency=getT23_congruency;


	function setT23_congruency(v){
		this.T23_congruency=v;
	}
	this.setT23_congruency=setT23_congruency;

	this.T24_accuracy=null;


	function getT24_accuracy() {
		return this.T24_accuracy;
	}
	this.getT24_accuracy=getT24_accuracy;


	function setT24_accuracy(v){
		this.T24_accuracy=v;
	}
	this.setT24_accuracy=setT24_accuracy;

	this.T24_responsetime=null;


	function getT24_responsetime() {
		return this.T24_responsetime;
	}
	this.getT24_responsetime=getT24_responsetime;


	function setT24_responsetime(v){
		this.T24_responsetime=v;
	}
	this.setT24_responsetime=setT24_responsetime;

	this.T24_congruency=null;


	function getT24_congruency() {
		return this.T24_congruency;
	}
	this.getT24_congruency=getT24_congruency;


	function setT24_congruency(v){
		this.T24_congruency=v;
	}
	this.setT24_congruency=setT24_congruency;

	this.T25_accuracy=null;


	function getT25_accuracy() {
		return this.T25_accuracy;
	}
	this.getT25_accuracy=getT25_accuracy;


	function setT25_accuracy(v){
		this.T25_accuracy=v;
	}
	this.setT25_accuracy=setT25_accuracy;

	this.T25_responsetime=null;


	function getT25_responsetime() {
		return this.T25_responsetime;
	}
	this.getT25_responsetime=getT25_responsetime;


	function setT25_responsetime(v){
		this.T25_responsetime=v;
	}
	this.setT25_responsetime=setT25_responsetime;

	this.T25_congruency=null;


	function getT25_congruency() {
		return this.T25_congruency;
	}
	this.getT25_congruency=getT25_congruency;


	function setT25_congruency(v){
		this.T25_congruency=v;
	}
	this.setT25_congruency=setT25_congruency;

	this.T26_accuracy=null;


	function getT26_accuracy() {
		return this.T26_accuracy;
	}
	this.getT26_accuracy=getT26_accuracy;


	function setT26_accuracy(v){
		this.T26_accuracy=v;
	}
	this.setT26_accuracy=setT26_accuracy;

	this.T26_responsetime=null;


	function getT26_responsetime() {
		return this.T26_responsetime;
	}
	this.getT26_responsetime=getT26_responsetime;


	function setT26_responsetime(v){
		this.T26_responsetime=v;
	}
	this.setT26_responsetime=setT26_responsetime;

	this.T26_congruency=null;


	function getT26_congruency() {
		return this.T26_congruency;
	}
	this.getT26_congruency=getT26_congruency;


	function setT26_congruency(v){
		this.T26_congruency=v;
	}
	this.setT26_congruency=setT26_congruency;

	this.T27_accuracy=null;


	function getT27_accuracy() {
		return this.T27_accuracy;
	}
	this.getT27_accuracy=getT27_accuracy;


	function setT27_accuracy(v){
		this.T27_accuracy=v;
	}
	this.setT27_accuracy=setT27_accuracy;

	this.T27_responsetime=null;


	function getT27_responsetime() {
		return this.T27_responsetime;
	}
	this.getT27_responsetime=getT27_responsetime;


	function setT27_responsetime(v){
		this.T27_responsetime=v;
	}
	this.setT27_responsetime=setT27_responsetime;

	this.T27_congruency=null;


	function getT27_congruency() {
		return this.T27_congruency;
	}
	this.getT27_congruency=getT27_congruency;


	function setT27_congruency(v){
		this.T27_congruency=v;
	}
	this.setT27_congruency=setT27_congruency;

	this.T28_accuracy=null;


	function getT28_accuracy() {
		return this.T28_accuracy;
	}
	this.getT28_accuracy=getT28_accuracy;


	function setT28_accuracy(v){
		this.T28_accuracy=v;
	}
	this.setT28_accuracy=setT28_accuracy;

	this.T28_responsetime=null;


	function getT28_responsetime() {
		return this.T28_responsetime;
	}
	this.getT28_responsetime=getT28_responsetime;


	function setT28_responsetime(v){
		this.T28_responsetime=v;
	}
	this.setT28_responsetime=setT28_responsetime;

	this.T28_congruency=null;


	function getT28_congruency() {
		return this.T28_congruency;
	}
	this.getT28_congruency=getT28_congruency;


	function setT28_congruency(v){
		this.T28_congruency=v;
	}
	this.setT28_congruency=setT28_congruency;

	this.T29_accuracy=null;


	function getT29_accuracy() {
		return this.T29_accuracy;
	}
	this.getT29_accuracy=getT29_accuracy;


	function setT29_accuracy(v){
		this.T29_accuracy=v;
	}
	this.setT29_accuracy=setT29_accuracy;

	this.T29_responsetime=null;


	function getT29_responsetime() {
		return this.T29_responsetime;
	}
	this.getT29_responsetime=getT29_responsetime;


	function setT29_responsetime(v){
		this.T29_responsetime=v;
	}
	this.setT29_responsetime=setT29_responsetime;

	this.T29_congruency=null;


	function getT29_congruency() {
		return this.T29_congruency;
	}
	this.getT29_congruency=getT29_congruency;


	function setT29_congruency(v){
		this.T29_congruency=v;
	}
	this.setT29_congruency=setT29_congruency;

	this.T30_accuracy=null;


	function getT30_accuracy() {
		return this.T30_accuracy;
	}
	this.getT30_accuracy=getT30_accuracy;


	function setT30_accuracy(v){
		this.T30_accuracy=v;
	}
	this.setT30_accuracy=setT30_accuracy;

	this.T30_responsetime=null;


	function getT30_responsetime() {
		return this.T30_responsetime;
	}
	this.getT30_responsetime=getT30_responsetime;


	function setT30_responsetime(v){
		this.T30_responsetime=v;
	}
	this.setT30_responsetime=setT30_responsetime;

	this.T30_congruency=null;


	function getT30_congruency() {
		return this.T30_congruency;
	}
	this.getT30_congruency=getT30_congruency;


	function setT30_congruency(v){
		this.T30_congruency=v;
	}
	this.setT30_congruency=setT30_congruency;

	this.T31_accuracy=null;


	function getT31_accuracy() {
		return this.T31_accuracy;
	}
	this.getT31_accuracy=getT31_accuracy;


	function setT31_accuracy(v){
		this.T31_accuracy=v;
	}
	this.setT31_accuracy=setT31_accuracy;

	this.T31_responsetime=null;


	function getT31_responsetime() {
		return this.T31_responsetime;
	}
	this.getT31_responsetime=getT31_responsetime;


	function setT31_responsetime(v){
		this.T31_responsetime=v;
	}
	this.setT31_responsetime=setT31_responsetime;

	this.T31_congruency=null;


	function getT31_congruency() {
		return this.T31_congruency;
	}
	this.getT31_congruency=getT31_congruency;


	function setT31_congruency(v){
		this.T31_congruency=v;
	}
	this.setT31_congruency=setT31_congruency;

	this.T32_accuracy=null;


	function getT32_accuracy() {
		return this.T32_accuracy;
	}
	this.getT32_accuracy=getT32_accuracy;


	function setT32_accuracy(v){
		this.T32_accuracy=v;
	}
	this.setT32_accuracy=setT32_accuracy;

	this.T32_responsetime=null;


	function getT32_responsetime() {
		return this.T32_responsetime;
	}
	this.getT32_responsetime=getT32_responsetime;


	function setT32_responsetime(v){
		this.T32_responsetime=v;
	}
	this.setT32_responsetime=setT32_responsetime;

	this.T32_congruency=null;


	function getT32_congruency() {
		return this.T32_congruency;
	}
	this.getT32_congruency=getT32_congruency;


	function setT32_congruency(v){
		this.T32_congruency=v;
	}
	this.setT32_congruency=setT32_congruency;

	this.T33_accuracy=null;


	function getT33_accuracy() {
		return this.T33_accuracy;
	}
	this.getT33_accuracy=getT33_accuracy;


	function setT33_accuracy(v){
		this.T33_accuracy=v;
	}
	this.setT33_accuracy=setT33_accuracy;

	this.T33_responsetime=null;


	function getT33_responsetime() {
		return this.T33_responsetime;
	}
	this.getT33_responsetime=getT33_responsetime;


	function setT33_responsetime(v){
		this.T33_responsetime=v;
	}
	this.setT33_responsetime=setT33_responsetime;

	this.T33_congruency=null;


	function getT33_congruency() {
		return this.T33_congruency;
	}
	this.getT33_congruency=getT33_congruency;


	function setT33_congruency(v){
		this.T33_congruency=v;
	}
	this.setT33_congruency=setT33_congruency;

	this.T34_accuracy=null;


	function getT34_accuracy() {
		return this.T34_accuracy;
	}
	this.getT34_accuracy=getT34_accuracy;


	function setT34_accuracy(v){
		this.T34_accuracy=v;
	}
	this.setT34_accuracy=setT34_accuracy;

	this.T34_responsetime=null;


	function getT34_responsetime() {
		return this.T34_responsetime;
	}
	this.getT34_responsetime=getT34_responsetime;


	function setT34_responsetime(v){
		this.T34_responsetime=v;
	}
	this.setT34_responsetime=setT34_responsetime;

	this.T34_congruency=null;


	function getT34_congruency() {
		return this.T34_congruency;
	}
	this.getT34_congruency=getT34_congruency;


	function setT34_congruency(v){
		this.T34_congruency=v;
	}
	this.setT34_congruency=setT34_congruency;

	this.T35_accuracy=null;


	function getT35_accuracy() {
		return this.T35_accuracy;
	}
	this.getT35_accuracy=getT35_accuracy;


	function setT35_accuracy(v){
		this.T35_accuracy=v;
	}
	this.setT35_accuracy=setT35_accuracy;

	this.T35_responsetime=null;


	function getT35_responsetime() {
		return this.T35_responsetime;
	}
	this.getT35_responsetime=getT35_responsetime;


	function setT35_responsetime(v){
		this.T35_responsetime=v;
	}
	this.setT35_responsetime=setT35_responsetime;

	this.T35_congruency=null;


	function getT35_congruency() {
		return this.T35_congruency;
	}
	this.getT35_congruency=getT35_congruency;


	function setT35_congruency(v){
		this.T35_congruency=v;
	}
	this.setT35_congruency=setT35_congruency;

	this.T36_accuracy=null;


	function getT36_accuracy() {
		return this.T36_accuracy;
	}
	this.getT36_accuracy=getT36_accuracy;


	function setT36_accuracy(v){
		this.T36_accuracy=v;
	}
	this.setT36_accuracy=setT36_accuracy;

	this.T36_responsetime=null;


	function getT36_responsetime() {
		return this.T36_responsetime;
	}
	this.getT36_responsetime=getT36_responsetime;


	function setT36_responsetime(v){
		this.T36_responsetime=v;
	}
	this.setT36_responsetime=setT36_responsetime;

	this.T36_congruency=null;


	function getT36_congruency() {
		return this.T36_congruency;
	}
	this.getT36_congruency=getT36_congruency;


	function setT36_congruency(v){
		this.T36_congruency=v;
	}
	this.setT36_congruency=setT36_congruency;

	this.T37_accuracy=null;


	function getT37_accuracy() {
		return this.T37_accuracy;
	}
	this.getT37_accuracy=getT37_accuracy;


	function setT37_accuracy(v){
		this.T37_accuracy=v;
	}
	this.setT37_accuracy=setT37_accuracy;

	this.T37_responsetime=null;


	function getT37_responsetime() {
		return this.T37_responsetime;
	}
	this.getT37_responsetime=getT37_responsetime;


	function setT37_responsetime(v){
		this.T37_responsetime=v;
	}
	this.setT37_responsetime=setT37_responsetime;

	this.T37_congruency=null;


	function getT37_congruency() {
		return this.T37_congruency;
	}
	this.getT37_congruency=getT37_congruency;


	function setT37_congruency(v){
		this.T37_congruency=v;
	}
	this.setT37_congruency=setT37_congruency;

	this.T38_accuracy=null;


	function getT38_accuracy() {
		return this.T38_accuracy;
	}
	this.getT38_accuracy=getT38_accuracy;


	function setT38_accuracy(v){
		this.T38_accuracy=v;
	}
	this.setT38_accuracy=setT38_accuracy;

	this.T38_responsetime=null;


	function getT38_responsetime() {
		return this.T38_responsetime;
	}
	this.getT38_responsetime=getT38_responsetime;


	function setT38_responsetime(v){
		this.T38_responsetime=v;
	}
	this.setT38_responsetime=setT38_responsetime;

	this.T38_congruency=null;


	function getT38_congruency() {
		return this.T38_congruency;
	}
	this.getT38_congruency=getT38_congruency;


	function setT38_congruency(v){
		this.T38_congruency=v;
	}
	this.setT38_congruency=setT38_congruency;

	this.T39_accuracy=null;


	function getT39_accuracy() {
		return this.T39_accuracy;
	}
	this.getT39_accuracy=getT39_accuracy;


	function setT39_accuracy(v){
		this.T39_accuracy=v;
	}
	this.setT39_accuracy=setT39_accuracy;

	this.T39_responsetime=null;


	function getT39_responsetime() {
		return this.T39_responsetime;
	}
	this.getT39_responsetime=getT39_responsetime;


	function setT39_responsetime(v){
		this.T39_responsetime=v;
	}
	this.setT39_responsetime=setT39_responsetime;

	this.T39_congruency=null;


	function getT39_congruency() {
		return this.T39_congruency;
	}
	this.getT39_congruency=getT39_congruency;


	function setT39_congruency(v){
		this.T39_congruency=v;
	}
	this.setT39_congruency=setT39_congruency;

	this.T40_accuracy=null;


	function getT40_accuracy() {
		return this.T40_accuracy;
	}
	this.getT40_accuracy=getT40_accuracy;


	function setT40_accuracy(v){
		this.T40_accuracy=v;
	}
	this.setT40_accuracy=setT40_accuracy;

	this.T40_responsetime=null;


	function getT40_responsetime() {
		return this.T40_responsetime;
	}
	this.getT40_responsetime=getT40_responsetime;


	function setT40_responsetime(v){
		this.T40_responsetime=v;
	}
	this.setT40_responsetime=setT40_responsetime;

	this.T40_congruency=null;


	function getT40_congruency() {
		return this.T40_congruency;
	}
	this.getT40_congruency=getT40_congruency;


	function setT40_congruency(v){
		this.T40_congruency=v;
	}
	this.setT40_congruency=setT40_congruency;

	this.T41_accuracy=null;


	function getT41_accuracy() {
		return this.T41_accuracy;
	}
	this.getT41_accuracy=getT41_accuracy;


	function setT41_accuracy(v){
		this.T41_accuracy=v;
	}
	this.setT41_accuracy=setT41_accuracy;

	this.T41_responsetime=null;


	function getT41_responsetime() {
		return this.T41_responsetime;
	}
	this.getT41_responsetime=getT41_responsetime;


	function setT41_responsetime(v){
		this.T41_responsetime=v;
	}
	this.setT41_responsetime=setT41_responsetime;

	this.T41_congruency=null;


	function getT41_congruency() {
		return this.T41_congruency;
	}
	this.getT41_congruency=getT41_congruency;


	function setT41_congruency(v){
		this.T41_congruency=v;
	}
	this.setT41_congruency=setT41_congruency;

	this.T42_accuracy=null;


	function getT42_accuracy() {
		return this.T42_accuracy;
	}
	this.getT42_accuracy=getT42_accuracy;


	function setT42_accuracy(v){
		this.T42_accuracy=v;
	}
	this.setT42_accuracy=setT42_accuracy;

	this.T42_responsetime=null;


	function getT42_responsetime() {
		return this.T42_responsetime;
	}
	this.getT42_responsetime=getT42_responsetime;


	function setT42_responsetime(v){
		this.T42_responsetime=v;
	}
	this.setT42_responsetime=setT42_responsetime;

	this.T42_congruency=null;


	function getT42_congruency() {
		return this.T42_congruency;
	}
	this.getT42_congruency=getT42_congruency;


	function setT42_congruency(v){
		this.T42_congruency=v;
	}
	this.setT42_congruency=setT42_congruency;

	this.T43_accuracy=null;


	function getT43_accuracy() {
		return this.T43_accuracy;
	}
	this.getT43_accuracy=getT43_accuracy;


	function setT43_accuracy(v){
		this.T43_accuracy=v;
	}
	this.setT43_accuracy=setT43_accuracy;

	this.T43_responsetime=null;


	function getT43_responsetime() {
		return this.T43_responsetime;
	}
	this.getT43_responsetime=getT43_responsetime;


	function setT43_responsetime(v){
		this.T43_responsetime=v;
	}
	this.setT43_responsetime=setT43_responsetime;

	this.T43_congruency=null;


	function getT43_congruency() {
		return this.T43_congruency;
	}
	this.getT43_congruency=getT43_congruency;


	function setT43_congruency(v){
		this.T43_congruency=v;
	}
	this.setT43_congruency=setT43_congruency;

	this.T44_accuracy=null;


	function getT44_accuracy() {
		return this.T44_accuracy;
	}
	this.getT44_accuracy=getT44_accuracy;


	function setT44_accuracy(v){
		this.T44_accuracy=v;
	}
	this.setT44_accuracy=setT44_accuracy;

	this.T44_responsetime=null;


	function getT44_responsetime() {
		return this.T44_responsetime;
	}
	this.getT44_responsetime=getT44_responsetime;


	function setT44_responsetime(v){
		this.T44_responsetime=v;
	}
	this.setT44_responsetime=setT44_responsetime;

	this.T44_congruency=null;


	function getT44_congruency() {
		return this.T44_congruency;
	}
	this.getT44_congruency=getT44_congruency;


	function setT44_congruency(v){
		this.T44_congruency=v;
	}
	this.setT44_congruency=setT44_congruency;

	this.T45_accuracy=null;


	function getT45_accuracy() {
		return this.T45_accuracy;
	}
	this.getT45_accuracy=getT45_accuracy;


	function setT45_accuracy(v){
		this.T45_accuracy=v;
	}
	this.setT45_accuracy=setT45_accuracy;

	this.T45_responsetime=null;


	function getT45_responsetime() {
		return this.T45_responsetime;
	}
	this.getT45_responsetime=getT45_responsetime;


	function setT45_responsetime(v){
		this.T45_responsetime=v;
	}
	this.setT45_responsetime=setT45_responsetime;

	this.T45_congruency=null;


	function getT45_congruency() {
		return this.T45_congruency;
	}
	this.getT45_congruency=getT45_congruency;


	function setT45_congruency(v){
		this.T45_congruency=v;
	}
	this.setT45_congruency=setT45_congruency;

	this.T46_accuracy=null;


	function getT46_accuracy() {
		return this.T46_accuracy;
	}
	this.getT46_accuracy=getT46_accuracy;


	function setT46_accuracy(v){
		this.T46_accuracy=v;
	}
	this.setT46_accuracy=setT46_accuracy;

	this.T46_responsetime=null;


	function getT46_responsetime() {
		return this.T46_responsetime;
	}
	this.getT46_responsetime=getT46_responsetime;


	function setT46_responsetime(v){
		this.T46_responsetime=v;
	}
	this.setT46_responsetime=setT46_responsetime;

	this.T46_congruency=null;


	function getT46_congruency() {
		return this.T46_congruency;
	}
	this.getT46_congruency=getT46_congruency;


	function setT46_congruency(v){
		this.T46_congruency=v;
	}
	this.setT46_congruency=setT46_congruency;

	this.T47_accuracy=null;


	function getT47_accuracy() {
		return this.T47_accuracy;
	}
	this.getT47_accuracy=getT47_accuracy;


	function setT47_accuracy(v){
		this.T47_accuracy=v;
	}
	this.setT47_accuracy=setT47_accuracy;

	this.T47_responsetime=null;


	function getT47_responsetime() {
		return this.T47_responsetime;
	}
	this.getT47_responsetime=getT47_responsetime;


	function setT47_responsetime(v){
		this.T47_responsetime=v;
	}
	this.setT47_responsetime=setT47_responsetime;

	this.T47_congruency=null;


	function getT47_congruency() {
		return this.T47_congruency;
	}
	this.getT47_congruency=getT47_congruency;


	function setT47_congruency(v){
		this.T47_congruency=v;
	}
	this.setT47_congruency=setT47_congruency;

	this.T48_accuracy=null;


	function getT48_accuracy() {
		return this.T48_accuracy;
	}
	this.getT48_accuracy=getT48_accuracy;


	function setT48_accuracy(v){
		this.T48_accuracy=v;
	}
	this.setT48_accuracy=setT48_accuracy;

	this.T48_responsetime=null;


	function getT48_responsetime() {
		return this.T48_responsetime;
	}
	this.getT48_responsetime=getT48_responsetime;


	function setT48_responsetime(v){
		this.T48_responsetime=v;
	}
	this.setT48_responsetime=setT48_responsetime;

	this.T48_congruency=null;


	function getT48_congruency() {
		return this.T48_congruency;
	}
	this.getT48_congruency=getT48_congruency;


	function setT48_congruency(v){
		this.T48_congruency=v;
	}
	this.setT48_congruency=setT48_congruency;

	this.T49_accuracy=null;


	function getT49_accuracy() {
		return this.T49_accuracy;
	}
	this.getT49_accuracy=getT49_accuracy;


	function setT49_accuracy(v){
		this.T49_accuracy=v;
	}
	this.setT49_accuracy=setT49_accuracy;

	this.T49_responsetime=null;


	function getT49_responsetime() {
		return this.T49_responsetime;
	}
	this.getT49_responsetime=getT49_responsetime;


	function setT49_responsetime(v){
		this.T49_responsetime=v;
	}
	this.setT49_responsetime=setT49_responsetime;

	this.T49_congruency=null;


	function getT49_congruency() {
		return this.T49_congruency;
	}
	this.getT49_congruency=getT49_congruency;


	function setT49_congruency(v){
		this.T49_congruency=v;
	}
	this.setT49_congruency=setT49_congruency;

	this.T50_accuracy=null;


	function getT50_accuracy() {
		return this.T50_accuracy;
	}
	this.getT50_accuracy=getT50_accuracy;


	function setT50_accuracy(v){
		this.T50_accuracy=v;
	}
	this.setT50_accuracy=setT50_accuracy;

	this.T50_responsetime=null;


	function getT50_responsetime() {
		return this.T50_responsetime;
	}
	this.getT50_responsetime=getT50_responsetime;


	function setT50_responsetime(v){
		this.T50_responsetime=v;
	}
	this.setT50_responsetime=setT50_responsetime;

	this.T50_congruency=null;


	function getT50_congruency() {
		return this.T50_congruency;
	}
	this.getT50_congruency=getT50_congruency;


	function setT50_congruency(v){
		this.T50_congruency=v;
	}
	this.setT50_congruency=setT50_congruency;

	this.T51_accuracy=null;


	function getT51_accuracy() {
		return this.T51_accuracy;
	}
	this.getT51_accuracy=getT51_accuracy;


	function setT51_accuracy(v){
		this.T51_accuracy=v;
	}
	this.setT51_accuracy=setT51_accuracy;

	this.T51_responsetime=null;


	function getT51_responsetime() {
		return this.T51_responsetime;
	}
	this.getT51_responsetime=getT51_responsetime;


	function setT51_responsetime(v){
		this.T51_responsetime=v;
	}
	this.setT51_responsetime=setT51_responsetime;

	this.T51_congruency=null;


	function getT51_congruency() {
		return this.T51_congruency;
	}
	this.getT51_congruency=getT51_congruency;


	function setT51_congruency(v){
		this.T51_congruency=v;
	}
	this.setT51_congruency=setT51_congruency;

	this.T52_accuracy=null;


	function getT52_accuracy() {
		return this.T52_accuracy;
	}
	this.getT52_accuracy=getT52_accuracy;


	function setT52_accuracy(v){
		this.T52_accuracy=v;
	}
	this.setT52_accuracy=setT52_accuracy;

	this.T52_responsetime=null;


	function getT52_responsetime() {
		return this.T52_responsetime;
	}
	this.getT52_responsetime=getT52_responsetime;


	function setT52_responsetime(v){
		this.T52_responsetime=v;
	}
	this.setT52_responsetime=setT52_responsetime;

	this.T52_congruency=null;


	function getT52_congruency() {
		return this.T52_congruency;
	}
	this.getT52_congruency=getT52_congruency;


	function setT52_congruency(v){
		this.T52_congruency=v;
	}
	this.setT52_congruency=setT52_congruency;

	this.T53_accuracy=null;


	function getT53_accuracy() {
		return this.T53_accuracy;
	}
	this.getT53_accuracy=getT53_accuracy;


	function setT53_accuracy(v){
		this.T53_accuracy=v;
	}
	this.setT53_accuracy=setT53_accuracy;

	this.T53_responsetime=null;


	function getT53_responsetime() {
		return this.T53_responsetime;
	}
	this.getT53_responsetime=getT53_responsetime;


	function setT53_responsetime(v){
		this.T53_responsetime=v;
	}
	this.setT53_responsetime=setT53_responsetime;

	this.T53_congruency=null;


	function getT53_congruency() {
		return this.T53_congruency;
	}
	this.getT53_congruency=getT53_congruency;


	function setT53_congruency(v){
		this.T53_congruency=v;
	}
	this.setT53_congruency=setT53_congruency;

	this.T54_accuracy=null;


	function getT54_accuracy() {
		return this.T54_accuracy;
	}
	this.getT54_accuracy=getT54_accuracy;


	function setT54_accuracy(v){
		this.T54_accuracy=v;
	}
	this.setT54_accuracy=setT54_accuracy;

	this.T54_responsetime=null;


	function getT54_responsetime() {
		return this.T54_responsetime;
	}
	this.getT54_responsetime=getT54_responsetime;


	function setT54_responsetime(v){
		this.T54_responsetime=v;
	}
	this.setT54_responsetime=setT54_responsetime;

	this.T54_congruency=null;


	function getT54_congruency() {
		return this.T54_congruency;
	}
	this.getT54_congruency=getT54_congruency;


	function setT54_congruency(v){
		this.T54_congruency=v;
	}
	this.setT54_congruency=setT54_congruency;

	this.T55_accuracy=null;


	function getT55_accuracy() {
		return this.T55_accuracy;
	}
	this.getT55_accuracy=getT55_accuracy;


	function setT55_accuracy(v){
		this.T55_accuracy=v;
	}
	this.setT55_accuracy=setT55_accuracy;

	this.T55_responsetime=null;


	function getT55_responsetime() {
		return this.T55_responsetime;
	}
	this.getT55_responsetime=getT55_responsetime;


	function setT55_responsetime(v){
		this.T55_responsetime=v;
	}
	this.setT55_responsetime=setT55_responsetime;

	this.T55_congruency=null;


	function getT55_congruency() {
		return this.T55_congruency;
	}
	this.getT55_congruency=getT55_congruency;


	function setT55_congruency(v){
		this.T55_congruency=v;
	}
	this.setT55_congruency=setT55_congruency;

	this.T56_accuracy=null;


	function getT56_accuracy() {
		return this.T56_accuracy;
	}
	this.getT56_accuracy=getT56_accuracy;


	function setT56_accuracy(v){
		this.T56_accuracy=v;
	}
	this.setT56_accuracy=setT56_accuracy;

	this.T56_responsetime=null;


	function getT56_responsetime() {
		return this.T56_responsetime;
	}
	this.getT56_responsetime=getT56_responsetime;


	function setT56_responsetime(v){
		this.T56_responsetime=v;
	}
	this.setT56_responsetime=setT56_responsetime;

	this.T56_congruency=null;


	function getT56_congruency() {
		return this.T56_congruency;
	}
	this.getT56_congruency=getT56_congruency;


	function setT56_congruency(v){
		this.T56_congruency=v;
	}
	this.setT56_congruency=setT56_congruency;

	this.T57_accuracy=null;


	function getT57_accuracy() {
		return this.T57_accuracy;
	}
	this.getT57_accuracy=getT57_accuracy;


	function setT57_accuracy(v){
		this.T57_accuracy=v;
	}
	this.setT57_accuracy=setT57_accuracy;

	this.T57_responsetime=null;


	function getT57_responsetime() {
		return this.T57_responsetime;
	}
	this.getT57_responsetime=getT57_responsetime;


	function setT57_responsetime(v){
		this.T57_responsetime=v;
	}
	this.setT57_responsetime=setT57_responsetime;

	this.T57_congruency=null;


	function getT57_congruency() {
		return this.T57_congruency;
	}
	this.getT57_congruency=getT57_congruency;


	function setT57_congruency(v){
		this.T57_congruency=v;
	}
	this.setT57_congruency=setT57_congruency;

	this.T58_accuracy=null;


	function getT58_accuracy() {
		return this.T58_accuracy;
	}
	this.getT58_accuracy=getT58_accuracy;


	function setT58_accuracy(v){
		this.T58_accuracy=v;
	}
	this.setT58_accuracy=setT58_accuracy;

	this.T58_responsetime=null;


	function getT58_responsetime() {
		return this.T58_responsetime;
	}
	this.getT58_responsetime=getT58_responsetime;


	function setT58_responsetime(v){
		this.T58_responsetime=v;
	}
	this.setT58_responsetime=setT58_responsetime;

	this.T58_congruency=null;


	function getT58_congruency() {
		return this.T58_congruency;
	}
	this.getT58_congruency=getT58_congruency;


	function setT58_congruency(v){
		this.T58_congruency=v;
	}
	this.setT58_congruency=setT58_congruency;

	this.T59_accuracy=null;


	function getT59_accuracy() {
		return this.T59_accuracy;
	}
	this.getT59_accuracy=getT59_accuracy;


	function setT59_accuracy(v){
		this.T59_accuracy=v;
	}
	this.setT59_accuracy=setT59_accuracy;

	this.T59_responsetime=null;


	function getT59_responsetime() {
		return this.T59_responsetime;
	}
	this.getT59_responsetime=getT59_responsetime;


	function setT59_responsetime(v){
		this.T59_responsetime=v;
	}
	this.setT59_responsetime=setT59_responsetime;

	this.T59_congruency=null;


	function getT59_congruency() {
		return this.T59_congruency;
	}
	this.getT59_congruency=getT59_congruency;


	function setT59_congruency(v){
		this.T59_congruency=v;
	}
	this.setT59_congruency=setT59_congruency;

	this.T60_accuracy=null;


	function getT60_accuracy() {
		return this.T60_accuracy;
	}
	this.getT60_accuracy=getT60_accuracy;


	function setT60_accuracy(v){
		this.T60_accuracy=v;
	}
	this.setT60_accuracy=setT60_accuracy;

	this.T60_responsetime=null;


	function getT60_responsetime() {
		return this.T60_responsetime;
	}
	this.getT60_responsetime=getT60_responsetime;


	function setT60_responsetime(v){
		this.T60_responsetime=v;
	}
	this.setT60_responsetime=setT60_responsetime;

	this.T60_congruency=null;


	function getT60_congruency() {
		return this.T60_congruency;
	}
	this.getT60_congruency=getT60_congruency;


	function setT60_congruency(v){
		this.T60_congruency=v;
	}
	this.setT60_congruency=setT60_congruency;

	this.T61_accuracy=null;


	function getT61_accuracy() {
		return this.T61_accuracy;
	}
	this.getT61_accuracy=getT61_accuracy;


	function setT61_accuracy(v){
		this.T61_accuracy=v;
	}
	this.setT61_accuracy=setT61_accuracy;

	this.T61_responsetime=null;


	function getT61_responsetime() {
		return this.T61_responsetime;
	}
	this.getT61_responsetime=getT61_responsetime;


	function setT61_responsetime(v){
		this.T61_responsetime=v;
	}
	this.setT61_responsetime=setT61_responsetime;

	this.T61_congruency=null;


	function getT61_congruency() {
		return this.T61_congruency;
	}
	this.getT61_congruency=getT61_congruency;


	function setT61_congruency(v){
		this.T61_congruency=v;
	}
	this.setT61_congruency=setT61_congruency;

	this.T62_accuracy=null;


	function getT62_accuracy() {
		return this.T62_accuracy;
	}
	this.getT62_accuracy=getT62_accuracy;


	function setT62_accuracy(v){
		this.T62_accuracy=v;
	}
	this.setT62_accuracy=setT62_accuracy;

	this.T62_responsetime=null;


	function getT62_responsetime() {
		return this.T62_responsetime;
	}
	this.getT62_responsetime=getT62_responsetime;


	function setT62_responsetime(v){
		this.T62_responsetime=v;
	}
	this.setT62_responsetime=setT62_responsetime;

	this.T62_congruency=null;


	function getT62_congruency() {
		return this.T62_congruency;
	}
	this.getT62_congruency=getT62_congruency;


	function setT62_congruency(v){
		this.T62_congruency=v;
	}
	this.setT62_congruency=setT62_congruency;

	this.T63_accuracy=null;


	function getT63_accuracy() {
		return this.T63_accuracy;
	}
	this.getT63_accuracy=getT63_accuracy;


	function setT63_accuracy(v){
		this.T63_accuracy=v;
	}
	this.setT63_accuracy=setT63_accuracy;

	this.T63_responsetime=null;


	function getT63_responsetime() {
		return this.T63_responsetime;
	}
	this.getT63_responsetime=getT63_responsetime;


	function setT63_responsetime(v){
		this.T63_responsetime=v;
	}
	this.setT63_responsetime=setT63_responsetime;

	this.T63_congruency=null;


	function getT63_congruency() {
		return this.T63_congruency;
	}
	this.getT63_congruency=getT63_congruency;


	function setT63_congruency(v){
		this.T63_congruency=v;
	}
	this.setT63_congruency=setT63_congruency;

	this.T64_accuracy=null;


	function getT64_accuracy() {
		return this.T64_accuracy;
	}
	this.getT64_accuracy=getT64_accuracy;


	function setT64_accuracy(v){
		this.T64_accuracy=v;
	}
	this.setT64_accuracy=setT64_accuracy;

	this.T64_responsetime=null;


	function getT64_responsetime() {
		return this.T64_responsetime;
	}
	this.getT64_responsetime=getT64_responsetime;


	function setT64_responsetime(v){
		this.T64_responsetime=v;
	}
	this.setT64_responsetime=setT64_responsetime;

	this.T64_congruency=null;


	function getT64_congruency() {
		return this.T64_congruency;
	}
	this.getT64_congruency=getT64_congruency;


	function setT64_congruency(v){
		this.T64_congruency=v;
	}
	this.setT64_congruency=setT64_congruency;

	this.T65_accuracy=null;


	function getT65_accuracy() {
		return this.T65_accuracy;
	}
	this.getT65_accuracy=getT65_accuracy;


	function setT65_accuracy(v){
		this.T65_accuracy=v;
	}
	this.setT65_accuracy=setT65_accuracy;

	this.T65_responsetime=null;


	function getT65_responsetime() {
		return this.T65_responsetime;
	}
	this.getT65_responsetime=getT65_responsetime;


	function setT65_responsetime(v){
		this.T65_responsetime=v;
	}
	this.setT65_responsetime=setT65_responsetime;

	this.T65_congruency=null;


	function getT65_congruency() {
		return this.T65_congruency;
	}
	this.getT65_congruency=getT65_congruency;


	function setT65_congruency(v){
		this.T65_congruency=v;
	}
	this.setT65_congruency=setT65_congruency;

	this.T66_accuracy=null;


	function getT66_accuracy() {
		return this.T66_accuracy;
	}
	this.getT66_accuracy=getT66_accuracy;


	function setT66_accuracy(v){
		this.T66_accuracy=v;
	}
	this.setT66_accuracy=setT66_accuracy;

	this.T66_responsetime=null;


	function getT66_responsetime() {
		return this.T66_responsetime;
	}
	this.getT66_responsetime=getT66_responsetime;


	function setT66_responsetime(v){
		this.T66_responsetime=v;
	}
	this.setT66_responsetime=setT66_responsetime;

	this.T66_congruency=null;


	function getT66_congruency() {
		return this.T66_congruency;
	}
	this.getT66_congruency=getT66_congruency;


	function setT66_congruency(v){
		this.T66_congruency=v;
	}
	this.setT66_congruency=setT66_congruency;

	this.T67_accuracy=null;


	function getT67_accuracy() {
		return this.T67_accuracy;
	}
	this.getT67_accuracy=getT67_accuracy;


	function setT67_accuracy(v){
		this.T67_accuracy=v;
	}
	this.setT67_accuracy=setT67_accuracy;

	this.T67_responsetime=null;


	function getT67_responsetime() {
		return this.T67_responsetime;
	}
	this.getT67_responsetime=getT67_responsetime;


	function setT67_responsetime(v){
		this.T67_responsetime=v;
	}
	this.setT67_responsetime=setT67_responsetime;

	this.T67_congruency=null;


	function getT67_congruency() {
		return this.T67_congruency;
	}
	this.getT67_congruency=getT67_congruency;


	function setT67_congruency(v){
		this.T67_congruency=v;
	}
	this.setT67_congruency=setT67_congruency;

	this.T68_accuracy=null;


	function getT68_accuracy() {
		return this.T68_accuracy;
	}
	this.getT68_accuracy=getT68_accuracy;


	function setT68_accuracy(v){
		this.T68_accuracy=v;
	}
	this.setT68_accuracy=setT68_accuracy;

	this.T68_responsetime=null;


	function getT68_responsetime() {
		return this.T68_responsetime;
	}
	this.getT68_responsetime=getT68_responsetime;


	function setT68_responsetime(v){
		this.T68_responsetime=v;
	}
	this.setT68_responsetime=setT68_responsetime;

	this.T68_congruency=null;


	function getT68_congruency() {
		return this.T68_congruency;
	}
	this.getT68_congruency=getT68_congruency;


	function setT68_congruency(v){
		this.T68_congruency=v;
	}
	this.setT68_congruency=setT68_congruency;

	this.T69_accuracy=null;


	function getT69_accuracy() {
		return this.T69_accuracy;
	}
	this.getT69_accuracy=getT69_accuracy;


	function setT69_accuracy(v){
		this.T69_accuracy=v;
	}
	this.setT69_accuracy=setT69_accuracy;

	this.T69_responsetime=null;


	function getT69_responsetime() {
		return this.T69_responsetime;
	}
	this.getT69_responsetime=getT69_responsetime;


	function setT69_responsetime(v){
		this.T69_responsetime=v;
	}
	this.setT69_responsetime=setT69_responsetime;

	this.T69_congruency=null;


	function getT69_congruency() {
		return this.T69_congruency;
	}
	this.getT69_congruency=getT69_congruency;


	function setT69_congruency(v){
		this.T69_congruency=v;
	}
	this.setT69_congruency=setT69_congruency;

	this.T70_accuracy=null;


	function getT70_accuracy() {
		return this.T70_accuracy;
	}
	this.getT70_accuracy=getT70_accuracy;


	function setT70_accuracy(v){
		this.T70_accuracy=v;
	}
	this.setT70_accuracy=setT70_accuracy;

	this.T70_responsetime=null;


	function getT70_responsetime() {
		return this.T70_responsetime;
	}
	this.getT70_responsetime=getT70_responsetime;


	function setT70_responsetime(v){
		this.T70_responsetime=v;
	}
	this.setT70_responsetime=setT70_responsetime;

	this.T70_congruency=null;


	function getT70_congruency() {
		return this.T70_congruency;
	}
	this.getT70_congruency=getT70_congruency;


	function setT70_congruency(v){
		this.T70_congruency=v;
	}
	this.setT70_congruency=setT70_congruency;

	this.T71_accuracy=null;


	function getT71_accuracy() {
		return this.T71_accuracy;
	}
	this.getT71_accuracy=getT71_accuracy;


	function setT71_accuracy(v){
		this.T71_accuracy=v;
	}
	this.setT71_accuracy=setT71_accuracy;

	this.T71_responsetime=null;


	function getT71_responsetime() {
		return this.T71_responsetime;
	}
	this.getT71_responsetime=getT71_responsetime;


	function setT71_responsetime(v){
		this.T71_responsetime=v;
	}
	this.setT71_responsetime=setT71_responsetime;

	this.T71_congruency=null;


	function getT71_congruency() {
		return this.T71_congruency;
	}
	this.getT71_congruency=getT71_congruency;


	function setT71_congruency(v){
		this.T71_congruency=v;
	}
	this.setT71_congruency=setT71_congruency;

	this.T72_accuracy=null;


	function getT72_accuracy() {
		return this.T72_accuracy;
	}
	this.getT72_accuracy=getT72_accuracy;


	function setT72_accuracy(v){
		this.T72_accuracy=v;
	}
	this.setT72_accuracy=setT72_accuracy;

	this.T72_responsetime=null;


	function getT72_responsetime() {
		return this.T72_responsetime;
	}
	this.getT72_responsetime=getT72_responsetime;


	function setT72_responsetime(v){
		this.T72_responsetime=v;
	}
	this.setT72_responsetime=setT72_responsetime;

	this.T72_congruency=null;


	function getT72_congruency() {
		return this.T72_congruency;
	}
	this.getT72_congruency=getT72_congruency;


	function setT72_congruency(v){
		this.T72_congruency=v;
	}
	this.setT72_congruency=setT72_congruency;

	this.T73_accuracy=null;


	function getT73_accuracy() {
		return this.T73_accuracy;
	}
	this.getT73_accuracy=getT73_accuracy;


	function setT73_accuracy(v){
		this.T73_accuracy=v;
	}
	this.setT73_accuracy=setT73_accuracy;

	this.T73_responsetime=null;


	function getT73_responsetime() {
		return this.T73_responsetime;
	}
	this.getT73_responsetime=getT73_responsetime;


	function setT73_responsetime(v){
		this.T73_responsetime=v;
	}
	this.setT73_responsetime=setT73_responsetime;

	this.T73_congruency=null;


	function getT73_congruency() {
		return this.T73_congruency;
	}
	this.getT73_congruency=getT73_congruency;


	function setT73_congruency(v){
		this.T73_congruency=v;
	}
	this.setT73_congruency=setT73_congruency;

	this.T74_accuracy=null;


	function getT74_accuracy() {
		return this.T74_accuracy;
	}
	this.getT74_accuracy=getT74_accuracy;


	function setT74_accuracy(v){
		this.T74_accuracy=v;
	}
	this.setT74_accuracy=setT74_accuracy;

	this.T74_responsetime=null;


	function getT74_responsetime() {
		return this.T74_responsetime;
	}
	this.getT74_responsetime=getT74_responsetime;


	function setT74_responsetime(v){
		this.T74_responsetime=v;
	}
	this.setT74_responsetime=setT74_responsetime;

	this.T74_congruency=null;


	function getT74_congruency() {
		return this.T74_congruency;
	}
	this.getT74_congruency=getT74_congruency;


	function setT74_congruency(v){
		this.T74_congruency=v;
	}
	this.setT74_congruency=setT74_congruency;

	this.T75_accuracy=null;


	function getT75_accuracy() {
		return this.T75_accuracy;
	}
	this.getT75_accuracy=getT75_accuracy;


	function setT75_accuracy(v){
		this.T75_accuracy=v;
	}
	this.setT75_accuracy=setT75_accuracy;

	this.T75_responsetime=null;


	function getT75_responsetime() {
		return this.T75_responsetime;
	}
	this.getT75_responsetime=getT75_responsetime;


	function setT75_responsetime(v){
		this.T75_responsetime=v;
	}
	this.setT75_responsetime=setT75_responsetime;

	this.T75_congruency=null;


	function getT75_congruency() {
		return this.T75_congruency;
	}
	this.getT75_congruency=getT75_congruency;


	function setT75_congruency(v){
		this.T75_congruency=v;
	}
	this.setT75_congruency=setT75_congruency;

	this.T76_accuracy=null;


	function getT76_accuracy() {
		return this.T76_accuracy;
	}
	this.getT76_accuracy=getT76_accuracy;


	function setT76_accuracy(v){
		this.T76_accuracy=v;
	}
	this.setT76_accuracy=setT76_accuracy;

	this.T76_responsetime=null;


	function getT76_responsetime() {
		return this.T76_responsetime;
	}
	this.getT76_responsetime=getT76_responsetime;


	function setT76_responsetime(v){
		this.T76_responsetime=v;
	}
	this.setT76_responsetime=setT76_responsetime;

	this.T76_congruency=null;


	function getT76_congruency() {
		return this.T76_congruency;
	}
	this.getT76_congruency=getT76_congruency;


	function setT76_congruency(v){
		this.T76_congruency=v;
	}
	this.setT76_congruency=setT76_congruency;

	this.T77_accuracy=null;


	function getT77_accuracy() {
		return this.T77_accuracy;
	}
	this.getT77_accuracy=getT77_accuracy;


	function setT77_accuracy(v){
		this.T77_accuracy=v;
	}
	this.setT77_accuracy=setT77_accuracy;

	this.T77_responsetime=null;


	function getT77_responsetime() {
		return this.T77_responsetime;
	}
	this.getT77_responsetime=getT77_responsetime;


	function setT77_responsetime(v){
		this.T77_responsetime=v;
	}
	this.setT77_responsetime=setT77_responsetime;

	this.T77_congruency=null;


	function getT77_congruency() {
		return this.T77_congruency;
	}
	this.getT77_congruency=getT77_congruency;


	function setT77_congruency(v){
		this.T77_congruency=v;
	}
	this.setT77_congruency=setT77_congruency;

	this.T78_accuracy=null;


	function getT78_accuracy() {
		return this.T78_accuracy;
	}
	this.getT78_accuracy=getT78_accuracy;


	function setT78_accuracy(v){
		this.T78_accuracy=v;
	}
	this.setT78_accuracy=setT78_accuracy;

	this.T78_responsetime=null;


	function getT78_responsetime() {
		return this.T78_responsetime;
	}
	this.getT78_responsetime=getT78_responsetime;


	function setT78_responsetime(v){
		this.T78_responsetime=v;
	}
	this.setT78_responsetime=setT78_responsetime;

	this.T78_congruency=null;


	function getT78_congruency() {
		return this.T78_congruency;
	}
	this.getT78_congruency=getT78_congruency;


	function setT78_congruency(v){
		this.T78_congruency=v;
	}
	this.setT78_congruency=setT78_congruency;

	this.T79_accuracy=null;


	function getT79_accuracy() {
		return this.T79_accuracy;
	}
	this.getT79_accuracy=getT79_accuracy;


	function setT79_accuracy(v){
		this.T79_accuracy=v;
	}
	this.setT79_accuracy=setT79_accuracy;

	this.T79_responsetime=null;


	function getT79_responsetime() {
		return this.T79_responsetime;
	}
	this.getT79_responsetime=getT79_responsetime;


	function setT79_responsetime(v){
		this.T79_responsetime=v;
	}
	this.setT79_responsetime=setT79_responsetime;

	this.T79_congruency=null;


	function getT79_congruency() {
		return this.T79_congruency;
	}
	this.getT79_congruency=getT79_congruency;


	function setT79_congruency(v){
		this.T79_congruency=v;
	}
	this.setT79_congruency=setT79_congruency;

	this.T80_accuracy=null;


	function getT80_accuracy() {
		return this.T80_accuracy;
	}
	this.getT80_accuracy=getT80_accuracy;


	function setT80_accuracy(v){
		this.T80_accuracy=v;
	}
	this.setT80_accuracy=setT80_accuracy;

	this.T80_responsetime=null;


	function getT80_responsetime() {
		return this.T80_responsetime;
	}
	this.getT80_responsetime=getT80_responsetime;


	function setT80_responsetime(v){
		this.T80_responsetime=v;
	}
	this.setT80_responsetime=setT80_responsetime;

	this.T80_congruency=null;


	function getT80_congruency() {
		return this.T80_congruency;
	}
	this.getT80_congruency=getT80_congruency;


	function setT80_congruency(v){
		this.T80_congruency=v;
	}
	this.setT80_congruency=setT80_congruency;

	this.T81_accuracy=null;


	function getT81_accuracy() {
		return this.T81_accuracy;
	}
	this.getT81_accuracy=getT81_accuracy;


	function setT81_accuracy(v){
		this.T81_accuracy=v;
	}
	this.setT81_accuracy=setT81_accuracy;

	this.T81_responsetime=null;


	function getT81_responsetime() {
		return this.T81_responsetime;
	}
	this.getT81_responsetime=getT81_responsetime;


	function setT81_responsetime(v){
		this.T81_responsetime=v;
	}
	this.setT81_responsetime=setT81_responsetime;

	this.T81_congruency=null;


	function getT81_congruency() {
		return this.T81_congruency;
	}
	this.getT81_congruency=getT81_congruency;


	function setT81_congruency(v){
		this.T81_congruency=v;
	}
	this.setT81_congruency=setT81_congruency;

	this.T82_accuracy=null;


	function getT82_accuracy() {
		return this.T82_accuracy;
	}
	this.getT82_accuracy=getT82_accuracy;


	function setT82_accuracy(v){
		this.T82_accuracy=v;
	}
	this.setT82_accuracy=setT82_accuracy;

	this.T82_responsetime=null;


	function getT82_responsetime() {
		return this.T82_responsetime;
	}
	this.getT82_responsetime=getT82_responsetime;


	function setT82_responsetime(v){
		this.T82_responsetime=v;
	}
	this.setT82_responsetime=setT82_responsetime;

	this.T82_congruency=null;


	function getT82_congruency() {
		return this.T82_congruency;
	}
	this.getT82_congruency=getT82_congruency;


	function setT82_congruency(v){
		this.T82_congruency=v;
	}
	this.setT82_congruency=setT82_congruency;

	this.T83_accuracy=null;


	function getT83_accuracy() {
		return this.T83_accuracy;
	}
	this.getT83_accuracy=getT83_accuracy;


	function setT83_accuracy(v){
		this.T83_accuracy=v;
	}
	this.setT83_accuracy=setT83_accuracy;

	this.T83_responsetime=null;


	function getT83_responsetime() {
		return this.T83_responsetime;
	}
	this.getT83_responsetime=getT83_responsetime;


	function setT83_responsetime(v){
		this.T83_responsetime=v;
	}
	this.setT83_responsetime=setT83_responsetime;

	this.T83_congruency=null;


	function getT83_congruency() {
		return this.T83_congruency;
	}
	this.getT83_congruency=getT83_congruency;


	function setT83_congruency(v){
		this.T83_congruency=v;
	}
	this.setT83_congruency=setT83_congruency;

	this.T84_accuracy=null;


	function getT84_accuracy() {
		return this.T84_accuracy;
	}
	this.getT84_accuracy=getT84_accuracy;


	function setT84_accuracy(v){
		this.T84_accuracy=v;
	}
	this.setT84_accuracy=setT84_accuracy;

	this.T84_responsetime=null;


	function getT84_responsetime() {
		return this.T84_responsetime;
	}
	this.getT84_responsetime=getT84_responsetime;


	function setT84_responsetime(v){
		this.T84_responsetime=v;
	}
	this.setT84_responsetime=setT84_responsetime;

	this.T84_congruency=null;


	function getT84_congruency() {
		return this.T84_congruency;
	}
	this.getT84_congruency=getT84_congruency;


	function setT84_congruency(v){
		this.T84_congruency=v;
	}
	this.setT84_congruency=setT84_congruency;

	this.T85_accuracy=null;


	function getT85_accuracy() {
		return this.T85_accuracy;
	}
	this.getT85_accuracy=getT85_accuracy;


	function setT85_accuracy(v){
		this.T85_accuracy=v;
	}
	this.setT85_accuracy=setT85_accuracy;

	this.T85_responsetime=null;


	function getT85_responsetime() {
		return this.T85_responsetime;
	}
	this.getT85_responsetime=getT85_responsetime;


	function setT85_responsetime(v){
		this.T85_responsetime=v;
	}
	this.setT85_responsetime=setT85_responsetime;

	this.T85_congruency=null;


	function getT85_congruency() {
		return this.T85_congruency;
	}
	this.getT85_congruency=getT85_congruency;


	function setT85_congruency(v){
		this.T85_congruency=v;
	}
	this.setT85_congruency=setT85_congruency;

	this.T86_accuracy=null;


	function getT86_accuracy() {
		return this.T86_accuracy;
	}
	this.getT86_accuracy=getT86_accuracy;


	function setT86_accuracy(v){
		this.T86_accuracy=v;
	}
	this.setT86_accuracy=setT86_accuracy;

	this.T86_responsetime=null;


	function getT86_responsetime() {
		return this.T86_responsetime;
	}
	this.getT86_responsetime=getT86_responsetime;


	function setT86_responsetime(v){
		this.T86_responsetime=v;
	}
	this.setT86_responsetime=setT86_responsetime;

	this.T86_congruency=null;


	function getT86_congruency() {
		return this.T86_congruency;
	}
	this.getT86_congruency=getT86_congruency;


	function setT86_congruency(v){
		this.T86_congruency=v;
	}
	this.setT86_congruency=setT86_congruency;

	this.T87_accuracy=null;


	function getT87_accuracy() {
		return this.T87_accuracy;
	}
	this.getT87_accuracy=getT87_accuracy;


	function setT87_accuracy(v){
		this.T87_accuracy=v;
	}
	this.setT87_accuracy=setT87_accuracy;

	this.T87_responsetime=null;


	function getT87_responsetime() {
		return this.T87_responsetime;
	}
	this.getT87_responsetime=getT87_responsetime;


	function setT87_responsetime(v){
		this.T87_responsetime=v;
	}
	this.setT87_responsetime=setT87_responsetime;

	this.T87_congruency=null;


	function getT87_congruency() {
		return this.T87_congruency;
	}
	this.getT87_congruency=getT87_congruency;


	function setT87_congruency(v){
		this.T87_congruency=v;
	}
	this.setT87_congruency=setT87_congruency;

	this.T88_accuracy=null;


	function getT88_accuracy() {
		return this.T88_accuracy;
	}
	this.getT88_accuracy=getT88_accuracy;


	function setT88_accuracy(v){
		this.T88_accuracy=v;
	}
	this.setT88_accuracy=setT88_accuracy;

	this.T88_responsetime=null;


	function getT88_responsetime() {
		return this.T88_responsetime;
	}
	this.getT88_responsetime=getT88_responsetime;


	function setT88_responsetime(v){
		this.T88_responsetime=v;
	}
	this.setT88_responsetime=setT88_responsetime;

	this.T88_congruency=null;


	function getT88_congruency() {
		return this.T88_congruency;
	}
	this.getT88_congruency=getT88_congruency;


	function setT88_congruency(v){
		this.T88_congruency=v;
	}
	this.setT88_congruency=setT88_congruency;

	this.T89_accuracy=null;


	function getT89_accuracy() {
		return this.T89_accuracy;
	}
	this.getT89_accuracy=getT89_accuracy;


	function setT89_accuracy(v){
		this.T89_accuracy=v;
	}
	this.setT89_accuracy=setT89_accuracy;

	this.T89_responsetime=null;


	function getT89_responsetime() {
		return this.T89_responsetime;
	}
	this.getT89_responsetime=getT89_responsetime;


	function setT89_responsetime(v){
		this.T89_responsetime=v;
	}
	this.setT89_responsetime=setT89_responsetime;

	this.T89_congruency=null;


	function getT89_congruency() {
		return this.T89_congruency;
	}
	this.getT89_congruency=getT89_congruency;


	function setT89_congruency(v){
		this.T89_congruency=v;
	}
	this.setT89_congruency=setT89_congruency;

	this.T90_accuracy=null;


	function getT90_accuracy() {
		return this.T90_accuracy;
	}
	this.getT90_accuracy=getT90_accuracy;


	function setT90_accuracy(v){
		this.T90_accuracy=v;
	}
	this.setT90_accuracy=setT90_accuracy;

	this.T90_responsetime=null;


	function getT90_responsetime() {
		return this.T90_responsetime;
	}
	this.getT90_responsetime=getT90_responsetime;


	function setT90_responsetime(v){
		this.T90_responsetime=v;
	}
	this.setT90_responsetime=setT90_responsetime;

	this.T90_congruency=null;


	function getT90_congruency() {
		return this.T90_congruency;
	}
	this.getT90_congruency=getT90_congruency;


	function setT90_congruency(v){
		this.T90_congruency=v;
	}
	this.setT90_congruency=setT90_congruency;

	this.T91_accuracy=null;


	function getT91_accuracy() {
		return this.T91_accuracy;
	}
	this.getT91_accuracy=getT91_accuracy;


	function setT91_accuracy(v){
		this.T91_accuracy=v;
	}
	this.setT91_accuracy=setT91_accuracy;

	this.T91_responsetime=null;


	function getT91_responsetime() {
		return this.T91_responsetime;
	}
	this.getT91_responsetime=getT91_responsetime;


	function setT91_responsetime(v){
		this.T91_responsetime=v;
	}
	this.setT91_responsetime=setT91_responsetime;

	this.T91_congruency=null;


	function getT91_congruency() {
		return this.T91_congruency;
	}
	this.getT91_congruency=getT91_congruency;


	function setT91_congruency(v){
		this.T91_congruency=v;
	}
	this.setT91_congruency=setT91_congruency;

	this.T92_accuracy=null;


	function getT92_accuracy() {
		return this.T92_accuracy;
	}
	this.getT92_accuracy=getT92_accuracy;


	function setT92_accuracy(v){
		this.T92_accuracy=v;
	}
	this.setT92_accuracy=setT92_accuracy;

	this.T92_responsetime=null;


	function getT92_responsetime() {
		return this.T92_responsetime;
	}
	this.getT92_responsetime=getT92_responsetime;


	function setT92_responsetime(v){
		this.T92_responsetime=v;
	}
	this.setT92_responsetime=setT92_responsetime;

	this.T92_congruency=null;


	function getT92_congruency() {
		return this.T92_congruency;
	}
	this.getT92_congruency=getT92_congruency;


	function setT92_congruency(v){
		this.T92_congruency=v;
	}
	this.setT92_congruency=setT92_congruency;

	this.T93_accuracy=null;


	function getT93_accuracy() {
		return this.T93_accuracy;
	}
	this.getT93_accuracy=getT93_accuracy;


	function setT93_accuracy(v){
		this.T93_accuracy=v;
	}
	this.setT93_accuracy=setT93_accuracy;

	this.T93_responsetime=null;


	function getT93_responsetime() {
		return this.T93_responsetime;
	}
	this.getT93_responsetime=getT93_responsetime;


	function setT93_responsetime(v){
		this.T93_responsetime=v;
	}
	this.setT93_responsetime=setT93_responsetime;

	this.T93_congruency=null;


	function getT93_congruency() {
		return this.T93_congruency;
	}
	this.getT93_congruency=getT93_congruency;


	function setT93_congruency(v){
		this.T93_congruency=v;
	}
	this.setT93_congruency=setT93_congruency;

	this.T94_accuracy=null;


	function getT94_accuracy() {
		return this.T94_accuracy;
	}
	this.getT94_accuracy=getT94_accuracy;


	function setT94_accuracy(v){
		this.T94_accuracy=v;
	}
	this.setT94_accuracy=setT94_accuracy;

	this.T94_responsetime=null;


	function getT94_responsetime() {
		return this.T94_responsetime;
	}
	this.getT94_responsetime=getT94_responsetime;


	function setT94_responsetime(v){
		this.T94_responsetime=v;
	}
	this.setT94_responsetime=setT94_responsetime;

	this.T94_congruency=null;


	function getT94_congruency() {
		return this.T94_congruency;
	}
	this.getT94_congruency=getT94_congruency;


	function setT94_congruency(v){
		this.T94_congruency=v;
	}
	this.setT94_congruency=setT94_congruency;

	this.T95_accuracy=null;


	function getT95_accuracy() {
		return this.T95_accuracy;
	}
	this.getT95_accuracy=getT95_accuracy;


	function setT95_accuracy(v){
		this.T95_accuracy=v;
	}
	this.setT95_accuracy=setT95_accuracy;

	this.T95_responsetime=null;


	function getT95_responsetime() {
		return this.T95_responsetime;
	}
	this.getT95_responsetime=getT95_responsetime;


	function setT95_responsetime(v){
		this.T95_responsetime=v;
	}
	this.setT95_responsetime=setT95_responsetime;

	this.T95_congruency=null;


	function getT95_congruency() {
		return this.T95_congruency;
	}
	this.getT95_congruency=getT95_congruency;


	function setT95_congruency(v){
		this.T95_congruency=v;
	}
	this.setT95_congruency=setT95_congruency;

	this.T96_accuracy=null;


	function getT96_accuracy() {
		return this.T96_accuracy;
	}
	this.getT96_accuracy=getT96_accuracy;


	function setT96_accuracy(v){
		this.T96_accuracy=v;
	}
	this.setT96_accuracy=setT96_accuracy;

	this.T96_responsetime=null;


	function getT96_responsetime() {
		return this.T96_responsetime;
	}
	this.getT96_responsetime=getT96_responsetime;


	function setT96_responsetime(v){
		this.T96_responsetime=v;
	}
	this.setT96_responsetime=setT96_responsetime;

	this.T96_congruency=null;


	function getT96_congruency() {
		return this.T96_congruency;
	}
	this.getT96_congruency=getT96_congruency;


	function setT96_congruency(v){
		this.T96_congruency=v;
	}
	this.setT96_congruency=setT96_congruency;

	this.T97_accuracy=null;


	function getT97_accuracy() {
		return this.T97_accuracy;
	}
	this.getT97_accuracy=getT97_accuracy;


	function setT97_accuracy(v){
		this.T97_accuracy=v;
	}
	this.setT97_accuracy=setT97_accuracy;

	this.T97_responsetime=null;


	function getT97_responsetime() {
		return this.T97_responsetime;
	}
	this.getT97_responsetime=getT97_responsetime;


	function setT97_responsetime(v){
		this.T97_responsetime=v;
	}
	this.setT97_responsetime=setT97_responsetime;

	this.T97_congruency=null;


	function getT97_congruency() {
		return this.T97_congruency;
	}
	this.getT97_congruency=getT97_congruency;


	function setT97_congruency(v){
		this.T97_congruency=v;
	}
	this.setT97_congruency=setT97_congruency;

	this.T98_accuracy=null;


	function getT98_accuracy() {
		return this.T98_accuracy;
	}
	this.getT98_accuracy=getT98_accuracy;


	function setT98_accuracy(v){
		this.T98_accuracy=v;
	}
	this.setT98_accuracy=setT98_accuracy;

	this.T98_responsetime=null;


	function getT98_responsetime() {
		return this.T98_responsetime;
	}
	this.getT98_responsetime=getT98_responsetime;


	function setT98_responsetime(v){
		this.T98_responsetime=v;
	}
	this.setT98_responsetime=setT98_responsetime;

	this.T98_congruency=null;


	function getT98_congruency() {
		return this.T98_congruency;
	}
	this.getT98_congruency=getT98_congruency;


	function setT98_congruency(v){
		this.T98_congruency=v;
	}
	this.setT98_congruency=setT98_congruency;

	this.T99_accuracy=null;


	function getT99_accuracy() {
		return this.T99_accuracy;
	}
	this.getT99_accuracy=getT99_accuracy;


	function setT99_accuracy(v){
		this.T99_accuracy=v;
	}
	this.setT99_accuracy=setT99_accuracy;

	this.T99_responsetime=null;


	function getT99_responsetime() {
		return this.T99_responsetime;
	}
	this.getT99_responsetime=getT99_responsetime;


	function setT99_responsetime(v){
		this.T99_responsetime=v;
	}
	this.setT99_responsetime=setT99_responsetime;

	this.T99_congruency=null;


	function getT99_congruency() {
		return this.T99_congruency;
	}
	this.getT99_congruency=getT99_congruency;


	function setT99_congruency(v){
		this.T99_congruency=v;
	}
	this.setT99_congruency=setT99_congruency;

	this.T100_accuracy=null;


	function getT100_accuracy() {
		return this.T100_accuracy;
	}
	this.getT100_accuracy=getT100_accuracy;


	function setT100_accuracy(v){
		this.T100_accuracy=v;
	}
	this.setT100_accuracy=setT100_accuracy;

	this.T100_responsetime=null;


	function getT100_responsetime() {
		return this.T100_responsetime;
	}
	this.getT100_responsetime=getT100_responsetime;


	function setT100_responsetime(v){
		this.T100_responsetime=v;
	}
	this.setT100_responsetime=setT100_responsetime;

	this.T100_congruency=null;


	function getT100_congruency() {
		return this.T100_congruency;
	}
	this.getT100_congruency=getT100_congruency;


	function setT100_congruency(v){
		this.T100_congruency=v;
	}
	this.setT100_congruency=setT100_congruency;

	this.T101_accuracy=null;


	function getT101_accuracy() {
		return this.T101_accuracy;
	}
	this.getT101_accuracy=getT101_accuracy;


	function setT101_accuracy(v){
		this.T101_accuracy=v;
	}
	this.setT101_accuracy=setT101_accuracy;

	this.T101_responsetime=null;


	function getT101_responsetime() {
		return this.T101_responsetime;
	}
	this.getT101_responsetime=getT101_responsetime;


	function setT101_responsetime(v){
		this.T101_responsetime=v;
	}
	this.setT101_responsetime=setT101_responsetime;

	this.T101_congruency=null;


	function getT101_congruency() {
		return this.T101_congruency;
	}
	this.getT101_congruency=getT101_congruency;


	function setT101_congruency(v){
		this.T101_congruency=v;
	}
	this.setT101_congruency=setT101_congruency;

	this.T102_accuracy=null;


	function getT102_accuracy() {
		return this.T102_accuracy;
	}
	this.getT102_accuracy=getT102_accuracy;


	function setT102_accuracy(v){
		this.T102_accuracy=v;
	}
	this.setT102_accuracy=setT102_accuracy;

	this.T102_responsetime=null;


	function getT102_responsetime() {
		return this.T102_responsetime;
	}
	this.getT102_responsetime=getT102_responsetime;


	function setT102_responsetime(v){
		this.T102_responsetime=v;
	}
	this.setT102_responsetime=setT102_responsetime;

	this.T102_congruency=null;


	function getT102_congruency() {
		return this.T102_congruency;
	}
	this.getT102_congruency=getT102_congruency;


	function setT102_congruency(v){
		this.T102_congruency=v;
	}
	this.setT102_congruency=setT102_congruency;

	this.T103_accuracy=null;


	function getT103_accuracy() {
		return this.T103_accuracy;
	}
	this.getT103_accuracy=getT103_accuracy;


	function setT103_accuracy(v){
		this.T103_accuracy=v;
	}
	this.setT103_accuracy=setT103_accuracy;

	this.T103_responsetime=null;


	function getT103_responsetime() {
		return this.T103_responsetime;
	}
	this.getT103_responsetime=getT103_responsetime;


	function setT103_responsetime(v){
		this.T103_responsetime=v;
	}
	this.setT103_responsetime=setT103_responsetime;

	this.T103_congruency=null;


	function getT103_congruency() {
		return this.T103_congruency;
	}
	this.getT103_congruency=getT103_congruency;


	function setT103_congruency(v){
		this.T103_congruency=v;
	}
	this.setT103_congruency=setT103_congruency;

	this.T104_accuracy=null;


	function getT104_accuracy() {
		return this.T104_accuracy;
	}
	this.getT104_accuracy=getT104_accuracy;


	function setT104_accuracy(v){
		this.T104_accuracy=v;
	}
	this.setT104_accuracy=setT104_accuracy;

	this.T104_responsetime=null;


	function getT104_responsetime() {
		return this.T104_responsetime;
	}
	this.getT104_responsetime=getT104_responsetime;


	function setT104_responsetime(v){
		this.T104_responsetime=v;
	}
	this.setT104_responsetime=setT104_responsetime;

	this.T104_congruency=null;


	function getT104_congruency() {
		return this.T104_congruency;
	}
	this.getT104_congruency=getT104_congruency;


	function setT104_congruency(v){
		this.T104_congruency=v;
	}
	this.setT104_congruency=setT104_congruency;

	this.T105_accuracy=null;


	function getT105_accuracy() {
		return this.T105_accuracy;
	}
	this.getT105_accuracy=getT105_accuracy;


	function setT105_accuracy(v){
		this.T105_accuracy=v;
	}
	this.setT105_accuracy=setT105_accuracy;

	this.T105_responsetime=null;


	function getT105_responsetime() {
		return this.T105_responsetime;
	}
	this.getT105_responsetime=getT105_responsetime;


	function setT105_responsetime(v){
		this.T105_responsetime=v;
	}
	this.setT105_responsetime=setT105_responsetime;

	this.T105_congruency=null;


	function getT105_congruency() {
		return this.T105_congruency;
	}
	this.getT105_congruency=getT105_congruency;


	function setT105_congruency(v){
		this.T105_congruency=v;
	}
	this.setT105_congruency=setT105_congruency;

	this.T106_accuracy=null;


	function getT106_accuracy() {
		return this.T106_accuracy;
	}
	this.getT106_accuracy=getT106_accuracy;


	function setT106_accuracy(v){
		this.T106_accuracy=v;
	}
	this.setT106_accuracy=setT106_accuracy;

	this.T106_responsetime=null;


	function getT106_responsetime() {
		return this.T106_responsetime;
	}
	this.getT106_responsetime=getT106_responsetime;


	function setT106_responsetime(v){
		this.T106_responsetime=v;
	}
	this.setT106_responsetime=setT106_responsetime;

	this.T106_congruency=null;


	function getT106_congruency() {
		return this.T106_congruency;
	}
	this.getT106_congruency=getT106_congruency;


	function setT106_congruency(v){
		this.T106_congruency=v;
	}
	this.setT106_congruency=setT106_congruency;

	this.T107_accuracy=null;


	function getT107_accuracy() {
		return this.T107_accuracy;
	}
	this.getT107_accuracy=getT107_accuracy;


	function setT107_accuracy(v){
		this.T107_accuracy=v;
	}
	this.setT107_accuracy=setT107_accuracy;

	this.T107_responsetime=null;


	function getT107_responsetime() {
		return this.T107_responsetime;
	}
	this.getT107_responsetime=getT107_responsetime;


	function setT107_responsetime(v){
		this.T107_responsetime=v;
	}
	this.setT107_responsetime=setT107_responsetime;

	this.T107_congruency=null;


	function getT107_congruency() {
		return this.T107_congruency;
	}
	this.getT107_congruency=getT107_congruency;


	function setT107_congruency(v){
		this.T107_congruency=v;
	}
	this.setT107_congruency=setT107_congruency;

	this.T108_accuracy=null;


	function getT108_accuracy() {
		return this.T108_accuracy;
	}
	this.getT108_accuracy=getT108_accuracy;


	function setT108_accuracy(v){
		this.T108_accuracy=v;
	}
	this.setT108_accuracy=setT108_accuracy;

	this.T108_responsetime=null;


	function getT108_responsetime() {
		return this.T108_responsetime;
	}
	this.getT108_responsetime=getT108_responsetime;


	function setT108_responsetime(v){
		this.T108_responsetime=v;
	}
	this.setT108_responsetime=setT108_responsetime;

	this.T108_congruency=null;


	function getT108_congruency() {
		return this.T108_congruency;
	}
	this.getT108_congruency=getT108_congruency;


	function setT108_congruency(v){
		this.T108_congruency=v;
	}
	this.setT108_congruency=setT108_congruency;

	this.T109_accuracy=null;


	function getT109_accuracy() {
		return this.T109_accuracy;
	}
	this.getT109_accuracy=getT109_accuracy;


	function setT109_accuracy(v){
		this.T109_accuracy=v;
	}
	this.setT109_accuracy=setT109_accuracy;

	this.T109_responsetime=null;


	function getT109_responsetime() {
		return this.T109_responsetime;
	}
	this.getT109_responsetime=getT109_responsetime;


	function setT109_responsetime(v){
		this.T109_responsetime=v;
	}
	this.setT109_responsetime=setT109_responsetime;

	this.T109_congruency=null;


	function getT109_congruency() {
		return this.T109_congruency;
	}
	this.getT109_congruency=getT109_congruency;


	function setT109_congruency(v){
		this.T109_congruency=v;
	}
	this.setT109_congruency=setT109_congruency;

	this.T110_accuracy=null;


	function getT110_accuracy() {
		return this.T110_accuracy;
	}
	this.getT110_accuracy=getT110_accuracy;


	function setT110_accuracy(v){
		this.T110_accuracy=v;
	}
	this.setT110_accuracy=setT110_accuracy;

	this.T110_responsetime=null;


	function getT110_responsetime() {
		return this.T110_responsetime;
	}
	this.getT110_responsetime=getT110_responsetime;


	function setT110_responsetime(v){
		this.T110_responsetime=v;
	}
	this.setT110_responsetime=setT110_responsetime;

	this.T110_congruency=null;


	function getT110_congruency() {
		return this.T110_congruency;
	}
	this.getT110_congruency=getT110_congruency;


	function setT110_congruency(v){
		this.T110_congruency=v;
	}
	this.setT110_congruency=setT110_congruency;

	this.T111_accuracy=null;


	function getT111_accuracy() {
		return this.T111_accuracy;
	}
	this.getT111_accuracy=getT111_accuracy;


	function setT111_accuracy(v){
		this.T111_accuracy=v;
	}
	this.setT111_accuracy=setT111_accuracy;

	this.T111_responsetime=null;


	function getT111_responsetime() {
		return this.T111_responsetime;
	}
	this.getT111_responsetime=getT111_responsetime;


	function setT111_responsetime(v){
		this.T111_responsetime=v;
	}
	this.setT111_responsetime=setT111_responsetime;

	this.T111_congruency=null;


	function getT111_congruency() {
		return this.T111_congruency;
	}
	this.getT111_congruency=getT111_congruency;


	function setT111_congruency(v){
		this.T111_congruency=v;
	}
	this.setT111_congruency=setT111_congruency;

	this.T112_accuracy=null;


	function getT112_accuracy() {
		return this.T112_accuracy;
	}
	this.getT112_accuracy=getT112_accuracy;


	function setT112_accuracy(v){
		this.T112_accuracy=v;
	}
	this.setT112_accuracy=setT112_accuracy;

	this.T112_responsetime=null;


	function getT112_responsetime() {
		return this.T112_responsetime;
	}
	this.getT112_responsetime=getT112_responsetime;


	function setT112_responsetime(v){
		this.T112_responsetime=v;
	}
	this.setT112_responsetime=setT112_responsetime;

	this.T112_congruency=null;


	function getT112_congruency() {
		return this.T112_congruency;
	}
	this.getT112_congruency=getT112_congruency;


	function setT112_congruency(v){
		this.T112_congruency=v;
	}
	this.setT112_congruency=setT112_congruency;

	this.T113_accuracy=null;


	function getT113_accuracy() {
		return this.T113_accuracy;
	}
	this.getT113_accuracy=getT113_accuracy;


	function setT113_accuracy(v){
		this.T113_accuracy=v;
	}
	this.setT113_accuracy=setT113_accuracy;

	this.T113_responsetime=null;


	function getT113_responsetime() {
		return this.T113_responsetime;
	}
	this.getT113_responsetime=getT113_responsetime;


	function setT113_responsetime(v){
		this.T113_responsetime=v;
	}
	this.setT113_responsetime=setT113_responsetime;

	this.T113_congruency=null;


	function getT113_congruency() {
		return this.T113_congruency;
	}
	this.getT113_congruency=getT113_congruency;


	function setT113_congruency(v){
		this.T113_congruency=v;
	}
	this.setT113_congruency=setT113_congruency;

	this.T114_accuracy=null;


	function getT114_accuracy() {
		return this.T114_accuracy;
	}
	this.getT114_accuracy=getT114_accuracy;


	function setT114_accuracy(v){
		this.T114_accuracy=v;
	}
	this.setT114_accuracy=setT114_accuracy;

	this.T114_responsetime=null;


	function getT114_responsetime() {
		return this.T114_responsetime;
	}
	this.getT114_responsetime=getT114_responsetime;


	function setT114_responsetime(v){
		this.T114_responsetime=v;
	}
	this.setT114_responsetime=setT114_responsetime;

	this.T114_congruency=null;


	function getT114_congruency() {
		return this.T114_congruency;
	}
	this.getT114_congruency=getT114_congruency;


	function setT114_congruency(v){
		this.T114_congruency=v;
	}
	this.setT114_congruency=setT114_congruency;

	this.T115_accuracy=null;


	function getT115_accuracy() {
		return this.T115_accuracy;
	}
	this.getT115_accuracy=getT115_accuracy;


	function setT115_accuracy(v){
		this.T115_accuracy=v;
	}
	this.setT115_accuracy=setT115_accuracy;

	this.T115_responsetime=null;


	function getT115_responsetime() {
		return this.T115_responsetime;
	}
	this.getT115_responsetime=getT115_responsetime;


	function setT115_responsetime(v){
		this.T115_responsetime=v;
	}
	this.setT115_responsetime=setT115_responsetime;

	this.T115_congruency=null;


	function getT115_congruency() {
		return this.T115_congruency;
	}
	this.getT115_congruency=getT115_congruency;


	function setT115_congruency(v){
		this.T115_congruency=v;
	}
	this.setT115_congruency=setT115_congruency;

	this.T116_accuracy=null;


	function getT116_accuracy() {
		return this.T116_accuracy;
	}
	this.getT116_accuracy=getT116_accuracy;


	function setT116_accuracy(v){
		this.T116_accuracy=v;
	}
	this.setT116_accuracy=setT116_accuracy;

	this.T116_responsetime=null;


	function getT116_responsetime() {
		return this.T116_responsetime;
	}
	this.getT116_responsetime=getT116_responsetime;


	function setT116_responsetime(v){
		this.T116_responsetime=v;
	}
	this.setT116_responsetime=setT116_responsetime;

	this.T116_congruency=null;


	function getT116_congruency() {
		return this.T116_congruency;
	}
	this.getT116_congruency=getT116_congruency;


	function setT116_congruency(v){
		this.T116_congruency=v;
	}
	this.setT116_congruency=setT116_congruency;

	this.T117_accuracy=null;


	function getT117_accuracy() {
		return this.T117_accuracy;
	}
	this.getT117_accuracy=getT117_accuracy;


	function setT117_accuracy(v){
		this.T117_accuracy=v;
	}
	this.setT117_accuracy=setT117_accuracy;

	this.T117_responsetime=null;


	function getT117_responsetime() {
		return this.T117_responsetime;
	}
	this.getT117_responsetime=getT117_responsetime;


	function setT117_responsetime(v){
		this.T117_responsetime=v;
	}
	this.setT117_responsetime=setT117_responsetime;

	this.T117_congruency=null;


	function getT117_congruency() {
		return this.T117_congruency;
	}
	this.getT117_congruency=getT117_congruency;


	function setT117_congruency(v){
		this.T117_congruency=v;
	}
	this.setT117_congruency=setT117_congruency;

	this.T118_accuracy=null;


	function getT118_accuracy() {
		return this.T118_accuracy;
	}
	this.getT118_accuracy=getT118_accuracy;


	function setT118_accuracy(v){
		this.T118_accuracy=v;
	}
	this.setT118_accuracy=setT118_accuracy;

	this.T118_responsetime=null;


	function getT118_responsetime() {
		return this.T118_responsetime;
	}
	this.getT118_responsetime=getT118_responsetime;


	function setT118_responsetime(v){
		this.T118_responsetime=v;
	}
	this.setT118_responsetime=setT118_responsetime;

	this.T118_congruency=null;


	function getT118_congruency() {
		return this.T118_congruency;
	}
	this.getT118_congruency=getT118_congruency;


	function setT118_congruency(v){
		this.T118_congruency=v;
	}
	this.setT118_congruency=setT118_congruency;

	this.T119_accuracy=null;


	function getT119_accuracy() {
		return this.T119_accuracy;
	}
	this.getT119_accuracy=getT119_accuracy;


	function setT119_accuracy(v){
		this.T119_accuracy=v;
	}
	this.setT119_accuracy=setT119_accuracy;

	this.T119_responsetime=null;


	function getT119_responsetime() {
		return this.T119_responsetime;
	}
	this.getT119_responsetime=getT119_responsetime;


	function setT119_responsetime(v){
		this.T119_responsetime=v;
	}
	this.setT119_responsetime=setT119_responsetime;

	this.T119_congruency=null;


	function getT119_congruency() {
		return this.T119_congruency;
	}
	this.getT119_congruency=getT119_congruency;


	function setT119_congruency(v){
		this.T119_congruency=v;
	}
	this.setT119_congruency=setT119_congruency;

	this.T120_accuracy=null;


	function getT120_accuracy() {
		return this.T120_accuracy;
	}
	this.getT120_accuracy=getT120_accuracy;


	function setT120_accuracy(v){
		this.T120_accuracy=v;
	}
	this.setT120_accuracy=setT120_accuracy;

	this.T120_responsetime=null;


	function getT120_responsetime() {
		return this.T120_responsetime;
	}
	this.getT120_responsetime=getT120_responsetime;


	function setT120_responsetime(v){
		this.T120_responsetime=v;
	}
	this.setT120_responsetime=setT120_responsetime;

	this.T120_congruency=null;


	function getT120_congruency() {
		return this.T120_congruency;
	}
	this.getT120_congruency=getT120_congruency;


	function setT120_congruency(v){
		this.T120_congruency=v;
	}
	this.setT120_congruency=setT120_congruency;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="T1/accuracy"){
				return this.T1_accuracy ;
			} else 
			if(xmlPath=="T1/responseTime"){
				return this.T1_responsetime ;
			} else 
			if(xmlPath=="T1/congruency"){
				return this.T1_congruency ;
			} else 
			if(xmlPath=="T2/accuracy"){
				return this.T2_accuracy ;
			} else 
			if(xmlPath=="T2/responseTime"){
				return this.T2_responsetime ;
			} else 
			if(xmlPath=="T2/congruency"){
				return this.T2_congruency ;
			} else 
			if(xmlPath=="T3/accuracy"){
				return this.T3_accuracy ;
			} else 
			if(xmlPath=="T3/responseTime"){
				return this.T3_responsetime ;
			} else 
			if(xmlPath=="T3/congruency"){
				return this.T3_congruency ;
			} else 
			if(xmlPath=="T4/accuracy"){
				return this.T4_accuracy ;
			} else 
			if(xmlPath=="T4/responseTime"){
				return this.T4_responsetime ;
			} else 
			if(xmlPath=="T4/congruency"){
				return this.T4_congruency ;
			} else 
			if(xmlPath=="T5/accuracy"){
				return this.T5_accuracy ;
			} else 
			if(xmlPath=="T5/responseTime"){
				return this.T5_responsetime ;
			} else 
			if(xmlPath=="T5/congruency"){
				return this.T5_congruency ;
			} else 
			if(xmlPath=="T6/accuracy"){
				return this.T6_accuracy ;
			} else 
			if(xmlPath=="T6/responseTime"){
				return this.T6_responsetime ;
			} else 
			if(xmlPath=="T6/congruency"){
				return this.T6_congruency ;
			} else 
			if(xmlPath=="T7/accuracy"){
				return this.T7_accuracy ;
			} else 
			if(xmlPath=="T7/responseTime"){
				return this.T7_responsetime ;
			} else 
			if(xmlPath=="T7/congruency"){
				return this.T7_congruency ;
			} else 
			if(xmlPath=="T8/accuracy"){
				return this.T8_accuracy ;
			} else 
			if(xmlPath=="T8/responseTime"){
				return this.T8_responsetime ;
			} else 
			if(xmlPath=="T8/congruency"){
				return this.T8_congruency ;
			} else 
			if(xmlPath=="T9/accuracy"){
				return this.T9_accuracy ;
			} else 
			if(xmlPath=="T9/responseTime"){
				return this.T9_responsetime ;
			} else 
			if(xmlPath=="T9/congruency"){
				return this.T9_congruency ;
			} else 
			if(xmlPath=="T10/accuracy"){
				return this.T10_accuracy ;
			} else 
			if(xmlPath=="T10/responseTime"){
				return this.T10_responsetime ;
			} else 
			if(xmlPath=="T10/congruency"){
				return this.T10_congruency ;
			} else 
			if(xmlPath=="T11/accuracy"){
				return this.T11_accuracy ;
			} else 
			if(xmlPath=="T11/responseTime"){
				return this.T11_responsetime ;
			} else 
			if(xmlPath=="T11/congruency"){
				return this.T11_congruency ;
			} else 
			if(xmlPath=="T12/accuracy"){
				return this.T12_accuracy ;
			} else 
			if(xmlPath=="T12/responseTime"){
				return this.T12_responsetime ;
			} else 
			if(xmlPath=="T12/congruency"){
				return this.T12_congruency ;
			} else 
			if(xmlPath=="T13/accuracy"){
				return this.T13_accuracy ;
			} else 
			if(xmlPath=="T13/responseTime"){
				return this.T13_responsetime ;
			} else 
			if(xmlPath=="T13/congruency"){
				return this.T13_congruency ;
			} else 
			if(xmlPath=="T14/accuracy"){
				return this.T14_accuracy ;
			} else 
			if(xmlPath=="T14/responseTime"){
				return this.T14_responsetime ;
			} else 
			if(xmlPath=="T14/congruency"){
				return this.T14_congruency ;
			} else 
			if(xmlPath=="T15/accuracy"){
				return this.T15_accuracy ;
			} else 
			if(xmlPath=="T15/responseTime"){
				return this.T15_responsetime ;
			} else 
			if(xmlPath=="T15/congruency"){
				return this.T15_congruency ;
			} else 
			if(xmlPath=="T16/accuracy"){
				return this.T16_accuracy ;
			} else 
			if(xmlPath=="T16/responseTime"){
				return this.T16_responsetime ;
			} else 
			if(xmlPath=="T16/congruency"){
				return this.T16_congruency ;
			} else 
			if(xmlPath=="T17/accuracy"){
				return this.T17_accuracy ;
			} else 
			if(xmlPath=="T17/responseTime"){
				return this.T17_responsetime ;
			} else 
			if(xmlPath=="T17/congruency"){
				return this.T17_congruency ;
			} else 
			if(xmlPath=="T18/accuracy"){
				return this.T18_accuracy ;
			} else 
			if(xmlPath=="T18/responseTime"){
				return this.T18_responsetime ;
			} else 
			if(xmlPath=="T18/congruency"){
				return this.T18_congruency ;
			} else 
			if(xmlPath=="T19/accuracy"){
				return this.T19_accuracy ;
			} else 
			if(xmlPath=="T19/responseTime"){
				return this.T19_responsetime ;
			} else 
			if(xmlPath=="T19/congruency"){
				return this.T19_congruency ;
			} else 
			if(xmlPath=="T20/accuracy"){
				return this.T20_accuracy ;
			} else 
			if(xmlPath=="T20/responseTime"){
				return this.T20_responsetime ;
			} else 
			if(xmlPath=="T20/congruency"){
				return this.T20_congruency ;
			} else 
			if(xmlPath=="T21/accuracy"){
				return this.T21_accuracy ;
			} else 
			if(xmlPath=="T21/responseTime"){
				return this.T21_responsetime ;
			} else 
			if(xmlPath=="T21/congruency"){
				return this.T21_congruency ;
			} else 
			if(xmlPath=="T22/accuracy"){
				return this.T22_accuracy ;
			} else 
			if(xmlPath=="T22/responseTime"){
				return this.T22_responsetime ;
			} else 
			if(xmlPath=="T22/congruency"){
				return this.T22_congruency ;
			} else 
			if(xmlPath=="T23/accuracy"){
				return this.T23_accuracy ;
			} else 
			if(xmlPath=="T23/responseTime"){
				return this.T23_responsetime ;
			} else 
			if(xmlPath=="T23/congruency"){
				return this.T23_congruency ;
			} else 
			if(xmlPath=="T24/accuracy"){
				return this.T24_accuracy ;
			} else 
			if(xmlPath=="T24/responseTime"){
				return this.T24_responsetime ;
			} else 
			if(xmlPath=="T24/congruency"){
				return this.T24_congruency ;
			} else 
			if(xmlPath=="T25/accuracy"){
				return this.T25_accuracy ;
			} else 
			if(xmlPath=="T25/responseTime"){
				return this.T25_responsetime ;
			} else 
			if(xmlPath=="T25/congruency"){
				return this.T25_congruency ;
			} else 
			if(xmlPath=="T26/accuracy"){
				return this.T26_accuracy ;
			} else 
			if(xmlPath=="T26/responseTime"){
				return this.T26_responsetime ;
			} else 
			if(xmlPath=="T26/congruency"){
				return this.T26_congruency ;
			} else 
			if(xmlPath=="T27/accuracy"){
				return this.T27_accuracy ;
			} else 
			if(xmlPath=="T27/responseTime"){
				return this.T27_responsetime ;
			} else 
			if(xmlPath=="T27/congruency"){
				return this.T27_congruency ;
			} else 
			if(xmlPath=="T28/accuracy"){
				return this.T28_accuracy ;
			} else 
			if(xmlPath=="T28/responseTime"){
				return this.T28_responsetime ;
			} else 
			if(xmlPath=="T28/congruency"){
				return this.T28_congruency ;
			} else 
			if(xmlPath=="T29/accuracy"){
				return this.T29_accuracy ;
			} else 
			if(xmlPath=="T29/responseTime"){
				return this.T29_responsetime ;
			} else 
			if(xmlPath=="T29/congruency"){
				return this.T29_congruency ;
			} else 
			if(xmlPath=="T30/accuracy"){
				return this.T30_accuracy ;
			} else 
			if(xmlPath=="T30/responseTime"){
				return this.T30_responsetime ;
			} else 
			if(xmlPath=="T30/congruency"){
				return this.T30_congruency ;
			} else 
			if(xmlPath=="T31/accuracy"){
				return this.T31_accuracy ;
			} else 
			if(xmlPath=="T31/responseTime"){
				return this.T31_responsetime ;
			} else 
			if(xmlPath=="T31/congruency"){
				return this.T31_congruency ;
			} else 
			if(xmlPath=="T32/accuracy"){
				return this.T32_accuracy ;
			} else 
			if(xmlPath=="T32/responseTime"){
				return this.T32_responsetime ;
			} else 
			if(xmlPath=="T32/congruency"){
				return this.T32_congruency ;
			} else 
			if(xmlPath=="T33/accuracy"){
				return this.T33_accuracy ;
			} else 
			if(xmlPath=="T33/responseTime"){
				return this.T33_responsetime ;
			} else 
			if(xmlPath=="T33/congruency"){
				return this.T33_congruency ;
			} else 
			if(xmlPath=="T34/accuracy"){
				return this.T34_accuracy ;
			} else 
			if(xmlPath=="T34/responseTime"){
				return this.T34_responsetime ;
			} else 
			if(xmlPath=="T34/congruency"){
				return this.T34_congruency ;
			} else 
			if(xmlPath=="T35/accuracy"){
				return this.T35_accuracy ;
			} else 
			if(xmlPath=="T35/responseTime"){
				return this.T35_responsetime ;
			} else 
			if(xmlPath=="T35/congruency"){
				return this.T35_congruency ;
			} else 
			if(xmlPath=="T36/accuracy"){
				return this.T36_accuracy ;
			} else 
			if(xmlPath=="T36/responseTime"){
				return this.T36_responsetime ;
			} else 
			if(xmlPath=="T36/congruency"){
				return this.T36_congruency ;
			} else 
			if(xmlPath=="T37/accuracy"){
				return this.T37_accuracy ;
			} else 
			if(xmlPath=="T37/responseTime"){
				return this.T37_responsetime ;
			} else 
			if(xmlPath=="T37/congruency"){
				return this.T37_congruency ;
			} else 
			if(xmlPath=="T38/accuracy"){
				return this.T38_accuracy ;
			} else 
			if(xmlPath=="T38/responseTime"){
				return this.T38_responsetime ;
			} else 
			if(xmlPath=="T38/congruency"){
				return this.T38_congruency ;
			} else 
			if(xmlPath=="T39/accuracy"){
				return this.T39_accuracy ;
			} else 
			if(xmlPath=="T39/responseTime"){
				return this.T39_responsetime ;
			} else 
			if(xmlPath=="T39/congruency"){
				return this.T39_congruency ;
			} else 
			if(xmlPath=="T40/accuracy"){
				return this.T40_accuracy ;
			} else 
			if(xmlPath=="T40/responseTime"){
				return this.T40_responsetime ;
			} else 
			if(xmlPath=="T40/congruency"){
				return this.T40_congruency ;
			} else 
			if(xmlPath=="T41/accuracy"){
				return this.T41_accuracy ;
			} else 
			if(xmlPath=="T41/responseTime"){
				return this.T41_responsetime ;
			} else 
			if(xmlPath=="T41/congruency"){
				return this.T41_congruency ;
			} else 
			if(xmlPath=="T42/accuracy"){
				return this.T42_accuracy ;
			} else 
			if(xmlPath=="T42/responseTime"){
				return this.T42_responsetime ;
			} else 
			if(xmlPath=="T42/congruency"){
				return this.T42_congruency ;
			} else 
			if(xmlPath=="T43/accuracy"){
				return this.T43_accuracy ;
			} else 
			if(xmlPath=="T43/responseTime"){
				return this.T43_responsetime ;
			} else 
			if(xmlPath=="T43/congruency"){
				return this.T43_congruency ;
			} else 
			if(xmlPath=="T44/accuracy"){
				return this.T44_accuracy ;
			} else 
			if(xmlPath=="T44/responseTime"){
				return this.T44_responsetime ;
			} else 
			if(xmlPath=="T44/congruency"){
				return this.T44_congruency ;
			} else 
			if(xmlPath=="T45/accuracy"){
				return this.T45_accuracy ;
			} else 
			if(xmlPath=="T45/responseTime"){
				return this.T45_responsetime ;
			} else 
			if(xmlPath=="T45/congruency"){
				return this.T45_congruency ;
			} else 
			if(xmlPath=="T46/accuracy"){
				return this.T46_accuracy ;
			} else 
			if(xmlPath=="T46/responseTime"){
				return this.T46_responsetime ;
			} else 
			if(xmlPath=="T46/congruency"){
				return this.T46_congruency ;
			} else 
			if(xmlPath=="T47/accuracy"){
				return this.T47_accuracy ;
			} else 
			if(xmlPath=="T47/responseTime"){
				return this.T47_responsetime ;
			} else 
			if(xmlPath=="T47/congruency"){
				return this.T47_congruency ;
			} else 
			if(xmlPath=="T48/accuracy"){
				return this.T48_accuracy ;
			} else 
			if(xmlPath=="T48/responseTime"){
				return this.T48_responsetime ;
			} else 
			if(xmlPath=="T48/congruency"){
				return this.T48_congruency ;
			} else 
			if(xmlPath=="T49/accuracy"){
				return this.T49_accuracy ;
			} else 
			if(xmlPath=="T49/responseTime"){
				return this.T49_responsetime ;
			} else 
			if(xmlPath=="T49/congruency"){
				return this.T49_congruency ;
			} else 
			if(xmlPath=="T50/accuracy"){
				return this.T50_accuracy ;
			} else 
			if(xmlPath=="T50/responseTime"){
				return this.T50_responsetime ;
			} else 
			if(xmlPath=="T50/congruency"){
				return this.T50_congruency ;
			} else 
			if(xmlPath=="T51/accuracy"){
				return this.T51_accuracy ;
			} else 
			if(xmlPath=="T51/responseTime"){
				return this.T51_responsetime ;
			} else 
			if(xmlPath=="T51/congruency"){
				return this.T51_congruency ;
			} else 
			if(xmlPath=="T52/accuracy"){
				return this.T52_accuracy ;
			} else 
			if(xmlPath=="T52/responseTime"){
				return this.T52_responsetime ;
			} else 
			if(xmlPath=="T52/congruency"){
				return this.T52_congruency ;
			} else 
			if(xmlPath=="T53/accuracy"){
				return this.T53_accuracy ;
			} else 
			if(xmlPath=="T53/responseTime"){
				return this.T53_responsetime ;
			} else 
			if(xmlPath=="T53/congruency"){
				return this.T53_congruency ;
			} else 
			if(xmlPath=="T54/accuracy"){
				return this.T54_accuracy ;
			} else 
			if(xmlPath=="T54/responseTime"){
				return this.T54_responsetime ;
			} else 
			if(xmlPath=="T54/congruency"){
				return this.T54_congruency ;
			} else 
			if(xmlPath=="T55/accuracy"){
				return this.T55_accuracy ;
			} else 
			if(xmlPath=="T55/responseTime"){
				return this.T55_responsetime ;
			} else 
			if(xmlPath=="T55/congruency"){
				return this.T55_congruency ;
			} else 
			if(xmlPath=="T56/accuracy"){
				return this.T56_accuracy ;
			} else 
			if(xmlPath=="T56/responseTime"){
				return this.T56_responsetime ;
			} else 
			if(xmlPath=="T56/congruency"){
				return this.T56_congruency ;
			} else 
			if(xmlPath=="T57/accuracy"){
				return this.T57_accuracy ;
			} else 
			if(xmlPath=="T57/responseTime"){
				return this.T57_responsetime ;
			} else 
			if(xmlPath=="T57/congruency"){
				return this.T57_congruency ;
			} else 
			if(xmlPath=="T58/accuracy"){
				return this.T58_accuracy ;
			} else 
			if(xmlPath=="T58/responseTime"){
				return this.T58_responsetime ;
			} else 
			if(xmlPath=="T58/congruency"){
				return this.T58_congruency ;
			} else 
			if(xmlPath=="T59/accuracy"){
				return this.T59_accuracy ;
			} else 
			if(xmlPath=="T59/responseTime"){
				return this.T59_responsetime ;
			} else 
			if(xmlPath=="T59/congruency"){
				return this.T59_congruency ;
			} else 
			if(xmlPath=="T60/accuracy"){
				return this.T60_accuracy ;
			} else 
			if(xmlPath=="T60/responseTime"){
				return this.T60_responsetime ;
			} else 
			if(xmlPath=="T60/congruency"){
				return this.T60_congruency ;
			} else 
			if(xmlPath=="T61/accuracy"){
				return this.T61_accuracy ;
			} else 
			if(xmlPath=="T61/responseTime"){
				return this.T61_responsetime ;
			} else 
			if(xmlPath=="T61/congruency"){
				return this.T61_congruency ;
			} else 
			if(xmlPath=="T62/accuracy"){
				return this.T62_accuracy ;
			} else 
			if(xmlPath=="T62/responseTime"){
				return this.T62_responsetime ;
			} else 
			if(xmlPath=="T62/congruency"){
				return this.T62_congruency ;
			} else 
			if(xmlPath=="T63/accuracy"){
				return this.T63_accuracy ;
			} else 
			if(xmlPath=="T63/responseTime"){
				return this.T63_responsetime ;
			} else 
			if(xmlPath=="T63/congruency"){
				return this.T63_congruency ;
			} else 
			if(xmlPath=="T64/accuracy"){
				return this.T64_accuracy ;
			} else 
			if(xmlPath=="T64/responseTime"){
				return this.T64_responsetime ;
			} else 
			if(xmlPath=="T64/congruency"){
				return this.T64_congruency ;
			} else 
			if(xmlPath=="T65/accuracy"){
				return this.T65_accuracy ;
			} else 
			if(xmlPath=="T65/responseTime"){
				return this.T65_responsetime ;
			} else 
			if(xmlPath=="T65/congruency"){
				return this.T65_congruency ;
			} else 
			if(xmlPath=="T66/accuracy"){
				return this.T66_accuracy ;
			} else 
			if(xmlPath=="T66/responseTime"){
				return this.T66_responsetime ;
			} else 
			if(xmlPath=="T66/congruency"){
				return this.T66_congruency ;
			} else 
			if(xmlPath=="T67/accuracy"){
				return this.T67_accuracy ;
			} else 
			if(xmlPath=="T67/responseTime"){
				return this.T67_responsetime ;
			} else 
			if(xmlPath=="T67/congruency"){
				return this.T67_congruency ;
			} else 
			if(xmlPath=="T68/accuracy"){
				return this.T68_accuracy ;
			} else 
			if(xmlPath=="T68/responseTime"){
				return this.T68_responsetime ;
			} else 
			if(xmlPath=="T68/congruency"){
				return this.T68_congruency ;
			} else 
			if(xmlPath=="T69/accuracy"){
				return this.T69_accuracy ;
			} else 
			if(xmlPath=="T69/responseTime"){
				return this.T69_responsetime ;
			} else 
			if(xmlPath=="T69/congruency"){
				return this.T69_congruency ;
			} else 
			if(xmlPath=="T70/accuracy"){
				return this.T70_accuracy ;
			} else 
			if(xmlPath=="T70/responseTime"){
				return this.T70_responsetime ;
			} else 
			if(xmlPath=="T70/congruency"){
				return this.T70_congruency ;
			} else 
			if(xmlPath=="T71/accuracy"){
				return this.T71_accuracy ;
			} else 
			if(xmlPath=="T71/responseTime"){
				return this.T71_responsetime ;
			} else 
			if(xmlPath=="T71/congruency"){
				return this.T71_congruency ;
			} else 
			if(xmlPath=="T72/accuracy"){
				return this.T72_accuracy ;
			} else 
			if(xmlPath=="T72/responseTime"){
				return this.T72_responsetime ;
			} else 
			if(xmlPath=="T72/congruency"){
				return this.T72_congruency ;
			} else 
			if(xmlPath=="T73/accuracy"){
				return this.T73_accuracy ;
			} else 
			if(xmlPath=="T73/responseTime"){
				return this.T73_responsetime ;
			} else 
			if(xmlPath=="T73/congruency"){
				return this.T73_congruency ;
			} else 
			if(xmlPath=="T74/accuracy"){
				return this.T74_accuracy ;
			} else 
			if(xmlPath=="T74/responseTime"){
				return this.T74_responsetime ;
			} else 
			if(xmlPath=="T74/congruency"){
				return this.T74_congruency ;
			} else 
			if(xmlPath=="T75/accuracy"){
				return this.T75_accuracy ;
			} else 
			if(xmlPath=="T75/responseTime"){
				return this.T75_responsetime ;
			} else 
			if(xmlPath=="T75/congruency"){
				return this.T75_congruency ;
			} else 
			if(xmlPath=="T76/accuracy"){
				return this.T76_accuracy ;
			} else 
			if(xmlPath=="T76/responseTime"){
				return this.T76_responsetime ;
			} else 
			if(xmlPath=="T76/congruency"){
				return this.T76_congruency ;
			} else 
			if(xmlPath=="T77/accuracy"){
				return this.T77_accuracy ;
			} else 
			if(xmlPath=="T77/responseTime"){
				return this.T77_responsetime ;
			} else 
			if(xmlPath=="T77/congruency"){
				return this.T77_congruency ;
			} else 
			if(xmlPath=="T78/accuracy"){
				return this.T78_accuracy ;
			} else 
			if(xmlPath=="T78/responseTime"){
				return this.T78_responsetime ;
			} else 
			if(xmlPath=="T78/congruency"){
				return this.T78_congruency ;
			} else 
			if(xmlPath=="T79/accuracy"){
				return this.T79_accuracy ;
			} else 
			if(xmlPath=="T79/responseTime"){
				return this.T79_responsetime ;
			} else 
			if(xmlPath=="T79/congruency"){
				return this.T79_congruency ;
			} else 
			if(xmlPath=="T80/accuracy"){
				return this.T80_accuracy ;
			} else 
			if(xmlPath=="T80/responseTime"){
				return this.T80_responsetime ;
			} else 
			if(xmlPath=="T80/congruency"){
				return this.T80_congruency ;
			} else 
			if(xmlPath=="T81/accuracy"){
				return this.T81_accuracy ;
			} else 
			if(xmlPath=="T81/responseTime"){
				return this.T81_responsetime ;
			} else 
			if(xmlPath=="T81/congruency"){
				return this.T81_congruency ;
			} else 
			if(xmlPath=="T82/accuracy"){
				return this.T82_accuracy ;
			} else 
			if(xmlPath=="T82/responseTime"){
				return this.T82_responsetime ;
			} else 
			if(xmlPath=="T82/congruency"){
				return this.T82_congruency ;
			} else 
			if(xmlPath=="T83/accuracy"){
				return this.T83_accuracy ;
			} else 
			if(xmlPath=="T83/responseTime"){
				return this.T83_responsetime ;
			} else 
			if(xmlPath=="T83/congruency"){
				return this.T83_congruency ;
			} else 
			if(xmlPath=="T84/accuracy"){
				return this.T84_accuracy ;
			} else 
			if(xmlPath=="T84/responseTime"){
				return this.T84_responsetime ;
			} else 
			if(xmlPath=="T84/congruency"){
				return this.T84_congruency ;
			} else 
			if(xmlPath=="T85/accuracy"){
				return this.T85_accuracy ;
			} else 
			if(xmlPath=="T85/responseTime"){
				return this.T85_responsetime ;
			} else 
			if(xmlPath=="T85/congruency"){
				return this.T85_congruency ;
			} else 
			if(xmlPath=="T86/accuracy"){
				return this.T86_accuracy ;
			} else 
			if(xmlPath=="T86/responseTime"){
				return this.T86_responsetime ;
			} else 
			if(xmlPath=="T86/congruency"){
				return this.T86_congruency ;
			} else 
			if(xmlPath=="T87/accuracy"){
				return this.T87_accuracy ;
			} else 
			if(xmlPath=="T87/responseTime"){
				return this.T87_responsetime ;
			} else 
			if(xmlPath=="T87/congruency"){
				return this.T87_congruency ;
			} else 
			if(xmlPath=="T88/accuracy"){
				return this.T88_accuracy ;
			} else 
			if(xmlPath=="T88/responseTime"){
				return this.T88_responsetime ;
			} else 
			if(xmlPath=="T88/congruency"){
				return this.T88_congruency ;
			} else 
			if(xmlPath=="T89/accuracy"){
				return this.T89_accuracy ;
			} else 
			if(xmlPath=="T89/responseTime"){
				return this.T89_responsetime ;
			} else 
			if(xmlPath=="T89/congruency"){
				return this.T89_congruency ;
			} else 
			if(xmlPath=="T90/accuracy"){
				return this.T90_accuracy ;
			} else 
			if(xmlPath=="T90/responseTime"){
				return this.T90_responsetime ;
			} else 
			if(xmlPath=="T90/congruency"){
				return this.T90_congruency ;
			} else 
			if(xmlPath=="T91/accuracy"){
				return this.T91_accuracy ;
			} else 
			if(xmlPath=="T91/responseTime"){
				return this.T91_responsetime ;
			} else 
			if(xmlPath=="T91/congruency"){
				return this.T91_congruency ;
			} else 
			if(xmlPath=="T92/accuracy"){
				return this.T92_accuracy ;
			} else 
			if(xmlPath=="T92/responseTime"){
				return this.T92_responsetime ;
			} else 
			if(xmlPath=="T92/congruency"){
				return this.T92_congruency ;
			} else 
			if(xmlPath=="T93/accuracy"){
				return this.T93_accuracy ;
			} else 
			if(xmlPath=="T93/responseTime"){
				return this.T93_responsetime ;
			} else 
			if(xmlPath=="T93/congruency"){
				return this.T93_congruency ;
			} else 
			if(xmlPath=="T94/accuracy"){
				return this.T94_accuracy ;
			} else 
			if(xmlPath=="T94/responseTime"){
				return this.T94_responsetime ;
			} else 
			if(xmlPath=="T94/congruency"){
				return this.T94_congruency ;
			} else 
			if(xmlPath=="T95/accuracy"){
				return this.T95_accuracy ;
			} else 
			if(xmlPath=="T95/responseTime"){
				return this.T95_responsetime ;
			} else 
			if(xmlPath=="T95/congruency"){
				return this.T95_congruency ;
			} else 
			if(xmlPath=="T96/accuracy"){
				return this.T96_accuracy ;
			} else 
			if(xmlPath=="T96/responseTime"){
				return this.T96_responsetime ;
			} else 
			if(xmlPath=="T96/congruency"){
				return this.T96_congruency ;
			} else 
			if(xmlPath=="T97/accuracy"){
				return this.T97_accuracy ;
			} else 
			if(xmlPath=="T97/responseTime"){
				return this.T97_responsetime ;
			} else 
			if(xmlPath=="T97/congruency"){
				return this.T97_congruency ;
			} else 
			if(xmlPath=="T98/accuracy"){
				return this.T98_accuracy ;
			} else 
			if(xmlPath=="T98/responseTime"){
				return this.T98_responsetime ;
			} else 
			if(xmlPath=="T98/congruency"){
				return this.T98_congruency ;
			} else 
			if(xmlPath=="T99/accuracy"){
				return this.T99_accuracy ;
			} else 
			if(xmlPath=="T99/responseTime"){
				return this.T99_responsetime ;
			} else 
			if(xmlPath=="T99/congruency"){
				return this.T99_congruency ;
			} else 
			if(xmlPath=="T100/accuracy"){
				return this.T100_accuracy ;
			} else 
			if(xmlPath=="T100/responseTime"){
				return this.T100_responsetime ;
			} else 
			if(xmlPath=="T100/congruency"){
				return this.T100_congruency ;
			} else 
			if(xmlPath=="T101/accuracy"){
				return this.T101_accuracy ;
			} else 
			if(xmlPath=="T101/responseTime"){
				return this.T101_responsetime ;
			} else 
			if(xmlPath=="T101/congruency"){
				return this.T101_congruency ;
			} else 
			if(xmlPath=="T102/accuracy"){
				return this.T102_accuracy ;
			} else 
			if(xmlPath=="T102/responseTime"){
				return this.T102_responsetime ;
			} else 
			if(xmlPath=="T102/congruency"){
				return this.T102_congruency ;
			} else 
			if(xmlPath=="T103/accuracy"){
				return this.T103_accuracy ;
			} else 
			if(xmlPath=="T103/responseTime"){
				return this.T103_responsetime ;
			} else 
			if(xmlPath=="T103/congruency"){
				return this.T103_congruency ;
			} else 
			if(xmlPath=="T104/accuracy"){
				return this.T104_accuracy ;
			} else 
			if(xmlPath=="T104/responseTime"){
				return this.T104_responsetime ;
			} else 
			if(xmlPath=="T104/congruency"){
				return this.T104_congruency ;
			} else 
			if(xmlPath=="T105/accuracy"){
				return this.T105_accuracy ;
			} else 
			if(xmlPath=="T105/responseTime"){
				return this.T105_responsetime ;
			} else 
			if(xmlPath=="T105/congruency"){
				return this.T105_congruency ;
			} else 
			if(xmlPath=="T106/accuracy"){
				return this.T106_accuracy ;
			} else 
			if(xmlPath=="T106/responseTime"){
				return this.T106_responsetime ;
			} else 
			if(xmlPath=="T106/congruency"){
				return this.T106_congruency ;
			} else 
			if(xmlPath=="T107/accuracy"){
				return this.T107_accuracy ;
			} else 
			if(xmlPath=="T107/responseTime"){
				return this.T107_responsetime ;
			} else 
			if(xmlPath=="T107/congruency"){
				return this.T107_congruency ;
			} else 
			if(xmlPath=="T108/accuracy"){
				return this.T108_accuracy ;
			} else 
			if(xmlPath=="T108/responseTime"){
				return this.T108_responsetime ;
			} else 
			if(xmlPath=="T108/congruency"){
				return this.T108_congruency ;
			} else 
			if(xmlPath=="T109/accuracy"){
				return this.T109_accuracy ;
			} else 
			if(xmlPath=="T109/responseTime"){
				return this.T109_responsetime ;
			} else 
			if(xmlPath=="T109/congruency"){
				return this.T109_congruency ;
			} else 
			if(xmlPath=="T110/accuracy"){
				return this.T110_accuracy ;
			} else 
			if(xmlPath=="T110/responseTime"){
				return this.T110_responsetime ;
			} else 
			if(xmlPath=="T110/congruency"){
				return this.T110_congruency ;
			} else 
			if(xmlPath=="T111/accuracy"){
				return this.T111_accuracy ;
			} else 
			if(xmlPath=="T111/responseTime"){
				return this.T111_responsetime ;
			} else 
			if(xmlPath=="T111/congruency"){
				return this.T111_congruency ;
			} else 
			if(xmlPath=="T112/accuracy"){
				return this.T112_accuracy ;
			} else 
			if(xmlPath=="T112/responseTime"){
				return this.T112_responsetime ;
			} else 
			if(xmlPath=="T112/congruency"){
				return this.T112_congruency ;
			} else 
			if(xmlPath=="T113/accuracy"){
				return this.T113_accuracy ;
			} else 
			if(xmlPath=="T113/responseTime"){
				return this.T113_responsetime ;
			} else 
			if(xmlPath=="T113/congruency"){
				return this.T113_congruency ;
			} else 
			if(xmlPath=="T114/accuracy"){
				return this.T114_accuracy ;
			} else 
			if(xmlPath=="T114/responseTime"){
				return this.T114_responsetime ;
			} else 
			if(xmlPath=="T114/congruency"){
				return this.T114_congruency ;
			} else 
			if(xmlPath=="T115/accuracy"){
				return this.T115_accuracy ;
			} else 
			if(xmlPath=="T115/responseTime"){
				return this.T115_responsetime ;
			} else 
			if(xmlPath=="T115/congruency"){
				return this.T115_congruency ;
			} else 
			if(xmlPath=="T116/accuracy"){
				return this.T116_accuracy ;
			} else 
			if(xmlPath=="T116/responseTime"){
				return this.T116_responsetime ;
			} else 
			if(xmlPath=="T116/congruency"){
				return this.T116_congruency ;
			} else 
			if(xmlPath=="T117/accuracy"){
				return this.T117_accuracy ;
			} else 
			if(xmlPath=="T117/responseTime"){
				return this.T117_responsetime ;
			} else 
			if(xmlPath=="T117/congruency"){
				return this.T117_congruency ;
			} else 
			if(xmlPath=="T118/accuracy"){
				return this.T118_accuracy ;
			} else 
			if(xmlPath=="T118/responseTime"){
				return this.T118_responsetime ;
			} else 
			if(xmlPath=="T118/congruency"){
				return this.T118_congruency ;
			} else 
			if(xmlPath=="T119/accuracy"){
				return this.T119_accuracy ;
			} else 
			if(xmlPath=="T119/responseTime"){
				return this.T119_responsetime ;
			} else 
			if(xmlPath=="T119/congruency"){
				return this.T119_congruency ;
			} else 
			if(xmlPath=="T120/accuracy"){
				return this.T120_accuracy ;
			} else 
			if(xmlPath=="T120/responseTime"){
				return this.T120_responsetime ;
			} else 
			if(xmlPath=="T120/congruency"){
				return this.T120_congruency ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="T1/accuracy"){
				this.T1_accuracy=value;
			} else 
			if(xmlPath=="T1/responseTime"){
				this.T1_responsetime=value;
			} else 
			if(xmlPath=="T1/congruency"){
				this.T1_congruency=value;
			} else 
			if(xmlPath=="T2/accuracy"){
				this.T2_accuracy=value;
			} else 
			if(xmlPath=="T2/responseTime"){
				this.T2_responsetime=value;
			} else 
			if(xmlPath=="T2/congruency"){
				this.T2_congruency=value;
			} else 
			if(xmlPath=="T3/accuracy"){
				this.T3_accuracy=value;
			} else 
			if(xmlPath=="T3/responseTime"){
				this.T3_responsetime=value;
			} else 
			if(xmlPath=="T3/congruency"){
				this.T3_congruency=value;
			} else 
			if(xmlPath=="T4/accuracy"){
				this.T4_accuracy=value;
			} else 
			if(xmlPath=="T4/responseTime"){
				this.T4_responsetime=value;
			} else 
			if(xmlPath=="T4/congruency"){
				this.T4_congruency=value;
			} else 
			if(xmlPath=="T5/accuracy"){
				this.T5_accuracy=value;
			} else 
			if(xmlPath=="T5/responseTime"){
				this.T5_responsetime=value;
			} else 
			if(xmlPath=="T5/congruency"){
				this.T5_congruency=value;
			} else 
			if(xmlPath=="T6/accuracy"){
				this.T6_accuracy=value;
			} else 
			if(xmlPath=="T6/responseTime"){
				this.T6_responsetime=value;
			} else 
			if(xmlPath=="T6/congruency"){
				this.T6_congruency=value;
			} else 
			if(xmlPath=="T7/accuracy"){
				this.T7_accuracy=value;
			} else 
			if(xmlPath=="T7/responseTime"){
				this.T7_responsetime=value;
			} else 
			if(xmlPath=="T7/congruency"){
				this.T7_congruency=value;
			} else 
			if(xmlPath=="T8/accuracy"){
				this.T8_accuracy=value;
			} else 
			if(xmlPath=="T8/responseTime"){
				this.T8_responsetime=value;
			} else 
			if(xmlPath=="T8/congruency"){
				this.T8_congruency=value;
			} else 
			if(xmlPath=="T9/accuracy"){
				this.T9_accuracy=value;
			} else 
			if(xmlPath=="T9/responseTime"){
				this.T9_responsetime=value;
			} else 
			if(xmlPath=="T9/congruency"){
				this.T9_congruency=value;
			} else 
			if(xmlPath=="T10/accuracy"){
				this.T10_accuracy=value;
			} else 
			if(xmlPath=="T10/responseTime"){
				this.T10_responsetime=value;
			} else 
			if(xmlPath=="T10/congruency"){
				this.T10_congruency=value;
			} else 
			if(xmlPath=="T11/accuracy"){
				this.T11_accuracy=value;
			} else 
			if(xmlPath=="T11/responseTime"){
				this.T11_responsetime=value;
			} else 
			if(xmlPath=="T11/congruency"){
				this.T11_congruency=value;
			} else 
			if(xmlPath=="T12/accuracy"){
				this.T12_accuracy=value;
			} else 
			if(xmlPath=="T12/responseTime"){
				this.T12_responsetime=value;
			} else 
			if(xmlPath=="T12/congruency"){
				this.T12_congruency=value;
			} else 
			if(xmlPath=="T13/accuracy"){
				this.T13_accuracy=value;
			} else 
			if(xmlPath=="T13/responseTime"){
				this.T13_responsetime=value;
			} else 
			if(xmlPath=="T13/congruency"){
				this.T13_congruency=value;
			} else 
			if(xmlPath=="T14/accuracy"){
				this.T14_accuracy=value;
			} else 
			if(xmlPath=="T14/responseTime"){
				this.T14_responsetime=value;
			} else 
			if(xmlPath=="T14/congruency"){
				this.T14_congruency=value;
			} else 
			if(xmlPath=="T15/accuracy"){
				this.T15_accuracy=value;
			} else 
			if(xmlPath=="T15/responseTime"){
				this.T15_responsetime=value;
			} else 
			if(xmlPath=="T15/congruency"){
				this.T15_congruency=value;
			} else 
			if(xmlPath=="T16/accuracy"){
				this.T16_accuracy=value;
			} else 
			if(xmlPath=="T16/responseTime"){
				this.T16_responsetime=value;
			} else 
			if(xmlPath=="T16/congruency"){
				this.T16_congruency=value;
			} else 
			if(xmlPath=="T17/accuracy"){
				this.T17_accuracy=value;
			} else 
			if(xmlPath=="T17/responseTime"){
				this.T17_responsetime=value;
			} else 
			if(xmlPath=="T17/congruency"){
				this.T17_congruency=value;
			} else 
			if(xmlPath=="T18/accuracy"){
				this.T18_accuracy=value;
			} else 
			if(xmlPath=="T18/responseTime"){
				this.T18_responsetime=value;
			} else 
			if(xmlPath=="T18/congruency"){
				this.T18_congruency=value;
			} else 
			if(xmlPath=="T19/accuracy"){
				this.T19_accuracy=value;
			} else 
			if(xmlPath=="T19/responseTime"){
				this.T19_responsetime=value;
			} else 
			if(xmlPath=="T19/congruency"){
				this.T19_congruency=value;
			} else 
			if(xmlPath=="T20/accuracy"){
				this.T20_accuracy=value;
			} else 
			if(xmlPath=="T20/responseTime"){
				this.T20_responsetime=value;
			} else 
			if(xmlPath=="T20/congruency"){
				this.T20_congruency=value;
			} else 
			if(xmlPath=="T21/accuracy"){
				this.T21_accuracy=value;
			} else 
			if(xmlPath=="T21/responseTime"){
				this.T21_responsetime=value;
			} else 
			if(xmlPath=="T21/congruency"){
				this.T21_congruency=value;
			} else 
			if(xmlPath=="T22/accuracy"){
				this.T22_accuracy=value;
			} else 
			if(xmlPath=="T22/responseTime"){
				this.T22_responsetime=value;
			} else 
			if(xmlPath=="T22/congruency"){
				this.T22_congruency=value;
			} else 
			if(xmlPath=="T23/accuracy"){
				this.T23_accuracy=value;
			} else 
			if(xmlPath=="T23/responseTime"){
				this.T23_responsetime=value;
			} else 
			if(xmlPath=="T23/congruency"){
				this.T23_congruency=value;
			} else 
			if(xmlPath=="T24/accuracy"){
				this.T24_accuracy=value;
			} else 
			if(xmlPath=="T24/responseTime"){
				this.T24_responsetime=value;
			} else 
			if(xmlPath=="T24/congruency"){
				this.T24_congruency=value;
			} else 
			if(xmlPath=="T25/accuracy"){
				this.T25_accuracy=value;
			} else 
			if(xmlPath=="T25/responseTime"){
				this.T25_responsetime=value;
			} else 
			if(xmlPath=="T25/congruency"){
				this.T25_congruency=value;
			} else 
			if(xmlPath=="T26/accuracy"){
				this.T26_accuracy=value;
			} else 
			if(xmlPath=="T26/responseTime"){
				this.T26_responsetime=value;
			} else 
			if(xmlPath=="T26/congruency"){
				this.T26_congruency=value;
			} else 
			if(xmlPath=="T27/accuracy"){
				this.T27_accuracy=value;
			} else 
			if(xmlPath=="T27/responseTime"){
				this.T27_responsetime=value;
			} else 
			if(xmlPath=="T27/congruency"){
				this.T27_congruency=value;
			} else 
			if(xmlPath=="T28/accuracy"){
				this.T28_accuracy=value;
			} else 
			if(xmlPath=="T28/responseTime"){
				this.T28_responsetime=value;
			} else 
			if(xmlPath=="T28/congruency"){
				this.T28_congruency=value;
			} else 
			if(xmlPath=="T29/accuracy"){
				this.T29_accuracy=value;
			} else 
			if(xmlPath=="T29/responseTime"){
				this.T29_responsetime=value;
			} else 
			if(xmlPath=="T29/congruency"){
				this.T29_congruency=value;
			} else 
			if(xmlPath=="T30/accuracy"){
				this.T30_accuracy=value;
			} else 
			if(xmlPath=="T30/responseTime"){
				this.T30_responsetime=value;
			} else 
			if(xmlPath=="T30/congruency"){
				this.T30_congruency=value;
			} else 
			if(xmlPath=="T31/accuracy"){
				this.T31_accuracy=value;
			} else 
			if(xmlPath=="T31/responseTime"){
				this.T31_responsetime=value;
			} else 
			if(xmlPath=="T31/congruency"){
				this.T31_congruency=value;
			} else 
			if(xmlPath=="T32/accuracy"){
				this.T32_accuracy=value;
			} else 
			if(xmlPath=="T32/responseTime"){
				this.T32_responsetime=value;
			} else 
			if(xmlPath=="T32/congruency"){
				this.T32_congruency=value;
			} else 
			if(xmlPath=="T33/accuracy"){
				this.T33_accuracy=value;
			} else 
			if(xmlPath=="T33/responseTime"){
				this.T33_responsetime=value;
			} else 
			if(xmlPath=="T33/congruency"){
				this.T33_congruency=value;
			} else 
			if(xmlPath=="T34/accuracy"){
				this.T34_accuracy=value;
			} else 
			if(xmlPath=="T34/responseTime"){
				this.T34_responsetime=value;
			} else 
			if(xmlPath=="T34/congruency"){
				this.T34_congruency=value;
			} else 
			if(xmlPath=="T35/accuracy"){
				this.T35_accuracy=value;
			} else 
			if(xmlPath=="T35/responseTime"){
				this.T35_responsetime=value;
			} else 
			if(xmlPath=="T35/congruency"){
				this.T35_congruency=value;
			} else 
			if(xmlPath=="T36/accuracy"){
				this.T36_accuracy=value;
			} else 
			if(xmlPath=="T36/responseTime"){
				this.T36_responsetime=value;
			} else 
			if(xmlPath=="T36/congruency"){
				this.T36_congruency=value;
			} else 
			if(xmlPath=="T37/accuracy"){
				this.T37_accuracy=value;
			} else 
			if(xmlPath=="T37/responseTime"){
				this.T37_responsetime=value;
			} else 
			if(xmlPath=="T37/congruency"){
				this.T37_congruency=value;
			} else 
			if(xmlPath=="T38/accuracy"){
				this.T38_accuracy=value;
			} else 
			if(xmlPath=="T38/responseTime"){
				this.T38_responsetime=value;
			} else 
			if(xmlPath=="T38/congruency"){
				this.T38_congruency=value;
			} else 
			if(xmlPath=="T39/accuracy"){
				this.T39_accuracy=value;
			} else 
			if(xmlPath=="T39/responseTime"){
				this.T39_responsetime=value;
			} else 
			if(xmlPath=="T39/congruency"){
				this.T39_congruency=value;
			} else 
			if(xmlPath=="T40/accuracy"){
				this.T40_accuracy=value;
			} else 
			if(xmlPath=="T40/responseTime"){
				this.T40_responsetime=value;
			} else 
			if(xmlPath=="T40/congruency"){
				this.T40_congruency=value;
			} else 
			if(xmlPath=="T41/accuracy"){
				this.T41_accuracy=value;
			} else 
			if(xmlPath=="T41/responseTime"){
				this.T41_responsetime=value;
			} else 
			if(xmlPath=="T41/congruency"){
				this.T41_congruency=value;
			} else 
			if(xmlPath=="T42/accuracy"){
				this.T42_accuracy=value;
			} else 
			if(xmlPath=="T42/responseTime"){
				this.T42_responsetime=value;
			} else 
			if(xmlPath=="T42/congruency"){
				this.T42_congruency=value;
			} else 
			if(xmlPath=="T43/accuracy"){
				this.T43_accuracy=value;
			} else 
			if(xmlPath=="T43/responseTime"){
				this.T43_responsetime=value;
			} else 
			if(xmlPath=="T43/congruency"){
				this.T43_congruency=value;
			} else 
			if(xmlPath=="T44/accuracy"){
				this.T44_accuracy=value;
			} else 
			if(xmlPath=="T44/responseTime"){
				this.T44_responsetime=value;
			} else 
			if(xmlPath=="T44/congruency"){
				this.T44_congruency=value;
			} else 
			if(xmlPath=="T45/accuracy"){
				this.T45_accuracy=value;
			} else 
			if(xmlPath=="T45/responseTime"){
				this.T45_responsetime=value;
			} else 
			if(xmlPath=="T45/congruency"){
				this.T45_congruency=value;
			} else 
			if(xmlPath=="T46/accuracy"){
				this.T46_accuracy=value;
			} else 
			if(xmlPath=="T46/responseTime"){
				this.T46_responsetime=value;
			} else 
			if(xmlPath=="T46/congruency"){
				this.T46_congruency=value;
			} else 
			if(xmlPath=="T47/accuracy"){
				this.T47_accuracy=value;
			} else 
			if(xmlPath=="T47/responseTime"){
				this.T47_responsetime=value;
			} else 
			if(xmlPath=="T47/congruency"){
				this.T47_congruency=value;
			} else 
			if(xmlPath=="T48/accuracy"){
				this.T48_accuracy=value;
			} else 
			if(xmlPath=="T48/responseTime"){
				this.T48_responsetime=value;
			} else 
			if(xmlPath=="T48/congruency"){
				this.T48_congruency=value;
			} else 
			if(xmlPath=="T49/accuracy"){
				this.T49_accuracy=value;
			} else 
			if(xmlPath=="T49/responseTime"){
				this.T49_responsetime=value;
			} else 
			if(xmlPath=="T49/congruency"){
				this.T49_congruency=value;
			} else 
			if(xmlPath=="T50/accuracy"){
				this.T50_accuracy=value;
			} else 
			if(xmlPath=="T50/responseTime"){
				this.T50_responsetime=value;
			} else 
			if(xmlPath=="T50/congruency"){
				this.T50_congruency=value;
			} else 
			if(xmlPath=="T51/accuracy"){
				this.T51_accuracy=value;
			} else 
			if(xmlPath=="T51/responseTime"){
				this.T51_responsetime=value;
			} else 
			if(xmlPath=="T51/congruency"){
				this.T51_congruency=value;
			} else 
			if(xmlPath=="T52/accuracy"){
				this.T52_accuracy=value;
			} else 
			if(xmlPath=="T52/responseTime"){
				this.T52_responsetime=value;
			} else 
			if(xmlPath=="T52/congruency"){
				this.T52_congruency=value;
			} else 
			if(xmlPath=="T53/accuracy"){
				this.T53_accuracy=value;
			} else 
			if(xmlPath=="T53/responseTime"){
				this.T53_responsetime=value;
			} else 
			if(xmlPath=="T53/congruency"){
				this.T53_congruency=value;
			} else 
			if(xmlPath=="T54/accuracy"){
				this.T54_accuracy=value;
			} else 
			if(xmlPath=="T54/responseTime"){
				this.T54_responsetime=value;
			} else 
			if(xmlPath=="T54/congruency"){
				this.T54_congruency=value;
			} else 
			if(xmlPath=="T55/accuracy"){
				this.T55_accuracy=value;
			} else 
			if(xmlPath=="T55/responseTime"){
				this.T55_responsetime=value;
			} else 
			if(xmlPath=="T55/congruency"){
				this.T55_congruency=value;
			} else 
			if(xmlPath=="T56/accuracy"){
				this.T56_accuracy=value;
			} else 
			if(xmlPath=="T56/responseTime"){
				this.T56_responsetime=value;
			} else 
			if(xmlPath=="T56/congruency"){
				this.T56_congruency=value;
			} else 
			if(xmlPath=="T57/accuracy"){
				this.T57_accuracy=value;
			} else 
			if(xmlPath=="T57/responseTime"){
				this.T57_responsetime=value;
			} else 
			if(xmlPath=="T57/congruency"){
				this.T57_congruency=value;
			} else 
			if(xmlPath=="T58/accuracy"){
				this.T58_accuracy=value;
			} else 
			if(xmlPath=="T58/responseTime"){
				this.T58_responsetime=value;
			} else 
			if(xmlPath=="T58/congruency"){
				this.T58_congruency=value;
			} else 
			if(xmlPath=="T59/accuracy"){
				this.T59_accuracy=value;
			} else 
			if(xmlPath=="T59/responseTime"){
				this.T59_responsetime=value;
			} else 
			if(xmlPath=="T59/congruency"){
				this.T59_congruency=value;
			} else 
			if(xmlPath=="T60/accuracy"){
				this.T60_accuracy=value;
			} else 
			if(xmlPath=="T60/responseTime"){
				this.T60_responsetime=value;
			} else 
			if(xmlPath=="T60/congruency"){
				this.T60_congruency=value;
			} else 
			if(xmlPath=="T61/accuracy"){
				this.T61_accuracy=value;
			} else 
			if(xmlPath=="T61/responseTime"){
				this.T61_responsetime=value;
			} else 
			if(xmlPath=="T61/congruency"){
				this.T61_congruency=value;
			} else 
			if(xmlPath=="T62/accuracy"){
				this.T62_accuracy=value;
			} else 
			if(xmlPath=="T62/responseTime"){
				this.T62_responsetime=value;
			} else 
			if(xmlPath=="T62/congruency"){
				this.T62_congruency=value;
			} else 
			if(xmlPath=="T63/accuracy"){
				this.T63_accuracy=value;
			} else 
			if(xmlPath=="T63/responseTime"){
				this.T63_responsetime=value;
			} else 
			if(xmlPath=="T63/congruency"){
				this.T63_congruency=value;
			} else 
			if(xmlPath=="T64/accuracy"){
				this.T64_accuracy=value;
			} else 
			if(xmlPath=="T64/responseTime"){
				this.T64_responsetime=value;
			} else 
			if(xmlPath=="T64/congruency"){
				this.T64_congruency=value;
			} else 
			if(xmlPath=="T65/accuracy"){
				this.T65_accuracy=value;
			} else 
			if(xmlPath=="T65/responseTime"){
				this.T65_responsetime=value;
			} else 
			if(xmlPath=="T65/congruency"){
				this.T65_congruency=value;
			} else 
			if(xmlPath=="T66/accuracy"){
				this.T66_accuracy=value;
			} else 
			if(xmlPath=="T66/responseTime"){
				this.T66_responsetime=value;
			} else 
			if(xmlPath=="T66/congruency"){
				this.T66_congruency=value;
			} else 
			if(xmlPath=="T67/accuracy"){
				this.T67_accuracy=value;
			} else 
			if(xmlPath=="T67/responseTime"){
				this.T67_responsetime=value;
			} else 
			if(xmlPath=="T67/congruency"){
				this.T67_congruency=value;
			} else 
			if(xmlPath=="T68/accuracy"){
				this.T68_accuracy=value;
			} else 
			if(xmlPath=="T68/responseTime"){
				this.T68_responsetime=value;
			} else 
			if(xmlPath=="T68/congruency"){
				this.T68_congruency=value;
			} else 
			if(xmlPath=="T69/accuracy"){
				this.T69_accuracy=value;
			} else 
			if(xmlPath=="T69/responseTime"){
				this.T69_responsetime=value;
			} else 
			if(xmlPath=="T69/congruency"){
				this.T69_congruency=value;
			} else 
			if(xmlPath=="T70/accuracy"){
				this.T70_accuracy=value;
			} else 
			if(xmlPath=="T70/responseTime"){
				this.T70_responsetime=value;
			} else 
			if(xmlPath=="T70/congruency"){
				this.T70_congruency=value;
			} else 
			if(xmlPath=="T71/accuracy"){
				this.T71_accuracy=value;
			} else 
			if(xmlPath=="T71/responseTime"){
				this.T71_responsetime=value;
			} else 
			if(xmlPath=="T71/congruency"){
				this.T71_congruency=value;
			} else 
			if(xmlPath=="T72/accuracy"){
				this.T72_accuracy=value;
			} else 
			if(xmlPath=="T72/responseTime"){
				this.T72_responsetime=value;
			} else 
			if(xmlPath=="T72/congruency"){
				this.T72_congruency=value;
			} else 
			if(xmlPath=="T73/accuracy"){
				this.T73_accuracy=value;
			} else 
			if(xmlPath=="T73/responseTime"){
				this.T73_responsetime=value;
			} else 
			if(xmlPath=="T73/congruency"){
				this.T73_congruency=value;
			} else 
			if(xmlPath=="T74/accuracy"){
				this.T74_accuracy=value;
			} else 
			if(xmlPath=="T74/responseTime"){
				this.T74_responsetime=value;
			} else 
			if(xmlPath=="T74/congruency"){
				this.T74_congruency=value;
			} else 
			if(xmlPath=="T75/accuracy"){
				this.T75_accuracy=value;
			} else 
			if(xmlPath=="T75/responseTime"){
				this.T75_responsetime=value;
			} else 
			if(xmlPath=="T75/congruency"){
				this.T75_congruency=value;
			} else 
			if(xmlPath=="T76/accuracy"){
				this.T76_accuracy=value;
			} else 
			if(xmlPath=="T76/responseTime"){
				this.T76_responsetime=value;
			} else 
			if(xmlPath=="T76/congruency"){
				this.T76_congruency=value;
			} else 
			if(xmlPath=="T77/accuracy"){
				this.T77_accuracy=value;
			} else 
			if(xmlPath=="T77/responseTime"){
				this.T77_responsetime=value;
			} else 
			if(xmlPath=="T77/congruency"){
				this.T77_congruency=value;
			} else 
			if(xmlPath=="T78/accuracy"){
				this.T78_accuracy=value;
			} else 
			if(xmlPath=="T78/responseTime"){
				this.T78_responsetime=value;
			} else 
			if(xmlPath=="T78/congruency"){
				this.T78_congruency=value;
			} else 
			if(xmlPath=="T79/accuracy"){
				this.T79_accuracy=value;
			} else 
			if(xmlPath=="T79/responseTime"){
				this.T79_responsetime=value;
			} else 
			if(xmlPath=="T79/congruency"){
				this.T79_congruency=value;
			} else 
			if(xmlPath=="T80/accuracy"){
				this.T80_accuracy=value;
			} else 
			if(xmlPath=="T80/responseTime"){
				this.T80_responsetime=value;
			} else 
			if(xmlPath=="T80/congruency"){
				this.T80_congruency=value;
			} else 
			if(xmlPath=="T81/accuracy"){
				this.T81_accuracy=value;
			} else 
			if(xmlPath=="T81/responseTime"){
				this.T81_responsetime=value;
			} else 
			if(xmlPath=="T81/congruency"){
				this.T81_congruency=value;
			} else 
			if(xmlPath=="T82/accuracy"){
				this.T82_accuracy=value;
			} else 
			if(xmlPath=="T82/responseTime"){
				this.T82_responsetime=value;
			} else 
			if(xmlPath=="T82/congruency"){
				this.T82_congruency=value;
			} else 
			if(xmlPath=="T83/accuracy"){
				this.T83_accuracy=value;
			} else 
			if(xmlPath=="T83/responseTime"){
				this.T83_responsetime=value;
			} else 
			if(xmlPath=="T83/congruency"){
				this.T83_congruency=value;
			} else 
			if(xmlPath=="T84/accuracy"){
				this.T84_accuracy=value;
			} else 
			if(xmlPath=="T84/responseTime"){
				this.T84_responsetime=value;
			} else 
			if(xmlPath=="T84/congruency"){
				this.T84_congruency=value;
			} else 
			if(xmlPath=="T85/accuracy"){
				this.T85_accuracy=value;
			} else 
			if(xmlPath=="T85/responseTime"){
				this.T85_responsetime=value;
			} else 
			if(xmlPath=="T85/congruency"){
				this.T85_congruency=value;
			} else 
			if(xmlPath=="T86/accuracy"){
				this.T86_accuracy=value;
			} else 
			if(xmlPath=="T86/responseTime"){
				this.T86_responsetime=value;
			} else 
			if(xmlPath=="T86/congruency"){
				this.T86_congruency=value;
			} else 
			if(xmlPath=="T87/accuracy"){
				this.T87_accuracy=value;
			} else 
			if(xmlPath=="T87/responseTime"){
				this.T87_responsetime=value;
			} else 
			if(xmlPath=="T87/congruency"){
				this.T87_congruency=value;
			} else 
			if(xmlPath=="T88/accuracy"){
				this.T88_accuracy=value;
			} else 
			if(xmlPath=="T88/responseTime"){
				this.T88_responsetime=value;
			} else 
			if(xmlPath=="T88/congruency"){
				this.T88_congruency=value;
			} else 
			if(xmlPath=="T89/accuracy"){
				this.T89_accuracy=value;
			} else 
			if(xmlPath=="T89/responseTime"){
				this.T89_responsetime=value;
			} else 
			if(xmlPath=="T89/congruency"){
				this.T89_congruency=value;
			} else 
			if(xmlPath=="T90/accuracy"){
				this.T90_accuracy=value;
			} else 
			if(xmlPath=="T90/responseTime"){
				this.T90_responsetime=value;
			} else 
			if(xmlPath=="T90/congruency"){
				this.T90_congruency=value;
			} else 
			if(xmlPath=="T91/accuracy"){
				this.T91_accuracy=value;
			} else 
			if(xmlPath=="T91/responseTime"){
				this.T91_responsetime=value;
			} else 
			if(xmlPath=="T91/congruency"){
				this.T91_congruency=value;
			} else 
			if(xmlPath=="T92/accuracy"){
				this.T92_accuracy=value;
			} else 
			if(xmlPath=="T92/responseTime"){
				this.T92_responsetime=value;
			} else 
			if(xmlPath=="T92/congruency"){
				this.T92_congruency=value;
			} else 
			if(xmlPath=="T93/accuracy"){
				this.T93_accuracy=value;
			} else 
			if(xmlPath=="T93/responseTime"){
				this.T93_responsetime=value;
			} else 
			if(xmlPath=="T93/congruency"){
				this.T93_congruency=value;
			} else 
			if(xmlPath=="T94/accuracy"){
				this.T94_accuracy=value;
			} else 
			if(xmlPath=="T94/responseTime"){
				this.T94_responsetime=value;
			} else 
			if(xmlPath=="T94/congruency"){
				this.T94_congruency=value;
			} else 
			if(xmlPath=="T95/accuracy"){
				this.T95_accuracy=value;
			} else 
			if(xmlPath=="T95/responseTime"){
				this.T95_responsetime=value;
			} else 
			if(xmlPath=="T95/congruency"){
				this.T95_congruency=value;
			} else 
			if(xmlPath=="T96/accuracy"){
				this.T96_accuracy=value;
			} else 
			if(xmlPath=="T96/responseTime"){
				this.T96_responsetime=value;
			} else 
			if(xmlPath=="T96/congruency"){
				this.T96_congruency=value;
			} else 
			if(xmlPath=="T97/accuracy"){
				this.T97_accuracy=value;
			} else 
			if(xmlPath=="T97/responseTime"){
				this.T97_responsetime=value;
			} else 
			if(xmlPath=="T97/congruency"){
				this.T97_congruency=value;
			} else 
			if(xmlPath=="T98/accuracy"){
				this.T98_accuracy=value;
			} else 
			if(xmlPath=="T98/responseTime"){
				this.T98_responsetime=value;
			} else 
			if(xmlPath=="T98/congruency"){
				this.T98_congruency=value;
			} else 
			if(xmlPath=="T99/accuracy"){
				this.T99_accuracy=value;
			} else 
			if(xmlPath=="T99/responseTime"){
				this.T99_responsetime=value;
			} else 
			if(xmlPath=="T99/congruency"){
				this.T99_congruency=value;
			} else 
			if(xmlPath=="T100/accuracy"){
				this.T100_accuracy=value;
			} else 
			if(xmlPath=="T100/responseTime"){
				this.T100_responsetime=value;
			} else 
			if(xmlPath=="T100/congruency"){
				this.T100_congruency=value;
			} else 
			if(xmlPath=="T101/accuracy"){
				this.T101_accuracy=value;
			} else 
			if(xmlPath=="T101/responseTime"){
				this.T101_responsetime=value;
			} else 
			if(xmlPath=="T101/congruency"){
				this.T101_congruency=value;
			} else 
			if(xmlPath=="T102/accuracy"){
				this.T102_accuracy=value;
			} else 
			if(xmlPath=="T102/responseTime"){
				this.T102_responsetime=value;
			} else 
			if(xmlPath=="T102/congruency"){
				this.T102_congruency=value;
			} else 
			if(xmlPath=="T103/accuracy"){
				this.T103_accuracy=value;
			} else 
			if(xmlPath=="T103/responseTime"){
				this.T103_responsetime=value;
			} else 
			if(xmlPath=="T103/congruency"){
				this.T103_congruency=value;
			} else 
			if(xmlPath=="T104/accuracy"){
				this.T104_accuracy=value;
			} else 
			if(xmlPath=="T104/responseTime"){
				this.T104_responsetime=value;
			} else 
			if(xmlPath=="T104/congruency"){
				this.T104_congruency=value;
			} else 
			if(xmlPath=="T105/accuracy"){
				this.T105_accuracy=value;
			} else 
			if(xmlPath=="T105/responseTime"){
				this.T105_responsetime=value;
			} else 
			if(xmlPath=="T105/congruency"){
				this.T105_congruency=value;
			} else 
			if(xmlPath=="T106/accuracy"){
				this.T106_accuracy=value;
			} else 
			if(xmlPath=="T106/responseTime"){
				this.T106_responsetime=value;
			} else 
			if(xmlPath=="T106/congruency"){
				this.T106_congruency=value;
			} else 
			if(xmlPath=="T107/accuracy"){
				this.T107_accuracy=value;
			} else 
			if(xmlPath=="T107/responseTime"){
				this.T107_responsetime=value;
			} else 
			if(xmlPath=="T107/congruency"){
				this.T107_congruency=value;
			} else 
			if(xmlPath=="T108/accuracy"){
				this.T108_accuracy=value;
			} else 
			if(xmlPath=="T108/responseTime"){
				this.T108_responsetime=value;
			} else 
			if(xmlPath=="T108/congruency"){
				this.T108_congruency=value;
			} else 
			if(xmlPath=="T109/accuracy"){
				this.T109_accuracy=value;
			} else 
			if(xmlPath=="T109/responseTime"){
				this.T109_responsetime=value;
			} else 
			if(xmlPath=="T109/congruency"){
				this.T109_congruency=value;
			} else 
			if(xmlPath=="T110/accuracy"){
				this.T110_accuracy=value;
			} else 
			if(xmlPath=="T110/responseTime"){
				this.T110_responsetime=value;
			} else 
			if(xmlPath=="T110/congruency"){
				this.T110_congruency=value;
			} else 
			if(xmlPath=="T111/accuracy"){
				this.T111_accuracy=value;
			} else 
			if(xmlPath=="T111/responseTime"){
				this.T111_responsetime=value;
			} else 
			if(xmlPath=="T111/congruency"){
				this.T111_congruency=value;
			} else 
			if(xmlPath=="T112/accuracy"){
				this.T112_accuracy=value;
			} else 
			if(xmlPath=="T112/responseTime"){
				this.T112_responsetime=value;
			} else 
			if(xmlPath=="T112/congruency"){
				this.T112_congruency=value;
			} else 
			if(xmlPath=="T113/accuracy"){
				this.T113_accuracy=value;
			} else 
			if(xmlPath=="T113/responseTime"){
				this.T113_responsetime=value;
			} else 
			if(xmlPath=="T113/congruency"){
				this.T113_congruency=value;
			} else 
			if(xmlPath=="T114/accuracy"){
				this.T114_accuracy=value;
			} else 
			if(xmlPath=="T114/responseTime"){
				this.T114_responsetime=value;
			} else 
			if(xmlPath=="T114/congruency"){
				this.T114_congruency=value;
			} else 
			if(xmlPath=="T115/accuracy"){
				this.T115_accuracy=value;
			} else 
			if(xmlPath=="T115/responseTime"){
				this.T115_responsetime=value;
			} else 
			if(xmlPath=="T115/congruency"){
				this.T115_congruency=value;
			} else 
			if(xmlPath=="T116/accuracy"){
				this.T116_accuracy=value;
			} else 
			if(xmlPath=="T116/responseTime"){
				this.T116_responsetime=value;
			} else 
			if(xmlPath=="T116/congruency"){
				this.T116_congruency=value;
			} else 
			if(xmlPath=="T117/accuracy"){
				this.T117_accuracy=value;
			} else 
			if(xmlPath=="T117/responseTime"){
				this.T117_responsetime=value;
			} else 
			if(xmlPath=="T117/congruency"){
				this.T117_congruency=value;
			} else 
			if(xmlPath=="T118/accuracy"){
				this.T118_accuracy=value;
			} else 
			if(xmlPath=="T118/responseTime"){
				this.T118_responsetime=value;
			} else 
			if(xmlPath=="T118/congruency"){
				this.T118_congruency=value;
			} else 
			if(xmlPath=="T119/accuracy"){
				this.T119_accuracy=value;
			} else 
			if(xmlPath=="T119/responseTime"){
				this.T119_responsetime=value;
			} else 
			if(xmlPath=="T119/congruency"){
				this.T119_congruency=value;
			} else 
			if(xmlPath=="T120/accuracy"){
				this.T120_accuracy=value;
			} else 
			if(xmlPath=="T120/responseTime"){
				this.T120_responsetime=value;
			} else 
			if(xmlPath=="T120/congruency"){
				this.T120_congruency=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="T1/accuracy"){
			return "field_data";
		}else if (xmlPath=="T1/responseTime"){
			return "field_data";
		}else if (xmlPath=="T1/congruency"){
			return "field_data";
		}else if (xmlPath=="T2/accuracy"){
			return "field_data";
		}else if (xmlPath=="T2/responseTime"){
			return "field_data";
		}else if (xmlPath=="T2/congruency"){
			return "field_data";
		}else if (xmlPath=="T3/accuracy"){
			return "field_data";
		}else if (xmlPath=="T3/responseTime"){
			return "field_data";
		}else if (xmlPath=="T3/congruency"){
			return "field_data";
		}else if (xmlPath=="T4/accuracy"){
			return "field_data";
		}else if (xmlPath=="T4/responseTime"){
			return "field_data";
		}else if (xmlPath=="T4/congruency"){
			return "field_data";
		}else if (xmlPath=="T5/accuracy"){
			return "field_data";
		}else if (xmlPath=="T5/responseTime"){
			return "field_data";
		}else if (xmlPath=="T5/congruency"){
			return "field_data";
		}else if (xmlPath=="T6/accuracy"){
			return "field_data";
		}else if (xmlPath=="T6/responseTime"){
			return "field_data";
		}else if (xmlPath=="T6/congruency"){
			return "field_data";
		}else if (xmlPath=="T7/accuracy"){
			return "field_data";
		}else if (xmlPath=="T7/responseTime"){
			return "field_data";
		}else if (xmlPath=="T7/congruency"){
			return "field_data";
		}else if (xmlPath=="T8/accuracy"){
			return "field_data";
		}else if (xmlPath=="T8/responseTime"){
			return "field_data";
		}else if (xmlPath=="T8/congruency"){
			return "field_data";
		}else if (xmlPath=="T9/accuracy"){
			return "field_data";
		}else if (xmlPath=="T9/responseTime"){
			return "field_data";
		}else if (xmlPath=="T9/congruency"){
			return "field_data";
		}else if (xmlPath=="T10/accuracy"){
			return "field_data";
		}else if (xmlPath=="T10/responseTime"){
			return "field_data";
		}else if (xmlPath=="T10/congruency"){
			return "field_data";
		}else if (xmlPath=="T11/accuracy"){
			return "field_data";
		}else if (xmlPath=="T11/responseTime"){
			return "field_data";
		}else if (xmlPath=="T11/congruency"){
			return "field_data";
		}else if (xmlPath=="T12/accuracy"){
			return "field_data";
		}else if (xmlPath=="T12/responseTime"){
			return "field_data";
		}else if (xmlPath=="T12/congruency"){
			return "field_data";
		}else if (xmlPath=="T13/accuracy"){
			return "field_data";
		}else if (xmlPath=="T13/responseTime"){
			return "field_data";
		}else if (xmlPath=="T13/congruency"){
			return "field_data";
		}else if (xmlPath=="T14/accuracy"){
			return "field_data";
		}else if (xmlPath=="T14/responseTime"){
			return "field_data";
		}else if (xmlPath=="T14/congruency"){
			return "field_data";
		}else if (xmlPath=="T15/accuracy"){
			return "field_data";
		}else if (xmlPath=="T15/responseTime"){
			return "field_data";
		}else if (xmlPath=="T15/congruency"){
			return "field_data";
		}else if (xmlPath=="T16/accuracy"){
			return "field_data";
		}else if (xmlPath=="T16/responseTime"){
			return "field_data";
		}else if (xmlPath=="T16/congruency"){
			return "field_data";
		}else if (xmlPath=="T17/accuracy"){
			return "field_data";
		}else if (xmlPath=="T17/responseTime"){
			return "field_data";
		}else if (xmlPath=="T17/congruency"){
			return "field_data";
		}else if (xmlPath=="T18/accuracy"){
			return "field_data";
		}else if (xmlPath=="T18/responseTime"){
			return "field_data";
		}else if (xmlPath=="T18/congruency"){
			return "field_data";
		}else if (xmlPath=="T19/accuracy"){
			return "field_data";
		}else if (xmlPath=="T19/responseTime"){
			return "field_data";
		}else if (xmlPath=="T19/congruency"){
			return "field_data";
		}else if (xmlPath=="T20/accuracy"){
			return "field_data";
		}else if (xmlPath=="T20/responseTime"){
			return "field_data";
		}else if (xmlPath=="T20/congruency"){
			return "field_data";
		}else if (xmlPath=="T21/accuracy"){
			return "field_data";
		}else if (xmlPath=="T21/responseTime"){
			return "field_data";
		}else if (xmlPath=="T21/congruency"){
			return "field_data";
		}else if (xmlPath=="T22/accuracy"){
			return "field_data";
		}else if (xmlPath=="T22/responseTime"){
			return "field_data";
		}else if (xmlPath=="T22/congruency"){
			return "field_data";
		}else if (xmlPath=="T23/accuracy"){
			return "field_data";
		}else if (xmlPath=="T23/responseTime"){
			return "field_data";
		}else if (xmlPath=="T23/congruency"){
			return "field_data";
		}else if (xmlPath=="T24/accuracy"){
			return "field_data";
		}else if (xmlPath=="T24/responseTime"){
			return "field_data";
		}else if (xmlPath=="T24/congruency"){
			return "field_data";
		}else if (xmlPath=="T25/accuracy"){
			return "field_data";
		}else if (xmlPath=="T25/responseTime"){
			return "field_data";
		}else if (xmlPath=="T25/congruency"){
			return "field_data";
		}else if (xmlPath=="T26/accuracy"){
			return "field_data";
		}else if (xmlPath=="T26/responseTime"){
			return "field_data";
		}else if (xmlPath=="T26/congruency"){
			return "field_data";
		}else if (xmlPath=="T27/accuracy"){
			return "field_data";
		}else if (xmlPath=="T27/responseTime"){
			return "field_data";
		}else if (xmlPath=="T27/congruency"){
			return "field_data";
		}else if (xmlPath=="T28/accuracy"){
			return "field_data";
		}else if (xmlPath=="T28/responseTime"){
			return "field_data";
		}else if (xmlPath=="T28/congruency"){
			return "field_data";
		}else if (xmlPath=="T29/accuracy"){
			return "field_data";
		}else if (xmlPath=="T29/responseTime"){
			return "field_data";
		}else if (xmlPath=="T29/congruency"){
			return "field_data";
		}else if (xmlPath=="T30/accuracy"){
			return "field_data";
		}else if (xmlPath=="T30/responseTime"){
			return "field_data";
		}else if (xmlPath=="T30/congruency"){
			return "field_data";
		}else if (xmlPath=="T31/accuracy"){
			return "field_data";
		}else if (xmlPath=="T31/responseTime"){
			return "field_data";
		}else if (xmlPath=="T31/congruency"){
			return "field_data";
		}else if (xmlPath=="T32/accuracy"){
			return "field_data";
		}else if (xmlPath=="T32/responseTime"){
			return "field_data";
		}else if (xmlPath=="T32/congruency"){
			return "field_data";
		}else if (xmlPath=="T33/accuracy"){
			return "field_data";
		}else if (xmlPath=="T33/responseTime"){
			return "field_data";
		}else if (xmlPath=="T33/congruency"){
			return "field_data";
		}else if (xmlPath=="T34/accuracy"){
			return "field_data";
		}else if (xmlPath=="T34/responseTime"){
			return "field_data";
		}else if (xmlPath=="T34/congruency"){
			return "field_data";
		}else if (xmlPath=="T35/accuracy"){
			return "field_data";
		}else if (xmlPath=="T35/responseTime"){
			return "field_data";
		}else if (xmlPath=="T35/congruency"){
			return "field_data";
		}else if (xmlPath=="T36/accuracy"){
			return "field_data";
		}else if (xmlPath=="T36/responseTime"){
			return "field_data";
		}else if (xmlPath=="T36/congruency"){
			return "field_data";
		}else if (xmlPath=="T37/accuracy"){
			return "field_data";
		}else if (xmlPath=="T37/responseTime"){
			return "field_data";
		}else if (xmlPath=="T37/congruency"){
			return "field_data";
		}else if (xmlPath=="T38/accuracy"){
			return "field_data";
		}else if (xmlPath=="T38/responseTime"){
			return "field_data";
		}else if (xmlPath=="T38/congruency"){
			return "field_data";
		}else if (xmlPath=="T39/accuracy"){
			return "field_data";
		}else if (xmlPath=="T39/responseTime"){
			return "field_data";
		}else if (xmlPath=="T39/congruency"){
			return "field_data";
		}else if (xmlPath=="T40/accuracy"){
			return "field_data";
		}else if (xmlPath=="T40/responseTime"){
			return "field_data";
		}else if (xmlPath=="T40/congruency"){
			return "field_data";
		}else if (xmlPath=="T41/accuracy"){
			return "field_data";
		}else if (xmlPath=="T41/responseTime"){
			return "field_data";
		}else if (xmlPath=="T41/congruency"){
			return "field_data";
		}else if (xmlPath=="T42/accuracy"){
			return "field_data";
		}else if (xmlPath=="T42/responseTime"){
			return "field_data";
		}else if (xmlPath=="T42/congruency"){
			return "field_data";
		}else if (xmlPath=="T43/accuracy"){
			return "field_data";
		}else if (xmlPath=="T43/responseTime"){
			return "field_data";
		}else if (xmlPath=="T43/congruency"){
			return "field_data";
		}else if (xmlPath=="T44/accuracy"){
			return "field_data";
		}else if (xmlPath=="T44/responseTime"){
			return "field_data";
		}else if (xmlPath=="T44/congruency"){
			return "field_data";
		}else if (xmlPath=="T45/accuracy"){
			return "field_data";
		}else if (xmlPath=="T45/responseTime"){
			return "field_data";
		}else if (xmlPath=="T45/congruency"){
			return "field_data";
		}else if (xmlPath=="T46/accuracy"){
			return "field_data";
		}else if (xmlPath=="T46/responseTime"){
			return "field_data";
		}else if (xmlPath=="T46/congruency"){
			return "field_data";
		}else if (xmlPath=="T47/accuracy"){
			return "field_data";
		}else if (xmlPath=="T47/responseTime"){
			return "field_data";
		}else if (xmlPath=="T47/congruency"){
			return "field_data";
		}else if (xmlPath=="T48/accuracy"){
			return "field_data";
		}else if (xmlPath=="T48/responseTime"){
			return "field_data";
		}else if (xmlPath=="T48/congruency"){
			return "field_data";
		}else if (xmlPath=="T49/accuracy"){
			return "field_data";
		}else if (xmlPath=="T49/responseTime"){
			return "field_data";
		}else if (xmlPath=="T49/congruency"){
			return "field_data";
		}else if (xmlPath=="T50/accuracy"){
			return "field_data";
		}else if (xmlPath=="T50/responseTime"){
			return "field_data";
		}else if (xmlPath=="T50/congruency"){
			return "field_data";
		}else if (xmlPath=="T51/accuracy"){
			return "field_data";
		}else if (xmlPath=="T51/responseTime"){
			return "field_data";
		}else if (xmlPath=="T51/congruency"){
			return "field_data";
		}else if (xmlPath=="T52/accuracy"){
			return "field_data";
		}else if (xmlPath=="T52/responseTime"){
			return "field_data";
		}else if (xmlPath=="T52/congruency"){
			return "field_data";
		}else if (xmlPath=="T53/accuracy"){
			return "field_data";
		}else if (xmlPath=="T53/responseTime"){
			return "field_data";
		}else if (xmlPath=="T53/congruency"){
			return "field_data";
		}else if (xmlPath=="T54/accuracy"){
			return "field_data";
		}else if (xmlPath=="T54/responseTime"){
			return "field_data";
		}else if (xmlPath=="T54/congruency"){
			return "field_data";
		}else if (xmlPath=="T55/accuracy"){
			return "field_data";
		}else if (xmlPath=="T55/responseTime"){
			return "field_data";
		}else if (xmlPath=="T55/congruency"){
			return "field_data";
		}else if (xmlPath=="T56/accuracy"){
			return "field_data";
		}else if (xmlPath=="T56/responseTime"){
			return "field_data";
		}else if (xmlPath=="T56/congruency"){
			return "field_data";
		}else if (xmlPath=="T57/accuracy"){
			return "field_data";
		}else if (xmlPath=="T57/responseTime"){
			return "field_data";
		}else if (xmlPath=="T57/congruency"){
			return "field_data";
		}else if (xmlPath=="T58/accuracy"){
			return "field_data";
		}else if (xmlPath=="T58/responseTime"){
			return "field_data";
		}else if (xmlPath=="T58/congruency"){
			return "field_data";
		}else if (xmlPath=="T59/accuracy"){
			return "field_data";
		}else if (xmlPath=="T59/responseTime"){
			return "field_data";
		}else if (xmlPath=="T59/congruency"){
			return "field_data";
		}else if (xmlPath=="T60/accuracy"){
			return "field_data";
		}else if (xmlPath=="T60/responseTime"){
			return "field_data";
		}else if (xmlPath=="T60/congruency"){
			return "field_data";
		}else if (xmlPath=="T61/accuracy"){
			return "field_data";
		}else if (xmlPath=="T61/responseTime"){
			return "field_data";
		}else if (xmlPath=="T61/congruency"){
			return "field_data";
		}else if (xmlPath=="T62/accuracy"){
			return "field_data";
		}else if (xmlPath=="T62/responseTime"){
			return "field_data";
		}else if (xmlPath=="T62/congruency"){
			return "field_data";
		}else if (xmlPath=="T63/accuracy"){
			return "field_data";
		}else if (xmlPath=="T63/responseTime"){
			return "field_data";
		}else if (xmlPath=="T63/congruency"){
			return "field_data";
		}else if (xmlPath=="T64/accuracy"){
			return "field_data";
		}else if (xmlPath=="T64/responseTime"){
			return "field_data";
		}else if (xmlPath=="T64/congruency"){
			return "field_data";
		}else if (xmlPath=="T65/accuracy"){
			return "field_data";
		}else if (xmlPath=="T65/responseTime"){
			return "field_data";
		}else if (xmlPath=="T65/congruency"){
			return "field_data";
		}else if (xmlPath=="T66/accuracy"){
			return "field_data";
		}else if (xmlPath=="T66/responseTime"){
			return "field_data";
		}else if (xmlPath=="T66/congruency"){
			return "field_data";
		}else if (xmlPath=="T67/accuracy"){
			return "field_data";
		}else if (xmlPath=="T67/responseTime"){
			return "field_data";
		}else if (xmlPath=="T67/congruency"){
			return "field_data";
		}else if (xmlPath=="T68/accuracy"){
			return "field_data";
		}else if (xmlPath=="T68/responseTime"){
			return "field_data";
		}else if (xmlPath=="T68/congruency"){
			return "field_data";
		}else if (xmlPath=="T69/accuracy"){
			return "field_data";
		}else if (xmlPath=="T69/responseTime"){
			return "field_data";
		}else if (xmlPath=="T69/congruency"){
			return "field_data";
		}else if (xmlPath=="T70/accuracy"){
			return "field_data";
		}else if (xmlPath=="T70/responseTime"){
			return "field_data";
		}else if (xmlPath=="T70/congruency"){
			return "field_data";
		}else if (xmlPath=="T71/accuracy"){
			return "field_data";
		}else if (xmlPath=="T71/responseTime"){
			return "field_data";
		}else if (xmlPath=="T71/congruency"){
			return "field_data";
		}else if (xmlPath=="T72/accuracy"){
			return "field_data";
		}else if (xmlPath=="T72/responseTime"){
			return "field_data";
		}else if (xmlPath=="T72/congruency"){
			return "field_data";
		}else if (xmlPath=="T73/accuracy"){
			return "field_data";
		}else if (xmlPath=="T73/responseTime"){
			return "field_data";
		}else if (xmlPath=="T73/congruency"){
			return "field_data";
		}else if (xmlPath=="T74/accuracy"){
			return "field_data";
		}else if (xmlPath=="T74/responseTime"){
			return "field_data";
		}else if (xmlPath=="T74/congruency"){
			return "field_data";
		}else if (xmlPath=="T75/accuracy"){
			return "field_data";
		}else if (xmlPath=="T75/responseTime"){
			return "field_data";
		}else if (xmlPath=="T75/congruency"){
			return "field_data";
		}else if (xmlPath=="T76/accuracy"){
			return "field_data";
		}else if (xmlPath=="T76/responseTime"){
			return "field_data";
		}else if (xmlPath=="T76/congruency"){
			return "field_data";
		}else if (xmlPath=="T77/accuracy"){
			return "field_data";
		}else if (xmlPath=="T77/responseTime"){
			return "field_data";
		}else if (xmlPath=="T77/congruency"){
			return "field_data";
		}else if (xmlPath=="T78/accuracy"){
			return "field_data";
		}else if (xmlPath=="T78/responseTime"){
			return "field_data";
		}else if (xmlPath=="T78/congruency"){
			return "field_data";
		}else if (xmlPath=="T79/accuracy"){
			return "field_data";
		}else if (xmlPath=="T79/responseTime"){
			return "field_data";
		}else if (xmlPath=="T79/congruency"){
			return "field_data";
		}else if (xmlPath=="T80/accuracy"){
			return "field_data";
		}else if (xmlPath=="T80/responseTime"){
			return "field_data";
		}else if (xmlPath=="T80/congruency"){
			return "field_data";
		}else if (xmlPath=="T81/accuracy"){
			return "field_data";
		}else if (xmlPath=="T81/responseTime"){
			return "field_data";
		}else if (xmlPath=="T81/congruency"){
			return "field_data";
		}else if (xmlPath=="T82/accuracy"){
			return "field_data";
		}else if (xmlPath=="T82/responseTime"){
			return "field_data";
		}else if (xmlPath=="T82/congruency"){
			return "field_data";
		}else if (xmlPath=="T83/accuracy"){
			return "field_data";
		}else if (xmlPath=="T83/responseTime"){
			return "field_data";
		}else if (xmlPath=="T83/congruency"){
			return "field_data";
		}else if (xmlPath=="T84/accuracy"){
			return "field_data";
		}else if (xmlPath=="T84/responseTime"){
			return "field_data";
		}else if (xmlPath=="T84/congruency"){
			return "field_data";
		}else if (xmlPath=="T85/accuracy"){
			return "field_data";
		}else if (xmlPath=="T85/responseTime"){
			return "field_data";
		}else if (xmlPath=="T85/congruency"){
			return "field_data";
		}else if (xmlPath=="T86/accuracy"){
			return "field_data";
		}else if (xmlPath=="T86/responseTime"){
			return "field_data";
		}else if (xmlPath=="T86/congruency"){
			return "field_data";
		}else if (xmlPath=="T87/accuracy"){
			return "field_data";
		}else if (xmlPath=="T87/responseTime"){
			return "field_data";
		}else if (xmlPath=="T87/congruency"){
			return "field_data";
		}else if (xmlPath=="T88/accuracy"){
			return "field_data";
		}else if (xmlPath=="T88/responseTime"){
			return "field_data";
		}else if (xmlPath=="T88/congruency"){
			return "field_data";
		}else if (xmlPath=="T89/accuracy"){
			return "field_data";
		}else if (xmlPath=="T89/responseTime"){
			return "field_data";
		}else if (xmlPath=="T89/congruency"){
			return "field_data";
		}else if (xmlPath=="T90/accuracy"){
			return "field_data";
		}else if (xmlPath=="T90/responseTime"){
			return "field_data";
		}else if (xmlPath=="T90/congruency"){
			return "field_data";
		}else if (xmlPath=="T91/accuracy"){
			return "field_data";
		}else if (xmlPath=="T91/responseTime"){
			return "field_data";
		}else if (xmlPath=="T91/congruency"){
			return "field_data";
		}else if (xmlPath=="T92/accuracy"){
			return "field_data";
		}else if (xmlPath=="T92/responseTime"){
			return "field_data";
		}else if (xmlPath=="T92/congruency"){
			return "field_data";
		}else if (xmlPath=="T93/accuracy"){
			return "field_data";
		}else if (xmlPath=="T93/responseTime"){
			return "field_data";
		}else if (xmlPath=="T93/congruency"){
			return "field_data";
		}else if (xmlPath=="T94/accuracy"){
			return "field_data";
		}else if (xmlPath=="T94/responseTime"){
			return "field_data";
		}else if (xmlPath=="T94/congruency"){
			return "field_data";
		}else if (xmlPath=="T95/accuracy"){
			return "field_data";
		}else if (xmlPath=="T95/responseTime"){
			return "field_data";
		}else if (xmlPath=="T95/congruency"){
			return "field_data";
		}else if (xmlPath=="T96/accuracy"){
			return "field_data";
		}else if (xmlPath=="T96/responseTime"){
			return "field_data";
		}else if (xmlPath=="T96/congruency"){
			return "field_data";
		}else if (xmlPath=="T97/accuracy"){
			return "field_data";
		}else if (xmlPath=="T97/responseTime"){
			return "field_data";
		}else if (xmlPath=="T97/congruency"){
			return "field_data";
		}else if (xmlPath=="T98/accuracy"){
			return "field_data";
		}else if (xmlPath=="T98/responseTime"){
			return "field_data";
		}else if (xmlPath=="T98/congruency"){
			return "field_data";
		}else if (xmlPath=="T99/accuracy"){
			return "field_data";
		}else if (xmlPath=="T99/responseTime"){
			return "field_data";
		}else if (xmlPath=="T99/congruency"){
			return "field_data";
		}else if (xmlPath=="T100/accuracy"){
			return "field_data";
		}else if (xmlPath=="T100/responseTime"){
			return "field_data";
		}else if (xmlPath=="T100/congruency"){
			return "field_data";
		}else if (xmlPath=="T101/accuracy"){
			return "field_data";
		}else if (xmlPath=="T101/responseTime"){
			return "field_data";
		}else if (xmlPath=="T101/congruency"){
			return "field_data";
		}else if (xmlPath=="T102/accuracy"){
			return "field_data";
		}else if (xmlPath=="T102/responseTime"){
			return "field_data";
		}else if (xmlPath=="T102/congruency"){
			return "field_data";
		}else if (xmlPath=="T103/accuracy"){
			return "field_data";
		}else if (xmlPath=="T103/responseTime"){
			return "field_data";
		}else if (xmlPath=="T103/congruency"){
			return "field_data";
		}else if (xmlPath=="T104/accuracy"){
			return "field_data";
		}else if (xmlPath=="T104/responseTime"){
			return "field_data";
		}else if (xmlPath=="T104/congruency"){
			return "field_data";
		}else if (xmlPath=="T105/accuracy"){
			return "field_data";
		}else if (xmlPath=="T105/responseTime"){
			return "field_data";
		}else if (xmlPath=="T105/congruency"){
			return "field_data";
		}else if (xmlPath=="T106/accuracy"){
			return "field_data";
		}else if (xmlPath=="T106/responseTime"){
			return "field_data";
		}else if (xmlPath=="T106/congruency"){
			return "field_data";
		}else if (xmlPath=="T107/accuracy"){
			return "field_data";
		}else if (xmlPath=="T107/responseTime"){
			return "field_data";
		}else if (xmlPath=="T107/congruency"){
			return "field_data";
		}else if (xmlPath=="T108/accuracy"){
			return "field_data";
		}else if (xmlPath=="T108/responseTime"){
			return "field_data";
		}else if (xmlPath=="T108/congruency"){
			return "field_data";
		}else if (xmlPath=="T109/accuracy"){
			return "field_data";
		}else if (xmlPath=="T109/responseTime"){
			return "field_data";
		}else if (xmlPath=="T109/congruency"){
			return "field_data";
		}else if (xmlPath=="T110/accuracy"){
			return "field_data";
		}else if (xmlPath=="T110/responseTime"){
			return "field_data";
		}else if (xmlPath=="T110/congruency"){
			return "field_data";
		}else if (xmlPath=="T111/accuracy"){
			return "field_data";
		}else if (xmlPath=="T111/responseTime"){
			return "field_data";
		}else if (xmlPath=="T111/congruency"){
			return "field_data";
		}else if (xmlPath=="T112/accuracy"){
			return "field_data";
		}else if (xmlPath=="T112/responseTime"){
			return "field_data";
		}else if (xmlPath=="T112/congruency"){
			return "field_data";
		}else if (xmlPath=="T113/accuracy"){
			return "field_data";
		}else if (xmlPath=="T113/responseTime"){
			return "field_data";
		}else if (xmlPath=="T113/congruency"){
			return "field_data";
		}else if (xmlPath=="T114/accuracy"){
			return "field_data";
		}else if (xmlPath=="T114/responseTime"){
			return "field_data";
		}else if (xmlPath=="T114/congruency"){
			return "field_data";
		}else if (xmlPath=="T115/accuracy"){
			return "field_data";
		}else if (xmlPath=="T115/responseTime"){
			return "field_data";
		}else if (xmlPath=="T115/congruency"){
			return "field_data";
		}else if (xmlPath=="T116/accuracy"){
			return "field_data";
		}else if (xmlPath=="T116/responseTime"){
			return "field_data";
		}else if (xmlPath=="T116/congruency"){
			return "field_data";
		}else if (xmlPath=="T117/accuracy"){
			return "field_data";
		}else if (xmlPath=="T117/responseTime"){
			return "field_data";
		}else if (xmlPath=="T117/congruency"){
			return "field_data";
		}else if (xmlPath=="T118/accuracy"){
			return "field_data";
		}else if (xmlPath=="T118/responseTime"){
			return "field_data";
		}else if (xmlPath=="T118/congruency"){
			return "field_data";
		}else if (xmlPath=="T119/accuracy"){
			return "field_data";
		}else if (xmlPath=="T119/responseTime"){
			return "field_data";
		}else if (xmlPath=="T119/congruency"){
			return "field_data";
		}else if (xmlPath=="T120/accuracy"){
			return "field_data";
		}else if (xmlPath=="T120/responseTime"){
			return "field_data";
		}else if (xmlPath=="T120/congruency"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cbat:Simon";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cbat:Simon>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
			var child0=0;
			var att0=0;
			if(this.T1_responsetime!=null)
			child0++;
			if(this.T1_congruency!=null)
			child0++;
			if(this.T1_accuracy!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<cbat:T1";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T1_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T1_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T1_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T1_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T1_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T1_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T1>";
			}
			}

			var child1=0;
			var att1=0;
			if(this.T2_responsetime!=null)
			child1++;
			if(this.T2_congruency!=null)
			child1++;
			if(this.T2_accuracy!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<cbat:T2";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T2_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T2_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T2_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T2_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T2_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T2_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T2>";
			}
			}

			var child2=0;
			var att2=0;
			if(this.T3_accuracy!=null)
			child2++;
			if(this.T3_responsetime!=null)
			child2++;
			if(this.T3_congruency!=null)
			child2++;
			if(child2>0 || att2>0){
				xmlTxt+="\n<cbat:T3";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T3_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T3_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T3_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T3_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T3_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T3_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T3>";
			}
			}

			var child3=0;
			var att3=0;
			if(this.T4_accuracy!=null)
			child3++;
			if(this.T4_responsetime!=null)
			child3++;
			if(this.T4_congruency!=null)
			child3++;
			if(child3>0 || att3>0){
				xmlTxt+="\n<cbat:T4";
			if(child3==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T4_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T4_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T4_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T4_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T4_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T4_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T4>";
			}
			}

			var child4=0;
			var att4=0;
			if(this.T5_accuracy!=null)
			child4++;
			if(this.T5_congruency!=null)
			child4++;
			if(this.T5_responsetime!=null)
			child4++;
			if(child4>0 || att4>0){
				xmlTxt+="\n<cbat:T5";
			if(child4==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T5_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T5_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T5_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T5_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T5_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T5_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T5>";
			}
			}

			var child5=0;
			var att5=0;
			if(this.T6_responsetime!=null)
			child5++;
			if(this.T6_accuracy!=null)
			child5++;
			if(this.T6_congruency!=null)
			child5++;
			if(child5>0 || att5>0){
				xmlTxt+="\n<cbat:T6";
			if(child5==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T6_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T6_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T6_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T6_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T6_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T6_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T6>";
			}
			}

			var child6=0;
			var att6=0;
			if(this.T7_responsetime!=null)
			child6++;
			if(this.T7_accuracy!=null)
			child6++;
			if(this.T7_congruency!=null)
			child6++;
			if(child6>0 || att6>0){
				xmlTxt+="\n<cbat:T7";
			if(child6==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T7_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T7_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T7_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T7_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T7_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T7_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T7>";
			}
			}

			var child7=0;
			var att7=0;
			if(this.T8_responsetime!=null)
			child7++;
			if(this.T8_congruency!=null)
			child7++;
			if(this.T8_accuracy!=null)
			child7++;
			if(child7>0 || att7>0){
				xmlTxt+="\n<cbat:T8";
			if(child7==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T8_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T8_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T8_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T8_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T8_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T8_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T8>";
			}
			}

			var child8=0;
			var att8=0;
			if(this.T9_responsetime!=null)
			child8++;
			if(this.T9_accuracy!=null)
			child8++;
			if(this.T9_congruency!=null)
			child8++;
			if(child8>0 || att8>0){
				xmlTxt+="\n<cbat:T9";
			if(child8==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T9_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T9_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T9_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T9_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T9_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T9_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T9>";
			}
			}

			var child9=0;
			var att9=0;
			if(this.T10_congruency!=null)
			child9++;
			if(this.T10_responsetime!=null)
			child9++;
			if(this.T10_accuracy!=null)
			child9++;
			if(child9>0 || att9>0){
				xmlTxt+="\n<cbat:T10";
			if(child9==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T10_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T10_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T10_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T10_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T10_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T10_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T10>";
			}
			}

			var child10=0;
			var att10=0;
			if(this.T11_congruency!=null)
			child10++;
			if(this.T11_responsetime!=null)
			child10++;
			if(this.T11_accuracy!=null)
			child10++;
			if(child10>0 || att10>0){
				xmlTxt+="\n<cbat:T11";
			if(child10==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T11_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T11_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T11_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T11_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T11_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T11_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T11>";
			}
			}

			var child11=0;
			var att11=0;
			if(this.T12_responsetime!=null)
			child11++;
			if(this.T12_accuracy!=null)
			child11++;
			if(this.T12_congruency!=null)
			child11++;
			if(child11>0 || att11>0){
				xmlTxt+="\n<cbat:T12";
			if(child11==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T12_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T12_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T12_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T12_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T12_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T12_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T12>";
			}
			}

			var child12=0;
			var att12=0;
			if(this.T13_congruency!=null)
			child12++;
			if(this.T13_responsetime!=null)
			child12++;
			if(this.T13_accuracy!=null)
			child12++;
			if(child12>0 || att12>0){
				xmlTxt+="\n<cbat:T13";
			if(child12==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T13_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T13_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T13_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T13_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T13_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T13_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T13>";
			}
			}

			var child13=0;
			var att13=0;
			if(this.T14_responsetime!=null)
			child13++;
			if(this.T14_congruency!=null)
			child13++;
			if(this.T14_accuracy!=null)
			child13++;
			if(child13>0 || att13>0){
				xmlTxt+="\n<cbat:T14";
			if(child13==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T14_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T14_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T14_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T14_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T14_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T14_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T14>";
			}
			}

			var child14=0;
			var att14=0;
			if(this.T15_accuracy!=null)
			child14++;
			if(this.T15_congruency!=null)
			child14++;
			if(this.T15_responsetime!=null)
			child14++;
			if(child14>0 || att14>0){
				xmlTxt+="\n<cbat:T15";
			if(child14==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T15_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T15_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T15_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T15_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T15_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T15_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T15>";
			}
			}

			var child15=0;
			var att15=0;
			if(this.T16_accuracy!=null)
			child15++;
			if(this.T16_responsetime!=null)
			child15++;
			if(this.T16_congruency!=null)
			child15++;
			if(child15>0 || att15>0){
				xmlTxt+="\n<cbat:T16";
			if(child15==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T16_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T16_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T16_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T16_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T16_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T16_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T16>";
			}
			}

			var child16=0;
			var att16=0;
			if(this.T17_accuracy!=null)
			child16++;
			if(this.T17_responsetime!=null)
			child16++;
			if(this.T17_congruency!=null)
			child16++;
			if(child16>0 || att16>0){
				xmlTxt+="\n<cbat:T17";
			if(child16==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T17_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T17_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T17_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T17_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T17_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T17_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T17>";
			}
			}

			var child17=0;
			var att17=0;
			if(this.T18_accuracy!=null)
			child17++;
			if(this.T18_responsetime!=null)
			child17++;
			if(this.T18_congruency!=null)
			child17++;
			if(child17>0 || att17>0){
				xmlTxt+="\n<cbat:T18";
			if(child17==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T18_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T18_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T18_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T18_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T18_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T18_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T18>";
			}
			}

			var child18=0;
			var att18=0;
			if(this.T19_responsetime!=null)
			child18++;
			if(this.T19_accuracy!=null)
			child18++;
			if(this.T19_congruency!=null)
			child18++;
			if(child18>0 || att18>0){
				xmlTxt+="\n<cbat:T19";
			if(child18==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T19_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T19_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T19_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T19_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T19_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T19_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T19>";
			}
			}

			var child19=0;
			var att19=0;
			if(this.T20_responsetime!=null)
			child19++;
			if(this.T20_congruency!=null)
			child19++;
			if(this.T20_accuracy!=null)
			child19++;
			if(child19>0 || att19>0){
				xmlTxt+="\n<cbat:T20";
			if(child19==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T20_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T20_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T20_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T20_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T20_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T20_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T20>";
			}
			}

			var child20=0;
			var att20=0;
			if(this.T21_congruency!=null)
			child20++;
			if(this.T21_responsetime!=null)
			child20++;
			if(this.T21_accuracy!=null)
			child20++;
			if(child20>0 || att20>0){
				xmlTxt+="\n<cbat:T21";
			if(child20==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T21_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T21_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T21_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T21_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T21_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T21_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T21>";
			}
			}

			var child21=0;
			var att21=0;
			if(this.T22_accuracy!=null)
			child21++;
			if(this.T22_responsetime!=null)
			child21++;
			if(this.T22_congruency!=null)
			child21++;
			if(child21>0 || att21>0){
				xmlTxt+="\n<cbat:T22";
			if(child21==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T22_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T22_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T22_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T22_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T22_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T22_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T22>";
			}
			}

			var child22=0;
			var att22=0;
			if(this.T23_responsetime!=null)
			child22++;
			if(this.T23_accuracy!=null)
			child22++;
			if(this.T23_congruency!=null)
			child22++;
			if(child22>0 || att22>0){
				xmlTxt+="\n<cbat:T23";
			if(child22==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T23_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T23_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T23_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T23_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T23_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T23_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T23>";
			}
			}

			var child23=0;
			var att23=0;
			if(this.T24_responsetime!=null)
			child23++;
			if(this.T24_accuracy!=null)
			child23++;
			if(this.T24_congruency!=null)
			child23++;
			if(child23>0 || att23>0){
				xmlTxt+="\n<cbat:T24";
			if(child23==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T24_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T24_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T24_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T24_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T24_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T24_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T24>";
			}
			}

			var child24=0;
			var att24=0;
			if(this.T25_responsetime!=null)
			child24++;
			if(this.T25_accuracy!=null)
			child24++;
			if(this.T25_congruency!=null)
			child24++;
			if(child24>0 || att24>0){
				xmlTxt+="\n<cbat:T25";
			if(child24==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T25_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T25_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T25_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T25_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T25_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T25_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T25>";
			}
			}

			var child25=0;
			var att25=0;
			if(this.T26_responsetime!=null)
			child25++;
			if(this.T26_congruency!=null)
			child25++;
			if(this.T26_accuracy!=null)
			child25++;
			if(child25>0 || att25>0){
				xmlTxt+="\n<cbat:T26";
			if(child25==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T26_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T26_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T26_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T26_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T26_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T26_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T26>";
			}
			}

			var child26=0;
			var att26=0;
			if(this.T27_accuracy!=null)
			child26++;
			if(this.T27_congruency!=null)
			child26++;
			if(this.T27_responsetime!=null)
			child26++;
			if(child26>0 || att26>0){
				xmlTxt+="\n<cbat:T27";
			if(child26==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T27_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T27_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T27_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T27_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T27_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T27_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T27>";
			}
			}

			var child27=0;
			var att27=0;
			if(this.T28_accuracy!=null)
			child27++;
			if(this.T28_congruency!=null)
			child27++;
			if(this.T28_responsetime!=null)
			child27++;
			if(child27>0 || att27>0){
				xmlTxt+="\n<cbat:T28";
			if(child27==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T28_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T28_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T28_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T28_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T28_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T28_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T28>";
			}
			}

			var child28=0;
			var att28=0;
			if(this.T29_congruency!=null)
			child28++;
			if(this.T29_accuracy!=null)
			child28++;
			if(this.T29_responsetime!=null)
			child28++;
			if(child28>0 || att28>0){
				xmlTxt+="\n<cbat:T29";
			if(child28==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T29_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T29_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T29_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T29_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T29_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T29_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T29>";
			}
			}

			var child29=0;
			var att29=0;
			if(this.T30_responsetime!=null)
			child29++;
			if(this.T30_accuracy!=null)
			child29++;
			if(this.T30_congruency!=null)
			child29++;
			if(child29>0 || att29>0){
				xmlTxt+="\n<cbat:T30";
			if(child29==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T30_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T30_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T30_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T30_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T30_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T30_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T30>";
			}
			}

			var child30=0;
			var att30=0;
			if(this.T31_accuracy!=null)
			child30++;
			if(this.T31_congruency!=null)
			child30++;
			if(this.T31_responsetime!=null)
			child30++;
			if(child30>0 || att30>0){
				xmlTxt+="\n<cbat:T31";
			if(child30==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T31_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T31_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T31_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T31_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T31_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T31_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T31>";
			}
			}

			var child31=0;
			var att31=0;
			if(this.T32_congruency!=null)
			child31++;
			if(this.T32_accuracy!=null)
			child31++;
			if(this.T32_responsetime!=null)
			child31++;
			if(child31>0 || att31>0){
				xmlTxt+="\n<cbat:T32";
			if(child31==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T32_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T32_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T32_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T32_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T32_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T32_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T32>";
			}
			}

			var child32=0;
			var att32=0;
			if(this.T33_accuracy!=null)
			child32++;
			if(this.T33_congruency!=null)
			child32++;
			if(this.T33_responsetime!=null)
			child32++;
			if(child32>0 || att32>0){
				xmlTxt+="\n<cbat:T33";
			if(child32==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T33_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T33_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T33_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T33_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T33_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T33_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T33>";
			}
			}

			var child33=0;
			var att33=0;
			if(this.T34_accuracy!=null)
			child33++;
			if(this.T34_responsetime!=null)
			child33++;
			if(this.T34_congruency!=null)
			child33++;
			if(child33>0 || att33>0){
				xmlTxt+="\n<cbat:T34";
			if(child33==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T34_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T34_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T34_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T34_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T34_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T34_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T34>";
			}
			}

			var child34=0;
			var att34=0;
			if(this.T35_congruency!=null)
			child34++;
			if(this.T35_accuracy!=null)
			child34++;
			if(this.T35_responsetime!=null)
			child34++;
			if(child34>0 || att34>0){
				xmlTxt+="\n<cbat:T35";
			if(child34==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T35_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T35_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T35_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T35_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T35_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T35_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T35>";
			}
			}

			var child35=0;
			var att35=0;
			if(this.T36_congruency!=null)
			child35++;
			if(this.T36_responsetime!=null)
			child35++;
			if(this.T36_accuracy!=null)
			child35++;
			if(child35>0 || att35>0){
				xmlTxt+="\n<cbat:T36";
			if(child35==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T36_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T36_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T36_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T36_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T36_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T36_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T36>";
			}
			}

			var child36=0;
			var att36=0;
			if(this.T37_congruency!=null)
			child36++;
			if(this.T37_responsetime!=null)
			child36++;
			if(this.T37_accuracy!=null)
			child36++;
			if(child36>0 || att36>0){
				xmlTxt+="\n<cbat:T37";
			if(child36==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T37_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T37_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T37_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T37_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T37_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T37_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T37>";
			}
			}

			var child37=0;
			var att37=0;
			if(this.T38_congruency!=null)
			child37++;
			if(this.T38_responsetime!=null)
			child37++;
			if(this.T38_accuracy!=null)
			child37++;
			if(child37>0 || att37>0){
				xmlTxt+="\n<cbat:T38";
			if(child37==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T38_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T38_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T38_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T38_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T38_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T38_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T38>";
			}
			}

			var child38=0;
			var att38=0;
			if(this.T39_responsetime!=null)
			child38++;
			if(this.T39_congruency!=null)
			child38++;
			if(this.T39_accuracy!=null)
			child38++;
			if(child38>0 || att38>0){
				xmlTxt+="\n<cbat:T39";
			if(child38==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T39_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T39_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T39_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T39_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T39_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T39_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T39>";
			}
			}

			var child39=0;
			var att39=0;
			if(this.T40_congruency!=null)
			child39++;
			if(this.T40_responsetime!=null)
			child39++;
			if(this.T40_accuracy!=null)
			child39++;
			if(child39>0 || att39>0){
				xmlTxt+="\n<cbat:T40";
			if(child39==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T40_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T40_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T40_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T40_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T40_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T40_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T40>";
			}
			}

			var child40=0;
			var att40=0;
			if(this.T41_congruency!=null)
			child40++;
			if(this.T41_responsetime!=null)
			child40++;
			if(this.T41_accuracy!=null)
			child40++;
			if(child40>0 || att40>0){
				xmlTxt+="\n<cbat:T41";
			if(child40==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T41_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T41_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T41_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T41_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T41_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T41_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T41>";
			}
			}

			var child41=0;
			var att41=0;
			if(this.T42_responsetime!=null)
			child41++;
			if(this.T42_accuracy!=null)
			child41++;
			if(this.T42_congruency!=null)
			child41++;
			if(child41>0 || att41>0){
				xmlTxt+="\n<cbat:T42";
			if(child41==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T42_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T42_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T42_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T42_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T42_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T42_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T42>";
			}
			}

			var child42=0;
			var att42=0;
			if(this.T43_congruency!=null)
			child42++;
			if(this.T43_responsetime!=null)
			child42++;
			if(this.T43_accuracy!=null)
			child42++;
			if(child42>0 || att42>0){
				xmlTxt+="\n<cbat:T43";
			if(child42==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T43_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T43_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T43_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T43_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T43_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T43_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T43>";
			}
			}

			var child43=0;
			var att43=0;
			if(this.T44_responsetime!=null)
			child43++;
			if(this.T44_congruency!=null)
			child43++;
			if(this.T44_accuracy!=null)
			child43++;
			if(child43>0 || att43>0){
				xmlTxt+="\n<cbat:T44";
			if(child43==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T44_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T44_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T44_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T44_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T44_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T44_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T44>";
			}
			}

			var child44=0;
			var att44=0;
			if(this.T45_responsetime!=null)
			child44++;
			if(this.T45_accuracy!=null)
			child44++;
			if(this.T45_congruency!=null)
			child44++;
			if(child44>0 || att44>0){
				xmlTxt+="\n<cbat:T45";
			if(child44==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T45_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T45_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T45_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T45_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T45_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T45_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T45>";
			}
			}

			var child45=0;
			var att45=0;
			if(this.T46_congruency!=null)
			child45++;
			if(this.T46_responsetime!=null)
			child45++;
			if(this.T46_accuracy!=null)
			child45++;
			if(child45>0 || att45>0){
				xmlTxt+="\n<cbat:T46";
			if(child45==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T46_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T46_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T46_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T46_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T46_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T46_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T46>";
			}
			}

			var child46=0;
			var att46=0;
			if(this.T47_accuracy!=null)
			child46++;
			if(this.T47_responsetime!=null)
			child46++;
			if(this.T47_congruency!=null)
			child46++;
			if(child46>0 || att46>0){
				xmlTxt+="\n<cbat:T47";
			if(child46==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T47_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T47_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T47_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T47_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T47_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T47_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T47>";
			}
			}

			var child47=0;
			var att47=0;
			if(this.T48_congruency!=null)
			child47++;
			if(this.T48_accuracy!=null)
			child47++;
			if(this.T48_responsetime!=null)
			child47++;
			if(child47>0 || att47>0){
				xmlTxt+="\n<cbat:T48";
			if(child47==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T48_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T48_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T48_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T48_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T48_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T48_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T48>";
			}
			}

			var child48=0;
			var att48=0;
			if(this.T49_responsetime!=null)
			child48++;
			if(this.T49_accuracy!=null)
			child48++;
			if(this.T49_congruency!=null)
			child48++;
			if(child48>0 || att48>0){
				xmlTxt+="\n<cbat:T49";
			if(child48==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T49_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T49_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T49_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T49_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T49_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T49_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T49>";
			}
			}

			var child49=0;
			var att49=0;
			if(this.T50_responsetime!=null)
			child49++;
			if(this.T50_congruency!=null)
			child49++;
			if(this.T50_accuracy!=null)
			child49++;
			if(child49>0 || att49>0){
				xmlTxt+="\n<cbat:T50";
			if(child49==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T50_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T50_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T50_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T50_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T50_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T50_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T50>";
			}
			}

			var child50=0;
			var att50=0;
			if(this.T51_congruency!=null)
			child50++;
			if(this.T51_responsetime!=null)
			child50++;
			if(this.T51_accuracy!=null)
			child50++;
			if(child50>0 || att50>0){
				xmlTxt+="\n<cbat:T51";
			if(child50==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T51_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T51_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T51_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T51_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T51_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T51_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T51>";
			}
			}

			var child51=0;
			var att51=0;
			if(this.T52_accuracy!=null)
			child51++;
			if(this.T52_responsetime!=null)
			child51++;
			if(this.T52_congruency!=null)
			child51++;
			if(child51>0 || att51>0){
				xmlTxt+="\n<cbat:T52";
			if(child51==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T52_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T52_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T52_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T52_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T52_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T52_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T52>";
			}
			}

			var child52=0;
			var att52=0;
			if(this.T53_responsetime!=null)
			child52++;
			if(this.T53_accuracy!=null)
			child52++;
			if(this.T53_congruency!=null)
			child52++;
			if(child52>0 || att52>0){
				xmlTxt+="\n<cbat:T53";
			if(child52==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T53_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T53_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T53_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T53_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T53_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T53_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T53>";
			}
			}

			var child53=0;
			var att53=0;
			if(this.T54_accuracy!=null)
			child53++;
			if(this.T54_responsetime!=null)
			child53++;
			if(this.T54_congruency!=null)
			child53++;
			if(child53>0 || att53>0){
				xmlTxt+="\n<cbat:T54";
			if(child53==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T54_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T54_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T54_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T54_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T54_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T54_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T54>";
			}
			}

			var child54=0;
			var att54=0;
			if(this.T55_responsetime!=null)
			child54++;
			if(this.T55_accuracy!=null)
			child54++;
			if(this.T55_congruency!=null)
			child54++;
			if(child54>0 || att54>0){
				xmlTxt+="\n<cbat:T55";
			if(child54==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T55_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T55_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T55_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T55_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T55_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T55_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T55>";
			}
			}

			var child55=0;
			var att55=0;
			if(this.T56_responsetime!=null)
			child55++;
			if(this.T56_accuracy!=null)
			child55++;
			if(this.T56_congruency!=null)
			child55++;
			if(child55>0 || att55>0){
				xmlTxt+="\n<cbat:T56";
			if(child55==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T56_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T56_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T56_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T56_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T56_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T56_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T56>";
			}
			}

			var child56=0;
			var att56=0;
			if(this.T57_congruency!=null)
			child56++;
			if(this.T57_accuracy!=null)
			child56++;
			if(this.T57_responsetime!=null)
			child56++;
			if(child56>0 || att56>0){
				xmlTxt+="\n<cbat:T57";
			if(child56==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T57_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T57_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T57_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T57_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T57_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T57_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T57>";
			}
			}

			var child57=0;
			var att57=0;
			if(this.T58_accuracy!=null)
			child57++;
			if(this.T58_congruency!=null)
			child57++;
			if(this.T58_responsetime!=null)
			child57++;
			if(child57>0 || att57>0){
				xmlTxt+="\n<cbat:T58";
			if(child57==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T58_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T58_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T58_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T58_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T58_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T58_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T58>";
			}
			}

			var child58=0;
			var att58=0;
			if(this.T59_congruency!=null)
			child58++;
			if(this.T59_accuracy!=null)
			child58++;
			if(this.T59_responsetime!=null)
			child58++;
			if(child58>0 || att58>0){
				xmlTxt+="\n<cbat:T59";
			if(child58==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T59_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T59_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T59_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T59_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T59_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T59_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T59>";
			}
			}

			var child59=0;
			var att59=0;
			if(this.T60_responsetime!=null)
			child59++;
			if(this.T60_accuracy!=null)
			child59++;
			if(this.T60_congruency!=null)
			child59++;
			if(child59>0 || att59>0){
				xmlTxt+="\n<cbat:T60";
			if(child59==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T60_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T60_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T60_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T60_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T60_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T60_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T60>";
			}
			}

			var child60=0;
			var att60=0;
			if(this.T61_responsetime!=null)
			child60++;
			if(this.T61_accuracy!=null)
			child60++;
			if(this.T61_congruency!=null)
			child60++;
			if(child60>0 || att60>0){
				xmlTxt+="\n<cbat:T61";
			if(child60==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T61_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T61_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T61_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T61_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T61_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T61_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T61>";
			}
			}

			var child61=0;
			var att61=0;
			if(this.T62_congruency!=null)
			child61++;
			if(this.T62_accuracy!=null)
			child61++;
			if(this.T62_responsetime!=null)
			child61++;
			if(child61>0 || att61>0){
				xmlTxt+="\n<cbat:T62";
			if(child61==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T62_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T62_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T62_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T62_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T62_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T62_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T62>";
			}
			}

			var child62=0;
			var att62=0;
			if(this.T63_accuracy!=null)
			child62++;
			if(this.T63_congruency!=null)
			child62++;
			if(this.T63_responsetime!=null)
			child62++;
			if(child62>0 || att62>0){
				xmlTxt+="\n<cbat:T63";
			if(child62==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T63_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T63_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T63_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T63_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T63_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T63_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T63>";
			}
			}

			var child63=0;
			var att63=0;
			if(this.T64_accuracy!=null)
			child63++;
			if(this.T64_congruency!=null)
			child63++;
			if(this.T64_responsetime!=null)
			child63++;
			if(child63>0 || att63>0){
				xmlTxt+="\n<cbat:T64";
			if(child63==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T64_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T64_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T64_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T64_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T64_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T64_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T64>";
			}
			}

			var child64=0;
			var att64=0;
			if(this.T65_congruency!=null)
			child64++;
			if(this.T65_accuracy!=null)
			child64++;
			if(this.T65_responsetime!=null)
			child64++;
			if(child64>0 || att64>0){
				xmlTxt+="\n<cbat:T65";
			if(child64==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T65_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T65_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T65_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T65_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T65_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T65_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T65>";
			}
			}

			var child65=0;
			var att65=0;
			if(this.T66_accuracy!=null)
			child65++;
			if(this.T66_congruency!=null)
			child65++;
			if(this.T66_responsetime!=null)
			child65++;
			if(child65>0 || att65>0){
				xmlTxt+="\n<cbat:T66";
			if(child65==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T66_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T66_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T66_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T66_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T66_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T66_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T66>";
			}
			}

			var child66=0;
			var att66=0;
			if(this.T67_accuracy!=null)
			child66++;
			if(this.T67_responsetime!=null)
			child66++;
			if(this.T67_congruency!=null)
			child66++;
			if(child66>0 || att66>0){
				xmlTxt+="\n<cbat:T67";
			if(child66==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T67_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T67_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T67_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T67_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T67_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T67_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T67>";
			}
			}

			var child67=0;
			var att67=0;
			if(this.T68_congruency!=null)
			child67++;
			if(this.T68_responsetime!=null)
			child67++;
			if(this.T68_accuracy!=null)
			child67++;
			if(child67>0 || att67>0){
				xmlTxt+="\n<cbat:T68";
			if(child67==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T68_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T68_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T68_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T68_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T68_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T68_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T68>";
			}
			}

			var child68=0;
			var att68=0;
			if(this.T69_congruency!=null)
			child68++;
			if(this.T69_responsetime!=null)
			child68++;
			if(this.T69_accuracy!=null)
			child68++;
			if(child68>0 || att68>0){
				xmlTxt+="\n<cbat:T69";
			if(child68==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T69_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T69_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T69_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T69_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T69_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T69_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T69>";
			}
			}

			var child69=0;
			var att69=0;
			if(this.T70_congruency!=null)
			child69++;
			if(this.T70_accuracy!=null)
			child69++;
			if(this.T70_responsetime!=null)
			child69++;
			if(child69>0 || att69>0){
				xmlTxt+="\n<cbat:T70";
			if(child69==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T70_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T70_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T70_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T70_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T70_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T70_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T70>";
			}
			}

			var child70=0;
			var att70=0;
			if(this.T71_congruency!=null)
			child70++;
			if(this.T71_accuracy!=null)
			child70++;
			if(this.T71_responsetime!=null)
			child70++;
			if(child70>0 || att70>0){
				xmlTxt+="\n<cbat:T71";
			if(child70==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T71_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T71_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T71_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T71_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T71_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T71_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T71>";
			}
			}

			var child71=0;
			var att71=0;
			if(this.T72_responsetime!=null)
			child71++;
			if(this.T72_accuracy!=null)
			child71++;
			if(this.T72_congruency!=null)
			child71++;
			if(child71>0 || att71>0){
				xmlTxt+="\n<cbat:T72";
			if(child71==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T72_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T72_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T72_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T72_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T72_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T72_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T72>";
			}
			}

			var child72=0;
			var att72=0;
			if(this.T73_congruency!=null)
			child72++;
			if(this.T73_responsetime!=null)
			child72++;
			if(this.T73_accuracy!=null)
			child72++;
			if(child72>0 || att72>0){
				xmlTxt+="\n<cbat:T73";
			if(child72==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T73_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T73_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T73_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T73_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T73_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T73_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T73>";
			}
			}

			var child73=0;
			var att73=0;
			if(this.T74_congruency!=null)
			child73++;
			if(this.T74_responsetime!=null)
			child73++;
			if(this.T74_accuracy!=null)
			child73++;
			if(child73>0 || att73>0){
				xmlTxt+="\n<cbat:T74";
			if(child73==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T74_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T74_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T74_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T74_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T74_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T74_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T74>";
			}
			}

			var child74=0;
			var att74=0;
			if(this.T75_responsetime!=null)
			child74++;
			if(this.T75_congruency!=null)
			child74++;
			if(this.T75_accuracy!=null)
			child74++;
			if(child74>0 || att74>0){
				xmlTxt+="\n<cbat:T75";
			if(child74==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T75_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T75_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T75_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T75_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T75_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T75_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T75>";
			}
			}

			var child75=0;
			var att75=0;
			if(this.T76_congruency!=null)
			child75++;
			if(this.T76_responsetime!=null)
			child75++;
			if(this.T76_accuracy!=null)
			child75++;
			if(child75>0 || att75>0){
				xmlTxt+="\n<cbat:T76";
			if(child75==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T76_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T76_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T76_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T76_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T76_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T76_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T76>";
			}
			}

			var child76=0;
			var att76=0;
			if(this.T77_accuracy!=null)
			child76++;
			if(this.T77_responsetime!=null)
			child76++;
			if(this.T77_congruency!=null)
			child76++;
			if(child76>0 || att76>0){
				xmlTxt+="\n<cbat:T77";
			if(child76==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T77_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T77_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T77_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T77_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T77_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T77_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T77>";
			}
			}

			var child77=0;
			var att77=0;
			if(this.T78_responsetime!=null)
			child77++;
			if(this.T78_congruency!=null)
			child77++;
			if(this.T78_accuracy!=null)
			child77++;
			if(child77>0 || att77>0){
				xmlTxt+="\n<cbat:T78";
			if(child77==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T78_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T78_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T78_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T78_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T78_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T78_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T78>";
			}
			}

			var child78=0;
			var att78=0;
			if(this.T79_accuracy!=null)
			child78++;
			if(this.T79_responsetime!=null)
			child78++;
			if(this.T79_congruency!=null)
			child78++;
			if(child78>0 || att78>0){
				xmlTxt+="\n<cbat:T79";
			if(child78==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T79_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T79_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T79_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T79_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T79_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T79_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T79>";
			}
			}

			var child79=0;
			var att79=0;
			if(this.T80_responsetime!=null)
			child79++;
			if(this.T80_congruency!=null)
			child79++;
			if(this.T80_accuracy!=null)
			child79++;
			if(child79>0 || att79>0){
				xmlTxt+="\n<cbat:T80";
			if(child79==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T80_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T80_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T80_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T80_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T80_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T80_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T80>";
			}
			}

			var child80=0;
			var att80=0;
			if(this.T81_responsetime!=null)
			child80++;
			if(this.T81_accuracy!=null)
			child80++;
			if(this.T81_congruency!=null)
			child80++;
			if(child80>0 || att80>0){
				xmlTxt+="\n<cbat:T81";
			if(child80==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T81_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T81_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T81_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T81_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T81_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T81_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T81>";
			}
			}

			var child81=0;
			var att81=0;
			if(this.T82_responsetime!=null)
			child81++;
			if(this.T82_congruency!=null)
			child81++;
			if(this.T82_accuracy!=null)
			child81++;
			if(child81>0 || att81>0){
				xmlTxt+="\n<cbat:T82";
			if(child81==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T82_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T82_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T82_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T82_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T82_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T82_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T82>";
			}
			}

			var child82=0;
			var att82=0;
			if(this.T83_responsetime!=null)
			child82++;
			if(this.T83_congruency!=null)
			child82++;
			if(this.T83_accuracy!=null)
			child82++;
			if(child82>0 || att82>0){
				xmlTxt+="\n<cbat:T83";
			if(child82==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T83_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T83_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T83_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T83_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T83_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T83_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T83>";
			}
			}

			var child83=0;
			var att83=0;
			if(this.T84_accuracy!=null)
			child83++;
			if(this.T84_congruency!=null)
			child83++;
			if(this.T84_responsetime!=null)
			child83++;
			if(child83>0 || att83>0){
				xmlTxt+="\n<cbat:T84";
			if(child83==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T84_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T84_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T84_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T84_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T84_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T84_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T84>";
			}
			}

			var child84=0;
			var att84=0;
			if(this.T85_responsetime!=null)
			child84++;
			if(this.T85_accuracy!=null)
			child84++;
			if(this.T85_congruency!=null)
			child84++;
			if(child84>0 || att84>0){
				xmlTxt+="\n<cbat:T85";
			if(child84==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T85_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T85_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T85_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T85_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T85_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T85_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T85>";
			}
			}

			var child85=0;
			var att85=0;
			if(this.T86_responsetime!=null)
			child85++;
			if(this.T86_accuracy!=null)
			child85++;
			if(this.T86_congruency!=null)
			child85++;
			if(child85>0 || att85>0){
				xmlTxt+="\n<cbat:T86";
			if(child85==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T86_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T86_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T86_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T86_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T86_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T86_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T86>";
			}
			}

			var child86=0;
			var att86=0;
			if(this.T87_responsetime!=null)
			child86++;
			if(this.T87_congruency!=null)
			child86++;
			if(this.T87_accuracy!=null)
			child86++;
			if(child86>0 || att86>0){
				xmlTxt+="\n<cbat:T87";
			if(child86==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T87_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T87_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T87_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T87_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T87_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T87_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T87>";
			}
			}

			var child87=0;
			var att87=0;
			if(this.T88_accuracy!=null)
			child87++;
			if(this.T88_congruency!=null)
			child87++;
			if(this.T88_responsetime!=null)
			child87++;
			if(child87>0 || att87>0){
				xmlTxt+="\n<cbat:T88";
			if(child87==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T88_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T88_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T88_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T88_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T88_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T88_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T88>";
			}
			}

			var child88=0;
			var att88=0;
			if(this.T89_accuracy!=null)
			child88++;
			if(this.T89_congruency!=null)
			child88++;
			if(this.T89_responsetime!=null)
			child88++;
			if(child88>0 || att88>0){
				xmlTxt+="\n<cbat:T89";
			if(child88==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T89_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T89_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T89_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T89_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T89_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T89_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T89>";
			}
			}

			var child89=0;
			var att89=0;
			if(this.T90_responsetime!=null)
			child89++;
			if(this.T90_congruency!=null)
			child89++;
			if(this.T90_accuracy!=null)
			child89++;
			if(child89>0 || att89>0){
				xmlTxt+="\n<cbat:T90";
			if(child89==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T90_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T90_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T90_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T90_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T90_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T90_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T90>";
			}
			}

			var child90=0;
			var att90=0;
			if(this.T91_responsetime!=null)
			child90++;
			if(this.T91_accuracy!=null)
			child90++;
			if(this.T91_congruency!=null)
			child90++;
			if(child90>0 || att90>0){
				xmlTxt+="\n<cbat:T91";
			if(child90==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T91_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T91_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T91_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T91_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T91_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T91_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T91>";
			}
			}

			var child91=0;
			var att91=0;
			if(this.T92_accuracy!=null)
			child91++;
			if(this.T92_congruency!=null)
			child91++;
			if(this.T92_responsetime!=null)
			child91++;
			if(child91>0 || att91>0){
				xmlTxt+="\n<cbat:T92";
			if(child91==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T92_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T92_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T92_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T92_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T92_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T92_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T92>";
			}
			}

			var child92=0;
			var att92=0;
			if(this.T93_congruency!=null)
			child92++;
			if(this.T93_accuracy!=null)
			child92++;
			if(this.T93_responsetime!=null)
			child92++;
			if(child92>0 || att92>0){
				xmlTxt+="\n<cbat:T93";
			if(child92==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T93_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T93_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T93_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T93_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T93_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T93_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T93>";
			}
			}

			var child93=0;
			var att93=0;
			if(this.T94_accuracy!=null)
			child93++;
			if(this.T94_congruency!=null)
			child93++;
			if(this.T94_responsetime!=null)
			child93++;
			if(child93>0 || att93>0){
				xmlTxt+="\n<cbat:T94";
			if(child93==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T94_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T94_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T94_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T94_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T94_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T94_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T94>";
			}
			}

			var child94=0;
			var att94=0;
			if(this.T95_congruency!=null)
			child94++;
			if(this.T95_accuracy!=null)
			child94++;
			if(this.T95_responsetime!=null)
			child94++;
			if(child94>0 || att94>0){
				xmlTxt+="\n<cbat:T95";
			if(child94==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T95_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T95_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T95_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T95_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T95_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T95_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T95>";
			}
			}

			var child95=0;
			var att95=0;
			if(this.T96_congruency!=null)
			child95++;
			if(this.T96_accuracy!=null)
			child95++;
			if(this.T96_responsetime!=null)
			child95++;
			if(child95>0 || att95>0){
				xmlTxt+="\n<cbat:T96";
			if(child95==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T96_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T96_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T96_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T96_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T96_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T96_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T96>";
			}
			}

			var child96=0;
			var att96=0;
			if(this.T97_accuracy!=null)
			child96++;
			if(this.T97_responsetime!=null)
			child96++;
			if(this.T97_congruency!=null)
			child96++;
			if(child96>0 || att96>0){
				xmlTxt+="\n<cbat:T97";
			if(child96==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T97_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T97_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T97_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T97_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T97_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T97_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T97>";
			}
			}

			var child97=0;
			var att97=0;
			if(this.T98_congruency!=null)
			child97++;
			if(this.T98_responsetime!=null)
			child97++;
			if(this.T98_accuracy!=null)
			child97++;
			if(child97>0 || att97>0){
				xmlTxt+="\n<cbat:T98";
			if(child97==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T98_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T98_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T98_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T98_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T98_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T98_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T98>";
			}
			}

			var child98=0;
			var att98=0;
			if(this.T99_congruency!=null)
			child98++;
			if(this.T99_responsetime!=null)
			child98++;
			if(this.T99_accuracy!=null)
			child98++;
			if(child98>0 || att98>0){
				xmlTxt+="\n<cbat:T99";
			if(child98==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T99_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T99_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T99_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T99_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T99_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T99_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T99>";
			}
			}

			var child99=0;
			var att99=0;
			if(this.T100_responsetime!=null)
			child99++;
			if(this.T100_accuracy!=null)
			child99++;
			if(this.T100_congruency!=null)
			child99++;
			if(child99>0 || att99>0){
				xmlTxt+="\n<cbat:T100";
			if(child99==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T100_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T100_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T100_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T100_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T100_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T100_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T100>";
			}
			}

			var child100=0;
			var att100=0;
			if(this.T101_congruency!=null)
			child100++;
			if(this.T101_responsetime!=null)
			child100++;
			if(this.T101_accuracy!=null)
			child100++;
			if(child100>0 || att100>0){
				xmlTxt+="\n<cbat:T101";
			if(child100==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T101_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T101_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T101_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T101_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T101_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T101_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T101>";
			}
			}

			var child101=0;
			var att101=0;
			if(this.T102_responsetime!=null)
			child101++;
			if(this.T102_congruency!=null)
			child101++;
			if(this.T102_accuracy!=null)
			child101++;
			if(child101>0 || att101>0){
				xmlTxt+="\n<cbat:T102";
			if(child101==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T102_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T102_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T102_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T102_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T102_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T102_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T102>";
			}
			}

			var child102=0;
			var att102=0;
			if(this.T103_responsetime!=null)
			child102++;
			if(this.T103_accuracy!=null)
			child102++;
			if(this.T103_congruency!=null)
			child102++;
			if(child102>0 || att102>0){
				xmlTxt+="\n<cbat:T103";
			if(child102==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T103_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T103_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T103_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T103_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T103_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T103_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T103>";
			}
			}

			var child103=0;
			var att103=0;
			if(this.T104_congruency!=null)
			child103++;
			if(this.T104_responsetime!=null)
			child103++;
			if(this.T104_accuracy!=null)
			child103++;
			if(child103>0 || att103>0){
				xmlTxt+="\n<cbat:T104";
			if(child103==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T104_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T104_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T104_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T104_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T104_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T104_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T104>";
			}
			}

			var child104=0;
			var att104=0;
			if(this.T105_responsetime!=null)
			child104++;
			if(this.T105_congruency!=null)
			child104++;
			if(this.T105_accuracy!=null)
			child104++;
			if(child104>0 || att104>0){
				xmlTxt+="\n<cbat:T105";
			if(child104==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T105_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T105_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T105_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T105_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T105_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T105_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T105>";
			}
			}

			var child105=0;
			var att105=0;
			if(this.T106_responsetime!=null)
			child105++;
			if(this.T106_accuracy!=null)
			child105++;
			if(this.T106_congruency!=null)
			child105++;
			if(child105>0 || att105>0){
				xmlTxt+="\n<cbat:T106";
			if(child105==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T106_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T106_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T106_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T106_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T106_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T106_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T106>";
			}
			}

			var child106=0;
			var att106=0;
			if(this.T107_responsetime!=null)
			child106++;
			if(this.T107_accuracy!=null)
			child106++;
			if(this.T107_congruency!=null)
			child106++;
			if(child106>0 || att106>0){
				xmlTxt+="\n<cbat:T107";
			if(child106==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T107_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T107_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T107_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T107_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T107_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T107_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T107>";
			}
			}

			var child107=0;
			var att107=0;
			if(this.T108_responsetime!=null)
			child107++;
			if(this.T108_accuracy!=null)
			child107++;
			if(this.T108_congruency!=null)
			child107++;
			if(child107>0 || att107>0){
				xmlTxt+="\n<cbat:T108";
			if(child107==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T108_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T108_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T108_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T108_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T108_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T108_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T108>";
			}
			}

			var child108=0;
			var att108=0;
			if(this.T109_congruency!=null)
			child108++;
			if(this.T109_responsetime!=null)
			child108++;
			if(this.T109_accuracy!=null)
			child108++;
			if(child108>0 || att108>0){
				xmlTxt+="\n<cbat:T109";
			if(child108==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T109_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T109_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T109_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T109_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T109_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T109_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T109>";
			}
			}

			var child109=0;
			var att109=0;
			if(this.T110_responsetime!=null)
			child109++;
			if(this.T110_congruency!=null)
			child109++;
			if(this.T110_accuracy!=null)
			child109++;
			if(child109>0 || att109>0){
				xmlTxt+="\n<cbat:T110";
			if(child109==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T110_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T110_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T110_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T110_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T110_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T110_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T110>";
			}
			}

			var child110=0;
			var att110=0;
			if(this.T111_accuracy!=null)
			child110++;
			if(this.T111_congruency!=null)
			child110++;
			if(this.T111_responsetime!=null)
			child110++;
			if(child110>0 || att110>0){
				xmlTxt+="\n<cbat:T111";
			if(child110==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T111_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T111_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T111_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T111_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T111_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T111_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T111>";
			}
			}

			var child111=0;
			var att111=0;
			if(this.T112_congruency!=null)
			child111++;
			if(this.T112_responsetime!=null)
			child111++;
			if(this.T112_accuracy!=null)
			child111++;
			if(child111>0 || att111>0){
				xmlTxt+="\n<cbat:T112";
			if(child111==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T112_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T112_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T112_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T112_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T112_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T112_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T112>";
			}
			}

			var child112=0;
			var att112=0;
			if(this.T113_accuracy!=null)
			child112++;
			if(this.T113_congruency!=null)
			child112++;
			if(this.T113_responsetime!=null)
			child112++;
			if(child112>0 || att112>0){
				xmlTxt+="\n<cbat:T113";
			if(child112==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T113_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T113_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T113_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T113_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T113_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T113_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T113>";
			}
			}

			var child113=0;
			var att113=0;
			if(this.T114_accuracy!=null)
			child113++;
			if(this.T114_congruency!=null)
			child113++;
			if(this.T114_responsetime!=null)
			child113++;
			if(child113>0 || att113>0){
				xmlTxt+="\n<cbat:T114";
			if(child113==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T114_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T114_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T114_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T114_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T114_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T114_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T114>";
			}
			}

			var child114=0;
			var att114=0;
			if(this.T115_congruency!=null)
			child114++;
			if(this.T115_accuracy!=null)
			child114++;
			if(this.T115_responsetime!=null)
			child114++;
			if(child114>0 || att114>0){
				xmlTxt+="\n<cbat:T115";
			if(child114==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T115_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T115_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T115_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T115_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T115_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T115_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T115>";
			}
			}

			var child115=0;
			var att115=0;
			if(this.T116_accuracy!=null)
			child115++;
			if(this.T116_congruency!=null)
			child115++;
			if(this.T116_responsetime!=null)
			child115++;
			if(child115>0 || att115>0){
				xmlTxt+="\n<cbat:T116";
			if(child115==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T116_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T116_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T116_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T116_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T116_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T116_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T116>";
			}
			}

			var child116=0;
			var att116=0;
			if(this.T117_accuracy!=null)
			child116++;
			if(this.T117_responsetime!=null)
			child116++;
			if(this.T117_congruency!=null)
			child116++;
			if(child116>0 || att116>0){
				xmlTxt+="\n<cbat:T117";
			if(child116==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T117_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T117_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T117_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T117_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T117_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T117_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T117>";
			}
			}

			var child117=0;
			var att117=0;
			if(this.T118_congruency!=null)
			child117++;
			if(this.T118_accuracy!=null)
			child117++;
			if(this.T118_responsetime!=null)
			child117++;
			if(child117>0 || att117>0){
				xmlTxt+="\n<cbat:T118";
			if(child117==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T118_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T118_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T118_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T118_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T118_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T118_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T118>";
			}
			}

			var child118=0;
			var att118=0;
			if(this.T119_responsetime!=null)
			child118++;
			if(this.T119_accuracy!=null)
			child118++;
			if(this.T119_congruency!=null)
			child118++;
			if(child118>0 || att118>0){
				xmlTxt+="\n<cbat:T119";
			if(child118==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T119_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T119_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T119_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T119_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T119_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T119_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T119>";
			}
			}

			var child119=0;
			var att119=0;
			if(this.T120_accuracy!=null)
			child119++;
			if(this.T120_responsetime!=null)
			child119++;
			if(this.T120_congruency!=null)
			child119++;
			if(child119>0 || att119>0){
				xmlTxt+="\n<cbat:T120";
			if(child119==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.T120_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.T120_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.T120_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.T120_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
		if (this.T120_congruency!=null){
			xmlTxt+="\n<cbat:congruency";
			xmlTxt+=">";
			xmlTxt+=this.T120_congruency.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cbat:congruency>";
		}
				xmlTxt+="\n</cbat:T120>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
			if(this.T1_responsetime!=null) return true;
			if(this.T1_congruency!=null) return true;
			if(this.T1_accuracy!=null) return true;
			if(this.T2_responsetime!=null) return true;
			if(this.T2_congruency!=null) return true;
			if(this.T2_accuracy!=null) return true;
			if(this.T3_accuracy!=null) return true;
			if(this.T3_responsetime!=null) return true;
			if(this.T3_congruency!=null) return true;
			if(this.T4_accuracy!=null) return true;
			if(this.T4_responsetime!=null) return true;
			if(this.T4_congruency!=null) return true;
			if(this.T5_accuracy!=null) return true;
			if(this.T5_congruency!=null) return true;
			if(this.T5_responsetime!=null) return true;
			if(this.T6_responsetime!=null) return true;
			if(this.T6_accuracy!=null) return true;
			if(this.T6_congruency!=null) return true;
			if(this.T7_responsetime!=null) return true;
			if(this.T7_accuracy!=null) return true;
			if(this.T7_congruency!=null) return true;
			if(this.T8_responsetime!=null) return true;
			if(this.T8_congruency!=null) return true;
			if(this.T8_accuracy!=null) return true;
			if(this.T9_responsetime!=null) return true;
			if(this.T9_accuracy!=null) return true;
			if(this.T9_congruency!=null) return true;
			if(this.T10_congruency!=null) return true;
			if(this.T10_responsetime!=null) return true;
			if(this.T10_accuracy!=null) return true;
			if(this.T11_congruency!=null) return true;
			if(this.T11_responsetime!=null) return true;
			if(this.T11_accuracy!=null) return true;
			if(this.T12_responsetime!=null) return true;
			if(this.T12_accuracy!=null) return true;
			if(this.T12_congruency!=null) return true;
			if(this.T13_congruency!=null) return true;
			if(this.T13_responsetime!=null) return true;
			if(this.T13_accuracy!=null) return true;
			if(this.T14_responsetime!=null) return true;
			if(this.T14_congruency!=null) return true;
			if(this.T14_accuracy!=null) return true;
			if(this.T15_accuracy!=null) return true;
			if(this.T15_congruency!=null) return true;
			if(this.T15_responsetime!=null) return true;
			if(this.T16_accuracy!=null) return true;
			if(this.T16_responsetime!=null) return true;
			if(this.T16_congruency!=null) return true;
			if(this.T17_accuracy!=null) return true;
			if(this.T17_responsetime!=null) return true;
			if(this.T17_congruency!=null) return true;
			if(this.T18_accuracy!=null) return true;
			if(this.T18_responsetime!=null) return true;
			if(this.T18_congruency!=null) return true;
			if(this.T19_responsetime!=null) return true;
			if(this.T19_accuracy!=null) return true;
			if(this.T19_congruency!=null) return true;
			if(this.T20_responsetime!=null) return true;
			if(this.T20_congruency!=null) return true;
			if(this.T20_accuracy!=null) return true;
			if(this.T21_congruency!=null) return true;
			if(this.T21_responsetime!=null) return true;
			if(this.T21_accuracy!=null) return true;
			if(this.T22_accuracy!=null) return true;
			if(this.T22_responsetime!=null) return true;
			if(this.T22_congruency!=null) return true;
			if(this.T23_responsetime!=null) return true;
			if(this.T23_accuracy!=null) return true;
			if(this.T23_congruency!=null) return true;
			if(this.T24_responsetime!=null) return true;
			if(this.T24_accuracy!=null) return true;
			if(this.T24_congruency!=null) return true;
			if(this.T25_responsetime!=null) return true;
			if(this.T25_accuracy!=null) return true;
			if(this.T25_congruency!=null) return true;
			if(this.T26_responsetime!=null) return true;
			if(this.T26_congruency!=null) return true;
			if(this.T26_accuracy!=null) return true;
			if(this.T27_accuracy!=null) return true;
			if(this.T27_congruency!=null) return true;
			if(this.T27_responsetime!=null) return true;
			if(this.T28_accuracy!=null) return true;
			if(this.T28_congruency!=null) return true;
			if(this.T28_responsetime!=null) return true;
			if(this.T29_congruency!=null) return true;
			if(this.T29_accuracy!=null) return true;
			if(this.T29_responsetime!=null) return true;
			if(this.T30_responsetime!=null) return true;
			if(this.T30_accuracy!=null) return true;
			if(this.T30_congruency!=null) return true;
			if(this.T31_accuracy!=null) return true;
			if(this.T31_congruency!=null) return true;
			if(this.T31_responsetime!=null) return true;
			if(this.T32_congruency!=null) return true;
			if(this.T32_accuracy!=null) return true;
			if(this.T32_responsetime!=null) return true;
			if(this.T33_accuracy!=null) return true;
			if(this.T33_congruency!=null) return true;
			if(this.T33_responsetime!=null) return true;
			if(this.T34_accuracy!=null) return true;
			if(this.T34_responsetime!=null) return true;
			if(this.T34_congruency!=null) return true;
			if(this.T35_congruency!=null) return true;
			if(this.T35_accuracy!=null) return true;
			if(this.T35_responsetime!=null) return true;
			if(this.T36_congruency!=null) return true;
			if(this.T36_responsetime!=null) return true;
			if(this.T36_accuracy!=null) return true;
			if(this.T37_congruency!=null) return true;
			if(this.T37_responsetime!=null) return true;
			if(this.T37_accuracy!=null) return true;
			if(this.T38_congruency!=null) return true;
			if(this.T38_responsetime!=null) return true;
			if(this.T38_accuracy!=null) return true;
			if(this.T39_responsetime!=null) return true;
			if(this.T39_congruency!=null) return true;
			if(this.T39_accuracy!=null) return true;
			if(this.T40_congruency!=null) return true;
			if(this.T40_responsetime!=null) return true;
			if(this.T40_accuracy!=null) return true;
			if(this.T41_congruency!=null) return true;
			if(this.T41_responsetime!=null) return true;
			if(this.T41_accuracy!=null) return true;
			if(this.T42_responsetime!=null) return true;
			if(this.T42_accuracy!=null) return true;
			if(this.T42_congruency!=null) return true;
			if(this.T43_congruency!=null) return true;
			if(this.T43_responsetime!=null) return true;
			if(this.T43_accuracy!=null) return true;
			if(this.T44_responsetime!=null) return true;
			if(this.T44_congruency!=null) return true;
			if(this.T44_accuracy!=null) return true;
			if(this.T45_responsetime!=null) return true;
			if(this.T45_accuracy!=null) return true;
			if(this.T45_congruency!=null) return true;
			if(this.T46_congruency!=null) return true;
			if(this.T46_responsetime!=null) return true;
			if(this.T46_accuracy!=null) return true;
			if(this.T47_accuracy!=null) return true;
			if(this.T47_responsetime!=null) return true;
			if(this.T47_congruency!=null) return true;
			if(this.T48_congruency!=null) return true;
			if(this.T48_accuracy!=null) return true;
			if(this.T48_responsetime!=null) return true;
			if(this.T49_responsetime!=null) return true;
			if(this.T49_accuracy!=null) return true;
			if(this.T49_congruency!=null) return true;
			if(this.T50_responsetime!=null) return true;
			if(this.T50_congruency!=null) return true;
			if(this.T50_accuracy!=null) return true;
			if(this.T51_congruency!=null) return true;
			if(this.T51_responsetime!=null) return true;
			if(this.T51_accuracy!=null) return true;
			if(this.T52_accuracy!=null) return true;
			if(this.T52_responsetime!=null) return true;
			if(this.T52_congruency!=null) return true;
			if(this.T53_responsetime!=null) return true;
			if(this.T53_accuracy!=null) return true;
			if(this.T53_congruency!=null) return true;
			if(this.T54_accuracy!=null) return true;
			if(this.T54_responsetime!=null) return true;
			if(this.T54_congruency!=null) return true;
			if(this.T55_responsetime!=null) return true;
			if(this.T55_accuracy!=null) return true;
			if(this.T55_congruency!=null) return true;
			if(this.T56_responsetime!=null) return true;
			if(this.T56_accuracy!=null) return true;
			if(this.T56_congruency!=null) return true;
			if(this.T57_congruency!=null) return true;
			if(this.T57_accuracy!=null) return true;
			if(this.T57_responsetime!=null) return true;
			if(this.T58_accuracy!=null) return true;
			if(this.T58_congruency!=null) return true;
			if(this.T58_responsetime!=null) return true;
			if(this.T59_congruency!=null) return true;
			if(this.T59_accuracy!=null) return true;
			if(this.T59_responsetime!=null) return true;
			if(this.T60_responsetime!=null) return true;
			if(this.T60_accuracy!=null) return true;
			if(this.T60_congruency!=null) return true;
			if(this.T61_responsetime!=null) return true;
			if(this.T61_accuracy!=null) return true;
			if(this.T61_congruency!=null) return true;
			if(this.T62_congruency!=null) return true;
			if(this.T62_accuracy!=null) return true;
			if(this.T62_responsetime!=null) return true;
			if(this.T63_accuracy!=null) return true;
			if(this.T63_congruency!=null) return true;
			if(this.T63_responsetime!=null) return true;
			if(this.T64_accuracy!=null) return true;
			if(this.T64_congruency!=null) return true;
			if(this.T64_responsetime!=null) return true;
			if(this.T65_congruency!=null) return true;
			if(this.T65_accuracy!=null) return true;
			if(this.T65_responsetime!=null) return true;
			if(this.T66_accuracy!=null) return true;
			if(this.T66_congruency!=null) return true;
			if(this.T66_responsetime!=null) return true;
			if(this.T67_accuracy!=null) return true;
			if(this.T67_responsetime!=null) return true;
			if(this.T67_congruency!=null) return true;
			if(this.T68_congruency!=null) return true;
			if(this.T68_responsetime!=null) return true;
			if(this.T68_accuracy!=null) return true;
			if(this.T69_congruency!=null) return true;
			if(this.T69_responsetime!=null) return true;
			if(this.T69_accuracy!=null) return true;
			if(this.T70_congruency!=null) return true;
			if(this.T70_accuracy!=null) return true;
			if(this.T70_responsetime!=null) return true;
			if(this.T71_congruency!=null) return true;
			if(this.T71_accuracy!=null) return true;
			if(this.T71_responsetime!=null) return true;
			if(this.T72_responsetime!=null) return true;
			if(this.T72_accuracy!=null) return true;
			if(this.T72_congruency!=null) return true;
			if(this.T73_congruency!=null) return true;
			if(this.T73_responsetime!=null) return true;
			if(this.T73_accuracy!=null) return true;
			if(this.T74_congruency!=null) return true;
			if(this.T74_responsetime!=null) return true;
			if(this.T74_accuracy!=null) return true;
			if(this.T75_responsetime!=null) return true;
			if(this.T75_congruency!=null) return true;
			if(this.T75_accuracy!=null) return true;
			if(this.T76_congruency!=null) return true;
			if(this.T76_responsetime!=null) return true;
			if(this.T76_accuracy!=null) return true;
			if(this.T77_accuracy!=null) return true;
			if(this.T77_responsetime!=null) return true;
			if(this.T77_congruency!=null) return true;
			if(this.T78_responsetime!=null) return true;
			if(this.T78_congruency!=null) return true;
			if(this.T78_accuracy!=null) return true;
			if(this.T79_accuracy!=null) return true;
			if(this.T79_responsetime!=null) return true;
			if(this.T79_congruency!=null) return true;
			if(this.T80_responsetime!=null) return true;
			if(this.T80_congruency!=null) return true;
			if(this.T80_accuracy!=null) return true;
			if(this.T81_responsetime!=null) return true;
			if(this.T81_accuracy!=null) return true;
			if(this.T81_congruency!=null) return true;
			if(this.T82_responsetime!=null) return true;
			if(this.T82_congruency!=null) return true;
			if(this.T82_accuracy!=null) return true;
			if(this.T83_responsetime!=null) return true;
			if(this.T83_congruency!=null) return true;
			if(this.T83_accuracy!=null) return true;
			if(this.T84_accuracy!=null) return true;
			if(this.T84_congruency!=null) return true;
			if(this.T84_responsetime!=null) return true;
			if(this.T85_responsetime!=null) return true;
			if(this.T85_accuracy!=null) return true;
			if(this.T85_congruency!=null) return true;
			if(this.T86_responsetime!=null) return true;
			if(this.T86_accuracy!=null) return true;
			if(this.T86_congruency!=null) return true;
			if(this.T87_responsetime!=null) return true;
			if(this.T87_congruency!=null) return true;
			if(this.T87_accuracy!=null) return true;
			if(this.T88_accuracy!=null) return true;
			if(this.T88_congruency!=null) return true;
			if(this.T88_responsetime!=null) return true;
			if(this.T89_accuracy!=null) return true;
			if(this.T89_congruency!=null) return true;
			if(this.T89_responsetime!=null) return true;
			if(this.T90_responsetime!=null) return true;
			if(this.T90_congruency!=null) return true;
			if(this.T90_accuracy!=null) return true;
			if(this.T91_responsetime!=null) return true;
			if(this.T91_accuracy!=null) return true;
			if(this.T91_congruency!=null) return true;
			if(this.T92_accuracy!=null) return true;
			if(this.T92_congruency!=null) return true;
			if(this.T92_responsetime!=null) return true;
			if(this.T93_congruency!=null) return true;
			if(this.T93_accuracy!=null) return true;
			if(this.T93_responsetime!=null) return true;
			if(this.T94_accuracy!=null) return true;
			if(this.T94_congruency!=null) return true;
			if(this.T94_responsetime!=null) return true;
			if(this.T95_congruency!=null) return true;
			if(this.T95_accuracy!=null) return true;
			if(this.T95_responsetime!=null) return true;
			if(this.T96_congruency!=null) return true;
			if(this.T96_accuracy!=null) return true;
			if(this.T96_responsetime!=null) return true;
			if(this.T97_accuracy!=null) return true;
			if(this.T97_responsetime!=null) return true;
			if(this.T97_congruency!=null) return true;
			if(this.T98_congruency!=null) return true;
			if(this.T98_responsetime!=null) return true;
			if(this.T98_accuracy!=null) return true;
			if(this.T99_congruency!=null) return true;
			if(this.T99_responsetime!=null) return true;
			if(this.T99_accuracy!=null) return true;
			if(this.T100_responsetime!=null) return true;
			if(this.T100_accuracy!=null) return true;
			if(this.T100_congruency!=null) return true;
			if(this.T101_congruency!=null) return true;
			if(this.T101_responsetime!=null) return true;
			if(this.T101_accuracy!=null) return true;
			if(this.T102_responsetime!=null) return true;
			if(this.T102_congruency!=null) return true;
			if(this.T102_accuracy!=null) return true;
			if(this.T103_responsetime!=null) return true;
			if(this.T103_accuracy!=null) return true;
			if(this.T103_congruency!=null) return true;
			if(this.T104_congruency!=null) return true;
			if(this.T104_responsetime!=null) return true;
			if(this.T104_accuracy!=null) return true;
			if(this.T105_responsetime!=null) return true;
			if(this.T105_congruency!=null) return true;
			if(this.T105_accuracy!=null) return true;
			if(this.T106_responsetime!=null) return true;
			if(this.T106_accuracy!=null) return true;
			if(this.T106_congruency!=null) return true;
			if(this.T107_responsetime!=null) return true;
			if(this.T107_accuracy!=null) return true;
			if(this.T107_congruency!=null) return true;
			if(this.T108_responsetime!=null) return true;
			if(this.T108_accuracy!=null) return true;
			if(this.T108_congruency!=null) return true;
			if(this.T109_congruency!=null) return true;
			if(this.T109_responsetime!=null) return true;
			if(this.T109_accuracy!=null) return true;
			if(this.T110_responsetime!=null) return true;
			if(this.T110_congruency!=null) return true;
			if(this.T110_accuracy!=null) return true;
			if(this.T111_accuracy!=null) return true;
			if(this.T111_congruency!=null) return true;
			if(this.T111_responsetime!=null) return true;
			if(this.T112_congruency!=null) return true;
			if(this.T112_responsetime!=null) return true;
			if(this.T112_accuracy!=null) return true;
			if(this.T113_accuracy!=null) return true;
			if(this.T113_congruency!=null) return true;
			if(this.T113_responsetime!=null) return true;
			if(this.T114_accuracy!=null) return true;
			if(this.T114_congruency!=null) return true;
			if(this.T114_responsetime!=null) return true;
			if(this.T115_congruency!=null) return true;
			if(this.T115_accuracy!=null) return true;
			if(this.T115_responsetime!=null) return true;
			if(this.T116_accuracy!=null) return true;
			if(this.T116_congruency!=null) return true;
			if(this.T116_responsetime!=null) return true;
			if(this.T117_accuracy!=null) return true;
			if(this.T117_responsetime!=null) return true;
			if(this.T117_congruency!=null) return true;
			if(this.T118_congruency!=null) return true;
			if(this.T118_accuracy!=null) return true;
			if(this.T118_responsetime!=null) return true;
			if(this.T119_responsetime!=null) return true;
			if(this.T119_accuracy!=null) return true;
			if(this.T119_congruency!=null) return true;
			if(this.T120_accuracy!=null) return true;
			if(this.T120_responsetime!=null) return true;
			if(this.T120_congruency!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
