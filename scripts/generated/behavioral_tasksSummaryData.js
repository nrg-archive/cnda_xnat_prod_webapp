/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function behavioral_tasksSummaryData(){
this.xsiType="behavioral:tasksSummaryData";

	this.getSchemaElementName=function(){
		return "tasksSummaryData";
	}

	this.getFullSchemaElementName=function(){
		return "behavioral:tasksSummaryData";
	}
this.extension=dynamicJSLoad('xnat_mrAssessorData','generated/xnat_mrAssessorData.js');
	this.Tasks_task =new Array();

	function getTasks_task() {
		return this.Tasks_task;
	}
	this.getTasks_task=getTasks_task;


	function addTasks_task(v){
		this.Tasks_task.push(v);
	}
	this.addTasks_task=addTasks_task;
	this.Tasks_statisticsacrosstasks =null;
	function getTasks_statisticsacrosstasks() {
		return this.Tasks_statisticsacrosstasks;
	}
	this.getTasks_statisticsacrosstasks=getTasks_statisticsacrosstasks;


	function setTasks_statisticsacrosstasks(v){
		this.Tasks_statisticsacrosstasks =v;
	}
	this.setTasks_statisticsacrosstasks=setTasks_statisticsacrosstasks;

	this.Tasks_statisticsacrosstasks_TasksStatisticsacrosstasksBehavioralStatisticsId=null;


	function getTasks_statisticsacrosstasks_TasksStatisticsacrosstasksBehavioralStatisticsId(){
		return this.Tasks_statisticsacrosstasks_TasksStatisticsacrosstasksBehavioralStatisticsId;
	}
	this.getTasks_statisticsacrosstasks_TasksStatisticsacrosstasksBehavioralStatisticsId=getTasks_statisticsacrosstasks_TasksStatisticsacrosstasksBehavioralStatisticsId;


	function setTasks_statisticsacrosstasks_TasksStatisticsacrosstasksBehavioralStatisticsId(v){
		this.Tasks_statisticsacrosstasks_TasksStatisticsacrosstasksBehavioralStatisticsId=v;
	}
	this.setTasks_statisticsacrosstasks_TasksStatisticsacrosstasksBehavioralStatisticsId=setTasks_statisticsacrosstasks_TasksStatisticsacrosstasksBehavioralStatisticsId;
	this.Tasks_statisticsacrossruns =null;
	function getTasks_statisticsacrossruns() {
		return this.Tasks_statisticsacrossruns;
	}
	this.getTasks_statisticsacrossruns=getTasks_statisticsacrossruns;


	function setTasks_statisticsacrossruns(v){
		this.Tasks_statisticsacrossruns =v;
	}
	this.setTasks_statisticsacrossruns=setTasks_statisticsacrossruns;

	this.Tasks_statisticsacrossruns_TasksStatisticsacrossrunsBehavioralStatisticsId=null;


	function getTasks_statisticsacrossruns_TasksStatisticsacrossrunsBehavioralStatisticsId(){
		return this.Tasks_statisticsacrossruns_TasksStatisticsacrossrunsBehavioralStatisticsId;
	}
	this.getTasks_statisticsacrossruns_TasksStatisticsacrossrunsBehavioralStatisticsId=getTasks_statisticsacrossruns_TasksStatisticsacrossrunsBehavioralStatisticsId;


	function setTasks_statisticsacrossruns_TasksStatisticsacrossrunsBehavioralStatisticsId(v){
		this.Tasks_statisticsacrossruns_TasksStatisticsacrossrunsBehavioralStatisticsId=v;
	}
	this.setTasks_statisticsacrossruns_TasksStatisticsacrossrunsBehavioralStatisticsId=setTasks_statisticsacrossruns_TasksStatisticsacrossrunsBehavioralStatisticsId;

	this.TaskType=null;


	function getTaskType() {
		return this.TaskType;
	}
	this.getTaskType=getTaskType;


	function setTaskType(v){
		this.TaskType=v;
	}
	this.setTaskType=setTaskType;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="mrAssessorData"){
				return this.Mrassessordata ;
			} else 
			if(xmlPath.startsWith("mrAssessorData")){
				xmlPath=xmlPath.substring(14);
				if(xmlPath=="")return this.Mrassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Mrassessordata!=undefined)return this.Mrassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="Tasks/task"){
				return this.Tasks_task ;
			} else 
			if(xmlPath.startsWith("Tasks/task")){
				xmlPath=xmlPath.substring(10);
				if(xmlPath=="")return this.Tasks_task ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Tasks_task.length;whereCount++){

					var tempValue=this.Tasks_task[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Tasks_task[whereCount]);

					}

				}
				}else{

				whereArray=this.Tasks_task;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="Tasks/StatisticsAcrossTasks"){
				return this.Tasks_statisticsacrosstasks ;
			} else 
			if(xmlPath.startsWith("Tasks/StatisticsAcrossTasks")){
				xmlPath=xmlPath.substring(27);
				if(xmlPath=="")return this.Tasks_statisticsacrosstasks ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Tasks_statisticsacrosstasks!=undefined)return this.Tasks_statisticsacrosstasks.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="Tasks/StatisticsAcrossRuns"){
				return this.Tasks_statisticsacrossruns ;
			} else 
			if(xmlPath.startsWith("Tasks/StatisticsAcrossRuns")){
				xmlPath=xmlPath.substring(26);
				if(xmlPath=="")return this.Tasks_statisticsacrossruns ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Tasks_statisticsacrossruns!=undefined)return this.Tasks_statisticsacrossruns.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="task_type"){
				return this.TaskType ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="mrAssessorData"){
				this.Mrassessordata=value;
			} else 
			if(xmlPath.startsWith("mrAssessorData")){
				xmlPath=xmlPath.substring(14);
				if(xmlPath=="")return this.Mrassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Mrassessordata!=undefined){
					this.Mrassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Mrassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Mrassessordata= instanciateObject("xnat:mrAssessorData");//omUtils.js
						}
						if(options && options.where)this.Mrassessordata.setProperty(options.where.field,options.where.value);
						this.Mrassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="Tasks/task"){
				this.Tasks_task=value;
			} else 
			if(xmlPath.startsWith("Tasks/task")){
				xmlPath=xmlPath.substring(10);
				if(xmlPath=="")return this.Tasks_task ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Tasks_task.length;whereCount++){

					var tempValue=this.Tasks_task[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Tasks_task[whereCount]);

					}

				}
				}else{

				whereArray=this.Tasks_task;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("behavioral:tasksSummaryData_task");//omUtils.js
					}
					this.addTasks_task(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="Tasks/StatisticsAcrossTasks"){
				this.Tasks_statisticsacrosstasks=value;
			} else 
			if(xmlPath.startsWith("Tasks/StatisticsAcrossTasks")){
				xmlPath=xmlPath.substring(27);
				if(xmlPath=="")return this.Tasks_statisticsacrosstasks ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Tasks_statisticsacrosstasks!=undefined){
					this.Tasks_statisticsacrosstasks.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Tasks_statisticsacrosstasks= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Tasks_statisticsacrosstasks= instanciateObject("behavioral:statistics");//omUtils.js
						}
						if(options && options.where)this.Tasks_statisticsacrosstasks.setProperty(options.where.field,options.where.value);
						this.Tasks_statisticsacrosstasks.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="Tasks/StatisticsAcrossRuns"){
				this.Tasks_statisticsacrossruns=value;
			} else 
			if(xmlPath.startsWith("Tasks/StatisticsAcrossRuns")){
				xmlPath=xmlPath.substring(26);
				if(xmlPath=="")return this.Tasks_statisticsacrossruns ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Tasks_statisticsacrossruns!=undefined){
					this.Tasks_statisticsacrossruns.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Tasks_statisticsacrossruns= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Tasks_statisticsacrossruns= instanciateObject("behavioral:statistics");//omUtils.js
						}
						if(options && options.where)this.Tasks_statisticsacrossruns.setProperty(options.where.field,options.where.value);
						this.Tasks_statisticsacrossruns.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="task_type"){
				this.TaskType=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="Tasks/task"){
			this.addTasks_task(v);
		}else if (xmlPath=="Tasks/StatisticsAcrossTasks"){
			this.setTasks_statisticsacrosstasks(v);
		}else if (xmlPath=="Tasks/StatisticsAcrossRuns"){
			this.setTasks_statisticsacrossruns(v);
		}
		else{
			this.extension.setReferenceField(xmlPath,v);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="Tasks/task"){
			return "http://nrg.wustl.edu/behavioral:tasksSummaryData_task";
		}else if (xmlPath=="Tasks/StatisticsAcrossTasks"){
			return "http://nrg.wustl.edu/behavioral:statistics";
		}else if (xmlPath=="Tasks/StatisticsAcrossRuns"){
			return "http://nrg.wustl.edu/behavioral:statistics";
		}
		else{
			return this.extension.getReferenceFieldName(xmlPath);
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="Tasks/task"){
			return "field_multi_reference";
		}else if (xmlPath=="Tasks/StatisticsAcrossTasks"){
			return "field_single_reference";
		}else if (xmlPath=="Tasks/StatisticsAcrossRuns"){
			return "field_single_reference";
		}else if (xmlPath=="task_type"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<behavioral:TasksSummary";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</behavioral:TasksSummary>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		if (this.TaskType!=null)
			attTxt+=" task_type=\"" +this.TaskType +"\"";
		//NOT REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
			var child0=0;
			var att0=0;
			if(this.Tasks_statisticsacrosstasks!=null)
			child0++;
			if(this.Tasks_statisticsacrossruns!=null)
			child0++;
			child0+=this.Tasks_task.length;
			if(child0>0 || att0>0){
				xmlTxt+="\n<behavioral:Tasks";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var Tasks_taskCOUNT=0;Tasks_taskCOUNT<this.Tasks_task.length;Tasks_taskCOUNT++){
			xmlTxt +="\n<behavioral:task";
			xmlTxt +=this.Tasks_task[Tasks_taskCOUNT].getXMLAtts();
			if(this.Tasks_task[Tasks_taskCOUNT].xsiType!="behavioral:tasksSummaryData_task"){
				xmlTxt+=" xsi:type=\"" + this.Tasks_task[Tasks_taskCOUNT].xsiType + "\"";
			}
			if (this.Tasks_task[Tasks_taskCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Tasks_task[Tasks_taskCOUNT].getXMLBody(preventComments);
					xmlTxt+="</behavioral:task>";
			}else {xmlTxt+="/>";}
		}
		if (this.Tasks_statisticsacrosstasks!=null){
			xmlTxt+="\n<behavioral:StatisticsAcrossTasks";
			xmlTxt+=this.Tasks_statisticsacrosstasks.getXMLAtts();
			if(this.Tasks_statisticsacrosstasks.xsiType!="behavioral:statistics"){
				xmlTxt+=" xsi:type=\"" + this.Tasks_statisticsacrosstasks.xsiType + "\"";
			}
			if (this.Tasks_statisticsacrosstasks.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Tasks_statisticsacrosstasks.getXMLBody(preventComments);
				xmlTxt+="</behavioral:StatisticsAcrossTasks>";
			}else {xmlTxt+="/>";}
		}
		//NOT REQUIRED

		if (this.Tasks_statisticsacrossruns!=null){
			xmlTxt+="\n<behavioral:StatisticsAcrossRuns";
			xmlTxt+=this.Tasks_statisticsacrossruns.getXMLAtts();
			if(this.Tasks_statisticsacrossruns.xsiType!="behavioral:statistics"){
				xmlTxt+=" xsi:type=\"" + this.Tasks_statisticsacrossruns.xsiType + "\"";
			}
			if (this.Tasks_statisticsacrossruns.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Tasks_statisticsacrossruns.getXMLBody(preventComments);
				xmlTxt+="</behavioral:StatisticsAcrossRuns>";
			}else {xmlTxt+="/>";}
		}
		//NOT REQUIRED

				xmlTxt+="\n</behavioral:Tasks>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
			if(this.Tasks_statisticsacrosstasks!=null) return true;
			if(this.Tasks_statisticsacrossruns!=null) return true;
			if(this.Tasks_task.length>0)return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
