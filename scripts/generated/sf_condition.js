/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function sf_condition(){
this.xsiType="sf:condition";

	this.getSchemaElementName=function(){
		return "condition";
	}

	this.getFullSchemaElementName=function(){
		return "sf:condition";
	}

	this.Conditionstatus=null;


	function getConditionstatus() {
		return this.Conditionstatus;
	}
	this.getConditionstatus=getConditionstatus;


	function setConditionstatus(v){
		this.Conditionstatus=v;
	}
	this.setConditionstatus=setConditionstatus;

	this.Bodysystem=null;


	function getBodysystem() {
		return this.Bodysystem;
	}
	this.getBodysystem=getBodysystem;


	function setBodysystem(v){
		this.Bodysystem=v;
	}
	this.setBodysystem=setBodysystem;

	this.Condition=null;


	function getCondition() {
		return this.Condition;
	}
	this.getCondition=getCondition;


	function setCondition(v){
		this.Condition=v;
	}
	this.setCondition=setCondition;

	this.Conditionnotes=null;


	function getConditionnotes() {
		return this.Conditionnotes;
	}
	this.getConditionnotes=getConditionnotes;


	function setConditionnotes(v){
		this.Conditionnotes=v;
	}
	this.setConditionnotes=setConditionnotes;

	this.Diagnosisdate=null;


	function getDiagnosisdate() {
		return this.Diagnosisdate;
	}
	this.getDiagnosisdate=getDiagnosisdate;


	function setDiagnosisdate(v){
		this.Diagnosisdate=v;
	}
	this.setDiagnosisdate=setDiagnosisdate;

	this.Diagnosisdatedaynotreported=null;


	function getDiagnosisdatedaynotreported() {
		return this.Diagnosisdatedaynotreported;
	}
	this.getDiagnosisdatedaynotreported=getDiagnosisdatedaynotreported;


	function setDiagnosisdatedaynotreported(v){
		this.Diagnosisdatedaynotreported=v;
	}
	this.setDiagnosisdatedaynotreported=setDiagnosisdatedaynotreported;


	this.isDiagnosisdatedaynotreported=function(defaultValue) {
		if(this.Diagnosisdatedaynotreported==null)return defaultValue;
		if(this.Diagnosisdatedaynotreported=="1" || this.Diagnosisdatedaynotreported==true)return true;
		return false;
	}

	this.Diagnosisdatemonthnotreported=null;


	function getDiagnosisdatemonthnotreported() {
		return this.Diagnosisdatemonthnotreported;
	}
	this.getDiagnosisdatemonthnotreported=getDiagnosisdatemonthnotreported;


	function setDiagnosisdatemonthnotreported(v){
		this.Diagnosisdatemonthnotreported=v;
	}
	this.setDiagnosisdatemonthnotreported=setDiagnosisdatemonthnotreported;


	this.isDiagnosisdatemonthnotreported=function(defaultValue) {
		if(this.Diagnosisdatemonthnotreported==null)return defaultValue;
		if(this.Diagnosisdatemonthnotreported=="1" || this.Diagnosisdatemonthnotreported==true)return true;
		return false;
	}

	this.Diagnosisdateyearnotreported=null;


	function getDiagnosisdateyearnotreported() {
		return this.Diagnosisdateyearnotreported;
	}
	this.getDiagnosisdateyearnotreported=getDiagnosisdateyearnotreported;


	function setDiagnosisdateyearnotreported(v){
		this.Diagnosisdateyearnotreported=v;
	}
	this.setDiagnosisdateyearnotreported=setDiagnosisdateyearnotreported;


	this.isDiagnosisdateyearnotreported=function(defaultValue) {
		if(this.Diagnosisdateyearnotreported==null)return defaultValue;
		if(this.Diagnosisdateyearnotreported=="1" || this.Diagnosisdateyearnotreported==true)return true;
		return false;
	}

	this.SfConditionId=null;


	function getSfConditionId() {
		return this.SfConditionId;
	}
	this.getSfConditionId=getSfConditionId;


	function setSfConditionId(v){
		this.SfConditionId=v;
	}
	this.setSfConditionId=setSfConditionId;

	this.conditions_condition_sf_medical_id_fk=null;


	this.getconditions_condition_sf_medical_id=function() {
		return this.conditions_condition_sf_medical_id_fk;
	}


	this.setconditions_condition_sf_medical_id=function(v){
		this.conditions_condition_sf_medical_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="conditionStatus"){
				return this.Conditionstatus ;
			} else 
			if(xmlPath=="bodySystem"){
				return this.Bodysystem ;
			} else 
			if(xmlPath=="condition"){
				return this.Condition ;
			} else 
			if(xmlPath=="conditionNotes"){
				return this.Conditionnotes ;
			} else 
			if(xmlPath=="diagnosisDate"){
				return this.Diagnosisdate ;
			} else 
			if(xmlPath=="diagnosisDateDayNotReported"){
				return this.Diagnosisdatedaynotreported ;
			} else 
			if(xmlPath=="diagnosisDateMonthNotReported"){
				return this.Diagnosisdatemonthnotreported ;
			} else 
			if(xmlPath=="diagnosisDateYearNotReported"){
				return this.Diagnosisdateyearnotreported ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="sf_condition_id"){
				return this.SfConditionId ;
			} else 
			if(xmlPath=="conditions_condition_sf_medical_id"){
				return this.conditions_condition_sf_medical_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="conditionStatus"){
				this.Conditionstatus=value;
			} else 
			if(xmlPath=="bodySystem"){
				this.Bodysystem=value;
			} else 
			if(xmlPath=="condition"){
				this.Condition=value;
			} else 
			if(xmlPath=="conditionNotes"){
				this.Conditionnotes=value;
			} else 
			if(xmlPath=="diagnosisDate"){
				this.Diagnosisdate=value;
			} else 
			if(xmlPath=="diagnosisDateDayNotReported"){
				this.Diagnosisdatedaynotreported=value;
			} else 
			if(xmlPath=="diagnosisDateMonthNotReported"){
				this.Diagnosisdatemonthnotreported=value;
			} else 
			if(xmlPath=="diagnosisDateYearNotReported"){
				this.Diagnosisdateyearnotreported=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="sf_condition_id"){
				this.SfConditionId=value;
			} else 
			if(xmlPath=="conditions_condition_sf_medical_id"){
				this.conditions_condition_sf_medical_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="conditionStatus"){
			return "field_data";
		}else if (xmlPath=="bodySystem"){
			return "field_data";
		}else if (xmlPath=="condition"){
			return "field_data";
		}else if (xmlPath=="conditionNotes"){
			return "field_LONG_DATA";
		}else if (xmlPath=="diagnosisDate"){
			return "field_data";
		}else if (xmlPath=="diagnosisDateDayNotReported"){
			return "field_data";
		}else if (xmlPath=="diagnosisDateMonthNotReported"){
			return "field_data";
		}else if (xmlPath=="diagnosisDateYearNotReported"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<sf:condition";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</sf:condition>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.SfConditionId!=null){
				if(hiddenCount++>0)str+=",";
				str+="sf_condition_id=\"" + this.SfConditionId + "\"";
			}
			if(this.conditions_condition_sf_medical_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="conditions_condition_sf_medical_id=\"" + this.conditions_condition_sf_medical_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Conditionstatus!=null){
			xmlTxt+="\n<sf:conditionStatus";
			xmlTxt+=">";
			xmlTxt+=this.Conditionstatus.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:conditionStatus>";
		}
		if (this.Bodysystem!=null){
			xmlTxt+="\n<sf:bodySystem";
			xmlTxt+=">";
			xmlTxt+=this.Bodysystem.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:bodySystem>";
		}
		if (this.Condition!=null){
			xmlTxt+="\n<sf:condition";
			xmlTxt+=">";
			xmlTxt+=this.Condition.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:condition>";
		}
		if (this.Conditionnotes!=null){
			xmlTxt+="\n<sf:conditionNotes";
			xmlTxt+=">";
			xmlTxt+=this.Conditionnotes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:conditionNotes>";
		}
		if (this.Diagnosisdate!=null){
			xmlTxt+="\n<sf:diagnosisDate";
			xmlTxt+=">";
			xmlTxt+=this.Diagnosisdate;
			xmlTxt+="</sf:diagnosisDate>";
		}
		if (this.Diagnosisdatedaynotreported!=null){
			xmlTxt+="\n<sf:diagnosisDateDayNotReported";
			xmlTxt+=">";
			xmlTxt+=this.Diagnosisdatedaynotreported;
			xmlTxt+="</sf:diagnosisDateDayNotReported>";
		}
		if (this.Diagnosisdatemonthnotreported!=null){
			xmlTxt+="\n<sf:diagnosisDateMonthNotReported";
			xmlTxt+=">";
			xmlTxt+=this.Diagnosisdatemonthnotreported;
			xmlTxt+="</sf:diagnosisDateMonthNotReported>";
		}
		if (this.Diagnosisdateyearnotreported!=null){
			xmlTxt+="\n<sf:diagnosisDateYearNotReported";
			xmlTxt+=">";
			xmlTxt+=this.Diagnosisdateyearnotreported;
			xmlTxt+="</sf:diagnosisDateYearNotReported>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.SfConditionId!=null) return true;
			if (this.conditions_condition_sf_medical_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Conditionstatus!=null) return true;
		if (this.Bodysystem!=null) return true;
		if (this.Condition!=null) return true;
		if (this.Conditionnotes!=null) return true;
		if (this.Diagnosisdate!=null) return true;
		if (this.Diagnosisdatedaynotreported!=null) return true;
		if (this.Diagnosisdatemonthnotreported!=null) return true;
		if (this.Diagnosisdateyearnotreported!=null) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
