/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function dian_dermfibcolData(){
this.xsiType="dian:dermfibcolData";

	this.getSchemaElementName=function(){
		return "dermfibcolData";
	}

	this.getFullSchemaElementName=function(){
		return "dian:dermfibcolData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Tissuedt=null;


	function getTissuedt() {
		return this.Tissuedt;
	}
	this.getTissuedt=getTissuedt;


	function setTissuedt(v){
		this.Tissuedt=v;
	}
	this.setTissuedt=setTissuedt;

	this.Tissuetime=null;


	function getTissuetime() {
		return this.Tissuetime;
	}
	this.getTissuetime=getTissuetime;


	function setTissuetime(v){
		this.Tissuetime=v;
	}
	this.setTissuetime=setTissuetime;

	this.Dfcclinnm=null;


	function getDfcclinnm() {
		return this.Dfcclinnm;
	}
	this.getDfcclinnm=getDfcclinnm;


	function setDfcclinnm(v){
		this.Dfcclinnm=v;
	}
	this.setDfcclinnm=setDfcclinnm;

	this.Dfcship=null;


	function getDfcship() {
		return this.Dfcship;
	}
	this.getDfcship=getDfcship;


	function setDfcship(v){
		this.Dfcship=v;
	}
	this.setDfcship=setDfcship;

	this.Dfcshipdt=null;


	function getDfcshipdt() {
		return this.Dfcshipdt;
	}
	this.getDfcshipdt=getDfcshipdt;


	function setDfcshipdt(v){
		this.Dfcshipdt=v;
	}
	this.setDfcshipdt=setDfcshipdt;

	this.Dfctrackno=null;


	function getDfctrackno() {
		return this.Dfctrackno;
	}
	this.getDfctrackno=getDfctrackno;


	function setDfctrackno(v){
		this.Dfctrackno=v;
	}
	this.setDfctrackno=setDfctrackno;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="TISSUEDT"){
				return this.Tissuedt ;
			} else 
			if(xmlPath=="TISSUETIME"){
				return this.Tissuetime ;
			} else 
			if(xmlPath=="DFCCLINNM"){
				return this.Dfcclinnm ;
			} else 
			if(xmlPath=="DFCSHIP"){
				return this.Dfcship ;
			} else 
			if(xmlPath=="DFCSHIPDT"){
				return this.Dfcshipdt ;
			} else 
			if(xmlPath=="DFCTRACKNO"){
				return this.Dfctrackno ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="TISSUEDT"){
				this.Tissuedt=value;
			} else 
			if(xmlPath=="TISSUETIME"){
				this.Tissuetime=value;
			} else 
			if(xmlPath=="DFCCLINNM"){
				this.Dfcclinnm=value;
			} else 
			if(xmlPath=="DFCSHIP"){
				this.Dfcship=value;
			} else 
			if(xmlPath=="DFCSHIPDT"){
				this.Dfcshipdt=value;
			} else 
			if(xmlPath=="DFCTRACKNO"){
				this.Dfctrackno=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="TISSUEDT"){
			return "field_data";
		}else if (xmlPath=="TISSUETIME"){
			return "field_data";
		}else if (xmlPath=="DFCCLINNM"){
			return "field_data";
		}else if (xmlPath=="DFCSHIP"){
			return "field_data";
		}else if (xmlPath=="DFCSHIPDT"){
			return "field_data";
		}else if (xmlPath=="DFCTRACKNO"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<dian:DERMFIBCOL";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</dian:DERMFIBCOL>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Tissuedt!=null){
			xmlTxt+="\n<dian:TISSUEDT";
			xmlTxt+=">";
			xmlTxt+=this.Tissuedt;
			xmlTxt+="</dian:TISSUEDT>";
		}
		if (this.Tissuetime!=null){
			xmlTxt+="\n<dian:TISSUETIME";
			xmlTxt+=">";
			xmlTxt+=this.Tissuetime;
			xmlTxt+="</dian:TISSUETIME>";
		}
		if (this.Dfcclinnm!=null){
			xmlTxt+="\n<dian:DFCCLINNM";
			xmlTxt+=">";
			xmlTxt+=this.Dfcclinnm.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:DFCCLINNM>";
		}
		if (this.Dfcship!=null){
			xmlTxt+="\n<dian:DFCSHIP";
			xmlTxt+=">";
			xmlTxt+=this.Dfcship.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:DFCSHIP>";
		}
		if (this.Dfcshipdt!=null){
			xmlTxt+="\n<dian:DFCSHIPDT";
			xmlTxt+=">";
			xmlTxt+=this.Dfcshipdt;
			xmlTxt+="</dian:DFCSHIPDT>";
		}
		if (this.Dfctrackno!=null){
			xmlTxt+="\n<dian:DFCTRACKNO";
			xmlTxt+=">";
			xmlTxt+=this.Dfctrackno.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:DFCTRACKNO>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Tissuedt!=null) return true;
		if (this.Tissuetime!=null) return true;
		if (this.Dfcclinnm!=null) return true;
		if (this.Dfcship!=null) return true;
		if (this.Dfcshipdt!=null) return true;
		if (this.Dfctrackno!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
