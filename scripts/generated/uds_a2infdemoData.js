/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:04 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function uds_a2infdemoData(){
this.xsiType="uds:a2infdemoData";

	this.getSchemaElementName=function(){
		return "a2infdemoData";
	}

	this.getFullSchemaElementName=function(){
		return "uds:a2infdemoData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Examdate=null;


	function getExamdate() {
		return this.Examdate;
	}
	this.getExamdate=getExamdate;


	function setExamdate(v){
		this.Examdate=v;
	}
	this.setExamdate=setExamdate;

	this.Inbirmo=null;


	function getInbirmo() {
		return this.Inbirmo;
	}
	this.getInbirmo=getInbirmo;


	function setInbirmo(v){
		this.Inbirmo=v;
	}
	this.setInbirmo=setInbirmo;

	this.Inbiryr=null;


	function getInbiryr() {
		return this.Inbiryr;
	}
	this.getInbiryr=getInbiryr;


	function setInbiryr(v){
		this.Inbiryr=v;
	}
	this.setInbiryr=setInbiryr;

	this.Insex=null;


	function getInsex() {
		return this.Insex;
	}
	this.getInsex=getInsex;


	function setInsex(v){
		this.Insex=v;
	}
	this.setInsex=setInsex;

	this.Newinf=null;


	function getNewinf() {
		return this.Newinf;
	}
	this.getNewinf=getNewinf;


	function setNewinf(v){
		this.Newinf=v;
	}
	this.setNewinf=setNewinf;

	this.Inhisp=null;


	function getInhisp() {
		return this.Inhisp;
	}
	this.getInhisp=getInhisp;


	function setInhisp(v){
		this.Inhisp=v;
	}
	this.setInhisp=setInhisp;

	this.Inhispor=null;


	function getInhispor() {
		return this.Inhispor;
	}
	this.getInhispor=getInhispor;


	function setInhispor(v){
		this.Inhispor=v;
	}
	this.setInhispor=setInhispor;

	this.Inhispox=null;


	function getInhispox() {
		return this.Inhispox;
	}
	this.getInhispox=getInhispox;


	function setInhispox(v){
		this.Inhispox=v;
	}
	this.setInhispox=setInhispox;

	this.Inrace=null;


	function getInrace() {
		return this.Inrace;
	}
	this.getInrace=getInrace;


	function setInrace(v){
		this.Inrace=v;
	}
	this.setInrace=setInrace;

	this.Inracex=null;


	function getInracex() {
		return this.Inracex;
	}
	this.getInracex=getInracex;


	function setInracex(v){
		this.Inracex=v;
	}
	this.setInracex=setInracex;

	this.Inrasec=null;


	function getInrasec() {
		return this.Inrasec;
	}
	this.getInrasec=getInrasec;


	function setInrasec(v){
		this.Inrasec=v;
	}
	this.setInrasec=setInrasec;

	this.Inrasecx=null;


	function getInrasecx() {
		return this.Inrasecx;
	}
	this.getInrasecx=getInrasecx;


	function setInrasecx(v){
		this.Inrasecx=v;
	}
	this.setInrasecx=setInrasecx;

	this.Inrater=null;


	function getInrater() {
		return this.Inrater;
	}
	this.getInrater=getInrater;


	function setInrater(v){
		this.Inrater=v;
	}
	this.setInrater=setInrater;

	this.Inraterx=null;


	function getInraterx() {
		return this.Inraterx;
	}
	this.getInraterx=getInraterx;


	function setInraterx(v){
		this.Inraterx=v;
	}
	this.setInraterx=setInraterx;

	this.Ineduc=null;


	function getIneduc() {
		return this.Ineduc;
	}
	this.getIneduc=getIneduc;


	function setIneduc(v){
		this.Ineduc=v;
	}
	this.setIneduc=setIneduc;

	this.Inrelto=null;


	function getInrelto() {
		return this.Inrelto;
	}
	this.getInrelto=getInrelto;


	function setInrelto(v){
		this.Inrelto=v;
	}
	this.setInrelto=setInrelto;

	this.Inreltox=null;


	function getInreltox() {
		return this.Inreltox;
	}
	this.getInreltox=getInreltox;


	function setInreltox(v){
		this.Inreltox=v;
	}
	this.setInreltox=setInreltox;

	this.Inlivwth=null;


	function getInlivwth() {
		return this.Inlivwth;
	}
	this.getInlivwth=getInlivwth;


	function setInlivwth(v){
		this.Inlivwth=v;
	}
	this.setInlivwth=setInlivwth;

	this.Invisits=null;


	function getInvisits() {
		return this.Invisits;
	}
	this.getInvisits=getInvisits;


	function setInvisits(v){
		this.Invisits=v;
	}
	this.setInvisits=setInvisits;

	this.Incalls=null;


	function getIncalls() {
		return this.Incalls;
	}
	this.getIncalls=getIncalls;


	function setIncalls(v){
		this.Incalls=v;
	}
	this.setIncalls=setIncalls;

	this.Inrely=null;


	function getInrely() {
		return this.Inrely;
	}
	this.getInrely=getInrely;


	function setInrely(v){
		this.Inrely=v;
	}
	this.setInrely=setInrely;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="EXAMDATE"){
				return this.Examdate ;
			} else 
			if(xmlPath=="INBIRMO"){
				return this.Inbirmo ;
			} else 
			if(xmlPath=="INBIRYR"){
				return this.Inbiryr ;
			} else 
			if(xmlPath=="INSEX"){
				return this.Insex ;
			} else 
			if(xmlPath=="NEWINF"){
				return this.Newinf ;
			} else 
			if(xmlPath=="INHISP"){
				return this.Inhisp ;
			} else 
			if(xmlPath=="INHISPOR"){
				return this.Inhispor ;
			} else 
			if(xmlPath=="INHISPOX"){
				return this.Inhispox ;
			} else 
			if(xmlPath=="INRACE"){
				return this.Inrace ;
			} else 
			if(xmlPath=="INRACEX"){
				return this.Inracex ;
			} else 
			if(xmlPath=="INRASEC"){
				return this.Inrasec ;
			} else 
			if(xmlPath=="INRASECX"){
				return this.Inrasecx ;
			} else 
			if(xmlPath=="INRATER"){
				return this.Inrater ;
			} else 
			if(xmlPath=="INRATERX"){
				return this.Inraterx ;
			} else 
			if(xmlPath=="INEDUC"){
				return this.Ineduc ;
			} else 
			if(xmlPath=="INRELTO"){
				return this.Inrelto ;
			} else 
			if(xmlPath=="INRELTOX"){
				return this.Inreltox ;
			} else 
			if(xmlPath=="INLIVWTH"){
				return this.Inlivwth ;
			} else 
			if(xmlPath=="INVISITS"){
				return this.Invisits ;
			} else 
			if(xmlPath=="INCALLS"){
				return this.Incalls ;
			} else 
			if(xmlPath=="INRELY"){
				return this.Inrely ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="EXAMDATE"){
				this.Examdate=value;
			} else 
			if(xmlPath=="INBIRMO"){
				this.Inbirmo=value;
			} else 
			if(xmlPath=="INBIRYR"){
				this.Inbiryr=value;
			} else 
			if(xmlPath=="INSEX"){
				this.Insex=value;
			} else 
			if(xmlPath=="NEWINF"){
				this.Newinf=value;
			} else 
			if(xmlPath=="INHISP"){
				this.Inhisp=value;
			} else 
			if(xmlPath=="INHISPOR"){
				this.Inhispor=value;
			} else 
			if(xmlPath=="INHISPOX"){
				this.Inhispox=value;
			} else 
			if(xmlPath=="INRACE"){
				this.Inrace=value;
			} else 
			if(xmlPath=="INRACEX"){
				this.Inracex=value;
			} else 
			if(xmlPath=="INRASEC"){
				this.Inrasec=value;
			} else 
			if(xmlPath=="INRASECX"){
				this.Inrasecx=value;
			} else 
			if(xmlPath=="INRATER"){
				this.Inrater=value;
			} else 
			if(xmlPath=="INRATERX"){
				this.Inraterx=value;
			} else 
			if(xmlPath=="INEDUC"){
				this.Ineduc=value;
			} else 
			if(xmlPath=="INRELTO"){
				this.Inrelto=value;
			} else 
			if(xmlPath=="INRELTOX"){
				this.Inreltox=value;
			} else 
			if(xmlPath=="INLIVWTH"){
				this.Inlivwth=value;
			} else 
			if(xmlPath=="INVISITS"){
				this.Invisits=value;
			} else 
			if(xmlPath=="INCALLS"){
				this.Incalls=value;
			} else 
			if(xmlPath=="INRELY"){
				this.Inrely=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="EXAMDATE"){
			return "field_data";
		}else if (xmlPath=="INBIRMO"){
			return "field_data";
		}else if (xmlPath=="INBIRYR"){
			return "field_data";
		}else if (xmlPath=="INSEX"){
			return "field_data";
		}else if (xmlPath=="NEWINF"){
			return "field_data";
		}else if (xmlPath=="INHISP"){
			return "field_data";
		}else if (xmlPath=="INHISPOR"){
			return "field_data";
		}else if (xmlPath=="INHISPOX"){
			return "field_data";
		}else if (xmlPath=="INRACE"){
			return "field_data";
		}else if (xmlPath=="INRACEX"){
			return "field_data";
		}else if (xmlPath=="INRASEC"){
			return "field_data";
		}else if (xmlPath=="INRASECX"){
			return "field_data";
		}else if (xmlPath=="INRATER"){
			return "field_data";
		}else if (xmlPath=="INRATERX"){
			return "field_data";
		}else if (xmlPath=="INEDUC"){
			return "field_data";
		}else if (xmlPath=="INRELTO"){
			return "field_data";
		}else if (xmlPath=="INRELTOX"){
			return "field_data";
		}else if (xmlPath=="INLIVWTH"){
			return "field_data";
		}else if (xmlPath=="INVISITS"){
			return "field_data";
		}else if (xmlPath=="INCALLS"){
			return "field_data";
		}else if (xmlPath=="INRELY"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<uds:A2INFDEMO";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</uds:A2INFDEMO>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Examdate!=null){
			xmlTxt+="\n<uds:EXAMDATE";
			xmlTxt+=">";
			xmlTxt+=this.Examdate;
			xmlTxt+="</uds:EXAMDATE>";
		}
		if (this.Inbirmo!=null){
			xmlTxt+="\n<uds:INBIRMO";
			xmlTxt+=">";
			xmlTxt+=this.Inbirmo;
			xmlTxt+="</uds:INBIRMO>";
		}
		if (this.Inbiryr!=null){
			xmlTxt+="\n<uds:INBIRYR";
			xmlTxt+=">";
			xmlTxt+=this.Inbiryr;
			xmlTxt+="</uds:INBIRYR>";
		}
		if (this.Insex!=null){
			xmlTxt+="\n<uds:INSEX";
			xmlTxt+=">";
			xmlTxt+=this.Insex;
			xmlTxt+="</uds:INSEX>";
		}
		if (this.Newinf!=null){
			xmlTxt+="\n<uds:NEWINF";
			xmlTxt+=">";
			xmlTxt+=this.Newinf;
			xmlTxt+="</uds:NEWINF>";
		}
		if (this.Inhisp!=null){
			xmlTxt+="\n<uds:INHISP";
			xmlTxt+=">";
			xmlTxt+=this.Inhisp;
			xmlTxt+="</uds:INHISP>";
		}
		if (this.Inhispor!=null){
			xmlTxt+="\n<uds:INHISPOR";
			xmlTxt+=">";
			xmlTxt+=this.Inhispor;
			xmlTxt+="</uds:INHISPOR>";
		}
		if (this.Inhispox!=null){
			xmlTxt+="\n<uds:INHISPOX";
			xmlTxt+=">";
			xmlTxt+=this.Inhispox.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:INHISPOX>";
		}
		if (this.Inrace!=null){
			xmlTxt+="\n<uds:INRACE";
			xmlTxt+=">";
			xmlTxt+=this.Inrace;
			xmlTxt+="</uds:INRACE>";
		}
		if (this.Inracex!=null){
			xmlTxt+="\n<uds:INRACEX";
			xmlTxt+=">";
			xmlTxt+=this.Inracex.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:INRACEX>";
		}
		if (this.Inrasec!=null){
			xmlTxt+="\n<uds:INRASEC";
			xmlTxt+=">";
			xmlTxt+=this.Inrasec;
			xmlTxt+="</uds:INRASEC>";
		}
		if (this.Inrasecx!=null){
			xmlTxt+="\n<uds:INRASECX";
			xmlTxt+=">";
			xmlTxt+=this.Inrasecx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:INRASECX>";
		}
		if (this.Inrater!=null){
			xmlTxt+="\n<uds:INRATER";
			xmlTxt+=">";
			xmlTxt+=this.Inrater;
			xmlTxt+="</uds:INRATER>";
		}
		if (this.Inraterx!=null){
			xmlTxt+="\n<uds:INRATERX";
			xmlTxt+=">";
			xmlTxt+=this.Inraterx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:INRATERX>";
		}
		if (this.Ineduc!=null){
			xmlTxt+="\n<uds:INEDUC";
			xmlTxt+=">";
			xmlTxt+=this.Ineduc;
			xmlTxt+="</uds:INEDUC>";
		}
		if (this.Inrelto!=null){
			xmlTxt+="\n<uds:INRELTO";
			xmlTxt+=">";
			xmlTxt+=this.Inrelto;
			xmlTxt+="</uds:INRELTO>";
		}
		if (this.Inreltox!=null){
			xmlTxt+="\n<uds:INRELTOX";
			xmlTxt+=">";
			xmlTxt+=this.Inreltox.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:INRELTOX>";
		}
		if (this.Inlivwth!=null){
			xmlTxt+="\n<uds:INLIVWTH";
			xmlTxt+=">";
			xmlTxt+=this.Inlivwth;
			xmlTxt+="</uds:INLIVWTH>";
		}
		if (this.Invisits!=null){
			xmlTxt+="\n<uds:INVISITS";
			xmlTxt+=">";
			xmlTxt+=this.Invisits;
			xmlTxt+="</uds:INVISITS>";
		}
		if (this.Incalls!=null){
			xmlTxt+="\n<uds:INCALLS";
			xmlTxt+=">";
			xmlTxt+=this.Incalls;
			xmlTxt+="</uds:INCALLS>";
		}
		if (this.Inrely!=null){
			xmlTxt+="\n<uds:INRELY";
			xmlTxt+=">";
			xmlTxt+=this.Inrely;
			xmlTxt+="</uds:INRELY>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Examdate!=null) return true;
		if (this.Inbirmo!=null) return true;
		if (this.Inbiryr!=null) return true;
		if (this.Insex!=null) return true;
		if (this.Newinf!=null) return true;
		if (this.Inhisp!=null) return true;
		if (this.Inhispor!=null) return true;
		if (this.Inhispox!=null) return true;
		if (this.Inrace!=null) return true;
		if (this.Inracex!=null) return true;
		if (this.Inrasec!=null) return true;
		if (this.Inrasecx!=null) return true;
		if (this.Inrater!=null) return true;
		if (this.Inraterx!=null) return true;
		if (this.Ineduc!=null) return true;
		if (this.Inrelto!=null) return true;
		if (this.Inreltox!=null) return true;
		if (this.Inlivwth!=null) return true;
		if (this.Invisits!=null) return true;
		if (this.Incalls!=null) return true;
		if (this.Inrely!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
