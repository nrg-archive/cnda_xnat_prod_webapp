/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function dian_feedbackData(){
this.xsiType="dian:feedbackData";

	this.getSchemaElementName=function(){
		return "feedbackData";
	}

	this.getFullSchemaElementName=function(){
		return "dian:feedbackData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Autopsy=null;


	function getAutopsy() {
		return this.Autopsy;
	}
	this.getAutopsy=getAutopsy;


	function setAutopsy(v){
		this.Autopsy=v;
	}
	this.setAutopsy=setAutopsy;

	this.Decision=null;


	function getDecision() {
		return this.Decision;
	}
	this.getDecision=getDecision;


	function setDecision(v){
		this.Decision=v;
	}
	this.setDecision=setDecision;

	this.Refuse=null;


	function getRefuse() {
		return this.Refuse;
	}
	this.getRefuse=getRefuse;


	function setRefuse(v){
		this.Refuse=v;
	}
	this.setRefuse=setRefuse;

	this.Refuseoth=null;


	function getRefuseoth() {
		return this.Refuseoth;
	}
	this.getRefuseoth=getRefuseoth;


	function setRefuseoth(v){
		this.Refuseoth=v;
	}
	this.setRefuseoth=setRefuseoth;

	this.Clinfeed=null;


	function getClinfeed() {
		return this.Clinfeed;
	}
	this.getClinfeed=getClinfeed;


	function setClinfeed(v){
		this.Clinfeed=v;
	}
	this.setClinfeed=setClinfeed;

	this.Provider=null;


	function getProvider() {
		return this.Provider;
	}
	this.getProvider=getProvider;


	function setProvider(v){
		this.Provider=v;
	}
	this.setProvider=setProvider;

	this.Provoth=null;


	function getProvoth() {
		return this.Provoth;
	}
	this.getProvoth=getProvoth;


	function setProvoth(v){
		this.Provoth=v;
	}
	this.setProvoth=setProvoth;

	this.Present=null;


	function getPresent() {
		return this.Present;
	}
	this.getPresent=getPresent;


	function setPresent(v){
		this.Present=v;
	}
	this.setPresent=setPresent;

	this.Presoth=null;


	function getPresoth() {
		return this.Presoth;
	}
	this.getPresoth=getPresoth;


	function setPresoth(v){
		this.Presoth=v;
	}
	this.setPresoth=setPresoth;

	this.Feedto=null;


	function getFeedto() {
		return this.Feedto;
	}
	this.getFeedto=getFeedto;


	function setFeedto(v){
		this.Feedto=v;
	}
	this.setFeedto=setFeedto;

	this.Toother=null;


	function getToother() {
		return this.Toother;
	}
	this.getToother=getToother;


	function setToother(v){
		this.Toother=v;
	}
	this.setToother=setToother;

	this.Topics=null;


	function getTopics() {
		return this.Topics;
	}
	this.getTopics=getTopics;


	function setTopics(v){
		this.Topics=v;
	}
	this.setTopics=setTopics;

	this.Medspec=null;


	function getMedspec() {
		return this.Medspec;
	}
	this.getMedspec=getMedspec;


	function setMedspec(v){
		this.Medspec=v;
	}
	this.setMedspec=setMedspec;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="AUTOPSY"){
				return this.Autopsy ;
			} else 
			if(xmlPath=="DECISION"){
				return this.Decision ;
			} else 
			if(xmlPath=="REFUSE"){
				return this.Refuse ;
			} else 
			if(xmlPath=="REFUSEOTH"){
				return this.Refuseoth ;
			} else 
			if(xmlPath=="CLINFEED"){
				return this.Clinfeed ;
			} else 
			if(xmlPath=="PROVIDER"){
				return this.Provider ;
			} else 
			if(xmlPath=="PROVOTH"){
				return this.Provoth ;
			} else 
			if(xmlPath=="PRESENT"){
				return this.Present ;
			} else 
			if(xmlPath=="PRESOTH"){
				return this.Presoth ;
			} else 
			if(xmlPath=="FEEDTO"){
				return this.Feedto ;
			} else 
			if(xmlPath=="TOOTHER"){
				return this.Toother ;
			} else 
			if(xmlPath=="TOPICS"){
				return this.Topics ;
			} else 
			if(xmlPath=="MEDSPEC"){
				return this.Medspec ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="AUTOPSY"){
				this.Autopsy=value;
			} else 
			if(xmlPath=="DECISION"){
				this.Decision=value;
			} else 
			if(xmlPath=="REFUSE"){
				this.Refuse=value;
			} else 
			if(xmlPath=="REFUSEOTH"){
				this.Refuseoth=value;
			} else 
			if(xmlPath=="CLINFEED"){
				this.Clinfeed=value;
			} else 
			if(xmlPath=="PROVIDER"){
				this.Provider=value;
			} else 
			if(xmlPath=="PROVOTH"){
				this.Provoth=value;
			} else 
			if(xmlPath=="PRESENT"){
				this.Present=value;
			} else 
			if(xmlPath=="PRESOTH"){
				this.Presoth=value;
			} else 
			if(xmlPath=="FEEDTO"){
				this.Feedto=value;
			} else 
			if(xmlPath=="TOOTHER"){
				this.Toother=value;
			} else 
			if(xmlPath=="TOPICS"){
				this.Topics=value;
			} else 
			if(xmlPath=="MEDSPEC"){
				this.Medspec=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="AUTOPSY"){
			return "field_data";
		}else if (xmlPath=="DECISION"){
			return "field_data";
		}else if (xmlPath=="REFUSE"){
			return "field_data";
		}else if (xmlPath=="REFUSEOTH"){
			return "field_LONG_DATA";
		}else if (xmlPath=="CLINFEED"){
			return "field_data";
		}else if (xmlPath=="PROVIDER"){
			return "field_data";
		}else if (xmlPath=="PROVOTH"){
			return "field_data";
		}else if (xmlPath=="PRESENT"){
			return "field_data";
		}else if (xmlPath=="PRESOTH"){
			return "field_data";
		}else if (xmlPath=="FEEDTO"){
			return "field_data";
		}else if (xmlPath=="TOOTHER"){
			return "field_data";
		}else if (xmlPath=="TOPICS"){
			return "field_data";
		}else if (xmlPath=="MEDSPEC"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<dian:FEEDBACK";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</dian:FEEDBACK>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Autopsy!=null){
			xmlTxt+="\n<dian:AUTOPSY";
			xmlTxt+=">";
			xmlTxt+=this.Autopsy;
			xmlTxt+="</dian:AUTOPSY>";
		}
		if (this.Decision!=null){
			xmlTxt+="\n<dian:DECISION";
			xmlTxt+=">";
			xmlTxt+=this.Decision;
			xmlTxt+="</dian:DECISION>";
		}
		if (this.Refuse!=null){
			xmlTxt+="\n<dian:REFUSE";
			xmlTxt+=">";
			xmlTxt+=this.Refuse.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:REFUSE>";
		}
		if (this.Refuseoth!=null){
			xmlTxt+="\n<dian:REFUSEOTH";
			xmlTxt+=">";
			xmlTxt+=this.Refuseoth.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:REFUSEOTH>";
		}
		else{
			xmlTxt+="\n<dian:REFUSEOTH";
			xmlTxt+="/>";
		}

		if (this.Clinfeed!=null){
			xmlTxt+="\n<dian:CLINFEED";
			xmlTxt+=">";
			xmlTxt+=this.Clinfeed;
			xmlTxt+="</dian:CLINFEED>";
		}
		if (this.Provider!=null){
			xmlTxt+="\n<dian:PROVIDER";
			xmlTxt+=">";
			xmlTxt+=this.Provider.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:PROVIDER>";
		}
		if (this.Provoth!=null){
			xmlTxt+="\n<dian:PROVOTH";
			xmlTxt+=">";
			xmlTxt+=this.Provoth.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:PROVOTH>";
		}
		if (this.Present!=null){
			xmlTxt+="\n<dian:PRESENT";
			xmlTxt+=">";
			xmlTxt+=this.Present.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:PRESENT>";
		}
		if (this.Presoth!=null){
			xmlTxt+="\n<dian:PRESOTH";
			xmlTxt+=">";
			xmlTxt+=this.Presoth.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:PRESOTH>";
		}
		if (this.Feedto!=null){
			xmlTxt+="\n<dian:FEEDTO";
			xmlTxt+=">";
			xmlTxt+=this.Feedto.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:FEEDTO>";
		}
		if (this.Toother!=null){
			xmlTxt+="\n<dian:TOOTHER";
			xmlTxt+=">";
			xmlTxt+=this.Toother.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:TOOTHER>";
		}
		if (this.Topics!=null){
			xmlTxt+="\n<dian:TOPICS";
			xmlTxt+=">";
			xmlTxt+=this.Topics.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:TOPICS>";
		}
		if (this.Medspec!=null){
			xmlTxt+="\n<dian:MEDSPEC";
			xmlTxt+=">";
			xmlTxt+=this.Medspec.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:MEDSPEC>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Autopsy!=null) return true;
		if (this.Decision!=null) return true;
		if (this.Refuse!=null) return true;
		if (this.Refuseoth!=null) return true;
		return true;//REQUIRED REFUSEOTH
	}
}
