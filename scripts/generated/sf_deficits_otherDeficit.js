/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function sf_deficits_otherDeficit(){
this.xsiType="sf:deficits_otherDeficit";

	this.getSchemaElementName=function(){
		return "deficits_otherDeficit";
	}

	this.getFullSchemaElementName=function(){
		return "sf:deficits_otherDeficit";
	}

	this.Otherdeficitname=null;


	function getOtherdeficitname() {
		return this.Otherdeficitname;
	}
	this.getOtherdeficitname=getOtherdeficitname;


	function setOtherdeficitname(v){
		this.Otherdeficitname=v;
	}
	this.setOtherdeficitname=setOtherdeficitname;

	this.Otherdeficitpresent=null;


	function getOtherdeficitpresent() {
		return this.Otherdeficitpresent;
	}
	this.getOtherdeficitpresent=getOtherdeficitpresent;


	function setOtherdeficitpresent(v){
		this.Otherdeficitpresent=v;
	}
	this.setOtherdeficitpresent=setOtherdeficitpresent;


	this.isOtherdeficitpresent=function(defaultValue) {
		if(this.Otherdeficitpresent==null)return defaultValue;
		if(this.Otherdeficitpresent=="1" || this.Otherdeficitpresent==true)return true;
		return false;
	}

	this.SfDeficitsOtherdeficitId=null;


	function getSfDeficitsOtherdeficitId() {
		return this.SfDeficitsOtherdeficitId;
	}
	this.getSfDeficitsOtherdeficitId=getSfDeficitsOtherdeficitId;


	function setSfDeficitsOtherdeficitId(v){
		this.SfDeficitsOtherdeficitId=v;
	}
	this.setSfDeficitsOtherdeficitId=setSfDeficitsOtherdeficitId;

	this.otherdeficits_otherdeficit_sf_d_sf_deficits_id_fk=null;


	this.getotherdeficits_otherdeficit_sf_d_sf_deficits_id=function() {
		return this.otherdeficits_otherdeficit_sf_d_sf_deficits_id_fk;
	}


	this.setotherdeficits_otherdeficit_sf_d_sf_deficits_id=function(v){
		this.otherdeficits_otherdeficit_sf_d_sf_deficits_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="otherDeficitName"){
				return this.Otherdeficitname ;
			} else 
			if(xmlPath=="otherDeficitPresent"){
				return this.Otherdeficitpresent ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="sf_deficits_otherDeficit_id"){
				return this.SfDeficitsOtherdeficitId ;
			} else 
			if(xmlPath=="otherdeficits_otherdeficit_sf_d_sf_deficits_id"){
				return this.otherdeficits_otherdeficit_sf_d_sf_deficits_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="otherDeficitName"){
				this.Otherdeficitname=value;
			} else 
			if(xmlPath=="otherDeficitPresent"){
				this.Otherdeficitpresent=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="sf_deficits_otherDeficit_id"){
				this.SfDeficitsOtherdeficitId=value;
			} else 
			if(xmlPath=="otherdeficits_otherdeficit_sf_d_sf_deficits_id"){
				this.otherdeficits_otherdeficit_sf_d_sf_deficits_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="otherDeficitName"){
			return "field_data";
		}else if (xmlPath=="otherDeficitPresent"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<sf:deficits_otherDeficit";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</sf:deficits_otherDeficit>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.SfDeficitsOtherdeficitId!=null){
				if(hiddenCount++>0)str+=",";
				str+="sf_deficits_otherDeficit_id=\"" + this.SfDeficitsOtherdeficitId + "\"";
			}
			if(this.otherdeficits_otherdeficit_sf_d_sf_deficits_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="otherdeficits_otherdeficit_sf_d_sf_deficits_id=\"" + this.otherdeficits_otherdeficit_sf_d_sf_deficits_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Otherdeficitname!=null){
			xmlTxt+="\n<sf:otherDeficitName";
			xmlTxt+=">";
			xmlTxt+=this.Otherdeficitname.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:otherDeficitName>";
		}
		if (this.Otherdeficitpresent!=null){
			xmlTxt+="\n<sf:otherDeficitPresent";
			xmlTxt+=">";
			xmlTxt+=this.Otherdeficitpresent;
			xmlTxt+="</sf:otherDeficitPresent>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.SfDeficitsOtherdeficitId!=null) return true;
			if (this.otherdeficits_otherdeficit_sf_d_sf_deficits_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Otherdeficitname!=null) return true;
		if (this.Otherdeficitpresent!=null) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
