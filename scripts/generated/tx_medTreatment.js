/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function tx_medTreatment(){
this.xsiType="tx:medTreatment";

	this.getSchemaElementName=function(){
		return "medTreatment";
	}

	this.getFullSchemaElementName=function(){
		return "tx:medTreatment";
	}

	this.Code=null;


	function getCode() {
		return this.Code;
	}
	this.getCode=getCode;


	function setCode(v){
		this.Code=v;
	}
	this.setCode=setCode;

	this.Codetype=null;


	function getCodetype() {
		return this.Codetype;
	}
	this.getCodetype=getCodetype;


	function setCodetype(v){
		this.Codetype=v;
	}
	this.setCodetype=setCodetype;

	this.Name=null;


	function getName() {
		return this.Name;
	}
	this.getName=getName;


	function setName(v){
		this.Name=v;
	}
	this.setName=setName;

	this.Status=null;


	function getStatus() {
		return this.Status;
	}
	this.getStatus=getStatus;


	function setStatus(v){
		this.Status=v;
	}
	this.setStatus=setStatus;

	this.Doseamount=null;


	function getDoseamount() {
		return this.Doseamount;
	}
	this.getDoseamount=getDoseamount;


	function setDoseamount(v){
		this.Doseamount=v;
	}
	this.setDoseamount=setDoseamount;

	this.Doseunit=null;


	function getDoseunit() {
		return this.Doseunit;
	}
	this.getDoseunit=getDoseunit;


	function setDoseunit(v){
		this.Doseunit=v;
	}
	this.setDoseunit=setDoseunit;

	this.Doseschedule=null;


	function getDoseschedule() {
		return this.Doseschedule;
	}
	this.getDoseschedule=getDoseschedule;


	function setDoseschedule(v){
		this.Doseschedule=v;
	}
	this.setDoseschedule=setDoseschedule;

	this.Route=null;


	function getRoute() {
		return this.Route;
	}
	this.getRoute=getRoute;


	function setRoute(v){
		this.Route=v;
	}
	this.setRoute=setRoute;

	this.Indication=null;


	function getIndication() {
		return this.Indication;
	}
	this.getIndication=getIndication;


	function setIndication(v){
		this.Indication=v;
	}
	this.setIndication=setIndication;

	this.Startdate=null;


	function getStartdate() {
		return this.Startdate;
	}
	this.getStartdate=getStartdate;


	function setStartdate(v){
		this.Startdate=v;
	}
	this.setStartdate=setStartdate;

	this.Startdatedaynotreported=null;


	function getStartdatedaynotreported() {
		return this.Startdatedaynotreported;
	}
	this.getStartdatedaynotreported=getStartdatedaynotreported;


	function setStartdatedaynotreported(v){
		this.Startdatedaynotreported=v;
	}
	this.setStartdatedaynotreported=setStartdatedaynotreported;


	this.isStartdatedaynotreported=function(defaultValue) {
		if(this.Startdatedaynotreported==null)return defaultValue;
		if(this.Startdatedaynotreported=="1" || this.Startdatedaynotreported==true)return true;
		return false;
	}

	this.Startdatemonthnotreported=null;


	function getStartdatemonthnotreported() {
		return this.Startdatemonthnotreported;
	}
	this.getStartdatemonthnotreported=getStartdatemonthnotreported;


	function setStartdatemonthnotreported(v){
		this.Startdatemonthnotreported=v;
	}
	this.setStartdatemonthnotreported=setStartdatemonthnotreported;


	this.isStartdatemonthnotreported=function(defaultValue) {
		if(this.Startdatemonthnotreported==null)return defaultValue;
		if(this.Startdatemonthnotreported=="1" || this.Startdatemonthnotreported==true)return true;
		return false;
	}

	this.Startdateyearnotreported=null;


	function getStartdateyearnotreported() {
		return this.Startdateyearnotreported;
	}
	this.getStartdateyearnotreported=getStartdateyearnotreported;


	function setStartdateyearnotreported(v){
		this.Startdateyearnotreported=v;
	}
	this.setStartdateyearnotreported=setStartdateyearnotreported;


	this.isStartdateyearnotreported=function(defaultValue) {
		if(this.Startdateyearnotreported==null)return defaultValue;
		if(this.Startdateyearnotreported=="1" || this.Startdateyearnotreported==true)return true;
		return false;
	}

	this.Enddate=null;


	function getEnddate() {
		return this.Enddate;
	}
	this.getEnddate=getEnddate;


	function setEnddate(v){
		this.Enddate=v;
	}
	this.setEnddate=setEnddate;

	this.Enddatedaynotreported=null;


	function getEnddatedaynotreported() {
		return this.Enddatedaynotreported;
	}
	this.getEnddatedaynotreported=getEnddatedaynotreported;


	function setEnddatedaynotreported(v){
		this.Enddatedaynotreported=v;
	}
	this.setEnddatedaynotreported=setEnddatedaynotreported;


	this.isEnddatedaynotreported=function(defaultValue) {
		if(this.Enddatedaynotreported==null)return defaultValue;
		if(this.Enddatedaynotreported=="1" || this.Enddatedaynotreported==true)return true;
		return false;
	}

	this.Enddatemonthnotreported=null;


	function getEnddatemonthnotreported() {
		return this.Enddatemonthnotreported;
	}
	this.getEnddatemonthnotreported=getEnddatemonthnotreported;


	function setEnddatemonthnotreported(v){
		this.Enddatemonthnotreported=v;
	}
	this.setEnddatemonthnotreported=setEnddatemonthnotreported;


	this.isEnddatemonthnotreported=function(defaultValue) {
		if(this.Enddatemonthnotreported==null)return defaultValue;
		if(this.Enddatemonthnotreported=="1" || this.Enddatemonthnotreported==true)return true;
		return false;
	}

	this.Enddateyearnotreported=null;


	function getEnddateyearnotreported() {
		return this.Enddateyearnotreported;
	}
	this.getEnddateyearnotreported=getEnddateyearnotreported;


	function setEnddateyearnotreported(v){
		this.Enddateyearnotreported=v;
	}
	this.setEnddateyearnotreported=setEnddateyearnotreported;


	this.isEnddateyearnotreported=function(defaultValue) {
		if(this.Enddateyearnotreported==null)return defaultValue;
		if(this.Enddateyearnotreported=="1" || this.Enddateyearnotreported==true)return true;
		return false;
	}

	this.Clinicaltrialname=null;


	function getClinicaltrialname() {
		return this.Clinicaltrialname;
	}
	this.getClinicaltrialname=getClinicaltrialname;


	function setClinicaltrialname(v){
		this.Clinicaltrialname=v;
	}
	this.setClinicaltrialname=setClinicaltrialname;

	this.Clinicaltrialarm=null;


	function getClinicaltrialarm() {
		return this.Clinicaltrialarm;
	}
	this.getClinicaltrialarm=getClinicaltrialarm;


	function setClinicaltrialarm(v){
		this.Clinicaltrialarm=v;
	}
	this.setClinicaltrialarm=setClinicaltrialarm;

	this.Treatmentnotes=null;


	function getTreatmentnotes() {
		return this.Treatmentnotes;
	}
	this.getTreatmentnotes=getTreatmentnotes;


	function setTreatmentnotes(v){
		this.Treatmentnotes=v;
	}
	this.setTreatmentnotes=setTreatmentnotes;

	this.TxMedtreatmentId=null;


	function getTxMedtreatmentId() {
		return this.TxMedtreatmentId;
	}
	this.getTxMedtreatmentId=getTxMedtreatmentId;


	function setTxMedtreatmentId(v){
		this.TxMedtreatmentId=v;
	}
	this.setTxMedtreatmentId=setTxMedtreatmentId;

	this.medtreatments_medtreatment_tx_m_id_fk=null;


	this.getmedtreatments_medtreatment_tx_m_id=function() {
		return this.medtreatments_medtreatment_tx_m_id_fk;
	}


	this.setmedtreatments_medtreatment_tx_m_id=function(v){
		this.medtreatments_medtreatment_tx_m_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="code"){
				return this.Code ;
			} else 
			if(xmlPath=="codeType"){
				return this.Codetype ;
			} else 
			if(xmlPath=="name"){
				return this.Name ;
			} else 
			if(xmlPath=="status"){
				return this.Status ;
			} else 
			if(xmlPath=="doseAmount"){
				return this.Doseamount ;
			} else 
			if(xmlPath=="doseUnit"){
				return this.Doseunit ;
			} else 
			if(xmlPath=="doseSchedule"){
				return this.Doseschedule ;
			} else 
			if(xmlPath=="route"){
				return this.Route ;
			} else 
			if(xmlPath=="indication"){
				return this.Indication ;
			} else 
			if(xmlPath=="startDate"){
				return this.Startdate ;
			} else 
			if(xmlPath=="startDateDayNotReported"){
				return this.Startdatedaynotreported ;
			} else 
			if(xmlPath=="startDateMonthNotReported"){
				return this.Startdatemonthnotreported ;
			} else 
			if(xmlPath=="startDateYearNotReported"){
				return this.Startdateyearnotreported ;
			} else 
			if(xmlPath=="endDate"){
				return this.Enddate ;
			} else 
			if(xmlPath=="endDateDayNotReported"){
				return this.Enddatedaynotreported ;
			} else 
			if(xmlPath=="endDateMonthNotReported"){
				return this.Enddatemonthnotreported ;
			} else 
			if(xmlPath=="endDateYearNotReported"){
				return this.Enddateyearnotreported ;
			} else 
			if(xmlPath=="clinicalTrialName"){
				return this.Clinicaltrialname ;
			} else 
			if(xmlPath=="clinicalTrialArm"){
				return this.Clinicaltrialarm ;
			} else 
			if(xmlPath=="treatmentNotes"){
				return this.Treatmentnotes ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="tx_medTreatment_id"){
				return this.TxMedtreatmentId ;
			} else 
			if(xmlPath=="medtreatments_medtreatment_tx_m_id"){
				return this.medtreatments_medtreatment_tx_m_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="code"){
				this.Code=value;
			} else 
			if(xmlPath=="codeType"){
				this.Codetype=value;
			} else 
			if(xmlPath=="name"){
				this.Name=value;
			} else 
			if(xmlPath=="status"){
				this.Status=value;
			} else 
			if(xmlPath=="doseAmount"){
				this.Doseamount=value;
			} else 
			if(xmlPath=="doseUnit"){
				this.Doseunit=value;
			} else 
			if(xmlPath=="doseSchedule"){
				this.Doseschedule=value;
			} else 
			if(xmlPath=="route"){
				this.Route=value;
			} else 
			if(xmlPath=="indication"){
				this.Indication=value;
			} else 
			if(xmlPath=="startDate"){
				this.Startdate=value;
			} else 
			if(xmlPath=="startDateDayNotReported"){
				this.Startdatedaynotreported=value;
			} else 
			if(xmlPath=="startDateMonthNotReported"){
				this.Startdatemonthnotreported=value;
			} else 
			if(xmlPath=="startDateYearNotReported"){
				this.Startdateyearnotreported=value;
			} else 
			if(xmlPath=="endDate"){
				this.Enddate=value;
			} else 
			if(xmlPath=="endDateDayNotReported"){
				this.Enddatedaynotreported=value;
			} else 
			if(xmlPath=="endDateMonthNotReported"){
				this.Enddatemonthnotreported=value;
			} else 
			if(xmlPath=="endDateYearNotReported"){
				this.Enddateyearnotreported=value;
			} else 
			if(xmlPath=="clinicalTrialName"){
				this.Clinicaltrialname=value;
			} else 
			if(xmlPath=="clinicalTrialArm"){
				this.Clinicaltrialarm=value;
			} else 
			if(xmlPath=="treatmentNotes"){
				this.Treatmentnotes=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="tx_medTreatment_id"){
				this.TxMedtreatmentId=value;
			} else 
			if(xmlPath=="medtreatments_medtreatment_tx_m_id"){
				this.medtreatments_medtreatment_tx_m_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="code"){
			return "field_data";
		}else if (xmlPath=="codeType"){
			return "field_data";
		}else if (xmlPath=="name"){
			return "field_data";
		}else if (xmlPath=="status"){
			return "field_data";
		}else if (xmlPath=="doseAmount"){
			return "field_data";
		}else if (xmlPath=="doseUnit"){
			return "field_data";
		}else if (xmlPath=="doseSchedule"){
			return "field_data";
		}else if (xmlPath=="route"){
			return "field_data";
		}else if (xmlPath=="indication"){
			return "field_data";
		}else if (xmlPath=="startDate"){
			return "field_data";
		}else if (xmlPath=="startDateDayNotReported"){
			return "field_data";
		}else if (xmlPath=="startDateMonthNotReported"){
			return "field_data";
		}else if (xmlPath=="startDateYearNotReported"){
			return "field_data";
		}else if (xmlPath=="endDate"){
			return "field_data";
		}else if (xmlPath=="endDateDayNotReported"){
			return "field_data";
		}else if (xmlPath=="endDateMonthNotReported"){
			return "field_data";
		}else if (xmlPath=="endDateYearNotReported"){
			return "field_data";
		}else if (xmlPath=="clinicalTrialName"){
			return "field_data";
		}else if (xmlPath=="clinicalTrialArm"){
			return "field_data";
		}else if (xmlPath=="treatmentNotes"){
			return "field_LONG_DATA";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<tx:medTreatment";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</tx:medTreatment>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.TxMedtreatmentId!=null){
				if(hiddenCount++>0)str+=",";
				str+="tx_medTreatment_id=\"" + this.TxMedtreatmentId + "\"";
			}
			if(this.medtreatments_medtreatment_tx_m_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="medtreatments_medtreatment_tx_m_id=\"" + this.medtreatments_medtreatment_tx_m_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Code!=null){
			xmlTxt+="\n<tx:code";
			xmlTxt+=">";
			xmlTxt+=this.Code.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tx:code>";
		}
		if (this.Codetype!=null){
			xmlTxt+="\n<tx:codeType";
			xmlTxt+=">";
			xmlTxt+=this.Codetype.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tx:codeType>";
		}
		if (this.Name!=null){
			xmlTxt+="\n<tx:name";
			xmlTxt+=">";
			xmlTxt+=this.Name.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tx:name>";
		}
		if (this.Status!=null){
			xmlTxt+="\n<tx:status";
			xmlTxt+=">";
			xmlTxt+=this.Status.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tx:status>";
		}
		if (this.Doseamount!=null){
			xmlTxt+="\n<tx:doseAmount";
			xmlTxt+=">";
			xmlTxt+=this.Doseamount;
			xmlTxt+="</tx:doseAmount>";
		}
		if (this.Doseunit!=null){
			xmlTxt+="\n<tx:doseUnit";
			xmlTxt+=">";
			xmlTxt+=this.Doseunit.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tx:doseUnit>";
		}
		if (this.Doseschedule!=null){
			xmlTxt+="\n<tx:doseSchedule";
			xmlTxt+=">";
			xmlTxt+=this.Doseschedule.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tx:doseSchedule>";
		}
		if (this.Route!=null){
			xmlTxt+="\n<tx:route";
			xmlTxt+=">";
			xmlTxt+=this.Route.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tx:route>";
		}
		if (this.Indication!=null){
			xmlTxt+="\n<tx:indication";
			xmlTxt+=">";
			xmlTxt+=this.Indication.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tx:indication>";
		}
		if (this.Startdate!=null){
			xmlTxt+="\n<tx:startDate";
			xmlTxt+=">";
			xmlTxt+=this.Startdate;
			xmlTxt+="</tx:startDate>";
		}
		if (this.Startdatedaynotreported!=null){
			xmlTxt+="\n<tx:startDateDayNotReported";
			xmlTxt+=">";
			xmlTxt+=this.Startdatedaynotreported;
			xmlTxt+="</tx:startDateDayNotReported>";
		}
		if (this.Startdatemonthnotreported!=null){
			xmlTxt+="\n<tx:startDateMonthNotReported";
			xmlTxt+=">";
			xmlTxt+=this.Startdatemonthnotreported;
			xmlTxt+="</tx:startDateMonthNotReported>";
		}
		if (this.Startdateyearnotreported!=null){
			xmlTxt+="\n<tx:startDateYearNotReported";
			xmlTxt+=">";
			xmlTxt+=this.Startdateyearnotreported;
			xmlTxt+="</tx:startDateYearNotReported>";
		}
		if (this.Enddate!=null){
			xmlTxt+="\n<tx:endDate";
			xmlTxt+=">";
			xmlTxt+=this.Enddate;
			xmlTxt+="</tx:endDate>";
		}
		if (this.Enddatedaynotreported!=null){
			xmlTxt+="\n<tx:endDateDayNotReported";
			xmlTxt+=">";
			xmlTxt+=this.Enddatedaynotreported;
			xmlTxt+="</tx:endDateDayNotReported>";
		}
		if (this.Enddatemonthnotreported!=null){
			xmlTxt+="\n<tx:endDateMonthNotReported";
			xmlTxt+=">";
			xmlTxt+=this.Enddatemonthnotreported;
			xmlTxt+="</tx:endDateMonthNotReported>";
		}
		if (this.Enddateyearnotreported!=null){
			xmlTxt+="\n<tx:endDateYearNotReported";
			xmlTxt+=">";
			xmlTxt+=this.Enddateyearnotreported;
			xmlTxt+="</tx:endDateYearNotReported>";
		}
		if (this.Clinicaltrialname!=null){
			xmlTxt+="\n<tx:clinicalTrialName";
			xmlTxt+=">";
			xmlTxt+=this.Clinicaltrialname.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tx:clinicalTrialName>";
		}
		if (this.Clinicaltrialarm!=null){
			xmlTxt+="\n<tx:clinicalTrialArm";
			xmlTxt+=">";
			xmlTxt+=this.Clinicaltrialarm.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tx:clinicalTrialArm>";
		}
		if (this.Treatmentnotes!=null){
			xmlTxt+="\n<tx:treatmentNotes";
			xmlTxt+=">";
			xmlTxt+=this.Treatmentnotes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tx:treatmentNotes>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.TxMedtreatmentId!=null) return true;
			if (this.medtreatments_medtreatment_tx_m_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Code!=null) return true;
		if (this.Codetype!=null) return true;
		if (this.Name!=null) return true;
		if (this.Status!=null) return true;
		if (this.Doseamount!=null) return true;
		if (this.Doseunit!=null) return true;
		if (this.Doseschedule!=null) return true;
		if (this.Route!=null) return true;
		if (this.Indication!=null) return true;
		if (this.Startdate!=null) return true;
		if (this.Startdatedaynotreported!=null) return true;
		if (this.Startdatemonthnotreported!=null) return true;
		if (this.Startdateyearnotreported!=null) return true;
		if (this.Enddate!=null) return true;
		if (this.Enddatedaynotreported!=null) return true;
		if (this.Enddatemonthnotreported!=null) return true;
		if (this.Enddateyearnotreported!=null) return true;
		if (this.Clinicaltrialname!=null) return true;
		if (this.Clinicaltrialarm!=null) return true;
		if (this.Treatmentnotes!=null) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
