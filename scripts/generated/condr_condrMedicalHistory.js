/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:04 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function condr_condrMedicalHistory(){
this.xsiType="condr:condrMedicalHistory";

	this.getSchemaElementName=function(){
		return "condrMedicalHistory";
	}

	this.getFullSchemaElementName=function(){
		return "condr:condrMedicalHistory";
	}
this.extension=dynamicJSLoad('sf_medicalHistory','generated/sf_medicalHistory.js');

	this.Tumorpredisposition=null;


	function getTumorpredisposition() {
		return this.Tumorpredisposition;
	}
	this.getTumorpredisposition=getTumorpredisposition;


	function setTumorpredisposition(v){
		this.Tumorpredisposition=v;
	}
	this.setTumorpredisposition=setTumorpredisposition;

	this.Familytumorsyndrome=null;


	function getFamilytumorsyndrome() {
		return this.Familytumorsyndrome;
	}
	this.getFamilytumorsyndrome=getFamilytumorsyndrome;


	function setFamilytumorsyndrome(v){
		this.Familytumorsyndrome=v;
	}
	this.setFamilytumorsyndrome=setFamilytumorsyndrome;


	this.isFamilytumorsyndrome=function(defaultValue) {
		if(this.Familytumorsyndrome==null)return defaultValue;
		if(this.Familytumorsyndrome=="1" || this.Familytumorsyndrome==true)return true;
		return false;
	}

	this.Familytumorspecifictype=null;


	function getFamilytumorspecifictype() {
		return this.Familytumorspecifictype;
	}
	this.getFamilytumorspecifictype=getFamilytumorspecifictype;


	function setFamilytumorspecifictype(v){
		this.Familytumorspecifictype=v;
	}
	this.setFamilytumorspecifictype=setFamilytumorspecifictype;


	this.isFamilytumorspecifictype=function(defaultValue) {
		if(this.Familytumorspecifictype==null)return defaultValue;
		if(this.Familytumorspecifictype=="1" || this.Familytumorspecifictype==true)return true;
		return false;
	}

	this.Familytumornotes=null;


	function getFamilytumornotes() {
		return this.Familytumornotes;
	}
	this.getFamilytumornotes=getFamilytumornotes;


	function setFamilytumornotes(v){
		this.Familytumornotes=v;
	}
	this.setFamilytumornotes=setFamilytumornotes;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="medicalHistory"){
				return this.Medicalhistory ;
			} else 
			if(xmlPath.startsWith("medicalHistory")){
				xmlPath=xmlPath.substring(14);
				if(xmlPath=="")return this.Medicalhistory ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Medicalhistory!=undefined)return this.Medicalhistory.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="tumorPredisposition"){
				return this.Tumorpredisposition ;
			} else 
			if(xmlPath=="familyTumorSyndrome"){
				return this.Familytumorsyndrome ;
			} else 
			if(xmlPath=="familyTumorSpecificType"){
				return this.Familytumorspecifictype ;
			} else 
			if(xmlPath=="familyTumorNotes"){
				return this.Familytumornotes ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="medicalHistory"){
				this.Medicalhistory=value;
			} else 
			if(xmlPath.startsWith("medicalHistory")){
				xmlPath=xmlPath.substring(14);
				if(xmlPath=="")return this.Medicalhistory ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Medicalhistory!=undefined){
					this.Medicalhistory.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Medicalhistory= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Medicalhistory= instanciateObject("sf:medicalHistory");//omUtils.js
						}
						if(options && options.where)this.Medicalhistory.setProperty(options.where.field,options.where.value);
						this.Medicalhistory.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="tumorPredisposition"){
				this.Tumorpredisposition=value;
			} else 
			if(xmlPath=="familyTumorSyndrome"){
				this.Familytumorsyndrome=value;
			} else 
			if(xmlPath=="familyTumorSpecificType"){
				this.Familytumorspecifictype=value;
			} else 
			if(xmlPath=="familyTumorNotes"){
				this.Familytumornotes=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="tumorPredisposition"){
			return "field_data";
		}else if (xmlPath=="familyTumorSyndrome"){
			return "field_data";
		}else if (xmlPath=="familyTumorSpecificType"){
			return "field_data";
		}else if (xmlPath=="familyTumorNotes"){
			return "field_LONG_DATA";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<condr:CondrMedicalHistory";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</condr:CondrMedicalHistory>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Tumorpredisposition!=null){
			xmlTxt+="\n<condr:tumorPredisposition";
			xmlTxt+=">";
			xmlTxt+=this.Tumorpredisposition.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:tumorPredisposition>";
		}
		if (this.Familytumorsyndrome!=null){
			xmlTxt+="\n<condr:familyTumorSyndrome";
			xmlTxt+=">";
			xmlTxt+=this.Familytumorsyndrome;
			xmlTxt+="</condr:familyTumorSyndrome>";
		}
		if (this.Familytumorspecifictype!=null){
			xmlTxt+="\n<condr:familyTumorSpecificType";
			xmlTxt+=">";
			xmlTxt+=this.Familytumorspecifictype;
			xmlTxt+="</condr:familyTumorSpecificType>";
		}
		if (this.Familytumornotes!=null){
			xmlTxt+="\n<condr:familyTumorNotes";
			xmlTxt+=">";
			xmlTxt+=this.Familytumornotes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:familyTumorNotes>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Tumorpredisposition!=null) return true;
		if (this.Familytumorsyndrome!=null) return true;
		if (this.Familytumorspecifictype!=null) return true;
		if (this.Familytumornotes!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
