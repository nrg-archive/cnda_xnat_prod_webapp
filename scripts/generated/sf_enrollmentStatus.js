/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function sf_enrollmentStatus(){
this.xsiType="sf:enrollmentStatus";

	this.getSchemaElementName=function(){
		return "enrollmentStatus";
	}

	this.getFullSchemaElementName=function(){
		return "sf:enrollmentStatus";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Enrollmentdate=null;


	function getEnrollmentdate() {
		return this.Enrollmentdate;
	}
	this.getEnrollmentdate=getEnrollmentdate;


	function setEnrollmentdate(v){
		this.Enrollmentdate=v;
	}
	this.setEnrollmentdate=setEnrollmentdate;

	this.Initialdiagnosis=null;


	function getInitialdiagnosis() {
		return this.Initialdiagnosis;
	}
	this.getInitialdiagnosis=getInitialdiagnosis;


	function setInitialdiagnosis(v){
		this.Initialdiagnosis=v;
	}
	this.setInitialdiagnosis=setInitialdiagnosis;

	this.Initialdiagnosisdate=null;


	function getInitialdiagnosisdate() {
		return this.Initialdiagnosisdate;
	}
	this.getInitialdiagnosisdate=getInitialdiagnosisdate;


	function setInitialdiagnosisdate(v){
		this.Initialdiagnosisdate=v;
	}
	this.setInitialdiagnosisdate=setInitialdiagnosisdate;

	this.Pathologicdiagnosis=null;


	function getPathologicdiagnosis() {
		return this.Pathologicdiagnosis;
	}
	this.getPathologicdiagnosis=getPathologicdiagnosis;


	function setPathologicdiagnosis(v){
		this.Pathologicdiagnosis=v;
	}
	this.setPathologicdiagnosis=setPathologicdiagnosis;


	this.isPathologicdiagnosis=function(defaultValue) {
		if(this.Pathologicdiagnosis==null)return defaultValue;
		if(this.Pathologicdiagnosis=="1" || this.Pathologicdiagnosis==true)return true;
		return false;
	}

	this.Pathologicdiagnosistype=null;


	function getPathologicdiagnosistype() {
		return this.Pathologicdiagnosistype;
	}
	this.getPathologicdiagnosistype=getPathologicdiagnosistype;


	function setPathologicdiagnosistype(v){
		this.Pathologicdiagnosistype=v;
	}
	this.setPathologicdiagnosistype=setPathologicdiagnosistype;

	this.Pathologicdiagnosisdate=null;


	function getPathologicdiagnosisdate() {
		return this.Pathologicdiagnosisdate;
	}
	this.getPathologicdiagnosisdate=getPathologicdiagnosisdate;


	function setPathologicdiagnosisdate(v){
		this.Pathologicdiagnosisdate=v;
	}
	this.setPathologicdiagnosisdate=setPathologicdiagnosisdate;

	this.Previouslytreated=null;


	function getPreviouslytreated() {
		return this.Previouslytreated;
	}
	this.getPreviouslytreated=getPreviouslytreated;


	function setPreviouslytreated(v){
		this.Previouslytreated=v;
	}
	this.setPreviouslytreated=setPreviouslytreated;


	this.isPreviouslytreated=function(defaultValue) {
		if(this.Previouslytreated==null)return defaultValue;
		if(this.Previouslytreated=="1" || this.Previouslytreated==true)return true;
		return false;
	}

	this.Detailspriortoenrollment=null;


	function getDetailspriortoenrollment() {
		return this.Detailspriortoenrollment;
	}
	this.getDetailspriortoenrollment=getDetailspriortoenrollment;


	function setDetailspriortoenrollment(v){
		this.Detailspriortoenrollment=v;
	}
	this.setDetailspriortoenrollment=setDetailspriortoenrollment;

	this.Systemicdiagnosisdate=null;


	function getSystemicdiagnosisdate() {
		return this.Systemicdiagnosisdate;
	}
	this.getSystemicdiagnosisdate=getSystemicdiagnosisdate;


	function setSystemicdiagnosisdate(v){
		this.Systemicdiagnosisdate=v;
	}
	this.setSystemicdiagnosisdate=setSystemicdiagnosisdate;

	this.Recurrentatenrollment=null;


	function getRecurrentatenrollment() {
		return this.Recurrentatenrollment;
	}
	this.getRecurrentatenrollment=getRecurrentatenrollment;


	function setRecurrentatenrollment(v){
		this.Recurrentatenrollment=v;
	}
	this.setRecurrentatenrollment=setRecurrentatenrollment;


	this.isRecurrentatenrollment=function(defaultValue) {
		if(this.Recurrentatenrollment==null)return defaultValue;
		if(this.Recurrentatenrollment=="1" || this.Recurrentatenrollment==true)return true;
		return false;
	}

	this.Recurrentatenrollmentnotes=null;


	function getRecurrentatenrollmentnotes() {
		return this.Recurrentatenrollmentnotes;
	}
	this.getRecurrentatenrollmentnotes=getRecurrentatenrollmentnotes;


	function setRecurrentatenrollmentnotes(v){
		this.Recurrentatenrollmentnotes=v;
	}
	this.setRecurrentatenrollmentnotes=setRecurrentatenrollmentnotes;

	this.Exitdate=null;


	function getExitdate() {
		return this.Exitdate;
	}
	this.getExitdate=getExitdate;


	function setExitdate(v){
		this.Exitdate=v;
	}
	this.setExitdate=setExitdate;

	this.Exitreason=null;


	function getExitreason() {
		return this.Exitreason;
	}
	this.getExitreason=getExitreason;


	function setExitreason(v){
		this.Exitreason=v;
	}
	this.setExitreason=setExitreason;

	this.Exitnotes=null;


	function getExitnotes() {
		return this.Exitnotes;
	}
	this.getExitnotes=getExitnotes;


	function setExitnotes(v){
		this.Exitnotes=v;
	}
	this.setExitnotes=setExitnotes;

	this.Deathdate=null;


	function getDeathdate() {
		return this.Deathdate;
	}
	this.getDeathdate=getDeathdate;


	function setDeathdate(v){
		this.Deathdate=v;
	}
	this.setDeathdate=setDeathdate;

	this.Autopsied=null;


	function getAutopsied() {
		return this.Autopsied;
	}
	this.getAutopsied=getAutopsied;


	function setAutopsied(v){
		this.Autopsied=v;
	}
	this.setAutopsied=setAutopsied;


	this.isAutopsied=function(defaultValue) {
		if(this.Autopsied==null)return defaultValue;
		if(this.Autopsied=="1" || this.Autopsied==true)return true;
		return false;
	}

	this.Autopsiedoptions=null;


	function getAutopsiedoptions() {
		return this.Autopsiedoptions;
	}
	this.getAutopsiedoptions=getAutopsiedoptions;


	function setAutopsiedoptions(v){
		this.Autopsiedoptions=v;
	}
	this.setAutopsiedoptions=setAutopsiedoptions;

	this.Autopsynotes=null;


	function getAutopsynotes() {
		return this.Autopsynotes;
	}
	this.getAutopsynotes=getAutopsynotes;


	function setAutopsynotes(v){
		this.Autopsynotes=v;
	}
	this.setAutopsynotes=setAutopsynotes;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="enrollmentDate"){
				return this.Enrollmentdate ;
			} else 
			if(xmlPath=="initialDiagnosis"){
				return this.Initialdiagnosis ;
			} else 
			if(xmlPath=="initialDiagnosisDate"){
				return this.Initialdiagnosisdate ;
			} else 
			if(xmlPath=="pathologicDiagnosis"){
				return this.Pathologicdiagnosis ;
			} else 
			if(xmlPath=="pathologicDiagnosisType"){
				return this.Pathologicdiagnosistype ;
			} else 
			if(xmlPath=="pathologicDiagnosisDate"){
				return this.Pathologicdiagnosisdate ;
			} else 
			if(xmlPath=="previouslyTreated"){
				return this.Previouslytreated ;
			} else 
			if(xmlPath=="detailsPriorToEnrollment"){
				return this.Detailspriortoenrollment ;
			} else 
			if(xmlPath=="systemicDiagnosisDate"){
				return this.Systemicdiagnosisdate ;
			} else 
			if(xmlPath=="recurrentAtEnrollment"){
				return this.Recurrentatenrollment ;
			} else 
			if(xmlPath=="recurrentAtEnrollmentNotes"){
				return this.Recurrentatenrollmentnotes ;
			} else 
			if(xmlPath=="exitDate"){
				return this.Exitdate ;
			} else 
			if(xmlPath=="exitReason"){
				return this.Exitreason ;
			} else 
			if(xmlPath=="exitNotes"){
				return this.Exitnotes ;
			} else 
			if(xmlPath=="deathDate"){
				return this.Deathdate ;
			} else 
			if(xmlPath=="autopsied"){
				return this.Autopsied ;
			} else 
			if(xmlPath=="autopsiedOptions"){
				return this.Autopsiedoptions ;
			} else 
			if(xmlPath=="autopsyNotes"){
				return this.Autopsynotes ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="enrollmentDate"){
				this.Enrollmentdate=value;
			} else 
			if(xmlPath=="initialDiagnosis"){
				this.Initialdiagnosis=value;
			} else 
			if(xmlPath=="initialDiagnosisDate"){
				this.Initialdiagnosisdate=value;
			} else 
			if(xmlPath=="pathologicDiagnosis"){
				this.Pathologicdiagnosis=value;
			} else 
			if(xmlPath=="pathologicDiagnosisType"){
				this.Pathologicdiagnosistype=value;
			} else 
			if(xmlPath=="pathologicDiagnosisDate"){
				this.Pathologicdiagnosisdate=value;
			} else 
			if(xmlPath=="previouslyTreated"){
				this.Previouslytreated=value;
			} else 
			if(xmlPath=="detailsPriorToEnrollment"){
				this.Detailspriortoenrollment=value;
			} else 
			if(xmlPath=="systemicDiagnosisDate"){
				this.Systemicdiagnosisdate=value;
			} else 
			if(xmlPath=="recurrentAtEnrollment"){
				this.Recurrentatenrollment=value;
			} else 
			if(xmlPath=="recurrentAtEnrollmentNotes"){
				this.Recurrentatenrollmentnotes=value;
			} else 
			if(xmlPath=="exitDate"){
				this.Exitdate=value;
			} else 
			if(xmlPath=="exitReason"){
				this.Exitreason=value;
			} else 
			if(xmlPath=="exitNotes"){
				this.Exitnotes=value;
			} else 
			if(xmlPath=="deathDate"){
				this.Deathdate=value;
			} else 
			if(xmlPath=="autopsied"){
				this.Autopsied=value;
			} else 
			if(xmlPath=="autopsiedOptions"){
				this.Autopsiedoptions=value;
			} else 
			if(xmlPath=="autopsyNotes"){
				this.Autopsynotes=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="enrollmentDate"){
			return "field_data";
		}else if (xmlPath=="initialDiagnosis"){
			return "field_LONG_DATA";
		}else if (xmlPath=="initialDiagnosisDate"){
			return "field_data";
		}else if (xmlPath=="pathologicDiagnosis"){
			return "field_data";
		}else if (xmlPath=="pathologicDiagnosisType"){
			return "field_data";
		}else if (xmlPath=="pathologicDiagnosisDate"){
			return "field_data";
		}else if (xmlPath=="previouslyTreated"){
			return "field_data";
		}else if (xmlPath=="detailsPriorToEnrollment"){
			return "field_LONG_DATA";
		}else if (xmlPath=="systemicDiagnosisDate"){
			return "field_data";
		}else if (xmlPath=="recurrentAtEnrollment"){
			return "field_data";
		}else if (xmlPath=="recurrentAtEnrollmentNotes"){
			return "field_LONG_DATA";
		}else if (xmlPath=="exitDate"){
			return "field_data";
		}else if (xmlPath=="exitReason"){
			return "field_data";
		}else if (xmlPath=="exitNotes"){
			return "field_LONG_DATA";
		}else if (xmlPath=="deathDate"){
			return "field_data";
		}else if (xmlPath=="autopsied"){
			return "field_data";
		}else if (xmlPath=="autopsiedOptions"){
			return "field_data";
		}else if (xmlPath=="autopsyNotes"){
			return "field_LONG_DATA";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<sf:EnrollmentStatus";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</sf:EnrollmentStatus>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Enrollmentdate!=null){
			xmlTxt+="\n<sf:enrollmentDate";
			xmlTxt+=">";
			xmlTxt+=this.Enrollmentdate;
			xmlTxt+="</sf:enrollmentDate>";
		}
		if (this.Initialdiagnosis!=null){
			xmlTxt+="\n<sf:initialDiagnosis";
			xmlTxt+=">";
			xmlTxt+=this.Initialdiagnosis.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:initialDiagnosis>";
		}
		if (this.Initialdiagnosisdate!=null){
			xmlTxt+="\n<sf:initialDiagnosisDate";
			xmlTxt+=">";
			xmlTxt+=this.Initialdiagnosisdate;
			xmlTxt+="</sf:initialDiagnosisDate>";
		}
		if (this.Pathologicdiagnosis!=null){
			xmlTxt+="\n<sf:pathologicDiagnosis";
			xmlTxt+=">";
			xmlTxt+=this.Pathologicdiagnosis;
			xmlTxt+="</sf:pathologicDiagnosis>";
		}
		if (this.Pathologicdiagnosistype!=null){
			xmlTxt+="\n<sf:pathologicDiagnosisType";
			xmlTxt+=">";
			xmlTxt+=this.Pathologicdiagnosistype.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:pathologicDiagnosisType>";
		}
		if (this.Pathologicdiagnosisdate!=null){
			xmlTxt+="\n<sf:pathologicDiagnosisDate";
			xmlTxt+=">";
			xmlTxt+=this.Pathologicdiagnosisdate;
			xmlTxt+="</sf:pathologicDiagnosisDate>";
		}
		if (this.Previouslytreated!=null){
			xmlTxt+="\n<sf:previouslyTreated";
			xmlTxt+=">";
			xmlTxt+=this.Previouslytreated;
			xmlTxt+="</sf:previouslyTreated>";
		}
		if (this.Detailspriortoenrollment!=null){
			xmlTxt+="\n<sf:detailsPriorToEnrollment";
			xmlTxt+=">";
			xmlTxt+=this.Detailspriortoenrollment.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:detailsPriorToEnrollment>";
		}
		if (this.Systemicdiagnosisdate!=null){
			xmlTxt+="\n<sf:systemicDiagnosisDate";
			xmlTxt+=">";
			xmlTxt+=this.Systemicdiagnosisdate;
			xmlTxt+="</sf:systemicDiagnosisDate>";
		}
		if (this.Recurrentatenrollment!=null){
			xmlTxt+="\n<sf:recurrentAtEnrollment";
			xmlTxt+=">";
			xmlTxt+=this.Recurrentatenrollment;
			xmlTxt+="</sf:recurrentAtEnrollment>";
		}
		if (this.Recurrentatenrollmentnotes!=null){
			xmlTxt+="\n<sf:recurrentAtEnrollmentNotes";
			xmlTxt+=">";
			xmlTxt+=this.Recurrentatenrollmentnotes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:recurrentAtEnrollmentNotes>";
		}
		if (this.Exitdate!=null){
			xmlTxt+="\n<sf:exitDate";
			xmlTxt+=">";
			xmlTxt+=this.Exitdate;
			xmlTxt+="</sf:exitDate>";
		}
		if (this.Exitreason!=null){
			xmlTxt+="\n<sf:exitReason";
			xmlTxt+=">";
			xmlTxt+=this.Exitreason.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:exitReason>";
		}
		if (this.Exitnotes!=null){
			xmlTxt+="\n<sf:exitNotes";
			xmlTxt+=">";
			xmlTxt+=this.Exitnotes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:exitNotes>";
		}
		if (this.Deathdate!=null){
			xmlTxt+="\n<sf:deathDate";
			xmlTxt+=">";
			xmlTxt+=this.Deathdate;
			xmlTxt+="</sf:deathDate>";
		}
		if (this.Autopsied!=null){
			xmlTxt+="\n<sf:autopsied";
			xmlTxt+=">";
			xmlTxt+=this.Autopsied;
			xmlTxt+="</sf:autopsied>";
		}
		if (this.Autopsiedoptions!=null){
			xmlTxt+="\n<sf:autopsiedOptions";
			xmlTxt+=">";
			xmlTxt+=this.Autopsiedoptions.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:autopsiedOptions>";
		}
		if (this.Autopsynotes!=null){
			xmlTxt+="\n<sf:autopsyNotes";
			xmlTxt+=">";
			xmlTxt+=this.Autopsynotes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:autopsyNotes>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Enrollmentdate!=null) return true;
		if (this.Initialdiagnosis!=null) return true;
		if (this.Initialdiagnosisdate!=null) return true;
		if (this.Pathologicdiagnosis!=null) return true;
		if (this.Pathologicdiagnosistype!=null) return true;
		if (this.Pathologicdiagnosisdate!=null) return true;
		if (this.Previouslytreated!=null) return true;
		if (this.Detailspriortoenrollment!=null) return true;
		if (this.Systemicdiagnosisdate!=null) return true;
		if (this.Recurrentatenrollment!=null) return true;
		if (this.Recurrentatenrollmentnotes!=null) return true;
		if (this.Exitdate!=null) return true;
		if (this.Exitreason!=null) return true;
		if (this.Exitnotes!=null) return true;
		if (this.Deathdate!=null) return true;
		if (this.Autopsied!=null) return true;
		if (this.Autopsiedoptions!=null) return true;
		if (this.Autopsynotes!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
