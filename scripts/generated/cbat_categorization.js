/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cbat_categorization(){
this.xsiType="cbat:categorization";

	this.getSchemaElementName=function(){
		return "categorization";
	}

	this.getFullSchemaElementName=function(){
		return "cbat:categorization";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Yest1_accuracy=null;


	function getYest1_accuracy() {
		return this.Yest1_accuracy;
	}
	this.getYest1_accuracy=getYest1_accuracy;


	function setYest1_accuracy(v){
		this.Yest1_accuracy=v;
	}
	this.setYest1_accuracy=setYest1_accuracy;

	this.Yest1_responsetime=null;


	function getYest1_responsetime() {
		return this.Yest1_responsetime;
	}
	this.getYest1_responsetime=getYest1_responsetime;


	function setYest1_responsetime(v){
		this.Yest1_responsetime=v;
	}
	this.setYest1_responsetime=setYest1_responsetime;

	this.Yest2_accuracy=null;


	function getYest2_accuracy() {
		return this.Yest2_accuracy;
	}
	this.getYest2_accuracy=getYest2_accuracy;


	function setYest2_accuracy(v){
		this.Yest2_accuracy=v;
	}
	this.setYest2_accuracy=setYest2_accuracy;

	this.Yest2_responsetime=null;


	function getYest2_responsetime() {
		return this.Yest2_responsetime;
	}
	this.getYest2_responsetime=getYest2_responsetime;


	function setYest2_responsetime(v){
		this.Yest2_responsetime=v;
	}
	this.setYest2_responsetime=setYest2_responsetime;

	this.Yest3_accuracy=null;


	function getYest3_accuracy() {
		return this.Yest3_accuracy;
	}
	this.getYest3_accuracy=getYest3_accuracy;


	function setYest3_accuracy(v){
		this.Yest3_accuracy=v;
	}
	this.setYest3_accuracy=setYest3_accuracy;

	this.Yest3_responsetime=null;


	function getYest3_responsetime() {
		return this.Yest3_responsetime;
	}
	this.getYest3_responsetime=getYest3_responsetime;


	function setYest3_responsetime(v){
		this.Yest3_responsetime=v;
	}
	this.setYest3_responsetime=setYest3_responsetime;

	this.Yest4_accuracy=null;


	function getYest4_accuracy() {
		return this.Yest4_accuracy;
	}
	this.getYest4_accuracy=getYest4_accuracy;


	function setYest4_accuracy(v){
		this.Yest4_accuracy=v;
	}
	this.setYest4_accuracy=setYest4_accuracy;

	this.Yest4_responsetime=null;


	function getYest4_responsetime() {
		return this.Yest4_responsetime;
	}
	this.getYest4_responsetime=getYest4_responsetime;


	function setYest4_responsetime(v){
		this.Yest4_responsetime=v;
	}
	this.setYest4_responsetime=setYest4_responsetime;

	this.Yest5_accuracy=null;


	function getYest5_accuracy() {
		return this.Yest5_accuracy;
	}
	this.getYest5_accuracy=getYest5_accuracy;


	function setYest5_accuracy(v){
		this.Yest5_accuracy=v;
	}
	this.setYest5_accuracy=setYest5_accuracy;

	this.Yest5_responsetime=null;


	function getYest5_responsetime() {
		return this.Yest5_responsetime;
	}
	this.getYest5_responsetime=getYest5_responsetime;


	function setYest5_responsetime(v){
		this.Yest5_responsetime=v;
	}
	this.setYest5_responsetime=setYest5_responsetime;

	this.Yest6_accuracy=null;


	function getYest6_accuracy() {
		return this.Yest6_accuracy;
	}
	this.getYest6_accuracy=getYest6_accuracy;


	function setYest6_accuracy(v){
		this.Yest6_accuracy=v;
	}
	this.setYest6_accuracy=setYest6_accuracy;

	this.Yest6_responsetime=null;


	function getYest6_responsetime() {
		return this.Yest6_responsetime;
	}
	this.getYest6_responsetime=getYest6_responsetime;


	function setYest6_responsetime(v){
		this.Yest6_responsetime=v;
	}
	this.setYest6_responsetime=setYest6_responsetime;

	this.Yest7_accuracy=null;


	function getYest7_accuracy() {
		return this.Yest7_accuracy;
	}
	this.getYest7_accuracy=getYest7_accuracy;


	function setYest7_accuracy(v){
		this.Yest7_accuracy=v;
	}
	this.setYest7_accuracy=setYest7_accuracy;

	this.Yest7_responsetime=null;


	function getYest7_responsetime() {
		return this.Yest7_responsetime;
	}
	this.getYest7_responsetime=getYest7_responsetime;


	function setYest7_responsetime(v){
		this.Yest7_responsetime=v;
	}
	this.setYest7_responsetime=setYest7_responsetime;

	this.Yest8_accuracy=null;


	function getYest8_accuracy() {
		return this.Yest8_accuracy;
	}
	this.getYest8_accuracy=getYest8_accuracy;


	function setYest8_accuracy(v){
		this.Yest8_accuracy=v;
	}
	this.setYest8_accuracy=setYest8_accuracy;

	this.Yest8_responsetime=null;


	function getYest8_responsetime() {
		return this.Yest8_responsetime;
	}
	this.getYest8_responsetime=getYest8_responsetime;


	function setYest8_responsetime(v){
		this.Yest8_responsetime=v;
	}
	this.setYest8_responsetime=setYest8_responsetime;

	this.Yest9_accuracy=null;


	function getYest9_accuracy() {
		return this.Yest9_accuracy;
	}
	this.getYest9_accuracy=getYest9_accuracy;


	function setYest9_accuracy(v){
		this.Yest9_accuracy=v;
	}
	this.setYest9_accuracy=setYest9_accuracy;

	this.Yest9_responsetime=null;


	function getYest9_responsetime() {
		return this.Yest9_responsetime;
	}
	this.getYest9_responsetime=getYest9_responsetime;


	function setYest9_responsetime(v){
		this.Yest9_responsetime=v;
	}
	this.setYest9_responsetime=setYest9_responsetime;

	this.Yest10_accuracy=null;


	function getYest10_accuracy() {
		return this.Yest10_accuracy;
	}
	this.getYest10_accuracy=getYest10_accuracy;


	function setYest10_accuracy(v){
		this.Yest10_accuracy=v;
	}
	this.setYest10_accuracy=setYest10_accuracy;

	this.Yest10_responsetime=null;


	function getYest10_responsetime() {
		return this.Yest10_responsetime;
	}
	this.getYest10_responsetime=getYest10_responsetime;


	function setYest10_responsetime(v){
		this.Yest10_responsetime=v;
	}
	this.setYest10_responsetime=setYest10_responsetime;

	this.Yest11_accuracy=null;


	function getYest11_accuracy() {
		return this.Yest11_accuracy;
	}
	this.getYest11_accuracy=getYest11_accuracy;


	function setYest11_accuracy(v){
		this.Yest11_accuracy=v;
	}
	this.setYest11_accuracy=setYest11_accuracy;

	this.Yest11_responsetime=null;


	function getYest11_responsetime() {
		return this.Yest11_responsetime;
	}
	this.getYest11_responsetime=getYest11_responsetime;


	function setYest11_responsetime(v){
		this.Yest11_responsetime=v;
	}
	this.setYest11_responsetime=setYest11_responsetime;

	this.Yest12_accuracy=null;


	function getYest12_accuracy() {
		return this.Yest12_accuracy;
	}
	this.getYest12_accuracy=getYest12_accuracy;


	function setYest12_accuracy(v){
		this.Yest12_accuracy=v;
	}
	this.setYest12_accuracy=setYest12_accuracy;

	this.Yest12_responsetime=null;


	function getYest12_responsetime() {
		return this.Yest12_responsetime;
	}
	this.getYest12_responsetime=getYest12_responsetime;


	function setYest12_responsetime(v){
		this.Yest12_responsetime=v;
	}
	this.setYest12_responsetime=setYest12_responsetime;

	this.Yest13_accuracy=null;


	function getYest13_accuracy() {
		return this.Yest13_accuracy;
	}
	this.getYest13_accuracy=getYest13_accuracy;


	function setYest13_accuracy(v){
		this.Yest13_accuracy=v;
	}
	this.setYest13_accuracy=setYest13_accuracy;

	this.Yest13_responsetime=null;


	function getYest13_responsetime() {
		return this.Yest13_responsetime;
	}
	this.getYest13_responsetime=getYest13_responsetime;


	function setYest13_responsetime(v){
		this.Yest13_responsetime=v;
	}
	this.setYest13_responsetime=setYest13_responsetime;

	this.Yest14_accuracy=null;


	function getYest14_accuracy() {
		return this.Yest14_accuracy;
	}
	this.getYest14_accuracy=getYest14_accuracy;


	function setYest14_accuracy(v){
		this.Yest14_accuracy=v;
	}
	this.setYest14_accuracy=setYest14_accuracy;

	this.Yest14_responsetime=null;


	function getYest14_responsetime() {
		return this.Yest14_responsetime;
	}
	this.getYest14_responsetime=getYest14_responsetime;


	function setYest14_responsetime(v){
		this.Yest14_responsetime=v;
	}
	this.setYest14_responsetime=setYest14_responsetime;

	this.Yest15_accuracy=null;


	function getYest15_accuracy() {
		return this.Yest15_accuracy;
	}
	this.getYest15_accuracy=getYest15_accuracy;


	function setYest15_accuracy(v){
		this.Yest15_accuracy=v;
	}
	this.setYest15_accuracy=setYest15_accuracy;

	this.Yest15_responsetime=null;


	function getYest15_responsetime() {
		return this.Yest15_responsetime;
	}
	this.getYest15_responsetime=getYest15_responsetime;


	function setYest15_responsetime(v){
		this.Yest15_responsetime=v;
	}
	this.setYest15_responsetime=setYest15_responsetime;

	this.Yest16_accuracy=null;


	function getYest16_accuracy() {
		return this.Yest16_accuracy;
	}
	this.getYest16_accuracy=getYest16_accuracy;


	function setYest16_accuracy(v){
		this.Yest16_accuracy=v;
	}
	this.setYest16_accuracy=setYest16_accuracy;

	this.Yest16_responsetime=null;


	function getYest16_responsetime() {
		return this.Yest16_responsetime;
	}
	this.getYest16_responsetime=getYest16_responsetime;


	function setYest16_responsetime(v){
		this.Yest16_responsetime=v;
	}
	this.setYest16_responsetime=setYest16_responsetime;

	this.Yest17_accuracy=null;


	function getYest17_accuracy() {
		return this.Yest17_accuracy;
	}
	this.getYest17_accuracy=getYest17_accuracy;


	function setYest17_accuracy(v){
		this.Yest17_accuracy=v;
	}
	this.setYest17_accuracy=setYest17_accuracy;

	this.Yest17_responsetime=null;


	function getYest17_responsetime() {
		return this.Yest17_responsetime;
	}
	this.getYest17_responsetime=getYest17_responsetime;


	function setYest17_responsetime(v){
		this.Yest17_responsetime=v;
	}
	this.setYest17_responsetime=setYest17_responsetime;

	this.Yest18_accuracy=null;


	function getYest18_accuracy() {
		return this.Yest18_accuracy;
	}
	this.getYest18_accuracy=getYest18_accuracy;


	function setYest18_accuracy(v){
		this.Yest18_accuracy=v;
	}
	this.setYest18_accuracy=setYest18_accuracy;

	this.Yest18_responsetime=null;


	function getYest18_responsetime() {
		return this.Yest18_responsetime;
	}
	this.getYest18_responsetime=getYest18_responsetime;


	function setYest18_responsetime(v){
		this.Yest18_responsetime=v;
	}
	this.setYest18_responsetime=setYest18_responsetime;

	this.Yest19_accuracy=null;


	function getYest19_accuracy() {
		return this.Yest19_accuracy;
	}
	this.getYest19_accuracy=getYest19_accuracy;


	function setYest19_accuracy(v){
		this.Yest19_accuracy=v;
	}
	this.setYest19_accuracy=setYest19_accuracy;

	this.Yest19_responsetime=null;


	function getYest19_responsetime() {
		return this.Yest19_responsetime;
	}
	this.getYest19_responsetime=getYest19_responsetime;


	function setYest19_responsetime(v){
		this.Yest19_responsetime=v;
	}
	this.setYest19_responsetime=setYest19_responsetime;

	this.Yest20_accuracy=null;


	function getYest20_accuracy() {
		return this.Yest20_accuracy;
	}
	this.getYest20_accuracy=getYest20_accuracy;


	function setYest20_accuracy(v){
		this.Yest20_accuracy=v;
	}
	this.setYest20_accuracy=setYest20_accuracy;

	this.Yest20_responsetime=null;


	function getYest20_responsetime() {
		return this.Yest20_responsetime;
	}
	this.getYest20_responsetime=getYest20_responsetime;


	function setYest20_responsetime(v){
		this.Yest20_responsetime=v;
	}
	this.setYest20_responsetime=setYest20_responsetime;

	this.Yest21_accuracy=null;


	function getYest21_accuracy() {
		return this.Yest21_accuracy;
	}
	this.getYest21_accuracy=getYest21_accuracy;


	function setYest21_accuracy(v){
		this.Yest21_accuracy=v;
	}
	this.setYest21_accuracy=setYest21_accuracy;

	this.Yest21_responsetime=null;


	function getYest21_responsetime() {
		return this.Yest21_responsetime;
	}
	this.getYest21_responsetime=getYest21_responsetime;


	function setYest21_responsetime(v){
		this.Yest21_responsetime=v;
	}
	this.setYest21_responsetime=setYest21_responsetime;

	this.Yest22_accuracy=null;


	function getYest22_accuracy() {
		return this.Yest22_accuracy;
	}
	this.getYest22_accuracy=getYest22_accuracy;


	function setYest22_accuracy(v){
		this.Yest22_accuracy=v;
	}
	this.setYest22_accuracy=setYest22_accuracy;

	this.Yest22_responsetime=null;


	function getYest22_responsetime() {
		return this.Yest22_responsetime;
	}
	this.getYest22_responsetime=getYest22_responsetime;


	function setYest22_responsetime(v){
		this.Yest22_responsetime=v;
	}
	this.setYest22_responsetime=setYest22_responsetime;

	this.Yest23_accuracy=null;


	function getYest23_accuracy() {
		return this.Yest23_accuracy;
	}
	this.getYest23_accuracy=getYest23_accuracy;


	function setYest23_accuracy(v){
		this.Yest23_accuracy=v;
	}
	this.setYest23_accuracy=setYest23_accuracy;

	this.Yest23_responsetime=null;


	function getYest23_responsetime() {
		return this.Yest23_responsetime;
	}
	this.getYest23_responsetime=getYest23_responsetime;


	function setYest23_responsetime(v){
		this.Yest23_responsetime=v;
	}
	this.setYest23_responsetime=setYest23_responsetime;

	this.Yest24_accuracy=null;


	function getYest24_accuracy() {
		return this.Yest24_accuracy;
	}
	this.getYest24_accuracy=getYest24_accuracy;


	function setYest24_accuracy(v){
		this.Yest24_accuracy=v;
	}
	this.setYest24_accuracy=setYest24_accuracy;

	this.Yest24_responsetime=null;


	function getYest24_responsetime() {
		return this.Yest24_responsetime;
	}
	this.getYest24_responsetime=getYest24_responsetime;


	function setYest24_responsetime(v){
		this.Yest24_responsetime=v;
	}
	this.setYest24_responsetime=setYest24_responsetime;

	this.Yest25_accuracy=null;


	function getYest25_accuracy() {
		return this.Yest25_accuracy;
	}
	this.getYest25_accuracy=getYest25_accuracy;


	function setYest25_accuracy(v){
		this.Yest25_accuracy=v;
	}
	this.setYest25_accuracy=setYest25_accuracy;

	this.Yest25_responsetime=null;


	function getYest25_responsetime() {
		return this.Yest25_responsetime;
	}
	this.getYest25_responsetime=getYest25_responsetime;


	function setYest25_responsetime(v){
		this.Yest25_responsetime=v;
	}
	this.setYest25_responsetime=setYest25_responsetime;

	this.Yest26_accuracy=null;


	function getYest26_accuracy() {
		return this.Yest26_accuracy;
	}
	this.getYest26_accuracy=getYest26_accuracy;


	function setYest26_accuracy(v){
		this.Yest26_accuracy=v;
	}
	this.setYest26_accuracy=setYest26_accuracy;

	this.Yest26_responsetime=null;


	function getYest26_responsetime() {
		return this.Yest26_responsetime;
	}
	this.getYest26_responsetime=getYest26_responsetime;


	function setYest26_responsetime(v){
		this.Yest26_responsetime=v;
	}
	this.setYest26_responsetime=setYest26_responsetime;

	this.Yest27_accuracy=null;


	function getYest27_accuracy() {
		return this.Yest27_accuracy;
	}
	this.getYest27_accuracy=getYest27_accuracy;


	function setYest27_accuracy(v){
		this.Yest27_accuracy=v;
	}
	this.setYest27_accuracy=setYest27_accuracy;

	this.Yest27_responsetime=null;


	function getYest27_responsetime() {
		return this.Yest27_responsetime;
	}
	this.getYest27_responsetime=getYest27_responsetime;


	function setYest27_responsetime(v){
		this.Yest27_responsetime=v;
	}
	this.setYest27_responsetime=setYest27_responsetime;

	this.Yest28_accuracy=null;


	function getYest28_accuracy() {
		return this.Yest28_accuracy;
	}
	this.getYest28_accuracy=getYest28_accuracy;


	function setYest28_accuracy(v){
		this.Yest28_accuracy=v;
	}
	this.setYest28_accuracy=setYest28_accuracy;

	this.Yest28_responsetime=null;


	function getYest28_responsetime() {
		return this.Yest28_responsetime;
	}
	this.getYest28_responsetime=getYest28_responsetime;


	function setYest28_responsetime(v){
		this.Yest28_responsetime=v;
	}
	this.setYest28_responsetime=setYest28_responsetime;

	this.Yest29_accuracy=null;


	function getYest29_accuracy() {
		return this.Yest29_accuracy;
	}
	this.getYest29_accuracy=getYest29_accuracy;


	function setYest29_accuracy(v){
		this.Yest29_accuracy=v;
	}
	this.setYest29_accuracy=setYest29_accuracy;

	this.Yest29_responsetime=null;


	function getYest29_responsetime() {
		return this.Yest29_responsetime;
	}
	this.getYest29_responsetime=getYest29_responsetime;


	function setYest29_responsetime(v){
		this.Yest29_responsetime=v;
	}
	this.setYest29_responsetime=setYest29_responsetime;

	this.Yest30_accuracy=null;


	function getYest30_accuracy() {
		return this.Yest30_accuracy;
	}
	this.getYest30_accuracy=getYest30_accuracy;


	function setYest30_accuracy(v){
		this.Yest30_accuracy=v;
	}
	this.setYest30_accuracy=setYest30_accuracy;

	this.Yest30_responsetime=null;


	function getYest30_responsetime() {
		return this.Yest30_responsetime;
	}
	this.getYest30_responsetime=getYest30_responsetime;


	function setYest30_responsetime(v){
		this.Yest30_responsetime=v;
	}
	this.setYest30_responsetime=setYest30_responsetime;

	this.Yest31_accuracy=null;


	function getYest31_accuracy() {
		return this.Yest31_accuracy;
	}
	this.getYest31_accuracy=getYest31_accuracy;


	function setYest31_accuracy(v){
		this.Yest31_accuracy=v;
	}
	this.setYest31_accuracy=setYest31_accuracy;

	this.Yest31_responsetime=null;


	function getYest31_responsetime() {
		return this.Yest31_responsetime;
	}
	this.getYest31_responsetime=getYest31_responsetime;


	function setYest31_responsetime(v){
		this.Yest31_responsetime=v;
	}
	this.setYest31_responsetime=setYest31_responsetime;

	this.Yest32_accuracy=null;


	function getYest32_accuracy() {
		return this.Yest32_accuracy;
	}
	this.getYest32_accuracy=getYest32_accuracy;


	function setYest32_accuracy(v){
		this.Yest32_accuracy=v;
	}
	this.setYest32_accuracy=setYest32_accuracy;

	this.Yest32_responsetime=null;


	function getYest32_responsetime() {
		return this.Yest32_responsetime;
	}
	this.getYest32_responsetime=getYest32_responsetime;


	function setYest32_responsetime(v){
		this.Yest32_responsetime=v;
	}
	this.setYest32_responsetime=setYest32_responsetime;

	this.Yest33_accuracy=null;


	function getYest33_accuracy() {
		return this.Yest33_accuracy;
	}
	this.getYest33_accuracy=getYest33_accuracy;


	function setYest33_accuracy(v){
		this.Yest33_accuracy=v;
	}
	this.setYest33_accuracy=setYest33_accuracy;

	this.Yest33_responsetime=null;


	function getYest33_responsetime() {
		return this.Yest33_responsetime;
	}
	this.getYest33_responsetime=getYest33_responsetime;


	function setYest33_responsetime(v){
		this.Yest33_responsetime=v;
	}
	this.setYest33_responsetime=setYest33_responsetime;

	this.Yest34_accuracy=null;


	function getYest34_accuracy() {
		return this.Yest34_accuracy;
	}
	this.getYest34_accuracy=getYest34_accuracy;


	function setYest34_accuracy(v){
		this.Yest34_accuracy=v;
	}
	this.setYest34_accuracy=setYest34_accuracy;

	this.Yest34_responsetime=null;


	function getYest34_responsetime() {
		return this.Yest34_responsetime;
	}
	this.getYest34_responsetime=getYest34_responsetime;


	function setYest34_responsetime(v){
		this.Yest34_responsetime=v;
	}
	this.setYest34_responsetime=setYest34_responsetime;

	this.Yest35_accuracy=null;


	function getYest35_accuracy() {
		return this.Yest35_accuracy;
	}
	this.getYest35_accuracy=getYest35_accuracy;


	function setYest35_accuracy(v){
		this.Yest35_accuracy=v;
	}
	this.setYest35_accuracy=setYest35_accuracy;

	this.Yest35_responsetime=null;


	function getYest35_responsetime() {
		return this.Yest35_responsetime;
	}
	this.getYest35_responsetime=getYest35_responsetime;


	function setYest35_responsetime(v){
		this.Yest35_responsetime=v;
	}
	this.setYest35_responsetime=setYest35_responsetime;

	this.Yest36_accuracy=null;


	function getYest36_accuracy() {
		return this.Yest36_accuracy;
	}
	this.getYest36_accuracy=getYest36_accuracy;


	function setYest36_accuracy(v){
		this.Yest36_accuracy=v;
	}
	this.setYest36_accuracy=setYest36_accuracy;

	this.Yest36_responsetime=null;


	function getYest36_responsetime() {
		return this.Yest36_responsetime;
	}
	this.getYest36_responsetime=getYest36_responsetime;


	function setYest36_responsetime(v){
		this.Yest36_responsetime=v;
	}
	this.setYest36_responsetime=setYest36_responsetime;

	this.Yest37_accuracy=null;


	function getYest37_accuracy() {
		return this.Yest37_accuracy;
	}
	this.getYest37_accuracy=getYest37_accuracy;


	function setYest37_accuracy(v){
		this.Yest37_accuracy=v;
	}
	this.setYest37_accuracy=setYest37_accuracy;

	this.Yest37_responsetime=null;


	function getYest37_responsetime() {
		return this.Yest37_responsetime;
	}
	this.getYest37_responsetime=getYest37_responsetime;


	function setYest37_responsetime(v){
		this.Yest37_responsetime=v;
	}
	this.setYest37_responsetime=setYest37_responsetime;

	this.Yest38_accuracy=null;


	function getYest38_accuracy() {
		return this.Yest38_accuracy;
	}
	this.getYest38_accuracy=getYest38_accuracy;


	function setYest38_accuracy(v){
		this.Yest38_accuracy=v;
	}
	this.setYest38_accuracy=setYest38_accuracy;

	this.Yest38_responsetime=null;


	function getYest38_responsetime() {
		return this.Yest38_responsetime;
	}
	this.getYest38_responsetime=getYest38_responsetime;


	function setYest38_responsetime(v){
		this.Yest38_responsetime=v;
	}
	this.setYest38_responsetime=setYest38_responsetime;

	this.Yest39_accuracy=null;


	function getYest39_accuracy() {
		return this.Yest39_accuracy;
	}
	this.getYest39_accuracy=getYest39_accuracy;


	function setYest39_accuracy(v){
		this.Yest39_accuracy=v;
	}
	this.setYest39_accuracy=setYest39_accuracy;

	this.Yest39_responsetime=null;


	function getYest39_responsetime() {
		return this.Yest39_responsetime;
	}
	this.getYest39_responsetime=getYest39_responsetime;


	function setYest39_responsetime(v){
		this.Yest39_responsetime=v;
	}
	this.setYest39_responsetime=setYest39_responsetime;

	this.Yest40_accuracy=null;


	function getYest40_accuracy() {
		return this.Yest40_accuracy;
	}
	this.getYest40_accuracy=getYest40_accuracy;


	function setYest40_accuracy(v){
		this.Yest40_accuracy=v;
	}
	this.setYest40_accuracy=setYest40_accuracy;

	this.Yest40_responsetime=null;


	function getYest40_responsetime() {
		return this.Yest40_responsetime;
	}
	this.getYest40_responsetime=getYest40_responsetime;


	function setYest40_responsetime(v){
		this.Yest40_responsetime=v;
	}
	this.setYest40_responsetime=setYest40_responsetime;

	this.Yest41_accuracy=null;


	function getYest41_accuracy() {
		return this.Yest41_accuracy;
	}
	this.getYest41_accuracy=getYest41_accuracy;


	function setYest41_accuracy(v){
		this.Yest41_accuracy=v;
	}
	this.setYest41_accuracy=setYest41_accuracy;

	this.Yest41_responsetime=null;


	function getYest41_responsetime() {
		return this.Yest41_responsetime;
	}
	this.getYest41_responsetime=getYest41_responsetime;


	function setYest41_responsetime(v){
		this.Yest41_responsetime=v;
	}
	this.setYest41_responsetime=setYest41_responsetime;

	this.Yest42_accuracy=null;


	function getYest42_accuracy() {
		return this.Yest42_accuracy;
	}
	this.getYest42_accuracy=getYest42_accuracy;


	function setYest42_accuracy(v){
		this.Yest42_accuracy=v;
	}
	this.setYest42_accuracy=setYest42_accuracy;

	this.Yest42_responsetime=null;


	function getYest42_responsetime() {
		return this.Yest42_responsetime;
	}
	this.getYest42_responsetime=getYest42_responsetime;


	function setYest42_responsetime(v){
		this.Yest42_responsetime=v;
	}
	this.setYest42_responsetime=setYest42_responsetime;

	this.Yest43_accuracy=null;


	function getYest43_accuracy() {
		return this.Yest43_accuracy;
	}
	this.getYest43_accuracy=getYest43_accuracy;


	function setYest43_accuracy(v){
		this.Yest43_accuracy=v;
	}
	this.setYest43_accuracy=setYest43_accuracy;

	this.Yest43_responsetime=null;


	function getYest43_responsetime() {
		return this.Yest43_responsetime;
	}
	this.getYest43_responsetime=getYest43_responsetime;


	function setYest43_responsetime(v){
		this.Yest43_responsetime=v;
	}
	this.setYest43_responsetime=setYest43_responsetime;

	this.Yest44_accuracy=null;


	function getYest44_accuracy() {
		return this.Yest44_accuracy;
	}
	this.getYest44_accuracy=getYest44_accuracy;


	function setYest44_accuracy(v){
		this.Yest44_accuracy=v;
	}
	this.setYest44_accuracy=setYest44_accuracy;

	this.Yest44_responsetime=null;


	function getYest44_responsetime() {
		return this.Yest44_responsetime;
	}
	this.getYest44_responsetime=getYest44_responsetime;


	function setYest44_responsetime(v){
		this.Yest44_responsetime=v;
	}
	this.setYest44_responsetime=setYest44_responsetime;

	this.Yest45_accuracy=null;


	function getYest45_accuracy() {
		return this.Yest45_accuracy;
	}
	this.getYest45_accuracy=getYest45_accuracy;


	function setYest45_accuracy(v){
		this.Yest45_accuracy=v;
	}
	this.setYest45_accuracy=setYest45_accuracy;

	this.Yest45_responsetime=null;


	function getYest45_responsetime() {
		return this.Yest45_responsetime;
	}
	this.getYest45_responsetime=getYest45_responsetime;


	function setYest45_responsetime(v){
		this.Yest45_responsetime=v;
	}
	this.setYest45_responsetime=setYest45_responsetime;

	this.Yest46_accuracy=null;


	function getYest46_accuracy() {
		return this.Yest46_accuracy;
	}
	this.getYest46_accuracy=getYest46_accuracy;


	function setYest46_accuracy(v){
		this.Yest46_accuracy=v;
	}
	this.setYest46_accuracy=setYest46_accuracy;

	this.Yest46_responsetime=null;


	function getYest46_responsetime() {
		return this.Yest46_responsetime;
	}
	this.getYest46_responsetime=getYest46_responsetime;


	function setYest46_responsetime(v){
		this.Yest46_responsetime=v;
	}
	this.setYest46_responsetime=setYest46_responsetime;

	this.Yest47_accuracy=null;


	function getYest47_accuracy() {
		return this.Yest47_accuracy;
	}
	this.getYest47_accuracy=getYest47_accuracy;


	function setYest47_accuracy(v){
		this.Yest47_accuracy=v;
	}
	this.setYest47_accuracy=setYest47_accuracy;

	this.Yest47_responsetime=null;


	function getYest47_responsetime() {
		return this.Yest47_responsetime;
	}
	this.getYest47_responsetime=getYest47_responsetime;


	function setYest47_responsetime(v){
		this.Yest47_responsetime=v;
	}
	this.setYest47_responsetime=setYest47_responsetime;

	this.Yest48_accuracy=null;


	function getYest48_accuracy() {
		return this.Yest48_accuracy;
	}
	this.getYest48_accuracy=getYest48_accuracy;


	function setYest48_accuracy(v){
		this.Yest48_accuracy=v;
	}
	this.setYest48_accuracy=setYest48_accuracy;

	this.Yest48_responsetime=null;


	function getYest48_responsetime() {
		return this.Yest48_responsetime;
	}
	this.getYest48_responsetime=getYest48_responsetime;


	function setYest48_responsetime(v){
		this.Yest48_responsetime=v;
	}
	this.setYest48_responsetime=setYest48_responsetime;

	this.Yest49_accuracy=null;


	function getYest49_accuracy() {
		return this.Yest49_accuracy;
	}
	this.getYest49_accuracy=getYest49_accuracy;


	function setYest49_accuracy(v){
		this.Yest49_accuracy=v;
	}
	this.setYest49_accuracy=setYest49_accuracy;

	this.Yest49_responsetime=null;


	function getYest49_responsetime() {
		return this.Yest49_responsetime;
	}
	this.getYest49_responsetime=getYest49_responsetime;


	function setYest49_responsetime(v){
		this.Yest49_responsetime=v;
	}
	this.setYest49_responsetime=setYest49_responsetime;

	this.Yest50_accuracy=null;


	function getYest50_accuracy() {
		return this.Yest50_accuracy;
	}
	this.getYest50_accuracy=getYest50_accuracy;


	function setYest50_accuracy(v){
		this.Yest50_accuracy=v;
	}
	this.setYest50_accuracy=setYest50_accuracy;

	this.Yest50_responsetime=null;


	function getYest50_responsetime() {
		return this.Yest50_responsetime;
	}
	this.getYest50_responsetime=getYest50_responsetime;


	function setYest50_responsetime(v){
		this.Yest50_responsetime=v;
	}
	this.setYest50_responsetime=setYest50_responsetime;

	this.Yest51_accuracy=null;


	function getYest51_accuracy() {
		return this.Yest51_accuracy;
	}
	this.getYest51_accuracy=getYest51_accuracy;


	function setYest51_accuracy(v){
		this.Yest51_accuracy=v;
	}
	this.setYest51_accuracy=setYest51_accuracy;

	this.Yest51_responsetime=null;


	function getYest51_responsetime() {
		return this.Yest51_responsetime;
	}
	this.getYest51_responsetime=getYest51_responsetime;


	function setYest51_responsetime(v){
		this.Yest51_responsetime=v;
	}
	this.setYest51_responsetime=setYest51_responsetime;

	this.Yest52_accuracy=null;


	function getYest52_accuracy() {
		return this.Yest52_accuracy;
	}
	this.getYest52_accuracy=getYest52_accuracy;


	function setYest52_accuracy(v){
		this.Yest52_accuracy=v;
	}
	this.setYest52_accuracy=setYest52_accuracy;

	this.Yest52_responsetime=null;


	function getYest52_responsetime() {
		return this.Yest52_responsetime;
	}
	this.getYest52_responsetime=getYest52_responsetime;


	function setYest52_responsetime(v){
		this.Yest52_responsetime=v;
	}
	this.setYest52_responsetime=setYest52_responsetime;

	this.Yest53_accuracy=null;


	function getYest53_accuracy() {
		return this.Yest53_accuracy;
	}
	this.getYest53_accuracy=getYest53_accuracy;


	function setYest53_accuracy(v){
		this.Yest53_accuracy=v;
	}
	this.setYest53_accuracy=setYest53_accuracy;

	this.Yest53_responsetime=null;


	function getYest53_responsetime() {
		return this.Yest53_responsetime;
	}
	this.getYest53_responsetime=getYest53_responsetime;


	function setYest53_responsetime(v){
		this.Yest53_responsetime=v;
	}
	this.setYest53_responsetime=setYest53_responsetime;

	this.Yest54_accuracy=null;


	function getYest54_accuracy() {
		return this.Yest54_accuracy;
	}
	this.getYest54_accuracy=getYest54_accuracy;


	function setYest54_accuracy(v){
		this.Yest54_accuracy=v;
	}
	this.setYest54_accuracy=setYest54_accuracy;

	this.Yest54_responsetime=null;


	function getYest54_responsetime() {
		return this.Yest54_responsetime;
	}
	this.getYest54_responsetime=getYest54_responsetime;


	function setYest54_responsetime(v){
		this.Yest54_responsetime=v;
	}
	this.setYest54_responsetime=setYest54_responsetime;

	this.Yest55_accuracy=null;


	function getYest55_accuracy() {
		return this.Yest55_accuracy;
	}
	this.getYest55_accuracy=getYest55_accuracy;


	function setYest55_accuracy(v){
		this.Yest55_accuracy=v;
	}
	this.setYest55_accuracy=setYest55_accuracy;

	this.Yest55_responsetime=null;


	function getYest55_responsetime() {
		return this.Yest55_responsetime;
	}
	this.getYest55_responsetime=getYest55_responsetime;


	function setYest55_responsetime(v){
		this.Yest55_responsetime=v;
	}
	this.setYest55_responsetime=setYest55_responsetime;

	this.Yest56_accuracy=null;


	function getYest56_accuracy() {
		return this.Yest56_accuracy;
	}
	this.getYest56_accuracy=getYest56_accuracy;


	function setYest56_accuracy(v){
		this.Yest56_accuracy=v;
	}
	this.setYest56_accuracy=setYest56_accuracy;

	this.Yest56_responsetime=null;


	function getYest56_responsetime() {
		return this.Yest56_responsetime;
	}
	this.getYest56_responsetime=getYest56_responsetime;


	function setYest56_responsetime(v){
		this.Yest56_responsetime=v;
	}
	this.setYest56_responsetime=setYest56_responsetime;

	this.Yest57_accuracy=null;


	function getYest57_accuracy() {
		return this.Yest57_accuracy;
	}
	this.getYest57_accuracy=getYest57_accuracy;


	function setYest57_accuracy(v){
		this.Yest57_accuracy=v;
	}
	this.setYest57_accuracy=setYest57_accuracy;

	this.Yest57_responsetime=null;


	function getYest57_responsetime() {
		return this.Yest57_responsetime;
	}
	this.getYest57_responsetime=getYest57_responsetime;


	function setYest57_responsetime(v){
		this.Yest57_responsetime=v;
	}
	this.setYest57_responsetime=setYest57_responsetime;

	this.Yest58_accuracy=null;


	function getYest58_accuracy() {
		return this.Yest58_accuracy;
	}
	this.getYest58_accuracy=getYest58_accuracy;


	function setYest58_accuracy(v){
		this.Yest58_accuracy=v;
	}
	this.setYest58_accuracy=setYest58_accuracy;

	this.Yest58_responsetime=null;


	function getYest58_responsetime() {
		return this.Yest58_responsetime;
	}
	this.getYest58_responsetime=getYest58_responsetime;


	function setYest58_responsetime(v){
		this.Yest58_responsetime=v;
	}
	this.setYest58_responsetime=setYest58_responsetime;

	this.Yest59_accuracy=null;


	function getYest59_accuracy() {
		return this.Yest59_accuracy;
	}
	this.getYest59_accuracy=getYest59_accuracy;


	function setYest59_accuracy(v){
		this.Yest59_accuracy=v;
	}
	this.setYest59_accuracy=setYest59_accuracy;

	this.Yest59_responsetime=null;


	function getYest59_responsetime() {
		return this.Yest59_responsetime;
	}
	this.getYest59_responsetime=getYest59_responsetime;


	function setYest59_responsetime(v){
		this.Yest59_responsetime=v;
	}
	this.setYest59_responsetime=setYest59_responsetime;

	this.Yest60_accuracy=null;


	function getYest60_accuracy() {
		return this.Yest60_accuracy;
	}
	this.getYest60_accuracy=getYest60_accuracy;


	function setYest60_accuracy(v){
		this.Yest60_accuracy=v;
	}
	this.setYest60_accuracy=setYest60_accuracy;

	this.Yest60_responsetime=null;


	function getYest60_responsetime() {
		return this.Yest60_responsetime;
	}
	this.getYest60_responsetime=getYest60_responsetime;


	function setYest60_responsetime(v){
		this.Yest60_responsetime=v;
	}
	this.setYest60_responsetime=setYest60_responsetime;

	this.Yest61_accuracy=null;


	function getYest61_accuracy() {
		return this.Yest61_accuracy;
	}
	this.getYest61_accuracy=getYest61_accuracy;


	function setYest61_accuracy(v){
		this.Yest61_accuracy=v;
	}
	this.setYest61_accuracy=setYest61_accuracy;

	this.Yest61_responsetime=null;


	function getYest61_responsetime() {
		return this.Yest61_responsetime;
	}
	this.getYest61_responsetime=getYest61_responsetime;


	function setYest61_responsetime(v){
		this.Yest61_responsetime=v;
	}
	this.setYest61_responsetime=setYest61_responsetime;

	this.Yest62_accuracy=null;


	function getYest62_accuracy() {
		return this.Yest62_accuracy;
	}
	this.getYest62_accuracy=getYest62_accuracy;


	function setYest62_accuracy(v){
		this.Yest62_accuracy=v;
	}
	this.setYest62_accuracy=setYest62_accuracy;

	this.Yest62_responsetime=null;


	function getYest62_responsetime() {
		return this.Yest62_responsetime;
	}
	this.getYest62_responsetime=getYest62_responsetime;


	function setYest62_responsetime(v){
		this.Yest62_responsetime=v;
	}
	this.setYest62_responsetime=setYest62_responsetime;

	this.Yest63_accuracy=null;


	function getYest63_accuracy() {
		return this.Yest63_accuracy;
	}
	this.getYest63_accuracy=getYest63_accuracy;


	function setYest63_accuracy(v){
		this.Yest63_accuracy=v;
	}
	this.setYest63_accuracy=setYest63_accuracy;

	this.Yest63_responsetime=null;


	function getYest63_responsetime() {
		return this.Yest63_responsetime;
	}
	this.getYest63_responsetime=getYest63_responsetime;


	function setYest63_responsetime(v){
		this.Yest63_responsetime=v;
	}
	this.setYest63_responsetime=setYest63_responsetime;

	this.Yest64_accuracy=null;


	function getYest64_accuracy() {
		return this.Yest64_accuracy;
	}
	this.getYest64_accuracy=getYest64_accuracy;


	function setYest64_accuracy(v){
		this.Yest64_accuracy=v;
	}
	this.setYest64_accuracy=setYest64_accuracy;

	this.Yest64_responsetime=null;


	function getYest64_responsetime() {
		return this.Yest64_responsetime;
	}
	this.getYest64_responsetime=getYest64_responsetime;


	function setYest64_responsetime(v){
		this.Yest64_responsetime=v;
	}
	this.setYest64_responsetime=setYest64_responsetime;

	this.Yest65_accuracy=null;


	function getYest65_accuracy() {
		return this.Yest65_accuracy;
	}
	this.getYest65_accuracy=getYest65_accuracy;


	function setYest65_accuracy(v){
		this.Yest65_accuracy=v;
	}
	this.setYest65_accuracy=setYest65_accuracy;

	this.Yest65_responsetime=null;


	function getYest65_responsetime() {
		return this.Yest65_responsetime;
	}
	this.getYest65_responsetime=getYest65_responsetime;


	function setYest65_responsetime(v){
		this.Yest65_responsetime=v;
	}
	this.setYest65_responsetime=setYest65_responsetime;

	this.Yest66_accuracy=null;


	function getYest66_accuracy() {
		return this.Yest66_accuracy;
	}
	this.getYest66_accuracy=getYest66_accuracy;


	function setYest66_accuracy(v){
		this.Yest66_accuracy=v;
	}
	this.setYest66_accuracy=setYest66_accuracy;

	this.Yest66_responsetime=null;


	function getYest66_responsetime() {
		return this.Yest66_responsetime;
	}
	this.getYest66_responsetime=getYest66_responsetime;


	function setYest66_responsetime(v){
		this.Yest66_responsetime=v;
	}
	this.setYest66_responsetime=setYest66_responsetime;

	this.Yest67_accuracy=null;


	function getYest67_accuracy() {
		return this.Yest67_accuracy;
	}
	this.getYest67_accuracy=getYest67_accuracy;


	function setYest67_accuracy(v){
		this.Yest67_accuracy=v;
	}
	this.setYest67_accuracy=setYest67_accuracy;

	this.Yest67_responsetime=null;


	function getYest67_responsetime() {
		return this.Yest67_responsetime;
	}
	this.getYest67_responsetime=getYest67_responsetime;


	function setYest67_responsetime(v){
		this.Yest67_responsetime=v;
	}
	this.setYest67_responsetime=setYest67_responsetime;

	this.Yest68_accuracy=null;


	function getYest68_accuracy() {
		return this.Yest68_accuracy;
	}
	this.getYest68_accuracy=getYest68_accuracy;


	function setYest68_accuracy(v){
		this.Yest68_accuracy=v;
	}
	this.setYest68_accuracy=setYest68_accuracy;

	this.Yest68_responsetime=null;


	function getYest68_responsetime() {
		return this.Yest68_responsetime;
	}
	this.getYest68_responsetime=getYest68_responsetime;


	function setYest68_responsetime(v){
		this.Yest68_responsetime=v;
	}
	this.setYest68_responsetime=setYest68_responsetime;

	this.Yest69_accuracy=null;


	function getYest69_accuracy() {
		return this.Yest69_accuracy;
	}
	this.getYest69_accuracy=getYest69_accuracy;


	function setYest69_accuracy(v){
		this.Yest69_accuracy=v;
	}
	this.setYest69_accuracy=setYest69_accuracy;

	this.Yest69_responsetime=null;


	function getYest69_responsetime() {
		return this.Yest69_responsetime;
	}
	this.getYest69_responsetime=getYest69_responsetime;


	function setYest69_responsetime(v){
		this.Yest69_responsetime=v;
	}
	this.setYest69_responsetime=setYest69_responsetime;

	this.Yest70_accuracy=null;


	function getYest70_accuracy() {
		return this.Yest70_accuracy;
	}
	this.getYest70_accuracy=getYest70_accuracy;


	function setYest70_accuracy(v){
		this.Yest70_accuracy=v;
	}
	this.setYest70_accuracy=setYest70_accuracy;

	this.Yest70_responsetime=null;


	function getYest70_responsetime() {
		return this.Yest70_responsetime;
	}
	this.getYest70_responsetime=getYest70_responsetime;


	function setYest70_responsetime(v){
		this.Yest70_responsetime=v;
	}
	this.setYest70_responsetime=setYest70_responsetime;

	this.Yest71_accuracy=null;


	function getYest71_accuracy() {
		return this.Yest71_accuracy;
	}
	this.getYest71_accuracy=getYest71_accuracy;


	function setYest71_accuracy(v){
		this.Yest71_accuracy=v;
	}
	this.setYest71_accuracy=setYest71_accuracy;

	this.Yest71_responsetime=null;


	function getYest71_responsetime() {
		return this.Yest71_responsetime;
	}
	this.getYest71_responsetime=getYest71_responsetime;


	function setYest71_responsetime(v){
		this.Yest71_responsetime=v;
	}
	this.setYest71_responsetime=setYest71_responsetime;

	this.Yest72_accuracy=null;


	function getYest72_accuracy() {
		return this.Yest72_accuracy;
	}
	this.getYest72_accuracy=getYest72_accuracy;


	function setYest72_accuracy(v){
		this.Yest72_accuracy=v;
	}
	this.setYest72_accuracy=setYest72_accuracy;

	this.Yest72_responsetime=null;


	function getYest72_responsetime() {
		return this.Yest72_responsetime;
	}
	this.getYest72_responsetime=getYest72_responsetime;


	function setYest72_responsetime(v){
		this.Yest72_responsetime=v;
	}
	this.setYest72_responsetime=setYest72_responsetime;

	this.Yest73_accuracy=null;


	function getYest73_accuracy() {
		return this.Yest73_accuracy;
	}
	this.getYest73_accuracy=getYest73_accuracy;


	function setYest73_accuracy(v){
		this.Yest73_accuracy=v;
	}
	this.setYest73_accuracy=setYest73_accuracy;

	this.Yest73_responsetime=null;


	function getYest73_responsetime() {
		return this.Yest73_responsetime;
	}
	this.getYest73_responsetime=getYest73_responsetime;


	function setYest73_responsetime(v){
		this.Yest73_responsetime=v;
	}
	this.setYest73_responsetime=setYest73_responsetime;

	this.Yest74_accuracy=null;


	function getYest74_accuracy() {
		return this.Yest74_accuracy;
	}
	this.getYest74_accuracy=getYest74_accuracy;


	function setYest74_accuracy(v){
		this.Yest74_accuracy=v;
	}
	this.setYest74_accuracy=setYest74_accuracy;

	this.Yest74_responsetime=null;


	function getYest74_responsetime() {
		return this.Yest74_responsetime;
	}
	this.getYest74_responsetime=getYest74_responsetime;


	function setYest74_responsetime(v){
		this.Yest74_responsetime=v;
	}
	this.setYest74_responsetime=setYest74_responsetime;

	this.Yest75_accuracy=null;


	function getYest75_accuracy() {
		return this.Yest75_accuracy;
	}
	this.getYest75_accuracy=getYest75_accuracy;


	function setYest75_accuracy(v){
		this.Yest75_accuracy=v;
	}
	this.setYest75_accuracy=setYest75_accuracy;

	this.Yest75_responsetime=null;


	function getYest75_responsetime() {
		return this.Yest75_responsetime;
	}
	this.getYest75_responsetime=getYest75_responsetime;


	function setYest75_responsetime(v){
		this.Yest75_responsetime=v;
	}
	this.setYest75_responsetime=setYest75_responsetime;

	this.Yest76_accuracy=null;


	function getYest76_accuracy() {
		return this.Yest76_accuracy;
	}
	this.getYest76_accuracy=getYest76_accuracy;


	function setYest76_accuracy(v){
		this.Yest76_accuracy=v;
	}
	this.setYest76_accuracy=setYest76_accuracy;

	this.Yest76_responsetime=null;


	function getYest76_responsetime() {
		return this.Yest76_responsetime;
	}
	this.getYest76_responsetime=getYest76_responsetime;


	function setYest76_responsetime(v){
		this.Yest76_responsetime=v;
	}
	this.setYest76_responsetime=setYest76_responsetime;

	this.Yest77_accuracy=null;


	function getYest77_accuracy() {
		return this.Yest77_accuracy;
	}
	this.getYest77_accuracy=getYest77_accuracy;


	function setYest77_accuracy(v){
		this.Yest77_accuracy=v;
	}
	this.setYest77_accuracy=setYest77_accuracy;

	this.Yest77_responsetime=null;


	function getYest77_responsetime() {
		return this.Yest77_responsetime;
	}
	this.getYest77_responsetime=getYest77_responsetime;


	function setYest77_responsetime(v){
		this.Yest77_responsetime=v;
	}
	this.setYest77_responsetime=setYest77_responsetime;

	this.Yest78_accuracy=null;


	function getYest78_accuracy() {
		return this.Yest78_accuracy;
	}
	this.getYest78_accuracy=getYest78_accuracy;


	function setYest78_accuracy(v){
		this.Yest78_accuracy=v;
	}
	this.setYest78_accuracy=setYest78_accuracy;

	this.Yest78_responsetime=null;


	function getYest78_responsetime() {
		return this.Yest78_responsetime;
	}
	this.getYest78_responsetime=getYest78_responsetime;


	function setYest78_responsetime(v){
		this.Yest78_responsetime=v;
	}
	this.setYest78_responsetime=setYest78_responsetime;

	this.Yest79_accuracy=null;


	function getYest79_accuracy() {
		return this.Yest79_accuracy;
	}
	this.getYest79_accuracy=getYest79_accuracy;


	function setYest79_accuracy(v){
		this.Yest79_accuracy=v;
	}
	this.setYest79_accuracy=setYest79_accuracy;

	this.Yest79_responsetime=null;


	function getYest79_responsetime() {
		return this.Yest79_responsetime;
	}
	this.getYest79_responsetime=getYest79_responsetime;


	function setYest79_responsetime(v){
		this.Yest79_responsetime=v;
	}
	this.setYest79_responsetime=setYest79_responsetime;

	this.Yest80_accuracy=null;


	function getYest80_accuracy() {
		return this.Yest80_accuracy;
	}
	this.getYest80_accuracy=getYest80_accuracy;


	function setYest80_accuracy(v){
		this.Yest80_accuracy=v;
	}
	this.setYest80_accuracy=setYest80_accuracy;

	this.Yest80_responsetime=null;


	function getYest80_responsetime() {
		return this.Yest80_responsetime;
	}
	this.getYest80_responsetime=getYest80_responsetime;


	function setYest80_responsetime(v){
		this.Yest80_responsetime=v;
	}
	this.setYest80_responsetime=setYest80_responsetime;

	this.Not1_accuracy=null;


	function getNot1_accuracy() {
		return this.Not1_accuracy;
	}
	this.getNot1_accuracy=getNot1_accuracy;


	function setNot1_accuracy(v){
		this.Not1_accuracy=v;
	}
	this.setNot1_accuracy=setNot1_accuracy;

	this.Not1_responsetime=null;


	function getNot1_responsetime() {
		return this.Not1_responsetime;
	}
	this.getNot1_responsetime=getNot1_responsetime;


	function setNot1_responsetime(v){
		this.Not1_responsetime=v;
	}
	this.setNot1_responsetime=setNot1_responsetime;

	this.Not2_accuracy=null;


	function getNot2_accuracy() {
		return this.Not2_accuracy;
	}
	this.getNot2_accuracy=getNot2_accuracy;


	function setNot2_accuracy(v){
		this.Not2_accuracy=v;
	}
	this.setNot2_accuracy=setNot2_accuracy;

	this.Not2_responsetime=null;


	function getNot2_responsetime() {
		return this.Not2_responsetime;
	}
	this.getNot2_responsetime=getNot2_responsetime;


	function setNot2_responsetime(v){
		this.Not2_responsetime=v;
	}
	this.setNot2_responsetime=setNot2_responsetime;

	this.Not3_accuracy=null;


	function getNot3_accuracy() {
		return this.Not3_accuracy;
	}
	this.getNot3_accuracy=getNot3_accuracy;


	function setNot3_accuracy(v){
		this.Not3_accuracy=v;
	}
	this.setNot3_accuracy=setNot3_accuracy;

	this.Not3_responsetime=null;


	function getNot3_responsetime() {
		return this.Not3_responsetime;
	}
	this.getNot3_responsetime=getNot3_responsetime;


	function setNot3_responsetime(v){
		this.Not3_responsetime=v;
	}
	this.setNot3_responsetime=setNot3_responsetime;

	this.Not4_accuracy=null;


	function getNot4_accuracy() {
		return this.Not4_accuracy;
	}
	this.getNot4_accuracy=getNot4_accuracy;


	function setNot4_accuracy(v){
		this.Not4_accuracy=v;
	}
	this.setNot4_accuracy=setNot4_accuracy;

	this.Not4_responsetime=null;


	function getNot4_responsetime() {
		return this.Not4_responsetime;
	}
	this.getNot4_responsetime=getNot4_responsetime;


	function setNot4_responsetime(v){
		this.Not4_responsetime=v;
	}
	this.setNot4_responsetime=setNot4_responsetime;

	this.Not5_accuracy=null;


	function getNot5_accuracy() {
		return this.Not5_accuracy;
	}
	this.getNot5_accuracy=getNot5_accuracy;


	function setNot5_accuracy(v){
		this.Not5_accuracy=v;
	}
	this.setNot5_accuracy=setNot5_accuracy;

	this.Not5_responsetime=null;


	function getNot5_responsetime() {
		return this.Not5_responsetime;
	}
	this.getNot5_responsetime=getNot5_responsetime;


	function setNot5_responsetime(v){
		this.Not5_responsetime=v;
	}
	this.setNot5_responsetime=setNot5_responsetime;

	this.Not6_accuracy=null;


	function getNot6_accuracy() {
		return this.Not6_accuracy;
	}
	this.getNot6_accuracy=getNot6_accuracy;


	function setNot6_accuracy(v){
		this.Not6_accuracy=v;
	}
	this.setNot6_accuracy=setNot6_accuracy;

	this.Not6_responsetime=null;


	function getNot6_responsetime() {
		return this.Not6_responsetime;
	}
	this.getNot6_responsetime=getNot6_responsetime;


	function setNot6_responsetime(v){
		this.Not6_responsetime=v;
	}
	this.setNot6_responsetime=setNot6_responsetime;

	this.Not7_accuracy=null;


	function getNot7_accuracy() {
		return this.Not7_accuracy;
	}
	this.getNot7_accuracy=getNot7_accuracy;


	function setNot7_accuracy(v){
		this.Not7_accuracy=v;
	}
	this.setNot7_accuracy=setNot7_accuracy;

	this.Not7_responsetime=null;


	function getNot7_responsetime() {
		return this.Not7_responsetime;
	}
	this.getNot7_responsetime=getNot7_responsetime;


	function setNot7_responsetime(v){
		this.Not7_responsetime=v;
	}
	this.setNot7_responsetime=setNot7_responsetime;

	this.Not8_accuracy=null;


	function getNot8_accuracy() {
		return this.Not8_accuracy;
	}
	this.getNot8_accuracy=getNot8_accuracy;


	function setNot8_accuracy(v){
		this.Not8_accuracy=v;
	}
	this.setNot8_accuracy=setNot8_accuracy;

	this.Not8_responsetime=null;


	function getNot8_responsetime() {
		return this.Not8_responsetime;
	}
	this.getNot8_responsetime=getNot8_responsetime;


	function setNot8_responsetime(v){
		this.Not8_responsetime=v;
	}
	this.setNot8_responsetime=setNot8_responsetime;

	this.Not9_accuracy=null;


	function getNot9_accuracy() {
		return this.Not9_accuracy;
	}
	this.getNot9_accuracy=getNot9_accuracy;


	function setNot9_accuracy(v){
		this.Not9_accuracy=v;
	}
	this.setNot9_accuracy=setNot9_accuracy;

	this.Not9_responsetime=null;


	function getNot9_responsetime() {
		return this.Not9_responsetime;
	}
	this.getNot9_responsetime=getNot9_responsetime;


	function setNot9_responsetime(v){
		this.Not9_responsetime=v;
	}
	this.setNot9_responsetime=setNot9_responsetime;

	this.Not10_accuracy=null;


	function getNot10_accuracy() {
		return this.Not10_accuracy;
	}
	this.getNot10_accuracy=getNot10_accuracy;


	function setNot10_accuracy(v){
		this.Not10_accuracy=v;
	}
	this.setNot10_accuracy=setNot10_accuracy;

	this.Not10_responsetime=null;


	function getNot10_responsetime() {
		return this.Not10_responsetime;
	}
	this.getNot10_responsetime=getNot10_responsetime;


	function setNot10_responsetime(v){
		this.Not10_responsetime=v;
	}
	this.setNot10_responsetime=setNot10_responsetime;

	this.Not11_accuracy=null;


	function getNot11_accuracy() {
		return this.Not11_accuracy;
	}
	this.getNot11_accuracy=getNot11_accuracy;


	function setNot11_accuracy(v){
		this.Not11_accuracy=v;
	}
	this.setNot11_accuracy=setNot11_accuracy;

	this.Not11_responsetime=null;


	function getNot11_responsetime() {
		return this.Not11_responsetime;
	}
	this.getNot11_responsetime=getNot11_responsetime;


	function setNot11_responsetime(v){
		this.Not11_responsetime=v;
	}
	this.setNot11_responsetime=setNot11_responsetime;

	this.Not12_accuracy=null;


	function getNot12_accuracy() {
		return this.Not12_accuracy;
	}
	this.getNot12_accuracy=getNot12_accuracy;


	function setNot12_accuracy(v){
		this.Not12_accuracy=v;
	}
	this.setNot12_accuracy=setNot12_accuracy;

	this.Not12_responsetime=null;


	function getNot12_responsetime() {
		return this.Not12_responsetime;
	}
	this.getNot12_responsetime=getNot12_responsetime;


	function setNot12_responsetime(v){
		this.Not12_responsetime=v;
	}
	this.setNot12_responsetime=setNot12_responsetime;

	this.Not13_accuracy=null;


	function getNot13_accuracy() {
		return this.Not13_accuracy;
	}
	this.getNot13_accuracy=getNot13_accuracy;


	function setNot13_accuracy(v){
		this.Not13_accuracy=v;
	}
	this.setNot13_accuracy=setNot13_accuracy;

	this.Not13_responsetime=null;


	function getNot13_responsetime() {
		return this.Not13_responsetime;
	}
	this.getNot13_responsetime=getNot13_responsetime;


	function setNot13_responsetime(v){
		this.Not13_responsetime=v;
	}
	this.setNot13_responsetime=setNot13_responsetime;

	this.Not14_accuracy=null;


	function getNot14_accuracy() {
		return this.Not14_accuracy;
	}
	this.getNot14_accuracy=getNot14_accuracy;


	function setNot14_accuracy(v){
		this.Not14_accuracy=v;
	}
	this.setNot14_accuracy=setNot14_accuracy;

	this.Not14_responsetime=null;


	function getNot14_responsetime() {
		return this.Not14_responsetime;
	}
	this.getNot14_responsetime=getNot14_responsetime;


	function setNot14_responsetime(v){
		this.Not14_responsetime=v;
	}
	this.setNot14_responsetime=setNot14_responsetime;

	this.Not15_accuracy=null;


	function getNot15_accuracy() {
		return this.Not15_accuracy;
	}
	this.getNot15_accuracy=getNot15_accuracy;


	function setNot15_accuracy(v){
		this.Not15_accuracy=v;
	}
	this.setNot15_accuracy=setNot15_accuracy;

	this.Not15_responsetime=null;


	function getNot15_responsetime() {
		return this.Not15_responsetime;
	}
	this.getNot15_responsetime=getNot15_responsetime;


	function setNot15_responsetime(v){
		this.Not15_responsetime=v;
	}
	this.setNot15_responsetime=setNot15_responsetime;

	this.Not16_accuracy=null;


	function getNot16_accuracy() {
		return this.Not16_accuracy;
	}
	this.getNot16_accuracy=getNot16_accuracy;


	function setNot16_accuracy(v){
		this.Not16_accuracy=v;
	}
	this.setNot16_accuracy=setNot16_accuracy;

	this.Not16_responsetime=null;


	function getNot16_responsetime() {
		return this.Not16_responsetime;
	}
	this.getNot16_responsetime=getNot16_responsetime;


	function setNot16_responsetime(v){
		this.Not16_responsetime=v;
	}
	this.setNot16_responsetime=setNot16_responsetime;

	this.Not17_accuracy=null;


	function getNot17_accuracy() {
		return this.Not17_accuracy;
	}
	this.getNot17_accuracy=getNot17_accuracy;


	function setNot17_accuracy(v){
		this.Not17_accuracy=v;
	}
	this.setNot17_accuracy=setNot17_accuracy;

	this.Not17_responsetime=null;


	function getNot17_responsetime() {
		return this.Not17_responsetime;
	}
	this.getNot17_responsetime=getNot17_responsetime;


	function setNot17_responsetime(v){
		this.Not17_responsetime=v;
	}
	this.setNot17_responsetime=setNot17_responsetime;

	this.Not18_accuracy=null;


	function getNot18_accuracy() {
		return this.Not18_accuracy;
	}
	this.getNot18_accuracy=getNot18_accuracy;


	function setNot18_accuracy(v){
		this.Not18_accuracy=v;
	}
	this.setNot18_accuracy=setNot18_accuracy;

	this.Not18_responsetime=null;


	function getNot18_responsetime() {
		return this.Not18_responsetime;
	}
	this.getNot18_responsetime=getNot18_responsetime;


	function setNot18_responsetime(v){
		this.Not18_responsetime=v;
	}
	this.setNot18_responsetime=setNot18_responsetime;

	this.Not19_accuracy=null;


	function getNot19_accuracy() {
		return this.Not19_accuracy;
	}
	this.getNot19_accuracy=getNot19_accuracy;


	function setNot19_accuracy(v){
		this.Not19_accuracy=v;
	}
	this.setNot19_accuracy=setNot19_accuracy;

	this.Not19_responsetime=null;


	function getNot19_responsetime() {
		return this.Not19_responsetime;
	}
	this.getNot19_responsetime=getNot19_responsetime;


	function setNot19_responsetime(v){
		this.Not19_responsetime=v;
	}
	this.setNot19_responsetime=setNot19_responsetime;

	this.Not20_accuracy=null;


	function getNot20_accuracy() {
		return this.Not20_accuracy;
	}
	this.getNot20_accuracy=getNot20_accuracy;


	function setNot20_accuracy(v){
		this.Not20_accuracy=v;
	}
	this.setNot20_accuracy=setNot20_accuracy;

	this.Not20_responsetime=null;


	function getNot20_responsetime() {
		return this.Not20_responsetime;
	}
	this.getNot20_responsetime=getNot20_responsetime;


	function setNot20_responsetime(v){
		this.Not20_responsetime=v;
	}
	this.setNot20_responsetime=setNot20_responsetime;

	this.Not21_accuracy=null;


	function getNot21_accuracy() {
		return this.Not21_accuracy;
	}
	this.getNot21_accuracy=getNot21_accuracy;


	function setNot21_accuracy(v){
		this.Not21_accuracy=v;
	}
	this.setNot21_accuracy=setNot21_accuracy;

	this.Not21_responsetime=null;


	function getNot21_responsetime() {
		return this.Not21_responsetime;
	}
	this.getNot21_responsetime=getNot21_responsetime;


	function setNot21_responsetime(v){
		this.Not21_responsetime=v;
	}
	this.setNot21_responsetime=setNot21_responsetime;

	this.Not22_accuracy=null;


	function getNot22_accuracy() {
		return this.Not22_accuracy;
	}
	this.getNot22_accuracy=getNot22_accuracy;


	function setNot22_accuracy(v){
		this.Not22_accuracy=v;
	}
	this.setNot22_accuracy=setNot22_accuracy;

	this.Not22_responsetime=null;


	function getNot22_responsetime() {
		return this.Not22_responsetime;
	}
	this.getNot22_responsetime=getNot22_responsetime;


	function setNot22_responsetime(v){
		this.Not22_responsetime=v;
	}
	this.setNot22_responsetime=setNot22_responsetime;

	this.Not23_accuracy=null;


	function getNot23_accuracy() {
		return this.Not23_accuracy;
	}
	this.getNot23_accuracy=getNot23_accuracy;


	function setNot23_accuracy(v){
		this.Not23_accuracy=v;
	}
	this.setNot23_accuracy=setNot23_accuracy;

	this.Not23_responsetime=null;


	function getNot23_responsetime() {
		return this.Not23_responsetime;
	}
	this.getNot23_responsetime=getNot23_responsetime;


	function setNot23_responsetime(v){
		this.Not23_responsetime=v;
	}
	this.setNot23_responsetime=setNot23_responsetime;

	this.Not24_accuracy=null;


	function getNot24_accuracy() {
		return this.Not24_accuracy;
	}
	this.getNot24_accuracy=getNot24_accuracy;


	function setNot24_accuracy(v){
		this.Not24_accuracy=v;
	}
	this.setNot24_accuracy=setNot24_accuracy;

	this.Not24_responsetime=null;


	function getNot24_responsetime() {
		return this.Not24_responsetime;
	}
	this.getNot24_responsetime=getNot24_responsetime;


	function setNot24_responsetime(v){
		this.Not24_responsetime=v;
	}
	this.setNot24_responsetime=setNot24_responsetime;

	this.Not25_accuracy=null;


	function getNot25_accuracy() {
		return this.Not25_accuracy;
	}
	this.getNot25_accuracy=getNot25_accuracy;


	function setNot25_accuracy(v){
		this.Not25_accuracy=v;
	}
	this.setNot25_accuracy=setNot25_accuracy;

	this.Not25_responsetime=null;


	function getNot25_responsetime() {
		return this.Not25_responsetime;
	}
	this.getNot25_responsetime=getNot25_responsetime;


	function setNot25_responsetime(v){
		this.Not25_responsetime=v;
	}
	this.setNot25_responsetime=setNot25_responsetime;

	this.Not26_accuracy=null;


	function getNot26_accuracy() {
		return this.Not26_accuracy;
	}
	this.getNot26_accuracy=getNot26_accuracy;


	function setNot26_accuracy(v){
		this.Not26_accuracy=v;
	}
	this.setNot26_accuracy=setNot26_accuracy;

	this.Not26_responsetime=null;


	function getNot26_responsetime() {
		return this.Not26_responsetime;
	}
	this.getNot26_responsetime=getNot26_responsetime;


	function setNot26_responsetime(v){
		this.Not26_responsetime=v;
	}
	this.setNot26_responsetime=setNot26_responsetime;

	this.Not27_accuracy=null;


	function getNot27_accuracy() {
		return this.Not27_accuracy;
	}
	this.getNot27_accuracy=getNot27_accuracy;


	function setNot27_accuracy(v){
		this.Not27_accuracy=v;
	}
	this.setNot27_accuracy=setNot27_accuracy;

	this.Not27_responsetime=null;


	function getNot27_responsetime() {
		return this.Not27_responsetime;
	}
	this.getNot27_responsetime=getNot27_responsetime;


	function setNot27_responsetime(v){
		this.Not27_responsetime=v;
	}
	this.setNot27_responsetime=setNot27_responsetime;

	this.Not28_accuracy=null;


	function getNot28_accuracy() {
		return this.Not28_accuracy;
	}
	this.getNot28_accuracy=getNot28_accuracy;


	function setNot28_accuracy(v){
		this.Not28_accuracy=v;
	}
	this.setNot28_accuracy=setNot28_accuracy;

	this.Not28_responsetime=null;


	function getNot28_responsetime() {
		return this.Not28_responsetime;
	}
	this.getNot28_responsetime=getNot28_responsetime;


	function setNot28_responsetime(v){
		this.Not28_responsetime=v;
	}
	this.setNot28_responsetime=setNot28_responsetime;

	this.Not29_accuracy=null;


	function getNot29_accuracy() {
		return this.Not29_accuracy;
	}
	this.getNot29_accuracy=getNot29_accuracy;


	function setNot29_accuracy(v){
		this.Not29_accuracy=v;
	}
	this.setNot29_accuracy=setNot29_accuracy;

	this.Not29_responsetime=null;


	function getNot29_responsetime() {
		return this.Not29_responsetime;
	}
	this.getNot29_responsetime=getNot29_responsetime;


	function setNot29_responsetime(v){
		this.Not29_responsetime=v;
	}
	this.setNot29_responsetime=setNot29_responsetime;

	this.Not30_accuracy=null;


	function getNot30_accuracy() {
		return this.Not30_accuracy;
	}
	this.getNot30_accuracy=getNot30_accuracy;


	function setNot30_accuracy(v){
		this.Not30_accuracy=v;
	}
	this.setNot30_accuracy=setNot30_accuracy;

	this.Not30_responsetime=null;


	function getNot30_responsetime() {
		return this.Not30_responsetime;
	}
	this.getNot30_responsetime=getNot30_responsetime;


	function setNot30_responsetime(v){
		this.Not30_responsetime=v;
	}
	this.setNot30_responsetime=setNot30_responsetime;

	this.Not31_accuracy=null;


	function getNot31_accuracy() {
		return this.Not31_accuracy;
	}
	this.getNot31_accuracy=getNot31_accuracy;


	function setNot31_accuracy(v){
		this.Not31_accuracy=v;
	}
	this.setNot31_accuracy=setNot31_accuracy;

	this.Not31_responsetime=null;


	function getNot31_responsetime() {
		return this.Not31_responsetime;
	}
	this.getNot31_responsetime=getNot31_responsetime;


	function setNot31_responsetime(v){
		this.Not31_responsetime=v;
	}
	this.setNot31_responsetime=setNot31_responsetime;

	this.Not32_accuracy=null;


	function getNot32_accuracy() {
		return this.Not32_accuracy;
	}
	this.getNot32_accuracy=getNot32_accuracy;


	function setNot32_accuracy(v){
		this.Not32_accuracy=v;
	}
	this.setNot32_accuracy=setNot32_accuracy;

	this.Not32_responsetime=null;


	function getNot32_responsetime() {
		return this.Not32_responsetime;
	}
	this.getNot32_responsetime=getNot32_responsetime;


	function setNot32_responsetime(v){
		this.Not32_responsetime=v;
	}
	this.setNot32_responsetime=setNot32_responsetime;

	this.Not33_accuracy=null;


	function getNot33_accuracy() {
		return this.Not33_accuracy;
	}
	this.getNot33_accuracy=getNot33_accuracy;


	function setNot33_accuracy(v){
		this.Not33_accuracy=v;
	}
	this.setNot33_accuracy=setNot33_accuracy;

	this.Not33_responsetime=null;


	function getNot33_responsetime() {
		return this.Not33_responsetime;
	}
	this.getNot33_responsetime=getNot33_responsetime;


	function setNot33_responsetime(v){
		this.Not33_responsetime=v;
	}
	this.setNot33_responsetime=setNot33_responsetime;

	this.Not34_accuracy=null;


	function getNot34_accuracy() {
		return this.Not34_accuracy;
	}
	this.getNot34_accuracy=getNot34_accuracy;


	function setNot34_accuracy(v){
		this.Not34_accuracy=v;
	}
	this.setNot34_accuracy=setNot34_accuracy;

	this.Not34_responsetime=null;


	function getNot34_responsetime() {
		return this.Not34_responsetime;
	}
	this.getNot34_responsetime=getNot34_responsetime;


	function setNot34_responsetime(v){
		this.Not34_responsetime=v;
	}
	this.setNot34_responsetime=setNot34_responsetime;

	this.Not35_accuracy=null;


	function getNot35_accuracy() {
		return this.Not35_accuracy;
	}
	this.getNot35_accuracy=getNot35_accuracy;


	function setNot35_accuracy(v){
		this.Not35_accuracy=v;
	}
	this.setNot35_accuracy=setNot35_accuracy;

	this.Not35_responsetime=null;


	function getNot35_responsetime() {
		return this.Not35_responsetime;
	}
	this.getNot35_responsetime=getNot35_responsetime;


	function setNot35_responsetime(v){
		this.Not35_responsetime=v;
	}
	this.setNot35_responsetime=setNot35_responsetime;

	this.Not36_accuracy=null;


	function getNot36_accuracy() {
		return this.Not36_accuracy;
	}
	this.getNot36_accuracy=getNot36_accuracy;


	function setNot36_accuracy(v){
		this.Not36_accuracy=v;
	}
	this.setNot36_accuracy=setNot36_accuracy;

	this.Not36_responsetime=null;


	function getNot36_responsetime() {
		return this.Not36_responsetime;
	}
	this.getNot36_responsetime=getNot36_responsetime;


	function setNot36_responsetime(v){
		this.Not36_responsetime=v;
	}
	this.setNot36_responsetime=setNot36_responsetime;

	this.Not37_accuracy=null;


	function getNot37_accuracy() {
		return this.Not37_accuracy;
	}
	this.getNot37_accuracy=getNot37_accuracy;


	function setNot37_accuracy(v){
		this.Not37_accuracy=v;
	}
	this.setNot37_accuracy=setNot37_accuracy;

	this.Not37_responsetime=null;


	function getNot37_responsetime() {
		return this.Not37_responsetime;
	}
	this.getNot37_responsetime=getNot37_responsetime;


	function setNot37_responsetime(v){
		this.Not37_responsetime=v;
	}
	this.setNot37_responsetime=setNot37_responsetime;

	this.Not38_accuracy=null;


	function getNot38_accuracy() {
		return this.Not38_accuracy;
	}
	this.getNot38_accuracy=getNot38_accuracy;


	function setNot38_accuracy(v){
		this.Not38_accuracy=v;
	}
	this.setNot38_accuracy=setNot38_accuracy;

	this.Not38_responsetime=null;


	function getNot38_responsetime() {
		return this.Not38_responsetime;
	}
	this.getNot38_responsetime=getNot38_responsetime;


	function setNot38_responsetime(v){
		this.Not38_responsetime=v;
	}
	this.setNot38_responsetime=setNot38_responsetime;

	this.Not39_accuracy=null;


	function getNot39_accuracy() {
		return this.Not39_accuracy;
	}
	this.getNot39_accuracy=getNot39_accuracy;


	function setNot39_accuracy(v){
		this.Not39_accuracy=v;
	}
	this.setNot39_accuracy=setNot39_accuracy;

	this.Not39_responsetime=null;


	function getNot39_responsetime() {
		return this.Not39_responsetime;
	}
	this.getNot39_responsetime=getNot39_responsetime;


	function setNot39_responsetime(v){
		this.Not39_responsetime=v;
	}
	this.setNot39_responsetime=setNot39_responsetime;

	this.Not40_accuracy=null;


	function getNot40_accuracy() {
		return this.Not40_accuracy;
	}
	this.getNot40_accuracy=getNot40_accuracy;


	function setNot40_accuracy(v){
		this.Not40_accuracy=v;
	}
	this.setNot40_accuracy=setNot40_accuracy;

	this.Not40_responsetime=null;


	function getNot40_responsetime() {
		return this.Not40_responsetime;
	}
	this.getNot40_responsetime=getNot40_responsetime;


	function setNot40_responsetime(v){
		this.Not40_responsetime=v;
	}
	this.setNot40_responsetime=setNot40_responsetime;

	this.Not41_accuracy=null;


	function getNot41_accuracy() {
		return this.Not41_accuracy;
	}
	this.getNot41_accuracy=getNot41_accuracy;


	function setNot41_accuracy(v){
		this.Not41_accuracy=v;
	}
	this.setNot41_accuracy=setNot41_accuracy;

	this.Not41_responsetime=null;


	function getNot41_responsetime() {
		return this.Not41_responsetime;
	}
	this.getNot41_responsetime=getNot41_responsetime;


	function setNot41_responsetime(v){
		this.Not41_responsetime=v;
	}
	this.setNot41_responsetime=setNot41_responsetime;

	this.Not42_accuracy=null;


	function getNot42_accuracy() {
		return this.Not42_accuracy;
	}
	this.getNot42_accuracy=getNot42_accuracy;


	function setNot42_accuracy(v){
		this.Not42_accuracy=v;
	}
	this.setNot42_accuracy=setNot42_accuracy;

	this.Not42_responsetime=null;


	function getNot42_responsetime() {
		return this.Not42_responsetime;
	}
	this.getNot42_responsetime=getNot42_responsetime;


	function setNot42_responsetime(v){
		this.Not42_responsetime=v;
	}
	this.setNot42_responsetime=setNot42_responsetime;

	this.Not43_accuracy=null;


	function getNot43_accuracy() {
		return this.Not43_accuracy;
	}
	this.getNot43_accuracy=getNot43_accuracy;


	function setNot43_accuracy(v){
		this.Not43_accuracy=v;
	}
	this.setNot43_accuracy=setNot43_accuracy;

	this.Not43_responsetime=null;


	function getNot43_responsetime() {
		return this.Not43_responsetime;
	}
	this.getNot43_responsetime=getNot43_responsetime;


	function setNot43_responsetime(v){
		this.Not43_responsetime=v;
	}
	this.setNot43_responsetime=setNot43_responsetime;

	this.Not44_accuracy=null;


	function getNot44_accuracy() {
		return this.Not44_accuracy;
	}
	this.getNot44_accuracy=getNot44_accuracy;


	function setNot44_accuracy(v){
		this.Not44_accuracy=v;
	}
	this.setNot44_accuracy=setNot44_accuracy;

	this.Not44_responsetime=null;


	function getNot44_responsetime() {
		return this.Not44_responsetime;
	}
	this.getNot44_responsetime=getNot44_responsetime;


	function setNot44_responsetime(v){
		this.Not44_responsetime=v;
	}
	this.setNot44_responsetime=setNot44_responsetime;

	this.Not45_accuracy=null;


	function getNot45_accuracy() {
		return this.Not45_accuracy;
	}
	this.getNot45_accuracy=getNot45_accuracy;


	function setNot45_accuracy(v){
		this.Not45_accuracy=v;
	}
	this.setNot45_accuracy=setNot45_accuracy;

	this.Not45_responsetime=null;


	function getNot45_responsetime() {
		return this.Not45_responsetime;
	}
	this.getNot45_responsetime=getNot45_responsetime;


	function setNot45_responsetime(v){
		this.Not45_responsetime=v;
	}
	this.setNot45_responsetime=setNot45_responsetime;

	this.Not46_accuracy=null;


	function getNot46_accuracy() {
		return this.Not46_accuracy;
	}
	this.getNot46_accuracy=getNot46_accuracy;


	function setNot46_accuracy(v){
		this.Not46_accuracy=v;
	}
	this.setNot46_accuracy=setNot46_accuracy;

	this.Not46_responsetime=null;


	function getNot46_responsetime() {
		return this.Not46_responsetime;
	}
	this.getNot46_responsetime=getNot46_responsetime;


	function setNot46_responsetime(v){
		this.Not46_responsetime=v;
	}
	this.setNot46_responsetime=setNot46_responsetime;

	this.Not47_accuracy=null;


	function getNot47_accuracy() {
		return this.Not47_accuracy;
	}
	this.getNot47_accuracy=getNot47_accuracy;


	function setNot47_accuracy(v){
		this.Not47_accuracy=v;
	}
	this.setNot47_accuracy=setNot47_accuracy;

	this.Not47_responsetime=null;


	function getNot47_responsetime() {
		return this.Not47_responsetime;
	}
	this.getNot47_responsetime=getNot47_responsetime;


	function setNot47_responsetime(v){
		this.Not47_responsetime=v;
	}
	this.setNot47_responsetime=setNot47_responsetime;

	this.Not48_accuracy=null;


	function getNot48_accuracy() {
		return this.Not48_accuracy;
	}
	this.getNot48_accuracy=getNot48_accuracy;


	function setNot48_accuracy(v){
		this.Not48_accuracy=v;
	}
	this.setNot48_accuracy=setNot48_accuracy;

	this.Not48_responsetime=null;


	function getNot48_responsetime() {
		return this.Not48_responsetime;
	}
	this.getNot48_responsetime=getNot48_responsetime;


	function setNot48_responsetime(v){
		this.Not48_responsetime=v;
	}
	this.setNot48_responsetime=setNot48_responsetime;

	this.Not49_accuracy=null;


	function getNot49_accuracy() {
		return this.Not49_accuracy;
	}
	this.getNot49_accuracy=getNot49_accuracy;


	function setNot49_accuracy(v){
		this.Not49_accuracy=v;
	}
	this.setNot49_accuracy=setNot49_accuracy;

	this.Not49_responsetime=null;


	function getNot49_responsetime() {
		return this.Not49_responsetime;
	}
	this.getNot49_responsetime=getNot49_responsetime;


	function setNot49_responsetime(v){
		this.Not49_responsetime=v;
	}
	this.setNot49_responsetime=setNot49_responsetime;

	this.Not50_accuracy=null;


	function getNot50_accuracy() {
		return this.Not50_accuracy;
	}
	this.getNot50_accuracy=getNot50_accuracy;


	function setNot50_accuracy(v){
		this.Not50_accuracy=v;
	}
	this.setNot50_accuracy=setNot50_accuracy;

	this.Not50_responsetime=null;


	function getNot50_responsetime() {
		return this.Not50_responsetime;
	}
	this.getNot50_responsetime=getNot50_responsetime;


	function setNot50_responsetime(v){
		this.Not50_responsetime=v;
	}
	this.setNot50_responsetime=setNot50_responsetime;

	this.Not51_accuracy=null;


	function getNot51_accuracy() {
		return this.Not51_accuracy;
	}
	this.getNot51_accuracy=getNot51_accuracy;


	function setNot51_accuracy(v){
		this.Not51_accuracy=v;
	}
	this.setNot51_accuracy=setNot51_accuracy;

	this.Not51_responsetime=null;


	function getNot51_responsetime() {
		return this.Not51_responsetime;
	}
	this.getNot51_responsetime=getNot51_responsetime;


	function setNot51_responsetime(v){
		this.Not51_responsetime=v;
	}
	this.setNot51_responsetime=setNot51_responsetime;

	this.Not52_accuracy=null;


	function getNot52_accuracy() {
		return this.Not52_accuracy;
	}
	this.getNot52_accuracy=getNot52_accuracy;


	function setNot52_accuracy(v){
		this.Not52_accuracy=v;
	}
	this.setNot52_accuracy=setNot52_accuracy;

	this.Not52_responsetime=null;


	function getNot52_responsetime() {
		return this.Not52_responsetime;
	}
	this.getNot52_responsetime=getNot52_responsetime;


	function setNot52_responsetime(v){
		this.Not52_responsetime=v;
	}
	this.setNot52_responsetime=setNot52_responsetime;

	this.Not53_accuracy=null;


	function getNot53_accuracy() {
		return this.Not53_accuracy;
	}
	this.getNot53_accuracy=getNot53_accuracy;


	function setNot53_accuracy(v){
		this.Not53_accuracy=v;
	}
	this.setNot53_accuracy=setNot53_accuracy;

	this.Not53_responsetime=null;


	function getNot53_responsetime() {
		return this.Not53_responsetime;
	}
	this.getNot53_responsetime=getNot53_responsetime;


	function setNot53_responsetime(v){
		this.Not53_responsetime=v;
	}
	this.setNot53_responsetime=setNot53_responsetime;

	this.Not54_accuracy=null;


	function getNot54_accuracy() {
		return this.Not54_accuracy;
	}
	this.getNot54_accuracy=getNot54_accuracy;


	function setNot54_accuracy(v){
		this.Not54_accuracy=v;
	}
	this.setNot54_accuracy=setNot54_accuracy;

	this.Not54_responsetime=null;


	function getNot54_responsetime() {
		return this.Not54_responsetime;
	}
	this.getNot54_responsetime=getNot54_responsetime;


	function setNot54_responsetime(v){
		this.Not54_responsetime=v;
	}
	this.setNot54_responsetime=setNot54_responsetime;

	this.Not55_accuracy=null;


	function getNot55_accuracy() {
		return this.Not55_accuracy;
	}
	this.getNot55_accuracy=getNot55_accuracy;


	function setNot55_accuracy(v){
		this.Not55_accuracy=v;
	}
	this.setNot55_accuracy=setNot55_accuracy;

	this.Not55_responsetime=null;


	function getNot55_responsetime() {
		return this.Not55_responsetime;
	}
	this.getNot55_responsetime=getNot55_responsetime;


	function setNot55_responsetime(v){
		this.Not55_responsetime=v;
	}
	this.setNot55_responsetime=setNot55_responsetime;

	this.Not56_accuracy=null;


	function getNot56_accuracy() {
		return this.Not56_accuracy;
	}
	this.getNot56_accuracy=getNot56_accuracy;


	function setNot56_accuracy(v){
		this.Not56_accuracy=v;
	}
	this.setNot56_accuracy=setNot56_accuracy;

	this.Not56_responsetime=null;


	function getNot56_responsetime() {
		return this.Not56_responsetime;
	}
	this.getNot56_responsetime=getNot56_responsetime;


	function setNot56_responsetime(v){
		this.Not56_responsetime=v;
	}
	this.setNot56_responsetime=setNot56_responsetime;

	this.Not57_accuracy=null;


	function getNot57_accuracy() {
		return this.Not57_accuracy;
	}
	this.getNot57_accuracy=getNot57_accuracy;


	function setNot57_accuracy(v){
		this.Not57_accuracy=v;
	}
	this.setNot57_accuracy=setNot57_accuracy;

	this.Not57_responsetime=null;


	function getNot57_responsetime() {
		return this.Not57_responsetime;
	}
	this.getNot57_responsetime=getNot57_responsetime;


	function setNot57_responsetime(v){
		this.Not57_responsetime=v;
	}
	this.setNot57_responsetime=setNot57_responsetime;

	this.Not58_accuracy=null;


	function getNot58_accuracy() {
		return this.Not58_accuracy;
	}
	this.getNot58_accuracy=getNot58_accuracy;


	function setNot58_accuracy(v){
		this.Not58_accuracy=v;
	}
	this.setNot58_accuracy=setNot58_accuracy;

	this.Not58_responsetime=null;


	function getNot58_responsetime() {
		return this.Not58_responsetime;
	}
	this.getNot58_responsetime=getNot58_responsetime;


	function setNot58_responsetime(v){
		this.Not58_responsetime=v;
	}
	this.setNot58_responsetime=setNot58_responsetime;

	this.Not59_accuracy=null;


	function getNot59_accuracy() {
		return this.Not59_accuracy;
	}
	this.getNot59_accuracy=getNot59_accuracy;


	function setNot59_accuracy(v){
		this.Not59_accuracy=v;
	}
	this.setNot59_accuracy=setNot59_accuracy;

	this.Not59_responsetime=null;


	function getNot59_responsetime() {
		return this.Not59_responsetime;
	}
	this.getNot59_responsetime=getNot59_responsetime;


	function setNot59_responsetime(v){
		this.Not59_responsetime=v;
	}
	this.setNot59_responsetime=setNot59_responsetime;

	this.Not60_accuracy=null;


	function getNot60_accuracy() {
		return this.Not60_accuracy;
	}
	this.getNot60_accuracy=getNot60_accuracy;


	function setNot60_accuracy(v){
		this.Not60_accuracy=v;
	}
	this.setNot60_accuracy=setNot60_accuracy;

	this.Not60_responsetime=null;


	function getNot60_responsetime() {
		return this.Not60_responsetime;
	}
	this.getNot60_responsetime=getNot60_responsetime;


	function setNot60_responsetime(v){
		this.Not60_responsetime=v;
	}
	this.setNot60_responsetime=setNot60_responsetime;

	this.Not61_accuracy=null;


	function getNot61_accuracy() {
		return this.Not61_accuracy;
	}
	this.getNot61_accuracy=getNot61_accuracy;


	function setNot61_accuracy(v){
		this.Not61_accuracy=v;
	}
	this.setNot61_accuracy=setNot61_accuracy;

	this.Not61_responsetime=null;


	function getNot61_responsetime() {
		return this.Not61_responsetime;
	}
	this.getNot61_responsetime=getNot61_responsetime;


	function setNot61_responsetime(v){
		this.Not61_responsetime=v;
	}
	this.setNot61_responsetime=setNot61_responsetime;

	this.Not62_accuracy=null;


	function getNot62_accuracy() {
		return this.Not62_accuracy;
	}
	this.getNot62_accuracy=getNot62_accuracy;


	function setNot62_accuracy(v){
		this.Not62_accuracy=v;
	}
	this.setNot62_accuracy=setNot62_accuracy;

	this.Not62_responsetime=null;


	function getNot62_responsetime() {
		return this.Not62_responsetime;
	}
	this.getNot62_responsetime=getNot62_responsetime;


	function setNot62_responsetime(v){
		this.Not62_responsetime=v;
	}
	this.setNot62_responsetime=setNot62_responsetime;

	this.Not63_accuracy=null;


	function getNot63_accuracy() {
		return this.Not63_accuracy;
	}
	this.getNot63_accuracy=getNot63_accuracy;


	function setNot63_accuracy(v){
		this.Not63_accuracy=v;
	}
	this.setNot63_accuracy=setNot63_accuracy;

	this.Not63_responsetime=null;


	function getNot63_responsetime() {
		return this.Not63_responsetime;
	}
	this.getNot63_responsetime=getNot63_responsetime;


	function setNot63_responsetime(v){
		this.Not63_responsetime=v;
	}
	this.setNot63_responsetime=setNot63_responsetime;

	this.Not64_accuracy=null;


	function getNot64_accuracy() {
		return this.Not64_accuracy;
	}
	this.getNot64_accuracy=getNot64_accuracy;


	function setNot64_accuracy(v){
		this.Not64_accuracy=v;
	}
	this.setNot64_accuracy=setNot64_accuracy;

	this.Not64_responsetime=null;


	function getNot64_responsetime() {
		return this.Not64_responsetime;
	}
	this.getNot64_responsetime=getNot64_responsetime;


	function setNot64_responsetime(v){
		this.Not64_responsetime=v;
	}
	this.setNot64_responsetime=setNot64_responsetime;

	this.Not65_accuracy=null;


	function getNot65_accuracy() {
		return this.Not65_accuracy;
	}
	this.getNot65_accuracy=getNot65_accuracy;


	function setNot65_accuracy(v){
		this.Not65_accuracy=v;
	}
	this.setNot65_accuracy=setNot65_accuracy;

	this.Not65_responsetime=null;


	function getNot65_responsetime() {
		return this.Not65_responsetime;
	}
	this.getNot65_responsetime=getNot65_responsetime;


	function setNot65_responsetime(v){
		this.Not65_responsetime=v;
	}
	this.setNot65_responsetime=setNot65_responsetime;

	this.Not66_accuracy=null;


	function getNot66_accuracy() {
		return this.Not66_accuracy;
	}
	this.getNot66_accuracy=getNot66_accuracy;


	function setNot66_accuracy(v){
		this.Not66_accuracy=v;
	}
	this.setNot66_accuracy=setNot66_accuracy;

	this.Not66_responsetime=null;


	function getNot66_responsetime() {
		return this.Not66_responsetime;
	}
	this.getNot66_responsetime=getNot66_responsetime;


	function setNot66_responsetime(v){
		this.Not66_responsetime=v;
	}
	this.setNot66_responsetime=setNot66_responsetime;

	this.Not67_accuracy=null;


	function getNot67_accuracy() {
		return this.Not67_accuracy;
	}
	this.getNot67_accuracy=getNot67_accuracy;


	function setNot67_accuracy(v){
		this.Not67_accuracy=v;
	}
	this.setNot67_accuracy=setNot67_accuracy;

	this.Not67_responsetime=null;


	function getNot67_responsetime() {
		return this.Not67_responsetime;
	}
	this.getNot67_responsetime=getNot67_responsetime;


	function setNot67_responsetime(v){
		this.Not67_responsetime=v;
	}
	this.setNot67_responsetime=setNot67_responsetime;

	this.Not68_accuracy=null;


	function getNot68_accuracy() {
		return this.Not68_accuracy;
	}
	this.getNot68_accuracy=getNot68_accuracy;


	function setNot68_accuracy(v){
		this.Not68_accuracy=v;
	}
	this.setNot68_accuracy=setNot68_accuracy;

	this.Not68_responsetime=null;


	function getNot68_responsetime() {
		return this.Not68_responsetime;
	}
	this.getNot68_responsetime=getNot68_responsetime;


	function setNot68_responsetime(v){
		this.Not68_responsetime=v;
	}
	this.setNot68_responsetime=setNot68_responsetime;

	this.Not69_accuracy=null;


	function getNot69_accuracy() {
		return this.Not69_accuracy;
	}
	this.getNot69_accuracy=getNot69_accuracy;


	function setNot69_accuracy(v){
		this.Not69_accuracy=v;
	}
	this.setNot69_accuracy=setNot69_accuracy;

	this.Not69_responsetime=null;


	function getNot69_responsetime() {
		return this.Not69_responsetime;
	}
	this.getNot69_responsetime=getNot69_responsetime;


	function setNot69_responsetime(v){
		this.Not69_responsetime=v;
	}
	this.setNot69_responsetime=setNot69_responsetime;

	this.Not70_accuracy=null;


	function getNot70_accuracy() {
		return this.Not70_accuracy;
	}
	this.getNot70_accuracy=getNot70_accuracy;


	function setNot70_accuracy(v){
		this.Not70_accuracy=v;
	}
	this.setNot70_accuracy=setNot70_accuracy;

	this.Not70_responsetime=null;


	function getNot70_responsetime() {
		return this.Not70_responsetime;
	}
	this.getNot70_responsetime=getNot70_responsetime;


	function setNot70_responsetime(v){
		this.Not70_responsetime=v;
	}
	this.setNot70_responsetime=setNot70_responsetime;

	this.Not71_accuracy=null;


	function getNot71_accuracy() {
		return this.Not71_accuracy;
	}
	this.getNot71_accuracy=getNot71_accuracy;


	function setNot71_accuracy(v){
		this.Not71_accuracy=v;
	}
	this.setNot71_accuracy=setNot71_accuracy;

	this.Not71_responsetime=null;


	function getNot71_responsetime() {
		return this.Not71_responsetime;
	}
	this.getNot71_responsetime=getNot71_responsetime;


	function setNot71_responsetime(v){
		this.Not71_responsetime=v;
	}
	this.setNot71_responsetime=setNot71_responsetime;

	this.Not72_accuracy=null;


	function getNot72_accuracy() {
		return this.Not72_accuracy;
	}
	this.getNot72_accuracy=getNot72_accuracy;


	function setNot72_accuracy(v){
		this.Not72_accuracy=v;
	}
	this.setNot72_accuracy=setNot72_accuracy;

	this.Not72_responsetime=null;


	function getNot72_responsetime() {
		return this.Not72_responsetime;
	}
	this.getNot72_responsetime=getNot72_responsetime;


	function setNot72_responsetime(v){
		this.Not72_responsetime=v;
	}
	this.setNot72_responsetime=setNot72_responsetime;

	this.Not73_accuracy=null;


	function getNot73_accuracy() {
		return this.Not73_accuracy;
	}
	this.getNot73_accuracy=getNot73_accuracy;


	function setNot73_accuracy(v){
		this.Not73_accuracy=v;
	}
	this.setNot73_accuracy=setNot73_accuracy;

	this.Not73_responsetime=null;


	function getNot73_responsetime() {
		return this.Not73_responsetime;
	}
	this.getNot73_responsetime=getNot73_responsetime;


	function setNot73_responsetime(v){
		this.Not73_responsetime=v;
	}
	this.setNot73_responsetime=setNot73_responsetime;

	this.Not74_accuracy=null;


	function getNot74_accuracy() {
		return this.Not74_accuracy;
	}
	this.getNot74_accuracy=getNot74_accuracy;


	function setNot74_accuracy(v){
		this.Not74_accuracy=v;
	}
	this.setNot74_accuracy=setNot74_accuracy;

	this.Not74_responsetime=null;


	function getNot74_responsetime() {
		return this.Not74_responsetime;
	}
	this.getNot74_responsetime=getNot74_responsetime;


	function setNot74_responsetime(v){
		this.Not74_responsetime=v;
	}
	this.setNot74_responsetime=setNot74_responsetime;

	this.Not75_accuracy=null;


	function getNot75_accuracy() {
		return this.Not75_accuracy;
	}
	this.getNot75_accuracy=getNot75_accuracy;


	function setNot75_accuracy(v){
		this.Not75_accuracy=v;
	}
	this.setNot75_accuracy=setNot75_accuracy;

	this.Not75_responsetime=null;


	function getNot75_responsetime() {
		return this.Not75_responsetime;
	}
	this.getNot75_responsetime=getNot75_responsetime;


	function setNot75_responsetime(v){
		this.Not75_responsetime=v;
	}
	this.setNot75_responsetime=setNot75_responsetime;

	this.Not76_accuracy=null;


	function getNot76_accuracy() {
		return this.Not76_accuracy;
	}
	this.getNot76_accuracy=getNot76_accuracy;


	function setNot76_accuracy(v){
		this.Not76_accuracy=v;
	}
	this.setNot76_accuracy=setNot76_accuracy;

	this.Not76_responsetime=null;


	function getNot76_responsetime() {
		return this.Not76_responsetime;
	}
	this.getNot76_responsetime=getNot76_responsetime;


	function setNot76_responsetime(v){
		this.Not76_responsetime=v;
	}
	this.setNot76_responsetime=setNot76_responsetime;

	this.Not77_accuracy=null;


	function getNot77_accuracy() {
		return this.Not77_accuracy;
	}
	this.getNot77_accuracy=getNot77_accuracy;


	function setNot77_accuracy(v){
		this.Not77_accuracy=v;
	}
	this.setNot77_accuracy=setNot77_accuracy;

	this.Not77_responsetime=null;


	function getNot77_responsetime() {
		return this.Not77_responsetime;
	}
	this.getNot77_responsetime=getNot77_responsetime;


	function setNot77_responsetime(v){
		this.Not77_responsetime=v;
	}
	this.setNot77_responsetime=setNot77_responsetime;

	this.Not78_accuracy=null;


	function getNot78_accuracy() {
		return this.Not78_accuracy;
	}
	this.getNot78_accuracy=getNot78_accuracy;


	function setNot78_accuracy(v){
		this.Not78_accuracy=v;
	}
	this.setNot78_accuracy=setNot78_accuracy;

	this.Not78_responsetime=null;


	function getNot78_responsetime() {
		return this.Not78_responsetime;
	}
	this.getNot78_responsetime=getNot78_responsetime;


	function setNot78_responsetime(v){
		this.Not78_responsetime=v;
	}
	this.setNot78_responsetime=setNot78_responsetime;

	this.Not79_accuracy=null;


	function getNot79_accuracy() {
		return this.Not79_accuracy;
	}
	this.getNot79_accuracy=getNot79_accuracy;


	function setNot79_accuracy(v){
		this.Not79_accuracy=v;
	}
	this.setNot79_accuracy=setNot79_accuracy;

	this.Not79_responsetime=null;


	function getNot79_responsetime() {
		return this.Not79_responsetime;
	}
	this.getNot79_responsetime=getNot79_responsetime;


	function setNot79_responsetime(v){
		this.Not79_responsetime=v;
	}
	this.setNot79_responsetime=setNot79_responsetime;

	this.Not80_accuracy=null;


	function getNot80_accuracy() {
		return this.Not80_accuracy;
	}
	this.getNot80_accuracy=getNot80_accuracy;


	function setNot80_accuracy(v){
		this.Not80_accuracy=v;
	}
	this.setNot80_accuracy=setNot80_accuracy;

	this.Not80_responsetime=null;


	function getNot80_responsetime() {
		return this.Not80_responsetime;
	}
	this.getNot80_responsetime=getNot80_responsetime;


	function setNot80_responsetime(v){
		this.Not80_responsetime=v;
	}
	this.setNot80_responsetime=setNot80_responsetime;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="YesT1/accuracy"){
				return this.Yest1_accuracy ;
			} else 
			if(xmlPath=="YesT1/responseTime"){
				return this.Yest1_responsetime ;
			} else 
			if(xmlPath=="YesT2/accuracy"){
				return this.Yest2_accuracy ;
			} else 
			if(xmlPath=="YesT2/responseTime"){
				return this.Yest2_responsetime ;
			} else 
			if(xmlPath=="YesT3/accuracy"){
				return this.Yest3_accuracy ;
			} else 
			if(xmlPath=="YesT3/responseTime"){
				return this.Yest3_responsetime ;
			} else 
			if(xmlPath=="YesT4/accuracy"){
				return this.Yest4_accuracy ;
			} else 
			if(xmlPath=="YesT4/responseTime"){
				return this.Yest4_responsetime ;
			} else 
			if(xmlPath=="YesT5/accuracy"){
				return this.Yest5_accuracy ;
			} else 
			if(xmlPath=="YesT5/responseTime"){
				return this.Yest5_responsetime ;
			} else 
			if(xmlPath=="YesT6/accuracy"){
				return this.Yest6_accuracy ;
			} else 
			if(xmlPath=="YesT6/responseTime"){
				return this.Yest6_responsetime ;
			} else 
			if(xmlPath=="YesT7/accuracy"){
				return this.Yest7_accuracy ;
			} else 
			if(xmlPath=="YesT7/responseTime"){
				return this.Yest7_responsetime ;
			} else 
			if(xmlPath=="YesT8/accuracy"){
				return this.Yest8_accuracy ;
			} else 
			if(xmlPath=="YesT8/responseTime"){
				return this.Yest8_responsetime ;
			} else 
			if(xmlPath=="YesT9/accuracy"){
				return this.Yest9_accuracy ;
			} else 
			if(xmlPath=="YesT9/responseTime"){
				return this.Yest9_responsetime ;
			} else 
			if(xmlPath=="YesT10/accuracy"){
				return this.Yest10_accuracy ;
			} else 
			if(xmlPath=="YesT10/responseTime"){
				return this.Yest10_responsetime ;
			} else 
			if(xmlPath=="YesT11/accuracy"){
				return this.Yest11_accuracy ;
			} else 
			if(xmlPath=="YesT11/responseTime"){
				return this.Yest11_responsetime ;
			} else 
			if(xmlPath=="YesT12/accuracy"){
				return this.Yest12_accuracy ;
			} else 
			if(xmlPath=="YesT12/responseTime"){
				return this.Yest12_responsetime ;
			} else 
			if(xmlPath=="YesT13/accuracy"){
				return this.Yest13_accuracy ;
			} else 
			if(xmlPath=="YesT13/responseTime"){
				return this.Yest13_responsetime ;
			} else 
			if(xmlPath=="YesT14/accuracy"){
				return this.Yest14_accuracy ;
			} else 
			if(xmlPath=="YesT14/responseTime"){
				return this.Yest14_responsetime ;
			} else 
			if(xmlPath=="YesT15/accuracy"){
				return this.Yest15_accuracy ;
			} else 
			if(xmlPath=="YesT15/responseTime"){
				return this.Yest15_responsetime ;
			} else 
			if(xmlPath=="YesT16/accuracy"){
				return this.Yest16_accuracy ;
			} else 
			if(xmlPath=="YesT16/responseTime"){
				return this.Yest16_responsetime ;
			} else 
			if(xmlPath=="YesT17/accuracy"){
				return this.Yest17_accuracy ;
			} else 
			if(xmlPath=="YesT17/responseTime"){
				return this.Yest17_responsetime ;
			} else 
			if(xmlPath=="YesT18/accuracy"){
				return this.Yest18_accuracy ;
			} else 
			if(xmlPath=="YesT18/responseTime"){
				return this.Yest18_responsetime ;
			} else 
			if(xmlPath=="YesT19/accuracy"){
				return this.Yest19_accuracy ;
			} else 
			if(xmlPath=="YesT19/responseTime"){
				return this.Yest19_responsetime ;
			} else 
			if(xmlPath=="YesT20/accuracy"){
				return this.Yest20_accuracy ;
			} else 
			if(xmlPath=="YesT20/responseTime"){
				return this.Yest20_responsetime ;
			} else 
			if(xmlPath=="YesT21/accuracy"){
				return this.Yest21_accuracy ;
			} else 
			if(xmlPath=="YesT21/responseTime"){
				return this.Yest21_responsetime ;
			} else 
			if(xmlPath=="YesT22/accuracy"){
				return this.Yest22_accuracy ;
			} else 
			if(xmlPath=="YesT22/responseTime"){
				return this.Yest22_responsetime ;
			} else 
			if(xmlPath=="YesT23/accuracy"){
				return this.Yest23_accuracy ;
			} else 
			if(xmlPath=="YesT23/responseTime"){
				return this.Yest23_responsetime ;
			} else 
			if(xmlPath=="YesT24/accuracy"){
				return this.Yest24_accuracy ;
			} else 
			if(xmlPath=="YesT24/responseTime"){
				return this.Yest24_responsetime ;
			} else 
			if(xmlPath=="YesT25/accuracy"){
				return this.Yest25_accuracy ;
			} else 
			if(xmlPath=="YesT25/responseTime"){
				return this.Yest25_responsetime ;
			} else 
			if(xmlPath=="YesT26/accuracy"){
				return this.Yest26_accuracy ;
			} else 
			if(xmlPath=="YesT26/responseTime"){
				return this.Yest26_responsetime ;
			} else 
			if(xmlPath=="YesT27/accuracy"){
				return this.Yest27_accuracy ;
			} else 
			if(xmlPath=="YesT27/responseTime"){
				return this.Yest27_responsetime ;
			} else 
			if(xmlPath=="YesT28/accuracy"){
				return this.Yest28_accuracy ;
			} else 
			if(xmlPath=="YesT28/responseTime"){
				return this.Yest28_responsetime ;
			} else 
			if(xmlPath=="YesT29/accuracy"){
				return this.Yest29_accuracy ;
			} else 
			if(xmlPath=="YesT29/responseTime"){
				return this.Yest29_responsetime ;
			} else 
			if(xmlPath=="YesT30/accuracy"){
				return this.Yest30_accuracy ;
			} else 
			if(xmlPath=="YesT30/responseTime"){
				return this.Yest30_responsetime ;
			} else 
			if(xmlPath=="YesT31/accuracy"){
				return this.Yest31_accuracy ;
			} else 
			if(xmlPath=="YesT31/responseTime"){
				return this.Yest31_responsetime ;
			} else 
			if(xmlPath=="YesT32/accuracy"){
				return this.Yest32_accuracy ;
			} else 
			if(xmlPath=="YesT32/responseTime"){
				return this.Yest32_responsetime ;
			} else 
			if(xmlPath=="YesT33/accuracy"){
				return this.Yest33_accuracy ;
			} else 
			if(xmlPath=="YesT33/responseTime"){
				return this.Yest33_responsetime ;
			} else 
			if(xmlPath=="YesT34/accuracy"){
				return this.Yest34_accuracy ;
			} else 
			if(xmlPath=="YesT34/responseTime"){
				return this.Yest34_responsetime ;
			} else 
			if(xmlPath=="YesT35/accuracy"){
				return this.Yest35_accuracy ;
			} else 
			if(xmlPath=="YesT35/responseTime"){
				return this.Yest35_responsetime ;
			} else 
			if(xmlPath=="YesT36/accuracy"){
				return this.Yest36_accuracy ;
			} else 
			if(xmlPath=="YesT36/responseTime"){
				return this.Yest36_responsetime ;
			} else 
			if(xmlPath=="YesT37/accuracy"){
				return this.Yest37_accuracy ;
			} else 
			if(xmlPath=="YesT37/responseTime"){
				return this.Yest37_responsetime ;
			} else 
			if(xmlPath=="YesT38/accuracy"){
				return this.Yest38_accuracy ;
			} else 
			if(xmlPath=="YesT38/responseTime"){
				return this.Yest38_responsetime ;
			} else 
			if(xmlPath=="YesT39/accuracy"){
				return this.Yest39_accuracy ;
			} else 
			if(xmlPath=="YesT39/responseTime"){
				return this.Yest39_responsetime ;
			} else 
			if(xmlPath=="YesT40/accuracy"){
				return this.Yest40_accuracy ;
			} else 
			if(xmlPath=="YesT40/responseTime"){
				return this.Yest40_responsetime ;
			} else 
			if(xmlPath=="YesT41/accuracy"){
				return this.Yest41_accuracy ;
			} else 
			if(xmlPath=="YesT41/responseTime"){
				return this.Yest41_responsetime ;
			} else 
			if(xmlPath=="YesT42/accuracy"){
				return this.Yest42_accuracy ;
			} else 
			if(xmlPath=="YesT42/responseTime"){
				return this.Yest42_responsetime ;
			} else 
			if(xmlPath=="YesT43/accuracy"){
				return this.Yest43_accuracy ;
			} else 
			if(xmlPath=="YesT43/responseTime"){
				return this.Yest43_responsetime ;
			} else 
			if(xmlPath=="YesT44/accuracy"){
				return this.Yest44_accuracy ;
			} else 
			if(xmlPath=="YesT44/responseTime"){
				return this.Yest44_responsetime ;
			} else 
			if(xmlPath=="YesT45/accuracy"){
				return this.Yest45_accuracy ;
			} else 
			if(xmlPath=="YesT45/responseTime"){
				return this.Yest45_responsetime ;
			} else 
			if(xmlPath=="YesT46/accuracy"){
				return this.Yest46_accuracy ;
			} else 
			if(xmlPath=="YesT46/responseTime"){
				return this.Yest46_responsetime ;
			} else 
			if(xmlPath=="YesT47/accuracy"){
				return this.Yest47_accuracy ;
			} else 
			if(xmlPath=="YesT47/responseTime"){
				return this.Yest47_responsetime ;
			} else 
			if(xmlPath=="YesT48/accuracy"){
				return this.Yest48_accuracy ;
			} else 
			if(xmlPath=="YesT48/responseTime"){
				return this.Yest48_responsetime ;
			} else 
			if(xmlPath=="YesT49/accuracy"){
				return this.Yest49_accuracy ;
			} else 
			if(xmlPath=="YesT49/responseTime"){
				return this.Yest49_responsetime ;
			} else 
			if(xmlPath=="YesT50/accuracy"){
				return this.Yest50_accuracy ;
			} else 
			if(xmlPath=="YesT50/responseTime"){
				return this.Yest50_responsetime ;
			} else 
			if(xmlPath=="YesT51/accuracy"){
				return this.Yest51_accuracy ;
			} else 
			if(xmlPath=="YesT51/responseTime"){
				return this.Yest51_responsetime ;
			} else 
			if(xmlPath=="YesT52/accuracy"){
				return this.Yest52_accuracy ;
			} else 
			if(xmlPath=="YesT52/responseTime"){
				return this.Yest52_responsetime ;
			} else 
			if(xmlPath=="YesT53/accuracy"){
				return this.Yest53_accuracy ;
			} else 
			if(xmlPath=="YesT53/responseTime"){
				return this.Yest53_responsetime ;
			} else 
			if(xmlPath=="YesT54/accuracy"){
				return this.Yest54_accuracy ;
			} else 
			if(xmlPath=="YesT54/responseTime"){
				return this.Yest54_responsetime ;
			} else 
			if(xmlPath=="YesT55/accuracy"){
				return this.Yest55_accuracy ;
			} else 
			if(xmlPath=="YesT55/responseTime"){
				return this.Yest55_responsetime ;
			} else 
			if(xmlPath=="YesT56/accuracy"){
				return this.Yest56_accuracy ;
			} else 
			if(xmlPath=="YesT56/responseTime"){
				return this.Yest56_responsetime ;
			} else 
			if(xmlPath=="YesT57/accuracy"){
				return this.Yest57_accuracy ;
			} else 
			if(xmlPath=="YesT57/responseTime"){
				return this.Yest57_responsetime ;
			} else 
			if(xmlPath=="YesT58/accuracy"){
				return this.Yest58_accuracy ;
			} else 
			if(xmlPath=="YesT58/responseTime"){
				return this.Yest58_responsetime ;
			} else 
			if(xmlPath=="YesT59/accuracy"){
				return this.Yest59_accuracy ;
			} else 
			if(xmlPath=="YesT59/responseTime"){
				return this.Yest59_responsetime ;
			} else 
			if(xmlPath=="YesT60/accuracy"){
				return this.Yest60_accuracy ;
			} else 
			if(xmlPath=="YesT60/responseTime"){
				return this.Yest60_responsetime ;
			} else 
			if(xmlPath=="YesT61/accuracy"){
				return this.Yest61_accuracy ;
			} else 
			if(xmlPath=="YesT61/responseTime"){
				return this.Yest61_responsetime ;
			} else 
			if(xmlPath=="YesT62/accuracy"){
				return this.Yest62_accuracy ;
			} else 
			if(xmlPath=="YesT62/responseTime"){
				return this.Yest62_responsetime ;
			} else 
			if(xmlPath=="YesT63/accuracy"){
				return this.Yest63_accuracy ;
			} else 
			if(xmlPath=="YesT63/responseTime"){
				return this.Yest63_responsetime ;
			} else 
			if(xmlPath=="YesT64/accuracy"){
				return this.Yest64_accuracy ;
			} else 
			if(xmlPath=="YesT64/responseTime"){
				return this.Yest64_responsetime ;
			} else 
			if(xmlPath=="YesT65/accuracy"){
				return this.Yest65_accuracy ;
			} else 
			if(xmlPath=="YesT65/responseTime"){
				return this.Yest65_responsetime ;
			} else 
			if(xmlPath=="YesT66/accuracy"){
				return this.Yest66_accuracy ;
			} else 
			if(xmlPath=="YesT66/responseTime"){
				return this.Yest66_responsetime ;
			} else 
			if(xmlPath=="YesT67/accuracy"){
				return this.Yest67_accuracy ;
			} else 
			if(xmlPath=="YesT67/responseTime"){
				return this.Yest67_responsetime ;
			} else 
			if(xmlPath=="YesT68/accuracy"){
				return this.Yest68_accuracy ;
			} else 
			if(xmlPath=="YesT68/responseTime"){
				return this.Yest68_responsetime ;
			} else 
			if(xmlPath=="YesT69/accuracy"){
				return this.Yest69_accuracy ;
			} else 
			if(xmlPath=="YesT69/responseTime"){
				return this.Yest69_responsetime ;
			} else 
			if(xmlPath=="YesT70/accuracy"){
				return this.Yest70_accuracy ;
			} else 
			if(xmlPath=="YesT70/responseTime"){
				return this.Yest70_responsetime ;
			} else 
			if(xmlPath=="YesT71/accuracy"){
				return this.Yest71_accuracy ;
			} else 
			if(xmlPath=="YesT71/responseTime"){
				return this.Yest71_responsetime ;
			} else 
			if(xmlPath=="YesT72/accuracy"){
				return this.Yest72_accuracy ;
			} else 
			if(xmlPath=="YesT72/responseTime"){
				return this.Yest72_responsetime ;
			} else 
			if(xmlPath=="YesT73/accuracy"){
				return this.Yest73_accuracy ;
			} else 
			if(xmlPath=="YesT73/responseTime"){
				return this.Yest73_responsetime ;
			} else 
			if(xmlPath=="YesT74/accuracy"){
				return this.Yest74_accuracy ;
			} else 
			if(xmlPath=="YesT74/responseTime"){
				return this.Yest74_responsetime ;
			} else 
			if(xmlPath=="YesT75/accuracy"){
				return this.Yest75_accuracy ;
			} else 
			if(xmlPath=="YesT75/responseTime"){
				return this.Yest75_responsetime ;
			} else 
			if(xmlPath=="YesT76/accuracy"){
				return this.Yest76_accuracy ;
			} else 
			if(xmlPath=="YesT76/responseTime"){
				return this.Yest76_responsetime ;
			} else 
			if(xmlPath=="YesT77/accuracy"){
				return this.Yest77_accuracy ;
			} else 
			if(xmlPath=="YesT77/responseTime"){
				return this.Yest77_responsetime ;
			} else 
			if(xmlPath=="YesT78/accuracy"){
				return this.Yest78_accuracy ;
			} else 
			if(xmlPath=="YesT78/responseTime"){
				return this.Yest78_responsetime ;
			} else 
			if(xmlPath=="YesT79/accuracy"){
				return this.Yest79_accuracy ;
			} else 
			if(xmlPath=="YesT79/responseTime"){
				return this.Yest79_responsetime ;
			} else 
			if(xmlPath=="YesT80/accuracy"){
				return this.Yest80_accuracy ;
			} else 
			if(xmlPath=="YesT80/responseTime"){
				return this.Yest80_responsetime ;
			} else 
			if(xmlPath=="NoT1/accuracy"){
				return this.Not1_accuracy ;
			} else 
			if(xmlPath=="NoT1/responseTime"){
				return this.Not1_responsetime ;
			} else 
			if(xmlPath=="NoT2/accuracy"){
				return this.Not2_accuracy ;
			} else 
			if(xmlPath=="NoT2/responseTime"){
				return this.Not2_responsetime ;
			} else 
			if(xmlPath=="NoT3/accuracy"){
				return this.Not3_accuracy ;
			} else 
			if(xmlPath=="NoT3/responseTime"){
				return this.Not3_responsetime ;
			} else 
			if(xmlPath=="NoT4/accuracy"){
				return this.Not4_accuracy ;
			} else 
			if(xmlPath=="NoT4/responseTime"){
				return this.Not4_responsetime ;
			} else 
			if(xmlPath=="NoT5/accuracy"){
				return this.Not5_accuracy ;
			} else 
			if(xmlPath=="NoT5/responseTime"){
				return this.Not5_responsetime ;
			} else 
			if(xmlPath=="NoT6/accuracy"){
				return this.Not6_accuracy ;
			} else 
			if(xmlPath=="NoT6/responseTime"){
				return this.Not6_responsetime ;
			} else 
			if(xmlPath=="NoT7/accuracy"){
				return this.Not7_accuracy ;
			} else 
			if(xmlPath=="NoT7/responseTime"){
				return this.Not7_responsetime ;
			} else 
			if(xmlPath=="NoT8/accuracy"){
				return this.Not8_accuracy ;
			} else 
			if(xmlPath=="NoT8/responseTime"){
				return this.Not8_responsetime ;
			} else 
			if(xmlPath=="NoT9/accuracy"){
				return this.Not9_accuracy ;
			} else 
			if(xmlPath=="NoT9/responseTime"){
				return this.Not9_responsetime ;
			} else 
			if(xmlPath=="NoT10/accuracy"){
				return this.Not10_accuracy ;
			} else 
			if(xmlPath=="NoT10/responseTime"){
				return this.Not10_responsetime ;
			} else 
			if(xmlPath=="NoT11/accuracy"){
				return this.Not11_accuracy ;
			} else 
			if(xmlPath=="NoT11/responseTime"){
				return this.Not11_responsetime ;
			} else 
			if(xmlPath=="NoT12/accuracy"){
				return this.Not12_accuracy ;
			} else 
			if(xmlPath=="NoT12/responseTime"){
				return this.Not12_responsetime ;
			} else 
			if(xmlPath=="NoT13/accuracy"){
				return this.Not13_accuracy ;
			} else 
			if(xmlPath=="NoT13/responseTime"){
				return this.Not13_responsetime ;
			} else 
			if(xmlPath=="NoT14/accuracy"){
				return this.Not14_accuracy ;
			} else 
			if(xmlPath=="NoT14/responseTime"){
				return this.Not14_responsetime ;
			} else 
			if(xmlPath=="NoT15/accuracy"){
				return this.Not15_accuracy ;
			} else 
			if(xmlPath=="NoT15/responseTime"){
				return this.Not15_responsetime ;
			} else 
			if(xmlPath=="NoT16/accuracy"){
				return this.Not16_accuracy ;
			} else 
			if(xmlPath=="NoT16/responseTime"){
				return this.Not16_responsetime ;
			} else 
			if(xmlPath=="NoT17/accuracy"){
				return this.Not17_accuracy ;
			} else 
			if(xmlPath=="NoT17/responseTime"){
				return this.Not17_responsetime ;
			} else 
			if(xmlPath=="NoT18/accuracy"){
				return this.Not18_accuracy ;
			} else 
			if(xmlPath=="NoT18/responseTime"){
				return this.Not18_responsetime ;
			} else 
			if(xmlPath=="NoT19/accuracy"){
				return this.Not19_accuracy ;
			} else 
			if(xmlPath=="NoT19/responseTime"){
				return this.Not19_responsetime ;
			} else 
			if(xmlPath=="NoT20/accuracy"){
				return this.Not20_accuracy ;
			} else 
			if(xmlPath=="NoT20/responseTime"){
				return this.Not20_responsetime ;
			} else 
			if(xmlPath=="NoT21/accuracy"){
				return this.Not21_accuracy ;
			} else 
			if(xmlPath=="NoT21/responseTime"){
				return this.Not21_responsetime ;
			} else 
			if(xmlPath=="NoT22/accuracy"){
				return this.Not22_accuracy ;
			} else 
			if(xmlPath=="NoT22/responseTime"){
				return this.Not22_responsetime ;
			} else 
			if(xmlPath=="NoT23/accuracy"){
				return this.Not23_accuracy ;
			} else 
			if(xmlPath=="NoT23/responseTime"){
				return this.Not23_responsetime ;
			} else 
			if(xmlPath=="NoT24/accuracy"){
				return this.Not24_accuracy ;
			} else 
			if(xmlPath=="NoT24/responseTime"){
				return this.Not24_responsetime ;
			} else 
			if(xmlPath=="NoT25/accuracy"){
				return this.Not25_accuracy ;
			} else 
			if(xmlPath=="NoT25/responseTime"){
				return this.Not25_responsetime ;
			} else 
			if(xmlPath=="NoT26/accuracy"){
				return this.Not26_accuracy ;
			} else 
			if(xmlPath=="NoT26/responseTime"){
				return this.Not26_responsetime ;
			} else 
			if(xmlPath=="NoT27/accuracy"){
				return this.Not27_accuracy ;
			} else 
			if(xmlPath=="NoT27/responseTime"){
				return this.Not27_responsetime ;
			} else 
			if(xmlPath=="NoT28/accuracy"){
				return this.Not28_accuracy ;
			} else 
			if(xmlPath=="NoT28/responseTime"){
				return this.Not28_responsetime ;
			} else 
			if(xmlPath=="NoT29/accuracy"){
				return this.Not29_accuracy ;
			} else 
			if(xmlPath=="NoT29/responseTime"){
				return this.Not29_responsetime ;
			} else 
			if(xmlPath=="NoT30/accuracy"){
				return this.Not30_accuracy ;
			} else 
			if(xmlPath=="NoT30/responseTime"){
				return this.Not30_responsetime ;
			} else 
			if(xmlPath=="NoT31/accuracy"){
				return this.Not31_accuracy ;
			} else 
			if(xmlPath=="NoT31/responseTime"){
				return this.Not31_responsetime ;
			} else 
			if(xmlPath=="NoT32/accuracy"){
				return this.Not32_accuracy ;
			} else 
			if(xmlPath=="NoT32/responseTime"){
				return this.Not32_responsetime ;
			} else 
			if(xmlPath=="NoT33/accuracy"){
				return this.Not33_accuracy ;
			} else 
			if(xmlPath=="NoT33/responseTime"){
				return this.Not33_responsetime ;
			} else 
			if(xmlPath=="NoT34/accuracy"){
				return this.Not34_accuracy ;
			} else 
			if(xmlPath=="NoT34/responseTime"){
				return this.Not34_responsetime ;
			} else 
			if(xmlPath=="NoT35/accuracy"){
				return this.Not35_accuracy ;
			} else 
			if(xmlPath=="NoT35/responseTime"){
				return this.Not35_responsetime ;
			} else 
			if(xmlPath=="NoT36/accuracy"){
				return this.Not36_accuracy ;
			} else 
			if(xmlPath=="NoT36/responseTime"){
				return this.Not36_responsetime ;
			} else 
			if(xmlPath=="NoT37/accuracy"){
				return this.Not37_accuracy ;
			} else 
			if(xmlPath=="NoT37/responseTime"){
				return this.Not37_responsetime ;
			} else 
			if(xmlPath=="NoT38/accuracy"){
				return this.Not38_accuracy ;
			} else 
			if(xmlPath=="NoT38/responseTime"){
				return this.Not38_responsetime ;
			} else 
			if(xmlPath=="NoT39/accuracy"){
				return this.Not39_accuracy ;
			} else 
			if(xmlPath=="NoT39/responseTime"){
				return this.Not39_responsetime ;
			} else 
			if(xmlPath=="NoT40/accuracy"){
				return this.Not40_accuracy ;
			} else 
			if(xmlPath=="NoT40/responseTime"){
				return this.Not40_responsetime ;
			} else 
			if(xmlPath=="NoT41/accuracy"){
				return this.Not41_accuracy ;
			} else 
			if(xmlPath=="NoT41/responseTime"){
				return this.Not41_responsetime ;
			} else 
			if(xmlPath=="NoT42/accuracy"){
				return this.Not42_accuracy ;
			} else 
			if(xmlPath=="NoT42/responseTime"){
				return this.Not42_responsetime ;
			} else 
			if(xmlPath=="NoT43/accuracy"){
				return this.Not43_accuracy ;
			} else 
			if(xmlPath=="NoT43/responseTime"){
				return this.Not43_responsetime ;
			} else 
			if(xmlPath=="NoT44/accuracy"){
				return this.Not44_accuracy ;
			} else 
			if(xmlPath=="NoT44/responseTime"){
				return this.Not44_responsetime ;
			} else 
			if(xmlPath=="NoT45/accuracy"){
				return this.Not45_accuracy ;
			} else 
			if(xmlPath=="NoT45/responseTime"){
				return this.Not45_responsetime ;
			} else 
			if(xmlPath=="NoT46/accuracy"){
				return this.Not46_accuracy ;
			} else 
			if(xmlPath=="NoT46/responseTime"){
				return this.Not46_responsetime ;
			} else 
			if(xmlPath=="NoT47/accuracy"){
				return this.Not47_accuracy ;
			} else 
			if(xmlPath=="NoT47/responseTime"){
				return this.Not47_responsetime ;
			} else 
			if(xmlPath=="NoT48/accuracy"){
				return this.Not48_accuracy ;
			} else 
			if(xmlPath=="NoT48/responseTime"){
				return this.Not48_responsetime ;
			} else 
			if(xmlPath=="NoT49/accuracy"){
				return this.Not49_accuracy ;
			} else 
			if(xmlPath=="NoT49/responseTime"){
				return this.Not49_responsetime ;
			} else 
			if(xmlPath=="NoT50/accuracy"){
				return this.Not50_accuracy ;
			} else 
			if(xmlPath=="NoT50/responseTime"){
				return this.Not50_responsetime ;
			} else 
			if(xmlPath=="NoT51/accuracy"){
				return this.Not51_accuracy ;
			} else 
			if(xmlPath=="NoT51/responseTime"){
				return this.Not51_responsetime ;
			} else 
			if(xmlPath=="NoT52/accuracy"){
				return this.Not52_accuracy ;
			} else 
			if(xmlPath=="NoT52/responseTime"){
				return this.Not52_responsetime ;
			} else 
			if(xmlPath=="NoT53/accuracy"){
				return this.Not53_accuracy ;
			} else 
			if(xmlPath=="NoT53/responseTime"){
				return this.Not53_responsetime ;
			} else 
			if(xmlPath=="NoT54/accuracy"){
				return this.Not54_accuracy ;
			} else 
			if(xmlPath=="NoT54/responseTime"){
				return this.Not54_responsetime ;
			} else 
			if(xmlPath=="NoT55/accuracy"){
				return this.Not55_accuracy ;
			} else 
			if(xmlPath=="NoT55/responseTime"){
				return this.Not55_responsetime ;
			} else 
			if(xmlPath=="NoT56/accuracy"){
				return this.Not56_accuracy ;
			} else 
			if(xmlPath=="NoT56/responseTime"){
				return this.Not56_responsetime ;
			} else 
			if(xmlPath=="NoT57/accuracy"){
				return this.Not57_accuracy ;
			} else 
			if(xmlPath=="NoT57/responseTime"){
				return this.Not57_responsetime ;
			} else 
			if(xmlPath=="NoT58/accuracy"){
				return this.Not58_accuracy ;
			} else 
			if(xmlPath=="NoT58/responseTime"){
				return this.Not58_responsetime ;
			} else 
			if(xmlPath=="NoT59/accuracy"){
				return this.Not59_accuracy ;
			} else 
			if(xmlPath=="NoT59/responseTime"){
				return this.Not59_responsetime ;
			} else 
			if(xmlPath=="NoT60/accuracy"){
				return this.Not60_accuracy ;
			} else 
			if(xmlPath=="NoT60/responseTime"){
				return this.Not60_responsetime ;
			} else 
			if(xmlPath=="NoT61/accuracy"){
				return this.Not61_accuracy ;
			} else 
			if(xmlPath=="NoT61/responseTime"){
				return this.Not61_responsetime ;
			} else 
			if(xmlPath=="NoT62/accuracy"){
				return this.Not62_accuracy ;
			} else 
			if(xmlPath=="NoT62/responseTime"){
				return this.Not62_responsetime ;
			} else 
			if(xmlPath=="NoT63/accuracy"){
				return this.Not63_accuracy ;
			} else 
			if(xmlPath=="NoT63/responseTime"){
				return this.Not63_responsetime ;
			} else 
			if(xmlPath=="NoT64/accuracy"){
				return this.Not64_accuracy ;
			} else 
			if(xmlPath=="NoT64/responseTime"){
				return this.Not64_responsetime ;
			} else 
			if(xmlPath=="NoT65/accuracy"){
				return this.Not65_accuracy ;
			} else 
			if(xmlPath=="NoT65/responseTime"){
				return this.Not65_responsetime ;
			} else 
			if(xmlPath=="NoT66/accuracy"){
				return this.Not66_accuracy ;
			} else 
			if(xmlPath=="NoT66/responseTime"){
				return this.Not66_responsetime ;
			} else 
			if(xmlPath=="NoT67/accuracy"){
				return this.Not67_accuracy ;
			} else 
			if(xmlPath=="NoT67/responseTime"){
				return this.Not67_responsetime ;
			} else 
			if(xmlPath=="NoT68/accuracy"){
				return this.Not68_accuracy ;
			} else 
			if(xmlPath=="NoT68/responseTime"){
				return this.Not68_responsetime ;
			} else 
			if(xmlPath=="NoT69/accuracy"){
				return this.Not69_accuracy ;
			} else 
			if(xmlPath=="NoT69/responseTime"){
				return this.Not69_responsetime ;
			} else 
			if(xmlPath=="NoT70/accuracy"){
				return this.Not70_accuracy ;
			} else 
			if(xmlPath=="NoT70/responseTime"){
				return this.Not70_responsetime ;
			} else 
			if(xmlPath=="NoT71/accuracy"){
				return this.Not71_accuracy ;
			} else 
			if(xmlPath=="NoT71/responseTime"){
				return this.Not71_responsetime ;
			} else 
			if(xmlPath=="NoT72/accuracy"){
				return this.Not72_accuracy ;
			} else 
			if(xmlPath=="NoT72/responseTime"){
				return this.Not72_responsetime ;
			} else 
			if(xmlPath=="NoT73/accuracy"){
				return this.Not73_accuracy ;
			} else 
			if(xmlPath=="NoT73/responseTime"){
				return this.Not73_responsetime ;
			} else 
			if(xmlPath=="NoT74/accuracy"){
				return this.Not74_accuracy ;
			} else 
			if(xmlPath=="NoT74/responseTime"){
				return this.Not74_responsetime ;
			} else 
			if(xmlPath=="NoT75/accuracy"){
				return this.Not75_accuracy ;
			} else 
			if(xmlPath=="NoT75/responseTime"){
				return this.Not75_responsetime ;
			} else 
			if(xmlPath=="NoT76/accuracy"){
				return this.Not76_accuracy ;
			} else 
			if(xmlPath=="NoT76/responseTime"){
				return this.Not76_responsetime ;
			} else 
			if(xmlPath=="NoT77/accuracy"){
				return this.Not77_accuracy ;
			} else 
			if(xmlPath=="NoT77/responseTime"){
				return this.Not77_responsetime ;
			} else 
			if(xmlPath=="NoT78/accuracy"){
				return this.Not78_accuracy ;
			} else 
			if(xmlPath=="NoT78/responseTime"){
				return this.Not78_responsetime ;
			} else 
			if(xmlPath=="NoT79/accuracy"){
				return this.Not79_accuracy ;
			} else 
			if(xmlPath=="NoT79/responseTime"){
				return this.Not79_responsetime ;
			} else 
			if(xmlPath=="NoT80/accuracy"){
				return this.Not80_accuracy ;
			} else 
			if(xmlPath=="NoT80/responseTime"){
				return this.Not80_responsetime ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="YesT1/accuracy"){
				this.Yest1_accuracy=value;
			} else 
			if(xmlPath=="YesT1/responseTime"){
				this.Yest1_responsetime=value;
			} else 
			if(xmlPath=="YesT2/accuracy"){
				this.Yest2_accuracy=value;
			} else 
			if(xmlPath=="YesT2/responseTime"){
				this.Yest2_responsetime=value;
			} else 
			if(xmlPath=="YesT3/accuracy"){
				this.Yest3_accuracy=value;
			} else 
			if(xmlPath=="YesT3/responseTime"){
				this.Yest3_responsetime=value;
			} else 
			if(xmlPath=="YesT4/accuracy"){
				this.Yest4_accuracy=value;
			} else 
			if(xmlPath=="YesT4/responseTime"){
				this.Yest4_responsetime=value;
			} else 
			if(xmlPath=="YesT5/accuracy"){
				this.Yest5_accuracy=value;
			} else 
			if(xmlPath=="YesT5/responseTime"){
				this.Yest5_responsetime=value;
			} else 
			if(xmlPath=="YesT6/accuracy"){
				this.Yest6_accuracy=value;
			} else 
			if(xmlPath=="YesT6/responseTime"){
				this.Yest6_responsetime=value;
			} else 
			if(xmlPath=="YesT7/accuracy"){
				this.Yest7_accuracy=value;
			} else 
			if(xmlPath=="YesT7/responseTime"){
				this.Yest7_responsetime=value;
			} else 
			if(xmlPath=="YesT8/accuracy"){
				this.Yest8_accuracy=value;
			} else 
			if(xmlPath=="YesT8/responseTime"){
				this.Yest8_responsetime=value;
			} else 
			if(xmlPath=="YesT9/accuracy"){
				this.Yest9_accuracy=value;
			} else 
			if(xmlPath=="YesT9/responseTime"){
				this.Yest9_responsetime=value;
			} else 
			if(xmlPath=="YesT10/accuracy"){
				this.Yest10_accuracy=value;
			} else 
			if(xmlPath=="YesT10/responseTime"){
				this.Yest10_responsetime=value;
			} else 
			if(xmlPath=="YesT11/accuracy"){
				this.Yest11_accuracy=value;
			} else 
			if(xmlPath=="YesT11/responseTime"){
				this.Yest11_responsetime=value;
			} else 
			if(xmlPath=="YesT12/accuracy"){
				this.Yest12_accuracy=value;
			} else 
			if(xmlPath=="YesT12/responseTime"){
				this.Yest12_responsetime=value;
			} else 
			if(xmlPath=="YesT13/accuracy"){
				this.Yest13_accuracy=value;
			} else 
			if(xmlPath=="YesT13/responseTime"){
				this.Yest13_responsetime=value;
			} else 
			if(xmlPath=="YesT14/accuracy"){
				this.Yest14_accuracy=value;
			} else 
			if(xmlPath=="YesT14/responseTime"){
				this.Yest14_responsetime=value;
			} else 
			if(xmlPath=="YesT15/accuracy"){
				this.Yest15_accuracy=value;
			} else 
			if(xmlPath=="YesT15/responseTime"){
				this.Yest15_responsetime=value;
			} else 
			if(xmlPath=="YesT16/accuracy"){
				this.Yest16_accuracy=value;
			} else 
			if(xmlPath=="YesT16/responseTime"){
				this.Yest16_responsetime=value;
			} else 
			if(xmlPath=="YesT17/accuracy"){
				this.Yest17_accuracy=value;
			} else 
			if(xmlPath=="YesT17/responseTime"){
				this.Yest17_responsetime=value;
			} else 
			if(xmlPath=="YesT18/accuracy"){
				this.Yest18_accuracy=value;
			} else 
			if(xmlPath=="YesT18/responseTime"){
				this.Yest18_responsetime=value;
			} else 
			if(xmlPath=="YesT19/accuracy"){
				this.Yest19_accuracy=value;
			} else 
			if(xmlPath=="YesT19/responseTime"){
				this.Yest19_responsetime=value;
			} else 
			if(xmlPath=="YesT20/accuracy"){
				this.Yest20_accuracy=value;
			} else 
			if(xmlPath=="YesT20/responseTime"){
				this.Yest20_responsetime=value;
			} else 
			if(xmlPath=="YesT21/accuracy"){
				this.Yest21_accuracy=value;
			} else 
			if(xmlPath=="YesT21/responseTime"){
				this.Yest21_responsetime=value;
			} else 
			if(xmlPath=="YesT22/accuracy"){
				this.Yest22_accuracy=value;
			} else 
			if(xmlPath=="YesT22/responseTime"){
				this.Yest22_responsetime=value;
			} else 
			if(xmlPath=="YesT23/accuracy"){
				this.Yest23_accuracy=value;
			} else 
			if(xmlPath=="YesT23/responseTime"){
				this.Yest23_responsetime=value;
			} else 
			if(xmlPath=="YesT24/accuracy"){
				this.Yest24_accuracy=value;
			} else 
			if(xmlPath=="YesT24/responseTime"){
				this.Yest24_responsetime=value;
			} else 
			if(xmlPath=="YesT25/accuracy"){
				this.Yest25_accuracy=value;
			} else 
			if(xmlPath=="YesT25/responseTime"){
				this.Yest25_responsetime=value;
			} else 
			if(xmlPath=="YesT26/accuracy"){
				this.Yest26_accuracy=value;
			} else 
			if(xmlPath=="YesT26/responseTime"){
				this.Yest26_responsetime=value;
			} else 
			if(xmlPath=="YesT27/accuracy"){
				this.Yest27_accuracy=value;
			} else 
			if(xmlPath=="YesT27/responseTime"){
				this.Yest27_responsetime=value;
			} else 
			if(xmlPath=="YesT28/accuracy"){
				this.Yest28_accuracy=value;
			} else 
			if(xmlPath=="YesT28/responseTime"){
				this.Yest28_responsetime=value;
			} else 
			if(xmlPath=="YesT29/accuracy"){
				this.Yest29_accuracy=value;
			} else 
			if(xmlPath=="YesT29/responseTime"){
				this.Yest29_responsetime=value;
			} else 
			if(xmlPath=="YesT30/accuracy"){
				this.Yest30_accuracy=value;
			} else 
			if(xmlPath=="YesT30/responseTime"){
				this.Yest30_responsetime=value;
			} else 
			if(xmlPath=="YesT31/accuracy"){
				this.Yest31_accuracy=value;
			} else 
			if(xmlPath=="YesT31/responseTime"){
				this.Yest31_responsetime=value;
			} else 
			if(xmlPath=="YesT32/accuracy"){
				this.Yest32_accuracy=value;
			} else 
			if(xmlPath=="YesT32/responseTime"){
				this.Yest32_responsetime=value;
			} else 
			if(xmlPath=="YesT33/accuracy"){
				this.Yest33_accuracy=value;
			} else 
			if(xmlPath=="YesT33/responseTime"){
				this.Yest33_responsetime=value;
			} else 
			if(xmlPath=="YesT34/accuracy"){
				this.Yest34_accuracy=value;
			} else 
			if(xmlPath=="YesT34/responseTime"){
				this.Yest34_responsetime=value;
			} else 
			if(xmlPath=="YesT35/accuracy"){
				this.Yest35_accuracy=value;
			} else 
			if(xmlPath=="YesT35/responseTime"){
				this.Yest35_responsetime=value;
			} else 
			if(xmlPath=="YesT36/accuracy"){
				this.Yest36_accuracy=value;
			} else 
			if(xmlPath=="YesT36/responseTime"){
				this.Yest36_responsetime=value;
			} else 
			if(xmlPath=="YesT37/accuracy"){
				this.Yest37_accuracy=value;
			} else 
			if(xmlPath=="YesT37/responseTime"){
				this.Yest37_responsetime=value;
			} else 
			if(xmlPath=="YesT38/accuracy"){
				this.Yest38_accuracy=value;
			} else 
			if(xmlPath=="YesT38/responseTime"){
				this.Yest38_responsetime=value;
			} else 
			if(xmlPath=="YesT39/accuracy"){
				this.Yest39_accuracy=value;
			} else 
			if(xmlPath=="YesT39/responseTime"){
				this.Yest39_responsetime=value;
			} else 
			if(xmlPath=="YesT40/accuracy"){
				this.Yest40_accuracy=value;
			} else 
			if(xmlPath=="YesT40/responseTime"){
				this.Yest40_responsetime=value;
			} else 
			if(xmlPath=="YesT41/accuracy"){
				this.Yest41_accuracy=value;
			} else 
			if(xmlPath=="YesT41/responseTime"){
				this.Yest41_responsetime=value;
			} else 
			if(xmlPath=="YesT42/accuracy"){
				this.Yest42_accuracy=value;
			} else 
			if(xmlPath=="YesT42/responseTime"){
				this.Yest42_responsetime=value;
			} else 
			if(xmlPath=="YesT43/accuracy"){
				this.Yest43_accuracy=value;
			} else 
			if(xmlPath=="YesT43/responseTime"){
				this.Yest43_responsetime=value;
			} else 
			if(xmlPath=="YesT44/accuracy"){
				this.Yest44_accuracy=value;
			} else 
			if(xmlPath=="YesT44/responseTime"){
				this.Yest44_responsetime=value;
			} else 
			if(xmlPath=="YesT45/accuracy"){
				this.Yest45_accuracy=value;
			} else 
			if(xmlPath=="YesT45/responseTime"){
				this.Yest45_responsetime=value;
			} else 
			if(xmlPath=="YesT46/accuracy"){
				this.Yest46_accuracy=value;
			} else 
			if(xmlPath=="YesT46/responseTime"){
				this.Yest46_responsetime=value;
			} else 
			if(xmlPath=="YesT47/accuracy"){
				this.Yest47_accuracy=value;
			} else 
			if(xmlPath=="YesT47/responseTime"){
				this.Yest47_responsetime=value;
			} else 
			if(xmlPath=="YesT48/accuracy"){
				this.Yest48_accuracy=value;
			} else 
			if(xmlPath=="YesT48/responseTime"){
				this.Yest48_responsetime=value;
			} else 
			if(xmlPath=="YesT49/accuracy"){
				this.Yest49_accuracy=value;
			} else 
			if(xmlPath=="YesT49/responseTime"){
				this.Yest49_responsetime=value;
			} else 
			if(xmlPath=="YesT50/accuracy"){
				this.Yest50_accuracy=value;
			} else 
			if(xmlPath=="YesT50/responseTime"){
				this.Yest50_responsetime=value;
			} else 
			if(xmlPath=="YesT51/accuracy"){
				this.Yest51_accuracy=value;
			} else 
			if(xmlPath=="YesT51/responseTime"){
				this.Yest51_responsetime=value;
			} else 
			if(xmlPath=="YesT52/accuracy"){
				this.Yest52_accuracy=value;
			} else 
			if(xmlPath=="YesT52/responseTime"){
				this.Yest52_responsetime=value;
			} else 
			if(xmlPath=="YesT53/accuracy"){
				this.Yest53_accuracy=value;
			} else 
			if(xmlPath=="YesT53/responseTime"){
				this.Yest53_responsetime=value;
			} else 
			if(xmlPath=="YesT54/accuracy"){
				this.Yest54_accuracy=value;
			} else 
			if(xmlPath=="YesT54/responseTime"){
				this.Yest54_responsetime=value;
			} else 
			if(xmlPath=="YesT55/accuracy"){
				this.Yest55_accuracy=value;
			} else 
			if(xmlPath=="YesT55/responseTime"){
				this.Yest55_responsetime=value;
			} else 
			if(xmlPath=="YesT56/accuracy"){
				this.Yest56_accuracy=value;
			} else 
			if(xmlPath=="YesT56/responseTime"){
				this.Yest56_responsetime=value;
			} else 
			if(xmlPath=="YesT57/accuracy"){
				this.Yest57_accuracy=value;
			} else 
			if(xmlPath=="YesT57/responseTime"){
				this.Yest57_responsetime=value;
			} else 
			if(xmlPath=="YesT58/accuracy"){
				this.Yest58_accuracy=value;
			} else 
			if(xmlPath=="YesT58/responseTime"){
				this.Yest58_responsetime=value;
			} else 
			if(xmlPath=="YesT59/accuracy"){
				this.Yest59_accuracy=value;
			} else 
			if(xmlPath=="YesT59/responseTime"){
				this.Yest59_responsetime=value;
			} else 
			if(xmlPath=="YesT60/accuracy"){
				this.Yest60_accuracy=value;
			} else 
			if(xmlPath=="YesT60/responseTime"){
				this.Yest60_responsetime=value;
			} else 
			if(xmlPath=="YesT61/accuracy"){
				this.Yest61_accuracy=value;
			} else 
			if(xmlPath=="YesT61/responseTime"){
				this.Yest61_responsetime=value;
			} else 
			if(xmlPath=="YesT62/accuracy"){
				this.Yest62_accuracy=value;
			} else 
			if(xmlPath=="YesT62/responseTime"){
				this.Yest62_responsetime=value;
			} else 
			if(xmlPath=="YesT63/accuracy"){
				this.Yest63_accuracy=value;
			} else 
			if(xmlPath=="YesT63/responseTime"){
				this.Yest63_responsetime=value;
			} else 
			if(xmlPath=="YesT64/accuracy"){
				this.Yest64_accuracy=value;
			} else 
			if(xmlPath=="YesT64/responseTime"){
				this.Yest64_responsetime=value;
			} else 
			if(xmlPath=="YesT65/accuracy"){
				this.Yest65_accuracy=value;
			} else 
			if(xmlPath=="YesT65/responseTime"){
				this.Yest65_responsetime=value;
			} else 
			if(xmlPath=="YesT66/accuracy"){
				this.Yest66_accuracy=value;
			} else 
			if(xmlPath=="YesT66/responseTime"){
				this.Yest66_responsetime=value;
			} else 
			if(xmlPath=="YesT67/accuracy"){
				this.Yest67_accuracy=value;
			} else 
			if(xmlPath=="YesT67/responseTime"){
				this.Yest67_responsetime=value;
			} else 
			if(xmlPath=="YesT68/accuracy"){
				this.Yest68_accuracy=value;
			} else 
			if(xmlPath=="YesT68/responseTime"){
				this.Yest68_responsetime=value;
			} else 
			if(xmlPath=="YesT69/accuracy"){
				this.Yest69_accuracy=value;
			} else 
			if(xmlPath=="YesT69/responseTime"){
				this.Yest69_responsetime=value;
			} else 
			if(xmlPath=="YesT70/accuracy"){
				this.Yest70_accuracy=value;
			} else 
			if(xmlPath=="YesT70/responseTime"){
				this.Yest70_responsetime=value;
			} else 
			if(xmlPath=="YesT71/accuracy"){
				this.Yest71_accuracy=value;
			} else 
			if(xmlPath=="YesT71/responseTime"){
				this.Yest71_responsetime=value;
			} else 
			if(xmlPath=="YesT72/accuracy"){
				this.Yest72_accuracy=value;
			} else 
			if(xmlPath=="YesT72/responseTime"){
				this.Yest72_responsetime=value;
			} else 
			if(xmlPath=="YesT73/accuracy"){
				this.Yest73_accuracy=value;
			} else 
			if(xmlPath=="YesT73/responseTime"){
				this.Yest73_responsetime=value;
			} else 
			if(xmlPath=="YesT74/accuracy"){
				this.Yest74_accuracy=value;
			} else 
			if(xmlPath=="YesT74/responseTime"){
				this.Yest74_responsetime=value;
			} else 
			if(xmlPath=="YesT75/accuracy"){
				this.Yest75_accuracy=value;
			} else 
			if(xmlPath=="YesT75/responseTime"){
				this.Yest75_responsetime=value;
			} else 
			if(xmlPath=="YesT76/accuracy"){
				this.Yest76_accuracy=value;
			} else 
			if(xmlPath=="YesT76/responseTime"){
				this.Yest76_responsetime=value;
			} else 
			if(xmlPath=="YesT77/accuracy"){
				this.Yest77_accuracy=value;
			} else 
			if(xmlPath=="YesT77/responseTime"){
				this.Yest77_responsetime=value;
			} else 
			if(xmlPath=="YesT78/accuracy"){
				this.Yest78_accuracy=value;
			} else 
			if(xmlPath=="YesT78/responseTime"){
				this.Yest78_responsetime=value;
			} else 
			if(xmlPath=="YesT79/accuracy"){
				this.Yest79_accuracy=value;
			} else 
			if(xmlPath=="YesT79/responseTime"){
				this.Yest79_responsetime=value;
			} else 
			if(xmlPath=="YesT80/accuracy"){
				this.Yest80_accuracy=value;
			} else 
			if(xmlPath=="YesT80/responseTime"){
				this.Yest80_responsetime=value;
			} else 
			if(xmlPath=="NoT1/accuracy"){
				this.Not1_accuracy=value;
			} else 
			if(xmlPath=="NoT1/responseTime"){
				this.Not1_responsetime=value;
			} else 
			if(xmlPath=="NoT2/accuracy"){
				this.Not2_accuracy=value;
			} else 
			if(xmlPath=="NoT2/responseTime"){
				this.Not2_responsetime=value;
			} else 
			if(xmlPath=="NoT3/accuracy"){
				this.Not3_accuracy=value;
			} else 
			if(xmlPath=="NoT3/responseTime"){
				this.Not3_responsetime=value;
			} else 
			if(xmlPath=="NoT4/accuracy"){
				this.Not4_accuracy=value;
			} else 
			if(xmlPath=="NoT4/responseTime"){
				this.Not4_responsetime=value;
			} else 
			if(xmlPath=="NoT5/accuracy"){
				this.Not5_accuracy=value;
			} else 
			if(xmlPath=="NoT5/responseTime"){
				this.Not5_responsetime=value;
			} else 
			if(xmlPath=="NoT6/accuracy"){
				this.Not6_accuracy=value;
			} else 
			if(xmlPath=="NoT6/responseTime"){
				this.Not6_responsetime=value;
			} else 
			if(xmlPath=="NoT7/accuracy"){
				this.Not7_accuracy=value;
			} else 
			if(xmlPath=="NoT7/responseTime"){
				this.Not7_responsetime=value;
			} else 
			if(xmlPath=="NoT8/accuracy"){
				this.Not8_accuracy=value;
			} else 
			if(xmlPath=="NoT8/responseTime"){
				this.Not8_responsetime=value;
			} else 
			if(xmlPath=="NoT9/accuracy"){
				this.Not9_accuracy=value;
			} else 
			if(xmlPath=="NoT9/responseTime"){
				this.Not9_responsetime=value;
			} else 
			if(xmlPath=="NoT10/accuracy"){
				this.Not10_accuracy=value;
			} else 
			if(xmlPath=="NoT10/responseTime"){
				this.Not10_responsetime=value;
			} else 
			if(xmlPath=="NoT11/accuracy"){
				this.Not11_accuracy=value;
			} else 
			if(xmlPath=="NoT11/responseTime"){
				this.Not11_responsetime=value;
			} else 
			if(xmlPath=="NoT12/accuracy"){
				this.Not12_accuracy=value;
			} else 
			if(xmlPath=="NoT12/responseTime"){
				this.Not12_responsetime=value;
			} else 
			if(xmlPath=="NoT13/accuracy"){
				this.Not13_accuracy=value;
			} else 
			if(xmlPath=="NoT13/responseTime"){
				this.Not13_responsetime=value;
			} else 
			if(xmlPath=="NoT14/accuracy"){
				this.Not14_accuracy=value;
			} else 
			if(xmlPath=="NoT14/responseTime"){
				this.Not14_responsetime=value;
			} else 
			if(xmlPath=="NoT15/accuracy"){
				this.Not15_accuracy=value;
			} else 
			if(xmlPath=="NoT15/responseTime"){
				this.Not15_responsetime=value;
			} else 
			if(xmlPath=="NoT16/accuracy"){
				this.Not16_accuracy=value;
			} else 
			if(xmlPath=="NoT16/responseTime"){
				this.Not16_responsetime=value;
			} else 
			if(xmlPath=="NoT17/accuracy"){
				this.Not17_accuracy=value;
			} else 
			if(xmlPath=="NoT17/responseTime"){
				this.Not17_responsetime=value;
			} else 
			if(xmlPath=="NoT18/accuracy"){
				this.Not18_accuracy=value;
			} else 
			if(xmlPath=="NoT18/responseTime"){
				this.Not18_responsetime=value;
			} else 
			if(xmlPath=="NoT19/accuracy"){
				this.Not19_accuracy=value;
			} else 
			if(xmlPath=="NoT19/responseTime"){
				this.Not19_responsetime=value;
			} else 
			if(xmlPath=="NoT20/accuracy"){
				this.Not20_accuracy=value;
			} else 
			if(xmlPath=="NoT20/responseTime"){
				this.Not20_responsetime=value;
			} else 
			if(xmlPath=="NoT21/accuracy"){
				this.Not21_accuracy=value;
			} else 
			if(xmlPath=="NoT21/responseTime"){
				this.Not21_responsetime=value;
			} else 
			if(xmlPath=="NoT22/accuracy"){
				this.Not22_accuracy=value;
			} else 
			if(xmlPath=="NoT22/responseTime"){
				this.Not22_responsetime=value;
			} else 
			if(xmlPath=="NoT23/accuracy"){
				this.Not23_accuracy=value;
			} else 
			if(xmlPath=="NoT23/responseTime"){
				this.Not23_responsetime=value;
			} else 
			if(xmlPath=="NoT24/accuracy"){
				this.Not24_accuracy=value;
			} else 
			if(xmlPath=="NoT24/responseTime"){
				this.Not24_responsetime=value;
			} else 
			if(xmlPath=="NoT25/accuracy"){
				this.Not25_accuracy=value;
			} else 
			if(xmlPath=="NoT25/responseTime"){
				this.Not25_responsetime=value;
			} else 
			if(xmlPath=="NoT26/accuracy"){
				this.Not26_accuracy=value;
			} else 
			if(xmlPath=="NoT26/responseTime"){
				this.Not26_responsetime=value;
			} else 
			if(xmlPath=="NoT27/accuracy"){
				this.Not27_accuracy=value;
			} else 
			if(xmlPath=="NoT27/responseTime"){
				this.Not27_responsetime=value;
			} else 
			if(xmlPath=="NoT28/accuracy"){
				this.Not28_accuracy=value;
			} else 
			if(xmlPath=="NoT28/responseTime"){
				this.Not28_responsetime=value;
			} else 
			if(xmlPath=="NoT29/accuracy"){
				this.Not29_accuracy=value;
			} else 
			if(xmlPath=="NoT29/responseTime"){
				this.Not29_responsetime=value;
			} else 
			if(xmlPath=="NoT30/accuracy"){
				this.Not30_accuracy=value;
			} else 
			if(xmlPath=="NoT30/responseTime"){
				this.Not30_responsetime=value;
			} else 
			if(xmlPath=="NoT31/accuracy"){
				this.Not31_accuracy=value;
			} else 
			if(xmlPath=="NoT31/responseTime"){
				this.Not31_responsetime=value;
			} else 
			if(xmlPath=="NoT32/accuracy"){
				this.Not32_accuracy=value;
			} else 
			if(xmlPath=="NoT32/responseTime"){
				this.Not32_responsetime=value;
			} else 
			if(xmlPath=="NoT33/accuracy"){
				this.Not33_accuracy=value;
			} else 
			if(xmlPath=="NoT33/responseTime"){
				this.Not33_responsetime=value;
			} else 
			if(xmlPath=="NoT34/accuracy"){
				this.Not34_accuracy=value;
			} else 
			if(xmlPath=="NoT34/responseTime"){
				this.Not34_responsetime=value;
			} else 
			if(xmlPath=="NoT35/accuracy"){
				this.Not35_accuracy=value;
			} else 
			if(xmlPath=="NoT35/responseTime"){
				this.Not35_responsetime=value;
			} else 
			if(xmlPath=="NoT36/accuracy"){
				this.Not36_accuracy=value;
			} else 
			if(xmlPath=="NoT36/responseTime"){
				this.Not36_responsetime=value;
			} else 
			if(xmlPath=="NoT37/accuracy"){
				this.Not37_accuracy=value;
			} else 
			if(xmlPath=="NoT37/responseTime"){
				this.Not37_responsetime=value;
			} else 
			if(xmlPath=="NoT38/accuracy"){
				this.Not38_accuracy=value;
			} else 
			if(xmlPath=="NoT38/responseTime"){
				this.Not38_responsetime=value;
			} else 
			if(xmlPath=="NoT39/accuracy"){
				this.Not39_accuracy=value;
			} else 
			if(xmlPath=="NoT39/responseTime"){
				this.Not39_responsetime=value;
			} else 
			if(xmlPath=="NoT40/accuracy"){
				this.Not40_accuracy=value;
			} else 
			if(xmlPath=="NoT40/responseTime"){
				this.Not40_responsetime=value;
			} else 
			if(xmlPath=="NoT41/accuracy"){
				this.Not41_accuracy=value;
			} else 
			if(xmlPath=="NoT41/responseTime"){
				this.Not41_responsetime=value;
			} else 
			if(xmlPath=="NoT42/accuracy"){
				this.Not42_accuracy=value;
			} else 
			if(xmlPath=="NoT42/responseTime"){
				this.Not42_responsetime=value;
			} else 
			if(xmlPath=="NoT43/accuracy"){
				this.Not43_accuracy=value;
			} else 
			if(xmlPath=="NoT43/responseTime"){
				this.Not43_responsetime=value;
			} else 
			if(xmlPath=="NoT44/accuracy"){
				this.Not44_accuracy=value;
			} else 
			if(xmlPath=="NoT44/responseTime"){
				this.Not44_responsetime=value;
			} else 
			if(xmlPath=="NoT45/accuracy"){
				this.Not45_accuracy=value;
			} else 
			if(xmlPath=="NoT45/responseTime"){
				this.Not45_responsetime=value;
			} else 
			if(xmlPath=="NoT46/accuracy"){
				this.Not46_accuracy=value;
			} else 
			if(xmlPath=="NoT46/responseTime"){
				this.Not46_responsetime=value;
			} else 
			if(xmlPath=="NoT47/accuracy"){
				this.Not47_accuracy=value;
			} else 
			if(xmlPath=="NoT47/responseTime"){
				this.Not47_responsetime=value;
			} else 
			if(xmlPath=="NoT48/accuracy"){
				this.Not48_accuracy=value;
			} else 
			if(xmlPath=="NoT48/responseTime"){
				this.Not48_responsetime=value;
			} else 
			if(xmlPath=="NoT49/accuracy"){
				this.Not49_accuracy=value;
			} else 
			if(xmlPath=="NoT49/responseTime"){
				this.Not49_responsetime=value;
			} else 
			if(xmlPath=="NoT50/accuracy"){
				this.Not50_accuracy=value;
			} else 
			if(xmlPath=="NoT50/responseTime"){
				this.Not50_responsetime=value;
			} else 
			if(xmlPath=="NoT51/accuracy"){
				this.Not51_accuracy=value;
			} else 
			if(xmlPath=="NoT51/responseTime"){
				this.Not51_responsetime=value;
			} else 
			if(xmlPath=="NoT52/accuracy"){
				this.Not52_accuracy=value;
			} else 
			if(xmlPath=="NoT52/responseTime"){
				this.Not52_responsetime=value;
			} else 
			if(xmlPath=="NoT53/accuracy"){
				this.Not53_accuracy=value;
			} else 
			if(xmlPath=="NoT53/responseTime"){
				this.Not53_responsetime=value;
			} else 
			if(xmlPath=="NoT54/accuracy"){
				this.Not54_accuracy=value;
			} else 
			if(xmlPath=="NoT54/responseTime"){
				this.Not54_responsetime=value;
			} else 
			if(xmlPath=="NoT55/accuracy"){
				this.Not55_accuracy=value;
			} else 
			if(xmlPath=="NoT55/responseTime"){
				this.Not55_responsetime=value;
			} else 
			if(xmlPath=="NoT56/accuracy"){
				this.Not56_accuracy=value;
			} else 
			if(xmlPath=="NoT56/responseTime"){
				this.Not56_responsetime=value;
			} else 
			if(xmlPath=="NoT57/accuracy"){
				this.Not57_accuracy=value;
			} else 
			if(xmlPath=="NoT57/responseTime"){
				this.Not57_responsetime=value;
			} else 
			if(xmlPath=="NoT58/accuracy"){
				this.Not58_accuracy=value;
			} else 
			if(xmlPath=="NoT58/responseTime"){
				this.Not58_responsetime=value;
			} else 
			if(xmlPath=="NoT59/accuracy"){
				this.Not59_accuracy=value;
			} else 
			if(xmlPath=="NoT59/responseTime"){
				this.Not59_responsetime=value;
			} else 
			if(xmlPath=="NoT60/accuracy"){
				this.Not60_accuracy=value;
			} else 
			if(xmlPath=="NoT60/responseTime"){
				this.Not60_responsetime=value;
			} else 
			if(xmlPath=="NoT61/accuracy"){
				this.Not61_accuracy=value;
			} else 
			if(xmlPath=="NoT61/responseTime"){
				this.Not61_responsetime=value;
			} else 
			if(xmlPath=="NoT62/accuracy"){
				this.Not62_accuracy=value;
			} else 
			if(xmlPath=="NoT62/responseTime"){
				this.Not62_responsetime=value;
			} else 
			if(xmlPath=="NoT63/accuracy"){
				this.Not63_accuracy=value;
			} else 
			if(xmlPath=="NoT63/responseTime"){
				this.Not63_responsetime=value;
			} else 
			if(xmlPath=="NoT64/accuracy"){
				this.Not64_accuracy=value;
			} else 
			if(xmlPath=="NoT64/responseTime"){
				this.Not64_responsetime=value;
			} else 
			if(xmlPath=="NoT65/accuracy"){
				this.Not65_accuracy=value;
			} else 
			if(xmlPath=="NoT65/responseTime"){
				this.Not65_responsetime=value;
			} else 
			if(xmlPath=="NoT66/accuracy"){
				this.Not66_accuracy=value;
			} else 
			if(xmlPath=="NoT66/responseTime"){
				this.Not66_responsetime=value;
			} else 
			if(xmlPath=="NoT67/accuracy"){
				this.Not67_accuracy=value;
			} else 
			if(xmlPath=="NoT67/responseTime"){
				this.Not67_responsetime=value;
			} else 
			if(xmlPath=="NoT68/accuracy"){
				this.Not68_accuracy=value;
			} else 
			if(xmlPath=="NoT68/responseTime"){
				this.Not68_responsetime=value;
			} else 
			if(xmlPath=="NoT69/accuracy"){
				this.Not69_accuracy=value;
			} else 
			if(xmlPath=="NoT69/responseTime"){
				this.Not69_responsetime=value;
			} else 
			if(xmlPath=="NoT70/accuracy"){
				this.Not70_accuracy=value;
			} else 
			if(xmlPath=="NoT70/responseTime"){
				this.Not70_responsetime=value;
			} else 
			if(xmlPath=="NoT71/accuracy"){
				this.Not71_accuracy=value;
			} else 
			if(xmlPath=="NoT71/responseTime"){
				this.Not71_responsetime=value;
			} else 
			if(xmlPath=="NoT72/accuracy"){
				this.Not72_accuracy=value;
			} else 
			if(xmlPath=="NoT72/responseTime"){
				this.Not72_responsetime=value;
			} else 
			if(xmlPath=="NoT73/accuracy"){
				this.Not73_accuracy=value;
			} else 
			if(xmlPath=="NoT73/responseTime"){
				this.Not73_responsetime=value;
			} else 
			if(xmlPath=="NoT74/accuracy"){
				this.Not74_accuracy=value;
			} else 
			if(xmlPath=="NoT74/responseTime"){
				this.Not74_responsetime=value;
			} else 
			if(xmlPath=="NoT75/accuracy"){
				this.Not75_accuracy=value;
			} else 
			if(xmlPath=="NoT75/responseTime"){
				this.Not75_responsetime=value;
			} else 
			if(xmlPath=="NoT76/accuracy"){
				this.Not76_accuracy=value;
			} else 
			if(xmlPath=="NoT76/responseTime"){
				this.Not76_responsetime=value;
			} else 
			if(xmlPath=="NoT77/accuracy"){
				this.Not77_accuracy=value;
			} else 
			if(xmlPath=="NoT77/responseTime"){
				this.Not77_responsetime=value;
			} else 
			if(xmlPath=="NoT78/accuracy"){
				this.Not78_accuracy=value;
			} else 
			if(xmlPath=="NoT78/responseTime"){
				this.Not78_responsetime=value;
			} else 
			if(xmlPath=="NoT79/accuracy"){
				this.Not79_accuracy=value;
			} else 
			if(xmlPath=="NoT79/responseTime"){
				this.Not79_responsetime=value;
			} else 
			if(xmlPath=="NoT80/accuracy"){
				this.Not80_accuracy=value;
			} else 
			if(xmlPath=="NoT80/responseTime"){
				this.Not80_responsetime=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="YesT1/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT1/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT2/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT2/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT3/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT3/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT4/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT4/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT5/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT5/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT6/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT6/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT7/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT7/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT8/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT8/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT9/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT9/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT10/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT10/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT11/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT11/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT12/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT12/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT13/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT13/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT14/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT14/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT15/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT15/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT16/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT16/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT17/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT17/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT18/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT18/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT19/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT19/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT20/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT20/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT21/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT21/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT22/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT22/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT23/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT23/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT24/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT24/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT25/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT25/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT26/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT26/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT27/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT27/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT28/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT28/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT29/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT29/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT30/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT30/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT31/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT31/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT32/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT32/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT33/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT33/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT34/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT34/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT35/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT35/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT36/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT36/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT37/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT37/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT38/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT38/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT39/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT39/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT40/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT40/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT41/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT41/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT42/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT42/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT43/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT43/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT44/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT44/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT45/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT45/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT46/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT46/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT47/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT47/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT48/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT48/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT49/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT49/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT50/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT50/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT51/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT51/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT52/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT52/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT53/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT53/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT54/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT54/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT55/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT55/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT56/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT56/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT57/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT57/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT58/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT58/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT59/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT59/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT60/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT60/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT61/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT61/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT62/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT62/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT63/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT63/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT64/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT64/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT65/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT65/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT66/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT66/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT67/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT67/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT68/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT68/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT69/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT69/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT70/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT70/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT71/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT71/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT72/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT72/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT73/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT73/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT74/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT74/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT75/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT75/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT76/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT76/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT77/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT77/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT78/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT78/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT79/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT79/responseTime"){
			return "field_data";
		}else if (xmlPath=="YesT80/accuracy"){
			return "field_data";
		}else if (xmlPath=="YesT80/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT1/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT1/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT2/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT2/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT3/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT3/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT4/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT4/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT5/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT5/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT6/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT6/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT7/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT7/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT8/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT8/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT9/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT9/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT10/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT10/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT11/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT11/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT12/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT12/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT13/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT13/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT14/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT14/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT15/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT15/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT16/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT16/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT17/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT17/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT18/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT18/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT19/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT19/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT20/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT20/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT21/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT21/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT22/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT22/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT23/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT23/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT24/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT24/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT25/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT25/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT26/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT26/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT27/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT27/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT28/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT28/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT29/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT29/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT30/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT30/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT31/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT31/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT32/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT32/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT33/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT33/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT34/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT34/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT35/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT35/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT36/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT36/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT37/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT37/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT38/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT38/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT39/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT39/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT40/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT40/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT41/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT41/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT42/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT42/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT43/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT43/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT44/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT44/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT45/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT45/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT46/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT46/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT47/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT47/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT48/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT48/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT49/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT49/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT50/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT50/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT51/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT51/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT52/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT52/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT53/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT53/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT54/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT54/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT55/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT55/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT56/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT56/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT57/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT57/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT58/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT58/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT59/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT59/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT60/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT60/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT61/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT61/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT62/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT62/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT63/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT63/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT64/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT64/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT65/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT65/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT66/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT66/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT67/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT67/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT68/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT68/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT69/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT69/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT70/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT70/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT71/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT71/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT72/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT72/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT73/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT73/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT74/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT74/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT75/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT75/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT76/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT76/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT77/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT77/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT78/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT78/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT79/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT79/responseTime"){
			return "field_data";
		}else if (xmlPath=="NoT80/accuracy"){
			return "field_data";
		}else if (xmlPath=="NoT80/responseTime"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cbat:Categorization";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cbat:Categorization>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
			var child0=0;
			var att0=0;
			if(this.Yest1_responsetime!=null)
			child0++;
			if(this.Yest1_accuracy!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<cbat:YesT1";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest1_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest1_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest1_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest1_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT1>";
			}
			}

			var child1=0;
			var att1=0;
			if(this.Yest2_responsetime!=null)
			child1++;
			if(this.Yest2_accuracy!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<cbat:YesT2";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest2_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest2_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest2_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest2_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT2>";
			}
			}

			var child2=0;
			var att2=0;
			if(this.Yest3_responsetime!=null)
			child2++;
			if(this.Yest3_accuracy!=null)
			child2++;
			if(child2>0 || att2>0){
				xmlTxt+="\n<cbat:YesT3";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest3_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest3_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest3_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest3_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT3>";
			}
			}

			var child3=0;
			var att3=0;
			if(this.Yest4_responsetime!=null)
			child3++;
			if(this.Yest4_accuracy!=null)
			child3++;
			if(child3>0 || att3>0){
				xmlTxt+="\n<cbat:YesT4";
			if(child3==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest4_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest4_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest4_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest4_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT4>";
			}
			}

			var child4=0;
			var att4=0;
			if(this.Yest5_responsetime!=null)
			child4++;
			if(this.Yest5_accuracy!=null)
			child4++;
			if(child4>0 || att4>0){
				xmlTxt+="\n<cbat:YesT5";
			if(child4==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest5_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest5_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest5_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest5_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT5>";
			}
			}

			var child5=0;
			var att5=0;
			if(this.Yest6_responsetime!=null)
			child5++;
			if(this.Yest6_accuracy!=null)
			child5++;
			if(child5>0 || att5>0){
				xmlTxt+="\n<cbat:YesT6";
			if(child5==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest6_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest6_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest6_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest6_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT6>";
			}
			}

			var child6=0;
			var att6=0;
			if(this.Yest7_responsetime!=null)
			child6++;
			if(this.Yest7_accuracy!=null)
			child6++;
			if(child6>0 || att6>0){
				xmlTxt+="\n<cbat:YesT7";
			if(child6==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest7_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest7_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest7_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest7_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT7>";
			}
			}

			var child7=0;
			var att7=0;
			if(this.Yest8_accuracy!=null)
			child7++;
			if(this.Yest8_responsetime!=null)
			child7++;
			if(child7>0 || att7>0){
				xmlTxt+="\n<cbat:YesT8";
			if(child7==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest8_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest8_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest8_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest8_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT8>";
			}
			}

			var child8=0;
			var att8=0;
			if(this.Yest9_accuracy!=null)
			child8++;
			if(this.Yest9_responsetime!=null)
			child8++;
			if(child8>0 || att8>0){
				xmlTxt+="\n<cbat:YesT9";
			if(child8==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest9_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest9_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest9_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest9_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT9>";
			}
			}

			var child9=0;
			var att9=0;
			if(this.Yest10_accuracy!=null)
			child9++;
			if(this.Yest10_responsetime!=null)
			child9++;
			if(child9>0 || att9>0){
				xmlTxt+="\n<cbat:YesT10";
			if(child9==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest10_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest10_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest10_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest10_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT10>";
			}
			}

			var child10=0;
			var att10=0;
			if(this.Yest11_accuracy!=null)
			child10++;
			if(this.Yest11_responsetime!=null)
			child10++;
			if(child10>0 || att10>0){
				xmlTxt+="\n<cbat:YesT11";
			if(child10==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest11_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest11_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest11_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest11_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT11>";
			}
			}

			var child11=0;
			var att11=0;
			if(this.Yest12_responsetime!=null)
			child11++;
			if(this.Yest12_accuracy!=null)
			child11++;
			if(child11>0 || att11>0){
				xmlTxt+="\n<cbat:YesT12";
			if(child11==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest12_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest12_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest12_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest12_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT12>";
			}
			}

			var child12=0;
			var att12=0;
			if(this.Yest13_responsetime!=null)
			child12++;
			if(this.Yest13_accuracy!=null)
			child12++;
			if(child12>0 || att12>0){
				xmlTxt+="\n<cbat:YesT13";
			if(child12==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest13_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest13_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest13_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest13_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT13>";
			}
			}

			var child13=0;
			var att13=0;
			if(this.Yest14_responsetime!=null)
			child13++;
			if(this.Yest14_accuracy!=null)
			child13++;
			if(child13>0 || att13>0){
				xmlTxt+="\n<cbat:YesT14";
			if(child13==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest14_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest14_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest14_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest14_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT14>";
			}
			}

			var child14=0;
			var att14=0;
			if(this.Yest15_responsetime!=null)
			child14++;
			if(this.Yest15_accuracy!=null)
			child14++;
			if(child14>0 || att14>0){
				xmlTxt+="\n<cbat:YesT15";
			if(child14==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest15_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest15_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest15_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest15_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT15>";
			}
			}

			var child15=0;
			var att15=0;
			if(this.Yest16_accuracy!=null)
			child15++;
			if(this.Yest16_responsetime!=null)
			child15++;
			if(child15>0 || att15>0){
				xmlTxt+="\n<cbat:YesT16";
			if(child15==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest16_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest16_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest16_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest16_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT16>";
			}
			}

			var child16=0;
			var att16=0;
			if(this.Yest17_responsetime!=null)
			child16++;
			if(this.Yest17_accuracy!=null)
			child16++;
			if(child16>0 || att16>0){
				xmlTxt+="\n<cbat:YesT17";
			if(child16==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest17_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest17_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest17_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest17_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT17>";
			}
			}

			var child17=0;
			var att17=0;
			if(this.Yest18_accuracy!=null)
			child17++;
			if(this.Yest18_responsetime!=null)
			child17++;
			if(child17>0 || att17>0){
				xmlTxt+="\n<cbat:YesT18";
			if(child17==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest18_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest18_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest18_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest18_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT18>";
			}
			}

			var child18=0;
			var att18=0;
			if(this.Yest19_accuracy!=null)
			child18++;
			if(this.Yest19_responsetime!=null)
			child18++;
			if(child18>0 || att18>0){
				xmlTxt+="\n<cbat:YesT19";
			if(child18==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest19_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest19_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest19_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest19_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT19>";
			}
			}

			var child19=0;
			var att19=0;
			if(this.Yest20_responsetime!=null)
			child19++;
			if(this.Yest20_accuracy!=null)
			child19++;
			if(child19>0 || att19>0){
				xmlTxt+="\n<cbat:YesT20";
			if(child19==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest20_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest20_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest20_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest20_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT20>";
			}
			}

			var child20=0;
			var att20=0;
			if(this.Yest21_responsetime!=null)
			child20++;
			if(this.Yest21_accuracy!=null)
			child20++;
			if(child20>0 || att20>0){
				xmlTxt+="\n<cbat:YesT21";
			if(child20==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest21_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest21_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest21_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest21_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT21>";
			}
			}

			var child21=0;
			var att21=0;
			if(this.Yest22_responsetime!=null)
			child21++;
			if(this.Yest22_accuracy!=null)
			child21++;
			if(child21>0 || att21>0){
				xmlTxt+="\n<cbat:YesT22";
			if(child21==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest22_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest22_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest22_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest22_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT22>";
			}
			}

			var child22=0;
			var att22=0;
			if(this.Yest23_accuracy!=null)
			child22++;
			if(this.Yest23_responsetime!=null)
			child22++;
			if(child22>0 || att22>0){
				xmlTxt+="\n<cbat:YesT23";
			if(child22==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest23_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest23_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest23_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest23_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT23>";
			}
			}

			var child23=0;
			var att23=0;
			if(this.Yest24_accuracy!=null)
			child23++;
			if(this.Yest24_responsetime!=null)
			child23++;
			if(child23>0 || att23>0){
				xmlTxt+="\n<cbat:YesT24";
			if(child23==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest24_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest24_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest24_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest24_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT24>";
			}
			}

			var child24=0;
			var att24=0;
			if(this.Yest25_responsetime!=null)
			child24++;
			if(this.Yest25_accuracy!=null)
			child24++;
			if(child24>0 || att24>0){
				xmlTxt+="\n<cbat:YesT25";
			if(child24==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest25_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest25_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest25_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest25_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT25>";
			}
			}

			var child25=0;
			var att25=0;
			if(this.Yest26_responsetime!=null)
			child25++;
			if(this.Yest26_accuracy!=null)
			child25++;
			if(child25>0 || att25>0){
				xmlTxt+="\n<cbat:YesT26";
			if(child25==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest26_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest26_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest26_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest26_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT26>";
			}
			}

			var child26=0;
			var att26=0;
			if(this.Yest27_responsetime!=null)
			child26++;
			if(this.Yest27_accuracy!=null)
			child26++;
			if(child26>0 || att26>0){
				xmlTxt+="\n<cbat:YesT27";
			if(child26==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest27_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest27_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest27_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest27_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT27>";
			}
			}

			var child27=0;
			var att27=0;
			if(this.Yest28_responsetime!=null)
			child27++;
			if(this.Yest28_accuracy!=null)
			child27++;
			if(child27>0 || att27>0){
				xmlTxt+="\n<cbat:YesT28";
			if(child27==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest28_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest28_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest28_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest28_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT28>";
			}
			}

			var child28=0;
			var att28=0;
			if(this.Yest29_responsetime!=null)
			child28++;
			if(this.Yest29_accuracy!=null)
			child28++;
			if(child28>0 || att28>0){
				xmlTxt+="\n<cbat:YesT29";
			if(child28==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest29_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest29_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest29_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest29_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT29>";
			}
			}

			var child29=0;
			var att29=0;
			if(this.Yest30_accuracy!=null)
			child29++;
			if(this.Yest30_responsetime!=null)
			child29++;
			if(child29>0 || att29>0){
				xmlTxt+="\n<cbat:YesT30";
			if(child29==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest30_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest30_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest30_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest30_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT30>";
			}
			}

			var child30=0;
			var att30=0;
			if(this.Yest31_accuracy!=null)
			child30++;
			if(this.Yest31_responsetime!=null)
			child30++;
			if(child30>0 || att30>0){
				xmlTxt+="\n<cbat:YesT31";
			if(child30==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest31_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest31_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest31_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest31_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT31>";
			}
			}

			var child31=0;
			var att31=0;
			if(this.Yest32_accuracy!=null)
			child31++;
			if(this.Yest32_responsetime!=null)
			child31++;
			if(child31>0 || att31>0){
				xmlTxt+="\n<cbat:YesT32";
			if(child31==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest32_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest32_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest32_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest32_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT32>";
			}
			}

			var child32=0;
			var att32=0;
			if(this.Yest33_accuracy!=null)
			child32++;
			if(this.Yest33_responsetime!=null)
			child32++;
			if(child32>0 || att32>0){
				xmlTxt+="\n<cbat:YesT33";
			if(child32==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest33_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest33_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest33_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest33_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT33>";
			}
			}

			var child33=0;
			var att33=0;
			if(this.Yest34_accuracy!=null)
			child33++;
			if(this.Yest34_responsetime!=null)
			child33++;
			if(child33>0 || att33>0){
				xmlTxt+="\n<cbat:YesT34";
			if(child33==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest34_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest34_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest34_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest34_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT34>";
			}
			}

			var child34=0;
			var att34=0;
			if(this.Yest35_accuracy!=null)
			child34++;
			if(this.Yest35_responsetime!=null)
			child34++;
			if(child34>0 || att34>0){
				xmlTxt+="\n<cbat:YesT35";
			if(child34==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest35_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest35_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest35_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest35_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT35>";
			}
			}

			var child35=0;
			var att35=0;
			if(this.Yest36_accuracy!=null)
			child35++;
			if(this.Yest36_responsetime!=null)
			child35++;
			if(child35>0 || att35>0){
				xmlTxt+="\n<cbat:YesT36";
			if(child35==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest36_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest36_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest36_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest36_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT36>";
			}
			}

			var child36=0;
			var att36=0;
			if(this.Yest37_accuracy!=null)
			child36++;
			if(this.Yest37_responsetime!=null)
			child36++;
			if(child36>0 || att36>0){
				xmlTxt+="\n<cbat:YesT37";
			if(child36==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest37_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest37_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest37_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest37_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT37>";
			}
			}

			var child37=0;
			var att37=0;
			if(this.Yest38_responsetime!=null)
			child37++;
			if(this.Yest38_accuracy!=null)
			child37++;
			if(child37>0 || att37>0){
				xmlTxt+="\n<cbat:YesT38";
			if(child37==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest38_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest38_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest38_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest38_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT38>";
			}
			}

			var child38=0;
			var att38=0;
			if(this.Yest39_responsetime!=null)
			child38++;
			if(this.Yest39_accuracy!=null)
			child38++;
			if(child38>0 || att38>0){
				xmlTxt+="\n<cbat:YesT39";
			if(child38==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest39_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest39_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest39_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest39_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT39>";
			}
			}

			var child39=0;
			var att39=0;
			if(this.Yest40_accuracy!=null)
			child39++;
			if(this.Yest40_responsetime!=null)
			child39++;
			if(child39>0 || att39>0){
				xmlTxt+="\n<cbat:YesT40";
			if(child39==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest40_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest40_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest40_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest40_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT40>";
			}
			}

			var child40=0;
			var att40=0;
			if(this.Yest41_accuracy!=null)
			child40++;
			if(this.Yest41_responsetime!=null)
			child40++;
			if(child40>0 || att40>0){
				xmlTxt+="\n<cbat:YesT41";
			if(child40==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest41_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest41_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest41_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest41_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT41>";
			}
			}

			var child41=0;
			var att41=0;
			if(this.Yest42_accuracy!=null)
			child41++;
			if(this.Yest42_responsetime!=null)
			child41++;
			if(child41>0 || att41>0){
				xmlTxt+="\n<cbat:YesT42";
			if(child41==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest42_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest42_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest42_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest42_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT42>";
			}
			}

			var child42=0;
			var att42=0;
			if(this.Yest43_accuracy!=null)
			child42++;
			if(this.Yest43_responsetime!=null)
			child42++;
			if(child42>0 || att42>0){
				xmlTxt+="\n<cbat:YesT43";
			if(child42==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest43_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest43_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest43_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest43_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT43>";
			}
			}

			var child43=0;
			var att43=0;
			if(this.Yest44_responsetime!=null)
			child43++;
			if(this.Yest44_accuracy!=null)
			child43++;
			if(child43>0 || att43>0){
				xmlTxt+="\n<cbat:YesT44";
			if(child43==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest44_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest44_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest44_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest44_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT44>";
			}
			}

			var child44=0;
			var att44=0;
			if(this.Yest45_responsetime!=null)
			child44++;
			if(this.Yest45_accuracy!=null)
			child44++;
			if(child44>0 || att44>0){
				xmlTxt+="\n<cbat:YesT45";
			if(child44==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest45_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest45_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest45_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest45_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT45>";
			}
			}

			var child45=0;
			var att45=0;
			if(this.Yest46_responsetime!=null)
			child45++;
			if(this.Yest46_accuracy!=null)
			child45++;
			if(child45>0 || att45>0){
				xmlTxt+="\n<cbat:YesT46";
			if(child45==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest46_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest46_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest46_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest46_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT46>";
			}
			}

			var child46=0;
			var att46=0;
			if(this.Yest47_responsetime!=null)
			child46++;
			if(this.Yest47_accuracy!=null)
			child46++;
			if(child46>0 || att46>0){
				xmlTxt+="\n<cbat:YesT47";
			if(child46==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest47_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest47_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest47_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest47_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT47>";
			}
			}

			var child47=0;
			var att47=0;
			if(this.Yest48_accuracy!=null)
			child47++;
			if(this.Yest48_responsetime!=null)
			child47++;
			if(child47>0 || att47>0){
				xmlTxt+="\n<cbat:YesT48";
			if(child47==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest48_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest48_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest48_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest48_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT48>";
			}
			}

			var child48=0;
			var att48=0;
			if(this.Yest49_accuracy!=null)
			child48++;
			if(this.Yest49_responsetime!=null)
			child48++;
			if(child48>0 || att48>0){
				xmlTxt+="\n<cbat:YesT49";
			if(child48==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest49_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest49_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest49_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest49_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT49>";
			}
			}

			var child49=0;
			var att49=0;
			if(this.Yest50_responsetime!=null)
			child49++;
			if(this.Yest50_accuracy!=null)
			child49++;
			if(child49>0 || att49>0){
				xmlTxt+="\n<cbat:YesT50";
			if(child49==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest50_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest50_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest50_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest50_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT50>";
			}
			}

			var child50=0;
			var att50=0;
			if(this.Yest51_responsetime!=null)
			child50++;
			if(this.Yest51_accuracy!=null)
			child50++;
			if(child50>0 || att50>0){
				xmlTxt+="\n<cbat:YesT51";
			if(child50==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest51_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest51_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest51_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest51_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT51>";
			}
			}

			var child51=0;
			var att51=0;
			if(this.Yest52_responsetime!=null)
			child51++;
			if(this.Yest52_accuracy!=null)
			child51++;
			if(child51>0 || att51>0){
				xmlTxt+="\n<cbat:YesT52";
			if(child51==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest52_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest52_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest52_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest52_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT52>";
			}
			}

			var child52=0;
			var att52=0;
			if(this.Yest53_responsetime!=null)
			child52++;
			if(this.Yest53_accuracy!=null)
			child52++;
			if(child52>0 || att52>0){
				xmlTxt+="\n<cbat:YesT53";
			if(child52==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest53_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest53_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest53_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest53_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT53>";
			}
			}

			var child53=0;
			var att53=0;
			if(this.Yest54_responsetime!=null)
			child53++;
			if(this.Yest54_accuracy!=null)
			child53++;
			if(child53>0 || att53>0){
				xmlTxt+="\n<cbat:YesT54";
			if(child53==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest54_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest54_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest54_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest54_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT54>";
			}
			}

			var child54=0;
			var att54=0;
			if(this.Yest55_accuracy!=null)
			child54++;
			if(this.Yest55_responsetime!=null)
			child54++;
			if(child54>0 || att54>0){
				xmlTxt+="\n<cbat:YesT55";
			if(child54==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest55_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest55_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest55_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest55_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT55>";
			}
			}

			var child55=0;
			var att55=0;
			if(this.Yest56_accuracy!=null)
			child55++;
			if(this.Yest56_responsetime!=null)
			child55++;
			if(child55>0 || att55>0){
				xmlTxt+="\n<cbat:YesT56";
			if(child55==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest56_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest56_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest56_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest56_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT56>";
			}
			}

			var child56=0;
			var att56=0;
			if(this.Yest57_responsetime!=null)
			child56++;
			if(this.Yest57_accuracy!=null)
			child56++;
			if(child56>0 || att56>0){
				xmlTxt+="\n<cbat:YesT57";
			if(child56==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest57_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest57_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest57_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest57_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT57>";
			}
			}

			var child57=0;
			var att57=0;
			if(this.Yest58_responsetime!=null)
			child57++;
			if(this.Yest58_accuracy!=null)
			child57++;
			if(child57>0 || att57>0){
				xmlTxt+="\n<cbat:YesT58";
			if(child57==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest58_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest58_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest58_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest58_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT58>";
			}
			}

			var child58=0;
			var att58=0;
			if(this.Yest59_responsetime!=null)
			child58++;
			if(this.Yest59_accuracy!=null)
			child58++;
			if(child58>0 || att58>0){
				xmlTxt+="\n<cbat:YesT59";
			if(child58==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest59_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest59_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest59_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest59_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT59>";
			}
			}

			var child59=0;
			var att59=0;
			if(this.Yest60_accuracy!=null)
			child59++;
			if(this.Yest60_responsetime!=null)
			child59++;
			if(child59>0 || att59>0){
				xmlTxt+="\n<cbat:YesT60";
			if(child59==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest60_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest60_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest60_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest60_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT60>";
			}
			}

			var child60=0;
			var att60=0;
			if(this.Yest61_responsetime!=null)
			child60++;
			if(this.Yest61_accuracy!=null)
			child60++;
			if(child60>0 || att60>0){
				xmlTxt+="\n<cbat:YesT61";
			if(child60==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest61_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest61_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest61_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest61_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT61>";
			}
			}

			var child61=0;
			var att61=0;
			if(this.Yest62_accuracy!=null)
			child61++;
			if(this.Yest62_responsetime!=null)
			child61++;
			if(child61>0 || att61>0){
				xmlTxt+="\n<cbat:YesT62";
			if(child61==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest62_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest62_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest62_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest62_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT62>";
			}
			}

			var child62=0;
			var att62=0;
			if(this.Yest63_accuracy!=null)
			child62++;
			if(this.Yest63_responsetime!=null)
			child62++;
			if(child62>0 || att62>0){
				xmlTxt+="\n<cbat:YesT63";
			if(child62==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest63_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest63_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest63_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest63_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT63>";
			}
			}

			var child63=0;
			var att63=0;
			if(this.Yest64_accuracy!=null)
			child63++;
			if(this.Yest64_responsetime!=null)
			child63++;
			if(child63>0 || att63>0){
				xmlTxt+="\n<cbat:YesT64";
			if(child63==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest64_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest64_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest64_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest64_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT64>";
			}
			}

			var child64=0;
			var att64=0;
			if(this.Yest65_accuracy!=null)
			child64++;
			if(this.Yest65_responsetime!=null)
			child64++;
			if(child64>0 || att64>0){
				xmlTxt+="\n<cbat:YesT65";
			if(child64==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest65_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest65_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest65_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest65_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT65>";
			}
			}

			var child65=0;
			var att65=0;
			if(this.Yest66_accuracy!=null)
			child65++;
			if(this.Yest66_responsetime!=null)
			child65++;
			if(child65>0 || att65>0){
				xmlTxt+="\n<cbat:YesT66";
			if(child65==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest66_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest66_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest66_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest66_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT66>";
			}
			}

			var child66=0;
			var att66=0;
			if(this.Yest67_accuracy!=null)
			child66++;
			if(this.Yest67_responsetime!=null)
			child66++;
			if(child66>0 || att66>0){
				xmlTxt+="\n<cbat:YesT67";
			if(child66==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest67_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest67_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest67_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest67_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT67>";
			}
			}

			var child67=0;
			var att67=0;
			if(this.Yest68_accuracy!=null)
			child67++;
			if(this.Yest68_responsetime!=null)
			child67++;
			if(child67>0 || att67>0){
				xmlTxt+="\n<cbat:YesT68";
			if(child67==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest68_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest68_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest68_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest68_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT68>";
			}
			}

			var child68=0;
			var att68=0;
			if(this.Yest69_accuracy!=null)
			child68++;
			if(this.Yest69_responsetime!=null)
			child68++;
			if(child68>0 || att68>0){
				xmlTxt+="\n<cbat:YesT69";
			if(child68==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest69_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest69_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest69_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest69_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT69>";
			}
			}

			var child69=0;
			var att69=0;
			if(this.Yest70_accuracy!=null)
			child69++;
			if(this.Yest70_responsetime!=null)
			child69++;
			if(child69>0 || att69>0){
				xmlTxt+="\n<cbat:YesT70";
			if(child69==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest70_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest70_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest70_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest70_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT70>";
			}
			}

			var child70=0;
			var att70=0;
			if(this.Yest71_accuracy!=null)
			child70++;
			if(this.Yest71_responsetime!=null)
			child70++;
			if(child70>0 || att70>0){
				xmlTxt+="\n<cbat:YesT71";
			if(child70==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest71_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest71_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest71_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest71_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT71>";
			}
			}

			var child71=0;
			var att71=0;
			if(this.Yest72_accuracy!=null)
			child71++;
			if(this.Yest72_responsetime!=null)
			child71++;
			if(child71>0 || att71>0){
				xmlTxt+="\n<cbat:YesT72";
			if(child71==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest72_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest72_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest72_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest72_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT72>";
			}
			}

			var child72=0;
			var att72=0;
			if(this.Yest73_accuracy!=null)
			child72++;
			if(this.Yest73_responsetime!=null)
			child72++;
			if(child72>0 || att72>0){
				xmlTxt+="\n<cbat:YesT73";
			if(child72==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest73_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest73_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest73_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest73_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT73>";
			}
			}

			var child73=0;
			var att73=0;
			if(this.Yest74_responsetime!=null)
			child73++;
			if(this.Yest74_accuracy!=null)
			child73++;
			if(child73>0 || att73>0){
				xmlTxt+="\n<cbat:YesT74";
			if(child73==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest74_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest74_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest74_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest74_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT74>";
			}
			}

			var child74=0;
			var att74=0;
			if(this.Yest75_responsetime!=null)
			child74++;
			if(this.Yest75_accuracy!=null)
			child74++;
			if(child74>0 || att74>0){
				xmlTxt+="\n<cbat:YesT75";
			if(child74==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest75_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest75_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest75_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest75_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT75>";
			}
			}

			var child75=0;
			var att75=0;
			if(this.Yest76_responsetime!=null)
			child75++;
			if(this.Yest76_accuracy!=null)
			child75++;
			if(child75>0 || att75>0){
				xmlTxt+="\n<cbat:YesT76";
			if(child75==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest76_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest76_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest76_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest76_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT76>";
			}
			}

			var child76=0;
			var att76=0;
			if(this.Yest77_responsetime!=null)
			child76++;
			if(this.Yest77_accuracy!=null)
			child76++;
			if(child76>0 || att76>0){
				xmlTxt+="\n<cbat:YesT77";
			if(child76==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest77_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest77_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest77_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest77_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT77>";
			}
			}

			var child77=0;
			var att77=0;
			if(this.Yest78_responsetime!=null)
			child77++;
			if(this.Yest78_accuracy!=null)
			child77++;
			if(child77>0 || att77>0){
				xmlTxt+="\n<cbat:YesT78";
			if(child77==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest78_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest78_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest78_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest78_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT78>";
			}
			}

			var child78=0;
			var att78=0;
			if(this.Yest79_responsetime!=null)
			child78++;
			if(this.Yest79_accuracy!=null)
			child78++;
			if(child78>0 || att78>0){
				xmlTxt+="\n<cbat:YesT79";
			if(child78==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest79_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest79_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest79_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest79_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT79>";
			}
			}

			var child79=0;
			var att79=0;
			if(this.Yest80_responsetime!=null)
			child79++;
			if(this.Yest80_accuracy!=null)
			child79++;
			if(child79>0 || att79>0){
				xmlTxt+="\n<cbat:YesT80";
			if(child79==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Yest80_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Yest80_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Yest80_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Yest80_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:YesT80>";
			}
			}

			var child80=0;
			var att80=0;
			if(this.Not1_responsetime!=null)
			child80++;
			if(this.Not1_accuracy!=null)
			child80++;
			if(child80>0 || att80>0){
				xmlTxt+="\n<cbat:NoT1";
			if(child80==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not1_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not1_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not1_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not1_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT1>";
			}
			}

			var child81=0;
			var att81=0;
			if(this.Not2_responsetime!=null)
			child81++;
			if(this.Not2_accuracy!=null)
			child81++;
			if(child81>0 || att81>0){
				xmlTxt+="\n<cbat:NoT2";
			if(child81==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not2_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not2_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not2_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not2_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT2>";
			}
			}

			var child82=0;
			var att82=0;
			if(this.Not3_responsetime!=null)
			child82++;
			if(this.Not3_accuracy!=null)
			child82++;
			if(child82>0 || att82>0){
				xmlTxt+="\n<cbat:NoT3";
			if(child82==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not3_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not3_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not3_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not3_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT3>";
			}
			}

			var child83=0;
			var att83=0;
			if(this.Not4_accuracy!=null)
			child83++;
			if(this.Not4_responsetime!=null)
			child83++;
			if(child83>0 || att83>0){
				xmlTxt+="\n<cbat:NoT4";
			if(child83==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not4_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not4_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not4_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not4_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT4>";
			}
			}

			var child84=0;
			var att84=0;
			if(this.Not5_accuracy!=null)
			child84++;
			if(this.Not5_responsetime!=null)
			child84++;
			if(child84>0 || att84>0){
				xmlTxt+="\n<cbat:NoT5";
			if(child84==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not5_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not5_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not5_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not5_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT5>";
			}
			}

			var child85=0;
			var att85=0;
			if(this.Not6_accuracy!=null)
			child85++;
			if(this.Not6_responsetime!=null)
			child85++;
			if(child85>0 || att85>0){
				xmlTxt+="\n<cbat:NoT6";
			if(child85==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not6_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not6_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not6_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not6_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT6>";
			}
			}

			var child86=0;
			var att86=0;
			if(this.Not7_accuracy!=null)
			child86++;
			if(this.Not7_responsetime!=null)
			child86++;
			if(child86>0 || att86>0){
				xmlTxt+="\n<cbat:NoT7";
			if(child86==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not7_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not7_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not7_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not7_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT7>";
			}
			}

			var child87=0;
			var att87=0;
			if(this.Not8_accuracy!=null)
			child87++;
			if(this.Not8_responsetime!=null)
			child87++;
			if(child87>0 || att87>0){
				xmlTxt+="\n<cbat:NoT8";
			if(child87==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not8_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not8_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not8_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not8_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT8>";
			}
			}

			var child88=0;
			var att88=0;
			if(this.Not9_accuracy!=null)
			child88++;
			if(this.Not9_responsetime!=null)
			child88++;
			if(child88>0 || att88>0){
				xmlTxt+="\n<cbat:NoT9";
			if(child88==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not9_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not9_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not9_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not9_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT9>";
			}
			}

			var child89=0;
			var att89=0;
			if(this.Not10_accuracy!=null)
			child89++;
			if(this.Not10_responsetime!=null)
			child89++;
			if(child89>0 || att89>0){
				xmlTxt+="\n<cbat:NoT10";
			if(child89==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not10_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not10_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not10_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not10_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT10>";
			}
			}

			var child90=0;
			var att90=0;
			if(this.Not11_responsetime!=null)
			child90++;
			if(this.Not11_accuracy!=null)
			child90++;
			if(child90>0 || att90>0){
				xmlTxt+="\n<cbat:NoT11";
			if(child90==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not11_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not11_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not11_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not11_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT11>";
			}
			}

			var child91=0;
			var att91=0;
			if(this.Not12_accuracy!=null)
			child91++;
			if(this.Not12_responsetime!=null)
			child91++;
			if(child91>0 || att91>0){
				xmlTxt+="\n<cbat:NoT12";
			if(child91==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not12_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not12_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not12_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not12_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT12>";
			}
			}

			var child92=0;
			var att92=0;
			if(this.Not13_responsetime!=null)
			child92++;
			if(this.Not13_accuracy!=null)
			child92++;
			if(child92>0 || att92>0){
				xmlTxt+="\n<cbat:NoT13";
			if(child92==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not13_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not13_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not13_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not13_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT13>";
			}
			}

			var child93=0;
			var att93=0;
			if(this.Not14_responsetime!=null)
			child93++;
			if(this.Not14_accuracy!=null)
			child93++;
			if(child93>0 || att93>0){
				xmlTxt+="\n<cbat:NoT14";
			if(child93==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not14_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not14_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not14_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not14_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT14>";
			}
			}

			var child94=0;
			var att94=0;
			if(this.Not15_responsetime!=null)
			child94++;
			if(this.Not15_accuracy!=null)
			child94++;
			if(child94>0 || att94>0){
				xmlTxt+="\n<cbat:NoT15";
			if(child94==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not15_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not15_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not15_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not15_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT15>";
			}
			}

			var child95=0;
			var att95=0;
			if(this.Not16_responsetime!=null)
			child95++;
			if(this.Not16_accuracy!=null)
			child95++;
			if(child95>0 || att95>0){
				xmlTxt+="\n<cbat:NoT16";
			if(child95==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not16_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not16_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not16_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not16_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT16>";
			}
			}

			var child96=0;
			var att96=0;
			if(this.Not17_responsetime!=null)
			child96++;
			if(this.Not17_accuracy!=null)
			child96++;
			if(child96>0 || att96>0){
				xmlTxt+="\n<cbat:NoT17";
			if(child96==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not17_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not17_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not17_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not17_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT17>";
			}
			}

			var child97=0;
			var att97=0;
			if(this.Not18_responsetime!=null)
			child97++;
			if(this.Not18_accuracy!=null)
			child97++;
			if(child97>0 || att97>0){
				xmlTxt+="\n<cbat:NoT18";
			if(child97==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not18_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not18_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not18_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not18_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT18>";
			}
			}

			var child98=0;
			var att98=0;
			if(this.Not19_accuracy!=null)
			child98++;
			if(this.Not19_responsetime!=null)
			child98++;
			if(child98>0 || att98>0){
				xmlTxt+="\n<cbat:NoT19";
			if(child98==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not19_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not19_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not19_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not19_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT19>";
			}
			}

			var child99=0;
			var att99=0;
			if(this.Not20_responsetime!=null)
			child99++;
			if(this.Not20_accuracy!=null)
			child99++;
			if(child99>0 || att99>0){
				xmlTxt+="\n<cbat:NoT20";
			if(child99==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not20_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not20_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not20_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not20_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT20>";
			}
			}

			var child100=0;
			var att100=0;
			if(this.Not21_responsetime!=null)
			child100++;
			if(this.Not21_accuracy!=null)
			child100++;
			if(child100>0 || att100>0){
				xmlTxt+="\n<cbat:NoT21";
			if(child100==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not21_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not21_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not21_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not21_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT21>";
			}
			}

			var child101=0;
			var att101=0;
			if(this.Not22_responsetime!=null)
			child101++;
			if(this.Not22_accuracy!=null)
			child101++;
			if(child101>0 || att101>0){
				xmlTxt+="\n<cbat:NoT22";
			if(child101==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not22_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not22_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not22_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not22_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT22>";
			}
			}

			var child102=0;
			var att102=0;
			if(this.Not23_accuracy!=null)
			child102++;
			if(this.Not23_responsetime!=null)
			child102++;
			if(child102>0 || att102>0){
				xmlTxt+="\n<cbat:NoT23";
			if(child102==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not23_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not23_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not23_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not23_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT23>";
			}
			}

			var child103=0;
			var att103=0;
			if(this.Not24_accuracy!=null)
			child103++;
			if(this.Not24_responsetime!=null)
			child103++;
			if(child103>0 || att103>0){
				xmlTxt+="\n<cbat:NoT24";
			if(child103==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not24_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not24_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not24_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not24_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT24>";
			}
			}

			var child104=0;
			var att104=0;
			if(this.Not25_accuracy!=null)
			child104++;
			if(this.Not25_responsetime!=null)
			child104++;
			if(child104>0 || att104>0){
				xmlTxt+="\n<cbat:NoT25";
			if(child104==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not25_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not25_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not25_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not25_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT25>";
			}
			}

			var child105=0;
			var att105=0;
			if(this.Not26_responsetime!=null)
			child105++;
			if(this.Not26_accuracy!=null)
			child105++;
			if(child105>0 || att105>0){
				xmlTxt+="\n<cbat:NoT26";
			if(child105==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not26_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not26_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not26_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not26_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT26>";
			}
			}

			var child106=0;
			var att106=0;
			if(this.Not27_responsetime!=null)
			child106++;
			if(this.Not27_accuracy!=null)
			child106++;
			if(child106>0 || att106>0){
				xmlTxt+="\n<cbat:NoT27";
			if(child106==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not27_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not27_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not27_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not27_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT27>";
			}
			}

			var child107=0;
			var att107=0;
			if(this.Not28_accuracy!=null)
			child107++;
			if(this.Not28_responsetime!=null)
			child107++;
			if(child107>0 || att107>0){
				xmlTxt+="\n<cbat:NoT28";
			if(child107==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not28_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not28_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not28_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not28_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT28>";
			}
			}

			var child108=0;
			var att108=0;
			if(this.Not29_accuracy!=null)
			child108++;
			if(this.Not29_responsetime!=null)
			child108++;
			if(child108>0 || att108>0){
				xmlTxt+="\n<cbat:NoT29";
			if(child108==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not29_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not29_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not29_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not29_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT29>";
			}
			}

			var child109=0;
			var att109=0;
			if(this.Not30_responsetime!=null)
			child109++;
			if(this.Not30_accuracy!=null)
			child109++;
			if(child109>0 || att109>0){
				xmlTxt+="\n<cbat:NoT30";
			if(child109==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not30_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not30_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not30_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not30_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT30>";
			}
			}

			var child110=0;
			var att110=0;
			if(this.Not31_responsetime!=null)
			child110++;
			if(this.Not31_accuracy!=null)
			child110++;
			if(child110>0 || att110>0){
				xmlTxt+="\n<cbat:NoT31";
			if(child110==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not31_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not31_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not31_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not31_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT31>";
			}
			}

			var child111=0;
			var att111=0;
			if(this.Not32_responsetime!=null)
			child111++;
			if(this.Not32_accuracy!=null)
			child111++;
			if(child111>0 || att111>0){
				xmlTxt+="\n<cbat:NoT32";
			if(child111==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not32_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not32_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not32_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not32_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT32>";
			}
			}

			var child112=0;
			var att112=0;
			if(this.Not33_accuracy!=null)
			child112++;
			if(this.Not33_responsetime!=null)
			child112++;
			if(child112>0 || att112>0){
				xmlTxt+="\n<cbat:NoT33";
			if(child112==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not33_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not33_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not33_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not33_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT33>";
			}
			}

			var child113=0;
			var att113=0;
			if(this.Not34_accuracy!=null)
			child113++;
			if(this.Not34_responsetime!=null)
			child113++;
			if(child113>0 || att113>0){
				xmlTxt+="\n<cbat:NoT34";
			if(child113==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not34_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not34_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not34_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not34_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT34>";
			}
			}

			var child114=0;
			var att114=0;
			if(this.Not35_accuracy!=null)
			child114++;
			if(this.Not35_responsetime!=null)
			child114++;
			if(child114>0 || att114>0){
				xmlTxt+="\n<cbat:NoT35";
			if(child114==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not35_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not35_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not35_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not35_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT35>";
			}
			}

			var child115=0;
			var att115=0;
			if(this.Not36_accuracy!=null)
			child115++;
			if(this.Not36_responsetime!=null)
			child115++;
			if(child115>0 || att115>0){
				xmlTxt+="\n<cbat:NoT36";
			if(child115==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not36_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not36_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not36_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not36_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT36>";
			}
			}

			var child116=0;
			var att116=0;
			if(this.Not37_accuracy!=null)
			child116++;
			if(this.Not37_responsetime!=null)
			child116++;
			if(child116>0 || att116>0){
				xmlTxt+="\n<cbat:NoT37";
			if(child116==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not37_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not37_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not37_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not37_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT37>";
			}
			}

			var child117=0;
			var att117=0;
			if(this.Not38_accuracy!=null)
			child117++;
			if(this.Not38_responsetime!=null)
			child117++;
			if(child117>0 || att117>0){
				xmlTxt+="\n<cbat:NoT38";
			if(child117==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not38_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not38_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not38_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not38_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT38>";
			}
			}

			var child118=0;
			var att118=0;
			if(this.Not39_responsetime!=null)
			child118++;
			if(this.Not39_accuracy!=null)
			child118++;
			if(child118>0 || att118>0){
				xmlTxt+="\n<cbat:NoT39";
			if(child118==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not39_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not39_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not39_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not39_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT39>";
			}
			}

			var child119=0;
			var att119=0;
			if(this.Not40_accuracy!=null)
			child119++;
			if(this.Not40_responsetime!=null)
			child119++;
			if(child119>0 || att119>0){
				xmlTxt+="\n<cbat:NoT40";
			if(child119==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not40_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not40_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not40_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not40_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT40>";
			}
			}

			var child120=0;
			var att120=0;
			if(this.Not41_accuracy!=null)
			child120++;
			if(this.Not41_responsetime!=null)
			child120++;
			if(child120>0 || att120>0){
				xmlTxt+="\n<cbat:NoT41";
			if(child120==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not41_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not41_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not41_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not41_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT41>";
			}
			}

			var child121=0;
			var att121=0;
			if(this.Not42_accuracy!=null)
			child121++;
			if(this.Not42_responsetime!=null)
			child121++;
			if(child121>0 || att121>0){
				xmlTxt+="\n<cbat:NoT42";
			if(child121==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not42_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not42_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not42_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not42_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT42>";
			}
			}

			var child122=0;
			var att122=0;
			if(this.Not43_responsetime!=null)
			child122++;
			if(this.Not43_accuracy!=null)
			child122++;
			if(child122>0 || att122>0){
				xmlTxt+="\n<cbat:NoT43";
			if(child122==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not43_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not43_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not43_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not43_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT43>";
			}
			}

			var child123=0;
			var att123=0;
			if(this.Not44_responsetime!=null)
			child123++;
			if(this.Not44_accuracy!=null)
			child123++;
			if(child123>0 || att123>0){
				xmlTxt+="\n<cbat:NoT44";
			if(child123==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not44_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not44_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not44_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not44_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT44>";
			}
			}

			var child124=0;
			var att124=0;
			if(this.Not45_responsetime!=null)
			child124++;
			if(this.Not45_accuracy!=null)
			child124++;
			if(child124>0 || att124>0){
				xmlTxt+="\n<cbat:NoT45";
			if(child124==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not45_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not45_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not45_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not45_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT45>";
			}
			}

			var child125=0;
			var att125=0;
			if(this.Not46_responsetime!=null)
			child125++;
			if(this.Not46_accuracy!=null)
			child125++;
			if(child125>0 || att125>0){
				xmlTxt+="\n<cbat:NoT46";
			if(child125==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not46_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not46_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not46_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not46_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT46>";
			}
			}

			var child126=0;
			var att126=0;
			if(this.Not47_responsetime!=null)
			child126++;
			if(this.Not47_accuracy!=null)
			child126++;
			if(child126>0 || att126>0){
				xmlTxt+="\n<cbat:NoT47";
			if(child126==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not47_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not47_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not47_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not47_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT47>";
			}
			}

			var child127=0;
			var att127=0;
			if(this.Not48_responsetime!=null)
			child127++;
			if(this.Not48_accuracy!=null)
			child127++;
			if(child127>0 || att127>0){
				xmlTxt+="\n<cbat:NoT48";
			if(child127==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not48_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not48_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not48_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not48_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT48>";
			}
			}

			var child128=0;
			var att128=0;
			if(this.Not49_accuracy!=null)
			child128++;
			if(this.Not49_responsetime!=null)
			child128++;
			if(child128>0 || att128>0){
				xmlTxt+="\n<cbat:NoT49";
			if(child128==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not49_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not49_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not49_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not49_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT49>";
			}
			}

			var child129=0;
			var att129=0;
			if(this.Not50_responsetime!=null)
			child129++;
			if(this.Not50_accuracy!=null)
			child129++;
			if(child129>0 || att129>0){
				xmlTxt+="\n<cbat:NoT50";
			if(child129==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not50_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not50_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not50_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not50_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT50>";
			}
			}

			var child130=0;
			var att130=0;
			if(this.Not51_responsetime!=null)
			child130++;
			if(this.Not51_accuracy!=null)
			child130++;
			if(child130>0 || att130>0){
				xmlTxt+="\n<cbat:NoT51";
			if(child130==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not51_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not51_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not51_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not51_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT51>";
			}
			}

			var child131=0;
			var att131=0;
			if(this.Not52_responsetime!=null)
			child131++;
			if(this.Not52_accuracy!=null)
			child131++;
			if(child131>0 || att131>0){
				xmlTxt+="\n<cbat:NoT52";
			if(child131==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not52_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not52_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not52_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not52_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT52>";
			}
			}

			var child132=0;
			var att132=0;
			if(this.Not53_responsetime!=null)
			child132++;
			if(this.Not53_accuracy!=null)
			child132++;
			if(child132>0 || att132>0){
				xmlTxt+="\n<cbat:NoT53";
			if(child132==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not53_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not53_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not53_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not53_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT53>";
			}
			}

			var child133=0;
			var att133=0;
			if(this.Not54_accuracy!=null)
			child133++;
			if(this.Not54_responsetime!=null)
			child133++;
			if(child133>0 || att133>0){
				xmlTxt+="\n<cbat:NoT54";
			if(child133==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not54_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not54_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not54_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not54_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT54>";
			}
			}

			var child134=0;
			var att134=0;
			if(this.Not55_accuracy!=null)
			child134++;
			if(this.Not55_responsetime!=null)
			child134++;
			if(child134>0 || att134>0){
				xmlTxt+="\n<cbat:NoT55";
			if(child134==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not55_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not55_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not55_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not55_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT55>";
			}
			}

			var child135=0;
			var att135=0;
			if(this.Not56_responsetime!=null)
			child135++;
			if(this.Not56_accuracy!=null)
			child135++;
			if(child135>0 || att135>0){
				xmlTxt+="\n<cbat:NoT56";
			if(child135==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not56_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not56_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not56_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not56_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT56>";
			}
			}

			var child136=0;
			var att136=0;
			if(this.Not57_responsetime!=null)
			child136++;
			if(this.Not57_accuracy!=null)
			child136++;
			if(child136>0 || att136>0){
				xmlTxt+="\n<cbat:NoT57";
			if(child136==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not57_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not57_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not57_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not57_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT57>";
			}
			}

			var child137=0;
			var att137=0;
			if(this.Not58_accuracy!=null)
			child137++;
			if(this.Not58_responsetime!=null)
			child137++;
			if(child137>0 || att137>0){
				xmlTxt+="\n<cbat:NoT58";
			if(child137==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not58_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not58_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not58_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not58_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT58>";
			}
			}

			var child138=0;
			var att138=0;
			if(this.Not59_responsetime!=null)
			child138++;
			if(this.Not59_accuracy!=null)
			child138++;
			if(child138>0 || att138>0){
				xmlTxt+="\n<cbat:NoT59";
			if(child138==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not59_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not59_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not59_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not59_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT59>";
			}
			}

			var child139=0;
			var att139=0;
			if(this.Not60_accuracy!=null)
			child139++;
			if(this.Not60_responsetime!=null)
			child139++;
			if(child139>0 || att139>0){
				xmlTxt+="\n<cbat:NoT60";
			if(child139==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not60_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not60_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not60_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not60_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT60>";
			}
			}

			var child140=0;
			var att140=0;
			if(this.Not61_accuracy!=null)
			child140++;
			if(this.Not61_responsetime!=null)
			child140++;
			if(child140>0 || att140>0){
				xmlTxt+="\n<cbat:NoT61";
			if(child140==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not61_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not61_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not61_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not61_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT61>";
			}
			}

			var child141=0;
			var att141=0;
			if(this.Not62_responsetime!=null)
			child141++;
			if(this.Not62_accuracy!=null)
			child141++;
			if(child141>0 || att141>0){
				xmlTxt+="\n<cbat:NoT62";
			if(child141==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not62_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not62_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not62_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not62_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT62>";
			}
			}

			var child142=0;
			var att142=0;
			if(this.Not63_responsetime!=null)
			child142++;
			if(this.Not63_accuracy!=null)
			child142++;
			if(child142>0 || att142>0){
				xmlTxt+="\n<cbat:NoT63";
			if(child142==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not63_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not63_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not63_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not63_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT63>";
			}
			}

			var child143=0;
			var att143=0;
			if(this.Not64_responsetime!=null)
			child143++;
			if(this.Not64_accuracy!=null)
			child143++;
			if(child143>0 || att143>0){
				xmlTxt+="\n<cbat:NoT64";
			if(child143==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not64_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not64_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not64_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not64_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT64>";
			}
			}

			var child144=0;
			var att144=0;
			if(this.Not65_accuracy!=null)
			child144++;
			if(this.Not65_responsetime!=null)
			child144++;
			if(child144>0 || att144>0){
				xmlTxt+="\n<cbat:NoT65";
			if(child144==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not65_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not65_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not65_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not65_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT65>";
			}
			}

			var child145=0;
			var att145=0;
			if(this.Not66_accuracy!=null)
			child145++;
			if(this.Not66_responsetime!=null)
			child145++;
			if(child145>0 || att145>0){
				xmlTxt+="\n<cbat:NoT66";
			if(child145==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not66_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not66_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not66_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not66_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT66>";
			}
			}

			var child146=0;
			var att146=0;
			if(this.Not67_accuracy!=null)
			child146++;
			if(this.Not67_responsetime!=null)
			child146++;
			if(child146>0 || att146>0){
				xmlTxt+="\n<cbat:NoT67";
			if(child146==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not67_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not67_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not67_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not67_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT67>";
			}
			}

			var child147=0;
			var att147=0;
			if(this.Not68_accuracy!=null)
			child147++;
			if(this.Not68_responsetime!=null)
			child147++;
			if(child147>0 || att147>0){
				xmlTxt+="\n<cbat:NoT68";
			if(child147==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not68_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not68_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not68_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not68_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT68>";
			}
			}

			var child148=0;
			var att148=0;
			if(this.Not69_responsetime!=null)
			child148++;
			if(this.Not69_accuracy!=null)
			child148++;
			if(child148>0 || att148>0){
				xmlTxt+="\n<cbat:NoT69";
			if(child148==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not69_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not69_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not69_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not69_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT69>";
			}
			}

			var child149=0;
			var att149=0;
			if(this.Not70_accuracy!=null)
			child149++;
			if(this.Not70_responsetime!=null)
			child149++;
			if(child149>0 || att149>0){
				xmlTxt+="\n<cbat:NoT70";
			if(child149==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not70_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not70_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not70_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not70_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT70>";
			}
			}

			var child150=0;
			var att150=0;
			if(this.Not71_responsetime!=null)
			child150++;
			if(this.Not71_accuracy!=null)
			child150++;
			if(child150>0 || att150>0){
				xmlTxt+="\n<cbat:NoT71";
			if(child150==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not71_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not71_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not71_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not71_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT71>";
			}
			}

			var child151=0;
			var att151=0;
			if(this.Not72_accuracy!=null)
			child151++;
			if(this.Not72_responsetime!=null)
			child151++;
			if(child151>0 || att151>0){
				xmlTxt+="\n<cbat:NoT72";
			if(child151==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not72_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not72_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not72_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not72_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT72>";
			}
			}

			var child152=0;
			var att152=0;
			if(this.Not73_accuracy!=null)
			child152++;
			if(this.Not73_responsetime!=null)
			child152++;
			if(child152>0 || att152>0){
				xmlTxt+="\n<cbat:NoT73";
			if(child152==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not73_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not73_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not73_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not73_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT73>";
			}
			}

			var child153=0;
			var att153=0;
			if(this.Not74_accuracy!=null)
			child153++;
			if(this.Not74_responsetime!=null)
			child153++;
			if(child153>0 || att153>0){
				xmlTxt+="\n<cbat:NoT74";
			if(child153==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not74_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not74_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not74_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not74_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT74>";
			}
			}

			var child154=0;
			var att154=0;
			if(this.Not75_responsetime!=null)
			child154++;
			if(this.Not75_accuracy!=null)
			child154++;
			if(child154>0 || att154>0){
				xmlTxt+="\n<cbat:NoT75";
			if(child154==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not75_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not75_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not75_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not75_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT75>";
			}
			}

			var child155=0;
			var att155=0;
			if(this.Not76_responsetime!=null)
			child155++;
			if(this.Not76_accuracy!=null)
			child155++;
			if(child155>0 || att155>0){
				xmlTxt+="\n<cbat:NoT76";
			if(child155==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not76_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not76_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not76_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not76_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT76>";
			}
			}

			var child156=0;
			var att156=0;
			if(this.Not77_responsetime!=null)
			child156++;
			if(this.Not77_accuracy!=null)
			child156++;
			if(child156>0 || att156>0){
				xmlTxt+="\n<cbat:NoT77";
			if(child156==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not77_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not77_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not77_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not77_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT77>";
			}
			}

			var child157=0;
			var att157=0;
			if(this.Not78_responsetime!=null)
			child157++;
			if(this.Not78_accuracy!=null)
			child157++;
			if(child157>0 || att157>0){
				xmlTxt+="\n<cbat:NoT78";
			if(child157==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not78_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not78_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not78_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not78_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT78>";
			}
			}

			var child158=0;
			var att158=0;
			if(this.Not79_responsetime!=null)
			child158++;
			if(this.Not79_accuracy!=null)
			child158++;
			if(child158>0 || att158>0){
				xmlTxt+="\n<cbat:NoT79";
			if(child158==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not79_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not79_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not79_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not79_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT79>";
			}
			}

			var child159=0;
			var att159=0;
			if(this.Not80_responsetime!=null)
			child159++;
			if(this.Not80_accuracy!=null)
			child159++;
			if(child159>0 || att159>0){
				xmlTxt+="\n<cbat:NoT80";
			if(child159==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Not80_accuracy!=null){
			xmlTxt+="\n<cbat:accuracy";
			xmlTxt+=">";
			xmlTxt+=this.Not80_accuracy;
			xmlTxt+="</cbat:accuracy>";
		}
		if (this.Not80_responsetime!=null){
			xmlTxt+="\n<cbat:responseTime";
			xmlTxt+=">";
			xmlTxt+=this.Not80_responsetime;
			xmlTxt+="</cbat:responseTime>";
		}
				xmlTxt+="\n</cbat:NoT80>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
			if(this.Yest1_responsetime!=null) return true;
			if(this.Yest1_accuracy!=null) return true;
			if(this.Yest2_responsetime!=null) return true;
			if(this.Yest2_accuracy!=null) return true;
			if(this.Yest3_responsetime!=null) return true;
			if(this.Yest3_accuracy!=null) return true;
			if(this.Yest4_responsetime!=null) return true;
			if(this.Yest4_accuracy!=null) return true;
			if(this.Yest5_responsetime!=null) return true;
			if(this.Yest5_accuracy!=null) return true;
			if(this.Yest6_responsetime!=null) return true;
			if(this.Yest6_accuracy!=null) return true;
			if(this.Yest7_responsetime!=null) return true;
			if(this.Yest7_accuracy!=null) return true;
			if(this.Yest8_accuracy!=null) return true;
			if(this.Yest8_responsetime!=null) return true;
			if(this.Yest9_accuracy!=null) return true;
			if(this.Yest9_responsetime!=null) return true;
			if(this.Yest10_accuracy!=null) return true;
			if(this.Yest10_responsetime!=null) return true;
			if(this.Yest11_accuracy!=null) return true;
			if(this.Yest11_responsetime!=null) return true;
			if(this.Yest12_responsetime!=null) return true;
			if(this.Yest12_accuracy!=null) return true;
			if(this.Yest13_responsetime!=null) return true;
			if(this.Yest13_accuracy!=null) return true;
			if(this.Yest14_responsetime!=null) return true;
			if(this.Yest14_accuracy!=null) return true;
			if(this.Yest15_responsetime!=null) return true;
			if(this.Yest15_accuracy!=null) return true;
			if(this.Yest16_accuracy!=null) return true;
			if(this.Yest16_responsetime!=null) return true;
			if(this.Yest17_responsetime!=null) return true;
			if(this.Yest17_accuracy!=null) return true;
			if(this.Yest18_accuracy!=null) return true;
			if(this.Yest18_responsetime!=null) return true;
			if(this.Yest19_accuracy!=null) return true;
			if(this.Yest19_responsetime!=null) return true;
			if(this.Yest20_responsetime!=null) return true;
			if(this.Yest20_accuracy!=null) return true;
			if(this.Yest21_responsetime!=null) return true;
			if(this.Yest21_accuracy!=null) return true;
			if(this.Yest22_responsetime!=null) return true;
			if(this.Yest22_accuracy!=null) return true;
			if(this.Yest23_accuracy!=null) return true;
			if(this.Yest23_responsetime!=null) return true;
			if(this.Yest24_accuracy!=null) return true;
			if(this.Yest24_responsetime!=null) return true;
			if(this.Yest25_responsetime!=null) return true;
			if(this.Yest25_accuracy!=null) return true;
			if(this.Yest26_responsetime!=null) return true;
			if(this.Yest26_accuracy!=null) return true;
			if(this.Yest27_responsetime!=null) return true;
			if(this.Yest27_accuracy!=null) return true;
			if(this.Yest28_responsetime!=null) return true;
			if(this.Yest28_accuracy!=null) return true;
			if(this.Yest29_responsetime!=null) return true;
			if(this.Yest29_accuracy!=null) return true;
			if(this.Yest30_accuracy!=null) return true;
			if(this.Yest30_responsetime!=null) return true;
			if(this.Yest31_accuracy!=null) return true;
			if(this.Yest31_responsetime!=null) return true;
			if(this.Yest32_accuracy!=null) return true;
			if(this.Yest32_responsetime!=null) return true;
			if(this.Yest33_accuracy!=null) return true;
			if(this.Yest33_responsetime!=null) return true;
			if(this.Yest34_accuracy!=null) return true;
			if(this.Yest34_responsetime!=null) return true;
			if(this.Yest35_accuracy!=null) return true;
			if(this.Yest35_responsetime!=null) return true;
			if(this.Yest36_accuracy!=null) return true;
			if(this.Yest36_responsetime!=null) return true;
			if(this.Yest37_accuracy!=null) return true;
			if(this.Yest37_responsetime!=null) return true;
			if(this.Yest38_responsetime!=null) return true;
			if(this.Yest38_accuracy!=null) return true;
			if(this.Yest39_responsetime!=null) return true;
			if(this.Yest39_accuracy!=null) return true;
			if(this.Yest40_accuracy!=null) return true;
			if(this.Yest40_responsetime!=null) return true;
			if(this.Yest41_accuracy!=null) return true;
			if(this.Yest41_responsetime!=null) return true;
			if(this.Yest42_accuracy!=null) return true;
			if(this.Yest42_responsetime!=null) return true;
			if(this.Yest43_accuracy!=null) return true;
			if(this.Yest43_responsetime!=null) return true;
			if(this.Yest44_responsetime!=null) return true;
			if(this.Yest44_accuracy!=null) return true;
			if(this.Yest45_responsetime!=null) return true;
			if(this.Yest45_accuracy!=null) return true;
			if(this.Yest46_responsetime!=null) return true;
			if(this.Yest46_accuracy!=null) return true;
			if(this.Yest47_responsetime!=null) return true;
			if(this.Yest47_accuracy!=null) return true;
			if(this.Yest48_accuracy!=null) return true;
			if(this.Yest48_responsetime!=null) return true;
			if(this.Yest49_accuracy!=null) return true;
			if(this.Yest49_responsetime!=null) return true;
			if(this.Yest50_responsetime!=null) return true;
			if(this.Yest50_accuracy!=null) return true;
			if(this.Yest51_responsetime!=null) return true;
			if(this.Yest51_accuracy!=null) return true;
			if(this.Yest52_responsetime!=null) return true;
			if(this.Yest52_accuracy!=null) return true;
			if(this.Yest53_responsetime!=null) return true;
			if(this.Yest53_accuracy!=null) return true;
			if(this.Yest54_responsetime!=null) return true;
			if(this.Yest54_accuracy!=null) return true;
			if(this.Yest55_accuracy!=null) return true;
			if(this.Yest55_responsetime!=null) return true;
			if(this.Yest56_accuracy!=null) return true;
			if(this.Yest56_responsetime!=null) return true;
			if(this.Yest57_responsetime!=null) return true;
			if(this.Yest57_accuracy!=null) return true;
			if(this.Yest58_responsetime!=null) return true;
			if(this.Yest58_accuracy!=null) return true;
			if(this.Yest59_responsetime!=null) return true;
			if(this.Yest59_accuracy!=null) return true;
			if(this.Yest60_accuracy!=null) return true;
			if(this.Yest60_responsetime!=null) return true;
			if(this.Yest61_responsetime!=null) return true;
			if(this.Yest61_accuracy!=null) return true;
			if(this.Yest62_accuracy!=null) return true;
			if(this.Yest62_responsetime!=null) return true;
			if(this.Yest63_accuracy!=null) return true;
			if(this.Yest63_responsetime!=null) return true;
			if(this.Yest64_accuracy!=null) return true;
			if(this.Yest64_responsetime!=null) return true;
			if(this.Yest65_accuracy!=null) return true;
			if(this.Yest65_responsetime!=null) return true;
			if(this.Yest66_accuracy!=null) return true;
			if(this.Yest66_responsetime!=null) return true;
			if(this.Yest67_accuracy!=null) return true;
			if(this.Yest67_responsetime!=null) return true;
			if(this.Yest68_accuracy!=null) return true;
			if(this.Yest68_responsetime!=null) return true;
			if(this.Yest69_accuracy!=null) return true;
			if(this.Yest69_responsetime!=null) return true;
			if(this.Yest70_accuracy!=null) return true;
			if(this.Yest70_responsetime!=null) return true;
			if(this.Yest71_accuracy!=null) return true;
			if(this.Yest71_responsetime!=null) return true;
			if(this.Yest72_accuracy!=null) return true;
			if(this.Yest72_responsetime!=null) return true;
			if(this.Yest73_accuracy!=null) return true;
			if(this.Yest73_responsetime!=null) return true;
			if(this.Yest74_responsetime!=null) return true;
			if(this.Yest74_accuracy!=null) return true;
			if(this.Yest75_responsetime!=null) return true;
			if(this.Yest75_accuracy!=null) return true;
			if(this.Yest76_responsetime!=null) return true;
			if(this.Yest76_accuracy!=null) return true;
			if(this.Yest77_responsetime!=null) return true;
			if(this.Yest77_accuracy!=null) return true;
			if(this.Yest78_responsetime!=null) return true;
			if(this.Yest78_accuracy!=null) return true;
			if(this.Yest79_responsetime!=null) return true;
			if(this.Yest79_accuracy!=null) return true;
			if(this.Yest80_responsetime!=null) return true;
			if(this.Yest80_accuracy!=null) return true;
			if(this.Not1_responsetime!=null) return true;
			if(this.Not1_accuracy!=null) return true;
			if(this.Not2_responsetime!=null) return true;
			if(this.Not2_accuracy!=null) return true;
			if(this.Not3_responsetime!=null) return true;
			if(this.Not3_accuracy!=null) return true;
			if(this.Not4_accuracy!=null) return true;
			if(this.Not4_responsetime!=null) return true;
			if(this.Not5_accuracy!=null) return true;
			if(this.Not5_responsetime!=null) return true;
			if(this.Not6_accuracy!=null) return true;
			if(this.Not6_responsetime!=null) return true;
			if(this.Not7_accuracy!=null) return true;
			if(this.Not7_responsetime!=null) return true;
			if(this.Not8_accuracy!=null) return true;
			if(this.Not8_responsetime!=null) return true;
			if(this.Not9_accuracy!=null) return true;
			if(this.Not9_responsetime!=null) return true;
			if(this.Not10_accuracy!=null) return true;
			if(this.Not10_responsetime!=null) return true;
			if(this.Not11_responsetime!=null) return true;
			if(this.Not11_accuracy!=null) return true;
			if(this.Not12_accuracy!=null) return true;
			if(this.Not12_responsetime!=null) return true;
			if(this.Not13_responsetime!=null) return true;
			if(this.Not13_accuracy!=null) return true;
			if(this.Not14_responsetime!=null) return true;
			if(this.Not14_accuracy!=null) return true;
			if(this.Not15_responsetime!=null) return true;
			if(this.Not15_accuracy!=null) return true;
			if(this.Not16_responsetime!=null) return true;
			if(this.Not16_accuracy!=null) return true;
			if(this.Not17_responsetime!=null) return true;
			if(this.Not17_accuracy!=null) return true;
			if(this.Not18_responsetime!=null) return true;
			if(this.Not18_accuracy!=null) return true;
			if(this.Not19_accuracy!=null) return true;
			if(this.Not19_responsetime!=null) return true;
			if(this.Not20_responsetime!=null) return true;
			if(this.Not20_accuracy!=null) return true;
			if(this.Not21_responsetime!=null) return true;
			if(this.Not21_accuracy!=null) return true;
			if(this.Not22_responsetime!=null) return true;
			if(this.Not22_accuracy!=null) return true;
			if(this.Not23_accuracy!=null) return true;
			if(this.Not23_responsetime!=null) return true;
			if(this.Not24_accuracy!=null) return true;
			if(this.Not24_responsetime!=null) return true;
			if(this.Not25_accuracy!=null) return true;
			if(this.Not25_responsetime!=null) return true;
			if(this.Not26_responsetime!=null) return true;
			if(this.Not26_accuracy!=null) return true;
			if(this.Not27_responsetime!=null) return true;
			if(this.Not27_accuracy!=null) return true;
			if(this.Not28_accuracy!=null) return true;
			if(this.Not28_responsetime!=null) return true;
			if(this.Not29_accuracy!=null) return true;
			if(this.Not29_responsetime!=null) return true;
			if(this.Not30_responsetime!=null) return true;
			if(this.Not30_accuracy!=null) return true;
			if(this.Not31_responsetime!=null) return true;
			if(this.Not31_accuracy!=null) return true;
			if(this.Not32_responsetime!=null) return true;
			if(this.Not32_accuracy!=null) return true;
			if(this.Not33_accuracy!=null) return true;
			if(this.Not33_responsetime!=null) return true;
			if(this.Not34_accuracy!=null) return true;
			if(this.Not34_responsetime!=null) return true;
			if(this.Not35_accuracy!=null) return true;
			if(this.Not35_responsetime!=null) return true;
			if(this.Not36_accuracy!=null) return true;
			if(this.Not36_responsetime!=null) return true;
			if(this.Not37_accuracy!=null) return true;
			if(this.Not37_responsetime!=null) return true;
			if(this.Not38_accuracy!=null) return true;
			if(this.Not38_responsetime!=null) return true;
			if(this.Not39_responsetime!=null) return true;
			if(this.Not39_accuracy!=null) return true;
			if(this.Not40_accuracy!=null) return true;
			if(this.Not40_responsetime!=null) return true;
			if(this.Not41_accuracy!=null) return true;
			if(this.Not41_responsetime!=null) return true;
			if(this.Not42_accuracy!=null) return true;
			if(this.Not42_responsetime!=null) return true;
			if(this.Not43_responsetime!=null) return true;
			if(this.Not43_accuracy!=null) return true;
			if(this.Not44_responsetime!=null) return true;
			if(this.Not44_accuracy!=null) return true;
			if(this.Not45_responsetime!=null) return true;
			if(this.Not45_accuracy!=null) return true;
			if(this.Not46_responsetime!=null) return true;
			if(this.Not46_accuracy!=null) return true;
			if(this.Not47_responsetime!=null) return true;
			if(this.Not47_accuracy!=null) return true;
			if(this.Not48_responsetime!=null) return true;
			if(this.Not48_accuracy!=null) return true;
			if(this.Not49_accuracy!=null) return true;
			if(this.Not49_responsetime!=null) return true;
			if(this.Not50_responsetime!=null) return true;
			if(this.Not50_accuracy!=null) return true;
			if(this.Not51_responsetime!=null) return true;
			if(this.Not51_accuracy!=null) return true;
			if(this.Not52_responsetime!=null) return true;
			if(this.Not52_accuracy!=null) return true;
			if(this.Not53_responsetime!=null) return true;
			if(this.Not53_accuracy!=null) return true;
			if(this.Not54_accuracy!=null) return true;
			if(this.Not54_responsetime!=null) return true;
			if(this.Not55_accuracy!=null) return true;
			if(this.Not55_responsetime!=null) return true;
			if(this.Not56_responsetime!=null) return true;
			if(this.Not56_accuracy!=null) return true;
			if(this.Not57_responsetime!=null) return true;
			if(this.Not57_accuracy!=null) return true;
			if(this.Not58_accuracy!=null) return true;
			if(this.Not58_responsetime!=null) return true;
			if(this.Not59_responsetime!=null) return true;
			if(this.Not59_accuracy!=null) return true;
			if(this.Not60_accuracy!=null) return true;
			if(this.Not60_responsetime!=null) return true;
			if(this.Not61_accuracy!=null) return true;
			if(this.Not61_responsetime!=null) return true;
			if(this.Not62_responsetime!=null) return true;
			if(this.Not62_accuracy!=null) return true;
			if(this.Not63_responsetime!=null) return true;
			if(this.Not63_accuracy!=null) return true;
			if(this.Not64_responsetime!=null) return true;
			if(this.Not64_accuracy!=null) return true;
			if(this.Not65_accuracy!=null) return true;
			if(this.Not65_responsetime!=null) return true;
			if(this.Not66_accuracy!=null) return true;
			if(this.Not66_responsetime!=null) return true;
			if(this.Not67_accuracy!=null) return true;
			if(this.Not67_responsetime!=null) return true;
			if(this.Not68_accuracy!=null) return true;
			if(this.Not68_responsetime!=null) return true;
			if(this.Not69_responsetime!=null) return true;
			if(this.Not69_accuracy!=null) return true;
			if(this.Not70_accuracy!=null) return true;
			if(this.Not70_responsetime!=null) return true;
			if(this.Not71_responsetime!=null) return true;
			if(this.Not71_accuracy!=null) return true;
			if(this.Not72_accuracy!=null) return true;
			if(this.Not72_responsetime!=null) return true;
			if(this.Not73_accuracy!=null) return true;
			if(this.Not73_responsetime!=null) return true;
			if(this.Not74_accuracy!=null) return true;
			if(this.Not74_responsetime!=null) return true;
			if(this.Not75_responsetime!=null) return true;
			if(this.Not75_accuracy!=null) return true;
			if(this.Not76_responsetime!=null) return true;
			if(this.Not76_accuracy!=null) return true;
			if(this.Not77_responsetime!=null) return true;
			if(this.Not77_accuracy!=null) return true;
			if(this.Not78_responsetime!=null) return true;
			if(this.Not78_accuracy!=null) return true;
			if(this.Not79_responsetime!=null) return true;
			if(this.Not79_accuracy!=null) return true;
			if(this.Not80_responsetime!=null) return true;
			if(this.Not80_accuracy!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
