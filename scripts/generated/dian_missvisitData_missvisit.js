/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function dian_missvisitData_missvisit(){
this.xsiType="dian:missvisitData_missvisit";

	this.getSchemaElementName=function(){
		return "missvisitData_missvisit";
	}

	this.getFullSchemaElementName=function(){
		return "dian:missvisitData_missvisit";
	}

	this.Recno=null;


	function getRecno() {
		return this.Recno;
	}
	this.getRecno=getRecno;


	function setRecno(v){
		this.Recno=v;
	}
	this.setRecno=setRecno;

	this.Partial=null;


	function getPartial() {
		return this.Partial;
	}
	this.getPartial=getPartial;


	function setPartial(v){
		this.Partial=v;
	}
	this.setPartial=setPartial;

	this.Missother=null;


	function getMissother() {
		return this.Missother;
	}
	this.getMissother=getMissother;


	function setMissother(v){
		this.Missother=v;
	}
	this.setMissother=setMissother;

	this.Reason=null;


	function getReason() {
		return this.Reason;
	}
	this.getReason=getReason;


	function setReason(v){
		this.Reason=v;
	}
	this.setReason=setReason;

	this.Partcom=null;


	function getPartcom() {
		return this.Partcom;
	}
	this.getPartcom=getPartcom;


	function setPartcom(v){
		this.Partcom=v;
	}
	this.setPartcom=setPartcom;

	this.Followup=null;


	function getFollowup() {
		return this.Followup;
	}
	this.getFollowup=getFollowup;


	function setFollowup(v){
		this.Followup=v;
	}
	this.setFollowup=setFollowup;

	this.Resched=null;


	function getResched() {
		return this.Resched;
	}
	this.getResched=getResched;


	function setResched(v){
		this.Resched=v;
	}
	this.setResched=setResched;

	this.Reschedo=null;


	function getReschedo() {
		return this.Reschedo;
	}
	this.getReschedo=getReschedo;


	function setReschedo(v){
		this.Reschedo=v;
	}
	this.setReschedo=setReschedo;

	this.DianMissvisitdataMissvisitId=null;


	function getDianMissvisitdataMissvisitId() {
		return this.DianMissvisitdataMissvisitId;
	}
	this.getDianMissvisitdataMissvisitId=getDianMissvisitdataMissvisitId;


	function setDianMissvisitdataMissvisitId(v){
		this.DianMissvisitdataMissvisitId=v;
	}
	this.setDianMissvisitdataMissvisitId=setDianMissvisitdataMissvisitId;

	this.missvisitlist_missvisit_dian_mi_id_fk=null;


	this.getmissvisitlist_missvisit_dian_mi_id=function() {
		return this.missvisitlist_missvisit_dian_mi_id_fk;
	}


	this.setmissvisitlist_missvisit_dian_mi_id=function(v){
		this.missvisitlist_missvisit_dian_mi_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="RECNO"){
				return this.Recno ;
			} else 
			if(xmlPath=="PARTIAL"){
				return this.Partial ;
			} else 
			if(xmlPath=="MISSOTHER"){
				return this.Missother ;
			} else 
			if(xmlPath=="REASON"){
				return this.Reason ;
			} else 
			if(xmlPath=="PARTCOM"){
				return this.Partcom ;
			} else 
			if(xmlPath=="FOLLOWUP"){
				return this.Followup ;
			} else 
			if(xmlPath=="RESCHED"){
				return this.Resched ;
			} else 
			if(xmlPath=="RESCHEDO"){
				return this.Reschedo ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="dian_missvisitData_missvisit_id"){
				return this.DianMissvisitdataMissvisitId ;
			} else 
			if(xmlPath=="missvisitlist_missvisit_dian_mi_id"){
				return this.missvisitlist_missvisit_dian_mi_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="RECNO"){
				this.Recno=value;
			} else 
			if(xmlPath=="PARTIAL"){
				this.Partial=value;
			} else 
			if(xmlPath=="MISSOTHER"){
				this.Missother=value;
			} else 
			if(xmlPath=="REASON"){
				this.Reason=value;
			} else 
			if(xmlPath=="PARTCOM"){
				this.Partcom=value;
			} else 
			if(xmlPath=="FOLLOWUP"){
				this.Followup=value;
			} else 
			if(xmlPath=="RESCHED"){
				this.Resched=value;
			} else 
			if(xmlPath=="RESCHEDO"){
				this.Reschedo=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="dian_missvisitData_missvisit_id"){
				this.DianMissvisitdataMissvisitId=value;
			} else 
			if(xmlPath=="missvisitlist_missvisit_dian_mi_id"){
				this.missvisitlist_missvisit_dian_mi_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="RECNO"){
			return "field_data";
		}else if (xmlPath=="PARTIAL"){
			return "field_data";
		}else if (xmlPath=="MISSOTHER"){
			return "field_LONG_DATA";
		}else if (xmlPath=="REASON"){
			return "field_data";
		}else if (xmlPath=="PARTCOM"){
			return "field_LONG_DATA";
		}else if (xmlPath=="FOLLOWUP"){
			return "field_data";
		}else if (xmlPath=="RESCHED"){
			return "field_data";
		}else if (xmlPath=="RESCHEDO"){
			return "field_LONG_DATA";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<dian:missvisitData_missvisit";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</dian:missvisitData_missvisit>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.DianMissvisitdataMissvisitId!=null){
				if(hiddenCount++>0)str+=",";
				str+="dian_missvisitData_missvisit_id=\"" + this.DianMissvisitdataMissvisitId + "\"";
			}
			if(this.missvisitlist_missvisit_dian_mi_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="missvisitlist_missvisit_dian_mi_id=\"" + this.missvisitlist_missvisit_dian_mi_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Recno!=null){
			xmlTxt+="\n<dian:RECNO";
			xmlTxt+=">";
			xmlTxt+=this.Recno;
			xmlTxt+="</dian:RECNO>";
		}
		if (this.Partial!=null){
			xmlTxt+="\n<dian:PARTIAL";
			xmlTxt+=">";
			xmlTxt+=this.Partial.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:PARTIAL>";
		}
		if (this.Missother!=null){
			xmlTxt+="\n<dian:MISSOTHER";
			xmlTxt+=">";
			xmlTxt+=this.Missother.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:MISSOTHER>";
		}
		else{
			xmlTxt+="\n<dian:MISSOTHER";
			xmlTxt+="/>";
		}

		if (this.Reason!=null){
			xmlTxt+="\n<dian:REASON";
			xmlTxt+=">";
			xmlTxt+=this.Reason.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:REASON>";
		}
		if (this.Partcom!=null){
			xmlTxt+="\n<dian:PARTCOM";
			xmlTxt+=">";
			xmlTxt+=this.Partcom.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:PARTCOM>";
		}
		else{
			xmlTxt+="\n<dian:PARTCOM";
			xmlTxt+="/>";
		}

		if (this.Followup!=null){
			xmlTxt+="\n<dian:FOLLOWUP";
			xmlTxt+=">";
			xmlTxt+=this.Followup;
			xmlTxt+="</dian:FOLLOWUP>";
		}
		if (this.Resched!=null){
			xmlTxt+="\n<dian:RESCHED";
			xmlTxt+=">";
			xmlTxt+=this.Resched.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:RESCHED>";
		}
		if (this.Reschedo!=null){
			xmlTxt+="\n<dian:RESCHEDO";
			xmlTxt+=">";
			xmlTxt+=this.Reschedo.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:RESCHEDO>";
		}
		else{
			xmlTxt+="\n<dian:RESCHEDO";
			xmlTxt+="/>";
		}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.DianMissvisitdataMissvisitId!=null) return true;
			if (this.missvisitlist_missvisit_dian_mi_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Recno!=null) return true;
		if (this.Partial!=null) return true;
		if (this.Missother!=null) return true;
		return true;//REQUIRED MISSOTHER
	}
}
