/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function ados_ados2001Module3Data(){
this.xsiType="ados:ados2001Module3Data";

	this.getSchemaElementName=function(){
		return "ados2001Module3Data";
	}

	this.getFullSchemaElementName=function(){
		return "ados:ados2001Module3Data";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Commtot=null;


	function getCommtot() {
		return this.Commtot;
	}
	this.getCommtot=getCommtot;


	function setCommtot(v){
		this.Commtot=v;
	}
	this.setCommtot=setCommtot;

	this.CommtotNote=null;


	function getCommtotNote() {
		return this.CommtotNote;
	}
	this.getCommtotNote=getCommtotNote;


	function setCommtotNote(v){
		this.CommtotNote=v;
	}
	this.setCommtotNote=setCommtotNote;

	this.Scintot=null;


	function getScintot() {
		return this.Scintot;
	}
	this.getScintot=getScintot;


	function setScintot(v){
		this.Scintot=v;
	}
	this.setScintot=setScintot;

	this.ScintotNote=null;


	function getScintotNote() {
		return this.ScintotNote;
	}
	this.getScintotNote=getScintotNote;


	function setScintotNote(v){
		this.ScintotNote=v;
	}
	this.setScintotNote=setScintotNote;

	this.Cmsitot=null;


	function getCmsitot() {
		return this.Cmsitot;
	}
	this.getCmsitot=getCmsitot;


	function setCmsitot(v){
		this.Cmsitot=v;
	}
	this.setCmsitot=setCmsitot;

	this.CmsitotNote=null;


	function getCmsitotNote() {
		return this.CmsitotNote;
	}
	this.getCmsitotNote=getCmsitotNote;


	function setCmsitotNote(v){
		this.CmsitotNote=v;
	}
	this.setCmsitotNote=setCmsitotNote;

	this.Playtot=null;


	function getPlaytot() {
		return this.Playtot;
	}
	this.getPlaytot=getPlaytot;


	function setPlaytot(v){
		this.Playtot=v;
	}
	this.setPlaytot=setPlaytot;

	this.PlaytotNote=null;


	function getPlaytotNote() {
		return this.PlaytotNote;
	}
	this.getPlaytotNote=getPlaytotNote;


	function setPlaytotNote(v){
		this.PlaytotNote=v;
	}
	this.setPlaytotNote=setPlaytotNote;

	this.Imaginetot=null;


	function getImaginetot() {
		return this.Imaginetot;
	}
	this.getImaginetot=getImaginetot;


	function setImaginetot(v){
		this.Imaginetot=v;
	}
	this.setImaginetot=setImaginetot;

	this.ImaginetotNote=null;


	function getImaginetotNote() {
		return this.ImaginetotNote;
	}
	this.getImaginetotNote=getImaginetotNote;


	function setImaginetotNote(v){
		this.ImaginetotNote=v;
	}
	this.setImaginetotNote=setImaginetotNote;

	this.Sbritot=null;


	function getSbritot() {
		return this.Sbritot;
	}
	this.getSbritot=getSbritot;


	function setSbritot(v){
		this.Sbritot=v;
	}
	this.setSbritot=setSbritot;

	this.SbritotNote=null;


	function getSbritotNote() {
		return this.SbritotNote;
	}
	this.getSbritotNote=getSbritotNote;


	function setSbritotNote(v){
		this.SbritotNote=v;
	}
	this.setSbritotNote=setSbritotNote;

	this.Scoreform_diagnosis_adosclas=null;


	function getScoreform_diagnosis_adosclas() {
		return this.Scoreform_diagnosis_adosclas;
	}
	this.getScoreform_diagnosis_adosclas=getScoreform_diagnosis_adosclas;


	function setScoreform_diagnosis_adosclas(v){
		this.Scoreform_diagnosis_adosclas=v;
	}
	this.setScoreform_diagnosis_adosclas=setScoreform_diagnosis_adosclas;

	this.Scoreform_diagnosis_overalldiag=null;


	function getScoreform_diagnosis_overalldiag() {
		return this.Scoreform_diagnosis_overalldiag;
	}
	this.getScoreform_diagnosis_overalldiag=getScoreform_diagnosis_overalldiag;


	function setScoreform_diagnosis_overalldiag(v){
		this.Scoreform_diagnosis_overalldiag=v;
	}
	this.setScoreform_diagnosis_overalldiag=setScoreform_diagnosis_overalldiag;

	this.Scoreform_observation_constrtask=null;


	function getScoreform_observation_constrtask() {
		return this.Scoreform_observation_constrtask;
	}
	this.getScoreform_observation_constrtask=getScoreform_observation_constrtask;


	function setScoreform_observation_constrtask(v){
		this.Scoreform_observation_constrtask=v;
	}
	this.setScoreform_observation_constrtask=setScoreform_observation_constrtask;

	this.Scoreform_observation_makebelieve=null;


	function getScoreform_observation_makebelieve() {
		return this.Scoreform_observation_makebelieve;
	}
	this.getScoreform_observation_makebelieve=getScoreform_observation_makebelieve;


	function setScoreform_observation_makebelieve(v){
		this.Scoreform_observation_makebelieve=v;
	}
	this.setScoreform_observation_makebelieve=setScoreform_observation_makebelieve;

	this.Scoreform_observation_jointplay=null;


	function getScoreform_observation_jointplay() {
		return this.Scoreform_observation_jointplay;
	}
	this.getScoreform_observation_jointplay=getScoreform_observation_jointplay;


	function setScoreform_observation_jointplay(v){
		this.Scoreform_observation_jointplay=v;
	}
	this.setScoreform_observation_jointplay=setScoreform_observation_jointplay;

	this.Scoreform_observation_demonst=null;


	function getScoreform_observation_demonst() {
		return this.Scoreform_observation_demonst;
	}
	this.getScoreform_observation_demonst=getScoreform_observation_demonst;


	function setScoreform_observation_demonst(v){
		this.Scoreform_observation_demonst=v;
	}
	this.setScoreform_observation_demonst=setScoreform_observation_demonst;

	this.Scoreform_observation_descpicture=null;


	function getScoreform_observation_descpicture() {
		return this.Scoreform_observation_descpicture;
	}
	this.getScoreform_observation_descpicture=getScoreform_observation_descpicture;


	function setScoreform_observation_descpicture(v){
		this.Scoreform_observation_descpicture=v;
	}
	this.setScoreform_observation_descpicture=setScoreform_observation_descpicture;

	this.Scoreform_observation_tellstory=null;


	function getScoreform_observation_tellstory() {
		return this.Scoreform_observation_tellstory;
	}
	this.getScoreform_observation_tellstory=getScoreform_observation_tellstory;


	function setScoreform_observation_tellstory(v){
		this.Scoreform_observation_tellstory=v;
	}
	this.setScoreform_observation_tellstory=setScoreform_observation_tellstory;

	this.Scoreform_observation_cartoons=null;


	function getScoreform_observation_cartoons() {
		return this.Scoreform_observation_cartoons;
	}
	this.getScoreform_observation_cartoons=getScoreform_observation_cartoons;


	function setScoreform_observation_cartoons(v){
		this.Scoreform_observation_cartoons=v;
	}
	this.setScoreform_observation_cartoons=setScoreform_observation_cartoons;

	this.Scoreform_observation_convsreport=null;


	function getScoreform_observation_convsreport() {
		return this.Scoreform_observation_convsreport;
	}
	this.getScoreform_observation_convsreport=getScoreform_observation_convsreport;


	function setScoreform_observation_convsreport(v){
		this.Scoreform_observation_convsreport=v;
	}
	this.setScoreform_observation_convsreport=setScoreform_observation_convsreport;

	this.Scoreform_observation_emotions=null;


	function getScoreform_observation_emotions() {
		return this.Scoreform_observation_emotions;
	}
	this.getScoreform_observation_emotions=getScoreform_observation_emotions;


	function setScoreform_observation_emotions(v){
		this.Scoreform_observation_emotions=v;
	}
	this.setScoreform_observation_emotions=setScoreform_observation_emotions;

	this.Scoreform_observation_socdiffannoy=null;


	function getScoreform_observation_socdiffannoy() {
		return this.Scoreform_observation_socdiffannoy;
	}
	this.getScoreform_observation_socdiffannoy=getScoreform_observation_socdiffannoy;


	function setScoreform_observation_socdiffannoy(v){
		this.Scoreform_observation_socdiffannoy=v;
	}
	this.setScoreform_observation_socdiffannoy=setScoreform_observation_socdiffannoy;

	this.Scoreform_observation_break=null;


	function getScoreform_observation_break() {
		return this.Scoreform_observation_break;
	}
	this.getScoreform_observation_break=getScoreform_observation_break;


	function setScoreform_observation_break(v){
		this.Scoreform_observation_break=v;
	}
	this.setScoreform_observation_break=setScoreform_observation_break;

	this.Scoreform_observation_friendmarrg=null;


	function getScoreform_observation_friendmarrg() {
		return this.Scoreform_observation_friendmarrg;
	}
	this.getScoreform_observation_friendmarrg=getScoreform_observation_friendmarrg;


	function setScoreform_observation_friendmarrg(v){
		this.Scoreform_observation_friendmarrg=v;
	}
	this.setScoreform_observation_friendmarrg=setScoreform_observation_friendmarrg;

	this.Scoreform_observation_loneliness=null;


	function getScoreform_observation_loneliness() {
		return this.Scoreform_observation_loneliness;
	}
	this.getScoreform_observation_loneliness=getScoreform_observation_loneliness;


	function setScoreform_observation_loneliness(v){
		this.Scoreform_observation_loneliness=v;
	}
	this.setScoreform_observation_loneliness=setScoreform_observation_loneliness;

	this.Scoreform_observation_createstory=null;


	function getScoreform_observation_createstory() {
		return this.Scoreform_observation_createstory;
	}
	this.getScoreform_observation_createstory=getScoreform_observation_createstory;


	function setScoreform_observation_createstory(v){
		this.Scoreform_observation_createstory=v;
	}
	this.setScoreform_observation_createstory=setScoreform_observation_createstory;

	this.Scoreform_sectiona_nonechoedlang=null;


	function getScoreform_sectiona_nonechoedlang() {
		return this.Scoreform_sectiona_nonechoedlang;
	}
	this.getScoreform_sectiona_nonechoedlang=getScoreform_sectiona_nonechoedlang;


	function setScoreform_sectiona_nonechoedlang(v){
		this.Scoreform_sectiona_nonechoedlang=v;
	}
	this.setScoreform_sectiona_nonechoedlang=setScoreform_sectiona_nonechoedlang;

	this.Scoreform_sectiona_speechabnorm=null;


	function getScoreform_sectiona_speechabnorm() {
		return this.Scoreform_sectiona_speechabnorm;
	}
	this.getScoreform_sectiona_speechabnorm=getScoreform_sectiona_speechabnorm;


	function setScoreform_sectiona_speechabnorm(v){
		this.Scoreform_sectiona_speechabnorm=v;
	}
	this.setScoreform_sectiona_speechabnorm=setScoreform_sectiona_speechabnorm;

	this.Scoreform_sectiona_immedecholalia=null;


	function getScoreform_sectiona_immedecholalia() {
		return this.Scoreform_sectiona_immedecholalia;
	}
	this.getScoreform_sectiona_immedecholalia=getScoreform_sectiona_immedecholalia;


	function setScoreform_sectiona_immedecholalia(v){
		this.Scoreform_sectiona_immedecholalia=v;
	}
	this.setScoreform_sectiona_immedecholalia=setScoreform_sectiona_immedecholalia;

	this.Scoreform_sectiona_stidiosyncwords=null;


	function getScoreform_sectiona_stidiosyncwords() {
		return this.Scoreform_sectiona_stidiosyncwords;
	}
	this.getScoreform_sectiona_stidiosyncwords=getScoreform_sectiona_stidiosyncwords;


	function setScoreform_sectiona_stidiosyncwords(v){
		this.Scoreform_sectiona_stidiosyncwords=v;
	}
	this.setScoreform_sectiona_stidiosyncwords=setScoreform_sectiona_stidiosyncwords;

	this.Scoreform_sectiona_offinform=null;


	function getScoreform_sectiona_offinform() {
		return this.Scoreform_sectiona_offinform;
	}
	this.getScoreform_sectiona_offinform=getScoreform_sectiona_offinform;


	function setScoreform_sectiona_offinform(v){
		this.Scoreform_sectiona_offinform=v;
	}
	this.setScoreform_sectiona_offinform=setScoreform_sectiona_offinform;

	this.Scoreform_sectiona_askinform=null;


	function getScoreform_sectiona_askinform() {
		return this.Scoreform_sectiona_askinform;
	}
	this.getScoreform_sectiona_askinform=getScoreform_sectiona_askinform;


	function setScoreform_sectiona_askinform(v){
		this.Scoreform_sectiona_askinform=v;
	}
	this.setScoreform_sectiona_askinform=setScoreform_sectiona_askinform;

	this.Scoreform_sectiona_reportevents=null;


	function getScoreform_sectiona_reportevents() {
		return this.Scoreform_sectiona_reportevents;
	}
	this.getScoreform_sectiona_reportevents=getScoreform_sectiona_reportevents;


	function setScoreform_sectiona_reportevents(v){
		this.Scoreform_sectiona_reportevents=v;
	}
	this.setScoreform_sectiona_reportevents=setScoreform_sectiona_reportevents;

	this.Scoreform_sectiona_conversation=null;


	function getScoreform_sectiona_conversation() {
		return this.Scoreform_sectiona_conversation;
	}
	this.getScoreform_sectiona_conversation=getScoreform_sectiona_conversation;


	function setScoreform_sectiona_conversation(v){
		this.Scoreform_sectiona_conversation=v;
	}
	this.setScoreform_sectiona_conversation=setScoreform_sectiona_conversation;

	this.Scoreform_sectiona_descinformgest=null;


	function getScoreform_sectiona_descinformgest() {
		return this.Scoreform_sectiona_descinformgest;
	}
	this.getScoreform_sectiona_descinformgest=getScoreform_sectiona_descinformgest;


	function setScoreform_sectiona_descinformgest(v){
		this.Scoreform_sectiona_descinformgest=v;
	}
	this.setScoreform_sectiona_descinformgest=setScoreform_sectiona_descinformgest;

	this.Scoreform_sectionb_unusleyecont=null;


	function getScoreform_sectionb_unusleyecont() {
		return this.Scoreform_sectionb_unusleyecont;
	}
	this.getScoreform_sectionb_unusleyecont=getScoreform_sectionb_unusleyecont;


	function setScoreform_sectionb_unusleyecont(v){
		this.Scoreform_sectionb_unusleyecont=v;
	}
	this.setScoreform_sectionb_unusleyecont=setScoreform_sectionb_unusleyecont;

	this.Scoreform_sectionb_facexproth=null;


	function getScoreform_sectionb_facexproth() {
		return this.Scoreform_sectionb_facexproth;
	}
	this.getScoreform_sectionb_facexproth=getScoreform_sectionb_facexproth;


	function setScoreform_sectionb_facexproth(v){
		this.Scoreform_sectionb_facexproth=v;
	}
	this.setScoreform_sectionb_facexproth=setScoreform_sectionb_facexproth;

	this.Scoreform_sectionb_langprod=null;


	function getScoreform_sectionb_langprod() {
		return this.Scoreform_sectionb_langprod;
	}
	this.getScoreform_sectionb_langprod=getScoreform_sectionb_langprod;


	function setScoreform_sectionb_langprod(v){
		this.Scoreform_sectionb_langprod=v;
	}
	this.setScoreform_sectionb_langprod=setScoreform_sectionb_langprod;

	this.Scoreform_sectionb_shareenjoy=null;


	function getScoreform_sectionb_shareenjoy() {
		return this.Scoreform_sectionb_shareenjoy;
	}
	this.getScoreform_sectionb_shareenjoy=getScoreform_sectionb_shareenjoy;


	function setScoreform_sectionb_shareenjoy(v){
		this.Scoreform_sectionb_shareenjoy=v;
	}
	this.setScoreform_sectionb_shareenjoy=setScoreform_sectionb_shareenjoy;

	this.Scoreform_sectionb_empathcomm=null;


	function getScoreform_sectionb_empathcomm() {
		return this.Scoreform_sectionb_empathcomm;
	}
	this.getScoreform_sectionb_empathcomm=getScoreform_sectionb_empathcomm;


	function setScoreform_sectionb_empathcomm(v){
		this.Scoreform_sectionb_empathcomm=v;
	}
	this.setScoreform_sectionb_empathcomm=setScoreform_sectionb_empathcomm;

	this.Scoreform_sectionb_insight=null;


	function getScoreform_sectionb_insight() {
		return this.Scoreform_sectionb_insight;
	}
	this.getScoreform_sectionb_insight=getScoreform_sectionb_insight;


	function setScoreform_sectionb_insight(v){
		this.Scoreform_sectionb_insight=v;
	}
	this.setScoreform_sectionb_insight=setScoreform_sectionb_insight;

	this.Scoreform_sectionb_qualsocoverture=null;


	function getScoreform_sectionb_qualsocoverture() {
		return this.Scoreform_sectionb_qualsocoverture;
	}
	this.getScoreform_sectionb_qualsocoverture=getScoreform_sectionb_qualsocoverture;


	function setScoreform_sectionb_qualsocoverture(v){
		this.Scoreform_sectionb_qualsocoverture=v;
	}
	this.setScoreform_sectionb_qualsocoverture=setScoreform_sectionb_qualsocoverture;

	this.Scoreform_sectionb_qualsocresp=null;


	function getScoreform_sectionb_qualsocresp() {
		return this.Scoreform_sectionb_qualsocresp;
	}
	this.getScoreform_sectionb_qualsocresp=getScoreform_sectionb_qualsocresp;


	function setScoreform_sectionb_qualsocresp(v){
		this.Scoreform_sectionb_qualsocresp=v;
	}
	this.setScoreform_sectionb_qualsocresp=setScoreform_sectionb_qualsocresp;

	this.Scoreform_sectionb_amtrecipsoccomm=null;


	function getScoreform_sectionb_amtrecipsoccomm() {
		return this.Scoreform_sectionb_amtrecipsoccomm;
	}
	this.getScoreform_sectionb_amtrecipsoccomm=getScoreform_sectionb_amtrecipsoccomm;


	function setScoreform_sectionb_amtrecipsoccomm(v){
		this.Scoreform_sectionb_amtrecipsoccomm=v;
	}
	this.setScoreform_sectionb_amtrecipsoccomm=setScoreform_sectionb_amtrecipsoccomm;

	this.Scoreform_sectionb_overallqualrapp=null;


	function getScoreform_sectionb_overallqualrapp() {
		return this.Scoreform_sectionb_overallqualrapp;
	}
	this.getScoreform_sectionb_overallqualrapp=getScoreform_sectionb_overallqualrapp;


	function setScoreform_sectionb_overallqualrapp(v){
		this.Scoreform_sectionb_overallqualrapp=v;
	}
	this.setScoreform_sectionb_overallqualrapp=setScoreform_sectionb_overallqualrapp;

	this.Scoreform_sectionc_imagcreativ=null;


	function getScoreform_sectionc_imagcreativ() {
		return this.Scoreform_sectionc_imagcreativ;
	}
	this.getScoreform_sectionc_imagcreativ=getScoreform_sectionc_imagcreativ;


	function setScoreform_sectionc_imagcreativ(v){
		this.Scoreform_sectionc_imagcreativ=v;
	}
	this.setScoreform_sectionc_imagcreativ=setScoreform_sectionc_imagcreativ;

	this.Scoreform_sectiond_unuslsensint=null;


	function getScoreform_sectiond_unuslsensint() {
		return this.Scoreform_sectiond_unuslsensint;
	}
	this.getScoreform_sectiond_unuslsensint=getScoreform_sectiond_unuslsensint;


	function setScoreform_sectiond_unuslsensint(v){
		this.Scoreform_sectiond_unuslsensint=v;
	}
	this.setScoreform_sectiond_unuslsensint=setScoreform_sectiond_unuslsensint;

	this.Scoreform_sectiond_handfgrcomplx=null;


	function getScoreform_sectiond_handfgrcomplx() {
		return this.Scoreform_sectiond_handfgrcomplx;
	}
	this.getScoreform_sectiond_handfgrcomplx=getScoreform_sectiond_handfgrcomplx;


	function setScoreform_sectiond_handfgrcomplx(v){
		this.Scoreform_sectiond_handfgrcomplx=v;
	}
	this.setScoreform_sectiond_handfgrcomplx=setScoreform_sectiond_handfgrcomplx;

	this.Scoreform_sectiond_selfinjurbehav=null;


	function getScoreform_sectiond_selfinjurbehav() {
		return this.Scoreform_sectiond_selfinjurbehav;
	}
	this.getScoreform_sectiond_selfinjurbehav=getScoreform_sectiond_selfinjurbehav;


	function setScoreform_sectiond_selfinjurbehav(v){
		this.Scoreform_sectiond_selfinjurbehav=v;
	}
	this.setScoreform_sectiond_selfinjurbehav=setScoreform_sectiond_selfinjurbehav;

	this.Scoreform_sectiond_excessinterstunusl=null;


	function getScoreform_sectiond_excessinterstunusl() {
		return this.Scoreform_sectiond_excessinterstunusl;
	}
	this.getScoreform_sectiond_excessinterstunusl=getScoreform_sectiond_excessinterstunusl;


	function setScoreform_sectiond_excessinterstunusl(v){
		this.Scoreform_sectiond_excessinterstunusl=v;
	}
	this.setScoreform_sectiond_excessinterstunusl=setScoreform_sectiond_excessinterstunusl;

	this.Scoreform_sectiond_compulritual=null;


	function getScoreform_sectiond_compulritual() {
		return this.Scoreform_sectiond_compulritual;
	}
	this.getScoreform_sectiond_compulritual=getScoreform_sectiond_compulritual;


	function setScoreform_sectiond_compulritual(v){
		this.Scoreform_sectiond_compulritual=v;
	}
	this.setScoreform_sectiond_compulritual=setScoreform_sectiond_compulritual;

	this.Scoreform_sectione_overactagit=null;


	function getScoreform_sectione_overactagit() {
		return this.Scoreform_sectione_overactagit;
	}
	this.getScoreform_sectione_overactagit=getScoreform_sectione_overactagit;


	function setScoreform_sectione_overactagit(v){
		this.Scoreform_sectione_overactagit=v;
	}
	this.setScoreform_sectione_overactagit=setScoreform_sectione_overactagit;

	this.Scoreform_sectione_tantrmagress=null;


	function getScoreform_sectione_tantrmagress() {
		return this.Scoreform_sectione_tantrmagress;
	}
	this.getScoreform_sectione_tantrmagress=getScoreform_sectione_tantrmagress;


	function setScoreform_sectione_tantrmagress(v){
		this.Scoreform_sectione_tantrmagress=v;
	}
	this.setScoreform_sectione_tantrmagress=setScoreform_sectione_tantrmagress;

	this.Scoreform_sectione_anxiety=null;


	function getScoreform_sectione_anxiety() {
		return this.Scoreform_sectione_anxiety;
	}
	this.getScoreform_sectione_anxiety=getScoreform_sectione_anxiety;


	function setScoreform_sectione_anxiety(v){
		this.Scoreform_sectione_anxiety=v;
	}
	this.setScoreform_sectione_anxiety=setScoreform_sectione_anxiety;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="commtot"){
				return this.Commtot ;
			} else 
			if(xmlPath=="commtot_note"){
				return this.CommtotNote ;
			} else 
			if(xmlPath=="scintot"){
				return this.Scintot ;
			} else 
			if(xmlPath=="scintot_note"){
				return this.ScintotNote ;
			} else 
			if(xmlPath=="cmsitot"){
				return this.Cmsitot ;
			} else 
			if(xmlPath=="cmsitot_note"){
				return this.CmsitotNote ;
			} else 
			if(xmlPath=="playtot"){
				return this.Playtot ;
			} else 
			if(xmlPath=="playtot_note"){
				return this.PlaytotNote ;
			} else 
			if(xmlPath=="imaginetot"){
				return this.Imaginetot ;
			} else 
			if(xmlPath=="imaginetot_note"){
				return this.ImaginetotNote ;
			} else 
			if(xmlPath=="sbritot"){
				return this.Sbritot ;
			} else 
			if(xmlPath=="sbritot_note"){
				return this.SbritotNote ;
			} else 
			if(xmlPath=="ScoreForm/diagnosis/adosclas"){
				return this.Scoreform_diagnosis_adosclas ;
			} else 
			if(xmlPath=="ScoreForm/diagnosis/overallDiag"){
				return this.Scoreform_diagnosis_overalldiag ;
			} else 
			if(xmlPath=="ScoreForm/observation/constrTask"){
				return this.Scoreform_observation_constrtask ;
			} else 
			if(xmlPath=="ScoreForm/observation/makeBelieve"){
				return this.Scoreform_observation_makebelieve ;
			} else 
			if(xmlPath=="ScoreForm/observation/jointPlay"){
				return this.Scoreform_observation_jointplay ;
			} else 
			if(xmlPath=="ScoreForm/observation/demonst"){
				return this.Scoreform_observation_demonst ;
			} else 
			if(xmlPath=="ScoreForm/observation/descPicture"){
				return this.Scoreform_observation_descpicture ;
			} else 
			if(xmlPath=="ScoreForm/observation/tellStory"){
				return this.Scoreform_observation_tellstory ;
			} else 
			if(xmlPath=="ScoreForm/observation/cartoons"){
				return this.Scoreform_observation_cartoons ;
			} else 
			if(xmlPath=="ScoreForm/observation/convsReport"){
				return this.Scoreform_observation_convsreport ;
			} else 
			if(xmlPath=="ScoreForm/observation/emotions"){
				return this.Scoreform_observation_emotions ;
			} else 
			if(xmlPath=="ScoreForm/observation/socDiffAnnoy"){
				return this.Scoreform_observation_socdiffannoy ;
			} else 
			if(xmlPath=="ScoreForm/observation/break"){
				return this.Scoreform_observation_break ;
			} else 
			if(xmlPath=="ScoreForm/observation/friendMarrg"){
				return this.Scoreform_observation_friendmarrg ;
			} else 
			if(xmlPath=="ScoreForm/observation/loneliness"){
				return this.Scoreform_observation_loneliness ;
			} else 
			if(xmlPath=="ScoreForm/observation/createStory"){
				return this.Scoreform_observation_createstory ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/nonechoedLang"){
				return this.Scoreform_sectiona_nonechoedlang ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/speechAbnorm"){
				return this.Scoreform_sectiona_speechabnorm ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/immedEcholalia"){
				return this.Scoreform_sectiona_immedecholalia ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/stIdiosyncWords"){
				return this.Scoreform_sectiona_stidiosyncwords ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/offInform"){
				return this.Scoreform_sectiona_offinform ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/askInform"){
				return this.Scoreform_sectiona_askinform ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/reportEvents"){
				return this.Scoreform_sectiona_reportevents ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/conversation"){
				return this.Scoreform_sectiona_conversation ;
			} else 
			if(xmlPath=="ScoreForm/sectionA/descInformGest"){
				return this.Scoreform_sectiona_descinformgest ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/unuslEyeCont"){
				return this.Scoreform_sectionb_unusleyecont ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/facExprOth"){
				return this.Scoreform_sectionb_facexproth ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/langProd"){
				return this.Scoreform_sectionb_langprod ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/shareEnjoy"){
				return this.Scoreform_sectionb_shareenjoy ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/empathComm"){
				return this.Scoreform_sectionb_empathcomm ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/insight"){
				return this.Scoreform_sectionb_insight ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/qualSocOverture"){
				return this.Scoreform_sectionb_qualsocoverture ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/qualSocResp"){
				return this.Scoreform_sectionb_qualsocresp ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/amtRecipSocComm"){
				return this.Scoreform_sectionb_amtrecipsoccomm ;
			} else 
			if(xmlPath=="ScoreForm/sectionB/overallQualRapp"){
				return this.Scoreform_sectionb_overallqualrapp ;
			} else 
			if(xmlPath=="ScoreForm/sectionC/imagCreativ"){
				return this.Scoreform_sectionc_imagcreativ ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/unuslSensInt"){
				return this.Scoreform_sectiond_unuslsensint ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/handFgrComplx"){
				return this.Scoreform_sectiond_handfgrcomplx ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/selfInjurBehav"){
				return this.Scoreform_sectiond_selfinjurbehav ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/excessInterstUnusl"){
				return this.Scoreform_sectiond_excessinterstunusl ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/compulRitual"){
				return this.Scoreform_sectiond_compulritual ;
			} else 
			if(xmlPath=="ScoreForm/sectionE/overactAgit"){
				return this.Scoreform_sectione_overactagit ;
			} else 
			if(xmlPath=="ScoreForm/sectionE/tantrmAgress"){
				return this.Scoreform_sectione_tantrmagress ;
			} else 
			if(xmlPath=="ScoreForm/sectionE/anxiety"){
				return this.Scoreform_sectione_anxiety ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="commtot"){
				this.Commtot=value;
			} else 
			if(xmlPath=="commtot_note"){
				this.CommtotNote=value;
			} else 
			if(xmlPath=="scintot"){
				this.Scintot=value;
			} else 
			if(xmlPath=="scintot_note"){
				this.ScintotNote=value;
			} else 
			if(xmlPath=="cmsitot"){
				this.Cmsitot=value;
			} else 
			if(xmlPath=="cmsitot_note"){
				this.CmsitotNote=value;
			} else 
			if(xmlPath=="playtot"){
				this.Playtot=value;
			} else 
			if(xmlPath=="playtot_note"){
				this.PlaytotNote=value;
			} else 
			if(xmlPath=="imaginetot"){
				this.Imaginetot=value;
			} else 
			if(xmlPath=="imaginetot_note"){
				this.ImaginetotNote=value;
			} else 
			if(xmlPath=="sbritot"){
				this.Sbritot=value;
			} else 
			if(xmlPath=="sbritot_note"){
				this.SbritotNote=value;
			} else 
			if(xmlPath=="ScoreForm/diagnosis/adosclas"){
				this.Scoreform_diagnosis_adosclas=value;
			} else 
			if(xmlPath=="ScoreForm/diagnosis/overallDiag"){
				this.Scoreform_diagnosis_overalldiag=value;
			} else 
			if(xmlPath=="ScoreForm/observation/constrTask"){
				this.Scoreform_observation_constrtask=value;
			} else 
			if(xmlPath=="ScoreForm/observation/makeBelieve"){
				this.Scoreform_observation_makebelieve=value;
			} else 
			if(xmlPath=="ScoreForm/observation/jointPlay"){
				this.Scoreform_observation_jointplay=value;
			} else 
			if(xmlPath=="ScoreForm/observation/demonst"){
				this.Scoreform_observation_demonst=value;
			} else 
			if(xmlPath=="ScoreForm/observation/descPicture"){
				this.Scoreform_observation_descpicture=value;
			} else 
			if(xmlPath=="ScoreForm/observation/tellStory"){
				this.Scoreform_observation_tellstory=value;
			} else 
			if(xmlPath=="ScoreForm/observation/cartoons"){
				this.Scoreform_observation_cartoons=value;
			} else 
			if(xmlPath=="ScoreForm/observation/convsReport"){
				this.Scoreform_observation_convsreport=value;
			} else 
			if(xmlPath=="ScoreForm/observation/emotions"){
				this.Scoreform_observation_emotions=value;
			} else 
			if(xmlPath=="ScoreForm/observation/socDiffAnnoy"){
				this.Scoreform_observation_socdiffannoy=value;
			} else 
			if(xmlPath=="ScoreForm/observation/break"){
				this.Scoreform_observation_break=value;
			} else 
			if(xmlPath=="ScoreForm/observation/friendMarrg"){
				this.Scoreform_observation_friendmarrg=value;
			} else 
			if(xmlPath=="ScoreForm/observation/loneliness"){
				this.Scoreform_observation_loneliness=value;
			} else 
			if(xmlPath=="ScoreForm/observation/createStory"){
				this.Scoreform_observation_createstory=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/nonechoedLang"){
				this.Scoreform_sectiona_nonechoedlang=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/speechAbnorm"){
				this.Scoreform_sectiona_speechabnorm=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/immedEcholalia"){
				this.Scoreform_sectiona_immedecholalia=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/stIdiosyncWords"){
				this.Scoreform_sectiona_stidiosyncwords=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/offInform"){
				this.Scoreform_sectiona_offinform=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/askInform"){
				this.Scoreform_sectiona_askinform=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/reportEvents"){
				this.Scoreform_sectiona_reportevents=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/conversation"){
				this.Scoreform_sectiona_conversation=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA/descInformGest"){
				this.Scoreform_sectiona_descinformgest=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/unuslEyeCont"){
				this.Scoreform_sectionb_unusleyecont=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/facExprOth"){
				this.Scoreform_sectionb_facexproth=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/langProd"){
				this.Scoreform_sectionb_langprod=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/shareEnjoy"){
				this.Scoreform_sectionb_shareenjoy=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/empathComm"){
				this.Scoreform_sectionb_empathcomm=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/insight"){
				this.Scoreform_sectionb_insight=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/qualSocOverture"){
				this.Scoreform_sectionb_qualsocoverture=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/qualSocResp"){
				this.Scoreform_sectionb_qualsocresp=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/amtRecipSocComm"){
				this.Scoreform_sectionb_amtrecipsoccomm=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB/overallQualRapp"){
				this.Scoreform_sectionb_overallqualrapp=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC/imagCreativ"){
				this.Scoreform_sectionc_imagcreativ=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/unuslSensInt"){
				this.Scoreform_sectiond_unuslsensint=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/handFgrComplx"){
				this.Scoreform_sectiond_handfgrcomplx=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/selfInjurBehav"){
				this.Scoreform_sectiond_selfinjurbehav=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/excessInterstUnusl"){
				this.Scoreform_sectiond_excessinterstunusl=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/compulRitual"){
				this.Scoreform_sectiond_compulritual=value;
			} else 
			if(xmlPath=="ScoreForm/sectionE/overactAgit"){
				this.Scoreform_sectione_overactagit=value;
			} else 
			if(xmlPath=="ScoreForm/sectionE/tantrmAgress"){
				this.Scoreform_sectione_tantrmagress=value;
			} else 
			if(xmlPath=="ScoreForm/sectionE/anxiety"){
				this.Scoreform_sectione_anxiety=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="commtot"){
			return "field_data";
		}else if (xmlPath=="commtot_note"){
			return "field_data";
		}else if (xmlPath=="scintot"){
			return "field_data";
		}else if (xmlPath=="scintot_note"){
			return "field_data";
		}else if (xmlPath=="cmsitot"){
			return "field_data";
		}else if (xmlPath=="cmsitot_note"){
			return "field_data";
		}else if (xmlPath=="playtot"){
			return "field_data";
		}else if (xmlPath=="playtot_note"){
			return "field_data";
		}else if (xmlPath=="imaginetot"){
			return "field_data";
		}else if (xmlPath=="imaginetot_note"){
			return "field_data";
		}else if (xmlPath=="sbritot"){
			return "field_data";
		}else if (xmlPath=="sbritot_note"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/diagnosis/adosclas"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/diagnosis/overallDiag"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/constrTask"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/makeBelieve"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/jointPlay"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/demonst"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/descPicture"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/tellStory"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/cartoons"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/convsReport"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/emotions"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/socDiffAnnoy"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/break"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/friendMarrg"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/loneliness"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/observation/createStory"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/nonechoedLang"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/speechAbnorm"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/immedEcholalia"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/stIdiosyncWords"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/offInform"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/askInform"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/reportEvents"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/conversation"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA/descInformGest"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/unuslEyeCont"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/facExprOth"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/langProd"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/shareEnjoy"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/empathComm"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/insight"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/qualSocOverture"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/qualSocResp"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/amtRecipSocComm"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB/overallQualRapp"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC/imagCreativ"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/unuslSensInt"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/handFgrComplx"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/selfInjurBehav"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/excessInterstUnusl"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/compulRitual"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionE/overactAgit"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionE/tantrmAgress"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionE/anxiety"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<ados:ADOS2001Module3";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</ados:ADOS2001Module3>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Commtot!=null){
			xmlTxt+="\n<ados:commtot";
			xmlTxt+=">";
			xmlTxt+=this.Commtot;
			xmlTxt+="</ados:commtot>";
		}
		if (this.CommtotNote!=null){
			xmlTxt+="\n<ados:commtot_note";
			xmlTxt+=">";
			xmlTxt+=this.CommtotNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:commtot_note>";
		}
		if (this.Scintot!=null){
			xmlTxt+="\n<ados:scintot";
			xmlTxt+=">";
			xmlTxt+=this.Scintot;
			xmlTxt+="</ados:scintot>";
		}
		if (this.ScintotNote!=null){
			xmlTxt+="\n<ados:scintot_note";
			xmlTxt+=">";
			xmlTxt+=this.ScintotNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:scintot_note>";
		}
		if (this.Cmsitot!=null){
			xmlTxt+="\n<ados:cmsitot";
			xmlTxt+=">";
			xmlTxt+=this.Cmsitot;
			xmlTxt+="</ados:cmsitot>";
		}
		if (this.CmsitotNote!=null){
			xmlTxt+="\n<ados:cmsitot_note";
			xmlTxt+=">";
			xmlTxt+=this.CmsitotNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:cmsitot_note>";
		}
		if (this.Playtot!=null){
			xmlTxt+="\n<ados:playtot";
			xmlTxt+=">";
			xmlTxt+=this.Playtot;
			xmlTxt+="</ados:playtot>";
		}
		if (this.PlaytotNote!=null){
			xmlTxt+="\n<ados:playtot_note";
			xmlTxt+=">";
			xmlTxt+=this.PlaytotNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:playtot_note>";
		}
		if (this.Imaginetot!=null){
			xmlTxt+="\n<ados:imaginetot";
			xmlTxt+=">";
			xmlTxt+=this.Imaginetot;
			xmlTxt+="</ados:imaginetot>";
		}
		if (this.ImaginetotNote!=null){
			xmlTxt+="\n<ados:imaginetot_note";
			xmlTxt+=">";
			xmlTxt+=this.ImaginetotNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:imaginetot_note>";
		}
		if (this.Sbritot!=null){
			xmlTxt+="\n<ados:sbritot";
			xmlTxt+=">";
			xmlTxt+=this.Sbritot;
			xmlTxt+="</ados:sbritot>";
		}
		if (this.SbritotNote!=null){
			xmlTxt+="\n<ados:sbritot_note";
			xmlTxt+=">";
			xmlTxt+=this.SbritotNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:sbritot_note>";
		}
			var child0=0;
			var att0=0;
			if(this.Scoreform_observation_emotions!=null)
			child0++;
			if(this.Scoreform_sectionb_amtrecipsoccomm!=null)
			child0++;
			if(this.Scoreform_sectione_overactagit!=null)
			child0++;
			if(this.Scoreform_observation_demonst!=null)
			child0++;
			if(this.Scoreform_sectiona_stidiosyncwords!=null)
			child0++;
			if(this.Scoreform_diagnosis_overalldiag!=null)
			child0++;
			if(this.Scoreform_sectiona_conversation!=null)
			child0++;
			if(this.Scoreform_observation_break!=null)
			child0++;
			if(this.Scoreform_sectionb_overallqualrapp!=null)
			child0++;
			if(this.Scoreform_sectiona_speechabnorm!=null)
			child0++;
			if(this.Scoreform_sectionb_langprod!=null)
			child0++;
			if(this.Scoreform_sectiond_compulritual!=null)
			child0++;
			if(this.Scoreform_sectiond_selfinjurbehav!=null)
			child0++;
			if(this.Scoreform_sectionb_insight!=null)
			child0++;
			if(this.Scoreform_observation_friendmarrg!=null)
			child0++;
			if(this.Scoreform_sectionb_qualsocresp!=null)
			child0++;
			if(this.Scoreform_sectiond_excessinterstunusl!=null)
			child0++;
			if(this.Scoreform_observation_tellstory!=null)
			child0++;
			if(this.Scoreform_sectiond_unuslsensint!=null)
			child0++;
			if(this.Scoreform_diagnosis_adosclas!=null)
			child0++;
			if(this.Scoreform_sectionb_unusleyecont!=null)
			child0++;
			if(this.Scoreform_observation_socdiffannoy!=null)
			child0++;
			if(this.Scoreform_sectione_anxiety!=null)
			child0++;
			if(this.Scoreform_observation_constrtask!=null)
			child0++;
			if(this.Scoreform_sectionb_qualsocoverture!=null)
			child0++;
			if(this.Scoreform_sectiona_immedecholalia!=null)
			child0++;
			if(this.Scoreform_sectiona_offinform!=null)
			child0++;
			if(this.Scoreform_sectione_tantrmagress!=null)
			child0++;
			if(this.Scoreform_sectiona_askinform!=null)
			child0++;
			if(this.Scoreform_observation_descpicture!=null)
			child0++;
			if(this.Scoreform_observation_convsreport!=null)
			child0++;
			if(this.Scoreform_sectiona_nonechoedlang!=null)
			child0++;
			if(this.Scoreform_observation_loneliness!=null)
			child0++;
			if(this.Scoreform_sectionb_facexproth!=null)
			child0++;
			if(this.Scoreform_observation_createstory!=null)
			child0++;
			if(this.Scoreform_observation_cartoons!=null)
			child0++;
			if(this.Scoreform_sectionb_empathcomm!=null)
			child0++;
			if(this.Scoreform_sectiond_handfgrcomplx!=null)
			child0++;
			if(this.Scoreform_sectiona_descinformgest!=null)
			child0++;
			if(this.Scoreform_sectionc_imagcreativ!=null)
			child0++;
			if(this.Scoreform_sectionb_shareenjoy!=null)
			child0++;
			if(this.Scoreform_sectiona_reportevents!=null)
			child0++;
			if(this.Scoreform_observation_jointplay!=null)
			child0++;
			if(this.Scoreform_observation_makebelieve!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<ados:ScoreForm";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child1=0;
			var att1=0;
			if(this.Scoreform_diagnosis_overalldiag!=null)
			child1++;
			if(this.Scoreform_diagnosis_adosclas!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<ados:diagnosis";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_diagnosis_adosclas!=null){
			xmlTxt+="\n<ados:adosclas";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_diagnosis_adosclas.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:adosclas>";
		}
		if (this.Scoreform_diagnosis_overalldiag!=null){
			xmlTxt+="\n<ados:overallDiag";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_diagnosis_overalldiag.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:overallDiag>";
		}
				xmlTxt+="\n</ados:diagnosis>";
			}
			}

			var child2=0;
			var att2=0;
			if(this.Scoreform_observation_loneliness!=null)
			child2++;
			if(this.Scoreform_observation_friendmarrg!=null)
			child2++;
			if(this.Scoreform_observation_cartoons!=null)
			child2++;
			if(this.Scoreform_observation_jointplay!=null)
			child2++;
			if(this.Scoreform_observation_break!=null)
			child2++;
			if(this.Scoreform_observation_descpicture!=null)
			child2++;
			if(this.Scoreform_observation_convsreport!=null)
			child2++;
			if(this.Scoreform_observation_createstory!=null)
			child2++;
			if(this.Scoreform_observation_constrtask!=null)
			child2++;
			if(this.Scoreform_observation_socdiffannoy!=null)
			child2++;
			if(this.Scoreform_observation_makebelieve!=null)
			child2++;
			if(this.Scoreform_observation_tellstory!=null)
			child2++;
			if(this.Scoreform_observation_demonst!=null)
			child2++;
			if(this.Scoreform_observation_emotions!=null)
			child2++;
			if(child2>0 || att2>0){
				xmlTxt+="\n<ados:observation";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_observation_constrtask!=null){
			xmlTxt+="\n<ados:constrTask";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_constrtask.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:constrTask>";
		}
		if (this.Scoreform_observation_makebelieve!=null){
			xmlTxt+="\n<ados:makeBelieve";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_makebelieve.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:makeBelieve>";
		}
		if (this.Scoreform_observation_jointplay!=null){
			xmlTxt+="\n<ados:jointPlay";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_jointplay.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:jointPlay>";
		}
		if (this.Scoreform_observation_demonst!=null){
			xmlTxt+="\n<ados:demonst";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_demonst.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:demonst>";
		}
		if (this.Scoreform_observation_descpicture!=null){
			xmlTxt+="\n<ados:descPicture";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_descpicture.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:descPicture>";
		}
		if (this.Scoreform_observation_tellstory!=null){
			xmlTxt+="\n<ados:tellStory";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_tellstory.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:tellStory>";
		}
		if (this.Scoreform_observation_cartoons!=null){
			xmlTxt+="\n<ados:cartoons";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_cartoons.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:cartoons>";
		}
		if (this.Scoreform_observation_convsreport!=null){
			xmlTxt+="\n<ados:convsReport";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_convsreport.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:convsReport>";
		}
		if (this.Scoreform_observation_emotions!=null){
			xmlTxt+="\n<ados:emotions";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_emotions.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:emotions>";
		}
		if (this.Scoreform_observation_socdiffannoy!=null){
			xmlTxt+="\n<ados:socDiffAnnoy";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_socdiffannoy.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:socDiffAnnoy>";
		}
		if (this.Scoreform_observation_break!=null){
			xmlTxt+="\n<ados:break";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_break.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:break>";
		}
		if (this.Scoreform_observation_friendmarrg!=null){
			xmlTxt+="\n<ados:friendMarrg";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_friendmarrg.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:friendMarrg>";
		}
		if (this.Scoreform_observation_loneliness!=null){
			xmlTxt+="\n<ados:loneliness";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_loneliness.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:loneliness>";
		}
		if (this.Scoreform_observation_createstory!=null){
			xmlTxt+="\n<ados:createStory";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_observation_createstory.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ados:createStory>";
		}
				xmlTxt+="\n</ados:observation>";
			}
			}

			var child3=0;
			var att3=0;
			if(this.Scoreform_sectiona_descinformgest!=null)
			child3++;
			if(this.Scoreform_sectiona_nonechoedlang!=null)
			child3++;
			if(this.Scoreform_sectiona_offinform!=null)
			child3++;
			if(this.Scoreform_sectiona_speechabnorm!=null)
			child3++;
			if(this.Scoreform_sectiona_conversation!=null)
			child3++;
			if(this.Scoreform_sectiona_immedecholalia!=null)
			child3++;
			if(this.Scoreform_sectiona_stidiosyncwords!=null)
			child3++;
			if(this.Scoreform_sectiona_askinform!=null)
			child3++;
			if(this.Scoreform_sectiona_reportevents!=null)
			child3++;
			if(child3>0 || att3>0){
				xmlTxt+="\n<ados:sectionA";
			if(child3==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiona_nonechoedlang!=null){
			xmlTxt+="\n<ados:nonechoedLang";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_nonechoedlang;
			xmlTxt+="</ados:nonechoedLang>";
		}
		if (this.Scoreform_sectiona_speechabnorm!=null){
			xmlTxt+="\n<ados:speechAbnorm";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_speechabnorm;
			xmlTxt+="</ados:speechAbnorm>";
		}
		if (this.Scoreform_sectiona_immedecholalia!=null){
			xmlTxt+="\n<ados:immedEcholalia";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_immedecholalia;
			xmlTxt+="</ados:immedEcholalia>";
		}
		if (this.Scoreform_sectiona_stidiosyncwords!=null){
			xmlTxt+="\n<ados:stIdiosyncWords";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_stidiosyncwords;
			xmlTxt+="</ados:stIdiosyncWords>";
		}
		if (this.Scoreform_sectiona_offinform!=null){
			xmlTxt+="\n<ados:offInform";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_offinform;
			xmlTxt+="</ados:offInform>";
		}
		if (this.Scoreform_sectiona_askinform!=null){
			xmlTxt+="\n<ados:askInform";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_askinform;
			xmlTxt+="</ados:askInform>";
		}
		if (this.Scoreform_sectiona_reportevents!=null){
			xmlTxt+="\n<ados:reportEvents";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_reportevents;
			xmlTxt+="</ados:reportEvents>";
		}
		if (this.Scoreform_sectiona_conversation!=null){
			xmlTxt+="\n<ados:conversation";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_conversation;
			xmlTxt+="</ados:conversation>";
		}
		if (this.Scoreform_sectiona_descinformgest!=null){
			xmlTxt+="\n<ados:descInformGest";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona_descinformgest;
			xmlTxt+="</ados:descInformGest>";
		}
				xmlTxt+="\n</ados:sectionA>";
			}
			}

			var child4=0;
			var att4=0;
			if(this.Scoreform_sectionb_qualsocoverture!=null)
			child4++;
			if(this.Scoreform_sectionb_overallqualrapp!=null)
			child4++;
			if(this.Scoreform_sectionb_qualsocresp!=null)
			child4++;
			if(this.Scoreform_sectionb_insight!=null)
			child4++;
			if(this.Scoreform_sectionb_empathcomm!=null)
			child4++;
			if(this.Scoreform_sectionb_amtrecipsoccomm!=null)
			child4++;
			if(this.Scoreform_sectionb_shareenjoy!=null)
			child4++;
			if(this.Scoreform_sectionb_facexproth!=null)
			child4++;
			if(this.Scoreform_sectionb_unusleyecont!=null)
			child4++;
			if(this.Scoreform_sectionb_langprod!=null)
			child4++;
			if(child4>0 || att4>0){
				xmlTxt+="\n<ados:sectionB";
			if(child4==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionb_unusleyecont!=null){
			xmlTxt+="\n<ados:unuslEyeCont";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_unusleyecont;
			xmlTxt+="</ados:unuslEyeCont>";
		}
		if (this.Scoreform_sectionb_facexproth!=null){
			xmlTxt+="\n<ados:facExprOth";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_facexproth;
			xmlTxt+="</ados:facExprOth>";
		}
		if (this.Scoreform_sectionb_langprod!=null){
			xmlTxt+="\n<ados:langProd";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_langprod;
			xmlTxt+="</ados:langProd>";
		}
		if (this.Scoreform_sectionb_shareenjoy!=null){
			xmlTxt+="\n<ados:shareEnjoy";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_shareenjoy;
			xmlTxt+="</ados:shareEnjoy>";
		}
		if (this.Scoreform_sectionb_empathcomm!=null){
			xmlTxt+="\n<ados:empathComm";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_empathcomm;
			xmlTxt+="</ados:empathComm>";
		}
		if (this.Scoreform_sectionb_insight!=null){
			xmlTxt+="\n<ados:insight";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_insight;
			xmlTxt+="</ados:insight>";
		}
		if (this.Scoreform_sectionb_qualsocoverture!=null){
			xmlTxt+="\n<ados:qualSocOverture";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_qualsocoverture;
			xmlTxt+="</ados:qualSocOverture>";
		}
		if (this.Scoreform_sectionb_qualsocresp!=null){
			xmlTxt+="\n<ados:qualSocResp";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_qualsocresp;
			xmlTxt+="</ados:qualSocResp>";
		}
		if (this.Scoreform_sectionb_amtrecipsoccomm!=null){
			xmlTxt+="\n<ados:amtRecipSocComm";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_amtrecipsoccomm;
			xmlTxt+="</ados:amtRecipSocComm>";
		}
		if (this.Scoreform_sectionb_overallqualrapp!=null){
			xmlTxt+="\n<ados:overallQualRapp";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb_overallqualrapp;
			xmlTxt+="</ados:overallQualRapp>";
		}
				xmlTxt+="\n</ados:sectionB>";
			}
			}

			var child5=0;
			var att5=0;
			if(this.Scoreform_sectionc_imagcreativ!=null)
			child5++;
			if(child5>0 || att5>0){
				xmlTxt+="\n<ados:sectionC";
			if(child5==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionc_imagcreativ!=null){
			xmlTxt+="\n<ados:imagCreativ";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc_imagcreativ;
			xmlTxt+="</ados:imagCreativ>";
		}
				xmlTxt+="\n</ados:sectionC>";
			}
			}

			var child6=0;
			var att6=0;
			if(this.Scoreform_sectiond_selfinjurbehav!=null)
			child6++;
			if(this.Scoreform_sectiond_compulritual!=null)
			child6++;
			if(this.Scoreform_sectiond_handfgrcomplx!=null)
			child6++;
			if(this.Scoreform_sectiond_excessinterstunusl!=null)
			child6++;
			if(this.Scoreform_sectiond_unuslsensint!=null)
			child6++;
			if(child6>0 || att6>0){
				xmlTxt+="\n<ados:sectionD";
			if(child6==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiond_unuslsensint!=null){
			xmlTxt+="\n<ados:unuslSensInt";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_unuslsensint;
			xmlTxt+="</ados:unuslSensInt>";
		}
		if (this.Scoreform_sectiond_handfgrcomplx!=null){
			xmlTxt+="\n<ados:handFgrComplx";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_handfgrcomplx;
			xmlTxt+="</ados:handFgrComplx>";
		}
		if (this.Scoreform_sectiond_selfinjurbehav!=null){
			xmlTxt+="\n<ados:selfInjurBehav";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_selfinjurbehav;
			xmlTxt+="</ados:selfInjurBehav>";
		}
		if (this.Scoreform_sectiond_excessinterstunusl!=null){
			xmlTxt+="\n<ados:excessInterstUnusl";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_excessinterstunusl;
			xmlTxt+="</ados:excessInterstUnusl>";
		}
		if (this.Scoreform_sectiond_compulritual!=null){
			xmlTxt+="\n<ados:compulRitual";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_compulritual;
			xmlTxt+="</ados:compulRitual>";
		}
				xmlTxt+="\n</ados:sectionD>";
			}
			}

			var child7=0;
			var att7=0;
			if(this.Scoreform_sectione_tantrmagress!=null)
			child7++;
			if(this.Scoreform_sectione_overactagit!=null)
			child7++;
			if(this.Scoreform_sectione_anxiety!=null)
			child7++;
			if(child7>0 || att7>0){
				xmlTxt+="\n<ados:sectionE";
			if(child7==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectione_overactagit!=null){
			xmlTxt+="\n<ados:overactAgit";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectione_overactagit;
			xmlTxt+="</ados:overactAgit>";
		}
		if (this.Scoreform_sectione_tantrmagress!=null){
			xmlTxt+="\n<ados:tantrmAgress";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectione_tantrmagress;
			xmlTxt+="</ados:tantrmAgress>";
		}
		if (this.Scoreform_sectione_anxiety!=null){
			xmlTxt+="\n<ados:anxiety";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectione_anxiety;
			xmlTxt+="</ados:anxiety>";
		}
				xmlTxt+="\n</ados:sectionE>";
			}
			}

				xmlTxt+="\n</ados:ScoreForm>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Commtot!=null) return true;
		if (this.CommtotNote!=null) return true;
		if (this.Scintot!=null) return true;
		if (this.ScintotNote!=null) return true;
		if (this.Cmsitot!=null) return true;
		if (this.CmsitotNote!=null) return true;
		if (this.Playtot!=null) return true;
		if (this.PlaytotNote!=null) return true;
		if (this.Imaginetot!=null) return true;
		if (this.ImaginetotNote!=null) return true;
		if (this.Sbritot!=null) return true;
		if (this.SbritotNote!=null) return true;
			if(this.Scoreform_observation_emotions!=null) return true;
			if(this.Scoreform_sectionb_amtrecipsoccomm!=null) return true;
			if(this.Scoreform_sectione_overactagit!=null) return true;
			if(this.Scoreform_observation_demonst!=null) return true;
			if(this.Scoreform_sectiona_stidiosyncwords!=null) return true;
			if(this.Scoreform_diagnosis_overalldiag!=null) return true;
			if(this.Scoreform_sectiona_conversation!=null) return true;
			if(this.Scoreform_observation_break!=null) return true;
			if(this.Scoreform_sectionb_overallqualrapp!=null) return true;
			if(this.Scoreform_sectiona_speechabnorm!=null) return true;
			if(this.Scoreform_sectionb_langprod!=null) return true;
			if(this.Scoreform_sectiond_compulritual!=null) return true;
			if(this.Scoreform_sectiond_selfinjurbehav!=null) return true;
			if(this.Scoreform_sectionb_insight!=null) return true;
			if(this.Scoreform_observation_friendmarrg!=null) return true;
			if(this.Scoreform_sectionb_qualsocresp!=null) return true;
			if(this.Scoreform_sectiond_excessinterstunusl!=null) return true;
			if(this.Scoreform_observation_tellstory!=null) return true;
			if(this.Scoreform_sectiond_unuslsensint!=null) return true;
			if(this.Scoreform_diagnosis_adosclas!=null) return true;
			if(this.Scoreform_sectionb_unusleyecont!=null) return true;
			if(this.Scoreform_observation_socdiffannoy!=null) return true;
			if(this.Scoreform_sectione_anxiety!=null) return true;
			if(this.Scoreform_observation_constrtask!=null) return true;
			if(this.Scoreform_sectionb_qualsocoverture!=null) return true;
			if(this.Scoreform_sectiona_immedecholalia!=null) return true;
			if(this.Scoreform_sectiona_offinform!=null) return true;
			if(this.Scoreform_sectione_tantrmagress!=null) return true;
			if(this.Scoreform_sectiona_askinform!=null) return true;
			if(this.Scoreform_observation_descpicture!=null) return true;
			if(this.Scoreform_observation_convsreport!=null) return true;
			if(this.Scoreform_sectiona_nonechoedlang!=null) return true;
			if(this.Scoreform_observation_loneliness!=null) return true;
			if(this.Scoreform_sectionb_facexproth!=null) return true;
			if(this.Scoreform_observation_createstory!=null) return true;
			if(this.Scoreform_observation_cartoons!=null) return true;
			if(this.Scoreform_sectionb_empathcomm!=null) return true;
			if(this.Scoreform_sectiond_handfgrcomplx!=null) return true;
			if(this.Scoreform_sectiona_descinformgest!=null) return true;
			if(this.Scoreform_sectionc_imagcreativ!=null) return true;
			if(this.Scoreform_sectionb_shareenjoy!=null) return true;
			if(this.Scoreform_sectiona_reportevents!=null) return true;
			if(this.Scoreform_observation_jointplay!=null) return true;
			if(this.Scoreform_observation_makebelieve!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
