/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:04 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function uds_b4cdrData(){
this.xsiType="uds:b4cdrData";

	this.getSchemaElementName=function(){
		return "b4cdrData";
	}

	this.getFullSchemaElementName=function(){
		return "uds:b4cdrData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Initials=null;


	function getInitials() {
		return this.Initials;
	}
	this.getInitials=getInitials;


	function setInitials(v){
		this.Initials=v;
	}
	this.setInitials=setInitials;

	this.Memory=null;


	function getMemory() {
		return this.Memory;
	}
	this.getMemory=getMemory;


	function setMemory(v){
		this.Memory=v;
	}
	this.setMemory=setMemory;

	this.Orient=null;


	function getOrient() {
		return this.Orient;
	}
	this.getOrient=getOrient;


	function setOrient(v){
		this.Orient=v;
	}
	this.setOrient=setOrient;

	this.Judgment=null;


	function getJudgment() {
		return this.Judgment;
	}
	this.getJudgment=getJudgment;


	function setJudgment(v){
		this.Judgment=v;
	}
	this.setJudgment=setJudgment;

	this.Commun=null;


	function getCommun() {
		return this.Commun;
	}
	this.getCommun=getCommun;


	function setCommun(v){
		this.Commun=v;
	}
	this.setCommun=setCommun;

	this.Homehobb=null;


	function getHomehobb() {
		return this.Homehobb;
	}
	this.getHomehobb=getHomehobb;


	function setHomehobb(v){
		this.Homehobb=v;
	}
	this.setHomehobb=setHomehobb;

	this.Perscare=null;


	function getPerscare() {
		return this.Perscare;
	}
	this.getPerscare=getPerscare;


	function setPerscare(v){
		this.Perscare=v;
	}
	this.setPerscare=setPerscare;

	this.Cdrsum=null;


	function getCdrsum() {
		return this.Cdrsum;
	}
	this.getCdrsum=getCdrsum;


	function setCdrsum(v){
		this.Cdrsum=v;
	}
	this.setCdrsum=setCdrsum;

	this.Cdrglob=null;


	function getCdrglob() {
		return this.Cdrglob;
	}
	this.getCdrglob=getCdrglob;


	function setCdrglob(v){
		this.Cdrglob=v;
	}
	this.setCdrglob=setCdrglob;

	this.Comport=null;


	function getComport() {
		return this.Comport;
	}
	this.getComport=getComport;


	function setComport(v){
		this.Comport=v;
	}
	this.setComport=setComport;

	this.Cdrlang=null;


	function getCdrlang() {
		return this.Cdrlang;
	}
	this.getCdrlang=getCdrlang;


	function setCdrlang(v){
		this.Cdrlang=v;
	}
	this.setCdrlang=setCdrlang;

	this.Cdrsupp=null;


	function getCdrsupp() {
		return this.Cdrsupp;
	}
	this.getCdrsupp=getCdrsupp;


	function setCdrsupp(v){
		this.Cdrsupp=v;
	}
	this.setCdrsupp=setCdrsupp;

	this.Cdrtot=null;


	function getCdrtot() {
		return this.Cdrtot;
	}
	this.getCdrtot=getCdrtot;


	function setCdrtot(v){
		this.Cdrtot=v;
	}
	this.setCdrtot=setCdrtot;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="INITIALS"){
				return this.Initials ;
			} else 
			if(xmlPath=="MEMORY"){
				return this.Memory ;
			} else 
			if(xmlPath=="ORIENT"){
				return this.Orient ;
			} else 
			if(xmlPath=="JUDGMENT"){
				return this.Judgment ;
			} else 
			if(xmlPath=="COMMUN"){
				return this.Commun ;
			} else 
			if(xmlPath=="HOMEHOBB"){
				return this.Homehobb ;
			} else 
			if(xmlPath=="PERSCARE"){
				return this.Perscare ;
			} else 
			if(xmlPath=="CDRSUM"){
				return this.Cdrsum ;
			} else 
			if(xmlPath=="CDRGLOB"){
				return this.Cdrglob ;
			} else 
			if(xmlPath=="COMPORT"){
				return this.Comport ;
			} else 
			if(xmlPath=="CDRLANG"){
				return this.Cdrlang ;
			} else 
			if(xmlPath=="CDRSUPP"){
				return this.Cdrsupp ;
			} else 
			if(xmlPath=="CDRTOT"){
				return this.Cdrtot ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="INITIALS"){
				this.Initials=value;
			} else 
			if(xmlPath=="MEMORY"){
				this.Memory=value;
			} else 
			if(xmlPath=="ORIENT"){
				this.Orient=value;
			} else 
			if(xmlPath=="JUDGMENT"){
				this.Judgment=value;
			} else 
			if(xmlPath=="COMMUN"){
				this.Commun=value;
			} else 
			if(xmlPath=="HOMEHOBB"){
				this.Homehobb=value;
			} else 
			if(xmlPath=="PERSCARE"){
				this.Perscare=value;
			} else 
			if(xmlPath=="CDRSUM"){
				this.Cdrsum=value;
			} else 
			if(xmlPath=="CDRGLOB"){
				this.Cdrglob=value;
			} else 
			if(xmlPath=="COMPORT"){
				this.Comport=value;
			} else 
			if(xmlPath=="CDRLANG"){
				this.Cdrlang=value;
			} else 
			if(xmlPath=="CDRSUPP"){
				this.Cdrsupp=value;
			} else 
			if(xmlPath=="CDRTOT"){
				this.Cdrtot=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="INITIALS"){
			return "field_data";
		}else if (xmlPath=="MEMORY"){
			return "field_data";
		}else if (xmlPath=="ORIENT"){
			return "field_data";
		}else if (xmlPath=="JUDGMENT"){
			return "field_data";
		}else if (xmlPath=="COMMUN"){
			return "field_data";
		}else if (xmlPath=="HOMEHOBB"){
			return "field_data";
		}else if (xmlPath=="PERSCARE"){
			return "field_data";
		}else if (xmlPath=="CDRSUM"){
			return "field_data";
		}else if (xmlPath=="CDRGLOB"){
			return "field_data";
		}else if (xmlPath=="COMPORT"){
			return "field_data";
		}else if (xmlPath=="CDRLANG"){
			return "field_data";
		}else if (xmlPath=="CDRSUPP"){
			return "field_data";
		}else if (xmlPath=="CDRTOT"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<uds:B4CDR";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</uds:B4CDR>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Initials!=null){
			xmlTxt+="\n<uds:INITIALS";
			xmlTxt+=">";
			xmlTxt+=this.Initials.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:INITIALS>";
		}
		if (this.Memory!=null){
			xmlTxt+="\n<uds:MEMORY";
			xmlTxt+=">";
			xmlTxt+=this.Memory;
			xmlTxt+="</uds:MEMORY>";
		}
		if (this.Orient!=null){
			xmlTxt+="\n<uds:ORIENT";
			xmlTxt+=">";
			xmlTxt+=this.Orient;
			xmlTxt+="</uds:ORIENT>";
		}
		if (this.Judgment!=null){
			xmlTxt+="\n<uds:JUDGMENT";
			xmlTxt+=">";
			xmlTxt+=this.Judgment;
			xmlTxt+="</uds:JUDGMENT>";
		}
		if (this.Commun!=null){
			xmlTxt+="\n<uds:COMMUN";
			xmlTxt+=">";
			xmlTxt+=this.Commun;
			xmlTxt+="</uds:COMMUN>";
		}
		if (this.Homehobb!=null){
			xmlTxt+="\n<uds:HOMEHOBB";
			xmlTxt+=">";
			xmlTxt+=this.Homehobb;
			xmlTxt+="</uds:HOMEHOBB>";
		}
		if (this.Perscare!=null){
			xmlTxt+="\n<uds:PERSCARE";
			xmlTxt+=">";
			xmlTxt+=this.Perscare;
			xmlTxt+="</uds:PERSCARE>";
		}
		if (this.Cdrsum!=null){
			xmlTxt+="\n<uds:CDRSUM";
			xmlTxt+=">";
			xmlTxt+=this.Cdrsum;
			xmlTxt+="</uds:CDRSUM>";
		}
		if (this.Cdrglob!=null){
			xmlTxt+="\n<uds:CDRGLOB";
			xmlTxt+=">";
			xmlTxt+=this.Cdrglob;
			xmlTxt+="</uds:CDRGLOB>";
		}
		if (this.Comport!=null){
			xmlTxt+="\n<uds:COMPORT";
			xmlTxt+=">";
			xmlTxt+=this.Comport;
			xmlTxt+="</uds:COMPORT>";
		}
		if (this.Cdrlang!=null){
			xmlTxt+="\n<uds:CDRLANG";
			xmlTxt+=">";
			xmlTxt+=this.Cdrlang;
			xmlTxt+="</uds:CDRLANG>";
		}
		if (this.Cdrsupp!=null){
			xmlTxt+="\n<uds:CDRSUPP";
			xmlTxt+=">";
			xmlTxt+=this.Cdrsupp;
			xmlTxt+="</uds:CDRSUPP>";
		}
		if (this.Cdrtot!=null){
			xmlTxt+="\n<uds:CDRTOT";
			xmlTxt+=">";
			xmlTxt+=this.Cdrtot;
			xmlTxt+="</uds:CDRTOT>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Initials!=null) return true;
		if (this.Memory!=null) return true;
		if (this.Orient!=null) return true;
		if (this.Judgment!=null) return true;
		if (this.Commun!=null) return true;
		if (this.Homehobb!=null) return true;
		if (this.Perscare!=null) return true;
		if (this.Cdrsum!=null) return true;
		if (this.Cdrglob!=null) return true;
		if (this.Comport!=null) return true;
		if (this.Cdrlang!=null) return true;
		if (this.Cdrsupp!=null) return true;
		if (this.Cdrtot!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
