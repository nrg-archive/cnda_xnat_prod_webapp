/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function ipip_ipipptData(){
this.xsiType="ipip:ipipptData";

	this.getSchemaElementName=function(){
		return "ipipptData";
	}

	this.getFullSchemaElementName=function(){
		return "ipip:ipipptData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Source=null;


	function getSource() {
		return this.Source;
	}
	this.getSource=getSource;


	function setSource(v){
		this.Source=v;
	}
	this.setSource=setSource;

	this.Q1=null;


	function getQ1() {
		return this.Q1;
	}
	this.getQ1=getQ1;


	function setQ1(v){
		this.Q1=v;
	}
	this.setQ1=setQ1;

	this.Q2=null;


	function getQ2() {
		return this.Q2;
	}
	this.getQ2=getQ2;


	function setQ2(v){
		this.Q2=v;
	}
	this.setQ2=setQ2;

	this.Q3=null;


	function getQ3() {
		return this.Q3;
	}
	this.getQ3=getQ3;


	function setQ3(v){
		this.Q3=v;
	}
	this.setQ3=setQ3;

	this.Q4=null;


	function getQ4() {
		return this.Q4;
	}
	this.getQ4=getQ4;


	function setQ4(v){
		this.Q4=v;
	}
	this.setQ4=setQ4;

	this.Q5=null;


	function getQ5() {
		return this.Q5;
	}
	this.getQ5=getQ5;


	function setQ5(v){
		this.Q5=v;
	}
	this.setQ5=setQ5;

	this.Q6=null;


	function getQ6() {
		return this.Q6;
	}
	this.getQ6=getQ6;


	function setQ6(v){
		this.Q6=v;
	}
	this.setQ6=setQ6;

	this.Q7=null;


	function getQ7() {
		return this.Q7;
	}
	this.getQ7=getQ7;


	function setQ7(v){
		this.Q7=v;
	}
	this.setQ7=setQ7;

	this.Q8=null;


	function getQ8() {
		return this.Q8;
	}
	this.getQ8=getQ8;


	function setQ8(v){
		this.Q8=v;
	}
	this.setQ8=setQ8;

	this.Q9=null;


	function getQ9() {
		return this.Q9;
	}
	this.getQ9=getQ9;


	function setQ9(v){
		this.Q9=v;
	}
	this.setQ9=setQ9;

	this.Q10=null;


	function getQ10() {
		return this.Q10;
	}
	this.getQ10=getQ10;


	function setQ10(v){
		this.Q10=v;
	}
	this.setQ10=setQ10;

	this.Q11=null;


	function getQ11() {
		return this.Q11;
	}
	this.getQ11=getQ11;


	function setQ11(v){
		this.Q11=v;
	}
	this.setQ11=setQ11;

	this.Q12=null;


	function getQ12() {
		return this.Q12;
	}
	this.getQ12=getQ12;


	function setQ12(v){
		this.Q12=v;
	}
	this.setQ12=setQ12;

	this.Q13=null;


	function getQ13() {
		return this.Q13;
	}
	this.getQ13=getQ13;


	function setQ13(v){
		this.Q13=v;
	}
	this.setQ13=setQ13;

	this.Q14=null;


	function getQ14() {
		return this.Q14;
	}
	this.getQ14=getQ14;


	function setQ14(v){
		this.Q14=v;
	}
	this.setQ14=setQ14;

	this.Q15=null;


	function getQ15() {
		return this.Q15;
	}
	this.getQ15=getQ15;


	function setQ15(v){
		this.Q15=v;
	}
	this.setQ15=setQ15;

	this.Q16=null;


	function getQ16() {
		return this.Q16;
	}
	this.getQ16=getQ16;


	function setQ16(v){
		this.Q16=v;
	}
	this.setQ16=setQ16;

	this.Q17=null;


	function getQ17() {
		return this.Q17;
	}
	this.getQ17=getQ17;


	function setQ17(v){
		this.Q17=v;
	}
	this.setQ17=setQ17;

	this.Q18=null;


	function getQ18() {
		return this.Q18;
	}
	this.getQ18=getQ18;


	function setQ18(v){
		this.Q18=v;
	}
	this.setQ18=setQ18;

	this.Q19=null;


	function getQ19() {
		return this.Q19;
	}
	this.getQ19=getQ19;


	function setQ19(v){
		this.Q19=v;
	}
	this.setQ19=setQ19;

	this.Q20=null;


	function getQ20() {
		return this.Q20;
	}
	this.getQ20=getQ20;


	function setQ20(v){
		this.Q20=v;
	}
	this.setQ20=setQ20;

	this.Q21=null;


	function getQ21() {
		return this.Q21;
	}
	this.getQ21=getQ21;


	function setQ21(v){
		this.Q21=v;
	}
	this.setQ21=setQ21;

	this.Q22=null;


	function getQ22() {
		return this.Q22;
	}
	this.getQ22=getQ22;


	function setQ22(v){
		this.Q22=v;
	}
	this.setQ22=setQ22;

	this.Q23=null;


	function getQ23() {
		return this.Q23;
	}
	this.getQ23=getQ23;


	function setQ23(v){
		this.Q23=v;
	}
	this.setQ23=setQ23;

	this.Q24=null;


	function getQ24() {
		return this.Q24;
	}
	this.getQ24=getQ24;


	function setQ24(v){
		this.Q24=v;
	}
	this.setQ24=setQ24;

	this.Q25=null;


	function getQ25() {
		return this.Q25;
	}
	this.getQ25=getQ25;


	function setQ25(v){
		this.Q25=v;
	}
	this.setQ25=setQ25;

	this.Q26=null;


	function getQ26() {
		return this.Q26;
	}
	this.getQ26=getQ26;


	function setQ26(v){
		this.Q26=v;
	}
	this.setQ26=setQ26;

	this.Q27=null;


	function getQ27() {
		return this.Q27;
	}
	this.getQ27=getQ27;


	function setQ27(v){
		this.Q27=v;
	}
	this.setQ27=setQ27;

	this.Q28=null;


	function getQ28() {
		return this.Q28;
	}
	this.getQ28=getQ28;


	function setQ28(v){
		this.Q28=v;
	}
	this.setQ28=setQ28;

	this.Q29=null;


	function getQ29() {
		return this.Q29;
	}
	this.getQ29=getQ29;


	function setQ29(v){
		this.Q29=v;
	}
	this.setQ29=setQ29;

	this.Q30=null;


	function getQ30() {
		return this.Q30;
	}
	this.getQ30=getQ30;


	function setQ30(v){
		this.Q30=v;
	}
	this.setQ30=setQ30;

	this.Q31=null;


	function getQ31() {
		return this.Q31;
	}
	this.getQ31=getQ31;


	function setQ31(v){
		this.Q31=v;
	}
	this.setQ31=setQ31;

	this.Q32=null;


	function getQ32() {
		return this.Q32;
	}
	this.getQ32=getQ32;


	function setQ32(v){
		this.Q32=v;
	}
	this.setQ32=setQ32;

	this.Q33=null;


	function getQ33() {
		return this.Q33;
	}
	this.getQ33=getQ33;


	function setQ33(v){
		this.Q33=v;
	}
	this.setQ33=setQ33;

	this.Q34=null;


	function getQ34() {
		return this.Q34;
	}
	this.getQ34=getQ34;


	function setQ34(v){
		this.Q34=v;
	}
	this.setQ34=setQ34;

	this.Q35=null;


	function getQ35() {
		return this.Q35;
	}
	this.getQ35=getQ35;


	function setQ35(v){
		this.Q35=v;
	}
	this.setQ35=setQ35;

	this.Q36=null;


	function getQ36() {
		return this.Q36;
	}
	this.getQ36=getQ36;


	function setQ36(v){
		this.Q36=v;
	}
	this.setQ36=setQ36;

	this.Q37=null;


	function getQ37() {
		return this.Q37;
	}
	this.getQ37=getQ37;


	function setQ37(v){
		this.Q37=v;
	}
	this.setQ37=setQ37;

	this.Q38=null;


	function getQ38() {
		return this.Q38;
	}
	this.getQ38=getQ38;


	function setQ38(v){
		this.Q38=v;
	}
	this.setQ38=setQ38;

	this.Q39=null;


	function getQ39() {
		return this.Q39;
	}
	this.getQ39=getQ39;


	function setQ39(v){
		this.Q39=v;
	}
	this.setQ39=setQ39;

	this.Q40=null;


	function getQ40() {
		return this.Q40;
	}
	this.getQ40=getQ40;


	function setQ40(v){
		this.Q40=v;
	}
	this.setQ40=setQ40;

	this.Q41=null;


	function getQ41() {
		return this.Q41;
	}
	this.getQ41=getQ41;


	function setQ41(v){
		this.Q41=v;
	}
	this.setQ41=setQ41;

	this.Q42=null;


	function getQ42() {
		return this.Q42;
	}
	this.getQ42=getQ42;


	function setQ42(v){
		this.Q42=v;
	}
	this.setQ42=setQ42;

	this.Q43=null;


	function getQ43() {
		return this.Q43;
	}
	this.getQ43=getQ43;


	function setQ43(v){
		this.Q43=v;
	}
	this.setQ43=setQ43;

	this.Q44=null;


	function getQ44() {
		return this.Q44;
	}
	this.getQ44=getQ44;


	function setQ44(v){
		this.Q44=v;
	}
	this.setQ44=setQ44;

	this.Q45=null;


	function getQ45() {
		return this.Q45;
	}
	this.getQ45=getQ45;


	function setQ45(v){
		this.Q45=v;
	}
	this.setQ45=setQ45;

	this.Q46=null;


	function getQ46() {
		return this.Q46;
	}
	this.getQ46=getQ46;


	function setQ46(v){
		this.Q46=v;
	}
	this.setQ46=setQ46;

	this.Q47=null;


	function getQ47() {
		return this.Q47;
	}
	this.getQ47=getQ47;


	function setQ47(v){
		this.Q47=v;
	}
	this.setQ47=setQ47;

	this.Q48=null;


	function getQ48() {
		return this.Q48;
	}
	this.getQ48=getQ48;


	function setQ48(v){
		this.Q48=v;
	}
	this.setQ48=setQ48;

	this.Q49=null;


	function getQ49() {
		return this.Q49;
	}
	this.getQ49=getQ49;


	function setQ49(v){
		this.Q49=v;
	}
	this.setQ49=setQ49;

	this.Q50=null;


	function getQ50() {
		return this.Q50;
	}
	this.getQ50=getQ50;


	function setQ50(v){
		this.Q50=v;
	}
	this.setQ50=setQ50;

	this.Q51=null;


	function getQ51() {
		return this.Q51;
	}
	this.getQ51=getQ51;


	function setQ51(v){
		this.Q51=v;
	}
	this.setQ51=setQ51;

	this.Q52=null;


	function getQ52() {
		return this.Q52;
	}
	this.getQ52=getQ52;


	function setQ52(v){
		this.Q52=v;
	}
	this.setQ52=setQ52;

	this.Q53=null;


	function getQ53() {
		return this.Q53;
	}
	this.getQ53=getQ53;


	function setQ53(v){
		this.Q53=v;
	}
	this.setQ53=setQ53;

	this.Q54=null;


	function getQ54() {
		return this.Q54;
	}
	this.getQ54=getQ54;


	function setQ54(v){
		this.Q54=v;
	}
	this.setQ54=setQ54;

	this.Q55=null;


	function getQ55() {
		return this.Q55;
	}
	this.getQ55=getQ55;


	function setQ55(v){
		this.Q55=v;
	}
	this.setQ55=setQ55;

	this.Q56=null;


	function getQ56() {
		return this.Q56;
	}
	this.getQ56=getQ56;


	function setQ56(v){
		this.Q56=v;
	}
	this.setQ56=setQ56;

	this.Q57=null;


	function getQ57() {
		return this.Q57;
	}
	this.getQ57=getQ57;


	function setQ57(v){
		this.Q57=v;
	}
	this.setQ57=setQ57;

	this.Q58=null;


	function getQ58() {
		return this.Q58;
	}
	this.getQ58=getQ58;


	function setQ58(v){
		this.Q58=v;
	}
	this.setQ58=setQ58;

	this.Q59=null;


	function getQ59() {
		return this.Q59;
	}
	this.getQ59=getQ59;


	function setQ59(v){
		this.Q59=v;
	}
	this.setQ59=setQ59;

	this.Q60=null;


	function getQ60() {
		return this.Q60;
	}
	this.getQ60=getQ60;


	function setQ60(v){
		this.Q60=v;
	}
	this.setQ60=setQ60;

	this.Q61=null;


	function getQ61() {
		return this.Q61;
	}
	this.getQ61=getQ61;


	function setQ61(v){
		this.Q61=v;
	}
	this.setQ61=setQ61;

	this.Q62=null;


	function getQ62() {
		return this.Q62;
	}
	this.getQ62=getQ62;


	function setQ62(v){
		this.Q62=v;
	}
	this.setQ62=setQ62;

	this.Q63=null;


	function getQ63() {
		return this.Q63;
	}
	this.getQ63=getQ63;


	function setQ63(v){
		this.Q63=v;
	}
	this.setQ63=setQ63;

	this.Q64=null;


	function getQ64() {
		return this.Q64;
	}
	this.getQ64=getQ64;


	function setQ64(v){
		this.Q64=v;
	}
	this.setQ64=setQ64;

	this.Q65=null;


	function getQ65() {
		return this.Q65;
	}
	this.getQ65=getQ65;


	function setQ65(v){
		this.Q65=v;
	}
	this.setQ65=setQ65;

	this.Q66=null;


	function getQ66() {
		return this.Q66;
	}
	this.getQ66=getQ66;


	function setQ66(v){
		this.Q66=v;
	}
	this.setQ66=setQ66;

	this.Q67=null;


	function getQ67() {
		return this.Q67;
	}
	this.getQ67=getQ67;


	function setQ67(v){
		this.Q67=v;
	}
	this.setQ67=setQ67;

	this.Q68=null;


	function getQ68() {
		return this.Q68;
	}
	this.getQ68=getQ68;


	function setQ68(v){
		this.Q68=v;
	}
	this.setQ68=setQ68;

	this.Q69=null;


	function getQ69() {
		return this.Q69;
	}
	this.getQ69=getQ69;


	function setQ69(v){
		this.Q69=v;
	}
	this.setQ69=setQ69;

	this.Q70=null;


	function getQ70() {
		return this.Q70;
	}
	this.getQ70=getQ70;


	function setQ70(v){
		this.Q70=v;
	}
	this.setQ70=setQ70;

	this.Q71=null;


	function getQ71() {
		return this.Q71;
	}
	this.getQ71=getQ71;


	function setQ71(v){
		this.Q71=v;
	}
	this.setQ71=setQ71;

	this.Q72=null;


	function getQ72() {
		return this.Q72;
	}
	this.getQ72=getQ72;


	function setQ72(v){
		this.Q72=v;
	}
	this.setQ72=setQ72;

	this.Q73=null;


	function getQ73() {
		return this.Q73;
	}
	this.getQ73=getQ73;


	function setQ73(v){
		this.Q73=v;
	}
	this.setQ73=setQ73;

	this.Q74=null;


	function getQ74() {
		return this.Q74;
	}
	this.getQ74=getQ74;


	function setQ74(v){
		this.Q74=v;
	}
	this.setQ74=setQ74;

	this.Q75=null;


	function getQ75() {
		return this.Q75;
	}
	this.getQ75=getQ75;


	function setQ75(v){
		this.Q75=v;
	}
	this.setQ75=setQ75;

	this.Q76=null;


	function getQ76() {
		return this.Q76;
	}
	this.getQ76=getQ76;


	function setQ76(v){
		this.Q76=v;
	}
	this.setQ76=setQ76;

	this.Q77=null;


	function getQ77() {
		return this.Q77;
	}
	this.getQ77=getQ77;


	function setQ77(v){
		this.Q77=v;
	}
	this.setQ77=setQ77;

	this.Q78=null;


	function getQ78() {
		return this.Q78;
	}
	this.getQ78=getQ78;


	function setQ78(v){
		this.Q78=v;
	}
	this.setQ78=setQ78;

	this.Q79=null;


	function getQ79() {
		return this.Q79;
	}
	this.getQ79=getQ79;


	function setQ79(v){
		this.Q79=v;
	}
	this.setQ79=setQ79;

	this.Q80=null;


	function getQ80() {
		return this.Q80;
	}
	this.getQ80=getQ80;


	function setQ80(v){
		this.Q80=v;
	}
	this.setQ80=setQ80;

	this.Q81=null;


	function getQ81() {
		return this.Q81;
	}
	this.getQ81=getQ81;


	function setQ81(v){
		this.Q81=v;
	}
	this.setQ81=setQ81;

	this.Q82=null;


	function getQ82() {
		return this.Q82;
	}
	this.getQ82=getQ82;


	function setQ82(v){
		this.Q82=v;
	}
	this.setQ82=setQ82;

	this.Q83=null;


	function getQ83() {
		return this.Q83;
	}
	this.getQ83=getQ83;


	function setQ83(v){
		this.Q83=v;
	}
	this.setQ83=setQ83;

	this.Q84=null;


	function getQ84() {
		return this.Q84;
	}
	this.getQ84=getQ84;


	function setQ84(v){
		this.Q84=v;
	}
	this.setQ84=setQ84;

	this.Q85=null;


	function getQ85() {
		return this.Q85;
	}
	this.getQ85=getQ85;


	function setQ85(v){
		this.Q85=v;
	}
	this.setQ85=setQ85;

	this.Q86=null;


	function getQ86() {
		return this.Q86;
	}
	this.getQ86=getQ86;


	function setQ86(v){
		this.Q86=v;
	}
	this.setQ86=setQ86;

	this.Q87=null;


	function getQ87() {
		return this.Q87;
	}
	this.getQ87=getQ87;


	function setQ87(v){
		this.Q87=v;
	}
	this.setQ87=setQ87;

	this.Q88=null;


	function getQ88() {
		return this.Q88;
	}
	this.getQ88=getQ88;


	function setQ88(v){
		this.Q88=v;
	}
	this.setQ88=setQ88;

	this.Q89=null;


	function getQ89() {
		return this.Q89;
	}
	this.getQ89=getQ89;


	function setQ89(v){
		this.Q89=v;
	}
	this.setQ89=setQ89;

	this.Q90=null;


	function getQ90() {
		return this.Q90;
	}
	this.getQ90=getQ90;


	function setQ90(v){
		this.Q90=v;
	}
	this.setQ90=setQ90;

	this.Q91=null;


	function getQ91() {
		return this.Q91;
	}
	this.getQ91=getQ91;


	function setQ91(v){
		this.Q91=v;
	}
	this.setQ91=setQ91;

	this.Q92=null;


	function getQ92() {
		return this.Q92;
	}
	this.getQ92=getQ92;


	function setQ92(v){
		this.Q92=v;
	}
	this.setQ92=setQ92;

	this.Q93=null;


	function getQ93() {
		return this.Q93;
	}
	this.getQ93=getQ93;


	function setQ93(v){
		this.Q93=v;
	}
	this.setQ93=setQ93;

	this.Q94=null;


	function getQ94() {
		return this.Q94;
	}
	this.getQ94=getQ94;


	function setQ94(v){
		this.Q94=v;
	}
	this.setQ94=setQ94;

	this.Q95=null;


	function getQ95() {
		return this.Q95;
	}
	this.getQ95=getQ95;


	function setQ95(v){
		this.Q95=v;
	}
	this.setQ95=setQ95;

	this.Q96=null;


	function getQ96() {
		return this.Q96;
	}
	this.getQ96=getQ96;


	function setQ96(v){
		this.Q96=v;
	}
	this.setQ96=setQ96;

	this.Q97=null;


	function getQ97() {
		return this.Q97;
	}
	this.getQ97=getQ97;


	function setQ97(v){
		this.Q97=v;
	}
	this.setQ97=setQ97;

	this.Q98=null;


	function getQ98() {
		return this.Q98;
	}
	this.getQ98=getQ98;


	function setQ98(v){
		this.Q98=v;
	}
	this.setQ98=setQ98;

	this.Q99=null;


	function getQ99() {
		return this.Q99;
	}
	this.getQ99=getQ99;


	function setQ99(v){
		this.Q99=v;
	}
	this.setQ99=setQ99;

	this.Q100=null;


	function getQ100() {
		return this.Q100;
	}
	this.getQ100=getQ100;


	function setQ100(v){
		this.Q100=v;
	}
	this.setQ100=setQ100;

	this.Q101=null;


	function getQ101() {
		return this.Q101;
	}
	this.getQ101=getQ101;


	function setQ101(v){
		this.Q101=v;
	}
	this.setQ101=setQ101;

	this.Q102=null;


	function getQ102() {
		return this.Q102;
	}
	this.getQ102=getQ102;


	function setQ102(v){
		this.Q102=v;
	}
	this.setQ102=setQ102;

	this.Q103=null;


	function getQ103() {
		return this.Q103;
	}
	this.getQ103=getQ103;


	function setQ103(v){
		this.Q103=v;
	}
	this.setQ103=setQ103;

	this.Q104=null;


	function getQ104() {
		return this.Q104;
	}
	this.getQ104=getQ104;


	function setQ104(v){
		this.Q104=v;
	}
	this.setQ104=setQ104;

	this.Q105=null;


	function getQ105() {
		return this.Q105;
	}
	this.getQ105=getQ105;


	function setQ105(v){
		this.Q105=v;
	}
	this.setQ105=setQ105;

	this.Q106=null;


	function getQ106() {
		return this.Q106;
	}
	this.getQ106=getQ106;


	function setQ106(v){
		this.Q106=v;
	}
	this.setQ106=setQ106;

	this.Q107=null;


	function getQ107() {
		return this.Q107;
	}
	this.getQ107=getQ107;


	function setQ107(v){
		this.Q107=v;
	}
	this.setQ107=setQ107;

	this.Q108=null;


	function getQ108() {
		return this.Q108;
	}
	this.getQ108=getQ108;


	function setQ108(v){
		this.Q108=v;
	}
	this.setQ108=setQ108;

	this.Q109=null;


	function getQ109() {
		return this.Q109;
	}
	this.getQ109=getQ109;


	function setQ109(v){
		this.Q109=v;
	}
	this.setQ109=setQ109;

	this.Q110=null;


	function getQ110() {
		return this.Q110;
	}
	this.getQ110=getQ110;


	function setQ110(v){
		this.Q110=v;
	}
	this.setQ110=setQ110;

	this.Q111=null;


	function getQ111() {
		return this.Q111;
	}
	this.getQ111=getQ111;


	function setQ111(v){
		this.Q111=v;
	}
	this.setQ111=setQ111;

	this.Q112=null;


	function getQ112() {
		return this.Q112;
	}
	this.getQ112=getQ112;


	function setQ112(v){
		this.Q112=v;
	}
	this.setQ112=setQ112;

	this.Q113=null;


	function getQ113() {
		return this.Q113;
	}
	this.getQ113=getQ113;


	function setQ113(v){
		this.Q113=v;
	}
	this.setQ113=setQ113;

	this.Q114=null;


	function getQ114() {
		return this.Q114;
	}
	this.getQ114=getQ114;


	function setQ114(v){
		this.Q114=v;
	}
	this.setQ114=setQ114;

	this.Q115=null;


	function getQ115() {
		return this.Q115;
	}
	this.getQ115=getQ115;


	function setQ115(v){
		this.Q115=v;
	}
	this.setQ115=setQ115;

	this.Q116=null;


	function getQ116() {
		return this.Q116;
	}
	this.getQ116=getQ116;


	function setQ116(v){
		this.Q116=v;
	}
	this.setQ116=setQ116;

	this.Q117=null;


	function getQ117() {
		return this.Q117;
	}
	this.getQ117=getQ117;


	function setQ117(v){
		this.Q117=v;
	}
	this.setQ117=setQ117;

	this.Q118=null;


	function getQ118() {
		return this.Q118;
	}
	this.getQ118=getQ118;


	function setQ118(v){
		this.Q118=v;
	}
	this.setQ118=setQ118;

	this.Q119=null;


	function getQ119() {
		return this.Q119;
	}
	this.getQ119=getQ119;


	function setQ119(v){
		this.Q119=v;
	}
	this.setQ119=setQ119;

	this.Q120=null;


	function getQ120() {
		return this.Q120;
	}
	this.getQ120=getQ120;


	function setQ120(v){
		this.Q120=v;
	}
	this.setQ120=setQ120;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="source"){
				return this.Source ;
			} else 
			if(xmlPath=="Q1"){
				return this.Q1 ;
			} else 
			if(xmlPath=="Q2"){
				return this.Q2 ;
			} else 
			if(xmlPath=="Q3"){
				return this.Q3 ;
			} else 
			if(xmlPath=="Q4"){
				return this.Q4 ;
			} else 
			if(xmlPath=="Q5"){
				return this.Q5 ;
			} else 
			if(xmlPath=="Q6"){
				return this.Q6 ;
			} else 
			if(xmlPath=="Q7"){
				return this.Q7 ;
			} else 
			if(xmlPath=="Q8"){
				return this.Q8 ;
			} else 
			if(xmlPath=="Q9"){
				return this.Q9 ;
			} else 
			if(xmlPath=="Q10"){
				return this.Q10 ;
			} else 
			if(xmlPath=="Q11"){
				return this.Q11 ;
			} else 
			if(xmlPath=="Q12"){
				return this.Q12 ;
			} else 
			if(xmlPath=="Q13"){
				return this.Q13 ;
			} else 
			if(xmlPath=="Q14"){
				return this.Q14 ;
			} else 
			if(xmlPath=="Q15"){
				return this.Q15 ;
			} else 
			if(xmlPath=="Q16"){
				return this.Q16 ;
			} else 
			if(xmlPath=="Q17"){
				return this.Q17 ;
			} else 
			if(xmlPath=="Q18"){
				return this.Q18 ;
			} else 
			if(xmlPath=="Q19"){
				return this.Q19 ;
			} else 
			if(xmlPath=="Q20"){
				return this.Q20 ;
			} else 
			if(xmlPath=="Q21"){
				return this.Q21 ;
			} else 
			if(xmlPath=="Q22"){
				return this.Q22 ;
			} else 
			if(xmlPath=="Q23"){
				return this.Q23 ;
			} else 
			if(xmlPath=="Q24"){
				return this.Q24 ;
			} else 
			if(xmlPath=="Q25"){
				return this.Q25 ;
			} else 
			if(xmlPath=="Q26"){
				return this.Q26 ;
			} else 
			if(xmlPath=="Q27"){
				return this.Q27 ;
			} else 
			if(xmlPath=="Q28"){
				return this.Q28 ;
			} else 
			if(xmlPath=="Q29"){
				return this.Q29 ;
			} else 
			if(xmlPath=="Q30"){
				return this.Q30 ;
			} else 
			if(xmlPath=="Q31"){
				return this.Q31 ;
			} else 
			if(xmlPath=="Q32"){
				return this.Q32 ;
			} else 
			if(xmlPath=="Q33"){
				return this.Q33 ;
			} else 
			if(xmlPath=="Q34"){
				return this.Q34 ;
			} else 
			if(xmlPath=="Q35"){
				return this.Q35 ;
			} else 
			if(xmlPath=="Q36"){
				return this.Q36 ;
			} else 
			if(xmlPath=="Q37"){
				return this.Q37 ;
			} else 
			if(xmlPath=="Q38"){
				return this.Q38 ;
			} else 
			if(xmlPath=="Q39"){
				return this.Q39 ;
			} else 
			if(xmlPath=="Q40"){
				return this.Q40 ;
			} else 
			if(xmlPath=="Q41"){
				return this.Q41 ;
			} else 
			if(xmlPath=="Q42"){
				return this.Q42 ;
			} else 
			if(xmlPath=="Q43"){
				return this.Q43 ;
			} else 
			if(xmlPath=="Q44"){
				return this.Q44 ;
			} else 
			if(xmlPath=="Q45"){
				return this.Q45 ;
			} else 
			if(xmlPath=="Q46"){
				return this.Q46 ;
			} else 
			if(xmlPath=="Q47"){
				return this.Q47 ;
			} else 
			if(xmlPath=="Q48"){
				return this.Q48 ;
			} else 
			if(xmlPath=="Q49"){
				return this.Q49 ;
			} else 
			if(xmlPath=="Q50"){
				return this.Q50 ;
			} else 
			if(xmlPath=="Q51"){
				return this.Q51 ;
			} else 
			if(xmlPath=="Q52"){
				return this.Q52 ;
			} else 
			if(xmlPath=="Q53"){
				return this.Q53 ;
			} else 
			if(xmlPath=="Q54"){
				return this.Q54 ;
			} else 
			if(xmlPath=="Q55"){
				return this.Q55 ;
			} else 
			if(xmlPath=="Q56"){
				return this.Q56 ;
			} else 
			if(xmlPath=="Q57"){
				return this.Q57 ;
			} else 
			if(xmlPath=="Q58"){
				return this.Q58 ;
			} else 
			if(xmlPath=="Q59"){
				return this.Q59 ;
			} else 
			if(xmlPath=="Q60"){
				return this.Q60 ;
			} else 
			if(xmlPath=="Q61"){
				return this.Q61 ;
			} else 
			if(xmlPath=="Q62"){
				return this.Q62 ;
			} else 
			if(xmlPath=="Q63"){
				return this.Q63 ;
			} else 
			if(xmlPath=="Q64"){
				return this.Q64 ;
			} else 
			if(xmlPath=="Q65"){
				return this.Q65 ;
			} else 
			if(xmlPath=="Q66"){
				return this.Q66 ;
			} else 
			if(xmlPath=="Q67"){
				return this.Q67 ;
			} else 
			if(xmlPath=="Q68"){
				return this.Q68 ;
			} else 
			if(xmlPath=="Q69"){
				return this.Q69 ;
			} else 
			if(xmlPath=="Q70"){
				return this.Q70 ;
			} else 
			if(xmlPath=="Q71"){
				return this.Q71 ;
			} else 
			if(xmlPath=="Q72"){
				return this.Q72 ;
			} else 
			if(xmlPath=="Q73"){
				return this.Q73 ;
			} else 
			if(xmlPath=="Q74"){
				return this.Q74 ;
			} else 
			if(xmlPath=="Q75"){
				return this.Q75 ;
			} else 
			if(xmlPath=="Q76"){
				return this.Q76 ;
			} else 
			if(xmlPath=="Q77"){
				return this.Q77 ;
			} else 
			if(xmlPath=="Q78"){
				return this.Q78 ;
			} else 
			if(xmlPath=="Q79"){
				return this.Q79 ;
			} else 
			if(xmlPath=="Q80"){
				return this.Q80 ;
			} else 
			if(xmlPath=="Q81"){
				return this.Q81 ;
			} else 
			if(xmlPath=="Q82"){
				return this.Q82 ;
			} else 
			if(xmlPath=="Q83"){
				return this.Q83 ;
			} else 
			if(xmlPath=="Q84"){
				return this.Q84 ;
			} else 
			if(xmlPath=="Q85"){
				return this.Q85 ;
			} else 
			if(xmlPath=="Q86"){
				return this.Q86 ;
			} else 
			if(xmlPath=="Q87"){
				return this.Q87 ;
			} else 
			if(xmlPath=="Q88"){
				return this.Q88 ;
			} else 
			if(xmlPath=="Q89"){
				return this.Q89 ;
			} else 
			if(xmlPath=="Q90"){
				return this.Q90 ;
			} else 
			if(xmlPath=="Q91"){
				return this.Q91 ;
			} else 
			if(xmlPath=="Q92"){
				return this.Q92 ;
			} else 
			if(xmlPath=="Q93"){
				return this.Q93 ;
			} else 
			if(xmlPath=="Q94"){
				return this.Q94 ;
			} else 
			if(xmlPath=="Q95"){
				return this.Q95 ;
			} else 
			if(xmlPath=="Q96"){
				return this.Q96 ;
			} else 
			if(xmlPath=="Q97"){
				return this.Q97 ;
			} else 
			if(xmlPath=="Q98"){
				return this.Q98 ;
			} else 
			if(xmlPath=="Q99"){
				return this.Q99 ;
			} else 
			if(xmlPath=="Q100"){
				return this.Q100 ;
			} else 
			if(xmlPath=="Q101"){
				return this.Q101 ;
			} else 
			if(xmlPath=="Q102"){
				return this.Q102 ;
			} else 
			if(xmlPath=="Q103"){
				return this.Q103 ;
			} else 
			if(xmlPath=="Q104"){
				return this.Q104 ;
			} else 
			if(xmlPath=="Q105"){
				return this.Q105 ;
			} else 
			if(xmlPath=="Q106"){
				return this.Q106 ;
			} else 
			if(xmlPath=="Q107"){
				return this.Q107 ;
			} else 
			if(xmlPath=="Q108"){
				return this.Q108 ;
			} else 
			if(xmlPath=="Q109"){
				return this.Q109 ;
			} else 
			if(xmlPath=="Q110"){
				return this.Q110 ;
			} else 
			if(xmlPath=="Q111"){
				return this.Q111 ;
			} else 
			if(xmlPath=="Q112"){
				return this.Q112 ;
			} else 
			if(xmlPath=="Q113"){
				return this.Q113 ;
			} else 
			if(xmlPath=="Q114"){
				return this.Q114 ;
			} else 
			if(xmlPath=="Q115"){
				return this.Q115 ;
			} else 
			if(xmlPath=="Q116"){
				return this.Q116 ;
			} else 
			if(xmlPath=="Q117"){
				return this.Q117 ;
			} else 
			if(xmlPath=="Q118"){
				return this.Q118 ;
			} else 
			if(xmlPath=="Q119"){
				return this.Q119 ;
			} else 
			if(xmlPath=="Q120"){
				return this.Q120 ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="source"){
				this.Source=value;
			} else 
			if(xmlPath=="Q1"){
				this.Q1=value;
			} else 
			if(xmlPath=="Q2"){
				this.Q2=value;
			} else 
			if(xmlPath=="Q3"){
				this.Q3=value;
			} else 
			if(xmlPath=="Q4"){
				this.Q4=value;
			} else 
			if(xmlPath=="Q5"){
				this.Q5=value;
			} else 
			if(xmlPath=="Q6"){
				this.Q6=value;
			} else 
			if(xmlPath=="Q7"){
				this.Q7=value;
			} else 
			if(xmlPath=="Q8"){
				this.Q8=value;
			} else 
			if(xmlPath=="Q9"){
				this.Q9=value;
			} else 
			if(xmlPath=="Q10"){
				this.Q10=value;
			} else 
			if(xmlPath=="Q11"){
				this.Q11=value;
			} else 
			if(xmlPath=="Q12"){
				this.Q12=value;
			} else 
			if(xmlPath=="Q13"){
				this.Q13=value;
			} else 
			if(xmlPath=="Q14"){
				this.Q14=value;
			} else 
			if(xmlPath=="Q15"){
				this.Q15=value;
			} else 
			if(xmlPath=="Q16"){
				this.Q16=value;
			} else 
			if(xmlPath=="Q17"){
				this.Q17=value;
			} else 
			if(xmlPath=="Q18"){
				this.Q18=value;
			} else 
			if(xmlPath=="Q19"){
				this.Q19=value;
			} else 
			if(xmlPath=="Q20"){
				this.Q20=value;
			} else 
			if(xmlPath=="Q21"){
				this.Q21=value;
			} else 
			if(xmlPath=="Q22"){
				this.Q22=value;
			} else 
			if(xmlPath=="Q23"){
				this.Q23=value;
			} else 
			if(xmlPath=="Q24"){
				this.Q24=value;
			} else 
			if(xmlPath=="Q25"){
				this.Q25=value;
			} else 
			if(xmlPath=="Q26"){
				this.Q26=value;
			} else 
			if(xmlPath=="Q27"){
				this.Q27=value;
			} else 
			if(xmlPath=="Q28"){
				this.Q28=value;
			} else 
			if(xmlPath=="Q29"){
				this.Q29=value;
			} else 
			if(xmlPath=="Q30"){
				this.Q30=value;
			} else 
			if(xmlPath=="Q31"){
				this.Q31=value;
			} else 
			if(xmlPath=="Q32"){
				this.Q32=value;
			} else 
			if(xmlPath=="Q33"){
				this.Q33=value;
			} else 
			if(xmlPath=="Q34"){
				this.Q34=value;
			} else 
			if(xmlPath=="Q35"){
				this.Q35=value;
			} else 
			if(xmlPath=="Q36"){
				this.Q36=value;
			} else 
			if(xmlPath=="Q37"){
				this.Q37=value;
			} else 
			if(xmlPath=="Q38"){
				this.Q38=value;
			} else 
			if(xmlPath=="Q39"){
				this.Q39=value;
			} else 
			if(xmlPath=="Q40"){
				this.Q40=value;
			} else 
			if(xmlPath=="Q41"){
				this.Q41=value;
			} else 
			if(xmlPath=="Q42"){
				this.Q42=value;
			} else 
			if(xmlPath=="Q43"){
				this.Q43=value;
			} else 
			if(xmlPath=="Q44"){
				this.Q44=value;
			} else 
			if(xmlPath=="Q45"){
				this.Q45=value;
			} else 
			if(xmlPath=="Q46"){
				this.Q46=value;
			} else 
			if(xmlPath=="Q47"){
				this.Q47=value;
			} else 
			if(xmlPath=="Q48"){
				this.Q48=value;
			} else 
			if(xmlPath=="Q49"){
				this.Q49=value;
			} else 
			if(xmlPath=="Q50"){
				this.Q50=value;
			} else 
			if(xmlPath=="Q51"){
				this.Q51=value;
			} else 
			if(xmlPath=="Q52"){
				this.Q52=value;
			} else 
			if(xmlPath=="Q53"){
				this.Q53=value;
			} else 
			if(xmlPath=="Q54"){
				this.Q54=value;
			} else 
			if(xmlPath=="Q55"){
				this.Q55=value;
			} else 
			if(xmlPath=="Q56"){
				this.Q56=value;
			} else 
			if(xmlPath=="Q57"){
				this.Q57=value;
			} else 
			if(xmlPath=="Q58"){
				this.Q58=value;
			} else 
			if(xmlPath=="Q59"){
				this.Q59=value;
			} else 
			if(xmlPath=="Q60"){
				this.Q60=value;
			} else 
			if(xmlPath=="Q61"){
				this.Q61=value;
			} else 
			if(xmlPath=="Q62"){
				this.Q62=value;
			} else 
			if(xmlPath=="Q63"){
				this.Q63=value;
			} else 
			if(xmlPath=="Q64"){
				this.Q64=value;
			} else 
			if(xmlPath=="Q65"){
				this.Q65=value;
			} else 
			if(xmlPath=="Q66"){
				this.Q66=value;
			} else 
			if(xmlPath=="Q67"){
				this.Q67=value;
			} else 
			if(xmlPath=="Q68"){
				this.Q68=value;
			} else 
			if(xmlPath=="Q69"){
				this.Q69=value;
			} else 
			if(xmlPath=="Q70"){
				this.Q70=value;
			} else 
			if(xmlPath=="Q71"){
				this.Q71=value;
			} else 
			if(xmlPath=="Q72"){
				this.Q72=value;
			} else 
			if(xmlPath=="Q73"){
				this.Q73=value;
			} else 
			if(xmlPath=="Q74"){
				this.Q74=value;
			} else 
			if(xmlPath=="Q75"){
				this.Q75=value;
			} else 
			if(xmlPath=="Q76"){
				this.Q76=value;
			} else 
			if(xmlPath=="Q77"){
				this.Q77=value;
			} else 
			if(xmlPath=="Q78"){
				this.Q78=value;
			} else 
			if(xmlPath=="Q79"){
				this.Q79=value;
			} else 
			if(xmlPath=="Q80"){
				this.Q80=value;
			} else 
			if(xmlPath=="Q81"){
				this.Q81=value;
			} else 
			if(xmlPath=="Q82"){
				this.Q82=value;
			} else 
			if(xmlPath=="Q83"){
				this.Q83=value;
			} else 
			if(xmlPath=="Q84"){
				this.Q84=value;
			} else 
			if(xmlPath=="Q85"){
				this.Q85=value;
			} else 
			if(xmlPath=="Q86"){
				this.Q86=value;
			} else 
			if(xmlPath=="Q87"){
				this.Q87=value;
			} else 
			if(xmlPath=="Q88"){
				this.Q88=value;
			} else 
			if(xmlPath=="Q89"){
				this.Q89=value;
			} else 
			if(xmlPath=="Q90"){
				this.Q90=value;
			} else 
			if(xmlPath=="Q91"){
				this.Q91=value;
			} else 
			if(xmlPath=="Q92"){
				this.Q92=value;
			} else 
			if(xmlPath=="Q93"){
				this.Q93=value;
			} else 
			if(xmlPath=="Q94"){
				this.Q94=value;
			} else 
			if(xmlPath=="Q95"){
				this.Q95=value;
			} else 
			if(xmlPath=="Q96"){
				this.Q96=value;
			} else 
			if(xmlPath=="Q97"){
				this.Q97=value;
			} else 
			if(xmlPath=="Q98"){
				this.Q98=value;
			} else 
			if(xmlPath=="Q99"){
				this.Q99=value;
			} else 
			if(xmlPath=="Q100"){
				this.Q100=value;
			} else 
			if(xmlPath=="Q101"){
				this.Q101=value;
			} else 
			if(xmlPath=="Q102"){
				this.Q102=value;
			} else 
			if(xmlPath=="Q103"){
				this.Q103=value;
			} else 
			if(xmlPath=="Q104"){
				this.Q104=value;
			} else 
			if(xmlPath=="Q105"){
				this.Q105=value;
			} else 
			if(xmlPath=="Q106"){
				this.Q106=value;
			} else 
			if(xmlPath=="Q107"){
				this.Q107=value;
			} else 
			if(xmlPath=="Q108"){
				this.Q108=value;
			} else 
			if(xmlPath=="Q109"){
				this.Q109=value;
			} else 
			if(xmlPath=="Q110"){
				this.Q110=value;
			} else 
			if(xmlPath=="Q111"){
				this.Q111=value;
			} else 
			if(xmlPath=="Q112"){
				this.Q112=value;
			} else 
			if(xmlPath=="Q113"){
				this.Q113=value;
			} else 
			if(xmlPath=="Q114"){
				this.Q114=value;
			} else 
			if(xmlPath=="Q115"){
				this.Q115=value;
			} else 
			if(xmlPath=="Q116"){
				this.Q116=value;
			} else 
			if(xmlPath=="Q117"){
				this.Q117=value;
			} else 
			if(xmlPath=="Q118"){
				this.Q118=value;
			} else 
			if(xmlPath=="Q119"){
				this.Q119=value;
			} else 
			if(xmlPath=="Q120"){
				this.Q120=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="source"){
			return "field_data";
		}else if (xmlPath=="Q1"){
			return "field_data";
		}else if (xmlPath=="Q2"){
			return "field_data";
		}else if (xmlPath=="Q3"){
			return "field_data";
		}else if (xmlPath=="Q4"){
			return "field_data";
		}else if (xmlPath=="Q5"){
			return "field_data";
		}else if (xmlPath=="Q6"){
			return "field_data";
		}else if (xmlPath=="Q7"){
			return "field_data";
		}else if (xmlPath=="Q8"){
			return "field_data";
		}else if (xmlPath=="Q9"){
			return "field_data";
		}else if (xmlPath=="Q10"){
			return "field_data";
		}else if (xmlPath=="Q11"){
			return "field_data";
		}else if (xmlPath=="Q12"){
			return "field_data";
		}else if (xmlPath=="Q13"){
			return "field_data";
		}else if (xmlPath=="Q14"){
			return "field_data";
		}else if (xmlPath=="Q15"){
			return "field_data";
		}else if (xmlPath=="Q16"){
			return "field_data";
		}else if (xmlPath=="Q17"){
			return "field_data";
		}else if (xmlPath=="Q18"){
			return "field_data";
		}else if (xmlPath=="Q19"){
			return "field_data";
		}else if (xmlPath=="Q20"){
			return "field_data";
		}else if (xmlPath=="Q21"){
			return "field_data";
		}else if (xmlPath=="Q22"){
			return "field_data";
		}else if (xmlPath=="Q23"){
			return "field_data";
		}else if (xmlPath=="Q24"){
			return "field_data";
		}else if (xmlPath=="Q25"){
			return "field_data";
		}else if (xmlPath=="Q26"){
			return "field_data";
		}else if (xmlPath=="Q27"){
			return "field_data";
		}else if (xmlPath=="Q28"){
			return "field_data";
		}else if (xmlPath=="Q29"){
			return "field_data";
		}else if (xmlPath=="Q30"){
			return "field_data";
		}else if (xmlPath=="Q31"){
			return "field_data";
		}else if (xmlPath=="Q32"){
			return "field_data";
		}else if (xmlPath=="Q33"){
			return "field_data";
		}else if (xmlPath=="Q34"){
			return "field_data";
		}else if (xmlPath=="Q35"){
			return "field_data";
		}else if (xmlPath=="Q36"){
			return "field_data";
		}else if (xmlPath=="Q37"){
			return "field_data";
		}else if (xmlPath=="Q38"){
			return "field_data";
		}else if (xmlPath=="Q39"){
			return "field_data";
		}else if (xmlPath=="Q40"){
			return "field_data";
		}else if (xmlPath=="Q41"){
			return "field_data";
		}else if (xmlPath=="Q42"){
			return "field_data";
		}else if (xmlPath=="Q43"){
			return "field_data";
		}else if (xmlPath=="Q44"){
			return "field_data";
		}else if (xmlPath=="Q45"){
			return "field_data";
		}else if (xmlPath=="Q46"){
			return "field_data";
		}else if (xmlPath=="Q47"){
			return "field_data";
		}else if (xmlPath=="Q48"){
			return "field_data";
		}else if (xmlPath=="Q49"){
			return "field_data";
		}else if (xmlPath=="Q50"){
			return "field_data";
		}else if (xmlPath=="Q51"){
			return "field_data";
		}else if (xmlPath=="Q52"){
			return "field_data";
		}else if (xmlPath=="Q53"){
			return "field_data";
		}else if (xmlPath=="Q54"){
			return "field_data";
		}else if (xmlPath=="Q55"){
			return "field_data";
		}else if (xmlPath=="Q56"){
			return "field_data";
		}else if (xmlPath=="Q57"){
			return "field_data";
		}else if (xmlPath=="Q58"){
			return "field_data";
		}else if (xmlPath=="Q59"){
			return "field_data";
		}else if (xmlPath=="Q60"){
			return "field_data";
		}else if (xmlPath=="Q61"){
			return "field_data";
		}else if (xmlPath=="Q62"){
			return "field_data";
		}else if (xmlPath=="Q63"){
			return "field_data";
		}else if (xmlPath=="Q64"){
			return "field_data";
		}else if (xmlPath=="Q65"){
			return "field_data";
		}else if (xmlPath=="Q66"){
			return "field_data";
		}else if (xmlPath=="Q67"){
			return "field_data";
		}else if (xmlPath=="Q68"){
			return "field_data";
		}else if (xmlPath=="Q69"){
			return "field_data";
		}else if (xmlPath=="Q70"){
			return "field_data";
		}else if (xmlPath=="Q71"){
			return "field_data";
		}else if (xmlPath=="Q72"){
			return "field_data";
		}else if (xmlPath=="Q73"){
			return "field_data";
		}else if (xmlPath=="Q74"){
			return "field_data";
		}else if (xmlPath=="Q75"){
			return "field_data";
		}else if (xmlPath=="Q76"){
			return "field_data";
		}else if (xmlPath=="Q77"){
			return "field_data";
		}else if (xmlPath=="Q78"){
			return "field_data";
		}else if (xmlPath=="Q79"){
			return "field_data";
		}else if (xmlPath=="Q80"){
			return "field_data";
		}else if (xmlPath=="Q81"){
			return "field_data";
		}else if (xmlPath=="Q82"){
			return "field_data";
		}else if (xmlPath=="Q83"){
			return "field_data";
		}else if (xmlPath=="Q84"){
			return "field_data";
		}else if (xmlPath=="Q85"){
			return "field_data";
		}else if (xmlPath=="Q86"){
			return "field_data";
		}else if (xmlPath=="Q87"){
			return "field_data";
		}else if (xmlPath=="Q88"){
			return "field_data";
		}else if (xmlPath=="Q89"){
			return "field_data";
		}else if (xmlPath=="Q90"){
			return "field_data";
		}else if (xmlPath=="Q91"){
			return "field_data";
		}else if (xmlPath=="Q92"){
			return "field_data";
		}else if (xmlPath=="Q93"){
			return "field_data";
		}else if (xmlPath=="Q94"){
			return "field_data";
		}else if (xmlPath=="Q95"){
			return "field_data";
		}else if (xmlPath=="Q96"){
			return "field_data";
		}else if (xmlPath=="Q97"){
			return "field_data";
		}else if (xmlPath=="Q98"){
			return "field_data";
		}else if (xmlPath=="Q99"){
			return "field_data";
		}else if (xmlPath=="Q100"){
			return "field_data";
		}else if (xmlPath=="Q101"){
			return "field_data";
		}else if (xmlPath=="Q102"){
			return "field_data";
		}else if (xmlPath=="Q103"){
			return "field_data";
		}else if (xmlPath=="Q104"){
			return "field_data";
		}else if (xmlPath=="Q105"){
			return "field_data";
		}else if (xmlPath=="Q106"){
			return "field_data";
		}else if (xmlPath=="Q107"){
			return "field_data";
		}else if (xmlPath=="Q108"){
			return "field_data";
		}else if (xmlPath=="Q109"){
			return "field_data";
		}else if (xmlPath=="Q110"){
			return "field_data";
		}else if (xmlPath=="Q111"){
			return "field_data";
		}else if (xmlPath=="Q112"){
			return "field_data";
		}else if (xmlPath=="Q113"){
			return "field_data";
		}else if (xmlPath=="Q114"){
			return "field_data";
		}else if (xmlPath=="Q115"){
			return "field_data";
		}else if (xmlPath=="Q116"){
			return "field_data";
		}else if (xmlPath=="Q117"){
			return "field_data";
		}else if (xmlPath=="Q118"){
			return "field_data";
		}else if (xmlPath=="Q119"){
			return "field_data";
		}else if (xmlPath=="Q120"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<ipip:IPIPPT";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</ipip:IPIPPT>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Source!=null){
			xmlTxt+="\n<ipip:source";
			xmlTxt+=">";
			xmlTxt+=this.Source.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ipip:source>";
		}
		if (this.Q1!=null){
			xmlTxt+="\n<ipip:Q1";
			xmlTxt+=">";
			xmlTxt+=this.Q1;
			xmlTxt+="</ipip:Q1>";
		}
		if (this.Q2!=null){
			xmlTxt+="\n<ipip:Q2";
			xmlTxt+=">";
			xmlTxt+=this.Q2;
			xmlTxt+="</ipip:Q2>";
		}
		if (this.Q3!=null){
			xmlTxt+="\n<ipip:Q3";
			xmlTxt+=">";
			xmlTxt+=this.Q3;
			xmlTxt+="</ipip:Q3>";
		}
		if (this.Q4!=null){
			xmlTxt+="\n<ipip:Q4";
			xmlTxt+=">";
			xmlTxt+=this.Q4;
			xmlTxt+="</ipip:Q4>";
		}
		if (this.Q5!=null){
			xmlTxt+="\n<ipip:Q5";
			xmlTxt+=">";
			xmlTxt+=this.Q5;
			xmlTxt+="</ipip:Q5>";
		}
		if (this.Q6!=null){
			xmlTxt+="\n<ipip:Q6";
			xmlTxt+=">";
			xmlTxt+=this.Q6;
			xmlTxt+="</ipip:Q6>";
		}
		if (this.Q7!=null){
			xmlTxt+="\n<ipip:Q7";
			xmlTxt+=">";
			xmlTxt+=this.Q7;
			xmlTxt+="</ipip:Q7>";
		}
		if (this.Q8!=null){
			xmlTxt+="\n<ipip:Q8";
			xmlTxt+=">";
			xmlTxt+=this.Q8;
			xmlTxt+="</ipip:Q8>";
		}
		if (this.Q9!=null){
			xmlTxt+="\n<ipip:Q9";
			xmlTxt+=">";
			xmlTxt+=this.Q9;
			xmlTxt+="</ipip:Q9>";
		}
		if (this.Q10!=null){
			xmlTxt+="\n<ipip:Q10";
			xmlTxt+=">";
			xmlTxt+=this.Q10;
			xmlTxt+="</ipip:Q10>";
		}
		if (this.Q11!=null){
			xmlTxt+="\n<ipip:Q11";
			xmlTxt+=">";
			xmlTxt+=this.Q11;
			xmlTxt+="</ipip:Q11>";
		}
		if (this.Q12!=null){
			xmlTxt+="\n<ipip:Q12";
			xmlTxt+=">";
			xmlTxt+=this.Q12;
			xmlTxt+="</ipip:Q12>";
		}
		if (this.Q13!=null){
			xmlTxt+="\n<ipip:Q13";
			xmlTxt+=">";
			xmlTxt+=this.Q13;
			xmlTxt+="</ipip:Q13>";
		}
		if (this.Q14!=null){
			xmlTxt+="\n<ipip:Q14";
			xmlTxt+=">";
			xmlTxt+=this.Q14;
			xmlTxt+="</ipip:Q14>";
		}
		if (this.Q15!=null){
			xmlTxt+="\n<ipip:Q15";
			xmlTxt+=">";
			xmlTxt+=this.Q15;
			xmlTxt+="</ipip:Q15>";
		}
		if (this.Q16!=null){
			xmlTxt+="\n<ipip:Q16";
			xmlTxt+=">";
			xmlTxt+=this.Q16;
			xmlTxt+="</ipip:Q16>";
		}
		if (this.Q17!=null){
			xmlTxt+="\n<ipip:Q17";
			xmlTxt+=">";
			xmlTxt+=this.Q17;
			xmlTxt+="</ipip:Q17>";
		}
		if (this.Q18!=null){
			xmlTxt+="\n<ipip:Q18";
			xmlTxt+=">";
			xmlTxt+=this.Q18;
			xmlTxt+="</ipip:Q18>";
		}
		if (this.Q19!=null){
			xmlTxt+="\n<ipip:Q19";
			xmlTxt+=">";
			xmlTxt+=this.Q19;
			xmlTxt+="</ipip:Q19>";
		}
		if (this.Q20!=null){
			xmlTxt+="\n<ipip:Q20";
			xmlTxt+=">";
			xmlTxt+=this.Q20;
			xmlTxt+="</ipip:Q20>";
		}
		if (this.Q21!=null){
			xmlTxt+="\n<ipip:Q21";
			xmlTxt+=">";
			xmlTxt+=this.Q21;
			xmlTxt+="</ipip:Q21>";
		}
		if (this.Q22!=null){
			xmlTxt+="\n<ipip:Q22";
			xmlTxt+=">";
			xmlTxt+=this.Q22;
			xmlTxt+="</ipip:Q22>";
		}
		if (this.Q23!=null){
			xmlTxt+="\n<ipip:Q23";
			xmlTxt+=">";
			xmlTxt+=this.Q23;
			xmlTxt+="</ipip:Q23>";
		}
		if (this.Q24!=null){
			xmlTxt+="\n<ipip:Q24";
			xmlTxt+=">";
			xmlTxt+=this.Q24;
			xmlTxt+="</ipip:Q24>";
		}
		if (this.Q25!=null){
			xmlTxt+="\n<ipip:Q25";
			xmlTxt+=">";
			xmlTxt+=this.Q25;
			xmlTxt+="</ipip:Q25>";
		}
		if (this.Q26!=null){
			xmlTxt+="\n<ipip:Q26";
			xmlTxt+=">";
			xmlTxt+=this.Q26;
			xmlTxt+="</ipip:Q26>";
		}
		if (this.Q27!=null){
			xmlTxt+="\n<ipip:Q27";
			xmlTxt+=">";
			xmlTxt+=this.Q27;
			xmlTxt+="</ipip:Q27>";
		}
		if (this.Q28!=null){
			xmlTxt+="\n<ipip:Q28";
			xmlTxt+=">";
			xmlTxt+=this.Q28;
			xmlTxt+="</ipip:Q28>";
		}
		if (this.Q29!=null){
			xmlTxt+="\n<ipip:Q29";
			xmlTxt+=">";
			xmlTxt+=this.Q29;
			xmlTxt+="</ipip:Q29>";
		}
		if (this.Q30!=null){
			xmlTxt+="\n<ipip:Q30";
			xmlTxt+=">";
			xmlTxt+=this.Q30;
			xmlTxt+="</ipip:Q30>";
		}
		if (this.Q31!=null){
			xmlTxt+="\n<ipip:Q31";
			xmlTxt+=">";
			xmlTxt+=this.Q31;
			xmlTxt+="</ipip:Q31>";
		}
		if (this.Q32!=null){
			xmlTxt+="\n<ipip:Q32";
			xmlTxt+=">";
			xmlTxt+=this.Q32;
			xmlTxt+="</ipip:Q32>";
		}
		if (this.Q33!=null){
			xmlTxt+="\n<ipip:Q33";
			xmlTxt+=">";
			xmlTxt+=this.Q33;
			xmlTxt+="</ipip:Q33>";
		}
		if (this.Q34!=null){
			xmlTxt+="\n<ipip:Q34";
			xmlTxt+=">";
			xmlTxt+=this.Q34;
			xmlTxt+="</ipip:Q34>";
		}
		if (this.Q35!=null){
			xmlTxt+="\n<ipip:Q35";
			xmlTxt+=">";
			xmlTxt+=this.Q35;
			xmlTxt+="</ipip:Q35>";
		}
		if (this.Q36!=null){
			xmlTxt+="\n<ipip:Q36";
			xmlTxt+=">";
			xmlTxt+=this.Q36;
			xmlTxt+="</ipip:Q36>";
		}
		if (this.Q37!=null){
			xmlTxt+="\n<ipip:Q37";
			xmlTxt+=">";
			xmlTxt+=this.Q37;
			xmlTxt+="</ipip:Q37>";
		}
		if (this.Q38!=null){
			xmlTxt+="\n<ipip:Q38";
			xmlTxt+=">";
			xmlTxt+=this.Q38;
			xmlTxt+="</ipip:Q38>";
		}
		if (this.Q39!=null){
			xmlTxt+="\n<ipip:Q39";
			xmlTxt+=">";
			xmlTxt+=this.Q39;
			xmlTxt+="</ipip:Q39>";
		}
		if (this.Q40!=null){
			xmlTxt+="\n<ipip:Q40";
			xmlTxt+=">";
			xmlTxt+=this.Q40;
			xmlTxt+="</ipip:Q40>";
		}
		if (this.Q41!=null){
			xmlTxt+="\n<ipip:Q41";
			xmlTxt+=">";
			xmlTxt+=this.Q41;
			xmlTxt+="</ipip:Q41>";
		}
		if (this.Q42!=null){
			xmlTxt+="\n<ipip:Q42";
			xmlTxt+=">";
			xmlTxt+=this.Q42;
			xmlTxt+="</ipip:Q42>";
		}
		if (this.Q43!=null){
			xmlTxt+="\n<ipip:Q43";
			xmlTxt+=">";
			xmlTxt+=this.Q43;
			xmlTxt+="</ipip:Q43>";
		}
		if (this.Q44!=null){
			xmlTxt+="\n<ipip:Q44";
			xmlTxt+=">";
			xmlTxt+=this.Q44;
			xmlTxt+="</ipip:Q44>";
		}
		if (this.Q45!=null){
			xmlTxt+="\n<ipip:Q45";
			xmlTxt+=">";
			xmlTxt+=this.Q45;
			xmlTxt+="</ipip:Q45>";
		}
		if (this.Q46!=null){
			xmlTxt+="\n<ipip:Q46";
			xmlTxt+=">";
			xmlTxt+=this.Q46;
			xmlTxt+="</ipip:Q46>";
		}
		if (this.Q47!=null){
			xmlTxt+="\n<ipip:Q47";
			xmlTxt+=">";
			xmlTxt+=this.Q47;
			xmlTxt+="</ipip:Q47>";
		}
		if (this.Q48!=null){
			xmlTxt+="\n<ipip:Q48";
			xmlTxt+=">";
			xmlTxt+=this.Q48;
			xmlTxt+="</ipip:Q48>";
		}
		if (this.Q49!=null){
			xmlTxt+="\n<ipip:Q49";
			xmlTxt+=">";
			xmlTxt+=this.Q49;
			xmlTxt+="</ipip:Q49>";
		}
		if (this.Q50!=null){
			xmlTxt+="\n<ipip:Q50";
			xmlTxt+=">";
			xmlTxt+=this.Q50;
			xmlTxt+="</ipip:Q50>";
		}
		if (this.Q51!=null){
			xmlTxt+="\n<ipip:Q51";
			xmlTxt+=">";
			xmlTxt+=this.Q51;
			xmlTxt+="</ipip:Q51>";
		}
		if (this.Q52!=null){
			xmlTxt+="\n<ipip:Q52";
			xmlTxt+=">";
			xmlTxt+=this.Q52;
			xmlTxt+="</ipip:Q52>";
		}
		if (this.Q53!=null){
			xmlTxt+="\n<ipip:Q53";
			xmlTxt+=">";
			xmlTxt+=this.Q53;
			xmlTxt+="</ipip:Q53>";
		}
		if (this.Q54!=null){
			xmlTxt+="\n<ipip:Q54";
			xmlTxt+=">";
			xmlTxt+=this.Q54;
			xmlTxt+="</ipip:Q54>";
		}
		if (this.Q55!=null){
			xmlTxt+="\n<ipip:Q55";
			xmlTxt+=">";
			xmlTxt+=this.Q55;
			xmlTxt+="</ipip:Q55>";
		}
		if (this.Q56!=null){
			xmlTxt+="\n<ipip:Q56";
			xmlTxt+=">";
			xmlTxt+=this.Q56;
			xmlTxt+="</ipip:Q56>";
		}
		if (this.Q57!=null){
			xmlTxt+="\n<ipip:Q57";
			xmlTxt+=">";
			xmlTxt+=this.Q57;
			xmlTxt+="</ipip:Q57>";
		}
		if (this.Q58!=null){
			xmlTxt+="\n<ipip:Q58";
			xmlTxt+=">";
			xmlTxt+=this.Q58;
			xmlTxt+="</ipip:Q58>";
		}
		if (this.Q59!=null){
			xmlTxt+="\n<ipip:Q59";
			xmlTxt+=">";
			xmlTxt+=this.Q59;
			xmlTxt+="</ipip:Q59>";
		}
		if (this.Q60!=null){
			xmlTxt+="\n<ipip:Q60";
			xmlTxt+=">";
			xmlTxt+=this.Q60;
			xmlTxt+="</ipip:Q60>";
		}
		if (this.Q61!=null){
			xmlTxt+="\n<ipip:Q61";
			xmlTxt+=">";
			xmlTxt+=this.Q61;
			xmlTxt+="</ipip:Q61>";
		}
		if (this.Q62!=null){
			xmlTxt+="\n<ipip:Q62";
			xmlTxt+=">";
			xmlTxt+=this.Q62;
			xmlTxt+="</ipip:Q62>";
		}
		if (this.Q63!=null){
			xmlTxt+="\n<ipip:Q63";
			xmlTxt+=">";
			xmlTxt+=this.Q63;
			xmlTxt+="</ipip:Q63>";
		}
		if (this.Q64!=null){
			xmlTxt+="\n<ipip:Q64";
			xmlTxt+=">";
			xmlTxt+=this.Q64;
			xmlTxt+="</ipip:Q64>";
		}
		if (this.Q65!=null){
			xmlTxt+="\n<ipip:Q65";
			xmlTxt+=">";
			xmlTxt+=this.Q65;
			xmlTxt+="</ipip:Q65>";
		}
		if (this.Q66!=null){
			xmlTxt+="\n<ipip:Q66";
			xmlTxt+=">";
			xmlTxt+=this.Q66;
			xmlTxt+="</ipip:Q66>";
		}
		if (this.Q67!=null){
			xmlTxt+="\n<ipip:Q67";
			xmlTxt+=">";
			xmlTxt+=this.Q67;
			xmlTxt+="</ipip:Q67>";
		}
		if (this.Q68!=null){
			xmlTxt+="\n<ipip:Q68";
			xmlTxt+=">";
			xmlTxt+=this.Q68;
			xmlTxt+="</ipip:Q68>";
		}
		if (this.Q69!=null){
			xmlTxt+="\n<ipip:Q69";
			xmlTxt+=">";
			xmlTxt+=this.Q69;
			xmlTxt+="</ipip:Q69>";
		}
		if (this.Q70!=null){
			xmlTxt+="\n<ipip:Q70";
			xmlTxt+=">";
			xmlTxt+=this.Q70;
			xmlTxt+="</ipip:Q70>";
		}
		if (this.Q71!=null){
			xmlTxt+="\n<ipip:Q71";
			xmlTxt+=">";
			xmlTxt+=this.Q71;
			xmlTxt+="</ipip:Q71>";
		}
		if (this.Q72!=null){
			xmlTxt+="\n<ipip:Q72";
			xmlTxt+=">";
			xmlTxt+=this.Q72;
			xmlTxt+="</ipip:Q72>";
		}
		if (this.Q73!=null){
			xmlTxt+="\n<ipip:Q73";
			xmlTxt+=">";
			xmlTxt+=this.Q73;
			xmlTxt+="</ipip:Q73>";
		}
		if (this.Q74!=null){
			xmlTxt+="\n<ipip:Q74";
			xmlTxt+=">";
			xmlTxt+=this.Q74;
			xmlTxt+="</ipip:Q74>";
		}
		if (this.Q75!=null){
			xmlTxt+="\n<ipip:Q75";
			xmlTxt+=">";
			xmlTxt+=this.Q75;
			xmlTxt+="</ipip:Q75>";
		}
		if (this.Q76!=null){
			xmlTxt+="\n<ipip:Q76";
			xmlTxt+=">";
			xmlTxt+=this.Q76;
			xmlTxt+="</ipip:Q76>";
		}
		if (this.Q77!=null){
			xmlTxt+="\n<ipip:Q77";
			xmlTxt+=">";
			xmlTxt+=this.Q77;
			xmlTxt+="</ipip:Q77>";
		}
		if (this.Q78!=null){
			xmlTxt+="\n<ipip:Q78";
			xmlTxt+=">";
			xmlTxt+=this.Q78;
			xmlTxt+="</ipip:Q78>";
		}
		if (this.Q79!=null){
			xmlTxt+="\n<ipip:Q79";
			xmlTxt+=">";
			xmlTxt+=this.Q79;
			xmlTxt+="</ipip:Q79>";
		}
		if (this.Q80!=null){
			xmlTxt+="\n<ipip:Q80";
			xmlTxt+=">";
			xmlTxt+=this.Q80;
			xmlTxt+="</ipip:Q80>";
		}
		if (this.Q81!=null){
			xmlTxt+="\n<ipip:Q81";
			xmlTxt+=">";
			xmlTxt+=this.Q81;
			xmlTxt+="</ipip:Q81>";
		}
		if (this.Q82!=null){
			xmlTxt+="\n<ipip:Q82";
			xmlTxt+=">";
			xmlTxt+=this.Q82;
			xmlTxt+="</ipip:Q82>";
		}
		if (this.Q83!=null){
			xmlTxt+="\n<ipip:Q83";
			xmlTxt+=">";
			xmlTxt+=this.Q83;
			xmlTxt+="</ipip:Q83>";
		}
		if (this.Q84!=null){
			xmlTxt+="\n<ipip:Q84";
			xmlTxt+=">";
			xmlTxt+=this.Q84;
			xmlTxt+="</ipip:Q84>";
		}
		if (this.Q85!=null){
			xmlTxt+="\n<ipip:Q85";
			xmlTxt+=">";
			xmlTxt+=this.Q85;
			xmlTxt+="</ipip:Q85>";
		}
		if (this.Q86!=null){
			xmlTxt+="\n<ipip:Q86";
			xmlTxt+=">";
			xmlTxt+=this.Q86;
			xmlTxt+="</ipip:Q86>";
		}
		if (this.Q87!=null){
			xmlTxt+="\n<ipip:Q87";
			xmlTxt+=">";
			xmlTxt+=this.Q87;
			xmlTxt+="</ipip:Q87>";
		}
		if (this.Q88!=null){
			xmlTxt+="\n<ipip:Q88";
			xmlTxt+=">";
			xmlTxt+=this.Q88;
			xmlTxt+="</ipip:Q88>";
		}
		if (this.Q89!=null){
			xmlTxt+="\n<ipip:Q89";
			xmlTxt+=">";
			xmlTxt+=this.Q89;
			xmlTxt+="</ipip:Q89>";
		}
		if (this.Q90!=null){
			xmlTxt+="\n<ipip:Q90";
			xmlTxt+=">";
			xmlTxt+=this.Q90;
			xmlTxt+="</ipip:Q90>";
		}
		if (this.Q91!=null){
			xmlTxt+="\n<ipip:Q91";
			xmlTxt+=">";
			xmlTxt+=this.Q91;
			xmlTxt+="</ipip:Q91>";
		}
		if (this.Q92!=null){
			xmlTxt+="\n<ipip:Q92";
			xmlTxt+=">";
			xmlTxt+=this.Q92;
			xmlTxt+="</ipip:Q92>";
		}
		if (this.Q93!=null){
			xmlTxt+="\n<ipip:Q93";
			xmlTxt+=">";
			xmlTxt+=this.Q93;
			xmlTxt+="</ipip:Q93>";
		}
		if (this.Q94!=null){
			xmlTxt+="\n<ipip:Q94";
			xmlTxt+=">";
			xmlTxt+=this.Q94;
			xmlTxt+="</ipip:Q94>";
		}
		if (this.Q95!=null){
			xmlTxt+="\n<ipip:Q95";
			xmlTxt+=">";
			xmlTxt+=this.Q95;
			xmlTxt+="</ipip:Q95>";
		}
		if (this.Q96!=null){
			xmlTxt+="\n<ipip:Q96";
			xmlTxt+=">";
			xmlTxt+=this.Q96;
			xmlTxt+="</ipip:Q96>";
		}
		if (this.Q97!=null){
			xmlTxt+="\n<ipip:Q97";
			xmlTxt+=">";
			xmlTxt+=this.Q97;
			xmlTxt+="</ipip:Q97>";
		}
		if (this.Q98!=null){
			xmlTxt+="\n<ipip:Q98";
			xmlTxt+=">";
			xmlTxt+=this.Q98;
			xmlTxt+="</ipip:Q98>";
		}
		if (this.Q99!=null){
			xmlTxt+="\n<ipip:Q99";
			xmlTxt+=">";
			xmlTxt+=this.Q99;
			xmlTxt+="</ipip:Q99>";
		}
		if (this.Q100!=null){
			xmlTxt+="\n<ipip:Q100";
			xmlTxt+=">";
			xmlTxt+=this.Q100;
			xmlTxt+="</ipip:Q100>";
		}
		if (this.Q101!=null){
			xmlTxt+="\n<ipip:Q101";
			xmlTxt+=">";
			xmlTxt+=this.Q101;
			xmlTxt+="</ipip:Q101>";
		}
		if (this.Q102!=null){
			xmlTxt+="\n<ipip:Q102";
			xmlTxt+=">";
			xmlTxt+=this.Q102;
			xmlTxt+="</ipip:Q102>";
		}
		if (this.Q103!=null){
			xmlTxt+="\n<ipip:Q103";
			xmlTxt+=">";
			xmlTxt+=this.Q103;
			xmlTxt+="</ipip:Q103>";
		}
		if (this.Q104!=null){
			xmlTxt+="\n<ipip:Q104";
			xmlTxt+=">";
			xmlTxt+=this.Q104;
			xmlTxt+="</ipip:Q104>";
		}
		if (this.Q105!=null){
			xmlTxt+="\n<ipip:Q105";
			xmlTxt+=">";
			xmlTxt+=this.Q105;
			xmlTxt+="</ipip:Q105>";
		}
		if (this.Q106!=null){
			xmlTxt+="\n<ipip:Q106";
			xmlTxt+=">";
			xmlTxt+=this.Q106;
			xmlTxt+="</ipip:Q106>";
		}
		if (this.Q107!=null){
			xmlTxt+="\n<ipip:Q107";
			xmlTxt+=">";
			xmlTxt+=this.Q107;
			xmlTxt+="</ipip:Q107>";
		}
		if (this.Q108!=null){
			xmlTxt+="\n<ipip:Q108";
			xmlTxt+=">";
			xmlTxt+=this.Q108;
			xmlTxt+="</ipip:Q108>";
		}
		if (this.Q109!=null){
			xmlTxt+="\n<ipip:Q109";
			xmlTxt+=">";
			xmlTxt+=this.Q109;
			xmlTxt+="</ipip:Q109>";
		}
		if (this.Q110!=null){
			xmlTxt+="\n<ipip:Q110";
			xmlTxt+=">";
			xmlTxt+=this.Q110;
			xmlTxt+="</ipip:Q110>";
		}
		if (this.Q111!=null){
			xmlTxt+="\n<ipip:Q111";
			xmlTxt+=">";
			xmlTxt+=this.Q111;
			xmlTxt+="</ipip:Q111>";
		}
		if (this.Q112!=null){
			xmlTxt+="\n<ipip:Q112";
			xmlTxt+=">";
			xmlTxt+=this.Q112;
			xmlTxt+="</ipip:Q112>";
		}
		if (this.Q113!=null){
			xmlTxt+="\n<ipip:Q113";
			xmlTxt+=">";
			xmlTxt+=this.Q113;
			xmlTxt+="</ipip:Q113>";
		}
		if (this.Q114!=null){
			xmlTxt+="\n<ipip:Q114";
			xmlTxt+=">";
			xmlTxt+=this.Q114;
			xmlTxt+="</ipip:Q114>";
		}
		if (this.Q115!=null){
			xmlTxt+="\n<ipip:Q115";
			xmlTxt+=">";
			xmlTxt+=this.Q115;
			xmlTxt+="</ipip:Q115>";
		}
		if (this.Q116!=null){
			xmlTxt+="\n<ipip:Q116";
			xmlTxt+=">";
			xmlTxt+=this.Q116;
			xmlTxt+="</ipip:Q116>";
		}
		if (this.Q117!=null){
			xmlTxt+="\n<ipip:Q117";
			xmlTxt+=">";
			xmlTxt+=this.Q117;
			xmlTxt+="</ipip:Q117>";
		}
		if (this.Q118!=null){
			xmlTxt+="\n<ipip:Q118";
			xmlTxt+=">";
			xmlTxt+=this.Q118;
			xmlTxt+="</ipip:Q118>";
		}
		if (this.Q119!=null){
			xmlTxt+="\n<ipip:Q119";
			xmlTxt+=">";
			xmlTxt+=this.Q119;
			xmlTxt+="</ipip:Q119>";
		}
		if (this.Q120!=null){
			xmlTxt+="\n<ipip:Q120";
			xmlTxt+=">";
			xmlTxt+=this.Q120;
			xmlTxt+="</ipip:Q120>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Source!=null) return true;
		if (this.Q1!=null) return true;
		if (this.Q2!=null) return true;
		if (this.Q3!=null) return true;
		if (this.Q4!=null) return true;
		if (this.Q5!=null) return true;
		if (this.Q6!=null) return true;
		if (this.Q7!=null) return true;
		if (this.Q8!=null) return true;
		if (this.Q9!=null) return true;
		if (this.Q10!=null) return true;
		if (this.Q11!=null) return true;
		if (this.Q12!=null) return true;
		if (this.Q13!=null) return true;
		if (this.Q14!=null) return true;
		if (this.Q15!=null) return true;
		if (this.Q16!=null) return true;
		if (this.Q17!=null) return true;
		if (this.Q18!=null) return true;
		if (this.Q19!=null) return true;
		if (this.Q20!=null) return true;
		if (this.Q21!=null) return true;
		if (this.Q22!=null) return true;
		if (this.Q23!=null) return true;
		if (this.Q24!=null) return true;
		if (this.Q25!=null) return true;
		if (this.Q26!=null) return true;
		if (this.Q27!=null) return true;
		if (this.Q28!=null) return true;
		if (this.Q29!=null) return true;
		if (this.Q30!=null) return true;
		if (this.Q31!=null) return true;
		if (this.Q32!=null) return true;
		if (this.Q33!=null) return true;
		if (this.Q34!=null) return true;
		if (this.Q35!=null) return true;
		if (this.Q36!=null) return true;
		if (this.Q37!=null) return true;
		if (this.Q38!=null) return true;
		if (this.Q39!=null) return true;
		if (this.Q40!=null) return true;
		if (this.Q41!=null) return true;
		if (this.Q42!=null) return true;
		if (this.Q43!=null) return true;
		if (this.Q44!=null) return true;
		if (this.Q45!=null) return true;
		if (this.Q46!=null) return true;
		if (this.Q47!=null) return true;
		if (this.Q48!=null) return true;
		if (this.Q49!=null) return true;
		if (this.Q50!=null) return true;
		if (this.Q51!=null) return true;
		if (this.Q52!=null) return true;
		if (this.Q53!=null) return true;
		if (this.Q54!=null) return true;
		if (this.Q55!=null) return true;
		if (this.Q56!=null) return true;
		if (this.Q57!=null) return true;
		if (this.Q58!=null) return true;
		if (this.Q59!=null) return true;
		if (this.Q60!=null) return true;
		if (this.Q61!=null) return true;
		if (this.Q62!=null) return true;
		if (this.Q63!=null) return true;
		if (this.Q64!=null) return true;
		if (this.Q65!=null) return true;
		if (this.Q66!=null) return true;
		if (this.Q67!=null) return true;
		if (this.Q68!=null) return true;
		if (this.Q69!=null) return true;
		if (this.Q70!=null) return true;
		if (this.Q71!=null) return true;
		if (this.Q72!=null) return true;
		if (this.Q73!=null) return true;
		if (this.Q74!=null) return true;
		if (this.Q75!=null) return true;
		if (this.Q76!=null) return true;
		if (this.Q77!=null) return true;
		if (this.Q78!=null) return true;
		if (this.Q79!=null) return true;
		if (this.Q80!=null) return true;
		if (this.Q81!=null) return true;
		if (this.Q82!=null) return true;
		if (this.Q83!=null) return true;
		if (this.Q84!=null) return true;
		if (this.Q85!=null) return true;
		if (this.Q86!=null) return true;
		if (this.Q87!=null) return true;
		if (this.Q88!=null) return true;
		if (this.Q89!=null) return true;
		if (this.Q90!=null) return true;
		if (this.Q91!=null) return true;
		if (this.Q92!=null) return true;
		if (this.Q93!=null) return true;
		if (this.Q94!=null) return true;
		if (this.Q95!=null) return true;
		if (this.Q96!=null) return true;
		if (this.Q97!=null) return true;
		if (this.Q98!=null) return true;
		if (this.Q99!=null) return true;
		if (this.Q100!=null) return true;
		if (this.Q101!=null) return true;
		if (this.Q102!=null) return true;
		if (this.Q103!=null) return true;
		if (this.Q104!=null) return true;
		if (this.Q105!=null) return true;
		if (this.Q106!=null) return true;
		if (this.Q107!=null) return true;
		if (this.Q108!=null) return true;
		if (this.Q109!=null) return true;
		if (this.Q110!=null) return true;
		if (this.Q111!=null) return true;
		if (this.Q112!=null) return true;
		if (this.Q113!=null) return true;
		if (this.Q114!=null) return true;
		if (this.Q115!=null) return true;
		if (this.Q116!=null) return true;
		if (this.Q117!=null) return true;
		if (this.Q118!=null) return true;
		if (this.Q119!=null) return true;
		if (this.Q120!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
