/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:04 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function condr_progressionLogEntry(){
this.xsiType="condr:progressionLogEntry";

	this.getSchemaElementName=function(){
		return "progressionLogEntry";
	}

	this.getFullSchemaElementName=function(){
		return "condr:progressionLogEntry";
	}
this.extension=dynamicJSLoad('cnda_ext_saNode','generated/cnda_ext_saNode.js');

	this.Type=null;


	function getType() {
		return this.Type;
	}
	this.getType=getType;


	function setType(v){
		this.Type=v;
	}
	this.setType=setType;

	this.Kpsscore=null;


	function getKpsscore() {
		return this.Kpsscore;
	}
	this.getKpsscore=getKpsscore;


	function setKpsscore(v){
		this.Kpsscore=v;
	}
	this.setKpsscore=setKpsscore;

	this.Treatmentchange=null;


	function getTreatmentchange() {
		return this.Treatmentchange;
	}
	this.getTreatmentchange=getTreatmentchange;


	function setTreatmentchange(v){
		this.Treatmentchange=v;
	}
	this.setTreatmentchange=setTreatmentchange;


	this.isTreatmentchange=function(defaultValue) {
		if(this.Treatmentchange==null)return defaultValue;
		if(this.Treatmentchange=="1" || this.Treatmentchange==true)return true;
		return false;
	}

	this.Imaging=null;


	function getImaging() {
		return this.Imaging;
	}
	this.getImaging=getImaging;


	function setImaging(v){
		this.Imaging=v;
	}
	this.setImaging=setImaging;


	this.isImaging=function(defaultValue) {
		if(this.Imaging==null)return defaultValue;
		if(this.Imaging=="1" || this.Imaging==true)return true;
		return false;
	}

	this.Steroiddose=null;


	function getSteroiddose() {
		return this.Steroiddose;
	}
	this.getSteroiddose=getSteroiddose;


	function setSteroiddose(v){
		this.Steroiddose=v;
	}
	this.setSteroiddose=setSteroiddose;


	this.isSteroiddose=function(defaultValue) {
		if(this.Steroiddose==null)return defaultValue;
		if(this.Steroiddose=="1" || this.Steroiddose==true)return true;
		return false;
	}

	this.Steroiddoseamount=null;


	function getSteroiddoseamount() {
		return this.Steroiddoseamount;
	}
	this.getSteroiddoseamount=getSteroiddoseamount;


	function setSteroiddoseamount(v){
		this.Steroiddoseamount=v;
	}
	this.setSteroiddoseamount=setSteroiddoseamount;

	this.Functionalstatus=null;


	function getFunctionalstatus() {
		return this.Functionalstatus;
	}
	this.getFunctionalstatus=getFunctionalstatus;


	function setFunctionalstatus(v){
		this.Functionalstatus=v;
	}
	this.setFunctionalstatus=setFunctionalstatus;


	this.isFunctionalstatus=function(defaultValue) {
		if(this.Functionalstatus==null)return defaultValue;
		if(this.Functionalstatus=="1" || this.Functionalstatus==true)return true;
		return false;
	}

	this.Newneurologicaldefecit=null;


	function getNewneurologicaldefecit() {
		return this.Newneurologicaldefecit;
	}
	this.getNewneurologicaldefecit=getNewneurologicaldefecit;


	function setNewneurologicaldefecit(v){
		this.Newneurologicaldefecit=v;
	}
	this.setNewneurologicaldefecit=setNewneurologicaldefecit;


	this.isNewneurologicaldefecit=function(defaultValue) {
		if(this.Newneurologicaldefecit==null)return defaultValue;
		if(this.Newneurologicaldefecit=="1" || this.Newneurologicaldefecit==true)return true;
		return false;
	}

	this.Motorweakness=null;


	function getMotorweakness() {
		return this.Motorweakness;
	}
	this.getMotorweakness=getMotorweakness;


	function setMotorweakness(v){
		this.Motorweakness=v;
	}
	this.setMotorweakness=setMotorweakness;


	this.isMotorweakness=function(defaultValue) {
		if(this.Motorweakness==null)return defaultValue;
		if(this.Motorweakness=="1" || this.Motorweakness==true)return true;
		return false;
	}

	this.Sensorychange=null;


	function getSensorychange() {
		return this.Sensorychange;
	}
	this.getSensorychange=getSensorychange;


	function setSensorychange(v){
		this.Sensorychange=v;
	}
	this.setSensorychange=setSensorychange;


	this.isSensorychange=function(defaultValue) {
		if(this.Sensorychange==null)return defaultValue;
		if(this.Sensorychange=="1" || this.Sensorychange==true)return true;
		return false;
	}

	this.Severeheadache=null;


	function getSevereheadache() {
		return this.Severeheadache;
	}
	this.getSevereheadache=getSevereheadache;


	function setSevereheadache(v){
		this.Severeheadache=v;
	}
	this.setSevereheadache=setSevereheadache;


	this.isSevereheadache=function(defaultValue) {
		if(this.Severeheadache==null)return defaultValue;
		if(this.Severeheadache=="1" || this.Severeheadache==true)return true;
		return false;
	}

	this.Nausea=null;


	function getNausea() {
		return this.Nausea;
	}
	this.getNausea=getNausea;


	function setNausea(v){
		this.Nausea=v;
	}
	this.setNausea=setNausea;


	this.isNausea=function(defaultValue) {
		if(this.Nausea==null)return defaultValue;
		if(this.Nausea=="1" || this.Nausea==true)return true;
		return false;
	}

	this.Visualloss=null;


	function getVisualloss() {
		return this.Visualloss;
	}
	this.getVisualloss=getVisualloss;


	function setVisualloss(v){
		this.Visualloss=v;
	}
	this.setVisualloss=setVisualloss;


	this.isVisualloss=function(defaultValue) {
		if(this.Visualloss==null)return defaultValue;
		if(this.Visualloss=="1" || this.Visualloss==true)return true;
		return false;
	}

	this.Cognitivedecline=null;


	function getCognitivedecline() {
		return this.Cognitivedecline;
	}
	this.getCognitivedecline=getCognitivedecline;


	function setCognitivedecline(v){
		this.Cognitivedecline=v;
	}
	this.setCognitivedecline=setCognitivedecline;


	this.isCognitivedecline=function(defaultValue) {
		if(this.Cognitivedecline==null)return defaultValue;
		if(this.Cognitivedecline=="1" || this.Cognitivedecline==true)return true;
		return false;
	}

	this.Clinicaldeterioration=null;


	function getClinicaldeterioration() {
		return this.Clinicaldeterioration;
	}
	this.getClinicaldeterioration=getClinicaldeterioration;


	function setClinicaldeterioration(v){
		this.Clinicaldeterioration=v;
	}
	this.setClinicaldeterioration=setClinicaldeterioration;


	this.isClinicaldeterioration=function(defaultValue) {
		if(this.Clinicaldeterioration==null)return defaultValue;
		if(this.Clinicaldeterioration=="1" || this.Clinicaldeterioration==true)return true;
		return false;
	}

	this.Otherneurologicaldefecit=null;


	function getOtherneurologicaldefecit() {
		return this.Otherneurologicaldefecit;
	}
	this.getOtherneurologicaldefecit=getOtherneurologicaldefecit;


	function setOtherneurologicaldefecit(v){
		this.Otherneurologicaldefecit=v;
	}
	this.setOtherneurologicaldefecit=setOtherneurologicaldefecit;


	this.isOtherneurologicaldefecit=function(defaultValue) {
		if(this.Otherneurologicaldefecit==null)return defaultValue;
		if(this.Otherneurologicaldefecit=="1" || this.Otherneurologicaldefecit==true)return true;
		return false;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="saNode"){
				return this.Sanode ;
			} else 
			if(xmlPath.startsWith("saNode")){
				xmlPath=xmlPath.substring(6);
				if(xmlPath=="")return this.Sanode ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Sanode!=undefined)return this.Sanode.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="type"){
				return this.Type ;
			} else 
			if(xmlPath=="kpsScore"){
				return this.Kpsscore ;
			} else 
			if(xmlPath=="treatmentChange"){
				return this.Treatmentchange ;
			} else 
			if(xmlPath=="imaging"){
				return this.Imaging ;
			} else 
			if(xmlPath=="steroidDose"){
				return this.Steroiddose ;
			} else 
			if(xmlPath=="steroidDoseAmount"){
				return this.Steroiddoseamount ;
			} else 
			if(xmlPath=="functionalStatus"){
				return this.Functionalstatus ;
			} else 
			if(xmlPath=="newNeurologicalDefecit"){
				return this.Newneurologicaldefecit ;
			} else 
			if(xmlPath=="motorWeakness"){
				return this.Motorweakness ;
			} else 
			if(xmlPath=="sensoryChange"){
				return this.Sensorychange ;
			} else 
			if(xmlPath=="severeHeadache"){
				return this.Severeheadache ;
			} else 
			if(xmlPath=="nausea"){
				return this.Nausea ;
			} else 
			if(xmlPath=="visualLoss"){
				return this.Visualloss ;
			} else 
			if(xmlPath=="cognitiveDecline"){
				return this.Cognitivedecline ;
			} else 
			if(xmlPath=="clinicalDeterioration"){
				return this.Clinicaldeterioration ;
			} else 
			if(xmlPath=="otherNeurologicalDefecit"){
				return this.Otherneurologicaldefecit ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="saNode"){
				this.Sanode=value;
			} else 
			if(xmlPath.startsWith("saNode")){
				xmlPath=xmlPath.substring(6);
				if(xmlPath=="")return this.Sanode ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Sanode!=undefined){
					this.Sanode.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Sanode= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Sanode= instanciateObject("cnda_ext:saNode");//omUtils.js
						}
						if(options && options.where)this.Sanode.setProperty(options.where.field,options.where.value);
						this.Sanode.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="type"){
				this.Type=value;
			} else 
			if(xmlPath=="kpsScore"){
				this.Kpsscore=value;
			} else 
			if(xmlPath=="treatmentChange"){
				this.Treatmentchange=value;
			} else 
			if(xmlPath=="imaging"){
				this.Imaging=value;
			} else 
			if(xmlPath=="steroidDose"){
				this.Steroiddose=value;
			} else 
			if(xmlPath=="steroidDoseAmount"){
				this.Steroiddoseamount=value;
			} else 
			if(xmlPath=="functionalStatus"){
				this.Functionalstatus=value;
			} else 
			if(xmlPath=="newNeurologicalDefecit"){
				this.Newneurologicaldefecit=value;
			} else 
			if(xmlPath=="motorWeakness"){
				this.Motorweakness=value;
			} else 
			if(xmlPath=="sensoryChange"){
				this.Sensorychange=value;
			} else 
			if(xmlPath=="severeHeadache"){
				this.Severeheadache=value;
			} else 
			if(xmlPath=="nausea"){
				this.Nausea=value;
			} else 
			if(xmlPath=="visualLoss"){
				this.Visualloss=value;
			} else 
			if(xmlPath=="cognitiveDecline"){
				this.Cognitivedecline=value;
			} else 
			if(xmlPath=="clinicalDeterioration"){
				this.Clinicaldeterioration=value;
			} else 
			if(xmlPath=="otherNeurologicalDefecit"){
				this.Otherneurologicaldefecit=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="type"){
			return "field_data";
		}else if (xmlPath=="kpsScore"){
			return "field_data";
		}else if (xmlPath=="treatmentChange"){
			return "field_data";
		}else if (xmlPath=="imaging"){
			return "field_data";
		}else if (xmlPath=="steroidDose"){
			return "field_data";
		}else if (xmlPath=="steroidDoseAmount"){
			return "field_data";
		}else if (xmlPath=="functionalStatus"){
			return "field_data";
		}else if (xmlPath=="newNeurologicalDefecit"){
			return "field_data";
		}else if (xmlPath=="motorWeakness"){
			return "field_data";
		}else if (xmlPath=="sensoryChange"){
			return "field_data";
		}else if (xmlPath=="severeHeadache"){
			return "field_data";
		}else if (xmlPath=="nausea"){
			return "field_data";
		}else if (xmlPath=="visualLoss"){
			return "field_data";
		}else if (xmlPath=="cognitiveDecline"){
			return "field_data";
		}else if (xmlPath=="clinicalDeterioration"){
			return "field_data";
		}else if (xmlPath=="otherNeurologicalDefecit"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<condr:ProgressionLogEntry";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</condr:ProgressionLogEntry>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Type!=null){
			xmlTxt+="\n<condr:type";
			xmlTxt+=">";
			xmlTxt+=this.Type.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:type>";
		}
		if (this.Kpsscore!=null){
			xmlTxt+="\n<condr:kpsScore";
			xmlTxt+=">";
			xmlTxt+=this.Kpsscore;
			xmlTxt+="</condr:kpsScore>";
		}
		if (this.Treatmentchange!=null){
			xmlTxt+="\n<condr:treatmentChange";
			xmlTxt+=">";
			xmlTxt+=this.Treatmentchange;
			xmlTxt+="</condr:treatmentChange>";
		}
		if (this.Imaging!=null){
			xmlTxt+="\n<condr:imaging";
			xmlTxt+=">";
			xmlTxt+=this.Imaging;
			xmlTxt+="</condr:imaging>";
		}
		if (this.Steroiddose!=null){
			xmlTxt+="\n<condr:steroidDose";
			xmlTxt+=">";
			xmlTxt+=this.Steroiddose;
			xmlTxt+="</condr:steroidDose>";
		}
		if (this.Steroiddoseamount!=null){
			xmlTxt+="\n<condr:steroidDoseAmount";
			xmlTxt+=">";
			xmlTxt+=this.Steroiddoseamount;
			xmlTxt+="</condr:steroidDoseAmount>";
		}
		if (this.Functionalstatus!=null){
			xmlTxt+="\n<condr:functionalStatus";
			xmlTxt+=">";
			xmlTxt+=this.Functionalstatus;
			xmlTxt+="</condr:functionalStatus>";
		}
		if (this.Newneurologicaldefecit!=null){
			xmlTxt+="\n<condr:newNeurologicalDefecit";
			xmlTxt+=">";
			xmlTxt+=this.Newneurologicaldefecit;
			xmlTxt+="</condr:newNeurologicalDefecit>";
		}
		if (this.Motorweakness!=null){
			xmlTxt+="\n<condr:motorWeakness";
			xmlTxt+=">";
			xmlTxt+=this.Motorweakness;
			xmlTxt+="</condr:motorWeakness>";
		}
		if (this.Sensorychange!=null){
			xmlTxt+="\n<condr:sensoryChange";
			xmlTxt+=">";
			xmlTxt+=this.Sensorychange;
			xmlTxt+="</condr:sensoryChange>";
		}
		if (this.Severeheadache!=null){
			xmlTxt+="\n<condr:severeHeadache";
			xmlTxt+=">";
			xmlTxt+=this.Severeheadache;
			xmlTxt+="</condr:severeHeadache>";
		}
		if (this.Nausea!=null){
			xmlTxt+="\n<condr:nausea";
			xmlTxt+=">";
			xmlTxt+=this.Nausea;
			xmlTxt+="</condr:nausea>";
		}
		if (this.Visualloss!=null){
			xmlTxt+="\n<condr:visualLoss";
			xmlTxt+=">";
			xmlTxt+=this.Visualloss;
			xmlTxt+="</condr:visualLoss>";
		}
		if (this.Cognitivedecline!=null){
			xmlTxt+="\n<condr:cognitiveDecline";
			xmlTxt+=">";
			xmlTxt+=this.Cognitivedecline;
			xmlTxt+="</condr:cognitiveDecline>";
		}
		if (this.Clinicaldeterioration!=null){
			xmlTxt+="\n<condr:clinicalDeterioration";
			xmlTxt+=">";
			xmlTxt+=this.Clinicaldeterioration;
			xmlTxt+="</condr:clinicalDeterioration>";
		}
		if (this.Otherneurologicaldefecit!=null){
			xmlTxt+="\n<condr:otherNeurologicalDefecit";
			xmlTxt+=">";
			xmlTxt+=this.Otherneurologicaldefecit;
			xmlTxt+="</condr:otherNeurologicalDefecit>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Type!=null) return true;
		if (this.Kpsscore!=null) return true;
		if (this.Treatmentchange!=null) return true;
		if (this.Imaging!=null) return true;
		if (this.Steroiddose!=null) return true;
		if (this.Steroiddoseamount!=null) return true;
		if (this.Functionalstatus!=null) return true;
		if (this.Newneurologicaldefecit!=null) return true;
		if (this.Motorweakness!=null) return true;
		if (this.Sensorychange!=null) return true;
		if (this.Severeheadache!=null) return true;
		if (this.Nausea!=null) return true;
		if (this.Visualloss!=null) return true;
		if (this.Cognitivedecline!=null) return true;
		if (this.Clinicaldeterioration!=null) return true;
		if (this.Otherneurologicaldefecit!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
