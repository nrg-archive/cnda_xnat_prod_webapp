/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function dian_psybatData(){
this.xsiType="dian:psybatData";

	this.getSchemaElementName=function(){
		return "psybatData";
	}

	this.getFullSchemaElementName=function(){
		return "dian:psybatData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Initials=null;


	function getInitials() {
		return this.Initials;
	}
	this.getInitials=getInitials;


	function setInitials(v){
		this.Initials=v;
	}
	this.setInitials=setInitials;

	this.Letterf=null;


	function getLetterf() {
		return this.Letterf;
	}
	this.getLetterf=getLetterf;


	function setLetterf(v){
		this.Letterf=v;
	}
	this.setLetterf=setLetterf;

	this.Lettera=null;


	function getLettera() {
		return this.Lettera;
	}
	this.getLettera=getLettera;


	function setLettera(v){
		this.Lettera=v;
	}
	this.setLettera=setLettera;

	this.Letters=null;


	function getLetters() {
		return this.Letters;
	}
	this.getLetters=getLetters;


	function setLetters(v){
		this.Letters=v;
	}
	this.setLetters=setLetters;

	this.Wrlist=null;


	function getWrlist() {
		return this.Wrlist;
	}
	this.getWrlist=getWrlist;


	function setWrlist(v){
		this.Wrlist=v;
	}
	this.setWrlist=setWrlist;

	this.Wrimmed=null;


	function getWrimmed() {
		return this.Wrimmed;
	}
	this.getWrimmed=getWrimmed;


	function setWrimmed(v){
		this.Wrimmed=v;
	}
	this.setWrimmed=setWrimmed;

	this.Mmsetot=null;


	function getMmsetot() {
		return this.Mmsetot;
	}
	this.getMmsetot=getMmsetot;


	function setMmsetot(v){
		this.Mmsetot=v;
	}
	this.setMmsetot=setMmsetot;

	this.Logmemim=null;


	function getLogmemim() {
		return this.Logmemim;
	}
	this.getLogmemim=getLogmemim;


	function setLogmemim(v){
		this.Logmemim=v;
	}
	this.setLogmemim=setLogmemim;

	this.Dspfwd=null;


	function getDspfwd() {
		return this.Dspfwd;
	}
	this.getDspfwd=getDspfwd;


	function setDspfwd(v){
		this.Dspfwd=v;
	}
	this.setDspfwd=setDspfwd;

	this.Dspfwdl=null;


	function getDspfwdl() {
		return this.Dspfwdl;
	}
	this.getDspfwdl=getDspfwdl;


	function setDspfwdl(v){
		this.Dspfwdl=v;
	}
	this.setDspfwdl=setDspfwdl;

	this.Dspback=null;


	function getDspback() {
		return this.Dspback;
	}
	this.getDspback=getDspback;


	function setDspback(v){
		this.Dspback=v;
	}
	this.setDspback=setDspback;

	this.Dspbackl=null;


	function getDspbackl() {
		return this.Dspbackl;
	}
	this.getDspbackl=getDspbackl;


	function setDspbackl(v){
		this.Dspbackl=v;
	}
	this.setDspbackl=setDspbackl;

	this.Catanim=null;


	function getCatanim() {
		return this.Catanim;
	}
	this.getCatanim=getCatanim;


	function setCatanim(v){
		this.Catanim=v;
	}
	this.setCatanim=setCatanim;

	this.Catveg=null;


	function getCatveg() {
		return this.Catveg;
	}
	this.getCatveg=getCatveg;


	function setCatveg(v){
		this.Catveg=v;
	}
	this.setCatveg=setCatveg;

	this.Tratime=null;


	function getTratime() {
		return this.Tratime;
	}
	this.getTratime=getTratime;


	function setTratime(v){
		this.Tratime=v;
	}
	this.setTratime=setTratime;

	this.Traerrc=null;


	function getTraerrc() {
		return this.Traerrc;
	}
	this.getTraerrc=getTraerrc;


	function setTraerrc(v){
		this.Traerrc=v;
	}
	this.setTraerrc=setTraerrc;

	this.Tralines=null;


	function getTralines() {
		return this.Tralines;
	}
	this.getTralines=getTralines;


	function setTralines(v){
		this.Tralines=v;
	}
	this.setTralines=setTralines;

	this.Trbtime=null;


	function getTrbtime() {
		return this.Trbtime;
	}
	this.getTrbtime=getTrbtime;


	function setTrbtime(v){
		this.Trbtime=v;
	}
	this.setTrbtime=setTrbtime;

	this.Trberrc=null;


	function getTrberrc() {
		return this.Trberrc;
	}
	this.getTrberrc=getTrberrc;


	function setTrberrc(v){
		this.Trberrc=v;
	}
	this.setTrberrc=setTrberrc;

	this.Trblines=null;


	function getTrblines() {
		return this.Trblines;
	}
	this.getTrblines=getTrblines;


	function setTrblines(v){
		this.Trblines=v;
	}
	this.setTrblines=setTrblines;

	this.Digsymb=null;


	function getDigsymb() {
		return this.Digsymb;
	}
	this.getDigsymb=getDigsymb;


	function setDigsymb(v){
		this.Digsymb=v;
	}
	this.setDigsymb=setDigsymb;

	this.Logmemdl=null;


	function getLogmemdl() {
		return this.Logmemdl;
	}
	this.getLogmemdl=getLogmemdl;


	function setLogmemdl(v){
		this.Logmemdl=v;
	}
	this.setLogmemdl=setLogmemdl;

	this.Bnttot=null;


	function getBnttot() {
		return this.Bnttot;
	}
	this.getBnttot=getBnttot;


	function setBnttot(v){
		this.Bnttot=v;
	}
	this.setBnttot=setBnttot;

	this.Wrdelay=null;


	function getWrdelay() {
		return this.Wrdelay;
	}
	this.getWrdelay=getWrdelay;


	function setWrdelay(v){
		this.Wrdelay=v;
	}
	this.setWrdelay=setWrdelay;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="INITIALS"){
				return this.Initials ;
			} else 
			if(xmlPath=="LETTERF"){
				return this.Letterf ;
			} else 
			if(xmlPath=="LETTERA"){
				return this.Lettera ;
			} else 
			if(xmlPath=="LETTERS"){
				return this.Letters ;
			} else 
			if(xmlPath=="WRLIST"){
				return this.Wrlist ;
			} else 
			if(xmlPath=="WRIMMED"){
				return this.Wrimmed ;
			} else 
			if(xmlPath=="MMSETOT"){
				return this.Mmsetot ;
			} else 
			if(xmlPath=="LOGMEMIM"){
				return this.Logmemim ;
			} else 
			if(xmlPath=="DSPFWD"){
				return this.Dspfwd ;
			} else 
			if(xmlPath=="DSPFWDL"){
				return this.Dspfwdl ;
			} else 
			if(xmlPath=="DSPBACK"){
				return this.Dspback ;
			} else 
			if(xmlPath=="DSPBACKL"){
				return this.Dspbackl ;
			} else 
			if(xmlPath=="CATANIM"){
				return this.Catanim ;
			} else 
			if(xmlPath=="CATVEG"){
				return this.Catveg ;
			} else 
			if(xmlPath=="TRATIME"){
				return this.Tratime ;
			} else 
			if(xmlPath=="TRAERRC"){
				return this.Traerrc ;
			} else 
			if(xmlPath=="TRALINES"){
				return this.Tralines ;
			} else 
			if(xmlPath=="TRBTIME"){
				return this.Trbtime ;
			} else 
			if(xmlPath=="TRBERRC"){
				return this.Trberrc ;
			} else 
			if(xmlPath=="TRBLINES"){
				return this.Trblines ;
			} else 
			if(xmlPath=="DIGSYMB"){
				return this.Digsymb ;
			} else 
			if(xmlPath=="LOGMEMDL"){
				return this.Logmemdl ;
			} else 
			if(xmlPath=="BNTTOT"){
				return this.Bnttot ;
			} else 
			if(xmlPath=="WRDELAY"){
				return this.Wrdelay ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="INITIALS"){
				this.Initials=value;
			} else 
			if(xmlPath=="LETTERF"){
				this.Letterf=value;
			} else 
			if(xmlPath=="LETTERA"){
				this.Lettera=value;
			} else 
			if(xmlPath=="LETTERS"){
				this.Letters=value;
			} else 
			if(xmlPath=="WRLIST"){
				this.Wrlist=value;
			} else 
			if(xmlPath=="WRIMMED"){
				this.Wrimmed=value;
			} else 
			if(xmlPath=="MMSETOT"){
				this.Mmsetot=value;
			} else 
			if(xmlPath=="LOGMEMIM"){
				this.Logmemim=value;
			} else 
			if(xmlPath=="DSPFWD"){
				this.Dspfwd=value;
			} else 
			if(xmlPath=="DSPFWDL"){
				this.Dspfwdl=value;
			} else 
			if(xmlPath=="DSPBACK"){
				this.Dspback=value;
			} else 
			if(xmlPath=="DSPBACKL"){
				this.Dspbackl=value;
			} else 
			if(xmlPath=="CATANIM"){
				this.Catanim=value;
			} else 
			if(xmlPath=="CATVEG"){
				this.Catveg=value;
			} else 
			if(xmlPath=="TRATIME"){
				this.Tratime=value;
			} else 
			if(xmlPath=="TRAERRC"){
				this.Traerrc=value;
			} else 
			if(xmlPath=="TRALINES"){
				this.Tralines=value;
			} else 
			if(xmlPath=="TRBTIME"){
				this.Trbtime=value;
			} else 
			if(xmlPath=="TRBERRC"){
				this.Trberrc=value;
			} else 
			if(xmlPath=="TRBLINES"){
				this.Trblines=value;
			} else 
			if(xmlPath=="DIGSYMB"){
				this.Digsymb=value;
			} else 
			if(xmlPath=="LOGMEMDL"){
				this.Logmemdl=value;
			} else 
			if(xmlPath=="BNTTOT"){
				this.Bnttot=value;
			} else 
			if(xmlPath=="WRDELAY"){
				this.Wrdelay=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="INITIALS"){
			return "field_data";
		}else if (xmlPath=="LETTERF"){
			return "field_data";
		}else if (xmlPath=="LETTERA"){
			return "field_data";
		}else if (xmlPath=="LETTERS"){
			return "field_data";
		}else if (xmlPath=="WRLIST"){
			return "field_data";
		}else if (xmlPath=="WRIMMED"){
			return "field_data";
		}else if (xmlPath=="MMSETOT"){
			return "field_data";
		}else if (xmlPath=="LOGMEMIM"){
			return "field_data";
		}else if (xmlPath=="DSPFWD"){
			return "field_data";
		}else if (xmlPath=="DSPFWDL"){
			return "field_data";
		}else if (xmlPath=="DSPBACK"){
			return "field_data";
		}else if (xmlPath=="DSPBACKL"){
			return "field_data";
		}else if (xmlPath=="CATANIM"){
			return "field_data";
		}else if (xmlPath=="CATVEG"){
			return "field_data";
		}else if (xmlPath=="TRATIME"){
			return "field_data";
		}else if (xmlPath=="TRAERRC"){
			return "field_data";
		}else if (xmlPath=="TRALINES"){
			return "field_data";
		}else if (xmlPath=="TRBTIME"){
			return "field_data";
		}else if (xmlPath=="TRBERRC"){
			return "field_data";
		}else if (xmlPath=="TRBLINES"){
			return "field_data";
		}else if (xmlPath=="DIGSYMB"){
			return "field_data";
		}else if (xmlPath=="LOGMEMDL"){
			return "field_data";
		}else if (xmlPath=="BNTTOT"){
			return "field_data";
		}else if (xmlPath=="WRDELAY"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<dian:PSYBAT";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</dian:PSYBAT>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Initials!=null){
			xmlTxt+="\n<dian:INITIALS";
			xmlTxt+=">";
			xmlTxt+=this.Initials.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:INITIALS>";
		}
		if (this.Letterf!=null){
			xmlTxt+="\n<dian:LETTERF";
			xmlTxt+=">";
			xmlTxt+=this.Letterf;
			xmlTxt+="</dian:LETTERF>";
		}
		if (this.Lettera!=null){
			xmlTxt+="\n<dian:LETTERA";
			xmlTxt+=">";
			xmlTxt+=this.Lettera;
			xmlTxt+="</dian:LETTERA>";
		}
		if (this.Letters!=null){
			xmlTxt+="\n<dian:LETTERS";
			xmlTxt+=">";
			xmlTxt+=this.Letters;
			xmlTxt+="</dian:LETTERS>";
		}
		if (this.Wrlist!=null){
			xmlTxt+="\n<dian:WRLIST";
			xmlTxt+=">";
			xmlTxt+=this.Wrlist;
			xmlTxt+="</dian:WRLIST>";
		}
		if (this.Wrimmed!=null){
			xmlTxt+="\n<dian:WRIMMED";
			xmlTxt+=">";
			xmlTxt+=this.Wrimmed;
			xmlTxt+="</dian:WRIMMED>";
		}
		if (this.Mmsetot!=null){
			xmlTxt+="\n<dian:MMSETOT";
			xmlTxt+=">";
			xmlTxt+=this.Mmsetot;
			xmlTxt+="</dian:MMSETOT>";
		}
		if (this.Logmemim!=null){
			xmlTxt+="\n<dian:LOGMEMIM";
			xmlTxt+=">";
			xmlTxt+=this.Logmemim;
			xmlTxt+="</dian:LOGMEMIM>";
		}
		if (this.Dspfwd!=null){
			xmlTxt+="\n<dian:DSPFWD";
			xmlTxt+=">";
			xmlTxt+=this.Dspfwd;
			xmlTxt+="</dian:DSPFWD>";
		}
		if (this.Dspfwdl!=null){
			xmlTxt+="\n<dian:DSPFWDL";
			xmlTxt+=">";
			xmlTxt+=this.Dspfwdl;
			xmlTxt+="</dian:DSPFWDL>";
		}
		if (this.Dspback!=null){
			xmlTxt+="\n<dian:DSPBACK";
			xmlTxt+=">";
			xmlTxt+=this.Dspback;
			xmlTxt+="</dian:DSPBACK>";
		}
		if (this.Dspbackl!=null){
			xmlTxt+="\n<dian:DSPBACKL";
			xmlTxt+=">";
			xmlTxt+=this.Dspbackl;
			xmlTxt+="</dian:DSPBACKL>";
		}
		if (this.Catanim!=null){
			xmlTxt+="\n<dian:CATANIM";
			xmlTxt+=">";
			xmlTxt+=this.Catanim;
			xmlTxt+="</dian:CATANIM>";
		}
		if (this.Catveg!=null){
			xmlTxt+="\n<dian:CATVEG";
			xmlTxt+=">";
			xmlTxt+=this.Catveg;
			xmlTxt+="</dian:CATVEG>";
		}
		if (this.Tratime!=null){
			xmlTxt+="\n<dian:TRATIME";
			xmlTxt+=">";
			xmlTxt+=this.Tratime;
			xmlTxt+="</dian:TRATIME>";
		}
		if (this.Traerrc!=null){
			xmlTxt+="\n<dian:TRAERRC";
			xmlTxt+=">";
			xmlTxt+=this.Traerrc;
			xmlTxt+="</dian:TRAERRC>";
		}
		if (this.Tralines!=null){
			xmlTxt+="\n<dian:TRALINES";
			xmlTxt+=">";
			xmlTxt+=this.Tralines;
			xmlTxt+="</dian:TRALINES>";
		}
		if (this.Trbtime!=null){
			xmlTxt+="\n<dian:TRBTIME";
			xmlTxt+=">";
			xmlTxt+=this.Trbtime;
			xmlTxt+="</dian:TRBTIME>";
		}
		if (this.Trberrc!=null){
			xmlTxt+="\n<dian:TRBERRC";
			xmlTxt+=">";
			xmlTxt+=this.Trberrc;
			xmlTxt+="</dian:TRBERRC>";
		}
		if (this.Trblines!=null){
			xmlTxt+="\n<dian:TRBLINES";
			xmlTxt+=">";
			xmlTxt+=this.Trblines;
			xmlTxt+="</dian:TRBLINES>";
		}
		if (this.Digsymb!=null){
			xmlTxt+="\n<dian:DIGSYMB";
			xmlTxt+=">";
			xmlTxt+=this.Digsymb;
			xmlTxt+="</dian:DIGSYMB>";
		}
		if (this.Logmemdl!=null){
			xmlTxt+="\n<dian:LOGMEMDL";
			xmlTxt+=">";
			xmlTxt+=this.Logmemdl;
			xmlTxt+="</dian:LOGMEMDL>";
		}
		if (this.Bnttot!=null){
			xmlTxt+="\n<dian:BNTTOT";
			xmlTxt+=">";
			xmlTxt+=this.Bnttot;
			xmlTxt+="</dian:BNTTOT>";
		}
		if (this.Wrdelay!=null){
			xmlTxt+="\n<dian:WRDELAY";
			xmlTxt+=">";
			xmlTxt+=this.Wrdelay;
			xmlTxt+="</dian:WRDELAY>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Initials!=null) return true;
		if (this.Letterf!=null) return true;
		if (this.Lettera!=null) return true;
		if (this.Letters!=null) return true;
		if (this.Wrlist!=null) return true;
		if (this.Wrimmed!=null) return true;
		if (this.Mmsetot!=null) return true;
		if (this.Logmemim!=null) return true;
		if (this.Dspfwd!=null) return true;
		if (this.Dspfwdl!=null) return true;
		if (this.Dspback!=null) return true;
		if (this.Dspbackl!=null) return true;
		if (this.Catanim!=null) return true;
		if (this.Catveg!=null) return true;
		if (this.Tratime!=null) return true;
		if (this.Traerrc!=null) return true;
		if (this.Tralines!=null) return true;
		if (this.Trbtime!=null) return true;
		if (this.Trberrc!=null) return true;
		if (this.Trblines!=null) return true;
		if (this.Digsymb!=null) return true;
		if (this.Logmemdl!=null) return true;
		if (this.Bnttot!=null) return true;
		if (this.Wrdelay!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
