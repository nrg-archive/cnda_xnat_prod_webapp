/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function dian_bldLipidData(){
this.xsiType="dian:bldLipidData";

	this.getSchemaElementName=function(){
		return "bldLipidData";
	}

	this.getFullSchemaElementName=function(){
		return "dian:bldLipidData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Bldcollsess=null;


	function getBldcollsess() {
		return this.Bldcollsess;
	}
	this.getBldcollsess=getBldcollsess;


	function setBldcollsess(v){
		this.Bldcollsess=v;
	}
	this.setBldcollsess=setBldcollsess;

	this.Triglyc=null;


	function getTriglyc() {
		return this.Triglyc;
	}
	this.getTriglyc=getTriglyc;


	function setTriglyc(v){
		this.Triglyc=v;
	}
	this.setTriglyc=setTriglyc;

	this.Triglycunits=null;


	function getTriglycunits() {
		return this.Triglycunits;
	}
	this.getTriglycunits=getTriglycunits;


	function setTriglycunits(v){
		this.Triglycunits=v;
	}
	this.setTriglycunits=setTriglycunits;

	this.Totcholes=null;


	function getTotcholes() {
		return this.Totcholes;
	}
	this.getTotcholes=getTotcholes;


	function setTotcholes(v){
		this.Totcholes=v;
	}
	this.setTotcholes=setTotcholes;

	this.Totcholesunits=null;


	function getTotcholesunits() {
		return this.Totcholesunits;
	}
	this.getTotcholesunits=getTotcholesunits;


	function setTotcholesunits(v){
		this.Totcholesunits=v;
	}
	this.setTotcholesunits=setTotcholesunits;

	this.Hdl=null;


	function getHdl() {
		return this.Hdl;
	}
	this.getHdl=getHdl;


	function setHdl(v){
		this.Hdl=v;
	}
	this.setHdl=setHdl;

	this.Hdlunits=null;


	function getHdlunits() {
		return this.Hdlunits;
	}
	this.getHdlunits=getHdlunits;


	function setHdlunits(v){
		this.Hdlunits=v;
	}
	this.setHdlunits=setHdlunits;

	this.Ldl=null;


	function getLdl() {
		return this.Ldl;
	}
	this.getLdl=getLdl;


	function setLdl(v){
		this.Ldl=v;
	}
	this.setLdl=setLdl;

	this.Ldlunits=null;


	function getLdlunits() {
		return this.Ldlunits;
	}
	this.getLdlunits=getLdlunits;


	function setLdlunits(v){
		this.Ldlunits=v;
	}
	this.setLdlunits=setLdlunits;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="bldCollSess"){
				return this.Bldcollsess ;
			} else 
			if(xmlPath=="Triglyc"){
				return this.Triglyc ;
			} else 
			if(xmlPath=="TriglycUnits"){
				return this.Triglycunits ;
			} else 
			if(xmlPath=="TotCholes"){
				return this.Totcholes ;
			} else 
			if(xmlPath=="TotCholesUnits"){
				return this.Totcholesunits ;
			} else 
			if(xmlPath=="HDL"){
				return this.Hdl ;
			} else 
			if(xmlPath=="HDLUnits"){
				return this.Hdlunits ;
			} else 
			if(xmlPath=="LDL"){
				return this.Ldl ;
			} else 
			if(xmlPath=="LDLUnits"){
				return this.Ldlunits ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="bldCollSess"){
				this.Bldcollsess=value;
			} else 
			if(xmlPath=="Triglyc"){
				this.Triglyc=value;
			} else 
			if(xmlPath=="TriglycUnits"){
				this.Triglycunits=value;
			} else 
			if(xmlPath=="TotCholes"){
				this.Totcholes=value;
			} else 
			if(xmlPath=="TotCholesUnits"){
				this.Totcholesunits=value;
			} else 
			if(xmlPath=="HDL"){
				this.Hdl=value;
			} else 
			if(xmlPath=="HDLUnits"){
				this.Hdlunits=value;
			} else 
			if(xmlPath=="LDL"){
				this.Ldl=value;
			} else 
			if(xmlPath=="LDLUnits"){
				this.Ldlunits=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="bldCollSess"){
			return "field_data";
		}else if (xmlPath=="Triglyc"){
			return "field_data";
		}else if (xmlPath=="TriglycUnits"){
			return "field_data";
		}else if (xmlPath=="TotCholes"){
			return "field_data";
		}else if (xmlPath=="TotCholesUnits"){
			return "field_data";
		}else if (xmlPath=="HDL"){
			return "field_data";
		}else if (xmlPath=="HDLUnits"){
			return "field_data";
		}else if (xmlPath=="LDL"){
			return "field_data";
		}else if (xmlPath=="LDLUnits"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<dian:BLDLIPID";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</dian:BLDLIPID>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Bldcollsess!=null){
			xmlTxt+="\n<dian:bldCollSess";
			xmlTxt+=">";
			xmlTxt+=this.Bldcollsess.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:bldCollSess>";
		}
		if (this.Triglyc!=null){
			xmlTxt+="\n<dian:Triglyc";
			xmlTxt+=">";
			xmlTxt+=this.Triglyc;
			xmlTxt+="</dian:Triglyc>";
		}
		if (this.Triglycunits!=null){
			xmlTxt+="\n<dian:TriglycUnits";
			xmlTxt+=">";
			xmlTxt+=this.Triglycunits.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:TriglycUnits>";
		}
		if (this.Totcholes!=null){
			xmlTxt+="\n<dian:TotCholes";
			xmlTxt+=">";
			xmlTxt+=this.Totcholes;
			xmlTxt+="</dian:TotCholes>";
		}
		if (this.Totcholesunits!=null){
			xmlTxt+="\n<dian:TotCholesUnits";
			xmlTxt+=">";
			xmlTxt+=this.Totcholesunits.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:TotCholesUnits>";
		}
		if (this.Hdl!=null){
			xmlTxt+="\n<dian:HDL";
			xmlTxt+=">";
			xmlTxt+=this.Hdl;
			xmlTxt+="</dian:HDL>";
		}
		if (this.Hdlunits!=null){
			xmlTxt+="\n<dian:HDLUnits";
			xmlTxt+=">";
			xmlTxt+=this.Hdlunits.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:HDLUnits>";
		}
		if (this.Ldl!=null){
			xmlTxt+="\n<dian:LDL";
			xmlTxt+=">";
			xmlTxt+=this.Ldl;
			xmlTxt+="</dian:LDL>";
		}
		if (this.Ldlunits!=null){
			xmlTxt+="\n<dian:LDLUnits";
			xmlTxt+=">";
			xmlTxt+=this.Ldlunits.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:LDLUnits>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Bldcollsess!=null) return true;
		if (this.Triglyc!=null) return true;
		if (this.Triglycunits!=null) return true;
		if (this.Totcholes!=null) return true;
		if (this.Totcholesunits!=null) return true;
		if (this.Hdl!=null) return true;
		if (this.Hdlunits!=null) return true;
		if (this.Ldl!=null) return true;
		if (this.Ldlunits!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
