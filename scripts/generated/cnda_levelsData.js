/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cnda_levelsData(){
this.xsiType="cnda:levelsData";

	this.getSchemaElementName=function(){
		return "levelsData";
	}

	this.getFullSchemaElementName=function(){
		return "cnda:levelsData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Plasmald=null;


	function getPlasmald() {
		return this.Plasmald;
	}
	this.getPlasmald=getPlasmald;


	function setPlasmald(v){
		this.Plasmald=v;
	}
	this.setPlasmald=setPlasmald;

	this.Plasmada=null;


	function getPlasmada() {
		return this.Plasmada;
	}
	this.getPlasmada=getPlasmada;


	function setPlasmada(v){
		this.Plasmada=v;
	}
	this.setPlasmada=setPlasmada;

	this.Plasma3ome=null;


	function getPlasma3ome() {
		return this.Plasma3ome;
	}
	this.getPlasma3ome=getPlasma3ome;


	function setPlasma3ome(v){
		this.Plasma3ome=v;
	}
	this.setPlasma3ome=setPlasma3ome;

	this.Plasmadc=null;


	function getPlasmadc() {
		return this.Plasmadc;
	}
	this.getPlasmadc=getPlasmadc;


	function setPlasmadc(v){
		this.Plasmadc=v;
	}
	this.setPlasmadc=setPlasmadc;

	this.Plasmacd=null;


	function getPlasmacd() {
		return this.Plasmacd;
	}
	this.getPlasmacd=getPlasmacd;


	function setPlasmacd(v){
		this.Plasmacd=v;
	}
	this.setPlasmacd=setPlasmacd;

	this.Problem=null;


	function getProblem() {
		return this.Problem;
	}
	this.getProblem=getProblem;


	function setProblem(v){
		this.Problem=v;
	}
	this.setProblem=setProblem;


	this.isProblem=function(defaultValue) {
		if(this.Problem==null)return defaultValue;
		if(this.Problem=="1" || this.Problem==true)return true;
		return false;
	}

	this.Questionableld=null;


	function getQuestionableld() {
		return this.Questionableld;
	}
	this.getQuestionableld=getQuestionableld;


	function setQuestionableld(v){
		this.Questionableld=v;
	}
	this.setQuestionableld=setQuestionableld;


	this.isQuestionableld=function(defaultValue) {
		if(this.Questionableld==null)return defaultValue;
		if(this.Questionableld=="1" || this.Questionableld==true)return true;
		return false;
	}

	this.Questionableda=null;


	function getQuestionableda() {
		return this.Questionableda;
	}
	this.getQuestionableda=getQuestionableda;


	function setQuestionableda(v){
		this.Questionableda=v;
	}
	this.setQuestionableda=setQuestionableda;


	this.isQuestionableda=function(defaultValue) {
		if(this.Questionableda==null)return defaultValue;
		if(this.Questionableda=="1" || this.Questionableda==true)return true;
		return false;
	}

	this.Questionable3ome=null;


	function getQuestionable3ome() {
		return this.Questionable3ome;
	}
	this.getQuestionable3ome=getQuestionable3ome;


	function setQuestionable3ome(v){
		this.Questionable3ome=v;
	}
	this.setQuestionable3ome=setQuestionable3ome;


	this.isQuestionable3ome=function(defaultValue) {
		if(this.Questionable3ome==null)return defaultValue;
		if(this.Questionable3ome=="1" || this.Questionable3ome==true)return true;
		return false;
	}

	this.Questionabledc=null;


	function getQuestionabledc() {
		return this.Questionabledc;
	}
	this.getQuestionabledc=getQuestionabledc;


	function setQuestionabledc(v){
		this.Questionabledc=v;
	}
	this.setQuestionabledc=setQuestionabledc;


	this.isQuestionabledc=function(defaultValue) {
		if(this.Questionabledc==null)return defaultValue;
		if(this.Questionabledc=="1" || this.Questionabledc==true)return true;
		return false;
	}

	this.Questionablecd=null;


	function getQuestionablecd() {
		return this.Questionablecd;
	}
	this.getQuestionablecd=getQuestionablecd;


	function setQuestionablecd(v){
		this.Questionablecd=v;
	}
	this.setQuestionablecd=setQuestionablecd;


	this.isQuestionablecd=function(defaultValue) {
		if(this.Questionablecd==null)return defaultValue;
		if(this.Questionablecd=="1" || this.Questionablecd==true)return true;
		return false;
	}

	this.Knownforivld1=null;


	function getKnownforivld1() {
		return this.Knownforivld1;
	}
	this.getKnownforivld1=getKnownforivld1;


	function setKnownforivld1(v){
		this.Knownforivld1=v;
	}
	this.setKnownforivld1=setKnownforivld1;


	this.isKnownforivld1=function(defaultValue) {
		if(this.Knownforivld1==null)return defaultValue;
		if(this.Knownforivld1=="1" || this.Knownforivld1==true)return true;
		return false;
	}

	this.Rejectedforivld1=null;


	function getRejectedforivld1() {
		return this.Rejectedforivld1;
	}
	this.getRejectedforivld1=getRejectedforivld1;


	function setRejectedforivld1(v){
		this.Rejectedforivld1=v;
	}
	this.setRejectedforivld1=setRejectedforivld1;


	this.isRejectedforivld1=function(defaultValue) {
		if(this.Rejectedforivld1==null)return defaultValue;
		if(this.Rejectedforivld1=="1" || this.Rejectedforivld1==true)return true;
		return false;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="plasmaLD"){
				return this.Plasmald ;
			} else 
			if(xmlPath=="plasmaDA"){
				return this.Plasmada ;
			} else 
			if(xmlPath=="plasma3OMe"){
				return this.Plasma3ome ;
			} else 
			if(xmlPath=="plasmaDC"){
				return this.Plasmadc ;
			} else 
			if(xmlPath=="plasmaCD"){
				return this.Plasmacd ;
			} else 
			if(xmlPath=="problem"){
				return this.Problem ;
			} else 
			if(xmlPath=="questionableLD"){
				return this.Questionableld ;
			} else 
			if(xmlPath=="questionableDA"){
				return this.Questionableda ;
			} else 
			if(xmlPath=="questionable3OMe"){
				return this.Questionable3ome ;
			} else 
			if(xmlPath=="questionableDC"){
				return this.Questionabledc ;
			} else 
			if(xmlPath=="questionableCD"){
				return this.Questionablecd ;
			} else 
			if(xmlPath=="knownForIVLD1"){
				return this.Knownforivld1 ;
			} else 
			if(xmlPath=="rejectedForIVLD1"){
				return this.Rejectedforivld1 ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="plasmaLD"){
				this.Plasmald=value;
			} else 
			if(xmlPath=="plasmaDA"){
				this.Plasmada=value;
			} else 
			if(xmlPath=="plasma3OMe"){
				this.Plasma3ome=value;
			} else 
			if(xmlPath=="plasmaDC"){
				this.Plasmadc=value;
			} else 
			if(xmlPath=="plasmaCD"){
				this.Plasmacd=value;
			} else 
			if(xmlPath=="problem"){
				this.Problem=value;
			} else 
			if(xmlPath=="questionableLD"){
				this.Questionableld=value;
			} else 
			if(xmlPath=="questionableDA"){
				this.Questionableda=value;
			} else 
			if(xmlPath=="questionable3OMe"){
				this.Questionable3ome=value;
			} else 
			if(xmlPath=="questionableDC"){
				this.Questionabledc=value;
			} else 
			if(xmlPath=="questionableCD"){
				this.Questionablecd=value;
			} else 
			if(xmlPath=="knownForIVLD1"){
				this.Knownforivld1=value;
			} else 
			if(xmlPath=="rejectedForIVLD1"){
				this.Rejectedforivld1=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="plasmaLD"){
			return "field_data";
		}else if (xmlPath=="plasmaDA"){
			return "field_data";
		}else if (xmlPath=="plasma3OMe"){
			return "field_data";
		}else if (xmlPath=="plasmaDC"){
			return "field_data";
		}else if (xmlPath=="plasmaCD"){
			return "field_data";
		}else if (xmlPath=="problem"){
			return "field_data";
		}else if (xmlPath=="questionableLD"){
			return "field_data";
		}else if (xmlPath=="questionableDA"){
			return "field_data";
		}else if (xmlPath=="questionable3OMe"){
			return "field_data";
		}else if (xmlPath=="questionableDC"){
			return "field_data";
		}else if (xmlPath=="questionableCD"){
			return "field_data";
		}else if (xmlPath=="knownForIVLD1"){
			return "field_data";
		}else if (xmlPath=="rejectedForIVLD1"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cnda:Levels";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cnda:Levels>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Plasmald!=null){
			xmlTxt+="\n<cnda:plasmaLD";
			xmlTxt+=">";
			xmlTxt+=this.Plasmald;
			xmlTxt+="</cnda:plasmaLD>";
		}
		if (this.Plasmada!=null){
			xmlTxt+="\n<cnda:plasmaDA";
			xmlTxt+=">";
			xmlTxt+=this.Plasmada;
			xmlTxt+="</cnda:plasmaDA>";
		}
		if (this.Plasma3ome!=null){
			xmlTxt+="\n<cnda:plasma3OMe";
			xmlTxt+=">";
			xmlTxt+=this.Plasma3ome;
			xmlTxt+="</cnda:plasma3OMe>";
		}
		if (this.Plasmadc!=null){
			xmlTxt+="\n<cnda:plasmaDC";
			xmlTxt+=">";
			xmlTxt+=this.Plasmadc;
			xmlTxt+="</cnda:plasmaDC>";
		}
		if (this.Plasmacd!=null){
			xmlTxt+="\n<cnda:plasmaCD";
			xmlTxt+=">";
			xmlTxt+=this.Plasmacd;
			xmlTxt+="</cnda:plasmaCD>";
		}
		if (this.Problem!=null){
			xmlTxt+="\n<cnda:problem";
			xmlTxt+=">";
			xmlTxt+=this.Problem;
			xmlTxt+="</cnda:problem>";
		}
		if (this.Questionableld!=null){
			xmlTxt+="\n<cnda:questionableLD";
			xmlTxt+=">";
			xmlTxt+=this.Questionableld;
			xmlTxt+="</cnda:questionableLD>";
		}
		if (this.Questionableda!=null){
			xmlTxt+="\n<cnda:questionableDA";
			xmlTxt+=">";
			xmlTxt+=this.Questionableda;
			xmlTxt+="</cnda:questionableDA>";
		}
		if (this.Questionable3ome!=null){
			xmlTxt+="\n<cnda:questionable3OMe";
			xmlTxt+=">";
			xmlTxt+=this.Questionable3ome;
			xmlTxt+="</cnda:questionable3OMe>";
		}
		if (this.Questionabledc!=null){
			xmlTxt+="\n<cnda:questionableDC";
			xmlTxt+=">";
			xmlTxt+=this.Questionabledc;
			xmlTxt+="</cnda:questionableDC>";
		}
		if (this.Questionablecd!=null){
			xmlTxt+="\n<cnda:questionableCD";
			xmlTxt+=">";
			xmlTxt+=this.Questionablecd;
			xmlTxt+="</cnda:questionableCD>";
		}
		if (this.Knownforivld1!=null){
			xmlTxt+="\n<cnda:knownForIVLD1";
			xmlTxt+=">";
			xmlTxt+=this.Knownforivld1;
			xmlTxt+="</cnda:knownForIVLD1>";
		}
		if (this.Rejectedforivld1!=null){
			xmlTxt+="\n<cnda:rejectedForIVLD1";
			xmlTxt+=">";
			xmlTxt+=this.Rejectedforivld1;
			xmlTxt+="</cnda:rejectedForIVLD1>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Plasmald!=null) return true;
		if (this.Plasmada!=null) return true;
		if (this.Plasma3ome!=null) return true;
		if (this.Plasmadc!=null) return true;
		if (this.Plasmacd!=null) return true;
		if (this.Problem!=null) return true;
		if (this.Questionableld!=null) return true;
		if (this.Questionableda!=null) return true;
		if (this.Questionable3ome!=null) return true;
		if (this.Questionabledc!=null) return true;
		if (this.Questionablecd!=null) return true;
		if (this.Knownforivld1!=null) return true;
		if (this.Rejectedforivld1!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
