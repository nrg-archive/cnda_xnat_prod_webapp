/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:04 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function uds_a1subdemoData(){
this.xsiType="uds:a1subdemoData";

	this.getSchemaElementName=function(){
		return "a1subdemoData";
	}

	this.getFullSchemaElementName=function(){
		return "uds:a1subdemoData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Refer=null;


	function getRefer() {
		return this.Refer;
	}
	this.getRefer=getRefer;


	function setRefer(v){
		this.Refer=v;
	}
	this.setRefer=setRefer;

	this.Referx=null;


	function getReferx() {
		return this.Referx;
	}
	this.getReferx=getReferx;


	function setReferx(v){
		this.Referx=v;
	}
	this.setReferx=setReferx;

	this.Prestat=null;


	function getPrestat() {
		return this.Prestat;
	}
	this.getPrestat=getPrestat;


	function setPrestat(v){
		this.Prestat=v;
	}
	this.setPrestat=setPrestat;

	this.Birthmo=null;


	function getBirthmo() {
		return this.Birthmo;
	}
	this.getBirthmo=getBirthmo;


	function setBirthmo(v){
		this.Birthmo=v;
	}
	this.setBirthmo=setBirthmo;

	this.Birthyr=null;


	function getBirthyr() {
		return this.Birthyr;
	}
	this.getBirthyr=getBirthyr;


	function setBirthyr(v){
		this.Birthyr=v;
	}
	this.setBirthyr=setBirthyr;

	this.Sex=null;


	function getSex() {
		return this.Sex;
	}
	this.getSex=getSex;


	function setSex(v){
		this.Sex=v;
	}
	this.setSex=setSex;

	this.Hispanic=null;


	function getHispanic() {
		return this.Hispanic;
	}
	this.getHispanic=getHispanic;


	function setHispanic(v){
		this.Hispanic=v;
	}
	this.setHispanic=setHispanic;

	this.Hispor=null;


	function getHispor() {
		return this.Hispor;
	}
	this.getHispor=getHispor;


	function setHispor(v){
		this.Hispor=v;
	}
	this.setHispor=setHispor;

	this.Hisporx=null;


	function getHisporx() {
		return this.Hisporx;
	}
	this.getHisporx=getHisporx;


	function setHisporx(v){
		this.Hisporx=v;
	}
	this.setHisporx=setHisporx;

	this.Race=null;


	function getRace() {
		return this.Race;
	}
	this.getRace=getRace;


	function setRace(v){
		this.Race=v;
	}
	this.setRace=setRace;

	this.Racex=null;


	function getRacex() {
		return this.Racex;
	}
	this.getRacex=getRacex;


	function setRacex(v){
		this.Racex=v;
	}
	this.setRacex=setRacex;

	this.Racesec=null;


	function getRacesec() {
		return this.Racesec;
	}
	this.getRacesec=getRacesec;


	function setRacesec(v){
		this.Racesec=v;
	}
	this.setRacesec=setRacesec;

	this.Racesecx=null;


	function getRacesecx() {
		return this.Racesecx;
	}
	this.getRacesecx=getRacesecx;


	function setRacesecx(v){
		this.Racesecx=v;
	}
	this.setRacesecx=setRacesecx;

	this.Raceter=null;


	function getRaceter() {
		return this.Raceter;
	}
	this.getRaceter=getRaceter;


	function setRaceter(v){
		this.Raceter=v;
	}
	this.setRaceter=setRaceter;

	this.Raceterx=null;


	function getRaceterx() {
		return this.Raceterx;
	}
	this.getRaceterx=getRaceterx;


	function setRaceterx(v){
		this.Raceterx=v;
	}
	this.setRaceterx=setRaceterx;

	this.Primlang=null;


	function getPrimlang() {
		return this.Primlang;
	}
	this.getPrimlang=getPrimlang;


	function setPrimlang(v){
		this.Primlang=v;
	}
	this.setPrimlang=setPrimlang;

	this.Primlanx=null;


	function getPrimlanx() {
		return this.Primlanx;
	}
	this.getPrimlanx=getPrimlanx;


	function setPrimlanx(v){
		this.Primlanx=v;
	}
	this.setPrimlanx=setPrimlanx;

	this.Livsit=null;


	function getLivsit() {
		return this.Livsit;
	}
	this.getLivsit=getLivsit;


	function setLivsit(v){
		this.Livsit=v;
	}
	this.setLivsit=setLivsit;

	this.Livsitx=null;


	function getLivsitx() {
		return this.Livsitx;
	}
	this.getLivsitx=getLivsitx;


	function setLivsitx(v){
		this.Livsitx=v;
	}
	this.setLivsitx=setLivsitx;

	this.Independ=null;


	function getIndepend() {
		return this.Independ;
	}
	this.getIndepend=getIndepend;


	function setIndepend(v){
		this.Independ=v;
	}
	this.setIndepend=setIndepend;

	this.Residenc=null;


	function getResidenc() {
		return this.Residenc;
	}
	this.getResidenc=getResidenc;


	function setResidenc(v){
		this.Residenc=v;
	}
	this.setResidenc=setResidenc;

	this.Residenx=null;


	function getResidenx() {
		return this.Residenx;
	}
	this.getResidenx=getResidenx;


	function setResidenx(v){
		this.Residenx=v;
	}
	this.setResidenx=setResidenx;

	this.Zip=null;


	function getZip() {
		return this.Zip;
	}
	this.getZip=getZip;


	function setZip(v){
		this.Zip=v;
	}
	this.setZip=setZip;

	this.Maristat=null;


	function getMaristat() {
		return this.Maristat;
	}
	this.getMaristat=getMaristat;


	function setMaristat(v){
		this.Maristat=v;
	}
	this.setMaristat=setMaristat;

	this.Maristax=null;


	function getMaristax() {
		return this.Maristax;
	}
	this.getMaristax=getMaristax;


	function setMaristax(v){
		this.Maristax=v;
	}
	this.setMaristax=setMaristax;

	this.Handed=null;


	function getHanded() {
		return this.Handed;
	}
	this.getHanded=getHanded;


	function setHanded(v){
		this.Handed=v;
	}
	this.setHanded=setHanded;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="REFER"){
				return this.Refer ;
			} else 
			if(xmlPath=="REFERX"){
				return this.Referx ;
			} else 
			if(xmlPath=="PRESTAT"){
				return this.Prestat ;
			} else 
			if(xmlPath=="BIRTHMO"){
				return this.Birthmo ;
			} else 
			if(xmlPath=="BIRTHYR"){
				return this.Birthyr ;
			} else 
			if(xmlPath=="SEX"){
				return this.Sex ;
			} else 
			if(xmlPath=="HISPANIC"){
				return this.Hispanic ;
			} else 
			if(xmlPath=="HISPOR"){
				return this.Hispor ;
			} else 
			if(xmlPath=="HISPORX"){
				return this.Hisporx ;
			} else 
			if(xmlPath=="RACE"){
				return this.Race ;
			} else 
			if(xmlPath=="RACEX"){
				return this.Racex ;
			} else 
			if(xmlPath=="RACESEC"){
				return this.Racesec ;
			} else 
			if(xmlPath=="RACESECX"){
				return this.Racesecx ;
			} else 
			if(xmlPath=="RACETER"){
				return this.Raceter ;
			} else 
			if(xmlPath=="RACETERX"){
				return this.Raceterx ;
			} else 
			if(xmlPath=="PRIMLANG"){
				return this.Primlang ;
			} else 
			if(xmlPath=="PRIMLANX"){
				return this.Primlanx ;
			} else 
			if(xmlPath=="LIVSIT"){
				return this.Livsit ;
			} else 
			if(xmlPath=="LIVSITX"){
				return this.Livsitx ;
			} else 
			if(xmlPath=="INDEPEND"){
				return this.Independ ;
			} else 
			if(xmlPath=="RESIDENC"){
				return this.Residenc ;
			} else 
			if(xmlPath=="RESIDENX"){
				return this.Residenx ;
			} else 
			if(xmlPath=="ZIP"){
				return this.Zip ;
			} else 
			if(xmlPath=="MARISTAT"){
				return this.Maristat ;
			} else 
			if(xmlPath=="MARISTAX"){
				return this.Maristax ;
			} else 
			if(xmlPath=="HANDED"){
				return this.Handed ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="REFER"){
				this.Refer=value;
			} else 
			if(xmlPath=="REFERX"){
				this.Referx=value;
			} else 
			if(xmlPath=="PRESTAT"){
				this.Prestat=value;
			} else 
			if(xmlPath=="BIRTHMO"){
				this.Birthmo=value;
			} else 
			if(xmlPath=="BIRTHYR"){
				this.Birthyr=value;
			} else 
			if(xmlPath=="SEX"){
				this.Sex=value;
			} else 
			if(xmlPath=="HISPANIC"){
				this.Hispanic=value;
			} else 
			if(xmlPath=="HISPOR"){
				this.Hispor=value;
			} else 
			if(xmlPath=="HISPORX"){
				this.Hisporx=value;
			} else 
			if(xmlPath=="RACE"){
				this.Race=value;
			} else 
			if(xmlPath=="RACEX"){
				this.Racex=value;
			} else 
			if(xmlPath=="RACESEC"){
				this.Racesec=value;
			} else 
			if(xmlPath=="RACESECX"){
				this.Racesecx=value;
			} else 
			if(xmlPath=="RACETER"){
				this.Raceter=value;
			} else 
			if(xmlPath=="RACETERX"){
				this.Raceterx=value;
			} else 
			if(xmlPath=="PRIMLANG"){
				this.Primlang=value;
			} else 
			if(xmlPath=="PRIMLANX"){
				this.Primlanx=value;
			} else 
			if(xmlPath=="LIVSIT"){
				this.Livsit=value;
			} else 
			if(xmlPath=="LIVSITX"){
				this.Livsitx=value;
			} else 
			if(xmlPath=="INDEPEND"){
				this.Independ=value;
			} else 
			if(xmlPath=="RESIDENC"){
				this.Residenc=value;
			} else 
			if(xmlPath=="RESIDENX"){
				this.Residenx=value;
			} else 
			if(xmlPath=="ZIP"){
				this.Zip=value;
			} else 
			if(xmlPath=="MARISTAT"){
				this.Maristat=value;
			} else 
			if(xmlPath=="MARISTAX"){
				this.Maristax=value;
			} else 
			if(xmlPath=="HANDED"){
				this.Handed=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="REFER"){
			return "field_data";
		}else if (xmlPath=="REFERX"){
			return "field_data";
		}else if (xmlPath=="PRESTAT"){
			return "field_data";
		}else if (xmlPath=="BIRTHMO"){
			return "field_data";
		}else if (xmlPath=="BIRTHYR"){
			return "field_data";
		}else if (xmlPath=="SEX"){
			return "field_data";
		}else if (xmlPath=="HISPANIC"){
			return "field_data";
		}else if (xmlPath=="HISPOR"){
			return "field_data";
		}else if (xmlPath=="HISPORX"){
			return "field_data";
		}else if (xmlPath=="RACE"){
			return "field_data";
		}else if (xmlPath=="RACEX"){
			return "field_data";
		}else if (xmlPath=="RACESEC"){
			return "field_data";
		}else if (xmlPath=="RACESECX"){
			return "field_data";
		}else if (xmlPath=="RACETER"){
			return "field_data";
		}else if (xmlPath=="RACETERX"){
			return "field_data";
		}else if (xmlPath=="PRIMLANG"){
			return "field_data";
		}else if (xmlPath=="PRIMLANX"){
			return "field_data";
		}else if (xmlPath=="LIVSIT"){
			return "field_data";
		}else if (xmlPath=="LIVSITX"){
			return "field_data";
		}else if (xmlPath=="INDEPEND"){
			return "field_data";
		}else if (xmlPath=="RESIDENC"){
			return "field_data";
		}else if (xmlPath=="RESIDENX"){
			return "field_data";
		}else if (xmlPath=="ZIP"){
			return "field_data";
		}else if (xmlPath=="MARISTAT"){
			return "field_data";
		}else if (xmlPath=="MARISTAX"){
			return "field_data";
		}else if (xmlPath=="HANDED"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<uds:A1SUBDEMO";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</uds:A1SUBDEMO>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Refer!=null){
			xmlTxt+="\n<uds:REFER";
			xmlTxt+=">";
			xmlTxt+=this.Refer;
			xmlTxt+="</uds:REFER>";
		}
		if (this.Referx!=null){
			xmlTxt+="\n<uds:REFERX";
			xmlTxt+=">";
			xmlTxt+=this.Referx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:REFERX>";
		}
		if (this.Prestat!=null){
			xmlTxt+="\n<uds:PRESTAT";
			xmlTxt+=">";
			xmlTxt+=this.Prestat;
			xmlTxt+="</uds:PRESTAT>";
		}
		if (this.Birthmo!=null){
			xmlTxt+="\n<uds:BIRTHMO";
			xmlTxt+=">";
			xmlTxt+=this.Birthmo;
			xmlTxt+="</uds:BIRTHMO>";
		}
		if (this.Birthyr!=null){
			xmlTxt+="\n<uds:BIRTHYR";
			xmlTxt+=">";
			xmlTxt+=this.Birthyr;
			xmlTxt+="</uds:BIRTHYR>";
		}
		if (this.Sex!=null){
			xmlTxt+="\n<uds:SEX";
			xmlTxt+=">";
			xmlTxt+=this.Sex;
			xmlTxt+="</uds:SEX>";
		}
		if (this.Hispanic!=null){
			xmlTxt+="\n<uds:HISPANIC";
			xmlTxt+=">";
			xmlTxt+=this.Hispanic;
			xmlTxt+="</uds:HISPANIC>";
		}
		if (this.Hispor!=null){
			xmlTxt+="\n<uds:HISPOR";
			xmlTxt+=">";
			xmlTxt+=this.Hispor;
			xmlTxt+="</uds:HISPOR>";
		}
		if (this.Hisporx!=null){
			xmlTxt+="\n<uds:HISPORX";
			xmlTxt+=">";
			xmlTxt+=this.Hisporx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:HISPORX>";
		}
		if (this.Race!=null){
			xmlTxt+="\n<uds:RACE";
			xmlTxt+=">";
			xmlTxt+=this.Race;
			xmlTxt+="</uds:RACE>";
		}
		if (this.Racex!=null){
			xmlTxt+="\n<uds:RACEX";
			xmlTxt+=">";
			xmlTxt+=this.Racex.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:RACEX>";
		}
		if (this.Racesec!=null){
			xmlTxt+="\n<uds:RACESEC";
			xmlTxt+=">";
			xmlTxt+=this.Racesec;
			xmlTxt+="</uds:RACESEC>";
		}
		if (this.Racesecx!=null){
			xmlTxt+="\n<uds:RACESECX";
			xmlTxt+=">";
			xmlTxt+=this.Racesecx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:RACESECX>";
		}
		if (this.Raceter!=null){
			xmlTxt+="\n<uds:RACETER";
			xmlTxt+=">";
			xmlTxt+=this.Raceter;
			xmlTxt+="</uds:RACETER>";
		}
		if (this.Raceterx!=null){
			xmlTxt+="\n<uds:RACETERX";
			xmlTxt+=">";
			xmlTxt+=this.Raceterx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:RACETERX>";
		}
		if (this.Primlang!=null){
			xmlTxt+="\n<uds:PRIMLANG";
			xmlTxt+=">";
			xmlTxt+=this.Primlang;
			xmlTxt+="</uds:PRIMLANG>";
		}
		if (this.Primlanx!=null){
			xmlTxt+="\n<uds:PRIMLANX";
			xmlTxt+=">";
			xmlTxt+=this.Primlanx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:PRIMLANX>";
		}
		if (this.Livsit!=null){
			xmlTxt+="\n<uds:LIVSIT";
			xmlTxt+=">";
			xmlTxt+=this.Livsit;
			xmlTxt+="</uds:LIVSIT>";
		}
		if (this.Livsitx!=null){
			xmlTxt+="\n<uds:LIVSITX";
			xmlTxt+=">";
			xmlTxt+=this.Livsitx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:LIVSITX>";
		}
		if (this.Independ!=null){
			xmlTxt+="\n<uds:INDEPEND";
			xmlTxt+=">";
			xmlTxt+=this.Independ;
			xmlTxt+="</uds:INDEPEND>";
		}
		if (this.Residenc!=null){
			xmlTxt+="\n<uds:RESIDENC";
			xmlTxt+=">";
			xmlTxt+=this.Residenc;
			xmlTxt+="</uds:RESIDENC>";
		}
		if (this.Residenx!=null){
			xmlTxt+="\n<uds:RESIDENX";
			xmlTxt+=">";
			xmlTxt+=this.Residenx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:RESIDENX>";
		}
		if (this.Zip!=null){
			xmlTxt+="\n<uds:ZIP";
			xmlTxt+=">";
			xmlTxt+=this.Zip.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:ZIP>";
		}
		if (this.Maristat!=null){
			xmlTxt+="\n<uds:MARISTAT";
			xmlTxt+=">";
			xmlTxt+=this.Maristat;
			xmlTxt+="</uds:MARISTAT>";
		}
		if (this.Maristax!=null){
			xmlTxt+="\n<uds:MARISTAX";
			xmlTxt+=">";
			xmlTxt+=this.Maristax.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:MARISTAX>";
		}
		if (this.Handed!=null){
			xmlTxt+="\n<uds:HANDED";
			xmlTxt+=">";
			xmlTxt+=this.Handed;
			xmlTxt+="</uds:HANDED>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Refer!=null) return true;
		if (this.Referx!=null) return true;
		if (this.Prestat!=null) return true;
		if (this.Birthmo!=null) return true;
		if (this.Birthyr!=null) return true;
		if (this.Sex!=null) return true;
		if (this.Hispanic!=null) return true;
		if (this.Hispor!=null) return true;
		if (this.Hisporx!=null) return true;
		if (this.Race!=null) return true;
		if (this.Racex!=null) return true;
		if (this.Racesec!=null) return true;
		if (this.Racesecx!=null) return true;
		if (this.Raceter!=null) return true;
		if (this.Raceterx!=null) return true;
		if (this.Primlang!=null) return true;
		if (this.Primlanx!=null) return true;
		if (this.Livsit!=null) return true;
		if (this.Livsitx!=null) return true;
		if (this.Independ!=null) return true;
		if (this.Residenc!=null) return true;
		if (this.Residenx!=null) return true;
		if (this.Zip!=null) return true;
		if (this.Maristat!=null) return true;
		if (this.Maristax!=null) return true;
		if (this.Handed!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
