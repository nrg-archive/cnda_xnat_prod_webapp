/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:03 CDT 2017
 *
 */

/**
 * @author XDAT
 *
 */

function iq_iqAssessmentData(){
this.xsiType="iq:iqAssessmentData";

	this.getSchemaElementName=function(){
		return "iqAssessmentData";
	}

	this.getFullSchemaElementName=function(){
		return "iq:iqAssessmentData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Fsiq=null;


	function getFsiq() {
		return this.Fsiq;
	}
	this.getFsiq=getFsiq;


	function setFsiq(v){
		this.Fsiq=v;
	}
	this.setFsiq=setFsiq;

	this.Verbaliq=null;


	function getVerbaliq() {
		return this.Verbaliq;
	}
	this.getVerbaliq=getVerbaliq;


	function setVerbaliq(v){
		this.Verbaliq=v;
	}
	this.setVerbaliq=setVerbaliq;

	this.Performanceiq=null;


	function getPerformanceiq() {
		return this.Performanceiq;
	}
	this.getPerformanceiq=getPerformanceiq;


	function setPerformanceiq(v){
		this.Performanceiq=v;
	}
	this.setPerformanceiq=setPerformanceiq;

	this.Iqsrc=null;


	function getIqsrc() {
		return this.Iqsrc;
	}
	this.getIqsrc=getIqsrc;


	function setIqsrc(v){
		this.Iqsrc=v;
	}
	this.setIqsrc=setIqsrc;
	this.Iqtest =null;
	function getIqtest() {
		return this.Iqtest;
	}
	this.getIqtest=getIqtest;


	function setIqtest(v){
		this.Iqtest =v;
	}
	this.setIqtest=setIqtest;

	this.Iqtest_IqtestIqAbstractiqtestId=null;


	function getIqtest_IqtestIqAbstractiqtestId(){
		return this.Iqtest_IqtestIqAbstractiqtestId;
	}
	this.getIqtest_IqtestIqAbstractiqtestId=getIqtest_IqtestIqAbstractiqtestId;


	function setIqtest_IqtestIqAbstractiqtestId(v){
		this.Iqtest_IqtestIqAbstractiqtestId=v;
	}
	this.setIqtest_IqtestIqAbstractiqtestId=setIqtest_IqtestIqAbstractiqtestId;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="fsiq"){
				return this.Fsiq ;
			} else 
			if(xmlPath=="verbalIq"){
				return this.Verbaliq ;
			} else 
			if(xmlPath=="performanceIq"){
				return this.Performanceiq ;
			} else 
			if(xmlPath=="iqSrc"){
				return this.Iqsrc ;
			} else 
			if(xmlPath=="iqTest"){
				return this.Iqtest ;
			} else 
			if(xmlPath.startsWith("iqTest")){
				xmlPath=xmlPath.substring(6);
				if(xmlPath=="")return this.Iqtest ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Iqtest!=undefined)return this.Iqtest.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="fsiq"){
				this.Fsiq=value;
			} else 
			if(xmlPath=="verbalIq"){
				this.Verbaliq=value;
			} else 
			if(xmlPath=="performanceIq"){
				this.Performanceiq=value;
			} else 
			if(xmlPath=="iqSrc"){
				this.Iqsrc=value;
			} else 
			if(xmlPath=="iqTest"){
				this.Iqtest=value;
			} else 
			if(xmlPath.startsWith("iqTest")){
				xmlPath=xmlPath.substring(6);
				if(xmlPath=="")return this.Iqtest ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Iqtest!=undefined){
					this.Iqtest.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Iqtest= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Iqtest= instanciateObject("iq:abstractIQTest");//omUtils.js
						}
						if(options && options.where)this.Iqtest.setProperty(options.where.field,options.where.value);
						this.Iqtest.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="iqTest"){
			this.setIqtest(v);
		}
		else{
			this.extension.setReferenceField(xmlPath,v);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="iqTest"){
			return "http://nrg.wustl.edu/iq:abstractIQTest";
		}
		else{
			return this.extension.getReferenceFieldName(xmlPath);
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="fsiq"){
			return "field_data";
		}else if (xmlPath=="verbalIq"){
			return "field_data";
		}else if (xmlPath=="performanceIq"){
			return "field_data";
		}else if (xmlPath=="iqSrc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="iqTest"){
			return "field_single_reference";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<iq:IQAssessment";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mayo=\"http://nrg.wustl.edu/mayo\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wmh=\"http://nrg.wustl.edu/wmh\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</iq:IQAssessment>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Fsiq!=null){
			xmlTxt+="\n<iq:fsiq";
			xmlTxt+=">";
			xmlTxt+=this.Fsiq;
			xmlTxt+="</iq:fsiq>";
		}
		if (this.Verbaliq!=null){
			xmlTxt+="\n<iq:verbalIq";
			xmlTxt+=">";
			xmlTxt+=this.Verbaliq;
			xmlTxt+="</iq:verbalIq>";
		}
		if (this.Performanceiq!=null){
			xmlTxt+="\n<iq:performanceIq";
			xmlTxt+=">";
			xmlTxt+=this.Performanceiq;
			xmlTxt+="</iq:performanceIq>";
		}
		if (this.Iqsrc!=null){
			xmlTxt+="\n<iq:iqSrc";
			xmlTxt+=">";
			xmlTxt+=this.Iqsrc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</iq:iqSrc>";
		}
		if (this.Iqtest!=null){
			xmlTxt+="\n<iq:iqTest";
			xmlTxt+=this.Iqtest.getXMLAtts();
			if(this.Iqtest.xsiType!="iq:abstractIQTest"){
				xmlTxt+=" xsi:type=\"" + this.Iqtest.xsiType + "\"";
			}
			if (this.Iqtest.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Iqtest.getXMLBody(preventComments);
				xmlTxt+="</iq:iqTest>";
			}else {xmlTxt+="/>";}
		}
		//NOT REQUIRED

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Fsiq!=null) return true;
		if (this.Verbaliq!=null) return true;
		if (this.Performanceiq!=null) return true;
		if (this.Iqsrc!=null) return true;
		if (this.Iqtest!=null){
			if (this.Iqtest.hasXMLBodyContent()) return true;
		}
		//NOT REQUIRED

		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
