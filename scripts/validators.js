function addConfirmTextToSubmit(inputID, confirmFunction, confirmArguments, confirmMessage) {
	try {
		_addValidation(
			document.getElementById(inputID), 
			new TextboxValidator(
				document.getElementById(inputID),
				{
					message:confirmMessage,
					isValid:function()
					{
						return confirmFunction.apply(null, confirmArguments);
					}
				}
			)
		);
	} catch(e) {
		showMessage("page_body", "Exception", "An exception has occured.  Please contact technical support for assistance.");
		return false;
	}
}

function addConfirmSelectToSubmit(inputID, confirmFunction, confirmArguments, confirmMessage) {
	try {
		_addValidation(
			document.getElementById(inputID), 
			new SelectValidator(
				document.getElementById(inputID),
				{
					message:confirmMessage,
					isValid:function()
					{
						return confirmFunction.apply(null, confirmArguments);
					}
				}
			)
		);
	} catch(e) {
		showMessage("page_body", "Exception", "An exception has occured.  Please contact technical support for assistance.");
		return false;
	}
}


function addConfirmRadioToSubmit(inputID, confirmFunction, confirmArguments, confirmMessage) {
	try {
		_addValidation(
			document.getElementById(inputID), 
			new RadioButtonValidator(
				document.getElementById(inputID),
				{
					message:confirmMessage,
					isValid:function()
					{
						return confirmFunction.apply(null, confirmArguments);
					}
				}
			)
		);
	} catch(e) {
		showMessage("page_body", "Exception", "An exception has occured.  Please contact technical support for assistance.");
		return false;
	}
}


function confirmSessionLabel(_focus, inputID){  
	if(_focus==undefined) {
		_focus = true;
	}
	if(inputID==undefined) {
		inputID = elementName+"/label";
	}
	var valid = true;
  
  try{
	  var labelBox=getValueById(inputID);
	  
	  if(labelBox.obj.validated==undefined) {
		  labelBox.obj.value = fixSessionID(labelBox.obj.value);;
	  }
	  if(labelBox.obj.value.toLowerCase()=="null") {
	  	labelBox.obj.value="";
		labelBox.value="";
	  }
	  if(labelBox.value!="") {
	  	 labelBox.obj.validated=false;
	     removeAppendImage(inputID);
			try{
				if(eval("window.verifyExptId")!=undefined){
					verifyExptId();
				}
			}catch(e){
				if(!e.message.startsWith("verifyExptId is not defined")){
					throw e;
				}
			}
	  }else{
	  	  labelBox.obj.validated=true;
	   	  appendImage(inputID,"/images/checkmarkRed.gif");
	   	  valid=false;
	  }
		
	  if(window.scanSet!=undefined){
		  if(!window.scanSet.validate(_focus)){
		  	  valid=false;
		  }
	  }
  
	  return valid;
  } catch(e) {
	  showMessage("page_body", "Exception", "An exception has occured.  Please contact technical support for assistance.");
  	  return false;
  }
}

function confirmProjectLabel(_focus, inputID){  
	if(_focus==undefined) {
		_focus=true;
	}
	if(inputID==undefined) {
		inputID = elementName+"/project";
	}
  var valid =true;
  
  try{
	  var projBox=getValueById(inputID);
	  if(projBox.value!=""){
	     removeAppendImage(inputID);
	  }else{
	   	  appendImage(inputID,"/images/checkmarkRed.gif");
	   	  valid=false;
	  }
	  
	  if(window.scanSet!=undefined){
		  if(!window.scanSet.validate(_focus)){
		  	  valid=false;
		  }
	  }
  
	  return valid;
  }catch(e){
  	  showMessage("page_body", "Exception", "An exception has occured.  Please contact technical support for assistance.");
  	  return false;
  }
}

function confirmSubjectLabel(_focus, inputID){  
	if(_focus==undefined) {
		_focus=true;
	}
	if(inputID==undefined) {
		inputID = elementName+"/subject_id";
	}
  var valid =true;
  
  try{
	  var subBox=getValueById(inputID);
	  if(subBox.value!=""){
	  	 if(subBox.obj.selectedIndex!=undefined){
	  	 	if(subBox.obj.options[subBox.obj.selectedIndex].style.color=="red"){
	  	 		document.getElementById("subj_msg").innerHTML="* This " + XNAT.app.displayNames.singular.subject.toLowerCase() + " does not exist, and will be automatically created.  To populate demographic details for this " + XNAT.app.displayNames.singular.subject.toLowerCase() + " please use the 'Add New " + XNAT.app.displayNames.singular.subject + "' link.";
	  	 	}else{
	  	 		document.getElementById("subj_msg").innerHTML="";
	  	 	}
	  	 }
	     removeAppendImage(inputID);
	  }else{
	   	  appendImage(inputID,"/images/checkmarkRed.gif");
	   	  valid=false;
	  }

	  if(window.scanSet!=undefined){
		  if(!window.scanSet.validate(_focus)){
		  	  valid=false;
		  }
	  }
  
	  return valid;
  }catch(e){
  	  showMessage("page_body", "Exception", "An exception has occured.  Please contact technical support for assistance.");
  	  return false;
  }
}

function fixSessionID(val)
{
        var temp = val.trim();
        var newVal = '';
        temp = temp.split(' ');
        for(var c=0; c < temp.length; c++) {
                newVal += '' + temp[c];
        }
        
        newVal = newVal.replace(/[&]/,"_");
        newVal = newVal.replace(/[?]/,"_");
        newVal = newVal.replace(/[<]/,"_");
        newVal = newVal.replace(/[>]/,"_");
        newVal = newVal.replace(/[(]/,"_");
        newVal = newVal.replace(/[)]/,"_");
        newVal = newVal.replace(/[.]/,"_");
        newVal = newVal.replace(/[,]/,"_");
        newVal = newVal.replace(/[\^]/,"_");
        newVal = newVal.replace(/[@]/,"_");
        newVal = newVal.replace(/[!]/,"_");
        newVal = newVal.replace(/[%]/,"_");
        newVal = newVal.replace(/[*]/,"_");
        newVal = newVal.replace(/[#]/,"_");
        newVal = newVal.replace(/[$]/,"_");
        newVal = newVal.replace(/[\\]/,"_");
        newVal = newVal.replace(/[|]/,"_");
        newVal = newVal.replace(/[=]/,"_");
        newVal = newVal.replace(/[+]/,"_");
        newVal = newVal.replace(/[']/,"_");
        newVal = newVal.replace(/["]/,"_");
        newVal = newVal.replace(/[~]/,"_");
        newVal = newVal.replace(/[`]/,"_");
        newVal = newVal.replace(/[:]/,"_");
        newVal = newVal.replace(/[;]/,"_");
        newVal = newVal.replace(/[\/]/,"_");
        newVal = newVal.replace(/[\[]/,"_");
        newVal = newVal.replace(/[\]]/,"_");
        newVal = newVal.replace(/[{]/,"_");
        newVal = newVal.replace(/[}]/,"_");
        if(newVal!=temp){
      	  //alert("Removing invalid characters in " + XNAT.app.displayNames.singular.imageSession.toLowerCase() + ".");
        	// id of element message shows up in, title, message content
        	showMessage("page_body", "Exception", "Removing invalid characters in " + XNAT.app.displayNames.singular.imageSession.toLowerCase() + ".");
        }
        return newVal;
}
