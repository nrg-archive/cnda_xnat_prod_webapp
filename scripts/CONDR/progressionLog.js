/*
 * CONDR Progression Log
 *
 *
 *
 *
 */




// make sure XNAT and XNAT.app objects exist
if (typeof XNAT == 'undefined') var XNAT={};
if (typeof XNAT.app == 'undefined') XNAT.app={};
if (typeof XNAT.data == 'undefined') XNAT.data={};
if (typeof XNAT.utils == 'undefined') XNAT.utils={};
//var CONDR ;
if (typeof CONDR == 'undefined') var CONDR={};
if (typeof CONDR.progressionLog == 'undefined') CONDR.progressionLog={};

var $body, $prog_log_entries, $notes ;

(function(){

    if (!window.jQuery) return ; // we need jQuery
    var $ = jQuery ; // what else would $ be used for? just making sure.

    //////////////////////////////////////////////////
    // XNAT UTILITY FUNCTIONS
    //////////////////////////////////////////////////

    function getScriptDir(){
        var src = $('script[src]').last().attr('src');
        var path = src.split('/');
        path.splice(path.length - 1, 1);
        return path.join('/') + '/';
    }

    var script_path = getScriptDir();

    function appendCSS( _path, _filename ){
        _path = ( _path !== null ) ? _path : getScriptDir();
        if (!$('link[href*="' + _filename + '"]').length) {
            $('head').append('<link type="text/css" rel="stylesheet" href="' + _path + _filename + '">');
        }
    }

    appendCSS( script_path, 'progressionLog.css' );

    // if it's a jQuery DOM object, let it through, otherwise make it one
    // this only works for selectors
    function jQueryDOMobj(o){
        return (o.jquery) ? o : jQuery(o) ;
    }

    // if it's a plain object, let it through, otherwise send an empty object
    function jQueryPlainObj(o){
        return (jQuery.isPlainObject(o)) ? o : {} ;
    }

    //////////////////////////////////////////////////




    //////////////////////////////////////////////////
    // GET RESULTSET.RESULT ARRAY
    //////////////////////////////////////////////////
    // fetch the existing records
    // if '_array' paramater is passed (with an existing array)
    // the ResultSet.Result array will be appended to it
    // warning - we do not check for duplicates here
    XNAT.app.getResultSetResultArray = function( _url, _array, _async, _sort_by, _callbacks ){

        _url = _url || '';
        _array = _array || [];
        _async = _async || false ;
        _sort_by = _sort_by || null ;

        // if the _async parameter is either 'async' or true, make it async, otherwise not
        var async_ = ( _async == 'async' );

        if (_url > ''){
            var doAJAX = $.ajax({
                type: 'GET',
                url: _url,
                dataType: 'json',
                async: async_,
                success: function(json){
                    $.each(json.ResultSet.Result, function(){
                        _array.push(this);
                    });
                }
            });
            // callbacks with returned ajax data
            if (typeof _callbacks == 'object'){
                if (typeof _callbacks.done == 'function'){
                    doAJAX.done(function( data, textStatus, jqXHR ){
                        _callbacks.done( data, textStatus, jqXHR );
                    });
                }
                if (typeof _callbacks.fail == 'function'){
                    doAJAX.fail(function( data, textStatus, error ){
                        _callbacks.fail( data, textStatus, error );
                    });
                }
                if (typeof _callbacks.always == 'function'){
                    doAJAX.always(function( data_or_jqXHR, textStatus, jqXHR_or_error ){
                        _callbacks.always( data_or_jqXHR, textStatus, jqXHR_or_error );
                    });
                }
            }
            // sort the _array
            if (_sort_by !== null && _sort_by > ''){
                _array.sort(function(a,b){
                    if (a[_sort_by] > b[_sort_by]){
                        return 1;
                    }
                    if (a[_sort_by] < b[_sort_by]){
                        return -1;
                    }
                    return 0;
                });
            }
        }
        // we can do something with the data above, or return the array here
        return _array ;
    };
    //////////////////////////////////////////////////




    CONDR.progressionLog.recordCount = 0 ;




    //////////////////////////////////////////////////
    // SET UP URI TO GET PROGRESSION LOG COLLECTION
    //////////////////////////////////////////////////
    CONDR.progressionLog.getCollURI = function( saCollID ){
        var xsiType = 'condr:progressionLogEntry';
        return serverRoot +
            '/data' +
            '/experiments' +
            '?' + xsiType + '/saCollID=' + saCollID +
            '&xsiType=' + xsiType +
            '&columns='
            + 'ID,'
            + 'label,'
            + 'date,'
            + 'insert_date,'
            + 'note,'
            + xsiType + '/saCollID,'
            + xsiType + '/saLinkID,'
            + xsiType + '/type,'
            //+ xsiType + '/confirmedProgressionDate,'
            + xsiType + '/kpsScore,'
            + xsiType + '/treatmentChange,'
            + xsiType + '/imaging,'
            + xsiType + '/steroidDose,'
            + xsiType + '/steroidDoseAmount,'
            + xsiType + '/functionalStatus,'
            + xsiType + '/newNeurologicalDefecit,'
            + xsiType + '/motorWeakness,'
            + xsiType + '/sensoryChange,'
            + xsiType + '/severeHeadache,'
            + xsiType + '/nausea,'
            + xsiType + '/visualLoss,'
            + xsiType + '/cognitiveDecline,'
            + xsiType + '/clinicalDeterioration,'
            + xsiType + '/otherNeurologicalDefecit' +
            '&format=json';
    };
    //////////////////////////////////////////////////




    //////////////////////////////////////////////////
    // GET THE COLLECTION (ALL OF THE ENTRY RECORDS)
    //////////////////////////////////////////////////
    // set saCollID in the velocity template and run this function from there
    CONDR.progressionLog.getCollection = function( saCollID ){
        saCollID = saCollID || XNAT.data.context.projectID ;
        return CONDR.progressionLog.collection = XNAT.app.getResultSetResultArray( CONDR.progressionLog.getCollURI(saCollID), [], false, 'date' );
    };
    //////////////////////////////////////////////////




    //////////////////////////////////////////////////
    // SET UP URI TO GET PROGRESSION LOG ENTRY
    //////////////////////////////////////////////////
    CONDR.progressionLog.getEntryURI = function( exptID ){  // exptID is ID for a single log ENTRY
        var xsiType = 'condr:progressionLogEntry';
        return serverRoot +
            '/data' +
            '/experiments' +
            '?ID=' + exptID +
            '&columns='
            + 'ID,'
            + 'label,'
            + 'date,'
            + 'insert_date,'
            + 'note,'
            + xsiType + '/saCollID,'
            + xsiType + '/saLinkID,'
            + xsiType + '/type,'
            //+ xsiType + '/confirmedProgressionDate,'
            + xsiType + '/kpsScore,'
            + xsiType + '/treatmentChange,'
            + xsiType + '/imaging,'
            + xsiType + '/steroidDose,'
            + xsiType + '/steroidDoseAmount,'
            + xsiType + '/functionalStatus,'
            + xsiType + '/newNeurologicalDefecit,'
            + xsiType + '/motorWeakness,'
            + xsiType + '/sensoryChange,'
            + xsiType + '/severeHeadache,'
            + xsiType + '/nausea,'
            + xsiType + '/visualLoss,'
            + xsiType + '/cognitiveDecline,'
            + xsiType + '/clinicalDeterioration,'
            + xsiType + '/otherNeurologicalDefecit' +
            '&format=json';
    };
    //////////////////////////////////////////////////




    //////////////////////////////////////////////////
    // GET SINGLE ENTRY DATA FOR EDITING
    //////////////////////////////////////////////////
    // set saCollID in the velocity template and run this function from there
    CONDR.progressionLog.getEntryData = function( exptID ){
        var entryData = null ;
        if (typeof exptID != 'undefined') {
            var getRecord = $.ajax({
                type: 'GET',
                url: CONDR.progressionLog.getEntryURI(exptID),
                async: false
            });
            getRecord.done(function(json){
                entryData = json.ResultSet.Result[0];
            });
            getRecord.fail(function(data, status, error){
                xModalMessage('error','data: '+data+'<br><br> status: '+status+'<br><br> error: '+error);
                xModalLoadingClose();
                entryData = null ;
            });
        }
        return CONDR.progressionLog.entryData = entryData ;
    };
    //////////////////////////////////////////////////




    CONDR.progressionLog.newRecords=[];
    CONDR.progressionLog.changedRecords=[];




    //////////////////////////////////////////////////
    // STYLE NEW AND/OR EDITED RECORDS
    //////////////////////////////////////////////////
    CONDR.progressionLog.theseNewRecords = function( _newExptID ){

        if (typeof _newExptID == 'undefined') return ; // *NEED* THE NEW ID!

        if (CONDR.progressionLog.newRecords.indexOf(_newExptID) === -1){
            CONDR.progressionLog.newRecords.push(_newExptID)
        }

        var these_records, $these_records;

        if (CONDR.progressionLog.newRecords > []){
            these_records = '#'+CONDR.progressionLog.newRecords.join('.record, #')+'.record';
            $these_records = $(these_records);
            $these_records.addClass('new');
            //$these_records.css('color','#180');
            //alert(these_records);
        }

        if (CONDR.progressionLog.changedRecords > []){
            these_records = '#'+CONDR.progressionLog.changedRecords.join('.record, #')+'.record';
            $these_records = $(these_records);
            $these_records.addClass('changed');
            //$these_records.css('color','#180');
            //alert(these_records);
        }

    };
    //////////////////////////////////////////////////




    //////////////////////////////////////////////////
    // RENDER THE PROGRESSION LOG (COLLECTION) NOTES
    //////////////////////////////////////////////////
    // this is called after notes are edited/saved
    CONDR.progressionLog.renderNotes = function( _id ){
        _id = _id || CONDR.progressionLog.collID ;
        var getNotes = $.ajax({
            type: 'GET',
            cache: false,
            async: true,
            url: serverRoot +
                '/data' +
                '/experiments?ID=' + _id +
                '&columns=note' +
                '&format=json'
        });
        getNotes.done(function(json){
            var waitForJSON = setInterval(function(){
                if (json > ''){
                    clearInterval(waitForJSON);
                    var data = json.ResultSet.Result[0];
                    var notes = decodeURIComponent(data.note.replace(/%/g,'%25'));
                    if (notes > ''){
                        $('#add_prog_log_notes').hide();
                        $notes.find('p').html(notes);
                        $notes.show();
                    }
                    else {
                        $('#add_prog_log_notes').show();
                        $notes.find('p').html('');
                        $notes.hide();
                    }
                    xModalLoadingClose();
                }
            },100);

        });
        getNotes.fail(function(data, status, error){
            xModalMessage('error','data: '+data+'<br><br> status: '+status+'<br><br> error: '+error);
            xModalLoadingClose();
        });
    };
    //////////////////////////////////////////////////




    //////////////////////////////////////////////////
    // RENDER THE PROGRESSION LOG ENTRY RECORDS
    //////////////////////////////////////////////////
    // renderRecords takes an array of the lesion records and spits them into
    // a table inside the $container element (a jQuery DOM object)
    CONDR.progressionLog.renderRecords = function( _array, $container, _callback ){

        //
        // 1) the _array param comes in from AJAX
        // 2) (re?)render the stuff

        //var id                       = 'ID';
        //var label                    = 'label';
        //var uri                      = 'URI';
        //var date                     = 'date';
        //var insert_date              = 'insert_date';
        //var notes                    = 'note';

        var base = 'condr:progressionlogentry';

        var sacollid                 = base + '/sacollid';
        var salinkid                 = base + '/salinkid';
        var type                     = base + '/type';
        //var confirmedprogressiondate = base + '/confirmedprogressiondate';
        var kpsscore                 = base + '/kpsscore';
        var treatmentchange          = base + '/treatmentchange';
        var imaging                  = base + '/imaging';
        var steroiddose              = base + '/steroiddose';
        var steroiddoseamount        = base + '/steroiddoseamount';
        var functionalstatus         = base + '/functionalstatus';
        // defecit stuff
        var newneurologicaldefecit   = base + '/newneurologicaldefecit';
        var motorweakness            = base + '/motorweakness';
        var sensorychange            = base + '/sensorychange';
        var severeheadache           = base + '/severeheadache';
        var nausea                   = base + '/nausea';
        var visualloss               = base + '/visualloss';
        var cognitivedecline         = base + '/cognitivedecline';
        var clinicaldeterioration    = base + '/clinicaldeterioration';
        var otherneurologicaldefecit = base + '/otherneurologicaldefecit';

        // start the HTML
        var records_html = '\n' ;

        var no_records_html = '' +
            '<tr class="no_records">' + '\n' +
            '   <td colspan="8" style="text-align:center;"><p style="margin:20px auto;">(no records)</p></td>' +
            '</tr>' + '\n' +
            '';

            var count=0;

            $.each(_array, function(){

                CONDR.progressionLog.recordCount = ++count;

                var _date = new SplitDate(this['date'],'iso');

                var confirmed = {};
                confirmed.ok = false;
                confirmed.ID = this['ID'];
                confirmed.date = '1970-01-01';
                confirmed.display = 'N/A';
                confirmed.saLinkID = (this[salinkid] > '') ? this[salinkid] : 'none' ;

                if (this[type] === 'Confirmed Progression'){
                    $.each(_array, function(){
                        if (confirmed.ok === true) return ;
                        if (this['ID'] === confirmed.saLinkID){
                            confirmed.date = confirmed.display = this['date'];
                            confirmed.ok = true ;
                        }
                    })
                }

                //if (window.console && window.console.log) console.log(confirmed);

                var treatmentchange_bool = '0', treatmentchange_text = 'No';
                if (this[treatmentchange]+'' === '1'){
                    treatmentchange_bool = '1';
                    treatmentchange_text = 'Yes';
                }

                // leading underscore - input object
                var _outcome_determiners_input = {
                    'Imaging': { bool: this[imaging] }, // bool 0=false or 1=true
                    'Functional Status': { bool: this[functionalstatus] }, // bool
                    'Steroid Dose': {
                        bool:  this[steroiddose], // bool
                        amount: this[steroiddoseamount]
                    },
                    'New Neurological Deficit': {
                        bool: this[newneurologicaldefecit], // bool
                        options: {
                            'Motor Weakness': this[motorweakness], // bool
                            'Sensory Change': this[sensorychange], // bool
                            'Severe Headache': this[severeheadache], // bool
                            'Nausea': this[nausea], // bool
                            'Visual Loss': this[visualloss], // bool
                            'Cognitive Decline': this[cognitivedecline], // bool
                            'Clinical Deterioration': this[clinicaldeterioration], // bool
                            'Other (see notes)': this[otherneurologicaldefecit]  // bool
                        }
                    }
                };

                // trailing underscore - output array
                var outcome_determiners_output_ = [];

                $.each(_outcome_determiners_input, function(_prop, _val){
                    //console.log(_prop + ': ' + JSON.stringify(_val));
                    if (_prop !== 'New Neurological Deficit' && _val.bool+'' === '1') {
                        if (_prop === 'Steroid Dose' && _val.bool === '1'){
                            outcome_determiners_output_.push('Steroid Dose (' + _val.amount + ' mg/day)')
                        }
                        else {
                            outcome_determiners_output_.push(_prop);
                        }
                    }
                    else if (_prop === 'New Neurological Deficit' && _val.bool+'' === '1'){
                        var defecits = [];
                        $.each(_val.options, function(_x, _y){
                            if (_y+'' === '1') {
                                defecits.push(_x)
                            }
                        });
                        var new_defecits = 'New Neurological Deficit(s)';
                        if (defecits > []){ // make sure there's at least one selected before trying to list them
                            new_defecits += ': <i><span>' + defecits.join(', ') + '</span></i>';
                        }
                        outcome_determiners_output_.push(new_defecits);
                    }
                });

                var notes = '&mdash;';
                if (this['note'] > ''){
                    notes = '' +
                        '<span class="tip_icon note" style="margin:auto;position:absolute;top:0;right:0;bottom:0;left:0;">' + //
                        '<span class="tip shadowed" style="width:auto;min-width:150px;max-width:250px;left:-180px;top:-10px;z-index:10000;white-space:normal;background:#dff;word-wrap:break-word;">' +

                        this['note'] +

                        '</span>' +
                        '</span>' +
                        '';
                }

                var _kps = (this[kpsscore] > '' && this[kpsscore] !== '1') ? this[kpsscore]+'%' : 'N/S' ;

                records_html += '\n' +
                    '<tr id="'+ this['ID'] +'" class="prog_log record entry" title="'+ this['label'] +'" data-expt-id="'+ this['ID'] +'" data-expt-date="'+ _date.ms +'">' +
                    '' + '<td class="date mono nowrap text-center">'+ _date.iso +'</td>' +
                    '' + '<td class="type text-left">'+ this[type] +'</td>' +
                    '' + '<td class="confirmed_date mono nowrap text-center" data-confirmed-id="'+ confirmed.saLinkID +'" data-confirmed-date="'+ confirmed.date +'">'+ confirmed.display +'</td>' + // TODO: WHERE DO WE GET THE ENCOUNTER INFO???
                    '' + '<td class="KPS mono nowrap text-center" data-val="' + this[kpsscore] + '">' + _kps + '</td>' +
                    '' + '<td class="treatment_change text-center" data-bool="'+ treatmentchange_bool +'">'+ treatmentchange_text +'</td>' +
                    '' + '<td class="outcome_determiners text-left">'+ outcome_determiners_output_.join('; ') +'</td>' +
                    '' + '<td class="notes text-center">'+ notes +'</td>' +
                    '' + '<td class="text-center"><a class="edit nolink" href="'+ serverRoot +'/data/experiments/'+ this['ID'] +'">edit</a></td>' +
                    '</tr>' +
                    '';

            });

        $container.html(records_html);

        if (typeof _callback == 'function'){
            _callback( $container );
        }

    };
    //////////////////////////////////////////////////




    //////////////////////////////////////////////////
    // SET UP URI FOR PUT AND RETURN IT
    //////////////////////////////////////////////////
    CONDR.progressionLog.putURI = function( _data ){
        var xsiType = 'condr:progressionLogEntry';
        return serverRoot +
            '/data' +
            '/projects/CONDR' + //XNAT.data.context.projectID +
            '/subjects/' + XNAT.data.context.subjectLabel +
            '/experiments/' + _data.exptLabel + // REQUIRED
            '?xsiType=' + xsiType +
            '&' + xsiType + '/date=' + _data.date_ +
            '&' + xsiType + '/saCollID=' + _data.saCollID +
            '&' + xsiType + '/saLinkID=' + (_data.saLinkID || '') +
            '&' + xsiType + '/type=' + (_data.type_ || '') +
            //'&' + xsiType + '/confirmedProgressionDate=' + (_data.confirmedProgressionDate || '') +
            '&' + xsiType + '/kpsScore=' + (_data.kpsScore) + // REQUIRED
            '&' + xsiType + '/treatmentChange=' + (_data.treatmentChange || 0) +
            // outcome determiners
            '&' + xsiType + '/imaging=' + (_data.imaging || 0) +
            '&' + xsiType + '/functionalStatus=' + (_data.functionalStatus || 0) +
            '&' + xsiType + '/steroidDose=' + (_data.steroidDose || 0) +
            '&' + xsiType + '/steroidDoseAmount=' + (_data.steroidDoseAmount || 0) +
            // new neurological defecits
            '&' + xsiType + '/newNeurologicalDefecit=' + (_data.newNeurologicalDefecit || 0) +
            '&' + xsiType + '/motorWeakness=' + (_data.defecits.motorWeakness || 0) +
            '&' + xsiType + '/sensoryChange=' + (_data.defecits.sensoryChange || 0) +
            '&' + xsiType + '/severeHeadache=' + (_data.defecits.severeHeadache || 0) +
            '&' + xsiType + '/nausea=' + (_data.defecits.nausea || 0) +
            '&' + xsiType + '/visualLoss=' + (_data.defecits.visualLoss || 0) +
            '&' + xsiType + '/cognitiveDecline=' +  (_data.defecits.cognitiveDecline || 0) +
            '&' + xsiType + '/clinicalDeterioration=' + (_data.defecits.clinicalDeterioration || 0) +
            '&' + xsiType + '/otherNeurologicalDefecit=' + (_data.defecits.otherNeurologicalDefecit || 0) +
            '&' + xsiType + '/note=' + encodeURIComponent(_data.notes || '') +
            '&' + 'allowDataDeletion=true' +
            '&' + 'XNAT_CSRF=' + csrfToken
    };
    //////////////////////////////////////////////////




    //////////////////////////////////////////////////
    // PUT LOG ENTRY RECORD WITH REST VIA AJAX
    //////////////////////////////////////////////////
    CONDR.progressionLog.putRecord = function( putData, _$modal, _data ){

        _data = _data || {};

        var putRecord = $.ajax({
            type: 'PUT',
            url: putData,
            async: true
        });

        putRecord.done(function(exptID){
            var reRenderRecords = setInterval(function () {
                if (exptID > '') { //check if selected option is loaded
                    clearInterval(reRenderRecords); // stop checking

                    // if we have a "Confirmed Progression" and the saLinkID value is empty
                    // that means we're using the current entry as the confirmed progression
                    // so, after the initial PUT for the record, use the returned ID
                    // to insert that into the saLinkID property with another PUT
                    if (_data && _data.type_ === 'Confirmed Progression') {
                        if (_data.saLinkID === ''){
                            var putCurrentLinkID = $.ajax({
                                type: 'PUT',
                                url: serverRoot +
                                    '/data/projects/CONDR/subjects/' +
                                    XNAT.data.context.subjectLabel +
                                    '/experiments/' + exptID +
                                    //'?xsiType=condr:progressionLogEntry' +
                                    '?condr:progressionLogEntry/saLinkID=' + exptID +
                                    '&' + 'allowDataDeletion=true' +
                                    '&' + 'XNAT_CSRF=' + csrfToken,
                                async: false
                            });
                            putCurrentLinkID.done(function(_ID){
                                if (window.console && window.console.log) window.console.log('success: '+_ID);
                            });
                            putCurrentLinkID.fail(function(data, status, error){
                                xModalLoadingClose();
                                xModalMessage('error','data: '+data+'<br><br> status: '+status+'<br><br> error: '+error);
                            });
                        }
                    }

                    CONDR.progressionLog.records = CONDR.progressionLog.getCollection(CONDR.progressionLog.collID);
                    CONDR.progressionLog.renderRecords(
                        CONDR.progressionLog.records,
                        jq('#prog_log_entries').find('tbody'),
                        function(){
                            CONDR.progressionLog.theseNewRecords(exptID);
                            xModalCloseNew(_$modal.attr('id'));
                            xModalLoadingClose()
                        }
                    );
                    CONDR.progressionLog.selectedDeficits = [];
                    CONDR.progressionLog.typeChanged = false;
                }
            }, 100);
        });

        putRecord.fail(function(data, status, error){
            xModalLoadingClose();
            xModalMessage('error','data: '+data+'<br><br> status: '+status+'<br><br> error: '+error);
        });

    };
    //////////////////////////////////////////////////




    //////////////////////////////////////////////////
    // MAKE SURE FORM IS COMPLETE
    //////////////////////////////////////////////////
    CONDR.progressionLog.checkForm = function( $modal ){

        var errors=0, output={};

        output.msg = 'Please correct the following errors, then re-submit the data: <ul>';

        var $date_input = $modal.find('input[name="log_entry_date"]');

        var date_input_val = Date.parse($date_input.val());
        var todays_date = Date.parse(XNAT.data.todaysDate.iso);

        if (date_input_val < Date.parse(CONDR.progressionLog.surgeryDate)){
            output.date = false;
            output.msg += '<li>Enter a date on or after the surgery date (' + CONDR.progressionLog.surgeryDate + ')</li>';
            errors++;
        }
        else if (date_input_val > todays_date) {
            output.date = false;
            output.msg += '<li>You may not select a date in the future</li>';
            errors++;
        }
        else if ($date_input.val().length !== 10 || $date_input.val() === '    -  -  ') {  // '    -  -  ' is the value if the date field has been focused but is blank
            output.date = false;
            output.msg += '<li>Select a date</li>';
            errors++ ;
        }

        if ($modal.find('select[name="progression_type"]').val() === '-') {
            output.prog_type = false;
            output.msg += '<li>Select a Progression Type</li>';
            errors++ ;
        }

        if ($modal.find('select[name="progression_type"]').val() === 'Confirmed Progression'
                && $modal.find('select[name="confirmed_date"]').val() === '-'){
            output.confirmed_date = false;
            output.msg += '<li>Select a Confirmed Progression Date</li>';
            errors++
        }

        if ($modal.find('select[name="KPS"]').val() === '-' && $modal.find('select[name="progression_type"]').val() !== 'Death' ) {
            output.kps = false;
            output.msg += '<li>Select a KPS value</li>';
            errors++ ;
        }

        if ($modal.find('[name="treatment_change"]:checked').length === 0 && $modal.find('select[name="progression_type"]').val() !== 'Death' ) {
            output.treatment_change = false;
            output.msg += '<li>Choose an option for Treatment Change</li>';
            errors++ ;
        }

        output.msg += '</ul>';

        output.complete = (errors === 0);

        return output;

    };
    //////////////////////////////////////////////////




    //////////////////////////////////////////////////
    // SETUP DATA FOR SAVE
    //////////////////////////////////////////////////
    CONDR.progressionLog.saveRecord = function( _$modal, _opts ){

        var $modal = jQueryDOMobj(_$modal);
        _opts = jQueryPlainObj(_opts);

        var formCheck = CONDR.progressionLog.checkForm($modal);

        if (!formCheck.complete) {
            xModalMessage('Incomplete Data', formCheck.msg, 'OK', { height: 260 }); //
            return
        }

        var count = CONDR.progressionLog.recordCount + 1 ; // just add 1 here - increment property on (re)render of records

        var _date, date_, _type, _confirmed_date, _KPS, _treatment,
            _imaging, _func_status, _steroid, _steroid_dose_amount, _dose,
            _new_defecit, _deficits;

        var DATA={}; // set up object for putURI

        var exptNumber = XNAT.data.context.exptID.replace(/CNDA_/,'');
        DATA.exptLabel = XNAT.data.context.subjectLabel + '_' + exptNumber + '_LogEntry' +  zeroPad(count) + '_' + $.now() ;
        DATA.saCollID = CONDR.progressionLog.collID ;
        _date = $modal.find('[name="log_entry_date"]').val();
        date_ = new SplitDate(_date,'iso');
        DATA.date_ = date_.iso ;

        _type = $modal.find('[name="progression_type"]').val();
        DATA.type_ = (_type !== '-') ? _type : '' ; // set '-' to ''

        var $confirmed_date_menu = $modal.find('[name="confirmed_date"]');
        _confirmed_date = $confirmed_date_menu.val();

        if (DATA.type_ === 'Confirmed Progression') {
            if (_confirmed_date === 'current') {
                //DATA.confirmedProgressionDate = DATA.date_ ;
                DATA.saLinkID = '' ; // send empty saLinkID if the "current" entry is used - we'll know it's the "current" entry if type is "Confirmed Progression" and the saLinkID is blank
            }
            else {
                //DATA.confirmedProgressionDate = (_confirmed_date !== '-') ? _confirmed_date : '' ; // set '-' to ''
                DATA.saLinkID = $confirmed_date_menu.find(':selected').data('expt-id');
            }
        }
        else {
            DATA.confirmedProgressionDate = '';
        }

        _KPS = $modal.find('[name="KPS"]').val();
        DATA.kpsScore = (_KPS !== '-') ? _KPS : '' ; // set '-' to ''

        DATA.treatmentChange = $modal.find('[name="treatment_change"]:checked').val();

        // outcome determiners
        DATA.imaging = ($modal.find('[name="Imaging"]').is(':checked')) ? 1 : 0 ;
        DATA.functionalStatus = ($modal.find('[name="Functional Status"]').is(':checked')) ? 1 : 0 ;
        _steroid_dose_amount = $modal.find('[name="Steroid Dose Amount"]').val();
        if ($modal.find('[name="Steroid Dose"]').is(':checked')){
            DATA.steroidDose = 1 ;
            DATA.steroidDoseAmount = (!_steroid_dose_amount) ? 0 : _steroid_dose_amount ;
        }
        else {
            DATA.steroidDose = 0;
        }
        DATA.newNeurologicalDefecit = ($modal.find('[name="New Neurological Deficit"]').is(':checked')) ? 1 : 0 ;
        DATA.defecits={};
        try {
            $.each(CONDR.progressionLog.selectedDeficits, function(i,val){
                DATA.defecits[val] = 1 ;
            });
        }
        catch(e){
            if (window.console && window.console.log) window.console.log(e);
        }

        DATA.notes = $modal.find('textarea[name="notes"]').val();

        $.extend( true, DATA, _opts );

        // build the URI
        var putData = CONDR.progressionLog.putURI(DATA);

        xModalLoadingOpen({title:'Saving...'});

        CONDR.progressionLog.putRecord( putData, $modal, DATA );

    };
    //////////////////////////////////////////////////




    // initialize the "Chosen" plugin
    // must call this for every new modal
    function chosenInit( _$select, _opts ){
        var $select = jQueryDOMobj(_$select);
        _opts = jQueryPlainObj(_opts);
        var opts={};
        opts.allow_single_deselect = true ;
        opts.inherit_select_classes = true ;
        opts.disable_search = true ;
        //opts.display_disabled_options = false ;
        if ($select.data('width') !== ''){
            opts.width = $select.data('width');
        }
        $.extend(true, opts, _opts);
        $select.chosen(opts);
    }




    var record = 0;




    function ModalOpts(){
        this.className = 'modal' + (++record);
        this.width = 600;
        this.height = 660;
        //this.position: '5%',
        this.enter = false;
        this.title = 'Progression Record Data';
        this.content = $('#progression_record_modal').html();
        this.scroll = true ;
        this.buttons = {
            ok: {
                label: 'Save Record',
                close: false, // wait for success callback to call xmodal.close() //
                isDefault: true
            },
            cancel: {
                label: 'Cancel'
            }
        };
        //this.okLabel = this.buttons.ok.label ;
        //this.okClose = (this.buttons.ok.close !== false) ? 'yes' : 'no' ;
    }




    CONDR.progressionLog.renderConfirmedMenu = function( _$form ){

        // exit if the select isn't 'clean'
        // prevents this firing twice when .change() is triggered programatically
        if (CONDR.progressionLog.typeChanged === true) {
            CONDR.progressionLog.typeChanged = false ;
            return ;
        }
//        else {
//            CONDR.progressionLog.refreshConfirmed = true ;
//        }

        var $form = jQueryDOMobj(_$form);

        var $progression_type = $form.find('[name="progression_type"]');

        var $confirmed_select = $form.find('[name="confirmed_date"]');
        var $confirmed_label = $confirmed_select.closest('tr').find('label');

        var date_val = $form.find('[name="log_entry_date"]').val();
        var selected_date = new SplitDate(date_val);

        if ($progression_type.val() === 'Confirmed Progression'){
            if (!date_val || date_val === '    -  -  ') {
                xModalMessage('Select Date','Please select a date first.');
                CONDR.progressionLog.typeChanged = true ;
                $progression_type.val('-').change();
                $progression_type.trigger('chosen:updated');
                CONDR.progressionLog.typeChanged = false ;
                return ; // EXIT IF NO DATE IS SELECTED
            }
            //if (CONDR.progressionLog.refreshConfirmed === true){
                $confirmed_select.find('option').not('.blank, .divider, .current').remove();
                $.each(CONDR.progressionLog.records, function(){
                    var this_prog_type = this['condr:progressionlogentry/type'];
                    var this_prog_date = this['date'];
                    if (this_prog_type.indexOf('Progression') !== -1){
                        var conf_prog_date = new SplitDate(this_prog_date);
                        if (Date.parse(conf_prog_date.iso) <= Date.parse(selected_date.iso)){
                            var selected='';
                            if (CONDR.progressionLog.entryData &&
                                CONDR.progressionLog.entryData.ID &&
                                CONDR.progressionLog.entryData.ID === this['ID']){
                                    this_prog_type += ' (Current Entry)' ;
                            }
                            if (CONDR.progressionLog.entryData &&
                                CONDR.progressionLog.entryData['condr:progressionlogentry/salinkid'] &&
                                CONDR.progressionLog.entryData['condr:progressionlogentry/salinkid'] === this['ID']){
                                    $confirmed_select.find('option.current').remove();
                                    selected = 'selected="selected"';
                            }
                            $confirmed_select.append('' +
                                '<option ' + selected +
                                'data-expt-id="' + this.ID + '" ' +
                                'data-expt-date="' + this_prog_date + '" ' +
                                'data-date-num="' + Date.parse(conf_prog_date.iso) + '" ' +
                                'value="' + conf_prog_date.iso + '"' +
                                '>' + conf_prog_date.iso + ' - ' + this_prog_type +
                                '</option>' +
                                '')
                        }
                    }
                });
                $confirmed_label.removeClass('disabled');
                $confirmed_select.
                    removeClass('disabled').
                    removeAttr('disabled').
                    prop('disabled',false);
            //}
            //CONDR.progressionLog.refreshConfirmed = true ;
        }
        else {
            $confirmed_label.addClass('disabled');
            $confirmed_select.
                val('-').
                change().
                addClass('disabled').
                attr('disabled','disabled').
                prop('disabled',true);
        }
        $confirmed_select.trigger('chosen:updated');

    };



    CONDR.progressionLog.kpsScoreMenu = function ( _$form ){

        var $form = jQueryDOMobj(_$form);

        var $progression_type = $form.find('[name="progression_type"]');
        var $kps = $form.find('tr[title="KPS"]');
        var $kps_select = $kps.find('select[name="KPS"]');

        if ($progression_type.val() === 'Death'){
            $kps.find('td > label').addClass('disabled');
            $form.find('tr[title="Treatment Change"]').addClass('disabled');
            $form.find('tr[title="Treatment Change"] input').addClass('disabled').prop('disabled',true);
            $kps_select.addClass('disabled').prop('disabled',true);
        }
        else {
            $kps.find('td > label').removeClass('disabled');
            $form.find('tr[title="Treatment Change"]').removeClass('disabled');
            $form.find('tr[title="Treatment Change"] input').removeClass('disabled').prop('disabled',false);
            $kps_select.removeClass('disabled').prop('disabled',false);
        }

        $kps_select.val('-').change().trigger('chosen:updated');

    };



    //////////////////////////////////////////////////
    // BRINGS UP THE LOG ENTRY MODAL FOR EDITING
    //////////////////////////////////////////////////
    CONDR.progressionLog.openModal = function( _record_id, _opts ) {

        var new_record = false ; // set to false initially - it is an 'edit' function after all

        // call method with no args to edit as 'new'
        // if _opts are needed, pass null as first arg
        if (typeof _record_id == 'undefined' || _record_id === null) { new_record = true } //
        _opts = _opts || {} ;

        CONDR.progressionLog.selectedDeficits=[];

        var opts = new ModalOpts();
        //var $edit = $(this);
        //var $record = $edit.closest('tr.record');
        //var record_id = $record.data('expt-id');
        var record_id = _record_id || null ;
        var modal_id = record_id + '-edit';
        opts.id = modal_id ;
        var modal_class = opts.className ;
        var $modal, $form;
        var entryData = CONDR.progressionLog.entryData = null ;
        if (new_record === false) { entryData = CONDR.progressionLog.getEntryData(record_id) }
        var xsiType = 'condr:progressionlogentry';

        //alert(entryData[xsiType+'/confirmedprogressiondate']);

        opts.beforeShow = function(){

            //console.log(entryData);
            //$modal = $('div.xmodal.' + modal_class);
            $modal = $('#'+modal_id);
            $form = $modal.find('form');

            $form.addClass('active').attr('name', modal_class);//.

            var date_opts={};
            //date_opts.disabled = true;
            //date_opts.readonly = true;
            var $confirmed_date_menu = $form.find('select[name="confirmed_date"]');

            if (new_record === true) {
                XNAT.app.datePicker.init($form.find('.datepicker'));  //
                // nothing more to do if it's new
            }
            else {
                date_opts.defaultValue = entryData['date'] ;
                XNAT.app.datePicker.init($form.find('.datepicker'), date_opts);  //

                var prog_type = entryData[xsiType+'/type'] ;
                var $prog_type_menu = $form.find('[name="progression_type"]');

                $prog_type_menu.val(prog_type);
                $prog_type_menu.change();

                // this doesn't exist anymore
                //var confirmedprogressiondate = entryData[xsiType+'/confirmedprogressiondate'];

                var salinkid = entryData[xsiType+'/salinkid'];

                //alert(salinkid);

                if (prog_type === 'Confirmed Progression'){
                    $confirmed_date_menu.find('option').removeClass('default');
                    $confirmed_date_menu.find('[value="current"]').remove();
                    $confirmed_date_menu.find('[data-expt-id="'+salinkid+'"]').addClass('default').prop('selected',true);
                }

                var kps_score = entryData[xsiType+'/kpsscore'];
                if (kps_score === ''){
                    kps_score = '-';
                }
                $form.find('[name="KPS"]').val(kps_score).change();
                $form.find('[name="treatment_change"]').prop('checked',false); // uncheck both radio buttons
                $form.find('[name="treatment_change"][value="' + entryData[xsiType+'/treatmentchange'] + '"]').prop('checked',true);
                $form.find('[name="Imaging"][value="' + entryData[xsiType+'/imaging'] + '"]').prop('checked',true);
                $form.find('[name="Functional Status"][value="' + entryData[xsiType+'/functionalstatus'] + '"]').prop('checked',true);
                $form.find('[name="Steroid Dose"][value="' + entryData[xsiType+'/steroiddose'] + '"]').prop('checked',true);
                if ($form.find('[name="Steroid Dose"]').is(':checked')) {
                    $form.find('[name="Steroid Dose Amount"]').removeClass('disabled').removeAttr('disabled').prop('disabled',false);
                }
                $form.find('[name="Steroid Dose Amount"]').val(entryData[xsiType+'/steroiddoseamount']);
                $form.find('[name="New Neurological Deficit"][value="'+ entryData[xsiType+'/newneurologicaldefecit'] + '"]').prop('checked',true);

                var defecits = [
                    'motorWeakness',
                    'sensoryChange',
                    'severeHeadache',
                    'nausea',
                    'visualLoss',
                    'cognitiveDecline',
                    'clinicalDeterioration',
                    'otherNeurologicalDefecit'
                ];
                $.each(defecits, function( i, _val ){
                    var val = _val.toLowerCase();
                    var value_ = entryData[xsiType+'/'+val];
                    var bool = (value_ === '1');
                    // initial population of selectedDeficits array
                    if (bool) CONDR.progressionLog.selectedDeficits.push(_val);
                    $form.find('select.deficits').find('option[value="'+ _val + '"]').prop('selected',bool).attr('selected',bool);
                });
                if ($form.find('[name="New Neurological Deficit"]').is(':checked')){
                    $form.find('select.deficits').removeClass('disabled').removeAttr('disabled').prop('disabled',false);
                }

                $form.find('[name="notes"]').val(entryData['note']).html(entryData['note']);

            }

            $form.find('select').each(function(){chosenInit($(this))});
            // when the selection is changed, update the selectedDeficits array
            $form.find('select.deficits').chosen().change(function(){
                CONDR.progressionLog.selectedDeficits = $(this).val() || [];
            });

            CONDR.progressionLog.renderConfirmedMenu($form);

        };

        opts.buttons.ok.action = function(){
            // send $modal to be closed on success/done callback
            if (record_id !== null && CONDR.progressionLog.changedRecords.indexOf(record_id) === -1){
                CONDR.progressionLog.changedRecords.push(record_id)
            }
            if (new_record === true){
                CONDR.progressionLog.saveRecord( $modal );
            }
            else {
                CONDR.progressionLog.saveRecord( $modal, { exptLabel: record_id } );
            }
        };

        //opts.buttons.ok.close = true ;
        opts.okAction = opts.buttons.ok.action ;

        $.extend(true, opts, _opts);

        //new xModal.Modal(opts);
        xmodal.open(opts);

    };
    //////////////////////////////////////////////////




    //////////////////////////////////////////////////
    // BRINGS UP THE NOTES MODAL FOR EDITING
    //////////////////////////////////////////////////
    // saves notes then re-renders them on .done()
    CONDR.progressionLog.notesModal = function( _$target, _content ){

        var $target = (_$target && _$target.jquery) ? _$target : $notes ;
        var modal_content = '' +
            '<textarea style="width:450px;height:175px;" maxlength="255">' + //
                (_content || $target.find('p').text()) +
            '</textarea>' +
            '';

        var modal_opts={};
        modal_opts.id = 'notes_modal';
        modal_opts.width = 500;
        modal_opts.height = 300;
        modal_opts.title = 'Progression Log Notes';
        modal_opts.content = modal_content ;
        modal_opts.scroll = 'nope';
        modal_opts.closeBtn = 'hide';
        modal_opts.okLabel = 'Save and Close';
        modal_opts.okAction = function(){
            xModalLoadingOpen({title:'Saving notes...'});
            xModal.closeModal = false ;
            var $this_modal = $('#'+modal_opts.id);
            var notes = $this_modal.find('textarea').val();
            var saveNotes = $.ajax({
                type: 'PUT',
                cache: false,
                async: true,
                url: serverRoot +
                    '/data' +
                    '/projects/CONDR' +
                    '/subjects/'+ XNAT.data.context.subjectID +
                    '/experiments/' + CONDR.progressionLog.collID +
                    '?xsiType=condr:progressionLog' +
                    '&condr:progressionLog/note=' + encodeURIComponent(notes) +
                    '&allowDataDeletion=true' +
                    '&XNAT_CSRF=' + csrfToken
            });
            saveNotes.done(function(_id){
                var reRenderNotes = setInterval(function(){
                    if (_id > ''){
                        clearInterval(reRenderNotes);
                        CONDR.progressionLog.renderNotes(_id);
                        xModalLoadingClose();
                    }
                },100);
            });
            saveNotes.fail(function(data, status, error){
                xModalMessage('error','data: '+data+'<br><br> status: '+status+'<br><br> error: '+error);
                xModalLoadingClose();
            });

        };
        modal_opts.beforeShow = function(){
            xModal.closeModal = true ;
        };
        //new xModal.Modal(modal_opts);
        xmodal.open(modal_opts);
    };
    //////////////////////////////////////////////////




    // DOM stuff
    $(function(){

        $body = $(document.body);
        $prog_log_entries = $('#prog_log_entries');
        $notes = $('#prog_log_notes');

        $body.on('click', 'a.nolink, a[href="#"]', function(e){
            e.preventDefault();
        });




        //////////////////////////////////////////////////
        // ADD NEW PROGRESSION LOG ENTRY RECORD
        //////////////////////////////////////////////////
        $('#add_prog_log_record').click(function(){
            CONDR.progressionLog.openModal();
        });
        //////////////////////////////////////////////////




        //////////////////////////////////////////////////
        // EDIT EXISTING PROGRESSION LOG ENTRY RECORD
        //////////////////////////////////////////////////
        $prog_log_entries.on('click', 'a.edit', function(){
            //var $edit = $(this);
            //var $record = $edit.closest('tr.record');
            //var record_id = $record.data('expt-id');
            var record_id = $(this).closest('tr.record').data('expt-id');
            CONDR.progressionLog.openModal(record_id);
        });
        //////////////////////////////////////////////////




        CONDR.progressionLog.typeChanged = false ; // indicator for when "Type" is changed
        //CONDR.progressionLog.refreshConfirmed = true ; // toggle for refreshing the "Confirmed Progression Dates" menu




        $body.on('blur','form.active [name="log_entry_date"]',function(){
            //CONDR.progressionLog.refreshConfirmed = true ;
            CONDR.progressionLog.renderConfirmedMenu($(this).closest('form'));
        });

        $body.on('click','form.active td.calcell a.selector, form.active a.use-todays-date',function(){
            //CONDR.progressionLog.refreshConfirmed = true ;
            CONDR.progressionLog.renderConfirmedMenu($(this).closest('form'));
        });




        //////////////////////////////////////////////////
        // ENABLE SELECT FOR "CONFIRMED PROGRESSION DATE"
        //////////////////////////////////////////////////
        // enable "Confirmed Progression Date" select menu
        // if "Confirmed Progression" is selected for Progression Type
        $body.on('change','form.active [name="progression_type"]', function(e){
            var $form = $(this).closest('form');
            CONDR.progressionLog.renderConfirmedMenu($form);
            CONDR.progressionLog.kpsScoreMenu($form);
        });
        //////////////////////////////////////////////////




        //////////////////////////////////////////////////
        // ENABLE INPUT FOR "STEROID DOSE AMOUNT"
        //////////////////////////////////////////////////
        $body.on('change','form.active [name="Steroid Dose"]',function(){
            var $input = $(this).closest('fieldset').find('[name="Steroid Dose Amount"]');
            if ($(this).is(':checked')){
                $input.prop('disabled',false).focus().select();
            }
            else {
                $input.prop('disabled',true)
            }
        });
        //////////////////////////////////////////////////




        //////////////////////////////////////////////////
        // VALIDATE INPUT FOR "STEROID DOSE AMOUNT"
        //////////////////////////////////////////////////
        $body.on('blur','form.active [name="Steroid Dose Amount"]',function(){
            var amount = $(this).val();
            if (amount > '' && !$.isNumeric(amount)){
                xModalMessage('Invalid Data','Only numeric data is allowed for <b>Steroid Dose Amount</b>.','OK');
                $(this).val('').focus().select();
            }
        });
        //////////////////////////////////////////////////




        //////////////////////////////////////////////////
        // ENABLE MULTI-SELECT FOR DEFICIT OPTIONS
        //////////////////////////////////////////////////
        $body.on('change','form.active [name="New Neurological Deficit"]',function(){
            var $multi = $(this).closest('fieldset').find('select[name="deficits"]');
            if ($(this).is(':checked')){
                $multi.removeAttr('disabled').prop('disabled',false).focus();
            }
            else {
                $multi.find('option').removeAttr('selected').prop('selected',false);
                $multi.attr('disabled','disabled').prop('disabled',true);
            }
            //chosenInit($multi);
            $multi.trigger('chosen:updated');
        });
        //////////////////////////////////////////////////




        //////////////////////////////////////////////////
        // ADD NOTES FOR PROGRESSION LOG (COLLECTION)
        //////////////////////////////////////////////////
        $('#add_prog_log_notes').click(function(){
            CONDR.progressionLog.notesModal($notes,'');
        });
        //////////////////////////////////////////////////




        //////////////////////////////////////////////////
        // EDIT NOTES FOR PROGRESSION LOG (COLLECTION)
        //////////////////////////////////////////////////
        $notes.find('a.edit').click(function(){
            var notes = $notes.find('p').text(); // it might be 'better' to make a REST call for this, but getting it from the page is faster
            CONDR.progressionLog.notesModal($notes,notes);
        });
        //////////////////////////////////////////////////




    });

})();

