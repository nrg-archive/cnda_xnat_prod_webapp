<?xml version="1.0" encoding="UTF-8"?>
<!-- edited with XMLSpy v2007 sp2 (http://www.altova.com) by Washington University Radiolodical Science (Washington University in St. Louis) -->
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xnat="http://nrg.wustl.edu/xnat" xmlns:kblack="http://nrg.wustl.edu/wu_kblack" targetNamespace="http://nrg.wustl.edu/wu_kblack" elementFormDefault="qualified" attributeFormDefault="unqualified">
	<xs:import namespace="http://nrg.wustl.edu/xnat" schemaLocation="../xnat/xnat.xsd"/>
	<xs:element name="StudyParams" type="kblack:studyParamsData">
		<xs:annotation>
			<xs:documentation>Subject data and parameters for a single visit</xs:documentation>
		</xs:annotation>
	</xs:element>
	<xs:element name="VAS" type="kblack:vasData"/>
	<xs:complexType name="studyParamsData">
		<xs:annotation>
			<xs:documentation>Single visit ("study" in original database)</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="xnat:subjectAssessorData">
				<xs:sequence>
					<xs:element name="subjectCode" type="xs:string" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Subject code like p5283, TST3, N1A, etc.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="wasScreeningDay" type="xs:boolean" minOccurs="0"/>
					<xs:element name="studyType" type="xs:string" minOccurs="0"/>
					<xs:element name="weight" minOccurs="0">
						<xs:complexType>
							<xs:simpleContent>
								<xs:extension base="xs:float">
									<xs:attribute name="units" type="xs:string"/>
								</xs:extension>
							</xs:simpleContent>
						</xs:complexType>
					</xs:element>
					<xs:element name="infused" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Levodopa or saline?</xs:documentation>
						</xs:annotation>
						<xs:simpleType>
							<xs:restriction base="xs:string">
								<xs:enumeration value="levodopa"/>
								<xs:enumeration value="saline"/>
							</xs:restriction>
						</xs:simpleType>
					</xs:element>
					<xs:element name="CDdose" type="xs:float" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Carbidopa dose</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="CDtime" type="xs:time" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Carbidopa time</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="infusionStartTime" type="xs:time" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Start of loading dose</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="method" minOccurs="0">
						<xs:annotation>
							<xs:documentation>TODO: purpose?</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="loadDuration" type="xs:string" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Duration of loading dose (xs:duration would be better)</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="loadRate" type="xs:float" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Actual loading dose infusion rate (mL/min) - NOTE: half of mg/kg dose</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="maintRate" type="xs:float" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Actual maintenance infusion rate (mL/min) - NOTE: half of mg/kg dose</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="bloodIsAvailable" type="xs:boolean" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Are usable blood samples available?</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="sampleSite" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Where was blood drawn, relative to infusion site?</xs:documentation>
						</xs:annotation>
						<xs:simpleType>
							<xs:restriction base="xs:string">
								<xs:enumeration value="opposite side"/>
								<xs:enumeration value="same side distal"/>
								<xs:enumeration value="elsewhere"/>
								<xs:enumeration value="unknown"/>
							</xs:restriction>
						</xs:simpleType>
					</xs:element>
					<xs:element name="nausea" type="xs:boolean" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Did subject have nausea after drug?</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="vomiting" type="xs:boolean" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Did subject vomit after drug?</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="sedation" type="xs:boolean" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Did subject feel sleepy after drug?</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="dizzy" type="xs:boolean" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Did subject have lightheadedness or dizziness after drug?</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="dyskinesias" type="xs:boolean" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Did subject have dyskinesias after drug?</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="psychosis" type="xs:boolean" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Did subject have psychotic symptoms after drug?</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="ticsWorse" type="xs:boolean" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Did subject notice worsening of tics after drug?</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="ticsBetter" type="xs:boolean" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Did subject notice improvement in tics after drug?</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="otherSideEffects" type="xs:boolean" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Were there other side effects of drug?  If yes, note in comments.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="problem" type="xs:boolean" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Is there any problem with this visit?</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="knownForIVLD1" type="xs:boolean" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Was this subject included in the IVLD methods paper?</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="rejectedForIVLD1" type="xs:boolean" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Was this subject excluded from the IVLD methods paper?</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="blindnessSubject" type="xs:boolean" minOccurs="0"/>
					<xs:element name="blindnessInvestigator" type="xs:boolean" minOccurs="0"/>
					<xs:element name="blindnessCoordinator" type="xs:boolean" minOccurs="0"/>
					<xs:element name="pregnancyTest" type="xs:boolean" minOccurs="0"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="vasData">
		<xs:complexContent>
			<xs:extension base="xnat:subjectAssessorData">
				<xs:sequence>
					<xs:element name="nausea_vomiting" minOccurs="0">
						<xs:annotation>
							<xs:documentation>score in mm (0 = none)</xs:documentation>
						</xs:annotation>
						<xs:simpleType>
							<xs:restriction base="xs:integer">
								<xs:minInclusive value="0"/>
							</xs:restriction>
						</xs:simpleType>
					</xs:element>
					<xs:element name="sleepiness" minOccurs="0">
						<xs:annotation>
							<xs:documentation>score in mm (0 = none)</xs:documentation>
						</xs:annotation>
						<xs:simpleType>
							<xs:restriction base="xs:integer">
								<xs:minInclusive value="0"/>
							</xs:restriction>
						</xs:simpleType>
					</xs:element>
					<xs:element name="lightheadedness_dizziness" minOccurs="0">
						<xs:annotation>
							<xs:documentation>score in mm (0 = none)</xs:documentation>
						</xs:annotation>
						<xs:simpleType>
							<xs:restriction base="xs:integer">
								<xs:minInclusive value="0"/>
							</xs:restriction>
						</xs:simpleType>
					</xs:element>
					<xs:element name="overallWellBeing" type="xs:integer" minOccurs="0">
						<xs:annotation>
							<xs:documentation>score in mm (100 = life is just peachy)</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="problem" type="xs:boolean" minOccurs="0"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
</xs:schema>
