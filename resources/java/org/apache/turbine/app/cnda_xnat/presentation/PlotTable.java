//Copyright Washington University School of Medicine All Rights Reserved
/*
 * Created on Feb 26, 2007
 *
 */
package org.apache.turbine.app.cnda_xnat.presentation;

import java.util.ArrayList;

import org.nrg.xft.XFTTable;

public class PlotTable extends XFTTable {
    //INCLUDE types for each column
    ArrayList types = null;

    /**
     * @return the types
     */
    public ArrayList getTypes() {
        return types;
    }

    /**
     * @param types the types to set
     */
    public void setTypes(ArrayList types) {
        this.types = types;
    }
    
    
}
