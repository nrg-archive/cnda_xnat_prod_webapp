//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * ArcProperties.java
 *
 * Created on July 10, 2002, 2:44 PM
 */

package org.apache.turbine.app.cnda_xnat.utils.image;

import java.util.*;
import java.io.*;

import org.apache.log4j.Logger;
import org.nrg.xdat.XDATTool;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.XFT;
/**
 *
 * @author  dan
 * @version
 */
public class ArcProperties {

	static Logger logger = Logger.getLogger(ArcProperties.class);
    private static String propFile = XDATTool.GetSettingsDirectory() + "arc.properties";
    private static Properties props = null;
    
    /** Creates new ArcProperties */
    public ArcProperties() {
        
    }
    
    public static String getArcPath(){
        if (props == null)
            loadProperties();
        String arcPath = props.getProperty("arcPath");
        return arcPath;
    }
    
    public static String getSessionPath(String session){
        
        if (props == null)
            loadProperties();
        
        String toolPath = getToolPath();
        String sessionPath = "";
        try {
            ArrayList matches = XnatMrsessiondata.getXnatMrsessiondatasByField("xnat:mrSessionData.ID",session,null,false);
            XnatMrsessiondata mr=null;
            if (matches.size()>0)
            {
                mr = (XnatMrsessiondata)matches.get(0);
            }
			String tmp =  mr.getArchivePath();
			if (tmp.lastIndexOf(File.separator)!=-1)
				sessionPath = tmp.substring(0,tmp.lastIndexOf(File.separator));
        /*	String execStr = toolPath + "arc-find " + session;
        	System.out.println("ARCPROPERTIES \t " + execStr);
            Process proc = Runtime.getRuntime().exec(execStr);
            System.out.println("PROC " + proc.waitFor());
            //proc.wait();
            BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()) );
            sessionPath = reader.readLine();
          */  
        } catch (Exception e){
			if (File.separator.equals("/"))
			{
				e.printStackTrace();
				logger.error("",e);
			}
            
        }
        //System.out.println("ARC PROP SESSION PATH " + sessionPath);
        return sessionPath;
    }
    
    
   public static boolean exists(String session){
        
        if (props == null)
            loadProperties();
        
        //search through each of the arc subdirectories to see if there's already
        //a directory for this session
        String arcPath = getArcPath();
        File arc = new File(arcPath);
        File[] arcsubs = arc.listFiles();
        for (int i=0; i<arcsubs.length; i++){
            System.out.println("Current file: " + arcsubs[i].getName());
            if (! arcsubs[i].isDirectory())
                continue;
            File [] sessions = arcsubs[i].listFiles();
            System.out.println("Number of sessions in " + arcsubs[i].getName() + ": " + sessions.length);
            for (int j=0; j<sessions.length; j++){
                if (sessions[j].getName().equals(session))
                    return true;
            }
        }
        
        return false;
   }
    
    
    public static String getArcFile(){
        if (props == null)
            loadProperties();
        String arcFile = props.getProperty("arcFile");
        return arcFile;
    }
    
    public static String getWebArcPath(){
        if (props == null)
            loadProperties();
        String webArcPath = props.getProperty("webArcPath");
        return webArcPath;
    }
    
	public static String getImageArcPath(String type){
		 if (props == null)
			 loadProperties();
		 String typeImageArcPath = props.getProperty(type+"ImageArcPath");
		 return typeImageArcPath;
	 }
    
    public static String getPreArcPath(){
        if (props == null)
            loadProperties();
        String preArcPath = props.getProperty("preArcPath");
        return preArcPath;
    }
    
    public static String getToolPath(){
        if (props == null)
            loadProperties();
        String toolPath = props.getProperty("toolPath");
        return toolPath;
    }
    
    private static void loadProperties(){
        
        InputStream f;
        
        try {
            f = new FileInputStream(propFile);
            props = new Properties();
            props.load(f);
            f.close();
            //System.out.println(props);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String [] args){
        
        ArcProperties.getWebArcPath();
        ArcProperties.getArcPath();
        String sp = ArcProperties.getSessionPath("testsubj");
        System.out.println("session path for testsubj: " +  sp);
    }
}
