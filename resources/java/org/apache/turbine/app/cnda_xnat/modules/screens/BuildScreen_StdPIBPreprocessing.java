/* 
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 * 	
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.apache.turbine.app.cnda_xnat.modules.screens;

import java.sql.Time;
import java.util.ArrayList;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.base.BaseElement;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatPetscandata;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.schema.SchemaElement;
import org.nrg.xdat.turbine.modules.screens.SecureReport;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;
import org.nrg.xft.collections.ItemCollection;
import org.nrg.xft.schema.design.SchemaElementI;
import org.nrg.xft.search.ItemSearch;

public class BuildScreen_StdPIBPreprocessing extends SecureReport
{
    /**
     * Place all the data object in the context
     * for use in the template.
     */
    
    public void finalProcessing(RunData data, Context context)
    {
        XnatPetsessiondata pet = new XnatPetsessiondata(item);
        
        if (pet.getScans_scan() == null || pet.getScans_scan().size() == 0) {
            data.setMessage("No files uploaded for the session. Please upload files first");
            return;
        }
        
        ArrayList associatedMrs = getMrItems(data,pet.getSubjectId());
        ArrayList scanIds = getFirstInTimeUsableMPRAGEScanId(associatedMrs);
        ArrayList petScanIds = new ArrayList();
        
        for (int i = 0; i < pet.getScans_scan().size(); i++) {
            if (!((XnatPetscandata)pet.getScans_scan().get(i)).getType().startsWith("Transmission")) {
                petScanIds.add((XnatPetscandata)pet.getScans_scan().get(i));
            }
        }
        
        ArrayList restrictedMrs = new ArrayList();
        ArrayList restrictedScanIds = new ArrayList();
        
        for (int i = 0; i < associatedMrs.size(); i++) {
            if (!((String)scanIds.get(i)).equals("")) {
                restrictedMrs.add(associatedMrs.get(i));
                restrictedScanIds.add(scanIds.get(i));
            }
        }
        
        context.put("mrs",restrictedMrs);
        context.put("mrscanIds",restrictedScanIds);
        context.put("pet", pet);
        context.put("petScans", petScanIds);
        context.put("step",1);
        context.put("step1Disabled","disabled");
        boolean registered = pet.isAtlasRegistered();
        ArrayList regions = (ArrayList)pet.getRegions_region();
        if (!registered) {
            context.put("step2Disabled","disabled");
            context.put("step3Disabled","disabled");
        }else { 
            if (regions == null || regions.size() == 0) {
                context.put("step2Disabled","");
                context.put("step3Disabled","disabled");
            }else {
                context.put("step2Disabled","");
                context.put("step3Disabled","");
            }
        }
        if (registered) {
            data.setMessage("NOTE: This session has already been atlas registered. Rebuilding will result in deletion of ALL ROI files and time course analysis");
        }
    }
    
    private ArrayList getFirstInTimeUsableMPRAGEScanId(ArrayList mrs) {
        ArrayList scanids = new ArrayList();
        if (mrs == null || mrs.size() == 0) {
            return scanids;
        }
        String scanId = "";
        for (int i = 0; i < mrs.size(); i++) {
            XnatMrsessiondata mr = (XnatMrsessiondata) mrs.get(i);
            ArrayList mprageScans = mr.getScansByType("MPRAGE");
            if (mprageScans == null || mprageScans.size() == 0) {scanids.add(scanId); continue;}
            String firstScanId = null;
            String firstScanTime = null;
            for (int j = 0; j < mprageScans.size(); j++) {
                XnatMrscandata scan = (XnatMrscandata)mprageScans.get(j);
                if (scan.getQuality().equalsIgnoreCase("usable")) {
                    String scanTime = ((Time)scan.getStarttime()).toString();
                    if (firstScanId == null) {firstScanId = scan.getId(); firstScanTime = scanTime;}
                    else {
                        if (!earlierInTime(firstScanTime, scanTime)) {
                            firstScanId = scan.getId();
                            firstScanTime = scanTime;
                        }
                    }
                }
            }
            if (firstScanId != null)
                scanids.add(firstScanId);
            else 
                scanids.add(scanId);
        }
        return scanids;
    }
    
    private boolean earlierInTime(String time1, String time2) {
        boolean rtn = false;
        if (time1 == null || time2 == null) return true;
        String t1[] = time1.split(":");
        String t2[] = time2.split(":");
        if (t1 != null && t2 != null ) {
            int h1 = Integer.parseInt(t1[0]);
            int m1 = Integer.parseInt(t1[1]);
            int s1 = Integer.parseInt(t1[2]);
            int h2 = Integer.parseInt(t2[0]);
            int m2 = Integer.parseInt(t2[1]);
            int s2 = Integer.parseInt(t2[2]);
            if (h1 < h2 ) {return true;}
            if (h1 > h2) {return false;}
            if (m1 < m2) {return true;}
            if (m1 > m2) {return false;}
            if (s1 < s2) {return true;}
            if (s1 > s2) {return false;}
            return true;
        }
        return rtn;
    }
    
    private ArrayList getMrItems(RunData data, String search_value) {
        ArrayList rtn = new ArrayList();
        XnatMrsessiondata om = null;
        try {
            ItemSearch search = new ItemSearch();
            search.setUser(TurbineUtils.getUser(data));
            String elementName = "xnat:mrSessionData";
            SchemaElementI gwe = SchemaElement.GetElement(elementName);
            search.setElement(elementName);
            search.addCriteria("xnat:mrSessionData.subject_ID",search_value);
            boolean b = false;
            b= gwe.isPreLoad();
            search.setAllowMultiples(b);
            ItemCollection items = search.exec();
            ArrayList mrsByDate = items.getItems("xnat:mrSessionData.Date","DESC");
            if (mrsByDate.size() > 0)
            {
                for (int i =0; i <mrsByDate.size(); i++) {
                    ItemI item = (ItemI)mrsByDate.get(i);
                    om = (XnatMrsessiondata)BaseElement.GetGeneratedItem(item);
                    rtn.add(om);
                }
                return rtn;
            }else{
                return rtn;
            }
        }catch(Exception e) {e.printStackTrace();}
        return rtn;
    }

    
}
