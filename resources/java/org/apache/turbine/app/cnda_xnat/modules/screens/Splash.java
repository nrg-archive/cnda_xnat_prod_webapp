// Copyright 2010 Washington University School of Medicine All Rights Reserved
package org.apache.turbine.app.cnda_xnat.modules.screens;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.screens.SecureScreen;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFTTable;
import org.nrg.xft.db.PoolDBUtils;

public class Splash extends SecureScreen {

	@Override
	protected void doBuildTemplate(RunData data, Context context)
			throws Exception {
		try {
			
			Long proj_count=(Long)PoolDBUtils.ReturnStatisticQuery("SELECT COUNT(*) FROM xnat_projectData", "count", TurbineUtils.getUser(data).getDBName(), TurbineUtils.getUser(data).getUsername());
			context.put("proj_count", proj_count);
			
			Long sub_count=(Long)PoolDBUtils.ReturnStatisticQuery("SELECT COUNT(*) FROM xnat_subjectData", "count", TurbineUtils.getUser(data).getDBName(), TurbineUtils.getUser(data).getUsername());
			context.put("sub_count", sub_count);
			
			Long isd_count=(Long)PoolDBUtils.ReturnStatisticQuery("SELECT COUNT(*) FROM xnat_imageSessionData", "count", TurbineUtils.getUser(data).getDBName(), TurbineUtils.getUser(data).getUsername());
			context.put("isd_count", isd_count);
			
			XFTTable t = XFTTable.Execute("SELECT element_name,COUNT(ID) FROM xnat_experimentData expt LEFT JOIN xdat_meta_element xme ON expt.extension=xme.xdat_meta_element_id GROUP BY element_name;", TurbineUtils.getUser(data).getDBName(), TurbineUtils.getUser(data).getUsername());
			context.put("expt_counts", t.convertToHashtable("element_name", "count"));
			
			
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

}
