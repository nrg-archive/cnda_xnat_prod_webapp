/* 
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 * 	
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.apache.turbine.app.cnda_xnat.modules.screens;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.ArcPipelineparameterdata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xnat.turbine.modules.screens.DefaultPipelineScreen;

public class PipelineScreen_DtiQc extends DefaultPipelineScreen  {
	static Logger logger = Logger.getLogger(PipelineScreen_DtiQc.class);
 
	public void finalProcessing(RunData data, Context context){ 
		XnatMrsessiondata mr = (XnatMrsessiondata) om;
		context.put("mr", mr);
		
		context.put("projectSettings", projectParameters);
	     
		try {		 
			// get DTI scan labels from project setting
			ArcPipelineparameterdata dtiParam = getProjectPipelineSetting("dtis");
			context.put("dtiScanLabels", dtiParam.getCsvvalues());
		 }
		 catch(Exception e) {
			 logger.error("Possibly the project wide pipeline has not been set", e);
			 e.printStackTrace();
		 }
	 }
	 
	 public void preProcessing(RunData data, Context context)   {
	     super.preProcessing(data, context);
  	 }
}
