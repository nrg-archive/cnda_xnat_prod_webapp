//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Aug 18 11:41:38 CDT 2005
 *
 */
package org.apache.turbine.app.cnda_xnat.modules.screens;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Map;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.CndaRadiologyreaddata;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.modules.screens.EditScreenA;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;
import org.nrg.xft.XFTItem;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.exception.FieldNotFoundException;
import org.nrg.xft.exception.XFTInitException;
import org.nrg.xnat.turbine.utils.XNATUtils;

/**
 * @author XDAT
 *
 */
public class XDATScreen_edit_cnda_radiologyReadData extends EditScreenA {
	static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XDATScreen_edit_cnda_radiologyReadData.class);
	/* (non-Javadoc)
	 * @see org.nrg.xdat.turbine.modules.screens.EditScreenA#getElementName()
	 */
	public String getElementName() {
	    return "cnda:radiologyReadData";
	}
	
	public ItemI getEmptyItem(RunData data) throws Exception
	{
	    String s = getElementName();
		ItemI temp =  XFTItem.NewItem(s,TurbineUtils.getUser(data));
		return temp;
	}
	/* (non-Javadoc)
	 * @see org.nrg.xdat.turbine.modules.screens.SecureReport#finalProcessing(org.apache.turbine.util.RunData, org.apache.velocity.context.Context)
	 */
	public void finalProcessing(RunData data, Context context) {
	    Hashtable hash = XNATUtils.getInvestigatorsForCreate(getElementName(),data);
        context.put("investigators",hash);

        XDATUser user = TurbineUtils.getUser(data);
        try {        	
            if (data.getParameters().get("mrsession")!=null){
                if (item.getProperty("imageSession_ID")==null){
                    item.setProperty("imageSession_ID", data.getParameters().get("mrsession"));
                }
            }
            
            if (item.getProperty("reader")==null){
                String n ="";
                if (user.getFirstname()!=null){
                    n += user.getFirstname().substring(0,1) + ". ";
                }
                
                n += user.getLastname();
                item.setProperty("reader", n);
            }
            
            XnatProjectdata proj=null;
            CndaRadiologyreaddata rad = (CndaRadiologyreaddata) context.get("om");
            XnatMrsessiondata mr = rad.getMrSessionData();
            
        	if(mr!=null){            	
        		proj=mr.getPrimaryProject(false);
        	}else{
        		 if(data.getParameters().getString("project")!=null){
                 	proj=XnatProjectdata.getXnatProjectdatasById(data.getParameters().getString("project"), user, false);
                 }
        	}
                        
            if (item.getProperty("history")==null){
                if(mr!=null){
                    item.setProperty("history",mr.getSubjectData().getCohort());
                }
            }
            
            if (item.getProperty("technique")==null){
                if(mr!=null){
                    String tech = "";
                    Hashtable<String,Integer> counts = new Hashtable<String,Integer>();
                    
                    for(int i=0;i<mr.getScans_scan().size();i++){
                        XnatImagescandata scan = (XnatImagescandata)mr.getScans_scan().get(i);
                        if(scan.getType()!=null){
                            if (counts.get(scan.getType())==null){
                                counts.put(scan.getType(), new Integer(1)); 
                            }else{
                                counts.put(scan.getType(),counts.get(scan.getType())+1);
                            }
                        }
                    }
                    
                    for(Map.Entry<String,Integer> entry: counts.entrySet()){
                        if (!tech.equals(""))tech +=", ";
                        tech +="" + entry.getValue() + " " + entry.getKey();
                    }
                    
                    item.setProperty("technique", tech);
                }
            }
            
            long timestamp=Calendar.getInstance().getTimeInMillis();
            context.put("timestamp", timestamp);
            
            if (item.getProperty("date")==null){
                item.setProperty("date", Calendar.getInstance().getTime());
            }
            
            if(rad.getId()==null){
            	String s = mr.getIdentifier(proj.getId());
            	rad.setId(mr.getId() + "_RAD_" + timestamp);
            	rad.setLabel(s + "_RAD_" + timestamp);
            	rad.setProject(proj.getId());
            }
            
            
        } catch (XFTInitException e) {
            logger.error("",e);
        } catch (ElementNotFoundException e) {
            logger.error("",e);
        } catch (FieldNotFoundException e) {
            logger.error("",e);
        } catch (Exception e) {
            logger.error("",e);
        }
	}}
