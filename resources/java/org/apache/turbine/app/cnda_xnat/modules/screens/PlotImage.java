//Copyright Washington University School of Medicine All Rights Reserved
/*
 * Created on Feb 15, 2007
 *
 */
package org.apache.turbine.app.cnda_xnat.modules.screens;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.exceptions.IllegalAccessException;
import org.nrg.xdat.search.DisplaySearch;
import org.nrg.xdat.turbine.modules.screens.FileScreen;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFT;
import org.nrg.xft.exception.DBPoolException;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.exception.XFTInitException;
import org.nrg.xnat.turbine.modules.screens.IMGFile;

public class PlotImage  extends FileScreen {
    static org.apache.log4j.Logger logger = Logger.getLogger(IMGFile.class);


    public String getContentType(RunData data)
    {
       return "image/GIF";
    }
    /* (non-Javadoc)
     * @see org.nrg.xdat.turbine.modules.screens.FileScreen#getDownloadFile(org.apache.turbine.util.RunData, org.apache.velocity.context.Context)
     */
    public File getDownloadFile(RunData data, Context context) {
        String columnX= (String)TurbineUtils.GetPassedParameter("column_x",data);
        String columnY= (String)TurbineUtils.GetPassedParameter("column_y",data);
        
        File f = null;
        try {
            DisplaySearch search = TurbineUtils.getSearch(data);
            search.setPagingOn(false);
            org.nrg.xft.XFTTable table = (org.nrg.xft.XFTTable)search.execute(null,TurbineUtils.getUser(data).getLogin());
            search.setPagingOn(true);
            
            Integer xIndex = table.getColumnIndex(columnX);
            Integer yIndex = table.getColumnIndex(columnY);
            
            ArrayList xValues = new ArrayList();
            ArrayList yValues = new ArrayList();
            table.resetRowCursor();
            while (table.hasMoreRows())
            {
                Object[] row = table.nextRow();
                xValues.add(row[xIndex]);
                yValues.add(row[yIndex]);
            }
            
            //INSERT CODE HERE to generate image file from xValues and yValues ArrayLists
            //THEN RETURN THE CORRESPONDING FILE OBJECT
            
            System.out.println("X: " + xValues.toString());
            System.out.println("Y: " + yValues.toString());
            
            //BEGIN TEST CODE
            String path = XFT.GetSettingsDirectory();
            if (!(path.endsWith("/") || path.endsWith("\\")))
            {
                path += "/";
            }
            f = new File(path + "/images/gc.gif");
            //END TEST CODE
            //REMOVE the above TEST CODE.  IT IS JUST USED TO RETURN AN IMAGE FOR TEST PURPOSES.
        } catch (XFTInitException e) {
            logger.error("",e);
        } catch (ElementNotFoundException e) {
            logger.error("",e);
        } catch (DBPoolException e) {
            logger.error("",e);
        } catch (SQLException e) {
            logger.error("",e);
        } catch (IllegalAccessException e) {
            logger.error("",e);
        } catch (Exception e) {
            logger.error("",e);
        }
        
        return f;
    }
    
    public void logAccess(RunData data)
    {
    }
}