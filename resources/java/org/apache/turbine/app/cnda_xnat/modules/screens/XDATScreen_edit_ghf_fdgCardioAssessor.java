package org.apache.turbine.app.cnda_xnat.modules.screens;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.GhfAcetatecardioassessor;
import org.nrg.xdat.om.GhfFdgcardioassessor;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;
import org.nrg.xft.XFTItem;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.exception.FieldNotFoundException;
import org.nrg.xft.exception.XFTInitException;

/**
 * @author XDAT
 *
 */
public class XDATScreen_edit_ghf_fdgCardioAssessor extends org.nrg.xnat.turbine.modules.screens.EditImageAssessorScreen {
	static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XDATScreen_edit_ghf_fdgCardioAssessor.class);
	/* (non-Javadoc)
	 * @see org.nrg.xdat.turbine.modules.screens.EditScreenA#getElementName()
	 */
	public String getElementName() {
	    return "ghf:fdgCardioAssessor";
	}
	
	public ItemI getEmptyItem(RunData data) throws Exception
	{
		return super.getEmptyItem(data);
	}
	/* (non-Javadoc)
	 * @see org.nrg.xdat.turbine.modules.screens.SecureReport#finalProcessing(org.apache.turbine.util.RunData, org.apache.velocity.context.Context)
	 */
	public void finalProcessing(RunData data, Context context) {
        XDATUser user = TurbineUtils.getUser(data);
        
        try {        	
            if (item.getProperty("imageSession_ID") == null && data.getParameters().get("imagesession") != null) {
            	item.setProperty("imageSession_ID", data.getParameters().get("imagesession"));
            }
                        
            XnatProjectdata proj = null;
            GhfFdgcardioassessor fdg = (GhfFdgcardioassessor) context.get("om");
            XnatImagesessiondata image = fdg.getImageSessionData();
            
        	if (image != null) {
        		proj = image.getPrimaryProject(false);
        	}
        	else if (data.getParameters().getString("project") != null) {
	            proj = XnatProjectdata.getXnatProjectdatasById(data.getParameters().getString("project"), user, false);
        	}
        	
            String timestamp = new SimpleDateFormat("yyMMddhhmm").format(new Date());
            context.put("timestamp", timestamp);
            
            if (item.getProperty("date") == null) {
                item.setProperty("date", Calendar.getInstance().getTime());
            }
            
            if (fdg.getId() == null) {
            	String s = image.getIdentifier(proj.getId());
            	fdg.setId(image.getId() + "_FDG_" + timestamp);
            	fdg.setLabel(s + "_FDG_" + timestamp);
            	fdg.setProject(proj.getId());
            }
        } catch (XFTInitException e) {
            logger.error("",e);
        } catch (ElementNotFoundException e) {
            logger.error("",e);
        } catch (FieldNotFoundException e) {
            logger.error("",e);
        } catch (Exception e) {
            logger.error("",e);
        }
	}
}
