/* 
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 * 	
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.apache.turbine.app.cnda_xnat.modules.screens;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.base.BaseElement;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.om.XnatResource;
import org.nrg.xdat.om.base.BaseXnatExperimentdata.UnknownPrimaryProjectException;
import org.nrg.xdat.turbine.modules.screens.SecureScreen;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;
import org.nrg.xft.XFT;
import org.nrg.xft.XFTItem;

public class XDATScreen_downloadt1_xnat_petSessionData extends SecureScreen {
    
    XFTItem item = null;
    
    
    public void doBuildTemplate(RunData data, Context context)

    {

        try {

            item = (XFTItem)TurbineUtils.GetItemBySearch(data);

        } catch (Exception e1) {}

        

        if (item == null)

        {

            data.setMessage("Error: No item found.");

            TurbineUtils.OutputPassedParameters(data,context,this.getClass().getName());

        }else{

            try {

                context.put("item",item);

                context.put("user",TurbineUtils.getUser(data));

                

                context.put("element",org.nrg.xdat.schema.SchemaElement.GetElement(item.getXSIType()));

                context.put("search_element",data.getParameters().getString("search_element"));

                context.put("search_field",data.getParameters().getString("search_field"));

                context.put("search_value",data.getParameters().getString("search_value"));

                

                finalProcessing(data,context);

            } catch (Exception e) {

                data.setMessage(e.toString());

            }

        }

        

    }
    public void finalProcessing(RunData data, Context context) {
        
        ItemI om = BaseElement.GetGeneratedItem(item);
        XnatPetsessiondata pet = new XnatPetsessiondata(item);
        
        String t1Contents = data.getParameters().get("content");
        if (t1Contents==null) return;
        String[] contents = t1Contents.split(",");
        String zippedFileName = pet.getId() +"_t1";
        ArrayList<File> zipEntries = new ArrayList();
        String rootPath = null;
		try {
			rootPath = pet.getArchiveRootPath();
		} catch (UnknownPrimaryProjectException e) {
			logger.error("Could not get archive root path", e);
		}
        File dir = null;
        for (int i = 0; i < contents.length; i++) {
            String content = contents[i].trim();
            zippedFileName += "_" + content;
            ArrayList files = pet.getReconstructedFileByContent(content);
            for (int j = 0; j < files.size(); j++) {
                XnatAbstractresourceI reconOutFileByContent = (XnatAbstractresourceI)files.get(j);
                if (reconOutFileByContent instanceof XnatResource) {
                    XnatResource resource = (XnatResource)reconOutFileByContent;
                    ArrayList<File> associatedFiles = resource.getAssociatedFiles(rootPath, dir);
                    zipEntries.addAll(associatedFiles);
                }
            }
        }
        zippedFileName += ".zip";
        for (int i = 0; i < zipEntries.size(); i++) {
            logger.debug("File " + i + " "+ zipEntries.get(i).getAbsolutePath());
        }
        writeZippedFileToStream(data,zippedFileName,zipEntries);
        
    }
    
    
    private void writeZippedFileToStream(RunData data, String zippedFileName, ArrayList<File> filesToZip) {
        HttpServletResponse response= data.getResponse();
        String contentType = "application/zip";
        response.setContentType(contentType);
        response.setHeader("Content-Disposition","inline;filename=" + zippedFileName);
        try {
            OutputStream outStream = response.getOutputStream();
            byte b[] = new byte[1024];
            ZipOutputStream zout = new ZipOutputStream(outStream);
            zout.setLevel(Deflater.DEFAULT_COMPRESSION);
            for(int i = 0; i < filesToZip.size(); i++) {
              InputStream in = new FileInputStream(filesToZip.get(i));
              ZipEntry e = new ZipEntry(filesToZip.get(i).getName().replace(File.separatorChar,'/'));
              zout.putNextEntry(e);
              int len=0;
              while((len=in.read(b)) != -1) {
                zout.write(b,0,len);
                }
              zout.closeEntry();
              }
            zout.close();
        }catch(Exception e) {
           // e.printStackTrace();
            logger.error("Unable to create a zip file",e);
        }
    }
    
    static Logger logger = Logger.getLogger(XDATScreen_downloadt1_xnat_petSessionData.class);
  

    
   
    
}
