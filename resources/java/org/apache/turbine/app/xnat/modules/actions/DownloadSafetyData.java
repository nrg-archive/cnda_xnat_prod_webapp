package org.apache.turbine.app.xnat.modules.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ParameterParser;
import org.apache.velocity.context.Context;
import org.nrg.diantu.utils.UploadUtils;
import org.nrg.xdat.turbine.modules.actions.SecureAction;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xnat.turbine.utils.ArcSpecManager;
import org.restlet.data.Response;
import org.restlet.resource.Representation;

import au.com.bytecode.opencsv.CSVReader;

public class DownloadSafetyData extends SecureAction {
	static Logger logger = Logger.getLogger(DownloadSafetyData.class);

	@Override
	public void doPerform(RunData data, Context context) {
		ParameterParser paramParser = data.getParameters();

		// Get download date
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String timestamp = sdf.format(new Date());

		UploadUtils.initialize(TurbineUtils.getUser(data));

		try {
			// Get a list of all reads
			List<String[]> results = null;
			String readsLookupUri = "data/archive/experiments?xnat:experimentData/meta/status=locked,active&xsiType=mayo:mayoSafetyRead&format=csv";
			Response readsLookup = UploadUtils.get(readsLookupUri);

			Representation entity = readsLookup.getEntity();

			String text = entity.getText();

			StringReader mrsr = new StringReader(text);
			CSVReader mrCsvReader = new CSVReader(mrsr);

			results = mrCsvReader.readAll();
			mrCsvReader.close();

			// If we got any reads, retrieve each PDF and bundle together
			if (results != null && !results.isEmpty() && results.size() > 1) {
				// Remove header
				results.remove(0);

				File buildDir = createBuildDirectory();
				buildDir.mkdirs();

				File dl = new File(buildDir, "dca-safety-download" + timestamp + ".log");
				PrintWriter dlogger = new PrintWriter(dl);

				List<String> resourceLookupUris = new ArrayList<String>();
				// ID, Subject ID, Subject Label, subject Id, id, project, date, xsitype, label, datetme, uri
				for (String[] result : results) {
					resourceLookupUris.add(result[result.length - 1] + "/resources/Safety/files?format=csv");
				}

				Map<String, Response> responses = UploadUtils.bulkGet(resourceLookupUris);
				List<String[]> fileListing = new ArrayList<String[]>();
				String[] headers = null;

				for (Entry<String, Response> e : responses.entrySet()) {
					Response r = e.getValue();

					if (r.getStatus().isSuccess() && r.getEntity() != null) {
						try {
							String listing = r.getEntity().getText();
							System.out.println(listing);

							if (listing != null && !listing.isEmpty()) {
								StringReader sr = new StringReader(listing);
								CSVReader csvReader = new CSVReader(sr);
								List<String[]> resources = csvReader.readAll();
								csvReader.close();

								if (resources.size() > 1) {
									if (headers == null) {
										headers = resources.get(0);
									}

									fileListing.addAll(resources.subList(1, resources.size()));
								}
							}
						} catch (IOException ioe) {
							logger.error("BulkDownload error retrieving source file listing " + e.getKey(), ioe);
						}
					} else {
						dlogger.println("ERROR " + r.getStatus() + " when retrieving source file listing from "
								+ e.getKey() + "\r\n");
					}
				}

				int nameIndex = Arrays.asList(headers).indexOf("Name");
				int uriIndex = Arrays.asList(headers).indexOf("URI");

				for (String[] s : fileListing) {
					System.out.println(s[uriIndex]);
				}

				Response resource;
				InputStream f = null;

				File bdz = new File(buildDir, "DCASafetyBulkDownload" + timestamp + ".zip");
				ZipOutputStream out = new ZipOutputStream(new FileOutputStream(bdz));

				for (String[] s : fileListing) {

					//if (s[nameIndex].toLowerCase().endsWith(".zip")) {
					if (s[nameIndex].toLowerCase().endsWith(".pdf")) {
						try {
							resource = UploadUtils.get(s[uriIndex]);

							f = resource.getEntity().getStream();

							ZipEntry z = new ZipEntry(s[nameIndex]);

							out.putNextEntry(z);
							IOUtils.copy(f, out);

							dlogger.print("Added " + s[nameIndex] + " to zip file\r\n");
						} catch (IOException ioe) {
							dlogger.print("ERROR adding " + s[nameIndex] + " to zip file\r\n"); // Log and continue
						} finally {
							IOUtils.closeQuietly(f);
						}
					}
				}

				try {
					dlogger.flush();
					dlogger.close();

					InputStream i = new FileInputStream(dl);

					ZipEntry z = new ZipEntry("dca-safety-download" + timestamp + ".log");
					out.putNextEntry(z);
					IOUtils.copy(i, out);
				} catch (IOException ioe) {
					logger.error("BulkDownload: Could not add download log to the bundle.", ioe);
				}

				IOUtils.closeQuietly(out);

				HttpServletResponse res = data.getResponse();
				res.setContentType("application/zip");
				res.setHeader("Content-Disposition", "attachment; filename=\"DCASafetyBulkDownload" + timestamp
						+ ".zip\"");

				OutputStream hsr = res.getOutputStream();
				FileInputStream zipin = new FileInputStream(bdz);
				IOUtils.copy(zipin, hsr);

				hsr.flush();
			}
		} catch (IOException ioe) {
			logger.error("BulkDownload: I/O Error", ioe);
		}
	}

	private static File createBuildDirectory() {
		File buildDirectory;

		// Create temp directory
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String timestamp = sdf.format(new Date());

		String buildDirectoryPath = ArcSpecManager.GetInstance().getGlobalBuildPath() + "bulk" + timestamp;
		System.out.println("Build directory is " + buildDirectoryPath);

		buildDirectory = new File(buildDirectoryPath);

		try {
			buildDirectory.mkdirs();

			logger.debug("Build directory created");
		} catch (Exception e) {
			logger.error("BulkDownload: Can't create dirs " + buildDirectory.getPath() + " due to " + e.getMessage(), e);
		}

		logger.debug("Build directory path is " + buildDirectory.getPath());

		return buildDirectory;
	}
}
