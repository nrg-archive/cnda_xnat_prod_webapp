package org.apache.turbine.app.xnat.modules.screens;

import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ParameterParser;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.screens.SecureScreen;

public class DownloadSafetyDataResult extends SecureScreen {

	@Override
	protected void doBuildTemplate(RunData data, Context context) throws Exception {
		ParameterParser paramParser = data.getParameters();

		String subjectID = paramParser.getString("subjectID");
		String readID = paramParser.getString("readID");

		context.put("subjectID", subjectID);
		context.put("readID", readID);
	}
}
