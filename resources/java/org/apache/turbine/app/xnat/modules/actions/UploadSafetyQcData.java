package org.apache.turbine.app.xnat.modules.actions;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.fileupload.FileItem;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ParameterParser;
import org.apache.velocity.context.Context;
import org.nrg.diantu.safety.UploadHandler;
import org.nrg.xdat.turbine.modules.actions.SecureAction;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xnat.turbine.utils.ArcSpecManager;
import org.restlet.data.Status;

public class UploadSafetyQcData extends SecureAction {

	@Override
	public void doPerform(RunData data, Context context) throws Exception {
		ParameterParser paramParser = data.getParameters();

		FileItem zipFileItem = paramParser.getFileItem("read");

		Map<String, String> otherValues = new HashMap<String, String>();

		otherValues.put("project", paramParser.getString("project"));
		otherValues.put("subjectID", paramParser.getString("subjectID"));
		otherValues.put("scanDate", paramParser.getString("scanDate"));
		otherValues.put("readDate", paramParser.getString("readDate"));
		otherValues.put("readTime", paramParser.getString("readTime"));
		otherValues.put("studyUID", paramParser.getString("studyUID"));

		UploadHandler.doUpload(zipFileItem, otherValues, TurbineUtils.getUser(data));

		String result = UploadHandler.getResult();

		if (UploadHandler.getResultCode().isSuccess() || UploadHandler.getResultCode() == Status.CLIENT_ERROR_CONFLICT) {
			String siteConfigUrl = ArcSpecManager.GetInstance().getSiteUrl();
			String urlAfterProtocol = siteConfigUrl.split("//")[1]; // split off protocol
			String path = urlAfterProtocol.contains("/") ? urlAfterProtocol.split("/")[1] : "";

			String extraSlash = ((path.isEmpty() || path.endsWith("/")) ? "" : "/");

			String readLink = "";

			//String readLink = "/" + path + extraSlash + "data/archive/experiments/" + UploadHandler.getReadId()
			//		+ "?format=html";

			data.setRedirectURI(readLink);
		} else {
			data.setMessage(result);
			this.redirectToScreen("Error", data);
		}
	}
}
