package org.nrg.dcm.id;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SortedSet;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.XDATUser;
import org.nrg.riis.entities.RiisLoginRecord;
import org.nrg.riis.services.RiisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSortedSet;

/**
 * Custom DicomProjectIdentifier for the Riis Integration Module. 
 * Determines the projectId associated with a Dicom file based on 
 * the loginTime on the Riis System. (Stored in RiisLoginRecord) 
 */
public final class RiisDicomProjectIdentifier implements DicomProjectIdentifier {
   
   private static final ImmutableSortedSet<Integer> tags = ImmutableSortedSet.of();
   
   private RiisService  _riisService;  //The RiisService for retrieving the projectId.
   private final String _aeTitle;      //The AE Title associated with this Identifier
   
   //Expected date format to be extracted from the dicom file "Tag.StudyDate_Tag.StudyTime"
   //DICOM standard states that the date format yyyy.MM.dd should be supported for backward compatibility.
   //Currently this module does not support this. 
   private final SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd_HHmmss");
   
   private final Logger logger = LoggerFactory.getLogger(RiisDicomProjectIdentifier.class);
    
   /**
    * Class constructor - Sets the _aeTitle for this class. 
    * @param aeTitle - The DICOM aeTitle this ProjectIdentifier is associated with. 
    */
   public RiisDicomProjectIdentifier(final String aeTitle) { _aeTitle = aeTitle;}

   @Override
   public SortedSet<Integer> getTags() { return tags; }

   @Override
   public XnatProjectdata apply(final XDATUser user, final DicomObject o) {
      return XnatProjectdata.getProjectByIDorAlias(getProjectId(o), user, false);
   }
   
   /**
    * Method retrieves the RiisService from XDAT if it is null.
    * (I think) the Dicom Identifiers are created before the ContextServices,
    * so bad things happen if you put this in the constructor.
    */
   private void getService(){
      if (null == _riisService){
         _riisService = XDAT.getContextService().getBean(RiisService.class);
      }
   }
   
  /**
   * Gets the name of the project from the RiisService for the given dicom object.
   * @param  DicomObject o - The dicom object we want to know about.
   * @return String projName - The name of the project, or an empty 
   *         string "" if no project name was found. 
   */
   private String getProjectId(final DicomObject o){
      //Set a default project so we know where it should
      //go if we dont find a project id.  If the default project
      //doesnt exist then we stick it in unassigned. 
      String projId = _aeTitle + "_unassigned";
      
      //Get the services from XDAT if we havent done so already.
      getService();
      
      //Retrieve the Dicom Date Strings from the Header.
      String dicomDateStr = o.getString(Tag.StudyDate) + "_" + o.getString(Tag.StudyTime);
      
      //Make sure the service still isn't null. 
      if(null != _riisService){
         
         try{
            //Convert the dateStr into a Java Date object using SimpleDateFormat.parse()
            Date dicomDate = f.parse(dicomDateStr);
         
            //Get the LoginRecord with the closest date/time to the date/time in the dicom file ("StudyDate_StudyTime")
            logger.debug("Calling getLoginRecordWithClosestDate(" + dicomDateStr + "," + _aeTitle +")");
            RiisLoginRecord rec = _riisService.getLoginRecordWithClosestDate(dicomDate, _aeTitle);
         
            //If the LoginRecord is not null, get the projectId associated with this login record.
            if(rec != null){ projId = rec.getProjectId(); }
            
         }catch(Exception e){ 
            logger.error("The Study Date/Study Time extracted from dicom was not in the expected format. Date recieved: " + 
                          dicomDateStr + " Expected format: yyyyMMdd_HHmmss", e);
         }
      }
      logger.debug("getProjectId() returning" + projId);
      return projId;
   }
}
