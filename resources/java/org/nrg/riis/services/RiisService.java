package org.nrg.riis.services;

import org.nrg.riis.entities.RiisLoginRecord;
import java.text.ParseException;
import java.util.List;
import java.util.Date;

/**
 * RiisService interface.  All methods implemented in DefaultRiisService.java
 */
public interface RiisService{
   
   public void storeLoginRecord(final String projectId, final Date loginTime, final String aeTitle);
   
   public List <RiisLoginRecord> getLoginRecords(final int maxResults);
   
   public List <RiisLoginRecord> getLoginRecordsForAeTitle(final String aeTitle, final int maxResults);
      
   public List <RiisLoginRecord> getLoginRecordsForProjectId(final String projectId, final int maxResults);
   
   public RiisLoginRecord getLoginRecordWithClosestDate(final Date dicomDate, final String aeTitle);
   
   public boolean canUserUseRiis(String userName);
}
