package org.nrg.riis.daos;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.springframework.stereotype.Repository;

import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.nrg.riis.entities.RiisLoginRecord;
import org.nrg.config.entities.Configuration;

@Repository
public final class RiisLoginRecordDAO extends AbstractHibernateDAO<RiisLoginRecord> {

   /** 
    * Returns a list of all LoginRecords stored in the database.
    * List is ordered by descending loginTime. 
    * @return list of all RiisLoginRecord objects. Null size == 0;
    */
   public List<RiisLoginRecord> getAll(){      
        List retList = getSession().createCriteria(getParameterizedType())
                                   .addOrder(Order.desc("loginTime"))
                                   .list();   
        
        // If the retList is size 0: return null, else return retList.
        return (retList == null || retList.size() == 0) ? null : retList;
   }
   
   /** 
    * Returns a list of all LoginRecords stored in the database.
    * List is ordered by descending loginTime. 
    * @param int maxResults - the maximum number of records to return.
    * @return list of all RiisLoginRecord objects. Null size == 0;
    */
   public List<RiisLoginRecord> getAll(final int maxResults){      
        List retList = getSession().createCriteria(getParameterizedType())
                                   .addOrder(Order.desc("loginTime"))
                                   .setMaxResults(maxResults)
                                   .list();   
        
        // If the retList is size 0: return null, else return retList.
        return (retList == null || retList.size() == 0) ? null : retList;
   }
   
   /**
    * Returns a list of all LoginRecords with the given aeTitle.
    * List is ordered by descending loginTime.  
    * @param String aeTitle - the aeTitle that we want to check for.
    * @param int maxResults - the maximum number of records to return.
    * @return list of all RiisLoginRecord objects with the aeTitle. Null if size == 0.
    */
   public List<RiisLoginRecord> findRecordsByAeTitle(final String aeTitle, final int maxResults){   
      List retList = getSession().createCriteria(getParameterizedType())
                                 .add(Restrictions.eq("aeTitle", aeTitle))
                                 .addOrder(Order.desc("loginTime"))
                                 .setMaxResults(maxResults)
                                 .list();
      
      // If the retList is size 0: return null, else return retList.
      return (retList == null || retList.size() == 0) ? null : retList;
   }
   
   /**
    * Returns a list of all LoginRecords for the given project.
    * List is ordered by descending loginTime.  
    * @param String project - the project that we want to check for.
    * @param int maxResults - the maximum number of records to return.
    * @return list of all RiisLoginRecord objects wih the project. Null if size == 0.
    */
   public List<RiisLoginRecord> findRecordsByProjectId(final String projectId, final int maxResults){
      List retList = getSession().createCriteria(getParameterizedType())
                                 .add(Restrictions.eq("projectId", projectId))
                                 .addOrder(Order.desc("loginTime"))
                                 .setMaxResults(maxResults)
                                 .list();
      
      // If the retList is size 0: return null, else return retList
      return (retList == null || retList.size() == 0) ? null : retList;
   }
   
   /**
    * Returns a list of all LoginRecords for the given aeTitle where the 
    * login time is between the two given dates. (minDate and maxDate) 
    * List is ordered by descending loginTime. 
    * @param minDate - the minimum date.
    * @param maxDate - the maximum date.
    * @param aeTitle - the aeTitle to filter on.
    * @return retList - a list of loginRecords. Null if size == 0.
    */
   public List<RiisLoginRecord> findRecordsWithinDateRange(final Date minDate, final Date maxDate, final String aeTitle){
      List retList = getSession().createCriteria(getParameterizedType())
                                 .add(Restrictions.eq("aeTitle", aeTitle))
                                 .add(Restrictions.between("loginTime", minDate, maxDate))
                                 .addOrder(Order.desc("loginTime"))
                                 .list();
      
      // If the retList is size 0: return null, else return retList
      return (retList == null || retList.size() == 0) ? null : retList;
   }
} 
