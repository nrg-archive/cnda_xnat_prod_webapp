package org.nrg.riis.entities;

import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;
import org.nrg.framework.orm.hibernate.annotations.Auditable;
import javax.persistence.Entity;
import java.util.Date;
import java.lang.StringBuilder;

/**
 * RiisLoginRecord Hibernate Entitiy.  This is represents one
 * row in the database. 
 */
@Entity
@Auditable
public final class RiisLoginRecord extends AbstractHibernateEntity {
   
   private String  projectId;  //Id of the project
   private String  aeTitle;    //The Dicom AE Title we are expecting the scan to come to.  
   private Date    loginTime;  //The login time on the RIIS System. 
   
   //Default constructor 
   public RiisLoginRecord(){ }
   
   /**
    *  Class Constructor - Sets the private memebers projectId, loginTime and aeTitle.
    *  @param String projectId - The ID of the project.
    *  @param String aeTitle   - The AE title we are expecting the scan to come from.
    *  @param Date loginTime   - The date/time of the RIIS login.
    */
   public RiisLoginRecord(final String projectId, final Date loginTime, final String aeTitle){
      this.projectId = projectId;
      this.aeTitle   = aeTitle;
      this.loginTime = loginTime;
   }
   
   /**
    * Prints all private members to a formatted string.
    * @return The string representation of the loginRecord.
    */
   @Override public String toString(){
      StringBuilder strB = new StringBuilder();
      strB.append("Project Id: ").append(this.projectId)
          .append("\tLogin Time: ").append(this.loginTime)
          .append("\tAE Title: ").append(this.aeTitle);
      
      return strB.toString();
   }

/*                                          *
 *        Accessors and Mutators            *
 *                                          */
   
   public String getProjectId(){ return projectId; }
   
   public String getAeTitle(){ return aeTitle; }
   
   public Date getLoginTime(){ return loginTime; }

   public void setProjectId(final String projectId){ this.projectId = projectId; }
   
   public void setAeTitle(final String aeTitle){ this.aeTitle = aeTitle; }
   
   public void setLoginTime(final Date loginTime){ this.loginTime = loginTime; }
   
}