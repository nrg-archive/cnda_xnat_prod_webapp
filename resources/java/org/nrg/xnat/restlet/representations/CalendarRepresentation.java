package org.nrg.xnat.restlet.representations;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.util.ArrayList;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.lang.StringBuilder;
import org.apache.log4j.Logger;
import org.restlet.data.MediaType;
import org.restlet.resource.OutputRepresentation;
import org.nrg.xft.XFT;
import org.nrg.xft.XFTTable;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnatCalendar.CalendarDataManager;

public class CalendarRepresentation extends OutputRepresentation {
   
   protected final CalendarDataManager _calendarData;
   private final static Logger _logger = Logger.getLogger(CalendarRepresentation.class);
   private MediaType _mt = null;
   
   public CalendarRepresentation(CalendarDataManager calendarData, MediaType mediaType) throws Exception{
      
      super(mediaType);
      _mt = mediaType;

      if(null == calendarData || calendarData.getNumEvents() == 0){
         throw new Exception("Calendar data is not valid. Cannot create calendar.");
      }
      _calendarData = calendarData;
      
      _logger.debug("New Calendar Representation");
   }
   
   @Override
   public void write(OutputStream os) throws IOException {

      OutputStreamWriter sw         = new OutputStreamWriter(os);
      BufferedWriter     writer     = new BufferedWriter(sw);
      StringBuilder      strBuilder = new StringBuilder();
      
      //Init to empty string so compiler doesnt get mad.
      String sDateStr = "";
      String eDateStr = "";
      
    //  String tz  = _calendarData.getTimeZone();
         
      //Write the calendar header
      writer.write("BEGIN:VCALENDAR\r\nVERSION:2.0\r\nPRODID:-//Washington University School of Medicine//XNAT Calendar Module v1.0//EN\r\nMETHOD:PUBLISH");
      
      //Create a new event for each session in the table
      for(int event = 0; event<_calendarData.getNumEvents(); event++){ 
         
         //Reset variables
         strBuilder.setLength(0);
         eDateStr = "";
         sDateStr = "";
         
         try{ 
            //Attempt to get the start/time date of the session.
            sDateStr = getDateString(_calendarData.getEventStartDate(event)); 
         }
         catch(Exception e){
            //Skip session if we cant find start date. 
            _logger.error("Could not retrieve start date for session " + _calendarData.getSessionId(event) + " Skipping ...",e);
            continue;
         }
         
         //If this is not an all day event get the end time of the session.
         if(!_calendarData.isAllDayEvent(event)){
            try{
               eDateStr = getDateString(_calendarData.getEventEndDate(event));
            }
            catch(Exception e){
               _logger.error("Could not get the end time of session " + _calendarData.getSessionId(event));
            }
         }
         
         //Add the session to the calendar if the user can read the project.
         _logger.debug("Creating event for session " + _calendarData.getSessionId(event));
            
         //Build the event String.
         strBuilder.append("\r\nBEGIN:VEVENT\r\nUID:")
                   .append(_calendarData.getEventUID(event)) 
                   .append("\r\nDTSTAMP:")
                   .append(sDateStr)
                   .append("\r\nDTSTART:")
                   .append(sDateStr);
         
         // Add DTEND if this is not an all day event
         if(!_calendarData.isAllDayEvent(event) && !eDateStr.equals("")){
            strBuilder.append("\r\nDTEND:").append(eDateStr);
         }
         
         strBuilder.append("\r\nORGANIZER;CN=")
                   .append(_calendarData.getOrganizerEmail())
                   .append(":MAILTO:")
                   .append(_calendarData.getOrganizerEmail())
                   .append("\r\nDESCRIPTION:")
                   .append(_calendarData.getSessionURL(event))
                   .append("\r\nSUMMARY:")
                   .append(_calendarData.getEventSummary(event))
                   .append("\r\nEND:VEVENT");
           
          //Write the event String and flush.
          writer.write(strBuilder.toString());
          writer.flush(); 
       } 
      
      writer.write("\r\nEND:VCALENDAR\r\n");
      writer.flush();
   }
   
   /**
    * Method takes two dates and parses them to form a 
    * String DateStamp that the is appropriate format for ical.
    * @param date - The date containing the day, month and year
    * @param time - The date object containing the hour, minute and seconds.
    * @return
    */
   protected String getDateString(final Date date) throws Exception{
      
      if(null == date){ throw new Exception("Date is null.  Cannot convert to string.");}
      
      //Create the appropriate date formatters
      DateFormat df = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
      
      //Format the date and time. If time is null set to 00:00:00
      return df.format(date);
   }
}