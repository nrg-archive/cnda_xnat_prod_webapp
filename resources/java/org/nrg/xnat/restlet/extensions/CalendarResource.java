package org.nrg.xnat.restlet.extensions;

import org.apache.log4j.Logger;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.ElementSecurity;
import org.nrg.xft.XFTTable;
import org.nrg.xft.db.ViewManager;
import org.nrg.xft.search.QueryOrganizer;

import java.util.Arrays;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.Variant;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Map;
import org.nrg.xnat.restlet.representations.CalendarRepresentation;
import org.nrg.xnat.restlet.representations.JSONCalendarRepresentation;
import org.nrg.xnatCalendar.CalendarDataManager;

@XnatRestlet({"/services/calendar"})
public class CalendarResource extends SecureResource {

   //Private variables for calender options and MediaType.
   private final MediaType  _mt;
   private final Map<CalendarDataManager.QueryOptionType, Set<String>> calendarOptions = new HashMap<CalendarDataManager.QueryOptionType, Set<String>>();
   
   private final static Logger _logger = Logger.getLogger(CalendarResource.class);
   
   public CalendarResource(Context context, Request request, Response response){
      
      super(context, request, response);
      setModifiable(false);
      
      //Add Variants
      getVariants().add(new Variant(MediaType.APPLICATION_JSON));
      getVariants().add(new Variant(MediaType.TEXT_CALENDAR));
      
      //Save the media type requested by the user.
      _mt = this.getRequestedMediaType();
      
      //Insert user query Variables into the calendar Options map.
      for(Map.Entry<String, String> queryEntry : this.getQueryVariableMap().entrySet()){
         insertCalendarOption(queryEntry.getKey(), queryEntry.getValue());
      }
   }
   
   /**
    * Inserts calendar options into the calendarOptions hashmap.  
    * @param k - the key  (e.g. project_id, investigator, scanner_name, etc)
    * @param v - the value (e.g. test1, doe_john, scanner1, etc)
    */
   public void insertCalendarOption(String k, String v){
      if((null != k && !k.isEmpty()) && (null != v && !v.isEmpty())){
         try{
            calendarOptions.put(CalendarDataManager.QueryOptionType.valueOf(k.toUpperCase()), new LinkedHashSet<String>(Arrays.asList(v.split(","))));
         }
         catch(Exception e){ /* Ignore invalid options */ }
      }
   }
   
   /*
    * (non-Javadoc)
    * @see org.nrg.xnat.restlet.resources.SecureResource#getRequestedMediaType()
    */
   @Override public MediaType getRequestedMediaType(){
      
      //Calendar media type by default
      MediaType retType = MediaType.TEXT_CALENDAR;
      
      //If the user requests json
    /*  if((null != this.requested_format && !this.requested_format.isEmpty()) && this.requested_format.equals("json")) {
         retType = MediaType.APPLICATION_JSON;
      } */
      
      return retType;
   }
  
   
   /*
    * (non-Javadoc)
    * @see org.restlet.resource.Resource#getRepresentation(org.restlet.resource.Variant)
    */
   @Override public Representation getRepresentation(Variant variant) { 
      try{
           CalendarDataManager c = new CalendarDataManager(calendarOptions, user);
           return (_mt == MediaType.APPLICATION_JSON) ? new JSONCalendarRepresentation(c, _mt) : new  CalendarRepresentation(c, _mt);
      }
      catch(Exception e){
         //TODO Add appropriate error message and status.
         _logger.error("",e);
         
         this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, e.getMessage());
         return null;
      }
   } 
}
