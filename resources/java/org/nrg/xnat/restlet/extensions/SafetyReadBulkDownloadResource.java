package org.nrg.xnat.restlet.extensions;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.nrg.diantu.safety.UploadHandler;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.restlet.Context;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.ext.fileupload.RestletFileUpload;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;

@XnatRestlet("/services/safetyread")
public class SafetyReadBulkDownloadResource extends SecureResource {
	public SafetyReadBulkDownloadResource(Context context, Request request, Response response) {
		super(context, request, response);
		this.getVariants().add(new Variant(MediaType.ALL));
		setModifiable(true);
	}

	@Override
	public Representation represent(Variant variant) throws ResourceException {
		Representation representation = new StringRepresentation("Hello, Safety!", MediaType.TEXT_PLAIN);
		return representation;
	}

	public void acceptRepresentation(Representation entity) throws ResourceException {
		Representation rep = null;

		System.out.println("Got request");
		StringBuilder response = new StringBuilder();

		if (entity != null && MediaType.MULTIPART_FORM_DATA.equals(entity.getMediaType(), true)) {
			// Get file items from upload
			DiskFileItemFactory factory = new DiskFileItemFactory();
			factory.setSizeThreshold(1000240);

			RestletFileUpload upload = new RestletFileUpload(factory);

			List<FileItem> items;

			try {
				items = upload.parseRequest(getRequest());
			} catch (org.apache.commons.fileupload.FileUploadException e) {
				items = Collections.emptyList();
			}

			// Look for a file with the field name "read"; put other fields in otherValues
			FileItem fi = null;
			Map<String, String> otherValues = new HashMap<String, String>();

			for (FileItem f : items) {
				if (f.isFormField()) {
					otherValues.put(f.getFieldName(), f.getString());
				} else {
					if ("read".equals(f.getFieldName())) {
						fi = f;
					}
				}
			}

			if (otherValues.isEmpty()) {
				Form form = getRequest().getResourceRef().getQueryAsForm();
				otherValues = form.getValuesMap();
			}

			if (fi != null && fi.getSize() > 0) {
				UploadHandler.doUpload(fi, otherValues, user);

				rep = new StringRepresentation(UploadHandler.getResult(), MediaType.TEXT_PLAIN);
				getResponse().setEntity(rep);
				getResponse().setStatus(UploadHandler.getResultCode());
			}
		} else {
			response.append("ERROR: No zip file found or zip file was empty.");
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			rep = new StringRepresentation(response.toString(), MediaType.TEXT_PLAIN);
		}

		getResponse().setEntity(rep);
	}
}
