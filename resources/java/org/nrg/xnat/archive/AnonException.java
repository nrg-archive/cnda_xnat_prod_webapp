package org.nrg.xnat.archive;


public class AnonException extends Exception {

	public AnonException(Throwable e) {
		super(e);
	}

}
