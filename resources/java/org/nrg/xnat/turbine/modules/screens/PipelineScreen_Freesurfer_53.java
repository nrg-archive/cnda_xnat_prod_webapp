package org.nrg.xnat.turbine.modules.screens;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.model.ArcPipelinedataI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatQcmanualassessordataI;
import org.nrg.xdat.model.XnatQcscandataI;
import org.nrg.xdat.om.*;
import org.nrg.xnat.turbine.utils.ArcSpecManager;
import org.nrg.xdat.turbine.utils.TurbineUtils;

@SuppressWarnings("unused")
public class PipelineScreen_Freesurfer_53 extends DefaultPipelineScreen {


    public final String SCANTYPE_CONFIG_TOOL_NAME = "params";
    public final String SCANTYPE_CONFIG_FILE_NAME = "mr_scan_types";
    public final String T2_CONFIG_TOOL_NAME = "params";
    public final String T2_CONFIG_FILE_NAME = "t2_scan_types";
    public final String FLAIR_CONFIG_TOOL_NAME = "params";
    public final String FLAIR_CONFIG_FILE_NAME = "flair_scan_types";


    static Logger logger = Logger.getLogger(PipelineScreen_Freesurfer_53.class);
    public void finalProcessing(RunData data, Context context){


        try {
            String pipelinePath = (String)TurbineUtils.GetPassedParameter("pipeline",data);
            String schemaType = om.getXSIType();
            ArcProject arcProject = ArcSpecManager.GetFreshInstance().getProjectArc(project);

            if (schemaType.equals(XnatProjectdata.SCHEMA_ELEMENT_NAME)) {
                ArcProjectPipeline pipelineData = (ArcProjectPipeline)arcProject.getPipelineByPath(pipelinePath);
                context.put("pipeline", pipelineData);
                setParameters(pipelineData, context);
            }else {
                ArcPipelinedataI pipelineData = arcProject.getPipelineForDescendantByPath(schemaType, pipelinePath);
                context.put("pipeline", pipelineData);
                setParameters(pipelineData, context);
            }

            if (!(om instanceof XnatImagesessiondata)) {
                String m ="Cannot run Freesurfer on a non-image session.";
                data.setMessage(m);
                throw new Exception(m);
            }

            ArrayList<XnatImagescandata> mrScans = getScansFromScantypeConfig(SCANTYPE_CONFIG_TOOL_NAME, SCANTYPE_CONFIG_FILE_NAME);
            ArrayList<XnatImagescandata> t2Scans = getScansFromScantypeConfig(T2_CONFIG_TOOL_NAME, T2_CONFIG_FILE_NAME);
            ArrayList<XnatImagescandata> flairScans = getScansFromScantypeConfig(FLAIR_CONFIG_TOOL_NAME, FLAIR_CONFIG_FILE_NAME);

            context.put("mrScans", mrScans.size()>0 ? mrScans : ((XnatImagesessiondata) om).getScans_scan());
            context.put("t2Scans", t2Scans.size()>0 ? t2Scans : ((XnatImagesessiondata) om).getScans_scan());
            context.put("flairScans", flairScans.size() > 0 ? flairScans : ((XnatImagesessiondata) om).getScans_scan());


            HashSet<XnatImagescandata> structuralScansSet = Sets.newHashSet();
            structuralScansSet.addAll(mrScans);
            structuralScansSet.addAll(t2Scans);
            structuralScansSet.addAll(flairScans);


            HashMap<String,ArrayList<String>> allScanResourceLabels = Maps.newHashMap();
            HashSet<String> scanResourceLabelSet = Sets.newHashSet();
            for (XnatImagescandata scan : structuralScansSet) {
                ArrayList<String> scanResourceLabels = Lists.newArrayList();
                for ( XnatAbstractresourceI scanResource : scan.getFile() ) {
                    if (!(scanResource instanceof XnatResourcecatalog)) {
                        continue;
                    }
                    String scanResourceLabel = scanResource.getLabel();
                    if (StringUtils.isNotBlank(scanResourceLabel)) {
                        scanResourceLabels.add(scanResourceLabel);
                    } else {
                        scanResourceLabels.add("null");
                    }
                }
                allScanResourceLabels.put(scan.getId(),scanResourceLabels);

                scanResourceLabelSet.addAll(scanResourceLabels);
                if (scanResourceLabelSet.contains("null")) {
                    scanResourceLabels.remove("null");
                }
            }
            context.put("allScanResourceLabels",allScanResourceLabels);
            context.put("scanResourceLabelSet",scanResourceLabelSet);

            HashSet<String> bestScans = Sets.newHashSet();
            String qcFlagParam = projectParameters.get("use_manual_qc").getCsvvalues();
            if (StringUtils.isNotBlank(qcFlagParam) && qcFlagParam.equals("1")) {
                final ArrayList<XnatQcmanualassessordataI> assessors = ((XnatImagesessiondata) om).getMinimalLoadAssessors(XnatQcmanualassessordata.SCHEMA_ELEMENT_NAME);
                if (assessors != null) {
                    for (XnatQcmanualassessordataI qc : assessors) {
                        if (qc != null) {
                            List<XnatQcscandataI> qcScans = qc.getScans_scan();
                            if (qcScans != null) {
                                for (XnatQcscandataI qcScan : qcScans) {
                                    if (qcScan.getXSIType().equals("mayo:mayoMrQcScanData")) {
                                        String qcRating = qcScan.getRating();
                                        if (StringUtils.isNotBlank(qcRating) && qcRating.equals("1")) {
                                            bestScans.add(qcScan.getImagescanId());
                                        }
                                    } else {
                                        String qcRatingScale = qcScan.getRating_scale();
                                        String qcRating = qcScan.getRating();
                                        if (StringUtils.isNotBlank(qcRatingScale) && StringUtils.isNotBlank(qcRating) &&
                                                qcRatingScale.equalsIgnoreCase("DIAN") && qcRating.equals("1")) {
                                            bestScans.add(qcScan.getImagescanId());
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (bestScans.isEmpty()) {
                    String m = "Cannot run Freesurfer. Could not find best scan from manual QC.";
                    data.setMessage(m);
                    throw new Exception(m);
                }
            }
            context.put("bestScans",bestScans);

            context.put("projectParameters", projectParameters);

        }catch(Exception e) {
            logger.error("Possibly the project wide pipeline has not been set", e);
            e.printStackTrace();
        }
    }
}
