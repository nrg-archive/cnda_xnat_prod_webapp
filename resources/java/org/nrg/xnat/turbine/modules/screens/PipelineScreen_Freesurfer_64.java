
package org.nrg.xnat.turbine.modules.screens;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.ArrayList;
import java.util.List;

import org.nrg.xdat.model.ArcPipelinedataI;
import org.nrg.xdat.om.*;
import org.nrg.xnat.turbine.utils.ArcSpecManager;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xnat.turbine.modules.screens.DefaultPipelineScreen;

public class PipelineScreen_Freesurfer_64 extends DefaultPipelineScreen {

    // final String LOCATION = "Freesurfer";
    // final String NAME =  "Freesurfer_64bit_v5.1.xml";

    public static final String MPRAGE = "MPRAGE";
    public static final String MPRAGE_PARAM = "mprs";

    String mprageScanType;
    static Logger logger = Logger.getLogger(PipelineScreen_Freesurfer_64.class);
    public void finalProcessing(RunData data, Context context){


        try {
            String projectId = ((String)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedParameter("project",data));
            String pipelinePath = ((String)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedParameter("pipeline",data));
            String schemaType = ((String)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedParameter("schema_type",data));
            ArcProject arcProject = ArcSpecManager.GetFreshInstance().getProjectArc(projectId);

            if (schemaType.equals(XnatProjectdata.SCHEMA_ELEMENT_NAME)) {
                ArcProjectPipeline pipelineData = (ArcProjectPipeline)arcProject.getPipelineByPath(pipelinePath);
                context.put("pipeline", pipelineData);
                setParameters(pipelineData, context);
            }else {
                ArcPipelinedataI pipelineData = arcProject.getPipelineForDescendantByPath(schemaType, pipelinePath);
                context.put("pipeline", pipelineData);
                setParameters(pipelineData, context);
            }

            if (! (om instanceof XnatMrsessiondata || om instanceof XnatPetsessiondata)) {
                throw new Exception("om should be of type XnatMrsessiondata or XnatPetsessiondata");
            }
            if (om instanceof XnatMrsessiondata) {
                XnatMrsessiondata mr = (XnatMrsessiondata) om;

                String selfStatus = WrkWorkflowdata.GetLatestWorkFlowStatusByPipeline(mr.getId(), XnatMrsessiondata.SCHEMA_ELEMENT_NAME, pipelinePath, mr.getProject(), TurbineUtils.getUser(data));
                if (selfStatus != null && selfStatus.equalsIgnoreCase(WrkWorkflowdata.COMPLETE)) {
                    data.setMessage("This pipeline has already completed. Relaunching the pipeline may result in loss of processed files");
                }

                ArcPipelineparameterdata mprageParam = getProjectPipelineSetting(MPRAGE_PARAM);
                if (mprageParam != null) {
                    mprageScanType = mprageParam.getCsvvalues();
                    setHasDicomFiles(mr, mprageScanType, context);
                }

            } else if (om instanceof XnatPetsessiondata) {
                XnatPetsessiondata petmr = (XnatPetsessiondata) om;

                String selfStatus = WrkWorkflowdata.GetLatestWorkFlowStatusByPipeline(petmr.getId(), XnatPetsessiondata.SCHEMA_ELEMENT_NAME, pipelinePath, petmr.getProject(), TurbineUtils.getUser(data));
                if (selfStatus != null && selfStatus.equalsIgnoreCase(WrkWorkflowdata.COMPLETE)) {
                    data.setMessage("This pipeline has already completed. Relaunching the pipeline may result in loss of processed files");
                }

                ArcPipelineparameterdata mprageParam = getProjectPipelineSetting(MPRAGE_PARAM);
                if (mprageParam != null) {
                    mprageScanType = mprageParam.getCsvvalues();
                    setHasDicomFiles(petmr, mprageScanType, context);
                }
            }

            LinkedHashMap<String,String> buildableScanTypes = new LinkedHashMap<String,String>();
            if (mprageScanType != null) {
                buildableScanTypes.put(MPRAGE, mprageScanType);
            }
            context.put("buildableScanTypes", buildableScanTypes);
            context.put("projectSettings", projectParameters);
            context.put("mpragekey", MPRAGE);

        }catch(Exception e) {
            logger.error("Possibly the project wide pipeline has not been set", e);
            e.printStackTrace();
        }
    }

}
