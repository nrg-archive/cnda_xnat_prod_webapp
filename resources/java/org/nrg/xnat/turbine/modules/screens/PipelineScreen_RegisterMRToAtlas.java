
package org.nrg.xnat.turbine.modules.screens;

import java.util.*;

import org.nrg.xdat.model.ArcPipelinedataI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.om.*;
import org.nrg.xnat.turbine.utils.ArcSpecManager;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.utils.TurbineUtils;

public class PipelineScreen_RegisterMRToAtlas extends DefaultPipelineScreen  {

    public static final String SCANTYPE_PARAM = "scantype";

    static Logger logger = Logger.getLogger(PipelineScreen_RegisterMRToAtlas.class);

    public void finalProcessing(RunData data, Context context){

        try {
            String projectId = ((String)TurbineUtils.GetPassedParameter("project",data));
            String pipelinePath = ((String)TurbineUtils.GetPassedParameter("pipeline",data));
            String schemaType = ((String)TurbineUtils.GetPassedParameter("schema_type",data));
            ArcProject arcProject = ArcSpecManager.GetFreshInstance().getProjectArc(projectId);

            if (schemaType.equals(XnatProjectdata.SCHEMA_ELEMENT_NAME)) {
                ArcProjectPipeline pipelineData = (ArcProjectPipeline)arcProject.getPipelineByPath(pipelinePath);
                context.put("pipeline", pipelineData);
                setParameters(pipelineData, context);
            }else {
                ArcPipelinedataI pipelineData = arcProject.getPipelineForDescendantByPath(schemaType, pipelinePath);
                context.put("pipeline", pipelineData);
                setParameters(pipelineData, context);
            }


            HashSet<XnatImagescandata> returnScans = new HashSet<>();
            ArcPipelineparameterdata scanTypeParam = getProjectPipelineSetting(SCANTYPE_PARAM);
            String mrScanType;
            if (scanTypeParam != null) {
                mrScanType = scanTypeParam.getCsvvalues();
            } else {
                String m = "Cannot determine T1 scan type. Thus, cannot run pipeline.";
                data.setMessage(m);
                throw new Exception(m);

            }

            for (String scanType : mrScanType.split(",")) {
                ArrayList<XnatImagescandata> scans = ((XnatImagesessiondata) om).getScansByType(scanType.trim());
                if (scans != null && !scans.isEmpty()) {
                    returnScans.addAll(scans);
                }
            }

            if (returnScans.isEmpty()) {
                String m = "Cannot run pipeline. There are no scans of type "+mrScanType+" in the session.";
                data.setMessage(m);
                throw new Exception(m);
            }

            context.put("mrScans",returnScans);

            HashMap<String,ArrayList<String>> allScanResourceLabels = new HashMap<String,ArrayList<String>>();
            Set<String> scanResourceLabelSet = new HashSet<String>();
            for (XnatImagescandata scan : returnScans) {
                ArrayList<String> scanResourceLabels = new ArrayList<String>();
                for ( XnatAbstractresourceI scanResource : scan.getFile() ) {
                    if (!(scanResource instanceof XnatResourcecatalog)) {
                        continue;
                    }
                    String scanResourceLabel = scanResource.getLabel();
                    if (StringUtils.isNotBlank(scanResourceLabel)) {
                        scanResourceLabels.add(scanResourceLabel);
                        scanResourceLabelSet.add(scanResourceLabel);
                    } else {
                        scanResourceLabels.add("null");
                    }
                }
                allScanResourceLabels.put(scan.getId(),scanResourceLabels);
            }
            context.put("allScanResourceLabels",allScanResourceLabels);
            context.put("scanResourceLabelSet",scanResourceLabelSet);


            context.put("projectSettings", projectParameters);


        }catch(Exception e) {
            logger.error("Possibly the project wide pipeline has not been set", e);
            e.printStackTrace();
        }
    }
}
