package org.nrg.xnat.turbine.modules.screens;

import java.util.ArrayList;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.turbine.modules.screens.SecureReport;

public class PipelineScreen_ManualPetProcessingStep2 extends SecureReport{
	    public void finalProcessing(RunData data, Context context)
	    {
	       context.put("step",2);
	       context.put("step1Disabled","");
	       context.put("step2Disabled","disabled");
	       XnatPetsessiondata pet = new XnatPetsessiondata(item);
	       ArrayList regions = (ArrayList)pet.getRegions_region();
	       if (regions == null || regions.size() == 0) {
	           context.put("step3Disabled","disabled");
	       }else
	           context.put("step3Disabled","");

	    }
	}


