package org.nrg.xnat.turbine.modules.screens;

import java.util.*;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.nrg.config.entities.Configuration;
import org.nrg.framework.constants.Scope;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.bean.CatCatalogBean;
import org.nrg.xdat.model.*;
import org.nrg.xdat.om.*;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.search.ItemSearch;
import org.nrg.xnat.utils.CatalogUtils;


@SuppressWarnings("unused")
public class PipelineScreen_PUP extends DefaultPipelineScreen  {
    public final String PIPELINE_NAME = "PETUnifiedPipeline";
    public final String PARAMS_CONFIG_TOOL_NAME = "pipelines";
    public final String PARAMS_CONFIG_FILE_NAME = PIPELINE_NAME + "_default_params";

    public final String SCANTYPE_CONFIG_TOOL_NAME = "params";
    public final String SCANTYPE_CONFIG_FILE_NAME = "mr_scan_types";

    public final String FILTERMAP_CONFIG_TOOL_NAME = "pipelines";
    public final String FILTERMAP_CONFIG_FILE_NAME = PIPELINE_NAME + "_scanner_filter_map";

    public final String HLMAP_CONFIG_TOOL_NAME = "pipelines";
    public final String HLMAP_CONFIG_FILE_NAME = PIPELINE_NAME + "_tracer_hl_map";

    private ObjectMapper mapper = new ObjectMapper();
    private List<String> mrScanTypeList;

    public void finalProcessing(RunData data, Context context) {
        HashMap<String,Object> objectsWeNeed = Maps.newHashMap();

        XnatImagesessiondata pet = new XnatImagesessiondata(item);
        context.put("pet", pet);

        String mrScanTypeCsv;
        try {
            mrScanTypeCsv = getConfig(SCANTYPE_CONFIG_TOOL_NAME, SCANTYPE_CONFIG_FILE_NAME, project);
        } catch (Exception e) {
            logger.error("PipelineScreen_PUP: Cannot determine T1 scan types from config");
            mrScanTypeCsv = "";
        }
        mrScanTypeList = Lists.transform(Lists.newArrayList(mrScanTypeCsv.split(",")), new Function<String, String>() {
            @Override
            public String apply(String input) {
                return input.trim();
            }
        });

        // Session-level params
        objectsWeNeed.put("sessionId", pet.getId());
        objectsWeNeed.put("sessionLabel", pet.getLabel());

        final String tracer;
        if (pet.getXSIType().equalsIgnoreCase("xnat:petSessionData")) {
            tracer = (new XnatPetsessiondata(item)).getTracer_name();
        } else if (pet.getXSIType().equalsIgnoreCase("xnat:petmrSessionData")) {
            tracer = (new XnatPetmrsessiondata(item)).getTracer_name();
        } else {
            data.setMessage("An error has occurred. Cannot find tracer for session of type "+pet.getXSIType()+".");
            return;
        }
        if (StringUtils.isBlank(tracer)) {
            data.setMessage("An error has occurred. Session has no tracer information.");
            return;
        }
        objectsWeNeed.put("tracer",tracer);

        try {
            String half_life = getHalfLifeFromTracer(tracer);
            objectsWeNeed.put("half_life",half_life);
        } catch (Exception e) {
            data.setMessage("An error has occurred. "+e.getMessage());
            return;
        }

        // Scan-level params
        final List<Map<String, String>> petScans = Lists.newArrayList();
        for (final XnatImagescandataI scan : pet.getScans_scan()) {
            if (scan.getXSIType().equalsIgnoreCase("xnat:petScanData")) {
                final Map<String, String> scanInfo = Maps.newHashMap();

                final XnatPetscandata petScan = (XnatPetscandata) scan;
                scanInfo.put("id", petScan.getId());
                scanInfo.put("label", petScan.getType());

                final String scanner = StringUtils.isBlank(scan.getScanner_model()) ? "" : scan.getScanner_model();
                try {
                    Map<String,String> filterParams = getFilterParamsFromScanner(scanner);
                    scanInfo.put("filterxy",filterParams.get("filterxy"));
                    scanInfo.put("filterz",filterParams.get("filterz"));
                } catch (Exception e) {
                    data.setMessage("An error has occurred. "+e.getMessage());
                    return;
                }

                boolean hasDICOM = false;
                boolean hasNIFTI = false;
                for (final XnatAbstractresourceI scanResource : scan.getFile() ) {
                    if (StringUtils.isNotBlank(scanResource.getLabel())) {
                        if (scanResource.getLabel().equalsIgnoreCase("DICOM")) {
                            hasDICOM = true;
                        } else if (scanResource.getLabel().equalsIgnoreCase("NIFTI")) {
                            hasNIFTI = true;
                        }
                    }
                }

                if (hasDICOM && hasNIFTI) {
                    // This is the most complex case to handle: scan has both dicom and nifti resources.
                    // We will just add two "different" scans to the petScans list for each resource.
                    // This will allow the user to select the scan and the resource at the same time using
                    // the drop-down that we already have.
                    final Map<String, String> secondScanInfo = Maps.newHashMap(scanInfo);
                    secondScanInfo.put("label", secondScanInfo.get("label") + " - NIFTI");
                    secondScanInfo.put("format", "5");
                    secondScanInfo.put("format_type", "NIFTI");
                    petScans.add(secondScanInfo);

                    scanInfo.put("label", scanInfo.get("label") + " - DICOM");
                    scanInfo.put("format", "0");
                    scanInfo.put("format_type", "DICOM");
                } else if (hasDICOM) {
                    scanInfo.put("format", "0");
                    scanInfo.put("format_type", "DICOM");
                } else if (hasNIFTI) {
                    scanInfo.put("format", "5");
                    scanInfo.put("format_type", "NIFTI");
                } else {
                    // If the scan does not have a DICOM or NIFTI resource, assume ECAT
                    scanInfo.put("format", "1");
                    scanInfo.put("format_type", "_null_");
                }

                petScans.add(scanInfo);
            }
        }
        objectsWeNeed.put("petScanId", petScans);

        // Subject-level params
        try {
            XnatSubjectdata subject = new XnatSubjectdata(ItemSearch.GetItem("xnat:subjectData.ID", pet.getSubjectId(), TurbineUtils.getUser(data), false));
            objectsWeNeed.put("subject",subject.getLabel());
            objectsWeNeed.put("subjectResources",getSubjectResources(subject));
        } catch (Exception e) {
            data.setMessage("An error has occurred. Could not find subject resources. "+e.getMessage());
            return;
        }

        // Project param defaults
        try{
            objectsWeNeed.put("projectParams", getProjectParamDefaults());
        } catch (Exception e) {
            data.setMessage("An error has occurred. Could not find project parameters. "+e.getMessage());
            return;
        }

        // Write json, put into velocity context
        try {
            context.put("jsonWeNeed", mapper.writeValueAsString(objectsWeNeed));
        } catch (Exception e) {
            data.setMessage("Error rendering Java objects into JSON. "+e.getMessage());
        }
    }

    public ArrayList<HashMap<String,Object>> getSubjectResources(XnatSubjectdata subject) throws Exception {
        String REGISTERED_MR_RESOURCE_NAME_ROOT = "REGISTERED_";
        String CUSTOM_ROI_RESOURCE_NAME_ROOT = "ROI_";
        String ROI_IMG_FORMAT= "\\.4dfp\\.img";
        String ROI_TEXT_FORMAT= "\\.txt";

        ArrayList<HashMap<String,Object>> resources = Lists.newArrayList();

        for (XnatSubjectassessordataI expt : subject.getExperiments_experiment()) {
            XnatImagesessiondata imageSession;
            if (!(expt instanceof XnatImagesessiondata)) {
                continue;
            }
            try {
                imageSession = (XnatImagesessiondata)expt;
            } catch (ClassCastException e) {
                continue;
            }

            List<XnatImagescandata> scans = imageSession.getScansByTypeList(mrScanTypeList);
            if (scans == null || scans.size() == 0) {
                continue;
            }

            String rootPath = imageSession.getArchiveRootPath();

            // Get info that all MR sessions will have
            HashMap<String,Object> sessionInfo = Maps.newHashMap();
            sessionInfo.put("mrlabel", imageSession.getLabel());
            sessionInfo.put("mrid", imageSession.getId());
            sessionInfo.put("mrdate", imageSession.getDate() != null ? imageSession.getDate().toString() : "");

            // Check for Freesurfers
            ArrayList<HashMap<String,String>> sessionFSInfo = Lists.newArrayList();
            for (XnatImageassessordataI assessor : imageSession.getAssessors_assessor()) {
                if (assessor.getXSIType().equalsIgnoreCase("fs:fsData")) {
                    FsFsdata fsAssessor = (FsFsdata)assessor;
                    HashMap<String,String> fsInfo = Maps.newHashMap();
                    fsInfo.put("fsid", fsAssessor.getId());
                    fsInfo.put("fsdate", fsAssessor.getDate() != null ? fsAssessor.getDate().toString() : "");
                    fsInfo.put("fsversion", fsAssessor.getFsVersion());

                    if (fsAssessor.getValidation() == null || StringUtils.isBlank(fsAssessor.getValidation().getStatus())) {
                        fsInfo.put("qcstatus","No QC");
                    } else {
                        fsInfo.put("qcstatus", fsAssessor.getValidation().getStatus());
                    }

                    sessionFSInfo.add(fsInfo);
                }
            }
            sessionInfo.put("freesurfers", sessionFSInfo);

            // Check for registered MRs and hand-drawn ROIs
            ArrayList<HashMap<String,String>> sessionCustomROIInfo = Lists.newArrayList();
            for (XnatImagescandata scan : scans) {
                HashMap<String,HashMap<String,String>> resourceAtlases = Maps.newHashMap();

                for ( XnatAbstractresourceI scanResource : scan.getFile() ) {
                    String scanResourceLabel = scanResource.getLabel();
                    if (!(scanResource instanceof XnatResourcecatalog)) {
                        continue;
                    }
                    XnatResourcecatalog scanResCat = ((XnatResourcecatalog) scanResource);

                    if (scanResourceLabel.matches(REGISTERED_MR_RESOURCE_NAME_ROOT + ".*")) {
                        String atlas = scanResourceLabel.substring(REGISTERED_MR_RESOURCE_NAME_ROOT.length());
                        HashMap<String,String> resourceAtlasInfo = resourceAtlases.get(atlas);
                        if (resourceAtlasInfo == null) {
                            resourceAtlasInfo = Maps.newHashMap();
                        }
                        resourceAtlasInfo.put("hasRegisteredMR","true");
                        resourceAtlases.put(atlas,resourceAtlasInfo);
                    }

                    if (scanResourceLabel.matches(CUSTOM_ROI_RESOURCE_NAME_ROOT + ".*")) {
                        String atlas = scanResourceLabel.substring(CUSTOM_ROI_RESOURCE_NAME_ROOT.length());

                        // We have found an ROI_{atlas} resource. Now check if it has the files we need.
                        CatCatalogBean customROICatalog = CatalogUtils.getCatalog(rootPath, scanResCat);
                        Collection<CatEntryI> roiImgCollection = CatalogUtils.getEntriesByRegex(customROICatalog, ".*" + ROI_IMG_FORMAT);
                        if (roiImgCollection.size() == 0) {
                            continue;
                        } else if (roiImgCollection.size() > 1) {
                            throw new Exception("Found more than one ROI img file for session " + imageSession.getLabel() + ", scan " + scan.getId() + ", resource " + scanResourceLabel);
                        }
                        String roiimg = Iterables.get(roiImgCollection,0).getName().replaceAll(ROI_IMG_FORMAT,"");

                        Collection<CatEntryI> roiTextFilesCollection = CatalogUtils.getEntriesByRegex(customROICatalog, ".*" + ROI_TEXT_FORMAT);
                        if (roiTextFilesCollection.size() == 0) {
                            continue;
                        } else if (roiTextFilesCollection.size() > 1) {
                            throw new Exception("Found more than one ROI txt file for session " + imageSession.getLabel() + ", scan " + scan.getId() + ", resource " + scanResourceLabel);
                        }
                        String roitextname = Iterables.get(roiTextFilesCollection, 0).getName();

                        HashMap<String,String> resourceAtlasInfo = resourceAtlases.get(atlas);
                        if (resourceAtlasInfo == null) {
                            resourceAtlasInfo = Maps.newHashMap();
                        }
                        resourceAtlasInfo.put("hasCustomROI","true");
                        resourceAtlasInfo.put("roiimg",roiimg);
                        resourceAtlasInfo.put("roitextname",roitextname);
                        resourceAtlases.put(atlas,resourceAtlasInfo);
                    }
                }
                for (Map.Entry<String,HashMap<String,String>> atlasEntry : resourceAtlases.entrySet()) {
                    String atlas = atlasEntry.getKey();
                    HashMap<String,String> atlasInfo = atlasEntry.getValue();

                    String hasCustomROI = atlasInfo.get("hasCustomROI");
                    String hasRegisteredMR = atlasInfo.get("hasRegisteredMR");
                    if (!( hasCustomROI==null || hasRegisteredMR==null || hasCustomROI.isEmpty() || hasRegisteredMR.isEmpty())) {
                        HashMap<String, String> customROIInfo = Maps.newHashMap();
                        customROIInfo.put("scanid", scan.getId());
                        customROIInfo.put("atltarg", atlas);
                        customROIInfo.put("roiimg", atlasInfo.get("roiimg"));
                        customROIInfo.put("text", atlasInfo.get("roitextname"));
                        sessionCustomROIInfo.add(customROIInfo);
                    }
                }
            }
            sessionInfo.put("customroi",sessionCustomROIInfo);

            resources.add(sessionInfo);
        }

        return resources;
    }


    public String getHalfLifeFromTracer(String tracer) throws Exception {
        String half_life_config = getConfig(HLMAP_CONFIG_TOOL_NAME, HLMAP_CONFIG_FILE_NAME);
        Map<String,String> half_life_map = mapper.readValue(half_life_config, new TypeReference<Map<String,String>>() {});
        String half_life = half_life_map.get(tracer);
        if (StringUtils.isBlank(half_life)) {
            throw new Exception("Could not find half life from tracer "+tracer);
        }
        return half_life;
    }

    public Map<String,String> getFilterParamsFromScanner(String scanner) throws Exception {
        String filterConfig = getConfig(FILTERMAP_CONFIG_TOOL_NAME, FILTERMAP_CONFIG_FILE_NAME);
        Map<String,Map<String,String>> filterMap = mapper.readValue(filterConfig, new TypeReference<Map<String,Map<String,String>>>() {});
        Map<String,String> filterParams = filterMap.get(scanner);
        if (filterParams==null || !(filterParams.containsKey("filterxy") && filterParams.containsKey("filterz"))) {
            throw new Exception("Could not find filterxy,filterz from scanner "+scanner);
        }
        return filterParams;
    }

    public String getConfig(String configToolName, String configFileName, String project) throws Exception {
        Configuration config = XDAT.getConfigService().getConfig(configToolName, configFileName, Scope.Project, project);
        if (config == null || config.getContents() == null) {
            throw new Exception("Cannot find config at /projects/"+project+"/config/"+configToolName+"/"+configFileName);
        }
        return config.getContents().trim();
    }

    public String getConfig(String configToolName, String configFileName) throws Exception {
        Configuration config = XDAT.getConfigService().getConfig(configToolName, configFileName);
        if (config == null || config.getContents() == null) {
            throw new Exception("Cannot find config at /config/"+configToolName+"/"+configFileName);
        }
        return config.getContents().trim();
    }

    public Map<String,Object> getProjectParamDefaults() {
        Map<String,Object> retParams;

        try {
            Configuration projectParamConfig = XDAT.getConfigService().getConfig(PARAMS_CONFIG_TOOL_NAME, PARAMS_CONFIG_FILE_NAME, Scope.Project, project);
            String projectParamString = projectParamConfig.getContents().trim();
            retParams = mapper.readValue(projectParamString, new TypeReference<Map<String,Object>>() {});
        } catch (Exception e) {
            retParams = Maps.newHashMap();

            for (Map.Entry<String,ArcPipelineparameterdata> paramEntry : projectParameters.entrySet()) {
                if (paramEntry.getValue() != null && StringUtils.isNotBlank(paramEntry.getValue().getCsvvalues())) {
                    HashMap<String,String> formattedParam = Maps.newHashMap();
                    formattedParam.put("default",paramEntry.getValue().getCsvvalues());
                    formattedParam.put("type","text");
                    retParams.put(paramEntry.getKey(),formattedParam);
                }
            }
        }

        return retParams;
    }

}
