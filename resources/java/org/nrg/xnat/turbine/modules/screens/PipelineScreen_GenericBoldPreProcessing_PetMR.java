package org.nrg.xnat.turbine.modules.screens;

import java.io.File;
import java.util.*;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.launchers.GenericBoldPreProcessing_PetMRLauncher;
import org.nrg.pipeline.launchers.StdBuildLauncher;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.om.*;
import org.nrg.xdat.turbine.utils.TurbineUtils;

@SuppressWarnings("unused")
public class PipelineScreen_GenericBoldPreProcessing_PetMR extends DefaultPipelineScreen {

    String mprageScanType;
    String tseScanType;
    String t1wScanType;
    String pdt2ScanType;
    String boldScanType;

    static Logger logger = Logger.getLogger(PipelineScreen_GenericBoldPreProcessing_PetMR.class);


    public void finalProcessing(RunData data, Context context){
        try {
            XnatPetsessiondata pet = (XnatPetsessiondata) om;
            context.put("pet", pet);

            ArcPipelineparameterdata mprageParam = getProjectPipelineSetting(GenericBoldPreProcessing_PetMRLauncher.MPRAGE_PARAM);
            if (mprageParam != null) {
                mprageScanType = mprageParam.getCsvvalues();
            }
            ArcPipelineparameterdata tseParam = getProjectPipelineSetting(GenericBoldPreProcessing_PetMRLauncher.TSE_PARAM);
            if (tseParam != null) {
                tseScanType = tseParam.getCsvvalues();
            }
            ArcPipelineparameterdata t1wParam = getProjectPipelineSetting(GenericBoldPreProcessing_PetMRLauncher.T1W_PARAM);
            if (t1wParam != null) {
                t1wScanType = t1wParam.getCsvvalues();
            }
            ArcPipelineparameterdata pdt2Param = getProjectPipelineSetting(GenericBoldPreProcessing_PetMRLauncher.PDT2_PARAM);
            if (pdt2Param != null) {
                pdt2ScanType = pdt2Param.getCsvvalues();
            }
            ArcPipelineparameterdata epiParam = getProjectPipelineSetting(GenericBoldPreProcessing_PetMRLauncher.EPI_PARAM);
            if (epiParam != null) {
                boldScanType = epiParam.getCsvvalues();
            }
            String selfStatus = WrkWorkflowdata.GetLatestWorkFlowStatusByPipeline(pet.getId(), XnatPetsessiondata.SCHEMA_ELEMENT_NAME, GenericBoldPreProcessing_PetMRLauncher.LOCATION+File.separator +GenericBoldPreProcessing_PetMRLauncher.NAME, pet.getProject(), TurbineUtils.getUser(data));
            if (selfStatus.equalsIgnoreCase(WrkWorkflowdata.COMPLETE)) {
                data.setMessage("This pipeline has already completed. Relaunching the pipeline may result in loss of processed files");
            }
            String pathToTarget = getTarget();
            context.put("target",pathToTarget);
            context.put("target_name",getTargetName(pathToTarget));
            context.put("projectSettings", projectParameters);
            LinkedHashMap<String,String> buildableScanTypes = new LinkedHashMap<String,String>();
            if (mprageScanType != null) {
                buildableScanTypes.put(GenericBoldPreProcessing_PetMRLauncher.MPRAGE, mprageScanType);
            }
            if (tseScanType != null) {
                buildableScanTypes.put(GenericBoldPreProcessing_PetMRLauncher.TSE,tseScanType);
            }
            if (t1wScanType != null) {
                buildableScanTypes.put(GenericBoldPreProcessing_PetMRLauncher.T1W,t1wScanType);
            }
            if (pdt2ScanType != null) {
                buildableScanTypes.put(GenericBoldPreProcessing_PetMRLauncher.PDT2,pdt2ScanType);
            }
            if (boldScanType != null) {
                buildableScanTypes.put(StdBuildLauncher.EPI, boldScanType);
            }

            context.put("buildableScanTypes", buildableScanTypes);

            String cross_day_register = getProjectPipelineSetting("cross_day_register").getCsvvalues();
            ArrayList<XnatImagesessiondata> sessionList = new ArrayList<XnatImagesessiondata>();
            HashMap<String, ArrayList<XnatResourcecatalog>> sessionAssessorMap = new HashMap<String, ArrayList<XnatResourcecatalog>>();
            if (cross_day_register.equals("1")) {
                for (XnatImagesessiondata session : XnatPetsessiondata.GetAllPreviousImagingSessions(pet, pet.getProject(), TurbineUtils.getUser(data))) {
                    for (XnatAbstractresourceI absRes : session.getResources_resource()) {
                        if (!(absRes instanceof XnatResourcecatalog)) {
                            continue;
                        }
                        XnatResourcecatalog res = (XnatResourcecatalog) absRes;
                        if (res.getLabel().startsWith("BOLD_")) {
                            if (!sessionList.contains(session)) {
                                sessionList.add(session);
                                sessionAssessorMap.put(session.getLabel(),new ArrayList<XnatResourcecatalog>());
                            }
                            sessionAssessorMap.get(session.getLabel()).add(res);
                        }
                    }
                }
                context.put("cross_day_list", sessionList);
                context.put("session_assessor_map",sessionAssessorMap);
                int oldestSessionIndex = 0;
                for (int i=0; i<sessionList.size(); i++) {
                    if (sessionAssessorMap.get(sessionList.get(i).getLabel()).size() > 0) {
                        oldestSessionIndex = i;
                    }
                }
                context.put("oldest_session_index",oldestSessionIndex);
            }
        }catch(Exception e) {
            logger.error("Possibly the project wide pipeline has not been set", e);
            e.printStackTrace();
        }
    }

    public void preProcessing(RunData data, Context context) {
        super.preProcessing(data, context);
    }

    protected String getTarget() {
        String rtn = null;
        for (Map.Entry<String,ArcPipelineparameterdata> paramsEntry : projectParameters.entrySet()) {
            if (paramsEntry.getKey().equalsIgnoreCase("target")) {
                rtn = paramsEntry.getValue().getCsvvalues();
                break;
            }
        }
        return rtn;
    }

    protected String getTargetName(String atlasPath) {
        String rtn = null; if (atlasPath == null) return rtn;
        rtn = atlasPath;
        int indexOfLastSlash = atlasPath.lastIndexOf(File.separator);
        if (indexOfLastSlash != -1) {
            String targetName = atlasPath.substring(indexOfLastSlash + 1);
            int indexOfDot = targetName.indexOf(".");
            if (indexOfDot != -1) {
               rtn = targetName.substring(0,indexOfDot);
            }else {
                rtn = targetName;
            }
        }
        return rtn;
    }
}
