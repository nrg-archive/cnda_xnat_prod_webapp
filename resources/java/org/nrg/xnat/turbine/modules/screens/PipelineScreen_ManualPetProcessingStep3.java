package org.nrg.xnat.turbine.modules.screens;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.model.ArcPipelinedataI;
import org.nrg.xdat.model.ArcPipelineparameterdataI;
import org.nrg.xdat.om.ArcPipelineparameterdata;
import org.nrg.xdat.om.ArcProject;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.turbine.modules.screens.SecureReport;
import org.nrg.xnat.turbine.utils.ArcSpecManager;

public class PipelineScreen_ManualPetProcessingStep3 extends SecureReport {
	protected Hashtable<String, ArcPipelineparameterdata > projectParameters ;

	public void finalProcessing(RunData data, Context context) {
		projectParameters = new Hashtable<String, ArcPipelineparameterdata >();
		String pipelineName = "ManualPetProcessing";

		XnatPetsessiondata pet = new XnatPetsessiondata(item);
	    	try {
				String projectId = ((String)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedParameter("project",data));
		    	ArcProject arcProject = ArcSpecManager.GetFreshInstance().getProjectArc(projectId);

		    	ArrayList<ArcPipelinedataI> pipelines = arcProject.getPipelinesForDescendant(pet.getXSIType(), pipelineName, "EXACT");

		    	ArcPipelinedataI pipelineData = pipelines.get(0);
		    	List<ArcPipelineparameterdataI> params = pipelineData.getParameters_parameter();
		        for (int i = 0; i < params.size(); i++) {
		        	ArcPipelineparameterdata aParam = (ArcPipelineparameterdata)params.get(i);
		            projectParameters.put(aParam.getName(),aParam);
		        }
		    	context.put("projectSettings", projectParameters);
		        context.put("pet", pet);
		        context.put("step",3);
		        context.put("step1Disabled","disabled");
		        context.put("step2Disabled","disabled");
		        context.put("step3Disabled","");

	    	}catch(Exception e) {
				logger.error("",e);
	    	}

	    }

	}
