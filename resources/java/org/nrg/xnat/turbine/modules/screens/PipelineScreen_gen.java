/*
 * gen pipeline screen v. 01/22/2014
*/

//package org.apache.turbine.app.cnda_xnat.modules.screens;
package org.nrg.xnat.turbine.modules.screens; 

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeMap;

import org.apache.batik.dom.util.HashTable;
import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;

public class PipelineScreen_gen extends DefaultPipelineScreen 
{	
	//script path is hard coded for security.
	String m_script_path="/home/shared/NRG/mmilch/cnda_scripts/bin/gen";
	String m_an_id;
	
	LinkedList<String> m_types=new LinkedList<String>();
	LinkedList<String> m_typeDescrs=new LinkedList<String>();
	LinkedList<String> m_params=new LinkedList<String>();
	LinkedList<String> m_paramDescrs=new LinkedList<String>();
	LinkedList<String> m_defaultVals=new LinkedList<String>();
	LinkedList<String> m_paramsRequired=new LinkedList<String>();
	LinkedList<String> m_typesRequired=new LinkedList<String>();
	
	String[] m_scanlib_ids;
    static Logger logger = Logger.getLogger(PipelineScreen_gen.class);
	public void finalProcessing(RunData data, Context context)
	{
		XnatMrsessiondata mr = (XnatMrsessiondata) om;
        context.put("mr", mr);
        
		try
		{
//			m_script_path = getProjectPipelineSetting("GEN_SCRIPT_PATH").getCsvvalues();
			m_an_id = getProjectPipelineSetting("an_id").getCsvvalues();
		}catch(Exception e)
		{
			logger.error("Apparently, analysis ID was not set in the pipeline project settings");	
			e.printStackTrace();
			return;
		}
		m_an_id=m_an_id.replaceAll("[^A-Za-z0-9_-]", "");
		//populate scanlib id's
		String prefix=m_script_path+"/";
		prefix.replaceAll("//", "/");
		String f=prefix+m_an_id+".xml";
		try
		{
			LoadDescriptor(new File(f));
		}
		catch(Exception e)
		{
			logger.error("Cannot load or parse parameter file " + f);
			e.printStackTrace();
			return;
		}
		LinkedList<String> buildableScans=new LinkedList<String>();
        for(XnatImagescandataI scan :  mr.getScans_scan())
        {
        	buildableScans.add(scan.getId());
        }
        context.put("buildableScans", buildableScans);
        context.put("projectSettings", projectParameters);
        context.put("scanTypes",m_types);
        context.put("scanTypeDescrs",m_typeDescrs);
        context.put("typesRequired", m_typesRequired);
        context.put("runtimeParams",m_params);
        context.put("runtimeParamDescrs", m_paramDescrs);
        context.put("an_id", m_an_id);
        context.put("defaultVals", m_defaultVals);
        context.put("paramsRequired",m_paramsRequired);        
	}
    public void preProcessing(RunData data, Context context) {
        super.preProcessing(data, context);	
    }
	private String executeCommand(String command)
	{
		StringBuffer output = new StringBuffer();
		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader = 
	                        new BufferedReader(new InputStreamReader(p.getInputStream()));
	
	        String line = "";			
			while ((line = reader.readLine())!= null) 
			{
				output.append(line); //+ "\n");
			}	
		}
		catch (Exception e) 
		{
			logger.error("Error executing system command: "+command);
			e.printStackTrace();
		}	
		return output.toString();	
	}
	void LoadDescriptor(File f) throws DocumentException,NoSuchFieldException,MalformedURLException
	{
		Document d = new SAXReader().read(f);
		Element root = d.getRootElement();
		if (root.getName().toLowerCase().compareTo("genparams") != 0)
			throw new NoSuchFieldException();
		Element el,type,param;
		String name,descr,def_val,param_req,type_req;
		for (Iterator<Element> it = root.elementIterator(); it.hasNext();)
		{
			el = it.next();
			if (el.getName().toLowerCase().compareTo("types") == 0)
			{
				for (Iterator<Element> it1=el.elementIterator(); it1.hasNext();)
				{
					type=it1.next();
					if ( type.getName().toLowerCase().compareTo("type") == 0 )
					{
						name=type.attributeValue("name");
						descr=type.attributeValue("descr");
						type_req=type.attributeValue("required");
						if (name==null || descr==null)	
							throw new NoSuchFieldException();
						m_types.add(name);
						m_typeDescrs.add(descr);
						if(type_req!=null)
							m_typesRequired.add(type_req);
						else 
							m_typesRequired.add("0");
					}
				}
			}
			else if (el.getName().toLowerCase().compareTo("params") == 0)
			{
				for (Iterator<Element> it2=el.elementIterator(); it2.hasNext();)
				{
					param=it2.next();
					if ( param.getName().toLowerCase().compareTo("param") == 0 )
					{
						name=param.attributeValue("name");
						descr=param.attributeValue("descr");
						def_val=param.attributeValue("default_val");
						param_req=param.attributeValue("required");						
						if (name==null || descr==null)	
							throw new NoSuchFieldException();		
						m_params.add(name);
						m_paramDescrs.add(descr);
						if(def_val==null) m_defaultVals.add("");
						else			  m_defaultVals.add(def_val);
						if (param_req==null) m_paramsRequired.add("0");
						else m_paramsRequired.add(param_req);
					}
				}
			}
		}
	}
}
