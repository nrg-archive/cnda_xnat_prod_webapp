/*
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 *
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.nrg.xnat.turbine.modules.screens;

import java.util.ArrayList;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.PipelineManager;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.turbine.modules.screens.SecureReport;
import org.nrg.xdat.turbine.utils.TurbineUtils;

public class ManualPetProcessingSelector extends SecureReport{

    public void finalProcessing(RunData data, Context context) {
        boolean buildRoi = false;
        boolean createRoi = false;

        XnatPetsessiondata pet = new XnatPetsessiondata(item);
        //String buildPage = data.getParameters().get("buildpage");
        //String createRoiPage = data.getParameters().get("createroipage");
        //String buildRoiPage = data.getParameters().get("buildroipage");

        String buildRoiPage = "PipelineScreen_ManualPetPprocessingStep3.vm";
        String buildPage = "PipelineScreen_ManualPetPprocessing.vm";
        String createRoiPage  = "PipelineScreen_ManualPetPprocessingStep2.vm";

       try {
           ArrayList<WrkWorkflowdata> wrkList = PipelineManager.getWorkFlowsOrderByLaunchTimeDesc(pet.getId(), pet.getXSIType(), TurbineUtils.getUser(data));
           if (wrkList != null ) {
        	   String status = wrkList.get(0).getStatus();
               if (status.equalsIgnoreCase("RUNNING") || status.equalsIgnoreCase("QUEUED")) {
                   data.setMessage("<p><b>The build process for this session is currently " + status  +". Please wait for the email notification to perform additional build steps</b></p>");
                   this.doRedirect(data,"ClosePage.vm");
                   return;
               }
           }
       }catch(Exception e) {e.printStackTrace();}

       if (pet.isAtlasRegistered()) {
            ArrayList unassessedRegions = pet.getUnAssessedRegions();
            ArrayList assessedRegions = pet.getAssessedRegions();
            if (unassessedRegions.size() == 0 && assessedRegions.size() == 0) {
                createRoi = true; // no roi's uploaded
            }else if (unassessedRegions.size() == 0 && assessedRegions.size() != 0) {
                createRoi = true; // all regions have been assessed
            }else if (unassessedRegions.size() != 0 && assessedRegions.size() == 0) {
                buildRoi = true; // none of the regions have been assessed
            }else if (unassessedRegions.size() > 0 ) {
                buildRoi = true;
            }else
                createRoi = true;
        }
        if (data.getMessage() != null) {
            data.setMessage(data.getMessage());
        }
        try {
            if (buildRoi) {
                context.put("step",3);
                this.doRedirect(data, buildRoiPage );

            }else if (createRoi){
                context.put("step",2);
                this.doRedirect(data, createRoiPage );
            }else {
                context.put("step",1);
                this.doRedirect(data, buildPage );
            }
        }catch(Exception e) {e.printStackTrace();}
    }

}
