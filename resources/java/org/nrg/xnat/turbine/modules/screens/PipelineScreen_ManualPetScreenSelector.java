package org.nrg.xnat.turbine.modules.screens;

import java.util.ArrayList;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.PipelineManager;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.turbine.modules.screens.SecureReport;
import org.nrg.xdat.turbine.utils.TurbineUtils;

public class PipelineScreen_ManualPetScreenSelector extends SecureReport{

	public void finalProcessing(RunData data, Context context) {
        boolean buildRoi = false;
        boolean createRoi = false;

        XnatPetsessiondata pet = new XnatPetsessiondata(item);

        String buildRoiPage = "PipelineScreen_ManualPetProcessingStep3.vm";
        String buildPage = "PipelineScreen_ManualPetProcessing.vm";
        String createRoiPage  = "PipelineScreen_ManualPetProcessingStep2.vm";
        String dataType = "mpet:ManualPETTimeCourse";
       try {
           ArrayList<WrkWorkflowdata> wrkList = PipelineManager.getWorkFlowsOrderByLaunchTimeDesc(pet.getId(), pet.getXSIType(), TurbineUtils.getUser(data));
           if (wrkList != null ) {
        	   String status = wrkList.get(0).getStatus();
               if (status.equalsIgnoreCase("RUNNING") || status.equalsIgnoreCase("QUEUED")) {
                   data.setMessage("<p><b>The build process for this session is currently " + status  +". Please wait for the email notification to perform additional build steps</b></p>");
                   this.doRedirect(data,"ClosePage.vm");
                   return;
               }
           }
       }catch(Exception e) {e.printStackTrace();}

       if (pet.hasMPRAGEInAtlasSpace()) {
            ArrayList unassessedRegions = pet.getUnAssessedRegions(dataType);
            ArrayList assessedRegions = pet.getAssessedRegions(dataType);
            if (unassessedRegions.size() == 0 && assessedRegions.size() == 0) {
                createRoi = true; // no roi's uploaded
            }else if (unassessedRegions.size() == 0 && assessedRegions.size() != 0) {
                createRoi = true; // all regions have been assessed
            }else if (unassessedRegions.size() != 0 && assessedRegions.size() == 0) {
                buildRoi = true; // none of the regions have been assessed
            }else if (unassessedRegions.size() > 0 ) {
                buildRoi = true;
            }else
                createRoi = true;
        }
        if (data.getMessage() != null) {
            data.setMessage(data.getMessage());
        }
        try {
            if (buildRoi) {
                context.put("step",3);
                this.doRedirect(data, buildRoiPage );

            }else if (createRoi){
                context.put("step",2);
                this.doRedirect(data, createRoiPage );
            }else {
                context.put("step",1);
                this.doRedirect(data, buildPage );
            }
        }catch(Exception e) {e.printStackTrace();}
    }

}
