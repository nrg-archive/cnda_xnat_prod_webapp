package org.nrg.xnatCalendar;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.nrg.xdat.om.ArcArchivespecification;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xft.XFT;
import org.nrg.xft.XFTTable;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import org.nrg.xdat.security.XDATUser;
import java.util.Iterator;
import java.util.Calendar;

/**
 * CalendarDatamanager Class is a wrapper for XFTTable.
 * It encapsulates all the XFTTable column indexing .. So instead of 
 * asking for information on a session, they can ask for information
 * on a calendar event. 
 *
 */
public final class CalendarDataManager {
   
   //Defines user defined elements that we can put in the event summary (event subject)
   private enum EventSummaryType { PROJECT_ID, PROJECT_LABEL, SUBJECT_ID, SUBJECT_LABEL, 
                                   SESSION_ID, SESSION_LABEL, SCANNER_NAME, INVESTIGATOR 
                                 };

   //Defines Options the user can use to refine the calendar database search. The string value of each enum
   //is the database column value to compare on. (e.x. WHERE PROJECTS = "test1")
   public enum QueryOptionType { 
                                 INVESTIGATORS       { public String toString(){ return "inv.investigator_lastname||'_'||inv.investigator_firstname"; } },
                                 PROJECTS            { public String toString(){ return "exp.project";              } },
                                 SUBJECTS            { public String toString(){ return "subj_dat.label";           } },
                                 SCANNERNAME         { public String toString(){ return "img.scanner";              } },
                                 SCANNERMODEL        { public String toString(){ return "img.scanner_model";        } },
                                 SCANNERMANUFACTURER { public String toString(){ return "img.scanner_manufacturer"; } },
                                 XSITYPE             { public String toString(){ return "xme.element_name";         } },
                                 EVENTSUBJECT                                                                         
                               };

   //ArrayList of Event Data.
   private final ArrayList<Object[]> _events;
   
   //User making the rest call.
   private final XDATUser _usr;
   
   //This xnat instance's site url.
   private final String _siteUrl, _usrEmail;
   
   //User defined eventSummaryTypes.
   private final Set<String> _eventSummaryTypes;
   
   //Indexes required to retrieve data from the table.
   private final Integer _dateIdx, _timeIdx, _endTimeIdx, _sessionIdIdx, _projIdx, _projLabelIdx, 
                         _subjectLabelIdx, _subjectIdIdx, _scannerIdx, _scannerModIdx, _scannerManIdx, _invFnameIdx, _invLnameIdx;
   
   private final static Logger _logger = Logger.getLogger(CalendarDataManager.class);
   
   
   /**
    * Class constructor.  Takes an XFTTable to store as the data for the 
    * calendar. Throws exception if the table is null, empty or if the table
    * does not contain necessary data to create a calendar. 
    * @param t - the XFTTable
    * @throws Exception 
    */
   public CalendarDataManager(Map<QueryOptionType, Set<String>> queryOptions, XDATUser usr) throws Exception{

      //Make sure queryOptions and the user are not null.
      if(null == queryOptions || null == usr){ 
         throw new Exception("Parameter was null, could not build calendar.");
      }
      
      //Save the user making the rest call.
      _usr = usr;

      //Save and remove eventSubject from the queryOptions.
      _eventSummaryTypes = queryOptions.remove(QueryOptionType.EVENTSUBJECT);
      
      //We must have one single project if we have a subject.
      if(queryOptions.containsKey(QueryOptionType.SUBJECTS) && (!queryOptions.containsKey(QueryOptionType.PROJECTS) || queryOptions.get(QueryOptionType.PROJECTS).size() != 1)){
         throw new Exception("To query by subject you must also specify exactly one project.");
      }
      
      // This conditional is weird.
      // The first call is to verify that the user is querying by scanner.
      // The second call is to make sure that all options are set.
      if(!validateScannerOptions(queryOptions)){
         throw new Exception("To query by scanner the scannerName, scannerModel, and scannerManufacturer options must all be set.");
      }
      
      //Build and Execute database query.
      XFTTable t = XFTTable.Execute(buildQueryString(queryOptions), _usr.getDBName(), _usr.getLogin());
      
      //If the table is null or empty just bail out. 
      if(null == t || t.size() == 0){ 
         throw new Exception("Unable to find what you are looking for.");
      }
      
      //Column indexes used to access data within the event array. 
      _dateIdx          = t.getColumnIndex("start_date");
      _timeIdx          = t.getColumnIndex("start_time");
      _sessionIdIdx     = t.getColumnIndex("session_id");
      _projIdx          = t.getColumnIndex("project_id");
      _endTimeIdx       = t.getColumnIndex("end_time");
      _projLabelIdx     = t.getColumnIndex("project_label");
      _subjectLabelIdx  = t.getColumnIndex("subject_label");
      _subjectIdIdx     = t.getColumnIndex("subject_id");
      _scannerIdx       = t.getColumnIndex("scanner");
      _scannerModIdx    = t.getColumnIndex("scanner_model");
      _scannerManIdx    = t.getColumnIndex("scanner_manufacturer");
      _invFnameIdx      = t.getColumnIndex("investigator_firstname");
      _invLnameIdx      = t.getColumnIndex("investigator_lastname");
      
      //Save the rows in the table as calendar events.
      _events = validateEvents(t.rows());
      
      //If the event array is null or empty after validation.
      if(null == _events || _events.size() == 0){ throw new Exception("Unable to find what you are looking for."); }
      
      //save the site URL
      _siteUrl = retrieveSiteUrl();
      
      //Save the users email.
      _usrEmail = _usr.getEmail();
   }
   
   //
   //
   //Private Methods
   //
   //
   
   /**
    * This function determines if we are querying by a scanner, if so,
    * it makes sure all the required scanner option fields are set.
    * @param q - A Hashmap containing all of the query options.
    * @return True if all scanner option fields are set,
    *         False if all scanner options are not set
    *         True if not doing a scanner query
    */
   private Boolean validateScannerOptions(Map<QueryOptionType, Set<String>> q){
      // Are we trying to do a scanner query?
      if (q.containsKey(QueryOptionType.SCANNERNAME) || q.containsKey(QueryOptionType.SCANNERMODEL) || q.containsKey(QueryOptionType.SCANNERMANUFACTURER)){
         // Returns true if ALL options are set, false otherwise
         return (q.containsKey(QueryOptionType.SCANNERNAME) && q.containsKey(QueryOptionType.SCANNERMODEL) && q.containsKey(QueryOptionType.SCANNERMANUFACTURER));
      }
      
      // Return true if we are not doing a scanner query
      return true; 
   }
   
   /**
    * Retrieves an element from the calendar.
    * @param eventIdx - the event we are interested in.
    * @param columnIdx - the element we are interested in
    * @return The Element string.  If the element is null we 
    *         return empty string.
    */
   private String getCalendarEventElementString(int eventIdx, Integer columnIdx){
      String retString = (String)_events.get(eventIdx)[columnIdx];
      return (null != retString) ? retString : ""; 
   }
   
   /**
    * Function builds a query string based on the user defined options stored in
    * queryOptions.
    * @return String - the query String.
    * @throws Exception
    */
   private String buildQueryString(Map<QueryOptionType, Set<String>> queryOptions) throws Exception{
      
      StringBuilder queryBuilder = new StringBuilder();
      
      //This is the basic query.  Everything here is required by the calendarDataManager.
      queryBuilder.append("SELECT exp.id AS session_id, xme.element_name AS xsiType, exp.project AS project_id, proj.project_label, subj_asses.subject_id AS subject_id, subj_dat.label AS subject_label, exp.date AS start_date, scan_times.start_time AS start_time, scan_times.end_time AS end_time, exp.label, inv.investigator_firstname, inv.investigator_lastname, img.scanner, img.scanner_manufacturer, img.scanner_model FROM xnat_experimentdata exp ")
                  .append("LEFT JOIN (SELECT image_session_id,MAX(starttime) AS end_time,MIN(starttime) AS start_time FROM xnat_imagescandata GROUP BY image_session_id)scan_times ON exp.id=scan_times.image_session_id ")
                  .append("LEFT JOIN (SELECT name AS project_label, id, pi_xnat_investigatordata_id AS inv_id FROM xnat_projectdata) proj ON proj.id = exp.project ")
                  .append("LEFT JOIN (SELECT firstname investigator_firstname, lastname AS investigator_lastname, xnat_investigatordata_id AS id FROM xnat_investigatordata) inv ON inv.id = proj.inv_id ")
                  .append("LEFT JOIN (SELECT scanner, scanner_manufacturer, scanner_model, id FROM xnat_imagesessiondata) img ON img.id = exp.id ")
                  .append("LEFT JOIN (SELECT * FROM xdat_meta_element) xme ON exp.extension = xme.xdat_meta_element_id ")
                  .append("LEFT JOIN (SELECT subject_id, id FROM xnat_subjectassessordata) subj_asses ON exp.id = subj_asses.id ")
                  .append("LEFT JOIN (SELECT label, id FROM xnat_subjectdata) subj_dat ON subj_asses.subject_id = subj_dat.id");

      if(null == queryOptions || queryOptions.size() == 0){ 
         // If no user defined options, return here.
         return queryBuilder.toString(); 
      }

      Iterator<Map.Entry<QueryOptionType, Set<String>>> it = queryOptions.entrySet().iterator();
      queryBuilder.append(" WHERE ( ( ").append(getOptionStr(it.next())).append(" ) ");

      while(it.hasNext()){
         queryBuilder.append(" AND ( ").append(getOptionStr(it.next())).append(" ) ");
      }
      
      queryBuilder.append(" )");
      return queryBuilder.toString();
   }

   private String getOptionStr(Map.Entry<QueryOptionType, Set<String>> options){
      
      StringBuilder optionBuilder = new StringBuilder();
      Iterator<String> it = options.getValue().iterator();
      
      optionBuilder.append(" ( ( ").append(options.getKey()).append(" = '").append(StringEscapeUtils.escapeSql(it.next())).append("' ) ");
      
      while(it.hasNext()){
         optionBuilder.append(" OR ( ").append(options.getKey()).append(" = '").append(StringEscapeUtils.escapeSql(it.next())).append("' ) ");
      }
      
      optionBuilder.append(" ) ");
      return optionBuilder.toString();
   }
   
   /**
    * Returns the string for an eventSummaryType for a given event.
    * @param summaryType the summary type
    * @param eventIdx - The index of the event we are interested in. 
    * @return String - the String for the summary type
    */
   private String getStringforSummaryType(EventSummaryType summaryType, int eventIdx) throws Exception{
      String retString = "";
      
      switch(summaryType){
         case PROJECT_ID:      retString = getProjectId(eventIdx);
                               break;
         case PROJECT_LABEL:   retString = getProjectLabel(eventIdx);
                               break;
         case SUBJECT_ID:      retString = getSubjectId(eventIdx);
                               break;
         case SUBJECT_LABEL:   retString = getSubjectLabel(eventIdx);
                               break;
         case INVESTIGATOR:    retString = getInvestigatorFullName(eventIdx);
                               break;
         case SCANNER_NAME:    retString = getScannerName(eventIdx);
                               break;
         case SESSION_ID:
         case SESSION_LABEL:   retString = getSessionId(eventIdx);
                               break;
         default:              //Ignore any input that is not supported.
                               break;
      }
      
      if(null == retString || retString.isEmpty()){ 
         throw new Exception("Field is null or empty.");
      }
      
      return retString;
   }
   
   /**
    * Function determines if a user can read the given project.
    * @param projId - the id of the project
    * @return true if user can read false if they cant.
    */
   private boolean canUserReadProject(String projId){
      
      if(null == projId || projId.isEmpty()){ return false; }
      
      final XnatProjectdata p = XnatProjectdata.getXnatProjectdatasById(projId, _usr, false);
      try{
         return _usr.canRead(("xnat:subjectData/project").intern(), p.getId());
      }
      catch(Exception e){ } 
      
      return false;
   }
   
   /**
    * Method attempts to retrieve the SiteUrl from ArcSpec.  If that fails
    * it tries to retrieve it from XFT.  If that fails then it returns an
    * empty string. 
    * @return String the siteUrl.
    */
   private String retrieveSiteUrl(){
      
      String siteUrl = null;
      ArrayList<ArcArchivespecification> allSpecs = ArcArchivespecification.getAllArcArchivespecifications(null,false);
      ArcArchivespecification arcSpec = (null != allSpecs) ? allSpecs.get(0) : null;
      
      //If arcSpec is not null; get the site url
      if(null != arcSpec){ siteUrl = arcSpec.getSiteUrl(); }
      
      //If _siteUrl empty or is still null; get the site url from XFT
      if(null == siteUrl || siteUrl.isEmpty()){ siteUrl = XFT.GetSiteURL(); }
      
      //If it is still null after that just set it to empty string.
      if(null == siteUrl){ siteUrl = ""; }
      
      return siteUrl;
   }
   
   /**
    * This Function loops throw the list of events and removes any
    * events that the user cannot see.
    * @param events - the list of events
    * @return ArrayList the list of events that the user is allowed to see.
    */
   private ArrayList<Object[]> validateEvents(ArrayList<Object[]> events){
     
      ArrayList<Object[]> r = new ArrayList<Object[]>();
      for(Object[] e : events){
         if(!canUserReadProject((String)e[_projIdx])){
              r.add(e);
         }
      }
      
      events.removeAll(r);
      return events;
   }
   
   /**
    * Function takes a date and a time and combines them into 1 date object.
    * If null is passed in for t ... this function will add the time of 00:00:00
    * 
    * @param d - The date object containing the date
    * @param t - The date object containing the time
    * @return - A date object containing both date and time. 
    * @throws - Exception if d is null.
    */
   private Date combineDateAndTime(Date d, Date t) throws Exception{
      
      if (null == d ){ throw new Exception("Date was null ... Unable to combine date/time into one object.");}
      
      Calendar dCal = Calendar.getInstance();
      Calendar tCal = Calendar.getInstance();
      
      dCal.setTime(d);
      
      if(null == t){
         dCal.set(Calendar.HOUR_OF_DAY, 00);
         dCal.set(Calendar.MINUTE, 00);
         dCal.set(Calendar.SECOND, 00);
      }else{
         tCal.setTime(t);
         dCal.set(Calendar.HOUR_OF_DAY, tCal.get(Calendar.HOUR_OF_DAY));
         dCal.set(Calendar.MINUTE, tCal.get(Calendar.MINUTE));
         dCal.set(Calendar.SECOND, tCal.get(Calendar.SECOND));
      }

      return dCal.getTime();
   }
   
   //
   //
   //Public Methods
   //
   //
   
   /**
    * Returns a string to be used as the Event Summary (i.e. Event Title)
    * The method checks the _eventSummaryTypes.  If this has been set by the
    * user then the users title fields are used.  If not, the default (session ID) 
    * is used. 
    * 
    * @param eventIdx - The index of the event we are interested in. 
    * @return String - the event summary.
    */
   public String getEventSummary(int eventIdx){
      
      //If no event summary was specified return the default.
      if(null == _eventSummaryTypes || _eventSummaryTypes.size() == 0){
         return getSessionId(eventIdx) + "\\, " + getProjectId(eventIdx);
      }
      
      StringBuilder strBuilder = new StringBuilder();
      String [] a = _eventSummaryTypes.toArray(new String [_eventSummaryTypes.size()]);
      
      //Loop through the user defined summary types.  Append each one into a string separated by a comma.
      for(int i=0; i<a.length; i++){
         try{ 
            strBuilder.append(getStringforSummaryType(EventSummaryType.valueOf(a[i].toUpperCase()), eventIdx));
            if((a.length ) > 0 && (a.length - 1) != i){ strBuilder.append("\\,"); }
         }
         catch(Exception e){ /* Ignore any options we don't recognize */ }
      }
      
      return strBuilder.toString();
   }
   
   /**
    * Retrieves the number of calendar events in the XFTTable. 
    * @return the number of events.
    */
   public int getNumEvents(){
      return _events.size();
   }
   
   /**
    * Retrieves and ArrayList of events in the XFTTable.
    * @return ArrayList of events.
    */
   public ArrayList<Object[]> getEvents(){
      return _events;
   }
   
   /**
    * Returns just the end time for the event. 
    * 
    * @param eventIdx - The index of the event we are interested in. 
    * @return Date the end time.
    */
   public Date getEventEndTime(int eventIdx){
      return (Date)_events.get(eventIdx)[_endTimeIdx];
   }
   
   /**
    * Returns just the start time for the event. 
    * 
    * @param eventIdx - The index of the event we are interested in. 
    * @return Date the start time.
    */
   public Date getEventStartTime(int eventIdx){
      return (Date)_events.get(eventIdx)[_timeIdx];
   }
   
   /**
    * Returns the start date and time for the event. 
    * 
    * @param eventIdx - The index of the event we are interested in. 
    * @return Date the start date.
    * @throws Exception if start Date is null.
    */
   public Date getEventStartDate(int eventIdx) throws Exception{
      Date sDate = (Date)_events.get(eventIdx)[_dateIdx];
      Date sTime = getEventStartTime(eventIdx);
      
      return combineDateAndTime(sDate, isAllDayEvent(eventIdx) ? null : sTime);
   }
   
   
   /**
    * Since we dont store the end date of a scan in XNAT, this
    * method will calculate the end date. If end time < start time 
    * then end date = start date + 1. 
    * 
    * @param eventIdx - The index of the event we are interested in. 
    * @return Date the start date.
    * @throws Exception if start Date, start Time or end Time is null.
    */
   public Date getEventEndDate(int eventIdx) throws Exception{
      Date sTime = getEventStartTime(eventIdx);
      Date eTime = getEventEndTime(eventIdx);
      Date sDate = getEventStartDate(eventIdx);
     
      if(null == sDate || null == sTime || null == eTime){ throw new Exception("Start Date/Time or End Time was Null."); }
      
      //We need to determine if the scan started on one day and ended on another.  
      Calendar cal = Calendar.getInstance();
      cal.setTime(sDate);
      
      //If the end time is before the start time add 1 day to the end date.
      // (We are assuming that no scans are longer than 12 hours)
      if(eTime.before(sTime)){
         cal.add(Calendar.DATE, 1);
      }
      
      return combineDateAndTime(cal.getTime(), eTime);
   }
   
   public boolean isAllDayEvent(int eventIdx){
      return (getEventStartTime(eventIdx) == null || getEventEndTime(eventIdx) == null);
   }
   
   
   /** 
    * This method will return the investigator last name and first name
    * separated by a '_'
    * @param eventIdx - The index of the event we are interested in. 
    * @return String investigator name or "" if lname or fname is null or empty
    */
   public String getInvestigatorFullName(int eventIdx){
      String lname = getInvestigatorLastname(eventIdx);
      String fname = getInvestigatorFirstName(eventIdx);
      
      if((lname != null && fname != null) && (!lname.isEmpty() && !fname.isEmpty())){
         return lname + "_" + fname;
      }else{
         return "";
      }
   }
   
   /**
    * Gets the URL to the event (i.e. event body)
    * @param eventIdx - The index of the event we are interested in. 
    * @return - String the event URL. 
    */
   public String getSessionURL(int eventIdx){
      String retStr = null == _siteUrl ? "" : _siteUrl;
      return retStr + "/data/experiments/" + getSessionId(eventIdx) + "?format=html";
   }
   
   /**
    * Returns the event Unique Identifier. (session ID)
    * @param eventIdx - The index of the event we are interested in. 
    * @return String the event UID.
    */
   public String getEventUID(int eventIdx){
      return getCalendarEventElementString(eventIdx, _sessionIdIdx);
   }
   
   /**
    * Returns the project Id for the event. 
    * @param eventIdx - The index of the event we are interested in. 
    * @return String the project Id.
    */
   public String getProjectId(int eventIdx){
      return getCalendarEventElementString(eventIdx, _projIdx);
   }
   
   /**
    * Returns the project label for the event.
    * @param eventIdx - The index of the event we are interested in.
    * @return String - the project label.
    */
   public String getProjectLabel(int eventIdx){
      return getCalendarEventElementString(eventIdx, _projLabelIdx);
   }
   
   /**
    * Returns the subject label for the event.
    * @param eventIdx - The index of the event we are interested in.
    * @return String the subject label.
    */
   public String getSubjectLabel(int eventIdx){
      return getCalendarEventElementString(eventIdx, _subjectLabelIdx);
   }
   
   /**
    * Returns the subject id for the event.
    * @param eventIdx - The index of the event we are interested in.
    * @return String the subject id.
    */
   public String getSubjectId(int eventIdx){
      return getCalendarEventElementString(eventIdx, _subjectIdIdx);
   }
   
   /**
    * Returns the session Id for the event. 
    * @param eventIdx - The index of the event we are interested in. 
    * @return String the session Id.
    */
   public String getSessionId(int eventIdx){
      return getCalendarEventElementString(eventIdx, _sessionIdIdx);
   }
   
   /**
    * Returns the investigator's first name. 
    * @param eventIdx - The index of the event we are interested in. 
    * @return String the investigator's first name.
    */
   public String getInvestigatorFirstName(int eventIdx){
      return getCalendarEventElementString(eventIdx, _invFnameIdx);
   }
   
   /**
    * Returns the investigator's last name. 
    * @param eventIdx - The index of the event we are interested in. 
    * @return String the investigator's last name.
    */
   public String getInvestigatorLastname(int eventIdx){
      return getCalendarEventElementString(eventIdx, _invLnameIdx);
   }
   
   /**
    * Returns the scanner name. 
    * @param eventIdx - The index of the event we are interested in. 
    * @return String the scanner name.
    */
   public String getScannerName(int eventIdx){
      return getCalendarEventElementString(eventIdx, _scannerIdx);
   }
   
   /**
    * Returns the scanner model. 
    * @param eventIdx - The index of the event we are interested in. 
    * @return String the scanner's model.
    */
   public String getScannerModel(int eventIdx){
      return getCalendarEventElementString(eventIdx, _scannerModIdx);
   }
   
   /**
    * Returns the scanner Manufacturer. 
    * @param eventIdx - The index of the event we are interested in. 
    * @return String the scanner's Manufacturer.
    */
   public String getScannerManufacturer(int eventIdx){
      return getCalendarEventElementString(eventIdx, _scannerManIdx);
   }
   
   /**
    * Returns the Event Organizer's email. 
    * @param eventIdx - The index of the event we are interested in. 
    * @return String the email of the organizers.
    * 
    * Note: this isn't being use but I am going to leave it in case we put it in later.
    */
   public String getOrganizerEmail(){
      return (null != _usrEmail) ? _usrEmail : "";
   }
   
   /**
    * This function currently doesnt do anything.  There are plans to
    * support timezones on a project level.  If we ever get around to 
    * implementing that this will return the timezone for an event's project.
    * 
    * Timezones should be defined in the timezone config file.
    * 
    * @param eventIdx - The index of the event we are interested in.
    * @return String the timezone. 
    * 
    */
   public String getTimeZone(int event){
     return "America/Chicago";
   }
}