package org.nrg.diantu.utils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import org.nrg.diantu.safety.exception.EmailConfigurationException;
import org.nrg.mail.api.MailMessage;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.entities.AliasToken;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.services.AliasTokenService;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xnat.turbine.utils.ArcSpecManager;
import org.restlet.Client;
import org.restlet.data.ChallengeResponse;
import org.restlet.data.ChallengeScheme;
import org.restlet.data.CookieSetting;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.restlet.data.Protocol;
import org.restlet.data.Reference;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.resource.FileRepresentation;
import org.restlet.resource.Representation;
import org.restlet.resource.StringRepresentation;

public class UploadUtils {
	private static XDATUser user;
	private static String jsessionid;

	public static void initialize(XDATUser us) {
		user = us;

		Client client = new Client(Protocol.HTTP);
		Reference jsessionUri = new Reference(getSiteURL() + "data/JSESSION");
		Request createJsession = new Request(Method.POST, jsessionUri);
		createJsession.setChallengeResponse(UploadUtils.getCredentials());

		try {
			jsessionid = client.handle(createJsession).getEntity().getText();
		} catch (IOException ioe) {
			System.out.println("Failed to retrieve jsession.");
		}
	}

	private static String getSiteURL() {
		// Get URL from site config
		String siteConfigUrl = ArcSpecManager.GetInstance().getSiteUrl();

		System.out.println("Arcspec site URL is " + siteConfigUrl);

		// Get path in case XNAT is not running as root application
		String urlAfterProtocol = siteConfigUrl.split("//")[1]; // split off protocol

		String path = urlAfterProtocol.contains("/") ? urlAfterProtocol.split("/")[1] : "";

		System.out.println("Extra path is " + path);

		// Make request to localhost to work around SSL issues
		String suffix = "";

		if (!path.isEmpty() && !path.equals("/")) {
			if (path.endsWith("/")) {
				suffix = path;
			} else {
				suffix = path + "/";
			}
		}

		System.out.println("Calculated siteurl is " + "http://localhost:8080/" + suffix);

		return "http://localhost:8080/" + suffix;
	}

	private static ChallengeResponse getCredentials() {
		AliasToken token = XDAT.getContextService().getBean(AliasTokenService.class).issueTokenForUser(user);

		ChallengeResponse credentials = new ChallengeResponse(ChallengeScheme.HTTP_BASIC, token.getAlias(),
				Long.toString(token.getSecret()));

		return credentials;
	}

	public static Response uploadXML(String baseUri, String xml) {
		Client client = new Client(Protocol.HTTP);

		Reference newExperimentUri = new Reference(getSiteURL() + baseUri
				+ "?allowDataDeletion=true&event_reason=upload&inbody=true");
		System.out.println("PUT to " + newExperimentUri);

		Request createExperiment = new Request(Method.PUT, newExperimentUri);

		Representation newExp = new StringRepresentation(xml);
		createExperiment.setEntity(newExp);

		createExperiment.setChallengeResponse(UploadUtils.getCredentials());

		return client.handle(createExperiment);
	}

	public static Response createResource(String baseUri) {
		Client client = new Client(Protocol.HTTP);

		String fileUri = getSiteURL() + baseUri + "/resources/Safety";
		System.out.println("PUT to " + fileUri);

		Reference resourceUri = new Reference(fileUri);
		Request uploadResource = new Request(Method.PUT, resourceUri);
		
		uploadResource.setChallengeResponse(UploadUtils.getCredentials());

		return client.handle(uploadResource);
	}

	public static Response uploadResource(String baseUri, File file, String contentTag, MediaType mediaType) {
		Client client = new Client(Protocol.HTTP);

		System.out.println("Sending " + file.getName());
		System.out.println(file.getName() + " exists? " + file.exists());

		String fileUri = getSiteURL() + baseUri + "/resources/Safety/files/" + file.getName() + "?content="
				+ contentTag + "&overwrite=true";
		System.out.println("PUT to " + fileUri);

		Reference resourceUri = new Reference(fileUri);
		Request uploadResource = new Request(Method.PUT, resourceUri);

		FileRepresentation fileRep = new FileRepresentation(file, mediaType);
		uploadResource.setEntity(fileRep);

		uploadResource.setChallengeResponse(UploadUtils.getCredentials());

		return client.handle(uploadResource);
	}

	public static Response get(String uri) {
		if (uri.startsWith("/")) {
			uri = uri.substring(1);
		}

		Client client = new Client(Protocol.HTTP);
		Reference getUri = new Reference(getSiteURL() + uri);
		Request get = new Request(Method.GET, getUri);

		if (jsessionid == null || jsessionid.isEmpty()) {
			get.setChallengeResponse(UploadUtils.getCredentials());
		} else {
			System.out.println("Using jsession " + jsessionid);
			CookieSetting jsessionCookie = new CookieSetting("JSESSIONID", jsessionid);
			get.getCookies().add(jsessionCookie);
		}

		System.out.println("GET from " + getSiteURL() + uri);

		return client.handle(get);
	}

	public static Map<String, Response> bulkGet(List<String> uris) {
		Client client = new Client(Protocol.HTTP);
		Map<String, Response> responses = new HashMap<String, Response>();

		for (String uri : uris) {
			if (uri.startsWith("/")) {
				uri = uri.substring(1);
			}

			Reference getUri = new Reference(getSiteURL() + uri);
			Request get = new Request(Method.GET, getUri);

			if (jsessionid == null || jsessionid.isEmpty()) {
				get.setChallengeResponse(UploadUtils.getCredentials());
			} else {
				System.out.println("Using jsession " + jsessionid);
				CookieSetting jsessionCookie = new CookieSetting("JSESSIONID", jsessionid);
				get.getCookies().add(jsessionCookie);
			}

			System.out.println("GET from " + getSiteURL() + uri);

			responses.put(uri, client.handle(get));
		}

		return responses;
	}

	public static String getConfiguration(String project, String tool, String path) {
		XnatProjectdata p = XnatProjectdata.getXnatProjectdatasById(project, user, false);

		final Long projectDataInfo = new Long((Integer) p.getItem().getProps().get("projectdata_info"));

		System.out.println("projectDataInfo is " + projectDataInfo);

		return XDAT.getConfigService().getConfigContents("safetyread", "email/to", projectDataInfo);
	}

	public static void sendMessage(String[] tos, String[] ccs, String header, String body) throws MessagingException,
			EmailConfigurationException {
		//try {
		MailMessage message = new MailMessage();

		// When receiving email send requests through the REST service, the from address is always the admin, with the mail sent on behalf of the validating user.
		message.setFrom(AdminUtils.getAdminEmailId());
		message.setOnBehalfOf(user.getEmail());

		// Handle all the addresses
		if (tos == null || tos.length == 0) {
			throw new EmailConfigurationException("Error reading from configuration");
		}
		message.setTos(tos);

		if (!(ccs == null || ccs.length == 0)) {
			message.setCcs(ccs);
		}

		// Set the message guts.
		message.setSubject(header);
		message.setHtml(body);

		// Finally send the mail.
		XDAT.getMailService().sendMessage(message);
	}
}
