package org.nrg.diantu.utils;

public class UIDFormatUtils {
	public static String formatUID(String uid) {
		String formattedUID;

		if (uid.contains("_")) {
			formattedUID = uid.replace('_', '.').trim();
		} else {
			formattedUID = uid.trim();
		}

		return formattedUID;
	}
}
