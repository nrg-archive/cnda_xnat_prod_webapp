package org.nrg.diantu.safety;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.nrg.diantu.safety.exception.MissingMetadataException;
import org.nrg.diantu.safety.exception.SubjectMismatchException;
import org.nrg.diantu.safety.exception.UIDMismatchException;
import org.nrg.diantu.utils.DateFormatUtils;
import org.nrg.diantu.utils.UIDFormatUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import au.com.bytecode.opencsv.CSVReader;

public class SafetyQcParser {
	private static String subjectID;
	private static String scanDate;
	private static String readDate;
	private static String readTime;
	private static String project;
	private static String studyUID;
	private static String visit;
	private static String sessionLabel;

	private static SafetyRead parsedRead;
	private static ManualQc parsedManualQc;

	private static boolean hasRead;
	private static boolean hasQc;
	private boolean debug = false;

	public SafetyQcParser(Map<String, String> params) {
		subjectID = params.get("subjectID");
		scanDate = params.get("scanDate");
		readDate = params.get("readDate");
		readTime = params.get("readTime");
		project = params.get("project");
		studyUID = params.get(UIDFormatUtils.formatUID("studyUID"));

		if (params.containsKey("debug") && params.get("debug").equalsIgnoreCase("true")) {
			debug = true;
		}
	}

	public SafetyRead getParsedRead() {
		return hasRead ? parsedRead : null;
	}

	public ManualQc getParsedManualQc() {
		return hasQc ? parsedManualQc : null;
	}

	public void parse(String buildDirectory, File zipFile, Document sessionXml) throws ZipException, IOException,
			SubjectMismatchException, UIDMismatchException, MissingMetadataException {
		// Open zip file for reading
		ZipFile read = new ZipFile(zipFile);

		// See what data we have
		String readPrefix = subjectID + "_" + scanDate + "_" + readDate + "_" + readTime + "_";
		String qcPrefix = subjectID + "_" + studyUID + "_" + scanDate + "_" + readDate + "_" + readTime;

		if (hasEntry(readPrefix + "Findings.csv", read) && hasEntry(readPrefix + "Notes.csv", read)
				&& hasEntry(readPrefix + "Review.csv", read) && hasEntry(readPrefix + "Details.csv", read)) {
			hasRead = true;
			System.out.println("HAS READ");
		} else {
			hasRead = false;
		}

		if (hasEntry(qcPrefix + "_StudyQC.csv", read) && hasEntry(qcPrefix + "_SeriesQC.csv", read)) {
			hasQc = true;
			System.out.println("HAS QC");
		} else {
			hasQc = false;
		}

		// Fetch visit and scan ID information from SessionXML
		Map<String, String> uidToIdMapping = handleSessionXml(sessionXml);

		// Read and parse Safety data
		if (hasRead) {
			InputStream findingsStream = getInputStreamForEntry(readPrefix + "Findings.csv", read);
			List<Map<String, String>> find = readFindings(findingsStream);
			findingsStream.close();

			InputStream notesStream = getInputStreamForEntry(readPrefix + "Notes.csv", read);
			List<String[]> noteList = readNotes(notesStream);
			notesStream.close();

			InputStream reviewStream = getInputStreamForEntry(readPrefix + "Review.csv", read);
			SortedMap<String, String> flags = readReview(reviewStream);
			reviewStream.close();

			// Store original files as resources
			InputStream pdfStream = getInputStreamForEntry(readPrefix + "Read.pdf", read);
			File pdfFile = createTempFile(pdfStream, readPrefix + "Read.pdf", buildDirectory);

			FileInputStream zipFileStream = new FileInputStream(zipFile);
			File tempZipFile = createTempFile(zipFileStream, zipFile.getName(), buildDirectory);

			// Save data in read
			SafetyRead r = new SafetyRead(project, subjectID, DateFormatUtils.formatDateForXML(readDate),
					DateFormatUtils.formatTime(readTime), visit, debug);

			r.setFindings(find);

			String note = prepareNote(noteList, flags);
			r.setNote(note);

			Boolean flagForReview = flags.get(flags.lastKey()).equals("Yes") ? true : false;
			r.setFlagForReview(flagForReview);

			r.setPdf(pdfFile);
			r.setZip(tempZipFile);

			parsedRead = r; // final commit
		}

		if (hasQc) {
			// Read and parse QC data
			InputStream studyQcStream = getInputStreamForEntry(qcPrefix + "_StudyQC.csv", read);
			Map<String, String> studyQc = readStudyQc(studyQcStream);
			studyQcStream.close();

			InputStream seriesQcStream = getInputStreamForEntry(qcPrefix + "_SeriesQC.csv", read);
			List<Map<String, String>> seriesQc = readSeriesQc(seriesQcStream);
			seriesQcStream.close();

			// Save data in manual QC
			ManualQc q = new ManualQc(project, subjectID, DateFormatUtils.formatDateForXML(readDate),
					DateFormatUtils.formatTime(readTime), studyUID.replace('_', '.'), visit, sessionLabel);

			q.setUidToIdMapping(uidToIdMapping);
			q.setSeriesQc(seriesQc);
			q.setStudyQc(studyQc);

			// Save data in manual QC
			parsedManualQc = q;
		}

		read.close();
	}

	private static File createTempFile(InputStream is, String filename, String buildDirectory) throws IOException,
			FileNotFoundException {
		// Read file into byte array
		ByteArrayOutputStream fileData = new ByteArrayOutputStream();
		int readSoFar = 0;
		byte[] buffer = new byte[1024];

		while ((readSoFar = is.read(buffer, 0, buffer.length)) != -1) {
			fileData.write(buffer, 0, readSoFar);
		}

		fileData.flush();
		is.close();

		// Create temp file
		String tempFileName = buildDirectory + "/" + filename;
		FileOutputStream tempFileStream = new FileOutputStream(tempFileName);
		tempFileStream.write(fileData.toByteArray());
		tempFileStream.close();

		// Return a reference to the temp file
		File tempFile = new File(tempFileName);
		return tempFile;
	}

	private static boolean hasEntry(String entryName, ZipFile file) {
		System.out.println(entryName + " " + file.getEntry(entryName));
		return file.getEntry(entryName) == null ? false : true;
	}

	private static InputStream getInputStreamForEntry(String entryName, ZipFile file) throws IOException {
		System.out.println("Entry name is " + entryName);
		System.out.println("Zipfile is " + (file == null ? "null" : "not null"));

		ZipEntry entry = file.getEntry(entryName);
		System.out.println("Entry is " + (entry == null ? "null" : "not null"));

		InputStream stream = file.getInputStream(entry);
		System.out.println("Stream is " + (stream == null ? "null" : "not null"));

		return stream;
	}

	public Map<String, String> handleSessionXml(Document sess) throws UIDMismatchException, MissingMetadataException {
		HashMap<String, String> dicomUidToXnatIdMap = new HashMap<String, String>();

		Node sessionRoot = sess.getElementsByTagName("xnat:PETMRSession").getLength() == 0 ? sess.getElementsByTagName(
				"xnat:MRSession").item(0) : sess.getElementsByTagName("xnat:PETMRSession").item(0);

		String imageSessionID = sessionRoot.getAttributes().getNamedItem("ID").getNodeValue();
		String imageSessionStudyUID = sessionRoot.getAttributes().getNamedItem("UID").getNodeValue();

		if (sessionRoot.getAttributes().getNamedItem("visit_id") != null) {
			visit = sessionRoot.getAttributes().getNamedItem("visit_id").getNodeValue();
		} else if (project.endsWith("_QC")) { // QC scans in DIAN-L do not get visit set 
			visit = "QC";
		} else {
			throw new MissingMetadataException("Session " + imageSessionID + " has no visit ID set");
		}

		sessionLabel = sessionRoot.getAttributes().getNamedItem("label").getNodeValue();

		String correctlyFormattedInputStudyUID = UIDFormatUtils.formatUID(studyUID);

		if (!correctlyFormattedInputStudyUID.equals(imageSessionStudyUID)) {
			throw new UIDMismatchException(correctlyFormattedInputStudyUID, imageSessionStudyUID, imageSessionID);
		}

		dicomUidToXnatIdMap.put(imageSessionStudyUID, imageSessionID);

		System.out.println("Found an image session ID of " + imageSessionID);

		NodeList scans = sess.getElementsByTagName("xnat:scan");

		for (int j = 0; j < scans.getLength(); ++j) {
			// Ignore <xnat:scan> elements from assessors

			// <xnat:scan> element will be contained in a scans element, check that the parent of
			// the container element for this scan is a session type and not an assessor like eg Manual QC
			// which also contains <xnat:scan>s
			if (scans.item(j).getParentNode().getParentNode().getNodeName().contains("Session")) {
				NamedNodeMap attributes = scans.item(j).getAttributes();

				String id = attributes.getNamedItem("ID").getNodeValue();
				String uid = attributes.getNamedItem("UID").getNodeValue();

				System.out.println("UID " + uid + " has ID " + id);

				dicomUidToXnatIdMap.put(uid, id);
			}
		}

		return dicomUidToXnatIdMap;
	}

	private static List<Map<String, String>> readSeriesQc(InputStream is) throws IOException {
		CSVReader reader = new CSVReader(new InputStreamReader(is));
		List<String[]> rawData = reader.readAll();
		reader.close();

		// first line, subject ID, study UID, blank fields
		String[] headerRow = rawData.remove(0);
		String subjectID = headerRow[0];
		String studyUid = UIDFormatUtils.formatUID(headerRow[1]);
		System.out.println("Subject " + subjectID);
		System.out.println("Study UID " + studyUid);

		// second line, headers
		rawData.remove(0); // ignore headers

		// third line and after: UID, type, protocol pass/fail, OPTIONAL
		// protocol comment, image pass/fail, OPTIONAL image comment, OPTIONAL
		// chosen yes/no
		List<Map<String, String>> seriesQc = new ArrayList<Map<String, String>>();

		int i = 0;
		for (String[] row : rawData) {
			Map<String, String> series = new HashMap<String, String>();

			System.out.println("SERIES " + i);

			System.out.println("Series UID: " + UIDFormatUtils.formatUID(row[0])); // mayo:scanUid
			series.put("Series UID", UIDFormatUtils.formatUID(row[0]));

			System.out.println("Series Type: " + row[1]); // mayo:scanType
			series.put("Series Type", row[1]);

			System.out.println("Protocol: " + row[2]); // mayo:protocol
			series.put("Protocol", row[2]);

			System.out.println("Protocol Comment: " + row[3]); // mayo:protocolComment
			series.put("Protocol Comment", row[3]);

			System.out.println("Image: " + row[4]); // xnat:pass
			series.put("Image", row[4]);

			System.out.println("Image Comment: " + row[5]); // xnat:comments
			series.put("Image Comment", row[5]);

			System.out.println("Chosen: " + (row[6].equals("true") ? "1" : "0")); // xnat:rating with scale attribute
			series.put("Chosen", (row[6].equals("true") ? "1" : "0"));

			if (series.get("Image") != null && !series.get("Image").isEmpty()) {
				seriesQc.add(series);
			}

			++i;
		}

		return seriesQc;
	}

	private static Map<String, String> readStudyQc(InputStream is) throws IOException {
		CSVReader reader = new CSVReader(new InputStreamReader(is));
		List<String[]> rawData = reader.readAll();
		reader.close();

		Map<String, String> studyQc = new HashMap<String, String>();

		// first line, split at comma into subject ID, study UID
		String[] headerRow = rawData.remove(0);
		String subjectID = headerRow[0];
		String studyUid = headerRow[1];

		System.out.println("Subject " + subjectID);
		studyQc.put("Subject", headerRow[0]);

		System.out.println("Study UID " + UIDFormatUtils.formatUID(studyUid));
		studyQc.put("Study UID", UIDFormatUtils.formatUID(studyUid));

		String[] headers = rawData.remove(0); // second line, headers
		String[] studyData = rawData.remove(0); // third line, integer pass/fail, comments

		for (int i = 0; i < headers.length; ++i) {
			studyQc.put(headers[i], studyData[i]);
			System.out.println(headers[i] + ": " + studyData[i]);
		}

		/*
		 * String[] studyData = rawData.remove(0); String studyQuality =
		 * studyData[0]; String comments = studyData[1]; String rescan = null;
		 * 
		 * if (studyData.length > 2) { rescan = studyData[2]; }
		 * 
		 * System.out.println("Study Quality: " + studyQuality);
		 * studyQc.put("Study Quality", studyQuality);
		 * 
		 * System.out.println("Study Comments: " + comments);
		 * studyQc.put("Study Comments", comments);
		 * 
		 * System.out.println("Rescan Requested: " + rescan);
		 * studyQc.put("Rescan Requested", rescan.toUpperCase());
		 */

		return studyQc;
	}

	private static List<Map<String, String>> readFindings(InputStream is) throws SubjectMismatchException,
			FileNotFoundException, IOException {
		CSVReader reader = new CSVReader(new InputStreamReader(is));
		List<String[]> rawData = reader.readAll();
		reader.close();

		String subjID = getSubjectID(rawData.remove(0)[0]);

		if (!subjID.equals(subjectID)) {
			throw new SubjectMismatchException(subjectID, subjID, "Findings");
		}

		// Need to find out how many visits we have
		List<String> visits = new ArrayList<String>(Arrays.asList(rawData.remove(0)));

		// Remove "MRI Issues" header and empty strings
		visits.remove(0);
		while (visits.contains("")) {
			visits.remove("");
		}

		// Take off "Definite Possible Status" header row
		String[] headers = rawData.remove(0);

		// Find number of unique headers; take -1 for leftmost empty string
		int columnsPerVisit = new HashSet<String>(Arrays.asList(headers)).size() - 1;

		// Ignore the No Findings row- empty findings will be removed later
		rawData.remove(0);

		// Handle remaining data
		List<Map<String, String>> findings = new ArrayList<Map<String, String>>();

		for (int i = 0; i < visits.size(); ++i) {
			int startColumn = i * columnsPerVisit + 1;

			for (String[] row : rawData) {
				Map<String, String> finding = new HashMap<String, String>();

				finding.put("Visit", visits.get(i));
				finding.put("Finding type", row[0]);

				for (int j = 0; j < columnsPerVisit; ++j) {
					finding.put(headers[startColumn + j], row[startColumn + j]);
				}

				String def = finding.get("definite");
				String pos = finding.get("possible");
				String stat = finding.get("status");

				// Add if definite or possible findings are present, or image is unreadable
				if ((def != null && !def.isEmpty()) || (pos != null && !pos.isEmpty()) || stat.equals("No Image")
						|| stat.equals("Failed Image") || stat.equals("See Notes") || stat.equals("No Change")) {
					findings.add(finding);
				}
			}
		}

		for (Map<String, String> finding : findings) {
			// Complex values
			if (finding.get("definite").contains("(")) {
				String definite = finding.get("definite");
				String numDefinite = definite.split("\\(")[0].trim();
				String magDefinite = definite.split("\\(")[1].split("\\)")[0].split(" ")[0].trim();

				finding.put("definite", numDefinite);
				finding.put("definite magnitude", magDefinite);

			}

			if (finding.get("possible").contains("(")) {
				String possible = finding.get("possible");
				String numPossible = possible.split("\\(")[0].trim();
				String magPossible = possible.split("\\(")[1].split("\\)")[0].split(" ")[0].trim();

				finding.put("possible", numPossible);
				finding.put("possible magnitude", magPossible);
			}

			// Handle dates
			String visitString = finding.get("Visit");
			finding.put("Visit", DateFormatUtils.formatDateForXML(visitString));
			finding.put("VisitTime", DateFormatUtils.extractTimeFromDatestamp(visitString));
		}

		return findings;
	}

	private static List<String[]> readNotes(InputStream is) throws SubjectMismatchException, IOException {
		CSVReader reader = new CSVReader(new InputStreamReader(is));
		List<String[]> rawData = reader.readAll();
		reader.close();

		String subjID = getSubjectID(rawData.remove(0)[0]);

		if (!subjID.equals(subjectID)) {
			throw new SubjectMismatchException(subjectID, subjID, "Notes");
		}

		NoteDateComparator sort = new NoteDateComparator(rawData.remove(0));
		Collections.sort(rawData, sort);

		return rawData;
	}

	private static SortedMap<String, String> readReview(InputStream is) throws SubjectMismatchException, IOException {
		CSVReader reader = new CSVReader(new InputStreamReader(is));
		List<String[]> rawData = reader.readAll();
		reader.close();

		String subjID = getSubjectID(rawData.remove(0)[0]);

		if (!subjID.equals(subjectID)) {
			throw new SubjectMismatchException(subjectID, subjID, "Review");
		}

		String[] visits = rawData.remove(0);
		String[] flags = rawData.remove(0);
		SortedMap<String, String> visflags = new TreeMap<String, String>();

		for (int i = 1; i < visits.length; ++i) {
			visflags.put(visits[i], flags[i]);
		}

		return visflags;
	}

	private static String prepareNote(List<String[]> rawNotes, SortedMap<String, String> flags) {
		StringBuilder noteBlob = new StringBuilder();

		// Loop through flags, which has an entry for each visit.
		for (String date : flags.keySet()) {
			// Gather any comments for this visit
			List<String[]> visitNotes = new ArrayList<String[]>();

			for (String[] row : rawNotes) {
				System.out.println(row[1] + " == " + date + " ? " + row[1].equals(date));
				if (row[1].equals(date)) {
					visitNotes.add(row);
				}
			}

			// Add information about flags to the session note
			noteBlob.append("Session conducted on " + DateFormatUtils.formatDateForXML(date) + " at "
					+ DateFormatUtils.extractTimeFromDatestamp(date) + " was");

			if (flags.get(date).equals("Yes")) {
				noteBlob.append(" FLAGGED for review");
			} else {
				noteBlob.append(" not flagged for review");
			}

			// If there are any comments, also append them to the session note 
			if (!visitNotes.isEmpty()) {
				noteBlob.append(";");

				String[] firstNote = visitNotes.remove(0);

				noteBlob.append(" read on " + DateFormatUtils.formatDateForXML(firstNote[0]) + " at "
						+ DateFormatUtils.extractTimeFromDatestamp(firstNote[0]) + " with comment '"
						+ firstNote[2].trim() + "'");

				System.out.println("VisitNotes: " + visitNotes.size());

				for (String[] row : visitNotes) {
					String readDate = row[0];

					noteBlob.append("; re-read on " + DateFormatUtils.formatDateForXML(readDate) + " at "
							+ DateFormatUtils.extractTimeFromDatestamp(readDate) + " with comment '" + row[2].trim()
							+ "'");
				}

				noteBlob.append(".\n\n");
			} else {
				noteBlob.append("; no comments.\n\n");
			}
		}

		return noteBlob.toString().trim();
	}

	private static String getSubjectID(String header) {
		String subjID = header.split(",")[0];
		return subjID;
	}
}
