package org.nrg.diantu.safety;

import java.util.Arrays;
import java.util.Comparator;

public class NoteDateComparator implements Comparator<String[]> {
	private int scanDateIndex;
	private int readDateIndex;

	public NoteDateComparator(String[] headers) {
		readDateIndex = Arrays.asList(headers).indexOf("Scan Date");
		readDateIndex = Arrays.asList(headers).indexOf("Read Date");
	}

	@Override
	public int compare(String[] o1, String[] o2) {
		long o1ScanDate = Long.parseLong(o1[scanDateIndex].replace(':', '0'));
		long o2ScanDate = Long.parseLong(o2[scanDateIndex].replace(':', '0'));
		long o1ReadDate = Long.parseLong(o1[readDateIndex].replace(':', '0'));
		long o2ReadDate = Long.parseLong(o2[readDateIndex].replace(':', '0'));

		int check;

		if (o1ScanDate < o2ScanDate) {
			check = -1;
		} else if (o1ScanDate > o2ScanDate) {
			check = 1;
		} else {
			if (o1ReadDate < o2ReadDate) {
				check = -1;
			} else if (o1ReadDate > o2ReadDate) {
				check = 1;
			} else {
				check = 0;
			}
		}

		return check;
	}
}
