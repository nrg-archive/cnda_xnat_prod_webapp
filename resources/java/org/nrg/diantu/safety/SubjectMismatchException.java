package org.nrg.diantu.safety;

public class SubjectMismatchException extends RuntimeException {
	private static final long serialVersionUID = 1872734118558018023L;

	public SubjectMismatchException(String expected, String found, String file) {
		super("Expected subject " + expected + ", but found subject " + found
				+ " in file " + file);
	}
}
