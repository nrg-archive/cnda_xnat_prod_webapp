package org.nrg.diantu.safety;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipException;

import org.apache.commons.fileupload.FileItem;
import org.nrg.diantu.safety.exception.MissingMetadataException;
import org.nrg.diantu.safety.exception.SessionLookupException;
import org.nrg.diantu.safety.exception.SubjectMismatchException;
import org.nrg.diantu.safety.exception.UIDMismatchException;
import org.nrg.diantu.utils.DateFormatUtils;
import org.nrg.diantu.utils.UploadUtils;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xnat.turbine.utils.ArcSpecManager;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.w3c.dom.Document;

import au.com.bytecode.opencsv.CSVReader;

public class UploadHandler {
	static StringBuilder result;
	static Status resultCode;

	//private static File buildDirectory;

	public static String getResult() {
		return result.toString();
	}

	public static Status getResultCode() {
		return resultCode;
	}

	public static void doUpload(FileItem zipFileItem, Map<String, String> otherValues, XDATUser user) {
		// Initialize uploader with XDATUser
		UploadUtils.initialize(user);

		// For final results
		result = new StringBuilder();

		try {
			// Create temp directory in build folder
			File buildDirectory = createBuildDirectory();

			// Write zip file to build directory
			File zipFile = new File(buildDirectory, zipFileItem.getName());
			zipFileItem.write(zipFile);

			System.out.println("Zip File is " + zipFile);

			// Look up session XML
			Document sessionXML = getSessionXML(otherValues);

			// Parse the files
			SafetyQcParser p = new SafetyQcParser(otherValues);
			p.parse(buildDirectory.getPath(), zipFile, sessionXML);

			// Capture parsed data
			SafetyRead r = p.getParsedRead();
			ManualQc mqc = p.getParsedManualQc();

			// Upload data
			Boolean readSuccess = false;
			Boolean mqcSuccess = false;
			String scanDate = DateFormatUtils.formatDateForREST(otherValues.get("scanDate"));

			// Upload read if read is present
			if (r != null) {
				readSuccess = r.uploadRead(scanDate);
				result.append(r.getResult());

			} else {
				result.append("No read uploaded. ");
			}

			// Upload mqc if mqc is present and read is absent or read has already succeeded
			if (mqc != null && (r == null || readSuccess == true)) {
				mqcSuccess = mqc.uploadQc(scanDate);
				result.append(mqc.getResult());
			} else {
				result.append("No QC uploaded. ");
			}

			// Calculate the result code
			if (r != null && mqc != null) {
				if (readSuccess == true && mqcSuccess == true) {
					resultCode = Status.SUCCESS_CREATED;
				} else if (readSuccess == false) {
					resultCode = r.getResultCode();
				} else if (mqcSuccess == false) {
					resultCode = mqc.getResultCode();
				}
			} else if (r != null) {
				resultCode = r.getResultCode();
			} else if (mqc != null) {
				resultCode = mqc.getResultCode();
			} else if (r == null && mqc == null) {
				resultCode = Status.CLIENT_ERROR_BAD_REQUEST;
				result.append("Neither read nor QC found. Check request parameters and ensure that files are at the top level of the ZIPped file.");
			}

		} catch (ZipException ze) {
			resultCode = Status.SERVER_ERROR_INTERNAL;
			result.append("ERROR: Zip file problem " + ze.getMessage());
		} catch (IOException ioe) {
			resultCode = Status.SERVER_ERROR_INTERNAL;
			result.append("ERROR: IO problem " + ioe.getMessage());
		} catch (SessionLookupException ioe) {
			resultCode = Status.SERVER_ERROR_INTERNAL;
			result.append("ERROR: Session Lookup problem " + ioe.getMessage());
		} catch (SubjectMismatchException sme) {
			resultCode = Status.CLIENT_ERROR_BAD_REQUEST;
			result.append("ERROR: Subject Mismatch " + sme.getMessage());
		} catch (UIDMismatchException uidme) {
			resultCode = Status.CLIENT_ERROR_BAD_REQUEST;
			result.append("ERROR: Study UID Mismatch " + uidme.getMessage());
		} catch (MissingMetadataException mme) {
			resultCode = Status.CLIENT_ERROR_BAD_REQUEST;
			result.append("ERROR: Missing Metadata " + mme.getMessage());
		} catch (Exception e) {
			resultCode = Status.SERVER_ERROR_INTERNAL;
			result.append("ERROR: Unknown problem " + e.getMessage());
			e.printStackTrace();

			if (otherValues.get("debug").equals("true")) {
				StackTraceElement[] ste = e.getStackTrace();
				for (StackTraceElement s : ste) {
					result.append(s.toString() + "\n");
				}
			}
		}
	}

	private static File createBuildDirectory() {
		File buildDirectory;

		// Create temp directory
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String timestamp = sdf.format(new Date());

		String buildDirectoryPath = ArcSpecManager.GetInstance().getGlobalBuildPath() + "safety" + timestamp;
		System.out.println("Build directory is " + buildDirectoryPath);

		buildDirectory = new File(buildDirectoryPath);

		try {
			buildDirectory.mkdirs();

			System.out.println("Build directory created");
		} catch (Exception e) {
			System.out.println("Can't create dirs " + buildDirectory.getPath() + " due to " + e.getMessage());
		}

		System.out.println("Build directory path is " + buildDirectory.getPath());

		return buildDirectory;
	}

	private static Document getSessionXML(Map<String, String> otherValues) throws IOException, SessionLookupException {
		String project = otherValues.get("project");
		String subject = otherValues.get("subjectID");
		String uid = otherValues.get("studyUID");
		String restFormatScanDate = DateFormatUtils.formatDateForREST(otherValues.get("scanDate"));

		String sessionLookupUri = "data/archive/experiments?xsiType=xnat:imageSessionData&project=" + project
				+ "&subject_label=" + subject + "&date=" + restFormatScanDate + "&xnat:imageSessionData/UID=" + uid
				+ "&xnat:imageSessionData/meta/status=locked,active&sortBy=insert_date&sortOrder=DESC&format=csv";

		System.out.println("SESSION LOOKUP URI IS " + sessionLookupUri);

		Response sessIDLookup = UploadUtils.get(sessionLookupUri);

		StringReader sr = new StringReader(sessIDLookup.getEntity().getText());
		CSVReader csvReader = new CSVReader(sr);
		List<String[]> results = csvReader.readAll();
		csvReader.close();

		if (results == null || results.size() < 2) {
			throw new SessionLookupException("No sessions matched:\nProject " + project + "\nSubject " + subject
					+ "\nDate " + restFormatScanDate + "\nUID " + uid);
		} else if (results.size() > 2) {
			// Filter to MR
			String mrLookupUri = "data/archive/experiments?xsiType=xnat:imageSessionData&project=" + project
					+ "&subject_label=" + subject + "&date=" + restFormatScanDate + "&xnat:mrSessionData/UID=" + uid
					+ "&xnat:imageSessionData/meta/status=locked,active&sortBy=insert_date&sortOrder=DESC&format=csv";

			System.out.println("Multiple sessions found, filtering to MR only");
			System.out.println("MR LOOKUP URI IS " + mrLookupUri);

			Response mrIDLookup = UploadUtils.get(mrLookupUri);

			StringReader mrsr = new StringReader(mrIDLookup.getEntity().getText());
			CSVReader mrCsvReader = new CSVReader(mrsr);
			results = mrCsvReader.readAll();
			mrCsvReader.close();

			// Still too many?
			if (results.size() > 2) {
				// size - 1 to remove headers
				throw new SessionLookupException("Too many sessions (" + (results.size() - 1) + ") matched:\nProject "
						+ project + "\nSubject " + subject + "\nDate " + restFormatScanDate + "\nUID " + uid);
			}
		}

		String sessID = results.get(1)[0]; // First cell of second row (first result after headers) will be XNAT ID

		System.out.println("SESSION LOOKUP STATUS IS " + sessIDLookup.getStatus());

		String sessionUri = "data/archive/projects/" + project + "/subjects/" + subject + "/experiments/" + sessID
				+ "?format=xml";

		System.out.println("SESSION URI IS " + sessionUri);

		Response sess = UploadUtils.get(sessionUri);
		System.out.println("SESSION PULL STATUS IS " + sess.getStatus());

		System.out.println("Session is " + sess);
		System.out.println("Session DOM entity is " + sess.getEntityAsDom());
		System.out.println("Session Document is " + sess.getEntityAsDom().getDocument());

		return sess.getEntityAsDom().getDocument();
	}
}
