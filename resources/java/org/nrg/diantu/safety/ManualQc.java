package org.nrg.diantu.safety;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.nrg.diantu.safety.exception.UploadException;
import org.nrg.diantu.utils.UploadUtils;
import org.nrg.xnat.turbine.utils.ArcSpecManager;
import org.restlet.data.MediaType;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.DomRepresentation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class ManualQc {
	Map<String, String> studyQc;
	List<Map<String, String>> seriesQc;
	Map<String, String> scanUidToIdMap;

	String date;
	String time;
	String project;
	String subject;
	String visit;

	String studyUID;
	String imageSessionID;
	String sessionLabel;

	String studyPass;
	String studyComments;

	private StringBuilder result;
	private Status resultCode;

	public String getResult() {
		return result.toString();
	}

	public Status getResultCode() {
		return resultCode;
	}

	public ManualQc(String proj, String subj, String dat, String tim, String studUid, String vis, String sess) {
		project = proj;
		subject = subj;
		date = dat;
		time = tim;
		studyUID = studUid;
		visit = vis;
		sessionLabel = sess;
	}

	public void setUidToIdMapping(Map<String, String> uidMap) {
		scanUidToIdMap = uidMap;

		setImageSessionID(scanUidToIdMap.get(studyUID));
	}

	public String getLabel() {
		return subject + "_" + visit + "_QC_" + date.replace("-", "") + time.replace(":", "");
	}

	public String getID() {
		return imageSessionID + "_" + visit + "_QC_" + date.replace("-", "") + time.replace(":", "");
	}

	public String getImageSessionID() {
		return imageSessionID;
	}

	public String getProject() {
		return project;
	}

	public String getSubject() {
		return subject;
	}

	public String getUri() {
		return "data/archive/projects/" + getProject() + "/subjects/" + getSubject() + "/experiments/"
				+ getImageSessionID() + "/assessors/" + getLabel();
	}

	public Boolean getRescan() {
		Boolean rescanStatus = null;

		if (studyQc.get("Rescan Requested") != null) {
			rescanStatus = studyQc.get("Rescan Requested").toUpperCase().equals("TRUE") ? true : false;
		}

		return rescanStatus;
	}

	public void setImageSessionID(String sessID) {
		imageSessionID = sessID;
	}

	public void setStudyQc(Map<String, String> studQc) {
		studyQc = studQc;
	}

	public void setSeriesQc(List<Map<String, String>> serQc) {
		seriesQc = serQc;
	}

	static String checkEmailSetUp(String project) throws IOException {
		String uri = "data/projects/" + project + "/resources/notifications/files/xnat_mrSessionData_transfer.lst";
		String toList = UploadUtils.get(uri).getEntity().getText();

		System.out.println("QC to list is " + toList);

		return toList;
	}

	public String getXML() {
		try {
			String xnatNS = "http://nrg.wustl.edu/xnat";
			String mayoNS = "http://nrg.wustl.edu/mayo";
			String xsiNS = "http://www.w3.org/2001/XMLSchema-instance";

			DomRepresentation representation = new DomRepresentation(MediaType.TEXT_XML);
			Document d = representation.getDocument();

			Element rootEl = d.createElementNS(xnatNS, "xnat:QCManualAssessment");
			rootEl.setAttribute("ID", getID());
			rootEl.setAttribute("project", project);
			rootEl.setAttribute("visit_id", visit);
			rootEl.setAttribute("label", getLabel());
			d.appendChild(rootEl);

			Element dateEl = d.createElementNS(xnatNS, "xnat:date");
			dateEl.appendChild(d.createTextNode(date));
			rootEl.appendChild(dateEl);

			Element fieldsEl = d.createElementNS(xnatNS, "xnat:fields");

			Element fieldEl = d.createElementNS(xnatNS, "xnat:field");
			fieldEl.setAttribute("name", "studyUID");
			fieldEl.appendChild(d.createTextNode(studyUID));

			fieldsEl.appendChild(fieldEl);
			rootEl.appendChild(fieldsEl);

			Element imageSessIdEl = d.createElementNS(xnatNS, "xnat:imageSession_ID");
			imageSessIdEl.appendChild(d.createTextNode(imageSessionID));
			rootEl.appendChild(imageSessIdEl);

			Element scansEl = d.createElementNS(xnatNS, "xnat:scans");

			for (Map<String, String> series : seriesQc) {
				Element scanEl = d.createElementNS(xnatNS, "xnat:scan");
				scanEl.setAttributeNS(xsiNS, "xsi:type", "mayo:mayoMrQcScanData");
				scanEl.setAttribute("xmlns:mayo", mayoNS);

				Element imageScanIdEl = d.createElementNS(xnatNS, "xnat:imageScan_ID");
				imageScanIdEl.appendChild(d.createTextNode(scanUidToIdMap.get(series.get("Series UID"))));
				scanEl.appendChild(imageScanIdEl);

				Element scanUidEl = d.createElementNS(mayoNS, "mayo:scanUid");
				scanUidEl.appendChild(d.createTextNode(series.get("Series UID")));
				scanEl.appendChild(scanUidEl);

				Element scanTypeEl = d.createElementNS(mayoNS, "mayo:scanType");
				scanTypeEl.appendChild(d.createTextNode(series.get("Series Type")));
				scanEl.appendChild(scanTypeEl);

				Element protocolEl = d.createElementNS(mayoNS, "mayo:protocol");
				protocolEl.appendChild(d.createTextNode(series.get("Protocol")));
				scanEl.appendChild(protocolEl);

				String seriesProtocolComments = series.get("Protocol Comment");
				if (seriesProtocolComments != null && !seriesProtocolComments.isEmpty()
						&& !seriesProtocolComments.trim().isEmpty()) {
					Element protocolCommentEl = d.createElementNS(mayoNS, "mayo:protocolComment");
					protocolCommentEl.appendChild(d.createTextNode(seriesProtocolComments));
					scanEl.appendChild(protocolCommentEl);
				}

				Element imageEl = d.createElementNS(xnatNS, "xnat:pass");
				imageEl.appendChild(d.createTextNode(series.get("Image")));
				scanEl.appendChild(imageEl);

				String seriesComments = series.get("Image Comment");
				if (seriesComments != null && !seriesComments.isEmpty() && !seriesComments.trim().isEmpty()) {
					Element imageCommentEl = d.createElementNS(xnatNS, "xnat:comments");
					imageCommentEl.appendChild(d.createTextNode(seriesComments));
					scanEl.appendChild(imageCommentEl);
				}

				Element chosenEl = d.createElementNS(mayoNS, "mayo:chosen");
				chosenEl.appendChild(d.createTextNode(series.get("Chosen").equals("1") ? "1" : "0"));
				scanEl.appendChild(chosenEl);

				scansEl.appendChild(scanEl);
			}

			rootEl.appendChild(scansEl);

			String comments = studyQc.get("Comments");
			if (comments != null && !comments.isEmpty() && !comments.trim().isEmpty()) {
				Element studyCommentsEl = d.createElementNS(xnatNS, "xnat:comments");
				studyCommentsEl.appendChild(d.createTextNode(comments));
				rootEl.appendChild(studyCommentsEl);
			}

			Element studyPassEl = d.createElementNS(xnatNS, "xnat:pass");
			studyPassEl.appendChild(d.createTextNode(studyQc.get("Study QC")));
			rootEl.appendChild(studyPassEl);

			if (studyQc.get("Rescan Requested") != null) {
				Element rescanEl = d.createElementNS(xnatNS, "xnat:rescan");
				rescanEl.appendChild(d
						.createTextNode(studyQc.get("Rescan Requested").toUpperCase().equals("TRUE") ? "1" : "0"));
				rootEl.appendChild(rescanEl);
			}

			if (studyQc.get("Protocol QC") != null) {
				Element protocolEl = d.createElementNS(xnatNS, "xnat:protocol");
				protocolEl.appendChild(d.createTextNode(studyQc.get("Protocol QC")));
				rootEl.appendChild(protocolEl);
			}

			String protocolComments = studyQc.get("Protocol Comments");
			if (protocolComments != null && !protocolComments.isEmpty() && !protocolComments.trim().isEmpty()) {
				Element protocolCommentEl = d.createElementNS(xnatNS, "xnat:protocolComments");
				protocolCommentEl.appendChild(d.createTextNode(protocolComments));
				rootEl.appendChild(protocolCommentEl);
			}

			rootEl.normalize();

			// For debugging
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

			transformer.transform(new DOMSource(d), new StreamResult(new OutputStreamWriter(System.out, "UTF-8")));

			StringWriter outString = new StringWriter();
			transformer.transform(new DOMSource(d), new StreamResult(outString));

			return outString.toString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	void sendRescanEmail(String scanDate) throws MessagingException, IOException {
		System.out.println("Trying to send rescan e-mail...");

		String uri = "data/projects/" + project + "/resources/notifications/files/xnat_mrSessionData_transfer.lst";
		String toList = UploadUtils.get(uri).getEntity().getText();
		Status toListStatus = UploadUtils.get(uri).getStatus();

		System.out.println("To list is " + toList);

		if (toListStatus.getCode() != 404 && toList != null && !toList.isEmpty()) {
			String[] tos = toList.trim().split(",");

			// Set the message guts.
			String header = "CNDA Notice: DIAN RESCAN REQUIRED for Subject " + subject + " MR Scan Done On " + scanDate;

			String sessionUri = "data/archive/projects/" + project + "/subjects/" + subject + "/experiments/"
					+ imageSessionID;
			String externalUrl = ArcSpecManager.GetInstance().getSiteUrl();

			String sessLink = externalUrl + (externalUrl.endsWith("/") ? "" : "/") + sessionUri;
			String qcLink = externalUrl + (externalUrl.endsWith("/") ? "" : "/") + getUri();

			String html = "<p>DIAN MR <a href=\"" + sessLink + "\">" + sessionLabel + "</a> for subject " + subject
					+ " done on " + scanDate + " has been FLAGGED FOR RESCAN";

			if (studyQc.get("Comments") != null && !studyQc.get("Comments").isEmpty()) {
				html += ", with the following comments from the QC reviewer: " + studyQc.get("Comments");
			}

			html += ". View full quality control evaluation in the CNDA <a href=\"" + qcLink + "\">here</a>.</p>";

			// Finally send the mail- no CC so cc is null
			UploadUtils.sendMessage(tos, null, header, html);
		}
	}

	Boolean uploadQc(String date) {
		result = new StringBuilder();
		Boolean mqcSuccess = false;

		// Upload QC information...
		Response qcFeedback = UploadUtils.uploadXML(getUri(), getXML());

		if (qcFeedback == null) {
			throw new UploadException("QC upload was null.");
		}

		System.out.println("XML response: " + qcFeedback.getStatus());

		try {
			if (qcFeedback.getStatus().isSuccess()) {
				mqcSuccess = true;
				resultCode = Status.SUCCESS_CREATED;

				if (getRescan() != null && getRescan() == true) {
					String toList = checkEmailSetUp(getProject());

					if (toList == null || toList.isEmpty()) {
						result.append("QC rescan e-mail list not set. ");
					}

					sendRescanEmail(date);
				}

				String qcId;
				String var = qcFeedback.getEntity().getText();
				qcId = var;

				result.append("QC " + qcId + " created.");
			} else {
				resultCode = Status.SERVER_ERROR_INTERNAL;
				result.append("ERROR: Failed to create QC experiment with status code " + qcFeedback.getStatus());

				// if null result.append("ERROR: Failed to create QC experiment.");
			}
		} catch (IOException e) {
			if (mqcSuccess == true) {
				result.append("Experiment created");
			} else {
				resultCode = Status.SERVER_ERROR_INTERNAL;
				result.append("ERROR: IO problem " + e.getMessage());

				e.printStackTrace();

				StackTraceElement[] ste = e.getStackTrace();
				for (StackTraceElement s : ste) {
					result.append(s.toString() + "\n");
				}
			}
		} catch (Exception e) {
			resultCode = Status.SERVER_ERROR_INTERNAL;
			result.append("ERROR: " + e.getMessage());

			e.printStackTrace();

			StackTraceElement[] ste = e.getStackTrace();
			for (StackTraceElement s : ste) {
				result.append(s.toString() + "\n");
			}
		}

		return mqcSuccess;
	}
}
