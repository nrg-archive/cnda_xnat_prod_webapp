package org.nrg.diantu.safety;

public class DateFormatUtils {
	public static String formatDateForXML(String datestamp) {
		return datestamp.substring(0, 4) + "-" + datestamp.substring(4, 6) + "-" + datestamp.substring(6, 8);
	}

	public static String extractTimeFromDatestamp(String datestamp) {
		System.out.println("Datestamp is " + datestamp);
		return datestamp.split(":")[1].substring(0, 2) + ":" + datestamp.split(":")[1].substring(2, 4) + ":00";
	}

	public static String formatDateForREST(String date) {
		return date.substring(4, 6) + "/" + date.substring(6) + "/" + date.substring(0, 4);
	}

	public static String formatTime(String time) {
		return time.substring(0, 2) + ":" + time.substring(2) + ":00";
	}
}
