package org.nrg.diantu.safety.exception;

public class ReadUploadException extends RuntimeException {
	private static final long serialVersionUID = -2284543952285886762L;

	public ReadUploadException(String message) {
		super(message);
	}
}
