package org.nrg.diantu.safety.exception;

public class ResourceUploadException extends RuntimeException {
	private static final long serialVersionUID = -1851487373057532850L;

	public ResourceUploadException(String message) {
		super(message);
	}
}
