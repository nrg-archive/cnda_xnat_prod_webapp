package org.nrg.diantu.safety.exception;

public class SessionLookupException extends RuntimeException {
	private static final long serialVersionUID = -6476123827619093774L;

	public SessionLookupException(String message) {
		super(message);
	}
}
