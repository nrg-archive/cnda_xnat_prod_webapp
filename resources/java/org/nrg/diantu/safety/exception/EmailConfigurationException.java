package org.nrg.diantu.safety.exception;

public class EmailConfigurationException extends RuntimeException {
	private static final long serialVersionUID = -6476123827619093774L;

	public EmailConfigurationException(String message) {
		super(message);
	}
}
