package org.nrg.diantu.safety.exception;

public class UploadException extends RuntimeException {
	private static final long serialVersionUID = -2284543952285886762L;

	public UploadException(String message) {
		super(message);
	}
}
