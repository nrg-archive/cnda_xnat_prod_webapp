package org.nrg.diantu.safety.exception;

public class QcUploadException extends RuntimeException {
	public QcUploadException(String message) {
		super(message);
	}
}
