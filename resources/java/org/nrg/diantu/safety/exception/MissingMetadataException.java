package org.nrg.diantu.safety.exception;

public class MissingMetadataException extends RuntimeException {
	private static final long serialVersionUID = -6476123827619093777L;

	public MissingMetadataException(String message) {
		super(message);
	}
}
