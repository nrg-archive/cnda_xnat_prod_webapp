package org.nrg.diantu.safety.exception;

public class UIDMismatchException extends RuntimeException {
	private static final long serialVersionUID = 1872734118558018023L;

	public UIDMismatchException(String expected, String found, String session) {
		super("Expected UID " + expected + " from upload, but found UID " + found + " in session " + session);
	}
}
