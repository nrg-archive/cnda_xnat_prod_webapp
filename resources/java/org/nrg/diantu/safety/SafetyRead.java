package org.nrg.diantu.safety;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.nrg.diantu.safety.exception.ResourceUploadException;
import org.nrg.diantu.safety.exception.UploadException;
import org.nrg.diantu.utils.UploadUtils;
import org.nrg.xnat.turbine.utils.ArcSpecManager;
import org.restlet.data.MediaType;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.DomRepresentation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import au.com.bytecode.opencsv.CSVReader;

public class SafetyRead {
	private List<Map<String, String>> findings;
	private String note;
	private Boolean flagForReview;
	private File pdf;
	private File zip;

	private String project;
	private String date;
	private String time;
	private String subject;
	private String visit;

	private StringBuilder result;
	private Status resultCode;
	private boolean debug;

	public String getResult() {
		return result.toString();
	}

	public Status getResultCode() {
		return resultCode;
	}

	public SafetyRead(String proj, String subj, String dat, String tim, String vis, boolean debug) {
		project = proj;
		subject = subj;
		date = dat;
		time = tim;
		visit = vis;
		this.debug = debug;
	}

	public void setNote(String n) {
		note = n;
	}

	public void setFindings(List<Map<String, String>> f) {
		findings = f;
	}

	public void setFlagForReview(Boolean flag) {
		flagForReview = flag;
	}

	public void setPdf(File p) {
		pdf = p;
	}

	public void setZip(File z) {
		zip = z;
	}

	public void printOut() {
		System.out.println(findings);
		System.out.println(note);
		System.out.println(flagForReview);
	}

	public String getDate() {
		return date;
	}

	public String getLabel() {
		return subject + "_" + visit + "_READ_" + date.replace("-", "") + time.replace(":", "");
	}

	public String getProject() {
		return project;
	}

	public String getSubject() {
		return subject;
	}

	public File getPdf() {
		return pdf;
	}

	public File getZip() {
		return zip;
	}

	public String getUri() {
		return "data/archive/projects/" + getProject() + "/subjects/" + getSubject() + "/experiments/" + getLabel();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append(findings.toString() + "\n");
		sb.append(note + "\n");
		sb.append(flagForReview.toString());

		return sb.toString();
	}

	public String getXML() {
		try {
			String xnatNS = "http://nrg.wustl.edu/xnat";
			String mayoNS = "http://nrg.wustl.edu/mayo";

			DomRepresentation representation = new DomRepresentation(MediaType.TEXT_XML);
			Document d = representation.getDocument();

			Element rootEl = d.createElementNS(mayoNS, "mayo:mayoSafetyRead");
			rootEl.setAttribute("ID", ""); // Attribute is required, but leave blank so XNAT will assign ID
			rootEl.setAttribute("project", project);
			rootEl.setAttribute("visit_id", visit);
			rootEl.setAttribute("label", getLabel());
			d.appendChild(rootEl);

			Element dateEl = d.createElementNS(xnatNS, "xnat:date");
			dateEl.appendChild(d.createTextNode(date));
			rootEl.appendChild(dateEl);

			Element timeEl = d.createElementNS(xnatNS, "xnat:time");
			timeEl.appendChild(d.createTextNode(time));
			rootEl.appendChild(timeEl);

			Element noteEl = d.createElementNS(xnatNS, "xnat:note");
			noteEl.appendChild(d.createTextNode(note));
			rootEl.appendChild(noteEl);

			Element subjEl = d.createElementNS(xnatNS, "xnat:subject_ID");
			subjEl.appendChild(d.createTextNode(subject));
			rootEl.appendChild(subjEl);

			Element flagEl = d.createElementNS(mayoNS, "mayo:flagForReview");
			flagEl.appendChild(d.createTextNode(flagForReview ? "1" : "0"));
			rootEl.appendChild(flagEl);

			Element findingsEl = d.createElementNS(mayoNS, "mayo:findings");

			for (Map<String, String> find : findings) {
				Element findingEl = d.createElementNS(mayoNS, "mayo:finding");

				Element visitEl = d.createElementNS(mayoNS, "mayo:visit");
				visitEl.appendChild(d.createTextNode(find.get("Visit")));
				findingEl.appendChild(visitEl);

				Element visitTimeEl = d.createElementNS(mayoNS, "mayo:visitTime");
				visitTimeEl.appendChild(d.createTextNode(find.get("VisitTime")));
				findingEl.appendChild(visitTimeEl);

				Element findingTypeEl = d.createElementNS(mayoNS, "mayo:findingType");
				findingTypeEl.appendChild(d.createTextNode(find.get("Finding type")));
				findingEl.appendChild(findingTypeEl);

				// Change or no image
				String status = find.get("status");

				if (status.equals("No Image")) {
					Element noImageEl = d.createElementNS(mayoNS, "mayo:noImage");
					noImageEl.appendChild(d.createTextNode("1"));
					findingEl.appendChild(noImageEl);
				} else if (status.equals("Failed Image")) {
					Element noImageEl = d.createElementNS(mayoNS, "mayo:failedImage");
					noImageEl.appendChild(d.createTextNode("1"));
					findingEl.appendChild(noImageEl);
				} else {
					if (status.equals("Change")) {
						Element changeEl = d.createElementNS(mayoNS, "mayo:change");
						changeEl.appendChild(d.createTextNode("1"));
						findingEl.appendChild(changeEl);
					} else if (status.equals("No Change")) {
						Element changeEl = d.createElementNS(mayoNS, "mayo:change");
						changeEl.appendChild(d.createTextNode("0"));
						findingEl.appendChild(changeEl);
					} else if (status.equals("See Notes")) {
						Element noImageEl = d.createElementNS(mayoNS, "mayo:seeNotes");
						noImageEl.appendChild(d.createTextNode("1"));
						findingEl.appendChild(noImageEl);
					} else {
						System.out.println("Status is \"" + status + "\"");
					}

					Element definiteCountEl = d.createElementNS(mayoNS, "mayo:definiteCount");
					definiteCountEl.appendChild(d.createTextNode(find.get("definite")));
					findingEl.appendChild(definiteCountEl);

					if (find.get("definite magnitude") != null) {
						Element definiteMagEl = d.createElementNS(mayoNS, "mayo:definiteMagnitude");
						definiteMagEl.appendChild(d.createTextNode(find.get("definite magnitude")));
						findingEl.appendChild(definiteMagEl);
					}

					Element possibleCountEl = d.createElementNS(mayoNS, "mayo:possibleCount");
					possibleCountEl.appendChild(d.createTextNode(find.get("possible")));
					findingEl.appendChild(possibleCountEl);

					if (find.get("possible magnitude") != null) {
						Element possibleMagEl = d.createElementNS(mayoNS, "mayo:possibleMagnitude");
						possibleMagEl.appendChild(d.createTextNode(find.get("possible magnitude")));
						findingEl.appendChild(possibleMagEl);
					}
				}

				//Element changeEl = d.createElementNS(mayoNS, "mayo:change");
				//changeEl.appendChild(d.createTextNode((find.get("status") != null && !find.get("status").isEmpty()) ? (find
				//						.get("status").equals("Change") ? "1" : "0") : find.get("status")));
				//findingEl.appendChild(changeEl);

				findingsEl.appendChild(findingEl);
			}

			rootEl.appendChild(findingsEl);
			rootEl.normalize();

			// For debugging
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

			transformer.transform(new DOMSource(d), new StreamResult(new OutputStreamWriter(System.out, "UTF-8")));

			StringWriter outString = new StringWriter();
			transformer.transform(new DOMSource(d), new StreamResult(outString));

			return outString.toString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	Boolean uploadRead(String restFormatScanDate) {
		result = new StringBuilder();
		Boolean readSuccess = false;

		try {
			String toList = checkEmailSetUp(getProject());

			if (toList == null || toList.isEmpty()) {
				result.append("Read e-mail list not set. ");
			}

			// Upload read
			Response feedback = UploadUtils.uploadXML(getUri(), getXML());

			if (feedback == null) {
				throw new UploadException("Response was null.");
			}

			System.out.println("XML response: " + feedback.getStatus());

			if (!feedback.getStatus().isSuccess()) {
				throw new UploadException("upload failed with status code " + feedback.getStatus() + " and message "
						+ feedback.getStatus());
			}

			try {
				String id = feedback.getEntity().getText();
				result.append("Read " + id + " created. ");
			} catch (IOException ioe) {

			}

			sendCompletedNotification(restFormatScanDate);

			// Upload pdf
			Response pdfFeedback = UploadUtils.uploadResource(getUri(), getPdf(), "readPdf", MediaType.APPLICATION_PDF);

			if (pdfFeedback == null) {
				throw new ResourceUploadException("PDF response was null.");
			}

			System.out.println("PDF response: " + pdfFeedback.getStatus());

			if (!pdfFeedback.getStatus().isSuccess()) {
				if (pdfFeedback.getStatus().getCode() == Status.SERVER_ERROR_INTERNAL.getCode()) {
					result.append(" PDF was not uploaded. Check to see if a previous source file exists. ");
				} else {
					throw new ResourceUploadException("PDF upload failed with status code " + pdfFeedback.getStatus()
							+ " and message " + pdfFeedback.getStatus());
				}
			}

			// Upload zip
			Response zipFeedback = UploadUtils.uploadResource(getUri(), getZip(), "sourceFiles",
					MediaType.APPLICATION_ZIP);

			if (zipFeedback == null) {
				throw new ResourceUploadException("ZIP response was null.");
			}

			System.out.println("ZIP response: " + zipFeedback.getStatus());

			if (!zipFeedback.getStatus().isSuccess()) {
				if (zipFeedback.getStatus().getCode() == Status.SERVER_ERROR_INTERNAL.getCode()) {
					result.append(" ZIP was not uploaded. Check to see if a previous source file exists. ");
				} else {
					throw new ResourceUploadException("ZIP upload failed with status code " + zipFeedback.getStatus()
							+ " and message " + zipFeedback.getStatus());
				}
			}

			readSuccess = true;
		// E-mail is not required in CNDA
		//} catch (EmailConfigurationException ece) { 
			//	resultCode = Status.SERVER_ERROR_INTERNAL;
			//	result.append("ERROR: E-mail configuration problem " + ece.getMessage());
		} catch (MessagingException me) {
			resultCode = Status.SERVER_ERROR_INTERNAL;
			result.append("ERROR: E-mail problem " + me.getMessage());
		} catch (UploadException rufe) {
			resultCode = Status.SERVER_ERROR_INTERNAL;
			result.append("ERROR: Failed to create read experiment: " + rufe.getMessage());
		} catch (ResourceUploadException rue) {
			resultCode = Status.SERVER_ERROR_INTERNAL;
			result.append("ERROR: Failed to upload source file: " + rue.getMessage());
		} catch (Exception e) {
			if (result == null) {
				System.out.println("Result null!");
			} else {
				resultCode = Status.SERVER_ERROR_INTERNAL;
				result.append("ERROR: Unknown problem " + e.getMessage() + "\n");
				e.printStackTrace();

				StackTraceElement[] ste = e.getStackTrace();
				for (StackTraceElement s : ste) {
					result.append(s.toString() + "\n");
				}
			}
		}

		return readSuccess;
	}

	static String checkEmailSetUp(String project) {
		String toList = UploadUtils.getConfiguration(project, "safetyread", "email/to");

		System.out.println("To list is " + toList);

		return toList;
	}

	void sendCompletedNotification(String scanDate) throws MessagingException, IOException {
		System.out.println("Trying to send e-mail...");

		String toList = UploadUtils.getConfiguration(project, "safetyread", "email/to");

		System.out.println("To list is " + toList);

		if (toList != null && !toList.isEmpty()) {
			String[] tos = toList.trim().split(",");

			String ccList = UploadUtils.getConfiguration(project, "safetyread", "email/cc");
			String[] ccs = null;

			if (ccList != null) {
				ccs = ccList.trim().split(",");
			}

			// Look up subject arm
			String arm = lookupArm();
			String armtext;

			if (arm == null || arm.isEmpty()) {
				armtext = "with no arm assigned";
			} else {
				armtext = "assigned to arm " + arm;
			}

			// Set the message guts.
			String header = "CNDA Update: DIAN Safety Read Available For Subject " + subject + " MR Scan Done On "
					+ scanDate;

			if (flagForReview) {
				header = "CNDA Update: DIAN Safety Read FLAGGED FOR REVIEW For Subject " + subject
						+ " MR Scan Done On " + scanDate;
			}

			String readUri = "data/archive/projects/" + project + "/subjects/" + subject + "/experiments/" + getLabel();
			String externalUrl = ArcSpecManager.GetInstance().getSiteUrl();

			String readLink = externalUrl + (externalUrl.endsWith("/") ? "" : "/") + readUri;

			String html = "<p>DIAN  Safety Read for subject " + subject + " " + armtext + " MR scan done on "
					+ scanDate + " is now available for review in the CNDA <a href=\"" + readLink + "\">here</a>.</p>";

			if (flagForReview) {
				html = html + "<p><b>This session has been flagged for review.</b></p>";
			}

			// Finally send the mail.
			UploadUtils.sendMessage(tos, ccs, header, html);
		}
	}

	String lookupArm() throws IOException {
		String subjectLookupUri = "data/archive/subjects?columns=group&label=" + subject + "&project=" + project
				+ "&format=csv";
		Response subjectLookup = UploadUtils.get(subjectLookupUri);

		StringReader sr = new StringReader(subjectLookup.getEntity().getText());
		CSVReader csvReader = new CSVReader(sr);
		List<String[]> results = csvReader.readAll();
		csvReader.close();

		String arm;

		if (results == null || results.size() < 2) {
			System.out.println("No subjects matched!");

			// Fall back on projectless search in case of sharing problem
			String pSubjectLookupUri = "data/archive/subjects?columns=group&label=" + subject + "&format=csv";
			Response pSubjectLookup = UploadUtils.get(pSubjectLookupUri);

			StringReader psr = new StringReader(pSubjectLookup.getEntity().getText());
			CSVReader pcsvReader = new CSVReader(psr);
			List<String[]> presults = pcsvReader.readAll();
			pcsvReader.close();

			if (presults == null || presults.size() < 2 || presults.size() > 2) {
				arm = null;
			} else {
				arm = presults.get(1)[1];
			}
		} else {
			arm = results.get(1)[1];
		}

		return arm;
	}
}
