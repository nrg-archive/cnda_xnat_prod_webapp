// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Mon Aug 04 12:50:13 CDT 2008
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class PipePipelinedetailsParameter extends BasePipePipelinedetailsParameter {

	public PipePipelinedetailsParameter(ItemI item)
	{
		super(item);
	}

	public PipePipelinedetailsParameter(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BasePipePipelinedetailsParameter(UserI user)
	 **/
	public PipePipelinedetailsParameter()
	{}

	public PipePipelinedetailsParameter(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
