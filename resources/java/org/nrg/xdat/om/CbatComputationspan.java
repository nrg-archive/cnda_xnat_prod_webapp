/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CbatComputationspan extends BaseCbatComputationspan {

	public CbatComputationspan(ItemI item)
	{
		super(item);
	}

	public CbatComputationspan(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCbatComputationspan(UserI user)
	 **/
	public CbatComputationspan()
	{}

	public CbatComputationspan(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
