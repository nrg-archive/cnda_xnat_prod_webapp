/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class UdsB7faqdata extends BaseUdsB7faqdata {

	public UdsB7faqdata(ItemI item)
	{
		super(item);
	}

	public UdsB7faqdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsB7faqdata(UserI user)
	 **/
	public UdsB7faqdata()
	{}

	public UdsB7faqdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
