/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CondrBraincolldata extends BaseCondrBraincolldata {

	public CondrBraincolldata(ItemI item)
	{
		super(item);
	}

	public CondrBraincolldata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCondrBraincolldata(UserI user)
	 **/
	public CondrBraincolldata()
	{}

	public CondrBraincolldata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
