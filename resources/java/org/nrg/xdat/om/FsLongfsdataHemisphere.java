/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class FsLongfsdataHemisphere extends BaseFsLongfsdataHemisphere {

	public FsLongfsdataHemisphere(ItemI item)
	{
		super(item);
	}

	public FsLongfsdataHemisphere(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsLongfsdataHemisphere(UserI user)
	 **/
	public FsLongfsdataHemisphere()
	{}

	public FsLongfsdataHemisphere(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
