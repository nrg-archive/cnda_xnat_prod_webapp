/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class PupPuptimecoursedataRoi extends BasePupPuptimecoursedataRoi {

	public PupPuptimecoursedataRoi(ItemI item)
	{
		super(item);
	}

	public PupPuptimecoursedataRoi(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BasePupPuptimecoursedataRoi(UserI user)
	 **/
	public PupPuptimecoursedataRoi()
	{}

	public PupPuptimecoursedataRoi(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
