/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class SfMedicalhistoryAllergy extends BaseSfMedicalhistoryAllergy {

	public SfMedicalhistoryAllergy(ItemI item)
	{
		super(item);
	}

	public SfMedicalhistoryAllergy(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseSfMedicalhistoryAllergy(UserI user)
	 **/
	public SfMedicalhistoryAllergy()
	{}

	public SfMedicalhistoryAllergy(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
