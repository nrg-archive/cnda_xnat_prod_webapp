/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CondrBraincolldataSurgicalext extends BaseCondrBraincolldataSurgicalext {

	public CondrBraincolldataSurgicalext(ItemI item)
	{
		super(item);
	}

	public CondrBraincolldataSurgicalext(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCondrBraincolldataSurgicalext(UserI user)
	 **/
	public CondrBraincolldataSurgicalext()
	{}

	public CondrBraincolldataSurgicalext(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
