/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class DianAv45metadata extends BaseDianAv45metadata {

	public DianAv45metadata(ItemI item)
	{
		super(item);
	}

	public DianAv45metadata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseDianAv45metadata(UserI user)
	 **/
	public DianAv45metadata()
	{}

	public DianAv45metadata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
