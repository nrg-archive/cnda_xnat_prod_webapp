/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class UdsA4drugsdata extends BaseUdsA4drugsdata {

	public UdsA4drugsdata(ItemI item)
	{
		super(item);
	}

	public UdsA4drugsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsA4drugsdata(UserI user)
	 **/
	public UdsA4drugsdata()
	{}

	public UdsA4drugsdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
