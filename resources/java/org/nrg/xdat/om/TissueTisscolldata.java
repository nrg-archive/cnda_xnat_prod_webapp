/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class TissueTisscolldata extends BaseTissueTisscolldata {

	public TissueTisscolldata(ItemI item)
	{
		super(item);
	}

	public TissueTisscolldata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseTissueTisscolldata(UserI user)
	 **/
	public TissueTisscolldata()
	{}

	public TissueTisscolldata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
