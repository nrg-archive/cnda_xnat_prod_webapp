/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class GeneticsGenotypesessiondata extends BaseGeneticsGenotypesessiondata {

	public GeneticsGenotypesessiondata(ItemI item)
	{
		super(item);
	}

	public GeneticsGenotypesessiondata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseGeneticsGenotypesessiondata(UserI user)
	 **/
	public GeneticsGenotypesessiondata()
	{}

	public GeneticsGenotypesessiondata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
