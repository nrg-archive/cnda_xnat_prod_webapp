/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class NihssNihstrokescaledata extends BaseNihssNihstrokescaledata {

	public NihssNihstrokescaledata(ItemI item)
	{
		super(item);
	}

	public NihssNihstrokescaledata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseNihssNihstrokescaledata(UserI user)
	 **/
	public NihssNihstrokescaledata()
	{}

	public NihssNihstrokescaledata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
