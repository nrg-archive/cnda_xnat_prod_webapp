/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class DianEligdata extends BaseDianEligdata {

	public DianEligdata(ItemI item)
	{
		super(item);
	}

	public DianEligdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseDianEligdata(UserI user)
	 **/
	public DianEligdata()
	{}

	public DianEligdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
