/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:17:18 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatAupdrs3data extends BaseXnatAupdrs3data {

	public XnatAupdrs3data(ItemI item)
	{
		super(item);
	}

	public XnatAupdrs3data(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatAupdrs3data(UserI user)
	 **/
	public XnatAupdrs3data()
	{}

	public XnatAupdrs3data(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
