/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CndaAtrophynildataPeak extends BaseCndaAtrophynildataPeak {

	public CndaAtrophynildataPeak(ItemI item)
	{
		super(item);
	}

	public CndaAtrophynildataPeak(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaAtrophynildataPeak(UserI user)
	 **/
	public CndaAtrophynildataPeak()
	{}

	public CndaAtrophynildataPeak(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
