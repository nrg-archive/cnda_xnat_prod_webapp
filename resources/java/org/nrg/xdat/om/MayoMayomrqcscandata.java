/*
 * GENERATED FILE
 * Created on Tue Jan 06 11:43:42 CST 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class MayoMayomrqcscandata extends BaseMayoMayomrqcscandata {

	public MayoMayomrqcscandata(ItemI item)
	{
		super(item);
	}

	public MayoMayomrqcscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseMayoMayomrqcscandata(UserI user)
	 **/
	public MayoMayomrqcscandata()
	{}

	public MayoMayomrqcscandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

	@Override
	public String getRating() {
		return getChosen() ? "1" : "0";
	}

}
