/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class DianDcappdata extends BaseDianDcappdata {

	public DianDcappdata(ItemI item)
	{
		super(item);
	}

	public DianDcappdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseDianDcappdata(UserI user)
	 **/
	public DianDcappdata()
	{}

	public DianDcappdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
