/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CbatCvoe extends BaseCbatCvoe {

	public CbatCvoe(ItemI item)
	{
		super(item);
	}

	public CbatCvoe(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCbatCvoe(UserI user)
	 **/
	public CbatCvoe()
	{}

	public CbatCvoe(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
