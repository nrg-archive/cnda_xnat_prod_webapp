/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CndaLevelsdata extends BaseCndaLevelsdata {

	public CndaLevelsdata(ItemI item)
	{
		super(item);
	}

	public CndaLevelsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaLevelsdata(UserI user)
	 **/
	public CndaLevelsdata()
	{}

	public CndaLevelsdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
