/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class UdsB4cdrdata extends BaseUdsB4cdrdata {

	public UdsB4cdrdata(ItemI item)
	{
		super(item);
	}

	public UdsB4cdrdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsB4cdrdata(UserI user)
	 **/
	public UdsB4cdrdata()
	{}

	public UdsB4cdrdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
