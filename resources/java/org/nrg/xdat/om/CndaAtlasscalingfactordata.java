//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Aug 18 11:41:39 CDT 2005
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.text.NumberFormat;
import java.util.*;

/**
 * @author XDAT
 *
 */
public class CndaAtlasscalingfactordata extends BaseCndaAtlasscalingfactordata {
	public CndaAtlasscalingfactordata(ItemI item)
	{
		super(item);
	}

	public CndaAtlasscalingfactordata(UserI user)
	{
		super(user);
	}

	public CndaAtlasscalingfactordata()
	{}

	public CndaAtlasscalingfactordata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

	public String getEicvDisplay()
	{
		NumberFormat formatter = NumberFormat.getInstance();
		formatter.setGroupingUsed(false);
		formatter.setMaximumFractionDigits(0);
		formatter.setMinimumFractionDigits(0);
		return formatter.format(this.getEicv());
	}
	
	public String getScalingfactorDisplay()
	{
		NumberFormat formatter = NumberFormat.getInstance();
		formatter.setGroupingUsed(false);
		formatter.setMaximumFractionDigits(2);
		formatter.setMinimumFractionDigits(2);
		return formatter.format(this.getScalingfactor());
	}
	
	public String getEtaDisplay()
	{
		NumberFormat formatter = NumberFormat.getInstance();
		formatter.setGroupingUsed(false);
		formatter.setMaximumFractionDigits(2);
		formatter.setMinimumFractionDigits(2);
		return formatter.format(this.getEta());
	}
}
