//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Aug 18 11:41:40 CDT 2005
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.text.NumberFormat;
import java.util.*;

/**
 * @author XDAT
 *
 */
public class CndaSegmentationfastdata extends BaseCndaSegmentationfastdata {

	public CndaSegmentationfastdata(ItemI item)
	{
		super(item);
	}

	public CndaSegmentationfastdata(UserI user)
	{
		super(user);
	}

	public CndaSegmentationfastdata()
	{}

	public CndaSegmentationfastdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

	public String getNWBV()
	{
        Double f= this.getBrainpercent();
	    if (f==null)
	    {
	        return null;
	    }else if (f instanceof Number){
		    NumberFormat formatter = NumberFormat.getInstance();
			formatter.setGroupingUsed(false);
			formatter.setMaximumFractionDigits(2);
			formatter.setMinimumFractionDigits(2);
			return formatter.format(f);
	    }else{
	        return f.toString();
	    }
	}

}
