/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class FsFsdata extends BaseFsFsdata {

	public FsFsdata(ItemI item)
	{
		super(item);
	}

	public FsFsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsFsdata(UserI user)
	 **/
	public FsFsdata()
	{}

	public FsFsdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
