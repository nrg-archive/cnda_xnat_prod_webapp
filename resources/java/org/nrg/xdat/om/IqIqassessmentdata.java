/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class IqIqassessmentdata extends BaseIqIqassessmentdata {

	public IqIqassessmentdata(ItemI item)
	{
		super(item);
	}

	public IqIqassessmentdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseIqIqassessmentdata(UserI user)
	 **/
	public IqIqassessmentdata()
	{}

	public IqIqassessmentdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
