/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:17:18 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatOtherqcscandata extends BaseXnatOtherqcscandata {

	public XnatOtherqcscandata(ItemI item)
	{
		super(item);
	}

	public XnatOtherqcscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatOtherqcscandata(UserI user)
	 **/
	public XnatOtherqcscandata()
	{}

	public XnatOtherqcscandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
