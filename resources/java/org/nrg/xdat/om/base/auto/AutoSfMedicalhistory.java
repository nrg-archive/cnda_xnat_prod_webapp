/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoSfMedicalhistory extends XnatSubjectassessordata implements org.nrg.xdat.model.SfMedicalhistoryI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoSfMedicalhistory.class);
	public static String SCHEMA_ELEMENT_NAME="sf:medicalHistory";

	public AutoSfMedicalhistory(ItemI item)
	{
		super(item);
	}

	public AutoSfMedicalhistory(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoSfMedicalhistory(UserI user)
	 **/
	public AutoSfMedicalhistory(){}

	public AutoSfMedicalhistory(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "sf:medicalHistory";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.SfCondition> _Conditions_condition =null;

	/**
	 * conditions/condition
	 * @return Returns an List of org.nrg.xdat.om.SfCondition
	 */
	public <A extends org.nrg.xdat.model.SfConditionI> List<A> getConditions_condition() {
		try{
			if (_Conditions_condition==null){
				_Conditions_condition=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("conditions/condition"));
			}
			return (List<A>) _Conditions_condition;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.SfCondition>();}
	}

	/**
	 * Sets the value for conditions/condition.
	 * @param v Value to Set.
	 */
	public void setConditions_condition(ItemI v) throws Exception{
		_Conditions_condition =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/conditions/condition",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/conditions/condition",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * conditions/condition
	 * Adds org.nrg.xdat.model.SfConditionI
	 */
	public <A extends org.nrg.xdat.model.SfConditionI> void addConditions_condition(A item) throws Exception{
	setConditions_condition((ItemI)item);
	}

	/**
	 * Removes the conditions/condition of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeConditions_condition(int index) throws java.lang.IndexOutOfBoundsException {
		_Conditions_condition =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/conditions/condition",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.SfCondition _Hypertension =null;

	/**
	 * hypertension
	 * @return org.nrg.xdat.om.SfCondition
	 */
	public org.nrg.xdat.om.SfCondition getHypertension() {
		try{
			if (_Hypertension==null){
				_Hypertension=((SfCondition)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("hypertension")));
				return _Hypertension;
			}else {
				return _Hypertension;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for hypertension.
	 * @param v Value to Set.
	 */
	public void setHypertension(ItemI v) throws Exception{
		_Hypertension =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/hypertension",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/hypertension",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * hypertension
	 * set org.nrg.xdat.model.SfConditionI
	 */
	public <A extends org.nrg.xdat.model.SfConditionI> void setHypertension(A item) throws Exception{
	setHypertension((ItemI)item);
	}

	/**
	 * Removes the hypertension.
	 * */
	public void removeHypertension() {
		_Hypertension =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/hypertension",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _HypertensionFK=null;

	/**
	 * @return Returns the sf:medicalHistory/hypertension_sf_condition_id.
	 */
	public Integer getHypertensionFK(){
		try{
			if (_HypertensionFK==null){
				_HypertensionFK=getIntegerProperty("sf:medicalHistory/hypertension_sf_condition_id");
				return _HypertensionFK;
			}else {
				return _HypertensionFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sf:medicalHistory/hypertension_sf_condition_id.
	 * @param v Value to Set.
	 */
	public void setHypertensionFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/hypertension_sf_condition_id",v);
		_HypertensionFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.SfCondition _Diabetes =null;

	/**
	 * diabetes
	 * @return org.nrg.xdat.om.SfCondition
	 */
	public org.nrg.xdat.om.SfCondition getDiabetes() {
		try{
			if (_Diabetes==null){
				_Diabetes=((SfCondition)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("diabetes")));
				return _Diabetes;
			}else {
				return _Diabetes;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for diabetes.
	 * @param v Value to Set.
	 */
	public void setDiabetes(ItemI v) throws Exception{
		_Diabetes =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/diabetes",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/diabetes",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * diabetes
	 * set org.nrg.xdat.model.SfConditionI
	 */
	public <A extends org.nrg.xdat.model.SfConditionI> void setDiabetes(A item) throws Exception{
	setDiabetes((ItemI)item);
	}

	/**
	 * Removes the diabetes.
	 * */
	public void removeDiabetes() {
		_Diabetes =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/diabetes",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _DiabetesFK=null;

	/**
	 * @return Returns the sf:medicalHistory/diabetes_sf_condition_id.
	 */
	public Integer getDiabetesFK(){
		try{
			if (_DiabetesFK==null){
				_DiabetesFK=getIntegerProperty("sf:medicalHistory/diabetes_sf_condition_id");
				return _DiabetesFK;
			}else {
				return _DiabetesFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sf:medicalHistory/diabetes_sf_condition_id.
	 * @param v Value to Set.
	 */
	public void setDiabetesFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/diabetes_sf_condition_id",v);
		_DiabetesFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.SfCondition _Hypercholesterolemia =null;

	/**
	 * hypercholesterolemia
	 * @return org.nrg.xdat.om.SfCondition
	 */
	public org.nrg.xdat.om.SfCondition getHypercholesterolemia() {
		try{
			if (_Hypercholesterolemia==null){
				_Hypercholesterolemia=((SfCondition)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("hypercholesterolemia")));
				return _Hypercholesterolemia;
			}else {
				return _Hypercholesterolemia;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for hypercholesterolemia.
	 * @param v Value to Set.
	 */
	public void setHypercholesterolemia(ItemI v) throws Exception{
		_Hypercholesterolemia =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/hypercholesterolemia",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/hypercholesterolemia",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * hypercholesterolemia
	 * set org.nrg.xdat.model.SfConditionI
	 */
	public <A extends org.nrg.xdat.model.SfConditionI> void setHypercholesterolemia(A item) throws Exception{
	setHypercholesterolemia((ItemI)item);
	}

	/**
	 * Removes the hypercholesterolemia.
	 * */
	public void removeHypercholesterolemia() {
		_Hypercholesterolemia =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/hypercholesterolemia",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _HypercholesterolemiaFK=null;

	/**
	 * @return Returns the sf:medicalHistory/hypercholesterolemia_sf_condition_id.
	 */
	public Integer getHypercholesterolemiaFK(){
		try{
			if (_HypercholesterolemiaFK==null){
				_HypercholesterolemiaFK=getIntegerProperty("sf:medicalHistory/hypercholesterolemia_sf_condition_id");
				return _HypercholesterolemiaFK;
			}else {
				return _HypercholesterolemiaFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sf:medicalHistory/hypercholesterolemia_sf_condition_id.
	 * @param v Value to Set.
	 */
	public void setHypercholesterolemiaFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/hypercholesterolemia_sf_condition_id",v);
		_HypercholesterolemiaFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.SfCondition _Stroke =null;

	/**
	 * stroke
	 * @return org.nrg.xdat.om.SfCondition
	 */
	public org.nrg.xdat.om.SfCondition getStroke() {
		try{
			if (_Stroke==null){
				_Stroke=((SfCondition)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("stroke")));
				return _Stroke;
			}else {
				return _Stroke;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for stroke.
	 * @param v Value to Set.
	 */
	public void setStroke(ItemI v) throws Exception{
		_Stroke =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/stroke",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/stroke",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * stroke
	 * set org.nrg.xdat.model.SfConditionI
	 */
	public <A extends org.nrg.xdat.model.SfConditionI> void setStroke(A item) throws Exception{
	setStroke((ItemI)item);
	}

	/**
	 * Removes the stroke.
	 * */
	public void removeStroke() {
		_Stroke =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/stroke",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _StrokeFK=null;

	/**
	 * @return Returns the sf:medicalHistory/stroke_sf_condition_id.
	 */
	public Integer getStrokeFK(){
		try{
			if (_StrokeFK==null){
				_StrokeFK=getIntegerProperty("sf:medicalHistory/stroke_sf_condition_id");
				return _StrokeFK;
			}else {
				return _StrokeFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sf:medicalHistory/stroke_sf_condition_id.
	 * @param v Value to Set.
	 */
	public void setStrokeFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/stroke_sf_condition_id",v);
		_StrokeFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.SfCondition _Autoimmunedisease =null;

	/**
	 * autoimmuneDisease
	 * @return org.nrg.xdat.om.SfCondition
	 */
	public org.nrg.xdat.om.SfCondition getAutoimmunedisease() {
		try{
			if (_Autoimmunedisease==null){
				_Autoimmunedisease=((SfCondition)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("autoimmuneDisease")));
				return _Autoimmunedisease;
			}else {
				return _Autoimmunedisease;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for autoimmuneDisease.
	 * @param v Value to Set.
	 */
	public void setAutoimmunedisease(ItemI v) throws Exception{
		_Autoimmunedisease =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/autoimmuneDisease",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/autoimmuneDisease",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * autoimmuneDisease
	 * set org.nrg.xdat.model.SfConditionI
	 */
	public <A extends org.nrg.xdat.model.SfConditionI> void setAutoimmunedisease(A item) throws Exception{
	setAutoimmunedisease((ItemI)item);
	}

	/**
	 * Removes the autoimmuneDisease.
	 * */
	public void removeAutoimmunedisease() {
		_Autoimmunedisease =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/autoimmuneDisease",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _AutoimmunediseaseFK=null;

	/**
	 * @return Returns the sf:medicalHistory/autoimmunedisease_sf_condition_id.
	 */
	public Integer getAutoimmunediseaseFK(){
		try{
			if (_AutoimmunediseaseFK==null){
				_AutoimmunediseaseFK=getIntegerProperty("sf:medicalHistory/autoimmunedisease_sf_condition_id");
				return _AutoimmunediseaseFK;
			}else {
				return _AutoimmunediseaseFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sf:medicalHistory/autoimmunedisease_sf_condition_id.
	 * @param v Value to Set.
	 */
	public void setAutoimmunediseaseFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/autoimmunedisease_sf_condition_id",v);
		_AutoimmunediseaseFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.SfCondition _Headache =null;

	/**
	 * headache
	 * @return org.nrg.xdat.om.SfCondition
	 */
	public org.nrg.xdat.om.SfCondition getHeadache() {
		try{
			if (_Headache==null){
				_Headache=((SfCondition)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("headache")));
				return _Headache;
			}else {
				return _Headache;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for headache.
	 * @param v Value to Set.
	 */
	public void setHeadache(ItemI v) throws Exception{
		_Headache =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/headache",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/headache",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * headache
	 * set org.nrg.xdat.model.SfConditionI
	 */
	public <A extends org.nrg.xdat.model.SfConditionI> void setHeadache(A item) throws Exception{
	setHeadache((ItemI)item);
	}

	/**
	 * Removes the headache.
	 * */
	public void removeHeadache() {
		_Headache =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/headache",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _HeadacheFK=null;

	/**
	 * @return Returns the sf:medicalHistory/headache_sf_condition_id.
	 */
	public Integer getHeadacheFK(){
		try{
			if (_HeadacheFK==null){
				_HeadacheFK=getIntegerProperty("sf:medicalHistory/headache_sf_condition_id");
				return _HeadacheFK;
			}else {
				return _HeadacheFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sf:medicalHistory/headache_sf_condition_id.
	 * @param v Value to Set.
	 */
	public void setHeadacheFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/headache_sf_condition_id",v);
		_HeadacheFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.SfCondition _Seizure =null;

	/**
	 * seizure
	 * @return org.nrg.xdat.om.SfCondition
	 */
	public org.nrg.xdat.om.SfCondition getSeizure() {
		try{
			if (_Seizure==null){
				_Seizure=((SfCondition)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("seizure")));
				return _Seizure;
			}else {
				return _Seizure;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for seizure.
	 * @param v Value to Set.
	 */
	public void setSeizure(ItemI v) throws Exception{
		_Seizure =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/seizure",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/seizure",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * seizure
	 * set org.nrg.xdat.model.SfConditionI
	 */
	public <A extends org.nrg.xdat.model.SfConditionI> void setSeizure(A item) throws Exception{
	setSeizure((ItemI)item);
	}

	/**
	 * Removes the seizure.
	 * */
	public void removeSeizure() {
		_Seizure =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/seizure",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _SeizureFK=null;

	/**
	 * @return Returns the sf:medicalHistory/seizure_sf_condition_id.
	 */
	public Integer getSeizureFK(){
		try{
			if (_SeizureFK==null){
				_SeizureFK=getIntegerProperty("sf:medicalHistory/seizure_sf_condition_id");
				return _SeizureFK;
			}else {
				return _SeizureFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sf:medicalHistory/seizure_sf_condition_id.
	 * @param v Value to Set.
	 */
	public void setSeizureFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/seizure_sf_condition_id",v);
		_SeizureFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.SfMedicalhistoryAllergy> _Allergies_allergy =null;

	/**
	 * allergies/allergy
	 * @return Returns an List of org.nrg.xdat.om.SfMedicalhistoryAllergy
	 */
	public <A extends org.nrg.xdat.model.SfMedicalhistoryAllergyI> List<A> getAllergies_allergy() {
		try{
			if (_Allergies_allergy==null){
				_Allergies_allergy=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("allergies/allergy"));
			}
			return (List<A>) _Allergies_allergy;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.SfMedicalhistoryAllergy>();}
	}

	/**
	 * Sets the value for allergies/allergy.
	 * @param v Value to Set.
	 */
	public void setAllergies_allergy(ItemI v) throws Exception{
		_Allergies_allergy =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/allergies/allergy",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/allergies/allergy",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * allergies/allergy
	 * Adds org.nrg.xdat.model.SfMedicalhistoryAllergyI
	 */
	public <A extends org.nrg.xdat.model.SfMedicalhistoryAllergyI> void addAllergies_allergy(A item) throws Exception{
	setAllergies_allergy((ItemI)item);
	}

	/**
	 * Removes the allergies/allergy of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeAllergies_allergy(int index) throws java.lang.IndexOutOfBoundsException {
		_Allergies_allergy =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/allergies/allergy",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Smokingstatus=null;

	/**
	 * @return Returns the smokingStatus.
	 */
	public String getSmokingstatus(){
		try{
			if (_Smokingstatus==null){
				_Smokingstatus=getStringProperty("smokingStatus");
				return _Smokingstatus;
			}else {
				return _Smokingstatus;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for smokingStatus.
	 * @param v Value to Set.
	 */
	public void setSmokingstatus(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/smokingStatus",v);
		_Smokingstatus=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Smokingppd=null;

	/**
	 * @return Returns the smokingPpd.
	 */
	public Object getSmokingppd(){
		try{
			if (_Smokingppd==null){
				_Smokingppd=getProperty("smokingPpd");
				return _Smokingppd;
			}else {
				return _Smokingppd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for smokingPpd.
	 * @param v Value to Set.
	 */
	public void setSmokingppd(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/smokingPpd",v);
		_Smokingppd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Alcoholstatus=null;

	/**
	 * @return Returns the alcoholStatus.
	 */
	public String getAlcoholstatus(){
		try{
			if (_Alcoholstatus==null){
				_Alcoholstatus=getStringProperty("alcoholStatus");
				return _Alcoholstatus;
			}else {
				return _Alcoholstatus;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for alcoholStatus.
	 * @param v Value to Set.
	 */
	public void setAlcoholstatus(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/alcoholStatus",v);
		_Alcoholstatus=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Alcoholuse=null;

	/**
	 * @return Returns the alcoholUse.
	 */
	public String getAlcoholuse(){
		try{
			if (_Alcoholuse==null){
				_Alcoholuse=getStringProperty("alcoholUse");
				return _Alcoholuse;
			}else {
				return _Alcoholuse;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for alcoholUse.
	 * @param v Value to Set.
	 */
	public void setAlcoholuse(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/alcoholUse",v);
		_Alcoholuse=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Illicitdrugstatus=null;

	/**
	 * @return Returns the illicitDrugStatus.
	 */
	public String getIllicitdrugstatus(){
		try{
			if (_Illicitdrugstatus==null){
				_Illicitdrugstatus=getStringProperty("illicitDrugStatus");
				return _Illicitdrugstatus;
			}else {
				return _Illicitdrugstatus;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for illicitDrugStatus.
	 * @param v Value to Set.
	 */
	public void setIllicitdrugstatus(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/illicitDrugStatus",v);
		_Illicitdrugstatus=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Illicitdrugdetails=null;

	/**
	 * @return Returns the illicitDrugDetails.
	 */
	public String getIllicitdrugdetails(){
		try{
			if (_Illicitdrugdetails==null){
				_Illicitdrugdetails=getStringProperty("illicitDrugDetails");
				return _Illicitdrugdetails;
			}else {
				return _Illicitdrugdetails;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for illicitDrugDetails.
	 * @param v Value to Set.
	 */
	public void setIllicitdrugdetails(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/illicitDrugDetails",v);
		_Illicitdrugdetails=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.SfDeficits _Deficits =null;

	/**
	 * deficits
	 * @return org.nrg.xdat.om.SfDeficits
	 */
	public org.nrg.xdat.om.SfDeficits getDeficits() {
		try{
			if (_Deficits==null){
				_Deficits=((SfDeficits)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("deficits")));
				return _Deficits;
			}else {
				return _Deficits;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for deficits.
	 * @param v Value to Set.
	 */
	public void setDeficits(ItemI v) throws Exception{
		_Deficits =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/deficits",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/deficits",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * deficits
	 * set org.nrg.xdat.model.SfDeficitsI
	 */
	public <A extends org.nrg.xdat.model.SfDeficitsI> void setDeficits(A item) throws Exception{
	setDeficits((ItemI)item);
	}

	/**
	 * Removes the deficits.
	 * */
	public void removeDeficits() {
		_Deficits =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/deficits",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _DeficitsFK=null;

	/**
	 * @return Returns the sf:medicalHistory/deficits_sf_deficits_id.
	 */
	public Integer getDeficitsFK(){
		try{
			if (_DeficitsFK==null){
				_DeficitsFK=getIntegerProperty("sf:medicalHistory/deficits_sf_deficits_id");
				return _DeficitsFK;
			}else {
				return _DeficitsFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sf:medicalHistory/deficits_sf_deficits_id.
	 * @param v Value to Set.
	 */
	public void setDeficitsFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/deficits_sf_deficits_id",v);
		_DeficitsFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Deficitnotes=null;

	/**
	 * @return Returns the deficitNotes.
	 */
	public String getDeficitnotes(){
		try{
			if (_Deficitnotes==null){
				_Deficitnotes=getStringProperty("deficitNotes");
				return _Deficitnotes;
			}else {
				return _Deficitnotes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for deficitNotes.
	 * @param v Value to Set.
	 */
	public void setDeficitnotes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/deficitNotes",v);
		_Deficitnotes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Medicalhistorynotes=null;

	/**
	 * @return Returns the medicalHistoryNotes.
	 */
	public String getMedicalhistorynotes(){
		try{
			if (_Medicalhistorynotes==null){
				_Medicalhistorynotes=getStringProperty("medicalHistoryNotes");
				return _Medicalhistorynotes;
			}else {
				return _Medicalhistorynotes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for medicalHistoryNotes.
	 * @param v Value to Set.
	 */
	public void setMedicalhistorynotes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/medicalHistoryNotes",v);
		_Medicalhistorynotes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.SfMedicalhistory> getAllSfMedicalhistorys(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfMedicalhistory> al = new ArrayList<org.nrg.xdat.om.SfMedicalhistory>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.SfMedicalhistory> getSfMedicalhistorysByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfMedicalhistory> al = new ArrayList<org.nrg.xdat.om.SfMedicalhistory>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.SfMedicalhistory> getSfMedicalhistorysByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfMedicalhistory> al = new ArrayList<org.nrg.xdat.om.SfMedicalhistory>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static SfMedicalhistory getSfMedicalhistorysById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("sf:medicalHistory/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (SfMedicalhistory) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //conditions/condition
	        for(org.nrg.xdat.model.SfConditionI childConditions_condition : this.getConditions_condition()){
	            if (childConditions_condition!=null){
	              for(ResourceFile rf: ((SfCondition)childConditions_condition).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("conditions/condition[" + ((SfCondition)childConditions_condition).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("conditions/condition/" + ((SfCondition)childConditions_condition).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	        //hypertension
	        SfCondition childHypertension = (SfCondition)this.getHypertension();
	            if (childHypertension!=null){
	              for(ResourceFile rf: ((SfCondition)childHypertension).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("hypertension[" + ((SfCondition)childHypertension).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("hypertension/" + ((SfCondition)childHypertension).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //diabetes
	        SfCondition childDiabetes = (SfCondition)this.getDiabetes();
	            if (childDiabetes!=null){
	              for(ResourceFile rf: ((SfCondition)childDiabetes).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("diabetes[" + ((SfCondition)childDiabetes).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("diabetes/" + ((SfCondition)childDiabetes).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //hypercholesterolemia
	        SfCondition childHypercholesterolemia = (SfCondition)this.getHypercholesterolemia();
	            if (childHypercholesterolemia!=null){
	              for(ResourceFile rf: ((SfCondition)childHypercholesterolemia).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("hypercholesterolemia[" + ((SfCondition)childHypercholesterolemia).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("hypercholesterolemia/" + ((SfCondition)childHypercholesterolemia).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //stroke
	        SfCondition childStroke = (SfCondition)this.getStroke();
	            if (childStroke!=null){
	              for(ResourceFile rf: ((SfCondition)childStroke).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("stroke[" + ((SfCondition)childStroke).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("stroke/" + ((SfCondition)childStroke).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //autoimmuneDisease
	        SfCondition childAutoimmunedisease = (SfCondition)this.getAutoimmunedisease();
	            if (childAutoimmunedisease!=null){
	              for(ResourceFile rf: ((SfCondition)childAutoimmunedisease).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("autoimmuneDisease[" + ((SfCondition)childAutoimmunedisease).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("autoimmuneDisease/" + ((SfCondition)childAutoimmunedisease).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //headache
	        SfCondition childHeadache = (SfCondition)this.getHeadache();
	            if (childHeadache!=null){
	              for(ResourceFile rf: ((SfCondition)childHeadache).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("headache[" + ((SfCondition)childHeadache).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("headache/" + ((SfCondition)childHeadache).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //seizure
	        SfCondition childSeizure = (SfCondition)this.getSeizure();
	            if (childSeizure!=null){
	              for(ResourceFile rf: ((SfCondition)childSeizure).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("seizure[" + ((SfCondition)childSeizure).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("seizure/" + ((SfCondition)childSeizure).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //allergies/allergy
	        for(org.nrg.xdat.model.SfMedicalhistoryAllergyI childAllergies_allergy : this.getAllergies_allergy()){
	            if (childAllergies_allergy!=null){
	              for(ResourceFile rf: ((SfMedicalhistoryAllergy)childAllergies_allergy).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("allergies/allergy[" + ((SfMedicalhistoryAllergy)childAllergies_allergy).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("allergies/allergy/" + ((SfMedicalhistoryAllergy)childAllergies_allergy).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	        //deficits
	        SfDeficits childDeficits = (SfDeficits)this.getDeficits();
	            if (childDeficits!=null){
	              for(ResourceFile rf: ((SfDeficits)childDeficits).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("deficits[" + ((SfDeficits)childDeficits).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("deficits/" + ((SfDeficits)childDeficits).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
