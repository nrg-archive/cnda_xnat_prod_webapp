/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoBehavioralTaskssummarydataTaskRun extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.BehavioralTaskssummarydataTaskRunI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoBehavioralTaskssummarydataTaskRun.class);
	public static String SCHEMA_ELEMENT_NAME="behavioral:tasksSummaryData_task_run";

	public AutoBehavioralTaskssummarydataTaskRun(ItemI item)
	{
		super(item);
	}

	public AutoBehavioralTaskssummarydataTaskRun(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoBehavioralTaskssummarydataTaskRun(UserI user)
	 **/
	public AutoBehavioralTaskssummarydataTaskRun(){}

	public AutoBehavioralTaskssummarydataTaskRun(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "behavioral:tasksSummaryData_task_run";
	}
	 private org.nrg.xdat.om.BehavioralStatistics _Statistics =null;

	/**
	 * statistics
	 * @return org.nrg.xdat.om.BehavioralStatistics
	 */
	public org.nrg.xdat.om.BehavioralStatistics getStatistics() {
		try{
			if (_Statistics==null){
				_Statistics=((BehavioralStatistics)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("statistics")));
				return _Statistics;
			}else {
				return _Statistics;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for statistics.
	 * @param v Value to Set.
	 */
	public void setStatistics(ItemI v) throws Exception{
		_Statistics =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/statistics",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/statistics",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * statistics
	 * set org.nrg.xdat.model.BehavioralStatisticsI
	 */
	public <A extends org.nrg.xdat.model.BehavioralStatisticsI> void setStatistics(A item) throws Exception{
	setStatistics((ItemI)item);
	}

	/**
	 * Removes the statistics.
	 * */
	public void removeStatistics() {
		_Statistics =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/statistics",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _StatisticsFK=null;

	/**
	 * @return Returns the behavioral:tasksSummaryData_task_run/statistics_behavioral_statistics_id.
	 */
	public Integer getStatisticsFK(){
		try{
			if (_StatisticsFK==null){
				_StatisticsFK=getIntegerProperty("behavioral:tasksSummaryData_task_run/statistics_behavioral_statistics_id");
				return _StatisticsFK;
			}else {
				return _StatisticsFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for behavioral:tasksSummaryData_task_run/statistics_behavioral_statistics_id.
	 * @param v Value to Set.
	 */
	public void setStatisticsFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/statistics_behavioral_statistics_id",v);
		_StatisticsFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Number=null;

	/**
	 * @return Returns the number.
	 */
	public Integer getNumber() {
		try{
			if (_Number==null){
				_Number=getIntegerProperty("number");
				return _Number;
			}else {
				return _Number;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for number.
	 * @param v Value to Set.
	 */
	public void setNumber(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/number",v);
		_Number=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _BehavioralTaskssummarydataTaskRunId=null;

	/**
	 * @return Returns the behavioral_tasksSummaryData_task_run_id.
	 */
	public Integer getBehavioralTaskssummarydataTaskRunId() {
		try{
			if (_BehavioralTaskssummarydataTaskRunId==null){
				_BehavioralTaskssummarydataTaskRunId=getIntegerProperty("behavioral_tasksSummaryData_task_run_id");
				return _BehavioralTaskssummarydataTaskRunId;
			}else {
				return _BehavioralTaskssummarydataTaskRunId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for behavioral_tasksSummaryData_task_run_id.
	 * @param v Value to Set.
	 */
	public void setBehavioralTaskssummarydataTaskRunId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/behavioral_tasksSummaryData_task_run_id",v);
		_BehavioralTaskssummarydataTaskRunId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydataTaskRun> getAllBehavioralTaskssummarydataTaskRuns(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydataTaskRun> al = new ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydataTaskRun>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydataTaskRun> getBehavioralTaskssummarydataTaskRunsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydataTaskRun> al = new ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydataTaskRun>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydataTaskRun> getBehavioralTaskssummarydataTaskRunsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydataTaskRun> al = new ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydataTaskRun>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static BehavioralTaskssummarydataTaskRun getBehavioralTaskssummarydataTaskRunsByBehavioralTaskssummarydataTaskRunId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("behavioral:tasksSummaryData_task_run/behavioral_tasksSummaryData_task_run_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (BehavioralTaskssummarydataTaskRun) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //statistics
	        BehavioralStatistics childStatistics = (BehavioralStatistics)this.getStatistics();
	            if (childStatistics!=null){
	              for(ResourceFile rf: ((BehavioralStatistics)childStatistics).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("statistics[" + ((BehavioralStatistics)childStatistics).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("statistics/" + ((BehavioralStatistics)childStatistics).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
