/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:00 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoIpipIpipptdata extends XnatSubjectassessordata implements org.nrg.xdat.model.IpipIpipptdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoIpipIpipptdata.class);
	public static String SCHEMA_ELEMENT_NAME="ipip:ipipptData";

	public AutoIpipIpipptdata(ItemI item)
	{
		super(item);
	}

	public AutoIpipIpipptdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoIpipIpipptdata(UserI user)
	 **/
	public AutoIpipIpipptdata(){}

	public AutoIpipIpipptdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "ipip:ipipptData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Source=null;

	/**
	 * @return Returns the source.
	 */
	public String getSource(){
		try{
			if (_Source==null){
				_Source=getStringProperty("source");
				return _Source;
			}else {
				return _Source;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for source.
	 * @param v Value to Set.
	 */
	public void setSource(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/source",v);
		_Source=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q1=null;

	/**
	 * @return Returns the Q1.
	 */
	public Integer getQ1() {
		try{
			if (_Q1==null){
				_Q1=getIntegerProperty("Q1");
				return _Q1;
			}else {
				return _Q1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q1.
	 * @param v Value to Set.
	 */
	public void setQ1(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q1",v);
		_Q1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q2=null;

	/**
	 * @return Returns the Q2.
	 */
	public Integer getQ2() {
		try{
			if (_Q2==null){
				_Q2=getIntegerProperty("Q2");
				return _Q2;
			}else {
				return _Q2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q2.
	 * @param v Value to Set.
	 */
	public void setQ2(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q2",v);
		_Q2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q3=null;

	/**
	 * @return Returns the Q3.
	 */
	public Integer getQ3() {
		try{
			if (_Q3==null){
				_Q3=getIntegerProperty("Q3");
				return _Q3;
			}else {
				return _Q3;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q3.
	 * @param v Value to Set.
	 */
	public void setQ3(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q3",v);
		_Q3=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q4=null;

	/**
	 * @return Returns the Q4.
	 */
	public Integer getQ4() {
		try{
			if (_Q4==null){
				_Q4=getIntegerProperty("Q4");
				return _Q4;
			}else {
				return _Q4;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q4.
	 * @param v Value to Set.
	 */
	public void setQ4(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q4",v);
		_Q4=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q5=null;

	/**
	 * @return Returns the Q5.
	 */
	public Integer getQ5() {
		try{
			if (_Q5==null){
				_Q5=getIntegerProperty("Q5");
				return _Q5;
			}else {
				return _Q5;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q5.
	 * @param v Value to Set.
	 */
	public void setQ5(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q5",v);
		_Q5=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q6=null;

	/**
	 * @return Returns the Q6.
	 */
	public Integer getQ6() {
		try{
			if (_Q6==null){
				_Q6=getIntegerProperty("Q6");
				return _Q6;
			}else {
				return _Q6;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q6.
	 * @param v Value to Set.
	 */
	public void setQ6(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q6",v);
		_Q6=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q7=null;

	/**
	 * @return Returns the Q7.
	 */
	public Integer getQ7() {
		try{
			if (_Q7==null){
				_Q7=getIntegerProperty("Q7");
				return _Q7;
			}else {
				return _Q7;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q7.
	 * @param v Value to Set.
	 */
	public void setQ7(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q7",v);
		_Q7=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q8=null;

	/**
	 * @return Returns the Q8.
	 */
	public Integer getQ8() {
		try{
			if (_Q8==null){
				_Q8=getIntegerProperty("Q8");
				return _Q8;
			}else {
				return _Q8;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q8.
	 * @param v Value to Set.
	 */
	public void setQ8(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q8",v);
		_Q8=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q9=null;

	/**
	 * @return Returns the Q9.
	 */
	public Integer getQ9() {
		try{
			if (_Q9==null){
				_Q9=getIntegerProperty("Q9");
				return _Q9;
			}else {
				return _Q9;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q9.
	 * @param v Value to Set.
	 */
	public void setQ9(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q9",v);
		_Q9=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q10=null;

	/**
	 * @return Returns the Q10.
	 */
	public Integer getQ10() {
		try{
			if (_Q10==null){
				_Q10=getIntegerProperty("Q10");
				return _Q10;
			}else {
				return _Q10;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q10.
	 * @param v Value to Set.
	 */
	public void setQ10(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q10",v);
		_Q10=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q11=null;

	/**
	 * @return Returns the Q11.
	 */
	public Integer getQ11() {
		try{
			if (_Q11==null){
				_Q11=getIntegerProperty("Q11");
				return _Q11;
			}else {
				return _Q11;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q11.
	 * @param v Value to Set.
	 */
	public void setQ11(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q11",v);
		_Q11=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q12=null;

	/**
	 * @return Returns the Q12.
	 */
	public Integer getQ12() {
		try{
			if (_Q12==null){
				_Q12=getIntegerProperty("Q12");
				return _Q12;
			}else {
				return _Q12;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q12.
	 * @param v Value to Set.
	 */
	public void setQ12(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q12",v);
		_Q12=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q13=null;

	/**
	 * @return Returns the Q13.
	 */
	public Integer getQ13() {
		try{
			if (_Q13==null){
				_Q13=getIntegerProperty("Q13");
				return _Q13;
			}else {
				return _Q13;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q13.
	 * @param v Value to Set.
	 */
	public void setQ13(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q13",v);
		_Q13=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q14=null;

	/**
	 * @return Returns the Q14.
	 */
	public Integer getQ14() {
		try{
			if (_Q14==null){
				_Q14=getIntegerProperty("Q14");
				return _Q14;
			}else {
				return _Q14;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q14.
	 * @param v Value to Set.
	 */
	public void setQ14(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q14",v);
		_Q14=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q15=null;

	/**
	 * @return Returns the Q15.
	 */
	public Integer getQ15() {
		try{
			if (_Q15==null){
				_Q15=getIntegerProperty("Q15");
				return _Q15;
			}else {
				return _Q15;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q15.
	 * @param v Value to Set.
	 */
	public void setQ15(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q15",v);
		_Q15=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q16=null;

	/**
	 * @return Returns the Q16.
	 */
	public Integer getQ16() {
		try{
			if (_Q16==null){
				_Q16=getIntegerProperty("Q16");
				return _Q16;
			}else {
				return _Q16;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q16.
	 * @param v Value to Set.
	 */
	public void setQ16(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q16",v);
		_Q16=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q17=null;

	/**
	 * @return Returns the Q17.
	 */
	public Integer getQ17() {
		try{
			if (_Q17==null){
				_Q17=getIntegerProperty("Q17");
				return _Q17;
			}else {
				return _Q17;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q17.
	 * @param v Value to Set.
	 */
	public void setQ17(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q17",v);
		_Q17=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q18=null;

	/**
	 * @return Returns the Q18.
	 */
	public Integer getQ18() {
		try{
			if (_Q18==null){
				_Q18=getIntegerProperty("Q18");
				return _Q18;
			}else {
				return _Q18;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q18.
	 * @param v Value to Set.
	 */
	public void setQ18(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q18",v);
		_Q18=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q19=null;

	/**
	 * @return Returns the Q19.
	 */
	public Integer getQ19() {
		try{
			if (_Q19==null){
				_Q19=getIntegerProperty("Q19");
				return _Q19;
			}else {
				return _Q19;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q19.
	 * @param v Value to Set.
	 */
	public void setQ19(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q19",v);
		_Q19=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q20=null;

	/**
	 * @return Returns the Q20.
	 */
	public Integer getQ20() {
		try{
			if (_Q20==null){
				_Q20=getIntegerProperty("Q20");
				return _Q20;
			}else {
				return _Q20;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q20.
	 * @param v Value to Set.
	 */
	public void setQ20(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q20",v);
		_Q20=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q21=null;

	/**
	 * @return Returns the Q21.
	 */
	public Integer getQ21() {
		try{
			if (_Q21==null){
				_Q21=getIntegerProperty("Q21");
				return _Q21;
			}else {
				return _Q21;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q21.
	 * @param v Value to Set.
	 */
	public void setQ21(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q21",v);
		_Q21=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q22=null;

	/**
	 * @return Returns the Q22.
	 */
	public Integer getQ22() {
		try{
			if (_Q22==null){
				_Q22=getIntegerProperty("Q22");
				return _Q22;
			}else {
				return _Q22;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q22.
	 * @param v Value to Set.
	 */
	public void setQ22(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q22",v);
		_Q22=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q23=null;

	/**
	 * @return Returns the Q23.
	 */
	public Integer getQ23() {
		try{
			if (_Q23==null){
				_Q23=getIntegerProperty("Q23");
				return _Q23;
			}else {
				return _Q23;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q23.
	 * @param v Value to Set.
	 */
	public void setQ23(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q23",v);
		_Q23=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q24=null;

	/**
	 * @return Returns the Q24.
	 */
	public Integer getQ24() {
		try{
			if (_Q24==null){
				_Q24=getIntegerProperty("Q24");
				return _Q24;
			}else {
				return _Q24;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q24.
	 * @param v Value to Set.
	 */
	public void setQ24(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q24",v);
		_Q24=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q25=null;

	/**
	 * @return Returns the Q25.
	 */
	public Integer getQ25() {
		try{
			if (_Q25==null){
				_Q25=getIntegerProperty("Q25");
				return _Q25;
			}else {
				return _Q25;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q25.
	 * @param v Value to Set.
	 */
	public void setQ25(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q25",v);
		_Q25=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q26=null;

	/**
	 * @return Returns the Q26.
	 */
	public Integer getQ26() {
		try{
			if (_Q26==null){
				_Q26=getIntegerProperty("Q26");
				return _Q26;
			}else {
				return _Q26;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q26.
	 * @param v Value to Set.
	 */
	public void setQ26(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q26",v);
		_Q26=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q27=null;

	/**
	 * @return Returns the Q27.
	 */
	public Integer getQ27() {
		try{
			if (_Q27==null){
				_Q27=getIntegerProperty("Q27");
				return _Q27;
			}else {
				return _Q27;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q27.
	 * @param v Value to Set.
	 */
	public void setQ27(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q27",v);
		_Q27=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q28=null;

	/**
	 * @return Returns the Q28.
	 */
	public Integer getQ28() {
		try{
			if (_Q28==null){
				_Q28=getIntegerProperty("Q28");
				return _Q28;
			}else {
				return _Q28;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q28.
	 * @param v Value to Set.
	 */
	public void setQ28(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q28",v);
		_Q28=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q29=null;

	/**
	 * @return Returns the Q29.
	 */
	public Integer getQ29() {
		try{
			if (_Q29==null){
				_Q29=getIntegerProperty("Q29");
				return _Q29;
			}else {
				return _Q29;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q29.
	 * @param v Value to Set.
	 */
	public void setQ29(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q29",v);
		_Q29=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q30=null;

	/**
	 * @return Returns the Q30.
	 */
	public Integer getQ30() {
		try{
			if (_Q30==null){
				_Q30=getIntegerProperty("Q30");
				return _Q30;
			}else {
				return _Q30;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q30.
	 * @param v Value to Set.
	 */
	public void setQ30(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q30",v);
		_Q30=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q31=null;

	/**
	 * @return Returns the Q31.
	 */
	public Integer getQ31() {
		try{
			if (_Q31==null){
				_Q31=getIntegerProperty("Q31");
				return _Q31;
			}else {
				return _Q31;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q31.
	 * @param v Value to Set.
	 */
	public void setQ31(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q31",v);
		_Q31=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q32=null;

	/**
	 * @return Returns the Q32.
	 */
	public Integer getQ32() {
		try{
			if (_Q32==null){
				_Q32=getIntegerProperty("Q32");
				return _Q32;
			}else {
				return _Q32;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q32.
	 * @param v Value to Set.
	 */
	public void setQ32(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q32",v);
		_Q32=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q33=null;

	/**
	 * @return Returns the Q33.
	 */
	public Integer getQ33() {
		try{
			if (_Q33==null){
				_Q33=getIntegerProperty("Q33");
				return _Q33;
			}else {
				return _Q33;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q33.
	 * @param v Value to Set.
	 */
	public void setQ33(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q33",v);
		_Q33=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q34=null;

	/**
	 * @return Returns the Q34.
	 */
	public Integer getQ34() {
		try{
			if (_Q34==null){
				_Q34=getIntegerProperty("Q34");
				return _Q34;
			}else {
				return _Q34;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q34.
	 * @param v Value to Set.
	 */
	public void setQ34(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q34",v);
		_Q34=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q35=null;

	/**
	 * @return Returns the Q35.
	 */
	public Integer getQ35() {
		try{
			if (_Q35==null){
				_Q35=getIntegerProperty("Q35");
				return _Q35;
			}else {
				return _Q35;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q35.
	 * @param v Value to Set.
	 */
	public void setQ35(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q35",v);
		_Q35=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q36=null;

	/**
	 * @return Returns the Q36.
	 */
	public Integer getQ36() {
		try{
			if (_Q36==null){
				_Q36=getIntegerProperty("Q36");
				return _Q36;
			}else {
				return _Q36;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q36.
	 * @param v Value to Set.
	 */
	public void setQ36(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q36",v);
		_Q36=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q37=null;

	/**
	 * @return Returns the Q37.
	 */
	public Integer getQ37() {
		try{
			if (_Q37==null){
				_Q37=getIntegerProperty("Q37");
				return _Q37;
			}else {
				return _Q37;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q37.
	 * @param v Value to Set.
	 */
	public void setQ37(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q37",v);
		_Q37=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q38=null;

	/**
	 * @return Returns the Q38.
	 */
	public Integer getQ38() {
		try{
			if (_Q38==null){
				_Q38=getIntegerProperty("Q38");
				return _Q38;
			}else {
				return _Q38;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q38.
	 * @param v Value to Set.
	 */
	public void setQ38(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q38",v);
		_Q38=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q39=null;

	/**
	 * @return Returns the Q39.
	 */
	public Integer getQ39() {
		try{
			if (_Q39==null){
				_Q39=getIntegerProperty("Q39");
				return _Q39;
			}else {
				return _Q39;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q39.
	 * @param v Value to Set.
	 */
	public void setQ39(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q39",v);
		_Q39=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q40=null;

	/**
	 * @return Returns the Q40.
	 */
	public Integer getQ40() {
		try{
			if (_Q40==null){
				_Q40=getIntegerProperty("Q40");
				return _Q40;
			}else {
				return _Q40;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q40.
	 * @param v Value to Set.
	 */
	public void setQ40(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q40",v);
		_Q40=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q41=null;

	/**
	 * @return Returns the Q41.
	 */
	public Integer getQ41() {
		try{
			if (_Q41==null){
				_Q41=getIntegerProperty("Q41");
				return _Q41;
			}else {
				return _Q41;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q41.
	 * @param v Value to Set.
	 */
	public void setQ41(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q41",v);
		_Q41=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q42=null;

	/**
	 * @return Returns the Q42.
	 */
	public Integer getQ42() {
		try{
			if (_Q42==null){
				_Q42=getIntegerProperty("Q42");
				return _Q42;
			}else {
				return _Q42;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q42.
	 * @param v Value to Set.
	 */
	public void setQ42(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q42",v);
		_Q42=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q43=null;

	/**
	 * @return Returns the Q43.
	 */
	public Integer getQ43() {
		try{
			if (_Q43==null){
				_Q43=getIntegerProperty("Q43");
				return _Q43;
			}else {
				return _Q43;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q43.
	 * @param v Value to Set.
	 */
	public void setQ43(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q43",v);
		_Q43=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q44=null;

	/**
	 * @return Returns the Q44.
	 */
	public Integer getQ44() {
		try{
			if (_Q44==null){
				_Q44=getIntegerProperty("Q44");
				return _Q44;
			}else {
				return _Q44;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q44.
	 * @param v Value to Set.
	 */
	public void setQ44(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q44",v);
		_Q44=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q45=null;

	/**
	 * @return Returns the Q45.
	 */
	public Integer getQ45() {
		try{
			if (_Q45==null){
				_Q45=getIntegerProperty("Q45");
				return _Q45;
			}else {
				return _Q45;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q45.
	 * @param v Value to Set.
	 */
	public void setQ45(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q45",v);
		_Q45=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q46=null;

	/**
	 * @return Returns the Q46.
	 */
	public Integer getQ46() {
		try{
			if (_Q46==null){
				_Q46=getIntegerProperty("Q46");
				return _Q46;
			}else {
				return _Q46;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q46.
	 * @param v Value to Set.
	 */
	public void setQ46(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q46",v);
		_Q46=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q47=null;

	/**
	 * @return Returns the Q47.
	 */
	public Integer getQ47() {
		try{
			if (_Q47==null){
				_Q47=getIntegerProperty("Q47");
				return _Q47;
			}else {
				return _Q47;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q47.
	 * @param v Value to Set.
	 */
	public void setQ47(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q47",v);
		_Q47=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q48=null;

	/**
	 * @return Returns the Q48.
	 */
	public Integer getQ48() {
		try{
			if (_Q48==null){
				_Q48=getIntegerProperty("Q48");
				return _Q48;
			}else {
				return _Q48;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q48.
	 * @param v Value to Set.
	 */
	public void setQ48(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q48",v);
		_Q48=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q49=null;

	/**
	 * @return Returns the Q49.
	 */
	public Integer getQ49() {
		try{
			if (_Q49==null){
				_Q49=getIntegerProperty("Q49");
				return _Q49;
			}else {
				return _Q49;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q49.
	 * @param v Value to Set.
	 */
	public void setQ49(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q49",v);
		_Q49=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q50=null;

	/**
	 * @return Returns the Q50.
	 */
	public Integer getQ50() {
		try{
			if (_Q50==null){
				_Q50=getIntegerProperty("Q50");
				return _Q50;
			}else {
				return _Q50;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q50.
	 * @param v Value to Set.
	 */
	public void setQ50(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q50",v);
		_Q50=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q51=null;

	/**
	 * @return Returns the Q51.
	 */
	public Integer getQ51() {
		try{
			if (_Q51==null){
				_Q51=getIntegerProperty("Q51");
				return _Q51;
			}else {
				return _Q51;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q51.
	 * @param v Value to Set.
	 */
	public void setQ51(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q51",v);
		_Q51=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q52=null;

	/**
	 * @return Returns the Q52.
	 */
	public Integer getQ52() {
		try{
			if (_Q52==null){
				_Q52=getIntegerProperty("Q52");
				return _Q52;
			}else {
				return _Q52;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q52.
	 * @param v Value to Set.
	 */
	public void setQ52(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q52",v);
		_Q52=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q53=null;

	/**
	 * @return Returns the Q53.
	 */
	public Integer getQ53() {
		try{
			if (_Q53==null){
				_Q53=getIntegerProperty("Q53");
				return _Q53;
			}else {
				return _Q53;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q53.
	 * @param v Value to Set.
	 */
	public void setQ53(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q53",v);
		_Q53=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q54=null;

	/**
	 * @return Returns the Q54.
	 */
	public Integer getQ54() {
		try{
			if (_Q54==null){
				_Q54=getIntegerProperty("Q54");
				return _Q54;
			}else {
				return _Q54;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q54.
	 * @param v Value to Set.
	 */
	public void setQ54(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q54",v);
		_Q54=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q55=null;

	/**
	 * @return Returns the Q55.
	 */
	public Integer getQ55() {
		try{
			if (_Q55==null){
				_Q55=getIntegerProperty("Q55");
				return _Q55;
			}else {
				return _Q55;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q55.
	 * @param v Value to Set.
	 */
	public void setQ55(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q55",v);
		_Q55=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q56=null;

	/**
	 * @return Returns the Q56.
	 */
	public Integer getQ56() {
		try{
			if (_Q56==null){
				_Q56=getIntegerProperty("Q56");
				return _Q56;
			}else {
				return _Q56;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q56.
	 * @param v Value to Set.
	 */
	public void setQ56(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q56",v);
		_Q56=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q57=null;

	/**
	 * @return Returns the Q57.
	 */
	public Integer getQ57() {
		try{
			if (_Q57==null){
				_Q57=getIntegerProperty("Q57");
				return _Q57;
			}else {
				return _Q57;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q57.
	 * @param v Value to Set.
	 */
	public void setQ57(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q57",v);
		_Q57=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q58=null;

	/**
	 * @return Returns the Q58.
	 */
	public Integer getQ58() {
		try{
			if (_Q58==null){
				_Q58=getIntegerProperty("Q58");
				return _Q58;
			}else {
				return _Q58;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q58.
	 * @param v Value to Set.
	 */
	public void setQ58(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q58",v);
		_Q58=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q59=null;

	/**
	 * @return Returns the Q59.
	 */
	public Integer getQ59() {
		try{
			if (_Q59==null){
				_Q59=getIntegerProperty("Q59");
				return _Q59;
			}else {
				return _Q59;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q59.
	 * @param v Value to Set.
	 */
	public void setQ59(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q59",v);
		_Q59=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q60=null;

	/**
	 * @return Returns the Q60.
	 */
	public Integer getQ60() {
		try{
			if (_Q60==null){
				_Q60=getIntegerProperty("Q60");
				return _Q60;
			}else {
				return _Q60;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q60.
	 * @param v Value to Set.
	 */
	public void setQ60(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q60",v);
		_Q60=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q61=null;

	/**
	 * @return Returns the Q61.
	 */
	public Integer getQ61() {
		try{
			if (_Q61==null){
				_Q61=getIntegerProperty("Q61");
				return _Q61;
			}else {
				return _Q61;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q61.
	 * @param v Value to Set.
	 */
	public void setQ61(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q61",v);
		_Q61=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q62=null;

	/**
	 * @return Returns the Q62.
	 */
	public Integer getQ62() {
		try{
			if (_Q62==null){
				_Q62=getIntegerProperty("Q62");
				return _Q62;
			}else {
				return _Q62;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q62.
	 * @param v Value to Set.
	 */
	public void setQ62(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q62",v);
		_Q62=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q63=null;

	/**
	 * @return Returns the Q63.
	 */
	public Integer getQ63() {
		try{
			if (_Q63==null){
				_Q63=getIntegerProperty("Q63");
				return _Q63;
			}else {
				return _Q63;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q63.
	 * @param v Value to Set.
	 */
	public void setQ63(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q63",v);
		_Q63=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q64=null;

	/**
	 * @return Returns the Q64.
	 */
	public Integer getQ64() {
		try{
			if (_Q64==null){
				_Q64=getIntegerProperty("Q64");
				return _Q64;
			}else {
				return _Q64;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q64.
	 * @param v Value to Set.
	 */
	public void setQ64(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q64",v);
		_Q64=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q65=null;

	/**
	 * @return Returns the Q65.
	 */
	public Integer getQ65() {
		try{
			if (_Q65==null){
				_Q65=getIntegerProperty("Q65");
				return _Q65;
			}else {
				return _Q65;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q65.
	 * @param v Value to Set.
	 */
	public void setQ65(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q65",v);
		_Q65=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q66=null;

	/**
	 * @return Returns the Q66.
	 */
	public Integer getQ66() {
		try{
			if (_Q66==null){
				_Q66=getIntegerProperty("Q66");
				return _Q66;
			}else {
				return _Q66;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q66.
	 * @param v Value to Set.
	 */
	public void setQ66(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q66",v);
		_Q66=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q67=null;

	/**
	 * @return Returns the Q67.
	 */
	public Integer getQ67() {
		try{
			if (_Q67==null){
				_Q67=getIntegerProperty("Q67");
				return _Q67;
			}else {
				return _Q67;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q67.
	 * @param v Value to Set.
	 */
	public void setQ67(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q67",v);
		_Q67=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q68=null;

	/**
	 * @return Returns the Q68.
	 */
	public Integer getQ68() {
		try{
			if (_Q68==null){
				_Q68=getIntegerProperty("Q68");
				return _Q68;
			}else {
				return _Q68;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q68.
	 * @param v Value to Set.
	 */
	public void setQ68(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q68",v);
		_Q68=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q69=null;

	/**
	 * @return Returns the Q69.
	 */
	public Integer getQ69() {
		try{
			if (_Q69==null){
				_Q69=getIntegerProperty("Q69");
				return _Q69;
			}else {
				return _Q69;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q69.
	 * @param v Value to Set.
	 */
	public void setQ69(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q69",v);
		_Q69=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q70=null;

	/**
	 * @return Returns the Q70.
	 */
	public Integer getQ70() {
		try{
			if (_Q70==null){
				_Q70=getIntegerProperty("Q70");
				return _Q70;
			}else {
				return _Q70;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q70.
	 * @param v Value to Set.
	 */
	public void setQ70(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q70",v);
		_Q70=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q71=null;

	/**
	 * @return Returns the Q71.
	 */
	public Integer getQ71() {
		try{
			if (_Q71==null){
				_Q71=getIntegerProperty("Q71");
				return _Q71;
			}else {
				return _Q71;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q71.
	 * @param v Value to Set.
	 */
	public void setQ71(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q71",v);
		_Q71=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q72=null;

	/**
	 * @return Returns the Q72.
	 */
	public Integer getQ72() {
		try{
			if (_Q72==null){
				_Q72=getIntegerProperty("Q72");
				return _Q72;
			}else {
				return _Q72;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q72.
	 * @param v Value to Set.
	 */
	public void setQ72(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q72",v);
		_Q72=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q73=null;

	/**
	 * @return Returns the Q73.
	 */
	public Integer getQ73() {
		try{
			if (_Q73==null){
				_Q73=getIntegerProperty("Q73");
				return _Q73;
			}else {
				return _Q73;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q73.
	 * @param v Value to Set.
	 */
	public void setQ73(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q73",v);
		_Q73=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q74=null;

	/**
	 * @return Returns the Q74.
	 */
	public Integer getQ74() {
		try{
			if (_Q74==null){
				_Q74=getIntegerProperty("Q74");
				return _Q74;
			}else {
				return _Q74;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q74.
	 * @param v Value to Set.
	 */
	public void setQ74(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q74",v);
		_Q74=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q75=null;

	/**
	 * @return Returns the Q75.
	 */
	public Integer getQ75() {
		try{
			if (_Q75==null){
				_Q75=getIntegerProperty("Q75");
				return _Q75;
			}else {
				return _Q75;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q75.
	 * @param v Value to Set.
	 */
	public void setQ75(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q75",v);
		_Q75=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q76=null;

	/**
	 * @return Returns the Q76.
	 */
	public Integer getQ76() {
		try{
			if (_Q76==null){
				_Q76=getIntegerProperty("Q76");
				return _Q76;
			}else {
				return _Q76;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q76.
	 * @param v Value to Set.
	 */
	public void setQ76(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q76",v);
		_Q76=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q77=null;

	/**
	 * @return Returns the Q77.
	 */
	public Integer getQ77() {
		try{
			if (_Q77==null){
				_Q77=getIntegerProperty("Q77");
				return _Q77;
			}else {
				return _Q77;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q77.
	 * @param v Value to Set.
	 */
	public void setQ77(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q77",v);
		_Q77=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q78=null;

	/**
	 * @return Returns the Q78.
	 */
	public Integer getQ78() {
		try{
			if (_Q78==null){
				_Q78=getIntegerProperty("Q78");
				return _Q78;
			}else {
				return _Q78;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q78.
	 * @param v Value to Set.
	 */
	public void setQ78(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q78",v);
		_Q78=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q79=null;

	/**
	 * @return Returns the Q79.
	 */
	public Integer getQ79() {
		try{
			if (_Q79==null){
				_Q79=getIntegerProperty("Q79");
				return _Q79;
			}else {
				return _Q79;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q79.
	 * @param v Value to Set.
	 */
	public void setQ79(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q79",v);
		_Q79=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q80=null;

	/**
	 * @return Returns the Q80.
	 */
	public Integer getQ80() {
		try{
			if (_Q80==null){
				_Q80=getIntegerProperty("Q80");
				return _Q80;
			}else {
				return _Q80;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q80.
	 * @param v Value to Set.
	 */
	public void setQ80(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q80",v);
		_Q80=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q81=null;

	/**
	 * @return Returns the Q81.
	 */
	public Integer getQ81() {
		try{
			if (_Q81==null){
				_Q81=getIntegerProperty("Q81");
				return _Q81;
			}else {
				return _Q81;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q81.
	 * @param v Value to Set.
	 */
	public void setQ81(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q81",v);
		_Q81=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q82=null;

	/**
	 * @return Returns the Q82.
	 */
	public Integer getQ82() {
		try{
			if (_Q82==null){
				_Q82=getIntegerProperty("Q82");
				return _Q82;
			}else {
				return _Q82;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q82.
	 * @param v Value to Set.
	 */
	public void setQ82(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q82",v);
		_Q82=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q83=null;

	/**
	 * @return Returns the Q83.
	 */
	public Integer getQ83() {
		try{
			if (_Q83==null){
				_Q83=getIntegerProperty("Q83");
				return _Q83;
			}else {
				return _Q83;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q83.
	 * @param v Value to Set.
	 */
	public void setQ83(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q83",v);
		_Q83=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q84=null;

	/**
	 * @return Returns the Q84.
	 */
	public Integer getQ84() {
		try{
			if (_Q84==null){
				_Q84=getIntegerProperty("Q84");
				return _Q84;
			}else {
				return _Q84;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q84.
	 * @param v Value to Set.
	 */
	public void setQ84(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q84",v);
		_Q84=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q85=null;

	/**
	 * @return Returns the Q85.
	 */
	public Integer getQ85() {
		try{
			if (_Q85==null){
				_Q85=getIntegerProperty("Q85");
				return _Q85;
			}else {
				return _Q85;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q85.
	 * @param v Value to Set.
	 */
	public void setQ85(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q85",v);
		_Q85=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q86=null;

	/**
	 * @return Returns the Q86.
	 */
	public Integer getQ86() {
		try{
			if (_Q86==null){
				_Q86=getIntegerProperty("Q86");
				return _Q86;
			}else {
				return _Q86;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q86.
	 * @param v Value to Set.
	 */
	public void setQ86(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q86",v);
		_Q86=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q87=null;

	/**
	 * @return Returns the Q87.
	 */
	public Integer getQ87() {
		try{
			if (_Q87==null){
				_Q87=getIntegerProperty("Q87");
				return _Q87;
			}else {
				return _Q87;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q87.
	 * @param v Value to Set.
	 */
	public void setQ87(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q87",v);
		_Q87=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q88=null;

	/**
	 * @return Returns the Q88.
	 */
	public Integer getQ88() {
		try{
			if (_Q88==null){
				_Q88=getIntegerProperty("Q88");
				return _Q88;
			}else {
				return _Q88;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q88.
	 * @param v Value to Set.
	 */
	public void setQ88(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q88",v);
		_Q88=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q89=null;

	/**
	 * @return Returns the Q89.
	 */
	public Integer getQ89() {
		try{
			if (_Q89==null){
				_Q89=getIntegerProperty("Q89");
				return _Q89;
			}else {
				return _Q89;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q89.
	 * @param v Value to Set.
	 */
	public void setQ89(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q89",v);
		_Q89=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q90=null;

	/**
	 * @return Returns the Q90.
	 */
	public Integer getQ90() {
		try{
			if (_Q90==null){
				_Q90=getIntegerProperty("Q90");
				return _Q90;
			}else {
				return _Q90;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q90.
	 * @param v Value to Set.
	 */
	public void setQ90(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q90",v);
		_Q90=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q91=null;

	/**
	 * @return Returns the Q91.
	 */
	public Integer getQ91() {
		try{
			if (_Q91==null){
				_Q91=getIntegerProperty("Q91");
				return _Q91;
			}else {
				return _Q91;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q91.
	 * @param v Value to Set.
	 */
	public void setQ91(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q91",v);
		_Q91=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q92=null;

	/**
	 * @return Returns the Q92.
	 */
	public Integer getQ92() {
		try{
			if (_Q92==null){
				_Q92=getIntegerProperty("Q92");
				return _Q92;
			}else {
				return _Q92;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q92.
	 * @param v Value to Set.
	 */
	public void setQ92(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q92",v);
		_Q92=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q93=null;

	/**
	 * @return Returns the Q93.
	 */
	public Integer getQ93() {
		try{
			if (_Q93==null){
				_Q93=getIntegerProperty("Q93");
				return _Q93;
			}else {
				return _Q93;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q93.
	 * @param v Value to Set.
	 */
	public void setQ93(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q93",v);
		_Q93=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q94=null;

	/**
	 * @return Returns the Q94.
	 */
	public Integer getQ94() {
		try{
			if (_Q94==null){
				_Q94=getIntegerProperty("Q94");
				return _Q94;
			}else {
				return _Q94;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q94.
	 * @param v Value to Set.
	 */
	public void setQ94(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q94",v);
		_Q94=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q95=null;

	/**
	 * @return Returns the Q95.
	 */
	public Integer getQ95() {
		try{
			if (_Q95==null){
				_Q95=getIntegerProperty("Q95");
				return _Q95;
			}else {
				return _Q95;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q95.
	 * @param v Value to Set.
	 */
	public void setQ95(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q95",v);
		_Q95=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q96=null;

	/**
	 * @return Returns the Q96.
	 */
	public Integer getQ96() {
		try{
			if (_Q96==null){
				_Q96=getIntegerProperty("Q96");
				return _Q96;
			}else {
				return _Q96;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q96.
	 * @param v Value to Set.
	 */
	public void setQ96(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q96",v);
		_Q96=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q97=null;

	/**
	 * @return Returns the Q97.
	 */
	public Integer getQ97() {
		try{
			if (_Q97==null){
				_Q97=getIntegerProperty("Q97");
				return _Q97;
			}else {
				return _Q97;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q97.
	 * @param v Value to Set.
	 */
	public void setQ97(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q97",v);
		_Q97=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q98=null;

	/**
	 * @return Returns the Q98.
	 */
	public Integer getQ98() {
		try{
			if (_Q98==null){
				_Q98=getIntegerProperty("Q98");
				return _Q98;
			}else {
				return _Q98;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q98.
	 * @param v Value to Set.
	 */
	public void setQ98(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q98",v);
		_Q98=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q99=null;

	/**
	 * @return Returns the Q99.
	 */
	public Integer getQ99() {
		try{
			if (_Q99==null){
				_Q99=getIntegerProperty("Q99");
				return _Q99;
			}else {
				return _Q99;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q99.
	 * @param v Value to Set.
	 */
	public void setQ99(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q99",v);
		_Q99=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q100=null;

	/**
	 * @return Returns the Q100.
	 */
	public Integer getQ100() {
		try{
			if (_Q100==null){
				_Q100=getIntegerProperty("Q100");
				return _Q100;
			}else {
				return _Q100;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q100.
	 * @param v Value to Set.
	 */
	public void setQ100(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q100",v);
		_Q100=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q101=null;

	/**
	 * @return Returns the Q101.
	 */
	public Integer getQ101() {
		try{
			if (_Q101==null){
				_Q101=getIntegerProperty("Q101");
				return _Q101;
			}else {
				return _Q101;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q101.
	 * @param v Value to Set.
	 */
	public void setQ101(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q101",v);
		_Q101=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q102=null;

	/**
	 * @return Returns the Q102.
	 */
	public Integer getQ102() {
		try{
			if (_Q102==null){
				_Q102=getIntegerProperty("Q102");
				return _Q102;
			}else {
				return _Q102;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q102.
	 * @param v Value to Set.
	 */
	public void setQ102(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q102",v);
		_Q102=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q103=null;

	/**
	 * @return Returns the Q103.
	 */
	public Integer getQ103() {
		try{
			if (_Q103==null){
				_Q103=getIntegerProperty("Q103");
				return _Q103;
			}else {
				return _Q103;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q103.
	 * @param v Value to Set.
	 */
	public void setQ103(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q103",v);
		_Q103=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q104=null;

	/**
	 * @return Returns the Q104.
	 */
	public Integer getQ104() {
		try{
			if (_Q104==null){
				_Q104=getIntegerProperty("Q104");
				return _Q104;
			}else {
				return _Q104;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q104.
	 * @param v Value to Set.
	 */
	public void setQ104(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q104",v);
		_Q104=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q105=null;

	/**
	 * @return Returns the Q105.
	 */
	public Integer getQ105() {
		try{
			if (_Q105==null){
				_Q105=getIntegerProperty("Q105");
				return _Q105;
			}else {
				return _Q105;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q105.
	 * @param v Value to Set.
	 */
	public void setQ105(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q105",v);
		_Q105=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q106=null;

	/**
	 * @return Returns the Q106.
	 */
	public Integer getQ106() {
		try{
			if (_Q106==null){
				_Q106=getIntegerProperty("Q106");
				return _Q106;
			}else {
				return _Q106;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q106.
	 * @param v Value to Set.
	 */
	public void setQ106(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q106",v);
		_Q106=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q107=null;

	/**
	 * @return Returns the Q107.
	 */
	public Integer getQ107() {
		try{
			if (_Q107==null){
				_Q107=getIntegerProperty("Q107");
				return _Q107;
			}else {
				return _Q107;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q107.
	 * @param v Value to Set.
	 */
	public void setQ107(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q107",v);
		_Q107=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q108=null;

	/**
	 * @return Returns the Q108.
	 */
	public Integer getQ108() {
		try{
			if (_Q108==null){
				_Q108=getIntegerProperty("Q108");
				return _Q108;
			}else {
				return _Q108;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q108.
	 * @param v Value to Set.
	 */
	public void setQ108(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q108",v);
		_Q108=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q109=null;

	/**
	 * @return Returns the Q109.
	 */
	public Integer getQ109() {
		try{
			if (_Q109==null){
				_Q109=getIntegerProperty("Q109");
				return _Q109;
			}else {
				return _Q109;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q109.
	 * @param v Value to Set.
	 */
	public void setQ109(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q109",v);
		_Q109=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q110=null;

	/**
	 * @return Returns the Q110.
	 */
	public Integer getQ110() {
		try{
			if (_Q110==null){
				_Q110=getIntegerProperty("Q110");
				return _Q110;
			}else {
				return _Q110;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q110.
	 * @param v Value to Set.
	 */
	public void setQ110(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q110",v);
		_Q110=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q111=null;

	/**
	 * @return Returns the Q111.
	 */
	public Integer getQ111() {
		try{
			if (_Q111==null){
				_Q111=getIntegerProperty("Q111");
				return _Q111;
			}else {
				return _Q111;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q111.
	 * @param v Value to Set.
	 */
	public void setQ111(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q111",v);
		_Q111=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q112=null;

	/**
	 * @return Returns the Q112.
	 */
	public Integer getQ112() {
		try{
			if (_Q112==null){
				_Q112=getIntegerProperty("Q112");
				return _Q112;
			}else {
				return _Q112;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q112.
	 * @param v Value to Set.
	 */
	public void setQ112(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q112",v);
		_Q112=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q113=null;

	/**
	 * @return Returns the Q113.
	 */
	public Integer getQ113() {
		try{
			if (_Q113==null){
				_Q113=getIntegerProperty("Q113");
				return _Q113;
			}else {
				return _Q113;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q113.
	 * @param v Value to Set.
	 */
	public void setQ113(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q113",v);
		_Q113=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q114=null;

	/**
	 * @return Returns the Q114.
	 */
	public Integer getQ114() {
		try{
			if (_Q114==null){
				_Q114=getIntegerProperty("Q114");
				return _Q114;
			}else {
				return _Q114;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q114.
	 * @param v Value to Set.
	 */
	public void setQ114(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q114",v);
		_Q114=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q115=null;

	/**
	 * @return Returns the Q115.
	 */
	public Integer getQ115() {
		try{
			if (_Q115==null){
				_Q115=getIntegerProperty("Q115");
				return _Q115;
			}else {
				return _Q115;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q115.
	 * @param v Value to Set.
	 */
	public void setQ115(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q115",v);
		_Q115=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q116=null;

	/**
	 * @return Returns the Q116.
	 */
	public Integer getQ116() {
		try{
			if (_Q116==null){
				_Q116=getIntegerProperty("Q116");
				return _Q116;
			}else {
				return _Q116;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q116.
	 * @param v Value to Set.
	 */
	public void setQ116(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q116",v);
		_Q116=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q117=null;

	/**
	 * @return Returns the Q117.
	 */
	public Integer getQ117() {
		try{
			if (_Q117==null){
				_Q117=getIntegerProperty("Q117");
				return _Q117;
			}else {
				return _Q117;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q117.
	 * @param v Value to Set.
	 */
	public void setQ117(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q117",v);
		_Q117=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q118=null;

	/**
	 * @return Returns the Q118.
	 */
	public Integer getQ118() {
		try{
			if (_Q118==null){
				_Q118=getIntegerProperty("Q118");
				return _Q118;
			}else {
				return _Q118;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q118.
	 * @param v Value to Set.
	 */
	public void setQ118(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q118",v);
		_Q118=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q119=null;

	/**
	 * @return Returns the Q119.
	 */
	public Integer getQ119() {
		try{
			if (_Q119==null){
				_Q119=getIntegerProperty("Q119");
				return _Q119;
			}else {
				return _Q119;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q119.
	 * @param v Value to Set.
	 */
	public void setQ119(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q119",v);
		_Q119=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q120=null;

	/**
	 * @return Returns the Q120.
	 */
	public Integer getQ120() {
		try{
			if (_Q120==null){
				_Q120=getIntegerProperty("Q120");
				return _Q120;
			}else {
				return _Q120;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q120.
	 * @param v Value to Set.
	 */
	public void setQ120(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q120",v);
		_Q120=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.IpipIpipptdata> getAllIpipIpipptdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.IpipIpipptdata> al = new ArrayList<org.nrg.xdat.om.IpipIpipptdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.IpipIpipptdata> getIpipIpipptdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.IpipIpipptdata> al = new ArrayList<org.nrg.xdat.om.IpipIpipptdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.IpipIpipptdata> getIpipIpipptdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.IpipIpipptdata> al = new ArrayList<org.nrg.xdat.om.IpipIpipptdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static IpipIpipptdata getIpipIpipptdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("ipip:ipipptData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (IpipIpipptdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
