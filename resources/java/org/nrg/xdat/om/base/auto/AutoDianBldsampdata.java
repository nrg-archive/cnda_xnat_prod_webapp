/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:00 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianBldsampdata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianBldsampdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianBldsampdata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:bldsampData";

	public AutoDianBldsampdata(ItemI item)
	{
		super(item);
	}

	public AutoDianBldsampdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianBldsampdata(UserI user)
	 **/
	public AutoDianBldsampdata(){}

	public AutoDianBldsampdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:bldsampData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Gencoll=null;

	/**
	 * @return Returns the GENCOLL.
	 */
	public Integer getGencoll() {
		try{
			if (_Gencoll==null){
				_Gencoll=getIntegerProperty("GENCOLL");
				return _Gencoll;
			}else {
				return _Gencoll;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GENCOLL.
	 * @param v Value to Set.
	 */
	public void setGencoll(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GENCOLL",v);
		_Gencoll=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Gendate=null;

	/**
	 * @return Returns the GENDATE.
	 */
	public Object getGendate(){
		try{
			if (_Gendate==null){
				_Gendate=getProperty("GENDATE");
				return _Gendate;
			}else {
				return _Gendate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GENDATE.
	 * @param v Value to Set.
	 */
	public void setGendate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GENDATE",v);
		_Gendate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Gentime=null;

	/**
	 * @return Returns the GENTIME.
	 */
	public Integer getGentime() {
		try{
			if (_Gentime==null){
				_Gentime=getIntegerProperty("GENTIME");
				return _Gentime;
			}else {
				return _Gentime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GENTIME.
	 * @param v Value to Set.
	 */
	public void setGentime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GENTIME",v);
		_Gentime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Genship=null;

	/**
	 * @return Returns the GENSHIP.
	 */
	public Integer getGenship() {
		try{
			if (_Genship==null){
				_Genship=getIntegerProperty("GENSHIP");
				return _Genship;
			}else {
				return _Genship;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GENSHIP.
	 * @param v Value to Set.
	 */
	public void setGenship(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GENSHIP",v);
		_Genship=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Genshdt=null;

	/**
	 * @return Returns the GENSHDT.
	 */
	public Object getGenshdt(){
		try{
			if (_Genshdt==null){
				_Genshdt=getProperty("GENSHDT");
				return _Genshdt;
			}else {
				return _Genshdt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GENSHDT.
	 * @param v Value to Set.
	 */
	public void setGenshdt(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GENSHDT",v);
		_Genshdt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Genshnum=null;

	/**
	 * @return Returns the GENSHNUM.
	 */
	public String getGenshnum(){
		try{
			if (_Genshnum==null){
				_Genshnum=getStringProperty("GENSHNUM");
				return _Genshnum;
			}else {
				return _Genshnum;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GENSHNUM.
	 * @param v Value to Set.
	 */
	public void setGenshnum(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GENSHNUM",v);
		_Genshnum=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Gendatervd=null;

	/**
	 * @return Returns the GENDATERVD.
	 */
	public Object getGendatervd(){
		try{
			if (_Gendatervd==null){
				_Gendatervd=getProperty("GENDATERVD");
				return _Gendatervd;
			}else {
				return _Gendatervd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GENDATERVD.
	 * @param v Value to Set.
	 */
	public void setGendatervd(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GENDATERVD",v);
		_Gendatervd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Genvolrvd=null;

	/**
	 * @return Returns the GENVOLRVD.
	 */
	public Double getGenvolrvd() {
		try{
			if (_Genvolrvd==null){
				_Genvolrvd=getDoubleProperty("GENVOLRVD");
				return _Genvolrvd;
			}else {
				return _Genvolrvd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GENVOLRVD.
	 * @param v Value to Set.
	 */
	public void setGenvolrvd(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GENVOLRVD",v);
		_Genvolrvd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Genvialsrvd=null;

	/**
	 * @return Returns the GENVIALSRVD.
	 */
	public Integer getGenvialsrvd() {
		try{
			if (_Genvialsrvd==null){
				_Genvialsrvd=getIntegerProperty("GENVIALSRVD");
				return _Genvialsrvd;
			}else {
				return _Genvialsrvd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GENVIALSRVD.
	 * @param v Value to Set.
	 */
	public void setGenvialsrvd(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GENVIALSRVD",v);
		_Genvialsrvd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Immcoll=null;

	/**
	 * @return Returns the IMMCOLL.
	 */
	public Integer getImmcoll() {
		try{
			if (_Immcoll==null){
				_Immcoll=getIntegerProperty("IMMCOLL");
				return _Immcoll;
			}else {
				return _Immcoll;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for IMMCOLL.
	 * @param v Value to Set.
	 */
	public void setImmcoll(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/IMMCOLL",v);
		_Immcoll=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Immdate=null;

	/**
	 * @return Returns the IMMDATE.
	 */
	public Object getImmdate(){
		try{
			if (_Immdate==null){
				_Immdate=getProperty("IMMDATE");
				return _Immdate;
			}else {
				return _Immdate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for IMMDATE.
	 * @param v Value to Set.
	 */
	public void setImmdate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/IMMDATE",v);
		_Immdate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Immtime=null;

	/**
	 * @return Returns the IMMTIME.
	 */
	public Integer getImmtime() {
		try{
			if (_Immtime==null){
				_Immtime=getIntegerProperty("IMMTIME");
				return _Immtime;
			}else {
				return _Immtime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for IMMTIME.
	 * @param v Value to Set.
	 */
	public void setImmtime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/IMMTIME",v);
		_Immtime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Immship=null;

	/**
	 * @return Returns the IMMSHIP.
	 */
	public Integer getImmship() {
		try{
			if (_Immship==null){
				_Immship=getIntegerProperty("IMMSHIP");
				return _Immship;
			}else {
				return _Immship;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for IMMSHIP.
	 * @param v Value to Set.
	 */
	public void setImmship(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/IMMSHIP",v);
		_Immship=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Immshdt=null;

	/**
	 * @return Returns the IMMSHDT.
	 */
	public Object getImmshdt(){
		try{
			if (_Immshdt==null){
				_Immshdt=getProperty("IMMSHDT");
				return _Immshdt;
			}else {
				return _Immshdt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for IMMSHDT.
	 * @param v Value to Set.
	 */
	public void setImmshdt(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/IMMSHDT",v);
		_Immshdt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Immshnum=null;

	/**
	 * @return Returns the IMMSHNUM.
	 */
	public String getImmshnum(){
		try{
			if (_Immshnum==null){
				_Immshnum=getStringProperty("IMMSHNUM");
				return _Immshnum;
			}else {
				return _Immshnum;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for IMMSHNUM.
	 * @param v Value to Set.
	 */
	public void setImmshnum(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/IMMSHNUM",v);
		_Immshnum=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Immdatervd=null;

	/**
	 * @return Returns the IMMDATERVD.
	 */
	public Object getImmdatervd(){
		try{
			if (_Immdatervd==null){
				_Immdatervd=getProperty("IMMDATERVD");
				return _Immdatervd;
			}else {
				return _Immdatervd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for IMMDATERVD.
	 * @param v Value to Set.
	 */
	public void setImmdatervd(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/IMMDATERVD",v);
		_Immdatervd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Immvolrvd=null;

	/**
	 * @return Returns the IMMVOLRVD.
	 */
	public Double getImmvolrvd() {
		try{
			if (_Immvolrvd==null){
				_Immvolrvd=getDoubleProperty("IMMVOLRVD");
				return _Immvolrvd;
			}else {
				return _Immvolrvd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for IMMVOLRVD.
	 * @param v Value to Set.
	 */
	public void setImmvolrvd(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/IMMVOLRVD",v);
		_Immvolrvd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Immvialsrvd=null;

	/**
	 * @return Returns the IMMVIALSRVD.
	 */
	public Integer getImmvialsrvd() {
		try{
			if (_Immvialsrvd==null){
				_Immvialsrvd=getIntegerProperty("IMMVIALSRVD");
				return _Immvialsrvd;
			}else {
				return _Immvialsrvd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for IMMVIALSRVD.
	 * @param v Value to Set.
	 */
	public void setImmvialsrvd(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/IMMVIALSRVD",v);
		_Immvialsrvd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Biocoll=null;

	/**
	 * @return Returns the BIOCOLL.
	 */
	public Integer getBiocoll() {
		try{
			if (_Biocoll==null){
				_Biocoll=getIntegerProperty("BIOCOLL");
				return _Biocoll;
			}else {
				return _Biocoll;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BIOCOLL.
	 * @param v Value to Set.
	 */
	public void setBiocoll(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BIOCOLL",v);
		_Biocoll=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Biodate=null;

	/**
	 * @return Returns the BIODATE.
	 */
	public Object getBiodate(){
		try{
			if (_Biodate==null){
				_Biodate=getProperty("BIODATE");
				return _Biodate;
			}else {
				return _Biodate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BIODATE.
	 * @param v Value to Set.
	 */
	public void setBiodate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BIODATE",v);
		_Biodate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Biotime=null;

	/**
	 * @return Returns the BIOTIME.
	 */
	public Integer getBiotime() {
		try{
			if (_Biotime==null){
				_Biotime=getIntegerProperty("BIOTIME");
				return _Biotime;
			}else {
				return _Biotime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BIOTIME.
	 * @param v Value to Set.
	 */
	public void setBiotime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BIOTIME",v);
		_Biotime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Biofast=null;

	/**
	 * @return Returns the BIOFAST.
	 */
	public Integer getBiofast() {
		try{
			if (_Biofast==null){
				_Biofast=getIntegerProperty("BIOFAST");
				return _Biofast;
			}else {
				return _Biofast;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BIOFAST.
	 * @param v Value to Set.
	 */
	public void setBiofast(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BIOFAST",v);
		_Biofast=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Plasvol=null;

	/**
	 * @return Returns the PLASVOL.
	 */
	public Integer getPlasvol() {
		try{
			if (_Plasvol==null){
				_Plasvol=getIntegerProperty("PLASVOL");
				return _Plasvol;
			}else {
				return _Plasvol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PLASVOL.
	 * @param v Value to Set.
	 */
	public void setPlasvol(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PLASVOL",v);
		_Plasvol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Plasvolrvd=null;

	/**
	 * @return Returns the PLASVOLRVD.
	 */
	public Double getPlasvolrvd() {
		try{
			if (_Plasvolrvd==null){
				_Plasvolrvd=getDoubleProperty("PLASVOLRVD");
				return _Plasvolrvd;
			}else {
				return _Plasvolrvd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PLASVOLRVD.
	 * @param v Value to Set.
	 */
	public void setPlasvolrvd(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PLASVOLRVD",v);
		_Plasvolrvd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Plasvialsrvd=null;

	/**
	 * @return Returns the PLASVIALSRVD.
	 */
	public Integer getPlasvialsrvd() {
		try{
			if (_Plasvialsrvd==null){
				_Plasvialsrvd=getIntegerProperty("PLASVIALSRVD");
				return _Plasvialsrvd;
			}else {
				return _Plasvialsrvd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PLASVIALSRVD.
	 * @param v Value to Set.
	 */
	public void setPlasvialsrvd(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PLASVIALSRVD",v);
		_Plasvialsrvd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Plasfroz=null;

	/**
	 * @return Returns the PLASFROZ.
	 */
	public Integer getPlasfroz() {
		try{
			if (_Plasfroz==null){
				_Plasfroz=getIntegerProperty("PLASFROZ");
				return _Plasfroz;
			}else {
				return _Plasfroz;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PLASFROZ.
	 * @param v Value to Set.
	 */
	public void setPlasfroz(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PLASFROZ",v);
		_Plasfroz=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Servol=null;

	/**
	 * @return Returns the SERVOL.
	 */
	public Integer getServol() {
		try{
			if (_Servol==null){
				_Servol=getIntegerProperty("SERVOL");
				return _Servol;
			}else {
				return _Servol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SERVOL.
	 * @param v Value to Set.
	 */
	public void setServol(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SERVOL",v);
		_Servol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Servolrvd=null;

	/**
	 * @return Returns the SERVOLRVD.
	 */
	public Double getServolrvd() {
		try{
			if (_Servolrvd==null){
				_Servolrvd=getDoubleProperty("SERVOLRVD");
				return _Servolrvd;
			}else {
				return _Servolrvd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SERVOLRVD.
	 * @param v Value to Set.
	 */
	public void setServolrvd(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SERVOLRVD",v);
		_Servolrvd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Servialsrvd=null;

	/**
	 * @return Returns the SERVIALSRVD.
	 */
	public Integer getServialsrvd() {
		try{
			if (_Servialsrvd==null){
				_Servialsrvd=getIntegerProperty("SERVIALSRVD");
				return _Servialsrvd;
			}else {
				return _Servialsrvd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SERVIALSRVD.
	 * @param v Value to Set.
	 */
	public void setServialsrvd(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SERVIALSRVD",v);
		_Servialsrvd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Serfroz=null;

	/**
	 * @return Returns the SERFROZ.
	 */
	public Integer getSerfroz() {
		try{
			if (_Serfroz==null){
				_Serfroz=getIntegerProperty("SERFROZ");
				return _Serfroz;
			}else {
				return _Serfroz;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SERFROZ.
	 * @param v Value to Set.
	 */
	public void setSerfroz(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SERFROZ",v);
		_Serfroz=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Bioship=null;

	/**
	 * @return Returns the BIOSHIP.
	 */
	public Integer getBioship() {
		try{
			if (_Bioship==null){
				_Bioship=getIntegerProperty("BIOSHIP");
				return _Bioship;
			}else {
				return _Bioship;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BIOSHIP.
	 * @param v Value to Set.
	 */
	public void setBioship(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BIOSHIP",v);
		_Bioship=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Bioshdt=null;

	/**
	 * @return Returns the BIOSHDT.
	 */
	public Object getBioshdt(){
		try{
			if (_Bioshdt==null){
				_Bioshdt=getProperty("BIOSHDT");
				return _Bioshdt;
			}else {
				return _Bioshdt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BIOSHDT.
	 * @param v Value to Set.
	 */
	public void setBioshdt(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BIOSHDT",v);
		_Bioshdt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Bioshnum=null;

	/**
	 * @return Returns the BIOSHNUM.
	 */
	public String getBioshnum(){
		try{
			if (_Bioshnum==null){
				_Bioshnum=getStringProperty("BIOSHNUM");
				return _Bioshnum;
			}else {
				return _Bioshnum;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BIOSHNUM.
	 * @param v Value to Set.
	 */
	public void setBioshnum(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BIOSHNUM",v);
		_Bioshnum=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Biodatervd=null;

	/**
	 * @return Returns the BIODATERVD.
	 */
	public Object getBiodatervd(){
		try{
			if (_Biodatervd==null){
				_Biodatervd=getProperty("BIODATERVD");
				return _Biodatervd;
			}else {
				return _Biodatervd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BIODATERVD.
	 * @param v Value to Set.
	 */
	public void setBiodatervd(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BIODATERVD",v);
		_Biodatervd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Bufvol=null;

	/**
	 * @return Returns the BUFVOL.
	 */
	public Integer getBufvol() {
		try{
			if (_Bufvol==null){
				_Bufvol=getIntegerProperty("BUFVOL");
				return _Bufvol;
			}else {
				return _Bufvol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BUFVOL.
	 * @param v Value to Set.
	 */
	public void setBufvol(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BUFVOL",v);
		_Bufvol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Buffroz=null;

	/**
	 * @return Returns the BUFFROZ.
	 */
	public Integer getBuffroz() {
		try{
			if (_Buffroz==null){
				_Buffroz=getIntegerProperty("BUFFROZ");
				return _Buffroz;
			}else {
				return _Buffroz;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BUFFROZ.
	 * @param v Value to Set.
	 */
	public void setBuffroz(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BUFFROZ",v);
		_Buffroz=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rnapax=null;

	/**
	 * @return Returns the RNAPAX.
	 */
	public Integer getRnapax() {
		try{
			if (_Rnapax==null){
				_Rnapax=getIntegerProperty("RNAPAX");
				return _Rnapax;
			}else {
				return _Rnapax;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RNAPAX.
	 * @param v Value to Set.
	 */
	public void setRnapax(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RNAPAX",v);
		_Rnapax=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rnadisc=null;

	/**
	 * @return Returns the RNADISC.
	 */
	public Integer getRnadisc() {
		try{
			if (_Rnadisc==null){
				_Rnadisc=getIntegerProperty("RNADISC");
				return _Rnadisc;
			}else {
				return _Rnadisc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RNADISC.
	 * @param v Value to Set.
	 */
	public void setRnadisc(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RNADISC",v);
		_Rnadisc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rnaship=null;

	/**
	 * @return Returns the RNASHIP.
	 */
	public Integer getRnaship() {
		try{
			if (_Rnaship==null){
				_Rnaship=getIntegerProperty("RNASHIP");
				return _Rnaship;
			}else {
				return _Rnaship;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RNASHIP.
	 * @param v Value to Set.
	 */
	public void setRnaship(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RNASHIP",v);
		_Rnaship=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Rnashdt=null;

	/**
	 * @return Returns the RNASHDT.
	 */
	public Object getRnashdt(){
		try{
			if (_Rnashdt==null){
				_Rnashdt=getProperty("RNASHDT");
				return _Rnashdt;
			}else {
				return _Rnashdt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RNASHDT.
	 * @param v Value to Set.
	 */
	public void setRnashdt(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RNASHDT",v);
		_Rnashdt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rnashnum=null;

	/**
	 * @return Returns the RNASHNUM.
	 */
	public String getRnashnum(){
		try{
			if (_Rnashnum==null){
				_Rnashnum=getStringProperty("RNASHNUM");
				return _Rnashnum;
			}else {
				return _Rnashnum;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RNASHNUM.
	 * @param v Value to Set.
	 */
	public void setRnashnum(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RNASHNUM",v);
		_Rnashnum=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rnacoll=null;

	/**
	 * @return Returns the RNACOLL.
	 */
	public Integer getRnacoll() {
		try{
			if (_Rnacoll==null){
				_Rnacoll=getIntegerProperty("RNACOLL");
				return _Rnacoll;
			}else {
				return _Rnacoll;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RNACOLL.
	 * @param v Value to Set.
	 */
	public void setRnacoll(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RNACOLL",v);
		_Rnacoll=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianBldsampdata> getAllDianBldsampdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianBldsampdata> al = new ArrayList<org.nrg.xdat.om.DianBldsampdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianBldsampdata> getDianBldsampdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianBldsampdata> al = new ArrayList<org.nrg.xdat.om.DianBldsampdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianBldsampdata> getDianBldsampdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianBldsampdata> al = new ArrayList<org.nrg.xdat.om.DianBldsampdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianBldsampdata getDianBldsampdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:bldsampData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianBldsampdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
