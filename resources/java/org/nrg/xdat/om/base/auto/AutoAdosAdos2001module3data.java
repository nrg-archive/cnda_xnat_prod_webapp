/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoAdosAdos2001module3data extends XnatSubjectassessordata implements org.nrg.xdat.model.AdosAdos2001module3dataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoAdosAdos2001module3data.class);
	public static String SCHEMA_ELEMENT_NAME="ados:ados2001Module3Data";

	public AutoAdosAdos2001module3data(ItemI item)
	{
		super(item);
	}

	public AutoAdosAdos2001module3data(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoAdosAdos2001module3data(UserI user)
	 **/
	public AutoAdosAdos2001module3data(){}

	public AutoAdosAdos2001module3data(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "ados:ados2001Module3Data";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Commtot=null;

	/**
	 * @return Returns the commtot.
	 */
	public Integer getCommtot() {
		try{
			if (_Commtot==null){
				_Commtot=getIntegerProperty("commtot");
				return _Commtot;
			}else {
				return _Commtot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for commtot.
	 * @param v Value to Set.
	 */
	public void setCommtot(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/commtot",v);
		_Commtot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _CommtotNote=null;

	/**
	 * @return Returns the commtot_note.
	 */
	public String getCommtotNote(){
		try{
			if (_CommtotNote==null){
				_CommtotNote=getStringProperty("commtot_note");
				return _CommtotNote;
			}else {
				return _CommtotNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for commtot_note.
	 * @param v Value to Set.
	 */
	public void setCommtotNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/commtot_note",v);
		_CommtotNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scintot=null;

	/**
	 * @return Returns the scintot.
	 */
	public Integer getScintot() {
		try{
			if (_Scintot==null){
				_Scintot=getIntegerProperty("scintot");
				return _Scintot;
			}else {
				return _Scintot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for scintot.
	 * @param v Value to Set.
	 */
	public void setScintot(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/scintot",v);
		_Scintot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _ScintotNote=null;

	/**
	 * @return Returns the scintot_note.
	 */
	public String getScintotNote(){
		try{
			if (_ScintotNote==null){
				_ScintotNote=getStringProperty("scintot_note");
				return _ScintotNote;
			}else {
				return _ScintotNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for scintot_note.
	 * @param v Value to Set.
	 */
	public void setScintotNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/scintot_note",v);
		_ScintotNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cmsitot=null;

	/**
	 * @return Returns the cmsitot.
	 */
	public Integer getCmsitot() {
		try{
			if (_Cmsitot==null){
				_Cmsitot=getIntegerProperty("cmsitot");
				return _Cmsitot;
			}else {
				return _Cmsitot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cmsitot.
	 * @param v Value to Set.
	 */
	public void setCmsitot(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/cmsitot",v);
		_Cmsitot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _CmsitotNote=null;

	/**
	 * @return Returns the cmsitot_note.
	 */
	public String getCmsitotNote(){
		try{
			if (_CmsitotNote==null){
				_CmsitotNote=getStringProperty("cmsitot_note");
				return _CmsitotNote;
			}else {
				return _CmsitotNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cmsitot_note.
	 * @param v Value to Set.
	 */
	public void setCmsitotNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/cmsitot_note",v);
		_CmsitotNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Playtot=null;

	/**
	 * @return Returns the playtot.
	 */
	public Integer getPlaytot() {
		try{
			if (_Playtot==null){
				_Playtot=getIntegerProperty("playtot");
				return _Playtot;
			}else {
				return _Playtot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for playtot.
	 * @param v Value to Set.
	 */
	public void setPlaytot(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/playtot",v);
		_Playtot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _PlaytotNote=null;

	/**
	 * @return Returns the playtot_note.
	 */
	public String getPlaytotNote(){
		try{
			if (_PlaytotNote==null){
				_PlaytotNote=getStringProperty("playtot_note");
				return _PlaytotNote;
			}else {
				return _PlaytotNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for playtot_note.
	 * @param v Value to Set.
	 */
	public void setPlaytotNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/playtot_note",v);
		_PlaytotNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Imaginetot=null;

	/**
	 * @return Returns the imaginetot.
	 */
	public Integer getImaginetot() {
		try{
			if (_Imaginetot==null){
				_Imaginetot=getIntegerProperty("imaginetot");
				return _Imaginetot;
			}else {
				return _Imaginetot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for imaginetot.
	 * @param v Value to Set.
	 */
	public void setImaginetot(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/imaginetot",v);
		_Imaginetot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _ImaginetotNote=null;

	/**
	 * @return Returns the imaginetot_note.
	 */
	public String getImaginetotNote(){
		try{
			if (_ImaginetotNote==null){
				_ImaginetotNote=getStringProperty("imaginetot_note");
				return _ImaginetotNote;
			}else {
				return _ImaginetotNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for imaginetot_note.
	 * @param v Value to Set.
	 */
	public void setImaginetotNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/imaginetot_note",v);
		_ImaginetotNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sbritot=null;

	/**
	 * @return Returns the sbritot.
	 */
	public Integer getSbritot() {
		try{
			if (_Sbritot==null){
				_Sbritot=getIntegerProperty("sbritot");
				return _Sbritot;
			}else {
				return _Sbritot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sbritot.
	 * @param v Value to Set.
	 */
	public void setSbritot(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sbritot",v);
		_Sbritot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _SbritotNote=null;

	/**
	 * @return Returns the sbritot_note.
	 */
	public String getSbritotNote(){
		try{
			if (_SbritotNote==null){
				_SbritotNote=getStringProperty("sbritot_note");
				return _SbritotNote;
			}else {
				return _SbritotNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sbritot_note.
	 * @param v Value to Set.
	 */
	public void setSbritotNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sbritot_note",v);
		_SbritotNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_diagnosis_adosclas=null;

	/**
	 * @return Returns the ScoreForm/diagnosis/adosclas.
	 */
	public String getScoreform_diagnosis_adosclas(){
		try{
			if (_Scoreform_diagnosis_adosclas==null){
				_Scoreform_diagnosis_adosclas=getStringProperty("ScoreForm/diagnosis/adosclas");
				return _Scoreform_diagnosis_adosclas;
			}else {
				return _Scoreform_diagnosis_adosclas;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/diagnosis/adosclas.
	 * @param v Value to Set.
	 */
	public void setScoreform_diagnosis_adosclas(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/diagnosis/adosclas",v);
		_Scoreform_diagnosis_adosclas=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_diagnosis_overalldiag=null;

	/**
	 * @return Returns the ScoreForm/diagnosis/overallDiag.
	 */
	public String getScoreform_diagnosis_overalldiag(){
		try{
			if (_Scoreform_diagnosis_overalldiag==null){
				_Scoreform_diagnosis_overalldiag=getStringProperty("ScoreForm/diagnosis/overallDiag");
				return _Scoreform_diagnosis_overalldiag;
			}else {
				return _Scoreform_diagnosis_overalldiag;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/diagnosis/overallDiag.
	 * @param v Value to Set.
	 */
	public void setScoreform_diagnosis_overalldiag(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/diagnosis/overallDiag",v);
		_Scoreform_diagnosis_overalldiag=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_constrtask=null;

	/**
	 * @return Returns the ScoreForm/observation/constrTask.
	 */
	public String getScoreform_observation_constrtask(){
		try{
			if (_Scoreform_observation_constrtask==null){
				_Scoreform_observation_constrtask=getStringProperty("ScoreForm/observation/constrTask");
				return _Scoreform_observation_constrtask;
			}else {
				return _Scoreform_observation_constrtask;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/constrTask.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_constrtask(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/constrTask",v);
		_Scoreform_observation_constrtask=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_makebelieve=null;

	/**
	 * @return Returns the ScoreForm/observation/makeBelieve.
	 */
	public String getScoreform_observation_makebelieve(){
		try{
			if (_Scoreform_observation_makebelieve==null){
				_Scoreform_observation_makebelieve=getStringProperty("ScoreForm/observation/makeBelieve");
				return _Scoreform_observation_makebelieve;
			}else {
				return _Scoreform_observation_makebelieve;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/makeBelieve.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_makebelieve(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/makeBelieve",v);
		_Scoreform_observation_makebelieve=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_jointplay=null;

	/**
	 * @return Returns the ScoreForm/observation/jointPlay.
	 */
	public String getScoreform_observation_jointplay(){
		try{
			if (_Scoreform_observation_jointplay==null){
				_Scoreform_observation_jointplay=getStringProperty("ScoreForm/observation/jointPlay");
				return _Scoreform_observation_jointplay;
			}else {
				return _Scoreform_observation_jointplay;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/jointPlay.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_jointplay(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/jointPlay",v);
		_Scoreform_observation_jointplay=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_demonst=null;

	/**
	 * @return Returns the ScoreForm/observation/demonst.
	 */
	public String getScoreform_observation_demonst(){
		try{
			if (_Scoreform_observation_demonst==null){
				_Scoreform_observation_demonst=getStringProperty("ScoreForm/observation/demonst");
				return _Scoreform_observation_demonst;
			}else {
				return _Scoreform_observation_demonst;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/demonst.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_demonst(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/demonst",v);
		_Scoreform_observation_demonst=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_descpicture=null;

	/**
	 * @return Returns the ScoreForm/observation/descPicture.
	 */
	public String getScoreform_observation_descpicture(){
		try{
			if (_Scoreform_observation_descpicture==null){
				_Scoreform_observation_descpicture=getStringProperty("ScoreForm/observation/descPicture");
				return _Scoreform_observation_descpicture;
			}else {
				return _Scoreform_observation_descpicture;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/descPicture.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_descpicture(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/descPicture",v);
		_Scoreform_observation_descpicture=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_tellstory=null;

	/**
	 * @return Returns the ScoreForm/observation/tellStory.
	 */
	public String getScoreform_observation_tellstory(){
		try{
			if (_Scoreform_observation_tellstory==null){
				_Scoreform_observation_tellstory=getStringProperty("ScoreForm/observation/tellStory");
				return _Scoreform_observation_tellstory;
			}else {
				return _Scoreform_observation_tellstory;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/tellStory.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_tellstory(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/tellStory",v);
		_Scoreform_observation_tellstory=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_cartoons=null;

	/**
	 * @return Returns the ScoreForm/observation/cartoons.
	 */
	public String getScoreform_observation_cartoons(){
		try{
			if (_Scoreform_observation_cartoons==null){
				_Scoreform_observation_cartoons=getStringProperty("ScoreForm/observation/cartoons");
				return _Scoreform_observation_cartoons;
			}else {
				return _Scoreform_observation_cartoons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/cartoons.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_cartoons(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/cartoons",v);
		_Scoreform_observation_cartoons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_convsreport=null;

	/**
	 * @return Returns the ScoreForm/observation/convsReport.
	 */
	public String getScoreform_observation_convsreport(){
		try{
			if (_Scoreform_observation_convsreport==null){
				_Scoreform_observation_convsreport=getStringProperty("ScoreForm/observation/convsReport");
				return _Scoreform_observation_convsreport;
			}else {
				return _Scoreform_observation_convsreport;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/convsReport.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_convsreport(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/convsReport",v);
		_Scoreform_observation_convsreport=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_emotions=null;

	/**
	 * @return Returns the ScoreForm/observation/emotions.
	 */
	public String getScoreform_observation_emotions(){
		try{
			if (_Scoreform_observation_emotions==null){
				_Scoreform_observation_emotions=getStringProperty("ScoreForm/observation/emotions");
				return _Scoreform_observation_emotions;
			}else {
				return _Scoreform_observation_emotions;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/emotions.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_emotions(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/emotions",v);
		_Scoreform_observation_emotions=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_socdiffannoy=null;

	/**
	 * @return Returns the ScoreForm/observation/socDiffAnnoy.
	 */
	public String getScoreform_observation_socdiffannoy(){
		try{
			if (_Scoreform_observation_socdiffannoy==null){
				_Scoreform_observation_socdiffannoy=getStringProperty("ScoreForm/observation/socDiffAnnoy");
				return _Scoreform_observation_socdiffannoy;
			}else {
				return _Scoreform_observation_socdiffannoy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/socDiffAnnoy.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_socdiffannoy(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/socDiffAnnoy",v);
		_Scoreform_observation_socdiffannoy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_break=null;

	/**
	 * @return Returns the ScoreForm/observation/break.
	 */
	public String getScoreform_observation_break(){
		try{
			if (_Scoreform_observation_break==null){
				_Scoreform_observation_break=getStringProperty("ScoreForm/observation/break");
				return _Scoreform_observation_break;
			}else {
				return _Scoreform_observation_break;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/break.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_break(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/break",v);
		_Scoreform_observation_break=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_friendmarrg=null;

	/**
	 * @return Returns the ScoreForm/observation/friendMarrg.
	 */
	public String getScoreform_observation_friendmarrg(){
		try{
			if (_Scoreform_observation_friendmarrg==null){
				_Scoreform_observation_friendmarrg=getStringProperty("ScoreForm/observation/friendMarrg");
				return _Scoreform_observation_friendmarrg;
			}else {
				return _Scoreform_observation_friendmarrg;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/friendMarrg.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_friendmarrg(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/friendMarrg",v);
		_Scoreform_observation_friendmarrg=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_loneliness=null;

	/**
	 * @return Returns the ScoreForm/observation/loneliness.
	 */
	public String getScoreform_observation_loneliness(){
		try{
			if (_Scoreform_observation_loneliness==null){
				_Scoreform_observation_loneliness=getStringProperty("ScoreForm/observation/loneliness");
				return _Scoreform_observation_loneliness;
			}else {
				return _Scoreform_observation_loneliness;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/loneliness.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_loneliness(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/loneliness",v);
		_Scoreform_observation_loneliness=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_createstory=null;

	/**
	 * @return Returns the ScoreForm/observation/createStory.
	 */
	public String getScoreform_observation_createstory(){
		try{
			if (_Scoreform_observation_createstory==null){
				_Scoreform_observation_createstory=getStringProperty("ScoreForm/observation/createStory");
				return _Scoreform_observation_createstory;
			}else {
				return _Scoreform_observation_createstory;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/createStory.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_createstory(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/createStory",v);
		_Scoreform_observation_createstory=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_nonechoedlang=null;

	/**
	 * @return Returns the ScoreForm/sectionA/nonechoedLang.
	 */
	public Integer getScoreform_sectiona_nonechoedlang() {
		try{
			if (_Scoreform_sectiona_nonechoedlang==null){
				_Scoreform_sectiona_nonechoedlang=getIntegerProperty("ScoreForm/sectionA/nonechoedLang");
				return _Scoreform_sectiona_nonechoedlang;
			}else {
				return _Scoreform_sectiona_nonechoedlang;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/nonechoedLang.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_nonechoedlang(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/nonechoedLang",v);
		_Scoreform_sectiona_nonechoedlang=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_speechabnorm=null;

	/**
	 * @return Returns the ScoreForm/sectionA/speechAbnorm.
	 */
	public Integer getScoreform_sectiona_speechabnorm() {
		try{
			if (_Scoreform_sectiona_speechabnorm==null){
				_Scoreform_sectiona_speechabnorm=getIntegerProperty("ScoreForm/sectionA/speechAbnorm");
				return _Scoreform_sectiona_speechabnorm;
			}else {
				return _Scoreform_sectiona_speechabnorm;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/speechAbnorm.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_speechabnorm(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/speechAbnorm",v);
		_Scoreform_sectiona_speechabnorm=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_immedecholalia=null;

	/**
	 * @return Returns the ScoreForm/sectionA/immedEcholalia.
	 */
	public Integer getScoreform_sectiona_immedecholalia() {
		try{
			if (_Scoreform_sectiona_immedecholalia==null){
				_Scoreform_sectiona_immedecholalia=getIntegerProperty("ScoreForm/sectionA/immedEcholalia");
				return _Scoreform_sectiona_immedecholalia;
			}else {
				return _Scoreform_sectiona_immedecholalia;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/immedEcholalia.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_immedecholalia(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/immedEcholalia",v);
		_Scoreform_sectiona_immedecholalia=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_stidiosyncwords=null;

	/**
	 * @return Returns the ScoreForm/sectionA/stIdiosyncWords.
	 */
	public Integer getScoreform_sectiona_stidiosyncwords() {
		try{
			if (_Scoreform_sectiona_stidiosyncwords==null){
				_Scoreform_sectiona_stidiosyncwords=getIntegerProperty("ScoreForm/sectionA/stIdiosyncWords");
				return _Scoreform_sectiona_stidiosyncwords;
			}else {
				return _Scoreform_sectiona_stidiosyncwords;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/stIdiosyncWords.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_stidiosyncwords(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/stIdiosyncWords",v);
		_Scoreform_sectiona_stidiosyncwords=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_offinform=null;

	/**
	 * @return Returns the ScoreForm/sectionA/offInform.
	 */
	public Integer getScoreform_sectiona_offinform() {
		try{
			if (_Scoreform_sectiona_offinform==null){
				_Scoreform_sectiona_offinform=getIntegerProperty("ScoreForm/sectionA/offInform");
				return _Scoreform_sectiona_offinform;
			}else {
				return _Scoreform_sectiona_offinform;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/offInform.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_offinform(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/offInform",v);
		_Scoreform_sectiona_offinform=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_askinform=null;

	/**
	 * @return Returns the ScoreForm/sectionA/askInform.
	 */
	public Integer getScoreform_sectiona_askinform() {
		try{
			if (_Scoreform_sectiona_askinform==null){
				_Scoreform_sectiona_askinform=getIntegerProperty("ScoreForm/sectionA/askInform");
				return _Scoreform_sectiona_askinform;
			}else {
				return _Scoreform_sectiona_askinform;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/askInform.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_askinform(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/askInform",v);
		_Scoreform_sectiona_askinform=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_reportevents=null;

	/**
	 * @return Returns the ScoreForm/sectionA/reportEvents.
	 */
	public Integer getScoreform_sectiona_reportevents() {
		try{
			if (_Scoreform_sectiona_reportevents==null){
				_Scoreform_sectiona_reportevents=getIntegerProperty("ScoreForm/sectionA/reportEvents");
				return _Scoreform_sectiona_reportevents;
			}else {
				return _Scoreform_sectiona_reportevents;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/reportEvents.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_reportevents(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/reportEvents",v);
		_Scoreform_sectiona_reportevents=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_conversation=null;

	/**
	 * @return Returns the ScoreForm/sectionA/conversation.
	 */
	public Integer getScoreform_sectiona_conversation() {
		try{
			if (_Scoreform_sectiona_conversation==null){
				_Scoreform_sectiona_conversation=getIntegerProperty("ScoreForm/sectionA/conversation");
				return _Scoreform_sectiona_conversation;
			}else {
				return _Scoreform_sectiona_conversation;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/conversation.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_conversation(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/conversation",v);
		_Scoreform_sectiona_conversation=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_descinformgest=null;

	/**
	 * @return Returns the ScoreForm/sectionA/descInformGest.
	 */
	public Integer getScoreform_sectiona_descinformgest() {
		try{
			if (_Scoreform_sectiona_descinformgest==null){
				_Scoreform_sectiona_descinformgest=getIntegerProperty("ScoreForm/sectionA/descInformGest");
				return _Scoreform_sectiona_descinformgest;
			}else {
				return _Scoreform_sectiona_descinformgest;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/descInformGest.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_descinformgest(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/descInformGest",v);
		_Scoreform_sectiona_descinformgest=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_unusleyecont=null;

	/**
	 * @return Returns the ScoreForm/sectionB/unuslEyeCont.
	 */
	public Integer getScoreform_sectionb_unusleyecont() {
		try{
			if (_Scoreform_sectionb_unusleyecont==null){
				_Scoreform_sectionb_unusleyecont=getIntegerProperty("ScoreForm/sectionB/unuslEyeCont");
				return _Scoreform_sectionb_unusleyecont;
			}else {
				return _Scoreform_sectionb_unusleyecont;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/unuslEyeCont.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_unusleyecont(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/unuslEyeCont",v);
		_Scoreform_sectionb_unusleyecont=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_facexproth=null;

	/**
	 * @return Returns the ScoreForm/sectionB/facExprOth.
	 */
	public Integer getScoreform_sectionb_facexproth() {
		try{
			if (_Scoreform_sectionb_facexproth==null){
				_Scoreform_sectionb_facexproth=getIntegerProperty("ScoreForm/sectionB/facExprOth");
				return _Scoreform_sectionb_facexproth;
			}else {
				return _Scoreform_sectionb_facexproth;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/facExprOth.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_facexproth(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/facExprOth",v);
		_Scoreform_sectionb_facexproth=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_langprod=null;

	/**
	 * @return Returns the ScoreForm/sectionB/langProd.
	 */
	public Integer getScoreform_sectionb_langprod() {
		try{
			if (_Scoreform_sectionb_langprod==null){
				_Scoreform_sectionb_langprod=getIntegerProperty("ScoreForm/sectionB/langProd");
				return _Scoreform_sectionb_langprod;
			}else {
				return _Scoreform_sectionb_langprod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/langProd.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_langprod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/langProd",v);
		_Scoreform_sectionb_langprod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_shareenjoy=null;

	/**
	 * @return Returns the ScoreForm/sectionB/shareEnjoy.
	 */
	public Integer getScoreform_sectionb_shareenjoy() {
		try{
			if (_Scoreform_sectionb_shareenjoy==null){
				_Scoreform_sectionb_shareenjoy=getIntegerProperty("ScoreForm/sectionB/shareEnjoy");
				return _Scoreform_sectionb_shareenjoy;
			}else {
				return _Scoreform_sectionb_shareenjoy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/shareEnjoy.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_shareenjoy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/shareEnjoy",v);
		_Scoreform_sectionb_shareenjoy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_empathcomm=null;

	/**
	 * @return Returns the ScoreForm/sectionB/empathComm.
	 */
	public Integer getScoreform_sectionb_empathcomm() {
		try{
			if (_Scoreform_sectionb_empathcomm==null){
				_Scoreform_sectionb_empathcomm=getIntegerProperty("ScoreForm/sectionB/empathComm");
				return _Scoreform_sectionb_empathcomm;
			}else {
				return _Scoreform_sectionb_empathcomm;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/empathComm.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_empathcomm(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/empathComm",v);
		_Scoreform_sectionb_empathcomm=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_insight=null;

	/**
	 * @return Returns the ScoreForm/sectionB/insight.
	 */
	public Integer getScoreform_sectionb_insight() {
		try{
			if (_Scoreform_sectionb_insight==null){
				_Scoreform_sectionb_insight=getIntegerProperty("ScoreForm/sectionB/insight");
				return _Scoreform_sectionb_insight;
			}else {
				return _Scoreform_sectionb_insight;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/insight.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_insight(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/insight",v);
		_Scoreform_sectionb_insight=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_qualsocoverture=null;

	/**
	 * @return Returns the ScoreForm/sectionB/qualSocOverture.
	 */
	public Integer getScoreform_sectionb_qualsocoverture() {
		try{
			if (_Scoreform_sectionb_qualsocoverture==null){
				_Scoreform_sectionb_qualsocoverture=getIntegerProperty("ScoreForm/sectionB/qualSocOverture");
				return _Scoreform_sectionb_qualsocoverture;
			}else {
				return _Scoreform_sectionb_qualsocoverture;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/qualSocOverture.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_qualsocoverture(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/qualSocOverture",v);
		_Scoreform_sectionb_qualsocoverture=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_qualsocresp=null;

	/**
	 * @return Returns the ScoreForm/sectionB/qualSocResp.
	 */
	public Integer getScoreform_sectionb_qualsocresp() {
		try{
			if (_Scoreform_sectionb_qualsocresp==null){
				_Scoreform_sectionb_qualsocresp=getIntegerProperty("ScoreForm/sectionB/qualSocResp");
				return _Scoreform_sectionb_qualsocresp;
			}else {
				return _Scoreform_sectionb_qualsocresp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/qualSocResp.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_qualsocresp(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/qualSocResp",v);
		_Scoreform_sectionb_qualsocresp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_amtrecipsoccomm=null;

	/**
	 * @return Returns the ScoreForm/sectionB/amtRecipSocComm.
	 */
	public Integer getScoreform_sectionb_amtrecipsoccomm() {
		try{
			if (_Scoreform_sectionb_amtrecipsoccomm==null){
				_Scoreform_sectionb_amtrecipsoccomm=getIntegerProperty("ScoreForm/sectionB/amtRecipSocComm");
				return _Scoreform_sectionb_amtrecipsoccomm;
			}else {
				return _Scoreform_sectionb_amtrecipsoccomm;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/amtRecipSocComm.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_amtrecipsoccomm(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/amtRecipSocComm",v);
		_Scoreform_sectionb_amtrecipsoccomm=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_overallqualrapp=null;

	/**
	 * @return Returns the ScoreForm/sectionB/overallQualRapp.
	 */
	public Integer getScoreform_sectionb_overallqualrapp() {
		try{
			if (_Scoreform_sectionb_overallqualrapp==null){
				_Scoreform_sectionb_overallqualrapp=getIntegerProperty("ScoreForm/sectionB/overallQualRapp");
				return _Scoreform_sectionb_overallqualrapp;
			}else {
				return _Scoreform_sectionb_overallqualrapp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/overallQualRapp.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_overallqualrapp(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/overallQualRapp",v);
		_Scoreform_sectionb_overallqualrapp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc_imagcreativ=null;

	/**
	 * @return Returns the ScoreForm/sectionC/imagCreativ.
	 */
	public Integer getScoreform_sectionc_imagcreativ() {
		try{
			if (_Scoreform_sectionc_imagcreativ==null){
				_Scoreform_sectionc_imagcreativ=getIntegerProperty("ScoreForm/sectionC/imagCreativ");
				return _Scoreform_sectionc_imagcreativ;
			}else {
				return _Scoreform_sectionc_imagcreativ;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC/imagCreativ.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc_imagcreativ(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC/imagCreativ",v);
		_Scoreform_sectionc_imagcreativ=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_unuslsensint=null;

	/**
	 * @return Returns the ScoreForm/sectionD/unuslSensInt.
	 */
	public Integer getScoreform_sectiond_unuslsensint() {
		try{
			if (_Scoreform_sectiond_unuslsensint==null){
				_Scoreform_sectiond_unuslsensint=getIntegerProperty("ScoreForm/sectionD/unuslSensInt");
				return _Scoreform_sectiond_unuslsensint;
			}else {
				return _Scoreform_sectiond_unuslsensint;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/unuslSensInt.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_unuslsensint(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/unuslSensInt",v);
		_Scoreform_sectiond_unuslsensint=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_handfgrcomplx=null;

	/**
	 * @return Returns the ScoreForm/sectionD/handFgrComplx.
	 */
	public Integer getScoreform_sectiond_handfgrcomplx() {
		try{
			if (_Scoreform_sectiond_handfgrcomplx==null){
				_Scoreform_sectiond_handfgrcomplx=getIntegerProperty("ScoreForm/sectionD/handFgrComplx");
				return _Scoreform_sectiond_handfgrcomplx;
			}else {
				return _Scoreform_sectiond_handfgrcomplx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/handFgrComplx.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_handfgrcomplx(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/handFgrComplx",v);
		_Scoreform_sectiond_handfgrcomplx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_selfinjurbehav=null;

	/**
	 * @return Returns the ScoreForm/sectionD/selfInjurBehav.
	 */
	public Integer getScoreform_sectiond_selfinjurbehav() {
		try{
			if (_Scoreform_sectiond_selfinjurbehav==null){
				_Scoreform_sectiond_selfinjurbehav=getIntegerProperty("ScoreForm/sectionD/selfInjurBehav");
				return _Scoreform_sectiond_selfinjurbehav;
			}else {
				return _Scoreform_sectiond_selfinjurbehav;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/selfInjurBehav.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_selfinjurbehav(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/selfInjurBehav",v);
		_Scoreform_sectiond_selfinjurbehav=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_excessinterstunusl=null;

	/**
	 * @return Returns the ScoreForm/sectionD/excessInterstUnusl.
	 */
	public Integer getScoreform_sectiond_excessinterstunusl() {
		try{
			if (_Scoreform_sectiond_excessinterstunusl==null){
				_Scoreform_sectiond_excessinterstunusl=getIntegerProperty("ScoreForm/sectionD/excessInterstUnusl");
				return _Scoreform_sectiond_excessinterstunusl;
			}else {
				return _Scoreform_sectiond_excessinterstunusl;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/excessInterstUnusl.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_excessinterstunusl(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/excessInterstUnusl",v);
		_Scoreform_sectiond_excessinterstunusl=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_compulritual=null;

	/**
	 * @return Returns the ScoreForm/sectionD/compulRitual.
	 */
	public Integer getScoreform_sectiond_compulritual() {
		try{
			if (_Scoreform_sectiond_compulritual==null){
				_Scoreform_sectiond_compulritual=getIntegerProperty("ScoreForm/sectionD/compulRitual");
				return _Scoreform_sectiond_compulritual;
			}else {
				return _Scoreform_sectiond_compulritual;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/compulRitual.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_compulritual(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/compulRitual",v);
		_Scoreform_sectiond_compulritual=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectione_overactagit=null;

	/**
	 * @return Returns the ScoreForm/sectionE/overactAgit.
	 */
	public Integer getScoreform_sectione_overactagit() {
		try{
			if (_Scoreform_sectione_overactagit==null){
				_Scoreform_sectione_overactagit=getIntegerProperty("ScoreForm/sectionE/overactAgit");
				return _Scoreform_sectione_overactagit;
			}else {
				return _Scoreform_sectione_overactagit;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionE/overactAgit.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectione_overactagit(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionE/overactAgit",v);
		_Scoreform_sectione_overactagit=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectione_tantrmagress=null;

	/**
	 * @return Returns the ScoreForm/sectionE/tantrmAgress.
	 */
	public Integer getScoreform_sectione_tantrmagress() {
		try{
			if (_Scoreform_sectione_tantrmagress==null){
				_Scoreform_sectione_tantrmagress=getIntegerProperty("ScoreForm/sectionE/tantrmAgress");
				return _Scoreform_sectione_tantrmagress;
			}else {
				return _Scoreform_sectione_tantrmagress;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionE/tantrmAgress.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectione_tantrmagress(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionE/tantrmAgress",v);
		_Scoreform_sectione_tantrmagress=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectione_anxiety=null;

	/**
	 * @return Returns the ScoreForm/sectionE/anxiety.
	 */
	public Integer getScoreform_sectione_anxiety() {
		try{
			if (_Scoreform_sectione_anxiety==null){
				_Scoreform_sectione_anxiety=getIntegerProperty("ScoreForm/sectionE/anxiety");
				return _Scoreform_sectione_anxiety;
			}else {
				return _Scoreform_sectione_anxiety;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionE/anxiety.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectione_anxiety(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionE/anxiety",v);
		_Scoreform_sectione_anxiety=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.AdosAdos2001module3data> getAllAdosAdos2001module3datas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.AdosAdos2001module3data> al = new ArrayList<org.nrg.xdat.om.AdosAdos2001module3data>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.AdosAdos2001module3data> getAdosAdos2001module3datasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.AdosAdos2001module3data> al = new ArrayList<org.nrg.xdat.om.AdosAdos2001module3data>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.AdosAdos2001module3data> getAdosAdos2001module3datasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.AdosAdos2001module3data> al = new ArrayList<org.nrg.xdat.om.AdosAdos2001module3data>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static AdosAdos2001module3data getAdosAdos2001module3datasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("ados:ados2001Module3Data/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (AdosAdos2001module3data) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
