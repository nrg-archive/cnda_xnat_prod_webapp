/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCondrBraincolldataTumorloc extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.CondrBraincolldataTumorlocI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCondrBraincolldataTumorloc.class);
	public static String SCHEMA_ELEMENT_NAME="condr:brainCollData_tumorLoc";

	public AutoCondrBraincolldataTumorloc(ItemI item)
	{
		super(item);
	}

	public AutoCondrBraincolldataTumorloc(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCondrBraincolldataTumorloc(UserI user)
	 **/
	public AutoCondrBraincolldataTumorloc(){}

	public AutoCondrBraincolldataTumorloc(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "condr:brainCollData_tumorLoc";
	}

	//FIELD

	private String _Tumorloc=null;

	/**
	 * @return Returns the tumorLoc.
	 */
	public String getTumorloc(){
		try{
			if (_Tumorloc==null){
				_Tumorloc=getStringProperty("tumorLoc");
				return _Tumorloc;
			}else {
				return _Tumorloc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tumorLoc.
	 * @param v Value to Set.
	 */
	public void setTumorloc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tumorLoc",v);
		_Tumorloc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CondrBraincolldataTumorlocId=null;

	/**
	 * @return Returns the condr_brainCollData_tumorLoc_id.
	 */
	public Integer getCondrBraincolldataTumorlocId() {
		try{
			if (_CondrBraincolldataTumorlocId==null){
				_CondrBraincolldataTumorlocId=getIntegerProperty("condr_brainCollData_tumorLoc_id");
				return _CondrBraincolldataTumorlocId;
			}else {
				return _CondrBraincolldataTumorlocId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for condr_brainCollData_tumorLoc_id.
	 * @param v Value to Set.
	 */
	public void setCondrBraincolldataTumorlocId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/condr_brainCollData_tumorLoc_id",v);
		_CondrBraincolldataTumorlocId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CondrBraincolldataTumorloc> getAllCondrBraincolldataTumorlocs(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrBraincolldataTumorloc> al = new ArrayList<org.nrg.xdat.om.CondrBraincolldataTumorloc>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrBraincolldataTumorloc> getCondrBraincolldataTumorlocsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrBraincolldataTumorloc> al = new ArrayList<org.nrg.xdat.om.CondrBraincolldataTumorloc>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrBraincolldataTumorloc> getCondrBraincolldataTumorlocsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrBraincolldataTumorloc> al = new ArrayList<org.nrg.xdat.om.CondrBraincolldataTumorloc>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CondrBraincolldataTumorloc getCondrBraincolldataTumorlocsByCondrBraincolldataTumorlocId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("condr:brainCollData_tumorLoc/condr_brainCollData_tumorLoc_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CondrBraincolldataTumorloc) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
