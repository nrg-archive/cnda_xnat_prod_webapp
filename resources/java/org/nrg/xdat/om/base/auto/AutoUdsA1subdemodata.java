/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoUdsA1subdemodata extends XnatSubjectassessordata implements org.nrg.xdat.model.UdsA1subdemodataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoUdsA1subdemodata.class);
	public static String SCHEMA_ELEMENT_NAME="uds:a1subdemoData";

	public AutoUdsA1subdemodata(ItemI item)
	{
		super(item);
	}

	public AutoUdsA1subdemodata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoUdsA1subdemodata(UserI user)
	 **/
	public AutoUdsA1subdemodata(){}

	public AutoUdsA1subdemodata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "uds:a1subdemoData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Refer=null;

	/**
	 * @return Returns the REFER.
	 */
	public Integer getRefer() {
		try{
			if (_Refer==null){
				_Refer=getIntegerProperty("REFER");
				return _Refer;
			}else {
				return _Refer;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REFER.
	 * @param v Value to Set.
	 */
	public void setRefer(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REFER",v);
		_Refer=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Referx=null;

	/**
	 * @return Returns the REFERX.
	 */
	public String getReferx(){
		try{
			if (_Referx==null){
				_Referx=getStringProperty("REFERX");
				return _Referx;
			}else {
				return _Referx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REFERX.
	 * @param v Value to Set.
	 */
	public void setReferx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REFERX",v);
		_Referx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Prestat=null;

	/**
	 * @return Returns the PRESTAT.
	 */
	public Integer getPrestat() {
		try{
			if (_Prestat==null){
				_Prestat=getIntegerProperty("PRESTAT");
				return _Prestat;
			}else {
				return _Prestat;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PRESTAT.
	 * @param v Value to Set.
	 */
	public void setPrestat(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PRESTAT",v);
		_Prestat=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Birthmo=null;

	/**
	 * @return Returns the BIRTHMO.
	 */
	public Integer getBirthmo() {
		try{
			if (_Birthmo==null){
				_Birthmo=getIntegerProperty("BIRTHMO");
				return _Birthmo;
			}else {
				return _Birthmo;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BIRTHMO.
	 * @param v Value to Set.
	 */
	public void setBirthmo(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BIRTHMO",v);
		_Birthmo=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Birthyr=null;

	/**
	 * @return Returns the BIRTHYR.
	 */
	public Integer getBirthyr() {
		try{
			if (_Birthyr==null){
				_Birthyr=getIntegerProperty("BIRTHYR");
				return _Birthyr;
			}else {
				return _Birthyr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BIRTHYR.
	 * @param v Value to Set.
	 */
	public void setBirthyr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BIRTHYR",v);
		_Birthyr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sex=null;

	/**
	 * @return Returns the SEX.
	 */
	public Integer getSex() {
		try{
			if (_Sex==null){
				_Sex=getIntegerProperty("SEX");
				return _Sex;
			}else {
				return _Sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SEX.
	 * @param v Value to Set.
	 */
	public void setSex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SEX",v);
		_Sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Hispanic=null;

	/**
	 * @return Returns the HISPANIC.
	 */
	public Integer getHispanic() {
		try{
			if (_Hispanic==null){
				_Hispanic=getIntegerProperty("HISPANIC");
				return _Hispanic;
			}else {
				return _Hispanic;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HISPANIC.
	 * @param v Value to Set.
	 */
	public void setHispanic(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HISPANIC",v);
		_Hispanic=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Hispor=null;

	/**
	 * @return Returns the HISPOR.
	 */
	public Integer getHispor() {
		try{
			if (_Hispor==null){
				_Hispor=getIntegerProperty("HISPOR");
				return _Hispor;
			}else {
				return _Hispor;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HISPOR.
	 * @param v Value to Set.
	 */
	public void setHispor(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HISPOR",v);
		_Hispor=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Hisporx=null;

	/**
	 * @return Returns the HISPORX.
	 */
	public String getHisporx(){
		try{
			if (_Hisporx==null){
				_Hisporx=getStringProperty("HISPORX");
				return _Hisporx;
			}else {
				return _Hisporx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HISPORX.
	 * @param v Value to Set.
	 */
	public void setHisporx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HISPORX",v);
		_Hisporx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Race=null;

	/**
	 * @return Returns the RACE.
	 */
	public Integer getRace() {
		try{
			if (_Race==null){
				_Race=getIntegerProperty("RACE");
				return _Race;
			}else {
				return _Race;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RACE.
	 * @param v Value to Set.
	 */
	public void setRace(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RACE",v);
		_Race=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Racex=null;

	/**
	 * @return Returns the RACEX.
	 */
	public String getRacex(){
		try{
			if (_Racex==null){
				_Racex=getStringProperty("RACEX");
				return _Racex;
			}else {
				return _Racex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RACEX.
	 * @param v Value to Set.
	 */
	public void setRacex(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RACEX",v);
		_Racex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Racesec=null;

	/**
	 * @return Returns the RACESEC.
	 */
	public Integer getRacesec() {
		try{
			if (_Racesec==null){
				_Racesec=getIntegerProperty("RACESEC");
				return _Racesec;
			}else {
				return _Racesec;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RACESEC.
	 * @param v Value to Set.
	 */
	public void setRacesec(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RACESEC",v);
		_Racesec=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Racesecx=null;

	/**
	 * @return Returns the RACESECX.
	 */
	public String getRacesecx(){
		try{
			if (_Racesecx==null){
				_Racesecx=getStringProperty("RACESECX");
				return _Racesecx;
			}else {
				return _Racesecx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RACESECX.
	 * @param v Value to Set.
	 */
	public void setRacesecx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RACESECX",v);
		_Racesecx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Raceter=null;

	/**
	 * @return Returns the RACETER.
	 */
	public Integer getRaceter() {
		try{
			if (_Raceter==null){
				_Raceter=getIntegerProperty("RACETER");
				return _Raceter;
			}else {
				return _Raceter;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RACETER.
	 * @param v Value to Set.
	 */
	public void setRaceter(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RACETER",v);
		_Raceter=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Raceterx=null;

	/**
	 * @return Returns the RACETERX.
	 */
	public String getRaceterx(){
		try{
			if (_Raceterx==null){
				_Raceterx=getStringProperty("RACETERX");
				return _Raceterx;
			}else {
				return _Raceterx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RACETERX.
	 * @param v Value to Set.
	 */
	public void setRaceterx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RACETERX",v);
		_Raceterx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Primlang=null;

	/**
	 * @return Returns the PRIMLANG.
	 */
	public Integer getPrimlang() {
		try{
			if (_Primlang==null){
				_Primlang=getIntegerProperty("PRIMLANG");
				return _Primlang;
			}else {
				return _Primlang;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PRIMLANG.
	 * @param v Value to Set.
	 */
	public void setPrimlang(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PRIMLANG",v);
		_Primlang=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Primlanx=null;

	/**
	 * @return Returns the PRIMLANX.
	 */
	public String getPrimlanx(){
		try{
			if (_Primlanx==null){
				_Primlanx=getStringProperty("PRIMLANX");
				return _Primlanx;
			}else {
				return _Primlanx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PRIMLANX.
	 * @param v Value to Set.
	 */
	public void setPrimlanx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PRIMLANX",v);
		_Primlanx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Livsit=null;

	/**
	 * @return Returns the LIVSIT.
	 */
	public Integer getLivsit() {
		try{
			if (_Livsit==null){
				_Livsit=getIntegerProperty("LIVSIT");
				return _Livsit;
			}else {
				return _Livsit;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LIVSIT.
	 * @param v Value to Set.
	 */
	public void setLivsit(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LIVSIT",v);
		_Livsit=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Livsitx=null;

	/**
	 * @return Returns the LIVSITX.
	 */
	public String getLivsitx(){
		try{
			if (_Livsitx==null){
				_Livsitx=getStringProperty("LIVSITX");
				return _Livsitx;
			}else {
				return _Livsitx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LIVSITX.
	 * @param v Value to Set.
	 */
	public void setLivsitx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LIVSITX",v);
		_Livsitx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Independ=null;

	/**
	 * @return Returns the INDEPEND.
	 */
	public Integer getIndepend() {
		try{
			if (_Independ==null){
				_Independ=getIntegerProperty("INDEPEND");
				return _Independ;
			}else {
				return _Independ;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INDEPEND.
	 * @param v Value to Set.
	 */
	public void setIndepend(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INDEPEND",v);
		_Independ=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Residenc=null;

	/**
	 * @return Returns the RESIDENC.
	 */
	public Integer getResidenc() {
		try{
			if (_Residenc==null){
				_Residenc=getIntegerProperty("RESIDENC");
				return _Residenc;
			}else {
				return _Residenc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RESIDENC.
	 * @param v Value to Set.
	 */
	public void setResidenc(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RESIDENC",v);
		_Residenc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Residenx=null;

	/**
	 * @return Returns the RESIDENX.
	 */
	public String getResidenx(){
		try{
			if (_Residenx==null){
				_Residenx=getStringProperty("RESIDENX");
				return _Residenx;
			}else {
				return _Residenx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RESIDENX.
	 * @param v Value to Set.
	 */
	public void setResidenx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RESIDENX",v);
		_Residenx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Zip=null;

	/**
	 * @return Returns the ZIP.
	 */
	public String getZip(){
		try{
			if (_Zip==null){
				_Zip=getStringProperty("ZIP");
				return _Zip;
			}else {
				return _Zip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ZIP.
	 * @param v Value to Set.
	 */
	public void setZip(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ZIP",v);
		_Zip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Maristat=null;

	/**
	 * @return Returns the MARISTAT.
	 */
	public Integer getMaristat() {
		try{
			if (_Maristat==null){
				_Maristat=getIntegerProperty("MARISTAT");
				return _Maristat;
			}else {
				return _Maristat;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MARISTAT.
	 * @param v Value to Set.
	 */
	public void setMaristat(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MARISTAT",v);
		_Maristat=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Maristax=null;

	/**
	 * @return Returns the MARISTAX.
	 */
	public String getMaristax(){
		try{
			if (_Maristax==null){
				_Maristax=getStringProperty("MARISTAX");
				return _Maristax;
			}else {
				return _Maristax;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MARISTAX.
	 * @param v Value to Set.
	 */
	public void setMaristax(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MARISTAX",v);
		_Maristax=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Handed=null;

	/**
	 * @return Returns the HANDED.
	 */
	public Integer getHanded() {
		try{
			if (_Handed==null){
				_Handed=getIntegerProperty("HANDED");
				return _Handed;
			}else {
				return _Handed;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HANDED.
	 * @param v Value to Set.
	 */
	public void setHanded(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HANDED",v);
		_Handed=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.UdsA1subdemodata> getAllUdsA1subdemodatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsA1subdemodata> al = new ArrayList<org.nrg.xdat.om.UdsA1subdemodata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsA1subdemodata> getUdsA1subdemodatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsA1subdemodata> al = new ArrayList<org.nrg.xdat.om.UdsA1subdemodata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsA1subdemodata> getUdsA1subdemodatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsA1subdemodata> al = new ArrayList<org.nrg.xdat.om.UdsA1subdemodata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static UdsA1subdemodata getUdsA1subdemodatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("uds:a1subdemoData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (UdsA1subdemodata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
