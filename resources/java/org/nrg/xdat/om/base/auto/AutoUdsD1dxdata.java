/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoUdsD1dxdata extends XnatSubjectassessordata implements org.nrg.xdat.model.UdsD1dxdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoUdsD1dxdata.class);
	public static String SCHEMA_ELEMENT_NAME="uds:d1dxData";

	public AutoUdsD1dxdata(ItemI item)
	{
		super(item);
	}

	public AutoUdsD1dxdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoUdsD1dxdata(UserI user)
	 **/
	public AutoUdsD1dxdata(){}

	public AutoUdsD1dxdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "uds:d1dxData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Whodiddx=null;

	/**
	 * @return Returns the WHODIDDX.
	 */
	public Integer getWhodiddx() {
		try{
			if (_Whodiddx==null){
				_Whodiddx=getIntegerProperty("WHODIDDX");
				return _Whodiddx;
			}else {
				return _Whodiddx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WHODIDDX.
	 * @param v Value to Set.
	 */
	public void setWhodiddx(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WHODIDDX",v);
		_Whodiddx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Normcog=null;

	/**
	 * @return Returns the NORMCOG.
	 */
	public Integer getNormcog() {
		try{
			if (_Normcog==null){
				_Normcog=getIntegerProperty("NORMCOG");
				return _Normcog;
			}else {
				return _Normcog;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NORMCOG.
	 * @param v Value to Set.
	 */
	public void setNormcog(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NORMCOG",v);
		_Normcog=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Demented=null;

	/**
	 * @return Returns the DEMENTED.
	 */
	public Integer getDemented() {
		try{
			if (_Demented==null){
				_Demented=getIntegerProperty("DEMENTED");
				return _Demented;
			}else {
				return _Demented;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DEMENTED.
	 * @param v Value to Set.
	 */
	public void setDemented(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DEMENTED",v);
		_Demented=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mciamem=null;

	/**
	 * @return Returns the MCIAMEM.
	 */
	public Integer getMciamem() {
		try{
			if (_Mciamem==null){
				_Mciamem=getIntegerProperty("MCIAMEM");
				return _Mciamem;
			}else {
				return _Mciamem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MCIAMEM.
	 * @param v Value to Set.
	 */
	public void setMciamem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MCIAMEM",v);
		_Mciamem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mciaplus=null;

	/**
	 * @return Returns the MCIAPLUS.
	 */
	public Integer getMciaplus() {
		try{
			if (_Mciaplus==null){
				_Mciaplus=getIntegerProperty("MCIAPLUS");
				return _Mciaplus;
			}else {
				return _Mciaplus;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MCIAPLUS.
	 * @param v Value to Set.
	 */
	public void setMciaplus(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MCIAPLUS",v);
		_Mciaplus=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mciaplan=null;

	/**
	 * @return Returns the MCIAPLAN.
	 */
	public Integer getMciaplan() {
		try{
			if (_Mciaplan==null){
				_Mciaplan=getIntegerProperty("MCIAPLAN");
				return _Mciaplan;
			}else {
				return _Mciaplan;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MCIAPLAN.
	 * @param v Value to Set.
	 */
	public void setMciaplan(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MCIAPLAN",v);
		_Mciaplan=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mciapatt=null;

	/**
	 * @return Returns the MCIAPATT.
	 */
	public Integer getMciapatt() {
		try{
			if (_Mciapatt==null){
				_Mciapatt=getIntegerProperty("MCIAPATT");
				return _Mciapatt;
			}else {
				return _Mciapatt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MCIAPATT.
	 * @param v Value to Set.
	 */
	public void setMciapatt(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MCIAPATT",v);
		_Mciapatt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mciapex=null;

	/**
	 * @return Returns the MCIAPEX.
	 */
	public Integer getMciapex() {
		try{
			if (_Mciapex==null){
				_Mciapex=getIntegerProperty("MCIAPEX");
				return _Mciapex;
			}else {
				return _Mciapex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MCIAPEX.
	 * @param v Value to Set.
	 */
	public void setMciapex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MCIAPEX",v);
		_Mciapex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mciapvis=null;

	/**
	 * @return Returns the MCIAPVIS.
	 */
	public Integer getMciapvis() {
		try{
			if (_Mciapvis==null){
				_Mciapvis=getIntegerProperty("MCIAPVIS");
				return _Mciapvis;
			}else {
				return _Mciapvis;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MCIAPVIS.
	 * @param v Value to Set.
	 */
	public void setMciapvis(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MCIAPVIS",v);
		_Mciapvis=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mcinon1=null;

	/**
	 * @return Returns the MCINON1.
	 */
	public Integer getMcinon1() {
		try{
			if (_Mcinon1==null){
				_Mcinon1=getIntegerProperty("MCINON1");
				return _Mcinon1;
			}else {
				return _Mcinon1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MCINON1.
	 * @param v Value to Set.
	 */
	public void setMcinon1(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MCINON1",v);
		_Mcinon1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mcin1lan=null;

	/**
	 * @return Returns the MCIN1LAN.
	 */
	public Integer getMcin1lan() {
		try{
			if (_Mcin1lan==null){
				_Mcin1lan=getIntegerProperty("MCIN1LAN");
				return _Mcin1lan;
			}else {
				return _Mcin1lan;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MCIN1LAN.
	 * @param v Value to Set.
	 */
	public void setMcin1lan(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MCIN1LAN",v);
		_Mcin1lan=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mcin1att=null;

	/**
	 * @return Returns the MCIN1ATT.
	 */
	public Integer getMcin1att() {
		try{
			if (_Mcin1att==null){
				_Mcin1att=getIntegerProperty("MCIN1ATT");
				return _Mcin1att;
			}else {
				return _Mcin1att;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MCIN1ATT.
	 * @param v Value to Set.
	 */
	public void setMcin1att(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MCIN1ATT",v);
		_Mcin1att=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mcin1ex=null;

	/**
	 * @return Returns the MCIN1EX.
	 */
	public Integer getMcin1ex() {
		try{
			if (_Mcin1ex==null){
				_Mcin1ex=getIntegerProperty("MCIN1EX");
				return _Mcin1ex;
			}else {
				return _Mcin1ex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MCIN1EX.
	 * @param v Value to Set.
	 */
	public void setMcin1ex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MCIN1EX",v);
		_Mcin1ex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mcin1vis=null;

	/**
	 * @return Returns the MCIN1VIS.
	 */
	public Integer getMcin1vis() {
		try{
			if (_Mcin1vis==null){
				_Mcin1vis=getIntegerProperty("MCIN1VIS");
				return _Mcin1vis;
			}else {
				return _Mcin1vis;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MCIN1VIS.
	 * @param v Value to Set.
	 */
	public void setMcin1vis(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MCIN1VIS",v);
		_Mcin1vis=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mcinon2=null;

	/**
	 * @return Returns the MCINON2.
	 */
	public Integer getMcinon2() {
		try{
			if (_Mcinon2==null){
				_Mcinon2=getIntegerProperty("MCINON2");
				return _Mcinon2;
			}else {
				return _Mcinon2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MCINON2.
	 * @param v Value to Set.
	 */
	public void setMcinon2(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MCINON2",v);
		_Mcinon2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mcin2lan=null;

	/**
	 * @return Returns the MCIN2LAN.
	 */
	public Integer getMcin2lan() {
		try{
			if (_Mcin2lan==null){
				_Mcin2lan=getIntegerProperty("MCIN2LAN");
				return _Mcin2lan;
			}else {
				return _Mcin2lan;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MCIN2LAN.
	 * @param v Value to Set.
	 */
	public void setMcin2lan(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MCIN2LAN",v);
		_Mcin2lan=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mcin2att=null;

	/**
	 * @return Returns the MCIN2ATT.
	 */
	public Integer getMcin2att() {
		try{
			if (_Mcin2att==null){
				_Mcin2att=getIntegerProperty("MCIN2ATT");
				return _Mcin2att;
			}else {
				return _Mcin2att;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MCIN2ATT.
	 * @param v Value to Set.
	 */
	public void setMcin2att(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MCIN2ATT",v);
		_Mcin2att=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mcin2ex=null;

	/**
	 * @return Returns the MCIN2EX.
	 */
	public Integer getMcin2ex() {
		try{
			if (_Mcin2ex==null){
				_Mcin2ex=getIntegerProperty("MCIN2EX");
				return _Mcin2ex;
			}else {
				return _Mcin2ex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MCIN2EX.
	 * @param v Value to Set.
	 */
	public void setMcin2ex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MCIN2EX",v);
		_Mcin2ex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mcin2vis=null;

	/**
	 * @return Returns the MCIN2VIS.
	 */
	public Integer getMcin2vis() {
		try{
			if (_Mcin2vis==null){
				_Mcin2vis=getIntegerProperty("MCIN2VIS");
				return _Mcin2vis;
			}else {
				return _Mcin2vis;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MCIN2VIS.
	 * @param v Value to Set.
	 */
	public void setMcin2vis(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MCIN2VIS",v);
		_Mcin2vis=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Impnomci=null;

	/**
	 * @return Returns the IMPNOMCI.
	 */
	public Integer getImpnomci() {
		try{
			if (_Impnomci==null){
				_Impnomci=getIntegerProperty("IMPNOMCI");
				return _Impnomci;
			}else {
				return _Impnomci;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for IMPNOMCI.
	 * @param v Value to Set.
	 */
	public void setImpnomci(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/IMPNOMCI",v);
		_Impnomci=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Probad=null;

	/**
	 * @return Returns the PROBAD.
	 */
	public Integer getProbad() {
		try{
			if (_Probad==null){
				_Probad=getIntegerProperty("PROBAD");
				return _Probad;
			}else {
				return _Probad;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PROBAD.
	 * @param v Value to Set.
	 */
	public void setProbad(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PROBAD",v);
		_Probad=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Probadif=null;

	/**
	 * @return Returns the PROBADIF.
	 */
	public Integer getProbadif() {
		try{
			if (_Probadif==null){
				_Probadif=getIntegerProperty("PROBADIF");
				return _Probadif;
			}else {
				return _Probadif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PROBADIF.
	 * @param v Value to Set.
	 */
	public void setProbadif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PROBADIF",v);
		_Probadif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Possad=null;

	/**
	 * @return Returns the POSSAD.
	 */
	public Integer getPossad() {
		try{
			if (_Possad==null){
				_Possad=getIntegerProperty("POSSAD");
				return _Possad;
			}else {
				return _Possad;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for POSSAD.
	 * @param v Value to Set.
	 */
	public void setPossad(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/POSSAD",v);
		_Possad=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Possadif=null;

	/**
	 * @return Returns the POSSADIF.
	 */
	public Integer getPossadif() {
		try{
			if (_Possadif==null){
				_Possadif=getIntegerProperty("POSSADIF");
				return _Possadif;
			}else {
				return _Possadif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for POSSADIF.
	 * @param v Value to Set.
	 */
	public void setPossadif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/POSSADIF",v);
		_Possadif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Dlb=null;

	/**
	 * @return Returns the DLB.
	 */
	public Integer getDlb() {
		try{
			if (_Dlb==null){
				_Dlb=getIntegerProperty("DLB");
				return _Dlb;
			}else {
				return _Dlb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DLB.
	 * @param v Value to Set.
	 */
	public void setDlb(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DLB",v);
		_Dlb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Dlbif=null;

	/**
	 * @return Returns the DLBIF.
	 */
	public Integer getDlbif() {
		try{
			if (_Dlbif==null){
				_Dlbif=getIntegerProperty("DLBIF");
				return _Dlbif;
			}else {
				return _Dlbif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DLBIF.
	 * @param v Value to Set.
	 */
	public void setDlbif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DLBIF",v);
		_Dlbif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Vasc=null;

	/**
	 * @return Returns the VASC.
	 */
	public Integer getVasc() {
		try{
			if (_Vasc==null){
				_Vasc=getIntegerProperty("VASC");
				return _Vasc;
			}else {
				return _Vasc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VASC.
	 * @param v Value to Set.
	 */
	public void setVasc(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VASC",v);
		_Vasc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Vascif=null;

	/**
	 * @return Returns the VASCIF.
	 */
	public Integer getVascif() {
		try{
			if (_Vascif==null){
				_Vascif=getIntegerProperty("VASCIF");
				return _Vascif;
			}else {
				return _Vascif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VASCIF.
	 * @param v Value to Set.
	 */
	public void setVascif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VASCIF",v);
		_Vascif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Vascps=null;

	/**
	 * @return Returns the VASCPS.
	 */
	public Integer getVascps() {
		try{
			if (_Vascps==null){
				_Vascps=getIntegerProperty("VASCPS");
				return _Vascps;
			}else {
				return _Vascps;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VASCPS.
	 * @param v Value to Set.
	 */
	public void setVascps(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VASCPS",v);
		_Vascps=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Vascpsif=null;

	/**
	 * @return Returns the VASCPSIF.
	 */
	public Integer getVascpsif() {
		try{
			if (_Vascpsif==null){
				_Vascpsif=getIntegerProperty("VASCPSIF");
				return _Vascpsif;
			}else {
				return _Vascpsif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VASCPSIF.
	 * @param v Value to Set.
	 */
	public void setVascpsif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VASCPSIF",v);
		_Vascpsif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Alcdem=null;

	/**
	 * @return Returns the ALCDEM.
	 */
	public Integer getAlcdem() {
		try{
			if (_Alcdem==null){
				_Alcdem=getIntegerProperty("ALCDEM");
				return _Alcdem;
			}else {
				return _Alcdem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ALCDEM.
	 * @param v Value to Set.
	 */
	public void setAlcdem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ALCDEM",v);
		_Alcdem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Alcdemif=null;

	/**
	 * @return Returns the ALCDEMIF.
	 */
	public Integer getAlcdemif() {
		try{
			if (_Alcdemif==null){
				_Alcdemif=getIntegerProperty("ALCDEMIF");
				return _Alcdemif;
			}else {
				return _Alcdemif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ALCDEMIF.
	 * @param v Value to Set.
	 */
	public void setAlcdemif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ALCDEMIF",v);
		_Alcdemif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Demun=null;

	/**
	 * @return Returns the DEMUN.
	 */
	public Integer getDemun() {
		try{
			if (_Demun==null){
				_Demun=getIntegerProperty("DEMUN");
				return _Demun;
			}else {
				return _Demun;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DEMUN.
	 * @param v Value to Set.
	 */
	public void setDemun(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DEMUN",v);
		_Demun=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Demunif=null;

	/**
	 * @return Returns the DEMUNIF.
	 */
	public Integer getDemunif() {
		try{
			if (_Demunif==null){
				_Demunif=getIntegerProperty("DEMUNIF");
				return _Demunif;
			}else {
				return _Demunif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DEMUNIF.
	 * @param v Value to Set.
	 */
	public void setDemunif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DEMUNIF",v);
		_Demunif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ftd=null;

	/**
	 * @return Returns the FTD.
	 */
	public Integer getFtd() {
		try{
			if (_Ftd==null){
				_Ftd=getIntegerProperty("FTD");
				return _Ftd;
			}else {
				return _Ftd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for FTD.
	 * @param v Value to Set.
	 */
	public void setFtd(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/FTD",v);
		_Ftd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ftdif=null;

	/**
	 * @return Returns the FTDIF.
	 */
	public Integer getFtdif() {
		try{
			if (_Ftdif==null){
				_Ftdif=getIntegerProperty("FTDIF");
				return _Ftdif;
			}else {
				return _Ftdif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for FTDIF.
	 * @param v Value to Set.
	 */
	public void setFtdif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/FTDIF",v);
		_Ftdif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ppaph=null;

	/**
	 * @return Returns the PPAPH.
	 */
	public Integer getPpaph() {
		try{
			if (_Ppaph==null){
				_Ppaph=getIntegerProperty("PPAPH");
				return _Ppaph;
			}else {
				return _Ppaph;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PPAPH.
	 * @param v Value to Set.
	 */
	public void setPpaph(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PPAPH",v);
		_Ppaph=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ppaphif=null;

	/**
	 * @return Returns the PPAPHIF.
	 */
	public Integer getPpaphif() {
		try{
			if (_Ppaphif==null){
				_Ppaphif=getIntegerProperty("PPAPHIF");
				return _Ppaphif;
			}else {
				return _Ppaphif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PPAPHIF.
	 * @param v Value to Set.
	 */
	public void setPpaphif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PPAPHIF",v);
		_Ppaphif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pnaph=null;

	/**
	 * @return Returns the PNAPH.
	 */
	public Integer getPnaph() {
		try{
			if (_Pnaph==null){
				_Pnaph=getIntegerProperty("PNAPH");
				return _Pnaph;
			}else {
				return _Pnaph;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PNAPH.
	 * @param v Value to Set.
	 */
	public void setPnaph(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PNAPH",v);
		_Pnaph=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Semdeman=null;

	/**
	 * @return Returns the SEMDEMAN.
	 */
	public Integer getSemdeman() {
		try{
			if (_Semdeman==null){
				_Semdeman=getIntegerProperty("SEMDEMAN");
				return _Semdeman;
			}else {
				return _Semdeman;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SEMDEMAN.
	 * @param v Value to Set.
	 */
	public void setSemdeman(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SEMDEMAN",v);
		_Semdeman=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Semdemag=null;

	/**
	 * @return Returns the SEMDEMAG.
	 */
	public Integer getSemdemag() {
		try{
			if (_Semdemag==null){
				_Semdemag=getIntegerProperty("SEMDEMAG");
				return _Semdemag;
			}else {
				return _Semdemag;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SEMDEMAG.
	 * @param v Value to Set.
	 */
	public void setSemdemag(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SEMDEMAG",v);
		_Semdemag=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ppaothr=null;

	/**
	 * @return Returns the PPAOTHR.
	 */
	public Integer getPpaothr() {
		try{
			if (_Ppaothr==null){
				_Ppaothr=getIntegerProperty("PPAOTHR");
				return _Ppaothr;
			}else {
				return _Ppaothr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PPAOTHR.
	 * @param v Value to Set.
	 */
	public void setPpaothr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PPAOTHR",v);
		_Ppaothr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Psp=null;

	/**
	 * @return Returns the PSP.
	 */
	public Integer getPsp() {
		try{
			if (_Psp==null){
				_Psp=getIntegerProperty("PSP");
				return _Psp;
			}else {
				return _Psp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PSP.
	 * @param v Value to Set.
	 */
	public void setPsp(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PSP",v);
		_Psp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pspif=null;

	/**
	 * @return Returns the PSPIF.
	 */
	public Integer getPspif() {
		try{
			if (_Pspif==null){
				_Pspif=getIntegerProperty("PSPIF");
				return _Pspif;
			}else {
				return _Pspif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PSPIF.
	 * @param v Value to Set.
	 */
	public void setPspif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PSPIF",v);
		_Pspif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cort=null;

	/**
	 * @return Returns the CORT.
	 */
	public Integer getCort() {
		try{
			if (_Cort==null){
				_Cort=getIntegerProperty("CORT");
				return _Cort;
			}else {
				return _Cort;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CORT.
	 * @param v Value to Set.
	 */
	public void setCort(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CORT",v);
		_Cort=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cortif=null;

	/**
	 * @return Returns the CORTIF.
	 */
	public Integer getCortif() {
		try{
			if (_Cortif==null){
				_Cortif=getIntegerProperty("CORTIF");
				return _Cortif;
			}else {
				return _Cortif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CORTIF.
	 * @param v Value to Set.
	 */
	public void setCortif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CORTIF",v);
		_Cortif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Hunt=null;

	/**
	 * @return Returns the HUNT.
	 */
	public Integer getHunt() {
		try{
			if (_Hunt==null){
				_Hunt=getIntegerProperty("HUNT");
				return _Hunt;
			}else {
				return _Hunt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HUNT.
	 * @param v Value to Set.
	 */
	public void setHunt(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HUNT",v);
		_Hunt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Huntif=null;

	/**
	 * @return Returns the HUNTIF.
	 */
	public Integer getHuntif() {
		try{
			if (_Huntif==null){
				_Huntif=getIntegerProperty("HUNTIF");
				return _Huntif;
			}else {
				return _Huntif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HUNTIF.
	 * @param v Value to Set.
	 */
	public void setHuntif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HUNTIF",v);
		_Huntif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Prion=null;

	/**
	 * @return Returns the PRION.
	 */
	public Integer getPrion() {
		try{
			if (_Prion==null){
				_Prion=getIntegerProperty("PRION");
				return _Prion;
			}else {
				return _Prion;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PRION.
	 * @param v Value to Set.
	 */
	public void setPrion(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PRION",v);
		_Prion=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Prionif=null;

	/**
	 * @return Returns the PRIONIF.
	 */
	public Integer getPrionif() {
		try{
			if (_Prionif==null){
				_Prionif=getIntegerProperty("PRIONIF");
				return _Prionif;
			}else {
				return _Prionif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PRIONIF.
	 * @param v Value to Set.
	 */
	public void setPrionif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PRIONIF",v);
		_Prionif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Meds=null;

	/**
	 * @return Returns the MEDS.
	 */
	public Integer getMeds() {
		try{
			if (_Meds==null){
				_Meds=getIntegerProperty("MEDS");
				return _Meds;
			}else {
				return _Meds;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MEDS.
	 * @param v Value to Set.
	 */
	public void setMeds(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MEDS",v);
		_Meds=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Medsif=null;

	/**
	 * @return Returns the MEDSIF.
	 */
	public Integer getMedsif() {
		try{
			if (_Medsif==null){
				_Medsif=getIntegerProperty("MEDSIF");
				return _Medsif;
			}else {
				return _Medsif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MEDSIF.
	 * @param v Value to Set.
	 */
	public void setMedsif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MEDSIF",v);
		_Medsif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Dysill=null;

	/**
	 * @return Returns the DYSILL.
	 */
	public Integer getDysill() {
		try{
			if (_Dysill==null){
				_Dysill=getIntegerProperty("DYSILL");
				return _Dysill;
			}else {
				return _Dysill;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DYSILL.
	 * @param v Value to Set.
	 */
	public void setDysill(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DYSILL",v);
		_Dysill=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Dysillif=null;

	/**
	 * @return Returns the DYSILLIF.
	 */
	public Integer getDysillif() {
		try{
			if (_Dysillif==null){
				_Dysillif=getIntegerProperty("DYSILLIF");
				return _Dysillif;
			}else {
				return _Dysillif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DYSILLIF.
	 * @param v Value to Set.
	 */
	public void setDysillif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DYSILLIF",v);
		_Dysillif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Dep=null;

	/**
	 * @return Returns the DEP.
	 */
	public Integer getDep() {
		try{
			if (_Dep==null){
				_Dep=getIntegerProperty("DEP");
				return _Dep;
			}else {
				return _Dep;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DEP.
	 * @param v Value to Set.
	 */
	public void setDep(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DEP",v);
		_Dep=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Depif=null;

	/**
	 * @return Returns the DEPIF.
	 */
	public Integer getDepif() {
		try{
			if (_Depif==null){
				_Depif=getIntegerProperty("DEPIF");
				return _Depif;
			}else {
				return _Depif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DEPIF.
	 * @param v Value to Set.
	 */
	public void setDepif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DEPIF",v);
		_Depif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Othpsy=null;

	/**
	 * @return Returns the OTHPSY.
	 */
	public Integer getOthpsy() {
		try{
			if (_Othpsy==null){
				_Othpsy=getIntegerProperty("OTHPSY");
				return _Othpsy;
			}else {
				return _Othpsy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OTHPSY.
	 * @param v Value to Set.
	 */
	public void setOthpsy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OTHPSY",v);
		_Othpsy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Othpsyif=null;

	/**
	 * @return Returns the OTHPSYIF.
	 */
	public Integer getOthpsyif() {
		try{
			if (_Othpsyif==null){
				_Othpsyif=getIntegerProperty("OTHPSYIF");
				return _Othpsyif;
			}else {
				return _Othpsyif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OTHPSYIF.
	 * @param v Value to Set.
	 */
	public void setOthpsyif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OTHPSYIF",v);
		_Othpsyif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Downs=null;

	/**
	 * @return Returns the DOWNS.
	 */
	public Integer getDowns() {
		try{
			if (_Downs==null){
				_Downs=getIntegerProperty("DOWNS");
				return _Downs;
			}else {
				return _Downs;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DOWNS.
	 * @param v Value to Set.
	 */
	public void setDowns(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DOWNS",v);
		_Downs=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Downsif=null;

	/**
	 * @return Returns the DOWNSIF.
	 */
	public Integer getDownsif() {
		try{
			if (_Downsif==null){
				_Downsif=getIntegerProperty("DOWNSIF");
				return _Downsif;
			}else {
				return _Downsif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DOWNSIF.
	 * @param v Value to Set.
	 */
	public void setDownsif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DOWNSIF",v);
		_Downsif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Park=null;

	/**
	 * @return Returns the PARK.
	 */
	public Integer getPark() {
		try{
			if (_Park==null){
				_Park=getIntegerProperty("PARK");
				return _Park;
			}else {
				return _Park;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PARK.
	 * @param v Value to Set.
	 */
	public void setPark(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PARK",v);
		_Park=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Parkif=null;

	/**
	 * @return Returns the PARKIF.
	 */
	public Integer getParkif() {
		try{
			if (_Parkif==null){
				_Parkif=getIntegerProperty("PARKIF");
				return _Parkif;
			}else {
				return _Parkif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PARKIF.
	 * @param v Value to Set.
	 */
	public void setParkif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PARKIF",v);
		_Parkif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Stroke=null;

	/**
	 * @return Returns the STROKE.
	 */
	public Integer getStroke() {
		try{
			if (_Stroke==null){
				_Stroke=getIntegerProperty("STROKE");
				return _Stroke;
			}else {
				return _Stroke;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for STROKE.
	 * @param v Value to Set.
	 */
	public void setStroke(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/STROKE",v);
		_Stroke=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Strokif=null;

	/**
	 * @return Returns the STROKIF.
	 */
	public Integer getStrokif() {
		try{
			if (_Strokif==null){
				_Strokif=getIntegerProperty("STROKIF");
				return _Strokif;
			}else {
				return _Strokif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for STROKIF.
	 * @param v Value to Set.
	 */
	public void setStrokif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/STROKIF",v);
		_Strokif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Hyceph=null;

	/**
	 * @return Returns the HYCEPH.
	 */
	public Integer getHyceph() {
		try{
			if (_Hyceph==null){
				_Hyceph=getIntegerProperty("HYCEPH");
				return _Hyceph;
			}else {
				return _Hyceph;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HYCEPH.
	 * @param v Value to Set.
	 */
	public void setHyceph(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HYCEPH",v);
		_Hyceph=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Hycephif=null;

	/**
	 * @return Returns the HYCEPHIF.
	 */
	public Integer getHycephif() {
		try{
			if (_Hycephif==null){
				_Hycephif=getIntegerProperty("HYCEPHIF");
				return _Hycephif;
			}else {
				return _Hycephif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HYCEPHIF.
	 * @param v Value to Set.
	 */
	public void setHycephif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HYCEPHIF",v);
		_Hycephif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Brninj=null;

	/**
	 * @return Returns the BRNINJ.
	 */
	public Integer getBrninj() {
		try{
			if (_Brninj==null){
				_Brninj=getIntegerProperty("BRNINJ");
				return _Brninj;
			}else {
				return _Brninj;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BRNINJ.
	 * @param v Value to Set.
	 */
	public void setBrninj(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BRNINJ",v);
		_Brninj=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Brninjif=null;

	/**
	 * @return Returns the BRNINJIF.
	 */
	public Integer getBrninjif() {
		try{
			if (_Brninjif==null){
				_Brninjif=getIntegerProperty("BRNINJIF");
				return _Brninjif;
			}else {
				return _Brninjif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BRNINJIF.
	 * @param v Value to Set.
	 */
	public void setBrninjif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BRNINJIF",v);
		_Brninjif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Neop=null;

	/**
	 * @return Returns the NEOP.
	 */
	public Integer getNeop() {
		try{
			if (_Neop==null){
				_Neop=getIntegerProperty("NEOP");
				return _Neop;
			}else {
				return _Neop;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NEOP.
	 * @param v Value to Set.
	 */
	public void setNeop(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NEOP",v);
		_Neop=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Neopif=null;

	/**
	 * @return Returns the NEOPIF.
	 */
	public Integer getNeopif() {
		try{
			if (_Neopif==null){
				_Neopif=getIntegerProperty("NEOPIF");
				return _Neopif;
			}else {
				return _Neopif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NEOPIF.
	 * @param v Value to Set.
	 */
	public void setNeopif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NEOPIF",v);
		_Neopif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cogoth=null;

	/**
	 * @return Returns the COGOTH.
	 */
	public Integer getCogoth() {
		try{
			if (_Cogoth==null){
				_Cogoth=getIntegerProperty("COGOTH");
				return _Cogoth;
			}else {
				return _Cogoth;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COGOTH.
	 * @param v Value to Set.
	 */
	public void setCogoth(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COGOTH",v);
		_Cogoth=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Cogothx=null;

	/**
	 * @return Returns the COGOTHX.
	 */
	public String getCogothx(){
		try{
			if (_Cogothx==null){
				_Cogothx=getStringProperty("COGOTHX");
				return _Cogothx;
			}else {
				return _Cogothx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COGOTHX.
	 * @param v Value to Set.
	 */
	public void setCogothx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COGOTHX",v);
		_Cogothx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cogothif=null;

	/**
	 * @return Returns the COGOTHIF.
	 */
	public Integer getCogothif() {
		try{
			if (_Cogothif==null){
				_Cogothif=getIntegerProperty("COGOTHIF");
				return _Cogothif;
			}else {
				return _Cogothif;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COGOTHIF.
	 * @param v Value to Set.
	 */
	public void setCogothif(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COGOTHIF",v);
		_Cogothif=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cogoth2=null;

	/**
	 * @return Returns the COGOTH2.
	 */
	public Integer getCogoth2() {
		try{
			if (_Cogoth2==null){
				_Cogoth2=getIntegerProperty("COGOTH2");
				return _Cogoth2;
			}else {
				return _Cogoth2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COGOTH2.
	 * @param v Value to Set.
	 */
	public void setCogoth2(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COGOTH2",v);
		_Cogoth2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Cogoth2x=null;

	/**
	 * @return Returns the COGOTH2X.
	 */
	public String getCogoth2x(){
		try{
			if (_Cogoth2x==null){
				_Cogoth2x=getStringProperty("COGOTH2X");
				return _Cogoth2x;
			}else {
				return _Cogoth2x;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COGOTH2X.
	 * @param v Value to Set.
	 */
	public void setCogoth2x(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COGOTH2X",v);
		_Cogoth2x=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cogoth2f=null;

	/**
	 * @return Returns the COGOTH2F.
	 */
	public Integer getCogoth2f() {
		try{
			if (_Cogoth2f==null){
				_Cogoth2f=getIntegerProperty("COGOTH2F");
				return _Cogoth2f;
			}else {
				return _Cogoth2f;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COGOTH2F.
	 * @param v Value to Set.
	 */
	public void setCogoth2f(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COGOTH2F",v);
		_Cogoth2f=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cogoth3=null;

	/**
	 * @return Returns the COGOTH3.
	 */
	public Integer getCogoth3() {
		try{
			if (_Cogoth3==null){
				_Cogoth3=getIntegerProperty("COGOTH3");
				return _Cogoth3;
			}else {
				return _Cogoth3;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COGOTH3.
	 * @param v Value to Set.
	 */
	public void setCogoth3(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COGOTH3",v);
		_Cogoth3=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Cogoth3x=null;

	/**
	 * @return Returns the COGOTH3X.
	 */
	public String getCogoth3x(){
		try{
			if (_Cogoth3x==null){
				_Cogoth3x=getStringProperty("COGOTH3X");
				return _Cogoth3x;
			}else {
				return _Cogoth3x;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COGOTH3X.
	 * @param v Value to Set.
	 */
	public void setCogoth3x(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COGOTH3X",v);
		_Cogoth3x=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cogoth3f=null;

	/**
	 * @return Returns the COGOTH3F.
	 */
	public Integer getCogoth3f() {
		try{
			if (_Cogoth3f==null){
				_Cogoth3f=getIntegerProperty("COGOTH3F");
				return _Cogoth3f;
			}else {
				return _Cogoth3f;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COGOTH3F.
	 * @param v Value to Set.
	 */
	public void setCogoth3f(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COGOTH3F",v);
		_Cogoth3f=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.UdsD1dxdata> getAllUdsD1dxdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsD1dxdata> al = new ArrayList<org.nrg.xdat.om.UdsD1dxdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsD1dxdata> getUdsD1dxdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsD1dxdata> al = new ArrayList<org.nrg.xdat.om.UdsD1dxdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsD1dxdata> getUdsD1dxdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsD1dxdata> al = new ArrayList<org.nrg.xdat.om.UdsD1dxdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static UdsD1dxdata getUdsD1dxdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("uds:d1dxData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (UdsD1dxdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
