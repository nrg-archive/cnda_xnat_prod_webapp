/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaDtiregion extends XnatVolumetricregion implements org.nrg.xdat.model.CndaDtiregionI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaDtiregion.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:dtiRegion";

	public AutoCndaDtiregion(ItemI item)
	{
		super(item);
	}

	public AutoCndaDtiregion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaDtiregion(UserI user)
	 **/
	public AutoCndaDtiregion(){}

	public AutoCndaDtiregion(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:dtiRegion";
	}
	 private org.nrg.xdat.om.XnatVolumetricregion _Volumetricregion =null;

	/**
	 * volumetricRegion
	 * @return org.nrg.xdat.om.XnatVolumetricregion
	 */
	public org.nrg.xdat.om.XnatVolumetricregion getVolumetricregion() {
		try{
			if (_Volumetricregion==null){
				_Volumetricregion=((XnatVolumetricregion)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("volumetricRegion")));
				return _Volumetricregion;
			}else {
				return _Volumetricregion;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for volumetricRegion.
	 * @param v Value to Set.
	 */
	public void setVolumetricregion(ItemI v) throws Exception{
		_Volumetricregion =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/volumetricRegion",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/volumetricRegion",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * volumetricRegion
	 * set org.nrg.xdat.model.XnatVolumetricregionI
	 */
	public <A extends org.nrg.xdat.model.XnatVolumetricregionI> void setVolumetricregion(A item) throws Exception{
	setVolumetricregion((ItemI)item);
	}

	/**
	 * Removes the volumetricRegion.
	 * */
	public void removeVolumetricregion() {
		_Volumetricregion =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/volumetricRegion",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Manisotropy=null;

	/**
	 * @return Returns the mAnisotropy.
	 */
	public Double getManisotropy() {
		try{
			if (_Manisotropy==null){
				_Manisotropy=getDoubleProperty("mAnisotropy");
				return _Manisotropy;
			}else {
				return _Manisotropy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for mAnisotropy.
	 * @param v Value to Set.
	 */
	public void setManisotropy(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/mAnisotropy",v);
		_Manisotropy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Mdiffusivity=null;

	/**
	 * @return Returns the mDiffusivity.
	 */
	public Double getMdiffusivity() {
		try{
			if (_Mdiffusivity==null){
				_Mdiffusivity=getDoubleProperty("mDiffusivity");
				return _Mdiffusivity;
			}else {
				return _Mdiffusivity;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for mDiffusivity.
	 * @param v Value to Set.
	 */
	public void setMdiffusivity(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/mDiffusivity",v);
		_Mdiffusivity=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Imagefile=null;

	/**
	 * @return Returns the imageFile.
	 */
	public String getImagefile(){
		try{
			if (_Imagefile==null){
				_Imagefile=getStringProperty("imageFile");
				return _Imagefile;
			}else {
				return _Imagefile;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for imageFile.
	 * @param v Value to Set.
	 */
	public void setImagefile(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/imageFile",v);
		_Imagefile=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Statsfile=null;

	/**
	 * @return Returns the statsFile.
	 */
	public String getStatsfile(){
		try{
			if (_Statsfile==null){
				_Statsfile=getStringProperty("statsFile");
				return _Statsfile;
			}else {
				return _Statsfile;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for statsFile.
	 * @param v Value to Set.
	 */
	public void setStatsfile(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/statsFile",v);
		_Statsfile=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaDtiregion> getAllCndaDtiregions(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaDtiregion> al = new ArrayList<org.nrg.xdat.om.CndaDtiregion>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaDtiregion> getCndaDtiregionsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaDtiregion> al = new ArrayList<org.nrg.xdat.om.CndaDtiregion>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaDtiregion> getCndaDtiregionsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaDtiregion> al = new ArrayList<org.nrg.xdat.om.CndaDtiregion>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaDtiregion getCndaDtiregionsByXnatVolumetricregionId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:dtiRegion/xnat_volumetricregion_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaDtiregion) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //volumetricRegion
	        XnatVolumetricregion childVolumetricregion = (XnatVolumetricregion)this.getVolumetricregion();
	            if (childVolumetricregion!=null){
	              for(ResourceFile rf: ((XnatVolumetricregion)childVolumetricregion).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("volumetricRegion[" + ((XnatVolumetricregion)childVolumetricregion).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("volumetricRegion/" + ((XnatVolumetricregion)childVolumetricregion).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
