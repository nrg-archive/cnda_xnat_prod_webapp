/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:01 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoFsAsegregionanalysis extends XnatMrassessordata implements org.nrg.xdat.model.FsAsegregionanalysisI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoFsAsegregionanalysis.class);
	public static String SCHEMA_ELEMENT_NAME="fs:asegRegionAnalysis";

	public AutoFsAsegregionanalysis(ItemI item)
	{
		super(item);
	}

	public AutoFsAsegregionanalysis(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoFsAsegregionanalysis(UserI user)
	 **/
	public AutoFsAsegregionanalysis(){}

	public AutoFsAsegregionanalysis(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "fs:asegRegionAnalysis";
	}
	 private org.nrg.xdat.om.XnatMrassessordata _Mrassessordata =null;

	/**
	 * mrAssessorData
	 * @return org.nrg.xdat.om.XnatMrassessordata
	 */
	public org.nrg.xdat.om.XnatMrassessordata getMrassessordata() {
		try{
			if (_Mrassessordata==null){
				_Mrassessordata=((XnatMrassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("mrAssessorData")));
				return _Mrassessordata;
			}else {
				return _Mrassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for mrAssessorData.
	 * @param v Value to Set.
	 */
	public void setMrassessordata(ItemI v) throws Exception{
		_Mrassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/mrAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/mrAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * mrAssessorData
	 * set org.nrg.xdat.model.XnatMrassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatMrassessordataI> void setMrassessordata(A item) throws Exception{
	setMrassessordata((ItemI)item);
	}

	/**
	 * Removes the mrAssessorData.
	 * */
	public void removeMrassessordata() {
		_Mrassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/mrAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Brainmasknvox=null;

	/**
	 * @return Returns the BrainMaskNVox.
	 */
	public Double getBrainmasknvox() {
		try{
			if (_Brainmasknvox==null){
				_Brainmasknvox=getDoubleProperty("BrainMaskNVox");
				return _Brainmasknvox;
			}else {
				return _Brainmasknvox;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BrainMaskNVox.
	 * @param v Value to Set.
	 */
	public void setBrainmasknvox(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BrainMaskNVox",v);
		_Brainmasknvox=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Brainmaskvol=null;

	/**
	 * @return Returns the BrainMaskVol.
	 */
	public Double getBrainmaskvol() {
		try{
			if (_Brainmaskvol==null){
				_Brainmaskvol=getDoubleProperty("BrainMaskVol");
				return _Brainmaskvol;
			}else {
				return _Brainmaskvol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BrainMaskVol.
	 * @param v Value to Set.
	 */
	public void setBrainmaskvol(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BrainMaskVol",v);
		_Brainmaskvol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Brainsegnvox=null;

	/**
	 * @return Returns the BrainSegNVox.
	 */
	public Double getBrainsegnvox() {
		try{
			if (_Brainsegnvox==null){
				_Brainsegnvox=getDoubleProperty("BrainSegNVox");
				return _Brainsegnvox;
			}else {
				return _Brainsegnvox;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BrainSegNVox.
	 * @param v Value to Set.
	 */
	public void setBrainsegnvox(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BrainSegNVox",v);
		_Brainsegnvox=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Brainsegvol=null;

	/**
	 * @return Returns the BrainSegVol.
	 */
	public Double getBrainsegvol() {
		try{
			if (_Brainsegvol==null){
				_Brainsegvol=getDoubleProperty("BrainSegVol");
				return _Brainsegvol;
			}else {
				return _Brainsegvol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BrainSegVol.
	 * @param v Value to Set.
	 */
	public void setBrainsegvol(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BrainSegVol",v);
		_Brainsegvol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Icv=null;

	/**
	 * @return Returns the ICV.
	 */
	public Double getIcv() {
		try{
			if (_Icv==null){
				_Icv=getDoubleProperty("ICV");
				return _Icv;
			}else {
				return _Icv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ICV.
	 * @param v Value to Set.
	 */
	public void setIcv(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ICV",v);
		_Icv=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.FsAsegregionanalysisRegion> _Regions_region =null;

	/**
	 * regions/region
	 * @return Returns an List of org.nrg.xdat.om.FsAsegregionanalysisRegion
	 */
	public <A extends org.nrg.xdat.model.FsAsegregionanalysisRegionI> List<A> getRegions_region() {
		try{
			if (_Regions_region==null){
				_Regions_region=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("regions/region"));
			}
			return (List<A>) _Regions_region;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.FsAsegregionanalysisRegion>();}
	}

	/**
	 * Sets the value for regions/region.
	 * @param v Value to Set.
	 */
	public void setRegions_region(ItemI v) throws Exception{
		_Regions_region =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/regions/region",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/regions/region",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * regions/region
	 * Adds org.nrg.xdat.model.FsAsegregionanalysisRegionI
	 */
	public <A extends org.nrg.xdat.model.FsAsegregionanalysisRegionI> void addRegions_region(A item) throws Exception{
	setRegions_region((ItemI)item);
	}

	/**
	 * Removes the regions/region of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeRegions_region(int index) throws java.lang.IndexOutOfBoundsException {
		_Regions_region =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/regions/region",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.FsAsegregionanalysis> getAllFsAsegregionanalysiss(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.FsAsegregionanalysis> al = new ArrayList<org.nrg.xdat.om.FsAsegregionanalysis>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.FsAsegregionanalysis> getFsAsegregionanalysissByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.FsAsegregionanalysis> al = new ArrayList<org.nrg.xdat.om.FsAsegregionanalysis>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.FsAsegregionanalysis> getFsAsegregionanalysissByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.FsAsegregionanalysis> al = new ArrayList<org.nrg.xdat.om.FsAsegregionanalysis>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static FsAsegregionanalysis getFsAsegregionanalysissById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("fs:asegRegionAnalysis/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (FsAsegregionanalysis) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		XFTItem mr = org.nrg.xft.search.ItemSearch.GetItem("xnat:mrSessionData.ID",getItem().getProperty("fs:asegRegionAnalysis.imageSession_ID"),getItem().getUser(),false);
		al.add(mr);
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",mr.getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //mrAssessorData
	        XnatMrassessordata childMrassessordata = (XnatMrassessordata)this.getMrassessordata();
	            if (childMrassessordata!=null){
	              for(ResourceFile rf: ((XnatMrassessordata)childMrassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("mrAssessorData[" + ((XnatMrassessordata)childMrassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("mrAssessorData/" + ((XnatMrassessordata)childMrassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //regions/region
	        for(org.nrg.xdat.model.FsAsegregionanalysisRegionI childRegions_region : this.getRegions_region()){
	            if (childRegions_region!=null){
	              for(ResourceFile rf: ((FsAsegregionanalysisRegion)childRegions_region).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("regions/region[" + ((FsAsegregionanalysisRegion)childRegions_region).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("regions/region/" + ((FsAsegregionanalysisRegion)childRegions_region).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
