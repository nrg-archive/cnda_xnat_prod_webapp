/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:01 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianPibmetadata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianPibmetadataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianPibmetadata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:pibmetaData";

	public AutoDianPibmetadata(ItemI item)
	{
		super(item);
	}

	public AutoDianPibmetadata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianPibmetadata(UserI user)
	 **/
	public AutoDianPibmetadata(){}

	public AutoDianPibmetadata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:pibmetaData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scandone=null;

	/**
	 * @return Returns the SCANDONE.
	 */
	public Integer getScandone() {
		try{
			if (_Scandone==null){
				_Scandone=getIntegerProperty("SCANDONE");
				return _Scandone;
			}else {
				return _Scandone;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SCANDONE.
	 * @param v Value to Set.
	 */
	public void setScandone(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SCANDONE",v);
		_Scandone=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Scandate=null;

	/**
	 * @return Returns the SCANDATE.
	 */
	public Object getScandate(){
		try{
			if (_Scandate==null){
				_Scandate=getProperty("SCANDATE");
				return _Scandate;
			}else {
				return _Scandate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SCANDATE.
	 * @param v Value to Set.
	 */
	public void setScandate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SCANDATE",v);
		_Scandate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Ndreason=null;

	/**
	 * @return Returns the NDREASON.
	 */
	public String getNdreason(){
		try{
			if (_Ndreason==null){
				_Ndreason=getStringProperty("NDREASON");
				return _Ndreason;
			}else {
				return _Ndreason;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NDREASON.
	 * @param v Value to Set.
	 */
	public void setNdreason(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NDREASON",v);
		_Ndreason=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Ndother=null;

	/**
	 * @return Returns the NDOTHER.
	 */
	public String getNdother(){
		try{
			if (_Ndother==null){
				_Ndother=getStringProperty("NDOTHER");
				return _Ndother;
			}else {
				return _Ndother;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NDOTHER.
	 * @param v Value to Set.
	 */
	public void setNdother(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NDOTHER",v);
		_Ndother=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianPibmetadata> getAllDianPibmetadatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianPibmetadata> al = new ArrayList<org.nrg.xdat.om.DianPibmetadata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianPibmetadata> getDianPibmetadatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianPibmetadata> al = new ArrayList<org.nrg.xdat.om.DianPibmetadata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianPibmetadata> getDianPibmetadatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianPibmetadata> al = new ArrayList<org.nrg.xdat.om.DianPibmetadata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianPibmetadata getDianPibmetadatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:pibmetaData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianPibmetadata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
