/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaAtrophynildata extends XnatMrassessordata implements org.nrg.xdat.model.CndaAtrophynildataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaAtrophynildata.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:atrophyNilData";

	public AutoCndaAtrophynildata(ItemI item)
	{
		super(item);
	}

	public AutoCndaAtrophynildata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaAtrophynildata(UserI user)
	 **/
	public AutoCndaAtrophynildata(){}

	public AutoCndaAtrophynildata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:atrophyNilData";
	}
	 private org.nrg.xdat.om.XnatMrassessordata _Mrassessordata =null;

	/**
	 * mrAssessorData
	 * @return org.nrg.xdat.om.XnatMrassessordata
	 */
	public org.nrg.xdat.om.XnatMrassessordata getMrassessordata() {
		try{
			if (_Mrassessordata==null){
				_Mrassessordata=((XnatMrassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("mrAssessorData")));
				return _Mrassessordata;
			}else {
				return _Mrassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for mrAssessorData.
	 * @param v Value to Set.
	 */
	public void setMrassessordata(ItemI v) throws Exception{
		_Mrassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/mrAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/mrAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * mrAssessorData
	 * set org.nrg.xdat.model.XnatMrassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatMrassessordataI> void setMrassessordata(A item) throws Exception{
	setMrassessordata((ItemI)item);
	}

	/**
	 * Removes the mrAssessorData.
	 * */
	public void removeMrassessordata() {
		_Mrassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/mrAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.XnatVolumetricregion> _Regions_region =null;

	/**
	 * regions/region
	 * @return Returns an List of org.nrg.xdat.om.XnatVolumetricregion
	 */
	public <A extends org.nrg.xdat.model.XnatVolumetricregionI> List<A> getRegions_region() {
		try{
			if (_Regions_region==null){
				_Regions_region=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("regions/region"));
			}
			return (List<A>) _Regions_region;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.XnatVolumetricregion>();}
	}

	/**
	 * Sets the value for regions/region.
	 * @param v Value to Set.
	 */
	public void setRegions_region(ItemI v) throws Exception{
		_Regions_region =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/regions/region",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/regions/region",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * regions/region
	 * Adds org.nrg.xdat.model.XnatVolumetricregionI
	 */
	public <A extends org.nrg.xdat.model.XnatVolumetricregionI> void addRegions_region(A item) throws Exception{
	setRegions_region((ItemI)item);
	}

	/**
	 * Removes the regions/region of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeRegions_region(int index) throws java.lang.IndexOutOfBoundsException {
		_Regions_region =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/regions/region",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.CndaAtrophynildataPeak> _Peaks_peak =null;

	/**
	 * peaks/peak
	 * @return Returns an List of org.nrg.xdat.om.CndaAtrophynildataPeak
	 */
	public <A extends org.nrg.xdat.model.CndaAtrophynildataPeakI> List<A> getPeaks_peak() {
		try{
			if (_Peaks_peak==null){
				_Peaks_peak=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("peaks/peak"));
			}
			return (List<A>) _Peaks_peak;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.CndaAtrophynildataPeak>();}
	}

	/**
	 * Sets the value for peaks/peak.
	 * @param v Value to Set.
	 */
	public void setPeaks_peak(ItemI v) throws Exception{
		_Peaks_peak =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/peaks/peak",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/peaks/peak",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * peaks/peak
	 * Adds org.nrg.xdat.model.CndaAtrophynildataPeakI
	 */
	public <A extends org.nrg.xdat.model.CndaAtrophynildataPeakI> void addPeaks_peak(A item) throws Exception{
	setPeaks_peak((ItemI)item);
	}

	/**
	 * Removes the peaks/peak of the given index.
	 * @param index Index of child to remove.
	 */
	public void removePeaks_peak(int index) throws java.lang.IndexOutOfBoundsException {
		_Peaks_peak =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/peaks/peak",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CsfRatio_csf=null;

	/**
	 * @return Returns the csf_ratio/csf.
	 */
	public Double getCsfRatio_csf() {
		try{
			if (_CsfRatio_csf==null){
				_CsfRatio_csf=getDoubleProperty("csf_ratio/csf");
				return _CsfRatio_csf;
			}else {
				return _CsfRatio_csf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for csf_ratio/csf.
	 * @param v Value to Set.
	 */
	public void setCsfRatio_csf(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/csf_ratio/csf",v);
		_CsfRatio_csf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CsfRatio_total=null;

	/**
	 * @return Returns the csf_ratio/total.
	 */
	public Double getCsfRatio_total() {
		try{
			if (_CsfRatio_total==null){
				_CsfRatio_total=getDoubleProperty("csf_ratio/total");
				return _CsfRatio_total;
			}else {
				return _CsfRatio_total;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for csf_ratio/total.
	 * @param v Value to Set.
	 */
	public void setCsfRatio_total(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/csf_ratio/total",v);
		_CsfRatio_total=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CsfRatio_value=null;

	/**
	 * @return Returns the csf_ratio/value.
	 */
	public Double getCsfRatio_value() {
		try{
			if (_CsfRatio_value==null){
				_CsfRatio_value=getDoubleProperty("csf_ratio/value");
				return _CsfRatio_value;
			}else {
				return _CsfRatio_value;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for csf_ratio/value.
	 * @param v Value to Set.
	 */
	public void setCsfRatio_value(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/csf_ratio/value",v);
		_CsfRatio_value=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaAtrophynildata> getAllCndaAtrophynildatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaAtrophynildata> al = new ArrayList<org.nrg.xdat.om.CndaAtrophynildata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaAtrophynildata> getCndaAtrophynildatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaAtrophynildata> al = new ArrayList<org.nrg.xdat.om.CndaAtrophynildata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaAtrophynildata> getCndaAtrophynildatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaAtrophynildata> al = new ArrayList<org.nrg.xdat.om.CndaAtrophynildata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaAtrophynildata getCndaAtrophynildatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:atrophyNilData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaAtrophynildata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		XFTItem mr = org.nrg.xft.search.ItemSearch.GetItem("xnat:mrSessionData.ID",getItem().getProperty("cnda:atrophyNilData.imageSession_ID"),getItem().getUser(),false);
		al.add(mr);
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",mr.getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //mrAssessorData
	        XnatMrassessordata childMrassessordata = (XnatMrassessordata)this.getMrassessordata();
	            if (childMrassessordata!=null){
	              for(ResourceFile rf: ((XnatMrassessordata)childMrassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("mrAssessorData[" + ((XnatMrassessordata)childMrassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("mrAssessorData/" + ((XnatMrassessordata)childMrassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //regions/region
	        for(org.nrg.xdat.model.XnatVolumetricregionI childRegions_region : this.getRegions_region()){
	            if (childRegions_region!=null){
	              for(ResourceFile rf: ((XnatVolumetricregion)childRegions_region).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("regions/region[" + ((XnatVolumetricregion)childRegions_region).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("regions/region/" + ((XnatVolumetricregion)childRegions_region).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	        //peaks/peak
	        for(org.nrg.xdat.model.CndaAtrophynildataPeakI childPeaks_peak : this.getPeaks_peak()){
	            if (childPeaks_peak!=null){
	              for(ResourceFile rf: ((CndaAtrophynildataPeak)childPeaks_peak).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("peaks/peak[" + ((CndaAtrophynildataPeak)childPeaks_peak).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("peaks/peak/" + ((CndaAtrophynildataPeak)childPeaks_peak).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
