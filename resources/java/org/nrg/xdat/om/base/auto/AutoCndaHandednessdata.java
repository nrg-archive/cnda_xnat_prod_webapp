/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaHandednessdata extends XnatSubjectassessordata implements org.nrg.xdat.model.CndaHandednessdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaHandednessdata.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:handednessData";

	public AutoCndaHandednessdata(ItemI item)
	{
		super(item);
	}

	public AutoCndaHandednessdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaHandednessdata(UserI user)
	 **/
	public AutoCndaHandednessdata(){}

	public AutoCndaHandednessdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:handednessData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _TaskPrefs_letter=null;

	/**
	 * @return Returns the task_prefs/letter.
	 */
	public Integer getTaskPrefs_letter() {
		try{
			if (_TaskPrefs_letter==null){
				_TaskPrefs_letter=getIntegerProperty("task_prefs/letter");
				return _TaskPrefs_letter;
			}else {
				return _TaskPrefs_letter;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for task_prefs/letter.
	 * @param v Value to Set.
	 */
	public void setTaskPrefs_letter(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/task_prefs/letter",v);
		_TaskPrefs_letter=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _TaskPrefs_ball=null;

	/**
	 * @return Returns the task_prefs/ball.
	 */
	public Integer getTaskPrefs_ball() {
		try{
			if (_TaskPrefs_ball==null){
				_TaskPrefs_ball=getIntegerProperty("task_prefs/ball");
				return _TaskPrefs_ball;
			}else {
				return _TaskPrefs_ball;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for task_prefs/ball.
	 * @param v Value to Set.
	 */
	public void setTaskPrefs_ball(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/task_prefs/ball",v);
		_TaskPrefs_ball=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _TaskPrefs_racquet=null;

	/**
	 * @return Returns the task_prefs/racquet.
	 */
	public Integer getTaskPrefs_racquet() {
		try{
			if (_TaskPrefs_racquet==null){
				_TaskPrefs_racquet=getIntegerProperty("task_prefs/racquet");
				return _TaskPrefs_racquet;
			}else {
				return _TaskPrefs_racquet;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for task_prefs/racquet.
	 * @param v Value to Set.
	 */
	public void setTaskPrefs_racquet(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/task_prefs/racquet",v);
		_TaskPrefs_racquet=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _TaskPrefs_broom=null;

	/**
	 * @return Returns the task_prefs/broom.
	 */
	public Integer getTaskPrefs_broom() {
		try{
			if (_TaskPrefs_broom==null){
				_TaskPrefs_broom=getIntegerProperty("task_prefs/broom");
				return _TaskPrefs_broom;
			}else {
				return _TaskPrefs_broom;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for task_prefs/broom.
	 * @param v Value to Set.
	 */
	public void setTaskPrefs_broom(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/task_prefs/broom",v);
		_TaskPrefs_broom=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _TaskPrefs_shovel=null;

	/**
	 * @return Returns the task_prefs/shovel.
	 */
	public Integer getTaskPrefs_shovel() {
		try{
			if (_TaskPrefs_shovel==null){
				_TaskPrefs_shovel=getIntegerProperty("task_prefs/shovel");
				return _TaskPrefs_shovel;
			}else {
				return _TaskPrefs_shovel;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for task_prefs/shovel.
	 * @param v Value to Set.
	 */
	public void setTaskPrefs_shovel(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/task_prefs/shovel",v);
		_TaskPrefs_shovel=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _TaskPrefs_match=null;

	/**
	 * @return Returns the task_prefs/match.
	 */
	public Integer getTaskPrefs_match() {
		try{
			if (_TaskPrefs_match==null){
				_TaskPrefs_match=getIntegerProperty("task_prefs/match");
				return _TaskPrefs_match;
			}else {
				return _TaskPrefs_match;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for task_prefs/match.
	 * @param v Value to Set.
	 */
	public void setTaskPrefs_match(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/task_prefs/match",v);
		_TaskPrefs_match=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _TaskPrefs_scissors=null;

	/**
	 * @return Returns the task_prefs/scissors.
	 */
	public Integer getTaskPrefs_scissors() {
		try{
			if (_TaskPrefs_scissors==null){
				_TaskPrefs_scissors=getIntegerProperty("task_prefs/scissors");
				return _TaskPrefs_scissors;
			}else {
				return _TaskPrefs_scissors;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for task_prefs/scissors.
	 * @param v Value to Set.
	 */
	public void setTaskPrefs_scissors(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/task_prefs/scissors",v);
		_TaskPrefs_scissors=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _TaskPrefs_thread=null;

	/**
	 * @return Returns the task_prefs/thread.
	 */
	public Integer getTaskPrefs_thread() {
		try{
			if (_TaskPrefs_thread==null){
				_TaskPrefs_thread=getIntegerProperty("task_prefs/thread");
				return _TaskPrefs_thread;
			}else {
				return _TaskPrefs_thread;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for task_prefs/thread.
	 * @param v Value to Set.
	 */
	public void setTaskPrefs_thread(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/task_prefs/thread",v);
		_TaskPrefs_thread=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _TaskPrefs_cards=null;

	/**
	 * @return Returns the task_prefs/cards.
	 */
	public Integer getTaskPrefs_cards() {
		try{
			if (_TaskPrefs_cards==null){
				_TaskPrefs_cards=getIntegerProperty("task_prefs/cards");
				return _TaskPrefs_cards;
			}else {
				return _TaskPrefs_cards;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for task_prefs/cards.
	 * @param v Value to Set.
	 */
	public void setTaskPrefs_cards(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/task_prefs/cards",v);
		_TaskPrefs_cards=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _TaskPrefs_hammer=null;

	/**
	 * @return Returns the task_prefs/hammer.
	 */
	public Integer getTaskPrefs_hammer() {
		try{
			if (_TaskPrefs_hammer==null){
				_TaskPrefs_hammer=getIntegerProperty("task_prefs/hammer");
				return _TaskPrefs_hammer;
			}else {
				return _TaskPrefs_hammer;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for task_prefs/hammer.
	 * @param v Value to Set.
	 */
	public void setTaskPrefs_hammer(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/task_prefs/hammer",v);
		_TaskPrefs_hammer=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _TaskPrefs_toothbrush=null;

	/**
	 * @return Returns the task_prefs/toothbrush.
	 */
	public Integer getTaskPrefs_toothbrush() {
		try{
			if (_TaskPrefs_toothbrush==null){
				_TaskPrefs_toothbrush=getIntegerProperty("task_prefs/toothbrush");
				return _TaskPrefs_toothbrush;
			}else {
				return _TaskPrefs_toothbrush;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for task_prefs/toothbrush.
	 * @param v Value to Set.
	 */
	public void setTaskPrefs_toothbrush(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/task_prefs/toothbrush",v);
		_TaskPrefs_toothbrush=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _TaskPrefs_jar=null;

	/**
	 * @return Returns the task_prefs/jar.
	 */
	public Integer getTaskPrefs_jar() {
		try{
			if (_TaskPrefs_jar==null){
				_TaskPrefs_jar=getIntegerProperty("task_prefs/jar");
				return _TaskPrefs_jar;
			}else {
				return _TaskPrefs_jar;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for task_prefs/jar.
	 * @param v Value to Set.
	 */
	public void setTaskPrefs_jar(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/task_prefs/jar",v);
		_TaskPrefs_jar=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _ParentsLeft_mother=null;

	/**
	 * @return Returns the parents_left/mother.
	 */
	public String getParentsLeft_mother(){
		try{
			if (_ParentsLeft_mother==null){
				_ParentsLeft_mother=getStringProperty("parents_left/mother");
				return _ParentsLeft_mother;
			}else {
				return _ParentsLeft_mother;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for parents_left/mother.
	 * @param v Value to Set.
	 */
	public void setParentsLeft_mother(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/parents_left/mother",v);
		_ParentsLeft_mother=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _ParentsLeft_father=null;

	/**
	 * @return Returns the parents_left/father.
	 */
	public String getParentsLeft_father(){
		try{
			if (_ParentsLeft_father==null){
				_ParentsLeft_father=getStringProperty("parents_left/father");
				return _ParentsLeft_father;
			}else {
				return _ParentsLeft_father;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for parents_left/father.
	 * @param v Value to Set.
	 */
	public void setParentsLeft_father(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/parents_left/father",v);
		_ParentsLeft_father=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Siblings_male_count=null;

	/**
	 * @return Returns the siblings/male/count.
	 */
	public Integer getSiblings_male_count() {
		try{
			if (_Siblings_male_count==null){
				_Siblings_male_count=getIntegerProperty("siblings/male/count");
				return _Siblings_male_count;
			}else {
				return _Siblings_male_count;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for siblings/male/count.
	 * @param v Value to Set.
	 */
	public void setSiblings_male_count(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/siblings/male/count",v);
		_Siblings_male_count=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Siblings_male_leftHanded=null;

	/**
	 * @return Returns the siblings/male/left_handed.
	 */
	public Integer getSiblings_male_leftHanded() {
		try{
			if (_Siblings_male_leftHanded==null){
				_Siblings_male_leftHanded=getIntegerProperty("siblings/male/left_handed");
				return _Siblings_male_leftHanded;
			}else {
				return _Siblings_male_leftHanded;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for siblings/male/left_handed.
	 * @param v Value to Set.
	 */
	public void setSiblings_male_leftHanded(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/siblings/male/left_handed",v);
		_Siblings_male_leftHanded=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Siblings_female_count=null;

	/**
	 * @return Returns the siblings/female/count.
	 */
	public Integer getSiblings_female_count() {
		try{
			if (_Siblings_female_count==null){
				_Siblings_female_count=getIntegerProperty("siblings/female/count");
				return _Siblings_female_count;
			}else {
				return _Siblings_female_count;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for siblings/female/count.
	 * @param v Value to Set.
	 */
	public void setSiblings_female_count(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/siblings/female/count",v);
		_Siblings_female_count=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Siblings_female_leftHanded=null;

	/**
	 * @return Returns the siblings/female/left_handed.
	 */
	public Integer getSiblings_female_leftHanded() {
		try{
			if (_Siblings_female_leftHanded==null){
				_Siblings_female_leftHanded=getIntegerProperty("siblings/female/left_handed");
				return _Siblings_female_leftHanded;
			}else {
				return _Siblings_female_leftHanded;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for siblings/female/left_handed.
	 * @param v Value to Set.
	 */
	public void setSiblings_female_leftHanded(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/siblings/female/left_handed",v);
		_Siblings_female_leftHanded=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaHandednessdata> getAllCndaHandednessdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaHandednessdata> al = new ArrayList<org.nrg.xdat.om.CndaHandednessdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaHandednessdata> getCndaHandednessdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaHandednessdata> al = new ArrayList<org.nrg.xdat.om.CndaHandednessdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaHandednessdata> getCndaHandednessdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaHandednessdata> al = new ArrayList<org.nrg.xdat.om.CndaHandednessdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaHandednessdata getCndaHandednessdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:handednessData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaHandednessdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
