/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCondrMetsMetsclinencdata extends CndaExtSanode implements org.nrg.xdat.model.CondrMetsMetsclinencdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCondrMetsMetsclinencdata.class);
	public static String SCHEMA_ELEMENT_NAME="condr_mets:metsClinEncData";

	public AutoCondrMetsMetsclinencdata(ItemI item)
	{
		super(item);
	}

	public AutoCondrMetsMetsclinencdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCondrMetsMetsclinencdata(UserI user)
	 **/
	public AutoCondrMetsMetsclinencdata(){}

	public AutoCondrMetsMetsclinencdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "condr_mets:metsClinEncData";
	}
	 private org.nrg.xdat.om.CndaExtSanode _Sanode =null;

	/**
	 * saNode
	 * @return org.nrg.xdat.om.CndaExtSanode
	 */
	public org.nrg.xdat.om.CndaExtSanode getSanode() {
		try{
			if (_Sanode==null){
				_Sanode=((CndaExtSanode)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("saNode")));
				return _Sanode;
			}else {
				return _Sanode;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for saNode.
	 * @param v Value to Set.
	 */
	public void setSanode(ItemI v) throws Exception{
		_Sanode =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/saNode",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/saNode",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * saNode
	 * set org.nrg.xdat.model.CndaExtSanodeI
	 */
	public <A extends org.nrg.xdat.model.CndaExtSanodeI> void setSanode(A item) throws Exception{
	setSanode((ItemI)item);
	}

	/**
	 * Removes the saNode.
	 * */
	public void removeSanode() {
		_Sanode =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/saNode",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Tumorvolume=null;

	/**
	 * @return Returns the tumorVolume.
	 */
	public Double getTumorvolume() {
		try{
			if (_Tumorvolume==null){
				_Tumorvolume=getDoubleProperty("tumorVolume");
				return _Tumorvolume;
			}else {
				return _Tumorvolume;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tumorVolume.
	 * @param v Value to Set.
	 */
	public void setTumorvolume(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tumorVolume",v);
		_Tumorvolume=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Responsetotreatment=null;

	/**
	 * @return Returns the responseToTreatment.
	 */
	public String getResponsetotreatment(){
		try{
			if (_Responsetotreatment==null){
				_Responsetotreatment=getStringProperty("responseToTreatment");
				return _Responsetotreatment;
			}else {
				return _Responsetotreatment;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for responseToTreatment.
	 * @param v Value to Set.
	 */
	public void setResponsetotreatment(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/responseToTreatment",v);
		_Responsetotreatment=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Progressionstatus=null;

	/**
	 * @return Returns the progressionStatus.
	 */
	public String getProgressionstatus(){
		try{
			if (_Progressionstatus==null){
				_Progressionstatus=getStringProperty("progressionStatus");
				return _Progressionstatus;
			}else {
				return _Progressionstatus;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for progressionStatus.
	 * @param v Value to Set.
	 */
	public void setProgressionstatus(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/progressionStatus",v);
		_Progressionstatus=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Treatmentplan=null;

	/**
	 * @return Returns the treatmentPlan.
	 */
	public String getTreatmentplan(){
		try{
			if (_Treatmentplan==null){
				_Treatmentplan=getStringProperty("treatmentPlan");
				return _Treatmentplan;
			}else {
				return _Treatmentplan;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for treatmentPlan.
	 * @param v Value to Set.
	 */
	public void setTreatmentplan(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/treatmentPlan",v);
		_Treatmentplan=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Encnotes=null;

	/**
	 * @return Returns the encNotes.
	 */
	public String getEncnotes(){
		try{
			if (_Encnotes==null){
				_Encnotes=getStringProperty("encNotes");
				return _Encnotes;
			}else {
				return _Encnotes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for encNotes.
	 * @param v Value to Set.
	 */
	public void setEncnotes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/encNotes",v);
		_Encnotes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CondrMetsMetsclinencdata> getAllCondrMetsMetsclinencdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrMetsMetsclinencdata> al = new ArrayList<org.nrg.xdat.om.CondrMetsMetsclinencdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrMetsMetsclinencdata> getCondrMetsMetsclinencdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrMetsMetsclinencdata> al = new ArrayList<org.nrg.xdat.om.CondrMetsMetsclinencdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrMetsMetsclinencdata> getCondrMetsMetsclinencdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrMetsMetsclinencdata> al = new ArrayList<org.nrg.xdat.om.CondrMetsMetsclinencdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CondrMetsMetsclinencdata getCondrMetsMetsclinencdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("condr_mets:metsClinEncData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CondrMetsMetsclinencdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //saNode
	        CndaExtSanode childSanode = (CndaExtSanode)this.getSanode();
	            if (childSanode!=null){
	              for(ResourceFile rf: ((CndaExtSanode)childSanode).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("saNode[" + ((CndaExtSanode)childSanode).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("saNode/" + ((CndaExtSanode)childSanode).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
