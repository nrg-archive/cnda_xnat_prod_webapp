/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:01 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianPlasmadata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianPlasmadataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianPlasmadata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:plasmaData";

	public AutoDianPlasmadata(ItemI item)
	{
		super(item);
	}

	public AutoDianPlasmadata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianPlasmadata(UserI user)
	 **/
	public AutoDianPlasmadata(){}

	public AutoDianPlasmadata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:plasmaData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Bldcollsess=null;

	/**
	 * @return Returns the bldCollSess.
	 */
	public String getBldcollsess(){
		try{
			if (_Bldcollsess==null){
				_Bldcollsess=getStringProperty("bldCollSess");
				return _Bldcollsess;
			}else {
				return _Bldcollsess;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for bldCollSess.
	 * @param v Value to Set.
	 */
	public void setBldcollsess(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/bldCollSess",v);
		_Bldcollsess=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Ab40=null;

	/**
	 * @return Returns the Ab40.
	 */
	public Double getAb40() {
		try{
			if (_Ab40==null){
				_Ab40=getDoubleProperty("Ab40");
				return _Ab40;
			}else {
				return _Ab40;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Ab40.
	 * @param v Value to Set.
	 */
	public void setAb40(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Ab40",v);
		_Ab40=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Ab40units=null;

	/**
	 * @return Returns the Ab40Units.
	 */
	public String getAb40units(){
		try{
			if (_Ab40units==null){
				_Ab40units=getStringProperty("Ab40Units");
				return _Ab40units;
			}else {
				return _Ab40units;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Ab40Units.
	 * @param v Value to Set.
	 */
	public void setAb40units(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Ab40Units",v);
		_Ab40units=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Ab40lot=null;

	/**
	 * @return Returns the Ab40Lot.
	 */
	public String getAb40lot(){
		try{
			if (_Ab40lot==null){
				_Ab40lot=getStringProperty("Ab40Lot");
				return _Ab40lot;
			}else {
				return _Ab40lot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Ab40Lot.
	 * @param v Value to Set.
	 */
	public void setAb40lot(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Ab40Lot",v);
		_Ab40lot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Ab40plate=null;

	/**
	 * @return Returns the Ab40Plate.
	 */
	public String getAb40plate(){
		try{
			if (_Ab40plate==null){
				_Ab40plate=getStringProperty("Ab40Plate");
				return _Ab40plate;
			}else {
				return _Ab40plate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Ab40Plate.
	 * @param v Value to Set.
	 */
	public void setAb40plate(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Ab40Plate",v);
		_Ab40plate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Ab40assaydate=null;

	/**
	 * @return Returns the Ab40AssayDate.
	 */
	public Object getAb40assaydate(){
		try{
			if (_Ab40assaydate==null){
				_Ab40assaydate=getProperty("Ab40AssayDate");
				return _Ab40assaydate;
			}else {
				return _Ab40assaydate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Ab40AssayDate.
	 * @param v Value to Set.
	 */
	public void setAb40assaydate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Ab40AssayDate",v);
		_Ab40assaydate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Ab42=null;

	/**
	 * @return Returns the Ab42.
	 */
	public Double getAb42() {
		try{
			if (_Ab42==null){
				_Ab42=getDoubleProperty("Ab42");
				return _Ab42;
			}else {
				return _Ab42;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Ab42.
	 * @param v Value to Set.
	 */
	public void setAb42(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Ab42",v);
		_Ab42=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Ab42units=null;

	/**
	 * @return Returns the Ab42Units.
	 */
	public String getAb42units(){
		try{
			if (_Ab42units==null){
				_Ab42units=getStringProperty("Ab42Units");
				return _Ab42units;
			}else {
				return _Ab42units;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Ab42Units.
	 * @param v Value to Set.
	 */
	public void setAb42units(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Ab42Units",v);
		_Ab42units=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Ab42lot=null;

	/**
	 * @return Returns the Ab42Lot.
	 */
	public String getAb42lot(){
		try{
			if (_Ab42lot==null){
				_Ab42lot=getStringProperty("Ab42Lot");
				return _Ab42lot;
			}else {
				return _Ab42lot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Ab42Lot.
	 * @param v Value to Set.
	 */
	public void setAb42lot(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Ab42Lot",v);
		_Ab42lot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Ab42plate=null;

	/**
	 * @return Returns the Ab42Plate.
	 */
	public String getAb42plate(){
		try{
			if (_Ab42plate==null){
				_Ab42plate=getStringProperty("Ab42Plate");
				return _Ab42plate;
			}else {
				return _Ab42plate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Ab42Plate.
	 * @param v Value to Set.
	 */
	public void setAb42plate(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Ab42Plate",v);
		_Ab42plate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Ab42assaydate=null;

	/**
	 * @return Returns the Ab42AssayDate.
	 */
	public Object getAb42assaydate(){
		try{
			if (_Ab42assaydate==null){
				_Ab42assaydate=getProperty("Ab42AssayDate");
				return _Ab42assaydate;
			}else {
				return _Ab42assaydate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Ab42AssayDate.
	 * @param v Value to Set.
	 */
	public void setAb42assaydate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Ab42AssayDate",v);
		_Ab42assaydate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Abx40=null;

	/**
	 * @return Returns the Abx40.
	 */
	public Double getAbx40() {
		try{
			if (_Abx40==null){
				_Abx40=getDoubleProperty("Abx40");
				return _Abx40;
			}else {
				return _Abx40;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Abx40.
	 * @param v Value to Set.
	 */
	public void setAbx40(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Abx40",v);
		_Abx40=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Abx40units=null;

	/**
	 * @return Returns the Abx40Units.
	 */
	public String getAbx40units(){
		try{
			if (_Abx40units==null){
				_Abx40units=getStringProperty("Abx40Units");
				return _Abx40units;
			}else {
				return _Abx40units;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Abx40Units.
	 * @param v Value to Set.
	 */
	public void setAbx40units(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Abx40Units",v);
		_Abx40units=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Abx40lot=null;

	/**
	 * @return Returns the Abx40Lot.
	 */
	public String getAbx40lot(){
		try{
			if (_Abx40lot==null){
				_Abx40lot=getStringProperty("Abx40Lot");
				return _Abx40lot;
			}else {
				return _Abx40lot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Abx40Lot.
	 * @param v Value to Set.
	 */
	public void setAbx40lot(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Abx40Lot",v);
		_Abx40lot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Abx40plate=null;

	/**
	 * @return Returns the Abx40Plate.
	 */
	public String getAbx40plate(){
		try{
			if (_Abx40plate==null){
				_Abx40plate=getStringProperty("Abx40Plate");
				return _Abx40plate;
			}else {
				return _Abx40plate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Abx40Plate.
	 * @param v Value to Set.
	 */
	public void setAbx40plate(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Abx40Plate",v);
		_Abx40plate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Abx40assaydate=null;

	/**
	 * @return Returns the Abx40AssayDate.
	 */
	public Object getAbx40assaydate(){
		try{
			if (_Abx40assaydate==null){
				_Abx40assaydate=getProperty("Abx40AssayDate");
				return _Abx40assaydate;
			}else {
				return _Abx40assaydate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Abx40AssayDate.
	 * @param v Value to Set.
	 */
	public void setAbx40assaydate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Abx40AssayDate",v);
		_Abx40assaydate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Abx42=null;

	/**
	 * @return Returns the Abx42.
	 */
	public Double getAbx42() {
		try{
			if (_Abx42==null){
				_Abx42=getDoubleProperty("Abx42");
				return _Abx42;
			}else {
				return _Abx42;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Abx42.
	 * @param v Value to Set.
	 */
	public void setAbx42(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Abx42",v);
		_Abx42=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Abx42units=null;

	/**
	 * @return Returns the Abx42Units.
	 */
	public String getAbx42units(){
		try{
			if (_Abx42units==null){
				_Abx42units=getStringProperty("Abx42Units");
				return _Abx42units;
			}else {
				return _Abx42units;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Abx42Units.
	 * @param v Value to Set.
	 */
	public void setAbx42units(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Abx42Units",v);
		_Abx42units=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Abx42lot=null;

	/**
	 * @return Returns the Abx42Lot.
	 */
	public String getAbx42lot(){
		try{
			if (_Abx42lot==null){
				_Abx42lot=getStringProperty("Abx42Lot");
				return _Abx42lot;
			}else {
				return _Abx42lot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Abx42Lot.
	 * @param v Value to Set.
	 */
	public void setAbx42lot(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Abx42Lot",v);
		_Abx42lot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Abx42plate=null;

	/**
	 * @return Returns the Abx42Plate.
	 */
	public String getAbx42plate(){
		try{
			if (_Abx42plate==null){
				_Abx42plate=getStringProperty("Abx42Plate");
				return _Abx42plate;
			}else {
				return _Abx42plate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Abx42Plate.
	 * @param v Value to Set.
	 */
	public void setAbx42plate(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Abx42Plate",v);
		_Abx42plate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Abx42assaydate=null;

	/**
	 * @return Returns the Abx42AssayDate.
	 */
	public Object getAbx42assaydate(){
		try{
			if (_Abx42assaydate==null){
				_Abx42assaydate=getProperty("Abx42AssayDate");
				return _Abx42assaydate;
			}else {
				return _Abx42assaydate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Abx42AssayDate.
	 * @param v Value to Set.
	 */
	public void setAbx42assaydate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Abx42AssayDate",v);
		_Abx42assaydate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _PlasmaAb40PlusQcfails=null;

	/**
	 * @return Returns the PLASMA_AB40_plus_QCFAILS.
	 */
	public Double getPlasmaAb40PlusQcfails() {
		try{
			if (_PlasmaAb40PlusQcfails==null){
				_PlasmaAb40PlusQcfails=getDoubleProperty("PLASMA_AB40_plus_QCFAILS");
				return _PlasmaAb40PlusQcfails;
			}else {
				return _PlasmaAb40PlusQcfails;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PLASMA_AB40_plus_QCFAILS.
	 * @param v Value to Set.
	 */
	public void setPlasmaAb40PlusQcfails(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PLASMA_AB40_plus_QCFAILS",v);
		_PlasmaAb40PlusQcfails=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _PlasmaAb40Comments=null;

	/**
	 * @return Returns the PLASMA_AB40_COMMENTS.
	 */
	public String getPlasmaAb40Comments(){
		try{
			if (_PlasmaAb40Comments==null){
				_PlasmaAb40Comments=getStringProperty("PLASMA_AB40_COMMENTS");
				return _PlasmaAb40Comments;
			}else {
				return _PlasmaAb40Comments;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PLASMA_AB40_COMMENTS.
	 * @param v Value to Set.
	 */
	public void setPlasmaAb40Comments(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PLASMA_AB40_COMMENTS",v);
		_PlasmaAb40Comments=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _PlasmaAb42PlusQcfails=null;

	/**
	 * @return Returns the PLASMA_AB42_plus_QCFAILS.
	 */
	public Double getPlasmaAb42PlusQcfails() {
		try{
			if (_PlasmaAb42PlusQcfails==null){
				_PlasmaAb42PlusQcfails=getDoubleProperty("PLASMA_AB42_plus_QCFAILS");
				return _PlasmaAb42PlusQcfails;
			}else {
				return _PlasmaAb42PlusQcfails;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PLASMA_AB42_plus_QCFAILS.
	 * @param v Value to Set.
	 */
	public void setPlasmaAb42PlusQcfails(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PLASMA_AB42_plus_QCFAILS",v);
		_PlasmaAb42PlusQcfails=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _PlasmaAb42Comments=null;

	/**
	 * @return Returns the PLASMA_AB42_COMMENTS.
	 */
	public String getPlasmaAb42Comments(){
		try{
			if (_PlasmaAb42Comments==null){
				_PlasmaAb42Comments=getStringProperty("PLASMA_AB42_COMMENTS");
				return _PlasmaAb42Comments;
			}else {
				return _PlasmaAb42Comments;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PLASMA_AB42_COMMENTS.
	 * @param v Value to Set.
	 */
	public void setPlasmaAb42Comments(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PLASMA_AB42_COMMENTS",v);
		_PlasmaAb42Comments=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianPlasmadata> getAllDianPlasmadatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianPlasmadata> al = new ArrayList<org.nrg.xdat.om.DianPlasmadata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianPlasmadata> getDianPlasmadatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianPlasmadata> al = new ArrayList<org.nrg.xdat.om.DianPlasmadata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianPlasmadata> getDianPlasmadatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianPlasmadata> al = new ArrayList<org.nrg.xdat.om.DianPlasmadata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianPlasmadata getDianPlasmadatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:plasmaData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianPlasmadata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
