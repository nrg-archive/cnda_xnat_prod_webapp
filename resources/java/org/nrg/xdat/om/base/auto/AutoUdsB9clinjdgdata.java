/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoUdsB9clinjdgdata extends XnatSubjectassessordata implements org.nrg.xdat.model.UdsB9clinjdgdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoUdsB9clinjdgdata.class);
	public static String SCHEMA_ELEMENT_NAME="uds:b9clinjdgData";

	public AutoUdsB9clinjdgdata(ItemI item)
	{
		super(item);
	}

	public AutoUdsB9clinjdgdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoUdsB9clinjdgdata(UserI user)
	 **/
	public AutoUdsB9clinjdgdata(){}

	public AutoUdsB9clinjdgdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "uds:b9clinjdgData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _B9chg=null;

	/**
	 * @return Returns the B9CHG.
	 */
	public Integer getB9chg() {
		try{
			if (_B9chg==null){
				_B9chg=getIntegerProperty("B9CHG");
				return _B9chg;
			}else {
				return _B9chg;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for B9CHG.
	 * @param v Value to Set.
	 */
	public void setB9chg(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/B9CHG",v);
		_B9chg=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Decsub=null;

	/**
	 * @return Returns the DECSUB.
	 */
	public Integer getDecsub() {
		try{
			if (_Decsub==null){
				_Decsub=getIntegerProperty("DECSUB");
				return _Decsub;
			}else {
				return _Decsub;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DECSUB.
	 * @param v Value to Set.
	 */
	public void setDecsub(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DECSUB",v);
		_Decsub=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Decin=null;

	/**
	 * @return Returns the DECIN.
	 */
	public Integer getDecin() {
		try{
			if (_Decin==null){
				_Decin=getIntegerProperty("DECIN");
				return _Decin;
			}else {
				return _Decin;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DECIN.
	 * @param v Value to Set.
	 */
	public void setDecin(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DECIN",v);
		_Decin=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Decclin=null;

	/**
	 * @return Returns the DECCLIN.
	 */
	public Integer getDecclin() {
		try{
			if (_Decclin==null){
				_Decclin=getIntegerProperty("DECCLIN");
				return _Decclin;
			}else {
				return _Decclin;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DECCLIN.
	 * @param v Value to Set.
	 */
	public void setDecclin(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DECCLIN",v);
		_Decclin=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Decage=null;

	/**
	 * @return Returns the DECAGE.
	 */
	public Integer getDecage() {
		try{
			if (_Decage==null){
				_Decage=getIntegerProperty("DECAGE");
				return _Decage;
			}else {
				return _Decage;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DECAGE.
	 * @param v Value to Set.
	 */
	public void setDecage(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DECAGE",v);
		_Decage=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cogmem=null;

	/**
	 * @return Returns the COGMEM.
	 */
	public Integer getCogmem() {
		try{
			if (_Cogmem==null){
				_Cogmem=getIntegerProperty("COGMEM");
				return _Cogmem;
			}else {
				return _Cogmem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COGMEM.
	 * @param v Value to Set.
	 */
	public void setCogmem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COGMEM",v);
		_Cogmem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cogjudg=null;

	/**
	 * @return Returns the COGJUDG.
	 */
	public Integer getCogjudg() {
		try{
			if (_Cogjudg==null){
				_Cogjudg=getIntegerProperty("COGJUDG");
				return _Cogjudg;
			}else {
				return _Cogjudg;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COGJUDG.
	 * @param v Value to Set.
	 */
	public void setCogjudg(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COGJUDG",v);
		_Cogjudg=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Coglang=null;

	/**
	 * @return Returns the COGLANG.
	 */
	public Integer getCoglang() {
		try{
			if (_Coglang==null){
				_Coglang=getIntegerProperty("COGLANG");
				return _Coglang;
			}else {
				return _Coglang;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COGLANG.
	 * @param v Value to Set.
	 */
	public void setCoglang(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COGLANG",v);
		_Coglang=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cogvis=null;

	/**
	 * @return Returns the COGVIS.
	 */
	public Integer getCogvis() {
		try{
			if (_Cogvis==null){
				_Cogvis=getIntegerProperty("COGVIS");
				return _Cogvis;
			}else {
				return _Cogvis;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COGVIS.
	 * @param v Value to Set.
	 */
	public void setCogvis(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COGVIS",v);
		_Cogvis=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cogattn=null;

	/**
	 * @return Returns the COGATTN.
	 */
	public Integer getCogattn() {
		try{
			if (_Cogattn==null){
				_Cogattn=getIntegerProperty("COGATTN");
				return _Cogattn;
			}else {
				return _Cogattn;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COGATTN.
	 * @param v Value to Set.
	 */
	public void setCogattn(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COGATTN",v);
		_Cogattn=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cogfluc=null;

	/**
	 * @return Returns the COGFLUC.
	 */
	public Integer getCogfluc() {
		try{
			if (_Cogfluc==null){
				_Cogfluc=getIntegerProperty("COGFLUC");
				return _Cogfluc;
			}else {
				return _Cogfluc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COGFLUC.
	 * @param v Value to Set.
	 */
	public void setCogfluc(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COGFLUC",v);
		_Cogfluc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cogothr=null;

	/**
	 * @return Returns the COGOTHR.
	 */
	public Integer getCogothr() {
		try{
			if (_Cogothr==null){
				_Cogothr=getIntegerProperty("COGOTHR");
				return _Cogothr;
			}else {
				return _Cogothr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COGOTHR.
	 * @param v Value to Set.
	 */
	public void setCogothr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COGOTHR",v);
		_Cogothr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Cogothrx=null;

	/**
	 * @return Returns the COGOTHRX.
	 */
	public String getCogothrx(){
		try{
			if (_Cogothrx==null){
				_Cogothrx=getStringProperty("COGOTHRX");
				return _Cogothrx;
			}else {
				return _Cogothrx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COGOTHRX.
	 * @param v Value to Set.
	 */
	public void setCogothrx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COGOTHRX",v);
		_Cogothrx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cogfrst=null;

	/**
	 * @return Returns the COGFRST.
	 */
	public Integer getCogfrst() {
		try{
			if (_Cogfrst==null){
				_Cogfrst=getIntegerProperty("COGFRST");
				return _Cogfrst;
			}else {
				return _Cogfrst;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COGFRST.
	 * @param v Value to Set.
	 */
	public void setCogfrst(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COGFRST",v);
		_Cogfrst=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Cogfrstx=null;

	/**
	 * @return Returns the COGFRSTX.
	 */
	public String getCogfrstx(){
		try{
			if (_Cogfrstx==null){
				_Cogfrstx=getStringProperty("COGFRSTX");
				return _Cogfrstx;
			}else {
				return _Cogfrstx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COGFRSTX.
	 * @param v Value to Set.
	 */
	public void setCogfrstx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COGFRSTX",v);
		_Cogfrstx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cogmode=null;

	/**
	 * @return Returns the COGMODE.
	 */
	public Integer getCogmode() {
		try{
			if (_Cogmode==null){
				_Cogmode=getIntegerProperty("COGMODE");
				return _Cogmode;
			}else {
				return _Cogmode;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COGMODE.
	 * @param v Value to Set.
	 */
	public void setCogmode(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COGMODE",v);
		_Cogmode=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Cogmodex=null;

	/**
	 * @return Returns the COGMODEX.
	 */
	public String getCogmodex(){
		try{
			if (_Cogmodex==null){
				_Cogmodex=getStringProperty("COGMODEX");
				return _Cogmodex;
			}else {
				return _Cogmodex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COGMODEX.
	 * @param v Value to Set.
	 */
	public void setCogmodex(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COGMODEX",v);
		_Cogmodex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Beapathy=null;

	/**
	 * @return Returns the BEAPATHY.
	 */
	public Integer getBeapathy() {
		try{
			if (_Beapathy==null){
				_Beapathy=getIntegerProperty("BEAPATHY");
				return _Beapathy;
			}else {
				return _Beapathy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BEAPATHY.
	 * @param v Value to Set.
	 */
	public void setBeapathy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BEAPATHY",v);
		_Beapathy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Bedep=null;

	/**
	 * @return Returns the BEDEP.
	 */
	public Integer getBedep() {
		try{
			if (_Bedep==null){
				_Bedep=getIntegerProperty("BEDEP");
				return _Bedep;
			}else {
				return _Bedep;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BEDEP.
	 * @param v Value to Set.
	 */
	public void setBedep(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BEDEP",v);
		_Bedep=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Bevhall=null;

	/**
	 * @return Returns the BEVHALL.
	 */
	public Integer getBevhall() {
		try{
			if (_Bevhall==null){
				_Bevhall=getIntegerProperty("BEVHALL");
				return _Bevhall;
			}else {
				return _Bevhall;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BEVHALL.
	 * @param v Value to Set.
	 */
	public void setBevhall(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BEVHALL",v);
		_Bevhall=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Bevwell=null;

	/**
	 * @return Returns the BEVWELL.
	 */
	public Integer getBevwell() {
		try{
			if (_Bevwell==null){
				_Bevwell=getIntegerProperty("BEVWELL");
				return _Bevwell;
			}else {
				return _Bevwell;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BEVWELL.
	 * @param v Value to Set.
	 */
	public void setBevwell(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BEVWELL",v);
		_Bevwell=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Beahall=null;

	/**
	 * @return Returns the BEAHALL.
	 */
	public Integer getBeahall() {
		try{
			if (_Beahall==null){
				_Beahall=getIntegerProperty("BEAHALL");
				return _Beahall;
			}else {
				return _Beahall;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BEAHALL.
	 * @param v Value to Set.
	 */
	public void setBeahall(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BEAHALL",v);
		_Beahall=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Bedel=null;

	/**
	 * @return Returns the BEDEL.
	 */
	public Integer getBedel() {
		try{
			if (_Bedel==null){
				_Bedel=getIntegerProperty("BEDEL");
				return _Bedel;
			}else {
				return _Bedel;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BEDEL.
	 * @param v Value to Set.
	 */
	public void setBedel(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BEDEL",v);
		_Bedel=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Bedisin=null;

	/**
	 * @return Returns the BEDISIN.
	 */
	public Integer getBedisin() {
		try{
			if (_Bedisin==null){
				_Bedisin=getIntegerProperty("BEDISIN");
				return _Bedisin;
			}else {
				return _Bedisin;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BEDISIN.
	 * @param v Value to Set.
	 */
	public void setBedisin(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BEDISIN",v);
		_Bedisin=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Beirrit=null;

	/**
	 * @return Returns the BEIRRIT.
	 */
	public Integer getBeirrit() {
		try{
			if (_Beirrit==null){
				_Beirrit=getIntegerProperty("BEIRRIT");
				return _Beirrit;
			}else {
				return _Beirrit;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BEIRRIT.
	 * @param v Value to Set.
	 */
	public void setBeirrit(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BEIRRIT",v);
		_Beirrit=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Beagit=null;

	/**
	 * @return Returns the BEAGIT.
	 */
	public Integer getBeagit() {
		try{
			if (_Beagit==null){
				_Beagit=getIntegerProperty("BEAGIT");
				return _Beagit;
			}else {
				return _Beagit;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BEAGIT.
	 * @param v Value to Set.
	 */
	public void setBeagit(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BEAGIT",v);
		_Beagit=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Beperch=null;

	/**
	 * @return Returns the BEPERCH.
	 */
	public Integer getBeperch() {
		try{
			if (_Beperch==null){
				_Beperch=getIntegerProperty("BEPERCH");
				return _Beperch;
			}else {
				return _Beperch;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BEPERCH.
	 * @param v Value to Set.
	 */
	public void setBeperch(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BEPERCH",v);
		_Beperch=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Berem=null;

	/**
	 * @return Returns the BEREM.
	 */
	public Integer getBerem() {
		try{
			if (_Berem==null){
				_Berem=getIntegerProperty("BEREM");
				return _Berem;
			}else {
				return _Berem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BEREM.
	 * @param v Value to Set.
	 */
	public void setBerem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BEREM",v);
		_Berem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Beothr=null;

	/**
	 * @return Returns the BEOTHR.
	 */
	public Integer getBeothr() {
		try{
			if (_Beothr==null){
				_Beothr=getIntegerProperty("BEOTHR");
				return _Beothr;
			}else {
				return _Beothr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BEOTHR.
	 * @param v Value to Set.
	 */
	public void setBeothr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BEOTHR",v);
		_Beothr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Beothrx=null;

	/**
	 * @return Returns the BEOTHRX.
	 */
	public String getBeothrx(){
		try{
			if (_Beothrx==null){
				_Beothrx=getStringProperty("BEOTHRX");
				return _Beothrx;
			}else {
				return _Beothrx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BEOTHRX.
	 * @param v Value to Set.
	 */
	public void setBeothrx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BEOTHRX",v);
		_Beothrx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Befrst=null;

	/**
	 * @return Returns the BEFRST.
	 */
	public Integer getBefrst() {
		try{
			if (_Befrst==null){
				_Befrst=getIntegerProperty("BEFRST");
				return _Befrst;
			}else {
				return _Befrst;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BEFRST.
	 * @param v Value to Set.
	 */
	public void setBefrst(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BEFRST",v);
		_Befrst=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Befrstx=null;

	/**
	 * @return Returns the BEFRSTX.
	 */
	public String getBefrstx(){
		try{
			if (_Befrstx==null){
				_Befrstx=getStringProperty("BEFRSTX");
				return _Befrstx;
			}else {
				return _Befrstx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BEFRSTX.
	 * @param v Value to Set.
	 */
	public void setBefrstx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BEFRSTX",v);
		_Befrstx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Bemode=null;

	/**
	 * @return Returns the BEMODE.
	 */
	public Integer getBemode() {
		try{
			if (_Bemode==null){
				_Bemode=getIntegerProperty("BEMODE");
				return _Bemode;
			}else {
				return _Bemode;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BEMODE.
	 * @param v Value to Set.
	 */
	public void setBemode(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BEMODE",v);
		_Bemode=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Bemodex=null;

	/**
	 * @return Returns the BEMODEX.
	 */
	public String getBemodex(){
		try{
			if (_Bemodex==null){
				_Bemodex=getStringProperty("BEMODEX");
				return _Bemodex;
			}else {
				return _Bemodex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BEMODEX.
	 * @param v Value to Set.
	 */
	public void setBemodex(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BEMODEX",v);
		_Bemodex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mogait=null;

	/**
	 * @return Returns the MOGAIT.
	 */
	public Integer getMogait() {
		try{
			if (_Mogait==null){
				_Mogait=getIntegerProperty("MOGAIT");
				return _Mogait;
			}else {
				return _Mogait;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MOGAIT.
	 * @param v Value to Set.
	 */
	public void setMogait(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MOGAIT",v);
		_Mogait=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mofalls=null;

	/**
	 * @return Returns the MOFALLS.
	 */
	public Integer getMofalls() {
		try{
			if (_Mofalls==null){
				_Mofalls=getIntegerProperty("MOFALLS");
				return _Mofalls;
			}else {
				return _Mofalls;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MOFALLS.
	 * @param v Value to Set.
	 */
	public void setMofalls(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MOFALLS",v);
		_Mofalls=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Motrem=null;

	/**
	 * @return Returns the MOTREM.
	 */
	public Integer getMotrem() {
		try{
			if (_Motrem==null){
				_Motrem=getIntegerProperty("MOTREM");
				return _Motrem;
			}else {
				return _Motrem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MOTREM.
	 * @param v Value to Set.
	 */
	public void setMotrem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MOTREM",v);
		_Motrem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Moslow=null;

	/**
	 * @return Returns the MOSLOW.
	 */
	public Integer getMoslow() {
		try{
			if (_Moslow==null){
				_Moslow=getIntegerProperty("MOSLOW");
				return _Moslow;
			}else {
				return _Moslow;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MOSLOW.
	 * @param v Value to Set.
	 */
	public void setMoslow(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MOSLOW",v);
		_Moslow=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mofrst=null;

	/**
	 * @return Returns the MOFRST.
	 */
	public Integer getMofrst() {
		try{
			if (_Mofrst==null){
				_Mofrst=getIntegerProperty("MOFRST");
				return _Mofrst;
			}else {
				return _Mofrst;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MOFRST.
	 * @param v Value to Set.
	 */
	public void setMofrst(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MOFRST",v);
		_Mofrst=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Momode=null;

	/**
	 * @return Returns the MOMODE.
	 */
	public Integer getMomode() {
		try{
			if (_Momode==null){
				_Momode=getIntegerProperty("MOMODE");
				return _Momode;
			}else {
				return _Momode;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MOMODE.
	 * @param v Value to Set.
	 */
	public void setMomode(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MOMODE",v);
		_Momode=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Momodex=null;

	/**
	 * @return Returns the MOMODEX.
	 */
	public String getMomodex(){
		try{
			if (_Momodex==null){
				_Momodex=getStringProperty("MOMODEX");
				return _Momodex;
			}else {
				return _Momodex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MOMODEX.
	 * @param v Value to Set.
	 */
	public void setMomodex(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MOMODEX",v);
		_Momodex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Momopark=null;

	/**
	 * @return Returns the MOMOPARK.
	 */
	public Integer getMomopark() {
		try{
			if (_Momopark==null){
				_Momopark=getIntegerProperty("MOMOPARK");
				return _Momopark;
			}else {
				return _Momopark;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MOMOPARK.
	 * @param v Value to Set.
	 */
	public void setMomopark(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MOMOPARK",v);
		_Momopark=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Course=null;

	/**
	 * @return Returns the COURSE.
	 */
	public Integer getCourse() {
		try{
			if (_Course==null){
				_Course=getIntegerProperty("COURSE");
				return _Course;
			}else {
				return _Course;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COURSE.
	 * @param v Value to Set.
	 */
	public void setCourse(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COURSE",v);
		_Course=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Frstchg=null;

	/**
	 * @return Returns the FRSTCHG.
	 */
	public Integer getFrstchg() {
		try{
			if (_Frstchg==null){
				_Frstchg=getIntegerProperty("FRSTCHG");
				return _Frstchg;
			}else {
				return _Frstchg;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for FRSTCHG.
	 * @param v Value to Set.
	 */
	public void setFrstchg(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/FRSTCHG",v);
		_Frstchg=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.UdsB9clinjdgdata> getAllUdsB9clinjdgdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsB9clinjdgdata> al = new ArrayList<org.nrg.xdat.om.UdsB9clinjdgdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsB9clinjdgdata> getUdsB9clinjdgdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsB9clinjdgdata> al = new ArrayList<org.nrg.xdat.om.UdsB9clinjdgdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsB9clinjdgdata> getUdsB9clinjdgdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsB9clinjdgdata> al = new ArrayList<org.nrg.xdat.om.UdsB9clinjdgdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static UdsB9clinjdgdata getUdsB9clinjdgdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("uds:b9clinjdgData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (UdsB9clinjdgdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
