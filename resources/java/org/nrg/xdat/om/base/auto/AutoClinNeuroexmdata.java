/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:01 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoClinNeuroexmdata extends XnatSubjectassessordata implements org.nrg.xdat.model.ClinNeuroexmdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoClinNeuroexmdata.class);
	public static String SCHEMA_ELEMENT_NAME="clin:neuroexmData";

	public AutoClinNeuroexmdata(ItemI item)
	{
		super(item);
	}

	public AutoClinNeuroexmdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoClinNeuroexmdata(UserI user)
	 **/
	public AutoClinNeuroexmdata(){}

	public AutoClinNeuroexmdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "clin:neuroexmData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Nxvisual=null;

	/**
	 * @return Returns the NXVISUAL.
	 */
	public Integer getNxvisual() {
		try{
			if (_Nxvisual==null){
				_Nxvisual=getIntegerProperty("NXVISUAL");
				return _Nxvisual;
			}else {
				return _Nxvisual;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXVISUAL.
	 * @param v Value to Set.
	 */
	public void setNxvisual(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXVISUAL",v);
		_Nxvisual=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Nxvisdes=null;

	/**
	 * @return Returns the NXVISDES.
	 */
	public String getNxvisdes(){
		try{
			if (_Nxvisdes==null){
				_Nxvisdes=getStringProperty("NXVISDES");
				return _Nxvisdes;
			}else {
				return _Nxvisdes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXVISDES.
	 * @param v Value to Set.
	 */
	public void setNxvisdes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXVISDES",v);
		_Nxvisdes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Nxaudito=null;

	/**
	 * @return Returns the NXAUDITO.
	 */
	public Integer getNxaudito() {
		try{
			if (_Nxaudito==null){
				_Nxaudito=getIntegerProperty("NXAUDITO");
				return _Nxaudito;
			}else {
				return _Nxaudito;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXAUDITO.
	 * @param v Value to Set.
	 */
	public void setNxaudito(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXAUDITO",v);
		_Nxaudito=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Nxauddes=null;

	/**
	 * @return Returns the NXAUDDES.
	 */
	public String getNxauddes(){
		try{
			if (_Nxauddes==null){
				_Nxauddes=getStringProperty("NXAUDDES");
				return _Nxauddes;
			}else {
				return _Nxauddes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXAUDDES.
	 * @param v Value to Set.
	 */
	public void setNxauddes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXAUDDES",v);
		_Nxauddes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Nxtremor=null;

	/**
	 * @return Returns the NXTREMOR.
	 */
	public Integer getNxtremor() {
		try{
			if (_Nxtremor==null){
				_Nxtremor=getIntegerProperty("NXTREMOR");
				return _Nxtremor;
			}else {
				return _Nxtremor;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXTREMOR.
	 * @param v Value to Set.
	 */
	public void setNxtremor(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXTREMOR",v);
		_Nxtremor=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Nxtredes=null;

	/**
	 * @return Returns the NXTREDES.
	 */
	public String getNxtredes(){
		try{
			if (_Nxtredes==null){
				_Nxtredes=getStringProperty("NXTREDES");
				return _Nxtredes;
			}else {
				return _Nxtredes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXTREDES.
	 * @param v Value to Set.
	 */
	public void setNxtredes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXTREDES",v);
		_Nxtredes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Nxconsci=null;

	/**
	 * @return Returns the NXCONSCI.
	 */
	public Integer getNxconsci() {
		try{
			if (_Nxconsci==null){
				_Nxconsci=getIntegerProperty("NXCONSCI");
				return _Nxconsci;
			}else {
				return _Nxconsci;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXCONSCI.
	 * @param v Value to Set.
	 */
	public void setNxconsci(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXCONSCI",v);
		_Nxconsci=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Nxcondes=null;

	/**
	 * @return Returns the NXCONDES.
	 */
	public String getNxcondes(){
		try{
			if (_Nxcondes==null){
				_Nxcondes=getStringProperty("NXCONDES");
				return _Nxcondes;
			}else {
				return _Nxcondes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXCONDES.
	 * @param v Value to Set.
	 */
	public void setNxcondes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXCONDES",v);
		_Nxcondes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Nxnerve=null;

	/**
	 * @return Returns the NXNERVE.
	 */
	public Integer getNxnerve() {
		try{
			if (_Nxnerve==null){
				_Nxnerve=getIntegerProperty("NXNERVE");
				return _Nxnerve;
			}else {
				return _Nxnerve;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXNERVE.
	 * @param v Value to Set.
	 */
	public void setNxnerve(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXNERVE",v);
		_Nxnerve=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Nxnerdes=null;

	/**
	 * @return Returns the NXNERDES.
	 */
	public String getNxnerdes(){
		try{
			if (_Nxnerdes==null){
				_Nxnerdes=getStringProperty("NXNERDES");
				return _Nxnerdes;
			}else {
				return _Nxnerdes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXNERDES.
	 * @param v Value to Set.
	 */
	public void setNxnerdes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXNERDES",v);
		_Nxnerdes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Nxmotor=null;

	/**
	 * @return Returns the NXMOTOR.
	 */
	public Integer getNxmotor() {
		try{
			if (_Nxmotor==null){
				_Nxmotor=getIntegerProperty("NXMOTOR");
				return _Nxmotor;
			}else {
				return _Nxmotor;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXMOTOR.
	 * @param v Value to Set.
	 */
	public void setNxmotor(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXMOTOR",v);
		_Nxmotor=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Nxmotdes=null;

	/**
	 * @return Returns the NXMOTDES.
	 */
	public String getNxmotdes(){
		try{
			if (_Nxmotdes==null){
				_Nxmotdes=getStringProperty("NXMOTDES");
				return _Nxmotdes;
			}else {
				return _Nxmotdes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXMOTDES.
	 * @param v Value to Set.
	 */
	public void setNxmotdes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXMOTDES",v);
		_Nxmotdes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Nxfinger=null;

	/**
	 * @return Returns the NXFINGER.
	 */
	public Integer getNxfinger() {
		try{
			if (_Nxfinger==null){
				_Nxfinger=getIntegerProperty("NXFINGER");
				return _Nxfinger;
			}else {
				return _Nxfinger;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXFINGER.
	 * @param v Value to Set.
	 */
	public void setNxfinger(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXFINGER",v);
		_Nxfinger=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Nxfindes=null;

	/**
	 * @return Returns the NXFINDES.
	 */
	public String getNxfindes(){
		try{
			if (_Nxfindes==null){
				_Nxfindes=getStringProperty("NXFINDES");
				return _Nxfindes;
			}else {
				return _Nxfindes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXFINDES.
	 * @param v Value to Set.
	 */
	public void setNxfindes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXFINDES",v);
		_Nxfindes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Nxheel=null;

	/**
	 * @return Returns the NXHEEL.
	 */
	public Integer getNxheel() {
		try{
			if (_Nxheel==null){
				_Nxheel=getIntegerProperty("NXHEEL");
				return _Nxheel;
			}else {
				return _Nxheel;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXHEEL.
	 * @param v Value to Set.
	 */
	public void setNxheel(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXHEEL",v);
		_Nxheel=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Nxheedes=null;

	/**
	 * @return Returns the NXHEEDES.
	 */
	public String getNxheedes(){
		try{
			if (_Nxheedes==null){
				_Nxheedes=getStringProperty("NXHEEDES");
				return _Nxheedes;
			}else {
				return _Nxheedes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXHEEDES.
	 * @param v Value to Set.
	 */
	public void setNxheedes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXHEEDES",v);
		_Nxheedes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Nxsensor=null;

	/**
	 * @return Returns the NXSENSOR.
	 */
	public Integer getNxsensor() {
		try{
			if (_Nxsensor==null){
				_Nxsensor=getIntegerProperty("NXSENSOR");
				return _Nxsensor;
			}else {
				return _Nxsensor;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXSENSOR.
	 * @param v Value to Set.
	 */
	public void setNxsensor(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXSENSOR",v);
		_Nxsensor=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Nxsendes=null;

	/**
	 * @return Returns the NXSENDES.
	 */
	public String getNxsendes(){
		try{
			if (_Nxsendes==null){
				_Nxsendes=getStringProperty("NXSENDES");
				return _Nxsendes;
			}else {
				return _Nxsendes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXSENDES.
	 * @param v Value to Set.
	 */
	public void setNxsendes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXSENDES",v);
		_Nxsendes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Nxtendon=null;

	/**
	 * @return Returns the NXTENDON.
	 */
	public Integer getNxtendon() {
		try{
			if (_Nxtendon==null){
				_Nxtendon=getIntegerProperty("NXTENDON");
				return _Nxtendon;
			}else {
				return _Nxtendon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXTENDON.
	 * @param v Value to Set.
	 */
	public void setNxtendon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXTENDON",v);
		_Nxtendon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Nxtendes=null;

	/**
	 * @return Returns the NXTENDES.
	 */
	public String getNxtendes(){
		try{
			if (_Nxtendes==null){
				_Nxtendes=getStringProperty("NXTENDES");
				return _Nxtendes;
			}else {
				return _Nxtendes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXTENDES.
	 * @param v Value to Set.
	 */
	public void setNxtendes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXTENDES",v);
		_Nxtendes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Nxplanta=null;

	/**
	 * @return Returns the NXPLANTA.
	 */
	public Integer getNxplanta() {
		try{
			if (_Nxplanta==null){
				_Nxplanta=getIntegerProperty("NXPLANTA");
				return _Nxplanta;
			}else {
				return _Nxplanta;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXPLANTA.
	 * @param v Value to Set.
	 */
	public void setNxplanta(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXPLANTA",v);
		_Nxplanta=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Nxplades=null;

	/**
	 * @return Returns the NXPLADES.
	 */
	public String getNxplades(){
		try{
			if (_Nxplades==null){
				_Nxplades=getStringProperty("NXPLADES");
				return _Nxplades;
			}else {
				return _Nxplades;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXPLADES.
	 * @param v Value to Set.
	 */
	public void setNxplades(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXPLADES",v);
		_Nxplades=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Nxgait=null;

	/**
	 * @return Returns the NXGAIT.
	 */
	public Integer getNxgait() {
		try{
			if (_Nxgait==null){
				_Nxgait=getIntegerProperty("NXGAIT");
				return _Nxgait;
			}else {
				return _Nxgait;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXGAIT.
	 * @param v Value to Set.
	 */
	public void setNxgait(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXGAIT",v);
		_Nxgait=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Nxgaides=null;

	/**
	 * @return Returns the NXGAIDES.
	 */
	public String getNxgaides(){
		try{
			if (_Nxgaides==null){
				_Nxgaides=getStringProperty("NXGAIDES");
				return _Nxgaides;
			}else {
				return _Nxgaides;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXGAIDES.
	 * @param v Value to Set.
	 */
	public void setNxgaides(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXGAIDES",v);
		_Nxgaides=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Nxother=null;

	/**
	 * @return Returns the NXOTHER.
	 */
	public Integer getNxother() {
		try{
			if (_Nxother==null){
				_Nxother=getIntegerProperty("NXOTHER");
				return _Nxother;
			}else {
				return _Nxother;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXOTHER.
	 * @param v Value to Set.
	 */
	public void setNxother(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXOTHER",v);
		_Nxother=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Nxothspe=null;

	/**
	 * @return Returns the NXOTHSPE.
	 */
	public String getNxothspe(){
		try{
			if (_Nxothspe==null){
				_Nxothspe=getStringProperty("NXOTHSPE");
				return _Nxothspe;
			}else {
				return _Nxothspe;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXOTHSPE.
	 * @param v Value to Set.
	 */
	public void setNxothspe(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXOTHSPE",v);
		_Nxothspe=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Nxgencom=null;

	/**
	 * @return Returns the NXGENCOM.
	 */
	public String getNxgencom(){
		try{
			if (_Nxgencom==null){
				_Nxgencom=getStringProperty("NXGENCOM");
				return _Nxgencom;
			}else {
				return _Nxgencom;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXGENCOM.
	 * @param v Value to Set.
	 */
	public void setNxgencom(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXGENCOM",v);
		_Nxgencom=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.ClinNeuroexmdata> getAllClinNeuroexmdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.ClinNeuroexmdata> al = new ArrayList<org.nrg.xdat.om.ClinNeuroexmdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.ClinNeuroexmdata> getClinNeuroexmdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.ClinNeuroexmdata> al = new ArrayList<org.nrg.xdat.om.ClinNeuroexmdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.ClinNeuroexmdata> getClinNeuroexmdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.ClinNeuroexmdata> al = new ArrayList<org.nrg.xdat.om.ClinNeuroexmdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ClinNeuroexmdata getClinNeuroexmdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("clin:neuroexmData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (ClinNeuroexmdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
