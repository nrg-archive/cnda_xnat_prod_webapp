/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoTissueTisscolldata extends XnatSubjectassessordata implements org.nrg.xdat.model.TissueTisscolldataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoTissueTisscolldata.class);
	public static String SCHEMA_ELEMENT_NAME="tissue:tissCollData";

	public AutoTissueTisscolldata(ItemI item)
	{
		super(item);
	}

	public AutoTissueTisscolldata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoTissueTisscolldata(UserI user)
	 **/
	public AutoTissueTisscolldata(){}

	public AutoTissueTisscolldata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "tissue:tissCollData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Tisstype=null;

	/**
	 * @return Returns the tissType.
	 */
	public String getTisstype(){
		try{
			if (_Tisstype==null){
				_Tisstype=getStringProperty("tissType");
				return _Tisstype;
			}else {
				return _Tisstype;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tissType.
	 * @param v Value to Set.
	 */
	public void setTisstype(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tissType",v);
		_Tisstype=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Collector=null;

	/**
	 * @return Returns the collector.
	 */
	public String getCollector(){
		try{
			if (_Collector==null){
				_Collector=getStringProperty("collector");
				return _Collector;
			}else {
				return _Collector;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for collector.
	 * @param v Value to Set.
	 */
	public void setCollector(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/collector",v);
		_Collector=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Method=null;

	/**
	 * @return Returns the method.
	 */
	public String getMethod(){
		try{
			if (_Method==null){
				_Method=getStringProperty("method");
				return _Method;
			}else {
				return _Method;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for method.
	 * @param v Value to Set.
	 */
	public void setMethod(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/method",v);
		_Method=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Complicsbool=null;

	/**
	 * @return Returns the complicsBool.
	 */
	public Boolean getComplicsbool() {
		try{
			if (_Complicsbool==null){
				_Complicsbool=getBooleanProperty("complicsBool");
				return _Complicsbool;
			}else {
				return _Complicsbool;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for complicsBool.
	 * @param v Value to Set.
	 */
	public void setComplicsbool(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/complicsBool",v);
		_Complicsbool=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Complicsdetails=null;

	/**
	 * @return Returns the complicsDetails.
	 */
	public String getComplicsdetails(){
		try{
			if (_Complicsdetails==null){
				_Complicsdetails=getStringProperty("complicsDetails");
				return _Complicsdetails;
			}else {
				return _Complicsdetails;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for complicsDetails.
	 * @param v Value to Set.
	 */
	public void setComplicsdetails(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/complicsDetails",v);
		_Complicsdetails=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Altlabel1=null;

	/**
	 * @return Returns the altLabel1.
	 */
	public String getAltlabel1(){
		try{
			if (_Altlabel1==null){
				_Altlabel1=getStringProperty("altLabel1");
				return _Altlabel1;
			}else {
				return _Altlabel1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for altLabel1.
	 * @param v Value to Set.
	 */
	public void setAltlabel1(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/altLabel1",v);
		_Altlabel1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Altlabel1ref=null;

	/**
	 * @return Returns the altLabel1Ref.
	 */
	public String getAltlabel1ref(){
		try{
			if (_Altlabel1ref==null){
				_Altlabel1ref=getStringProperty("altLabel1Ref");
				return _Altlabel1ref;
			}else {
				return _Altlabel1ref;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for altLabel1Ref.
	 * @param v Value to Set.
	 */
	public void setAltlabel1ref(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/altLabel1Ref",v);
		_Altlabel1ref=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Altlabel2=null;

	/**
	 * @return Returns the altLabel2.
	 */
	public String getAltlabel2(){
		try{
			if (_Altlabel2==null){
				_Altlabel2=getStringProperty("altLabel2");
				return _Altlabel2;
			}else {
				return _Altlabel2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for altLabel2.
	 * @param v Value to Set.
	 */
	public void setAltlabel2(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/altLabel2",v);
		_Altlabel2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Altlabel2ref=null;

	/**
	 * @return Returns the altLabel2Ref.
	 */
	public String getAltlabel2ref(){
		try{
			if (_Altlabel2ref==null){
				_Altlabel2ref=getStringProperty("altLabel2Ref");
				return _Altlabel2ref;
			}else {
				return _Altlabel2ref;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for altLabel2Ref.
	 * @param v Value to Set.
	 */
	public void setAltlabel2ref(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/altLabel2Ref",v);
		_Altlabel2ref=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Altlabel3=null;

	/**
	 * @return Returns the altLabel3.
	 */
	public String getAltlabel3(){
		try{
			if (_Altlabel3==null){
				_Altlabel3=getStringProperty("altLabel3");
				return _Altlabel3;
			}else {
				return _Altlabel3;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for altLabel3.
	 * @param v Value to Set.
	 */
	public void setAltlabel3(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/altLabel3",v);
		_Altlabel3=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Altlabel3ref=null;

	/**
	 * @return Returns the altLabel3Ref.
	 */
	public String getAltlabel3ref(){
		try{
			if (_Altlabel3ref==null){
				_Altlabel3ref=getStringProperty("altLabel3Ref");
				return _Altlabel3ref;
			}else {
				return _Altlabel3ref;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for altLabel3Ref.
	 * @param v Value to Set.
	 */
	public void setAltlabel3ref(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/altLabel3Ref",v);
		_Altlabel3ref=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.TissueTisscolldata> getAllTissueTisscolldatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TissueTisscolldata> al = new ArrayList<org.nrg.xdat.om.TissueTisscolldata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.TissueTisscolldata> getTissueTisscolldatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TissueTisscolldata> al = new ArrayList<org.nrg.xdat.om.TissueTisscolldata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.TissueTisscolldata> getTissueTisscolldatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TissueTisscolldata> al = new ArrayList<org.nrg.xdat.om.TissueTisscolldata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static TissueTisscolldata getTissueTisscolldatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("tissue:tissCollData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (TissueTisscolldata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
