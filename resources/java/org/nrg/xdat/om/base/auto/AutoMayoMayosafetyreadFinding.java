/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoMayoMayosafetyreadFinding extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.MayoMayosafetyreadFindingI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoMayoMayosafetyreadFinding.class);
	public static String SCHEMA_ELEMENT_NAME="mayo:mayoSafetyRead_finding";

	public AutoMayoMayosafetyreadFinding(ItemI item)
	{
		super(item);
	}

	public AutoMayoMayosafetyreadFinding(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoMayoMayosafetyreadFinding(UserI user)
	 **/
	public AutoMayoMayosafetyreadFinding(){}

	public AutoMayoMayosafetyreadFinding(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "mayo:mayoSafetyRead_finding";
	}

	//FIELD

	private Object _Visit=null;

	/**
	 * @return Returns the visit.
	 */
	public Object getVisit(){
		try{
			if (_Visit==null){
				_Visit=getProperty("visit");
				return _Visit;
			}else {
				return _Visit;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for visit.
	 * @param v Value to Set.
	 */
	public void setVisit(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/visit",v);
		_Visit=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Visittime=null;

	/**
	 * @return Returns the visitTime.
	 */
	public Object getVisittime(){
		try{
			if (_Visittime==null){
				_Visittime=getProperty("visitTime");
				return _Visittime;
			}else {
				return _Visittime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for visitTime.
	 * @param v Value to Set.
	 */
	public void setVisittime(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/visitTime",v);
		_Visittime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Findingtype=null;

	/**
	 * @return Returns the findingType.
	 */
	public String getFindingtype(){
		try{
			if (_Findingtype==null){
				_Findingtype=getStringProperty("findingType");
				return _Findingtype;
			}else {
				return _Findingtype;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for findingType.
	 * @param v Value to Set.
	 */
	public void setFindingtype(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/findingType",v);
		_Findingtype=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Change=null;

	/**
	 * @return Returns the change.
	 */
	public Boolean getChange() {
		try{
			if (_Change==null){
				_Change=getBooleanProperty("change");
				return _Change;
			}else {
				return _Change;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for change.
	 * @param v Value to Set.
	 */
	public void setChange(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/change",v);
		_Change=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Definitecount=null;

	/**
	 * @return Returns the definiteCount.
	 */
	public Integer getDefinitecount() {
		try{
			if (_Definitecount==null){
				_Definitecount=getIntegerProperty("definiteCount");
				return _Definitecount;
			}else {
				return _Definitecount;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for definiteCount.
	 * @param v Value to Set.
	 */
	public void setDefinitecount(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/definiteCount",v);
		_Definitecount=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Definitemagnitude=null;

	/**
	 * @return Returns the definiteMagnitude.
	 */
	public Double getDefinitemagnitude() {
		try{
			if (_Definitemagnitude==null){
				_Definitemagnitude=getDoubleProperty("definiteMagnitude");
				return _Definitemagnitude;
			}else {
				return _Definitemagnitude;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for definiteMagnitude.
	 * @param v Value to Set.
	 */
	public void setDefinitemagnitude(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/definiteMagnitude",v);
		_Definitemagnitude=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Possiblecount=null;

	/**
	 * @return Returns the possibleCount.
	 */
	public Integer getPossiblecount() {
		try{
			if (_Possiblecount==null){
				_Possiblecount=getIntegerProperty("possibleCount");
				return _Possiblecount;
			}else {
				return _Possiblecount;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for possibleCount.
	 * @param v Value to Set.
	 */
	public void setPossiblecount(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/possibleCount",v);
		_Possiblecount=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Possiblemagnitude=null;

	/**
	 * @return Returns the possibleMagnitude.
	 */
	public Double getPossiblemagnitude() {
		try{
			if (_Possiblemagnitude==null){
				_Possiblemagnitude=getDoubleProperty("possibleMagnitude");
				return _Possiblemagnitude;
			}else {
				return _Possiblemagnitude;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for possibleMagnitude.
	 * @param v Value to Set.
	 */
	public void setPossiblemagnitude(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/possibleMagnitude",v);
		_Possiblemagnitude=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Noimage=null;

	/**
	 * @return Returns the noImage.
	 */
	public Boolean getNoimage() {
		try{
			if (_Noimage==null){
				_Noimage=getBooleanProperty("noImage");
				return _Noimage;
			}else {
				return _Noimage;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for noImage.
	 * @param v Value to Set.
	 */
	public void setNoimage(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/noImage",v);
		_Noimage=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Failedimage=null;

	/**
	 * @return Returns the failedImage.
	 */
	public Boolean getFailedimage() {
		try{
			if (_Failedimage==null){
				_Failedimage=getBooleanProperty("failedImage");
				return _Failedimage;
			}else {
				return _Failedimage;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for failedImage.
	 * @param v Value to Set.
	 */
	public void setFailedimage(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/failedImage",v);
		_Failedimage=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Seenotes=null;

	/**
	 * @return Returns the seeNotes.
	 */
	public Boolean getSeenotes() {
		try{
			if (_Seenotes==null){
				_Seenotes=getBooleanProperty("seeNotes");
				return _Seenotes;
			}else {
				return _Seenotes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for seeNotes.
	 * @param v Value to Set.
	 */
	public void setSeenotes(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/seeNotes",v);
		_Seenotes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _MayoMayosafetyreadFindingId=null;

	/**
	 * @return Returns the mayo_mayoSafetyRead_finding_id.
	 */
	public Integer getMayoMayosafetyreadFindingId() {
		try{
			if (_MayoMayosafetyreadFindingId==null){
				_MayoMayosafetyreadFindingId=getIntegerProperty("mayo_mayoSafetyRead_finding_id");
				return _MayoMayosafetyreadFindingId;
			}else {
				return _MayoMayosafetyreadFindingId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for mayo_mayoSafetyRead_finding_id.
	 * @param v Value to Set.
	 */
	public void setMayoMayosafetyreadFindingId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/mayo_mayoSafetyRead_finding_id",v);
		_MayoMayosafetyreadFindingId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.MayoMayosafetyreadFinding> getAllMayoMayosafetyreadFindings(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.MayoMayosafetyreadFinding> al = new ArrayList<org.nrg.xdat.om.MayoMayosafetyreadFinding>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.MayoMayosafetyreadFinding> getMayoMayosafetyreadFindingsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.MayoMayosafetyreadFinding> al = new ArrayList<org.nrg.xdat.om.MayoMayosafetyreadFinding>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.MayoMayosafetyreadFinding> getMayoMayosafetyreadFindingsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.MayoMayosafetyreadFinding> al = new ArrayList<org.nrg.xdat.om.MayoMayosafetyreadFinding>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static MayoMayosafetyreadFinding getMayoMayosafetyreadFindingsByMayoMayosafetyreadFindingId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("mayo:mayoSafetyRead_finding/mayo_mayoSafetyRead_finding_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (MayoMayosafetyreadFinding) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
