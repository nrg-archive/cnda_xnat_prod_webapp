/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:01 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoFsAparcregionanalysis extends XnatMrassessordata implements org.nrg.xdat.model.FsAparcregionanalysisI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoFsAparcregionanalysis.class);
	public static String SCHEMA_ELEMENT_NAME="fs:aparcRegionAnalysis";

	public AutoFsAparcregionanalysis(ItemI item)
	{
		super(item);
	}

	public AutoFsAparcregionanalysis(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoFsAparcregionanalysis(UserI user)
	 **/
	public AutoFsAparcregionanalysis(){}

	public AutoFsAparcregionanalysis(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "fs:aparcRegionAnalysis";
	}
	 private org.nrg.xdat.om.XnatMrassessordata _Mrassessordata =null;

	/**
	 * mrAssessorData
	 * @return org.nrg.xdat.om.XnatMrassessordata
	 */
	public org.nrg.xdat.om.XnatMrassessordata getMrassessordata() {
		try{
			if (_Mrassessordata==null){
				_Mrassessordata=((XnatMrassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("mrAssessorData")));
				return _Mrassessordata;
			}else {
				return _Mrassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for mrAssessorData.
	 * @param v Value to Set.
	 */
	public void setMrassessordata(ItemI v) throws Exception{
		_Mrassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/mrAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/mrAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * mrAssessorData
	 * set org.nrg.xdat.model.XnatMrassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatMrassessordataI> void setMrassessordata(A item) throws Exception{
	setMrassessordata((ItemI)item);
	}

	/**
	 * Removes the mrAssessorData.
	 * */
	public void removeMrassessordata() {
		_Mrassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/mrAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.FsAparcregionanalysisHemisphere> _Hemisphere =null;

	/**
	 * hemisphere
	 * @return Returns an List of org.nrg.xdat.om.FsAparcregionanalysisHemisphere
	 */
	public <A extends org.nrg.xdat.model.FsAparcregionanalysisHemisphereI> List<A> getHemisphere() {
		try{
			if (_Hemisphere==null){
				_Hemisphere=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("hemisphere"));
			}
			return (List<A>) _Hemisphere;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.FsAparcregionanalysisHemisphere>();}
	}

	/**
	 * Sets the value for hemisphere.
	 * @param v Value to Set.
	 */
	public void setHemisphere(ItemI v) throws Exception{
		_Hemisphere =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/hemisphere",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/hemisphere",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * hemisphere
	 * Adds org.nrg.xdat.model.FsAparcregionanalysisHemisphereI
	 */
	public <A extends org.nrg.xdat.model.FsAparcregionanalysisHemisphereI> void addHemisphere(A item) throws Exception{
	setHemisphere((ItemI)item);
	}

	/**
	 * Removes the hemisphere of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeHemisphere(int index) throws java.lang.IndexOutOfBoundsException {
		_Hemisphere =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/hemisphere",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.FsAparcregionanalysis> getAllFsAparcregionanalysiss(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.FsAparcregionanalysis> al = new ArrayList<org.nrg.xdat.om.FsAparcregionanalysis>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.FsAparcregionanalysis> getFsAparcregionanalysissByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.FsAparcregionanalysis> al = new ArrayList<org.nrg.xdat.om.FsAparcregionanalysis>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.FsAparcregionanalysis> getFsAparcregionanalysissByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.FsAparcregionanalysis> al = new ArrayList<org.nrg.xdat.om.FsAparcregionanalysis>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static FsAparcregionanalysis getFsAparcregionanalysissById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("fs:aparcRegionAnalysis/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (FsAparcregionanalysis) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		XFTItem mr = org.nrg.xft.search.ItemSearch.GetItem("xnat:mrSessionData.ID",getItem().getProperty("fs:aparcRegionAnalysis.imageSession_ID"),getItem().getUser(),false);
		al.add(mr);
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",mr.getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //mrAssessorData
	        XnatMrassessordata childMrassessordata = (XnatMrassessordata)this.getMrassessordata();
	            if (childMrassessordata!=null){
	              for(ResourceFile rf: ((XnatMrassessordata)childMrassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("mrAssessorData[" + ((XnatMrassessordata)childMrassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("mrAssessorData/" + ((XnatMrassessordata)childMrassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //hemisphere
	        for(org.nrg.xdat.model.FsAparcregionanalysisHemisphereI childHemisphere : this.getHemisphere()){
	            if (childHemisphere!=null){
	              for(ResourceFile rf: ((FsAparcregionanalysisHemisphere)childHemisphere).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("hemisphere[" + ((FsAparcregionanalysisHemisphere)childHemisphere).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("hemisphere/" + ((FsAparcregionanalysisHemisphere)childHemisphere).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
