/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoUdsA5subhstdata extends XnatSubjectassessordata implements org.nrg.xdat.model.UdsA5subhstdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoUdsA5subhstdata.class);
	public static String SCHEMA_ELEMENT_NAME="uds:a5subhstData";

	public AutoUdsA5subhstdata(ItemI item)
	{
		super(item);
	}

	public AutoUdsA5subhstdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoUdsA5subhstdata(UserI user)
	 **/
	public AutoUdsA5subhstdata(){}

	public AutoUdsA5subhstdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "uds:a5subhstData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Examdate=null;

	/**
	 * @return Returns the EXAMDATE.
	 */
	public Object getExamdate(){
		try{
			if (_Examdate==null){
				_Examdate=getProperty("EXAMDATE");
				return _Examdate;
			}else {
				return _Examdate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for EXAMDATE.
	 * @param v Value to Set.
	 */
	public void setExamdate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/EXAMDATE",v);
		_Examdate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvhatt=null;

	/**
	 * @return Returns the CVHATT.
	 */
	public Integer getCvhatt() {
		try{
			if (_Cvhatt==null){
				_Cvhatt=getIntegerProperty("CVHATT");
				return _Cvhatt;
			}else {
				return _Cvhatt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVHATT.
	 * @param v Value to Set.
	 */
	public void setCvhatt(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVHATT",v);
		_Cvhatt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvafib=null;

	/**
	 * @return Returns the CVAFIB.
	 */
	public Integer getCvafib() {
		try{
			if (_Cvafib==null){
				_Cvafib=getIntegerProperty("CVAFIB");
				return _Cvafib;
			}else {
				return _Cvafib;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVAFIB.
	 * @param v Value to Set.
	 */
	public void setCvafib(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVAFIB",v);
		_Cvafib=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvangio=null;

	/**
	 * @return Returns the CVANGIO.
	 */
	public Integer getCvangio() {
		try{
			if (_Cvangio==null){
				_Cvangio=getIntegerProperty("CVANGIO");
				return _Cvangio;
			}else {
				return _Cvangio;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVANGIO.
	 * @param v Value to Set.
	 */
	public void setCvangio(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVANGIO",v);
		_Cvangio=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvbypass=null;

	/**
	 * @return Returns the CVBYPASS.
	 */
	public Integer getCvbypass() {
		try{
			if (_Cvbypass==null){
				_Cvbypass=getIntegerProperty("CVBYPASS");
				return _Cvbypass;
			}else {
				return _Cvbypass;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVBYPASS.
	 * @param v Value to Set.
	 */
	public void setCvbypass(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVBYPASS",v);
		_Cvbypass=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvpace=null;

	/**
	 * @return Returns the CVPACE.
	 */
	public Integer getCvpace() {
		try{
			if (_Cvpace==null){
				_Cvpace=getIntegerProperty("CVPACE");
				return _Cvpace;
			}else {
				return _Cvpace;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVPACE.
	 * @param v Value to Set.
	 */
	public void setCvpace(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVPACE",v);
		_Cvpace=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvchf=null;

	/**
	 * @return Returns the CVCHF.
	 */
	public Integer getCvchf() {
		try{
			if (_Cvchf==null){
				_Cvchf=getIntegerProperty("CVCHF");
				return _Cvchf;
			}else {
				return _Cvchf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVCHF.
	 * @param v Value to Set.
	 */
	public void setCvchf(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVCHF",v);
		_Cvchf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvothr=null;

	/**
	 * @return Returns the CVOTHR.
	 */
	public Integer getCvothr() {
		try{
			if (_Cvothr==null){
				_Cvothr=getIntegerProperty("CVOTHR");
				return _Cvothr;
			}else {
				return _Cvothr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOTHR.
	 * @param v Value to Set.
	 */
	public void setCvothr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOTHR",v);
		_Cvothr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Cvothrx=null;

	/**
	 * @return Returns the CVOTHRX.
	 */
	public String getCvothrx(){
		try{
			if (_Cvothrx==null){
				_Cvothrx=getStringProperty("CVOTHRX");
				return _Cvothrx;
			}else {
				return _Cvothrx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOTHRX.
	 * @param v Value to Set.
	 */
	public void setCvothrx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOTHRX",v);
		_Cvothrx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cbstroke=null;

	/**
	 * @return Returns the CBSTROKE.
	 */
	public Integer getCbstroke() {
		try{
			if (_Cbstroke==null){
				_Cbstroke=getIntegerProperty("CBSTROKE");
				return _Cbstroke;
			}else {
				return _Cbstroke;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CBSTROKE.
	 * @param v Value to Set.
	 */
	public void setCbstroke(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CBSTROKE",v);
		_Cbstroke=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Strok1yr=null;

	/**
	 * @return Returns the STROK1YR.
	 */
	public Integer getStrok1yr() {
		try{
			if (_Strok1yr==null){
				_Strok1yr=getIntegerProperty("STROK1YR");
				return _Strok1yr;
			}else {
				return _Strok1yr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for STROK1YR.
	 * @param v Value to Set.
	 */
	public void setStrok1yr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/STROK1YR",v);
		_Strok1yr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Strok2yr=null;

	/**
	 * @return Returns the STROK2YR.
	 */
	public Integer getStrok2yr() {
		try{
			if (_Strok2yr==null){
				_Strok2yr=getIntegerProperty("STROK2YR");
				return _Strok2yr;
			}else {
				return _Strok2yr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for STROK2YR.
	 * @param v Value to Set.
	 */
	public void setStrok2yr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/STROK2YR",v);
		_Strok2yr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Strok3yr=null;

	/**
	 * @return Returns the STROK3YR.
	 */
	public Integer getStrok3yr() {
		try{
			if (_Strok3yr==null){
				_Strok3yr=getIntegerProperty("STROK3YR");
				return _Strok3yr;
			}else {
				return _Strok3yr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for STROK3YR.
	 * @param v Value to Set.
	 */
	public void setStrok3yr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/STROK3YR",v);
		_Strok3yr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Strok4yr=null;

	/**
	 * @return Returns the STROK4YR.
	 */
	public Integer getStrok4yr() {
		try{
			if (_Strok4yr==null){
				_Strok4yr=getIntegerProperty("STROK4YR");
				return _Strok4yr;
			}else {
				return _Strok4yr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for STROK4YR.
	 * @param v Value to Set.
	 */
	public void setStrok4yr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/STROK4YR",v);
		_Strok4yr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Strok5yr=null;

	/**
	 * @return Returns the STROK5YR.
	 */
	public Integer getStrok5yr() {
		try{
			if (_Strok5yr==null){
				_Strok5yr=getIntegerProperty("STROK5YR");
				return _Strok5yr;
			}else {
				return _Strok5yr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for STROK5YR.
	 * @param v Value to Set.
	 */
	public void setStrok5yr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/STROK5YR",v);
		_Strok5yr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Strok6yr=null;

	/**
	 * @return Returns the STROK6YR.
	 */
	public Integer getStrok6yr() {
		try{
			if (_Strok6yr==null){
				_Strok6yr=getIntegerProperty("STROK6YR");
				return _Strok6yr;
			}else {
				return _Strok6yr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for STROK6YR.
	 * @param v Value to Set.
	 */
	public void setStrok6yr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/STROK6YR",v);
		_Strok6yr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cbtia=null;

	/**
	 * @return Returns the CBTIA.
	 */
	public Integer getCbtia() {
		try{
			if (_Cbtia==null){
				_Cbtia=getIntegerProperty("CBTIA");
				return _Cbtia;
			}else {
				return _Cbtia;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CBTIA.
	 * @param v Value to Set.
	 */
	public void setCbtia(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CBTIA",v);
		_Cbtia=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tia1yr=null;

	/**
	 * @return Returns the TIA1YR.
	 */
	public Integer getTia1yr() {
		try{
			if (_Tia1yr==null){
				_Tia1yr=getIntegerProperty("TIA1YR");
				return _Tia1yr;
			}else {
				return _Tia1yr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TIA1YR.
	 * @param v Value to Set.
	 */
	public void setTia1yr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TIA1YR",v);
		_Tia1yr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tia2yr=null;

	/**
	 * @return Returns the TIA2YR.
	 */
	public Integer getTia2yr() {
		try{
			if (_Tia2yr==null){
				_Tia2yr=getIntegerProperty("TIA2YR");
				return _Tia2yr;
			}else {
				return _Tia2yr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TIA2YR.
	 * @param v Value to Set.
	 */
	public void setTia2yr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TIA2YR",v);
		_Tia2yr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tia3yr=null;

	/**
	 * @return Returns the TIA3YR.
	 */
	public Integer getTia3yr() {
		try{
			if (_Tia3yr==null){
				_Tia3yr=getIntegerProperty("TIA3YR");
				return _Tia3yr;
			}else {
				return _Tia3yr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TIA3YR.
	 * @param v Value to Set.
	 */
	public void setTia3yr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TIA3YR",v);
		_Tia3yr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tia4yr=null;

	/**
	 * @return Returns the TIA4YR.
	 */
	public Integer getTia4yr() {
		try{
			if (_Tia4yr==null){
				_Tia4yr=getIntegerProperty("TIA4YR");
				return _Tia4yr;
			}else {
				return _Tia4yr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TIA4YR.
	 * @param v Value to Set.
	 */
	public void setTia4yr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TIA4YR",v);
		_Tia4yr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tia5yr=null;

	/**
	 * @return Returns the TIA5YR.
	 */
	public Integer getTia5yr() {
		try{
			if (_Tia5yr==null){
				_Tia5yr=getIntegerProperty("TIA5YR");
				return _Tia5yr;
			}else {
				return _Tia5yr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TIA5YR.
	 * @param v Value to Set.
	 */
	public void setTia5yr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TIA5YR",v);
		_Tia5yr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tia6yr=null;

	/**
	 * @return Returns the TIA6YR.
	 */
	public Integer getTia6yr() {
		try{
			if (_Tia6yr==null){
				_Tia6yr=getIntegerProperty("TIA6YR");
				return _Tia6yr;
			}else {
				return _Tia6yr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TIA6YR.
	 * @param v Value to Set.
	 */
	public void setTia6yr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TIA6YR",v);
		_Tia6yr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cbothr=null;

	/**
	 * @return Returns the CBOTHR.
	 */
	public Integer getCbothr() {
		try{
			if (_Cbothr==null){
				_Cbothr=getIntegerProperty("CBOTHR");
				return _Cbothr;
			}else {
				return _Cbothr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CBOTHR.
	 * @param v Value to Set.
	 */
	public void setCbothr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CBOTHR",v);
		_Cbothr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Cbothrx=null;

	/**
	 * @return Returns the CBOTHRX.
	 */
	public String getCbothrx(){
		try{
			if (_Cbothrx==null){
				_Cbothrx=getStringProperty("CBOTHRX");
				return _Cbothrx;
			}else {
				return _Cbothrx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CBOTHRX.
	 * @param v Value to Set.
	 */
	public void setCbothrx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CBOTHRX",v);
		_Cbothrx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pd=null;

	/**
	 * @return Returns the PD.
	 */
	public Integer getPd() {
		try{
			if (_Pd==null){
				_Pd=getIntegerProperty("PD");
				return _Pd;
			}else {
				return _Pd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PD.
	 * @param v Value to Set.
	 */
	public void setPd(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PD",v);
		_Pd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pdyr=null;

	/**
	 * @return Returns the PDYR.
	 */
	public Integer getPdyr() {
		try{
			if (_Pdyr==null){
				_Pdyr=getIntegerProperty("PDYR");
				return _Pdyr;
			}else {
				return _Pdyr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PDYR.
	 * @param v Value to Set.
	 */
	public void setPdyr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PDYR",v);
		_Pdyr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pdothr=null;

	/**
	 * @return Returns the PDOTHR.
	 */
	public Integer getPdothr() {
		try{
			if (_Pdothr==null){
				_Pdothr=getIntegerProperty("PDOTHR");
				return _Pdothr;
			}else {
				return _Pdothr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PDOTHR.
	 * @param v Value to Set.
	 */
	public void setPdothr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PDOTHR",v);
		_Pdothr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pdothryr=null;

	/**
	 * @return Returns the PDOTHRYR.
	 */
	public Integer getPdothryr() {
		try{
			if (_Pdothryr==null){
				_Pdothryr=getIntegerProperty("PDOTHRYR");
				return _Pdothryr;
			}else {
				return _Pdothryr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PDOTHRYR.
	 * @param v Value to Set.
	 */
	public void setPdothryr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PDOTHRYR",v);
		_Pdothryr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Seizures=null;

	/**
	 * @return Returns the SEIZURES.
	 */
	public Integer getSeizures() {
		try{
			if (_Seizures==null){
				_Seizures=getIntegerProperty("SEIZURES");
				return _Seizures;
			}else {
				return _Seizures;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SEIZURES.
	 * @param v Value to Set.
	 */
	public void setSeizures(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SEIZURES",v);
		_Seizures=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Traumbrf=null;

	/**
	 * @return Returns the TRAUMBRF.
	 */
	public Integer getTraumbrf() {
		try{
			if (_Traumbrf==null){
				_Traumbrf=getIntegerProperty("TRAUMBRF");
				return _Traumbrf;
			}else {
				return _Traumbrf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRAUMBRF.
	 * @param v Value to Set.
	 */
	public void setTraumbrf(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRAUMBRF",v);
		_Traumbrf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Traumext=null;

	/**
	 * @return Returns the TRAUMEXT.
	 */
	public Integer getTraumext() {
		try{
			if (_Traumext==null){
				_Traumext=getIntegerProperty("TRAUMEXT");
				return _Traumext;
			}else {
				return _Traumext;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRAUMEXT.
	 * @param v Value to Set.
	 */
	public void setTraumext(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRAUMEXT",v);
		_Traumext=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Traumchr=null;

	/**
	 * @return Returns the TRAUMCHR.
	 */
	public Integer getTraumchr() {
		try{
			if (_Traumchr==null){
				_Traumchr=getIntegerProperty("TRAUMCHR");
				return _Traumchr;
			}else {
				return _Traumchr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRAUMCHR.
	 * @param v Value to Set.
	 */
	public void setTraumchr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRAUMCHR",v);
		_Traumchr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ncothr=null;

	/**
	 * @return Returns the NCOTHR.
	 */
	public Integer getNcothr() {
		try{
			if (_Ncothr==null){
				_Ncothr=getIntegerProperty("NCOTHR");
				return _Ncothr;
			}else {
				return _Ncothr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NCOTHR.
	 * @param v Value to Set.
	 */
	public void setNcothr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NCOTHR",v);
		_Ncothr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Ncothrx=null;

	/**
	 * @return Returns the NCOTHRX.
	 */
	public String getNcothrx(){
		try{
			if (_Ncothrx==null){
				_Ncothrx=getStringProperty("NCOTHRX");
				return _Ncothrx;
			}else {
				return _Ncothrx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NCOTHRX.
	 * @param v Value to Set.
	 */
	public void setNcothrx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NCOTHRX",v);
		_Ncothrx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Hyperten=null;

	/**
	 * @return Returns the HYPERTEN.
	 */
	public Integer getHyperten() {
		try{
			if (_Hyperten==null){
				_Hyperten=getIntegerProperty("HYPERTEN");
				return _Hyperten;
			}else {
				return _Hyperten;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HYPERTEN.
	 * @param v Value to Set.
	 */
	public void setHyperten(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HYPERTEN",v);
		_Hyperten=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Hypercho=null;

	/**
	 * @return Returns the HYPERCHO.
	 */
	public Integer getHypercho() {
		try{
			if (_Hypercho==null){
				_Hypercho=getIntegerProperty("HYPERCHO");
				return _Hypercho;
			}else {
				return _Hypercho;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HYPERCHO.
	 * @param v Value to Set.
	 */
	public void setHypercho(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HYPERCHO",v);
		_Hypercho=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Diabetes=null;

	/**
	 * @return Returns the DIABETES.
	 */
	public Integer getDiabetes() {
		try{
			if (_Diabetes==null){
				_Diabetes=getIntegerProperty("DIABETES");
				return _Diabetes;
			}else {
				return _Diabetes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DIABETES.
	 * @param v Value to Set.
	 */
	public void setDiabetes(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DIABETES",v);
		_Diabetes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _B12def=null;

	/**
	 * @return Returns the B12DEF.
	 */
	public Integer getB12def() {
		try{
			if (_B12def==null){
				_B12def=getIntegerProperty("B12DEF");
				return _B12def;
			}else {
				return _B12def;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for B12DEF.
	 * @param v Value to Set.
	 */
	public void setB12def(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/B12DEF",v);
		_B12def=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Thyroid=null;

	/**
	 * @return Returns the THYROID.
	 */
	public Integer getThyroid() {
		try{
			if (_Thyroid==null){
				_Thyroid=getIntegerProperty("THYROID");
				return _Thyroid;
			}else {
				return _Thyroid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for THYROID.
	 * @param v Value to Set.
	 */
	public void setThyroid(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/THYROID",v);
		_Thyroid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Incontu=null;

	/**
	 * @return Returns the INCONTU.
	 */
	public Integer getIncontu() {
		try{
			if (_Incontu==null){
				_Incontu=getIntegerProperty("INCONTU");
				return _Incontu;
			}else {
				return _Incontu;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INCONTU.
	 * @param v Value to Set.
	 */
	public void setIncontu(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INCONTU",v);
		_Incontu=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Incontf=null;

	/**
	 * @return Returns the INCONTF.
	 */
	public Integer getIncontf() {
		try{
			if (_Incontf==null){
				_Incontf=getIntegerProperty("INCONTF");
				return _Incontf;
			}else {
				return _Incontf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INCONTF.
	 * @param v Value to Set.
	 */
	public void setIncontf(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INCONTF",v);
		_Incontf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Dep2yrs=null;

	/**
	 * @return Returns the DEP2YRS.
	 */
	public Integer getDep2yrs() {
		try{
			if (_Dep2yrs==null){
				_Dep2yrs=getIntegerProperty("DEP2YRS");
				return _Dep2yrs;
			}else {
				return _Dep2yrs;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DEP2YRS.
	 * @param v Value to Set.
	 */
	public void setDep2yrs(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DEP2YRS",v);
		_Dep2yrs=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Depothr=null;

	/**
	 * @return Returns the DEPOTHR.
	 */
	public Integer getDepothr() {
		try{
			if (_Depothr==null){
				_Depothr=getIntegerProperty("DEPOTHR");
				return _Depothr;
			}else {
				return _Depothr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DEPOTHR.
	 * @param v Value to Set.
	 */
	public void setDepothr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DEPOTHR",v);
		_Depothr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Alcohol=null;

	/**
	 * @return Returns the ALCOHOL.
	 */
	public Integer getAlcohol() {
		try{
			if (_Alcohol==null){
				_Alcohol=getIntegerProperty("ALCOHOL");
				return _Alcohol;
			}else {
				return _Alcohol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ALCOHOL.
	 * @param v Value to Set.
	 */
	public void setAlcohol(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ALCOHOL",v);
		_Alcohol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tobac30=null;

	/**
	 * @return Returns the TOBAC30.
	 */
	public Integer getTobac30() {
		try{
			if (_Tobac30==null){
				_Tobac30=getIntegerProperty("TOBAC30");
				return _Tobac30;
			}else {
				return _Tobac30;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TOBAC30.
	 * @param v Value to Set.
	 */
	public void setTobac30(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TOBAC30",v);
		_Tobac30=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tobac100=null;

	/**
	 * @return Returns the TOBAC100.
	 */
	public Integer getTobac100() {
		try{
			if (_Tobac100==null){
				_Tobac100=getIntegerProperty("TOBAC100");
				return _Tobac100;
			}else {
				return _Tobac100;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TOBAC100.
	 * @param v Value to Set.
	 */
	public void setTobac100(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TOBAC100",v);
		_Tobac100=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Smokyrs=null;

	/**
	 * @return Returns the SMOKYRS.
	 */
	public Integer getSmokyrs() {
		try{
			if (_Smokyrs==null){
				_Smokyrs=getIntegerProperty("SMOKYRS");
				return _Smokyrs;
			}else {
				return _Smokyrs;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SMOKYRS.
	 * @param v Value to Set.
	 */
	public void setSmokyrs(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SMOKYRS",v);
		_Smokyrs=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Packsper=null;

	/**
	 * @return Returns the PACKSPER.
	 */
	public Integer getPacksper() {
		try{
			if (_Packsper==null){
				_Packsper=getIntegerProperty("PACKSPER");
				return _Packsper;
			}else {
				return _Packsper;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PACKSPER.
	 * @param v Value to Set.
	 */
	public void setPacksper(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PACKSPER",v);
		_Packsper=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Quitsmok=null;

	/**
	 * @return Returns the QUITSMOK.
	 */
	public Integer getQuitsmok() {
		try{
			if (_Quitsmok==null){
				_Quitsmok=getIntegerProperty("QUITSMOK");
				return _Quitsmok;
			}else {
				return _Quitsmok;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for QUITSMOK.
	 * @param v Value to Set.
	 */
	public void setQuitsmok(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/QUITSMOK",v);
		_Quitsmok=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Abusothr=null;

	/**
	 * @return Returns the ABUSOTHR.
	 */
	public Integer getAbusothr() {
		try{
			if (_Abusothr==null){
				_Abusothr=getIntegerProperty("ABUSOTHR");
				return _Abusothr;
			}else {
				return _Abusothr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ABUSOTHR.
	 * @param v Value to Set.
	 */
	public void setAbusothr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ABUSOTHR",v);
		_Abusothr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Abusx=null;

	/**
	 * @return Returns the ABUSX.
	 */
	public String getAbusx(){
		try{
			if (_Abusx==null){
				_Abusx=getStringProperty("ABUSX");
				return _Abusx;
			}else {
				return _Abusx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ABUSX.
	 * @param v Value to Set.
	 */
	public void setAbusx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ABUSX",v);
		_Abusx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Psycdis=null;

	/**
	 * @return Returns the PSYCDIS.
	 */
	public Integer getPsycdis() {
		try{
			if (_Psycdis==null){
				_Psycdis=getIntegerProperty("PSYCDIS");
				return _Psycdis;
			}else {
				return _Psycdis;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PSYCDIS.
	 * @param v Value to Set.
	 */
	public void setPsycdis(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PSYCDIS",v);
		_Psycdis=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Psycdisx=null;

	/**
	 * @return Returns the PSYCDISX.
	 */
	public String getPsycdisx(){
		try{
			if (_Psycdisx==null){
				_Psycdisx=getStringProperty("PSYCDISX");
				return _Psycdisx;
			}else {
				return _Psycdisx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PSYCDISX.
	 * @param v Value to Set.
	 */
	public void setPsycdisx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PSYCDISX",v);
		_Psycdisx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.UdsA5subhstdata> getAllUdsA5subhstdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsA5subhstdata> al = new ArrayList<org.nrg.xdat.om.UdsA5subhstdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsA5subhstdata> getUdsA5subhstdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsA5subhstdata> al = new ArrayList<org.nrg.xdat.om.UdsA5subhstdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsA5subhstdata> getUdsA5subhstdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsA5subhstdata> al = new ArrayList<org.nrg.xdat.om.UdsA5subhstdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static UdsA5subhstdata getUdsA5subhstdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("uds:a5subhstData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (UdsA5subhstdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
