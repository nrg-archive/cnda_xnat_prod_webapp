/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:01 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoOptiOisprocdataFramesHb extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.OptiOisprocdataFramesHbI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoOptiOisprocdataFramesHb.class);
	public static String SCHEMA_ELEMENT_NAME="opti:oisProcData_frames_hb";

	public AutoOptiOisprocdataFramesHb(ItemI item)
	{
		super(item);
	}

	public AutoOptiOisprocdataFramesHb(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoOptiOisprocdataFramesHb(UserI user)
	 **/
	public AutoOptiOisprocdataFramesHb(){}

	public AutoOptiOisprocdataFramesHb(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "opti:oisProcData_frames_hb";
	}

	//FIELD

	private Integer _FramesHb=null;

	/**
	 * @return Returns the frames_hb.
	 */
	public Integer getFramesHb() {
		try{
			if (_FramesHb==null){
				_FramesHb=getIntegerProperty("frames_hb");
				return _FramesHb;
			}else {
				return _FramesHb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for frames_hb.
	 * @param v Value to Set.
	 */
	public void setFramesHb(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/frames_hb",v);
		_FramesHb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scanid=null;

	/**
	 * @return Returns the scanid.
	 */
	public String getScanid(){
		try{
			if (_Scanid==null){
				_Scanid=getStringProperty("scanid");
				return _Scanid;
			}else {
				return _Scanid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for scanid.
	 * @param v Value to Set.
	 */
	public void setScanid(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/scanid",v);
		_Scanid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _OptiOisprocdataFramesHbId=null;

	/**
	 * @return Returns the opti_oisProcData_frames_hb_id.
	 */
	public Integer getOptiOisprocdataFramesHbId() {
		try{
			if (_OptiOisprocdataFramesHbId==null){
				_OptiOisprocdataFramesHbId=getIntegerProperty("opti_oisProcData_frames_hb_id");
				return _OptiOisprocdataFramesHbId;
			}else {
				return _OptiOisprocdataFramesHbId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for opti_oisProcData_frames_hb_id.
	 * @param v Value to Set.
	 */
	public void setOptiOisprocdataFramesHbId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/opti_oisProcData_frames_hb_id",v);
		_OptiOisprocdataFramesHbId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.OptiOisprocdataFramesHb> getAllOptiOisprocdataFramesHbs(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.OptiOisprocdataFramesHb> al = new ArrayList<org.nrg.xdat.om.OptiOisprocdataFramesHb>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.OptiOisprocdataFramesHb> getOptiOisprocdataFramesHbsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.OptiOisprocdataFramesHb> al = new ArrayList<org.nrg.xdat.om.OptiOisprocdataFramesHb>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.OptiOisprocdataFramesHb> getOptiOisprocdataFramesHbsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.OptiOisprocdataFramesHb> al = new ArrayList<org.nrg.xdat.om.OptiOisprocdataFramesHb>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static OptiOisprocdataFramesHb getOptiOisprocdataFramesHbsByOptiOisprocdataFramesHbId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("opti:oisProcData_frames_hb/opti_oisProcData_frames_hb_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (OptiOisprocdataFramesHb) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
