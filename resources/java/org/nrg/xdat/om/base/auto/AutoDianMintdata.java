/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:01 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianMintdata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianMintdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianMintdata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:mintData";

	public AutoDianMintdata(ItemI item)
	{
		super(item);
	}

	public AutoDianMintdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianMintdata(UserI user)
	 **/
	public AutoDianMintdata(){}

	public AutoDianMintdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:mintData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Done=null;

	/**
	 * @return Returns the DONE.
	 */
	public Integer getDone() {
		try{
			if (_Done==null){
				_Done=getIntegerProperty("DONE");
				return _Done;
			}else {
				return _Done;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DONE.
	 * @param v Value to Set.
	 */
	public void setDone(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DONE",v);
		_Done=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ndreason=null;

	/**
	 * @return Returns the NDREASON.
	 */
	public Integer getNdreason() {
		try{
			if (_Ndreason==null){
				_Ndreason=getIntegerProperty("NDREASON");
				return _Ndreason;
			}else {
				return _Ndreason;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NDREASON.
	 * @param v Value to Set.
	 */
	public void setNdreason(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NDREASON",v);
		_Ndreason=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq1uncue=null;

	/**
	 * @return Returns the MTQ1UNCUE.
	 */
	public Integer getMtq1uncue() {
		try{
			if (_Mtq1uncue==null){
				_Mtq1uncue=getIntegerProperty("MTQ1UNCUE");
				return _Mtq1uncue;
			}else {
				return _Mtq1uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ1UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq1uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ1UNCUE",v);
		_Mtq1uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq1msip=null;

	/**
	 * @return Returns the MTQ1MSIP.
	 */
	public Integer getMtq1msip() {
		try{
			if (_Mtq1msip==null){
				_Mtq1msip=getIntegerProperty("MTQ1MSIP");
				return _Mtq1msip;
			}else {
				return _Mtq1msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ1MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq1msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ1MSIP",v);
		_Mtq1msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq1sem=null;

	/**
	 * @return Returns the MTQ1SEM.
	 */
	public Integer getMtq1sem() {
		try{
			if (_Mtq1sem==null){
				_Mtq1sem=getIntegerProperty("MTQ1SEM");
				return _Mtq1sem;
			}else {
				return _Mtq1sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ1SEM.
	 * @param v Value to Set.
	 */
	public void setMtq1sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ1SEM",v);
		_Mtq1sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq1phon=null;

	/**
	 * @return Returns the MTQ1PHON.
	 */
	public Integer getMtq1phon() {
		try{
			if (_Mtq1phon==null){
				_Mtq1phon=getIntegerProperty("MTQ1PHON");
				return _Mtq1phon;
			}else {
				return _Mtq1phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ1PHON.
	 * @param v Value to Set.
	 */
	public void setMtq1phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ1PHON",v);
		_Mtq1phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq2uncue=null;

	/**
	 * @return Returns the MTQ2UNCUE.
	 */
	public Integer getMtq2uncue() {
		try{
			if (_Mtq2uncue==null){
				_Mtq2uncue=getIntegerProperty("MTQ2UNCUE");
				return _Mtq2uncue;
			}else {
				return _Mtq2uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ2UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq2uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ2UNCUE",v);
		_Mtq2uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq2msip=null;

	/**
	 * @return Returns the MTQ2MSIP.
	 */
	public Integer getMtq2msip() {
		try{
			if (_Mtq2msip==null){
				_Mtq2msip=getIntegerProperty("MTQ2MSIP");
				return _Mtq2msip;
			}else {
				return _Mtq2msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ2MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq2msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ2MSIP",v);
		_Mtq2msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq2sem=null;

	/**
	 * @return Returns the MTQ2SEM.
	 */
	public Integer getMtq2sem() {
		try{
			if (_Mtq2sem==null){
				_Mtq2sem=getIntegerProperty("MTQ2SEM");
				return _Mtq2sem;
			}else {
				return _Mtq2sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ2SEM.
	 * @param v Value to Set.
	 */
	public void setMtq2sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ2SEM",v);
		_Mtq2sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq2phon=null;

	/**
	 * @return Returns the MTQ2PHON.
	 */
	public Integer getMtq2phon() {
		try{
			if (_Mtq2phon==null){
				_Mtq2phon=getIntegerProperty("MTQ2PHON");
				return _Mtq2phon;
			}else {
				return _Mtq2phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ2PHON.
	 * @param v Value to Set.
	 */
	public void setMtq2phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ2PHON",v);
		_Mtq2phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq3uncue=null;

	/**
	 * @return Returns the MTQ3UNCUE.
	 */
	public Integer getMtq3uncue() {
		try{
			if (_Mtq3uncue==null){
				_Mtq3uncue=getIntegerProperty("MTQ3UNCUE");
				return _Mtq3uncue;
			}else {
				return _Mtq3uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ3UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq3uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ3UNCUE",v);
		_Mtq3uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq3msip=null;

	/**
	 * @return Returns the MTQ3MSIP.
	 */
	public Integer getMtq3msip() {
		try{
			if (_Mtq3msip==null){
				_Mtq3msip=getIntegerProperty("MTQ3MSIP");
				return _Mtq3msip;
			}else {
				return _Mtq3msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ3MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq3msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ3MSIP",v);
		_Mtq3msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq3sem=null;

	/**
	 * @return Returns the MTQ3SEM.
	 */
	public Integer getMtq3sem() {
		try{
			if (_Mtq3sem==null){
				_Mtq3sem=getIntegerProperty("MTQ3SEM");
				return _Mtq3sem;
			}else {
				return _Mtq3sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ3SEM.
	 * @param v Value to Set.
	 */
	public void setMtq3sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ3SEM",v);
		_Mtq3sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq3phon=null;

	/**
	 * @return Returns the MTQ3PHON.
	 */
	public Integer getMtq3phon() {
		try{
			if (_Mtq3phon==null){
				_Mtq3phon=getIntegerProperty("MTQ3PHON");
				return _Mtq3phon;
			}else {
				return _Mtq3phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ3PHON.
	 * @param v Value to Set.
	 */
	public void setMtq3phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ3PHON",v);
		_Mtq3phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq4uncue=null;

	/**
	 * @return Returns the MTQ4UNCUE.
	 */
	public Integer getMtq4uncue() {
		try{
			if (_Mtq4uncue==null){
				_Mtq4uncue=getIntegerProperty("MTQ4UNCUE");
				return _Mtq4uncue;
			}else {
				return _Mtq4uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ4UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq4uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ4UNCUE",v);
		_Mtq4uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq4msip=null;

	/**
	 * @return Returns the MTQ4MSIP.
	 */
	public Integer getMtq4msip() {
		try{
			if (_Mtq4msip==null){
				_Mtq4msip=getIntegerProperty("MTQ4MSIP");
				return _Mtq4msip;
			}else {
				return _Mtq4msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ4MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq4msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ4MSIP",v);
		_Mtq4msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq4sem=null;

	/**
	 * @return Returns the MTQ4SEM.
	 */
	public Integer getMtq4sem() {
		try{
			if (_Mtq4sem==null){
				_Mtq4sem=getIntegerProperty("MTQ4SEM");
				return _Mtq4sem;
			}else {
				return _Mtq4sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ4SEM.
	 * @param v Value to Set.
	 */
	public void setMtq4sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ4SEM",v);
		_Mtq4sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq4phon=null;

	/**
	 * @return Returns the MTQ4PHON.
	 */
	public Integer getMtq4phon() {
		try{
			if (_Mtq4phon==null){
				_Mtq4phon=getIntegerProperty("MTQ4PHON");
				return _Mtq4phon;
			}else {
				return _Mtq4phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ4PHON.
	 * @param v Value to Set.
	 */
	public void setMtq4phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ4PHON",v);
		_Mtq4phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq5uncue=null;

	/**
	 * @return Returns the MTQ5UNCUE.
	 */
	public Integer getMtq5uncue() {
		try{
			if (_Mtq5uncue==null){
				_Mtq5uncue=getIntegerProperty("MTQ5UNCUE");
				return _Mtq5uncue;
			}else {
				return _Mtq5uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ5UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq5uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ5UNCUE",v);
		_Mtq5uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq5msip=null;

	/**
	 * @return Returns the MTQ5MSIP.
	 */
	public Integer getMtq5msip() {
		try{
			if (_Mtq5msip==null){
				_Mtq5msip=getIntegerProperty("MTQ5MSIP");
				return _Mtq5msip;
			}else {
				return _Mtq5msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ5MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq5msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ5MSIP",v);
		_Mtq5msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq5sem=null;

	/**
	 * @return Returns the MTQ5SEM.
	 */
	public Integer getMtq5sem() {
		try{
			if (_Mtq5sem==null){
				_Mtq5sem=getIntegerProperty("MTQ5SEM");
				return _Mtq5sem;
			}else {
				return _Mtq5sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ5SEM.
	 * @param v Value to Set.
	 */
	public void setMtq5sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ5SEM",v);
		_Mtq5sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq5phon=null;

	/**
	 * @return Returns the MTQ5PHON.
	 */
	public Integer getMtq5phon() {
		try{
			if (_Mtq5phon==null){
				_Mtq5phon=getIntegerProperty("MTQ5PHON");
				return _Mtq5phon;
			}else {
				return _Mtq5phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ5PHON.
	 * @param v Value to Set.
	 */
	public void setMtq5phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ5PHON",v);
		_Mtq5phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq6uncue=null;

	/**
	 * @return Returns the MTQ6UNCUE.
	 */
	public Integer getMtq6uncue() {
		try{
			if (_Mtq6uncue==null){
				_Mtq6uncue=getIntegerProperty("MTQ6UNCUE");
				return _Mtq6uncue;
			}else {
				return _Mtq6uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ6UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq6uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ6UNCUE",v);
		_Mtq6uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq6msip=null;

	/**
	 * @return Returns the MTQ6MSIP.
	 */
	public Integer getMtq6msip() {
		try{
			if (_Mtq6msip==null){
				_Mtq6msip=getIntegerProperty("MTQ6MSIP");
				return _Mtq6msip;
			}else {
				return _Mtq6msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ6MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq6msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ6MSIP",v);
		_Mtq6msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq6sem=null;

	/**
	 * @return Returns the MTQ6SEM.
	 */
	public Integer getMtq6sem() {
		try{
			if (_Mtq6sem==null){
				_Mtq6sem=getIntegerProperty("MTQ6SEM");
				return _Mtq6sem;
			}else {
				return _Mtq6sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ6SEM.
	 * @param v Value to Set.
	 */
	public void setMtq6sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ6SEM",v);
		_Mtq6sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq6phon=null;

	/**
	 * @return Returns the MTQ6PHON.
	 */
	public Integer getMtq6phon() {
		try{
			if (_Mtq6phon==null){
				_Mtq6phon=getIntegerProperty("MTQ6PHON");
				return _Mtq6phon;
			}else {
				return _Mtq6phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ6PHON.
	 * @param v Value to Set.
	 */
	public void setMtq6phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ6PHON",v);
		_Mtq6phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq7uncue=null;

	/**
	 * @return Returns the MTQ7UNCUE.
	 */
	public Integer getMtq7uncue() {
		try{
			if (_Mtq7uncue==null){
				_Mtq7uncue=getIntegerProperty("MTQ7UNCUE");
				return _Mtq7uncue;
			}else {
				return _Mtq7uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ7UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq7uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ7UNCUE",v);
		_Mtq7uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq7msip=null;

	/**
	 * @return Returns the MTQ7MSIP.
	 */
	public Integer getMtq7msip() {
		try{
			if (_Mtq7msip==null){
				_Mtq7msip=getIntegerProperty("MTQ7MSIP");
				return _Mtq7msip;
			}else {
				return _Mtq7msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ7MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq7msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ7MSIP",v);
		_Mtq7msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq7sem=null;

	/**
	 * @return Returns the MTQ7SEM.
	 */
	public Integer getMtq7sem() {
		try{
			if (_Mtq7sem==null){
				_Mtq7sem=getIntegerProperty("MTQ7SEM");
				return _Mtq7sem;
			}else {
				return _Mtq7sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ7SEM.
	 * @param v Value to Set.
	 */
	public void setMtq7sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ7SEM",v);
		_Mtq7sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq7phon=null;

	/**
	 * @return Returns the MTQ7PHON.
	 */
	public Integer getMtq7phon() {
		try{
			if (_Mtq7phon==null){
				_Mtq7phon=getIntegerProperty("MTQ7PHON");
				return _Mtq7phon;
			}else {
				return _Mtq7phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ7PHON.
	 * @param v Value to Set.
	 */
	public void setMtq7phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ7PHON",v);
		_Mtq7phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq8uncue=null;

	/**
	 * @return Returns the MTQ8UNCUE.
	 */
	public Integer getMtq8uncue() {
		try{
			if (_Mtq8uncue==null){
				_Mtq8uncue=getIntegerProperty("MTQ8UNCUE");
				return _Mtq8uncue;
			}else {
				return _Mtq8uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ8UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq8uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ8UNCUE",v);
		_Mtq8uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq8msip=null;

	/**
	 * @return Returns the MTQ8MSIP.
	 */
	public Integer getMtq8msip() {
		try{
			if (_Mtq8msip==null){
				_Mtq8msip=getIntegerProperty("MTQ8MSIP");
				return _Mtq8msip;
			}else {
				return _Mtq8msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ8MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq8msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ8MSIP",v);
		_Mtq8msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq8sem=null;

	/**
	 * @return Returns the MTQ8SEM.
	 */
	public Integer getMtq8sem() {
		try{
			if (_Mtq8sem==null){
				_Mtq8sem=getIntegerProperty("MTQ8SEM");
				return _Mtq8sem;
			}else {
				return _Mtq8sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ8SEM.
	 * @param v Value to Set.
	 */
	public void setMtq8sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ8SEM",v);
		_Mtq8sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq8phon=null;

	/**
	 * @return Returns the MTQ8PHON.
	 */
	public Integer getMtq8phon() {
		try{
			if (_Mtq8phon==null){
				_Mtq8phon=getIntegerProperty("MTQ8PHON");
				return _Mtq8phon;
			}else {
				return _Mtq8phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ8PHON.
	 * @param v Value to Set.
	 */
	public void setMtq8phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ8PHON",v);
		_Mtq8phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq9uncue=null;

	/**
	 * @return Returns the MTQ9UNCUE.
	 */
	public Integer getMtq9uncue() {
		try{
			if (_Mtq9uncue==null){
				_Mtq9uncue=getIntegerProperty("MTQ9UNCUE");
				return _Mtq9uncue;
			}else {
				return _Mtq9uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ9UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq9uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ9UNCUE",v);
		_Mtq9uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq9msip=null;

	/**
	 * @return Returns the MTQ9MSIP.
	 */
	public Integer getMtq9msip() {
		try{
			if (_Mtq9msip==null){
				_Mtq9msip=getIntegerProperty("MTQ9MSIP");
				return _Mtq9msip;
			}else {
				return _Mtq9msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ9MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq9msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ9MSIP",v);
		_Mtq9msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq9sem=null;

	/**
	 * @return Returns the MTQ9SEM.
	 */
	public Integer getMtq9sem() {
		try{
			if (_Mtq9sem==null){
				_Mtq9sem=getIntegerProperty("MTQ9SEM");
				return _Mtq9sem;
			}else {
				return _Mtq9sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ9SEM.
	 * @param v Value to Set.
	 */
	public void setMtq9sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ9SEM",v);
		_Mtq9sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq9phon=null;

	/**
	 * @return Returns the MTQ9PHON.
	 */
	public Integer getMtq9phon() {
		try{
			if (_Mtq9phon==null){
				_Mtq9phon=getIntegerProperty("MTQ9PHON");
				return _Mtq9phon;
			}else {
				return _Mtq9phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ9PHON.
	 * @param v Value to Set.
	 */
	public void setMtq9phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ9PHON",v);
		_Mtq9phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq10uncue=null;

	/**
	 * @return Returns the MTQ10UNCUE.
	 */
	public Integer getMtq10uncue() {
		try{
			if (_Mtq10uncue==null){
				_Mtq10uncue=getIntegerProperty("MTQ10UNCUE");
				return _Mtq10uncue;
			}else {
				return _Mtq10uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ10UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq10uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ10UNCUE",v);
		_Mtq10uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq10msip=null;

	/**
	 * @return Returns the MTQ10MSIP.
	 */
	public Integer getMtq10msip() {
		try{
			if (_Mtq10msip==null){
				_Mtq10msip=getIntegerProperty("MTQ10MSIP");
				return _Mtq10msip;
			}else {
				return _Mtq10msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ10MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq10msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ10MSIP",v);
		_Mtq10msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq10sem=null;

	/**
	 * @return Returns the MTQ10SEM.
	 */
	public Integer getMtq10sem() {
		try{
			if (_Mtq10sem==null){
				_Mtq10sem=getIntegerProperty("MTQ10SEM");
				return _Mtq10sem;
			}else {
				return _Mtq10sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ10SEM.
	 * @param v Value to Set.
	 */
	public void setMtq10sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ10SEM",v);
		_Mtq10sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq10phon=null;

	/**
	 * @return Returns the MTQ10PHON.
	 */
	public Integer getMtq10phon() {
		try{
			if (_Mtq10phon==null){
				_Mtq10phon=getIntegerProperty("MTQ10PHON");
				return _Mtq10phon;
			}else {
				return _Mtq10phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ10PHON.
	 * @param v Value to Set.
	 */
	public void setMtq10phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ10PHON",v);
		_Mtq10phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq11uncue=null;

	/**
	 * @return Returns the MTQ11UNCUE.
	 */
	public Integer getMtq11uncue() {
		try{
			if (_Mtq11uncue==null){
				_Mtq11uncue=getIntegerProperty("MTQ11UNCUE");
				return _Mtq11uncue;
			}else {
				return _Mtq11uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ11UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq11uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ11UNCUE",v);
		_Mtq11uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq11msip=null;

	/**
	 * @return Returns the MTQ11MSIP.
	 */
	public Integer getMtq11msip() {
		try{
			if (_Mtq11msip==null){
				_Mtq11msip=getIntegerProperty("MTQ11MSIP");
				return _Mtq11msip;
			}else {
				return _Mtq11msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ11MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq11msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ11MSIP",v);
		_Mtq11msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq11sem=null;

	/**
	 * @return Returns the MTQ11SEM.
	 */
	public Integer getMtq11sem() {
		try{
			if (_Mtq11sem==null){
				_Mtq11sem=getIntegerProperty("MTQ11SEM");
				return _Mtq11sem;
			}else {
				return _Mtq11sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ11SEM.
	 * @param v Value to Set.
	 */
	public void setMtq11sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ11SEM",v);
		_Mtq11sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq11phon=null;

	/**
	 * @return Returns the MTQ11PHON.
	 */
	public Integer getMtq11phon() {
		try{
			if (_Mtq11phon==null){
				_Mtq11phon=getIntegerProperty("MTQ11PHON");
				return _Mtq11phon;
			}else {
				return _Mtq11phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ11PHON.
	 * @param v Value to Set.
	 */
	public void setMtq11phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ11PHON",v);
		_Mtq11phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq12uncue=null;

	/**
	 * @return Returns the MTQ12UNCUE.
	 */
	public Integer getMtq12uncue() {
		try{
			if (_Mtq12uncue==null){
				_Mtq12uncue=getIntegerProperty("MTQ12UNCUE");
				return _Mtq12uncue;
			}else {
				return _Mtq12uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ12UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq12uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ12UNCUE",v);
		_Mtq12uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq12msip=null;

	/**
	 * @return Returns the MTQ12MSIP.
	 */
	public Integer getMtq12msip() {
		try{
			if (_Mtq12msip==null){
				_Mtq12msip=getIntegerProperty("MTQ12MSIP");
				return _Mtq12msip;
			}else {
				return _Mtq12msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ12MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq12msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ12MSIP",v);
		_Mtq12msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq12sem=null;

	/**
	 * @return Returns the MTQ12SEM.
	 */
	public Integer getMtq12sem() {
		try{
			if (_Mtq12sem==null){
				_Mtq12sem=getIntegerProperty("MTQ12SEM");
				return _Mtq12sem;
			}else {
				return _Mtq12sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ12SEM.
	 * @param v Value to Set.
	 */
	public void setMtq12sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ12SEM",v);
		_Mtq12sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq12phon=null;

	/**
	 * @return Returns the MTQ12PHON.
	 */
	public Integer getMtq12phon() {
		try{
			if (_Mtq12phon==null){
				_Mtq12phon=getIntegerProperty("MTQ12PHON");
				return _Mtq12phon;
			}else {
				return _Mtq12phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ12PHON.
	 * @param v Value to Set.
	 */
	public void setMtq12phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ12PHON",v);
		_Mtq12phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq13uncue=null;

	/**
	 * @return Returns the MTQ13UNCUE.
	 */
	public Integer getMtq13uncue() {
		try{
			if (_Mtq13uncue==null){
				_Mtq13uncue=getIntegerProperty("MTQ13UNCUE");
				return _Mtq13uncue;
			}else {
				return _Mtq13uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ13UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq13uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ13UNCUE",v);
		_Mtq13uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq13msip=null;

	/**
	 * @return Returns the MTQ13MSIP.
	 */
	public Integer getMtq13msip() {
		try{
			if (_Mtq13msip==null){
				_Mtq13msip=getIntegerProperty("MTQ13MSIP");
				return _Mtq13msip;
			}else {
				return _Mtq13msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ13MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq13msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ13MSIP",v);
		_Mtq13msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq13sem=null;

	/**
	 * @return Returns the MTQ13SEM.
	 */
	public Integer getMtq13sem() {
		try{
			if (_Mtq13sem==null){
				_Mtq13sem=getIntegerProperty("MTQ13SEM");
				return _Mtq13sem;
			}else {
				return _Mtq13sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ13SEM.
	 * @param v Value to Set.
	 */
	public void setMtq13sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ13SEM",v);
		_Mtq13sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq13phon=null;

	/**
	 * @return Returns the MTQ13PHON.
	 */
	public Integer getMtq13phon() {
		try{
			if (_Mtq13phon==null){
				_Mtq13phon=getIntegerProperty("MTQ13PHON");
				return _Mtq13phon;
			}else {
				return _Mtq13phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ13PHON.
	 * @param v Value to Set.
	 */
	public void setMtq13phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ13PHON",v);
		_Mtq13phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq14uncue=null;

	/**
	 * @return Returns the MTQ14UNCUE.
	 */
	public Integer getMtq14uncue() {
		try{
			if (_Mtq14uncue==null){
				_Mtq14uncue=getIntegerProperty("MTQ14UNCUE");
				return _Mtq14uncue;
			}else {
				return _Mtq14uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ14UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq14uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ14UNCUE",v);
		_Mtq14uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq14msip=null;

	/**
	 * @return Returns the MTQ14MSIP.
	 */
	public Integer getMtq14msip() {
		try{
			if (_Mtq14msip==null){
				_Mtq14msip=getIntegerProperty("MTQ14MSIP");
				return _Mtq14msip;
			}else {
				return _Mtq14msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ14MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq14msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ14MSIP",v);
		_Mtq14msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq14sem=null;

	/**
	 * @return Returns the MTQ14SEM.
	 */
	public Integer getMtq14sem() {
		try{
			if (_Mtq14sem==null){
				_Mtq14sem=getIntegerProperty("MTQ14SEM");
				return _Mtq14sem;
			}else {
				return _Mtq14sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ14SEM.
	 * @param v Value to Set.
	 */
	public void setMtq14sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ14SEM",v);
		_Mtq14sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq14phon=null;

	/**
	 * @return Returns the MTQ14PHON.
	 */
	public Integer getMtq14phon() {
		try{
			if (_Mtq14phon==null){
				_Mtq14phon=getIntegerProperty("MTQ14PHON");
				return _Mtq14phon;
			}else {
				return _Mtq14phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ14PHON.
	 * @param v Value to Set.
	 */
	public void setMtq14phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ14PHON",v);
		_Mtq14phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq15uncue=null;

	/**
	 * @return Returns the MTQ15UNCUE.
	 */
	public Integer getMtq15uncue() {
		try{
			if (_Mtq15uncue==null){
				_Mtq15uncue=getIntegerProperty("MTQ15UNCUE");
				return _Mtq15uncue;
			}else {
				return _Mtq15uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ15UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq15uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ15UNCUE",v);
		_Mtq15uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq15msip=null;

	/**
	 * @return Returns the MTQ15MSIP.
	 */
	public Integer getMtq15msip() {
		try{
			if (_Mtq15msip==null){
				_Mtq15msip=getIntegerProperty("MTQ15MSIP");
				return _Mtq15msip;
			}else {
				return _Mtq15msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ15MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq15msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ15MSIP",v);
		_Mtq15msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq15sem=null;

	/**
	 * @return Returns the MTQ15SEM.
	 */
	public Integer getMtq15sem() {
		try{
			if (_Mtq15sem==null){
				_Mtq15sem=getIntegerProperty("MTQ15SEM");
				return _Mtq15sem;
			}else {
				return _Mtq15sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ15SEM.
	 * @param v Value to Set.
	 */
	public void setMtq15sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ15SEM",v);
		_Mtq15sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq15phon=null;

	/**
	 * @return Returns the MTQ15PHON.
	 */
	public Integer getMtq15phon() {
		try{
			if (_Mtq15phon==null){
				_Mtq15phon=getIntegerProperty("MTQ15PHON");
				return _Mtq15phon;
			}else {
				return _Mtq15phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ15PHON.
	 * @param v Value to Set.
	 */
	public void setMtq15phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ15PHON",v);
		_Mtq15phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq16uncue=null;

	/**
	 * @return Returns the MTQ16UNCUE.
	 */
	public Integer getMtq16uncue() {
		try{
			if (_Mtq16uncue==null){
				_Mtq16uncue=getIntegerProperty("MTQ16UNCUE");
				return _Mtq16uncue;
			}else {
				return _Mtq16uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ16UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq16uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ16UNCUE",v);
		_Mtq16uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq16msip=null;

	/**
	 * @return Returns the MTQ16MSIP.
	 */
	public Integer getMtq16msip() {
		try{
			if (_Mtq16msip==null){
				_Mtq16msip=getIntegerProperty("MTQ16MSIP");
				return _Mtq16msip;
			}else {
				return _Mtq16msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ16MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq16msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ16MSIP",v);
		_Mtq16msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq16sem=null;

	/**
	 * @return Returns the MTQ16SEM.
	 */
	public Integer getMtq16sem() {
		try{
			if (_Mtq16sem==null){
				_Mtq16sem=getIntegerProperty("MTQ16SEM");
				return _Mtq16sem;
			}else {
				return _Mtq16sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ16SEM.
	 * @param v Value to Set.
	 */
	public void setMtq16sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ16SEM",v);
		_Mtq16sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq16phon=null;

	/**
	 * @return Returns the MTQ16PHON.
	 */
	public Integer getMtq16phon() {
		try{
			if (_Mtq16phon==null){
				_Mtq16phon=getIntegerProperty("MTQ16PHON");
				return _Mtq16phon;
			}else {
				return _Mtq16phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ16PHON.
	 * @param v Value to Set.
	 */
	public void setMtq16phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ16PHON",v);
		_Mtq16phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq17uncue=null;

	/**
	 * @return Returns the MTQ17UNCUE.
	 */
	public Integer getMtq17uncue() {
		try{
			if (_Mtq17uncue==null){
				_Mtq17uncue=getIntegerProperty("MTQ17UNCUE");
				return _Mtq17uncue;
			}else {
				return _Mtq17uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ17UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq17uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ17UNCUE",v);
		_Mtq17uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq17msip=null;

	/**
	 * @return Returns the MTQ17MSIP.
	 */
	public Integer getMtq17msip() {
		try{
			if (_Mtq17msip==null){
				_Mtq17msip=getIntegerProperty("MTQ17MSIP");
				return _Mtq17msip;
			}else {
				return _Mtq17msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ17MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq17msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ17MSIP",v);
		_Mtq17msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq17sem=null;

	/**
	 * @return Returns the MTQ17SEM.
	 */
	public Integer getMtq17sem() {
		try{
			if (_Mtq17sem==null){
				_Mtq17sem=getIntegerProperty("MTQ17SEM");
				return _Mtq17sem;
			}else {
				return _Mtq17sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ17SEM.
	 * @param v Value to Set.
	 */
	public void setMtq17sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ17SEM",v);
		_Mtq17sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq17phon=null;

	/**
	 * @return Returns the MTQ17PHON.
	 */
	public Integer getMtq17phon() {
		try{
			if (_Mtq17phon==null){
				_Mtq17phon=getIntegerProperty("MTQ17PHON");
				return _Mtq17phon;
			}else {
				return _Mtq17phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ17PHON.
	 * @param v Value to Set.
	 */
	public void setMtq17phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ17PHON",v);
		_Mtq17phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq18uncue=null;

	/**
	 * @return Returns the MTQ18UNCUE.
	 */
	public Integer getMtq18uncue() {
		try{
			if (_Mtq18uncue==null){
				_Mtq18uncue=getIntegerProperty("MTQ18UNCUE");
				return _Mtq18uncue;
			}else {
				return _Mtq18uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ18UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq18uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ18UNCUE",v);
		_Mtq18uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq18msip=null;

	/**
	 * @return Returns the MTQ18MSIP.
	 */
	public Integer getMtq18msip() {
		try{
			if (_Mtq18msip==null){
				_Mtq18msip=getIntegerProperty("MTQ18MSIP");
				return _Mtq18msip;
			}else {
				return _Mtq18msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ18MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq18msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ18MSIP",v);
		_Mtq18msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq18sem=null;

	/**
	 * @return Returns the MTQ18SEM.
	 */
	public Integer getMtq18sem() {
		try{
			if (_Mtq18sem==null){
				_Mtq18sem=getIntegerProperty("MTQ18SEM");
				return _Mtq18sem;
			}else {
				return _Mtq18sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ18SEM.
	 * @param v Value to Set.
	 */
	public void setMtq18sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ18SEM",v);
		_Mtq18sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq18phon=null;

	/**
	 * @return Returns the MTQ18PHON.
	 */
	public Integer getMtq18phon() {
		try{
			if (_Mtq18phon==null){
				_Mtq18phon=getIntegerProperty("MTQ18PHON");
				return _Mtq18phon;
			}else {
				return _Mtq18phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ18PHON.
	 * @param v Value to Set.
	 */
	public void setMtq18phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ18PHON",v);
		_Mtq18phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq19uncue=null;

	/**
	 * @return Returns the MTQ19UNCUE.
	 */
	public Integer getMtq19uncue() {
		try{
			if (_Mtq19uncue==null){
				_Mtq19uncue=getIntegerProperty("MTQ19UNCUE");
				return _Mtq19uncue;
			}else {
				return _Mtq19uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ19UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq19uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ19UNCUE",v);
		_Mtq19uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq19msip=null;

	/**
	 * @return Returns the MTQ19MSIP.
	 */
	public Integer getMtq19msip() {
		try{
			if (_Mtq19msip==null){
				_Mtq19msip=getIntegerProperty("MTQ19MSIP");
				return _Mtq19msip;
			}else {
				return _Mtq19msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ19MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq19msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ19MSIP",v);
		_Mtq19msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq19sem=null;

	/**
	 * @return Returns the MTQ19SEM.
	 */
	public Integer getMtq19sem() {
		try{
			if (_Mtq19sem==null){
				_Mtq19sem=getIntegerProperty("MTQ19SEM");
				return _Mtq19sem;
			}else {
				return _Mtq19sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ19SEM.
	 * @param v Value to Set.
	 */
	public void setMtq19sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ19SEM",v);
		_Mtq19sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq19phon=null;

	/**
	 * @return Returns the MTQ19PHON.
	 */
	public Integer getMtq19phon() {
		try{
			if (_Mtq19phon==null){
				_Mtq19phon=getIntegerProperty("MTQ19PHON");
				return _Mtq19phon;
			}else {
				return _Mtq19phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ19PHON.
	 * @param v Value to Set.
	 */
	public void setMtq19phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ19PHON",v);
		_Mtq19phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq20uncue=null;

	/**
	 * @return Returns the MTQ20UNCUE.
	 */
	public Integer getMtq20uncue() {
		try{
			if (_Mtq20uncue==null){
				_Mtq20uncue=getIntegerProperty("MTQ20UNCUE");
				return _Mtq20uncue;
			}else {
				return _Mtq20uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ20UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq20uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ20UNCUE",v);
		_Mtq20uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq20msip=null;

	/**
	 * @return Returns the MTQ20MSIP.
	 */
	public Integer getMtq20msip() {
		try{
			if (_Mtq20msip==null){
				_Mtq20msip=getIntegerProperty("MTQ20MSIP");
				return _Mtq20msip;
			}else {
				return _Mtq20msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ20MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq20msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ20MSIP",v);
		_Mtq20msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq20sem=null;

	/**
	 * @return Returns the MTQ20SEM.
	 */
	public Integer getMtq20sem() {
		try{
			if (_Mtq20sem==null){
				_Mtq20sem=getIntegerProperty("MTQ20SEM");
				return _Mtq20sem;
			}else {
				return _Mtq20sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ20SEM.
	 * @param v Value to Set.
	 */
	public void setMtq20sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ20SEM",v);
		_Mtq20sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq20phon=null;

	/**
	 * @return Returns the MTQ20PHON.
	 */
	public Integer getMtq20phon() {
		try{
			if (_Mtq20phon==null){
				_Mtq20phon=getIntegerProperty("MTQ20PHON");
				return _Mtq20phon;
			}else {
				return _Mtq20phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ20PHON.
	 * @param v Value to Set.
	 */
	public void setMtq20phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ20PHON",v);
		_Mtq20phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq21uncue=null;

	/**
	 * @return Returns the MTQ21UNCUE.
	 */
	public Integer getMtq21uncue() {
		try{
			if (_Mtq21uncue==null){
				_Mtq21uncue=getIntegerProperty("MTQ21UNCUE");
				return _Mtq21uncue;
			}else {
				return _Mtq21uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ21UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq21uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ21UNCUE",v);
		_Mtq21uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq21msip=null;

	/**
	 * @return Returns the MTQ21MSIP.
	 */
	public Integer getMtq21msip() {
		try{
			if (_Mtq21msip==null){
				_Mtq21msip=getIntegerProperty("MTQ21MSIP");
				return _Mtq21msip;
			}else {
				return _Mtq21msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ21MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq21msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ21MSIP",v);
		_Mtq21msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq21sem=null;

	/**
	 * @return Returns the MTQ21SEM.
	 */
	public Integer getMtq21sem() {
		try{
			if (_Mtq21sem==null){
				_Mtq21sem=getIntegerProperty("MTQ21SEM");
				return _Mtq21sem;
			}else {
				return _Mtq21sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ21SEM.
	 * @param v Value to Set.
	 */
	public void setMtq21sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ21SEM",v);
		_Mtq21sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq21phon=null;

	/**
	 * @return Returns the MTQ21PHON.
	 */
	public Integer getMtq21phon() {
		try{
			if (_Mtq21phon==null){
				_Mtq21phon=getIntegerProperty("MTQ21PHON");
				return _Mtq21phon;
			}else {
				return _Mtq21phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ21PHON.
	 * @param v Value to Set.
	 */
	public void setMtq21phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ21PHON",v);
		_Mtq21phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq22uncue=null;

	/**
	 * @return Returns the MTQ22UNCUE.
	 */
	public Integer getMtq22uncue() {
		try{
			if (_Mtq22uncue==null){
				_Mtq22uncue=getIntegerProperty("MTQ22UNCUE");
				return _Mtq22uncue;
			}else {
				return _Mtq22uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ22UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq22uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ22UNCUE",v);
		_Mtq22uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq22msip=null;

	/**
	 * @return Returns the MTQ22MSIP.
	 */
	public Integer getMtq22msip() {
		try{
			if (_Mtq22msip==null){
				_Mtq22msip=getIntegerProperty("MTQ22MSIP");
				return _Mtq22msip;
			}else {
				return _Mtq22msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ22MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq22msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ22MSIP",v);
		_Mtq22msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq22sem=null;

	/**
	 * @return Returns the MTQ22SEM.
	 */
	public Integer getMtq22sem() {
		try{
			if (_Mtq22sem==null){
				_Mtq22sem=getIntegerProperty("MTQ22SEM");
				return _Mtq22sem;
			}else {
				return _Mtq22sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ22SEM.
	 * @param v Value to Set.
	 */
	public void setMtq22sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ22SEM",v);
		_Mtq22sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq22phon=null;

	/**
	 * @return Returns the MTQ22PHON.
	 */
	public Integer getMtq22phon() {
		try{
			if (_Mtq22phon==null){
				_Mtq22phon=getIntegerProperty("MTQ22PHON");
				return _Mtq22phon;
			}else {
				return _Mtq22phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ22PHON.
	 * @param v Value to Set.
	 */
	public void setMtq22phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ22PHON",v);
		_Mtq22phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq23uncue=null;

	/**
	 * @return Returns the MTQ23UNCUE.
	 */
	public Integer getMtq23uncue() {
		try{
			if (_Mtq23uncue==null){
				_Mtq23uncue=getIntegerProperty("MTQ23UNCUE");
				return _Mtq23uncue;
			}else {
				return _Mtq23uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ23UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq23uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ23UNCUE",v);
		_Mtq23uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq23msip=null;

	/**
	 * @return Returns the MTQ23MSIP.
	 */
	public Integer getMtq23msip() {
		try{
			if (_Mtq23msip==null){
				_Mtq23msip=getIntegerProperty("MTQ23MSIP");
				return _Mtq23msip;
			}else {
				return _Mtq23msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ23MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq23msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ23MSIP",v);
		_Mtq23msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq23sem=null;

	/**
	 * @return Returns the MTQ23SEM.
	 */
	public Integer getMtq23sem() {
		try{
			if (_Mtq23sem==null){
				_Mtq23sem=getIntegerProperty("MTQ23SEM");
				return _Mtq23sem;
			}else {
				return _Mtq23sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ23SEM.
	 * @param v Value to Set.
	 */
	public void setMtq23sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ23SEM",v);
		_Mtq23sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq23phon=null;

	/**
	 * @return Returns the MTQ23PHON.
	 */
	public Integer getMtq23phon() {
		try{
			if (_Mtq23phon==null){
				_Mtq23phon=getIntegerProperty("MTQ23PHON");
				return _Mtq23phon;
			}else {
				return _Mtq23phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ23PHON.
	 * @param v Value to Set.
	 */
	public void setMtq23phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ23PHON",v);
		_Mtq23phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq24uncue=null;

	/**
	 * @return Returns the MTQ24UNCUE.
	 */
	public Integer getMtq24uncue() {
		try{
			if (_Mtq24uncue==null){
				_Mtq24uncue=getIntegerProperty("MTQ24UNCUE");
				return _Mtq24uncue;
			}else {
				return _Mtq24uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ24UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq24uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ24UNCUE",v);
		_Mtq24uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq24msip=null;

	/**
	 * @return Returns the MTQ24MSIP.
	 */
	public Integer getMtq24msip() {
		try{
			if (_Mtq24msip==null){
				_Mtq24msip=getIntegerProperty("MTQ24MSIP");
				return _Mtq24msip;
			}else {
				return _Mtq24msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ24MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq24msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ24MSIP",v);
		_Mtq24msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq24sem=null;

	/**
	 * @return Returns the MTQ24SEM.
	 */
	public Integer getMtq24sem() {
		try{
			if (_Mtq24sem==null){
				_Mtq24sem=getIntegerProperty("MTQ24SEM");
				return _Mtq24sem;
			}else {
				return _Mtq24sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ24SEM.
	 * @param v Value to Set.
	 */
	public void setMtq24sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ24SEM",v);
		_Mtq24sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq24phon=null;

	/**
	 * @return Returns the MTQ24PHON.
	 */
	public Integer getMtq24phon() {
		try{
			if (_Mtq24phon==null){
				_Mtq24phon=getIntegerProperty("MTQ24PHON");
				return _Mtq24phon;
			}else {
				return _Mtq24phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ24PHON.
	 * @param v Value to Set.
	 */
	public void setMtq24phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ24PHON",v);
		_Mtq24phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq25uncue=null;

	/**
	 * @return Returns the MTQ25UNCUE.
	 */
	public Integer getMtq25uncue() {
		try{
			if (_Mtq25uncue==null){
				_Mtq25uncue=getIntegerProperty("MTQ25UNCUE");
				return _Mtq25uncue;
			}else {
				return _Mtq25uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ25UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq25uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ25UNCUE",v);
		_Mtq25uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq25msip=null;

	/**
	 * @return Returns the MTQ25MSIP.
	 */
	public Integer getMtq25msip() {
		try{
			if (_Mtq25msip==null){
				_Mtq25msip=getIntegerProperty("MTQ25MSIP");
				return _Mtq25msip;
			}else {
				return _Mtq25msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ25MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq25msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ25MSIP",v);
		_Mtq25msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq25sem=null;

	/**
	 * @return Returns the MTQ25SEM.
	 */
	public Integer getMtq25sem() {
		try{
			if (_Mtq25sem==null){
				_Mtq25sem=getIntegerProperty("MTQ25SEM");
				return _Mtq25sem;
			}else {
				return _Mtq25sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ25SEM.
	 * @param v Value to Set.
	 */
	public void setMtq25sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ25SEM",v);
		_Mtq25sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq25phon=null;

	/**
	 * @return Returns the MTQ25PHON.
	 */
	public Integer getMtq25phon() {
		try{
			if (_Mtq25phon==null){
				_Mtq25phon=getIntegerProperty("MTQ25PHON");
				return _Mtq25phon;
			}else {
				return _Mtq25phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ25PHON.
	 * @param v Value to Set.
	 */
	public void setMtq25phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ25PHON",v);
		_Mtq25phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq26uncue=null;

	/**
	 * @return Returns the MTQ26UNCUE.
	 */
	public Integer getMtq26uncue() {
		try{
			if (_Mtq26uncue==null){
				_Mtq26uncue=getIntegerProperty("MTQ26UNCUE");
				return _Mtq26uncue;
			}else {
				return _Mtq26uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ26UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq26uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ26UNCUE",v);
		_Mtq26uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq26msip=null;

	/**
	 * @return Returns the MTQ26MSIP.
	 */
	public Integer getMtq26msip() {
		try{
			if (_Mtq26msip==null){
				_Mtq26msip=getIntegerProperty("MTQ26MSIP");
				return _Mtq26msip;
			}else {
				return _Mtq26msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ26MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq26msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ26MSIP",v);
		_Mtq26msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq26sem=null;

	/**
	 * @return Returns the MTQ26SEM.
	 */
	public Integer getMtq26sem() {
		try{
			if (_Mtq26sem==null){
				_Mtq26sem=getIntegerProperty("MTQ26SEM");
				return _Mtq26sem;
			}else {
				return _Mtq26sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ26SEM.
	 * @param v Value to Set.
	 */
	public void setMtq26sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ26SEM",v);
		_Mtq26sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq26phon=null;

	/**
	 * @return Returns the MTQ26PHON.
	 */
	public Integer getMtq26phon() {
		try{
			if (_Mtq26phon==null){
				_Mtq26phon=getIntegerProperty("MTQ26PHON");
				return _Mtq26phon;
			}else {
				return _Mtq26phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ26PHON.
	 * @param v Value to Set.
	 */
	public void setMtq26phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ26PHON",v);
		_Mtq26phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq27uncue=null;

	/**
	 * @return Returns the MTQ27UNCUE.
	 */
	public Integer getMtq27uncue() {
		try{
			if (_Mtq27uncue==null){
				_Mtq27uncue=getIntegerProperty("MTQ27UNCUE");
				return _Mtq27uncue;
			}else {
				return _Mtq27uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ27UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq27uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ27UNCUE",v);
		_Mtq27uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq27msip=null;

	/**
	 * @return Returns the MTQ27MSIP.
	 */
	public Integer getMtq27msip() {
		try{
			if (_Mtq27msip==null){
				_Mtq27msip=getIntegerProperty("MTQ27MSIP");
				return _Mtq27msip;
			}else {
				return _Mtq27msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ27MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq27msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ27MSIP",v);
		_Mtq27msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq27sem=null;

	/**
	 * @return Returns the MTQ27SEM.
	 */
	public Integer getMtq27sem() {
		try{
			if (_Mtq27sem==null){
				_Mtq27sem=getIntegerProperty("MTQ27SEM");
				return _Mtq27sem;
			}else {
				return _Mtq27sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ27SEM.
	 * @param v Value to Set.
	 */
	public void setMtq27sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ27SEM",v);
		_Mtq27sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq27phon=null;

	/**
	 * @return Returns the MTQ27PHON.
	 */
	public Integer getMtq27phon() {
		try{
			if (_Mtq27phon==null){
				_Mtq27phon=getIntegerProperty("MTQ27PHON");
				return _Mtq27phon;
			}else {
				return _Mtq27phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ27PHON.
	 * @param v Value to Set.
	 */
	public void setMtq27phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ27PHON",v);
		_Mtq27phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq28uncue=null;

	/**
	 * @return Returns the MTQ28UNCUE.
	 */
	public Integer getMtq28uncue() {
		try{
			if (_Mtq28uncue==null){
				_Mtq28uncue=getIntegerProperty("MTQ28UNCUE");
				return _Mtq28uncue;
			}else {
				return _Mtq28uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ28UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq28uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ28UNCUE",v);
		_Mtq28uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq28msip=null;

	/**
	 * @return Returns the MTQ28MSIP.
	 */
	public Integer getMtq28msip() {
		try{
			if (_Mtq28msip==null){
				_Mtq28msip=getIntegerProperty("MTQ28MSIP");
				return _Mtq28msip;
			}else {
				return _Mtq28msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ28MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq28msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ28MSIP",v);
		_Mtq28msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq28sem=null;

	/**
	 * @return Returns the MTQ28SEM.
	 */
	public Integer getMtq28sem() {
		try{
			if (_Mtq28sem==null){
				_Mtq28sem=getIntegerProperty("MTQ28SEM");
				return _Mtq28sem;
			}else {
				return _Mtq28sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ28SEM.
	 * @param v Value to Set.
	 */
	public void setMtq28sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ28SEM",v);
		_Mtq28sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq28phon=null;

	/**
	 * @return Returns the MTQ28PHON.
	 */
	public Integer getMtq28phon() {
		try{
			if (_Mtq28phon==null){
				_Mtq28phon=getIntegerProperty("MTQ28PHON");
				return _Mtq28phon;
			}else {
				return _Mtq28phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ28PHON.
	 * @param v Value to Set.
	 */
	public void setMtq28phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ28PHON",v);
		_Mtq28phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq29uncue=null;

	/**
	 * @return Returns the MTQ29UNCUE.
	 */
	public Integer getMtq29uncue() {
		try{
			if (_Mtq29uncue==null){
				_Mtq29uncue=getIntegerProperty("MTQ29UNCUE");
				return _Mtq29uncue;
			}else {
				return _Mtq29uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ29UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq29uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ29UNCUE",v);
		_Mtq29uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq29msip=null;

	/**
	 * @return Returns the MTQ29MSIP.
	 */
	public Integer getMtq29msip() {
		try{
			if (_Mtq29msip==null){
				_Mtq29msip=getIntegerProperty("MTQ29MSIP");
				return _Mtq29msip;
			}else {
				return _Mtq29msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ29MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq29msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ29MSIP",v);
		_Mtq29msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq29sem=null;

	/**
	 * @return Returns the MTQ29SEM.
	 */
	public Integer getMtq29sem() {
		try{
			if (_Mtq29sem==null){
				_Mtq29sem=getIntegerProperty("MTQ29SEM");
				return _Mtq29sem;
			}else {
				return _Mtq29sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ29SEM.
	 * @param v Value to Set.
	 */
	public void setMtq29sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ29SEM",v);
		_Mtq29sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq29phon=null;

	/**
	 * @return Returns the MTQ29PHON.
	 */
	public Integer getMtq29phon() {
		try{
			if (_Mtq29phon==null){
				_Mtq29phon=getIntegerProperty("MTQ29PHON");
				return _Mtq29phon;
			}else {
				return _Mtq29phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ29PHON.
	 * @param v Value to Set.
	 */
	public void setMtq29phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ29PHON",v);
		_Mtq29phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq30uncue=null;

	/**
	 * @return Returns the MTQ30UNCUE.
	 */
	public Integer getMtq30uncue() {
		try{
			if (_Mtq30uncue==null){
				_Mtq30uncue=getIntegerProperty("MTQ30UNCUE");
				return _Mtq30uncue;
			}else {
				return _Mtq30uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ30UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq30uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ30UNCUE",v);
		_Mtq30uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq30msip=null;

	/**
	 * @return Returns the MTQ30MSIP.
	 */
	public Integer getMtq30msip() {
		try{
			if (_Mtq30msip==null){
				_Mtq30msip=getIntegerProperty("MTQ30MSIP");
				return _Mtq30msip;
			}else {
				return _Mtq30msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ30MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq30msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ30MSIP",v);
		_Mtq30msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq30sem=null;

	/**
	 * @return Returns the MTQ30SEM.
	 */
	public Integer getMtq30sem() {
		try{
			if (_Mtq30sem==null){
				_Mtq30sem=getIntegerProperty("MTQ30SEM");
				return _Mtq30sem;
			}else {
				return _Mtq30sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ30SEM.
	 * @param v Value to Set.
	 */
	public void setMtq30sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ30SEM",v);
		_Mtq30sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq30phon=null;

	/**
	 * @return Returns the MTQ30PHON.
	 */
	public Integer getMtq30phon() {
		try{
			if (_Mtq30phon==null){
				_Mtq30phon=getIntegerProperty("MTQ30PHON");
				return _Mtq30phon;
			}else {
				return _Mtq30phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ30PHON.
	 * @param v Value to Set.
	 */
	public void setMtq30phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ30PHON",v);
		_Mtq30phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq31uncue=null;

	/**
	 * @return Returns the MTQ31UNCUE.
	 */
	public Integer getMtq31uncue() {
		try{
			if (_Mtq31uncue==null){
				_Mtq31uncue=getIntegerProperty("MTQ31UNCUE");
				return _Mtq31uncue;
			}else {
				return _Mtq31uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ31UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq31uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ31UNCUE",v);
		_Mtq31uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq31msip=null;

	/**
	 * @return Returns the MTQ31MSIP.
	 */
	public Integer getMtq31msip() {
		try{
			if (_Mtq31msip==null){
				_Mtq31msip=getIntegerProperty("MTQ31MSIP");
				return _Mtq31msip;
			}else {
				return _Mtq31msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ31MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq31msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ31MSIP",v);
		_Mtq31msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq31sem=null;

	/**
	 * @return Returns the MTQ31SEM.
	 */
	public Integer getMtq31sem() {
		try{
			if (_Mtq31sem==null){
				_Mtq31sem=getIntegerProperty("MTQ31SEM");
				return _Mtq31sem;
			}else {
				return _Mtq31sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ31SEM.
	 * @param v Value to Set.
	 */
	public void setMtq31sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ31SEM",v);
		_Mtq31sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq31phon=null;

	/**
	 * @return Returns the MTQ31PHON.
	 */
	public Integer getMtq31phon() {
		try{
			if (_Mtq31phon==null){
				_Mtq31phon=getIntegerProperty("MTQ31PHON");
				return _Mtq31phon;
			}else {
				return _Mtq31phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ31PHON.
	 * @param v Value to Set.
	 */
	public void setMtq31phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ31PHON",v);
		_Mtq31phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq32uncue=null;

	/**
	 * @return Returns the MTQ32UNCUE.
	 */
	public Integer getMtq32uncue() {
		try{
			if (_Mtq32uncue==null){
				_Mtq32uncue=getIntegerProperty("MTQ32UNCUE");
				return _Mtq32uncue;
			}else {
				return _Mtq32uncue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ32UNCUE.
	 * @param v Value to Set.
	 */
	public void setMtq32uncue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ32UNCUE",v);
		_Mtq32uncue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq32msip=null;

	/**
	 * @return Returns the MTQ32MSIP.
	 */
	public Integer getMtq32msip() {
		try{
			if (_Mtq32msip==null){
				_Mtq32msip=getIntegerProperty("MTQ32MSIP");
				return _Mtq32msip;
			}else {
				return _Mtq32msip;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ32MSIP.
	 * @param v Value to Set.
	 */
	public void setMtq32msip(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ32MSIP",v);
		_Mtq32msip=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq32sem=null;

	/**
	 * @return Returns the MTQ32SEM.
	 */
	public Integer getMtq32sem() {
		try{
			if (_Mtq32sem==null){
				_Mtq32sem=getIntegerProperty("MTQ32SEM");
				return _Mtq32sem;
			}else {
				return _Mtq32sem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ32SEM.
	 * @param v Value to Set.
	 */
	public void setMtq32sem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ32SEM",v);
		_Mtq32sem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mtq32phon=null;

	/**
	 * @return Returns the MTQ32PHON.
	 */
	public Integer getMtq32phon() {
		try{
			if (_Mtq32phon==null){
				_Mtq32phon=getIntegerProperty("MTQ32PHON");
				return _Mtq32phon;
			}else {
				return _Mtq32phon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTQ32PHON.
	 * @param v Value to Set.
	 */
	public void setMtq32phon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTQ32PHON",v);
		_Mtq32phon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mttotal=null;

	/**
	 * @return Returns the MTTOTAL.
	 */
	public Integer getMttotal() {
		try{
			if (_Mttotal==null){
				_Mttotal=getIntegerProperty("MTTOTAL");
				return _Mttotal;
			}else {
				return _Mttotal;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTTOTAL.
	 * @param v Value to Set.
	 */
	public void setMttotal(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTTOTAL",v);
		_Mttotal=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Mtcomm=null;

	/**
	 * @return Returns the MTCOMM.
	 */
	public String getMtcomm(){
		try{
			if (_Mtcomm==null){
				_Mtcomm=getStringProperty("MTCOMM");
				return _Mtcomm;
			}else {
				return _Mtcomm;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MTCOMM.
	 * @param v Value to Set.
	 */
	public void setMtcomm(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MTCOMM",v);
		_Mtcomm=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianMintdata> getAllDianMintdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianMintdata> al = new ArrayList<org.nrg.xdat.om.DianMintdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianMintdata> getDianMintdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianMintdata> al = new ArrayList<org.nrg.xdat.om.DianMintdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianMintdata> getDianMintdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianMintdata> al = new ArrayList<org.nrg.xdat.om.DianMintdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianMintdata getDianMintdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:mintData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianMintdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
