/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCondrBrainpathdata extends TissueLabresultdata implements org.nrg.xdat.model.CondrBrainpathdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCondrBrainpathdata.class);
	public static String SCHEMA_ELEMENT_NAME="condr:brainPathData";

	public AutoCondrBrainpathdata(ItemI item)
	{
		super(item);
	}

	public AutoCondrBrainpathdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCondrBrainpathdata(UserI user)
	 **/
	public AutoCondrBrainpathdata(){}

	public AutoCondrBrainpathdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "condr:brainPathData";
	}
	 private org.nrg.xdat.om.TissueLabresultdata _Labresultdata =null;

	/**
	 * labResultData
	 * @return org.nrg.xdat.om.TissueLabresultdata
	 */
	public org.nrg.xdat.om.TissueLabresultdata getLabresultdata() {
		try{
			if (_Labresultdata==null){
				_Labresultdata=((TissueLabresultdata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("labResultData")));
				return _Labresultdata;
			}else {
				return _Labresultdata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for labResultData.
	 * @param v Value to Set.
	 */
	public void setLabresultdata(ItemI v) throws Exception{
		_Labresultdata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/labResultData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/labResultData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * labResultData
	 * set org.nrg.xdat.model.TissueLabresultdataI
	 */
	public <A extends org.nrg.xdat.model.TissueLabresultdataI> void setLabresultdata(A item) throws Exception{
	setLabresultdata((ItemI)item);
	}

	/**
	 * Removes the labResultData.
	 * */
	public void removeLabresultdata() {
		_Labresultdata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/labResultData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Tumortype=null;

	/**
	 * @return Returns the tumorType.
	 */
	public String getTumortype(){
		try{
			if (_Tumortype==null){
				_Tumortype=getStringProperty("tumorType");
				return _Tumortype;
			}else {
				return _Tumortype;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tumorType.
	 * @param v Value to Set.
	 */
	public void setTumortype(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tumorType",v);
		_Tumortype=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Primarytumortype=null;

	/**
	 * @return Returns the primaryTumorType.
	 */
	public String getPrimarytumortype(){
		try{
			if (_Primarytumortype==null){
				_Primarytumortype=getStringProperty("primaryTumorType");
				return _Primarytumortype;
			}else {
				return _Primarytumortype;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for primaryTumorType.
	 * @param v Value to Set.
	 */
	public void setPrimarytumortype(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/primaryTumorType",v);
		_Primarytumortype=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Primarywhograde=null;

	/**
	 * @return Returns the primaryWHOGrade.
	 */
	public String getPrimarywhograde(){
		try{
			if (_Primarywhograde==null){
				_Primarywhograde=getStringProperty("primaryWHOGrade");
				return _Primarywhograde;
			}else {
				return _Primarywhograde;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for primaryWHOGrade.
	 * @param v Value to Set.
	 */
	public void setPrimarywhograde(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/primaryWHOGrade",v);
		_Primarywhograde=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Metastatorig=null;

	/**
	 * @return Returns the metastatOrig.
	 */
	public String getMetastatorig(){
		try{
			if (_Metastatorig==null){
				_Metastatorig=getStringProperty("metastatOrig");
				return _Metastatorig;
			}else {
				return _Metastatorig;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for metastatOrig.
	 * @param v Value to Set.
	 */
	public void setMetastatorig(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/metastatOrig",v);
		_Metastatorig=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Onep19qdeletions=null;

	/**
	 * @return Returns the onep19qDeletions.
	 */
	public String getOnep19qdeletions(){
		try{
			if (_Onep19qdeletions==null){
				_Onep19qdeletions=getStringProperty("onep19qDeletions");
				return _Onep19qdeletions;
			}else {
				return _Onep19qdeletions;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for onep19qDeletions.
	 * @param v Value to Set.
	 */
	public void setOnep19qdeletions(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/onep19qDeletions",v);
		_Onep19qdeletions=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Mgmtpromoterstatus=null;

	/**
	 * @return Returns the mgmtPromoterStatus.
	 */
	public String getMgmtpromoterstatus(){
		try{
			if (_Mgmtpromoterstatus==null){
				_Mgmtpromoterstatus=getStringProperty("mgmtPromoterStatus");
				return _Mgmtpromoterstatus;
			}else {
				return _Mgmtpromoterstatus;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for mgmtPromoterStatus.
	 * @param v Value to Set.
	 */
	public void setMgmtpromoterstatus(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/mgmtPromoterStatus",v);
		_Mgmtpromoterstatus=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Mib1index=null;

	/**
	 * @return Returns the mib1Index.
	 */
	public Double getMib1index() {
		try{
			if (_Mib1index==null){
				_Mib1index=getDoubleProperty("mib1Index");
				return _Mib1index;
			}else {
				return _Mib1index;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for mib1Index.
	 * @param v Value to Set.
	 */
	public void setMib1index(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/mib1Index",v);
		_Mib1index=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Mib1indexnotreported=null;

	/**
	 * @return Returns the mib1IndexNotReported.
	 */
	public Boolean getMib1indexnotreported() {
		try{
			if (_Mib1indexnotreported==null){
				_Mib1indexnotreported=getBooleanProperty("mib1IndexNotReported");
				return _Mib1indexnotreported;
			}else {
				return _Mib1indexnotreported;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for mib1IndexNotReported.
	 * @param v Value to Set.
	 */
	public void setMib1indexnotreported(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/mib1IndexNotReported",v);
		_Mib1indexnotreported=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Pten=null;

	/**
	 * @return Returns the pten.
	 */
	public String getPten(){
		try{
			if (_Pten==null){
				_Pten=getStringProperty("pten");
				return _Pten;
			}else {
				return _Pten;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for pten.
	 * @param v Value to Set.
	 */
	public void setPten(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/pten",v);
		_Pten=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _P53=null;

	/**
	 * @return Returns the p53.
	 */
	public String getP53(){
		try{
			if (_P53==null){
				_P53=getStringProperty("p53");
				return _P53;
			}else {
				return _P53;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for p53.
	 * @param v Value to Set.
	 */
	public void setP53(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/p53",v);
		_P53=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Othfeat=null;

	/**
	 * @return Returns the othFeat.
	 */
	public String getOthfeat(){
		try{
			if (_Othfeat==null){
				_Othfeat=getStringProperty("othFeat");
				return _Othfeat;
			}else {
				return _Othfeat;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for othFeat.
	 * @param v Value to Set.
	 */
	public void setOthfeat(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/othFeat",v);
		_Othfeat=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CondrBrainpathdata> getAllCondrBrainpathdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrBrainpathdata> al = new ArrayList<org.nrg.xdat.om.CondrBrainpathdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrBrainpathdata> getCondrBrainpathdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrBrainpathdata> al = new ArrayList<org.nrg.xdat.om.CondrBrainpathdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrBrainpathdata> getCondrBrainpathdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrBrainpathdata> al = new ArrayList<org.nrg.xdat.om.CondrBrainpathdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CondrBrainpathdata getCondrBrainpathdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("condr:brainPathData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CondrBrainpathdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //labResultData
	        TissueLabresultdata childLabresultdata = (TissueLabresultdata)this.getLabresultdata();
	            if (childLabresultdata!=null){
	              for(ResourceFile rf: ((TissueLabresultdata)childLabresultdata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("labResultData[" + ((TissueLabresultdata)childLabresultdata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("labResultData/" + ((TissueLabresultdata)childLabresultdata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
