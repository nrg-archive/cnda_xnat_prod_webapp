/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:00 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianCsfsampdata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianCsfsampdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianCsfsampdata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:csfsampData";

	public AutoDianCsfsampdata(ItemI item)
	{
		super(item);
	}

	public AutoDianCsfsampdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianCsfsampdata(UserI user)
	 **/
	public AutoDianCsfsampdata(){}

	public AutoDianCsfsampdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:csfsampData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Csfcoll=null;

	/**
	 * @return Returns the CSFCOLL.
	 */
	public Integer getCsfcoll() {
		try{
			if (_Csfcoll==null){
				_Csfcoll=getIntegerProperty("CSFCOLL");
				return _Csfcoll;
			}else {
				return _Csfcoll;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSFCOLL.
	 * @param v Value to Set.
	 */
	public void setCsfcoll(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSFCOLL",v);
		_Csfcoll=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Csfinit=null;

	/**
	 * @return Returns the CSFINIT.
	 */
	public String getCsfinit(){
		try{
			if (_Csfinit==null){
				_Csfinit=getStringProperty("CSFINIT");
				return _Csfinit;
			}else {
				return _Csfinit;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSFINIT.
	 * @param v Value to Set.
	 */
	public void setCsfinit(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSFINIT",v);
		_Csfinit=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Csfdate=null;

	/**
	 * @return Returns the CSFDATE.
	 */
	public Object getCsfdate(){
		try{
			if (_Csfdate==null){
				_Csfdate=getProperty("CSFDATE");
				return _Csfdate;
			}else {
				return _Csfdate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSFDATE.
	 * @param v Value to Set.
	 */
	public void setCsfdate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSFDATE",v);
		_Csfdate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Csffast=null;

	/**
	 * @return Returns the CSFFAST.
	 */
	public Integer getCsffast() {
		try{
			if (_Csffast==null){
				_Csffast=getIntegerProperty("CSFFAST");
				return _Csffast;
			}else {
				return _Csffast;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSFFAST.
	 * @param v Value to Set.
	 */
	public void setCsffast(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSFFAST",v);
		_Csffast=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Pretemp=null;

	/**
	 * @return Returns the PRETEMP.
	 */
	public Double getPretemp() {
		try{
			if (_Pretemp==null){
				_Pretemp=getDoubleProperty("PRETEMP");
				return _Pretemp;
			}else {
				return _Pretemp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PRETEMP.
	 * @param v Value to Set.
	 */
	public void setPretemp(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PRETEMP",v);
		_Pretemp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pretmpunt=null;

	/**
	 * @return Returns the PRETMPUNT.
	 */
	public Integer getPretmpunt() {
		try{
			if (_Pretmpunt==null){
				_Pretmpunt=getIntegerProperty("PRETMPUNT");
				return _Pretmpunt;
			}else {
				return _Pretmpunt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PRETMPUNT.
	 * @param v Value to Set.
	 */
	public void setPretmpunt(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PRETMPUNT",v);
		_Pretmpunt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Presys=null;

	/**
	 * @return Returns the PRESYS.
	 */
	public Integer getPresys() {
		try{
			if (_Presys==null){
				_Presys=getIntegerProperty("PRESYS");
				return _Presys;
			}else {
				return _Presys;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PRESYS.
	 * @param v Value to Set.
	 */
	public void setPresys(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PRESYS",v);
		_Presys=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Predia=null;

	/**
	 * @return Returns the PREDIA.
	 */
	public Integer getPredia() {
		try{
			if (_Predia==null){
				_Predia=getIntegerProperty("PREDIA");
				return _Predia;
			}else {
				return _Predia;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PREDIA.
	 * @param v Value to Set.
	 */
	public void setPredia(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PREDIA",v);
		_Predia=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Prepulse=null;

	/**
	 * @return Returns the PREPULSE.
	 */
	public Integer getPrepulse() {
		try{
			if (_Prepulse==null){
				_Prepulse=getIntegerProperty("PREPULSE");
				return _Prepulse;
			}else {
				return _Prepulse;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PREPULSE.
	 * @param v Value to Set.
	 */
	public void setPrepulse(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PREPULSE",v);
		_Prepulse=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Preresp=null;

	/**
	 * @return Returns the PRERESP.
	 */
	public Integer getPreresp() {
		try{
			if (_Preresp==null){
				_Preresp=getIntegerProperty("PRERESP");
				return _Preresp;
			}else {
				return _Preresp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PRERESP.
	 * @param v Value to Set.
	 */
	public void setPreresp(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PRERESP",v);
		_Preresp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Needle=null;

	/**
	 * @return Returns the NEEDLE.
	 */
	public Integer getNeedle() {
		try{
			if (_Needle==null){
				_Needle=getIntegerProperty("NEEDLE");
				return _Needle;
			}else {
				return _Needle;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NEEDLE.
	 * @param v Value to Set.
	 */
	public void setNeedle(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NEEDLE",v);
		_Needle=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Needleoth=null;

	/**
	 * @return Returns the NEEDLEOTH.
	 */
	public String getNeedleoth(){
		try{
			if (_Needleoth==null){
				_Needleoth=getStringProperty("NEEDLEOTH");
				return _Needleoth;
			}else {
				return _Needleoth;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NEEDLEOTH.
	 * @param v Value to Set.
	 */
	public void setNeedleoth(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NEEDLEOTH",v);
		_Needleoth=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Method=null;

	/**
	 * @return Returns the METHOD.
	 */
	public Integer getMethod() {
		try{
			if (_Method==null){
				_Method=getIntegerProperty("METHOD");
				return _Method;
			}else {
				return _Method;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for METHOD.
	 * @param v Value to Set.
	 */
	public void setMethod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/METHOD",v);
		_Method=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Lpsite=null;

	/**
	 * @return Returns the LPSITE.
	 */
	public Integer getLpsite() {
		try{
			if (_Lpsite==null){
				_Lpsite=getIntegerProperty("LPSITE");
				return _Lpsite;
			}else {
				return _Lpsite;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LPSITE.
	 * @param v Value to Set.
	 */
	public void setLpsite(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LPSITE",v);
		_Lpsite=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Lpsiteoth=null;

	/**
	 * @return Returns the LPSITEOTH.
	 */
	public String getLpsiteoth(){
		try{
			if (_Lpsiteoth==null){
				_Lpsiteoth=getStringProperty("LPSITEOTH");
				return _Lpsiteoth;
			}else {
				return _Lpsiteoth;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LPSITEOTH.
	 * @param v Value to Set.
	 */
	public void setLpsiteoth(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LPSITEOTH",v);
		_Lpsiteoth=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Position2=null;

	/**
	 * @return Returns the POSITION2.
	 */
	public Integer getPosition2() {
		try{
			if (_Position2==null){
				_Position2=getIntegerProperty("POSITION2");
				return _Position2;
			}else {
				return _Position2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for POSITION2.
	 * @param v Value to Set.
	 */
	public void setPosition2(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/POSITION2",v);
		_Position2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tubecoll=null;

	/**
	 * @return Returns the TUBECOLL.
	 */
	public Integer getTubecoll() {
		try{
			if (_Tubecoll==null){
				_Tubecoll=getIntegerProperty("TUBECOLL");
				return _Tubecoll;
			}else {
				return _Tubecoll;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TUBECOLL.
	 * @param v Value to Set.
	 */
	public void setTubecoll(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TUBECOLL",v);
		_Tubecoll=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tubeship=null;

	/**
	 * @return Returns the TUBESHIP.
	 */
	public Integer getTubeship() {
		try{
			if (_Tubeship==null){
				_Tubeship=getIntegerProperty("TUBESHIP");
				return _Tubeship;
			}else {
				return _Tubeship;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TUBESHIP.
	 * @param v Value to Set.
	 */
	public void setTubeship(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TUBESHIP",v);
		_Tubeship=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Styrtime=null;

	/**
	 * @return Returns the STYRTIME.
	 */
	public Integer getStyrtime() {
		try{
			if (_Styrtime==null){
				_Styrtime=getIntegerProperty("STYRTIME");
				return _Styrtime;
			}else {
				return _Styrtime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for STYRTIME.
	 * @param v Value to Set.
	 */
	public void setStyrtime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/STYRTIME",v);
		_Styrtime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Csftime=null;

	/**
	 * @return Returns the CSFTIME.
	 */
	public Integer getCsftime() {
		try{
			if (_Csftime==null){
				_Csftime=getIntegerProperty("CSFTIME");
				return _Csftime;
			}else {
				return _Csftime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSFTIME.
	 * @param v Value to Set.
	 */
	public void setCsftime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSFTIME",v);
		_Csftime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Csftrans=null;

	/**
	 * @return Returns the CSFTRANS.
	 */
	public String getCsftrans(){
		try{
			if (_Csftrans==null){
				_Csftrans=getStringProperty("CSFTRANS");
				return _Csftrans;
			}else {
				return _Csftrans;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSFTRANS.
	 * @param v Value to Set.
	 */
	public void setCsftrans(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSFTRANS",v);
		_Csftrans=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Volcoll=null;

	/**
	 * @return Returns the VOLCOLL.
	 */
	public Integer getVolcoll() {
		try{
			if (_Volcoll==null){
				_Volcoll=getIntegerProperty("VOLCOLL");
				return _Volcoll;
			}else {
				return _Volcoll;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VOLCOLL.
	 * @param v Value to Set.
	 */
	public void setVolcoll(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VOLCOLL",v);
		_Volcoll=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Voltrans=null;

	/**
	 * @return Returns the VOLTRANS.
	 */
	public Integer getVoltrans() {
		try{
			if (_Voltrans==null){
				_Voltrans=getIntegerProperty("VOLTRANS");
				return _Voltrans;
			}else {
				return _Voltrans;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VOLTRANS.
	 * @param v Value to Set.
	 */
	public void setVoltrans(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VOLTRANS",v);
		_Voltrans=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Loclab=null;

	/**
	 * @return Returns the LOCLAB.
	 */
	public Integer getLoclab() {
		try{
			if (_Loclab==null){
				_Loclab=getIntegerProperty("LOCLAB");
				return _Loclab;
			}else {
				return _Loclab;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LOCLAB.
	 * @param v Value to Set.
	 */
	public void setLoclab(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LOCLAB",v);
		_Loclab=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Timefroz=null;

	/**
	 * @return Returns the TIMEFROZ.
	 */
	public String getTimefroz(){
		try{
			if (_Timefroz==null){
				_Timefroz=getStringProperty("TIMEFROZ");
				return _Timefroz;
			}else {
				return _Timefroz;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TIMEFROZ.
	 * @param v Value to Set.
	 */
	public void setTimefroz(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TIMEFROZ",v);
		_Timefroz=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Notes=null;

	/**
	 * @return Returns the NOTES.
	 */
	public String getNotes(){
		try{
			if (_Notes==null){
				_Notes=getStringProperty("NOTES");
				return _Notes;
			}else {
				return _Notes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NOTES.
	 * @param v Value to Set.
	 */
	public void setNotes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NOTES",v);
		_Notes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Csfship=null;

	/**
	 * @return Returns the CSFSHIP.
	 */
	public Integer getCsfship() {
		try{
			if (_Csfship==null){
				_Csfship=getIntegerProperty("CSFSHIP");
				return _Csfship;
			}else {
				return _Csfship;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSFSHIP.
	 * @param v Value to Set.
	 */
	public void setCsfship(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSFSHIP",v);
		_Csfship=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Csfshdt=null;

	/**
	 * @return Returns the CSFSHDT.
	 */
	public Object getCsfshdt(){
		try{
			if (_Csfshdt==null){
				_Csfshdt=getProperty("CSFSHDT");
				return _Csfshdt;
			}else {
				return _Csfshdt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSFSHDT.
	 * @param v Value to Set.
	 */
	public void setCsfshdt(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSFSHDT",v);
		_Csfshdt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Csfshnum=null;

	/**
	 * @return Returns the CSFSHNUM.
	 */
	public String getCsfshnum(){
		try{
			if (_Csfshnum==null){
				_Csfshnum=getStringProperty("CSFSHNUM");
				return _Csfshnum;
			}else {
				return _Csfshnum;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSFSHNUM.
	 * @param v Value to Set.
	 */
	public void setCsfshnum(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSFSHNUM",v);
		_Csfshnum=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Csfdatervd=null;

	/**
	 * @return Returns the CSFDATERVD.
	 */
	public Object getCsfdatervd(){
		try{
			if (_Csfdatervd==null){
				_Csfdatervd=getProperty("CSFDATERVD");
				return _Csfdatervd;
			}else {
				return _Csfdatervd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSFDATERVD.
	 * @param v Value to Set.
	 */
	public void setCsfdatervd(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSFDATERVD",v);
		_Csfdatervd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Csfvolrvd=null;

	/**
	 * @return Returns the CSFVOLRVD.
	 */
	public Double getCsfvolrvd() {
		try{
			if (_Csfvolrvd==null){
				_Csfvolrvd=getDoubleProperty("CSFVOLRVD");
				return _Csfvolrvd;
			}else {
				return _Csfvolrvd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSFVOLRVD.
	 * @param v Value to Set.
	 */
	public void setCsfvolrvd(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSFVOLRVD",v);
		_Csfvolrvd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Csfvialsrvd=null;

	/**
	 * @return Returns the CSFVIALSRVD.
	 */
	public Integer getCsfvialsrvd() {
		try{
			if (_Csfvialsrvd==null){
				_Csfvialsrvd=getIntegerProperty("CSFVIALSRVD");
				return _Csfvialsrvd;
			}else {
				return _Csfvialsrvd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSFVIALSRVD.
	 * @param v Value to Set.
	 */
	public void setCsfvialsrvd(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSFVIALSRVD",v);
		_Csfvialsrvd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Position3=null;

	/**
	 * @return Returns the POSITION3.
	 */
	public Integer getPosition3() {
		try{
			if (_Position3==null){
				_Position3=getIntegerProperty("POSITION3");
				return _Position3;
			}else {
				return _Position3;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for POSITION3.
	 * @param v Value to Set.
	 */
	public void setPosition3(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/POSITION3",v);
		_Position3=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Posttemp=null;

	/**
	 * @return Returns the POSTTEMP.
	 */
	public Double getPosttemp() {
		try{
			if (_Posttemp==null){
				_Posttemp=getDoubleProperty("POSTTEMP");
				return _Posttemp;
			}else {
				return _Posttemp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for POSTTEMP.
	 * @param v Value to Set.
	 */
	public void setPosttemp(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/POSTTEMP",v);
		_Posttemp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Posttmpu=null;

	/**
	 * @return Returns the POSTTMPU.
	 */
	public Integer getPosttmpu() {
		try{
			if (_Posttmpu==null){
				_Posttmpu=getIntegerProperty("POSTTMPU");
				return _Posttmpu;
			}else {
				return _Posttmpu;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for POSTTMPU.
	 * @param v Value to Set.
	 */
	public void setPosttmpu(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/POSTTMPU",v);
		_Posttmpu=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Postsys=null;

	/**
	 * @return Returns the POSTSYS.
	 */
	public Integer getPostsys() {
		try{
			if (_Postsys==null){
				_Postsys=getIntegerProperty("POSTSYS");
				return _Postsys;
			}else {
				return _Postsys;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for POSTSYS.
	 * @param v Value to Set.
	 */
	public void setPostsys(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/POSTSYS",v);
		_Postsys=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Postdia=null;

	/**
	 * @return Returns the POSTDIA.
	 */
	public Integer getPostdia() {
		try{
			if (_Postdia==null){
				_Postdia=getIntegerProperty("POSTDIA");
				return _Postdia;
			}else {
				return _Postdia;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for POSTDIA.
	 * @param v Value to Set.
	 */
	public void setPostdia(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/POSTDIA",v);
		_Postdia=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Postpulse=null;

	/**
	 * @return Returns the POSTPULSE.
	 */
	public Integer getPostpulse() {
		try{
			if (_Postpulse==null){
				_Postpulse=getIntegerProperty("POSTPULSE");
				return _Postpulse;
			}else {
				return _Postpulse;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for POSTPULSE.
	 * @param v Value to Set.
	 */
	public void setPostpulse(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/POSTPULSE",v);
		_Postpulse=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Postresp=null;

	/**
	 * @return Returns the POSTRESP.
	 */
	public Integer getPostresp() {
		try{
			if (_Postresp==null){
				_Postresp=getIntegerProperty("POSTRESP");
				return _Postresp;
			}else {
				return _Postresp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for POSTRESP.
	 * @param v Value to Set.
	 */
	public void setPostresp(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/POSTRESP",v);
		_Postresp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Position4=null;

	/**
	 * @return Returns the POSITION4.
	 */
	public Integer getPosition4() {
		try{
			if (_Position4==null){
				_Position4=getIntegerProperty("POSITION4");
				return _Position4;
			}else {
				return _Position4;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for POSITION4.
	 * @param v Value to Set.
	 */
	public void setPosition4(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/POSITION4",v);
		_Position4=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Hrtemp=null;

	/**
	 * @return Returns the HRTEMP.
	 */
	public Double getHrtemp() {
		try{
			if (_Hrtemp==null){
				_Hrtemp=getDoubleProperty("HRTEMP");
				return _Hrtemp;
			}else {
				return _Hrtemp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HRTEMP.
	 * @param v Value to Set.
	 */
	public void setHrtemp(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HRTEMP",v);
		_Hrtemp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Hrtmpu=null;

	/**
	 * @return Returns the HRTMPU.
	 */
	public Integer getHrtmpu() {
		try{
			if (_Hrtmpu==null){
				_Hrtmpu=getIntegerProperty("HRTMPU");
				return _Hrtmpu;
			}else {
				return _Hrtmpu;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HRTMPU.
	 * @param v Value to Set.
	 */
	public void setHrtmpu(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HRTMPU",v);
		_Hrtmpu=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Hrsys=null;

	/**
	 * @return Returns the HRSYS.
	 */
	public Integer getHrsys() {
		try{
			if (_Hrsys==null){
				_Hrsys=getIntegerProperty("HRSYS");
				return _Hrsys;
			}else {
				return _Hrsys;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HRSYS.
	 * @param v Value to Set.
	 */
	public void setHrsys(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HRSYS",v);
		_Hrsys=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Hrdia=null;

	/**
	 * @return Returns the HRDIA.
	 */
	public Integer getHrdia() {
		try{
			if (_Hrdia==null){
				_Hrdia=getIntegerProperty("HRDIA");
				return _Hrdia;
			}else {
				return _Hrdia;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HRDIA.
	 * @param v Value to Set.
	 */
	public void setHrdia(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HRDIA",v);
		_Hrdia=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Hrpulse=null;

	/**
	 * @return Returns the HRPULSE.
	 */
	public Integer getHrpulse() {
		try{
			if (_Hrpulse==null){
				_Hrpulse=getIntegerProperty("HRPULSE");
				return _Hrpulse;
			}else {
				return _Hrpulse;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HRPULSE.
	 * @param v Value to Set.
	 */
	public void setHrpulse(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HRPULSE",v);
		_Hrpulse=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Hrresp=null;

	/**
	 * @return Returns the HRRESP.
	 */
	public Integer getHrresp() {
		try{
			if (_Hrresp==null){
				_Hrresp=getIntegerProperty("HRRESP");
				return _Hrresp;
			}else {
				return _Hrresp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HRRESP.
	 * @param v Value to Set.
	 */
	public void setHrresp(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HRRESP",v);
		_Hrresp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Comp=null;

	/**
	 * @return Returns the COMP.
	 */
	public Integer getComp() {
		try{
			if (_Comp==null){
				_Comp=getIntegerProperty("COMP");
				return _Comp;
			}else {
				return _Comp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COMP.
	 * @param v Value to Set.
	 */
	public void setComp(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COMP",v);
		_Comp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Compoth=null;

	/**
	 * @return Returns the COMPOTH.
	 */
	public String getCompoth(){
		try{
			if (_Compoth==null){
				_Compoth=getStringProperty("COMPOTH");
				return _Compoth;
			}else {
				return _Compoth;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COMPOTH.
	 * @param v Value to Set.
	 */
	public void setCompoth(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COMPOTH",v);
		_Compoth=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianCsfsampdata> getAllDianCsfsampdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianCsfsampdata> al = new ArrayList<org.nrg.xdat.om.DianCsfsampdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianCsfsampdata> getDianCsfsampdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianCsfsampdata> al = new ArrayList<org.nrg.xdat.om.DianCsfsampdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianCsfsampdata> getDianCsfsampdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianCsfsampdata> al = new ArrayList<org.nrg.xdat.om.DianCsfsampdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianCsfsampdata getDianCsfsampdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:csfsampData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianCsfsampdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
