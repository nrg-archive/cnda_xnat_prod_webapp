/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoSfEncounterlogEncounter extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.SfEncounterlogEncounterI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoSfEncounterlogEncounter.class);
	public static String SCHEMA_ELEMENT_NAME="sf:encounterLog_encounter";

	public AutoSfEncounterlogEncounter(ItemI item)
	{
		super(item);
	}

	public AutoSfEncounterlogEncounter(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoSfEncounterlogEncounter(UserI user)
	 **/
	public AutoSfEncounterlogEncounter(){}

	public AutoSfEncounterlogEncounter(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "sf:encounterLog_encounter";
	}

	//FIELD

	private Object _Date=null;

	/**
	 * @return Returns the date.
	 */
	public Object getDate(){
		try{
			if (_Date==null){
				_Date=getProperty("date");
				return _Date;
			}else {
				return _Date;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for date.
	 * @param v Value to Set.
	 */
	public void setDate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/date",v);
		_Date=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Time=null;

	/**
	 * @return Returns the time.
	 */
	public Object getTime(){
		try{
			if (_Time==null){
				_Time=getProperty("time");
				return _Time;
			}else {
				return _Time;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for time.
	 * @param v Value to Set.
	 */
	public void setTime(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/time",v);
		_Time=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _EncounterType=null;

	/**
	 * @return Returns the encounter_type.
	 */
	public String getEncounterType(){
		try{
			if (_EncounterType==null){
				_EncounterType=getStringProperty("encounter_type");
				return _EncounterType;
			}else {
				return _EncounterType;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for encounter_type.
	 * @param v Value to Set.
	 */
	public void setEncounterType(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/encounter_type",v);
		_EncounterType=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _ProjectTimepoint=null;

	/**
	 * @return Returns the project_timepoint.
	 */
	public String getProjectTimepoint(){
		try{
			if (_ProjectTimepoint==null){
				_ProjectTimepoint=getStringProperty("project_timepoint");
				return _ProjectTimepoint;
			}else {
				return _ProjectTimepoint;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for project_timepoint.
	 * @param v Value to Set.
	 */
	public void setProjectTimepoint(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/project_timepoint",v);
		_ProjectTimepoint=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _SubjectStatus=null;

	/**
	 * @return Returns the subject_status.
	 */
	public String getSubjectStatus(){
		try{
			if (_SubjectStatus==null){
				_SubjectStatus=getStringProperty("subject_status");
				return _SubjectStatus;
			}else {
				return _SubjectStatus;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for subject_status.
	 * @param v Value to Set.
	 */
	public void setSubjectStatus(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/subject_status",v);
		_SubjectStatus=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Comment=null;

	/**
	 * @return Returns the comment.
	 */
	public String getComment(){
		try{
			if (_Comment==null){
				_Comment=getStringProperty("comment");
				return _Comment;
			}else {
				return _Comment;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for comment.
	 * @param v Value to Set.
	 */
	public void setComment(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/comment",v);
		_Comment=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _EncounterNumber=null;

	/**
	 * @return Returns the encounter_number.
	 */
	public String getEncounterNumber(){
		try{
			if (_EncounterNumber==null){
				_EncounterNumber=getStringProperty("encounter_number");
				return _EncounterNumber;
			}else {
				return _EncounterNumber;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for encounter_number.
	 * @param v Value to Set.
	 */
	public void setEncounterNumber(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/encounter_number",v);
		_EncounterNumber=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _SfEncounterlogEncounterId=null;

	/**
	 * @return Returns the sf_encounterLog_encounter_id.
	 */
	public Integer getSfEncounterlogEncounterId() {
		try{
			if (_SfEncounterlogEncounterId==null){
				_SfEncounterlogEncounterId=getIntegerProperty("sf_encounterLog_encounter_id");
				return _SfEncounterlogEncounterId;
			}else {
				return _SfEncounterlogEncounterId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sf_encounterLog_encounter_id.
	 * @param v Value to Set.
	 */
	public void setSfEncounterlogEncounterId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sf_encounterLog_encounter_id",v);
		_SfEncounterlogEncounterId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.SfEncounterlogEncounter> getAllSfEncounterlogEncounters(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfEncounterlogEncounter> al = new ArrayList<org.nrg.xdat.om.SfEncounterlogEncounter>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.SfEncounterlogEncounter> getSfEncounterlogEncountersByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfEncounterlogEncounter> al = new ArrayList<org.nrg.xdat.om.SfEncounterlogEncounter>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.SfEncounterlogEncounter> getSfEncounterlogEncountersByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfEncounterlogEncounter> al = new ArrayList<org.nrg.xdat.om.SfEncounterlogEncounter>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static SfEncounterlogEncounter getSfEncounterlogEncountersBySfEncounterlogEncounterId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("sf:encounterLog_encounter/sf_encounterLog_encounter_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (SfEncounterlogEncounter) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
