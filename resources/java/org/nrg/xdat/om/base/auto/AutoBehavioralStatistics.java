/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoBehavioralStatistics extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.BehavioralStatisticsI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoBehavioralStatistics.class);
	public static String SCHEMA_ELEMENT_NAME="behavioral:statistics";

	public AutoBehavioralStatistics(ItemI item)
	{
		super(item);
	}

	public AutoBehavioralStatistics(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoBehavioralStatistics(UserI user)
	 **/
	public AutoBehavioralStatistics(){}

	public AutoBehavioralStatistics(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "behavioral:statistics";
	}

	//FIELD

	private Double _Correct=null;

	/**
	 * @return Returns the correct.
	 */
	public Double getCorrect() {
		try{
			if (_Correct==null){
				_Correct=getDoubleProperty("correct");
				return _Correct;
			}else {
				return _Correct;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for correct.
	 * @param v Value to Set.
	 */
	public void setCorrect(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/correct",v);
		_Correct=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Missed=null;

	/**
	 * @return Returns the missed.
	 */
	public Double getMissed() {
		try{
			if (_Missed==null){
				_Missed=getDoubleProperty("missed");
				return _Missed;
			}else {
				return _Missed;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for missed.
	 * @param v Value to Set.
	 */
	public void setMissed(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/missed",v);
		_Missed=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Wrong=null;

	/**
	 * @return Returns the wrong.
	 */
	public Double getWrong() {
		try{
			if (_Wrong==null){
				_Wrong=getDoubleProperty("wrong");
				return _Wrong;
			}else {
				return _Wrong;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wrong.
	 * @param v Value to Set.
	 */
	public void setWrong(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wrong",v);
		_Wrong=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _ResponseTime=null;

	/**
	 * @return Returns the response_time.
	 */
	public Double getResponseTime() {
		try{
			if (_ResponseTime==null){
				_ResponseTime=getDoubleProperty("response_time");
				return _ResponseTime;
			}else {
				return _ResponseTime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for response_time.
	 * @param v Value to Set.
	 */
	public void setResponseTime(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/response_time",v);
		_ResponseTime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Mean=null;

	/**
	 * @return Returns the mean.
	 */
	public Double getMean() {
		try{
			if (_Mean==null){
				_Mean=getDoubleProperty("mean");
				return _Mean;
			}else {
				return _Mean;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for mean.
	 * @param v Value to Set.
	 */
	public void setMean(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/mean",v);
		_Mean=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Median=null;

	/**
	 * @return Returns the median.
	 */
	public Double getMedian() {
		try{
			if (_Median==null){
				_Median=getDoubleProperty("median");
				return _Median;
			}else {
				return _Median;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for median.
	 * @param v Value to Set.
	 */
	public void setMedian(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/median",v);
		_Median=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Mode=null;

	/**
	 * @return Returns the mode.
	 */
	public Double getMode() {
		try{
			if (_Mode==null){
				_Mode=getDoubleProperty("mode");
				return _Mode;
			}else {
				return _Mode;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for mode.
	 * @param v Value to Set.
	 */
	public void setMode(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/mode",v);
		_Mode=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _StdDeviation=null;

	/**
	 * @return Returns the std_deviation.
	 */
	public Double getStdDeviation() {
		try{
			if (_StdDeviation==null){
				_StdDeviation=getDoubleProperty("std_deviation");
				return _StdDeviation;
			}else {
				return _StdDeviation;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for std_deviation.
	 * @param v Value to Set.
	 */
	public void setStdDeviation(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/std_deviation",v);
		_StdDeviation=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _StdError=null;

	/**
	 * @return Returns the std_error.
	 */
	public Double getStdError() {
		try{
			if (_StdError==null){
				_StdError=getDoubleProperty("std_error");
				return _StdError;
			}else {
				return _StdError;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for std_error.
	 * @param v Value to Set.
	 */
	public void setStdError(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/std_error",v);
		_StdError=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _WeightedAverage=null;

	/**
	 * @return Returns the weighted_average.
	 */
	public Double getWeightedAverage() {
		try{
			if (_WeightedAverage==null){
				_WeightedAverage=getDoubleProperty("weighted_average");
				return _WeightedAverage;
			}else {
				return _WeightedAverage;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for weighted_average.
	 * @param v Value to Set.
	 */
	public void setWeightedAverage(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/weighted_average",v);
		_WeightedAverage=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Maximum=null;

	/**
	 * @return Returns the maximum.
	 */
	public String getMaximum(){
		try{
			if (_Maximum==null){
				_Maximum=getStringProperty("maximum");
				return _Maximum;
			}else {
				return _Maximum;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for maximum.
	 * @param v Value to Set.
	 */
	public void setMaximum(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/maximum",v);
		_Maximum=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Minimum=null;

	/**
	 * @return Returns the minimum.
	 */
	public Double getMinimum() {
		try{
			if (_Minimum==null){
				_Minimum=getDoubleProperty("minimum");
				return _Minimum;
			}else {
				return _Minimum;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for minimum.
	 * @param v Value to Set.
	 */
	public void setMinimum(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/minimum",v);
		_Minimum=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.BehavioralStatisticsAdditional> _Additional =null;

	/**
	 * additional
	 * @return Returns an List of org.nrg.xdat.om.BehavioralStatisticsAdditional
	 */
	public <A extends org.nrg.xdat.model.BehavioralStatisticsAdditionalI> List<A> getAdditional() {
		try{
			if (_Additional==null){
				_Additional=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("additional"));
			}
			return (List<A>) _Additional;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.BehavioralStatisticsAdditional>();}
	}

	/**
	 * Sets the value for additional.
	 * @param v Value to Set.
	 */
	public void setAdditional(ItemI v) throws Exception{
		_Additional =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/additional",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/additional",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * additional
	 * Adds org.nrg.xdat.model.BehavioralStatisticsAdditionalI
	 */
	public <A extends org.nrg.xdat.model.BehavioralStatisticsAdditionalI> void addAdditional(A item) throws Exception{
	setAdditional((ItemI)item);
	}

	/**
	 * Removes the additional of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeAdditional(int index) throws java.lang.IndexOutOfBoundsException {
		_Additional =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/additional",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _BehavioralStatisticsId=null;

	/**
	 * @return Returns the behavioral_statistics_id.
	 */
	public Integer getBehavioralStatisticsId() {
		try{
			if (_BehavioralStatisticsId==null){
				_BehavioralStatisticsId=getIntegerProperty("behavioral_statistics_id");
				return _BehavioralStatisticsId;
			}else {
				return _BehavioralStatisticsId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for behavioral_statistics_id.
	 * @param v Value to Set.
	 */
	public void setBehavioralStatisticsId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/behavioral_statistics_id",v);
		_BehavioralStatisticsId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.BehavioralStatistics> getAllBehavioralStatisticss(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.BehavioralStatistics> al = new ArrayList<org.nrg.xdat.om.BehavioralStatistics>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.BehavioralStatistics> getBehavioralStatisticssByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.BehavioralStatistics> al = new ArrayList<org.nrg.xdat.om.BehavioralStatistics>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.BehavioralStatistics> getBehavioralStatisticssByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.BehavioralStatistics> al = new ArrayList<org.nrg.xdat.om.BehavioralStatistics>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static BehavioralStatistics getBehavioralStatisticssByBehavioralStatisticsId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("behavioral:statistics/behavioral_statistics_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (BehavioralStatistics) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //additional
	        for(org.nrg.xdat.model.BehavioralStatisticsAdditionalI childAdditional : this.getAdditional()){
	            if (childAdditional!=null){
	              for(ResourceFile rf: ((BehavioralStatisticsAdditional)childAdditional).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("additional[" + ((BehavioralStatisticsAdditional)childAdditional).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("additional/" + ((BehavioralStatisticsAdditional)childAdditional).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
