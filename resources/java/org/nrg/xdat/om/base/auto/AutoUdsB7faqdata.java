/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoUdsB7faqdata extends XnatSubjectassessordata implements org.nrg.xdat.model.UdsB7faqdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoUdsB7faqdata.class);
	public static String SCHEMA_ELEMENT_NAME="uds:b7faqData";

	public AutoUdsB7faqdata(ItemI item)
	{
		super(item);
	}

	public AutoUdsB7faqdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoUdsB7faqdata(UserI user)
	 **/
	public AutoUdsB7faqdata(){}

	public AutoUdsB7faqdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "uds:b7faqData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Bills=null;

	/**
	 * @return Returns the BILLS.
	 */
	public Integer getBills() {
		try{
			if (_Bills==null){
				_Bills=getIntegerProperty("BILLS");
				return _Bills;
			}else {
				return _Bills;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BILLS.
	 * @param v Value to Set.
	 */
	public void setBills(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BILLS",v);
		_Bills=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Taxes=null;

	/**
	 * @return Returns the TAXES.
	 */
	public Integer getTaxes() {
		try{
			if (_Taxes==null){
				_Taxes=getIntegerProperty("TAXES");
				return _Taxes;
			}else {
				return _Taxes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TAXES.
	 * @param v Value to Set.
	 */
	public void setTaxes(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TAXES",v);
		_Taxes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Shopping=null;

	/**
	 * @return Returns the SHOPPING.
	 */
	public Integer getShopping() {
		try{
			if (_Shopping==null){
				_Shopping=getIntegerProperty("SHOPPING");
				return _Shopping;
			}else {
				return _Shopping;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SHOPPING.
	 * @param v Value to Set.
	 */
	public void setShopping(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SHOPPING",v);
		_Shopping=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Games=null;

	/**
	 * @return Returns the GAMES.
	 */
	public Integer getGames() {
		try{
			if (_Games==null){
				_Games=getIntegerProperty("GAMES");
				return _Games;
			}else {
				return _Games;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GAMES.
	 * @param v Value to Set.
	 */
	public void setGames(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GAMES",v);
		_Games=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Stove=null;

	/**
	 * @return Returns the STOVE.
	 */
	public Integer getStove() {
		try{
			if (_Stove==null){
				_Stove=getIntegerProperty("STOVE");
				return _Stove;
			}else {
				return _Stove;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for STOVE.
	 * @param v Value to Set.
	 */
	public void setStove(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/STOVE",v);
		_Stove=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mealprep=null;

	/**
	 * @return Returns the MEALPREP.
	 */
	public Integer getMealprep() {
		try{
			if (_Mealprep==null){
				_Mealprep=getIntegerProperty("MEALPREP");
				return _Mealprep;
			}else {
				return _Mealprep;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MEALPREP.
	 * @param v Value to Set.
	 */
	public void setMealprep(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MEALPREP",v);
		_Mealprep=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Events=null;

	/**
	 * @return Returns the EVENTS.
	 */
	public Integer getEvents() {
		try{
			if (_Events==null){
				_Events=getIntegerProperty("EVENTS");
				return _Events;
			}else {
				return _Events;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for EVENTS.
	 * @param v Value to Set.
	 */
	public void setEvents(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/EVENTS",v);
		_Events=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Payattn=null;

	/**
	 * @return Returns the PAYATTN.
	 */
	public Integer getPayattn() {
		try{
			if (_Payattn==null){
				_Payattn=getIntegerProperty("PAYATTN");
				return _Payattn;
			}else {
				return _Payattn;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PAYATTN.
	 * @param v Value to Set.
	 */
	public void setPayattn(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PAYATTN",v);
		_Payattn=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Remdates=null;

	/**
	 * @return Returns the REMDATES.
	 */
	public Integer getRemdates() {
		try{
			if (_Remdates==null){
				_Remdates=getIntegerProperty("REMDATES");
				return _Remdates;
			}else {
				return _Remdates;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REMDATES.
	 * @param v Value to Set.
	 */
	public void setRemdates(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REMDATES",v);
		_Remdates=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Travel=null;

	/**
	 * @return Returns the TRAVEL.
	 */
	public Integer getTravel() {
		try{
			if (_Travel==null){
				_Travel=getIntegerProperty("TRAVEL");
				return _Travel;
			}else {
				return _Travel;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRAVEL.
	 * @param v Value to Set.
	 */
	public void setTravel(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRAVEL",v);
		_Travel=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.UdsB7faqdata> getAllUdsB7faqdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsB7faqdata> al = new ArrayList<org.nrg.xdat.om.UdsB7faqdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsB7faqdata> getUdsB7faqdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsB7faqdata> al = new ArrayList<org.nrg.xdat.om.UdsB7faqdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsB7faqdata> getUdsB7faqdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsB7faqdata> al = new ArrayList<org.nrg.xdat.om.UdsB7faqdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static UdsB7faqdata getUdsB7faqdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("uds:b7faqData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (UdsB7faqdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
