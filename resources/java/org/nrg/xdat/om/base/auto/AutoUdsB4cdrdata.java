/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoUdsB4cdrdata extends XnatSubjectassessordata implements org.nrg.xdat.model.UdsB4cdrdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoUdsB4cdrdata.class);
	public static String SCHEMA_ELEMENT_NAME="uds:b4cdrData";

	public AutoUdsB4cdrdata(ItemI item)
	{
		super(item);
	}

	public AutoUdsB4cdrdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoUdsB4cdrdata(UserI user)
	 **/
	public AutoUdsB4cdrdata(){}

	public AutoUdsB4cdrdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "uds:b4cdrData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Initials=null;

	/**
	 * @return Returns the INITIALS.
	 */
	public String getInitials(){
		try{
			if (_Initials==null){
				_Initials=getStringProperty("INITIALS");
				return _Initials;
			}else {
				return _Initials;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INITIALS.
	 * @param v Value to Set.
	 */
	public void setInitials(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INITIALS",v);
		_Initials=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Memory=null;

	/**
	 * @return Returns the MEMORY.
	 */
	public Double getMemory() {
		try{
			if (_Memory==null){
				_Memory=getDoubleProperty("MEMORY");
				return _Memory;
			}else {
				return _Memory;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MEMORY.
	 * @param v Value to Set.
	 */
	public void setMemory(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MEMORY",v);
		_Memory=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Orient=null;

	/**
	 * @return Returns the ORIENT.
	 */
	public Double getOrient() {
		try{
			if (_Orient==null){
				_Orient=getDoubleProperty("ORIENT");
				return _Orient;
			}else {
				return _Orient;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ORIENT.
	 * @param v Value to Set.
	 */
	public void setOrient(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ORIENT",v);
		_Orient=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Judgment=null;

	/**
	 * @return Returns the JUDGMENT.
	 */
	public Double getJudgment() {
		try{
			if (_Judgment==null){
				_Judgment=getDoubleProperty("JUDGMENT");
				return _Judgment;
			}else {
				return _Judgment;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for JUDGMENT.
	 * @param v Value to Set.
	 */
	public void setJudgment(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/JUDGMENT",v);
		_Judgment=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Commun=null;

	/**
	 * @return Returns the COMMUN.
	 */
	public Double getCommun() {
		try{
			if (_Commun==null){
				_Commun=getDoubleProperty("COMMUN");
				return _Commun;
			}else {
				return _Commun;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COMMUN.
	 * @param v Value to Set.
	 */
	public void setCommun(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COMMUN",v);
		_Commun=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Homehobb=null;

	/**
	 * @return Returns the HOMEHOBB.
	 */
	public Double getHomehobb() {
		try{
			if (_Homehobb==null){
				_Homehobb=getDoubleProperty("HOMEHOBB");
				return _Homehobb;
			}else {
				return _Homehobb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HOMEHOBB.
	 * @param v Value to Set.
	 */
	public void setHomehobb(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HOMEHOBB",v);
		_Homehobb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Perscare=null;

	/**
	 * @return Returns the PERSCARE.
	 */
	public Double getPerscare() {
		try{
			if (_Perscare==null){
				_Perscare=getDoubleProperty("PERSCARE");
				return _Perscare;
			}else {
				return _Perscare;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PERSCARE.
	 * @param v Value to Set.
	 */
	public void setPerscare(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PERSCARE",v);
		_Perscare=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Cdrsum=null;

	/**
	 * @return Returns the CDRSUM.
	 */
	public Double getCdrsum() {
		try{
			if (_Cdrsum==null){
				_Cdrsum=getDoubleProperty("CDRSUM");
				return _Cdrsum;
			}else {
				return _Cdrsum;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CDRSUM.
	 * @param v Value to Set.
	 */
	public void setCdrsum(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CDRSUM",v);
		_Cdrsum=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Cdrglob=null;

	/**
	 * @return Returns the CDRGLOB.
	 */
	public Double getCdrglob() {
		try{
			if (_Cdrglob==null){
				_Cdrglob=getDoubleProperty("CDRGLOB");
				return _Cdrglob;
			}else {
				return _Cdrglob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CDRGLOB.
	 * @param v Value to Set.
	 */
	public void setCdrglob(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CDRGLOB",v);
		_Cdrglob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Comport=null;

	/**
	 * @return Returns the COMPORT.
	 */
	public Double getComport() {
		try{
			if (_Comport==null){
				_Comport=getDoubleProperty("COMPORT");
				return _Comport;
			}else {
				return _Comport;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for COMPORT.
	 * @param v Value to Set.
	 */
	public void setComport(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/COMPORT",v);
		_Comport=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Cdrlang=null;

	/**
	 * @return Returns the CDRLANG.
	 */
	public Double getCdrlang() {
		try{
			if (_Cdrlang==null){
				_Cdrlang=getDoubleProperty("CDRLANG");
				return _Cdrlang;
			}else {
				return _Cdrlang;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CDRLANG.
	 * @param v Value to Set.
	 */
	public void setCdrlang(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CDRLANG",v);
		_Cdrlang=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Cdrsupp=null;

	/**
	 * @return Returns the CDRSUPP.
	 */
	public Double getCdrsupp() {
		try{
			if (_Cdrsupp==null){
				_Cdrsupp=getDoubleProperty("CDRSUPP");
				return _Cdrsupp;
			}else {
				return _Cdrsupp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CDRSUPP.
	 * @param v Value to Set.
	 */
	public void setCdrsupp(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CDRSUPP",v);
		_Cdrsupp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Cdrtot=null;

	/**
	 * @return Returns the CDRTOT.
	 */
	public Double getCdrtot() {
		try{
			if (_Cdrtot==null){
				_Cdrtot=getDoubleProperty("CDRTOT");
				return _Cdrtot;
			}else {
				return _Cdrtot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CDRTOT.
	 * @param v Value to Set.
	 */
	public void setCdrtot(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CDRTOT",v);
		_Cdrtot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.UdsB4cdrdata> getAllUdsB4cdrdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsB4cdrdata> al = new ArrayList<org.nrg.xdat.om.UdsB4cdrdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsB4cdrdata> getUdsB4cdrdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsB4cdrdata> al = new ArrayList<org.nrg.xdat.om.UdsB4cdrdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsB4cdrdata> getUdsB4cdrdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsB4cdrdata> al = new ArrayList<org.nrg.xdat.om.UdsB4cdrdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static UdsB4cdrdata getUdsB4cdrdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("uds:b4cdrData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (UdsB4cdrdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
