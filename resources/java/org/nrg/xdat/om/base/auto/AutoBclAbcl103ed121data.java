/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoBclAbcl103ed121data extends XnatSubjectassessordata implements org.nrg.xdat.model.BclAbcl103ed121dataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoBclAbcl103ed121data.class);
	public static String SCHEMA_ELEMENT_NAME="bcl:abcl1-03Ed121Data";

	public AutoBclAbcl103ed121data(ItemI item)
	{
		super(item);
	}

	public AutoBclAbcl103ed121data(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoBclAbcl103ed121data(UserI user)
	 **/
	public AutoBclAbcl103ed121data(){}

	public AutoBclAbcl103ed121data(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "bcl:abcl1-03Ed121Data";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _AnxdpRaw=null;

	/**
	 * @return Returns the anxdp_raw.
	 */
	public Integer getAnxdpRaw() {
		try{
			if (_AnxdpRaw==null){
				_AnxdpRaw=getIntegerProperty("anxdp_raw");
				return _AnxdpRaw;
			}else {
				return _AnxdpRaw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for anxdp_raw.
	 * @param v Value to Set.
	 */
	public void setAnxdpRaw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/anxdp_raw",v);
		_AnxdpRaw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _AnxdpT=null;

	/**
	 * @return Returns the anxdp_t.
	 */
	public Integer getAnxdpT() {
		try{
			if (_AnxdpT==null){
				_AnxdpT=getIntegerProperty("anxdp_t");
				return _AnxdpT;
			}else {
				return _AnxdpT;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for anxdp_t.
	 * @param v Value to Set.
	 */
	public void setAnxdpT(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/anxdp_t",v);
		_AnxdpT=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _AnxdpNote=null;

	/**
	 * @return Returns the anxdp_note.
	 */
	public String getAnxdpNote(){
		try{
			if (_AnxdpNote==null){
				_AnxdpNote=getStringProperty("anxdp_note");
				return _AnxdpNote;
			}else {
				return _AnxdpNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for anxdp_note.
	 * @param v Value to Set.
	 */
	public void setAnxdpNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/anxdp_note",v);
		_AnxdpNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _WthdpRaw=null;

	/**
	 * @return Returns the wthdp_raw.
	 */
	public Integer getWthdpRaw() {
		try{
			if (_WthdpRaw==null){
				_WthdpRaw=getIntegerProperty("wthdp_raw");
				return _WthdpRaw;
			}else {
				return _WthdpRaw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wthdp_raw.
	 * @param v Value to Set.
	 */
	public void setWthdpRaw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wthdp_raw",v);
		_WthdpRaw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _WthdpT=null;

	/**
	 * @return Returns the wthdp_t.
	 */
	public Integer getWthdpT() {
		try{
			if (_WthdpT==null){
				_WthdpT=getIntegerProperty("wthdp_t");
				return _WthdpT;
			}else {
				return _WthdpT;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wthdp_t.
	 * @param v Value to Set.
	 */
	public void setWthdpT(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wthdp_t",v);
		_WthdpT=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _WthdpNote=null;

	/**
	 * @return Returns the wthdp_note.
	 */
	public String getWthdpNote(){
		try{
			if (_WthdpNote==null){
				_WthdpNote=getStringProperty("wthdp_note");
				return _WthdpNote;
			}else {
				return _WthdpNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wthdp_note.
	 * @param v Value to Set.
	 */
	public void setWthdpNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wthdp_note",v);
		_WthdpNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _SomRaw=null;

	/**
	 * @return Returns the som_raw.
	 */
	public Integer getSomRaw() {
		try{
			if (_SomRaw==null){
				_SomRaw=getIntegerProperty("som_raw");
				return _SomRaw;
			}else {
				return _SomRaw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for som_raw.
	 * @param v Value to Set.
	 */
	public void setSomRaw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/som_raw",v);
		_SomRaw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _SomT=null;

	/**
	 * @return Returns the som_t.
	 */
	public Integer getSomT() {
		try{
			if (_SomT==null){
				_SomT=getIntegerProperty("som_t");
				return _SomT;
			}else {
				return _SomT;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for som_t.
	 * @param v Value to Set.
	 */
	public void setSomT(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/som_t",v);
		_SomT=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _SomNote=null;

	/**
	 * @return Returns the som_note.
	 */
	public String getSomNote(){
		try{
			if (_SomNote==null){
				_SomNote=getStringProperty("som_note");
				return _SomNote;
			}else {
				return _SomNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for som_note.
	 * @param v Value to Set.
	 */
	public void setSomNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/som_note",v);
		_SomNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _ThoRaw=null;

	/**
	 * @return Returns the tho_raw.
	 */
	public Integer getThoRaw() {
		try{
			if (_ThoRaw==null){
				_ThoRaw=getIntegerProperty("tho_raw");
				return _ThoRaw;
			}else {
				return _ThoRaw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tho_raw.
	 * @param v Value to Set.
	 */
	public void setThoRaw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tho_raw",v);
		_ThoRaw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _ThoT=null;

	/**
	 * @return Returns the tho_t.
	 */
	public Integer getThoT() {
		try{
			if (_ThoT==null){
				_ThoT=getIntegerProperty("tho_t");
				return _ThoT;
			}else {
				return _ThoT;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tho_t.
	 * @param v Value to Set.
	 */
	public void setThoT(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tho_t",v);
		_ThoT=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _ThoNote=null;

	/**
	 * @return Returns the tho_note.
	 */
	public String getThoNote(){
		try{
			if (_ThoNote==null){
				_ThoNote=getStringProperty("tho_note");
				return _ThoNote;
			}else {
				return _ThoNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tho_note.
	 * @param v Value to Set.
	 */
	public void setThoNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tho_note",v);
		_ThoNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _AttRaw=null;

	/**
	 * @return Returns the att_raw.
	 */
	public Integer getAttRaw() {
		try{
			if (_AttRaw==null){
				_AttRaw=getIntegerProperty("att_raw");
				return _AttRaw;
			}else {
				return _AttRaw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for att_raw.
	 * @param v Value to Set.
	 */
	public void setAttRaw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/att_raw",v);
		_AttRaw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _AttT=null;

	/**
	 * @return Returns the att_t.
	 */
	public Integer getAttT() {
		try{
			if (_AttT==null){
				_AttT=getIntegerProperty("att_t");
				return _AttT;
			}else {
				return _AttT;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for att_t.
	 * @param v Value to Set.
	 */
	public void setAttT(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/att_t",v);
		_AttT=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _AttNote=null;

	/**
	 * @return Returns the att_note.
	 */
	public String getAttNote(){
		try{
			if (_AttNote==null){
				_AttNote=getStringProperty("att_note");
				return _AttNote;
			}else {
				return _AttNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for att_note.
	 * @param v Value to Set.
	 */
	public void setAttNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/att_note",v);
		_AttNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _RuleRaw=null;

	/**
	 * @return Returns the rule_raw.
	 */
	public Integer getRuleRaw() {
		try{
			if (_RuleRaw==null){
				_RuleRaw=getIntegerProperty("rule_raw");
				return _RuleRaw;
			}else {
				return _RuleRaw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for rule_raw.
	 * @param v Value to Set.
	 */
	public void setRuleRaw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/rule_raw",v);
		_RuleRaw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _RuleT=null;

	/**
	 * @return Returns the rule_t.
	 */
	public Integer getRuleT() {
		try{
			if (_RuleT==null){
				_RuleT=getIntegerProperty("rule_t");
				return _RuleT;
			}else {
				return _RuleT;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for rule_t.
	 * @param v Value to Set.
	 */
	public void setRuleT(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/rule_t",v);
		_RuleT=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _RuleNote=null;

	/**
	 * @return Returns the rule_note.
	 */
	public String getRuleNote(){
		try{
			if (_RuleNote==null){
				_RuleNote=getStringProperty("rule_note");
				return _RuleNote;
			}else {
				return _RuleNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for rule_note.
	 * @param v Value to Set.
	 */
	public void setRuleNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/rule_note",v);
		_RuleNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _AggRaw=null;

	/**
	 * @return Returns the agg_raw.
	 */
	public Integer getAggRaw() {
		try{
			if (_AggRaw==null){
				_AggRaw=getIntegerProperty("agg_raw");
				return _AggRaw;
			}else {
				return _AggRaw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for agg_raw.
	 * @param v Value to Set.
	 */
	public void setAggRaw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/agg_raw",v);
		_AggRaw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _AggT=null;

	/**
	 * @return Returns the agg_t.
	 */
	public Integer getAggT() {
		try{
			if (_AggT==null){
				_AggT=getIntegerProperty("agg_t");
				return _AggT;
			}else {
				return _AggT;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for agg_t.
	 * @param v Value to Set.
	 */
	public void setAggT(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/agg_t",v);
		_AggT=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _AggNote=null;

	/**
	 * @return Returns the agg_note.
	 */
	public String getAggNote(){
		try{
			if (_AggNote==null){
				_AggNote=getStringProperty("agg_note");
				return _AggNote;
			}else {
				return _AggNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for agg_note.
	 * @param v Value to Set.
	 */
	public void setAggNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/agg_note",v);
		_AggNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _IntRaw=null;

	/**
	 * @return Returns the int_raw.
	 */
	public Integer getIntRaw() {
		try{
			if (_IntRaw==null){
				_IntRaw=getIntegerProperty("int_raw");
				return _IntRaw;
			}else {
				return _IntRaw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for int_raw.
	 * @param v Value to Set.
	 */
	public void setIntRaw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/int_raw",v);
		_IntRaw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _IntT=null;

	/**
	 * @return Returns the int_t.
	 */
	public Integer getIntT() {
		try{
			if (_IntT==null){
				_IntT=getIntegerProperty("int_t");
				return _IntT;
			}else {
				return _IntT;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for int_t.
	 * @param v Value to Set.
	 */
	public void setIntT(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/int_t",v);
		_IntT=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _IntNote=null;

	/**
	 * @return Returns the int_note.
	 */
	public String getIntNote(){
		try{
			if (_IntNote==null){
				_IntNote=getStringProperty("int_note");
				return _IntNote;
			}else {
				return _IntNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for int_note.
	 * @param v Value to Set.
	 */
	public void setIntNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/int_note",v);
		_IntNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_resprltnsubj=null;

	/**
	 * @return Returns the ScoreForm/respRltnSubj.
	 */
	public String getScoreform_resprltnsubj(){
		try{
			if (_Scoreform_resprltnsubj==null){
				_Scoreform_resprltnsubj=getStringProperty("ScoreForm/respRltnSubj");
				return _Scoreform_resprltnsubj;
			}else {
				return _Scoreform_resprltnsubj;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/respRltnSubj.
	 * @param v Value to Set.
	 */
	public void setScoreform_resprltnsubj(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/respRltnSubj",v);
		_Scoreform_resprltnsubj=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectioni_friendsa=null;

	/**
	 * @return Returns the ScoreForm/sectionI/friendsA.
	 */
	public Integer getScoreform_sectioni_friendsa() {
		try{
			if (_Scoreform_sectioni_friendsa==null){
				_Scoreform_sectioni_friendsa=getIntegerProperty("ScoreForm/sectionI/friendsA");
				return _Scoreform_sectioni_friendsa;
			}else {
				return _Scoreform_sectioni_friendsa;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionI/friendsA.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioni_friendsa(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionI/friendsA",v);
		_Scoreform_sectioni_friendsa=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectioni_friendsb=null;

	/**
	 * @return Returns the ScoreForm/sectionI/friendsB.
	 */
	public Integer getScoreform_sectioni_friendsb() {
		try{
			if (_Scoreform_sectioni_friendsb==null){
				_Scoreform_sectioni_friendsb=getIntegerProperty("ScoreForm/sectionI/friendsB");
				return _Scoreform_sectioni_friendsb;
			}else {
				return _Scoreform_sectioni_friendsb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionI/friendsB.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioni_friendsb(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionI/friendsB",v);
		_Scoreform_sectioni_friendsb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectioni_friendsc=null;

	/**
	 * @return Returns the ScoreForm/sectionI/friendsC.
	 */
	public Integer getScoreform_sectioni_friendsc() {
		try{
			if (_Scoreform_sectioni_friendsc==null){
				_Scoreform_sectioni_friendsc=getIntegerProperty("ScoreForm/sectionI/friendsC");
				return _Scoreform_sectioni_friendsc;
			}else {
				return _Scoreform_sectioni_friendsc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionI/friendsC.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioni_friendsc(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionI/friendsC",v);
		_Scoreform_sectioni_friendsc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectioni_friendsd=null;

	/**
	 * @return Returns the ScoreForm/sectionI/friendsD.
	 */
	public Integer getScoreform_sectioni_friendsd() {
		try{
			if (_Scoreform_sectioni_friendsd==null){
				_Scoreform_sectioni_friendsd=getIntegerProperty("ScoreForm/sectionI/friendsD");
				return _Scoreform_sectioni_friendsd;
			}else {
				return _Scoreform_sectioni_friendsd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionI/friendsD.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioni_friendsd(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionI/friendsD",v);
		_Scoreform_sectioni_friendsd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionii_maritalstatus=null;

	/**
	 * @return Returns the ScoreForm/sectionII/maritalStatus.
	 */
	public Integer getScoreform_sectionii_maritalstatus() {
		try{
			if (_Scoreform_sectionii_maritalstatus==null){
				_Scoreform_sectionii_maritalstatus=getIntegerProperty("ScoreForm/sectionII/maritalStatus");
				return _Scoreform_sectionii_maritalstatus;
			}else {
				return _Scoreform_sectionii_maritalstatus;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionII/maritalStatus.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionii_maritalstatus(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionII/maritalStatus",v);
		_Scoreform_sectionii_maritalstatus=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionii_maritalstatusother=null;

	/**
	 * @return Returns the ScoreForm/sectionII/maritalStatusOther.
	 */
	public String getScoreform_sectionii_maritalstatusother(){
		try{
			if (_Scoreform_sectionii_maritalstatusother==null){
				_Scoreform_sectionii_maritalstatusother=getStringProperty("ScoreForm/sectionII/maritalStatusOther");
				return _Scoreform_sectionii_maritalstatusother;
			}else {
				return _Scoreform_sectionii_maritalstatusother;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionII/maritalStatusOther.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionii_maritalstatusother(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionII/maritalStatusOther",v);
		_Scoreform_sectionii_maritalstatusother=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionii_maritallast6mnths=null;

	/**
	 * @return Returns the ScoreForm/sectionII/maritalLast6Mnths.
	 */
	public Integer getScoreform_sectionii_maritallast6mnths() {
		try{
			if (_Scoreform_sectionii_maritallast6mnths==null){
				_Scoreform_sectionii_maritallast6mnths=getIntegerProperty("ScoreForm/sectionII/maritalLast6Mnths");
				return _Scoreform_sectionii_maritallast6mnths;
			}else {
				return _Scoreform_sectionii_maritallast6mnths;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionII/maritalLast6Mnths.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionii_maritallast6mnths(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionII/maritalLast6Mnths",v);
		_Scoreform_sectionii_maritallast6mnths=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionii_maritala=null;

	/**
	 * @return Returns the ScoreForm/sectionII/maritalA.
	 */
	public Integer getScoreform_sectionii_maritala() {
		try{
			if (_Scoreform_sectionii_maritala==null){
				_Scoreform_sectionii_maritala=getIntegerProperty("ScoreForm/sectionII/maritalA");
				return _Scoreform_sectionii_maritala;
			}else {
				return _Scoreform_sectionii_maritala;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionII/maritalA.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionii_maritala(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionII/maritalA",v);
		_Scoreform_sectionii_maritala=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionii_maritalb=null;

	/**
	 * @return Returns the ScoreForm/sectionII/maritalB.
	 */
	public Integer getScoreform_sectionii_maritalb() {
		try{
			if (_Scoreform_sectionii_maritalb==null){
				_Scoreform_sectionii_maritalb=getIntegerProperty("ScoreForm/sectionII/maritalB");
				return _Scoreform_sectionii_maritalb;
			}else {
				return _Scoreform_sectionii_maritalb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionII/maritalB.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionii_maritalb(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionII/maritalB",v);
		_Scoreform_sectionii_maritalb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionii_maritalc=null;

	/**
	 * @return Returns the ScoreForm/sectionII/maritalC.
	 */
	public Integer getScoreform_sectionii_maritalc() {
		try{
			if (_Scoreform_sectionii_maritalc==null){
				_Scoreform_sectionii_maritalc=getIntegerProperty("ScoreForm/sectionII/maritalC");
				return _Scoreform_sectionii_maritalc;
			}else {
				return _Scoreform_sectionii_maritalc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionII/maritalC.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionii_maritalc(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionII/maritalC",v);
		_Scoreform_sectionii_maritalc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionii_maritald=null;

	/**
	 * @return Returns the ScoreForm/sectionII/maritalD.
	 */
	public Integer getScoreform_sectionii_maritald() {
		try{
			if (_Scoreform_sectionii_maritald==null){
				_Scoreform_sectionii_maritald=getIntegerProperty("ScoreForm/sectionII/maritalD");
				return _Scoreform_sectionii_maritald;
			}else {
				return _Scoreform_sectionii_maritald;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionII/maritalD.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionii_maritald(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionII/maritalD",v);
		_Scoreform_sectionii_maritald=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionii_maritale=null;

	/**
	 * @return Returns the ScoreForm/sectionII/maritalE.
	 */
	public Integer getScoreform_sectionii_maritale() {
		try{
			if (_Scoreform_sectionii_maritale==null){
				_Scoreform_sectionii_maritale=getIntegerProperty("ScoreForm/sectionII/maritalE");
				return _Scoreform_sectionii_maritale;
			}else {
				return _Scoreform_sectionii_maritale;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionII/maritalE.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionii_maritale(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionII/maritalE",v);
		_Scoreform_sectionii_maritale=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionii_maritalf=null;

	/**
	 * @return Returns the ScoreForm/sectionII/maritalF.
	 */
	public Integer getScoreform_sectionii_maritalf() {
		try{
			if (_Scoreform_sectionii_maritalf==null){
				_Scoreform_sectionii_maritalf=getIntegerProperty("ScoreForm/sectionII/maritalF");
				return _Scoreform_sectionii_maritalf;
			}else {
				return _Scoreform_sectionii_maritalf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionII/maritalF.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionii_maritalf(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionII/maritalF",v);
		_Scoreform_sectionii_maritalf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionii_maritalg=null;

	/**
	 * @return Returns the ScoreForm/sectionII/maritalG.
	 */
	public Integer getScoreform_sectionii_maritalg() {
		try{
			if (_Scoreform_sectionii_maritalg==null){
				_Scoreform_sectionii_maritalg=getIntegerProperty("ScoreForm/sectionII/maritalG");
				return _Scoreform_sectionii_maritalg;
			}else {
				return _Scoreform_sectionii_maritalg;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionII/maritalG.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionii_maritalg(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionII/maritalG",v);
		_Scoreform_sectionii_maritalg=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionii_maritalh=null;

	/**
	 * @return Returns the ScoreForm/sectionII/maritalH.
	 */
	public Integer getScoreform_sectionii_maritalh() {
		try{
			if (_Scoreform_sectionii_maritalh==null){
				_Scoreform_sectionii_maritalh=getIntegerProperty("ScoreForm/sectionII/maritalH");
				return _Scoreform_sectionii_maritalh;
			}else {
				return _Scoreform_sectionii_maritalh;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionII/maritalH.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionii_maritalh(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionII/maritalH",v);
		_Scoreform_sectionii_maritalh=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectioniii_handicap=null;

	/**
	 * @return Returns the ScoreForm/sectionIII/handicap.
	 */
	public Integer getScoreform_sectioniii_handicap() {
		try{
			if (_Scoreform_sectioniii_handicap==null){
				_Scoreform_sectioniii_handicap=getIntegerProperty("ScoreForm/sectionIII/handicap");
				return _Scoreform_sectioniii_handicap;
			}else {
				return _Scoreform_sectioniii_handicap;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionIII/handicap.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioniii_handicap(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionIII/handicap",v);
		_Scoreform_sectioniii_handicap=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectioniii_handicapexplain=null;

	/**
	 * @return Returns the ScoreForm/sectionIII/handicapExplain.
	 */
	public String getScoreform_sectioniii_handicapexplain(){
		try{
			if (_Scoreform_sectioniii_handicapexplain==null){
				_Scoreform_sectioniii_handicapexplain=getStringProperty("ScoreForm/sectionIII/handicapExplain");
				return _Scoreform_sectioniii_handicapexplain;
			}else {
				return _Scoreform_sectioniii_handicapexplain;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionIII/handicapExplain.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioniii_handicapexplain(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionIII/handicapExplain",v);
		_Scoreform_sectioniii_handicapexplain=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectioniv_concerns=null;

	/**
	 * @return Returns the ScoreForm/sectionIV/concerns.
	 */
	public String getScoreform_sectioniv_concerns(){
		try{
			if (_Scoreform_sectioniv_concerns==null){
				_Scoreform_sectioniv_concerns=getStringProperty("ScoreForm/sectionIV/concerns");
				return _Scoreform_sectioniv_concerns;
			}else {
				return _Scoreform_sectioniv_concerns;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionIV/concerns.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioniv_concerns(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionIV/concerns",v);
		_Scoreform_sectioniv_concerns=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionv_describe=null;

	/**
	 * @return Returns the ScoreForm/sectionV/describe.
	 */
	public String getScoreform_sectionv_describe(){
		try{
			if (_Scoreform_sectionv_describe==null){
				_Scoreform_sectionv_describe=getStringProperty("ScoreForm/sectionV/describe");
				return _Scoreform_sectionv_describe;
			}else {
				return _Scoreform_sectionv_describe;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionV/describe.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionv_describe(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionV/describe",v);
		_Scoreform_sectionv_describe=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question1=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question1.
	 */
	public Integer getScoreform_sectionvi_question1() {
		try{
			if (_Scoreform_sectionvi_question1==null){
				_Scoreform_sectionvi_question1=getIntegerProperty("ScoreForm/sectionVI/question1");
				return _Scoreform_sectionvi_question1;
			}else {
				return _Scoreform_sectionvi_question1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question1.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question1(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question1",v);
		_Scoreform_sectionvi_question1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question2=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question2.
	 */
	public Integer getScoreform_sectionvi_question2() {
		try{
			if (_Scoreform_sectionvi_question2==null){
				_Scoreform_sectionvi_question2=getIntegerProperty("ScoreForm/sectionVI/question2");
				return _Scoreform_sectionvi_question2;
			}else {
				return _Scoreform_sectionvi_question2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question2.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question2(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question2",v);
		_Scoreform_sectionvi_question2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question3=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question3.
	 */
	public Integer getScoreform_sectionvi_question3() {
		try{
			if (_Scoreform_sectionvi_question3==null){
				_Scoreform_sectionvi_question3=getIntegerProperty("ScoreForm/sectionVI/question3");
				return _Scoreform_sectionvi_question3;
			}else {
				return _Scoreform_sectionvi_question3;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question3.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question3(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question3",v);
		_Scoreform_sectionvi_question3=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question4=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question4.
	 */
	public Integer getScoreform_sectionvi_question4() {
		try{
			if (_Scoreform_sectionvi_question4==null){
				_Scoreform_sectionvi_question4=getIntegerProperty("ScoreForm/sectionVI/question4");
				return _Scoreform_sectionvi_question4;
			}else {
				return _Scoreform_sectionvi_question4;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question4.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question4(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question4",v);
		_Scoreform_sectionvi_question4=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question5=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question5.
	 */
	public Integer getScoreform_sectionvi_question5() {
		try{
			if (_Scoreform_sectionvi_question5==null){
				_Scoreform_sectionvi_question5=getIntegerProperty("ScoreForm/sectionVI/question5");
				return _Scoreform_sectionvi_question5;
			}else {
				return _Scoreform_sectionvi_question5;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question5.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question5(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question5",v);
		_Scoreform_sectionvi_question5=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question6=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question6.
	 */
	public Integer getScoreform_sectionvi_question6() {
		try{
			if (_Scoreform_sectionvi_question6==null){
				_Scoreform_sectionvi_question6=getIntegerProperty("ScoreForm/sectionVI/question6");
				return _Scoreform_sectionvi_question6;
			}else {
				return _Scoreform_sectionvi_question6;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question6.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question6(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question6",v);
		_Scoreform_sectionvi_question6=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionvi_question6desc=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question6Desc.
	 */
	public String getScoreform_sectionvi_question6desc(){
		try{
			if (_Scoreform_sectionvi_question6desc==null){
				_Scoreform_sectionvi_question6desc=getStringProperty("ScoreForm/sectionVI/question6Desc");
				return _Scoreform_sectionvi_question6desc;
			}else {
				return _Scoreform_sectionvi_question6desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question6Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question6desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question6Desc",v);
		_Scoreform_sectionvi_question6desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question7=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question7.
	 */
	public Integer getScoreform_sectionvi_question7() {
		try{
			if (_Scoreform_sectionvi_question7==null){
				_Scoreform_sectionvi_question7=getIntegerProperty("ScoreForm/sectionVI/question7");
				return _Scoreform_sectionvi_question7;
			}else {
				return _Scoreform_sectionvi_question7;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question7.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question7(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question7",v);
		_Scoreform_sectionvi_question7=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question8=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question8.
	 */
	public Integer getScoreform_sectionvi_question8() {
		try{
			if (_Scoreform_sectionvi_question8==null){
				_Scoreform_sectionvi_question8=getIntegerProperty("ScoreForm/sectionVI/question8");
				return _Scoreform_sectionvi_question8;
			}else {
				return _Scoreform_sectionvi_question8;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question8.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question8(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question8",v);
		_Scoreform_sectionvi_question8=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question9=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question9.
	 */
	public Integer getScoreform_sectionvi_question9() {
		try{
			if (_Scoreform_sectionvi_question9==null){
				_Scoreform_sectionvi_question9=getIntegerProperty("ScoreForm/sectionVI/question9");
				return _Scoreform_sectionvi_question9;
			}else {
				return _Scoreform_sectionvi_question9;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question9.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question9(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question9",v);
		_Scoreform_sectionvi_question9=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionvi_question9desc=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question9Desc.
	 */
	public String getScoreform_sectionvi_question9desc(){
		try{
			if (_Scoreform_sectionvi_question9desc==null){
				_Scoreform_sectionvi_question9desc=getStringProperty("ScoreForm/sectionVI/question9Desc");
				return _Scoreform_sectionvi_question9desc;
			}else {
				return _Scoreform_sectionvi_question9desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question9Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question9desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question9Desc",v);
		_Scoreform_sectionvi_question9desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question10=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question10.
	 */
	public Integer getScoreform_sectionvi_question10() {
		try{
			if (_Scoreform_sectionvi_question10==null){
				_Scoreform_sectionvi_question10=getIntegerProperty("ScoreForm/sectionVI/question10");
				return _Scoreform_sectionvi_question10;
			}else {
				return _Scoreform_sectionvi_question10;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question10.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question10(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question10",v);
		_Scoreform_sectionvi_question10=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question11=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question11.
	 */
	public Integer getScoreform_sectionvi_question11() {
		try{
			if (_Scoreform_sectionvi_question11==null){
				_Scoreform_sectionvi_question11=getIntegerProperty("ScoreForm/sectionVI/question11");
				return _Scoreform_sectionvi_question11;
			}else {
				return _Scoreform_sectionvi_question11;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question11.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question11(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question11",v);
		_Scoreform_sectionvi_question11=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question12=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question12.
	 */
	public Integer getScoreform_sectionvi_question12() {
		try{
			if (_Scoreform_sectionvi_question12==null){
				_Scoreform_sectionvi_question12=getIntegerProperty("ScoreForm/sectionVI/question12");
				return _Scoreform_sectionvi_question12;
			}else {
				return _Scoreform_sectionvi_question12;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question12.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question12(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question12",v);
		_Scoreform_sectionvi_question12=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question13=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question13.
	 */
	public Integer getScoreform_sectionvi_question13() {
		try{
			if (_Scoreform_sectionvi_question13==null){
				_Scoreform_sectionvi_question13=getIntegerProperty("ScoreForm/sectionVI/question13");
				return _Scoreform_sectionvi_question13;
			}else {
				return _Scoreform_sectionvi_question13;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question13.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question13(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question13",v);
		_Scoreform_sectionvi_question13=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question14=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question14.
	 */
	public Integer getScoreform_sectionvi_question14() {
		try{
			if (_Scoreform_sectionvi_question14==null){
				_Scoreform_sectionvi_question14=getIntegerProperty("ScoreForm/sectionVI/question14");
				return _Scoreform_sectionvi_question14;
			}else {
				return _Scoreform_sectionvi_question14;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question14.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question14(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question14",v);
		_Scoreform_sectionvi_question14=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question15=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question15.
	 */
	public Integer getScoreform_sectionvi_question15() {
		try{
			if (_Scoreform_sectionvi_question15==null){
				_Scoreform_sectionvi_question15=getIntegerProperty("ScoreForm/sectionVI/question15");
				return _Scoreform_sectionvi_question15;
			}else {
				return _Scoreform_sectionvi_question15;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question15.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question15(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question15",v);
		_Scoreform_sectionvi_question15=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question16=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question16.
	 */
	public Integer getScoreform_sectionvi_question16() {
		try{
			if (_Scoreform_sectionvi_question16==null){
				_Scoreform_sectionvi_question16=getIntegerProperty("ScoreForm/sectionVI/question16");
				return _Scoreform_sectionvi_question16;
			}else {
				return _Scoreform_sectionvi_question16;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question16.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question16(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question16",v);
		_Scoreform_sectionvi_question16=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question17=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question17.
	 */
	public Integer getScoreform_sectionvi_question17() {
		try{
			if (_Scoreform_sectionvi_question17==null){
				_Scoreform_sectionvi_question17=getIntegerProperty("ScoreForm/sectionVI/question17");
				return _Scoreform_sectionvi_question17;
			}else {
				return _Scoreform_sectionvi_question17;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question17.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question17(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question17",v);
		_Scoreform_sectionvi_question17=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question18=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question18.
	 */
	public Integer getScoreform_sectionvi_question18() {
		try{
			if (_Scoreform_sectionvi_question18==null){
				_Scoreform_sectionvi_question18=getIntegerProperty("ScoreForm/sectionVI/question18");
				return _Scoreform_sectionvi_question18;
			}else {
				return _Scoreform_sectionvi_question18;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question18.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question18(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question18",v);
		_Scoreform_sectionvi_question18=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question19=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question19.
	 */
	public Integer getScoreform_sectionvi_question19() {
		try{
			if (_Scoreform_sectionvi_question19==null){
				_Scoreform_sectionvi_question19=getIntegerProperty("ScoreForm/sectionVI/question19");
				return _Scoreform_sectionvi_question19;
			}else {
				return _Scoreform_sectionvi_question19;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question19.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question19(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question19",v);
		_Scoreform_sectionvi_question19=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question20=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question20.
	 */
	public Integer getScoreform_sectionvi_question20() {
		try{
			if (_Scoreform_sectionvi_question20==null){
				_Scoreform_sectionvi_question20=getIntegerProperty("ScoreForm/sectionVI/question20");
				return _Scoreform_sectionvi_question20;
			}else {
				return _Scoreform_sectionvi_question20;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question20.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question20(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question20",v);
		_Scoreform_sectionvi_question20=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question21=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question21.
	 */
	public Integer getScoreform_sectionvi_question21() {
		try{
			if (_Scoreform_sectionvi_question21==null){
				_Scoreform_sectionvi_question21=getIntegerProperty("ScoreForm/sectionVI/question21");
				return _Scoreform_sectionvi_question21;
			}else {
				return _Scoreform_sectionvi_question21;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question21.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question21(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question21",v);
		_Scoreform_sectionvi_question21=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question22=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question22.
	 */
	public Integer getScoreform_sectionvi_question22() {
		try{
			if (_Scoreform_sectionvi_question22==null){
				_Scoreform_sectionvi_question22=getIntegerProperty("ScoreForm/sectionVI/question22");
				return _Scoreform_sectionvi_question22;
			}else {
				return _Scoreform_sectionvi_question22;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question22.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question22(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question22",v);
		_Scoreform_sectionvi_question22=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question23=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question23.
	 */
	public Integer getScoreform_sectionvi_question23() {
		try{
			if (_Scoreform_sectionvi_question23==null){
				_Scoreform_sectionvi_question23=getIntegerProperty("ScoreForm/sectionVI/question23");
				return _Scoreform_sectionvi_question23;
			}else {
				return _Scoreform_sectionvi_question23;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question23.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question23(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question23",v);
		_Scoreform_sectionvi_question23=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question24=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question24.
	 */
	public Integer getScoreform_sectionvi_question24() {
		try{
			if (_Scoreform_sectionvi_question24==null){
				_Scoreform_sectionvi_question24=getIntegerProperty("ScoreForm/sectionVI/question24");
				return _Scoreform_sectionvi_question24;
			}else {
				return _Scoreform_sectionvi_question24;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question24.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question24(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question24",v);
		_Scoreform_sectionvi_question24=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question25=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question25.
	 */
	public Integer getScoreform_sectionvi_question25() {
		try{
			if (_Scoreform_sectionvi_question25==null){
				_Scoreform_sectionvi_question25=getIntegerProperty("ScoreForm/sectionVI/question25");
				return _Scoreform_sectionvi_question25;
			}else {
				return _Scoreform_sectionvi_question25;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question25.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question25(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question25",v);
		_Scoreform_sectionvi_question25=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question26=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question26.
	 */
	public Integer getScoreform_sectionvi_question26() {
		try{
			if (_Scoreform_sectionvi_question26==null){
				_Scoreform_sectionvi_question26=getIntegerProperty("ScoreForm/sectionVI/question26");
				return _Scoreform_sectionvi_question26;
			}else {
				return _Scoreform_sectionvi_question26;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question26.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question26(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question26",v);
		_Scoreform_sectionvi_question26=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question27=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question27.
	 */
	public Integer getScoreform_sectionvi_question27() {
		try{
			if (_Scoreform_sectionvi_question27==null){
				_Scoreform_sectionvi_question27=getIntegerProperty("ScoreForm/sectionVI/question27");
				return _Scoreform_sectionvi_question27;
			}else {
				return _Scoreform_sectionvi_question27;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question27.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question27(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question27",v);
		_Scoreform_sectionvi_question27=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question28=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question28.
	 */
	public Integer getScoreform_sectionvi_question28() {
		try{
			if (_Scoreform_sectionvi_question28==null){
				_Scoreform_sectionvi_question28=getIntegerProperty("ScoreForm/sectionVI/question28");
				return _Scoreform_sectionvi_question28;
			}else {
				return _Scoreform_sectionvi_question28;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question28.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question28(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question28",v);
		_Scoreform_sectionvi_question28=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question29=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question29.
	 */
	public Integer getScoreform_sectionvi_question29() {
		try{
			if (_Scoreform_sectionvi_question29==null){
				_Scoreform_sectionvi_question29=getIntegerProperty("ScoreForm/sectionVI/question29");
				return _Scoreform_sectionvi_question29;
			}else {
				return _Scoreform_sectionvi_question29;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question29.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question29(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question29",v);
		_Scoreform_sectionvi_question29=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionvi_question29desc=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question29Desc.
	 */
	public String getScoreform_sectionvi_question29desc(){
		try{
			if (_Scoreform_sectionvi_question29desc==null){
				_Scoreform_sectionvi_question29desc=getStringProperty("ScoreForm/sectionVI/question29Desc");
				return _Scoreform_sectionvi_question29desc;
			}else {
				return _Scoreform_sectionvi_question29desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question29Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question29desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question29Desc",v);
		_Scoreform_sectionvi_question29desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question30=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question30.
	 */
	public Integer getScoreform_sectionvi_question30() {
		try{
			if (_Scoreform_sectionvi_question30==null){
				_Scoreform_sectionvi_question30=getIntegerProperty("ScoreForm/sectionVI/question30");
				return _Scoreform_sectionvi_question30;
			}else {
				return _Scoreform_sectionvi_question30;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question30.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question30(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question30",v);
		_Scoreform_sectionvi_question30=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question31=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question31.
	 */
	public Integer getScoreform_sectionvi_question31() {
		try{
			if (_Scoreform_sectionvi_question31==null){
				_Scoreform_sectionvi_question31=getIntegerProperty("ScoreForm/sectionVI/question31");
				return _Scoreform_sectionvi_question31;
			}else {
				return _Scoreform_sectionvi_question31;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question31.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question31(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question31",v);
		_Scoreform_sectionvi_question31=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question32=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question32.
	 */
	public Integer getScoreform_sectionvi_question32() {
		try{
			if (_Scoreform_sectionvi_question32==null){
				_Scoreform_sectionvi_question32=getIntegerProperty("ScoreForm/sectionVI/question32");
				return _Scoreform_sectionvi_question32;
			}else {
				return _Scoreform_sectionvi_question32;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question32.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question32(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question32",v);
		_Scoreform_sectionvi_question32=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question33=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question33.
	 */
	public Integer getScoreform_sectionvi_question33() {
		try{
			if (_Scoreform_sectionvi_question33==null){
				_Scoreform_sectionvi_question33=getIntegerProperty("ScoreForm/sectionVI/question33");
				return _Scoreform_sectionvi_question33;
			}else {
				return _Scoreform_sectionvi_question33;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question33.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question33(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question33",v);
		_Scoreform_sectionvi_question33=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question34=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question34.
	 */
	public Integer getScoreform_sectionvi_question34() {
		try{
			if (_Scoreform_sectionvi_question34==null){
				_Scoreform_sectionvi_question34=getIntegerProperty("ScoreForm/sectionVI/question34");
				return _Scoreform_sectionvi_question34;
			}else {
				return _Scoreform_sectionvi_question34;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question34.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question34(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question34",v);
		_Scoreform_sectionvi_question34=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question35=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question35.
	 */
	public Integer getScoreform_sectionvi_question35() {
		try{
			if (_Scoreform_sectionvi_question35==null){
				_Scoreform_sectionvi_question35=getIntegerProperty("ScoreForm/sectionVI/question35");
				return _Scoreform_sectionvi_question35;
			}else {
				return _Scoreform_sectionvi_question35;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question35.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question35(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question35",v);
		_Scoreform_sectionvi_question35=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question36=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question36.
	 */
	public Integer getScoreform_sectionvi_question36() {
		try{
			if (_Scoreform_sectionvi_question36==null){
				_Scoreform_sectionvi_question36=getIntegerProperty("ScoreForm/sectionVI/question36");
				return _Scoreform_sectionvi_question36;
			}else {
				return _Scoreform_sectionvi_question36;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question36.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question36(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question36",v);
		_Scoreform_sectionvi_question36=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question37=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question37.
	 */
	public Integer getScoreform_sectionvi_question37() {
		try{
			if (_Scoreform_sectionvi_question37==null){
				_Scoreform_sectionvi_question37=getIntegerProperty("ScoreForm/sectionVI/question37");
				return _Scoreform_sectionvi_question37;
			}else {
				return _Scoreform_sectionvi_question37;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question37.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question37(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question37",v);
		_Scoreform_sectionvi_question37=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question38=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question38.
	 */
	public Integer getScoreform_sectionvi_question38() {
		try{
			if (_Scoreform_sectionvi_question38==null){
				_Scoreform_sectionvi_question38=getIntegerProperty("ScoreForm/sectionVI/question38");
				return _Scoreform_sectionvi_question38;
			}else {
				return _Scoreform_sectionvi_question38;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question38.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question38(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question38",v);
		_Scoreform_sectionvi_question38=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question39=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question39.
	 */
	public Integer getScoreform_sectionvi_question39() {
		try{
			if (_Scoreform_sectionvi_question39==null){
				_Scoreform_sectionvi_question39=getIntegerProperty("ScoreForm/sectionVI/question39");
				return _Scoreform_sectionvi_question39;
			}else {
				return _Scoreform_sectionvi_question39;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question39.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question39(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question39",v);
		_Scoreform_sectionvi_question39=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question40=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question40.
	 */
	public Integer getScoreform_sectionvi_question40() {
		try{
			if (_Scoreform_sectionvi_question40==null){
				_Scoreform_sectionvi_question40=getIntegerProperty("ScoreForm/sectionVI/question40");
				return _Scoreform_sectionvi_question40;
			}else {
				return _Scoreform_sectionvi_question40;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question40.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question40(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question40",v);
		_Scoreform_sectionvi_question40=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionvi_question40desc=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question40Desc.
	 */
	public String getScoreform_sectionvi_question40desc(){
		try{
			if (_Scoreform_sectionvi_question40desc==null){
				_Scoreform_sectionvi_question40desc=getStringProperty("ScoreForm/sectionVI/question40Desc");
				return _Scoreform_sectionvi_question40desc;
			}else {
				return _Scoreform_sectionvi_question40desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question40Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question40desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question40Desc",v);
		_Scoreform_sectionvi_question40desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question41=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question41.
	 */
	public Integer getScoreform_sectionvi_question41() {
		try{
			if (_Scoreform_sectionvi_question41==null){
				_Scoreform_sectionvi_question41=getIntegerProperty("ScoreForm/sectionVI/question41");
				return _Scoreform_sectionvi_question41;
			}else {
				return _Scoreform_sectionvi_question41;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question41.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question41(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question41",v);
		_Scoreform_sectionvi_question41=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question42=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question42.
	 */
	public Integer getScoreform_sectionvi_question42() {
		try{
			if (_Scoreform_sectionvi_question42==null){
				_Scoreform_sectionvi_question42=getIntegerProperty("ScoreForm/sectionVI/question42");
				return _Scoreform_sectionvi_question42;
			}else {
				return _Scoreform_sectionvi_question42;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question42.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question42(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question42",v);
		_Scoreform_sectionvi_question42=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question43=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question43.
	 */
	public Integer getScoreform_sectionvi_question43() {
		try{
			if (_Scoreform_sectionvi_question43==null){
				_Scoreform_sectionvi_question43=getIntegerProperty("ScoreForm/sectionVI/question43");
				return _Scoreform_sectionvi_question43;
			}else {
				return _Scoreform_sectionvi_question43;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question43.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question43(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question43",v);
		_Scoreform_sectionvi_question43=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question44=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question44.
	 */
	public Integer getScoreform_sectionvi_question44() {
		try{
			if (_Scoreform_sectionvi_question44==null){
				_Scoreform_sectionvi_question44=getIntegerProperty("ScoreForm/sectionVI/question44");
				return _Scoreform_sectionvi_question44;
			}else {
				return _Scoreform_sectionvi_question44;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question44.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question44(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question44",v);
		_Scoreform_sectionvi_question44=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question45=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question45.
	 */
	public Integer getScoreform_sectionvi_question45() {
		try{
			if (_Scoreform_sectionvi_question45==null){
				_Scoreform_sectionvi_question45=getIntegerProperty("ScoreForm/sectionVI/question45");
				return _Scoreform_sectionvi_question45;
			}else {
				return _Scoreform_sectionvi_question45;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question45.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question45(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question45",v);
		_Scoreform_sectionvi_question45=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question46=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question46.
	 */
	public Integer getScoreform_sectionvi_question46() {
		try{
			if (_Scoreform_sectionvi_question46==null){
				_Scoreform_sectionvi_question46=getIntegerProperty("ScoreForm/sectionVI/question46");
				return _Scoreform_sectionvi_question46;
			}else {
				return _Scoreform_sectionvi_question46;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question46.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question46(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question46",v);
		_Scoreform_sectionvi_question46=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionvi_question46desc=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question46Desc.
	 */
	public String getScoreform_sectionvi_question46desc(){
		try{
			if (_Scoreform_sectionvi_question46desc==null){
				_Scoreform_sectionvi_question46desc=getStringProperty("ScoreForm/sectionVI/question46Desc");
				return _Scoreform_sectionvi_question46desc;
			}else {
				return _Scoreform_sectionvi_question46desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question46Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question46desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question46Desc",v);
		_Scoreform_sectionvi_question46desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question47=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question47.
	 */
	public Integer getScoreform_sectionvi_question47() {
		try{
			if (_Scoreform_sectionvi_question47==null){
				_Scoreform_sectionvi_question47=getIntegerProperty("ScoreForm/sectionVI/question47");
				return _Scoreform_sectionvi_question47;
			}else {
				return _Scoreform_sectionvi_question47;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question47.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question47(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question47",v);
		_Scoreform_sectionvi_question47=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question48=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question48.
	 */
	public Integer getScoreform_sectionvi_question48() {
		try{
			if (_Scoreform_sectionvi_question48==null){
				_Scoreform_sectionvi_question48=getIntegerProperty("ScoreForm/sectionVI/question48");
				return _Scoreform_sectionvi_question48;
			}else {
				return _Scoreform_sectionvi_question48;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question48.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question48(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question48",v);
		_Scoreform_sectionvi_question48=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question49=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question49.
	 */
	public Integer getScoreform_sectionvi_question49() {
		try{
			if (_Scoreform_sectionvi_question49==null){
				_Scoreform_sectionvi_question49=getIntegerProperty("ScoreForm/sectionVI/question49");
				return _Scoreform_sectionvi_question49;
			}else {
				return _Scoreform_sectionvi_question49;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question49.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question49(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question49",v);
		_Scoreform_sectionvi_question49=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question50=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question50.
	 */
	public Integer getScoreform_sectionvi_question50() {
		try{
			if (_Scoreform_sectionvi_question50==null){
				_Scoreform_sectionvi_question50=getIntegerProperty("ScoreForm/sectionVI/question50");
				return _Scoreform_sectionvi_question50;
			}else {
				return _Scoreform_sectionvi_question50;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question50.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question50(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question50",v);
		_Scoreform_sectionvi_question50=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question51=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question51.
	 */
	public Integer getScoreform_sectionvi_question51() {
		try{
			if (_Scoreform_sectionvi_question51==null){
				_Scoreform_sectionvi_question51=getIntegerProperty("ScoreForm/sectionVI/question51");
				return _Scoreform_sectionvi_question51;
			}else {
				return _Scoreform_sectionvi_question51;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question51.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question51(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question51",v);
		_Scoreform_sectionvi_question51=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question52=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question52.
	 */
	public Integer getScoreform_sectionvi_question52() {
		try{
			if (_Scoreform_sectionvi_question52==null){
				_Scoreform_sectionvi_question52=getIntegerProperty("ScoreForm/sectionVI/question52");
				return _Scoreform_sectionvi_question52;
			}else {
				return _Scoreform_sectionvi_question52;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question52.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question52(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question52",v);
		_Scoreform_sectionvi_question52=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question53=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question53.
	 */
	public Integer getScoreform_sectionvi_question53() {
		try{
			if (_Scoreform_sectionvi_question53==null){
				_Scoreform_sectionvi_question53=getIntegerProperty("ScoreForm/sectionVI/question53");
				return _Scoreform_sectionvi_question53;
			}else {
				return _Scoreform_sectionvi_question53;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question53.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question53(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question53",v);
		_Scoreform_sectionvi_question53=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question54=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question54.
	 */
	public Integer getScoreform_sectionvi_question54() {
		try{
			if (_Scoreform_sectionvi_question54==null){
				_Scoreform_sectionvi_question54=getIntegerProperty("ScoreForm/sectionVI/question54");
				return _Scoreform_sectionvi_question54;
			}else {
				return _Scoreform_sectionvi_question54;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question54.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question54(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question54",v);
		_Scoreform_sectionvi_question54=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question55=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question55.
	 */
	public Integer getScoreform_sectionvi_question55() {
		try{
			if (_Scoreform_sectionvi_question55==null){
				_Scoreform_sectionvi_question55=getIntegerProperty("ScoreForm/sectionVI/question55");
				return _Scoreform_sectionvi_question55;
			}else {
				return _Scoreform_sectionvi_question55;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question55.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question55(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question55",v);
		_Scoreform_sectionvi_question55=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question56a=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question56a.
	 */
	public Integer getScoreform_sectionvi_question56a() {
		try{
			if (_Scoreform_sectionvi_question56a==null){
				_Scoreform_sectionvi_question56a=getIntegerProperty("ScoreForm/sectionVI/question56a");
				return _Scoreform_sectionvi_question56a;
			}else {
				return _Scoreform_sectionvi_question56a;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question56a.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question56a(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question56a",v);
		_Scoreform_sectionvi_question56a=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question56b=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question56b.
	 */
	public Integer getScoreform_sectionvi_question56b() {
		try{
			if (_Scoreform_sectionvi_question56b==null){
				_Scoreform_sectionvi_question56b=getIntegerProperty("ScoreForm/sectionVI/question56b");
				return _Scoreform_sectionvi_question56b;
			}else {
				return _Scoreform_sectionvi_question56b;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question56b.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question56b(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question56b",v);
		_Scoreform_sectionvi_question56b=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question56c=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question56c.
	 */
	public Integer getScoreform_sectionvi_question56c() {
		try{
			if (_Scoreform_sectionvi_question56c==null){
				_Scoreform_sectionvi_question56c=getIntegerProperty("ScoreForm/sectionVI/question56c");
				return _Scoreform_sectionvi_question56c;
			}else {
				return _Scoreform_sectionvi_question56c;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question56c.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question56c(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question56c",v);
		_Scoreform_sectionvi_question56c=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question56d=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question56d.
	 */
	public Integer getScoreform_sectionvi_question56d() {
		try{
			if (_Scoreform_sectionvi_question56d==null){
				_Scoreform_sectionvi_question56d=getIntegerProperty("ScoreForm/sectionVI/question56d");
				return _Scoreform_sectionvi_question56d;
			}else {
				return _Scoreform_sectionvi_question56d;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question56d.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question56d(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question56d",v);
		_Scoreform_sectionvi_question56d=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionvi_question56ddesc=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question56dDesc.
	 */
	public String getScoreform_sectionvi_question56ddesc(){
		try{
			if (_Scoreform_sectionvi_question56ddesc==null){
				_Scoreform_sectionvi_question56ddesc=getStringProperty("ScoreForm/sectionVI/question56dDesc");
				return _Scoreform_sectionvi_question56ddesc;
			}else {
				return _Scoreform_sectionvi_question56ddesc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question56dDesc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question56ddesc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question56dDesc",v);
		_Scoreform_sectionvi_question56ddesc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question56e=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question56e.
	 */
	public Integer getScoreform_sectionvi_question56e() {
		try{
			if (_Scoreform_sectionvi_question56e==null){
				_Scoreform_sectionvi_question56e=getIntegerProperty("ScoreForm/sectionVI/question56e");
				return _Scoreform_sectionvi_question56e;
			}else {
				return _Scoreform_sectionvi_question56e;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question56e.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question56e(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question56e",v);
		_Scoreform_sectionvi_question56e=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question56f=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question56f.
	 */
	public Integer getScoreform_sectionvi_question56f() {
		try{
			if (_Scoreform_sectionvi_question56f==null){
				_Scoreform_sectionvi_question56f=getIntegerProperty("ScoreForm/sectionVI/question56f");
				return _Scoreform_sectionvi_question56f;
			}else {
				return _Scoreform_sectionvi_question56f;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question56f.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question56f(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question56f",v);
		_Scoreform_sectionvi_question56f=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question56g=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question56g.
	 */
	public Integer getScoreform_sectionvi_question56g() {
		try{
			if (_Scoreform_sectionvi_question56g==null){
				_Scoreform_sectionvi_question56g=getIntegerProperty("ScoreForm/sectionVI/question56g");
				return _Scoreform_sectionvi_question56g;
			}else {
				return _Scoreform_sectionvi_question56g;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question56g.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question56g(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question56g",v);
		_Scoreform_sectionvi_question56g=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question57=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question57.
	 */
	public Integer getScoreform_sectionvi_question57() {
		try{
			if (_Scoreform_sectionvi_question57==null){
				_Scoreform_sectionvi_question57=getIntegerProperty("ScoreForm/sectionVI/question57");
				return _Scoreform_sectionvi_question57;
			}else {
				return _Scoreform_sectionvi_question57;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question57.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question57(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question57",v);
		_Scoreform_sectionvi_question57=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question58=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question58.
	 */
	public Integer getScoreform_sectionvi_question58() {
		try{
			if (_Scoreform_sectionvi_question58==null){
				_Scoreform_sectionvi_question58=getIntegerProperty("ScoreForm/sectionVI/question58");
				return _Scoreform_sectionvi_question58;
			}else {
				return _Scoreform_sectionvi_question58;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question58.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question58(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question58",v);
		_Scoreform_sectionvi_question58=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionvi_question58desc=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question58Desc.
	 */
	public String getScoreform_sectionvi_question58desc(){
		try{
			if (_Scoreform_sectionvi_question58desc==null){
				_Scoreform_sectionvi_question58desc=getStringProperty("ScoreForm/sectionVI/question58Desc");
				return _Scoreform_sectionvi_question58desc;
			}else {
				return _Scoreform_sectionvi_question58desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question58Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question58desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question58Desc",v);
		_Scoreform_sectionvi_question58desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question59=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question59.
	 */
	public Integer getScoreform_sectionvi_question59() {
		try{
			if (_Scoreform_sectionvi_question59==null){
				_Scoreform_sectionvi_question59=getIntegerProperty("ScoreForm/sectionVI/question59");
				return _Scoreform_sectionvi_question59;
			}else {
				return _Scoreform_sectionvi_question59;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question59.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question59(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question59",v);
		_Scoreform_sectionvi_question59=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question60=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question60.
	 */
	public Integer getScoreform_sectionvi_question60() {
		try{
			if (_Scoreform_sectionvi_question60==null){
				_Scoreform_sectionvi_question60=getIntegerProperty("ScoreForm/sectionVI/question60");
				return _Scoreform_sectionvi_question60;
			}else {
				return _Scoreform_sectionvi_question60;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question60.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question60(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question60",v);
		_Scoreform_sectionvi_question60=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question61=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question61.
	 */
	public Integer getScoreform_sectionvi_question61() {
		try{
			if (_Scoreform_sectionvi_question61==null){
				_Scoreform_sectionvi_question61=getIntegerProperty("ScoreForm/sectionVI/question61");
				return _Scoreform_sectionvi_question61;
			}else {
				return _Scoreform_sectionvi_question61;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question61.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question61(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question61",v);
		_Scoreform_sectionvi_question61=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question62=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question62.
	 */
	public Integer getScoreform_sectionvi_question62() {
		try{
			if (_Scoreform_sectionvi_question62==null){
				_Scoreform_sectionvi_question62=getIntegerProperty("ScoreForm/sectionVI/question62");
				return _Scoreform_sectionvi_question62;
			}else {
				return _Scoreform_sectionvi_question62;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question62.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question62(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question62",v);
		_Scoreform_sectionvi_question62=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question63=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question63.
	 */
	public Integer getScoreform_sectionvi_question63() {
		try{
			if (_Scoreform_sectionvi_question63==null){
				_Scoreform_sectionvi_question63=getIntegerProperty("ScoreForm/sectionVI/question63");
				return _Scoreform_sectionvi_question63;
			}else {
				return _Scoreform_sectionvi_question63;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question63.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question63(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question63",v);
		_Scoreform_sectionvi_question63=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question64=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question64.
	 */
	public Integer getScoreform_sectionvi_question64() {
		try{
			if (_Scoreform_sectionvi_question64==null){
				_Scoreform_sectionvi_question64=getIntegerProperty("ScoreForm/sectionVI/question64");
				return _Scoreform_sectionvi_question64;
			}else {
				return _Scoreform_sectionvi_question64;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question64.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question64(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question64",v);
		_Scoreform_sectionvi_question64=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question65=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question65.
	 */
	public Integer getScoreform_sectionvi_question65() {
		try{
			if (_Scoreform_sectionvi_question65==null){
				_Scoreform_sectionvi_question65=getIntegerProperty("ScoreForm/sectionVI/question65");
				return _Scoreform_sectionvi_question65;
			}else {
				return _Scoreform_sectionvi_question65;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question65.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question65(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question65",v);
		_Scoreform_sectionvi_question65=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question66=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question66.
	 */
	public Integer getScoreform_sectionvi_question66() {
		try{
			if (_Scoreform_sectionvi_question66==null){
				_Scoreform_sectionvi_question66=getIntegerProperty("ScoreForm/sectionVI/question66");
				return _Scoreform_sectionvi_question66;
			}else {
				return _Scoreform_sectionvi_question66;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question66.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question66(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question66",v);
		_Scoreform_sectionvi_question66=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionvi_question66desc=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question66Desc.
	 */
	public String getScoreform_sectionvi_question66desc(){
		try{
			if (_Scoreform_sectionvi_question66desc==null){
				_Scoreform_sectionvi_question66desc=getStringProperty("ScoreForm/sectionVI/question66Desc");
				return _Scoreform_sectionvi_question66desc;
			}else {
				return _Scoreform_sectionvi_question66desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question66Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question66desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question66Desc",v);
		_Scoreform_sectionvi_question66desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question67=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question67.
	 */
	public Integer getScoreform_sectionvi_question67() {
		try{
			if (_Scoreform_sectionvi_question67==null){
				_Scoreform_sectionvi_question67=getIntegerProperty("ScoreForm/sectionVI/question67");
				return _Scoreform_sectionvi_question67;
			}else {
				return _Scoreform_sectionvi_question67;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question67.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question67(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question67",v);
		_Scoreform_sectionvi_question67=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question68=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question68.
	 */
	public Integer getScoreform_sectionvi_question68() {
		try{
			if (_Scoreform_sectionvi_question68==null){
				_Scoreform_sectionvi_question68=getIntegerProperty("ScoreForm/sectionVI/question68");
				return _Scoreform_sectionvi_question68;
			}else {
				return _Scoreform_sectionvi_question68;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question68.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question68(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question68",v);
		_Scoreform_sectionvi_question68=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question69=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question69.
	 */
	public Integer getScoreform_sectionvi_question69() {
		try{
			if (_Scoreform_sectionvi_question69==null){
				_Scoreform_sectionvi_question69=getIntegerProperty("ScoreForm/sectionVI/question69");
				return _Scoreform_sectionvi_question69;
			}else {
				return _Scoreform_sectionvi_question69;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question69.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question69(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question69",v);
		_Scoreform_sectionvi_question69=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question70=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question70.
	 */
	public Integer getScoreform_sectionvi_question70() {
		try{
			if (_Scoreform_sectionvi_question70==null){
				_Scoreform_sectionvi_question70=getIntegerProperty("ScoreForm/sectionVI/question70");
				return _Scoreform_sectionvi_question70;
			}else {
				return _Scoreform_sectionvi_question70;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question70.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question70(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question70",v);
		_Scoreform_sectionvi_question70=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionvi_question70desc=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question70Desc.
	 */
	public String getScoreform_sectionvi_question70desc(){
		try{
			if (_Scoreform_sectionvi_question70desc==null){
				_Scoreform_sectionvi_question70desc=getStringProperty("ScoreForm/sectionVI/question70Desc");
				return _Scoreform_sectionvi_question70desc;
			}else {
				return _Scoreform_sectionvi_question70desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question70Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question70desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question70Desc",v);
		_Scoreform_sectionvi_question70desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question71=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question71.
	 */
	public Integer getScoreform_sectionvi_question71() {
		try{
			if (_Scoreform_sectionvi_question71==null){
				_Scoreform_sectionvi_question71=getIntegerProperty("ScoreForm/sectionVI/question71");
				return _Scoreform_sectionvi_question71;
			}else {
				return _Scoreform_sectionvi_question71;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question71.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question71(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question71",v);
		_Scoreform_sectionvi_question71=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question72=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question72.
	 */
	public Integer getScoreform_sectionvi_question72() {
		try{
			if (_Scoreform_sectionvi_question72==null){
				_Scoreform_sectionvi_question72=getIntegerProperty("ScoreForm/sectionVI/question72");
				return _Scoreform_sectionvi_question72;
			}else {
				return _Scoreform_sectionvi_question72;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question72.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question72(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question72",v);
		_Scoreform_sectionvi_question72=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question73=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question73.
	 */
	public Integer getScoreform_sectionvi_question73() {
		try{
			if (_Scoreform_sectionvi_question73==null){
				_Scoreform_sectionvi_question73=getIntegerProperty("ScoreForm/sectionVI/question73");
				return _Scoreform_sectionvi_question73;
			}else {
				return _Scoreform_sectionvi_question73;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question73.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question73(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question73",v);
		_Scoreform_sectionvi_question73=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question74=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question74.
	 */
	public Integer getScoreform_sectionvi_question74() {
		try{
			if (_Scoreform_sectionvi_question74==null){
				_Scoreform_sectionvi_question74=getIntegerProperty("ScoreForm/sectionVI/question74");
				return _Scoreform_sectionvi_question74;
			}else {
				return _Scoreform_sectionvi_question74;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question74.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question74(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question74",v);
		_Scoreform_sectionvi_question74=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question75=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question75.
	 */
	public Integer getScoreform_sectionvi_question75() {
		try{
			if (_Scoreform_sectionvi_question75==null){
				_Scoreform_sectionvi_question75=getIntegerProperty("ScoreForm/sectionVI/question75");
				return _Scoreform_sectionvi_question75;
			}else {
				return _Scoreform_sectionvi_question75;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question75.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question75(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question75",v);
		_Scoreform_sectionvi_question75=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question76=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question76.
	 */
	public Integer getScoreform_sectionvi_question76() {
		try{
			if (_Scoreform_sectionvi_question76==null){
				_Scoreform_sectionvi_question76=getIntegerProperty("ScoreForm/sectionVI/question76");
				return _Scoreform_sectionvi_question76;
			}else {
				return _Scoreform_sectionvi_question76;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question76.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question76(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question76",v);
		_Scoreform_sectionvi_question76=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question77=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question77.
	 */
	public Integer getScoreform_sectionvi_question77() {
		try{
			if (_Scoreform_sectionvi_question77==null){
				_Scoreform_sectionvi_question77=getIntegerProperty("ScoreForm/sectionVI/question77");
				return _Scoreform_sectionvi_question77;
			}else {
				return _Scoreform_sectionvi_question77;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question77.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question77(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question77",v);
		_Scoreform_sectionvi_question77=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionvi_question77desc=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question77Desc.
	 */
	public String getScoreform_sectionvi_question77desc(){
		try{
			if (_Scoreform_sectionvi_question77desc==null){
				_Scoreform_sectionvi_question77desc=getStringProperty("ScoreForm/sectionVI/question77Desc");
				return _Scoreform_sectionvi_question77desc;
			}else {
				return _Scoreform_sectionvi_question77desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question77Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question77desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question77Desc",v);
		_Scoreform_sectionvi_question77desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question78=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question78.
	 */
	public Integer getScoreform_sectionvi_question78() {
		try{
			if (_Scoreform_sectionvi_question78==null){
				_Scoreform_sectionvi_question78=getIntegerProperty("ScoreForm/sectionVI/question78");
				return _Scoreform_sectionvi_question78;
			}else {
				return _Scoreform_sectionvi_question78;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question78.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question78(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question78",v);
		_Scoreform_sectionvi_question78=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question79=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question79.
	 */
	public Integer getScoreform_sectionvi_question79() {
		try{
			if (_Scoreform_sectionvi_question79==null){
				_Scoreform_sectionvi_question79=getIntegerProperty("ScoreForm/sectionVI/question79");
				return _Scoreform_sectionvi_question79;
			}else {
				return _Scoreform_sectionvi_question79;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question79.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question79(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question79",v);
		_Scoreform_sectionvi_question79=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionvi_question79desc=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question79Desc.
	 */
	public String getScoreform_sectionvi_question79desc(){
		try{
			if (_Scoreform_sectionvi_question79desc==null){
				_Scoreform_sectionvi_question79desc=getStringProperty("ScoreForm/sectionVI/question79Desc");
				return _Scoreform_sectionvi_question79desc;
			}else {
				return _Scoreform_sectionvi_question79desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question79Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question79desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question79Desc",v);
		_Scoreform_sectionvi_question79desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question80=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question80.
	 */
	public Integer getScoreform_sectionvi_question80() {
		try{
			if (_Scoreform_sectionvi_question80==null){
				_Scoreform_sectionvi_question80=getIntegerProperty("ScoreForm/sectionVI/question80");
				return _Scoreform_sectionvi_question80;
			}else {
				return _Scoreform_sectionvi_question80;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question80.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question80(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question80",v);
		_Scoreform_sectionvi_question80=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question81=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question81.
	 */
	public Integer getScoreform_sectionvi_question81() {
		try{
			if (_Scoreform_sectionvi_question81==null){
				_Scoreform_sectionvi_question81=getIntegerProperty("ScoreForm/sectionVI/question81");
				return _Scoreform_sectionvi_question81;
			}else {
				return _Scoreform_sectionvi_question81;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question81.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question81(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question81",v);
		_Scoreform_sectionvi_question81=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question82=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question82.
	 */
	public Integer getScoreform_sectionvi_question82() {
		try{
			if (_Scoreform_sectionvi_question82==null){
				_Scoreform_sectionvi_question82=getIntegerProperty("ScoreForm/sectionVI/question82");
				return _Scoreform_sectionvi_question82;
			}else {
				return _Scoreform_sectionvi_question82;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question82.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question82(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question82",v);
		_Scoreform_sectionvi_question82=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question83=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question83.
	 */
	public Integer getScoreform_sectionvi_question83() {
		try{
			if (_Scoreform_sectionvi_question83==null){
				_Scoreform_sectionvi_question83=getIntegerProperty("ScoreForm/sectionVI/question83");
				return _Scoreform_sectionvi_question83;
			}else {
				return _Scoreform_sectionvi_question83;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question83.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question83(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question83",v);
		_Scoreform_sectionvi_question83=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question84=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question84.
	 */
	public Integer getScoreform_sectionvi_question84() {
		try{
			if (_Scoreform_sectionvi_question84==null){
				_Scoreform_sectionvi_question84=getIntegerProperty("ScoreForm/sectionVI/question84");
				return _Scoreform_sectionvi_question84;
			}else {
				return _Scoreform_sectionvi_question84;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question84.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question84(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question84",v);
		_Scoreform_sectionvi_question84=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionvi_question84desc=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question84Desc.
	 */
	public String getScoreform_sectionvi_question84desc(){
		try{
			if (_Scoreform_sectionvi_question84desc==null){
				_Scoreform_sectionvi_question84desc=getStringProperty("ScoreForm/sectionVI/question84Desc");
				return _Scoreform_sectionvi_question84desc;
			}else {
				return _Scoreform_sectionvi_question84desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question84Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question84desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question84Desc",v);
		_Scoreform_sectionvi_question84desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question85=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question85.
	 */
	public Integer getScoreform_sectionvi_question85() {
		try{
			if (_Scoreform_sectionvi_question85==null){
				_Scoreform_sectionvi_question85=getIntegerProperty("ScoreForm/sectionVI/question85");
				return _Scoreform_sectionvi_question85;
			}else {
				return _Scoreform_sectionvi_question85;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question85.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question85(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question85",v);
		_Scoreform_sectionvi_question85=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionvi_question85desc=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question85Desc.
	 */
	public String getScoreform_sectionvi_question85desc(){
		try{
			if (_Scoreform_sectionvi_question85desc==null){
				_Scoreform_sectionvi_question85desc=getStringProperty("ScoreForm/sectionVI/question85Desc");
				return _Scoreform_sectionvi_question85desc;
			}else {
				return _Scoreform_sectionvi_question85desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question85Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question85desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question85Desc",v);
		_Scoreform_sectionvi_question85desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question86=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question86.
	 */
	public Integer getScoreform_sectionvi_question86() {
		try{
			if (_Scoreform_sectionvi_question86==null){
				_Scoreform_sectionvi_question86=getIntegerProperty("ScoreForm/sectionVI/question86");
				return _Scoreform_sectionvi_question86;
			}else {
				return _Scoreform_sectionvi_question86;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question86.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question86(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question86",v);
		_Scoreform_sectionvi_question86=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question87=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question87.
	 */
	public Integer getScoreform_sectionvi_question87() {
		try{
			if (_Scoreform_sectionvi_question87==null){
				_Scoreform_sectionvi_question87=getIntegerProperty("ScoreForm/sectionVI/question87");
				return _Scoreform_sectionvi_question87;
			}else {
				return _Scoreform_sectionvi_question87;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question87.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question87(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question87",v);
		_Scoreform_sectionvi_question87=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question88=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question88.
	 */
	public Integer getScoreform_sectionvi_question88() {
		try{
			if (_Scoreform_sectionvi_question88==null){
				_Scoreform_sectionvi_question88=getIntegerProperty("ScoreForm/sectionVI/question88");
				return _Scoreform_sectionvi_question88;
			}else {
				return _Scoreform_sectionvi_question88;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question88.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question88(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question88",v);
		_Scoreform_sectionvi_question88=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question89=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question89.
	 */
	public Integer getScoreform_sectionvi_question89() {
		try{
			if (_Scoreform_sectionvi_question89==null){
				_Scoreform_sectionvi_question89=getIntegerProperty("ScoreForm/sectionVI/question89");
				return _Scoreform_sectionvi_question89;
			}else {
				return _Scoreform_sectionvi_question89;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question89.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question89(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question89",v);
		_Scoreform_sectionvi_question89=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question90=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question90.
	 */
	public Integer getScoreform_sectionvi_question90() {
		try{
			if (_Scoreform_sectionvi_question90==null){
				_Scoreform_sectionvi_question90=getIntegerProperty("ScoreForm/sectionVI/question90");
				return _Scoreform_sectionvi_question90;
			}else {
				return _Scoreform_sectionvi_question90;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question90.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question90(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question90",v);
		_Scoreform_sectionvi_question90=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question91=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question91.
	 */
	public Integer getScoreform_sectionvi_question91() {
		try{
			if (_Scoreform_sectionvi_question91==null){
				_Scoreform_sectionvi_question91=getIntegerProperty("ScoreForm/sectionVI/question91");
				return _Scoreform_sectionvi_question91;
			}else {
				return _Scoreform_sectionvi_question91;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question91.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question91(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question91",v);
		_Scoreform_sectionvi_question91=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question92=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question92.
	 */
	public Integer getScoreform_sectionvi_question92() {
		try{
			if (_Scoreform_sectionvi_question92==null){
				_Scoreform_sectionvi_question92=getIntegerProperty("ScoreForm/sectionVI/question92");
				return _Scoreform_sectionvi_question92;
			}else {
				return _Scoreform_sectionvi_question92;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question92.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question92(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question92",v);
		_Scoreform_sectionvi_question92=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionvi_question92desc=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question92Desc.
	 */
	public String getScoreform_sectionvi_question92desc(){
		try{
			if (_Scoreform_sectionvi_question92desc==null){
				_Scoreform_sectionvi_question92desc=getStringProperty("ScoreForm/sectionVI/question92Desc");
				return _Scoreform_sectionvi_question92desc;
			}else {
				return _Scoreform_sectionvi_question92desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question92Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question92desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question92Desc",v);
		_Scoreform_sectionvi_question92desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question93=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question93.
	 */
	public Integer getScoreform_sectionvi_question93() {
		try{
			if (_Scoreform_sectionvi_question93==null){
				_Scoreform_sectionvi_question93=getIntegerProperty("ScoreForm/sectionVI/question93");
				return _Scoreform_sectionvi_question93;
			}else {
				return _Scoreform_sectionvi_question93;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question93.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question93(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question93",v);
		_Scoreform_sectionvi_question93=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question94=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question94.
	 */
	public Integer getScoreform_sectionvi_question94() {
		try{
			if (_Scoreform_sectionvi_question94==null){
				_Scoreform_sectionvi_question94=getIntegerProperty("ScoreForm/sectionVI/question94");
				return _Scoreform_sectionvi_question94;
			}else {
				return _Scoreform_sectionvi_question94;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question94.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question94(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question94",v);
		_Scoreform_sectionvi_question94=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question95=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question95.
	 */
	public Integer getScoreform_sectionvi_question95() {
		try{
			if (_Scoreform_sectionvi_question95==null){
				_Scoreform_sectionvi_question95=getIntegerProperty("ScoreForm/sectionVI/question95");
				return _Scoreform_sectionvi_question95;
			}else {
				return _Scoreform_sectionvi_question95;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question95.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question95(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question95",v);
		_Scoreform_sectionvi_question95=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question96=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question96.
	 */
	public Integer getScoreform_sectionvi_question96() {
		try{
			if (_Scoreform_sectionvi_question96==null){
				_Scoreform_sectionvi_question96=getIntegerProperty("ScoreForm/sectionVI/question96");
				return _Scoreform_sectionvi_question96;
			}else {
				return _Scoreform_sectionvi_question96;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question96.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question96(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question96",v);
		_Scoreform_sectionvi_question96=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question97=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question97.
	 */
	public Integer getScoreform_sectionvi_question97() {
		try{
			if (_Scoreform_sectionvi_question97==null){
				_Scoreform_sectionvi_question97=getIntegerProperty("ScoreForm/sectionVI/question97");
				return _Scoreform_sectionvi_question97;
			}else {
				return _Scoreform_sectionvi_question97;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question97.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question97(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question97",v);
		_Scoreform_sectionvi_question97=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question98=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question98.
	 */
	public Integer getScoreform_sectionvi_question98() {
		try{
			if (_Scoreform_sectionvi_question98==null){
				_Scoreform_sectionvi_question98=getIntegerProperty("ScoreForm/sectionVI/question98");
				return _Scoreform_sectionvi_question98;
			}else {
				return _Scoreform_sectionvi_question98;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question98.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question98(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question98",v);
		_Scoreform_sectionvi_question98=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question99=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question99.
	 */
	public Integer getScoreform_sectionvi_question99() {
		try{
			if (_Scoreform_sectionvi_question99==null){
				_Scoreform_sectionvi_question99=getIntegerProperty("ScoreForm/sectionVI/question99");
				return _Scoreform_sectionvi_question99;
			}else {
				return _Scoreform_sectionvi_question99;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question99.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question99(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question99",v);
		_Scoreform_sectionvi_question99=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question100=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question100.
	 */
	public Integer getScoreform_sectionvi_question100() {
		try{
			if (_Scoreform_sectionvi_question100==null){
				_Scoreform_sectionvi_question100=getIntegerProperty("ScoreForm/sectionVI/question100");
				return _Scoreform_sectionvi_question100;
			}else {
				return _Scoreform_sectionvi_question100;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question100.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question100(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question100",v);
		_Scoreform_sectionvi_question100=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question101=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question101.
	 */
	public Integer getScoreform_sectionvi_question101() {
		try{
			if (_Scoreform_sectionvi_question101==null){
				_Scoreform_sectionvi_question101=getIntegerProperty("ScoreForm/sectionVI/question101");
				return _Scoreform_sectionvi_question101;
			}else {
				return _Scoreform_sectionvi_question101;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question101.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question101(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question101",v);
		_Scoreform_sectionvi_question101=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question102=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question102.
	 */
	public Integer getScoreform_sectionvi_question102() {
		try{
			if (_Scoreform_sectionvi_question102==null){
				_Scoreform_sectionvi_question102=getIntegerProperty("ScoreForm/sectionVI/question102");
				return _Scoreform_sectionvi_question102;
			}else {
				return _Scoreform_sectionvi_question102;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question102.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question102(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question102",v);
		_Scoreform_sectionvi_question102=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question103=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question103.
	 */
	public Integer getScoreform_sectionvi_question103() {
		try{
			if (_Scoreform_sectionvi_question103==null){
				_Scoreform_sectionvi_question103=getIntegerProperty("ScoreForm/sectionVI/question103");
				return _Scoreform_sectionvi_question103;
			}else {
				return _Scoreform_sectionvi_question103;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question103.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question103(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question103",v);
		_Scoreform_sectionvi_question103=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question104=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question104.
	 */
	public Integer getScoreform_sectionvi_question104() {
		try{
			if (_Scoreform_sectionvi_question104==null){
				_Scoreform_sectionvi_question104=getIntegerProperty("ScoreForm/sectionVI/question104");
				return _Scoreform_sectionvi_question104;
			}else {
				return _Scoreform_sectionvi_question104;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question104.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question104(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question104",v);
		_Scoreform_sectionvi_question104=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question105=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question105.
	 */
	public Integer getScoreform_sectionvi_question105() {
		try{
			if (_Scoreform_sectionvi_question105==null){
				_Scoreform_sectionvi_question105=getIntegerProperty("ScoreForm/sectionVI/question105");
				return _Scoreform_sectionvi_question105;
			}else {
				return _Scoreform_sectionvi_question105;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question105.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question105(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question105",v);
		_Scoreform_sectionvi_question105=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question106=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question106.
	 */
	public Integer getScoreform_sectionvi_question106() {
		try{
			if (_Scoreform_sectionvi_question106==null){
				_Scoreform_sectionvi_question106=getIntegerProperty("ScoreForm/sectionVI/question106");
				return _Scoreform_sectionvi_question106;
			}else {
				return _Scoreform_sectionvi_question106;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question106.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question106(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question106",v);
		_Scoreform_sectionvi_question106=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question107=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question107.
	 */
	public Integer getScoreform_sectionvi_question107() {
		try{
			if (_Scoreform_sectionvi_question107==null){
				_Scoreform_sectionvi_question107=getIntegerProperty("ScoreForm/sectionVI/question107");
				return _Scoreform_sectionvi_question107;
			}else {
				return _Scoreform_sectionvi_question107;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question107.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question107(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question107",v);
		_Scoreform_sectionvi_question107=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question108=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question108.
	 */
	public Integer getScoreform_sectionvi_question108() {
		try{
			if (_Scoreform_sectionvi_question108==null){
				_Scoreform_sectionvi_question108=getIntegerProperty("ScoreForm/sectionVI/question108");
				return _Scoreform_sectionvi_question108;
			}else {
				return _Scoreform_sectionvi_question108;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question108.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question108(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question108",v);
		_Scoreform_sectionvi_question108=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question109=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question109.
	 */
	public Integer getScoreform_sectionvi_question109() {
		try{
			if (_Scoreform_sectionvi_question109==null){
				_Scoreform_sectionvi_question109=getIntegerProperty("ScoreForm/sectionVI/question109");
				return _Scoreform_sectionvi_question109;
			}else {
				return _Scoreform_sectionvi_question109;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question109.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question109(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question109",v);
		_Scoreform_sectionvi_question109=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question110=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question110.
	 */
	public Integer getScoreform_sectionvi_question110() {
		try{
			if (_Scoreform_sectionvi_question110==null){
				_Scoreform_sectionvi_question110=getIntegerProperty("ScoreForm/sectionVI/question110");
				return _Scoreform_sectionvi_question110;
			}else {
				return _Scoreform_sectionvi_question110;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question110.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question110(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question110",v);
		_Scoreform_sectionvi_question110=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question111=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question111.
	 */
	public Integer getScoreform_sectionvi_question111() {
		try{
			if (_Scoreform_sectionvi_question111==null){
				_Scoreform_sectionvi_question111=getIntegerProperty("ScoreForm/sectionVI/question111");
				return _Scoreform_sectionvi_question111;
			}else {
				return _Scoreform_sectionvi_question111;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question111.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question111(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question111",v);
		_Scoreform_sectionvi_question111=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question112=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question112.
	 */
	public Integer getScoreform_sectionvi_question112() {
		try{
			if (_Scoreform_sectionvi_question112==null){
				_Scoreform_sectionvi_question112=getIntegerProperty("ScoreForm/sectionVI/question112");
				return _Scoreform_sectionvi_question112;
			}else {
				return _Scoreform_sectionvi_question112;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question112.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question112(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question112",v);
		_Scoreform_sectionvi_question112=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question113=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question113.
	 */
	public Integer getScoreform_sectionvi_question113() {
		try{
			if (_Scoreform_sectionvi_question113==null){
				_Scoreform_sectionvi_question113=getIntegerProperty("ScoreForm/sectionVI/question113");
				return _Scoreform_sectionvi_question113;
			}else {
				return _Scoreform_sectionvi_question113;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question113.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question113(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question113",v);
		_Scoreform_sectionvi_question113=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question114=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question114.
	 */
	public Integer getScoreform_sectionvi_question114() {
		try{
			if (_Scoreform_sectionvi_question114==null){
				_Scoreform_sectionvi_question114=getIntegerProperty("ScoreForm/sectionVI/question114");
				return _Scoreform_sectionvi_question114;
			}else {
				return _Scoreform_sectionvi_question114;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question114.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question114(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question114",v);
		_Scoreform_sectionvi_question114=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question115=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question115.
	 */
	public Integer getScoreform_sectionvi_question115() {
		try{
			if (_Scoreform_sectionvi_question115==null){
				_Scoreform_sectionvi_question115=getIntegerProperty("ScoreForm/sectionVI/question115");
				return _Scoreform_sectionvi_question115;
			}else {
				return _Scoreform_sectionvi_question115;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question115.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question115(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question115",v);
		_Scoreform_sectionvi_question115=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question116=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question116.
	 */
	public Integer getScoreform_sectionvi_question116() {
		try{
			if (_Scoreform_sectionvi_question116==null){
				_Scoreform_sectionvi_question116=getIntegerProperty("ScoreForm/sectionVI/question116");
				return _Scoreform_sectionvi_question116;
			}else {
				return _Scoreform_sectionvi_question116;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question116.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question116(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question116",v);
		_Scoreform_sectionvi_question116=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question117=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question117.
	 */
	public Integer getScoreform_sectionvi_question117() {
		try{
			if (_Scoreform_sectionvi_question117==null){
				_Scoreform_sectionvi_question117=getIntegerProperty("ScoreForm/sectionVI/question117");
				return _Scoreform_sectionvi_question117;
			}else {
				return _Scoreform_sectionvi_question117;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question117.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question117(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question117",v);
		_Scoreform_sectionvi_question117=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question118=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question118.
	 */
	public Integer getScoreform_sectionvi_question118() {
		try{
			if (_Scoreform_sectionvi_question118==null){
				_Scoreform_sectionvi_question118=getIntegerProperty("ScoreForm/sectionVI/question118");
				return _Scoreform_sectionvi_question118;
			}else {
				return _Scoreform_sectionvi_question118;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question118.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question118(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question118",v);
		_Scoreform_sectionvi_question118=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question119=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question119.
	 */
	public Integer getScoreform_sectionvi_question119() {
		try{
			if (_Scoreform_sectionvi_question119==null){
				_Scoreform_sectionvi_question119=getIntegerProperty("ScoreForm/sectionVI/question119");
				return _Scoreform_sectionvi_question119;
			}else {
				return _Scoreform_sectionvi_question119;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question119.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question119(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question119",v);
		_Scoreform_sectionvi_question119=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question120=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question120.
	 */
	public Integer getScoreform_sectionvi_question120() {
		try{
			if (_Scoreform_sectionvi_question120==null){
				_Scoreform_sectionvi_question120=getIntegerProperty("ScoreForm/sectionVI/question120");
				return _Scoreform_sectionvi_question120;
			}else {
				return _Scoreform_sectionvi_question120;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question120.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question120(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question120",v);
		_Scoreform_sectionvi_question120=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question121=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question121.
	 */
	public Integer getScoreform_sectionvi_question121() {
		try{
			if (_Scoreform_sectionvi_question121==null){
				_Scoreform_sectionvi_question121=getIntegerProperty("ScoreForm/sectionVI/question121");
				return _Scoreform_sectionvi_question121;
			}else {
				return _Scoreform_sectionvi_question121;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question121.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question121(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question121",v);
		_Scoreform_sectionvi_question121=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question122=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question122.
	 */
	public Integer getScoreform_sectionvi_question122() {
		try{
			if (_Scoreform_sectionvi_question122==null){
				_Scoreform_sectionvi_question122=getIntegerProperty("ScoreForm/sectionVI/question122");
				return _Scoreform_sectionvi_question122;
			}else {
				return _Scoreform_sectionvi_question122;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question122.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question122(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question122",v);
		_Scoreform_sectionvi_question122=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question123=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question123.
	 */
	public Integer getScoreform_sectionvi_question123() {
		try{
			if (_Scoreform_sectionvi_question123==null){
				_Scoreform_sectionvi_question123=getIntegerProperty("ScoreForm/sectionVI/question123");
				return _Scoreform_sectionvi_question123;
			}else {
				return _Scoreform_sectionvi_question123;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question123.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question123(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question123",v);
		_Scoreform_sectionvi_question123=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question124=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question124.
	 */
	public Integer getScoreform_sectionvi_question124() {
		try{
			if (_Scoreform_sectionvi_question124==null){
				_Scoreform_sectionvi_question124=getIntegerProperty("ScoreForm/sectionVI/question124");
				return _Scoreform_sectionvi_question124;
			}else {
				return _Scoreform_sectionvi_question124;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question124.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question124(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question124",v);
		_Scoreform_sectionvi_question124=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question125=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question125.
	 */
	public Integer getScoreform_sectionvi_question125() {
		try{
			if (_Scoreform_sectionvi_question125==null){
				_Scoreform_sectionvi_question125=getIntegerProperty("ScoreForm/sectionVI/question125");
				return _Scoreform_sectionvi_question125;
			}else {
				return _Scoreform_sectionvi_question125;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question125.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question125(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question125",v);
		_Scoreform_sectionvi_question125=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_question126=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/question126.
	 */
	public Integer getScoreform_sectionvi_question126() {
		try{
			if (_Scoreform_sectionvi_question126==null){
				_Scoreform_sectionvi_question126=getIntegerProperty("ScoreForm/sectionVI/question126");
				return _Scoreform_sectionvi_question126;
			}else {
				return _Scoreform_sectionvi_question126;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/question126.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_question126(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/question126",v);
		_Scoreform_sectionvi_question126=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.BclAbcl103ed121data> getAllBclAbcl103ed121datas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.BclAbcl103ed121data> al = new ArrayList<org.nrg.xdat.om.BclAbcl103ed121data>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.BclAbcl103ed121data> getBclAbcl103ed121datasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.BclAbcl103ed121data> al = new ArrayList<org.nrg.xdat.om.BclAbcl103ed121data>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.BclAbcl103ed121data> getBclAbcl103ed121datasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.BclAbcl103ed121data> al = new ArrayList<org.nrg.xdat.om.BclAbcl103ed121data>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static BclAbcl103ed121data getBclAbcl103ed121datasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("bcl:abcl1-03Ed121Data/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (BclAbcl103ed121data) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
