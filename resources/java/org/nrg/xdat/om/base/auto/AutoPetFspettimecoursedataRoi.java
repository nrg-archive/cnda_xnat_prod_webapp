/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoPetFspettimecoursedataRoi extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.PetFspettimecoursedataRoiI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoPetFspettimecoursedataRoi.class);
	public static String SCHEMA_ELEMENT_NAME="pet:fspetTimeCourseData_roi";

	public AutoPetFspettimecoursedataRoi(ItemI item)
	{
		super(item);
	}

	public AutoPetFspettimecoursedataRoi(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoPetFspettimecoursedataRoi(UserI user)
	 **/
	public AutoPetFspettimecoursedataRoi(){}

	public AutoPetFspettimecoursedataRoi(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "pet:fspetTimeCourseData_roi";
	}

	//FIELD

	private Integer _Nvox=null;

	/**
	 * @return Returns the NVox.
	 */
	public Integer getNvox() {
		try{
			if (_Nvox==null){
				_Nvox=getIntegerProperty("NVox");
				return _Nvox;
			}else {
				return _Nvox;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NVox.
	 * @param v Value to Set.
	 */
	public void setNvox(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NVox",v);
		_Nvox=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Bp=null;

	/**
	 * @return Returns the BP.
	 */
	public Double getBp() {
		try{
			if (_Bp==null){
				_Bp=getDoubleProperty("BP");
				return _Bp;
			}else {
				return _Bp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BP.
	 * @param v Value to Set.
	 */
	public void setBp(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BP",v);
		_Bp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _R2Bp=null;

	/**
	 * @return Returns the R2_BP.
	 */
	public Double getR2Bp() {
		try{
			if (_R2Bp==null){
				_R2Bp=getDoubleProperty("R2_BP");
				return _R2Bp;
			}else {
				return _R2Bp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for R2_BP.
	 * @param v Value to Set.
	 */
	public void setR2Bp(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/R2_BP",v);
		_R2Bp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _IntcBp=null;

	/**
	 * @return Returns the INTC_BP.
	 */
	public Double getIntcBp() {
		try{
			if (_IntcBp==null){
				_IntcBp=getDoubleProperty("INTC_BP");
				return _IntcBp;
			}else {
				return _IntcBp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INTC_BP.
	 * @param v Value to Set.
	 */
	public void setIntcBp(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INTC_BP",v);
		_IntcBp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _BpRsf=null;

	/**
	 * @return Returns the BP_RSF.
	 */
	public Double getBpRsf() {
		try{
			if (_BpRsf==null){
				_BpRsf=getDoubleProperty("BP_RSF");
				return _BpRsf;
			}else {
				return _BpRsf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BP_RSF.
	 * @param v Value to Set.
	 */
	public void setBpRsf(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BP_RSF",v);
		_BpRsf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _R2Rsf=null;

	/**
	 * @return Returns the R2_RSF.
	 */
	public Double getR2Rsf() {
		try{
			if (_R2Rsf==null){
				_R2Rsf=getDoubleProperty("R2_RSF");
				return _R2Rsf;
			}else {
				return _R2Rsf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for R2_RSF.
	 * @param v Value to Set.
	 */
	public void setR2Rsf(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/R2_RSF",v);
		_R2Rsf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _IntcRsf=null;

	/**
	 * @return Returns the INTC_RSF.
	 */
	public Double getIntcRsf() {
		try{
			if (_IntcRsf==null){
				_IntcRsf=getDoubleProperty("INTC_RSF");
				return _IntcRsf;
			}else {
				return _IntcRsf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INTC_RSF.
	 * @param v Value to Set.
	 */
	public void setIntcRsf(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INTC_RSF",v);
		_IntcRsf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _BpPvc2c=null;

	/**
	 * @return Returns the BP_PVC2C.
	 */
	public Double getBpPvc2c() {
		try{
			if (_BpPvc2c==null){
				_BpPvc2c=getDoubleProperty("BP_PVC2C");
				return _BpPvc2c;
			}else {
				return _BpPvc2c;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BP_PVC2C.
	 * @param v Value to Set.
	 */
	public void setBpPvc2c(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BP_PVC2C",v);
		_BpPvc2c=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _R2Pvc2c=null;

	/**
	 * @return Returns the R2_PVC2C.
	 */
	public Double getR2Pvc2c() {
		try{
			if (_R2Pvc2c==null){
				_R2Pvc2c=getDoubleProperty("R2_PVC2C");
				return _R2Pvc2c;
			}else {
				return _R2Pvc2c;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for R2_PVC2C.
	 * @param v Value to Set.
	 */
	public void setR2Pvc2c(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/R2_PVC2C",v);
		_R2Pvc2c=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _IntcPvc2c=null;

	/**
	 * @return Returns the INTC_PVC2C.
	 */
	public Double getIntcPvc2c() {
		try{
			if (_IntcPvc2c==null){
				_IntcPvc2c=getDoubleProperty("INTC_PVC2C");
				return _IntcPvc2c;
			}else {
				return _IntcPvc2c;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INTC_PVC2C.
	 * @param v Value to Set.
	 */
	public void setIntcPvc2c(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INTC_PVC2C",v);
		_IntcPvc2c=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Suvr=null;

	/**
	 * @return Returns the SUVR.
	 */
	public Double getSuvr() {
		try{
			if (_Suvr==null){
				_Suvr=getDoubleProperty("SUVR");
				return _Suvr;
			}else {
				return _Suvr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SUVR.
	 * @param v Value to Set.
	 */
	public void setSuvr(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SUVR",v);
		_Suvr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _SuvrRsf=null;

	/**
	 * @return Returns the SUVR_RSF.
	 */
	public Double getSuvrRsf() {
		try{
			if (_SuvrRsf==null){
				_SuvrRsf=getDoubleProperty("SUVR_RSF");
				return _SuvrRsf;
			}else {
				return _SuvrRsf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SUVR_RSF.
	 * @param v Value to Set.
	 */
	public void setSuvrRsf(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SUVR_RSF",v);
		_SuvrRsf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _SuvrPvc2c=null;

	/**
	 * @return Returns the SUVR_PVC2C.
	 */
	public Double getSuvrPvc2c() {
		try{
			if (_SuvrPvc2c==null){
				_SuvrPvc2c=getDoubleProperty("SUVR_PVC2C");
				return _SuvrPvc2c;
			}else {
				return _SuvrPvc2c;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SUVR_PVC2C.
	 * @param v Value to Set.
	 */
	public void setSuvrPvc2c(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SUVR_PVC2C",v);
		_SuvrPvc2c=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.XnatAddfield> _Addstats =null;

	/**
	 * addStats
	 * @return Returns an List of org.nrg.xdat.om.XnatAddfield
	 */
	public <A extends org.nrg.xdat.model.XnatAddfieldI> List<A> getAddstats() {
		try{
			if (_Addstats==null){
				_Addstats=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("addStats"));
			}
			return (List<A>) _Addstats;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.XnatAddfield>();}
	}

	/**
	 * Sets the value for addStats.
	 * @param v Value to Set.
	 */
	public void setAddstats(ItemI v) throws Exception{
		_Addstats =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/addStats",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/addStats",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * addStats
	 * Adds org.nrg.xdat.model.XnatAddfieldI
	 */
	public <A extends org.nrg.xdat.model.XnatAddfieldI> void addAddstats(A item) throws Exception{
	setAddstats((ItemI)item);
	}

	/**
	 * Removes the addStats of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeAddstats(int index) throws java.lang.IndexOutOfBoundsException {
		_Addstats =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/addStats",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Name=null;

	/**
	 * @return Returns the name.
	 */
	public String getName(){
		try{
			if (_Name==null){
				_Name=getStringProperty("name");
				return _Name;
			}else {
				return _Name;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for name.
	 * @param v Value to Set.
	 */
	public void setName(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/name",v);
		_Name=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _PetFspettimecoursedataRoiId=null;

	/**
	 * @return Returns the pet_fspetTimeCourseData_roi_id.
	 */
	public Integer getPetFspettimecoursedataRoiId() {
		try{
			if (_PetFspettimecoursedataRoiId==null){
				_PetFspettimecoursedataRoiId=getIntegerProperty("pet_fspetTimeCourseData_roi_id");
				return _PetFspettimecoursedataRoiId;
			}else {
				return _PetFspettimecoursedataRoiId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for pet_fspetTimeCourseData_roi_id.
	 * @param v Value to Set.
	 */
	public void setPetFspettimecoursedataRoiId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/pet_fspetTimeCourseData_roi_id",v);
		_PetFspettimecoursedataRoiId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.PetFspettimecoursedataRoi> getAllPetFspettimecoursedataRois(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.PetFspettimecoursedataRoi> al = new ArrayList<org.nrg.xdat.om.PetFspettimecoursedataRoi>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.PetFspettimecoursedataRoi> getPetFspettimecoursedataRoisByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.PetFspettimecoursedataRoi> al = new ArrayList<org.nrg.xdat.om.PetFspettimecoursedataRoi>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.PetFspettimecoursedataRoi> getPetFspettimecoursedataRoisByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.PetFspettimecoursedataRoi> al = new ArrayList<org.nrg.xdat.om.PetFspettimecoursedataRoi>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static PetFspettimecoursedataRoi getPetFspettimecoursedataRoisByPetFspettimecoursedataRoiId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("pet:fspetTimeCourseData_roi/pet_fspetTimeCourseData_roi_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (PetFspettimecoursedataRoi) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //addStats
	        for(org.nrg.xdat.model.XnatAddfieldI childAddstats : this.getAddstats()){
	            if (childAddstats!=null){
	              for(ResourceFile rf: ((XnatAddfield)childAddstats).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("addStats[" + ((XnatAddfield)childAddstats).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("addStats/" + ((XnatAddfield)childAddstats).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
