/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCondrMetsLesiondata extends CndaExtSanode implements org.nrg.xdat.model.CondrMetsLesiondataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCondrMetsLesiondata.class);
	public static String SCHEMA_ELEMENT_NAME="condr_mets:lesionData";

	public AutoCondrMetsLesiondata(ItemI item)
	{
		super(item);
	}

	public AutoCondrMetsLesiondata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCondrMetsLesiondata(UserI user)
	 **/
	public AutoCondrMetsLesiondata(){}

	public AutoCondrMetsLesiondata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "condr_mets:lesionData";
	}
	 private org.nrg.xdat.om.CndaExtSanode _Sanode =null;

	/**
	 * saNode
	 * @return org.nrg.xdat.om.CndaExtSanode
	 */
	public org.nrg.xdat.om.CndaExtSanode getSanode() {
		try{
			if (_Sanode==null){
				_Sanode=((CndaExtSanode)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("saNode")));
				return _Sanode;
			}else {
				return _Sanode;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for saNode.
	 * @param v Value to Set.
	 */
	public void setSanode(ItemI v) throws Exception{
		_Sanode =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/saNode",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/saNode",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * saNode
	 * set org.nrg.xdat.model.CndaExtSanodeI
	 */
	public <A extends org.nrg.xdat.model.CndaExtSanodeI> void setSanode(A item) throws Exception{
	setSanode((ItemI)item);
	}

	/**
	 * Removes the saNode.
	 * */
	public void removeSanode() {
		_Sanode =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/saNode",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Hemisphere=null;

	/**
	 * @return Returns the hemisphere.
	 */
	public String getHemisphere(){
		try{
			if (_Hemisphere==null){
				_Hemisphere=getStringProperty("hemisphere");
				return _Hemisphere;
			}else {
				return _Hemisphere;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for hemisphere.
	 * @param v Value to Set.
	 */
	public void setHemisphere(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/hemisphere",v);
		_Hemisphere=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Location=null;

	/**
	 * @return Returns the location.
	 */
	public String getLocation(){
		try{
			if (_Location==null){
				_Location=getStringProperty("location");
				return _Location;
			}else {
				return _Location;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for location.
	 * @param v Value to Set.
	 */
	public void setLocation(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/location",v);
		_Location=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Dateofdiagnosis=null;

	/**
	 * @return Returns the dateOfDiagnosis.
	 */
	public Object getDateofdiagnosis(){
		try{
			if (_Dateofdiagnosis==null){
				_Dateofdiagnosis=getProperty("dateOfDiagnosis");
				return _Dateofdiagnosis;
			}else {
				return _Dateofdiagnosis;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for dateOfDiagnosis.
	 * @param v Value to Set.
	 */
	public void setDateofdiagnosis(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/dateOfDiagnosis",v);
		_Dateofdiagnosis=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Description=null;

	/**
	 * @return Returns the description.
	 */
	public String getDescription(){
		try{
			if (_Description==null){
				_Description=getStringProperty("description");
				return _Description;
			}else {
				return _Description;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for description.
	 * @param v Value to Set.
	 */
	public void setDescription(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/description",v);
		_Description=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CondrMetsLesiondata> getAllCondrMetsLesiondatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrMetsLesiondata> al = new ArrayList<org.nrg.xdat.om.CondrMetsLesiondata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrMetsLesiondata> getCondrMetsLesiondatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrMetsLesiondata> al = new ArrayList<org.nrg.xdat.om.CondrMetsLesiondata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrMetsLesiondata> getCondrMetsLesiondatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrMetsLesiondata> al = new ArrayList<org.nrg.xdat.om.CondrMetsLesiondata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CondrMetsLesiondata getCondrMetsLesiondatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("condr_mets:lesionData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CondrMetsLesiondata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //saNode
	        CndaExtSanode childSanode = (CndaExtSanode)this.getSanode();
	            if (childSanode!=null){
	              for(ResourceFile rf: ((CndaExtSanode)childSanode).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("saNode[" + ((CndaExtSanode)childSanode).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("saNode/" + ((CndaExtSanode)childSanode).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
