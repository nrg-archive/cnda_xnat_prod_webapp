/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoLs2Lsactivitylimit extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.Ls2LsactivitylimitI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoLs2Lsactivitylimit.class);
	public static String SCHEMA_ELEMENT_NAME="ls2:lsActivityLimit";

	public AutoLs2Lsactivitylimit(ItemI item)
	{
		super(item);
	}

	public AutoLs2Lsactivitylimit(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoLs2Lsactivitylimit(UserI user)
	 **/
	public AutoLs2Lsactivitylimit(){}

	public AutoLs2Lsactivitylimit(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "ls2:lsActivityLimit";
	}

	//FIELD

	private String _Name=null;

	/**
	 * @return Returns the name.
	 */
	public String getName(){
		try{
			if (_Name==null){
				_Name=getStringProperty("name");
				return _Name;
			}else {
				return _Name;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for name.
	 * @param v Value to Set.
	 */
	public void setName(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/name",v);
		_Name=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Status=null;

	/**
	 * @return Returns the status.
	 */
	public Integer getStatus() {
		try{
			if (_Status==null){
				_Status=getIntegerProperty("status");
				return _Status;
			}else {
				return _Status;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for status.
	 * @param v Value to Set.
	 */
	public void setStatus(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/status",v);
		_Status=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ls2LsactivitylimitId=null;

	/**
	 * @return Returns the ls2_lsActivityLimit_id.
	 */
	public Integer getLs2LsactivitylimitId() {
		try{
			if (_Ls2LsactivitylimitId==null){
				_Ls2LsactivitylimitId=getIntegerProperty("ls2_lsActivityLimit_id");
				return _Ls2LsactivitylimitId;
			}else {
				return _Ls2LsactivitylimitId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ls2_lsActivityLimit_id.
	 * @param v Value to Set.
	 */
	public void setLs2LsactivitylimitId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ls2_lsActivityLimit_id",v);
		_Ls2LsactivitylimitId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.Ls2Lsactivitylimit> getAllLs2Lsactivitylimits(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.Ls2Lsactivitylimit> al = new ArrayList<org.nrg.xdat.om.Ls2Lsactivitylimit>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.Ls2Lsactivitylimit> getLs2LsactivitylimitsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.Ls2Lsactivitylimit> al = new ArrayList<org.nrg.xdat.om.Ls2Lsactivitylimit>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.Ls2Lsactivitylimit> getLs2LsactivitylimitsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.Ls2Lsactivitylimit> al = new ArrayList<org.nrg.xdat.om.Ls2Lsactivitylimit>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static Ls2Lsactivitylimit getLs2LsactivitylimitsByLs2LsactivitylimitId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("ls2:lsActivityLimit/ls2_lsActivityLimit_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (Ls2Lsactivitylimit) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
