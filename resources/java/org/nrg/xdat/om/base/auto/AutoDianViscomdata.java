/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:01 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianViscomdata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianViscomdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianViscomdata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:viscomData";

	public AutoDianViscomdata(ItemI item)
	{
		super(item);
	}

	public AutoDianViscomdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianViscomdata(UserI user)
	 **/
	public AutoDianViscomdata(){}

	public AutoDianViscomdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:viscomData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Viscomm1=null;

	/**
	 * @return Returns the VISCOMM1.
	 */
	public String getViscomm1(){
		try{
			if (_Viscomm1==null){
				_Viscomm1=getStringProperty("VISCOMM1");
				return _Viscomm1;
			}else {
				return _Viscomm1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VISCOMM1.
	 * @param v Value to Set.
	 */
	public void setViscomm1(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VISCOMM1",v);
		_Viscomm1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Viscomm2=null;

	/**
	 * @return Returns the VISCOMM2.
	 */
	public String getViscomm2(){
		try{
			if (_Viscomm2==null){
				_Viscomm2=getStringProperty("VISCOMM2");
				return _Viscomm2;
			}else {
				return _Viscomm2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VISCOMM2.
	 * @param v Value to Set.
	 */
	public void setViscomm2(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VISCOMM2",v);
		_Viscomm2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Viscomm3=null;

	/**
	 * @return Returns the VISCOMM3.
	 */
	public String getViscomm3(){
		try{
			if (_Viscomm3==null){
				_Viscomm3=getStringProperty("VISCOMM3");
				return _Viscomm3;
			}else {
				return _Viscomm3;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VISCOMM3.
	 * @param v Value to Set.
	 */
	public void setViscomm3(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VISCOMM3",v);
		_Viscomm3=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Viscomm4=null;

	/**
	 * @return Returns the VISCOMM4.
	 */
	public String getViscomm4(){
		try{
			if (_Viscomm4==null){
				_Viscomm4=getStringProperty("VISCOMM4");
				return _Viscomm4;
			}else {
				return _Viscomm4;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VISCOMM4.
	 * @param v Value to Set.
	 */
	public void setViscomm4(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VISCOMM4",v);
		_Viscomm4=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Viscomm5=null;

	/**
	 * @return Returns the VISCOMM5.
	 */
	public String getViscomm5(){
		try{
			if (_Viscomm5==null){
				_Viscomm5=getStringProperty("VISCOMM5");
				return _Viscomm5;
			}else {
				return _Viscomm5;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VISCOMM5.
	 * @param v Value to Set.
	 */
	public void setViscomm5(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VISCOMM5",v);
		_Viscomm5=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Viscomm6=null;

	/**
	 * @return Returns the VISCOMM6.
	 */
	public String getViscomm6(){
		try{
			if (_Viscomm6==null){
				_Viscomm6=getStringProperty("VISCOMM6");
				return _Viscomm6;
			}else {
				return _Viscomm6;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VISCOMM6.
	 * @param v Value to Set.
	 */
	public void setViscomm6(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VISCOMM6",v);
		_Viscomm6=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Viscomm7=null;

	/**
	 * @return Returns the VISCOMM7.
	 */
	public String getViscomm7(){
		try{
			if (_Viscomm7==null){
				_Viscomm7=getStringProperty("VISCOMM7");
				return _Viscomm7;
			}else {
				return _Viscomm7;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VISCOMM7.
	 * @param v Value to Set.
	 */
	public void setViscomm7(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VISCOMM7",v);
		_Viscomm7=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Viscomm8=null;

	/**
	 * @return Returns the VISCOMM8.
	 */
	public String getViscomm8(){
		try{
			if (_Viscomm8==null){
				_Viscomm8=getStringProperty("VISCOMM8");
				return _Viscomm8;
			}else {
				return _Viscomm8;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VISCOMM8.
	 * @param v Value to Set.
	 */
	public void setViscomm8(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VISCOMM8",v);
		_Viscomm8=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Viscomm9=null;

	/**
	 * @return Returns the VISCOMM9.
	 */
	public String getViscomm9(){
		try{
			if (_Viscomm9==null){
				_Viscomm9=getStringProperty("VISCOMM9");
				return _Viscomm9;
			}else {
				return _Viscomm9;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VISCOMM9.
	 * @param v Value to Set.
	 */
	public void setViscomm9(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VISCOMM9",v);
		_Viscomm9=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Viscomm10=null;

	/**
	 * @return Returns the VISCOMM10.
	 */
	public String getViscomm10(){
		try{
			if (_Viscomm10==null){
				_Viscomm10=getStringProperty("VISCOMM10");
				return _Viscomm10;
			}else {
				return _Viscomm10;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VISCOMM10.
	 * @param v Value to Set.
	 */
	public void setViscomm10(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VISCOMM10",v);
		_Viscomm10=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianViscomdata> getAllDianViscomdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianViscomdata> al = new ArrayList<org.nrg.xdat.om.DianViscomdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianViscomdata> getDianViscomdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianViscomdata> al = new ArrayList<org.nrg.xdat.om.DianViscomdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianViscomdata> getDianViscomdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianViscomdata> al = new ArrayList<org.nrg.xdat.om.DianViscomdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianViscomdata getDianViscomdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:viscomData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianViscomdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
