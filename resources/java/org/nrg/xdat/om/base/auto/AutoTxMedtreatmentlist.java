/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoTxMedtreatmentlist extends XnatSubjectassessordata implements org.nrg.xdat.model.TxMedtreatmentlistI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoTxMedtreatmentlist.class);
	public static String SCHEMA_ELEMENT_NAME="tx:medTreatmentList";

	public AutoTxMedtreatmentlist(ItemI item)
	{
		super(item);
	}

	public AutoTxMedtreatmentlist(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoTxMedtreatmentlist(UserI user)
	 **/
	public AutoTxMedtreatmentlist(){}

	public AutoTxMedtreatmentlist(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "tx:medTreatmentList";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.TxMedtreatment> _Medtreatments_medtreatment =null;

	/**
	 * medTreatments/medTreatment
	 * @return Returns an List of org.nrg.xdat.om.TxMedtreatment
	 */
	public <A extends org.nrg.xdat.model.TxMedtreatmentI> List<A> getMedtreatments_medtreatment() {
		try{
			if (_Medtreatments_medtreatment==null){
				_Medtreatments_medtreatment=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("medTreatments/medTreatment"));
			}
			return (List<A>) _Medtreatments_medtreatment;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.TxMedtreatment>();}
	}

	/**
	 * Sets the value for medTreatments/medTreatment.
	 * @param v Value to Set.
	 */
	public void setMedtreatments_medtreatment(ItemI v) throws Exception{
		_Medtreatments_medtreatment =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/medTreatments/medTreatment",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/medTreatments/medTreatment",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * medTreatments/medTreatment
	 * Adds org.nrg.xdat.model.TxMedtreatmentI
	 */
	public <A extends org.nrg.xdat.model.TxMedtreatmentI> void addMedtreatments_medtreatment(A item) throws Exception{
	setMedtreatments_medtreatment((ItemI)item);
	}

	/**
	 * Removes the medTreatments/medTreatment of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeMedtreatments_medtreatment(int index) throws java.lang.IndexOutOfBoundsException {
		_Medtreatments_medtreatment =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/medTreatments/medTreatment",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.TxMedtreatmentlist> getAllTxMedtreatmentlists(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TxMedtreatmentlist> al = new ArrayList<org.nrg.xdat.om.TxMedtreatmentlist>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.TxMedtreatmentlist> getTxMedtreatmentlistsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TxMedtreatmentlist> al = new ArrayList<org.nrg.xdat.om.TxMedtreatmentlist>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.TxMedtreatmentlist> getTxMedtreatmentlistsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TxMedtreatmentlist> al = new ArrayList<org.nrg.xdat.om.TxMedtreatmentlist>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static TxMedtreatmentlist getTxMedtreatmentlistsById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("tx:medTreatmentList/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (TxMedtreatmentlist) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //medTreatments/medTreatment
	        for(org.nrg.xdat.model.TxMedtreatmentI childMedtreatments_medtreatment : this.getMedtreatments_medtreatment()){
	            if (childMedtreatments_medtreatment!=null){
	              for(ResourceFile rf: ((TxMedtreatment)childMedtreatments_medtreatment).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("medTreatments/medTreatment[" + ((TxMedtreatment)childMedtreatments_medtreatment).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("medTreatments/medTreatment/" + ((TxMedtreatment)childMedtreatments_medtreatment).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
