/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaCndasubjectmetadata extends XnatSubjectmetadata implements org.nrg.xdat.model.CndaCndasubjectmetadataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaCndasubjectmetadata.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:cndaSubjectMetadata";

	public AutoCndaCndasubjectmetadata(ItemI item)
	{
		super(item);
	}

	public AutoCndaCndasubjectmetadata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaCndasubjectmetadata(UserI user)
	 **/
	public AutoCndaCndasubjectmetadata(){}

	public AutoCndaCndasubjectmetadata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:cndaSubjectMetadata";
	}
	 private org.nrg.xdat.om.XnatSubjectmetadata _Subjectmetadata =null;

	/**
	 * subjectMetadata
	 * @return org.nrg.xdat.om.XnatSubjectmetadata
	 */
	public org.nrg.xdat.om.XnatSubjectmetadata getSubjectmetadata() {
		try{
			if (_Subjectmetadata==null){
				_Subjectmetadata=((XnatSubjectmetadata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectMetadata")));
				return _Subjectmetadata;
			}else {
				return _Subjectmetadata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectMetadata.
	 * @param v Value to Set.
	 */
	public void setSubjectmetadata(ItemI v) throws Exception{
		_Subjectmetadata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectMetadata",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectMetadata",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectMetadata
	 * set org.nrg.xdat.model.XnatSubjectmetadataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectmetadataI> void setSubjectmetadata(A item) throws Exception{
	setSubjectmetadata((ItemI)item);
	}

	/**
	 * Removes the subjectMetadata.
	 * */
	public void removeSubjectmetadata() {
		_Subjectmetadata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectMetadata",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Map=null;

	/**
	 * @return Returns the map.
	 */
	public String getMap(){
		try{
			if (_Map==null){
				_Map=getStringProperty("map");
				return _Map;
			}else {
				return _Map;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for map.
	 * @param v Value to Set.
	 */
	public void setMap(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/map",v);
		_Map=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _LabId=null;

	/**
	 * @return Returns the lab_id.
	 */
	public String getLabId(){
		try{
			if (_LabId==null){
				_LabId=getStringProperty("lab_id");
				return _LabId;
			}else {
				return _LabId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for lab_id.
	 * @param v Value to Set.
	 */
	public void setLabId(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/lab_id",v);
		_LabId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Pib=null;

	/**
	 * @return Returns the pib.
	 */
	public String getPib(){
		try{
			if (_Pib==null){
				_Pib=getStringProperty("pib");
				return _Pib;
			}else {
				return _Pib;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for pib.
	 * @param v Value to Set.
	 */
	public void setPib(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/pib",v);
		_Pib=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaCndasubjectmetadata> getAllCndaCndasubjectmetadatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaCndasubjectmetadata> al = new ArrayList<org.nrg.xdat.om.CndaCndasubjectmetadata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaCndasubjectmetadata> getCndaCndasubjectmetadatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaCndasubjectmetadata> al = new ArrayList<org.nrg.xdat.om.CndaCndasubjectmetadata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaCndasubjectmetadata> getCndaCndasubjectmetadatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaCndasubjectmetadata> al = new ArrayList<org.nrg.xdat.om.CndaCndasubjectmetadata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaCndasubjectmetadata getCndaCndasubjectmetadatasByXnatAbstractsubjectmetadataId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:cndaSubjectMetadata/xnat_abstractsubjectmetadata_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaCndasubjectmetadata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static CndaCndasubjectmetadata getCndaCndasubjectmetadatasByMap(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:cndaSubjectMetadata/map",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaCndasubjectmetadata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static CndaCndasubjectmetadata getCndaCndasubjectmetadatasByLabId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:cndaSubjectMetadata/lab_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaCndasubjectmetadata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static CndaCndasubjectmetadata getCndaCndasubjectmetadatasByPib(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:cndaSubjectMetadata/pib",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaCndasubjectmetadata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectMetadata
	        XnatSubjectmetadata childSubjectmetadata = (XnatSubjectmetadata)this.getSubjectmetadata();
	            if (childSubjectmetadata!=null){
	              for(ResourceFile rf: ((XnatSubjectmetadata)childSubjectmetadata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectMetadata[" + ((XnatSubjectmetadata)childSubjectmetadata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectMetadata/" + ((XnatSubjectmetadata)childSubjectmetadata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
