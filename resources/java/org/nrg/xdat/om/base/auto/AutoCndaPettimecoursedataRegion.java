/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaPettimecoursedataRegion extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.CndaPettimecoursedataRegionI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaPettimecoursedataRegion.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:petTimeCourseData_region";

	public AutoCndaPettimecoursedataRegion(ItemI item)
	{
		super(item);
	}

	public AutoCndaPettimecoursedataRegion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaPettimecoursedataRegion(UserI user)
	 **/
	public AutoCndaPettimecoursedataRegion(){}

	public AutoCndaPettimecoursedataRegion(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:petTimeCourseData_region";
	}
	 private ArrayList<org.nrg.xdat.om.CndaPettimecoursedataRegionActivity> _Timeseries_activity =null;

	/**
	 * timeSeries/activity
	 * @return Returns an List of org.nrg.xdat.om.CndaPettimecoursedataRegionActivity
	 */
	public <A extends org.nrg.xdat.model.CndaPettimecoursedataRegionActivityI> List<A> getTimeseries_activity() {
		try{
			if (_Timeseries_activity==null){
				_Timeseries_activity=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("timeSeries/activity"));
			}
			return (List<A>) _Timeseries_activity;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.CndaPettimecoursedataRegionActivity>();}
	}

	/**
	 * Sets the value for timeSeries/activity.
	 * @param v Value to Set.
	 */
	public void setTimeseries_activity(ItemI v) throws Exception{
		_Timeseries_activity =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/timeSeries/activity",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/timeSeries/activity",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * timeSeries/activity
	 * Adds org.nrg.xdat.model.CndaPettimecoursedataRegionActivityI
	 */
	public <A extends org.nrg.xdat.model.CndaPettimecoursedataRegionActivityI> void addTimeseries_activity(A item) throws Exception{
	setTimeseries_activity((ItemI)item);
	}

	/**
	 * Removes the timeSeries/activity of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeTimeseries_activity(int index) throws java.lang.IndexOutOfBoundsException {
		_Timeseries_activity =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/timeSeries/activity",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Name=null;

	/**
	 * @return Returns the name.
	 */
	public String getName(){
		try{
			if (_Name==null){
				_Name=getStringProperty("name");
				return _Name;
			}else {
				return _Name;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for name.
	 * @param v Value to Set.
	 */
	public void setName(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/name",v);
		_Name=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Hemisphere=null;

	/**
	 * @return Returns the hemisphere.
	 */
	public String getHemisphere(){
		try{
			if (_Hemisphere==null){
				_Hemisphere=getStringProperty("hemisphere");
				return _Hemisphere;
			}else {
				return _Hemisphere;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for hemisphere.
	 * @param v Value to Set.
	 */
	public void setHemisphere(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/hemisphere",v);
		_Hemisphere=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CndaPettimecoursedataRegionId=null;

	/**
	 * @return Returns the cnda_petTimeCourseData_region_id.
	 */
	public Integer getCndaPettimecoursedataRegionId() {
		try{
			if (_CndaPettimecoursedataRegionId==null){
				_CndaPettimecoursedataRegionId=getIntegerProperty("cnda_petTimeCourseData_region_id");
				return _CndaPettimecoursedataRegionId;
			}else {
				return _CndaPettimecoursedataRegionId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda_petTimeCourseData_region_id.
	 * @param v Value to Set.
	 */
	public void setCndaPettimecoursedataRegionId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/cnda_petTimeCourseData_region_id",v);
		_CndaPettimecoursedataRegionId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaPettimecoursedataRegion> getAllCndaPettimecoursedataRegions(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaPettimecoursedataRegion> al = new ArrayList<org.nrg.xdat.om.CndaPettimecoursedataRegion>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaPettimecoursedataRegion> getCndaPettimecoursedataRegionsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaPettimecoursedataRegion> al = new ArrayList<org.nrg.xdat.om.CndaPettimecoursedataRegion>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaPettimecoursedataRegion> getCndaPettimecoursedataRegionsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaPettimecoursedataRegion> al = new ArrayList<org.nrg.xdat.om.CndaPettimecoursedataRegion>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaPettimecoursedataRegion getCndaPettimecoursedataRegionsByCndaPettimecoursedataRegionId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:petTimeCourseData_region/cnda_petTimeCourseData_region_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaPettimecoursedataRegion) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //timeSeries/activity
	        for(org.nrg.xdat.model.CndaPettimecoursedataRegionActivityI childTimeseries_activity : this.getTimeseries_activity()){
	            if (childTimeseries_activity!=null){
	              for(ResourceFile rf: ((CndaPettimecoursedataRegionActivity)childTimeseries_activity).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("timeSeries/activity[" + ((CndaPettimecoursedataRegionActivity)childTimeseries_activity).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("timeSeries/activity/" + ((CndaPettimecoursedataRegionActivity)childTimeseries_activity).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
