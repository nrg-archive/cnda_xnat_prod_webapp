/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoTxChemotherapytreatment extends TxBasetreatment implements org.nrg.xdat.model.TxChemotherapytreatmentI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoTxChemotherapytreatment.class);
	public static String SCHEMA_ELEMENT_NAME="tx:chemotherapyTreatment";

	public AutoTxChemotherapytreatment(ItemI item)
	{
		super(item);
	}

	public AutoTxChemotherapytreatment(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoTxChemotherapytreatment(UserI user)
	 **/
	public AutoTxChemotherapytreatment(){}

	public AutoTxChemotherapytreatment(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "tx:chemotherapyTreatment";
	}
	 private org.nrg.xdat.om.TxBasetreatment _Basetreatment =null;

	/**
	 * baseTreatment
	 * @return org.nrg.xdat.om.TxBasetreatment
	 */
	public org.nrg.xdat.om.TxBasetreatment getBasetreatment() {
		try{
			if (_Basetreatment==null){
				_Basetreatment=((TxBasetreatment)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("baseTreatment")));
				return _Basetreatment;
			}else {
				return _Basetreatment;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for baseTreatment.
	 * @param v Value to Set.
	 */
	public void setBasetreatment(ItemI v) throws Exception{
		_Basetreatment =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/baseTreatment",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/baseTreatment",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * baseTreatment
	 * set org.nrg.xdat.model.TxBasetreatmentI
	 */
	public <A extends org.nrg.xdat.model.TxBasetreatmentI> void setBasetreatment(A item) throws Exception{
	setBasetreatment((ItemI)item);
	}

	/**
	 * Removes the baseTreatment.
	 * */
	public void removeBasetreatment() {
		_Basetreatment =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/baseTreatment",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.TxChemotherapytreatment> getAllTxChemotherapytreatments(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TxChemotherapytreatment> al = new ArrayList<org.nrg.xdat.om.TxChemotherapytreatment>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.TxChemotherapytreatment> getTxChemotherapytreatmentsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TxChemotherapytreatment> al = new ArrayList<org.nrg.xdat.om.TxChemotherapytreatment>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.TxChemotherapytreatment> getTxChemotherapytreatmentsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TxChemotherapytreatment> al = new ArrayList<org.nrg.xdat.om.TxChemotherapytreatment>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static TxChemotherapytreatment getTxChemotherapytreatmentsById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("tx:chemotherapyTreatment/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (TxChemotherapytreatment) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //baseTreatment
	        TxBasetreatment childBasetreatment = (TxBasetreatment)this.getBasetreatment();
	            if (childBasetreatment!=null){
	              for(ResourceFile rf: ((TxBasetreatment)childBasetreatment).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("baseTreatment[" + ((TxBasetreatment)childBasetreatment).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("baseTreatment/" + ((TxBasetreatment)childBasetreatment).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
