/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:01 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoWmhWmhdata extends XnatImageassessordata implements org.nrg.xdat.model.WmhWmhdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoWmhWmhdata.class);
	public static String SCHEMA_ELEMENT_NAME="wmh:wmhData";

	public AutoWmhWmhdata(ItemI item)
	{
		super(item);
	}

	public AutoWmhWmhdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoWmhWmhdata(UserI user)
	 **/
	public AutoWmhWmhdata(){}

	public AutoWmhWmhdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "wmh:wmhData";
	}
	 private org.nrg.xdat.om.XnatImageassessordata _Imageassessordata =null;

	/**
	 * imageAssessorData
	 * @return org.nrg.xdat.om.XnatImageassessordata
	 */
	public org.nrg.xdat.om.XnatImageassessordata getImageassessordata() {
		try{
			if (_Imageassessordata==null){
				_Imageassessordata=((XnatImageassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("imageAssessorData")));
				return _Imageassessordata;
			}else {
				return _Imageassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for imageAssessorData.
	 * @param v Value to Set.
	 */
	public void setImageassessordata(ItemI v) throws Exception{
		_Imageassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/imageAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/imageAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * imageAssessorData
	 * set org.nrg.xdat.model.XnatImageassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatImageassessordataI> void setImageassessordata(A item) throws Exception{
	setImageassessordata((ItemI)item);
	}

	/**
	 * Removes the imageAssessorData.
	 * */
	public void removeImageassessordata() {
		_Imageassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/imageAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Volume=null;

	/**
	 * @return Returns the volume.
	 */
	public Double getVolume() {
		try{
			if (_Volume==null){
				_Volume=getDoubleProperty("volume");
				return _Volume;
			}else {
				return _Volume;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for volume.
	 * @param v Value to Set.
	 */
	public void setVolume(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/volume",v);
		_Volume=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Voxels=null;

	/**
	 * @return Returns the voxels.
	 */
	public Integer getVoxels() {
		try{
			if (_Voxels==null){
				_Voxels=getIntegerProperty("voxels");
				return _Voxels;
			}else {
				return _Voxels;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for voxels.
	 * @param v Value to Set.
	 */
	public void setVoxels(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/voxels",v);
		_Voxels=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Inputs_t1Session=null;

	/**
	 * @return Returns the inputs/t1_session.
	 */
	public String getInputs_t1Session(){
		try{
			if (_Inputs_t1Session==null){
				_Inputs_t1Session=getStringProperty("inputs/t1_session");
				return _Inputs_t1Session;
			}else {
				return _Inputs_t1Session;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for inputs/t1_session.
	 * @param v Value to Set.
	 */
	public void setInputs_t1Session(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/inputs/t1_session",v);
		_Inputs_t1Session=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Inputs_t1Scan=null;

	/**
	 * @return Returns the inputs/t1_scan.
	 */
	public String getInputs_t1Scan(){
		try{
			if (_Inputs_t1Scan==null){
				_Inputs_t1Scan=getStringProperty("inputs/t1_scan");
				return _Inputs_t1Scan;
			}else {
				return _Inputs_t1Scan;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for inputs/t1_scan.
	 * @param v Value to Set.
	 */
	public void setInputs_t1Scan(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/inputs/t1_scan",v);
		_Inputs_t1Scan=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Inputs_flairSession=null;

	/**
	 * @return Returns the inputs/flair_session.
	 */
	public String getInputs_flairSession(){
		try{
			if (_Inputs_flairSession==null){
				_Inputs_flairSession=getStringProperty("inputs/flair_session");
				return _Inputs_flairSession;
			}else {
				return _Inputs_flairSession;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for inputs/flair_session.
	 * @param v Value to Set.
	 */
	public void setInputs_flairSession(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/inputs/flair_session",v);
		_Inputs_flairSession=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Inputs_flairScan=null;

	/**
	 * @return Returns the inputs/flair_scan.
	 */
	public String getInputs_flairScan(){
		try{
			if (_Inputs_flairScan==null){
				_Inputs_flairScan=getStringProperty("inputs/flair_scan");
				return _Inputs_flairScan;
			}else {
				return _Inputs_flairScan;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for inputs/flair_scan.
	 * @param v Value to Set.
	 */
	public void setInputs_flairScan(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/inputs/flair_scan",v);
		_Inputs_flairScan=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.WmhWmhdata> getAllWmhWmhdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.WmhWmhdata> al = new ArrayList<org.nrg.xdat.om.WmhWmhdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.WmhWmhdata> getWmhWmhdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.WmhWmhdata> al = new ArrayList<org.nrg.xdat.om.WmhWmhdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.WmhWmhdata> getWmhWmhdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.WmhWmhdata> al = new ArrayList<org.nrg.xdat.om.WmhWmhdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static WmhWmhdata getWmhWmhdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("wmh:wmhData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (WmhWmhdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //imageAssessorData
	        XnatImageassessordata childImageassessordata = (XnatImageassessordata)this.getImageassessordata();
	            if (childImageassessordata!=null){
	              for(ResourceFile rf: ((XnatImageassessordata)childImageassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("imageAssessorData[" + ((XnatImageassessordata)childImageassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("imageAssessorData/" + ((XnatImageassessordata)childImageassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
