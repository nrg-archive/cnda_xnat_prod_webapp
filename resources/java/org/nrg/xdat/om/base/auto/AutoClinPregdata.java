/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:01 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoClinPregdata extends XnatSubjectassessordata implements org.nrg.xdat.model.ClinPregdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoClinPregdata.class);
	public static String SCHEMA_ELEMENT_NAME="clin:pregData";

	public AutoClinPregdata(ItemI item)
	{
		super(item);
	}

	public AutoClinPregdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoClinPregdata(UserI user)
	 **/
	public AutoClinPregdata(){}

	public AutoClinPregdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "clin:pregData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.ClinPregdataPreg> _Preglist_preg =null;

	/**
	 * pregList/preg
	 * @return Returns an List of org.nrg.xdat.om.ClinPregdataPreg
	 */
	public <A extends org.nrg.xdat.model.ClinPregdataPregI> List<A> getPreglist_preg() {
		try{
			if (_Preglist_preg==null){
				_Preglist_preg=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("pregList/preg"));
			}
			return (List<A>) _Preglist_preg;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.ClinPregdataPreg>();}
	}

	/**
	 * Sets the value for pregList/preg.
	 * @param v Value to Set.
	 */
	public void setPreglist_preg(ItemI v) throws Exception{
		_Preglist_preg =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/pregList/preg",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/pregList/preg",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * pregList/preg
	 * Adds org.nrg.xdat.model.ClinPregdataPregI
	 */
	public <A extends org.nrg.xdat.model.ClinPregdataPregI> void addPreglist_preg(A item) throws Exception{
	setPreglist_preg((ItemI)item);
	}

	/**
	 * Removes the pregList/preg of the given index.
	 * @param index Index of child to remove.
	 */
	public void removePreglist_preg(int index) throws java.lang.IndexOutOfBoundsException {
		_Preglist_preg =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/pregList/preg",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.ClinPregdata> getAllClinPregdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.ClinPregdata> al = new ArrayList<org.nrg.xdat.om.ClinPregdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.ClinPregdata> getClinPregdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.ClinPregdata> al = new ArrayList<org.nrg.xdat.om.ClinPregdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.ClinPregdata> getClinPregdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.ClinPregdata> al = new ArrayList<org.nrg.xdat.om.ClinPregdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ClinPregdata getClinPregdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("clin:pregData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (ClinPregdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //pregList/preg
	        for(org.nrg.xdat.model.ClinPregdataPregI childPreglist_preg : this.getPreglist_preg()){
	            if (childPreglist_preg!=null){
	              for(ResourceFile rf: ((ClinPregdataPreg)childPreglist_preg).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("pregList/preg[" + ((ClinPregdataPreg)childPreglist_preg).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("pregList/preg/" + ((ClinPregdataPreg)childPreglist_preg).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
