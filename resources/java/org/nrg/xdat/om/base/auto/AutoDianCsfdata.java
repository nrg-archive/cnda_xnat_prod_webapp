/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:01 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianCsfdata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianCsfdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianCsfdata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:csfData";

	public AutoDianCsfdata(ItemI item)
	{
		super(item);
	}

	public AutoDianCsfdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianCsfdata(UserI user)
	 **/
	public AutoDianCsfdata(){}

	public AutoDianCsfdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:csfData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Bldcollsess=null;

	/**
	 * @return Returns the bldCollSess.
	 */
	public String getBldcollsess(){
		try{
			if (_Bldcollsess==null){
				_Bldcollsess=getStringProperty("bldCollSess");
				return _Bldcollsess;
			}else {
				return _Bldcollsess;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for bldCollSess.
	 * @param v Value to Set.
	 */
	public void setBldcollsess(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/bldCollSess",v);
		_Bldcollsess=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Tau=null;

	/**
	 * @return Returns the tau.
	 */
	public Double getTau() {
		try{
			if (_Tau==null){
				_Tau=getDoubleProperty("tau");
				return _Tau;
			}else {
				return _Tau;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tau.
	 * @param v Value to Set.
	 */
	public void setTau(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tau",v);
		_Tau=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Tauunits=null;

	/**
	 * @return Returns the tauUnits.
	 */
	public String getTauunits(){
		try{
			if (_Tauunits==null){
				_Tauunits=getStringProperty("tauUnits");
				return _Tauunits;
			}else {
				return _Tauunits;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tauUnits.
	 * @param v Value to Set.
	 */
	public void setTauunits(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tauUnits",v);
		_Tauunits=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Ptau=null;

	/**
	 * @return Returns the ptau.
	 */
	public Double getPtau() {
		try{
			if (_Ptau==null){
				_Ptau=getDoubleProperty("ptau");
				return _Ptau;
			}else {
				return _Ptau;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ptau.
	 * @param v Value to Set.
	 */
	public void setPtau(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ptau",v);
		_Ptau=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Ptauunits=null;

	/**
	 * @return Returns the ptauUnits.
	 */
	public String getPtauunits(){
		try{
			if (_Ptauunits==null){
				_Ptauunits=getStringProperty("ptauUnits");
				return _Ptauunits;
			}else {
				return _Ptauunits;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ptauUnits.
	 * @param v Value to Set.
	 */
	public void setPtauunits(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ptauUnits",v);
		_Ptauunits=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Ab40=null;

	/**
	 * @return Returns the Ab40.
	 */
	public Double getAb40() {
		try{
			if (_Ab40==null){
				_Ab40=getDoubleProperty("Ab40");
				return _Ab40;
			}else {
				return _Ab40;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Ab40.
	 * @param v Value to Set.
	 */
	public void setAb40(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Ab40",v);
		_Ab40=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Ab40units=null;

	/**
	 * @return Returns the Ab40Units.
	 */
	public String getAb40units(){
		try{
			if (_Ab40units==null){
				_Ab40units=getStringProperty("Ab40Units");
				return _Ab40units;
			}else {
				return _Ab40units;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Ab40Units.
	 * @param v Value to Set.
	 */
	public void setAb40units(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Ab40Units",v);
		_Ab40units=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Ab40lot=null;

	/**
	 * @return Returns the Ab40Lot.
	 */
	public String getAb40lot(){
		try{
			if (_Ab40lot==null){
				_Ab40lot=getStringProperty("Ab40Lot");
				return _Ab40lot;
			}else {
				return _Ab40lot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Ab40Lot.
	 * @param v Value to Set.
	 */
	public void setAb40lot(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Ab40Lot",v);
		_Ab40lot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Ab40plate=null;

	/**
	 * @return Returns the Ab40Plate.
	 */
	public String getAb40plate(){
		try{
			if (_Ab40plate==null){
				_Ab40plate=getStringProperty("Ab40Plate");
				return _Ab40plate;
			}else {
				return _Ab40plate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Ab40Plate.
	 * @param v Value to Set.
	 */
	public void setAb40plate(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Ab40Plate",v);
		_Ab40plate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Ab40assaydate=null;

	/**
	 * @return Returns the Ab40AssayDate.
	 */
	public Object getAb40assaydate(){
		try{
			if (_Ab40assaydate==null){
				_Ab40assaydate=getProperty("Ab40AssayDate");
				return _Ab40assaydate;
			}else {
				return _Ab40assaydate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Ab40AssayDate.
	 * @param v Value to Set.
	 */
	public void setAb40assaydate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Ab40AssayDate",v);
		_Ab40assaydate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Ab42=null;

	/**
	 * @return Returns the Ab42.
	 */
	public Double getAb42() {
		try{
			if (_Ab42==null){
				_Ab42=getDoubleProperty("Ab42");
				return _Ab42;
			}else {
				return _Ab42;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Ab42.
	 * @param v Value to Set.
	 */
	public void setAb42(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Ab42",v);
		_Ab42=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Ab42units=null;

	/**
	 * @return Returns the Ab42Units.
	 */
	public String getAb42units(){
		try{
			if (_Ab42units==null){
				_Ab42units=getStringProperty("Ab42Units");
				return _Ab42units;
			}else {
				return _Ab42units;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Ab42Units.
	 * @param v Value to Set.
	 */
	public void setAb42units(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Ab42Units",v);
		_Ab42units=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Ab42lot=null;

	/**
	 * @return Returns the Ab42Lot.
	 */
	public String getAb42lot(){
		try{
			if (_Ab42lot==null){
				_Ab42lot=getStringProperty("Ab42Lot");
				return _Ab42lot;
			}else {
				return _Ab42lot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Ab42Lot.
	 * @param v Value to Set.
	 */
	public void setAb42lot(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Ab42Lot",v);
		_Ab42lot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Ab42plate=null;

	/**
	 * @return Returns the Ab42Plate.
	 */
	public String getAb42plate(){
		try{
			if (_Ab42plate==null){
				_Ab42plate=getStringProperty("Ab42Plate");
				return _Ab42plate;
			}else {
				return _Ab42plate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Ab42Plate.
	 * @param v Value to Set.
	 */
	public void setAb42plate(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Ab42Plate",v);
		_Ab42plate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Ab42assaydate=null;

	/**
	 * @return Returns the Ab42AssayDate.
	 */
	public Object getAb42assaydate(){
		try{
			if (_Ab42assaydate==null){
				_Ab42assaydate=getProperty("Ab42AssayDate");
				return _Ab42assaydate;
			}else {
				return _Ab42assaydate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Ab42AssayDate.
	 * @param v Value to Set.
	 */
	public void setAb42assaydate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Ab42AssayDate",v);
		_Ab42assaydate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Xmaptau=null;

	/**
	 * @return Returns the XMAPtau.
	 */
	public Double getXmaptau() {
		try{
			if (_Xmaptau==null){
				_Xmaptau=getDoubleProperty("XMAPtau");
				return _Xmaptau;
			}else {
				return _Xmaptau;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for XMAPtau.
	 * @param v Value to Set.
	 */
	public void setXmaptau(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/XMAPtau",v);
		_Xmaptau=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Xmaptauunits=null;

	/**
	 * @return Returns the XMAPtauUnits.
	 */
	public String getXmaptauunits(){
		try{
			if (_Xmaptauunits==null){
				_Xmaptauunits=getStringProperty("XMAPtauUnits");
				return _Xmaptauunits;
			}else {
				return _Xmaptauunits;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for XMAPtauUnits.
	 * @param v Value to Set.
	 */
	public void setXmaptauunits(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/XMAPtauUnits",v);
		_Xmaptauunits=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Xmaptaulot=null;

	/**
	 * @return Returns the XMAPtauLot.
	 */
	public String getXmaptaulot(){
		try{
			if (_Xmaptaulot==null){
				_Xmaptaulot=getStringProperty("XMAPtauLot");
				return _Xmaptaulot;
			}else {
				return _Xmaptaulot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for XMAPtauLot.
	 * @param v Value to Set.
	 */
	public void setXmaptaulot(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/XMAPtauLot",v);
		_Xmaptaulot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Xmaptauplate=null;

	/**
	 * @return Returns the XMAPtauPlate.
	 */
	public String getXmaptauplate(){
		try{
			if (_Xmaptauplate==null){
				_Xmaptauplate=getStringProperty("XMAPtauPlate");
				return _Xmaptauplate;
			}else {
				return _Xmaptauplate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for XMAPtauPlate.
	 * @param v Value to Set.
	 */
	public void setXmaptauplate(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/XMAPtauPlate",v);
		_Xmaptauplate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Xmaptauassaydate=null;

	/**
	 * @return Returns the XMAPtauAssayDate.
	 */
	public Object getXmaptauassaydate(){
		try{
			if (_Xmaptauassaydate==null){
				_Xmaptauassaydate=getProperty("XMAPtauAssayDate");
				return _Xmaptauassaydate;
			}else {
				return _Xmaptauassaydate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for XMAPtauAssayDate.
	 * @param v Value to Set.
	 */
	public void setXmaptauassaydate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/XMAPtauAssayDate",v);
		_Xmaptauassaydate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Xmapptau181=null;

	/**
	 * @return Returns the XMAPptau181.
	 */
	public Double getXmapptau181() {
		try{
			if (_Xmapptau181==null){
				_Xmapptau181=getDoubleProperty("XMAPptau181");
				return _Xmapptau181;
			}else {
				return _Xmapptau181;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for XMAPptau181.
	 * @param v Value to Set.
	 */
	public void setXmapptau181(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/XMAPptau181",v);
		_Xmapptau181=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Xmapptau181units=null;

	/**
	 * @return Returns the XMAPptau181Units.
	 */
	public String getXmapptau181units(){
		try{
			if (_Xmapptau181units==null){
				_Xmapptau181units=getStringProperty("XMAPptau181Units");
				return _Xmapptau181units;
			}else {
				return _Xmapptau181units;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for XMAPptau181Units.
	 * @param v Value to Set.
	 */
	public void setXmapptau181units(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/XMAPptau181Units",v);
		_Xmapptau181units=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Xmapptau181lot=null;

	/**
	 * @return Returns the XMAPptau181Lot.
	 */
	public String getXmapptau181lot(){
		try{
			if (_Xmapptau181lot==null){
				_Xmapptau181lot=getStringProperty("XMAPptau181Lot");
				return _Xmapptau181lot;
			}else {
				return _Xmapptau181lot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for XMAPptau181Lot.
	 * @param v Value to Set.
	 */
	public void setXmapptau181lot(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/XMAPptau181Lot",v);
		_Xmapptau181lot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Xmapptau181plate=null;

	/**
	 * @return Returns the XMAPptau181Plate.
	 */
	public String getXmapptau181plate(){
		try{
			if (_Xmapptau181plate==null){
				_Xmapptau181plate=getStringProperty("XMAPptau181Plate");
				return _Xmapptau181plate;
			}else {
				return _Xmapptau181plate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for XMAPptau181Plate.
	 * @param v Value to Set.
	 */
	public void setXmapptau181plate(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/XMAPptau181Plate",v);
		_Xmapptau181plate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Xmapptau181assaydate=null;

	/**
	 * @return Returns the XMAPptau181AssayDate.
	 */
	public Object getXmapptau181assaydate(){
		try{
			if (_Xmapptau181assaydate==null){
				_Xmapptau181assaydate=getProperty("XMAPptau181AssayDate");
				return _Xmapptau181assaydate;
			}else {
				return _Xmapptau181assaydate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for XMAPptau181AssayDate.
	 * @param v Value to Set.
	 */
	public void setXmapptau181assaydate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/XMAPptau181AssayDate",v);
		_Xmapptau181assaydate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Xmapab42=null;

	/**
	 * @return Returns the XMAPAb42.
	 */
	public Double getXmapab42() {
		try{
			if (_Xmapab42==null){
				_Xmapab42=getDoubleProperty("XMAPAb42");
				return _Xmapab42;
			}else {
				return _Xmapab42;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for XMAPAb42.
	 * @param v Value to Set.
	 */
	public void setXmapab42(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/XMAPAb42",v);
		_Xmapab42=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Xmapab42units=null;

	/**
	 * @return Returns the XMAPAb42Units.
	 */
	public String getXmapab42units(){
		try{
			if (_Xmapab42units==null){
				_Xmapab42units=getStringProperty("XMAPAb42Units");
				return _Xmapab42units;
			}else {
				return _Xmapab42units;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for XMAPAb42Units.
	 * @param v Value to Set.
	 */
	public void setXmapab42units(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/XMAPAb42Units",v);
		_Xmapab42units=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Xmapab42lot=null;

	/**
	 * @return Returns the XMAPAb42Lot.
	 */
	public String getXmapab42lot(){
		try{
			if (_Xmapab42lot==null){
				_Xmapab42lot=getStringProperty("XMAPAb42Lot");
				return _Xmapab42lot;
			}else {
				return _Xmapab42lot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for XMAPAb42Lot.
	 * @param v Value to Set.
	 */
	public void setXmapab42lot(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/XMAPAb42Lot",v);
		_Xmapab42lot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Xmapab42plate=null;

	/**
	 * @return Returns the XMAPAb42Plate.
	 */
	public String getXmapab42plate(){
		try{
			if (_Xmapab42plate==null){
				_Xmapab42plate=getStringProperty("XMAPAb42Plate");
				return _Xmapab42plate;
			}else {
				return _Xmapab42plate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for XMAPAb42Plate.
	 * @param v Value to Set.
	 */
	public void setXmapab42plate(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/XMAPAb42Plate",v);
		_Xmapab42plate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Xmapab42assaydate=null;

	/**
	 * @return Returns the XMAPAb42AssayDate.
	 */
	public Object getXmapab42assaydate(){
		try{
			if (_Xmapab42assaydate==null){
				_Xmapab42assaydate=getProperty("XMAPAb42AssayDate");
				return _Xmapab42assaydate;
			}else {
				return _Xmapab42assaydate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for XMAPAb42AssayDate.
	 * @param v Value to Set.
	 */
	public void setXmapab42assaydate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/XMAPAb42AssayDate",v);
		_Xmapab42assaydate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CsfAb40PlusQcfails=null;

	/**
	 * @return Returns the CSF_AB40_plus_QCFAILS.
	 */
	public Double getCsfAb40PlusQcfails() {
		try{
			if (_CsfAb40PlusQcfails==null){
				_CsfAb40PlusQcfails=getDoubleProperty("CSF_AB40_plus_QCFAILS");
				return _CsfAb40PlusQcfails;
			}else {
				return _CsfAb40PlusQcfails;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSF_AB40_plus_QCFAILS.
	 * @param v Value to Set.
	 */
	public void setCsfAb40PlusQcfails(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSF_AB40_plus_QCFAILS",v);
		_CsfAb40PlusQcfails=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _CsfAb40Comments=null;

	/**
	 * @return Returns the CSF_AB40_COMMENTS.
	 */
	public String getCsfAb40Comments(){
		try{
			if (_CsfAb40Comments==null){
				_CsfAb40Comments=getStringProperty("CSF_AB40_COMMENTS");
				return _CsfAb40Comments;
			}else {
				return _CsfAb40Comments;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSF_AB40_COMMENTS.
	 * @param v Value to Set.
	 */
	public void setCsfAb40Comments(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSF_AB40_COMMENTS",v);
		_CsfAb40Comments=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CsfAb42PlusQcfails=null;

	/**
	 * @return Returns the CSF_AB42_plus_QCFAILS.
	 */
	public Double getCsfAb42PlusQcfails() {
		try{
			if (_CsfAb42PlusQcfails==null){
				_CsfAb42PlusQcfails=getDoubleProperty("CSF_AB42_plus_QCFAILS");
				return _CsfAb42PlusQcfails;
			}else {
				return _CsfAb42PlusQcfails;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSF_AB42_plus_QCFAILS.
	 * @param v Value to Set.
	 */
	public void setCsfAb42PlusQcfails(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSF_AB42_plus_QCFAILS",v);
		_CsfAb42PlusQcfails=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _CsfAb42Comments=null;

	/**
	 * @return Returns the CSF_AB42_COMMENTS.
	 */
	public String getCsfAb42Comments(){
		try{
			if (_CsfAb42Comments==null){
				_CsfAb42Comments=getStringProperty("CSF_AB42_COMMENTS");
				return _CsfAb42Comments;
			}else {
				return _CsfAb42Comments;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSF_AB42_COMMENTS.
	 * @param v Value to Set.
	 */
	public void setCsfAb42Comments(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSF_AB42_COMMENTS",v);
		_CsfAb42Comments=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CsfXmapAb42PlusQcfails=null;

	/**
	 * @return Returns the CSF_XMAP_AB42_plus_QCFAILS.
	 */
	public Double getCsfXmapAb42PlusQcfails() {
		try{
			if (_CsfXmapAb42PlusQcfails==null){
				_CsfXmapAb42PlusQcfails=getDoubleProperty("CSF_XMAP_AB42_plus_QCFAILS");
				return _CsfXmapAb42PlusQcfails;
			}else {
				return _CsfXmapAb42PlusQcfails;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSF_XMAP_AB42_plus_QCFAILS.
	 * @param v Value to Set.
	 */
	public void setCsfXmapAb42PlusQcfails(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSF_XMAP_AB42_plus_QCFAILS",v);
		_CsfXmapAb42PlusQcfails=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _CsfXmapAb42Comments=null;

	/**
	 * @return Returns the CSF_XMAP_AB42_COMMENTS.
	 */
	public String getCsfXmapAb42Comments(){
		try{
			if (_CsfXmapAb42Comments==null){
				_CsfXmapAb42Comments=getStringProperty("CSF_XMAP_AB42_COMMENTS");
				return _CsfXmapAb42Comments;
			}else {
				return _CsfXmapAb42Comments;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSF_XMAP_AB42_COMMENTS.
	 * @param v Value to Set.
	 */
	public void setCsfXmapAb42Comments(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSF_XMAP_AB42_COMMENTS",v);
		_CsfXmapAb42Comments=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CsfXmaptauPlusQcfails=null;

	/**
	 * @return Returns the CSF_XMAPTAU_plus_QCFAILS.
	 */
	public Double getCsfXmaptauPlusQcfails() {
		try{
			if (_CsfXmaptauPlusQcfails==null){
				_CsfXmaptauPlusQcfails=getDoubleProperty("CSF_XMAPTAU_plus_QCFAILS");
				return _CsfXmaptauPlusQcfails;
			}else {
				return _CsfXmaptauPlusQcfails;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSF_XMAPTAU_plus_QCFAILS.
	 * @param v Value to Set.
	 */
	public void setCsfXmaptauPlusQcfails(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSF_XMAPTAU_plus_QCFAILS",v);
		_CsfXmaptauPlusQcfails=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _CsfXmaptauComments=null;

	/**
	 * @return Returns the CSF_XMAPTAU_COMMENTS.
	 */
	public String getCsfXmaptauComments(){
		try{
			if (_CsfXmaptauComments==null){
				_CsfXmaptauComments=getStringProperty("CSF_XMAPTAU_COMMENTS");
				return _CsfXmaptauComments;
			}else {
				return _CsfXmaptauComments;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSF_XMAPTAU_COMMENTS.
	 * @param v Value to Set.
	 */
	public void setCsfXmaptauComments(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSF_XMAPTAU_COMMENTS",v);
		_CsfXmaptauComments=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CsfXmapptau181PlusQcfails=null;

	/**
	 * @return Returns the CSF_XMAPPTAU181_plus_QCFAILS.
	 */
	public Double getCsfXmapptau181PlusQcfails() {
		try{
			if (_CsfXmapptau181PlusQcfails==null){
				_CsfXmapptau181PlusQcfails=getDoubleProperty("CSF_XMAPPTAU181_plus_QCFAILS");
				return _CsfXmapptau181PlusQcfails;
			}else {
				return _CsfXmapptau181PlusQcfails;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSF_XMAPPTAU181_plus_QCFAILS.
	 * @param v Value to Set.
	 */
	public void setCsfXmapptau181PlusQcfails(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSF_XMAPPTAU181_plus_QCFAILS",v);
		_CsfXmapptau181PlusQcfails=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _CsfXmapptau181Comments=null;

	/**
	 * @return Returns the CSF_XMAPPTAU181_COMMENTS.
	 */
	public String getCsfXmapptau181Comments(){
		try{
			if (_CsfXmapptau181Comments==null){
				_CsfXmapptau181Comments=getStringProperty("CSF_XMAPPTAU181_COMMENTS");
				return _CsfXmapptau181Comments;
			}else {
				return _CsfXmapptau181Comments;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSF_XMAPPTAU181_COMMENTS.
	 * @param v Value to Set.
	 */
	public void setCsfXmapptau181Comments(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSF_XMAPPTAU181_COMMENTS",v);
		_CsfXmapptau181Comments=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianCsfdata> getAllDianCsfdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianCsfdata> al = new ArrayList<org.nrg.xdat.om.DianCsfdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianCsfdata> getDianCsfdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianCsfdata> al = new ArrayList<org.nrg.xdat.om.DianCsfdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianCsfdata> getDianCsfdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianCsfdata> al = new ArrayList<org.nrg.xdat.om.DianCsfdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianCsfdata getDianCsfdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:csfData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianCsfdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
