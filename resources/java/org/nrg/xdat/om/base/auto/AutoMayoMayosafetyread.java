/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoMayoMayosafetyread extends XnatSubjectassessordata implements org.nrg.xdat.model.MayoMayosafetyreadI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoMayoMayosafetyread.class);
	public static String SCHEMA_ELEMENT_NAME="mayo:mayoSafetyRead";

	public AutoMayoMayosafetyread(ItemI item)
	{
		super(item);
	}

	public AutoMayoMayosafetyread(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoMayoMayosafetyread(UserI user)
	 **/
	public AutoMayoMayosafetyread(){}

	public AutoMayoMayosafetyread(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "mayo:mayoSafetyRead";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Flagforreview=null;

	/**
	 * @return Returns the flagForReview.
	 */
	public Boolean getFlagforreview() {
		try{
			if (_Flagforreview==null){
				_Flagforreview=getBooleanProperty("flagForReview");
				return _Flagforreview;
			}else {
				return _Flagforreview;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for flagForReview.
	 * @param v Value to Set.
	 */
	public void setFlagforreview(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/flagForReview",v);
		_Flagforreview=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.MayoMayosafetyreadFinding> _Findings_finding =null;

	/**
	 * findings/finding
	 * @return Returns an List of org.nrg.xdat.om.MayoMayosafetyreadFinding
	 */
	public <A extends org.nrg.xdat.model.MayoMayosafetyreadFindingI> List<A> getFindings_finding() {
		try{
			if (_Findings_finding==null){
				_Findings_finding=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("findings/finding"));
			}
			return (List<A>) _Findings_finding;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.MayoMayosafetyreadFinding>();}
	}

	/**
	 * Sets the value for findings/finding.
	 * @param v Value to Set.
	 */
	public void setFindings_finding(ItemI v) throws Exception{
		_Findings_finding =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/findings/finding",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/findings/finding",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * findings/finding
	 * Adds org.nrg.xdat.model.MayoMayosafetyreadFindingI
	 */
	public <A extends org.nrg.xdat.model.MayoMayosafetyreadFindingI> void addFindings_finding(A item) throws Exception{
	setFindings_finding((ItemI)item);
	}

	/**
	 * Removes the findings/finding of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeFindings_finding(int index) throws java.lang.IndexOutOfBoundsException {
		_Findings_finding =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/findings/finding",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.MayoMayosafetyread> getAllMayoMayosafetyreads(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.MayoMayosafetyread> al = new ArrayList<org.nrg.xdat.om.MayoMayosafetyread>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.MayoMayosafetyread> getMayoMayosafetyreadsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.MayoMayosafetyread> al = new ArrayList<org.nrg.xdat.om.MayoMayosafetyread>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.MayoMayosafetyread> getMayoMayosafetyreadsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.MayoMayosafetyread> al = new ArrayList<org.nrg.xdat.om.MayoMayosafetyread>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static MayoMayosafetyread getMayoMayosafetyreadsById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("mayo:mayoSafetyRead/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (MayoMayosafetyread) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //findings/finding
	        for(org.nrg.xdat.model.MayoMayosafetyreadFindingI childFindings_finding : this.getFindings_finding()){
	            if (childFindings_finding!=null){
	              for(ResourceFile rf: ((MayoMayosafetyreadFinding)childFindings_finding).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("findings/finding[" + ((MayoMayosafetyreadFinding)childFindings_finding).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("findings/finding/" + ((MayoMayosafetyreadFinding)childFindings_finding).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
