/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoLs2Lsdemographicdata extends XnatSubjectassessordata implements org.nrg.xdat.model.Ls2LsdemographicdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoLs2Lsdemographicdata.class);
	public static String SCHEMA_ELEMENT_NAME="ls2:lsDemographicData";

	public AutoLs2Lsdemographicdata(ItemI item)
	{
		super(item);
	}

	public AutoLs2Lsdemographicdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoLs2Lsdemographicdata(UserI user)
	 **/
	public AutoLs2Lsdemographicdata(){}

	public AutoLs2Lsdemographicdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "ls2:lsDemographicData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Educationlevel=null;

	/**
	 * @return Returns the educationLevel.
	 */
	public Integer getEducationlevel() {
		try{
			if (_Educationlevel==null){
				_Educationlevel=getIntegerProperty("educationLevel");
				return _Educationlevel;
			}else {
				return _Educationlevel;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for educationLevel.
	 * @param v Value to Set.
	 */
	public void setEducationlevel(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/educationLevel",v);
		_Educationlevel=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Maritalstatus=null;

	/**
	 * @return Returns the maritalStatus.
	 */
	public Integer getMaritalstatus() {
		try{
			if (_Maritalstatus==null){
				_Maritalstatus=getIntegerProperty("maritalStatus");
				return _Maritalstatus;
			}else {
				return _Maritalstatus;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for maritalStatus.
	 * @param v Value to Set.
	 */
	public void setMaritalstatus(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/maritalStatus",v);
		_Maritalstatus=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Maritalstatuso=null;

	/**
	 * @return Returns the maritalStatusO.
	 */
	public String getMaritalstatuso(){
		try{
			if (_Maritalstatuso==null){
				_Maritalstatuso=getStringProperty("maritalStatusO");
				return _Maritalstatuso;
			}else {
				return _Maritalstatuso;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for maritalStatusO.
	 * @param v Value to Set.
	 */
	public void setMaritalstatuso(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/maritalStatusO",v);
		_Maritalstatuso=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Housing=null;

	/**
	 * @return Returns the housing.
	 */
	public Integer getHousing() {
		try{
			if (_Housing==null){
				_Housing=getIntegerProperty("housing");
				return _Housing;
			}else {
				return _Housing;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for housing.
	 * @param v Value to Set.
	 */
	public void setHousing(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/housing",v);
		_Housing=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Housingo=null;

	/**
	 * @return Returns the housingO.
	 */
	public String getHousingo(){
		try{
			if (_Housingo==null){
				_Housingo=getStringProperty("housingO");
				return _Housingo;
			}else {
				return _Housingo;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for housingO.
	 * @param v Value to Set.
	 */
	public void setHousingo(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/housingO",v);
		_Housingo=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Livealone=null;

	/**
	 * @return Returns the liveAlone.
	 */
	public Integer getLivealone() {
		try{
			if (_Livealone==null){
				_Livealone=getIntegerProperty("liveAlone");
				return _Livealone;
			}else {
				return _Livealone;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for liveAlone.
	 * @param v Value to Set.
	 */
	public void setLivealone(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/liveAlone",v);
		_Livealone=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Engnow=null;

	/**
	 * @return Returns the engNow.
	 */
	public Integer getEngnow() {
		try{
			if (_Engnow==null){
				_Engnow=getIntegerProperty("engNow");
				return _Engnow;
			}else {
				return _Engnow;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for engNow.
	 * @param v Value to Set.
	 */
	public void setEngnow(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/engNow",v);
		_Engnow=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Engnowo=null;

	/**
	 * @return Returns the engNowO.
	 */
	public String getEngnowo(){
		try{
			if (_Engnowo==null){
				_Engnowo=getStringProperty("engNowO");
				return _Engnowo;
			}else {
				return _Engnowo;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for engNowO.
	 * @param v Value to Set.
	 */
	public void setEngnowo(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/engNowO",v);
		_Engnowo=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Engfirst=null;

	/**
	 * @return Returns the engFirst.
	 */
	public Integer getEngfirst() {
		try{
			if (_Engfirst==null){
				_Engfirst=getIntegerProperty("engFirst");
				return _Engfirst;
			}else {
				return _Engfirst;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for engFirst.
	 * @param v Value to Set.
	 */
	public void setEngfirst(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/engFirst",v);
		_Engfirst=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Engfirsto=null;

	/**
	 * @return Returns the engFirstO.
	 */
	public String getEngfirsto(){
		try{
			if (_Engfirsto==null){
				_Engfirsto=getStringProperty("engFirstO");
				return _Engfirsto;
			}else {
				return _Engfirsto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for engFirstO.
	 * @param v Value to Set.
	 */
	public void setEngfirsto(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/engFirstO",v);
		_Engfirsto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wrkft=null;

	/**
	 * @return Returns the wrkFt.
	 */
	public Integer getWrkft() {
		try{
			if (_Wrkft==null){
				_Wrkft=getIntegerProperty("wrkFt");
				return _Wrkft;
			}else {
				return _Wrkft;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wrkFt.
	 * @param v Value to Set.
	 */
	public void setWrkft(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wrkFt",v);
		_Wrkft=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wrkpt=null;

	/**
	 * @return Returns the wrkPt.
	 */
	public Integer getWrkpt() {
		try{
			if (_Wrkpt==null){
				_Wrkpt=getIntegerProperty("wrkPt");
				return _Wrkpt;
			}else {
				return _Wrkpt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wrkPt.
	 * @param v Value to Set.
	 */
	public void setWrkpt(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wrkPt",v);
		_Wrkpt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wrkstu=null;

	/**
	 * @return Returns the wrkStu.
	 */
	public Integer getWrkstu() {
		try{
			if (_Wrkstu==null){
				_Wrkstu=getIntegerProperty("wrkStu");
				return _Wrkstu;
			}else {
				return _Wrkstu;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wrkStu.
	 * @param v Value to Set.
	 */
	public void setWrkstu(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wrkStu",v);
		_Wrkstu=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wrkhome=null;

	/**
	 * @return Returns the wrkHome.
	 */
	public Integer getWrkhome() {
		try{
			if (_Wrkhome==null){
				_Wrkhome=getIntegerProperty("wrkHome");
				return _Wrkhome;
			}else {
				return _Wrkhome;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wrkHome.
	 * @param v Value to Set.
	 */
	public void setWrkhome(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wrkHome",v);
		_Wrkhome=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wrkret=null;

	/**
	 * @return Returns the wrkRet.
	 */
	public Integer getWrkret() {
		try{
			if (_Wrkret==null){
				_Wrkret=getIntegerProperty("wrkRet");
				return _Wrkret;
			}else {
				return _Wrkret;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wrkRet.
	 * @param v Value to Set.
	 */
	public void setWrkret(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wrkRet",v);
		_Wrkret=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wrkvol=null;

	/**
	 * @return Returns the wrkVol.
	 */
	public Integer getWrkvol() {
		try{
			if (_Wrkvol==null){
				_Wrkvol=getIntegerProperty("wrkVol");
				return _Wrkvol;
			}else {
				return _Wrkvol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wrkVol.
	 * @param v Value to Set.
	 */
	public void setWrkvol(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wrkVol",v);
		_Wrkvol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wrkseek=null;

	/**
	 * @return Returns the wrkSeek.
	 */
	public Integer getWrkseek() {
		try{
			if (_Wrkseek==null){
				_Wrkseek=getIntegerProperty("wrkSeek");
				return _Wrkseek;
			}else {
				return _Wrkseek;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wrkSeek.
	 * @param v Value to Set.
	 */
	public void setWrkseek(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wrkSeek",v);
		_Wrkseek=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wrkleave=null;

	/**
	 * @return Returns the wrkLeave.
	 */
	public Integer getWrkleave() {
		try{
			if (_Wrkleave==null){
				_Wrkleave=getIntegerProperty("wrkLeave");
				return _Wrkleave;
			}else {
				return _Wrkleave;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wrkLeave.
	 * @param v Value to Set.
	 */
	public void setWrkleave(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wrkLeave",v);
		_Wrkleave=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wrkoth=null;

	/**
	 * @return Returns the wrkOth.
	 */
	public Integer getWrkoth() {
		try{
			if (_Wrkoth==null){
				_Wrkoth=getIntegerProperty("wrkOth");
				return _Wrkoth;
			}else {
				return _Wrkoth;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wrkOth.
	 * @param v Value to Set.
	 */
	public void setWrkoth(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wrkOth",v);
		_Wrkoth=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wrkstat=null;

	/**
	 * @return Returns the wrkStat.
	 */
	public Integer getWrkstat() {
		try{
			if (_Wrkstat==null){
				_Wrkstat=getIntegerProperty("wrkStat");
				return _Wrkstat;
			}else {
				return _Wrkstat;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wrkStat.
	 * @param v Value to Set.
	 */
	public void setWrkstat(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wrkStat",v);
		_Wrkstat=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Currocc=null;

	/**
	 * @return Returns the currOcc.
	 */
	public String getCurrocc(){
		try{
			if (_Currocc==null){
				_Currocc=getStringProperty("currOcc");
				return _Currocc;
			}else {
				return _Currocc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for currOcc.
	 * @param v Value to Set.
	 */
	public void setCurrocc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/currOcc",v);
		_Currocc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Retocc=null;

	/**
	 * @return Returns the retOcc.
	 */
	public String getRetocc(){
		try{
			if (_Retocc==null){
				_Retocc=getStringProperty("retOcc");
				return _Retocc;
			}else {
				return _Retocc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for retOcc.
	 * @param v Value to Set.
	 */
	public void setRetocc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/retOcc",v);
		_Retocc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Retyr=null;

	/**
	 * @return Returns the retYr.
	 */
	public Object getRetyr(){
		try{
			if (_Retyr==null){
				_Retyr=getProperty("retYr");
				return _Retyr;
			}else {
				return _Retyr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for retYr.
	 * @param v Value to Set.
	 */
	public void setRetyr(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/retYr",v);
		_Retyr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Height=null;

	/**
	 * @return Returns the height.
	 */
	public Integer getHeight() {
		try{
			if (_Height==null){
				_Height=getIntegerProperty("height");
				return _Height;
			}else {
				return _Height;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for height.
	 * @param v Value to Set.
	 */
	public void setHeight(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/height",v);
		_Height=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Weight=null;

	/**
	 * @return Returns the weight.
	 */
	public Integer getWeight() {
		try{
			if (_Weight==null){
				_Weight=getIntegerProperty("weight");
				return _Weight;
			}else {
				return _Weight;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for weight.
	 * @param v Value to Set.
	 */
	public void setWeight(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/weight",v);
		_Weight=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Hand=null;

	/**
	 * @return Returns the hand.
	 */
	public Integer getHand() {
		try{
			if (_Hand==null){
				_Hand=getIntegerProperty("hand");
				return _Hand;
			}else {
				return _Hand;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for hand.
	 * @param v Value to Set.
	 */
	public void setHand(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/hand",v);
		_Hand=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ethnic=null;

	/**
	 * @return Returns the ethnic.
	 */
	public Integer getEthnic() {
		try{
			if (_Ethnic==null){
				_Ethnic=getIntegerProperty("ethnic");
				return _Ethnic;
			}else {
				return _Ethnic;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ethnic.
	 * @param v Value to Set.
	 */
	public void setEthnic(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ethnic",v);
		_Ethnic=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Race1=null;

	/**
	 * @return Returns the race1.
	 */
	public Integer getRace1() {
		try{
			if (_Race1==null){
				_Race1=getIntegerProperty("race1");
				return _Race1;
			}else {
				return _Race1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for race1.
	 * @param v Value to Set.
	 */
	public void setRace1(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/race1",v);
		_Race1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Race2=null;

	/**
	 * @return Returns the race2.
	 */
	public Integer getRace2() {
		try{
			if (_Race2==null){
				_Race2=getIntegerProperty("race2");
				return _Race2;
			}else {
				return _Race2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for race2.
	 * @param v Value to Set.
	 */
	public void setRace2(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/race2",v);
		_Race2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.Ls2Lsdemographicdata> getAllLs2Lsdemographicdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.Ls2Lsdemographicdata> al = new ArrayList<org.nrg.xdat.om.Ls2Lsdemographicdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.Ls2Lsdemographicdata> getLs2LsdemographicdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.Ls2Lsdemographicdata> al = new ArrayList<org.nrg.xdat.om.Ls2Lsdemographicdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.Ls2Lsdemographicdata> getLs2LsdemographicdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.Ls2Lsdemographicdata> al = new ArrayList<org.nrg.xdat.om.Ls2Lsdemographicdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static Ls2Lsdemographicdata getLs2LsdemographicdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("ls2:lsDemographicData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (Ls2Lsdemographicdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
