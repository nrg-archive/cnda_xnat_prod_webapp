/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoIqAbstractiqtest extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.IqAbstractiqtestI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoIqAbstractiqtest.class);
	public static String SCHEMA_ELEMENT_NAME="iq:abstractIQTest";

	public AutoIqAbstractiqtest(ItemI item)
	{
		super(item);
	}

	public AutoIqAbstractiqtest(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoIqAbstractiqtest(UserI user)
	 **/
	public AutoIqAbstractiqtest(){}

	public AutoIqAbstractiqtest(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "iq:abstractIQTest";
	}

	//FIELD

	private String _Note=null;

	/**
	 * @return Returns the note.
	 */
	public String getNote(){
		try{
			if (_Note==null){
				_Note=getStringProperty("note");
				return _Note;
			}else {
				return _Note;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for note.
	 * @param v Value to Set.
	 */
	public void setNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/note",v);
		_Note=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _IqAbstractiqtestId=null;

	/**
	 * @return Returns the iq_abstractIQTest_id.
	 */
	public Integer getIqAbstractiqtestId() {
		try{
			if (_IqAbstractiqtestId==null){
				_IqAbstractiqtestId=getIntegerProperty("iq_abstractIQTest_id");
				return _IqAbstractiqtestId;
			}else {
				return _IqAbstractiqtestId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for iq_abstractIQTest_id.
	 * @param v Value to Set.
	 */
	public void setIqAbstractiqtestId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/iq_abstractIQTest_id",v);
		_IqAbstractiqtestId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.IqAbstractiqtest> getAllIqAbstractiqtests(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.IqAbstractiqtest> al = new ArrayList<org.nrg.xdat.om.IqAbstractiqtest>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.IqAbstractiqtest> getIqAbstractiqtestsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.IqAbstractiqtest> al = new ArrayList<org.nrg.xdat.om.IqAbstractiqtest>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.IqAbstractiqtest> getIqAbstractiqtestsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.IqAbstractiqtest> al = new ArrayList<org.nrg.xdat.om.IqAbstractiqtest>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static IqAbstractiqtest getIqAbstractiqtestsByIqAbstractiqtestId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("iq:abstractIQTest/iq_abstractIQTest_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (IqAbstractiqtest) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        localLoop = preventLoop;
	
	return _return;
}
}
