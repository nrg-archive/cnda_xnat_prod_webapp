/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoPetFspettimecoursedata extends XnatPetassessordata implements org.nrg.xdat.model.PetFspettimecoursedataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoPetFspettimecoursedata.class);
	public static String SCHEMA_ELEMENT_NAME="pet:fspetTimeCourseData";

	public AutoPetFspettimecoursedata(ItemI item)
	{
		super(item);
	}

	public AutoPetFspettimecoursedata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoPetFspettimecoursedata(UserI user)
	 **/
	public AutoPetFspettimecoursedata(){}

	public AutoPetFspettimecoursedata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "pet:fspetTimeCourseData";
	}
	 private org.nrg.xdat.om.XnatPetassessordata _Petassessordata =null;

	/**
	 * petAssessorData
	 * @return org.nrg.xdat.om.XnatPetassessordata
	 */
	public org.nrg.xdat.om.XnatPetassessordata getPetassessordata() {
		try{
			if (_Petassessordata==null){
				_Petassessordata=((XnatPetassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("petAssessorData")));
				return _Petassessordata;
			}else {
				return _Petassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for petAssessorData.
	 * @param v Value to Set.
	 */
	public void setPetassessordata(ItemI v) throws Exception{
		_Petassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/petAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/petAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * petAssessorData
	 * set org.nrg.xdat.model.XnatPetassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatPetassessordataI> void setPetassessordata(A item) throws Exception{
	setPetassessordata((ItemI)item);
	}

	/**
	 * Removes the petAssessorData.
	 * */
	public void removePetassessordata() {
		_Petassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/petAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.PetFspettimecoursedataRoi> _Rois_roi =null;

	/**
	 * rois/roi
	 * @return Returns an List of org.nrg.xdat.om.PetFspettimecoursedataRoi
	 */
	public <A extends org.nrg.xdat.model.PetFspettimecoursedataRoiI> List<A> getRois_roi() {
		try{
			if (_Rois_roi==null){
				_Rois_roi=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("rois/roi"));
			}
			return (List<A>) _Rois_roi;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.PetFspettimecoursedataRoi>();}
	}

	/**
	 * Sets the value for rois/roi.
	 * @param v Value to Set.
	 */
	public void setRois_roi(ItemI v) throws Exception{
		_Rois_roi =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/rois/roi",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/rois/roi",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * rois/roi
	 * Adds org.nrg.xdat.model.PetFspettimecoursedataRoiI
	 */
	public <A extends org.nrg.xdat.model.PetFspettimecoursedataRoiI> void addRois_roi(A item) throws Exception{
	setRois_roi((ItemI)item);
	}

	/**
	 * Removes the rois/roi of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeRois_roi(int index) throws java.lang.IndexOutOfBoundsException {
		_Rois_roi =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/rois/roi",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.PetFspettimecoursedata> getAllPetFspettimecoursedatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.PetFspettimecoursedata> al = new ArrayList<org.nrg.xdat.om.PetFspettimecoursedata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.PetFspettimecoursedata> getPetFspettimecoursedatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.PetFspettimecoursedata> al = new ArrayList<org.nrg.xdat.om.PetFspettimecoursedata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.PetFspettimecoursedata> getPetFspettimecoursedatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.PetFspettimecoursedata> al = new ArrayList<org.nrg.xdat.om.PetFspettimecoursedata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static PetFspettimecoursedata getPetFspettimecoursedatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("pet:fspetTimeCourseData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (PetFspettimecoursedata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //petAssessorData
	        XnatPetassessordata childPetassessordata = (XnatPetassessordata)this.getPetassessordata();
	            if (childPetassessordata!=null){
	              for(ResourceFile rf: ((XnatPetassessordata)childPetassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("petAssessorData[" + ((XnatPetassessordata)childPetassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("petAssessorData/" + ((XnatPetassessordata)childPetassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //rois/roi
	        for(org.nrg.xdat.model.PetFspettimecoursedataRoiI childRois_roi : this.getRois_roi()){
	            if (childRois_roi!=null){
	              for(ResourceFile rf: ((PetFspettimecoursedataRoi)childRois_roi).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("rois/roi[" + ((PetFspettimecoursedataRoi)childRois_roi).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("rois/roi/" + ((PetFspettimecoursedataRoi)childRois_roi).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
