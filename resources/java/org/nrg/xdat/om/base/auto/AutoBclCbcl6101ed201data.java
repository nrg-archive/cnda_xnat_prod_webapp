/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoBclCbcl6101ed201data extends XnatSubjectassessordata implements org.nrg.xdat.model.BclCbcl6101ed201dataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoBclCbcl6101ed201data.class);
	public static String SCHEMA_ELEMENT_NAME="bcl:cbcl6-1-01Ed201Data";

	public AutoBclCbcl6101ed201data(ItemI item)
	{
		super(item);
	}

	public AutoBclCbcl6101ed201data(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoBclCbcl6101ed201data(UserI user)
	 **/
	public AutoBclCbcl6101ed201data(){}

	public AutoBclCbcl6101ed201data(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "bcl:cbcl6-1-01Ed201Data";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _AnxdpRaw=null;

	/**
	 * @return Returns the anxdp_raw.
	 */
	public Integer getAnxdpRaw() {
		try{
			if (_AnxdpRaw==null){
				_AnxdpRaw=getIntegerProperty("anxdp_raw");
				return _AnxdpRaw;
			}else {
				return _AnxdpRaw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for anxdp_raw.
	 * @param v Value to Set.
	 */
	public void setAnxdpRaw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/anxdp_raw",v);
		_AnxdpRaw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _AnxdpT=null;

	/**
	 * @return Returns the anxdp_t.
	 */
	public Integer getAnxdpT() {
		try{
			if (_AnxdpT==null){
				_AnxdpT=getIntegerProperty("anxdp_t");
				return _AnxdpT;
			}else {
				return _AnxdpT;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for anxdp_t.
	 * @param v Value to Set.
	 */
	public void setAnxdpT(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/anxdp_t",v);
		_AnxdpT=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _AnxdpNote=null;

	/**
	 * @return Returns the anxdp_note.
	 */
	public String getAnxdpNote(){
		try{
			if (_AnxdpNote==null){
				_AnxdpNote=getStringProperty("anxdp_note");
				return _AnxdpNote;
			}else {
				return _AnxdpNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for anxdp_note.
	 * @param v Value to Set.
	 */
	public void setAnxdpNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/anxdp_note",v);
		_AnxdpNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _WthdpRaw=null;

	/**
	 * @return Returns the wthdp_raw.
	 */
	public Integer getWthdpRaw() {
		try{
			if (_WthdpRaw==null){
				_WthdpRaw=getIntegerProperty("wthdp_raw");
				return _WthdpRaw;
			}else {
				return _WthdpRaw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wthdp_raw.
	 * @param v Value to Set.
	 */
	public void setWthdpRaw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wthdp_raw",v);
		_WthdpRaw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _WthdpT=null;

	/**
	 * @return Returns the wthdp_t.
	 */
	public Integer getWthdpT() {
		try{
			if (_WthdpT==null){
				_WthdpT=getIntegerProperty("wthdp_t");
				return _WthdpT;
			}else {
				return _WthdpT;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wthdp_t.
	 * @param v Value to Set.
	 */
	public void setWthdpT(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wthdp_t",v);
		_WthdpT=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _WthdpNote=null;

	/**
	 * @return Returns the wthdp_note.
	 */
	public String getWthdpNote(){
		try{
			if (_WthdpNote==null){
				_WthdpNote=getStringProperty("wthdp_note");
				return _WthdpNote;
			}else {
				return _WthdpNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wthdp_note.
	 * @param v Value to Set.
	 */
	public void setWthdpNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wthdp_note",v);
		_WthdpNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _SomRaw=null;

	/**
	 * @return Returns the som_raw.
	 */
	public Integer getSomRaw() {
		try{
			if (_SomRaw==null){
				_SomRaw=getIntegerProperty("som_raw");
				return _SomRaw;
			}else {
				return _SomRaw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for som_raw.
	 * @param v Value to Set.
	 */
	public void setSomRaw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/som_raw",v);
		_SomRaw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _SomT=null;

	/**
	 * @return Returns the som_t.
	 */
	public Integer getSomT() {
		try{
			if (_SomT==null){
				_SomT=getIntegerProperty("som_t");
				return _SomT;
			}else {
				return _SomT;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for som_t.
	 * @param v Value to Set.
	 */
	public void setSomT(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/som_t",v);
		_SomT=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _SomNote=null;

	/**
	 * @return Returns the som_note.
	 */
	public String getSomNote(){
		try{
			if (_SomNote==null){
				_SomNote=getStringProperty("som_note");
				return _SomNote;
			}else {
				return _SomNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for som_note.
	 * @param v Value to Set.
	 */
	public void setSomNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/som_note",v);
		_SomNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _SocRaw=null;

	/**
	 * @return Returns the soc_raw.
	 */
	public Integer getSocRaw() {
		try{
			if (_SocRaw==null){
				_SocRaw=getIntegerProperty("soc_raw");
				return _SocRaw;
			}else {
				return _SocRaw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for soc_raw.
	 * @param v Value to Set.
	 */
	public void setSocRaw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/soc_raw",v);
		_SocRaw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _SocT=null;

	/**
	 * @return Returns the soc_t.
	 */
	public Integer getSocT() {
		try{
			if (_SocT==null){
				_SocT=getIntegerProperty("soc_t");
				return _SocT;
			}else {
				return _SocT;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for soc_t.
	 * @param v Value to Set.
	 */
	public void setSocT(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/soc_t",v);
		_SocT=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _SocNote=null;

	/**
	 * @return Returns the soc_note.
	 */
	public String getSocNote(){
		try{
			if (_SocNote==null){
				_SocNote=getStringProperty("soc_note");
				return _SocNote;
			}else {
				return _SocNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for soc_note.
	 * @param v Value to Set.
	 */
	public void setSocNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/soc_note",v);
		_SocNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _ThoRaw=null;

	/**
	 * @return Returns the tho_raw.
	 */
	public Integer getThoRaw() {
		try{
			if (_ThoRaw==null){
				_ThoRaw=getIntegerProperty("tho_raw");
				return _ThoRaw;
			}else {
				return _ThoRaw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tho_raw.
	 * @param v Value to Set.
	 */
	public void setThoRaw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tho_raw",v);
		_ThoRaw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _ThoT=null;

	/**
	 * @return Returns the tho_t.
	 */
	public Integer getThoT() {
		try{
			if (_ThoT==null){
				_ThoT=getIntegerProperty("tho_t");
				return _ThoT;
			}else {
				return _ThoT;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tho_t.
	 * @param v Value to Set.
	 */
	public void setThoT(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tho_t",v);
		_ThoT=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _ThoNote=null;

	/**
	 * @return Returns the tho_note.
	 */
	public String getThoNote(){
		try{
			if (_ThoNote==null){
				_ThoNote=getStringProperty("tho_note");
				return _ThoNote;
			}else {
				return _ThoNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tho_note.
	 * @param v Value to Set.
	 */
	public void setThoNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tho_note",v);
		_ThoNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _AttRaw=null;

	/**
	 * @return Returns the att_raw.
	 */
	public Integer getAttRaw() {
		try{
			if (_AttRaw==null){
				_AttRaw=getIntegerProperty("att_raw");
				return _AttRaw;
			}else {
				return _AttRaw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for att_raw.
	 * @param v Value to Set.
	 */
	public void setAttRaw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/att_raw",v);
		_AttRaw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _AttT=null;

	/**
	 * @return Returns the att_t.
	 */
	public Integer getAttT() {
		try{
			if (_AttT==null){
				_AttT=getIntegerProperty("att_t");
				return _AttT;
			}else {
				return _AttT;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for att_t.
	 * @param v Value to Set.
	 */
	public void setAttT(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/att_t",v);
		_AttT=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _AttNote=null;

	/**
	 * @return Returns the att_note.
	 */
	public String getAttNote(){
		try{
			if (_AttNote==null){
				_AttNote=getStringProperty("att_note");
				return _AttNote;
			}else {
				return _AttNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for att_note.
	 * @param v Value to Set.
	 */
	public void setAttNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/att_note",v);
		_AttNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _RuleRaw=null;

	/**
	 * @return Returns the rule_raw.
	 */
	public Integer getRuleRaw() {
		try{
			if (_RuleRaw==null){
				_RuleRaw=getIntegerProperty("rule_raw");
				return _RuleRaw;
			}else {
				return _RuleRaw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for rule_raw.
	 * @param v Value to Set.
	 */
	public void setRuleRaw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/rule_raw",v);
		_RuleRaw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _RuleT=null;

	/**
	 * @return Returns the rule_t.
	 */
	public Integer getRuleT() {
		try{
			if (_RuleT==null){
				_RuleT=getIntegerProperty("rule_t");
				return _RuleT;
			}else {
				return _RuleT;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for rule_t.
	 * @param v Value to Set.
	 */
	public void setRuleT(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/rule_t",v);
		_RuleT=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _RuleNote=null;

	/**
	 * @return Returns the rule_note.
	 */
	public String getRuleNote(){
		try{
			if (_RuleNote==null){
				_RuleNote=getStringProperty("rule_note");
				return _RuleNote;
			}else {
				return _RuleNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for rule_note.
	 * @param v Value to Set.
	 */
	public void setRuleNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/rule_note",v);
		_RuleNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _AggRaw=null;

	/**
	 * @return Returns the agg_raw.
	 */
	public Integer getAggRaw() {
		try{
			if (_AggRaw==null){
				_AggRaw=getIntegerProperty("agg_raw");
				return _AggRaw;
			}else {
				return _AggRaw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for agg_raw.
	 * @param v Value to Set.
	 */
	public void setAggRaw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/agg_raw",v);
		_AggRaw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _AggT=null;

	/**
	 * @return Returns the agg_t.
	 */
	public Integer getAggT() {
		try{
			if (_AggT==null){
				_AggT=getIntegerProperty("agg_t");
				return _AggT;
			}else {
				return _AggT;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for agg_t.
	 * @param v Value to Set.
	 */
	public void setAggT(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/agg_t",v);
		_AggT=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _AggNote=null;

	/**
	 * @return Returns the agg_note.
	 */
	public String getAggNote(){
		try{
			if (_AggNote==null){
				_AggNote=getStringProperty("agg_note");
				return _AggNote;
			}else {
				return _AggNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for agg_note.
	 * @param v Value to Set.
	 */
	public void setAggNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/agg_note",v);
		_AggNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_resprltnsubj=null;

	/**
	 * @return Returns the ScoreForm/respRltnSubj.
	 */
	public String getScoreform_resprltnsubj(){
		try{
			if (_Scoreform_resprltnsubj==null){
				_Scoreform_resprltnsubj=getStringProperty("ScoreForm/respRltnSubj");
				return _Scoreform_resprltnsubj;
			}else {
				return _Scoreform_resprltnsubj;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/respRltnSubj.
	 * @param v Value to Set.
	 */
	public void setScoreform_resprltnsubj(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/respRltnSubj",v);
		_Scoreform_resprltnsubj=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_respgender=null;

	/**
	 * @return Returns the ScoreForm/respGender.
	 */
	public String getScoreform_respgender(){
		try{
			if (_Scoreform_respgender==null){
				_Scoreform_respgender=getStringProperty("ScoreForm/respGender");
				return _Scoreform_respgender;
			}else {
				return _Scoreform_respgender;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/respGender.
	 * @param v Value to Set.
	 */
	public void setScoreform_respgender(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/respGender",v);
		_Scoreform_respgender=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_fatherwork=null;

	/**
	 * @return Returns the ScoreForm/fatherWork.
	 */
	public String getScoreform_fatherwork(){
		try{
			if (_Scoreform_fatherwork==null){
				_Scoreform_fatherwork=getStringProperty("ScoreForm/fatherWork");
				return _Scoreform_fatherwork;
			}else {
				return _Scoreform_fatherwork;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/fatherWork.
	 * @param v Value to Set.
	 */
	public void setScoreform_fatherwork(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/fatherWork",v);
		_Scoreform_fatherwork=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_motherwork=null;

	/**
	 * @return Returns the ScoreForm/motherWork.
	 */
	public String getScoreform_motherwork(){
		try{
			if (_Scoreform_motherwork==null){
				_Scoreform_motherwork=getStringProperty("ScoreForm/motherWork");
				return _Scoreform_motherwork;
			}else {
				return _Scoreform_motherwork;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/motherWork.
	 * @param v Value to Set.
	 */
	public void setScoreform_motherwork(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/motherWork",v);
		_Scoreform_motherwork=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectioni_sportstopica=null;

	/**
	 * @return Returns the ScoreForm/sectionI/sportsTopica.
	 */
	public String getScoreform_sectioni_sportstopica(){
		try{
			if (_Scoreform_sectioni_sportstopica==null){
				_Scoreform_sectioni_sportstopica=getStringProperty("ScoreForm/sectionI/sportsTopica");
				return _Scoreform_sectioni_sportstopica;
			}else {
				return _Scoreform_sectioni_sportstopica;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionI/sportsTopica.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioni_sportstopica(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionI/sportsTopica",v);
		_Scoreform_sectioni_sportstopica=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectioni_sportstopicb=null;

	/**
	 * @return Returns the ScoreForm/sectionI/sportsTopicb.
	 */
	public String getScoreform_sectioni_sportstopicb(){
		try{
			if (_Scoreform_sectioni_sportstopicb==null){
				_Scoreform_sectioni_sportstopicb=getStringProperty("ScoreForm/sectionI/sportsTopicb");
				return _Scoreform_sectioni_sportstopicb;
			}else {
				return _Scoreform_sectioni_sportstopicb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionI/sportsTopicb.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioni_sportstopicb(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionI/sportsTopicb",v);
		_Scoreform_sectioni_sportstopicb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectioni_sportstopicc=null;

	/**
	 * @return Returns the ScoreForm/sectionI/sportsTopicc.
	 */
	public String getScoreform_sectioni_sportstopicc(){
		try{
			if (_Scoreform_sectioni_sportstopicc==null){
				_Scoreform_sectioni_sportstopicc=getStringProperty("ScoreForm/sectionI/sportsTopicc");
				return _Scoreform_sectioni_sportstopicc;
			}else {
				return _Scoreform_sectioni_sportstopicc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionI/sportsTopicc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioni_sportstopicc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionI/sportsTopicc",v);
		_Scoreform_sectioni_sportstopicc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectioni_sportstimea=null;

	/**
	 * @return Returns the ScoreForm/sectionI/sportsTimea.
	 */
	public Integer getScoreform_sectioni_sportstimea() {
		try{
			if (_Scoreform_sectioni_sportstimea==null){
				_Scoreform_sectioni_sportstimea=getIntegerProperty("ScoreForm/sectionI/sportsTimea");
				return _Scoreform_sectioni_sportstimea;
			}else {
				return _Scoreform_sectioni_sportstimea;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionI/sportsTimea.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioni_sportstimea(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionI/sportsTimea",v);
		_Scoreform_sectioni_sportstimea=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectioni_sportstimeb=null;

	/**
	 * @return Returns the ScoreForm/sectionI/sportsTimeb.
	 */
	public Integer getScoreform_sectioni_sportstimeb() {
		try{
			if (_Scoreform_sectioni_sportstimeb==null){
				_Scoreform_sectioni_sportstimeb=getIntegerProperty("ScoreForm/sectionI/sportsTimeb");
				return _Scoreform_sectioni_sportstimeb;
			}else {
				return _Scoreform_sectioni_sportstimeb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionI/sportsTimeb.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioni_sportstimeb(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionI/sportsTimeb",v);
		_Scoreform_sectioni_sportstimeb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectioni_sportstimec=null;

	/**
	 * @return Returns the ScoreForm/sectionI/sportsTimec.
	 */
	public Integer getScoreform_sectioni_sportstimec() {
		try{
			if (_Scoreform_sectioni_sportstimec==null){
				_Scoreform_sectioni_sportstimec=getIntegerProperty("ScoreForm/sectionI/sportsTimec");
				return _Scoreform_sectioni_sportstimec;
			}else {
				return _Scoreform_sectioni_sportstimec;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionI/sportsTimec.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioni_sportstimec(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionI/sportsTimec",v);
		_Scoreform_sectioni_sportstimec=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectioni_sportsdowella=null;

	/**
	 * @return Returns the ScoreForm/sectionI/sportsDoWella.
	 */
	public Integer getScoreform_sectioni_sportsdowella() {
		try{
			if (_Scoreform_sectioni_sportsdowella==null){
				_Scoreform_sectioni_sportsdowella=getIntegerProperty("ScoreForm/sectionI/sportsDoWella");
				return _Scoreform_sectioni_sportsdowella;
			}else {
				return _Scoreform_sectioni_sportsdowella;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionI/sportsDoWella.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioni_sportsdowella(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionI/sportsDoWella",v);
		_Scoreform_sectioni_sportsdowella=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectioni_sportsdowellb=null;

	/**
	 * @return Returns the ScoreForm/sectionI/sportsDoWellb.
	 */
	public Integer getScoreform_sectioni_sportsdowellb() {
		try{
			if (_Scoreform_sectioni_sportsdowellb==null){
				_Scoreform_sectioni_sportsdowellb=getIntegerProperty("ScoreForm/sectionI/sportsDoWellb");
				return _Scoreform_sectioni_sportsdowellb;
			}else {
				return _Scoreform_sectioni_sportsdowellb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionI/sportsDoWellb.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioni_sportsdowellb(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionI/sportsDoWellb",v);
		_Scoreform_sectioni_sportsdowellb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectioni_sportsdowellc=null;

	/**
	 * @return Returns the ScoreForm/sectionI/sportsDoWellc.
	 */
	public Integer getScoreform_sectioni_sportsdowellc() {
		try{
			if (_Scoreform_sectioni_sportsdowellc==null){
				_Scoreform_sectioni_sportsdowellc=getIntegerProperty("ScoreForm/sectionI/sportsDoWellc");
				return _Scoreform_sectioni_sportsdowellc;
			}else {
				return _Scoreform_sectioni_sportsdowellc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionI/sportsDoWellc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioni_sportsdowellc(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionI/sportsDoWellc",v);
		_Scoreform_sectioni_sportsdowellc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionii_hobbiestopica=null;

	/**
	 * @return Returns the ScoreForm/sectionII/hobbiesTopica.
	 */
	public String getScoreform_sectionii_hobbiestopica(){
		try{
			if (_Scoreform_sectionii_hobbiestopica==null){
				_Scoreform_sectionii_hobbiestopica=getStringProperty("ScoreForm/sectionII/hobbiesTopica");
				return _Scoreform_sectionii_hobbiestopica;
			}else {
				return _Scoreform_sectionii_hobbiestopica;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionII/hobbiesTopica.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionii_hobbiestopica(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionII/hobbiesTopica",v);
		_Scoreform_sectionii_hobbiestopica=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionii_hobbiestopicb=null;

	/**
	 * @return Returns the ScoreForm/sectionII/hobbiesTopicb.
	 */
	public String getScoreform_sectionii_hobbiestopicb(){
		try{
			if (_Scoreform_sectionii_hobbiestopicb==null){
				_Scoreform_sectionii_hobbiestopicb=getStringProperty("ScoreForm/sectionII/hobbiesTopicb");
				return _Scoreform_sectionii_hobbiestopicb;
			}else {
				return _Scoreform_sectionii_hobbiestopicb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionII/hobbiesTopicb.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionii_hobbiestopicb(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionII/hobbiesTopicb",v);
		_Scoreform_sectionii_hobbiestopicb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionii_hobbiestopicc=null;

	/**
	 * @return Returns the ScoreForm/sectionII/hobbiesTopicc.
	 */
	public String getScoreform_sectionii_hobbiestopicc(){
		try{
			if (_Scoreform_sectionii_hobbiestopicc==null){
				_Scoreform_sectionii_hobbiestopicc=getStringProperty("ScoreForm/sectionII/hobbiesTopicc");
				return _Scoreform_sectionii_hobbiestopicc;
			}else {
				return _Scoreform_sectionii_hobbiestopicc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionII/hobbiesTopicc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionii_hobbiestopicc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionII/hobbiesTopicc",v);
		_Scoreform_sectionii_hobbiestopicc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionii_hobbiestimea=null;

	/**
	 * @return Returns the ScoreForm/sectionII/hobbiesTimea.
	 */
	public Integer getScoreform_sectionii_hobbiestimea() {
		try{
			if (_Scoreform_sectionii_hobbiestimea==null){
				_Scoreform_sectionii_hobbiestimea=getIntegerProperty("ScoreForm/sectionII/hobbiesTimea");
				return _Scoreform_sectionii_hobbiestimea;
			}else {
				return _Scoreform_sectionii_hobbiestimea;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionII/hobbiesTimea.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionii_hobbiestimea(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionII/hobbiesTimea",v);
		_Scoreform_sectionii_hobbiestimea=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionii_hobbiestimeb=null;

	/**
	 * @return Returns the ScoreForm/sectionII/hobbiesTimeb.
	 */
	public Integer getScoreform_sectionii_hobbiestimeb() {
		try{
			if (_Scoreform_sectionii_hobbiestimeb==null){
				_Scoreform_sectionii_hobbiestimeb=getIntegerProperty("ScoreForm/sectionII/hobbiesTimeb");
				return _Scoreform_sectionii_hobbiestimeb;
			}else {
				return _Scoreform_sectionii_hobbiestimeb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionII/hobbiesTimeb.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionii_hobbiestimeb(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionII/hobbiesTimeb",v);
		_Scoreform_sectionii_hobbiestimeb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionii_hobbiestimec=null;

	/**
	 * @return Returns the ScoreForm/sectionII/hobbiesTimec.
	 */
	public Integer getScoreform_sectionii_hobbiestimec() {
		try{
			if (_Scoreform_sectionii_hobbiestimec==null){
				_Scoreform_sectionii_hobbiestimec=getIntegerProperty("ScoreForm/sectionII/hobbiesTimec");
				return _Scoreform_sectionii_hobbiestimec;
			}else {
				return _Scoreform_sectionii_hobbiestimec;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionII/hobbiesTimec.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionii_hobbiestimec(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionII/hobbiesTimec",v);
		_Scoreform_sectionii_hobbiestimec=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionii_hobbiesdowella=null;

	/**
	 * @return Returns the ScoreForm/sectionII/hobbiesDoWella.
	 */
	public Integer getScoreform_sectionii_hobbiesdowella() {
		try{
			if (_Scoreform_sectionii_hobbiesdowella==null){
				_Scoreform_sectionii_hobbiesdowella=getIntegerProperty("ScoreForm/sectionII/hobbiesDoWella");
				return _Scoreform_sectionii_hobbiesdowella;
			}else {
				return _Scoreform_sectionii_hobbiesdowella;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionII/hobbiesDoWella.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionii_hobbiesdowella(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionII/hobbiesDoWella",v);
		_Scoreform_sectionii_hobbiesdowella=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionii_hobbiesdowellb=null;

	/**
	 * @return Returns the ScoreForm/sectionII/hobbiesDoWellb.
	 */
	public Integer getScoreform_sectionii_hobbiesdowellb() {
		try{
			if (_Scoreform_sectionii_hobbiesdowellb==null){
				_Scoreform_sectionii_hobbiesdowellb=getIntegerProperty("ScoreForm/sectionII/hobbiesDoWellb");
				return _Scoreform_sectionii_hobbiesdowellb;
			}else {
				return _Scoreform_sectionii_hobbiesdowellb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionII/hobbiesDoWellb.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionii_hobbiesdowellb(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionII/hobbiesDoWellb",v);
		_Scoreform_sectionii_hobbiesdowellb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionii_hobbiesdowellc=null;

	/**
	 * @return Returns the ScoreForm/sectionII/hobbiesDoWellc.
	 */
	public Integer getScoreform_sectionii_hobbiesdowellc() {
		try{
			if (_Scoreform_sectionii_hobbiesdowellc==null){
				_Scoreform_sectionii_hobbiesdowellc=getIntegerProperty("ScoreForm/sectionII/hobbiesDoWellc");
				return _Scoreform_sectionii_hobbiesdowellc;
			}else {
				return _Scoreform_sectionii_hobbiesdowellc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionII/hobbiesDoWellc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionii_hobbiesdowellc(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionII/hobbiesDoWellc",v);
		_Scoreform_sectionii_hobbiesdowellc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectioniii_orgstopica=null;

	/**
	 * @return Returns the ScoreForm/sectionIII/orgsTopica.
	 */
	public String getScoreform_sectioniii_orgstopica(){
		try{
			if (_Scoreform_sectioniii_orgstopica==null){
				_Scoreform_sectioniii_orgstopica=getStringProperty("ScoreForm/sectionIII/orgsTopica");
				return _Scoreform_sectioniii_orgstopica;
			}else {
				return _Scoreform_sectioniii_orgstopica;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionIII/orgsTopica.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioniii_orgstopica(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionIII/orgsTopica",v);
		_Scoreform_sectioniii_orgstopica=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectioniii_orgstopicb=null;

	/**
	 * @return Returns the ScoreForm/sectionIII/orgsTopicb.
	 */
	public String getScoreform_sectioniii_orgstopicb(){
		try{
			if (_Scoreform_sectioniii_orgstopicb==null){
				_Scoreform_sectioniii_orgstopicb=getStringProperty("ScoreForm/sectionIII/orgsTopicb");
				return _Scoreform_sectioniii_orgstopicb;
			}else {
				return _Scoreform_sectioniii_orgstopicb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionIII/orgsTopicb.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioniii_orgstopicb(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionIII/orgsTopicb",v);
		_Scoreform_sectioniii_orgstopicb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectioniii_orgstopicc=null;

	/**
	 * @return Returns the ScoreForm/sectionIII/orgsTopicc.
	 */
	public String getScoreform_sectioniii_orgstopicc(){
		try{
			if (_Scoreform_sectioniii_orgstopicc==null){
				_Scoreform_sectioniii_orgstopicc=getStringProperty("ScoreForm/sectionIII/orgsTopicc");
				return _Scoreform_sectioniii_orgstopicc;
			}else {
				return _Scoreform_sectioniii_orgstopicc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionIII/orgsTopicc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioniii_orgstopicc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionIII/orgsTopicc",v);
		_Scoreform_sectioniii_orgstopicc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectioniii_orgsactivea=null;

	/**
	 * @return Returns the ScoreForm/sectionIII/orgsActivea.
	 */
	public Integer getScoreform_sectioniii_orgsactivea() {
		try{
			if (_Scoreform_sectioniii_orgsactivea==null){
				_Scoreform_sectioniii_orgsactivea=getIntegerProperty("ScoreForm/sectionIII/orgsActivea");
				return _Scoreform_sectioniii_orgsactivea;
			}else {
				return _Scoreform_sectioniii_orgsactivea;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionIII/orgsActivea.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioniii_orgsactivea(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionIII/orgsActivea",v);
		_Scoreform_sectioniii_orgsactivea=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectioniii_orgsactiveb=null;

	/**
	 * @return Returns the ScoreForm/sectionIII/orgsActiveb.
	 */
	public Integer getScoreform_sectioniii_orgsactiveb() {
		try{
			if (_Scoreform_sectioniii_orgsactiveb==null){
				_Scoreform_sectioniii_orgsactiveb=getIntegerProperty("ScoreForm/sectionIII/orgsActiveb");
				return _Scoreform_sectioniii_orgsactiveb;
			}else {
				return _Scoreform_sectioniii_orgsactiveb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionIII/orgsActiveb.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioniii_orgsactiveb(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionIII/orgsActiveb",v);
		_Scoreform_sectioniii_orgsactiveb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectioniii_orgsactivec=null;

	/**
	 * @return Returns the ScoreForm/sectionIII/orgsActivec.
	 */
	public Integer getScoreform_sectioniii_orgsactivec() {
		try{
			if (_Scoreform_sectioniii_orgsactivec==null){
				_Scoreform_sectioniii_orgsactivec=getIntegerProperty("ScoreForm/sectionIII/orgsActivec");
				return _Scoreform_sectioniii_orgsactivec;
			}else {
				return _Scoreform_sectioniii_orgsactivec;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionIII/orgsActivec.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioniii_orgsactivec(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionIII/orgsActivec",v);
		_Scoreform_sectioniii_orgsactivec=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectioniv_jobstopica=null;

	/**
	 * @return Returns the ScoreForm/sectionIV/jobsTopica.
	 */
	public String getScoreform_sectioniv_jobstopica(){
		try{
			if (_Scoreform_sectioniv_jobstopica==null){
				_Scoreform_sectioniv_jobstopica=getStringProperty("ScoreForm/sectionIV/jobsTopica");
				return _Scoreform_sectioniv_jobstopica;
			}else {
				return _Scoreform_sectioniv_jobstopica;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionIV/jobsTopica.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioniv_jobstopica(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionIV/jobsTopica",v);
		_Scoreform_sectioniv_jobstopica=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectioniv_jobstopicb=null;

	/**
	 * @return Returns the ScoreForm/sectionIV/jobsTopicb.
	 */
	public String getScoreform_sectioniv_jobstopicb(){
		try{
			if (_Scoreform_sectioniv_jobstopicb==null){
				_Scoreform_sectioniv_jobstopicb=getStringProperty("ScoreForm/sectionIV/jobsTopicb");
				return _Scoreform_sectioniv_jobstopicb;
			}else {
				return _Scoreform_sectioniv_jobstopicb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionIV/jobsTopicb.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioniv_jobstopicb(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionIV/jobsTopicb",v);
		_Scoreform_sectioniv_jobstopicb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectioniv_jobstopicc=null;

	/**
	 * @return Returns the ScoreForm/sectionIV/jobsTopicc.
	 */
	public String getScoreform_sectioniv_jobstopicc(){
		try{
			if (_Scoreform_sectioniv_jobstopicc==null){
				_Scoreform_sectioniv_jobstopicc=getStringProperty("ScoreForm/sectionIV/jobsTopicc");
				return _Scoreform_sectioniv_jobstopicc;
			}else {
				return _Scoreform_sectioniv_jobstopicc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionIV/jobsTopicc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioniv_jobstopicc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionIV/jobsTopicc",v);
		_Scoreform_sectioniv_jobstopicc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectioniv_jobswella=null;

	/**
	 * @return Returns the ScoreForm/sectionIV/jobsWella.
	 */
	public Integer getScoreform_sectioniv_jobswella() {
		try{
			if (_Scoreform_sectioniv_jobswella==null){
				_Scoreform_sectioniv_jobswella=getIntegerProperty("ScoreForm/sectionIV/jobsWella");
				return _Scoreform_sectioniv_jobswella;
			}else {
				return _Scoreform_sectioniv_jobswella;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionIV/jobsWella.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioniv_jobswella(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionIV/jobsWella",v);
		_Scoreform_sectioniv_jobswella=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectioniv_jobswellb=null;

	/**
	 * @return Returns the ScoreForm/sectionIV/jobsWellb.
	 */
	public Integer getScoreform_sectioniv_jobswellb() {
		try{
			if (_Scoreform_sectioniv_jobswellb==null){
				_Scoreform_sectioniv_jobswellb=getIntegerProperty("ScoreForm/sectionIV/jobsWellb");
				return _Scoreform_sectioniv_jobswellb;
			}else {
				return _Scoreform_sectioniv_jobswellb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionIV/jobsWellb.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioniv_jobswellb(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionIV/jobsWellb",v);
		_Scoreform_sectioniv_jobswellb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectioniv_jobswellc=null;

	/**
	 * @return Returns the ScoreForm/sectionIV/jobsWellc.
	 */
	public Integer getScoreform_sectioniv_jobswellc() {
		try{
			if (_Scoreform_sectioniv_jobswellc==null){
				_Scoreform_sectioniv_jobswellc=getIntegerProperty("ScoreForm/sectionIV/jobsWellc");
				return _Scoreform_sectioniv_jobswellc;
			}else {
				return _Scoreform_sectioniv_jobswellc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionIV/jobsWellc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectioniv_jobswellc(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionIV/jobsWellc",v);
		_Scoreform_sectioniv_jobswellc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionv_closefriends=null;

	/**
	 * @return Returns the ScoreForm/sectionV/closeFriends.
	 */
	public Integer getScoreform_sectionv_closefriends() {
		try{
			if (_Scoreform_sectionv_closefriends==null){
				_Scoreform_sectionv_closefriends=getIntegerProperty("ScoreForm/sectionV/closeFriends");
				return _Scoreform_sectionv_closefriends;
			}else {
				return _Scoreform_sectionv_closefriends;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionV/closeFriends.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionv_closefriends(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionV/closeFriends",v);
		_Scoreform_sectionv_closefriends=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionv_friendtimes=null;

	/**
	 * @return Returns the ScoreForm/sectionV/friendTimes.
	 */
	public Integer getScoreform_sectionv_friendtimes() {
		try{
			if (_Scoreform_sectionv_friendtimes==null){
				_Scoreform_sectionv_friendtimes=getIntegerProperty("ScoreForm/sectionV/friendTimes");
				return _Scoreform_sectionv_friendtimes;
			}else {
				return _Scoreform_sectionv_friendtimes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionV/friendTimes.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionv_friendtimes(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionV/friendTimes",v);
		_Scoreform_sectionv_friendtimes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_getalonga=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/getAlonga.
	 */
	public Integer getScoreform_sectionvi_getalonga() {
		try{
			if (_Scoreform_sectionvi_getalonga==null){
				_Scoreform_sectionvi_getalonga=getIntegerProperty("ScoreForm/sectionVI/getAlonga");
				return _Scoreform_sectionvi_getalonga;
			}else {
				return _Scoreform_sectionvi_getalonga;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/getAlonga.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_getalonga(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/getAlonga",v);
		_Scoreform_sectionvi_getalonga=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_getalongb=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/getAlongb.
	 */
	public Integer getScoreform_sectionvi_getalongb() {
		try{
			if (_Scoreform_sectionvi_getalongb==null){
				_Scoreform_sectionvi_getalongb=getIntegerProperty("ScoreForm/sectionVI/getAlongb");
				return _Scoreform_sectionvi_getalongb;
			}else {
				return _Scoreform_sectionvi_getalongb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/getAlongb.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_getalongb(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/getAlongb",v);
		_Scoreform_sectionvi_getalongb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_getalongc=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/getAlongc.
	 */
	public Integer getScoreform_sectionvi_getalongc() {
		try{
			if (_Scoreform_sectionvi_getalongc==null){
				_Scoreform_sectionvi_getalongc=getIntegerProperty("ScoreForm/sectionVI/getAlongc");
				return _Scoreform_sectionvi_getalongc;
			}else {
				return _Scoreform_sectionvi_getalongc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/getAlongc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_getalongc(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/getAlongc",v);
		_Scoreform_sectionvi_getalongc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvi_getalongd=null;

	/**
	 * @return Returns the ScoreForm/sectionVI/getAlongd.
	 */
	public Integer getScoreform_sectionvi_getalongd() {
		try{
			if (_Scoreform_sectionvi_getalongd==null){
				_Scoreform_sectionvi_getalongd=getIntegerProperty("ScoreForm/sectionVI/getAlongd");
				return _Scoreform_sectionvi_getalongd;
			}else {
				return _Scoreform_sectionvi_getalongd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVI/getAlongd.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvi_getalongd(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVI/getAlongd",v);
		_Scoreform_sectionvi_getalongd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionvii_academperfnoschool=null;

	/**
	 * @return Returns the ScoreForm/sectionVII/academPerfNoSchool.
	 */
	public String getScoreform_sectionvii_academperfnoschool(){
		try{
			if (_Scoreform_sectionvii_academperfnoschool==null){
				_Scoreform_sectionvii_academperfnoschool=getStringProperty("ScoreForm/sectionVII/academPerfNoSchool");
				return _Scoreform_sectionvii_academperfnoschool;
			}else {
				return _Scoreform_sectionvii_academperfnoschool;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVII/academPerfNoSchool.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvii_academperfnoschool(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVII/academPerfNoSchool",v);
		_Scoreform_sectionvii_academperfnoschool=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvii_academperfa=null;

	/**
	 * @return Returns the ScoreForm/sectionVII/academPerfa.
	 */
	public Integer getScoreform_sectionvii_academperfa() {
		try{
			if (_Scoreform_sectionvii_academperfa==null){
				_Scoreform_sectionvii_academperfa=getIntegerProperty("ScoreForm/sectionVII/academPerfa");
				return _Scoreform_sectionvii_academperfa;
			}else {
				return _Scoreform_sectionvii_academperfa;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVII/academPerfa.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvii_academperfa(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVII/academPerfa",v);
		_Scoreform_sectionvii_academperfa=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvii_academperfb=null;

	/**
	 * @return Returns the ScoreForm/sectionVII/academPerfb.
	 */
	public Integer getScoreform_sectionvii_academperfb() {
		try{
			if (_Scoreform_sectionvii_academperfb==null){
				_Scoreform_sectionvii_academperfb=getIntegerProperty("ScoreForm/sectionVII/academPerfb");
				return _Scoreform_sectionvii_academperfb;
			}else {
				return _Scoreform_sectionvii_academperfb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVII/academPerfb.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvii_academperfb(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVII/academPerfb",v);
		_Scoreform_sectionvii_academperfb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvii_academperfc=null;

	/**
	 * @return Returns the ScoreForm/sectionVII/academPerfc.
	 */
	public Integer getScoreform_sectionvii_academperfc() {
		try{
			if (_Scoreform_sectionvii_academperfc==null){
				_Scoreform_sectionvii_academperfc=getIntegerProperty("ScoreForm/sectionVII/academPerfc");
				return _Scoreform_sectionvii_academperfc;
			}else {
				return _Scoreform_sectionvii_academperfc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVII/academPerfc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvii_academperfc(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVII/academPerfc",v);
		_Scoreform_sectionvii_academperfc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvii_academperfd=null;

	/**
	 * @return Returns the ScoreForm/sectionVII/academPerfd.
	 */
	public Integer getScoreform_sectionvii_academperfd() {
		try{
			if (_Scoreform_sectionvii_academperfd==null){
				_Scoreform_sectionvii_academperfd=getIntegerProperty("ScoreForm/sectionVII/academPerfd");
				return _Scoreform_sectionvii_academperfd;
			}else {
				return _Scoreform_sectionvii_academperfd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVII/academPerfd.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvii_academperfd(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVII/academPerfd",v);
		_Scoreform_sectionvii_academperfd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionvii_academperfetopic=null;

	/**
	 * @return Returns the ScoreForm/sectionVII/academPerfeTopic.
	 */
	public String getScoreform_sectionvii_academperfetopic(){
		try{
			if (_Scoreform_sectionvii_academperfetopic==null){
				_Scoreform_sectionvii_academperfetopic=getStringProperty("ScoreForm/sectionVII/academPerfeTopic");
				return _Scoreform_sectionvii_academperfetopic;
			}else {
				return _Scoreform_sectionvii_academperfetopic;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVII/academPerfeTopic.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvii_academperfetopic(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVII/academPerfeTopic",v);
		_Scoreform_sectionvii_academperfetopic=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvii_academperfe=null;

	/**
	 * @return Returns the ScoreForm/sectionVII/academPerfe.
	 */
	public Integer getScoreform_sectionvii_academperfe() {
		try{
			if (_Scoreform_sectionvii_academperfe==null){
				_Scoreform_sectionvii_academperfe=getIntegerProperty("ScoreForm/sectionVII/academPerfe");
				return _Scoreform_sectionvii_academperfe;
			}else {
				return _Scoreform_sectionvii_academperfe;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVII/academPerfe.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvii_academperfe(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVII/academPerfe",v);
		_Scoreform_sectionvii_academperfe=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionvii_academperfftopic=null;

	/**
	 * @return Returns the ScoreForm/sectionVII/academPerffTopic.
	 */
	public String getScoreform_sectionvii_academperfftopic(){
		try{
			if (_Scoreform_sectionvii_academperfftopic==null){
				_Scoreform_sectionvii_academperfftopic=getStringProperty("ScoreForm/sectionVII/academPerffTopic");
				return _Scoreform_sectionvii_academperfftopic;
			}else {
				return _Scoreform_sectionvii_academperfftopic;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVII/academPerffTopic.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvii_academperfftopic(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVII/academPerffTopic",v);
		_Scoreform_sectionvii_academperfftopic=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvii_academperff=null;

	/**
	 * @return Returns the ScoreForm/sectionVII/academPerff.
	 */
	public Integer getScoreform_sectionvii_academperff() {
		try{
			if (_Scoreform_sectionvii_academperff==null){
				_Scoreform_sectionvii_academperff=getIntegerProperty("ScoreForm/sectionVII/academPerff");
				return _Scoreform_sectionvii_academperff;
			}else {
				return _Scoreform_sectionvii_academperff;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVII/academPerff.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvii_academperff(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVII/academPerff",v);
		_Scoreform_sectionvii_academperff=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionvii_academperfgtopic=null;

	/**
	 * @return Returns the ScoreForm/sectionVII/academPerfgTopic.
	 */
	public String getScoreform_sectionvii_academperfgtopic(){
		try{
			if (_Scoreform_sectionvii_academperfgtopic==null){
				_Scoreform_sectionvii_academperfgtopic=getStringProperty("ScoreForm/sectionVII/academPerfgTopic");
				return _Scoreform_sectionvii_academperfgtopic;
			}else {
				return _Scoreform_sectionvii_academperfgtopic;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVII/academPerfgTopic.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvii_academperfgtopic(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVII/academPerfgTopic",v);
		_Scoreform_sectionvii_academperfgtopic=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvii_academperfg=null;

	/**
	 * @return Returns the ScoreForm/sectionVII/academPerfg.
	 */
	public Integer getScoreform_sectionvii_academperfg() {
		try{
			if (_Scoreform_sectionvii_academperfg==null){
				_Scoreform_sectionvii_academperfg=getIntegerProperty("ScoreForm/sectionVII/academPerfg");
				return _Scoreform_sectionvii_academperfg;
			}else {
				return _Scoreform_sectionvii_academperfg;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVII/academPerfg.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvii_academperfg(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVII/academPerfg",v);
		_Scoreform_sectionvii_academperfg=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvii_speced=null;

	/**
	 * @return Returns the ScoreForm/sectionVII/specEd.
	 */
	public Integer getScoreform_sectionvii_speced() {
		try{
			if (_Scoreform_sectionvii_speced==null){
				_Scoreform_sectionvii_speced=getIntegerProperty("ScoreForm/sectionVII/specEd");
				return _Scoreform_sectionvii_speced;
			}else {
				return _Scoreform_sectionvii_speced;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVII/specEd.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvii_speced(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVII/specEd",v);
		_Scoreform_sectionvii_speced=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionvii_specedkind=null;

	/**
	 * @return Returns the ScoreForm/sectionVII/specEdKind.
	 */
	public String getScoreform_sectionvii_specedkind(){
		try{
			if (_Scoreform_sectionvii_specedkind==null){
				_Scoreform_sectionvii_specedkind=getStringProperty("ScoreForm/sectionVII/specEdKind");
				return _Scoreform_sectionvii_specedkind;
			}else {
				return _Scoreform_sectionvii_specedkind;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVII/specEdKind.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvii_specedkind(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVII/specEdKind",v);
		_Scoreform_sectionvii_specedkind=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvii_repeatgrade=null;

	/**
	 * @return Returns the ScoreForm/sectionVII/repeatGrade.
	 */
	public Integer getScoreform_sectionvii_repeatgrade() {
		try{
			if (_Scoreform_sectionvii_repeatgrade==null){
				_Scoreform_sectionvii_repeatgrade=getIntegerProperty("ScoreForm/sectionVII/repeatGrade");
				return _Scoreform_sectionvii_repeatgrade;
			}else {
				return _Scoreform_sectionvii_repeatgrade;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVII/repeatGrade.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvii_repeatgrade(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVII/repeatGrade",v);
		_Scoreform_sectionvii_repeatgrade=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionvii_repeatgradereason=null;

	/**
	 * @return Returns the ScoreForm/sectionVII/repeatGradeReason.
	 */
	public String getScoreform_sectionvii_repeatgradereason(){
		try{
			if (_Scoreform_sectionvii_repeatgradereason==null){
				_Scoreform_sectionvii_repeatgradereason=getStringProperty("ScoreForm/sectionVII/repeatGradeReason");
				return _Scoreform_sectionvii_repeatgradereason;
			}else {
				return _Scoreform_sectionvii_repeatgradereason;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVII/repeatGradeReason.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvii_repeatgradereason(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVII/repeatGradeReason",v);
		_Scoreform_sectionvii_repeatgradereason=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvii_schoolprob=null;

	/**
	 * @return Returns the ScoreForm/sectionVII/schoolProb.
	 */
	public Integer getScoreform_sectionvii_schoolprob() {
		try{
			if (_Scoreform_sectionvii_schoolprob==null){
				_Scoreform_sectionvii_schoolprob=getIntegerProperty("ScoreForm/sectionVII/schoolProb");
				return _Scoreform_sectionvii_schoolprob;
			}else {
				return _Scoreform_sectionvii_schoolprob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVII/schoolProb.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvii_schoolprob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVII/schoolProb",v);
		_Scoreform_sectionvii_schoolprob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_sectionvii_schoolprobdesc=null;

	/**
	 * @return Returns the ScoreForm/sectionVII/schoolProbDesc.
	 */
	public String getScoreform_sectionvii_schoolprobdesc(){
		try{
			if (_Scoreform_sectionvii_schoolprobdesc==null){
				_Scoreform_sectionvii_schoolprobdesc=getStringProperty("ScoreForm/sectionVII/schoolProbDesc");
				return _Scoreform_sectionvii_schoolprobdesc;
			}else {
				return _Scoreform_sectionvii_schoolprobdesc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVII/schoolProbDesc.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvii_schoolprobdesc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVII/schoolProbDesc",v);
		_Scoreform_sectionvii_schoolprobdesc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Scoreform_sectionvii_schoolprobstartdate=null;

	/**
	 * @return Returns the ScoreForm/sectionVII/schoolProbStartDate.
	 */
	public Object getScoreform_sectionvii_schoolprobstartdate(){
		try{
			if (_Scoreform_sectionvii_schoolprobstartdate==null){
				_Scoreform_sectionvii_schoolprobstartdate=getProperty("ScoreForm/sectionVII/schoolProbStartDate");
				return _Scoreform_sectionvii_schoolprobstartdate;
			}else {
				return _Scoreform_sectionvii_schoolprobstartdate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVII/schoolProbStartDate.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvii_schoolprobstartdate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVII/schoolProbStartDate",v);
		_Scoreform_sectionvii_schoolprobstartdate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionvii_schoolprobend=null;

	/**
	 * @return Returns the ScoreForm/sectionVII/schoolProbEnd.
	 */
	public Integer getScoreform_sectionvii_schoolprobend() {
		try{
			if (_Scoreform_sectionvii_schoolprobend==null){
				_Scoreform_sectionvii_schoolprobend=getIntegerProperty("ScoreForm/sectionVII/schoolProbEnd");
				return _Scoreform_sectionvii_schoolprobend;
			}else {
				return _Scoreform_sectionvii_schoolprobend;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVII/schoolProbEnd.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvii_schoolprobend(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVII/schoolProbEnd",v);
		_Scoreform_sectionvii_schoolprobend=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Scoreform_sectionvii_schoolprobenddate=null;

	/**
	 * @return Returns the ScoreForm/sectionVII/schoolProbEndDate.
	 */
	public Object getScoreform_sectionvii_schoolprobenddate(){
		try{
			if (_Scoreform_sectionvii_schoolprobenddate==null){
				_Scoreform_sectionvii_schoolprobenddate=getProperty("ScoreForm/sectionVII/schoolProbEndDate");
				return _Scoreform_sectionvii_schoolprobenddate;
			}else {
				return _Scoreform_sectionvii_schoolprobenddate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionVII/schoolProbEndDate.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionvii_schoolprobenddate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionVII/schoolProbEndDate",v);
		_Scoreform_sectionvii_schoolprobenddate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_illdisable=null;

	/**
	 * @return Returns the ScoreForm/illDisable.
	 */
	public Integer getScoreform_illdisable() {
		try{
			if (_Scoreform_illdisable==null){
				_Scoreform_illdisable=getIntegerProperty("ScoreForm/illDisable");
				return _Scoreform_illdisable;
			}else {
				return _Scoreform_illdisable;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/illDisable.
	 * @param v Value to Set.
	 */
	public void setScoreform_illdisable(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/illDisable",v);
		_Scoreform_illdisable=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_illdisabledesc=null;

	/**
	 * @return Returns the ScoreForm/illDisableDesc.
	 */
	public String getScoreform_illdisabledesc(){
		try{
			if (_Scoreform_illdisabledesc==null){
				_Scoreform_illdisabledesc=getStringProperty("ScoreForm/illDisableDesc");
				return _Scoreform_illdisabledesc;
			}else {
				return _Scoreform_illdisabledesc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/illDisableDesc.
	 * @param v Value to Set.
	 */
	public void setScoreform_illdisabledesc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/illDisableDesc",v);
		_Scoreform_illdisabledesc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_concernmost=null;

	/**
	 * @return Returns the ScoreForm/concernMost.
	 */
	public String getScoreform_concernmost(){
		try{
			if (_Scoreform_concernmost==null){
				_Scoreform_concernmost=getStringProperty("ScoreForm/concernMost");
				return _Scoreform_concernmost;
			}else {
				return _Scoreform_concernmost;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/concernMost.
	 * @param v Value to Set.
	 */
	public void setScoreform_concernmost(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/concernMost",v);
		_Scoreform_concernmost=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_bestthings=null;

	/**
	 * @return Returns the ScoreForm/bestThings.
	 */
	public String getScoreform_bestthings(){
		try{
			if (_Scoreform_bestthings==null){
				_Scoreform_bestthings=getStringProperty("ScoreForm/bestThings");
				return _Scoreform_bestthings;
			}else {
				return _Scoreform_bestthings;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/bestThings.
	 * @param v Value to Set.
	 */
	public void setScoreform_bestthings(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/bestThings",v);
		_Scoreform_bestthings=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question1=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question1.
	 */
	public Integer getScoreform_descitemlist_question1() {
		try{
			if (_Scoreform_descitemlist_question1==null){
				_Scoreform_descitemlist_question1=getIntegerProperty("ScoreForm/descItemList/question1");
				return _Scoreform_descitemlist_question1;
			}else {
				return _Scoreform_descitemlist_question1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question1.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question1(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question1",v);
		_Scoreform_descitemlist_question1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question2=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question2.
	 */
	public Integer getScoreform_descitemlist_question2() {
		try{
			if (_Scoreform_descitemlist_question2==null){
				_Scoreform_descitemlist_question2=getIntegerProperty("ScoreForm/descItemList/question2");
				return _Scoreform_descitemlist_question2;
			}else {
				return _Scoreform_descitemlist_question2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question2.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question2(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question2",v);
		_Scoreform_descitemlist_question2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_descitemlist_question2desc=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question2Desc.
	 */
	public String getScoreform_descitemlist_question2desc(){
		try{
			if (_Scoreform_descitemlist_question2desc==null){
				_Scoreform_descitemlist_question2desc=getStringProperty("ScoreForm/descItemList/question2Desc");
				return _Scoreform_descitemlist_question2desc;
			}else {
				return _Scoreform_descitemlist_question2desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question2Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question2desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question2Desc",v);
		_Scoreform_descitemlist_question2desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question3=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question3.
	 */
	public Integer getScoreform_descitemlist_question3() {
		try{
			if (_Scoreform_descitemlist_question3==null){
				_Scoreform_descitemlist_question3=getIntegerProperty("ScoreForm/descItemList/question3");
				return _Scoreform_descitemlist_question3;
			}else {
				return _Scoreform_descitemlist_question3;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question3.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question3(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question3",v);
		_Scoreform_descitemlist_question3=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question4=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question4.
	 */
	public Integer getScoreform_descitemlist_question4() {
		try{
			if (_Scoreform_descitemlist_question4==null){
				_Scoreform_descitemlist_question4=getIntegerProperty("ScoreForm/descItemList/question4");
				return _Scoreform_descitemlist_question4;
			}else {
				return _Scoreform_descitemlist_question4;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question4.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question4(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question4",v);
		_Scoreform_descitemlist_question4=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question5=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question5.
	 */
	public Integer getScoreform_descitemlist_question5() {
		try{
			if (_Scoreform_descitemlist_question5==null){
				_Scoreform_descitemlist_question5=getIntegerProperty("ScoreForm/descItemList/question5");
				return _Scoreform_descitemlist_question5;
			}else {
				return _Scoreform_descitemlist_question5;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question5.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question5(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question5",v);
		_Scoreform_descitemlist_question5=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question6=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question6.
	 */
	public Integer getScoreform_descitemlist_question6() {
		try{
			if (_Scoreform_descitemlist_question6==null){
				_Scoreform_descitemlist_question6=getIntegerProperty("ScoreForm/descItemList/question6");
				return _Scoreform_descitemlist_question6;
			}else {
				return _Scoreform_descitemlist_question6;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question6.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question6(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question6",v);
		_Scoreform_descitemlist_question6=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question7=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question7.
	 */
	public Integer getScoreform_descitemlist_question7() {
		try{
			if (_Scoreform_descitemlist_question7==null){
				_Scoreform_descitemlist_question7=getIntegerProperty("ScoreForm/descItemList/question7");
				return _Scoreform_descitemlist_question7;
			}else {
				return _Scoreform_descitemlist_question7;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question7.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question7(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question7",v);
		_Scoreform_descitemlist_question7=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question8=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question8.
	 */
	public Integer getScoreform_descitemlist_question8() {
		try{
			if (_Scoreform_descitemlist_question8==null){
				_Scoreform_descitemlist_question8=getIntegerProperty("ScoreForm/descItemList/question8");
				return _Scoreform_descitemlist_question8;
			}else {
				return _Scoreform_descitemlist_question8;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question8.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question8(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question8",v);
		_Scoreform_descitemlist_question8=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question9=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question9.
	 */
	public Integer getScoreform_descitemlist_question9() {
		try{
			if (_Scoreform_descitemlist_question9==null){
				_Scoreform_descitemlist_question9=getIntegerProperty("ScoreForm/descItemList/question9");
				return _Scoreform_descitemlist_question9;
			}else {
				return _Scoreform_descitemlist_question9;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question9.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question9(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question9",v);
		_Scoreform_descitemlist_question9=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_descitemlist_question9desc=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question9Desc.
	 */
	public String getScoreform_descitemlist_question9desc(){
		try{
			if (_Scoreform_descitemlist_question9desc==null){
				_Scoreform_descitemlist_question9desc=getStringProperty("ScoreForm/descItemList/question9Desc");
				return _Scoreform_descitemlist_question9desc;
			}else {
				return _Scoreform_descitemlist_question9desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question9Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question9desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question9Desc",v);
		_Scoreform_descitemlist_question9desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question10=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question10.
	 */
	public Integer getScoreform_descitemlist_question10() {
		try{
			if (_Scoreform_descitemlist_question10==null){
				_Scoreform_descitemlist_question10=getIntegerProperty("ScoreForm/descItemList/question10");
				return _Scoreform_descitemlist_question10;
			}else {
				return _Scoreform_descitemlist_question10;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question10.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question10(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question10",v);
		_Scoreform_descitemlist_question10=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question11=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question11.
	 */
	public Integer getScoreform_descitemlist_question11() {
		try{
			if (_Scoreform_descitemlist_question11==null){
				_Scoreform_descitemlist_question11=getIntegerProperty("ScoreForm/descItemList/question11");
				return _Scoreform_descitemlist_question11;
			}else {
				return _Scoreform_descitemlist_question11;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question11.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question11(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question11",v);
		_Scoreform_descitemlist_question11=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question12=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question12.
	 */
	public Integer getScoreform_descitemlist_question12() {
		try{
			if (_Scoreform_descitemlist_question12==null){
				_Scoreform_descitemlist_question12=getIntegerProperty("ScoreForm/descItemList/question12");
				return _Scoreform_descitemlist_question12;
			}else {
				return _Scoreform_descitemlist_question12;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question12.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question12(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question12",v);
		_Scoreform_descitemlist_question12=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question13=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question13.
	 */
	public Integer getScoreform_descitemlist_question13() {
		try{
			if (_Scoreform_descitemlist_question13==null){
				_Scoreform_descitemlist_question13=getIntegerProperty("ScoreForm/descItemList/question13");
				return _Scoreform_descitemlist_question13;
			}else {
				return _Scoreform_descitemlist_question13;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question13.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question13(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question13",v);
		_Scoreform_descitemlist_question13=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question14=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question14.
	 */
	public Integer getScoreform_descitemlist_question14() {
		try{
			if (_Scoreform_descitemlist_question14==null){
				_Scoreform_descitemlist_question14=getIntegerProperty("ScoreForm/descItemList/question14");
				return _Scoreform_descitemlist_question14;
			}else {
				return _Scoreform_descitemlist_question14;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question14.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question14(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question14",v);
		_Scoreform_descitemlist_question14=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question15=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question15.
	 */
	public Integer getScoreform_descitemlist_question15() {
		try{
			if (_Scoreform_descitemlist_question15==null){
				_Scoreform_descitemlist_question15=getIntegerProperty("ScoreForm/descItemList/question15");
				return _Scoreform_descitemlist_question15;
			}else {
				return _Scoreform_descitemlist_question15;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question15.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question15(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question15",v);
		_Scoreform_descitemlist_question15=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question16=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question16.
	 */
	public Integer getScoreform_descitemlist_question16() {
		try{
			if (_Scoreform_descitemlist_question16==null){
				_Scoreform_descitemlist_question16=getIntegerProperty("ScoreForm/descItemList/question16");
				return _Scoreform_descitemlist_question16;
			}else {
				return _Scoreform_descitemlist_question16;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question16.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question16(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question16",v);
		_Scoreform_descitemlist_question16=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question17=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question17.
	 */
	public Integer getScoreform_descitemlist_question17() {
		try{
			if (_Scoreform_descitemlist_question17==null){
				_Scoreform_descitemlist_question17=getIntegerProperty("ScoreForm/descItemList/question17");
				return _Scoreform_descitemlist_question17;
			}else {
				return _Scoreform_descitemlist_question17;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question17.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question17(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question17",v);
		_Scoreform_descitemlist_question17=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question18=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question18.
	 */
	public Integer getScoreform_descitemlist_question18() {
		try{
			if (_Scoreform_descitemlist_question18==null){
				_Scoreform_descitemlist_question18=getIntegerProperty("ScoreForm/descItemList/question18");
				return _Scoreform_descitemlist_question18;
			}else {
				return _Scoreform_descitemlist_question18;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question18.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question18(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question18",v);
		_Scoreform_descitemlist_question18=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question19=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question19.
	 */
	public Integer getScoreform_descitemlist_question19() {
		try{
			if (_Scoreform_descitemlist_question19==null){
				_Scoreform_descitemlist_question19=getIntegerProperty("ScoreForm/descItemList/question19");
				return _Scoreform_descitemlist_question19;
			}else {
				return _Scoreform_descitemlist_question19;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question19.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question19(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question19",v);
		_Scoreform_descitemlist_question19=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question20=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question20.
	 */
	public Integer getScoreform_descitemlist_question20() {
		try{
			if (_Scoreform_descitemlist_question20==null){
				_Scoreform_descitemlist_question20=getIntegerProperty("ScoreForm/descItemList/question20");
				return _Scoreform_descitemlist_question20;
			}else {
				return _Scoreform_descitemlist_question20;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question20.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question20(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question20",v);
		_Scoreform_descitemlist_question20=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question21=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question21.
	 */
	public Integer getScoreform_descitemlist_question21() {
		try{
			if (_Scoreform_descitemlist_question21==null){
				_Scoreform_descitemlist_question21=getIntegerProperty("ScoreForm/descItemList/question21");
				return _Scoreform_descitemlist_question21;
			}else {
				return _Scoreform_descitemlist_question21;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question21.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question21(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question21",v);
		_Scoreform_descitemlist_question21=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question22=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question22.
	 */
	public Integer getScoreform_descitemlist_question22() {
		try{
			if (_Scoreform_descitemlist_question22==null){
				_Scoreform_descitemlist_question22=getIntegerProperty("ScoreForm/descItemList/question22");
				return _Scoreform_descitemlist_question22;
			}else {
				return _Scoreform_descitemlist_question22;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question22.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question22(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question22",v);
		_Scoreform_descitemlist_question22=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question23=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question23.
	 */
	public Integer getScoreform_descitemlist_question23() {
		try{
			if (_Scoreform_descitemlist_question23==null){
				_Scoreform_descitemlist_question23=getIntegerProperty("ScoreForm/descItemList/question23");
				return _Scoreform_descitemlist_question23;
			}else {
				return _Scoreform_descitemlist_question23;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question23.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question23(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question23",v);
		_Scoreform_descitemlist_question23=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question24=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question24.
	 */
	public Integer getScoreform_descitemlist_question24() {
		try{
			if (_Scoreform_descitemlist_question24==null){
				_Scoreform_descitemlist_question24=getIntegerProperty("ScoreForm/descItemList/question24");
				return _Scoreform_descitemlist_question24;
			}else {
				return _Scoreform_descitemlist_question24;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question24.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question24(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question24",v);
		_Scoreform_descitemlist_question24=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question25=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question25.
	 */
	public Integer getScoreform_descitemlist_question25() {
		try{
			if (_Scoreform_descitemlist_question25==null){
				_Scoreform_descitemlist_question25=getIntegerProperty("ScoreForm/descItemList/question25");
				return _Scoreform_descitemlist_question25;
			}else {
				return _Scoreform_descitemlist_question25;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question25.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question25(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question25",v);
		_Scoreform_descitemlist_question25=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question26=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question26.
	 */
	public Integer getScoreform_descitemlist_question26() {
		try{
			if (_Scoreform_descitemlist_question26==null){
				_Scoreform_descitemlist_question26=getIntegerProperty("ScoreForm/descItemList/question26");
				return _Scoreform_descitemlist_question26;
			}else {
				return _Scoreform_descitemlist_question26;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question26.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question26(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question26",v);
		_Scoreform_descitemlist_question26=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question27=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question27.
	 */
	public Integer getScoreform_descitemlist_question27() {
		try{
			if (_Scoreform_descitemlist_question27==null){
				_Scoreform_descitemlist_question27=getIntegerProperty("ScoreForm/descItemList/question27");
				return _Scoreform_descitemlist_question27;
			}else {
				return _Scoreform_descitemlist_question27;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question27.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question27(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question27",v);
		_Scoreform_descitemlist_question27=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question28=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question28.
	 */
	public Integer getScoreform_descitemlist_question28() {
		try{
			if (_Scoreform_descitemlist_question28==null){
				_Scoreform_descitemlist_question28=getIntegerProperty("ScoreForm/descItemList/question28");
				return _Scoreform_descitemlist_question28;
			}else {
				return _Scoreform_descitemlist_question28;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question28.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question28(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question28",v);
		_Scoreform_descitemlist_question28=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question29=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question29.
	 */
	public Integer getScoreform_descitemlist_question29() {
		try{
			if (_Scoreform_descitemlist_question29==null){
				_Scoreform_descitemlist_question29=getIntegerProperty("ScoreForm/descItemList/question29");
				return _Scoreform_descitemlist_question29;
			}else {
				return _Scoreform_descitemlist_question29;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question29.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question29(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question29",v);
		_Scoreform_descitemlist_question29=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_descitemlist_question29desc=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question29Desc.
	 */
	public String getScoreform_descitemlist_question29desc(){
		try{
			if (_Scoreform_descitemlist_question29desc==null){
				_Scoreform_descitemlist_question29desc=getStringProperty("ScoreForm/descItemList/question29Desc");
				return _Scoreform_descitemlist_question29desc;
			}else {
				return _Scoreform_descitemlist_question29desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question29Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question29desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question29Desc",v);
		_Scoreform_descitemlist_question29desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question30=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question30.
	 */
	public Integer getScoreform_descitemlist_question30() {
		try{
			if (_Scoreform_descitemlist_question30==null){
				_Scoreform_descitemlist_question30=getIntegerProperty("ScoreForm/descItemList/question30");
				return _Scoreform_descitemlist_question30;
			}else {
				return _Scoreform_descitemlist_question30;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question30.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question30(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question30",v);
		_Scoreform_descitemlist_question30=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question31=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question31.
	 */
	public Integer getScoreform_descitemlist_question31() {
		try{
			if (_Scoreform_descitemlist_question31==null){
				_Scoreform_descitemlist_question31=getIntegerProperty("ScoreForm/descItemList/question31");
				return _Scoreform_descitemlist_question31;
			}else {
				return _Scoreform_descitemlist_question31;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question31.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question31(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question31",v);
		_Scoreform_descitemlist_question31=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question32=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question32.
	 */
	public Integer getScoreform_descitemlist_question32() {
		try{
			if (_Scoreform_descitemlist_question32==null){
				_Scoreform_descitemlist_question32=getIntegerProperty("ScoreForm/descItemList/question32");
				return _Scoreform_descitemlist_question32;
			}else {
				return _Scoreform_descitemlist_question32;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question32.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question32(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question32",v);
		_Scoreform_descitemlist_question32=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question33=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question33.
	 */
	public Integer getScoreform_descitemlist_question33() {
		try{
			if (_Scoreform_descitemlist_question33==null){
				_Scoreform_descitemlist_question33=getIntegerProperty("ScoreForm/descItemList/question33");
				return _Scoreform_descitemlist_question33;
			}else {
				return _Scoreform_descitemlist_question33;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question33.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question33(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question33",v);
		_Scoreform_descitemlist_question33=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question34=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question34.
	 */
	public Integer getScoreform_descitemlist_question34() {
		try{
			if (_Scoreform_descitemlist_question34==null){
				_Scoreform_descitemlist_question34=getIntegerProperty("ScoreForm/descItemList/question34");
				return _Scoreform_descitemlist_question34;
			}else {
				return _Scoreform_descitemlist_question34;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question34.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question34(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question34",v);
		_Scoreform_descitemlist_question34=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question35=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question35.
	 */
	public Integer getScoreform_descitemlist_question35() {
		try{
			if (_Scoreform_descitemlist_question35==null){
				_Scoreform_descitemlist_question35=getIntegerProperty("ScoreForm/descItemList/question35");
				return _Scoreform_descitemlist_question35;
			}else {
				return _Scoreform_descitemlist_question35;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question35.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question35(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question35",v);
		_Scoreform_descitemlist_question35=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question36=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question36.
	 */
	public Integer getScoreform_descitemlist_question36() {
		try{
			if (_Scoreform_descitemlist_question36==null){
				_Scoreform_descitemlist_question36=getIntegerProperty("ScoreForm/descItemList/question36");
				return _Scoreform_descitemlist_question36;
			}else {
				return _Scoreform_descitemlist_question36;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question36.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question36(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question36",v);
		_Scoreform_descitemlist_question36=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question37=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question37.
	 */
	public Integer getScoreform_descitemlist_question37() {
		try{
			if (_Scoreform_descitemlist_question37==null){
				_Scoreform_descitemlist_question37=getIntegerProperty("ScoreForm/descItemList/question37");
				return _Scoreform_descitemlist_question37;
			}else {
				return _Scoreform_descitemlist_question37;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question37.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question37(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question37",v);
		_Scoreform_descitemlist_question37=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question38=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question38.
	 */
	public Integer getScoreform_descitemlist_question38() {
		try{
			if (_Scoreform_descitemlist_question38==null){
				_Scoreform_descitemlist_question38=getIntegerProperty("ScoreForm/descItemList/question38");
				return _Scoreform_descitemlist_question38;
			}else {
				return _Scoreform_descitemlist_question38;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question38.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question38(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question38",v);
		_Scoreform_descitemlist_question38=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question39=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question39.
	 */
	public Integer getScoreform_descitemlist_question39() {
		try{
			if (_Scoreform_descitemlist_question39==null){
				_Scoreform_descitemlist_question39=getIntegerProperty("ScoreForm/descItemList/question39");
				return _Scoreform_descitemlist_question39;
			}else {
				return _Scoreform_descitemlist_question39;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question39.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question39(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question39",v);
		_Scoreform_descitemlist_question39=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question40=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question40.
	 */
	public Integer getScoreform_descitemlist_question40() {
		try{
			if (_Scoreform_descitemlist_question40==null){
				_Scoreform_descitemlist_question40=getIntegerProperty("ScoreForm/descItemList/question40");
				return _Scoreform_descitemlist_question40;
			}else {
				return _Scoreform_descitemlist_question40;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question40.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question40(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question40",v);
		_Scoreform_descitemlist_question40=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_descitemlist_question40desc=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question40Desc.
	 */
	public String getScoreform_descitemlist_question40desc(){
		try{
			if (_Scoreform_descitemlist_question40desc==null){
				_Scoreform_descitemlist_question40desc=getStringProperty("ScoreForm/descItemList/question40Desc");
				return _Scoreform_descitemlist_question40desc;
			}else {
				return _Scoreform_descitemlist_question40desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question40Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question40desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question40Desc",v);
		_Scoreform_descitemlist_question40desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question41=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question41.
	 */
	public Integer getScoreform_descitemlist_question41() {
		try{
			if (_Scoreform_descitemlist_question41==null){
				_Scoreform_descitemlist_question41=getIntegerProperty("ScoreForm/descItemList/question41");
				return _Scoreform_descitemlist_question41;
			}else {
				return _Scoreform_descitemlist_question41;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question41.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question41(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question41",v);
		_Scoreform_descitemlist_question41=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question42=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question42.
	 */
	public Integer getScoreform_descitemlist_question42() {
		try{
			if (_Scoreform_descitemlist_question42==null){
				_Scoreform_descitemlist_question42=getIntegerProperty("ScoreForm/descItemList/question42");
				return _Scoreform_descitemlist_question42;
			}else {
				return _Scoreform_descitemlist_question42;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question42.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question42(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question42",v);
		_Scoreform_descitemlist_question42=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question43=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question43.
	 */
	public Integer getScoreform_descitemlist_question43() {
		try{
			if (_Scoreform_descitemlist_question43==null){
				_Scoreform_descitemlist_question43=getIntegerProperty("ScoreForm/descItemList/question43");
				return _Scoreform_descitemlist_question43;
			}else {
				return _Scoreform_descitemlist_question43;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question43.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question43(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question43",v);
		_Scoreform_descitemlist_question43=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question44=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question44.
	 */
	public Integer getScoreform_descitemlist_question44() {
		try{
			if (_Scoreform_descitemlist_question44==null){
				_Scoreform_descitemlist_question44=getIntegerProperty("ScoreForm/descItemList/question44");
				return _Scoreform_descitemlist_question44;
			}else {
				return _Scoreform_descitemlist_question44;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question44.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question44(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question44",v);
		_Scoreform_descitemlist_question44=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question45=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question45.
	 */
	public Integer getScoreform_descitemlist_question45() {
		try{
			if (_Scoreform_descitemlist_question45==null){
				_Scoreform_descitemlist_question45=getIntegerProperty("ScoreForm/descItemList/question45");
				return _Scoreform_descitemlist_question45;
			}else {
				return _Scoreform_descitemlist_question45;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question45.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question45(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question45",v);
		_Scoreform_descitemlist_question45=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question46=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question46.
	 */
	public Integer getScoreform_descitemlist_question46() {
		try{
			if (_Scoreform_descitemlist_question46==null){
				_Scoreform_descitemlist_question46=getIntegerProperty("ScoreForm/descItemList/question46");
				return _Scoreform_descitemlist_question46;
			}else {
				return _Scoreform_descitemlist_question46;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question46.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question46(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question46",v);
		_Scoreform_descitemlist_question46=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_descitemlist_question46desc=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question46Desc.
	 */
	public String getScoreform_descitemlist_question46desc(){
		try{
			if (_Scoreform_descitemlist_question46desc==null){
				_Scoreform_descitemlist_question46desc=getStringProperty("ScoreForm/descItemList/question46Desc");
				return _Scoreform_descitemlist_question46desc;
			}else {
				return _Scoreform_descitemlist_question46desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question46Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question46desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question46Desc",v);
		_Scoreform_descitemlist_question46desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question47=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question47.
	 */
	public Integer getScoreform_descitemlist_question47() {
		try{
			if (_Scoreform_descitemlist_question47==null){
				_Scoreform_descitemlist_question47=getIntegerProperty("ScoreForm/descItemList/question47");
				return _Scoreform_descitemlist_question47;
			}else {
				return _Scoreform_descitemlist_question47;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question47.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question47(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question47",v);
		_Scoreform_descitemlist_question47=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question48=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question48.
	 */
	public Integer getScoreform_descitemlist_question48() {
		try{
			if (_Scoreform_descitemlist_question48==null){
				_Scoreform_descitemlist_question48=getIntegerProperty("ScoreForm/descItemList/question48");
				return _Scoreform_descitemlist_question48;
			}else {
				return _Scoreform_descitemlist_question48;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question48.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question48(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question48",v);
		_Scoreform_descitemlist_question48=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question49=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question49.
	 */
	public Integer getScoreform_descitemlist_question49() {
		try{
			if (_Scoreform_descitemlist_question49==null){
				_Scoreform_descitemlist_question49=getIntegerProperty("ScoreForm/descItemList/question49");
				return _Scoreform_descitemlist_question49;
			}else {
				return _Scoreform_descitemlist_question49;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question49.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question49(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question49",v);
		_Scoreform_descitemlist_question49=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question50=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question50.
	 */
	public Integer getScoreform_descitemlist_question50() {
		try{
			if (_Scoreform_descitemlist_question50==null){
				_Scoreform_descitemlist_question50=getIntegerProperty("ScoreForm/descItemList/question50");
				return _Scoreform_descitemlist_question50;
			}else {
				return _Scoreform_descitemlist_question50;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question50.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question50(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question50",v);
		_Scoreform_descitemlist_question50=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question51=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question51.
	 */
	public Integer getScoreform_descitemlist_question51() {
		try{
			if (_Scoreform_descitemlist_question51==null){
				_Scoreform_descitemlist_question51=getIntegerProperty("ScoreForm/descItemList/question51");
				return _Scoreform_descitemlist_question51;
			}else {
				return _Scoreform_descitemlist_question51;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question51.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question51(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question51",v);
		_Scoreform_descitemlist_question51=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question52=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question52.
	 */
	public Integer getScoreform_descitemlist_question52() {
		try{
			if (_Scoreform_descitemlist_question52==null){
				_Scoreform_descitemlist_question52=getIntegerProperty("ScoreForm/descItemList/question52");
				return _Scoreform_descitemlist_question52;
			}else {
				return _Scoreform_descitemlist_question52;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question52.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question52(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question52",v);
		_Scoreform_descitemlist_question52=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question53=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question53.
	 */
	public Integer getScoreform_descitemlist_question53() {
		try{
			if (_Scoreform_descitemlist_question53==null){
				_Scoreform_descitemlist_question53=getIntegerProperty("ScoreForm/descItemList/question53");
				return _Scoreform_descitemlist_question53;
			}else {
				return _Scoreform_descitemlist_question53;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question53.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question53(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question53",v);
		_Scoreform_descitemlist_question53=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question54=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question54.
	 */
	public Integer getScoreform_descitemlist_question54() {
		try{
			if (_Scoreform_descitemlist_question54==null){
				_Scoreform_descitemlist_question54=getIntegerProperty("ScoreForm/descItemList/question54");
				return _Scoreform_descitemlist_question54;
			}else {
				return _Scoreform_descitemlist_question54;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question54.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question54(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question54",v);
		_Scoreform_descitemlist_question54=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question55=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question55.
	 */
	public Integer getScoreform_descitemlist_question55() {
		try{
			if (_Scoreform_descitemlist_question55==null){
				_Scoreform_descitemlist_question55=getIntegerProperty("ScoreForm/descItemList/question55");
				return _Scoreform_descitemlist_question55;
			}else {
				return _Scoreform_descitemlist_question55;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question55.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question55(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question55",v);
		_Scoreform_descitemlist_question55=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question56a=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question56a.
	 */
	public Integer getScoreform_descitemlist_question56a() {
		try{
			if (_Scoreform_descitemlist_question56a==null){
				_Scoreform_descitemlist_question56a=getIntegerProperty("ScoreForm/descItemList/question56a");
				return _Scoreform_descitemlist_question56a;
			}else {
				return _Scoreform_descitemlist_question56a;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question56a.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question56a(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question56a",v);
		_Scoreform_descitemlist_question56a=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question56b=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question56b.
	 */
	public Integer getScoreform_descitemlist_question56b() {
		try{
			if (_Scoreform_descitemlist_question56b==null){
				_Scoreform_descitemlist_question56b=getIntegerProperty("ScoreForm/descItemList/question56b");
				return _Scoreform_descitemlist_question56b;
			}else {
				return _Scoreform_descitemlist_question56b;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question56b.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question56b(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question56b",v);
		_Scoreform_descitemlist_question56b=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question56c=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question56c.
	 */
	public Integer getScoreform_descitemlist_question56c() {
		try{
			if (_Scoreform_descitemlist_question56c==null){
				_Scoreform_descitemlist_question56c=getIntegerProperty("ScoreForm/descItemList/question56c");
				return _Scoreform_descitemlist_question56c;
			}else {
				return _Scoreform_descitemlist_question56c;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question56c.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question56c(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question56c",v);
		_Scoreform_descitemlist_question56c=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question56d=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question56d.
	 */
	public Integer getScoreform_descitemlist_question56d() {
		try{
			if (_Scoreform_descitemlist_question56d==null){
				_Scoreform_descitemlist_question56d=getIntegerProperty("ScoreForm/descItemList/question56d");
				return _Scoreform_descitemlist_question56d;
			}else {
				return _Scoreform_descitemlist_question56d;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question56d.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question56d(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question56d",v);
		_Scoreform_descitemlist_question56d=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_descitemlist_question56ddesc=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question56dDesc.
	 */
	public String getScoreform_descitemlist_question56ddesc(){
		try{
			if (_Scoreform_descitemlist_question56ddesc==null){
				_Scoreform_descitemlist_question56ddesc=getStringProperty("ScoreForm/descItemList/question56dDesc");
				return _Scoreform_descitemlist_question56ddesc;
			}else {
				return _Scoreform_descitemlist_question56ddesc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question56dDesc.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question56ddesc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question56dDesc",v);
		_Scoreform_descitemlist_question56ddesc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question56e=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question56e.
	 */
	public Integer getScoreform_descitemlist_question56e() {
		try{
			if (_Scoreform_descitemlist_question56e==null){
				_Scoreform_descitemlist_question56e=getIntegerProperty("ScoreForm/descItemList/question56e");
				return _Scoreform_descitemlist_question56e;
			}else {
				return _Scoreform_descitemlist_question56e;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question56e.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question56e(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question56e",v);
		_Scoreform_descitemlist_question56e=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question56f=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question56f.
	 */
	public Integer getScoreform_descitemlist_question56f() {
		try{
			if (_Scoreform_descitemlist_question56f==null){
				_Scoreform_descitemlist_question56f=getIntegerProperty("ScoreForm/descItemList/question56f");
				return _Scoreform_descitemlist_question56f;
			}else {
				return _Scoreform_descitemlist_question56f;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question56f.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question56f(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question56f",v);
		_Scoreform_descitemlist_question56f=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question56g=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question56g.
	 */
	public Integer getScoreform_descitemlist_question56g() {
		try{
			if (_Scoreform_descitemlist_question56g==null){
				_Scoreform_descitemlist_question56g=getIntegerProperty("ScoreForm/descItemList/question56g");
				return _Scoreform_descitemlist_question56g;
			}else {
				return _Scoreform_descitemlist_question56g;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question56g.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question56g(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question56g",v);
		_Scoreform_descitemlist_question56g=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_descitemlist_question56h=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question56h.
	 */
	public String getScoreform_descitemlist_question56h(){
		try{
			if (_Scoreform_descitemlist_question56h==null){
				_Scoreform_descitemlist_question56h=getStringProperty("ScoreForm/descItemList/question56h");
				return _Scoreform_descitemlist_question56h;
			}else {
				return _Scoreform_descitemlist_question56h;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question56h.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question56h(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question56h",v);
		_Scoreform_descitemlist_question56h=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question57=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question57.
	 */
	public Integer getScoreform_descitemlist_question57() {
		try{
			if (_Scoreform_descitemlist_question57==null){
				_Scoreform_descitemlist_question57=getIntegerProperty("ScoreForm/descItemList/question57");
				return _Scoreform_descitemlist_question57;
			}else {
				return _Scoreform_descitemlist_question57;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question57.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question57(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question57",v);
		_Scoreform_descitemlist_question57=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question58=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question58.
	 */
	public Integer getScoreform_descitemlist_question58() {
		try{
			if (_Scoreform_descitemlist_question58==null){
				_Scoreform_descitemlist_question58=getIntegerProperty("ScoreForm/descItemList/question58");
				return _Scoreform_descitemlist_question58;
			}else {
				return _Scoreform_descitemlist_question58;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question58.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question58(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question58",v);
		_Scoreform_descitemlist_question58=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_descitemlist_question58desc=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question58Desc.
	 */
	public String getScoreform_descitemlist_question58desc(){
		try{
			if (_Scoreform_descitemlist_question58desc==null){
				_Scoreform_descitemlist_question58desc=getStringProperty("ScoreForm/descItemList/question58Desc");
				return _Scoreform_descitemlist_question58desc;
			}else {
				return _Scoreform_descitemlist_question58desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question58Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question58desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question58Desc",v);
		_Scoreform_descitemlist_question58desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question59=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question59.
	 */
	public Integer getScoreform_descitemlist_question59() {
		try{
			if (_Scoreform_descitemlist_question59==null){
				_Scoreform_descitemlist_question59=getIntegerProperty("ScoreForm/descItemList/question59");
				return _Scoreform_descitemlist_question59;
			}else {
				return _Scoreform_descitemlist_question59;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question59.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question59(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question59",v);
		_Scoreform_descitemlist_question59=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question60=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question60.
	 */
	public Integer getScoreform_descitemlist_question60() {
		try{
			if (_Scoreform_descitemlist_question60==null){
				_Scoreform_descitemlist_question60=getIntegerProperty("ScoreForm/descItemList/question60");
				return _Scoreform_descitemlist_question60;
			}else {
				return _Scoreform_descitemlist_question60;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question60.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question60(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question60",v);
		_Scoreform_descitemlist_question60=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question61=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question61.
	 */
	public Integer getScoreform_descitemlist_question61() {
		try{
			if (_Scoreform_descitemlist_question61==null){
				_Scoreform_descitemlist_question61=getIntegerProperty("ScoreForm/descItemList/question61");
				return _Scoreform_descitemlist_question61;
			}else {
				return _Scoreform_descitemlist_question61;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question61.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question61(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question61",v);
		_Scoreform_descitemlist_question61=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question62=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question62.
	 */
	public Integer getScoreform_descitemlist_question62() {
		try{
			if (_Scoreform_descitemlist_question62==null){
				_Scoreform_descitemlist_question62=getIntegerProperty("ScoreForm/descItemList/question62");
				return _Scoreform_descitemlist_question62;
			}else {
				return _Scoreform_descitemlist_question62;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question62.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question62(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question62",v);
		_Scoreform_descitemlist_question62=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question63=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question63.
	 */
	public Integer getScoreform_descitemlist_question63() {
		try{
			if (_Scoreform_descitemlist_question63==null){
				_Scoreform_descitemlist_question63=getIntegerProperty("ScoreForm/descItemList/question63");
				return _Scoreform_descitemlist_question63;
			}else {
				return _Scoreform_descitemlist_question63;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question63.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question63(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question63",v);
		_Scoreform_descitemlist_question63=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question64=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question64.
	 */
	public Integer getScoreform_descitemlist_question64() {
		try{
			if (_Scoreform_descitemlist_question64==null){
				_Scoreform_descitemlist_question64=getIntegerProperty("ScoreForm/descItemList/question64");
				return _Scoreform_descitemlist_question64;
			}else {
				return _Scoreform_descitemlist_question64;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question64.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question64(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question64",v);
		_Scoreform_descitemlist_question64=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question65=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question65.
	 */
	public Integer getScoreform_descitemlist_question65() {
		try{
			if (_Scoreform_descitemlist_question65==null){
				_Scoreform_descitemlist_question65=getIntegerProperty("ScoreForm/descItemList/question65");
				return _Scoreform_descitemlist_question65;
			}else {
				return _Scoreform_descitemlist_question65;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question65.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question65(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question65",v);
		_Scoreform_descitemlist_question65=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question66=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question66.
	 */
	public Integer getScoreform_descitemlist_question66() {
		try{
			if (_Scoreform_descitemlist_question66==null){
				_Scoreform_descitemlist_question66=getIntegerProperty("ScoreForm/descItemList/question66");
				return _Scoreform_descitemlist_question66;
			}else {
				return _Scoreform_descitemlist_question66;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question66.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question66(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question66",v);
		_Scoreform_descitemlist_question66=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_descitemlist_question66desc=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question66Desc.
	 */
	public String getScoreform_descitemlist_question66desc(){
		try{
			if (_Scoreform_descitemlist_question66desc==null){
				_Scoreform_descitemlist_question66desc=getStringProperty("ScoreForm/descItemList/question66Desc");
				return _Scoreform_descitemlist_question66desc;
			}else {
				return _Scoreform_descitemlist_question66desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question66Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question66desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question66Desc",v);
		_Scoreform_descitemlist_question66desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question67=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question67.
	 */
	public Integer getScoreform_descitemlist_question67() {
		try{
			if (_Scoreform_descitemlist_question67==null){
				_Scoreform_descitemlist_question67=getIntegerProperty("ScoreForm/descItemList/question67");
				return _Scoreform_descitemlist_question67;
			}else {
				return _Scoreform_descitemlist_question67;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question67.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question67(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question67",v);
		_Scoreform_descitemlist_question67=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question68=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question68.
	 */
	public Integer getScoreform_descitemlist_question68() {
		try{
			if (_Scoreform_descitemlist_question68==null){
				_Scoreform_descitemlist_question68=getIntegerProperty("ScoreForm/descItemList/question68");
				return _Scoreform_descitemlist_question68;
			}else {
				return _Scoreform_descitemlist_question68;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question68.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question68(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question68",v);
		_Scoreform_descitemlist_question68=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question69=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question69.
	 */
	public Integer getScoreform_descitemlist_question69() {
		try{
			if (_Scoreform_descitemlist_question69==null){
				_Scoreform_descitemlist_question69=getIntegerProperty("ScoreForm/descItemList/question69");
				return _Scoreform_descitemlist_question69;
			}else {
				return _Scoreform_descitemlist_question69;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question69.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question69(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question69",v);
		_Scoreform_descitemlist_question69=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question70=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question70.
	 */
	public Integer getScoreform_descitemlist_question70() {
		try{
			if (_Scoreform_descitemlist_question70==null){
				_Scoreform_descitemlist_question70=getIntegerProperty("ScoreForm/descItemList/question70");
				return _Scoreform_descitemlist_question70;
			}else {
				return _Scoreform_descitemlist_question70;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question70.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question70(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question70",v);
		_Scoreform_descitemlist_question70=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_descitemlist_question70desc=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question70Desc.
	 */
	public String getScoreform_descitemlist_question70desc(){
		try{
			if (_Scoreform_descitemlist_question70desc==null){
				_Scoreform_descitemlist_question70desc=getStringProperty("ScoreForm/descItemList/question70Desc");
				return _Scoreform_descitemlist_question70desc;
			}else {
				return _Scoreform_descitemlist_question70desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question70Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question70desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question70Desc",v);
		_Scoreform_descitemlist_question70desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question71=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question71.
	 */
	public Integer getScoreform_descitemlist_question71() {
		try{
			if (_Scoreform_descitemlist_question71==null){
				_Scoreform_descitemlist_question71=getIntegerProperty("ScoreForm/descItemList/question71");
				return _Scoreform_descitemlist_question71;
			}else {
				return _Scoreform_descitemlist_question71;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question71.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question71(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question71",v);
		_Scoreform_descitemlist_question71=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question72=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question72.
	 */
	public Integer getScoreform_descitemlist_question72() {
		try{
			if (_Scoreform_descitemlist_question72==null){
				_Scoreform_descitemlist_question72=getIntegerProperty("ScoreForm/descItemList/question72");
				return _Scoreform_descitemlist_question72;
			}else {
				return _Scoreform_descitemlist_question72;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question72.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question72(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question72",v);
		_Scoreform_descitemlist_question72=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question73=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question73.
	 */
	public Integer getScoreform_descitemlist_question73() {
		try{
			if (_Scoreform_descitemlist_question73==null){
				_Scoreform_descitemlist_question73=getIntegerProperty("ScoreForm/descItemList/question73");
				return _Scoreform_descitemlist_question73;
			}else {
				return _Scoreform_descitemlist_question73;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question73.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question73(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question73",v);
		_Scoreform_descitemlist_question73=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_descitemlist_question73desc=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question73Desc.
	 */
	public String getScoreform_descitemlist_question73desc(){
		try{
			if (_Scoreform_descitemlist_question73desc==null){
				_Scoreform_descitemlist_question73desc=getStringProperty("ScoreForm/descItemList/question73Desc");
				return _Scoreform_descitemlist_question73desc;
			}else {
				return _Scoreform_descitemlist_question73desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question73Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question73desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question73Desc",v);
		_Scoreform_descitemlist_question73desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question74=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question74.
	 */
	public Integer getScoreform_descitemlist_question74() {
		try{
			if (_Scoreform_descitemlist_question74==null){
				_Scoreform_descitemlist_question74=getIntegerProperty("ScoreForm/descItemList/question74");
				return _Scoreform_descitemlist_question74;
			}else {
				return _Scoreform_descitemlist_question74;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question74.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question74(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question74",v);
		_Scoreform_descitemlist_question74=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question75=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question75.
	 */
	public Integer getScoreform_descitemlist_question75() {
		try{
			if (_Scoreform_descitemlist_question75==null){
				_Scoreform_descitemlist_question75=getIntegerProperty("ScoreForm/descItemList/question75");
				return _Scoreform_descitemlist_question75;
			}else {
				return _Scoreform_descitemlist_question75;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question75.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question75(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question75",v);
		_Scoreform_descitemlist_question75=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question76=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question76.
	 */
	public Integer getScoreform_descitemlist_question76() {
		try{
			if (_Scoreform_descitemlist_question76==null){
				_Scoreform_descitemlist_question76=getIntegerProperty("ScoreForm/descItemList/question76");
				return _Scoreform_descitemlist_question76;
			}else {
				return _Scoreform_descitemlist_question76;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question76.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question76(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question76",v);
		_Scoreform_descitemlist_question76=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question77=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question77.
	 */
	public Integer getScoreform_descitemlist_question77() {
		try{
			if (_Scoreform_descitemlist_question77==null){
				_Scoreform_descitemlist_question77=getIntegerProperty("ScoreForm/descItemList/question77");
				return _Scoreform_descitemlist_question77;
			}else {
				return _Scoreform_descitemlist_question77;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question77.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question77(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question77",v);
		_Scoreform_descitemlist_question77=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_descitemlist_question77desc=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question77Desc.
	 */
	public String getScoreform_descitemlist_question77desc(){
		try{
			if (_Scoreform_descitemlist_question77desc==null){
				_Scoreform_descitemlist_question77desc=getStringProperty("ScoreForm/descItemList/question77Desc");
				return _Scoreform_descitemlist_question77desc;
			}else {
				return _Scoreform_descitemlist_question77desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question77Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question77desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question77Desc",v);
		_Scoreform_descitemlist_question77desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question78=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question78.
	 */
	public Integer getScoreform_descitemlist_question78() {
		try{
			if (_Scoreform_descitemlist_question78==null){
				_Scoreform_descitemlist_question78=getIntegerProperty("ScoreForm/descItemList/question78");
				return _Scoreform_descitemlist_question78;
			}else {
				return _Scoreform_descitemlist_question78;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question78.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question78(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question78",v);
		_Scoreform_descitemlist_question78=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question79=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question79.
	 */
	public Integer getScoreform_descitemlist_question79() {
		try{
			if (_Scoreform_descitemlist_question79==null){
				_Scoreform_descitemlist_question79=getIntegerProperty("ScoreForm/descItemList/question79");
				return _Scoreform_descitemlist_question79;
			}else {
				return _Scoreform_descitemlist_question79;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question79.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question79(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question79",v);
		_Scoreform_descitemlist_question79=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_descitemlist_question79desc=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question79Desc.
	 */
	public String getScoreform_descitemlist_question79desc(){
		try{
			if (_Scoreform_descitemlist_question79desc==null){
				_Scoreform_descitemlist_question79desc=getStringProperty("ScoreForm/descItemList/question79Desc");
				return _Scoreform_descitemlist_question79desc;
			}else {
				return _Scoreform_descitemlist_question79desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question79Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question79desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question79Desc",v);
		_Scoreform_descitemlist_question79desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question80=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question80.
	 */
	public Integer getScoreform_descitemlist_question80() {
		try{
			if (_Scoreform_descitemlist_question80==null){
				_Scoreform_descitemlist_question80=getIntegerProperty("ScoreForm/descItemList/question80");
				return _Scoreform_descitemlist_question80;
			}else {
				return _Scoreform_descitemlist_question80;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question80.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question80(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question80",v);
		_Scoreform_descitemlist_question80=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question81=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question81.
	 */
	public Integer getScoreform_descitemlist_question81() {
		try{
			if (_Scoreform_descitemlist_question81==null){
				_Scoreform_descitemlist_question81=getIntegerProperty("ScoreForm/descItemList/question81");
				return _Scoreform_descitemlist_question81;
			}else {
				return _Scoreform_descitemlist_question81;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question81.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question81(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question81",v);
		_Scoreform_descitemlist_question81=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question82=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question82.
	 */
	public Integer getScoreform_descitemlist_question82() {
		try{
			if (_Scoreform_descitemlist_question82==null){
				_Scoreform_descitemlist_question82=getIntegerProperty("ScoreForm/descItemList/question82");
				return _Scoreform_descitemlist_question82;
			}else {
				return _Scoreform_descitemlist_question82;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question82.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question82(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question82",v);
		_Scoreform_descitemlist_question82=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question83=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question83.
	 */
	public Integer getScoreform_descitemlist_question83() {
		try{
			if (_Scoreform_descitemlist_question83==null){
				_Scoreform_descitemlist_question83=getIntegerProperty("ScoreForm/descItemList/question83");
				return _Scoreform_descitemlist_question83;
			}else {
				return _Scoreform_descitemlist_question83;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question83.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question83(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question83",v);
		_Scoreform_descitemlist_question83=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_descitemlist_question83desc=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question83Desc.
	 */
	public String getScoreform_descitemlist_question83desc(){
		try{
			if (_Scoreform_descitemlist_question83desc==null){
				_Scoreform_descitemlist_question83desc=getStringProperty("ScoreForm/descItemList/question83Desc");
				return _Scoreform_descitemlist_question83desc;
			}else {
				return _Scoreform_descitemlist_question83desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question83Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question83desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question83Desc",v);
		_Scoreform_descitemlist_question83desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question84=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question84.
	 */
	public Integer getScoreform_descitemlist_question84() {
		try{
			if (_Scoreform_descitemlist_question84==null){
				_Scoreform_descitemlist_question84=getIntegerProperty("ScoreForm/descItemList/question84");
				return _Scoreform_descitemlist_question84;
			}else {
				return _Scoreform_descitemlist_question84;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question84.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question84(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question84",v);
		_Scoreform_descitemlist_question84=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_descitemlist_question84desc=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question84Desc.
	 */
	public String getScoreform_descitemlist_question84desc(){
		try{
			if (_Scoreform_descitemlist_question84desc==null){
				_Scoreform_descitemlist_question84desc=getStringProperty("ScoreForm/descItemList/question84Desc");
				return _Scoreform_descitemlist_question84desc;
			}else {
				return _Scoreform_descitemlist_question84desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question84Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question84desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question84Desc",v);
		_Scoreform_descitemlist_question84desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question85=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question85.
	 */
	public Integer getScoreform_descitemlist_question85() {
		try{
			if (_Scoreform_descitemlist_question85==null){
				_Scoreform_descitemlist_question85=getIntegerProperty("ScoreForm/descItemList/question85");
				return _Scoreform_descitemlist_question85;
			}else {
				return _Scoreform_descitemlist_question85;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question85.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question85(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question85",v);
		_Scoreform_descitemlist_question85=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_descitemlist_question85desc=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question85Desc.
	 */
	public String getScoreform_descitemlist_question85desc(){
		try{
			if (_Scoreform_descitemlist_question85desc==null){
				_Scoreform_descitemlist_question85desc=getStringProperty("ScoreForm/descItemList/question85Desc");
				return _Scoreform_descitemlist_question85desc;
			}else {
				return _Scoreform_descitemlist_question85desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question85Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question85desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question85Desc",v);
		_Scoreform_descitemlist_question85desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question86=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question86.
	 */
	public Integer getScoreform_descitemlist_question86() {
		try{
			if (_Scoreform_descitemlist_question86==null){
				_Scoreform_descitemlist_question86=getIntegerProperty("ScoreForm/descItemList/question86");
				return _Scoreform_descitemlist_question86;
			}else {
				return _Scoreform_descitemlist_question86;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question86.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question86(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question86",v);
		_Scoreform_descitemlist_question86=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question87=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question87.
	 */
	public Integer getScoreform_descitemlist_question87() {
		try{
			if (_Scoreform_descitemlist_question87==null){
				_Scoreform_descitemlist_question87=getIntegerProperty("ScoreForm/descItemList/question87");
				return _Scoreform_descitemlist_question87;
			}else {
				return _Scoreform_descitemlist_question87;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question87.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question87(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question87",v);
		_Scoreform_descitemlist_question87=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question88=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question88.
	 */
	public Integer getScoreform_descitemlist_question88() {
		try{
			if (_Scoreform_descitemlist_question88==null){
				_Scoreform_descitemlist_question88=getIntegerProperty("ScoreForm/descItemList/question88");
				return _Scoreform_descitemlist_question88;
			}else {
				return _Scoreform_descitemlist_question88;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question88.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question88(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question88",v);
		_Scoreform_descitemlist_question88=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question89=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question89.
	 */
	public Integer getScoreform_descitemlist_question89() {
		try{
			if (_Scoreform_descitemlist_question89==null){
				_Scoreform_descitemlist_question89=getIntegerProperty("ScoreForm/descItemList/question89");
				return _Scoreform_descitemlist_question89;
			}else {
				return _Scoreform_descitemlist_question89;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question89.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question89(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question89",v);
		_Scoreform_descitemlist_question89=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question90=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question90.
	 */
	public Integer getScoreform_descitemlist_question90() {
		try{
			if (_Scoreform_descitemlist_question90==null){
				_Scoreform_descitemlist_question90=getIntegerProperty("ScoreForm/descItemList/question90");
				return _Scoreform_descitemlist_question90;
			}else {
				return _Scoreform_descitemlist_question90;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question90.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question90(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question90",v);
		_Scoreform_descitemlist_question90=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question91=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question91.
	 */
	public Integer getScoreform_descitemlist_question91() {
		try{
			if (_Scoreform_descitemlist_question91==null){
				_Scoreform_descitemlist_question91=getIntegerProperty("ScoreForm/descItemList/question91");
				return _Scoreform_descitemlist_question91;
			}else {
				return _Scoreform_descitemlist_question91;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question91.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question91(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question91",v);
		_Scoreform_descitemlist_question91=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question92=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question92.
	 */
	public Integer getScoreform_descitemlist_question92() {
		try{
			if (_Scoreform_descitemlist_question92==null){
				_Scoreform_descitemlist_question92=getIntegerProperty("ScoreForm/descItemList/question92");
				return _Scoreform_descitemlist_question92;
			}else {
				return _Scoreform_descitemlist_question92;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question92.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question92(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question92",v);
		_Scoreform_descitemlist_question92=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_descitemlist_question92desc=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question92Desc.
	 */
	public String getScoreform_descitemlist_question92desc(){
		try{
			if (_Scoreform_descitemlist_question92desc==null){
				_Scoreform_descitemlist_question92desc=getStringProperty("ScoreForm/descItemList/question92Desc");
				return _Scoreform_descitemlist_question92desc;
			}else {
				return _Scoreform_descitemlist_question92desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question92Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question92desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question92Desc",v);
		_Scoreform_descitemlist_question92desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question93=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question93.
	 */
	public Integer getScoreform_descitemlist_question93() {
		try{
			if (_Scoreform_descitemlist_question93==null){
				_Scoreform_descitemlist_question93=getIntegerProperty("ScoreForm/descItemList/question93");
				return _Scoreform_descitemlist_question93;
			}else {
				return _Scoreform_descitemlist_question93;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question93.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question93(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question93",v);
		_Scoreform_descitemlist_question93=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question94=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question94.
	 */
	public Integer getScoreform_descitemlist_question94() {
		try{
			if (_Scoreform_descitemlist_question94==null){
				_Scoreform_descitemlist_question94=getIntegerProperty("ScoreForm/descItemList/question94");
				return _Scoreform_descitemlist_question94;
			}else {
				return _Scoreform_descitemlist_question94;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question94.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question94(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question94",v);
		_Scoreform_descitemlist_question94=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question95=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question95.
	 */
	public Integer getScoreform_descitemlist_question95() {
		try{
			if (_Scoreform_descitemlist_question95==null){
				_Scoreform_descitemlist_question95=getIntegerProperty("ScoreForm/descItemList/question95");
				return _Scoreform_descitemlist_question95;
			}else {
				return _Scoreform_descitemlist_question95;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question95.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question95(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question95",v);
		_Scoreform_descitemlist_question95=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question96=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question96.
	 */
	public Integer getScoreform_descitemlist_question96() {
		try{
			if (_Scoreform_descitemlist_question96==null){
				_Scoreform_descitemlist_question96=getIntegerProperty("ScoreForm/descItemList/question96");
				return _Scoreform_descitemlist_question96;
			}else {
				return _Scoreform_descitemlist_question96;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question96.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question96(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question96",v);
		_Scoreform_descitemlist_question96=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question97=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question97.
	 */
	public Integer getScoreform_descitemlist_question97() {
		try{
			if (_Scoreform_descitemlist_question97==null){
				_Scoreform_descitemlist_question97=getIntegerProperty("ScoreForm/descItemList/question97");
				return _Scoreform_descitemlist_question97;
			}else {
				return _Scoreform_descitemlist_question97;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question97.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question97(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question97",v);
		_Scoreform_descitemlist_question97=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question98=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question98.
	 */
	public Integer getScoreform_descitemlist_question98() {
		try{
			if (_Scoreform_descitemlist_question98==null){
				_Scoreform_descitemlist_question98=getIntegerProperty("ScoreForm/descItemList/question98");
				return _Scoreform_descitemlist_question98;
			}else {
				return _Scoreform_descitemlist_question98;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question98.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question98(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question98",v);
		_Scoreform_descitemlist_question98=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question99=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question99.
	 */
	public Integer getScoreform_descitemlist_question99() {
		try{
			if (_Scoreform_descitemlist_question99==null){
				_Scoreform_descitemlist_question99=getIntegerProperty("ScoreForm/descItemList/question99");
				return _Scoreform_descitemlist_question99;
			}else {
				return _Scoreform_descitemlist_question99;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question99.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question99(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question99",v);
		_Scoreform_descitemlist_question99=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question100=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question100.
	 */
	public Integer getScoreform_descitemlist_question100() {
		try{
			if (_Scoreform_descitemlist_question100==null){
				_Scoreform_descitemlist_question100=getIntegerProperty("ScoreForm/descItemList/question100");
				return _Scoreform_descitemlist_question100;
			}else {
				return _Scoreform_descitemlist_question100;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question100.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question100(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question100",v);
		_Scoreform_descitemlist_question100=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_descitemlist_question100desc=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question100Desc.
	 */
	public String getScoreform_descitemlist_question100desc(){
		try{
			if (_Scoreform_descitemlist_question100desc==null){
				_Scoreform_descitemlist_question100desc=getStringProperty("ScoreForm/descItemList/question100Desc");
				return _Scoreform_descitemlist_question100desc;
			}else {
				return _Scoreform_descitemlist_question100desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question100Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question100desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question100Desc",v);
		_Scoreform_descitemlist_question100desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question101=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question101.
	 */
	public Integer getScoreform_descitemlist_question101() {
		try{
			if (_Scoreform_descitemlist_question101==null){
				_Scoreform_descitemlist_question101=getIntegerProperty("ScoreForm/descItemList/question101");
				return _Scoreform_descitemlist_question101;
			}else {
				return _Scoreform_descitemlist_question101;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question101.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question101(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question101",v);
		_Scoreform_descitemlist_question101=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question102=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question102.
	 */
	public Integer getScoreform_descitemlist_question102() {
		try{
			if (_Scoreform_descitemlist_question102==null){
				_Scoreform_descitemlist_question102=getIntegerProperty("ScoreForm/descItemList/question102");
				return _Scoreform_descitemlist_question102;
			}else {
				return _Scoreform_descitemlist_question102;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question102.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question102(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question102",v);
		_Scoreform_descitemlist_question102=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question103=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question103.
	 */
	public Integer getScoreform_descitemlist_question103() {
		try{
			if (_Scoreform_descitemlist_question103==null){
				_Scoreform_descitemlist_question103=getIntegerProperty("ScoreForm/descItemList/question103");
				return _Scoreform_descitemlist_question103;
			}else {
				return _Scoreform_descitemlist_question103;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question103.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question103(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question103",v);
		_Scoreform_descitemlist_question103=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question104=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question104.
	 */
	public Integer getScoreform_descitemlist_question104() {
		try{
			if (_Scoreform_descitemlist_question104==null){
				_Scoreform_descitemlist_question104=getIntegerProperty("ScoreForm/descItemList/question104");
				return _Scoreform_descitemlist_question104;
			}else {
				return _Scoreform_descitemlist_question104;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question104.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question104(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question104",v);
		_Scoreform_descitemlist_question104=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question105=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question105.
	 */
	public Integer getScoreform_descitemlist_question105() {
		try{
			if (_Scoreform_descitemlist_question105==null){
				_Scoreform_descitemlist_question105=getIntegerProperty("ScoreForm/descItemList/question105");
				return _Scoreform_descitemlist_question105;
			}else {
				return _Scoreform_descitemlist_question105;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question105.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question105(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question105",v);
		_Scoreform_descitemlist_question105=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_descitemlist_question105desc=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question105Desc.
	 */
	public String getScoreform_descitemlist_question105desc(){
		try{
			if (_Scoreform_descitemlist_question105desc==null){
				_Scoreform_descitemlist_question105desc=getStringProperty("ScoreForm/descItemList/question105Desc");
				return _Scoreform_descitemlist_question105desc;
			}else {
				return _Scoreform_descitemlist_question105desc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question105Desc.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question105desc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question105Desc",v);
		_Scoreform_descitemlist_question105desc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question106=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question106.
	 */
	public Integer getScoreform_descitemlist_question106() {
		try{
			if (_Scoreform_descitemlist_question106==null){
				_Scoreform_descitemlist_question106=getIntegerProperty("ScoreForm/descItemList/question106");
				return _Scoreform_descitemlist_question106;
			}else {
				return _Scoreform_descitemlist_question106;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question106.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question106(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question106",v);
		_Scoreform_descitemlist_question106=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question107=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question107.
	 */
	public Integer getScoreform_descitemlist_question107() {
		try{
			if (_Scoreform_descitemlist_question107==null){
				_Scoreform_descitemlist_question107=getIntegerProperty("ScoreForm/descItemList/question107");
				return _Scoreform_descitemlist_question107;
			}else {
				return _Scoreform_descitemlist_question107;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question107.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question107(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question107",v);
		_Scoreform_descitemlist_question107=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question108=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question108.
	 */
	public Integer getScoreform_descitemlist_question108() {
		try{
			if (_Scoreform_descitemlist_question108==null){
				_Scoreform_descitemlist_question108=getIntegerProperty("ScoreForm/descItemList/question108");
				return _Scoreform_descitemlist_question108;
			}else {
				return _Scoreform_descitemlist_question108;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question108.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question108(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question108",v);
		_Scoreform_descitemlist_question108=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question109=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question109.
	 */
	public Integer getScoreform_descitemlist_question109() {
		try{
			if (_Scoreform_descitemlist_question109==null){
				_Scoreform_descitemlist_question109=getIntegerProperty("ScoreForm/descItemList/question109");
				return _Scoreform_descitemlist_question109;
			}else {
				return _Scoreform_descitemlist_question109;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question109.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question109(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question109",v);
		_Scoreform_descitemlist_question109=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question110=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question110.
	 */
	public Integer getScoreform_descitemlist_question110() {
		try{
			if (_Scoreform_descitemlist_question110==null){
				_Scoreform_descitemlist_question110=getIntegerProperty("ScoreForm/descItemList/question110");
				return _Scoreform_descitemlist_question110;
			}else {
				return _Scoreform_descitemlist_question110;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question110.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question110(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question110",v);
		_Scoreform_descitemlist_question110=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question111=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question111.
	 */
	public Integer getScoreform_descitemlist_question111() {
		try{
			if (_Scoreform_descitemlist_question111==null){
				_Scoreform_descitemlist_question111=getIntegerProperty("ScoreForm/descItemList/question111");
				return _Scoreform_descitemlist_question111;
			}else {
				return _Scoreform_descitemlist_question111;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question111.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question111(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question111",v);
		_Scoreform_descitemlist_question111=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question112=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question112.
	 */
	public Integer getScoreform_descitemlist_question112() {
		try{
			if (_Scoreform_descitemlist_question112==null){
				_Scoreform_descitemlist_question112=getIntegerProperty("ScoreForm/descItemList/question112");
				return _Scoreform_descitemlist_question112;
			}else {
				return _Scoreform_descitemlist_question112;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question112.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question112(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question112",v);
		_Scoreform_descitemlist_question112=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_descitemlist_question113q1=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question113Q1.
	 */
	public String getScoreform_descitemlist_question113q1(){
		try{
			if (_Scoreform_descitemlist_question113q1==null){
				_Scoreform_descitemlist_question113q1=getStringProperty("ScoreForm/descItemList/question113Q1");
				return _Scoreform_descitemlist_question113q1;
			}else {
				return _Scoreform_descitemlist_question113q1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question113Q1.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question113q1(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question113Q1",v);
		_Scoreform_descitemlist_question113q1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question113a1=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question113A1.
	 */
	public Integer getScoreform_descitemlist_question113a1() {
		try{
			if (_Scoreform_descitemlist_question113a1==null){
				_Scoreform_descitemlist_question113a1=getIntegerProperty("ScoreForm/descItemList/question113A1");
				return _Scoreform_descitemlist_question113a1;
			}else {
				return _Scoreform_descitemlist_question113a1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question113A1.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question113a1(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question113A1",v);
		_Scoreform_descitemlist_question113a1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_descitemlist_question113q2=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question113Q2.
	 */
	public String getScoreform_descitemlist_question113q2(){
		try{
			if (_Scoreform_descitemlist_question113q2==null){
				_Scoreform_descitemlist_question113q2=getStringProperty("ScoreForm/descItemList/question113Q2");
				return _Scoreform_descitemlist_question113q2;
			}else {
				return _Scoreform_descitemlist_question113q2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question113Q2.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question113q2(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question113Q2",v);
		_Scoreform_descitemlist_question113q2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question113a2=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question113A2.
	 */
	public Integer getScoreform_descitemlist_question113a2() {
		try{
			if (_Scoreform_descitemlist_question113a2==null){
				_Scoreform_descitemlist_question113a2=getIntegerProperty("ScoreForm/descItemList/question113A2");
				return _Scoreform_descitemlist_question113a2;
			}else {
				return _Scoreform_descitemlist_question113a2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question113A2.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question113a2(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question113A2",v);
		_Scoreform_descitemlist_question113a2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_descitemlist_question113q3=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question113Q3.
	 */
	public String getScoreform_descitemlist_question113q3(){
		try{
			if (_Scoreform_descitemlist_question113q3==null){
				_Scoreform_descitemlist_question113q3=getStringProperty("ScoreForm/descItemList/question113Q3");
				return _Scoreform_descitemlist_question113q3;
			}else {
				return _Scoreform_descitemlist_question113q3;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question113Q3.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question113q3(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question113Q3",v);
		_Scoreform_descitemlist_question113q3=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_descitemlist_question113a3=null;

	/**
	 * @return Returns the ScoreForm/descItemList/question113A3.
	 */
	public Integer getScoreform_descitemlist_question113a3() {
		try{
			if (_Scoreform_descitemlist_question113a3==null){
				_Scoreform_descitemlist_question113a3=getIntegerProperty("ScoreForm/descItemList/question113A3");
				return _Scoreform_descitemlist_question113a3;
			}else {
				return _Scoreform_descitemlist_question113a3;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/descItemList/question113A3.
	 * @param v Value to Set.
	 */
	public void setScoreform_descitemlist_question113a3(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/descItemList/question113A3",v);
		_Scoreform_descitemlist_question113a3=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.BclCbcl6101ed201data> getAllBclCbcl6101ed201datas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.BclCbcl6101ed201data> al = new ArrayList<org.nrg.xdat.om.BclCbcl6101ed201data>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.BclCbcl6101ed201data> getBclCbcl6101ed201datasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.BclCbcl6101ed201data> al = new ArrayList<org.nrg.xdat.om.BclCbcl6101ed201data>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.BclCbcl6101ed201data> getBclCbcl6101ed201datasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.BclCbcl6101ed201data> al = new ArrayList<org.nrg.xdat.om.BclCbcl6101ed201data>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static BclCbcl6101ed201data getBclCbcl6101ed201datasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("bcl:cbcl6-1-01Ed201Data/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (BclCbcl6101ed201data) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
