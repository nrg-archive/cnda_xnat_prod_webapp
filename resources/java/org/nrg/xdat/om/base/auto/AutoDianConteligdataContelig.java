/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:00 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianConteligdataContelig extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.DianConteligdataConteligI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianConteligdataContelig.class);
	public static String SCHEMA_ELEMENT_NAME="dian:conteligData_contelig";

	public AutoDianConteligdataContelig(ItemI item)
	{
		super(item);
	}

	public AutoDianConteligdataContelig(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianConteligdataContelig(UserI user)
	 **/
	public AutoDianConteligdataContelig(){}

	public AutoDianConteligdataContelig(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:conteligData_contelig";
	}

	//FIELD

	private Integer _Recno=null;

	/**
	 * @return Returns the RECNO.
	 */
	public Integer getRecno() {
		try{
			if (_Recno==null){
				_Recno=getIntegerProperty("RECNO");
				return _Recno;
			}else {
				return _Recno;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RECNO.
	 * @param v Value to Set.
	 */
	public void setRecno(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RECNO",v);
		_Recno=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Eligmed=null;

	/**
	 * @return Returns the ELIGMED.
	 */
	public Integer getEligmed() {
		try{
			if (_Eligmed==null){
				_Eligmed=getIntegerProperty("ELIGMED");
				return _Eligmed;
			}else {
				return _Eligmed;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ELIGMED.
	 * @param v Value to Set.
	 */
	public void setEligmed(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ELIGMED",v);
		_Eligmed=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Elignurs=null;

	/**
	 * @return Returns the ELIGNURS.
	 */
	public Integer getElignurs() {
		try{
			if (_Elignurs==null){
				_Elignurs=getIntegerProperty("ELIGNURS");
				return _Elignurs;
			}else {
				return _Elignurs;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ELIGNURS.
	 * @param v Value to Set.
	 */
	public void setElignurs(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ELIGNURS",v);
		_Elignurs=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Eligmri=null;

	/**
	 * @return Returns the ELIGMRI.
	 */
	public Integer getEligmri() {
		try{
			if (_Eligmri==null){
				_Eligmri=getIntegerProperty("ELIGMRI");
				return _Eligmri;
			}else {
				return _Eligmri;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ELIGMRI.
	 * @param v Value to Set.
	 */
	public void setEligmri(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ELIGMRI",v);
		_Eligmri=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Eligpet=null;

	/**
	 * @return Returns the ELIGPET.
	 */
	public Integer getEligpet() {
		try{
			if (_Eligpet==null){
				_Eligpet=getIntegerProperty("ELIGPET");
				return _Eligpet;
			}else {
				return _Eligpet;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ELIGPET.
	 * @param v Value to Set.
	 */
	public void setEligpet(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ELIGPET",v);
		_Eligpet=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Eligpet2=null;

	/**
	 * @return Returns the ELIGPET2.
	 */
	public Integer getEligpet2() {
		try{
			if (_Eligpet2==null){
				_Eligpet2=getIntegerProperty("ELIGPET2");
				return _Eligpet2;
			}else {
				return _Eligpet2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ELIGPET2.
	 * @param v Value to Set.
	 */
	public void setEligpet2(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ELIGPET2",v);
		_Eligpet2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Eliglp=null;

	/**
	 * @return Returns the ELIGLP.
	 */
	public Integer getEliglp() {
		try{
			if (_Eliglp==null){
				_Eliglp=getIntegerProperty("ELIGLP");
				return _Eliglp;
			}else {
				return _Eliglp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ELIGLP.
	 * @param v Value to Set.
	 */
	public void setEliglp(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ELIGLP",v);
		_Eliglp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Eligmut=null;

	/**
	 * @return Returns the ELIGMUT.
	 */
	public Integer getEligmut() {
		try{
			if (_Eligmut==null){
				_Eligmut=getIntegerProperty("ELIGMUT");
				return _Eligmut;
			}else {
				return _Eligmut;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ELIGMUT.
	 * @param v Value to Set.
	 */
	public void setEligmut(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ELIGMUT",v);
		_Eligmut=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Eligneur=null;

	/**
	 * @return Returns the ELIGNEUR.
	 */
	public Integer getEligneur() {
		try{
			if (_Eligneur==null){
				_Eligneur=getIntegerProperty("ELIGNEUR");
				return _Eligneur;
			}else {
				return _Eligneur;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ELIGNEUR.
	 * @param v Value to Set.
	 */
	public void setEligneur(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ELIGNEUR",v);
		_Eligneur=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ptagree=null;

	/**
	 * @return Returns the PTAGREE.
	 */
	public Integer getPtagree() {
		try{
			if (_Ptagree==null){
				_Ptagree=getIntegerProperty("PTAGREE");
				return _Ptagree;
			}else {
				return _Ptagree;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PTAGREE.
	 * @param v Value to Set.
	 */
	public void setPtagree(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PTAGREE",v);
		_Ptagree=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _DianConteligdataConteligId=null;

	/**
	 * @return Returns the dian_conteligData_contelig_id.
	 */
	public Integer getDianConteligdataConteligId() {
		try{
			if (_DianConteligdataConteligId==null){
				_DianConteligdataConteligId=getIntegerProperty("dian_conteligData_contelig_id");
				return _DianConteligdataConteligId;
			}else {
				return _DianConteligdataConteligId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for dian_conteligData_contelig_id.
	 * @param v Value to Set.
	 */
	public void setDianConteligdataConteligId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/dian_conteligData_contelig_id",v);
		_DianConteligdataConteligId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianConteligdataContelig> getAllDianConteligdataConteligs(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianConteligdataContelig> al = new ArrayList<org.nrg.xdat.om.DianConteligdataContelig>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianConteligdataContelig> getDianConteligdataConteligsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianConteligdataContelig> al = new ArrayList<org.nrg.xdat.om.DianConteligdataContelig>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianConteligdataContelig> getDianConteligdataConteligsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianConteligdataContelig> al = new ArrayList<org.nrg.xdat.om.DianConteligdataContelig>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianConteligdataContelig getDianConteligdataConteligsByDianConteligdataConteligId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:conteligData_contelig/dian_conteligData_contelig_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianConteligdataContelig) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
