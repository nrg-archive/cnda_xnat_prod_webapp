/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoSrsSrsver2data extends XnatSubjectassessordata implements org.nrg.xdat.model.SrsSrsver2dataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoSrsSrsver2data.class);
	public static String SCHEMA_ELEMENT_NAME="srs:srsVer2Data";

	public AutoSrsSrsver2data(ItemI item)
	{
		super(item);
	}

	public AutoSrsSrsver2data(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoSrsSrsver2data(UserI user)
	 **/
	public AutoSrsSrsver2data(){}

	public AutoSrsSrsver2data(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "srs:srsVer2Data";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _TotalRaw=null;

	/**
	 * @return Returns the total_raw.
	 */
	public Integer getTotalRaw() {
		try{
			if (_TotalRaw==null){
				_TotalRaw=getIntegerProperty("total_raw");
				return _TotalRaw;
			}else {
				return _TotalRaw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for total_raw.
	 * @param v Value to Set.
	 */
	public void setTotalRaw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/total_raw",v);
		_TotalRaw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _TotalT=null;

	/**
	 * @return Returns the total_t.
	 */
	public Integer getTotalT() {
		try{
			if (_TotalT==null){
				_TotalT=getIntegerProperty("total_t");
				return _TotalT;
			}else {
				return _TotalT;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for total_t.
	 * @param v Value to Set.
	 */
	public void setTotalT(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/total_t",v);
		_TotalT=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _TotalNote=null;

	/**
	 * @return Returns the total_note.
	 */
	public String getTotalNote(){
		try{
			if (_TotalNote==null){
				_TotalNote=getStringProperty("total_note");
				return _TotalNote;
			}else {
				return _TotalNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for total_note.
	 * @param v Value to Set.
	 */
	public void setTotalNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/total_note",v);
		_TotalNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Testtype=null;

	/**
	 * @return Returns the testType.
	 */
	public String getTesttype(){
		try{
			if (_Testtype==null){
				_Testtype=getStringProperty("testType");
				return _Testtype;
			}else {
				return _Testtype;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for testType.
	 * @param v Value to Set.
	 */
	public void setTesttype(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/testType",v);
		_Testtype=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_filledoutbyrelat=null;

	/**
	 * @return Returns the ScoreForm/filledOutByRelat.
	 */
	public String getScoreform_filledoutbyrelat(){
		try{
			if (_Scoreform_filledoutbyrelat==null){
				_Scoreform_filledoutbyrelat=getStringProperty("ScoreForm/filledOutByRelat");
				return _Scoreform_filledoutbyrelat;
			}else {
				return _Scoreform_filledoutbyrelat;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/filledOutByRelat.
	 * @param v Value to Set.
	 */
	public void setScoreform_filledoutbyrelat(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/filledOutByRelat",v);
		_Scoreform_filledoutbyrelat=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_question1=null;

	/**
	 * @return Returns the ScoreForm/sectionA/question1.
	 */
	public Integer getScoreform_sectiona_question1() {
		try{
			if (_Scoreform_sectiona_question1==null){
				_Scoreform_sectiona_question1=getIntegerProperty("ScoreForm/sectionA/question1");
				return _Scoreform_sectiona_question1;
			}else {
				return _Scoreform_sectiona_question1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/question1.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_question1(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/question1",v);
		_Scoreform_sectiona_question1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_question2=null;

	/**
	 * @return Returns the ScoreForm/sectionA/question2.
	 */
	public Integer getScoreform_sectiona_question2() {
		try{
			if (_Scoreform_sectiona_question2==null){
				_Scoreform_sectiona_question2=getIntegerProperty("ScoreForm/sectionA/question2");
				return _Scoreform_sectiona_question2;
			}else {
				return _Scoreform_sectiona_question2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/question2.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_question2(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/question2",v);
		_Scoreform_sectiona_question2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_question3=null;

	/**
	 * @return Returns the ScoreForm/sectionA/question3.
	 */
	public Integer getScoreform_sectiona_question3() {
		try{
			if (_Scoreform_sectiona_question3==null){
				_Scoreform_sectiona_question3=getIntegerProperty("ScoreForm/sectionA/question3");
				return _Scoreform_sectiona_question3;
			}else {
				return _Scoreform_sectiona_question3;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/question3.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_question3(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/question3",v);
		_Scoreform_sectiona_question3=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_question4=null;

	/**
	 * @return Returns the ScoreForm/sectionA/question4.
	 */
	public Integer getScoreform_sectiona_question4() {
		try{
			if (_Scoreform_sectiona_question4==null){
				_Scoreform_sectiona_question4=getIntegerProperty("ScoreForm/sectionA/question4");
				return _Scoreform_sectiona_question4;
			}else {
				return _Scoreform_sectiona_question4;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/question4.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_question4(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/question4",v);
		_Scoreform_sectiona_question4=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_question5=null;

	/**
	 * @return Returns the ScoreForm/sectionA/question5.
	 */
	public Integer getScoreform_sectiona_question5() {
		try{
			if (_Scoreform_sectiona_question5==null){
				_Scoreform_sectiona_question5=getIntegerProperty("ScoreForm/sectionA/question5");
				return _Scoreform_sectiona_question5;
			}else {
				return _Scoreform_sectiona_question5;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/question5.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_question5(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/question5",v);
		_Scoreform_sectiona_question5=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_question6=null;

	/**
	 * @return Returns the ScoreForm/sectionA/question6.
	 */
	public Integer getScoreform_sectiona_question6() {
		try{
			if (_Scoreform_sectiona_question6==null){
				_Scoreform_sectiona_question6=getIntegerProperty("ScoreForm/sectionA/question6");
				return _Scoreform_sectiona_question6;
			}else {
				return _Scoreform_sectiona_question6;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/question6.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_question6(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/question6",v);
		_Scoreform_sectiona_question6=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_question7=null;

	/**
	 * @return Returns the ScoreForm/sectionA/question7.
	 */
	public Integer getScoreform_sectiona_question7() {
		try{
			if (_Scoreform_sectiona_question7==null){
				_Scoreform_sectiona_question7=getIntegerProperty("ScoreForm/sectionA/question7");
				return _Scoreform_sectiona_question7;
			}else {
				return _Scoreform_sectiona_question7;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/question7.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_question7(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/question7",v);
		_Scoreform_sectiona_question7=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_question8=null;

	/**
	 * @return Returns the ScoreForm/sectionA/question8.
	 */
	public Integer getScoreform_sectiona_question8() {
		try{
			if (_Scoreform_sectiona_question8==null){
				_Scoreform_sectiona_question8=getIntegerProperty("ScoreForm/sectionA/question8");
				return _Scoreform_sectiona_question8;
			}else {
				return _Scoreform_sectiona_question8;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/question8.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_question8(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/question8",v);
		_Scoreform_sectiona_question8=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_question9=null;

	/**
	 * @return Returns the ScoreForm/sectionA/question9.
	 */
	public Integer getScoreform_sectiona_question9() {
		try{
			if (_Scoreform_sectiona_question9==null){
				_Scoreform_sectiona_question9=getIntegerProperty("ScoreForm/sectionA/question9");
				return _Scoreform_sectiona_question9;
			}else {
				return _Scoreform_sectiona_question9;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/question9.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_question9(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/question9",v);
		_Scoreform_sectiona_question9=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_question10=null;

	/**
	 * @return Returns the ScoreForm/sectionA/question10.
	 */
	public Integer getScoreform_sectiona_question10() {
		try{
			if (_Scoreform_sectiona_question10==null){
				_Scoreform_sectiona_question10=getIntegerProperty("ScoreForm/sectionA/question10");
				return _Scoreform_sectiona_question10;
			}else {
				return _Scoreform_sectiona_question10;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/question10.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_question10(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/question10",v);
		_Scoreform_sectiona_question10=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_question11=null;

	/**
	 * @return Returns the ScoreForm/sectionA/question11.
	 */
	public Integer getScoreform_sectiona_question11() {
		try{
			if (_Scoreform_sectiona_question11==null){
				_Scoreform_sectiona_question11=getIntegerProperty("ScoreForm/sectionA/question11");
				return _Scoreform_sectiona_question11;
			}else {
				return _Scoreform_sectiona_question11;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/question11.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_question11(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/question11",v);
		_Scoreform_sectiona_question11=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_question12=null;

	/**
	 * @return Returns the ScoreForm/sectionA/question12.
	 */
	public Integer getScoreform_sectiona_question12() {
		try{
			if (_Scoreform_sectiona_question12==null){
				_Scoreform_sectiona_question12=getIntegerProperty("ScoreForm/sectionA/question12");
				return _Scoreform_sectiona_question12;
			}else {
				return _Scoreform_sectiona_question12;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/question12.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_question12(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/question12",v);
		_Scoreform_sectiona_question12=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_question13=null;

	/**
	 * @return Returns the ScoreForm/sectionB/question13.
	 */
	public Integer getScoreform_sectionb_question13() {
		try{
			if (_Scoreform_sectionb_question13==null){
				_Scoreform_sectionb_question13=getIntegerProperty("ScoreForm/sectionB/question13");
				return _Scoreform_sectionb_question13;
			}else {
				return _Scoreform_sectionb_question13;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/question13.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_question13(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/question13",v);
		_Scoreform_sectionb_question13=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_question14=null;

	/**
	 * @return Returns the ScoreForm/sectionB/question14.
	 */
	public Integer getScoreform_sectionb_question14() {
		try{
			if (_Scoreform_sectionb_question14==null){
				_Scoreform_sectionb_question14=getIntegerProperty("ScoreForm/sectionB/question14");
				return _Scoreform_sectionb_question14;
			}else {
				return _Scoreform_sectionb_question14;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/question14.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_question14(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/question14",v);
		_Scoreform_sectionb_question14=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_question15=null;

	/**
	 * @return Returns the ScoreForm/sectionB/question15.
	 */
	public Integer getScoreform_sectionb_question15() {
		try{
			if (_Scoreform_sectionb_question15==null){
				_Scoreform_sectionb_question15=getIntegerProperty("ScoreForm/sectionB/question15");
				return _Scoreform_sectionb_question15;
			}else {
				return _Scoreform_sectionb_question15;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/question15.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_question15(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/question15",v);
		_Scoreform_sectionb_question15=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_question16=null;

	/**
	 * @return Returns the ScoreForm/sectionB/question16.
	 */
	public Integer getScoreform_sectionb_question16() {
		try{
			if (_Scoreform_sectionb_question16==null){
				_Scoreform_sectionb_question16=getIntegerProperty("ScoreForm/sectionB/question16");
				return _Scoreform_sectionb_question16;
			}else {
				return _Scoreform_sectionb_question16;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/question16.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_question16(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/question16",v);
		_Scoreform_sectionb_question16=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_question17=null;

	/**
	 * @return Returns the ScoreForm/sectionB/question17.
	 */
	public Integer getScoreform_sectionb_question17() {
		try{
			if (_Scoreform_sectionb_question17==null){
				_Scoreform_sectionb_question17=getIntegerProperty("ScoreForm/sectionB/question17");
				return _Scoreform_sectionb_question17;
			}else {
				return _Scoreform_sectionb_question17;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/question17.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_question17(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/question17",v);
		_Scoreform_sectionb_question17=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_question18=null;

	/**
	 * @return Returns the ScoreForm/sectionB/question18.
	 */
	public Integer getScoreform_sectionb_question18() {
		try{
			if (_Scoreform_sectionb_question18==null){
				_Scoreform_sectionb_question18=getIntegerProperty("ScoreForm/sectionB/question18");
				return _Scoreform_sectionb_question18;
			}else {
				return _Scoreform_sectionb_question18;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/question18.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_question18(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/question18",v);
		_Scoreform_sectionb_question18=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_question19=null;

	/**
	 * @return Returns the ScoreForm/sectionB/question19.
	 */
	public Integer getScoreform_sectionb_question19() {
		try{
			if (_Scoreform_sectionb_question19==null){
				_Scoreform_sectionb_question19=getIntegerProperty("ScoreForm/sectionB/question19");
				return _Scoreform_sectionb_question19;
			}else {
				return _Scoreform_sectionb_question19;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/question19.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_question19(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/question19",v);
		_Scoreform_sectionb_question19=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_question20=null;

	/**
	 * @return Returns the ScoreForm/sectionB/question20.
	 */
	public Integer getScoreform_sectionb_question20() {
		try{
			if (_Scoreform_sectionb_question20==null){
				_Scoreform_sectionb_question20=getIntegerProperty("ScoreForm/sectionB/question20");
				return _Scoreform_sectionb_question20;
			}else {
				return _Scoreform_sectionb_question20;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/question20.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_question20(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/question20",v);
		_Scoreform_sectionb_question20=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc_question21=null;

	/**
	 * @return Returns the ScoreForm/sectionC/question21.
	 */
	public Integer getScoreform_sectionc_question21() {
		try{
			if (_Scoreform_sectionc_question21==null){
				_Scoreform_sectionc_question21=getIntegerProperty("ScoreForm/sectionC/question21");
				return _Scoreform_sectionc_question21;
			}else {
				return _Scoreform_sectionc_question21;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC/question21.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc_question21(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC/question21",v);
		_Scoreform_sectionc_question21=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc_question22=null;

	/**
	 * @return Returns the ScoreForm/sectionC/question22.
	 */
	public Integer getScoreform_sectionc_question22() {
		try{
			if (_Scoreform_sectionc_question22==null){
				_Scoreform_sectionc_question22=getIntegerProperty("ScoreForm/sectionC/question22");
				return _Scoreform_sectionc_question22;
			}else {
				return _Scoreform_sectionc_question22;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC/question22.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc_question22(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC/question22",v);
		_Scoreform_sectionc_question22=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc_question23=null;

	/**
	 * @return Returns the ScoreForm/sectionC/question23.
	 */
	public Integer getScoreform_sectionc_question23() {
		try{
			if (_Scoreform_sectionc_question23==null){
				_Scoreform_sectionc_question23=getIntegerProperty("ScoreForm/sectionC/question23");
				return _Scoreform_sectionc_question23;
			}else {
				return _Scoreform_sectionc_question23;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC/question23.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc_question23(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC/question23",v);
		_Scoreform_sectionc_question23=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc_question24=null;

	/**
	 * @return Returns the ScoreForm/sectionC/question24.
	 */
	public Integer getScoreform_sectionc_question24() {
		try{
			if (_Scoreform_sectionc_question24==null){
				_Scoreform_sectionc_question24=getIntegerProperty("ScoreForm/sectionC/question24");
				return _Scoreform_sectionc_question24;
			}else {
				return _Scoreform_sectionc_question24;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC/question24.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc_question24(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC/question24",v);
		_Scoreform_sectionc_question24=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc_question25=null;

	/**
	 * @return Returns the ScoreForm/sectionC/question25.
	 */
	public Integer getScoreform_sectionc_question25() {
		try{
			if (_Scoreform_sectionc_question25==null){
				_Scoreform_sectionc_question25=getIntegerProperty("ScoreForm/sectionC/question25");
				return _Scoreform_sectionc_question25;
			}else {
				return _Scoreform_sectionc_question25;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC/question25.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc_question25(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC/question25",v);
		_Scoreform_sectionc_question25=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc_question26=null;

	/**
	 * @return Returns the ScoreForm/sectionC/question26.
	 */
	public Integer getScoreform_sectionc_question26() {
		try{
			if (_Scoreform_sectionc_question26==null){
				_Scoreform_sectionc_question26=getIntegerProperty("ScoreForm/sectionC/question26");
				return _Scoreform_sectionc_question26;
			}else {
				return _Scoreform_sectionc_question26;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC/question26.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc_question26(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC/question26",v);
		_Scoreform_sectionc_question26=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc_question27=null;

	/**
	 * @return Returns the ScoreForm/sectionC/question27.
	 */
	public Integer getScoreform_sectionc_question27() {
		try{
			if (_Scoreform_sectionc_question27==null){
				_Scoreform_sectionc_question27=getIntegerProperty("ScoreForm/sectionC/question27");
				return _Scoreform_sectionc_question27;
			}else {
				return _Scoreform_sectionc_question27;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC/question27.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc_question27(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC/question27",v);
		_Scoreform_sectionc_question27=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc_question28=null;

	/**
	 * @return Returns the ScoreForm/sectionC/question28.
	 */
	public Integer getScoreform_sectionc_question28() {
		try{
			if (_Scoreform_sectionc_question28==null){
				_Scoreform_sectionc_question28=getIntegerProperty("ScoreForm/sectionC/question28");
				return _Scoreform_sectionc_question28;
			}else {
				return _Scoreform_sectionc_question28;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC/question28.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc_question28(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC/question28",v);
		_Scoreform_sectionc_question28=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc_question29=null;

	/**
	 * @return Returns the ScoreForm/sectionC/question29.
	 */
	public Integer getScoreform_sectionc_question29() {
		try{
			if (_Scoreform_sectionc_question29==null){
				_Scoreform_sectionc_question29=getIntegerProperty("ScoreForm/sectionC/question29");
				return _Scoreform_sectionc_question29;
			}else {
				return _Scoreform_sectionc_question29;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC/question29.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc_question29(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC/question29",v);
		_Scoreform_sectionc_question29=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc_question30=null;

	/**
	 * @return Returns the ScoreForm/sectionC/question30.
	 */
	public Integer getScoreform_sectionc_question30() {
		try{
			if (_Scoreform_sectionc_question30==null){
				_Scoreform_sectionc_question30=getIntegerProperty("ScoreForm/sectionC/question30");
				return _Scoreform_sectionc_question30;
			}else {
				return _Scoreform_sectionc_question30;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC/question30.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc_question30(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC/question30",v);
		_Scoreform_sectionc_question30=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_question31=null;

	/**
	 * @return Returns the ScoreForm/sectionD/question31.
	 */
	public Integer getScoreform_sectiond_question31() {
		try{
			if (_Scoreform_sectiond_question31==null){
				_Scoreform_sectiond_question31=getIntegerProperty("ScoreForm/sectionD/question31");
				return _Scoreform_sectiond_question31;
			}else {
				return _Scoreform_sectiond_question31;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/question31.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_question31(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/question31",v);
		_Scoreform_sectiond_question31=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_question32=null;

	/**
	 * @return Returns the ScoreForm/sectionD/question32.
	 */
	public Integer getScoreform_sectiond_question32() {
		try{
			if (_Scoreform_sectiond_question32==null){
				_Scoreform_sectiond_question32=getIntegerProperty("ScoreForm/sectionD/question32");
				return _Scoreform_sectiond_question32;
			}else {
				return _Scoreform_sectiond_question32;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/question32.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_question32(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/question32",v);
		_Scoreform_sectiond_question32=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_question33=null;

	/**
	 * @return Returns the ScoreForm/sectionD/question33.
	 */
	public Integer getScoreform_sectiond_question33() {
		try{
			if (_Scoreform_sectiond_question33==null){
				_Scoreform_sectiond_question33=getIntegerProperty("ScoreForm/sectionD/question33");
				return _Scoreform_sectiond_question33;
			}else {
				return _Scoreform_sectiond_question33;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/question33.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_question33(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/question33",v);
		_Scoreform_sectiond_question33=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_question34=null;

	/**
	 * @return Returns the ScoreForm/sectionD/question34.
	 */
	public Integer getScoreform_sectiond_question34() {
		try{
			if (_Scoreform_sectiond_question34==null){
				_Scoreform_sectiond_question34=getIntegerProperty("ScoreForm/sectionD/question34");
				return _Scoreform_sectiond_question34;
			}else {
				return _Scoreform_sectiond_question34;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/question34.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_question34(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/question34",v);
		_Scoreform_sectiond_question34=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_question35=null;

	/**
	 * @return Returns the ScoreForm/sectionD/question35.
	 */
	public Integer getScoreform_sectiond_question35() {
		try{
			if (_Scoreform_sectiond_question35==null){
				_Scoreform_sectiond_question35=getIntegerProperty("ScoreForm/sectionD/question35");
				return _Scoreform_sectiond_question35;
			}else {
				return _Scoreform_sectiond_question35;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/question35.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_question35(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/question35",v);
		_Scoreform_sectiond_question35=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_question36=null;

	/**
	 * @return Returns the ScoreForm/sectionD/question36.
	 */
	public Integer getScoreform_sectiond_question36() {
		try{
			if (_Scoreform_sectiond_question36==null){
				_Scoreform_sectiond_question36=getIntegerProperty("ScoreForm/sectionD/question36");
				return _Scoreform_sectiond_question36;
			}else {
				return _Scoreform_sectiond_question36;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/question36.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_question36(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/question36",v);
		_Scoreform_sectiond_question36=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_question37=null;

	/**
	 * @return Returns the ScoreForm/sectionD/question37.
	 */
	public Integer getScoreform_sectiond_question37() {
		try{
			if (_Scoreform_sectiond_question37==null){
				_Scoreform_sectiond_question37=getIntegerProperty("ScoreForm/sectionD/question37");
				return _Scoreform_sectiond_question37;
			}else {
				return _Scoreform_sectiond_question37;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/question37.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_question37(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/question37",v);
		_Scoreform_sectiond_question37=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_question38=null;

	/**
	 * @return Returns the ScoreForm/sectionD/question38.
	 */
	public Integer getScoreform_sectiond_question38() {
		try{
			if (_Scoreform_sectiond_question38==null){
				_Scoreform_sectiond_question38=getIntegerProperty("ScoreForm/sectionD/question38");
				return _Scoreform_sectiond_question38;
			}else {
				return _Scoreform_sectiond_question38;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/question38.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_question38(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/question38",v);
		_Scoreform_sectiond_question38=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_question39=null;

	/**
	 * @return Returns the ScoreForm/sectionD/question39.
	 */
	public Integer getScoreform_sectiond_question39() {
		try{
			if (_Scoreform_sectiond_question39==null){
				_Scoreform_sectiond_question39=getIntegerProperty("ScoreForm/sectionD/question39");
				return _Scoreform_sectiond_question39;
			}else {
				return _Scoreform_sectiond_question39;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/question39.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_question39(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/question39",v);
		_Scoreform_sectiond_question39=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectione_question40=null;

	/**
	 * @return Returns the ScoreForm/sectionE/question40.
	 */
	public Integer getScoreform_sectione_question40() {
		try{
			if (_Scoreform_sectione_question40==null){
				_Scoreform_sectione_question40=getIntegerProperty("ScoreForm/sectionE/question40");
				return _Scoreform_sectione_question40;
			}else {
				return _Scoreform_sectione_question40;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionE/question40.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectione_question40(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionE/question40",v);
		_Scoreform_sectione_question40=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectione_question41=null;

	/**
	 * @return Returns the ScoreForm/sectionE/question41.
	 */
	public Integer getScoreform_sectione_question41() {
		try{
			if (_Scoreform_sectione_question41==null){
				_Scoreform_sectione_question41=getIntegerProperty("ScoreForm/sectionE/question41");
				return _Scoreform_sectione_question41;
			}else {
				return _Scoreform_sectione_question41;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionE/question41.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectione_question41(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionE/question41",v);
		_Scoreform_sectione_question41=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectione_question42=null;

	/**
	 * @return Returns the ScoreForm/sectionE/question42.
	 */
	public Integer getScoreform_sectione_question42() {
		try{
			if (_Scoreform_sectione_question42==null){
				_Scoreform_sectione_question42=getIntegerProperty("ScoreForm/sectionE/question42");
				return _Scoreform_sectione_question42;
			}else {
				return _Scoreform_sectione_question42;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionE/question42.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectione_question42(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionE/question42",v);
		_Scoreform_sectione_question42=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectione_question43=null;

	/**
	 * @return Returns the ScoreForm/sectionE/question43.
	 */
	public Integer getScoreform_sectione_question43() {
		try{
			if (_Scoreform_sectione_question43==null){
				_Scoreform_sectione_question43=getIntegerProperty("ScoreForm/sectionE/question43");
				return _Scoreform_sectione_question43;
			}else {
				return _Scoreform_sectione_question43;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionE/question43.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectione_question43(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionE/question43",v);
		_Scoreform_sectione_question43=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectione_question44=null;

	/**
	 * @return Returns the ScoreForm/sectionE/question44.
	 */
	public Integer getScoreform_sectione_question44() {
		try{
			if (_Scoreform_sectione_question44==null){
				_Scoreform_sectione_question44=getIntegerProperty("ScoreForm/sectionE/question44");
				return _Scoreform_sectione_question44;
			}else {
				return _Scoreform_sectione_question44;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionE/question44.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectione_question44(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionE/question44",v);
		_Scoreform_sectione_question44=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectione_question45=null;

	/**
	 * @return Returns the ScoreForm/sectionE/question45.
	 */
	public Integer getScoreform_sectione_question45() {
		try{
			if (_Scoreform_sectione_question45==null){
				_Scoreform_sectione_question45=getIntegerProperty("ScoreForm/sectionE/question45");
				return _Scoreform_sectione_question45;
			}else {
				return _Scoreform_sectione_question45;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionE/question45.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectione_question45(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionE/question45",v);
		_Scoreform_sectione_question45=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectione_question46=null;

	/**
	 * @return Returns the ScoreForm/sectionE/question46.
	 */
	public Integer getScoreform_sectione_question46() {
		try{
			if (_Scoreform_sectione_question46==null){
				_Scoreform_sectione_question46=getIntegerProperty("ScoreForm/sectionE/question46");
				return _Scoreform_sectione_question46;
			}else {
				return _Scoreform_sectione_question46;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionE/question46.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectione_question46(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionE/question46",v);
		_Scoreform_sectione_question46=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectione_question47=null;

	/**
	 * @return Returns the ScoreForm/sectionE/question47.
	 */
	public Integer getScoreform_sectione_question47() {
		try{
			if (_Scoreform_sectione_question47==null){
				_Scoreform_sectione_question47=getIntegerProperty("ScoreForm/sectionE/question47");
				return _Scoreform_sectione_question47;
			}else {
				return _Scoreform_sectione_question47;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionE/question47.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectione_question47(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionE/question47",v);
		_Scoreform_sectione_question47=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectione_question48=null;

	/**
	 * @return Returns the ScoreForm/sectionE/question48.
	 */
	public Integer getScoreform_sectione_question48() {
		try{
			if (_Scoreform_sectione_question48==null){
				_Scoreform_sectione_question48=getIntegerProperty("ScoreForm/sectionE/question48");
				return _Scoreform_sectione_question48;
			}else {
				return _Scoreform_sectione_question48;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionE/question48.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectione_question48(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionE/question48",v);
		_Scoreform_sectione_question48=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectione_question49=null;

	/**
	 * @return Returns the ScoreForm/sectionE/question49.
	 */
	public Integer getScoreform_sectione_question49() {
		try{
			if (_Scoreform_sectione_question49==null){
				_Scoreform_sectione_question49=getIntegerProperty("ScoreForm/sectionE/question49");
				return _Scoreform_sectione_question49;
			}else {
				return _Scoreform_sectione_question49;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionE/question49.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectione_question49(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionE/question49",v);
		_Scoreform_sectione_question49=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionf_question50=null;

	/**
	 * @return Returns the ScoreForm/sectionF/question50.
	 */
	public Integer getScoreform_sectionf_question50() {
		try{
			if (_Scoreform_sectionf_question50==null){
				_Scoreform_sectionf_question50=getIntegerProperty("ScoreForm/sectionF/question50");
				return _Scoreform_sectionf_question50;
			}else {
				return _Scoreform_sectionf_question50;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionF/question50.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionf_question50(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionF/question50",v);
		_Scoreform_sectionf_question50=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionf_question51=null;

	/**
	 * @return Returns the ScoreForm/sectionF/question51.
	 */
	public Integer getScoreform_sectionf_question51() {
		try{
			if (_Scoreform_sectionf_question51==null){
				_Scoreform_sectionf_question51=getIntegerProperty("ScoreForm/sectionF/question51");
				return _Scoreform_sectionf_question51;
			}else {
				return _Scoreform_sectionf_question51;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionF/question51.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionf_question51(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionF/question51",v);
		_Scoreform_sectionf_question51=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionf_question52=null;

	/**
	 * @return Returns the ScoreForm/sectionF/question52.
	 */
	public Integer getScoreform_sectionf_question52() {
		try{
			if (_Scoreform_sectionf_question52==null){
				_Scoreform_sectionf_question52=getIntegerProperty("ScoreForm/sectionF/question52");
				return _Scoreform_sectionf_question52;
			}else {
				return _Scoreform_sectionf_question52;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionF/question52.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionf_question52(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionF/question52",v);
		_Scoreform_sectionf_question52=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionf_question53=null;

	/**
	 * @return Returns the ScoreForm/sectionF/question53.
	 */
	public Integer getScoreform_sectionf_question53() {
		try{
			if (_Scoreform_sectionf_question53==null){
				_Scoreform_sectionf_question53=getIntegerProperty("ScoreForm/sectionF/question53");
				return _Scoreform_sectionf_question53;
			}else {
				return _Scoreform_sectionf_question53;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionF/question53.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionf_question53(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionF/question53",v);
		_Scoreform_sectionf_question53=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionf_question54=null;

	/**
	 * @return Returns the ScoreForm/sectionF/question54.
	 */
	public Integer getScoreform_sectionf_question54() {
		try{
			if (_Scoreform_sectionf_question54==null){
				_Scoreform_sectionf_question54=getIntegerProperty("ScoreForm/sectionF/question54");
				return _Scoreform_sectionf_question54;
			}else {
				return _Scoreform_sectionf_question54;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionF/question54.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionf_question54(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionF/question54",v);
		_Scoreform_sectionf_question54=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionf_question55=null;

	/**
	 * @return Returns the ScoreForm/sectionF/question55.
	 */
	public Integer getScoreform_sectionf_question55() {
		try{
			if (_Scoreform_sectionf_question55==null){
				_Scoreform_sectionf_question55=getIntegerProperty("ScoreForm/sectionF/question55");
				return _Scoreform_sectionf_question55;
			}else {
				return _Scoreform_sectionf_question55;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionF/question55.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionf_question55(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionF/question55",v);
		_Scoreform_sectionf_question55=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionf_question56=null;

	/**
	 * @return Returns the ScoreForm/sectionF/question56.
	 */
	public Integer getScoreform_sectionf_question56() {
		try{
			if (_Scoreform_sectionf_question56==null){
				_Scoreform_sectionf_question56=getIntegerProperty("ScoreForm/sectionF/question56");
				return _Scoreform_sectionf_question56;
			}else {
				return _Scoreform_sectionf_question56;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionF/question56.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionf_question56(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionF/question56",v);
		_Scoreform_sectionf_question56=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionf_question57=null;

	/**
	 * @return Returns the ScoreForm/sectionF/question57.
	 */
	public Integer getScoreform_sectionf_question57() {
		try{
			if (_Scoreform_sectionf_question57==null){
				_Scoreform_sectionf_question57=getIntegerProperty("ScoreForm/sectionF/question57");
				return _Scoreform_sectionf_question57;
			}else {
				return _Scoreform_sectionf_question57;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionF/question57.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionf_question57(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionF/question57",v);
		_Scoreform_sectionf_question57=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionf_question58=null;

	/**
	 * @return Returns the ScoreForm/sectionF/question58.
	 */
	public Integer getScoreform_sectionf_question58() {
		try{
			if (_Scoreform_sectionf_question58==null){
				_Scoreform_sectionf_question58=getIntegerProperty("ScoreForm/sectionF/question58");
				return _Scoreform_sectionf_question58;
			}else {
				return _Scoreform_sectionf_question58;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionF/question58.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionf_question58(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionF/question58",v);
		_Scoreform_sectionf_question58=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionf_question59=null;

	/**
	 * @return Returns the ScoreForm/sectionF/question59.
	 */
	public Integer getScoreform_sectionf_question59() {
		try{
			if (_Scoreform_sectionf_question59==null){
				_Scoreform_sectionf_question59=getIntegerProperty("ScoreForm/sectionF/question59");
				return _Scoreform_sectionf_question59;
			}else {
				return _Scoreform_sectionf_question59;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionF/question59.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionf_question59(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionF/question59",v);
		_Scoreform_sectionf_question59=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiong_question60=null;

	/**
	 * @return Returns the ScoreForm/sectionG/question60.
	 */
	public Integer getScoreform_sectiong_question60() {
		try{
			if (_Scoreform_sectiong_question60==null){
				_Scoreform_sectiong_question60=getIntegerProperty("ScoreForm/sectionG/question60");
				return _Scoreform_sectiong_question60;
			}else {
				return _Scoreform_sectiong_question60;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionG/question60.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiong_question60(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionG/question60",v);
		_Scoreform_sectiong_question60=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiong_question61=null;

	/**
	 * @return Returns the ScoreForm/sectionG/question61.
	 */
	public Integer getScoreform_sectiong_question61() {
		try{
			if (_Scoreform_sectiong_question61==null){
				_Scoreform_sectiong_question61=getIntegerProperty("ScoreForm/sectionG/question61");
				return _Scoreform_sectiong_question61;
			}else {
				return _Scoreform_sectiong_question61;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionG/question61.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiong_question61(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionG/question61",v);
		_Scoreform_sectiong_question61=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiong_question62=null;

	/**
	 * @return Returns the ScoreForm/sectionG/question62.
	 */
	public Integer getScoreform_sectiong_question62() {
		try{
			if (_Scoreform_sectiong_question62==null){
				_Scoreform_sectiong_question62=getIntegerProperty("ScoreForm/sectionG/question62");
				return _Scoreform_sectiong_question62;
			}else {
				return _Scoreform_sectiong_question62;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionG/question62.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiong_question62(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionG/question62",v);
		_Scoreform_sectiong_question62=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiong_question63=null;

	/**
	 * @return Returns the ScoreForm/sectionG/question63.
	 */
	public Integer getScoreform_sectiong_question63() {
		try{
			if (_Scoreform_sectiong_question63==null){
				_Scoreform_sectiong_question63=getIntegerProperty("ScoreForm/sectionG/question63");
				return _Scoreform_sectiong_question63;
			}else {
				return _Scoreform_sectiong_question63;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionG/question63.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiong_question63(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionG/question63",v);
		_Scoreform_sectiong_question63=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiong_question64=null;

	/**
	 * @return Returns the ScoreForm/sectionG/question64.
	 */
	public Integer getScoreform_sectiong_question64() {
		try{
			if (_Scoreform_sectiong_question64==null){
				_Scoreform_sectiong_question64=getIntegerProperty("ScoreForm/sectionG/question64");
				return _Scoreform_sectiong_question64;
			}else {
				return _Scoreform_sectiong_question64;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionG/question64.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiong_question64(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionG/question64",v);
		_Scoreform_sectiong_question64=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiong_question65=null;

	/**
	 * @return Returns the ScoreForm/sectionG/question65.
	 */
	public Integer getScoreform_sectiong_question65() {
		try{
			if (_Scoreform_sectiong_question65==null){
				_Scoreform_sectiong_question65=getIntegerProperty("ScoreForm/sectionG/question65");
				return _Scoreform_sectiong_question65;
			}else {
				return _Scoreform_sectiong_question65;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionG/question65.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiong_question65(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionG/question65",v);
		_Scoreform_sectiong_question65=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.SrsSrsver2data> getAllSrsSrsver2datas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SrsSrsver2data> al = new ArrayList<org.nrg.xdat.om.SrsSrsver2data>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.SrsSrsver2data> getSrsSrsver2datasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SrsSrsver2data> al = new ArrayList<org.nrg.xdat.om.SrsSrsver2data>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.SrsSrsver2data> getSrsSrsver2datasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SrsSrsver2data> al = new ArrayList<org.nrg.xdat.om.SrsSrsver2data>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static SrsSrsver2data getSrsSrsver2datasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("srs:srsVer2Data/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (SrsSrsver2data) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
