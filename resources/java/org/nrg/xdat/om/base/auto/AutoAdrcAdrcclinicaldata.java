/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoAdrcAdrcclinicaldata extends XnatSubjectassessordata implements org.nrg.xdat.model.AdrcAdrcclinicaldataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoAdrcAdrcclinicaldata.class);
	public static String SCHEMA_ELEMENT_NAME="adrc:ADRCClinicalData";

	public AutoAdrcAdrcclinicaldata(ItemI item)
	{
		super(item);
	}

	public AutoAdrcAdrcclinicaldata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoAdrcAdrcclinicaldata(UserI user)
	 **/
	public AutoAdrcAdrcclinicaldata(){}

	public AutoAdrcAdrcclinicaldata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "adrc:ADRCClinicalData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mmse=null;

	/**
	 * @return Returns the mmse.
	 */
	public Integer getMmse() {
		try{
			if (_Mmse==null){
				_Mmse=getIntegerProperty("mmse");
				return _Mmse;
			}else {
				return _Mmse;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for mmse.
	 * @param v Value to Set.
	 */
	public void setMmse(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/mmse",v);
		_Mmse=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Ageatentry=null;

	/**
	 * @return Returns the ageAtEntry.
	 */
	public Double getAgeatentry() {
		try{
			if (_Ageatentry==null){
				_Ageatentry=getDoubleProperty("ageAtEntry");
				return _Ageatentry;
			}else {
				return _Ageatentry;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ageAtEntry.
	 * @param v Value to Set.
	 */
	public void setAgeatentry(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ageAtEntry",v);
		_Ageatentry=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Cdr=null;

	/**
	 * @return Returns the cdr.
	 */
	public Double getCdr() {
		try{
			if (_Cdr==null){
				_Cdr=getDoubleProperty("cdr");
				return _Cdr;
			}else {
				return _Cdr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cdr.
	 * @param v Value to Set.
	 */
	public void setCdr(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/cdr",v);
		_Cdr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Commun=null;

	/**
	 * @return Returns the commun.
	 */
	public Double getCommun() {
		try{
			if (_Commun==null){
				_Commun=getDoubleProperty("commun");
				return _Commun;
			}else {
				return _Commun;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for commun.
	 * @param v Value to Set.
	 */
	public void setCommun(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/commun",v);
		_Commun=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Dx1=null;

	/**
	 * @return Returns the dx1.
	 */
	public String getDx1(){
		try{
			if (_Dx1==null){
				_Dx1=getStringProperty("dx1");
				return _Dx1;
			}else {
				return _Dx1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for dx1.
	 * @param v Value to Set.
	 */
	public void setDx1(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/dx1",v);
		_Dx1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Dx2=null;

	/**
	 * @return Returns the dx2.
	 */
	public String getDx2(){
		try{
			if (_Dx2==null){
				_Dx2=getStringProperty("dx2");
				return _Dx2;
			}else {
				return _Dx2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for dx2.
	 * @param v Value to Set.
	 */
	public void setDx2(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/dx2",v);
		_Dx2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Dx3=null;

	/**
	 * @return Returns the dx3.
	 */
	public String getDx3(){
		try{
			if (_Dx3==null){
				_Dx3=getStringProperty("dx3");
				return _Dx3;
			}else {
				return _Dx3;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for dx3.
	 * @param v Value to Set.
	 */
	public void setDx3(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/dx3",v);
		_Dx3=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Dx4=null;

	/**
	 * @return Returns the dx4.
	 */
	public String getDx4(){
		try{
			if (_Dx4==null){
				_Dx4=getStringProperty("dx4");
				return _Dx4;
			}else {
				return _Dx4;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for dx4.
	 * @param v Value to Set.
	 */
	public void setDx4(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/dx4",v);
		_Dx4=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Dx5=null;

	/**
	 * @return Returns the dx5.
	 */
	public String getDx5(){
		try{
			if (_Dx5==null){
				_Dx5=getStringProperty("dx5");
				return _Dx5;
			}else {
				return _Dx5;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for dx5.
	 * @param v Value to Set.
	 */
	public void setDx5(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/dx5",v);
		_Dx5=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Homehobb=null;

	/**
	 * @return Returns the homehobb.
	 */
	public Double getHomehobb() {
		try{
			if (_Homehobb==null){
				_Homehobb=getDoubleProperty("homehobb");
				return _Homehobb;
			}else {
				return _Homehobb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for homehobb.
	 * @param v Value to Set.
	 */
	public void setHomehobb(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/homehobb",v);
		_Homehobb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Judgment=null;

	/**
	 * @return Returns the judgment.
	 */
	public Double getJudgment() {
		try{
			if (_Judgment==null){
				_Judgment=getDoubleProperty("judgment");
				return _Judgment;
			}else {
				return _Judgment;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for judgment.
	 * @param v Value to Set.
	 */
	public void setJudgment(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/judgment",v);
		_Judgment=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Memory=null;

	/**
	 * @return Returns the memory.
	 */
	public Double getMemory() {
		try{
			if (_Memory==null){
				_Memory=getDoubleProperty("memory");
				return _Memory;
			}else {
				return _Memory;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for memory.
	 * @param v Value to Set.
	 */
	public void setMemory(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/memory",v);
		_Memory=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Orient=null;

	/**
	 * @return Returns the orient.
	 */
	public Double getOrient() {
		try{
			if (_Orient==null){
				_Orient=getDoubleProperty("orient");
				return _Orient;
			}else {
				return _Orient;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for orient.
	 * @param v Value to Set.
	 */
	public void setOrient(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/orient",v);
		_Orient=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Perscare=null;

	/**
	 * @return Returns the perscare.
	 */
	public Double getPerscare() {
		try{
			if (_Perscare==null){
				_Perscare=getDoubleProperty("perscare");
				return _Perscare;
			}else {
				return _Perscare;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for perscare.
	 * @param v Value to Set.
	 */
	public void setPerscare(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/perscare",v);
		_Perscare=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Apoe=null;

	/**
	 * @return Returns the apoe.
	 */
	public String getApoe(){
		try{
			if (_Apoe==null){
				_Apoe=getStringProperty("apoe");
				return _Apoe;
			}else {
				return _Apoe;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for apoe.
	 * @param v Value to Set.
	 */
	public void setApoe(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/apoe",v);
		_Apoe=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Sumbox=null;

	/**
	 * @return Returns the sumbox.
	 */
	public Double getSumbox() {
		try{
			if (_Sumbox==null){
				_Sumbox=getDoubleProperty("sumbox");
				return _Sumbox;
			}else {
				return _Sumbox;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sumbox.
	 * @param v Value to Set.
	 */
	public void setSumbox(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sumbox",v);
		_Sumbox=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Acsparnt=null;

	/**
	 * @return Returns the acsparnt.
	 */
	public String getAcsparnt(){
		try{
			if (_Acsparnt==null){
				_Acsparnt=getStringProperty("acsparnt");
				return _Acsparnt;
			}else {
				return _Acsparnt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for acsparnt.
	 * @param v Value to Set.
	 */
	public void setAcsparnt(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/acsparnt",v);
		_Acsparnt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Height=null;

	/**
	 * @return Returns the height.
	 */
	public String getHeight(){
		try{
			if (_Height==null){
				_Height=getStringProperty("height");
				return _Height;
			}else {
				return _Height;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for height.
	 * @param v Value to Set.
	 */
	public void setHeight(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/height",v);
		_Height=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Weight=null;

	/**
	 * @return Returns the weight.
	 */
	public String getWeight(){
		try{
			if (_Weight==null){
				_Weight=getStringProperty("weight");
				return _Weight;
			}else {
				return _Weight;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for weight.
	 * @param v Value to Set.
	 */
	public void setWeight(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/weight",v);
		_Weight=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Primstudy=null;

	/**
	 * @return Returns the primStudy.
	 */
	public String getPrimstudy(){
		try{
			if (_Primstudy==null){
				_Primstudy=getStringProperty("primStudy");
				return _Primstudy;
			}else {
				return _Primstudy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for primStudy.
	 * @param v Value to Set.
	 */
	public void setPrimstudy(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/primStudy",v);
		_Primstudy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Acsstudy=null;

	/**
	 * @return Returns the acsStudy.
	 */
	public String getAcsstudy(){
		try{
			if (_Acsstudy==null){
				_Acsstudy=getStringProperty("acsStudy");
				return _Acsstudy;
			}else {
				return _Acsstudy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for acsStudy.
	 * @param v Value to Set.
	 */
	public void setAcsstudy(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/acsStudy",v);
		_Acsstudy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.AdrcAdrcclinicaldata> getAllAdrcAdrcclinicaldatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.AdrcAdrcclinicaldata> al = new ArrayList<org.nrg.xdat.om.AdrcAdrcclinicaldata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.AdrcAdrcclinicaldata> getAdrcAdrcclinicaldatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.AdrcAdrcclinicaldata> al = new ArrayList<org.nrg.xdat.om.AdrcAdrcclinicaldata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.AdrcAdrcclinicaldata> getAdrcAdrcclinicaldatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.AdrcAdrcclinicaldata> al = new ArrayList<org.nrg.xdat.om.AdrcAdrcclinicaldata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static AdrcAdrcclinicaldata getAdrcAdrcclinicaldatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("adrc:ADRCClinicalData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (AdrcAdrcclinicaldata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
