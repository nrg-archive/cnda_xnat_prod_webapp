/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:00 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianPostgendata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianPostgendataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianPostgendata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:postgenData";

	public AutoDianPostgendata(ItemI item)
	{
		super(item);
	}

	public AutoDianPostgendata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianPostgendata(UserI user)
	 **/
	public AutoDianPostgendata(){}

	public AutoDianPostgendata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:postgenData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Dtcouns=null;

	/**
	 * @return Returns the DTCOUNS.
	 */
	public Object getDtcouns(){
		try{
			if (_Dtcouns==null){
				_Dtcouns=getProperty("DTCOUNS");
				return _Dtcouns;
			}else {
				return _Dtcouns;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DTCOUNS.
	 * @param v Value to Set.
	 */
	public void setDtcouns(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DTCOUNS",v);
		_Dtcouns=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Dtresult=null;

	/**
	 * @return Returns the DTRESULT.
	 */
	public Object getDtresult(){
		try{
			if (_Dtresult==null){
				_Dtresult=getProperty("DTRESULT");
				return _Dtresult;
			}else {
				return _Dtresult;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DTRESULT.
	 * @param v Value to Set.
	 */
	public void setDtresult(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DTRESULT",v);
		_Dtresult=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pttalk=null;

	/**
	 * @return Returns the PTTALK.
	 */
	public Integer getPttalk() {
		try{
			if (_Pttalk==null){
				_Pttalk=getIntegerProperty("PTTALK");
				return _Pttalk;
			}else {
				return _Pttalk;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PTTALK.
	 * @param v Value to Set.
	 */
	public void setPttalk(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PTTALK",v);
		_Pttalk=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Supcouns=null;

	/**
	 * @return Returns the SUPCOUNS.
	 */
	public Integer getSupcouns() {
		try{
			if (_Supcouns==null){
				_Supcouns=getIntegerProperty("SUPCOUNS");
				return _Supcouns;
			}else {
				return _Supcouns;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SUPCOUNS.
	 * @param v Value to Set.
	 */
	public void setSupcouns(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SUPCOUNS",v);
		_Supcouns=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Supresult=null;

	/**
	 * @return Returns the SUPRESULT.
	 */
	public Integer getSupresult() {
		try{
			if (_Supresult==null){
				_Supresult=getIntegerProperty("SUPRESULT");
				return _Supresult;
			}else {
				return _Supresult;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SUPRESULT.
	 * @param v Value to Set.
	 */
	public void setSupresult(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SUPRESULT",v);
		_Supresult=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Share=null;

	/**
	 * @return Returns the SHARE.
	 */
	public Integer getShare() {
		try{
			if (_Share==null){
				_Share=getIntegerProperty("SHARE");
				return _Share;
			}else {
				return _Share;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SHARE.
	 * @param v Value to Set.
	 */
	public void setShare(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SHARE",v);
		_Share=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sharewho=null;

	/**
	 * @return Returns the SHAREWHO.
	 */
	public String getSharewho(){
		try{
			if (_Sharewho==null){
				_Sharewho=getStringProperty("SHAREWHO");
				return _Sharewho;
			}else {
				return _Sharewho;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SHAREWHO.
	 * @param v Value to Set.
	 */
	public void setSharewho(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SHAREWHO",v);
		_Sharewho=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Shareoth=null;

	/**
	 * @return Returns the SHAREOTH.
	 */
	public String getShareoth(){
		try{
			if (_Shareoth==null){
				_Shareoth=getStringProperty("SHAREOTH");
				return _Shareoth;
			}else {
				return _Shareoth;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SHAREOTH.
	 * @param v Value to Set.
	 */
	public void setShareoth(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SHAREOTH",v);
		_Shareoth=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Emotion=null;

	/**
	 * @return Returns the EMOTION.
	 */
	public Integer getEmotion() {
		try{
			if (_Emotion==null){
				_Emotion=getIntegerProperty("EMOTION");
				return _Emotion;
			}else {
				return _Emotion;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for EMOTION.
	 * @param v Value to Set.
	 */
	public void setEmotion(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/EMOTION",v);
		_Emotion=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Referral=null;

	/**
	 * @return Returns the REFERRAL.
	 */
	public Integer getReferral() {
		try{
			if (_Referral==null){
				_Referral=getIntegerProperty("REFERRAL");
				return _Referral;
			}else {
				return _Referral;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REFERRAL.
	 * @param v Value to Set.
	 */
	public void setReferral(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REFERRAL",v);
		_Referral=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Reftype=null;

	/**
	 * @return Returns the REFTYPE.
	 */
	public String getReftype(){
		try{
			if (_Reftype==null){
				_Reftype=getStringProperty("REFTYPE");
				return _Reftype;
			}else {
				return _Reftype;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REFTYPE.
	 * @param v Value to Set.
	 */
	public void setReftype(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REFTYPE",v);
		_Reftype=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianPostgendata> getAllDianPostgendatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianPostgendata> al = new ArrayList<org.nrg.xdat.om.DianPostgendata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianPostgendata> getDianPostgendatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianPostgendata> al = new ArrayList<org.nrg.xdat.om.DianPostgendata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianPostgendata> getDianPostgendatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianPostgendata> al = new ArrayList<org.nrg.xdat.om.DianPostgendata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianPostgendata getDianPostgendatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:postgenData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianPostgendata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
