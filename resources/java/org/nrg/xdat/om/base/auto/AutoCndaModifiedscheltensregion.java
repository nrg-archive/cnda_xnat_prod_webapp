/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaModifiedscheltensregion extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.CndaModifiedscheltensregionI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaModifiedscheltensregion.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:modifiedScheltensRegion";

	public AutoCndaModifiedscheltensregion(ItemI item)
	{
		super(item);
	}

	public AutoCndaModifiedscheltensregion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaModifiedscheltensregion(UserI user)
	 **/
	public AutoCndaModifiedscheltensregion(){}

	public AutoCndaModifiedscheltensregion(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:modifiedScheltensRegion";
	}

	//FIELD

	private Integer _Left_lesions_small=null;

	/**
	 * @return Returns the left/lesions/small.
	 */
	public Integer getLeft_lesions_small() {
		try{
			if (_Left_lesions_small==null){
				_Left_lesions_small=getIntegerProperty("left/lesions/small");
				return _Left_lesions_small;
			}else {
				return _Left_lesions_small;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for left/lesions/small.
	 * @param v Value to Set.
	 */
	public void setLeft_lesions_small(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/left/lesions/small",v);
		_Left_lesions_small=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Left_lesions_medium=null;

	/**
	 * @return Returns the left/lesions/medium.
	 */
	public Integer getLeft_lesions_medium() {
		try{
			if (_Left_lesions_medium==null){
				_Left_lesions_medium=getIntegerProperty("left/lesions/medium");
				return _Left_lesions_medium;
			}else {
				return _Left_lesions_medium;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for left/lesions/medium.
	 * @param v Value to Set.
	 */
	public void setLeft_lesions_medium(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/left/lesions/medium",v);
		_Left_lesions_medium=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Left_lesions_large=null;

	/**
	 * @return Returns the left/lesions/large.
	 */
	public Integer getLeft_lesions_large() {
		try{
			if (_Left_lesions_large==null){
				_Left_lesions_large=getIntegerProperty("left/lesions/large");
				return _Left_lesions_large;
			}else {
				return _Left_lesions_large;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for left/lesions/large.
	 * @param v Value to Set.
	 */
	public void setLeft_lesions_large(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/left/lesions/large",v);
		_Left_lesions_large=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Left_infarcts_medium=null;

	/**
	 * @return Returns the left/infarcts/medium.
	 */
	public Integer getLeft_infarcts_medium() {
		try{
			if (_Left_infarcts_medium==null){
				_Left_infarcts_medium=getIntegerProperty("left/infarcts/medium");
				return _Left_infarcts_medium;
			}else {
				return _Left_infarcts_medium;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for left/infarcts/medium.
	 * @param v Value to Set.
	 */
	public void setLeft_infarcts_medium(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/left/infarcts/medium",v);
		_Left_infarcts_medium=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Left_infarcts_large=null;

	/**
	 * @return Returns the left/infarcts/large.
	 */
	public Integer getLeft_infarcts_large() {
		try{
			if (_Left_infarcts_large==null){
				_Left_infarcts_large=getIntegerProperty("left/infarcts/large");
				return _Left_infarcts_large;
			}else {
				return _Left_infarcts_large;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for left/infarcts/large.
	 * @param v Value to Set.
	 */
	public void setLeft_infarcts_large(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/left/infarcts/large",v);
		_Left_infarcts_large=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Left_volume=null;

	/**
	 * @return Returns the left/volume.
	 */
	public Integer getLeft_volume() {
		try{
			if (_Left_volume==null){
				_Left_volume=getIntegerProperty("left/volume");
				return _Left_volume;
			}else {
				return _Left_volume;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for left/volume.
	 * @param v Value to Set.
	 */
	public void setLeft_volume(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/left/volume",v);
		_Left_volume=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Left_confluent=null;

	/**
	 * @return Returns the left/confluent.
	 */
	public Boolean getLeft_confluent() {
		try{
			if (_Left_confluent==null){
				_Left_confluent=getBooleanProperty("left/confluent");
				return _Left_confluent;
			}else {
				return _Left_confluent;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for left/confluent.
	 * @param v Value to Set.
	 */
	public void setLeft_confluent(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/left/confluent",v);
		_Left_confluent=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Right_lesions_small=null;

	/**
	 * @return Returns the right/lesions/small.
	 */
	public Integer getRight_lesions_small() {
		try{
			if (_Right_lesions_small==null){
				_Right_lesions_small=getIntegerProperty("right/lesions/small");
				return _Right_lesions_small;
			}else {
				return _Right_lesions_small;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for right/lesions/small.
	 * @param v Value to Set.
	 */
	public void setRight_lesions_small(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/right/lesions/small",v);
		_Right_lesions_small=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Right_lesions_medium=null;

	/**
	 * @return Returns the right/lesions/medium.
	 */
	public Integer getRight_lesions_medium() {
		try{
			if (_Right_lesions_medium==null){
				_Right_lesions_medium=getIntegerProperty("right/lesions/medium");
				return _Right_lesions_medium;
			}else {
				return _Right_lesions_medium;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for right/lesions/medium.
	 * @param v Value to Set.
	 */
	public void setRight_lesions_medium(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/right/lesions/medium",v);
		_Right_lesions_medium=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Right_lesions_large=null;

	/**
	 * @return Returns the right/lesions/large.
	 */
	public Integer getRight_lesions_large() {
		try{
			if (_Right_lesions_large==null){
				_Right_lesions_large=getIntegerProperty("right/lesions/large");
				return _Right_lesions_large;
			}else {
				return _Right_lesions_large;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for right/lesions/large.
	 * @param v Value to Set.
	 */
	public void setRight_lesions_large(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/right/lesions/large",v);
		_Right_lesions_large=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Right_infarcts_medium=null;

	/**
	 * @return Returns the right/infarcts/medium.
	 */
	public Integer getRight_infarcts_medium() {
		try{
			if (_Right_infarcts_medium==null){
				_Right_infarcts_medium=getIntegerProperty("right/infarcts/medium");
				return _Right_infarcts_medium;
			}else {
				return _Right_infarcts_medium;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for right/infarcts/medium.
	 * @param v Value to Set.
	 */
	public void setRight_infarcts_medium(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/right/infarcts/medium",v);
		_Right_infarcts_medium=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Right_infarcts_large=null;

	/**
	 * @return Returns the right/infarcts/large.
	 */
	public Integer getRight_infarcts_large() {
		try{
			if (_Right_infarcts_large==null){
				_Right_infarcts_large=getIntegerProperty("right/infarcts/large");
				return _Right_infarcts_large;
			}else {
				return _Right_infarcts_large;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for right/infarcts/large.
	 * @param v Value to Set.
	 */
	public void setRight_infarcts_large(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/right/infarcts/large",v);
		_Right_infarcts_large=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Right_volume=null;

	/**
	 * @return Returns the right/volume.
	 */
	public Integer getRight_volume() {
		try{
			if (_Right_volume==null){
				_Right_volume=getIntegerProperty("right/volume");
				return _Right_volume;
			}else {
				return _Right_volume;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for right/volume.
	 * @param v Value to Set.
	 */
	public void setRight_volume(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/right/volume",v);
		_Right_volume=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Right_confluent=null;

	/**
	 * @return Returns the right/confluent.
	 */
	public Boolean getRight_confluent() {
		try{
			if (_Right_confluent==null){
				_Right_confluent=getBooleanProperty("right/confluent");
				return _Right_confluent;
			}else {
				return _Right_confluent;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for right/confluent.
	 * @param v Value to Set.
	 */
	public void setRight_confluent(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/right/confluent",v);
		_Right_confluent=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scheltens=null;

	/**
	 * @return Returns the scheltens.
	 */
	public Integer getScheltens() {
		try{
			if (_Scheltens==null){
				_Scheltens=getIntegerProperty("scheltens");
				return _Scheltens;
			}else {
				return _Scheltens;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for scheltens.
	 * @param v Value to Set.
	 */
	public void setScheltens(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/scheltens",v);
		_Scheltens=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CndaModifiedscheltensregionId=null;

	/**
	 * @return Returns the cnda_modifiedScheltensRegion_id.
	 */
	public Integer getCndaModifiedscheltensregionId() {
		try{
			if (_CndaModifiedscheltensregionId==null){
				_CndaModifiedscheltensregionId=getIntegerProperty("cnda_modifiedScheltensRegion_id");
				return _CndaModifiedscheltensregionId;
			}else {
				return _CndaModifiedscheltensregionId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda_modifiedScheltensRegion_id.
	 * @param v Value to Set.
	 */
	public void setCndaModifiedscheltensregionId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/cnda_modifiedScheltensRegion_id",v);
		_CndaModifiedscheltensregionId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaModifiedscheltensregion> getAllCndaModifiedscheltensregions(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaModifiedscheltensregion> al = new ArrayList<org.nrg.xdat.om.CndaModifiedscheltensregion>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaModifiedscheltensregion> getCndaModifiedscheltensregionsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaModifiedscheltensregion> al = new ArrayList<org.nrg.xdat.om.CndaModifiedscheltensregion>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaModifiedscheltensregion> getCndaModifiedscheltensregionsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaModifiedscheltensregion> al = new ArrayList<org.nrg.xdat.om.CndaModifiedscheltensregion>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaModifiedscheltensregion getCndaModifiedscheltensregionsByCndaModifiedscheltensregionId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:modifiedScheltensRegion/cnda_modifiedScheltensRegion_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaModifiedscheltensregion) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
