/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoNihssNihstrokescaledata extends XnatSubjectassessordata implements org.nrg.xdat.model.NihssNihstrokescaledataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoNihssNihstrokescaledata.class);
	public static String SCHEMA_ELEMENT_NAME="nihSS:nihStrokeScaleData";

	public AutoNihssNihstrokescaledata(ItemI item)
	{
		super(item);
	}

	public AutoNihssNihstrokescaledata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoNihssNihstrokescaledata(UserI user)
	 **/
	public AutoNihssNihstrokescaledata(){}

	public AutoNihssNihstrokescaledata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "nihSS:nihStrokeScaleData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q1a=null;

	/**
	 * @return Returns the Q1a.
	 */
	public Integer getQ1a() {
		try{
			if (_Q1a==null){
				_Q1a=getIntegerProperty("Q1a");
				return _Q1a;
			}else {
				return _Q1a;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q1a.
	 * @param v Value to Set.
	 */
	public void setQ1a(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q1a",v);
		_Q1a=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q1b=null;

	/**
	 * @return Returns the Q1b.
	 */
	public Integer getQ1b() {
		try{
			if (_Q1b==null){
				_Q1b=getIntegerProperty("Q1b");
				return _Q1b;
			}else {
				return _Q1b;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q1b.
	 * @param v Value to Set.
	 */
	public void setQ1b(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q1b",v);
		_Q1b=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q1c=null;

	/**
	 * @return Returns the Q1c.
	 */
	public Integer getQ1c() {
		try{
			if (_Q1c==null){
				_Q1c=getIntegerProperty("Q1c");
				return _Q1c;
			}else {
				return _Q1c;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q1c.
	 * @param v Value to Set.
	 */
	public void setQ1c(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q1c",v);
		_Q1c=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q2=null;

	/**
	 * @return Returns the Q2.
	 */
	public Integer getQ2() {
		try{
			if (_Q2==null){
				_Q2=getIntegerProperty("Q2");
				return _Q2;
			}else {
				return _Q2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q2.
	 * @param v Value to Set.
	 */
	public void setQ2(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q2",v);
		_Q2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q3=null;

	/**
	 * @return Returns the Q3.
	 */
	public Integer getQ3() {
		try{
			if (_Q3==null){
				_Q3=getIntegerProperty("Q3");
				return _Q3;
			}else {
				return _Q3;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q3.
	 * @param v Value to Set.
	 */
	public void setQ3(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q3",v);
		_Q3=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q4=null;

	/**
	 * @return Returns the Q4.
	 */
	public Integer getQ4() {
		try{
			if (_Q4==null){
				_Q4=getIntegerProperty("Q4");
				return _Q4;
			}else {
				return _Q4;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q4.
	 * @param v Value to Set.
	 */
	public void setQ4(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q4",v);
		_Q4=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q5a=null;

	/**
	 * @return Returns the Q5a.
	 */
	public Integer getQ5a() {
		try{
			if (_Q5a==null){
				_Q5a=getIntegerProperty("Q5a");
				return _Q5a;
			}else {
				return _Q5a;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q5a.
	 * @param v Value to Set.
	 */
	public void setQ5a(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q5a",v);
		_Q5a=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Q5anote=null;

	/**
	 * @return Returns the Q5aNote.
	 */
	public String getQ5anote(){
		try{
			if (_Q5anote==null){
				_Q5anote=getStringProperty("Q5aNote");
				return _Q5anote;
			}else {
				return _Q5anote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q5aNote.
	 * @param v Value to Set.
	 */
	public void setQ5anote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q5aNote",v);
		_Q5anote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q5b=null;

	/**
	 * @return Returns the Q5b.
	 */
	public Integer getQ5b() {
		try{
			if (_Q5b==null){
				_Q5b=getIntegerProperty("Q5b");
				return _Q5b;
			}else {
				return _Q5b;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q5b.
	 * @param v Value to Set.
	 */
	public void setQ5b(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q5b",v);
		_Q5b=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Q5bnote=null;

	/**
	 * @return Returns the Q5bNote.
	 */
	public String getQ5bnote(){
		try{
			if (_Q5bnote==null){
				_Q5bnote=getStringProperty("Q5bNote");
				return _Q5bnote;
			}else {
				return _Q5bnote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q5bNote.
	 * @param v Value to Set.
	 */
	public void setQ5bnote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q5bNote",v);
		_Q5bnote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q6a=null;

	/**
	 * @return Returns the Q6a.
	 */
	public Integer getQ6a() {
		try{
			if (_Q6a==null){
				_Q6a=getIntegerProperty("Q6a");
				return _Q6a;
			}else {
				return _Q6a;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q6a.
	 * @param v Value to Set.
	 */
	public void setQ6a(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q6a",v);
		_Q6a=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Q6anote=null;

	/**
	 * @return Returns the Q6aNote.
	 */
	public String getQ6anote(){
		try{
			if (_Q6anote==null){
				_Q6anote=getStringProperty("Q6aNote");
				return _Q6anote;
			}else {
				return _Q6anote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q6aNote.
	 * @param v Value to Set.
	 */
	public void setQ6anote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q6aNote",v);
		_Q6anote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q6b=null;

	/**
	 * @return Returns the Q6b.
	 */
	public Integer getQ6b() {
		try{
			if (_Q6b==null){
				_Q6b=getIntegerProperty("Q6b");
				return _Q6b;
			}else {
				return _Q6b;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q6b.
	 * @param v Value to Set.
	 */
	public void setQ6b(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q6b",v);
		_Q6b=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Q6bnote=null;

	/**
	 * @return Returns the Q6bNote.
	 */
	public String getQ6bnote(){
		try{
			if (_Q6bnote==null){
				_Q6bnote=getStringProperty("Q6bNote");
				return _Q6bnote;
			}else {
				return _Q6bnote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q6bNote.
	 * @param v Value to Set.
	 */
	public void setQ6bnote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q6bNote",v);
		_Q6bnote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q7=null;

	/**
	 * @return Returns the Q7.
	 */
	public Integer getQ7() {
		try{
			if (_Q7==null){
				_Q7=getIntegerProperty("Q7");
				return _Q7;
			}else {
				return _Q7;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q7.
	 * @param v Value to Set.
	 */
	public void setQ7(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q7",v);
		_Q7=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Q7note=null;

	/**
	 * @return Returns the Q7Note.
	 */
	public String getQ7note(){
		try{
			if (_Q7note==null){
				_Q7note=getStringProperty("Q7Note");
				return _Q7note;
			}else {
				return _Q7note;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q7Note.
	 * @param v Value to Set.
	 */
	public void setQ7note(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q7Note",v);
		_Q7note=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q8=null;

	/**
	 * @return Returns the Q8.
	 */
	public Integer getQ8() {
		try{
			if (_Q8==null){
				_Q8=getIntegerProperty("Q8");
				return _Q8;
			}else {
				return _Q8;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q8.
	 * @param v Value to Set.
	 */
	public void setQ8(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q8",v);
		_Q8=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q9=null;

	/**
	 * @return Returns the Q9.
	 */
	public Integer getQ9() {
		try{
			if (_Q9==null){
				_Q9=getIntegerProperty("Q9");
				return _Q9;
			}else {
				return _Q9;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q9.
	 * @param v Value to Set.
	 */
	public void setQ9(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q9",v);
		_Q9=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q10=null;

	/**
	 * @return Returns the Q10.
	 */
	public Integer getQ10() {
		try{
			if (_Q10==null){
				_Q10=getIntegerProperty("Q10");
				return _Q10;
			}else {
				return _Q10;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q10.
	 * @param v Value to Set.
	 */
	public void setQ10(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q10",v);
		_Q10=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Q10note=null;

	/**
	 * @return Returns the Q10Note.
	 */
	public String getQ10note(){
		try{
			if (_Q10note==null){
				_Q10note=getStringProperty("Q10Note");
				return _Q10note;
			}else {
				return _Q10note;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q10Note.
	 * @param v Value to Set.
	 */
	public void setQ10note(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q10Note",v);
		_Q10note=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Q11=null;

	/**
	 * @return Returns the Q11.
	 */
	public Integer getQ11() {
		try{
			if (_Q11==null){
				_Q11=getIntegerProperty("Q11");
				return _Q11;
			}else {
				return _Q11;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Q11.
	 * @param v Value to Set.
	 */
	public void setQ11(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Q11",v);
		_Q11=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.NihssNihstrokescaledata> getAllNihssNihstrokescaledatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.NihssNihstrokescaledata> al = new ArrayList<org.nrg.xdat.om.NihssNihstrokescaledata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.NihssNihstrokescaledata> getNihssNihstrokescaledatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.NihssNihstrokescaledata> al = new ArrayList<org.nrg.xdat.om.NihssNihstrokescaledata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.NihssNihstrokescaledata> getNihssNihstrokescaledatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.NihssNihstrokescaledata> al = new ArrayList<org.nrg.xdat.om.NihssNihstrokescaledata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static NihssNihstrokescaledata getNihssNihstrokescaledatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("nihSS:nihStrokeScaleData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (NihssNihstrokescaledata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
