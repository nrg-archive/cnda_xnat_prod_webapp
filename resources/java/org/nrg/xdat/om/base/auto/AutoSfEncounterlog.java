/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoSfEncounterlog extends XnatSubjectassessordata implements org.nrg.xdat.model.SfEncounterlogI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoSfEncounterlog.class);
	public static String SCHEMA_ELEMENT_NAME="sf:encounterLog";

	public AutoSfEncounterlog(ItemI item)
	{
		super(item);
	}

	public AutoSfEncounterlog(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoSfEncounterlog(UserI user)
	 **/
	public AutoSfEncounterlog(){}

	public AutoSfEncounterlog(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "sf:encounterLog";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.SfEncounterlogEncounter> _Encounters_encounter =null;

	/**
	 * encounters/encounter
	 * @return Returns an List of org.nrg.xdat.om.SfEncounterlogEncounter
	 */
	public <A extends org.nrg.xdat.model.SfEncounterlogEncounterI> List<A> getEncounters_encounter() {
		try{
			if (_Encounters_encounter==null){
				_Encounters_encounter=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("encounters/encounter"));
			}
			return (List<A>) _Encounters_encounter;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.SfEncounterlogEncounter>();}
	}

	/**
	 * Sets the value for encounters/encounter.
	 * @param v Value to Set.
	 */
	public void setEncounters_encounter(ItemI v) throws Exception{
		_Encounters_encounter =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/encounters/encounter",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/encounters/encounter",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * encounters/encounter
	 * Adds org.nrg.xdat.model.SfEncounterlogEncounterI
	 */
	public <A extends org.nrg.xdat.model.SfEncounterlogEncounterI> void addEncounters_encounter(A item) throws Exception{
	setEncounters_encounter((ItemI)item);
	}

	/**
	 * Removes the encounters/encounter of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeEncounters_encounter(int index) throws java.lang.IndexOutOfBoundsException {
		_Encounters_encounter =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/encounters/encounter",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.SfEncounterlog> getAllSfEncounterlogs(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfEncounterlog> al = new ArrayList<org.nrg.xdat.om.SfEncounterlog>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.SfEncounterlog> getSfEncounterlogsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfEncounterlog> al = new ArrayList<org.nrg.xdat.om.SfEncounterlog>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.SfEncounterlog> getSfEncounterlogsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfEncounterlog> al = new ArrayList<org.nrg.xdat.om.SfEncounterlog>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static SfEncounterlog getSfEncounterlogsById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("sf:encounterLog/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (SfEncounterlog) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //encounters/encounter
	        for(org.nrg.xdat.model.SfEncounterlogEncounterI childEncounters_encounter : this.getEncounters_encounter()){
	            if (childEncounters_encounter!=null){
	              for(ResourceFile rf: ((SfEncounterlogEncounter)childEncounters_encounter).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("encounters/encounter[" + ((SfEncounterlogEncounter)childEncounters_encounter).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("encounters/encounter/" + ((SfEncounterlogEncounter)childEncounters_encounter).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
