/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:00 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianRegistrydata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianRegistrydataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianRegistrydata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:registryData";

	public AutoDianRegistrydata(ItemI item)
	{
		super(item);
	}

	public AutoDianRegistrydata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianRegistrydata(UserI user)
	 **/
	public AutoDianRegistrydata(){}

	public AutoDianRegistrydata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:registryData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Site=null;

	/**
	 * @return Returns the SITE.
	 */
	public String getSite(){
		try{
			if (_Site==null){
				_Site=getStringProperty("SITE");
				return _Site;
			}else {
				return _Site;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SITE.
	 * @param v Value to Set.
	 */
	public void setSite(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SITE",v);
		_Site=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rgcondct=null;

	/**
	 * @return Returns the RGCONDCT.
	 */
	public Integer getRgcondct() {
		try{
			if (_Rgcondct==null){
				_Rgcondct=getIntegerProperty("RGCONDCT");
				return _Rgcondct;
			}else {
				return _Rgcondct;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RGCONDCT.
	 * @param v Value to Set.
	 */
	public void setRgcondct(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RGCONDCT",v);
		_Rgcondct=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rgpartial=null;

	/**
	 * @return Returns the RGPARTIAL.
	 */
	public Integer getRgpartial() {
		try{
			if (_Rgpartial==null){
				_Rgpartial=getIntegerProperty("RGPARTIAL");
				return _Rgpartial;
			}else {
				return _Rgpartial;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RGPARTIAL.
	 * @param v Value to Set.
	 */
	public void setRgpartial(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RGPARTIAL",v);
		_Rgpartial=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Visdate=null;

	/**
	 * @return Returns the VISDATE.
	 */
	public Object getVisdate(){
		try{
			if (_Visdate==null){
				_Visdate=getProperty("VISDATE");
				return _Visdate;
			}else {
				return _Visdate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VISDATE.
	 * @param v Value to Set.
	 */
	public void setVisdate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VISDATE",v);
		_Visdate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Csid1=null;

	/**
	 * @return Returns the CSID1.
	 */
	public String getCsid1(){
		try{
			if (_Csid1==null){
				_Csid1=getStringProperty("CSID1");
				return _Csid1;
			}else {
				return _Csid1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSID1.
	 * @param v Value to Set.
	 */
	public void setCsid1(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSID1",v);
		_Csid1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Csid2=null;

	/**
	 * @return Returns the CSID2.
	 */
	public String getCsid2(){
		try{
			if (_Csid2==null){
				_Csid2=getStringProperty("CSID2");
				return _Csid2;
			}else {
				return _Csid2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSID2.
	 * @param v Value to Set.
	 */
	public void setCsid2(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSID2",v);
		_Csid2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Csdets=null;

	/**
	 * @return Returns the CSDETS.
	 */
	public String getCsdets(){
		try{
			if (_Csdets==null){
				_Csdets=getStringProperty("CSDETS");
				return _Csdets;
			}else {
				return _Csdets;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CSDETS.
	 * @param v Value to Set.
	 */
	public void setCsdets(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CSDETS",v);
		_Csdets=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Lang=null;

	/**
	 * @return Returns the LANG.
	 */
	public Integer getLang() {
		try{
			if (_Lang==null){
				_Lang=getIntegerProperty("LANG");
				return _Lang;
			}else {
				return _Lang;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LANG.
	 * @param v Value to Set.
	 */
	public void setLang(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LANG",v);
		_Lang=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianRegistrydata> getAllDianRegistrydatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianRegistrydata> al = new ArrayList<org.nrg.xdat.om.DianRegistrydata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianRegistrydata> getDianRegistrydatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianRegistrydata> al = new ArrayList<org.nrg.xdat.om.DianRegistrydata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianRegistrydata> getDianRegistrydatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianRegistrydata> al = new ArrayList<org.nrg.xdat.om.DianRegistrydata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianRegistrydata getDianRegistrydatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:registryData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianRegistrydata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
