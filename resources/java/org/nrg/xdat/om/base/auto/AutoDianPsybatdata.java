/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:00 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianPsybatdata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianPsybatdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianPsybatdata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:psybatData";

	public AutoDianPsybatdata(ItemI item)
	{
		super(item);
	}

	public AutoDianPsybatdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianPsybatdata(UserI user)
	 **/
	public AutoDianPsybatdata(){}

	public AutoDianPsybatdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:psybatData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Initials=null;

	/**
	 * @return Returns the INITIALS.
	 */
	public String getInitials(){
		try{
			if (_Initials==null){
				_Initials=getStringProperty("INITIALS");
				return _Initials;
			}else {
				return _Initials;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INITIALS.
	 * @param v Value to Set.
	 */
	public void setInitials(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INITIALS",v);
		_Initials=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Letterf=null;

	/**
	 * @return Returns the LETTERF.
	 */
	public Integer getLetterf() {
		try{
			if (_Letterf==null){
				_Letterf=getIntegerProperty("LETTERF");
				return _Letterf;
			}else {
				return _Letterf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LETTERF.
	 * @param v Value to Set.
	 */
	public void setLetterf(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LETTERF",v);
		_Letterf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Lettera=null;

	/**
	 * @return Returns the LETTERA.
	 */
	public Integer getLettera() {
		try{
			if (_Lettera==null){
				_Lettera=getIntegerProperty("LETTERA");
				return _Lettera;
			}else {
				return _Lettera;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LETTERA.
	 * @param v Value to Set.
	 */
	public void setLettera(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LETTERA",v);
		_Lettera=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Letters=null;

	/**
	 * @return Returns the LETTERS.
	 */
	public Integer getLetters() {
		try{
			if (_Letters==null){
				_Letters=getIntegerProperty("LETTERS");
				return _Letters;
			}else {
				return _Letters;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LETTERS.
	 * @param v Value to Set.
	 */
	public void setLetters(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LETTERS",v);
		_Letters=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wrlist=null;

	/**
	 * @return Returns the WRLIST.
	 */
	public Integer getWrlist() {
		try{
			if (_Wrlist==null){
				_Wrlist=getIntegerProperty("WRLIST");
				return _Wrlist;
			}else {
				return _Wrlist;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WRLIST.
	 * @param v Value to Set.
	 */
	public void setWrlist(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WRLIST",v);
		_Wrlist=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wrimmed=null;

	/**
	 * @return Returns the WRIMMED.
	 */
	public Integer getWrimmed() {
		try{
			if (_Wrimmed==null){
				_Wrimmed=getIntegerProperty("WRIMMED");
				return _Wrimmed;
			}else {
				return _Wrimmed;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WRIMMED.
	 * @param v Value to Set.
	 */
	public void setWrimmed(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WRIMMED",v);
		_Wrimmed=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mmsetot=null;

	/**
	 * @return Returns the MMSETOT.
	 */
	public Integer getMmsetot() {
		try{
			if (_Mmsetot==null){
				_Mmsetot=getIntegerProperty("MMSETOT");
				return _Mmsetot;
			}else {
				return _Mmsetot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MMSETOT.
	 * @param v Value to Set.
	 */
	public void setMmsetot(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MMSETOT",v);
		_Mmsetot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Logmemim=null;

	/**
	 * @return Returns the LOGMEMIM.
	 */
	public Integer getLogmemim() {
		try{
			if (_Logmemim==null){
				_Logmemim=getIntegerProperty("LOGMEMIM");
				return _Logmemim;
			}else {
				return _Logmemim;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LOGMEMIM.
	 * @param v Value to Set.
	 */
	public void setLogmemim(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LOGMEMIM",v);
		_Logmemim=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Dspfwd=null;

	/**
	 * @return Returns the DSPFWD.
	 */
	public Integer getDspfwd() {
		try{
			if (_Dspfwd==null){
				_Dspfwd=getIntegerProperty("DSPFWD");
				return _Dspfwd;
			}else {
				return _Dspfwd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DSPFWD.
	 * @param v Value to Set.
	 */
	public void setDspfwd(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DSPFWD",v);
		_Dspfwd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Dspfwdl=null;

	/**
	 * @return Returns the DSPFWDL.
	 */
	public Integer getDspfwdl() {
		try{
			if (_Dspfwdl==null){
				_Dspfwdl=getIntegerProperty("DSPFWDL");
				return _Dspfwdl;
			}else {
				return _Dspfwdl;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DSPFWDL.
	 * @param v Value to Set.
	 */
	public void setDspfwdl(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DSPFWDL",v);
		_Dspfwdl=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Dspback=null;

	/**
	 * @return Returns the DSPBACK.
	 */
	public Integer getDspback() {
		try{
			if (_Dspback==null){
				_Dspback=getIntegerProperty("DSPBACK");
				return _Dspback;
			}else {
				return _Dspback;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DSPBACK.
	 * @param v Value to Set.
	 */
	public void setDspback(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DSPBACK",v);
		_Dspback=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Dspbackl=null;

	/**
	 * @return Returns the DSPBACKL.
	 */
	public Integer getDspbackl() {
		try{
			if (_Dspbackl==null){
				_Dspbackl=getIntegerProperty("DSPBACKL");
				return _Dspbackl;
			}else {
				return _Dspbackl;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DSPBACKL.
	 * @param v Value to Set.
	 */
	public void setDspbackl(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DSPBACKL",v);
		_Dspbackl=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Catanim=null;

	/**
	 * @return Returns the CATANIM.
	 */
	public Integer getCatanim() {
		try{
			if (_Catanim==null){
				_Catanim=getIntegerProperty("CATANIM");
				return _Catanim;
			}else {
				return _Catanim;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CATANIM.
	 * @param v Value to Set.
	 */
	public void setCatanim(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CATANIM",v);
		_Catanim=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Catveg=null;

	/**
	 * @return Returns the CATVEG.
	 */
	public Integer getCatveg() {
		try{
			if (_Catveg==null){
				_Catveg=getIntegerProperty("CATVEG");
				return _Catveg;
			}else {
				return _Catveg;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CATVEG.
	 * @param v Value to Set.
	 */
	public void setCatveg(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CATVEG",v);
		_Catveg=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tratime=null;

	/**
	 * @return Returns the TRATIME.
	 */
	public Integer getTratime() {
		try{
			if (_Tratime==null){
				_Tratime=getIntegerProperty("TRATIME");
				return _Tratime;
			}else {
				return _Tratime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRATIME.
	 * @param v Value to Set.
	 */
	public void setTratime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRATIME",v);
		_Tratime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Traerrc=null;

	/**
	 * @return Returns the TRAERRC.
	 */
	public Integer getTraerrc() {
		try{
			if (_Traerrc==null){
				_Traerrc=getIntegerProperty("TRAERRC");
				return _Traerrc;
			}else {
				return _Traerrc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRAERRC.
	 * @param v Value to Set.
	 */
	public void setTraerrc(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRAERRC",v);
		_Traerrc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tralines=null;

	/**
	 * @return Returns the TRALINES.
	 */
	public Integer getTralines() {
		try{
			if (_Tralines==null){
				_Tralines=getIntegerProperty("TRALINES");
				return _Tralines;
			}else {
				return _Tralines;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRALINES.
	 * @param v Value to Set.
	 */
	public void setTralines(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRALINES",v);
		_Tralines=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Trbtime=null;

	/**
	 * @return Returns the TRBTIME.
	 */
	public Integer getTrbtime() {
		try{
			if (_Trbtime==null){
				_Trbtime=getIntegerProperty("TRBTIME");
				return _Trbtime;
			}else {
				return _Trbtime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRBTIME.
	 * @param v Value to Set.
	 */
	public void setTrbtime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRBTIME",v);
		_Trbtime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Trberrc=null;

	/**
	 * @return Returns the TRBERRC.
	 */
	public Integer getTrberrc() {
		try{
			if (_Trberrc==null){
				_Trberrc=getIntegerProperty("TRBERRC");
				return _Trberrc;
			}else {
				return _Trberrc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRBERRC.
	 * @param v Value to Set.
	 */
	public void setTrberrc(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRBERRC",v);
		_Trberrc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Trblines=null;

	/**
	 * @return Returns the TRBLINES.
	 */
	public Integer getTrblines() {
		try{
			if (_Trblines==null){
				_Trblines=getIntegerProperty("TRBLINES");
				return _Trblines;
			}else {
				return _Trblines;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRBLINES.
	 * @param v Value to Set.
	 */
	public void setTrblines(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRBLINES",v);
		_Trblines=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Digsymb=null;

	/**
	 * @return Returns the DIGSYMB.
	 */
	public Integer getDigsymb() {
		try{
			if (_Digsymb==null){
				_Digsymb=getIntegerProperty("DIGSYMB");
				return _Digsymb;
			}else {
				return _Digsymb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DIGSYMB.
	 * @param v Value to Set.
	 */
	public void setDigsymb(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DIGSYMB",v);
		_Digsymb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Logmemdl=null;

	/**
	 * @return Returns the LOGMEMDL.
	 */
	public Integer getLogmemdl() {
		try{
			if (_Logmemdl==null){
				_Logmemdl=getIntegerProperty("LOGMEMDL");
				return _Logmemdl;
			}else {
				return _Logmemdl;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LOGMEMDL.
	 * @param v Value to Set.
	 */
	public void setLogmemdl(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LOGMEMDL",v);
		_Logmemdl=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Bnttot=null;

	/**
	 * @return Returns the BNTTOT.
	 */
	public Integer getBnttot() {
		try{
			if (_Bnttot==null){
				_Bnttot=getIntegerProperty("BNTTOT");
				return _Bnttot;
			}else {
				return _Bnttot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BNTTOT.
	 * @param v Value to Set.
	 */
	public void setBnttot(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BNTTOT",v);
		_Bnttot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wrdelay=null;

	/**
	 * @return Returns the WRDELAY.
	 */
	public Integer getWrdelay() {
		try{
			if (_Wrdelay==null){
				_Wrdelay=getIntegerProperty("WRDELAY");
				return _Wrdelay;
			}else {
				return _Wrdelay;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WRDELAY.
	 * @param v Value to Set.
	 */
	public void setWrdelay(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WRDELAY",v);
		_Wrdelay=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianPsybatdata> getAllDianPsybatdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianPsybatdata> al = new ArrayList<org.nrg.xdat.om.DianPsybatdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianPsybatdata> getDianPsybatdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianPsybatdata> al = new ArrayList<org.nrg.xdat.om.DianPsybatdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianPsybatdata> getDianPsybatdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianPsybatdata> al = new ArrayList<org.nrg.xdat.om.DianPsybatdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianPsybatdata getDianPsybatdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:psybatData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianPsybatdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
