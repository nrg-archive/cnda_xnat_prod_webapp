/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoRadRadiologyreaddata extends XnatImageassessordata implements org.nrg.xdat.model.RadRadiologyreaddataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoRadRadiologyreaddata.class);
	public static String SCHEMA_ELEMENT_NAME="rad:radiologyReadData";

	public AutoRadRadiologyreaddata(ItemI item)
	{
		super(item);
	}

	public AutoRadRadiologyreaddata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoRadRadiologyreaddata(UserI user)
	 **/
	public AutoRadRadiologyreaddata(){}

	public AutoRadRadiologyreaddata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "rad:radiologyReadData";
	}
	 private org.nrg.xdat.om.XnatImageassessordata _Imageassessordata =null;

	/**
	 * imageAssessorData
	 * @return org.nrg.xdat.om.XnatImageassessordata
	 */
	public org.nrg.xdat.om.XnatImageassessordata getImageassessordata() {
		try{
			if (_Imageassessordata==null){
				_Imageassessordata=((XnatImageassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("imageAssessorData")));
				return _Imageassessordata;
			}else {
				return _Imageassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for imageAssessorData.
	 * @param v Value to Set.
	 */
	public void setImageassessordata(ItemI v) throws Exception{
		_Imageassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/imageAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/imageAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * imageAssessorData
	 * set org.nrg.xdat.model.XnatImageassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatImageassessordataI> void setImageassessordata(A item) throws Exception{
	setImageassessordata((ItemI)item);
	}

	/**
	 * Removes the imageAssessorData.
	 * */
	public void removeImageassessordata() {
		_Imageassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/imageAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Modality=null;

	/**
	 * @return Returns the modality.
	 */
	public String getModality(){
		try{
			if (_Modality==null){
				_Modality=getStringProperty("modality");
				return _Modality;
			}else {
				return _Modality;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for modality.
	 * @param v Value to Set.
	 */
	public void setModality(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/modality",v);
		_Modality=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Reader=null;

	/**
	 * @return Returns the reader.
	 */
	public String getReader(){
		try{
			if (_Reader==null){
				_Reader=getStringProperty("reader");
				return _Reader;
			}else {
				return _Reader;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for reader.
	 * @param v Value to Set.
	 */
	public void setReader(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/reader",v);
		_Reader=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Technique=null;

	/**
	 * @return Returns the technique.
	 */
	public String getTechnique(){
		try{
			if (_Technique==null){
				_Technique=getStringProperty("technique");
				return _Technique;
			}else {
				return _Technique;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for technique.
	 * @param v Value to Set.
	 */
	public void setTechnique(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/technique",v);
		_Technique=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Finding=null;

	/**
	 * @return Returns the finding.
	 */
	public String getFinding(){
		try{
			if (_Finding==null){
				_Finding=getStringProperty("finding");
				return _Finding;
			}else {
				return _Finding;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for finding.
	 * @param v Value to Set.
	 */
	public void setFinding(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/finding",v);
		_Finding=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Finding_normalStatus=null;

	/**
	 * @return Returns the finding/normal_status.
	 */
	public Boolean getFinding_normalStatus() {
		try{
			if (_Finding_normalStatus==null){
				_Finding_normalStatus=getBooleanProperty("finding/normal_status");
				return _Finding_normalStatus;
			}else {
				return _Finding_normalStatus;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for finding/normal_status.
	 * @param v Value to Set.
	 */
	public void setFinding_normalStatus(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/finding/normal_status",v);
		_Finding_normalStatus=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Diagnosis=null;

	/**
	 * @return Returns the diagnosis.
	 */
	public String getDiagnosis(){
		try{
			if (_Diagnosis==null){
				_Diagnosis=getStringProperty("diagnosis");
				return _Diagnosis;
			}else {
				return _Diagnosis;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for diagnosis.
	 * @param v Value to Set.
	 */
	public void setDiagnosis(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/diagnosis",v);
		_Diagnosis=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _HippocampalAtrophy=null;

	/**
	 * @return Returns the hippocampal_atrophy.
	 */
	public String getHippocampalAtrophy(){
		try{
			if (_HippocampalAtrophy==null){
				_HippocampalAtrophy=getStringProperty("hippocampal_atrophy");
				return _HippocampalAtrophy;
			}else {
				return _HippocampalAtrophy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for hippocampal_atrophy.
	 * @param v Value to Set.
	 */
	public void setHippocampalAtrophy(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/hippocampal_atrophy",v);
		_HippocampalAtrophy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _CorticalAtrophy=null;

	/**
	 * @return Returns the cortical_atrophy.
	 */
	public String getCorticalAtrophy(){
		try{
			if (_CorticalAtrophy==null){
				_CorticalAtrophy=getStringProperty("cortical_atrophy");
				return _CorticalAtrophy;
			}else {
				return _CorticalAtrophy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cortical_atrophy.
	 * @param v Value to Set.
	 */
	public void setCorticalAtrophy(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/cortical_atrophy",v);
		_CorticalAtrophy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Leukoaraiosis=null;

	/**
	 * @return Returns the leukoaraiosis.
	 */
	public String getLeukoaraiosis(){
		try{
			if (_Leukoaraiosis==null){
				_Leukoaraiosis=getStringProperty("leukoaraiosis");
				return _Leukoaraiosis;
			}else {
				return _Leukoaraiosis;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for leukoaraiosis.
	 * @param v Value to Set.
	 */
	public void setLeukoaraiosis(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/leukoaraiosis",v);
		_Leukoaraiosis=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _LargeInfarcts=null;

	/**
	 * @return Returns the large_infarcts.
	 */
	public Integer getLargeInfarcts() {
		try{
			if (_LargeInfarcts==null){
				_LargeInfarcts=getIntegerProperty("large_infarcts");
				return _LargeInfarcts;
			}else {
				return _LargeInfarcts;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for large_infarcts.
	 * @param v Value to Set.
	 */
	public void setLargeInfarcts(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/large_infarcts",v);
		_LargeInfarcts=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _SmallInfarcts=null;

	/**
	 * @return Returns the small_infarcts.
	 */
	public Integer getSmallInfarcts() {
		try{
			if (_SmallInfarcts==null){
				_SmallInfarcts=getIntegerProperty("small_infarcts");
				return _SmallInfarcts;
			}else {
				return _SmallInfarcts;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for small_infarcts.
	 * @param v Value to Set.
	 */
	public void setSmallInfarcts(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/small_infarcts",v);
		_SmallInfarcts=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Microbleeds=null;

	/**
	 * @return Returns the microbleeds.
	 */
	public Integer getMicrobleeds() {
		try{
			if (_Microbleeds==null){
				_Microbleeds=getIntegerProperty("microbleeds");
				return _Microbleeds;
			}else {
				return _Microbleeds;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for microbleeds.
	 * @param v Value to Set.
	 */
	public void setMicrobleeds(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/microbleeds",v);
		_Microbleeds=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _SiteMicrobleeds=null;

	/**
	 * @return Returns the site_microbleeds.
	 */
	public String getSiteMicrobleeds(){
		try{
			if (_SiteMicrobleeds==null){
				_SiteMicrobleeds=getStringProperty("site_microbleeds");
				return _SiteMicrobleeds;
			}else {
				return _SiteMicrobleeds;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for site_microbleeds.
	 * @param v Value to Set.
	 */
	public void setSiteMicrobleeds(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/site_microbleeds",v);
		_SiteMicrobleeds=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Lacunes=null;

	/**
	 * @return Returns the lacunes.
	 */
	public String getLacunes(){
		try{
			if (_Lacunes==null){
				_Lacunes=getStringProperty("lacunes");
				return _Lacunes;
			}else {
				return _Lacunes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for lacunes.
	 * @param v Value to Set.
	 */
	public void setLacunes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/lacunes",v);
		_Lacunes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _OtherSignificantFindings=null;

	/**
	 * @return Returns the other_significant_findings.
	 */
	public Boolean getOtherSignificantFindings() {
		try{
			if (_OtherSignificantFindings==null){
				_OtherSignificantFindings=getBooleanProperty("other_significant_findings");
				return _OtherSignificantFindings;
			}else {
				return _OtherSignificantFindings;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for other_significant_findings.
	 * @param v Value to Set.
	 */
	public void setOtherSignificantFindings(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/other_significant_findings",v);
		_OtherSignificantFindings=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _AgingChanges=null;

	/**
	 * @return Returns the aging_changes.
	 */
	public String getAgingChanges(){
		try{
			if (_AgingChanges==null){
				_AgingChanges=getStringProperty("aging_changes");
				return _AgingChanges;
			}else {
				return _AgingChanges;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for aging_changes.
	 * @param v Value to Set.
	 */
	public void setAgingChanges(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/aging_changes",v);
		_AgingChanges=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Comparison=null;

	/**
	 * @return Returns the comparison.
	 */
	public String getComparison(){
		try{
			if (_Comparison==null){
				_Comparison=getStringProperty("comparison");
				return _Comparison;
			}else {
				return _Comparison;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for comparison.
	 * @param v Value to Set.
	 */
	public void setComparison(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/comparison",v);
		_Comparison=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _History=null;

	/**
	 * @return Returns the history.
	 */
	public String getHistory(){
		try{
			if (_History==null){
				_History=getStringProperty("history");
				return _History;
			}else {
				return _History;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for history.
	 * @param v Value to Set.
	 */
	public void setHistory(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/history",v);
		_History=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Exam=null;

	/**
	 * @return Returns the exam.
	 */
	public String getExam(){
		try{
			if (_Exam==null){
				_Exam=getStringProperty("exam");
				return _Exam;
			}else {
				return _Exam;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for exam.
	 * @param v Value to Set.
	 */
	public void setExam(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/exam",v);
		_Exam=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _FollowupRecommended=null;

	/**
	 * @return Returns the followup_recommended.
	 */
	public Boolean getFollowupRecommended() {
		try{
			if (_FollowupRecommended==null){
				_FollowupRecommended=getBooleanProperty("followup_recommended");
				return _FollowupRecommended;
			}else {
				return _FollowupRecommended;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for followup_recommended.
	 * @param v Value to Set.
	 */
	public void setFollowupRecommended(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/followup_recommended",v);
		_FollowupRecommended=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.RadRadiologyreaddata> getAllRadRadiologyreaddatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.RadRadiologyreaddata> al = new ArrayList<org.nrg.xdat.om.RadRadiologyreaddata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.RadRadiologyreaddata> getRadRadiologyreaddatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.RadRadiologyreaddata> al = new ArrayList<org.nrg.xdat.om.RadRadiologyreaddata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.RadRadiologyreaddata> getRadRadiologyreaddatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.RadRadiologyreaddata> al = new ArrayList<org.nrg.xdat.om.RadRadiologyreaddata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static RadRadiologyreaddata getRadRadiologyreaddatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("rad:radiologyReadData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (RadRadiologyreaddata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //imageAssessorData
	        XnatImageassessordata childImageassessordata = (XnatImageassessordata)this.getImageassessordata();
	            if (childImageassessordata!=null){
	              for(ResourceFile rf: ((XnatImageassessordata)childImageassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("imageAssessorData[" + ((XnatImageassessordata)childImageassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("imageAssessorData/" + ((XnatImageassessordata)childImageassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
