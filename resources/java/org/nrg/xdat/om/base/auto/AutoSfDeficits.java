/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoSfDeficits extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.SfDeficitsI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoSfDeficits.class);
	public static String SCHEMA_ELEMENT_NAME="sf:deficits";

	public AutoSfDeficits(ItemI item)
	{
		super(item);
	}

	public AutoSfDeficits(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoSfDeficits(UserI user)
	 **/
	public AutoSfDeficits(){}

	public AutoSfDeficits(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "sf:deficits";
	}

	//FIELD

	private Boolean _Motorweakness=null;

	/**
	 * @return Returns the motorWeakness.
	 */
	public Boolean getMotorweakness() {
		try{
			if (_Motorweakness==null){
				_Motorweakness=getBooleanProperty("motorWeakness");
				return _Motorweakness;
			}else {
				return _Motorweakness;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for motorWeakness.
	 * @param v Value to Set.
	 */
	public void setMotorweakness(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/motorWeakness",v);
		_Motorweakness=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Sensorychange=null;

	/**
	 * @return Returns the sensoryChange.
	 */
	public Boolean getSensorychange() {
		try{
			if (_Sensorychange==null){
				_Sensorychange=getBooleanProperty("sensoryChange");
				return _Sensorychange;
			}else {
				return _Sensorychange;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sensoryChange.
	 * @param v Value to Set.
	 */
	public void setSensorychange(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/sensoryChange",v);
		_Sensorychange=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Severeheadache=null;

	/**
	 * @return Returns the severeHeadache.
	 */
	public Boolean getSevereheadache() {
		try{
			if (_Severeheadache==null){
				_Severeheadache=getBooleanProperty("severeHeadache");
				return _Severeheadache;
			}else {
				return _Severeheadache;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for severeHeadache.
	 * @param v Value to Set.
	 */
	public void setSevereheadache(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/severeHeadache",v);
		_Severeheadache=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Nausea=null;

	/**
	 * @return Returns the nausea.
	 */
	public Boolean getNausea() {
		try{
			if (_Nausea==null){
				_Nausea=getBooleanProperty("nausea");
				return _Nausea;
			}else {
				return _Nausea;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for nausea.
	 * @param v Value to Set.
	 */
	public void setNausea(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/nausea",v);
		_Nausea=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Visualloss=null;

	/**
	 * @return Returns the visualLoss.
	 */
	public Boolean getVisualloss() {
		try{
			if (_Visualloss==null){
				_Visualloss=getBooleanProperty("visualLoss");
				return _Visualloss;
			}else {
				return _Visualloss;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for visualLoss.
	 * @param v Value to Set.
	 */
	public void setVisualloss(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/visualLoss",v);
		_Visualloss=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Cognitivedecline=null;

	/**
	 * @return Returns the cognitiveDecline.
	 */
	public Boolean getCognitivedecline() {
		try{
			if (_Cognitivedecline==null){
				_Cognitivedecline=getBooleanProperty("cognitiveDecline");
				return _Cognitivedecline;
			}else {
				return _Cognitivedecline;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cognitiveDecline.
	 * @param v Value to Set.
	 */
	public void setCognitivedecline(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/cognitiveDecline",v);
		_Cognitivedecline=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Seizure=null;

	/**
	 * @return Returns the seizure.
	 */
	public Boolean getSeizure() {
		try{
			if (_Seizure==null){
				_Seizure=getBooleanProperty("seizure");
				return _Seizure;
			}else {
				return _Seizure;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for seizure.
	 * @param v Value to Set.
	 */
	public void setSeizure(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/seizure",v);
		_Seizure=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.SfDeficitsOtherdeficit> _Otherdeficits_otherdeficit =null;

	/**
	 * otherDeficits/otherDeficit
	 * @return Returns an List of org.nrg.xdat.om.SfDeficitsOtherdeficit
	 */
	public <A extends org.nrg.xdat.model.SfDeficitsOtherdeficitI> List<A> getOtherdeficits_otherdeficit() {
		try{
			if (_Otherdeficits_otherdeficit==null){
				_Otherdeficits_otherdeficit=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("otherDeficits/otherDeficit"));
			}
			return (List<A>) _Otherdeficits_otherdeficit;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.SfDeficitsOtherdeficit>();}
	}

	/**
	 * Sets the value for otherDeficits/otherDeficit.
	 * @param v Value to Set.
	 */
	public void setOtherdeficits_otherdeficit(ItemI v) throws Exception{
		_Otherdeficits_otherdeficit =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/otherDeficits/otherDeficit",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/otherDeficits/otherDeficit",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * otherDeficits/otherDeficit
	 * Adds org.nrg.xdat.model.SfDeficitsOtherdeficitI
	 */
	public <A extends org.nrg.xdat.model.SfDeficitsOtherdeficitI> void addOtherdeficits_otherdeficit(A item) throws Exception{
	setOtherdeficits_otherdeficit((ItemI)item);
	}

	/**
	 * Removes the otherDeficits/otherDeficit of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeOtherdeficits_otherdeficit(int index) throws java.lang.IndexOutOfBoundsException {
		_Otherdeficits_otherdeficit =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/otherDeficits/otherDeficit",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _SfDeficitsId=null;

	/**
	 * @return Returns the sf_deficits_id.
	 */
	public Integer getSfDeficitsId() {
		try{
			if (_SfDeficitsId==null){
				_SfDeficitsId=getIntegerProperty("sf_deficits_id");
				return _SfDeficitsId;
			}else {
				return _SfDeficitsId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sf_deficits_id.
	 * @param v Value to Set.
	 */
	public void setSfDeficitsId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sf_deficits_id",v);
		_SfDeficitsId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.SfDeficits> getAllSfDeficitss(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfDeficits> al = new ArrayList<org.nrg.xdat.om.SfDeficits>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.SfDeficits> getSfDeficitssByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfDeficits> al = new ArrayList<org.nrg.xdat.om.SfDeficits>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.SfDeficits> getSfDeficitssByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfDeficits> al = new ArrayList<org.nrg.xdat.om.SfDeficits>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static SfDeficits getSfDeficitssBySfDeficitsId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("sf:deficits/sf_deficits_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (SfDeficits) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //otherDeficits/otherDeficit
	        for(org.nrg.xdat.model.SfDeficitsOtherdeficitI childOtherdeficits_otherdeficit : this.getOtherdeficits_otherdeficit()){
	            if (childOtherdeficits_otherdeficit!=null){
	              for(ResourceFile rf: ((SfDeficitsOtherdeficit)childOtherdeficits_otherdeficit).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("otherDeficits/otherDeficit[" + ((SfDeficitsOtherdeficit)childOtherdeficits_otherdeficit).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("otherDeficits/otherDeficit/" + ((SfDeficitsOtherdeficit)childOtherdeficits_otherdeficit).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
