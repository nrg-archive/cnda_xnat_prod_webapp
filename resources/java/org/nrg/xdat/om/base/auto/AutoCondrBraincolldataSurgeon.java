/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCondrBraincolldataSurgeon extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.CondrBraincolldataSurgeonI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCondrBraincolldataSurgeon.class);
	public static String SCHEMA_ELEMENT_NAME="condr:brainCollData_surgeon";

	public AutoCondrBraincolldataSurgeon(ItemI item)
	{
		super(item);
	}

	public AutoCondrBraincolldataSurgeon(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCondrBraincolldataSurgeon(UserI user)
	 **/
	public AutoCondrBraincolldataSurgeon(){}

	public AutoCondrBraincolldataSurgeon(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "condr:brainCollData_surgeon";
	}

	//FIELD

	private String _Surgeon=null;

	/**
	 * @return Returns the surgeon.
	 */
	public String getSurgeon(){
		try{
			if (_Surgeon==null){
				_Surgeon=getStringProperty("surgeon");
				return _Surgeon;
			}else {
				return _Surgeon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for surgeon.
	 * @param v Value to Set.
	 */
	public void setSurgeon(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/surgeon",v);
		_Surgeon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CondrBraincolldataSurgeonId=null;

	/**
	 * @return Returns the condr_brainCollData_surgeon_id.
	 */
	public Integer getCondrBraincolldataSurgeonId() {
		try{
			if (_CondrBraincolldataSurgeonId==null){
				_CondrBraincolldataSurgeonId=getIntegerProperty("condr_brainCollData_surgeon_id");
				return _CondrBraincolldataSurgeonId;
			}else {
				return _CondrBraincolldataSurgeonId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for condr_brainCollData_surgeon_id.
	 * @param v Value to Set.
	 */
	public void setCondrBraincolldataSurgeonId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/condr_brainCollData_surgeon_id",v);
		_CondrBraincolldataSurgeonId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CondrBraincolldataSurgeon> getAllCondrBraincolldataSurgeons(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrBraincolldataSurgeon> al = new ArrayList<org.nrg.xdat.om.CondrBraincolldataSurgeon>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrBraincolldataSurgeon> getCondrBraincolldataSurgeonsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrBraincolldataSurgeon> al = new ArrayList<org.nrg.xdat.om.CondrBraincolldataSurgeon>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrBraincolldataSurgeon> getCondrBraincolldataSurgeonsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrBraincolldataSurgeon> al = new ArrayList<org.nrg.xdat.om.CondrBraincolldataSurgeon>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CondrBraincolldataSurgeon getCondrBraincolldataSurgeonsByCondrBraincolldataSurgeonId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("condr:brainCollData_surgeon/condr_brainCollData_surgeon_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CondrBraincolldataSurgeon) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
