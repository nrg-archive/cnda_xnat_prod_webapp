/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoMayoMayomrqcscandata extends XnatQcscandata implements org.nrg.xdat.model.MayoMayomrqcscandataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoMayoMayomrqcscandata.class);
	public static String SCHEMA_ELEMENT_NAME="mayo:mayoMrQcScanData";

	public AutoMayoMayomrqcscandata(ItemI item)
	{
		super(item);
	}

	public AutoMayoMayomrqcscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoMayoMayomrqcscandata(UserI user)
	 **/
	public AutoMayoMayomrqcscandata(){}

	public AutoMayoMayomrqcscandata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "mayo:mayoMrQcScanData";
	}
	 private org.nrg.xdat.om.XnatQcscandata _Qcscandata =null;

	/**
	 * qcScanData
	 * @return org.nrg.xdat.om.XnatQcscandata
	 */
	public org.nrg.xdat.om.XnatQcscandata getQcscandata() {
		try{
			if (_Qcscandata==null){
				_Qcscandata=((XnatQcscandata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("qcScanData")));
				return _Qcscandata;
			}else {
				return _Qcscandata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for qcScanData.
	 * @param v Value to Set.
	 */
	public void setQcscandata(ItemI v) throws Exception{
		_Qcscandata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/qcScanData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/qcScanData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * qcScanData
	 * set org.nrg.xdat.model.XnatQcscandataI
	 */
	public <A extends org.nrg.xdat.model.XnatQcscandataI> void setQcscandata(A item) throws Exception{
	setQcscandata((ItemI)item);
	}

	/**
	 * Removes the qcScanData.
	 * */
	public void removeQcscandata() {
		_Qcscandata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/qcScanData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scanuid=null;

	/**
	 * @return Returns the scanUid.
	 */
	public String getScanuid(){
		try{
			if (_Scanuid==null){
				_Scanuid=getStringProperty("scanUid");
				return _Scanuid;
			}else {
				return _Scanuid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for scanUid.
	 * @param v Value to Set.
	 */
	public void setScanuid(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/scanUid",v);
		_Scanuid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scantype=null;

	/**
	 * @return Returns the scanType.
	 */
	public String getScantype(){
		try{
			if (_Scantype==null){
				_Scantype=getStringProperty("scanType");
				return _Scantype;
			}else {
				return _Scantype;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for scanType.
	 * @param v Value to Set.
	 */
	public void setScantype(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/scanType",v);
		_Scantype=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Protocol=null;

	/**
	 * @return Returns the protocol.
	 */
	public Integer getProtocol() {
		try{
			if (_Protocol==null){
				_Protocol=getIntegerProperty("protocol");
				return _Protocol;
			}else {
				return _Protocol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for protocol.
	 * @param v Value to Set.
	 */
	public void setProtocol(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/protocol",v);
		_Protocol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Protocolcomment=null;

	/**
	 * @return Returns the protocolComment.
	 */
	public String getProtocolcomment(){
		try{
			if (_Protocolcomment==null){
				_Protocolcomment=getStringProperty("protocolComment");
				return _Protocolcomment;
			}else {
				return _Protocolcomment;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for protocolComment.
	 * @param v Value to Set.
	 */
	public void setProtocolcomment(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/protocolComment",v);
		_Protocolcomment=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Chosen=null;

	/**
	 * @return Returns the chosen.
	 */
	public Boolean getChosen() {
		try{
			if (_Chosen==null){
				_Chosen=getBooleanProperty("chosen");
				return _Chosen;
			}else {
				return _Chosen;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for chosen.
	 * @param v Value to Set.
	 */
	public void setChosen(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/chosen",v);
		_Chosen=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.MayoMayomrqcscandata> getAllMayoMayomrqcscandatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.MayoMayomrqcscandata> al = new ArrayList<org.nrg.xdat.om.MayoMayomrqcscandata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.MayoMayomrqcscandata> getMayoMayomrqcscandatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.MayoMayomrqcscandata> al = new ArrayList<org.nrg.xdat.om.MayoMayomrqcscandata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.MayoMayomrqcscandata> getMayoMayomrqcscandatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.MayoMayomrqcscandata> al = new ArrayList<org.nrg.xdat.om.MayoMayomrqcscandata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static MayoMayomrqcscandata getMayoMayomrqcscandatasByXnatQcscandataId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("mayo:mayoMrQcScanData/xnat_qcscandata_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (MayoMayomrqcscandata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //qcScanData
	        XnatQcscandata childQcscandata = (XnatQcscandata)this.getQcscandata();
	            if (childQcscandata!=null){
	              for(ResourceFile rf: ((XnatQcscandata)childQcscandata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("qcScanData[" + ((XnatQcscandata)childQcscandata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("qcScanData/" + ((XnatQcscandata)childQcscandata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
