/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCondrBraincolldata extends TissueTisscolldata implements org.nrg.xdat.model.CondrBraincolldataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCondrBraincolldata.class);
	public static String SCHEMA_ELEMENT_NAME="condr:brainCollData";

	public AutoCondrBraincolldata(ItemI item)
	{
		super(item);
	}

	public AutoCondrBraincolldata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCondrBraincolldata(UserI user)
	 **/
	public AutoCondrBraincolldata(){}

	public AutoCondrBraincolldata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "condr:brainCollData";
	}
	 private org.nrg.xdat.om.TissueTisscolldata _Tisscolldata =null;

	/**
	 * tissCollData
	 * @return org.nrg.xdat.om.TissueTisscolldata
	 */
	public org.nrg.xdat.om.TissueTisscolldata getTisscolldata() {
		try{
			if (_Tisscolldata==null){
				_Tisscolldata=((TissueTisscolldata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("tissCollData")));
				return _Tisscolldata;
			}else {
				return _Tisscolldata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for tissCollData.
	 * @param v Value to Set.
	 */
	public void setTisscolldata(ItemI v) throws Exception{
		_Tisscolldata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/tissCollData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/tissCollData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * tissCollData
	 * set org.nrg.xdat.model.TissueTisscolldataI
	 */
	public <A extends org.nrg.xdat.model.TissueTisscolldataI> void setTisscolldata(A item) throws Exception{
	setTisscolldata((ItemI)item);
	}

	/**
	 * Removes the tissCollData.
	 * */
	public void removeTisscolldata() {
		_Tisscolldata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/tissCollData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.CondrBraincolldataTumorloc> _Tumorlocs_tumorloc =null;

	/**
	 * tumorLocs/tumorLoc
	 * @return Returns an List of org.nrg.xdat.om.CondrBraincolldataTumorloc
	 */
	public <A extends org.nrg.xdat.model.CondrBraincolldataTumorlocI> List<A> getTumorlocs_tumorloc() {
		try{
			if (_Tumorlocs_tumorloc==null){
				_Tumorlocs_tumorloc=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("tumorLocs/tumorLoc"));
			}
			return (List<A>) _Tumorlocs_tumorloc;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.CondrBraincolldataTumorloc>();}
	}

	/**
	 * Sets the value for tumorLocs/tumorLoc.
	 * @param v Value to Set.
	 */
	public void setTumorlocs_tumorloc(ItemI v) throws Exception{
		_Tumorlocs_tumorloc =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/tumorLocs/tumorLoc",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/tumorLocs/tumorLoc",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * tumorLocs/tumorLoc
	 * Adds org.nrg.xdat.model.CondrBraincolldataTumorlocI
	 */
	public <A extends org.nrg.xdat.model.CondrBraincolldataTumorlocI> void addTumorlocs_tumorloc(A item) throws Exception{
	setTumorlocs_tumorloc((ItemI)item);
	}

	/**
	 * Removes the tumorLocs/tumorLoc of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeTumorlocs_tumorloc(int index) throws java.lang.IndexOutOfBoundsException {
		_Tumorlocs_tumorloc =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/tumorLocs/tumorLoc",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Tumorhemisphere=null;

	/**
	 * @return Returns the tumorHemisphere.
	 */
	public String getTumorhemisphere(){
		try{
			if (_Tumorhemisphere==null){
				_Tumorhemisphere=getStringProperty("tumorHemisphere");
				return _Tumorhemisphere;
			}else {
				return _Tumorhemisphere;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tumorHemisphere.
	 * @param v Value to Set.
	 */
	public void setTumorhemisphere(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tumorHemisphere",v);
		_Tumorhemisphere=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.CondrBraincolldataSurgicalext> _Surgicalexts_surgicalext =null;

	/**
	 * surgicalExts/surgicalExt
	 * @return Returns an List of org.nrg.xdat.om.CondrBraincolldataSurgicalext
	 */
	public <A extends org.nrg.xdat.model.CondrBraincolldataSurgicalextI> List<A> getSurgicalexts_surgicalext() {
		try{
			if (_Surgicalexts_surgicalext==null){
				_Surgicalexts_surgicalext=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("surgicalExts/surgicalExt"));
			}
			return (List<A>) _Surgicalexts_surgicalext;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.CondrBraincolldataSurgicalext>();}
	}

	/**
	 * Sets the value for surgicalExts/surgicalExt.
	 * @param v Value to Set.
	 */
	public void setSurgicalexts_surgicalext(ItemI v) throws Exception{
		_Surgicalexts_surgicalext =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/surgicalExts/surgicalExt",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/surgicalExts/surgicalExt",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * surgicalExts/surgicalExt
	 * Adds org.nrg.xdat.model.CondrBraincolldataSurgicalextI
	 */
	public <A extends org.nrg.xdat.model.CondrBraincolldataSurgicalextI> void addSurgicalexts_surgicalext(A item) throws Exception{
	setSurgicalexts_surgicalext((ItemI)item);
	}

	/**
	 * Removes the surgicalExts/surgicalExt of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeSurgicalexts_surgicalext(int index) throws java.lang.IndexOutOfBoundsException {
		_Surgicalexts_surgicalext =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/surgicalExts/surgicalExt",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Gliadelused=null;

	/**
	 * @return Returns the gliadelUsed.
	 */
	public Boolean getGliadelused() {
		try{
			if (_Gliadelused==null){
				_Gliadelused=getBooleanProperty("gliadelUsed");
				return _Gliadelused;
			}else {
				return _Gliadelused;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for gliadelUsed.
	 * @param v Value to Set.
	 */
	public void setGliadelused(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/gliadelUsed",v);
		_Gliadelused=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.CondrBraincolldataSurgeon> _Surgeons_surgeon =null;

	/**
	 * surgeons/surgeon
	 * @return Returns an List of org.nrg.xdat.om.CondrBraincolldataSurgeon
	 */
	public <A extends org.nrg.xdat.model.CondrBraincolldataSurgeonI> List<A> getSurgeons_surgeon() {
		try{
			if (_Surgeons_surgeon==null){
				_Surgeons_surgeon=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("surgeons/surgeon"));
			}
			return (List<A>) _Surgeons_surgeon;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.CondrBraincolldataSurgeon>();}
	}

	/**
	 * Sets the value for surgeons/surgeon.
	 * @param v Value to Set.
	 */
	public void setSurgeons_surgeon(ItemI v) throws Exception{
		_Surgeons_surgeon =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/surgeons/surgeon",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/surgeons/surgeon",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * surgeons/surgeon
	 * Adds org.nrg.xdat.model.CondrBraincolldataSurgeonI
	 */
	public <A extends org.nrg.xdat.model.CondrBraincolldataSurgeonI> void addSurgeons_surgeon(A item) throws Exception{
	setSurgeons_surgeon((ItemI)item);
	}

	/**
	 * Removes the surgeons/surgeon of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeSurgeons_surgeon(int index) throws java.lang.IndexOutOfBoundsException {
		_Surgeons_surgeon =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/surgeons/surgeon",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Bloodcoll=null;

	/**
	 * @return Returns the bloodColl.
	 */
	public Boolean getBloodcoll() {
		try{
			if (_Bloodcoll==null){
				_Bloodcoll=getBooleanProperty("bloodColl");
				return _Bloodcoll;
			}else {
				return _Bloodcoll;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for bloodColl.
	 * @param v Value to Set.
	 */
	public void setBloodcoll(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/bloodColl",v);
		_Bloodcoll=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Redtop=null;

	/**
	 * @return Returns the redTop.
	 */
	public Boolean getRedtop() {
		try{
			if (_Redtop==null){
				_Redtop=getBooleanProperty("redTop");
				return _Redtop;
			}else {
				return _Redtop;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for redTop.
	 * @param v Value to Set.
	 */
	public void setRedtop(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/redTop",v);
		_Redtop=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Purpletop=null;

	/**
	 * @return Returns the purpleTop.
	 */
	public Boolean getPurpletop() {
		try{
			if (_Purpletop==null){
				_Purpletop=getBooleanProperty("purpleTop");
				return _Purpletop;
			}else {
				return _Purpletop;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for purpleTop.
	 * @param v Value to Set.
	 */
	public void setPurpletop(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/purpleTop",v);
		_Purpletop=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Senttolab=null;

	/**
	 * @return Returns the sentToLab.
	 */
	public Boolean getSenttolab() {
		try{
			if (_Senttolab==null){
				_Senttolab=getBooleanProperty("sentToLab");
				return _Senttolab;
			}else {
				return _Senttolab;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sentToLab.
	 * @param v Value to Set.
	 */
	public void setSenttolab(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/sentToLab",v);
		_Senttolab=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Senttolabdetails=null;

	/**
	 * @return Returns the sentToLabDetails.
	 */
	public String getSenttolabdetails(){
		try{
			if (_Senttolabdetails==null){
				_Senttolabdetails=getStringProperty("sentToLabDetails");
				return _Senttolabdetails;
			}else {
				return _Senttolabdetails;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sentToLabDetails.
	 * @param v Value to Set.
	 */
	public void setSenttolabdetails(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sentToLabDetails",v);
		_Senttolabdetails=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Complications=null;

	/**
	 * @return Returns the complications.
	 */
	public Boolean getComplications() {
		try{
			if (_Complications==null){
				_Complications=getBooleanProperty("complications");
				return _Complications;
			}else {
				return _Complications;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for complications.
	 * @param v Value to Set.
	 */
	public void setComplications(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/complications",v);
		_Complications=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Complicationsdetails=null;

	/**
	 * @return Returns the complicationsDetails.
	 */
	public String getComplicationsdetails(){
		try{
			if (_Complicationsdetails==null){
				_Complicationsdetails=getStringProperty("complicationsDetails");
				return _Complicationsdetails;
			}else {
				return _Complicationsdetails;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for complicationsDetails.
	 * @param v Value to Set.
	 */
	public void setComplicationsdetails(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/complicationsDetails",v);
		_Complicationsdetails=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CondrBraincolldata> getAllCondrBraincolldatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrBraincolldata> al = new ArrayList<org.nrg.xdat.om.CondrBraincolldata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrBraincolldata> getCondrBraincolldatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrBraincolldata> al = new ArrayList<org.nrg.xdat.om.CondrBraincolldata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrBraincolldata> getCondrBraincolldatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrBraincolldata> al = new ArrayList<org.nrg.xdat.om.CondrBraincolldata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CondrBraincolldata getCondrBraincolldatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("condr:brainCollData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CondrBraincolldata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //tissCollData
	        TissueTisscolldata childTisscolldata = (TissueTisscolldata)this.getTisscolldata();
	            if (childTisscolldata!=null){
	              for(ResourceFile rf: ((TissueTisscolldata)childTisscolldata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("tissCollData[" + ((TissueTisscolldata)childTisscolldata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("tissCollData/" + ((TissueTisscolldata)childTisscolldata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //tumorLocs/tumorLoc
	        for(org.nrg.xdat.model.CondrBraincolldataTumorlocI childTumorlocs_tumorloc : this.getTumorlocs_tumorloc()){
	            if (childTumorlocs_tumorloc!=null){
	              for(ResourceFile rf: ((CondrBraincolldataTumorloc)childTumorlocs_tumorloc).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("tumorLocs/tumorLoc[" + ((CondrBraincolldataTumorloc)childTumorlocs_tumorloc).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("tumorLocs/tumorLoc/" + ((CondrBraincolldataTumorloc)childTumorlocs_tumorloc).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	        //surgicalExts/surgicalExt
	        for(org.nrg.xdat.model.CondrBraincolldataSurgicalextI childSurgicalexts_surgicalext : this.getSurgicalexts_surgicalext()){
	            if (childSurgicalexts_surgicalext!=null){
	              for(ResourceFile rf: ((CondrBraincolldataSurgicalext)childSurgicalexts_surgicalext).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("surgicalExts/surgicalExt[" + ((CondrBraincolldataSurgicalext)childSurgicalexts_surgicalext).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("surgicalExts/surgicalExt/" + ((CondrBraincolldataSurgicalext)childSurgicalexts_surgicalext).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	        //surgeons/surgeon
	        for(org.nrg.xdat.model.CondrBraincolldataSurgeonI childSurgeons_surgeon : this.getSurgeons_surgeon()){
	            if (childSurgeons_surgeon!=null){
	              for(ResourceFile rf: ((CondrBraincolldataSurgeon)childSurgeons_surgeon).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("surgeons/surgeon[" + ((CondrBraincolldataSurgeon)childSurgeons_surgeon).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("surgeons/surgeon/" + ((CondrBraincolldataSurgeon)childSurgeons_surgeon).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
