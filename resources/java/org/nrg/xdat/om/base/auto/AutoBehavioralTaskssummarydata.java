/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoBehavioralTaskssummarydata extends XnatMrassessordata implements org.nrg.xdat.model.BehavioralTaskssummarydataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoBehavioralTaskssummarydata.class);
	public static String SCHEMA_ELEMENT_NAME="behavioral:tasksSummaryData";

	public AutoBehavioralTaskssummarydata(ItemI item)
	{
		super(item);
	}

	public AutoBehavioralTaskssummarydata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoBehavioralTaskssummarydata(UserI user)
	 **/
	public AutoBehavioralTaskssummarydata(){}

	public AutoBehavioralTaskssummarydata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "behavioral:tasksSummaryData";
	}
	 private org.nrg.xdat.om.XnatMrassessordata _Mrassessordata =null;

	/**
	 * mrAssessorData
	 * @return org.nrg.xdat.om.XnatMrassessordata
	 */
	public org.nrg.xdat.om.XnatMrassessordata getMrassessordata() {
		try{
			if (_Mrassessordata==null){
				_Mrassessordata=((XnatMrassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("mrAssessorData")));
				return _Mrassessordata;
			}else {
				return _Mrassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for mrAssessorData.
	 * @param v Value to Set.
	 */
	public void setMrassessordata(ItemI v) throws Exception{
		_Mrassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/mrAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/mrAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * mrAssessorData
	 * set org.nrg.xdat.model.XnatMrassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatMrassessordataI> void setMrassessordata(A item) throws Exception{
	setMrassessordata((ItemI)item);
	}

	/**
	 * Removes the mrAssessorData.
	 * */
	public void removeMrassessordata() {
		_Mrassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/mrAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydataTask> _Tasks_task =null;

	/**
	 * Tasks/task
	 * @return Returns an List of org.nrg.xdat.om.BehavioralTaskssummarydataTask
	 */
	public <A extends org.nrg.xdat.model.BehavioralTaskssummarydataTaskI> List<A> getTasks_task() {
		try{
			if (_Tasks_task==null){
				_Tasks_task=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("Tasks/task"));
			}
			return (List<A>) _Tasks_task;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydataTask>();}
	}

	/**
	 * Sets the value for Tasks/task.
	 * @param v Value to Set.
	 */
	public void setTasks_task(ItemI v) throws Exception{
		_Tasks_task =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/Tasks/task",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/Tasks/task",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * Tasks/task
	 * Adds org.nrg.xdat.model.BehavioralTaskssummarydataTaskI
	 */
	public <A extends org.nrg.xdat.model.BehavioralTaskssummarydataTaskI> void addTasks_task(A item) throws Exception{
	setTasks_task((ItemI)item);
	}

	/**
	 * Removes the Tasks/task of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeTasks_task(int index) throws java.lang.IndexOutOfBoundsException {
		_Tasks_task =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/Tasks/task",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.BehavioralStatistics _Tasks_statisticsacrosstasks =null;

	/**
	 * Tasks/StatisticsAcrossTasks
	 * @return org.nrg.xdat.om.BehavioralStatistics
	 */
	public org.nrg.xdat.om.BehavioralStatistics getTasks_statisticsacrosstasks() {
		try{
			if (_Tasks_statisticsacrosstasks==null){
				_Tasks_statisticsacrosstasks=((BehavioralStatistics)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("Tasks/StatisticsAcrossTasks")));
				return _Tasks_statisticsacrosstasks;
			}else {
				return _Tasks_statisticsacrosstasks;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for Tasks/StatisticsAcrossTasks.
	 * @param v Value to Set.
	 */
	public void setTasks_statisticsacrosstasks(ItemI v) throws Exception{
		_Tasks_statisticsacrosstasks =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/Tasks/StatisticsAcrossTasks",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/Tasks/StatisticsAcrossTasks",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * Tasks/StatisticsAcrossTasks
	 * set org.nrg.xdat.model.BehavioralStatisticsI
	 */
	public <A extends org.nrg.xdat.model.BehavioralStatisticsI> void setTasks_statisticsacrosstasks(A item) throws Exception{
	setTasks_statisticsacrosstasks((ItemI)item);
	}

	/**
	 * Removes the Tasks/StatisticsAcrossTasks.
	 * */
	public void removeTasks_statisticsacrosstasks() {
		_Tasks_statisticsacrosstasks =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/Tasks/StatisticsAcrossTasks",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tasks_statisticsacrosstasksFK=null;

	/**
	 * @return Returns the behavioral:tasksSummaryData/tasks_statisticsacrosstasks_behavioral_statistics_id.
	 */
	public Integer getTasks_statisticsacrosstasksFK(){
		try{
			if (_Tasks_statisticsacrosstasksFK==null){
				_Tasks_statisticsacrosstasksFK=getIntegerProperty("behavioral:tasksSummaryData/tasks_statisticsacrosstasks_behavioral_statistics_id");
				return _Tasks_statisticsacrosstasksFK;
			}else {
				return _Tasks_statisticsacrosstasksFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for behavioral:tasksSummaryData/tasks_statisticsacrosstasks_behavioral_statistics_id.
	 * @param v Value to Set.
	 */
	public void setTasks_statisticsacrosstasksFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tasks_statisticsacrosstasks_behavioral_statistics_id",v);
		_Tasks_statisticsacrosstasksFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.BehavioralStatistics _Tasks_statisticsacrossruns =null;

	/**
	 * Tasks/StatisticsAcrossRuns
	 * @return org.nrg.xdat.om.BehavioralStatistics
	 */
	public org.nrg.xdat.om.BehavioralStatistics getTasks_statisticsacrossruns() {
		try{
			if (_Tasks_statisticsacrossruns==null){
				_Tasks_statisticsacrossruns=((BehavioralStatistics)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("Tasks/StatisticsAcrossRuns")));
				return _Tasks_statisticsacrossruns;
			}else {
				return _Tasks_statisticsacrossruns;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for Tasks/StatisticsAcrossRuns.
	 * @param v Value to Set.
	 */
	public void setTasks_statisticsacrossruns(ItemI v) throws Exception{
		_Tasks_statisticsacrossruns =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/Tasks/StatisticsAcrossRuns",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/Tasks/StatisticsAcrossRuns",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * Tasks/StatisticsAcrossRuns
	 * set org.nrg.xdat.model.BehavioralStatisticsI
	 */
	public <A extends org.nrg.xdat.model.BehavioralStatisticsI> void setTasks_statisticsacrossruns(A item) throws Exception{
	setTasks_statisticsacrossruns((ItemI)item);
	}

	/**
	 * Removes the Tasks/StatisticsAcrossRuns.
	 * */
	public void removeTasks_statisticsacrossruns() {
		_Tasks_statisticsacrossruns =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/Tasks/StatisticsAcrossRuns",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tasks_statisticsacrossrunsFK=null;

	/**
	 * @return Returns the behavioral:tasksSummaryData/tasks_statisticsacrossruns_behavioral_statistics_id.
	 */
	public Integer getTasks_statisticsacrossrunsFK(){
		try{
			if (_Tasks_statisticsacrossrunsFK==null){
				_Tasks_statisticsacrossrunsFK=getIntegerProperty("behavioral:tasksSummaryData/tasks_statisticsacrossruns_behavioral_statistics_id");
				return _Tasks_statisticsacrossrunsFK;
			}else {
				return _Tasks_statisticsacrossrunsFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for behavioral:tasksSummaryData/tasks_statisticsacrossruns_behavioral_statistics_id.
	 * @param v Value to Set.
	 */
	public void setTasks_statisticsacrossrunsFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tasks_statisticsacrossruns_behavioral_statistics_id",v);
		_Tasks_statisticsacrossrunsFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _TaskType=null;

	/**
	 * @return Returns the task_type.
	 */
	public String getTaskType(){
		try{
			if (_TaskType==null){
				_TaskType=getStringProperty("task_type");
				return _TaskType;
			}else {
				return _TaskType;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for task_type.
	 * @param v Value to Set.
	 */
	public void setTaskType(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/task_type",v);
		_TaskType=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydata> getAllBehavioralTaskssummarydatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydata> al = new ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydata> getBehavioralTaskssummarydatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydata> al = new ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydata> getBehavioralTaskssummarydatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydata> al = new ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static BehavioralTaskssummarydata getBehavioralTaskssummarydatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("behavioral:tasksSummaryData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (BehavioralTaskssummarydata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		XFTItem mr = org.nrg.xft.search.ItemSearch.GetItem("xnat:mrSessionData.ID",getItem().getProperty("behavioral:tasksSummaryData.imageSession_ID"),getItem().getUser(),false);
		al.add(mr);
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",mr.getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //mrAssessorData
	        XnatMrassessordata childMrassessordata = (XnatMrassessordata)this.getMrassessordata();
	            if (childMrassessordata!=null){
	              for(ResourceFile rf: ((XnatMrassessordata)childMrassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("mrAssessorData[" + ((XnatMrassessordata)childMrassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("mrAssessorData/" + ((XnatMrassessordata)childMrassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //Tasks/task
	        for(org.nrg.xdat.model.BehavioralTaskssummarydataTaskI childTasks_task : this.getTasks_task()){
	            if (childTasks_task!=null){
	              for(ResourceFile rf: ((BehavioralTaskssummarydataTask)childTasks_task).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("Tasks/task[" + ((BehavioralTaskssummarydataTask)childTasks_task).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("Tasks/task/" + ((BehavioralTaskssummarydataTask)childTasks_task).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	        //Tasks/StatisticsAcrossTasks
	        BehavioralStatistics childTasks_statisticsacrosstasks = (BehavioralStatistics)this.getTasks_statisticsacrosstasks();
	            if (childTasks_statisticsacrosstasks!=null){
	              for(ResourceFile rf: ((BehavioralStatistics)childTasks_statisticsacrosstasks).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("Tasks/StatisticsAcrossTasks[" + ((BehavioralStatistics)childTasks_statisticsacrosstasks).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("Tasks/StatisticsAcrossTasks/" + ((BehavioralStatistics)childTasks_statisticsacrosstasks).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //Tasks/StatisticsAcrossRuns
	        BehavioralStatistics childTasks_statisticsacrossruns = (BehavioralStatistics)this.getTasks_statisticsacrossruns();
	            if (childTasks_statisticsacrossruns!=null){
	              for(ResourceFile rf: ((BehavioralStatistics)childTasks_statisticsacrossruns).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("Tasks/StatisticsAcrossRuns[" + ((BehavioralStatistics)childTasks_statisticsacrossruns).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("Tasks/StatisticsAcrossRuns/" + ((BehavioralStatistics)childTasks_statisticsacrossruns).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
