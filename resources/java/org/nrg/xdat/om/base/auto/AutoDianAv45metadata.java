/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:01 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianAv45metadata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianAv45metadataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianAv45metadata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:av45metaData";

	public AutoDianAv45metadata(ItemI item)
	{
		super(item);
	}

	public AutoDianAv45metadata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianAv45metadata(UserI user)
	 **/
	public AutoDianAv45metadata(){}

	public AutoDianAv45metadata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:av45metaData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pmcondct=null;

	/**
	 * @return Returns the PMCONDCT.
	 */
	public Integer getPmcondct() {
		try{
			if (_Pmcondct==null){
				_Pmcondct=getIntegerProperty("PMCONDCT");
				return _Pmcondct;
			}else {
				return _Pmcondct;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PMCONDCT.
	 * @param v Value to Set.
	 */
	public void setPmcondct(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PMCONDCT",v);
		_Pmcondct=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Scandate=null;

	/**
	 * @return Returns the SCANDATE.
	 */
	public Object getScandate(){
		try{
			if (_Scandate==null){
				_Scandate=getProperty("SCANDATE");
				return _Scandate;
			}else {
				return _Scandate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SCANDATE.
	 * @param v Value to Set.
	 */
	public void setScandate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SCANDATE",v);
		_Scandate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Cndadate=null;

	/**
	 * @return Returns the CNDADATE.
	 */
	public Object getCndadate(){
		try{
			if (_Cndadate==null){
				_Cndadate=getProperty("CNDADATE");
				return _Cndadate;
			}else {
				return _Cndadate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CNDADATE.
	 * @param v Value to Set.
	 */
	public void setCndadate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CNDADATE",v);
		_Cndadate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Pmreason=null;

	/**
	 * @return Returns the PMREASON.
	 */
	public String getPmreason(){
		try{
			if (_Pmreason==null){
				_Pmreason=getStringProperty("PMREASON");
				return _Pmreason;
			}else {
				return _Pmreason;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PMREASON.
	 * @param v Value to Set.
	 */
	public void setPmreason(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PMREASON",v);
		_Pmreason=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Pmothspe=null;

	/**
	 * @return Returns the PMOTHSPE.
	 */
	public String getPmothspe(){
		try{
			if (_Pmothspe==null){
				_Pmothspe=getStringProperty("PMOTHSPE");
				return _Pmothspe;
			}else {
				return _Pmothspe;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PMOTHSPE.
	 * @param v Value to Set.
	 */
	public void setPmothspe(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PMOTHSPE",v);
		_Pmothspe=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianAv45metadata> getAllDianAv45metadatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianAv45metadata> al = new ArrayList<org.nrg.xdat.om.DianAv45metadata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianAv45metadata> getDianAv45metadatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianAv45metadata> al = new ArrayList<org.nrg.xdat.om.DianAv45metadata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianAv45metadata> getDianAv45metadatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianAv45metadata> al = new ArrayList<org.nrg.xdat.om.DianAv45metadata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianAv45metadata getDianAv45metadatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:av45metaData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianAv45metadata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
