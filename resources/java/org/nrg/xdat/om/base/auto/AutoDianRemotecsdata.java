/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:00 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianRemotecsdata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianRemotecsdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianRemotecsdata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:remotecsData";

	public AutoDianRemotecsdata(ItemI item)
	{
		super(item);
	}

	public AutoDianRemotecsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianRemotecsdata(UserI user)
	 **/
	public AutoDianRemotecsdata(){}

	public AutoDianRemotecsdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:remotecsData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ad8mem=null;

	/**
	 * @return Returns the AD8MEM.
	 */
	public Integer getAd8mem() {
		try{
			if (_Ad8mem==null){
				_Ad8mem=getIntegerProperty("AD8MEM");
				return _Ad8mem;
			}else {
				return _Ad8mem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AD8MEM.
	 * @param v Value to Set.
	 */
	public void setAd8mem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AD8MEM",v);
		_Ad8mem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ad8jud=null;

	/**
	 * @return Returns the AD8JUD.
	 */
	public Integer getAd8jud() {
		try{
			if (_Ad8jud==null){
				_Ad8jud=getIntegerProperty("AD8JUD");
				return _Ad8jud;
			}else {
				return _Ad8jud;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AD8JUD.
	 * @param v Value to Set.
	 */
	public void setAd8jud(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AD8JUD",v);
		_Ad8jud=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ad8hob=null;

	/**
	 * @return Returns the AD8HOB.
	 */
	public Integer getAd8hob() {
		try{
			if (_Ad8hob==null){
				_Ad8hob=getIntegerProperty("AD8HOB");
				return _Ad8hob;
			}else {
				return _Ad8hob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AD8HOB.
	 * @param v Value to Set.
	 */
	public void setAd8hob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AD8HOB",v);
		_Ad8hob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ad8rep=null;

	/**
	 * @return Returns the AD8REP.
	 */
	public Integer getAd8rep() {
		try{
			if (_Ad8rep==null){
				_Ad8rep=getIntegerProperty("AD8REP");
				return _Ad8rep;
			}else {
				return _Ad8rep;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AD8REP.
	 * @param v Value to Set.
	 */
	public void setAd8rep(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AD8REP",v);
		_Ad8rep=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ad8tool=null;

	/**
	 * @return Returns the AD8TOOL.
	 */
	public Integer getAd8tool() {
		try{
			if (_Ad8tool==null){
				_Ad8tool=getIntegerProperty("AD8TOOL");
				return _Ad8tool;
			}else {
				return _Ad8tool;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AD8TOOL.
	 * @param v Value to Set.
	 */
	public void setAd8tool(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AD8TOOL",v);
		_Ad8tool=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ad8for=null;

	/**
	 * @return Returns the AD8FOR.
	 */
	public Integer getAd8for() {
		try{
			if (_Ad8for==null){
				_Ad8for=getIntegerProperty("AD8FOR");
				return _Ad8for;
			}else {
				return _Ad8for;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AD8FOR.
	 * @param v Value to Set.
	 */
	public void setAd8for(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AD8FOR",v);
		_Ad8for=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ad8fin=null;

	/**
	 * @return Returns the AD8FIN.
	 */
	public Integer getAd8fin() {
		try{
			if (_Ad8fin==null){
				_Ad8fin=getIntegerProperty("AD8FIN");
				return _Ad8fin;
			}else {
				return _Ad8fin;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AD8FIN.
	 * @param v Value to Set.
	 */
	public void setAd8fin(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AD8FIN",v);
		_Ad8fin=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ad8appt=null;

	/**
	 * @return Returns the AD8APPT.
	 */
	public Integer getAd8appt() {
		try{
			if (_Ad8appt==null){
				_Ad8appt=getIntegerProperty("AD8APPT");
				return _Ad8appt;
			}else {
				return _Ad8appt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AD8APPT.
	 * @param v Value to Set.
	 */
	public void setAd8appt(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AD8APPT",v);
		_Ad8appt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Famhist=null;

	/**
	 * @return Returns the FAMHIST.
	 */
	public Integer getFamhist() {
		try{
			if (_Famhist==null){
				_Famhist=getIntegerProperty("FAMHIST");
				return _Famhist;
			}else {
				return _Famhist;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for FAMHIST.
	 * @param v Value to Set.
	 */
	public void setFamhist(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/FAMHIST",v);
		_Famhist=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Medcond=null;

	/**
	 * @return Returns the MEDCOND.
	 */
	public Integer getMedcond() {
		try{
			if (_Medcond==null){
				_Medcond=getIntegerProperty("MEDCOND");
				return _Medcond;
			}else {
				return _Medcond;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MEDCOND.
	 * @param v Value to Set.
	 */
	public void setMedcond(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MEDCOND",v);
		_Medcond=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Medspec=null;

	/**
	 * @return Returns the MEDSPEC.
	 */
	public String getMedspec(){
		try{
			if (_Medspec==null){
				_Medspec=getStringProperty("MEDSPEC");
				return _Medspec;
			}else {
				return _Medspec;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MEDSPEC.
	 * @param v Value to Set.
	 */
	public void setMedspec(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MEDSPEC",v);
		_Medspec=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Memory=null;

	/**
	 * @return Returns the MEMORY.
	 */
	public Integer getMemory() {
		try{
			if (_Memory==null){
				_Memory=getIntegerProperty("MEMORY");
				return _Memory;
			}else {
				return _Memory;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MEMORY.
	 * @param v Value to Set.
	 */
	public void setMemory(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MEMORY",v);
		_Memory=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Adl=null;

	/**
	 * @return Returns the ADL.
	 */
	public Integer getAdl() {
		try{
			if (_Adl==null){
				_Adl=getIntegerProperty("ADL");
				return _Adl;
			}else {
				return _Adl;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ADL.
	 * @param v Value to Set.
	 */
	public void setAdl(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ADL",v);
		_Adl=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianRemotecsdata> getAllDianRemotecsdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianRemotecsdata> al = new ArrayList<org.nrg.xdat.om.DianRemotecsdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianRemotecsdata> getDianRemotecsdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianRemotecsdata> al = new ArrayList<org.nrg.xdat.om.DianRemotecsdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianRemotecsdata> getDianRemotecsdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianRemotecsdata> al = new ArrayList<org.nrg.xdat.om.DianRemotecsdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianRemotecsdata getDianRemotecsdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:remotecsData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianRemotecsdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
