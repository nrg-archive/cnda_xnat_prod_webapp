/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCondrCondrmedicalhistory extends SfMedicalhistory implements org.nrg.xdat.model.CondrCondrmedicalhistoryI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCondrCondrmedicalhistory.class);
	public static String SCHEMA_ELEMENT_NAME="condr:condrMedicalHistory";

	public AutoCondrCondrmedicalhistory(ItemI item)
	{
		super(item);
	}

	public AutoCondrCondrmedicalhistory(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCondrCondrmedicalhistory(UserI user)
	 **/
	public AutoCondrCondrmedicalhistory(){}

	public AutoCondrCondrmedicalhistory(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "condr:condrMedicalHistory";
	}
	 private org.nrg.xdat.om.SfMedicalhistory _Medicalhistory =null;

	/**
	 * medicalHistory
	 * @return org.nrg.xdat.om.SfMedicalhistory
	 */
	public org.nrg.xdat.om.SfMedicalhistory getMedicalhistory() {
		try{
			if (_Medicalhistory==null){
				_Medicalhistory=((SfMedicalhistory)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("medicalHistory")));
				return _Medicalhistory;
			}else {
				return _Medicalhistory;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for medicalHistory.
	 * @param v Value to Set.
	 */
	public void setMedicalhistory(ItemI v) throws Exception{
		_Medicalhistory =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/medicalHistory",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/medicalHistory",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * medicalHistory
	 * set org.nrg.xdat.model.SfMedicalhistoryI
	 */
	public <A extends org.nrg.xdat.model.SfMedicalhistoryI> void setMedicalhistory(A item) throws Exception{
	setMedicalhistory((ItemI)item);
	}

	/**
	 * Removes the medicalHistory.
	 * */
	public void removeMedicalhistory() {
		_Medicalhistory =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/medicalHistory",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Tumorpredisposition=null;

	/**
	 * @return Returns the tumorPredisposition.
	 */
	public String getTumorpredisposition(){
		try{
			if (_Tumorpredisposition==null){
				_Tumorpredisposition=getStringProperty("tumorPredisposition");
				return _Tumorpredisposition;
			}else {
				return _Tumorpredisposition;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tumorPredisposition.
	 * @param v Value to Set.
	 */
	public void setTumorpredisposition(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tumorPredisposition",v);
		_Tumorpredisposition=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Familytumorsyndrome=null;

	/**
	 * @return Returns the familyTumorSyndrome.
	 */
	public Boolean getFamilytumorsyndrome() {
		try{
			if (_Familytumorsyndrome==null){
				_Familytumorsyndrome=getBooleanProperty("familyTumorSyndrome");
				return _Familytumorsyndrome;
			}else {
				return _Familytumorsyndrome;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for familyTumorSyndrome.
	 * @param v Value to Set.
	 */
	public void setFamilytumorsyndrome(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/familyTumorSyndrome",v);
		_Familytumorsyndrome=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Familytumorspecifictype=null;

	/**
	 * @return Returns the familyTumorSpecificType.
	 */
	public Boolean getFamilytumorspecifictype() {
		try{
			if (_Familytumorspecifictype==null){
				_Familytumorspecifictype=getBooleanProperty("familyTumorSpecificType");
				return _Familytumorspecifictype;
			}else {
				return _Familytumorspecifictype;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for familyTumorSpecificType.
	 * @param v Value to Set.
	 */
	public void setFamilytumorspecifictype(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/familyTumorSpecificType",v);
		_Familytumorspecifictype=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Familytumornotes=null;

	/**
	 * @return Returns the familyTumorNotes.
	 */
	public String getFamilytumornotes(){
		try{
			if (_Familytumornotes==null){
				_Familytumornotes=getStringProperty("familyTumorNotes");
				return _Familytumornotes;
			}else {
				return _Familytumornotes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for familyTumorNotes.
	 * @param v Value to Set.
	 */
	public void setFamilytumornotes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/familyTumorNotes",v);
		_Familytumornotes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CondrCondrmedicalhistory> getAllCondrCondrmedicalhistorys(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrCondrmedicalhistory> al = new ArrayList<org.nrg.xdat.om.CondrCondrmedicalhistory>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrCondrmedicalhistory> getCondrCondrmedicalhistorysByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrCondrmedicalhistory> al = new ArrayList<org.nrg.xdat.om.CondrCondrmedicalhistory>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrCondrmedicalhistory> getCondrCondrmedicalhistorysByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrCondrmedicalhistory> al = new ArrayList<org.nrg.xdat.om.CondrCondrmedicalhistory>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CondrCondrmedicalhistory getCondrCondrmedicalhistorysById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("condr:condrMedicalHistory/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CondrCondrmedicalhistory) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //medicalHistory
	        SfMedicalhistory childMedicalhistory = (SfMedicalhistory)this.getMedicalhistory();
	            if (childMedicalhistory!=null){
	              for(ResourceFile rf: ((SfMedicalhistory)childMedicalhistory).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("medicalHistory[" + ((SfMedicalhistory)childMedicalhistory).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("medicalHistory/" + ((SfMedicalhistory)childMedicalhistory).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
