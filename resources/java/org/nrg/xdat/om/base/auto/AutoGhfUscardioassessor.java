/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:00 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoGhfUscardioassessor extends XnatImageassessordata implements org.nrg.xdat.model.GhfUscardioassessorI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoGhfUscardioassessor.class);
	public static String SCHEMA_ELEMENT_NAME="ghf:usCardioAssessor";

	public AutoGhfUscardioassessor(ItemI item)
	{
		super(item);
	}

	public AutoGhfUscardioassessor(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoGhfUscardioassessor(UserI user)
	 **/
	public AutoGhfUscardioassessor(){}

	public AutoGhfUscardioassessor(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "ghf:usCardioAssessor";
	}
	 private org.nrg.xdat.om.XnatImageassessordata _Imageassessordata =null;

	/**
	 * imageAssessorData
	 * @return org.nrg.xdat.om.XnatImageassessordata
	 */
	public org.nrg.xdat.om.XnatImageassessordata getImageassessordata() {
		try{
			if (_Imageassessordata==null){
				_Imageassessordata=((XnatImageassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("imageAssessorData")));
				return _Imageassessordata;
			}else {
				return _Imageassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for imageAssessorData.
	 * @param v Value to Set.
	 */
	public void setImageassessordata(ItemI v) throws Exception{
		_Imageassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/imageAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/imageAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * imageAssessorData
	 * set org.nrg.xdat.model.XnatImageassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatImageassessordataI> void setImageassessordata(A item) throws Exception{
	setImageassessordata((ItemI)item);
	}

	/**
	 * Removes the imageAssessorData.
	 * */
	public void removeImageassessordata() {
		_Imageassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/imageAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Seqstudynumber=null;

	/**
	 * @return Returns the seqStudyNumber.
	 */
	public String getSeqstudynumber(){
		try{
			if (_Seqstudynumber==null){
				_Seqstudynumber=getStringProperty("seqStudyNumber");
				return _Seqstudynumber;
			}else {
				return _Seqstudynumber;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for seqStudyNumber.
	 * @param v Value to Set.
	 */
	public void setSeqstudynumber(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/seqStudyNumber",v);
		_Seqstudynumber=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Analyzableecho=null;

	/**
	 * @return Returns the analyzableEcho.
	 */
	public Boolean getAnalyzableecho() {
		try{
			if (_Analyzableecho==null){
				_Analyzableecho=getBooleanProperty("analyzableEcho");
				return _Analyzableecho;
			}else {
				return _Analyzableecho;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for analyzableEcho.
	 * @param v Value to Set.
	 */
	public void setAnalyzableecho(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/analyzableEcho",v);
		_Analyzableecho=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Image2dquality=null;

	/**
	 * @return Returns the image2dQuality.
	 */
	public String getImage2dquality(){
		try{
			if (_Image2dquality==null){
				_Image2dquality=getStringProperty("image2dQuality");
				return _Image2dquality;
			}else {
				return _Image2dquality;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for image2dQuality.
	 * @param v Value to Set.
	 */
	public void setImage2dquality(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/image2dQuality",v);
		_Image2dquality=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Imagedpquality=null;

	/**
	 * @return Returns the imageDpQuality.
	 */
	public String getImagedpquality(){
		try{
			if (_Imagedpquality==null){
				_Imagedpquality=getStringProperty("imageDpQuality");
				return _Imagedpquality;
			}else {
				return _Imagedpquality;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for imageDpQuality.
	 * @param v Value to Set.
	 */
	public void setImagedpquality(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/imageDpQuality",v);
		_Imagedpquality=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Dominantechorhythm=null;

	/**
	 * @return Returns the dominantEchoRhythm.
	 */
	public String getDominantechorhythm(){
		try{
			if (_Dominantechorhythm==null){
				_Dominantechorhythm=getStringProperty("dominantEchoRhythm");
				return _Dominantechorhythm;
			}else {
				return _Dominantechorhythm;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for dominantEchoRhythm.
	 * @param v Value to Set.
	 */
	public void setDominantechorhythm(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/dominantEchoRhythm",v);
		_Dominantechorhythm=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Heartrate=null;

	/**
	 * @return Returns the heartrate.
	 */
	public Integer getHeartrate() {
		try{
			if (_Heartrate==null){
				_Heartrate=getIntegerProperty("heartrate");
				return _Heartrate;
			}else {
				return _Heartrate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for heartrate.
	 * @param v Value to Set.
	 */
	public void setHeartrate(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/heartrate",v);
		_Heartrate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Lvotd1=null;

	/**
	 * @return Returns the lvotD1.
	 */
	public Double getLvotd1() {
		try{
			if (_Lvotd1==null){
				_Lvotd1=getDoubleProperty("lvotD1");
				return _Lvotd1;
			}else {
				return _Lvotd1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for lvotD1.
	 * @param v Value to Set.
	 */
	public void setLvotd1(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/lvotD1",v);
		_Lvotd1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Lvidd=null;

	/**
	 * @return Returns the lvidd.
	 */
	public Integer getLvidd() {
		try{
			if (_Lvidd==null){
				_Lvidd=getIntegerProperty("lvidd");
				return _Lvidd;
			}else {
				return _Lvidd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for lvidd.
	 * @param v Value to Set.
	 */
	public void setLvidd(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/lvidd",v);
		_Lvidd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ivs=null;

	/**
	 * @return Returns the ivs.
	 */
	public Integer getIvs() {
		try{
			if (_Ivs==null){
				_Ivs=getIntegerProperty("ivs");
				return _Ivs;
			}else {
				return _Ivs;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ivs.
	 * @param v Value to Set.
	 */
	public void setIvs(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ivs",v);
		_Ivs=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pw=null;

	/**
	 * @return Returns the pw.
	 */
	public Integer getPw() {
		try{
			if (_Pw==null){
				_Pw=getIntegerProperty("pw");
				return _Pw;
			}else {
				return _Pw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for pw.
	 * @param v Value to Set.
	 */
	public void setPw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/pw",v);
		_Pw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Alwt=null;

	/**
	 * @return Returns the alwt.
	 */
	public Integer getAlwt() {
		try{
			if (_Alwt==null){
				_Alwt=getIntegerProperty("alwt");
				return _Alwt;
			}else {
				return _Alwt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for alwt.
	 * @param v Value to Set.
	 */
	public void setAlwt(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/alwt",v);
		_Alwt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Lvids=null;

	/**
	 * @return Returns the lvids.
	 */
	public Integer getLvids() {
		try{
			if (_Lvids==null){
				_Lvids=getIntegerProperty("lvids");
				return _Lvids;
			}else {
				return _Lvids;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for lvids.
	 * @param v Value to Set.
	 */
	public void setLvids(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/lvids",v);
		_Lvids=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Lvedv=null;

	/**
	 * @return Returns the lvedv.
	 */
	public Integer getLvedv() {
		try{
			if (_Lvedv==null){
				_Lvedv=getIntegerProperty("lvedv");
				return _Lvedv;
			}else {
				return _Lvedv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for lvedv.
	 * @param v Value to Set.
	 */
	public void setLvedv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/lvedv",v);
		_Lvedv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Lvesv=null;

	/**
	 * @return Returns the lvesv.
	 */
	public Integer getLvesv() {
		try{
			if (_Lvesv==null){
				_Lvesv=getIntegerProperty("lvesv");
				return _Lvesv;
			}else {
				return _Lvesv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for lvesv.
	 * @param v Value to Set.
	 */
	public void setLvesv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/lvesv",v);
		_Lvesv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _V1=null;

	/**
	 * @return Returns the v1.
	 */
	public Integer getV1() {
		try{
			if (_V1==null){
				_V1=getIntegerProperty("v1");
				return _V1;
			}else {
				return _V1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for v1.
	 * @param v Value to Set.
	 */
	public void setV1(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/v1",v);
		_V1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Lvottvi=null;

	/**
	 * @return Returns the lvotTvi.
	 */
	public Double getLvottvi() {
		try{
			if (_Lvottvi==null){
				_Lvottvi=getDoubleProperty("lvotTvi");
				return _Lvottvi;
			}else {
				return _Lvottvi;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for lvotTvi.
	 * @param v Value to Set.
	 */
	public void setLvottvi(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/lvotTvi",v);
		_Lvottvi=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Valvstenosis_aortic=null;

	/**
	 * @return Returns the valvStenosis/aortic.
	 */
	public String getValvstenosis_aortic(){
		try{
			if (_Valvstenosis_aortic==null){
				_Valvstenosis_aortic=getStringProperty("valvStenosis/aortic");
				return _Valvstenosis_aortic;
			}else {
				return _Valvstenosis_aortic;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for valvStenosis/aortic.
	 * @param v Value to Set.
	 */
	public void setValvstenosis_aortic(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/valvStenosis/aortic",v);
		_Valvstenosis_aortic=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Valvstenosis_mitral=null;

	/**
	 * @return Returns the valvStenosis/mitral.
	 */
	public String getValvstenosis_mitral(){
		try{
			if (_Valvstenosis_mitral==null){
				_Valvstenosis_mitral=getStringProperty("valvStenosis/mitral");
				return _Valvstenosis_mitral;
			}else {
				return _Valvstenosis_mitral;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for valvStenosis/mitral.
	 * @param v Value to Set.
	 */
	public void setValvstenosis_mitral(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/valvStenosis/mitral",v);
		_Valvstenosis_mitral=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Valvstenosis_tricuspid=null;

	/**
	 * @return Returns the valvStenosis/tricuspid.
	 */
	public String getValvstenosis_tricuspid(){
		try{
			if (_Valvstenosis_tricuspid==null){
				_Valvstenosis_tricuspid=getStringProperty("valvStenosis/tricuspid");
				return _Valvstenosis_tricuspid;
			}else {
				return _Valvstenosis_tricuspid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for valvStenosis/tricuspid.
	 * @param v Value to Set.
	 */
	public void setValvstenosis_tricuspid(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/valvStenosis/tricuspid",v);
		_Valvstenosis_tricuspid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Valvstenosis_pulmonic=null;

	/**
	 * @return Returns the valvStenosis/pulmonic.
	 */
	public String getValvstenosis_pulmonic(){
		try{
			if (_Valvstenosis_pulmonic==null){
				_Valvstenosis_pulmonic=getStringProperty("valvStenosis/pulmonic");
				return _Valvstenosis_pulmonic;
			}else {
				return _Valvstenosis_pulmonic;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for valvStenosis/pulmonic.
	 * @param v Value to Set.
	 */
	public void setValvstenosis_pulmonic(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/valvStenosis/pulmonic",v);
		_Valvstenosis_pulmonic=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Valvregurg_aortic=null;

	/**
	 * @return Returns the valvRegurg/aortic.
	 */
	public String getValvregurg_aortic(){
		try{
			if (_Valvregurg_aortic==null){
				_Valvregurg_aortic=getStringProperty("valvRegurg/aortic");
				return _Valvregurg_aortic;
			}else {
				return _Valvregurg_aortic;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for valvRegurg/aortic.
	 * @param v Value to Set.
	 */
	public void setValvregurg_aortic(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/valvRegurg/aortic",v);
		_Valvregurg_aortic=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Valvregurg_mitral=null;

	/**
	 * @return Returns the valvRegurg/mitral.
	 */
	public String getValvregurg_mitral(){
		try{
			if (_Valvregurg_mitral==null){
				_Valvregurg_mitral=getStringProperty("valvRegurg/mitral");
				return _Valvregurg_mitral;
			}else {
				return _Valvregurg_mitral;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for valvRegurg/mitral.
	 * @param v Value to Set.
	 */
	public void setValvregurg_mitral(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/valvRegurg/mitral",v);
		_Valvregurg_mitral=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Valvregurg_tricuspid=null;

	/**
	 * @return Returns the valvRegurg/tricuspid.
	 */
	public String getValvregurg_tricuspid(){
		try{
			if (_Valvregurg_tricuspid==null){
				_Valvregurg_tricuspid=getStringProperty("valvRegurg/tricuspid");
				return _Valvregurg_tricuspid;
			}else {
				return _Valvregurg_tricuspid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for valvRegurg/tricuspid.
	 * @param v Value to Set.
	 */
	public void setValvregurg_tricuspid(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/valvRegurg/tricuspid",v);
		_Valvregurg_tricuspid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Valvregurg_pulmonic=null;

	/**
	 * @return Returns the valvRegurg/pulmonic.
	 */
	public String getValvregurg_pulmonic(){
		try{
			if (_Valvregurg_pulmonic==null){
				_Valvregurg_pulmonic=getStringProperty("valvRegurg/pulmonic");
				return _Valvregurg_pulmonic;
			}else {
				return _Valvregurg_pulmonic;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for valvRegurg/pulmonic.
	 * @param v Value to Set.
	 */
	public void setValvregurg_pulmonic(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/valvRegurg/pulmonic",v);
		_Valvregurg_pulmonic=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Tr=null;

	/**
	 * @return Returns the tr.
	 */
	public Double getTr() {
		try{
			if (_Tr==null){
				_Tr=getDoubleProperty("tr");
				return _Tr;
			}else {
				return _Tr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tr.
	 * @param v Value to Set.
	 */
	public void setTr(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tr",v);
		_Tr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _E=null;

	/**
	 * @return Returns the e.
	 */
	public Integer getE() {
		try{
			if (_E==null){
				_E=getIntegerProperty("e");
				return _E;
			}else {
				return _E;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for e.
	 * @param v Value to Set.
	 */
	public void setE(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/e",v);
		_E=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _A=null;

	/**
	 * @return Returns the a.
	 */
	public Integer getA() {
		try{
			if (_A==null){
				_A=getIntegerProperty("a");
				return _A;
			}else {
				return _A;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for a.
	 * @param v Value to Set.
	 */
	public void setA(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/a",v);
		_A=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mitraldt=null;

	/**
	 * @return Returns the mitralDt.
	 */
	public Integer getMitraldt() {
		try{
			if (_Mitraldt==null){
				_Mitraldt=getIntegerProperty("mitralDt");
				return _Mitraldt;
			}else {
				return _Mitraldt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for mitralDt.
	 * @param v Value to Set.
	 */
	public void setMitraldt(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/mitralDt",v);
		_Mitraldt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Eml=null;

	/**
	 * @return Returns the eml.
	 */
	public Double getEml() {
		try{
			if (_Eml==null){
				_Eml=getDoubleProperty("eml");
				return _Eml;
			}else {
				return _Eml;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for eml.
	 * @param v Value to Set.
	 */
	public void setEml(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/eml",v);
		_Eml=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Ems=null;

	/**
	 * @return Returns the ems.
	 */
	public Double getEms() {
		try{
			if (_Ems==null){
				_Ems=getDoubleProperty("ems");
				return _Ems;
			}else {
				return _Ems;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ems.
	 * @param v Value to Set.
	 */
	public void setEms(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ems",v);
		_Ems=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Primarysig=null;

	/**
	 * @return Returns the primarySig.
	 */
	public String getPrimarysig(){
		try{
			if (_Primarysig==null){
				_Primarysig=getStringProperty("primarySig");
				return _Primarysig;
			}else {
				return _Primarysig;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for primarySig.
	 * @param v Value to Set.
	 */
	public void setPrimarysig(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/primarySig",v);
		_Primarysig=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Secondarysig=null;

	/**
	 * @return Returns the secondarySig.
	 */
	public String getSecondarysig(){
		try{
			if (_Secondarysig==null){
				_Secondarysig=getStringProperty("secondarySig");
				return _Secondarysig;
			}else {
				return _Secondarysig;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for secondarySig.
	 * @param v Value to Set.
	 */
	public void setSecondarysig(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/secondarySig",v);
		_Secondarysig=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.GhfUscardioassessor> getAllGhfUscardioassessors(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.GhfUscardioassessor> al = new ArrayList<org.nrg.xdat.om.GhfUscardioassessor>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.GhfUscardioassessor> getGhfUscardioassessorsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.GhfUscardioassessor> al = new ArrayList<org.nrg.xdat.om.GhfUscardioassessor>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.GhfUscardioassessor> getGhfUscardioassessorsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.GhfUscardioassessor> al = new ArrayList<org.nrg.xdat.om.GhfUscardioassessor>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static GhfUscardioassessor getGhfUscardioassessorsById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("ghf:usCardioAssessor/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (GhfUscardioassessor) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //imageAssessorData
	        XnatImageassessordata childImageassessordata = (XnatImageassessordata)this.getImageassessordata();
	            if (childImageassessordata!=null){
	              for(ResourceFile rf: ((XnatImageassessordata)childImageassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("imageAssessorData[" + ((XnatImageassessordata)childImageassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("imageAssessorData/" + ((XnatImageassessordata)childImageassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
