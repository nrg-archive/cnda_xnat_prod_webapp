/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCbatCategorization extends XnatSubjectassessordata implements org.nrg.xdat.model.CbatCategorizationI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCbatCategorization.class);
	public static String SCHEMA_ELEMENT_NAME="cbat:categorization";

	public AutoCbatCategorization(ItemI item)
	{
		super(item);
	}

	public AutoCbatCategorization(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCbatCategorization(UserI user)
	 **/
	public AutoCbatCategorization(){}

	public AutoCbatCategorization(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cbat:categorization";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest1_accuracy=null;

	/**
	 * @return Returns the YesT1/accuracy.
	 */
	public Integer getYest1_accuracy() {
		try{
			if (_Yest1_accuracy==null){
				_Yest1_accuracy=getIntegerProperty("YesT1/accuracy");
				return _Yest1_accuracy;
			}else {
				return _Yest1_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT1/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest1_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT1/accuracy",v);
		_Yest1_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest1_responsetime=null;

	/**
	 * @return Returns the YesT1/responseTime.
	 */
	public Integer getYest1_responsetime() {
		try{
			if (_Yest1_responsetime==null){
				_Yest1_responsetime=getIntegerProperty("YesT1/responseTime");
				return _Yest1_responsetime;
			}else {
				return _Yest1_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT1/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest1_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT1/responseTime",v);
		_Yest1_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest2_accuracy=null;

	/**
	 * @return Returns the YesT2/accuracy.
	 */
	public Integer getYest2_accuracy() {
		try{
			if (_Yest2_accuracy==null){
				_Yest2_accuracy=getIntegerProperty("YesT2/accuracy");
				return _Yest2_accuracy;
			}else {
				return _Yest2_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT2/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest2_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT2/accuracy",v);
		_Yest2_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest2_responsetime=null;

	/**
	 * @return Returns the YesT2/responseTime.
	 */
	public Integer getYest2_responsetime() {
		try{
			if (_Yest2_responsetime==null){
				_Yest2_responsetime=getIntegerProperty("YesT2/responseTime");
				return _Yest2_responsetime;
			}else {
				return _Yest2_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT2/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest2_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT2/responseTime",v);
		_Yest2_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest3_accuracy=null;

	/**
	 * @return Returns the YesT3/accuracy.
	 */
	public Integer getYest3_accuracy() {
		try{
			if (_Yest3_accuracy==null){
				_Yest3_accuracy=getIntegerProperty("YesT3/accuracy");
				return _Yest3_accuracy;
			}else {
				return _Yest3_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT3/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest3_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT3/accuracy",v);
		_Yest3_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest3_responsetime=null;

	/**
	 * @return Returns the YesT3/responseTime.
	 */
	public Integer getYest3_responsetime() {
		try{
			if (_Yest3_responsetime==null){
				_Yest3_responsetime=getIntegerProperty("YesT3/responseTime");
				return _Yest3_responsetime;
			}else {
				return _Yest3_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT3/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest3_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT3/responseTime",v);
		_Yest3_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest4_accuracy=null;

	/**
	 * @return Returns the YesT4/accuracy.
	 */
	public Integer getYest4_accuracy() {
		try{
			if (_Yest4_accuracy==null){
				_Yest4_accuracy=getIntegerProperty("YesT4/accuracy");
				return _Yest4_accuracy;
			}else {
				return _Yest4_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT4/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest4_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT4/accuracy",v);
		_Yest4_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest4_responsetime=null;

	/**
	 * @return Returns the YesT4/responseTime.
	 */
	public Integer getYest4_responsetime() {
		try{
			if (_Yest4_responsetime==null){
				_Yest4_responsetime=getIntegerProperty("YesT4/responseTime");
				return _Yest4_responsetime;
			}else {
				return _Yest4_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT4/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest4_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT4/responseTime",v);
		_Yest4_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest5_accuracy=null;

	/**
	 * @return Returns the YesT5/accuracy.
	 */
	public Integer getYest5_accuracy() {
		try{
			if (_Yest5_accuracy==null){
				_Yest5_accuracy=getIntegerProperty("YesT5/accuracy");
				return _Yest5_accuracy;
			}else {
				return _Yest5_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT5/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest5_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT5/accuracy",v);
		_Yest5_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest5_responsetime=null;

	/**
	 * @return Returns the YesT5/responseTime.
	 */
	public Integer getYest5_responsetime() {
		try{
			if (_Yest5_responsetime==null){
				_Yest5_responsetime=getIntegerProperty("YesT5/responseTime");
				return _Yest5_responsetime;
			}else {
				return _Yest5_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT5/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest5_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT5/responseTime",v);
		_Yest5_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest6_accuracy=null;

	/**
	 * @return Returns the YesT6/accuracy.
	 */
	public Integer getYest6_accuracy() {
		try{
			if (_Yest6_accuracy==null){
				_Yest6_accuracy=getIntegerProperty("YesT6/accuracy");
				return _Yest6_accuracy;
			}else {
				return _Yest6_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT6/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest6_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT6/accuracy",v);
		_Yest6_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest6_responsetime=null;

	/**
	 * @return Returns the YesT6/responseTime.
	 */
	public Integer getYest6_responsetime() {
		try{
			if (_Yest6_responsetime==null){
				_Yest6_responsetime=getIntegerProperty("YesT6/responseTime");
				return _Yest6_responsetime;
			}else {
				return _Yest6_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT6/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest6_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT6/responseTime",v);
		_Yest6_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest7_accuracy=null;

	/**
	 * @return Returns the YesT7/accuracy.
	 */
	public Integer getYest7_accuracy() {
		try{
			if (_Yest7_accuracy==null){
				_Yest7_accuracy=getIntegerProperty("YesT7/accuracy");
				return _Yest7_accuracy;
			}else {
				return _Yest7_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT7/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest7_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT7/accuracy",v);
		_Yest7_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest7_responsetime=null;

	/**
	 * @return Returns the YesT7/responseTime.
	 */
	public Integer getYest7_responsetime() {
		try{
			if (_Yest7_responsetime==null){
				_Yest7_responsetime=getIntegerProperty("YesT7/responseTime");
				return _Yest7_responsetime;
			}else {
				return _Yest7_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT7/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest7_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT7/responseTime",v);
		_Yest7_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest8_accuracy=null;

	/**
	 * @return Returns the YesT8/accuracy.
	 */
	public Integer getYest8_accuracy() {
		try{
			if (_Yest8_accuracy==null){
				_Yest8_accuracy=getIntegerProperty("YesT8/accuracy");
				return _Yest8_accuracy;
			}else {
				return _Yest8_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT8/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest8_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT8/accuracy",v);
		_Yest8_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest8_responsetime=null;

	/**
	 * @return Returns the YesT8/responseTime.
	 */
	public Integer getYest8_responsetime() {
		try{
			if (_Yest8_responsetime==null){
				_Yest8_responsetime=getIntegerProperty("YesT8/responseTime");
				return _Yest8_responsetime;
			}else {
				return _Yest8_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT8/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest8_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT8/responseTime",v);
		_Yest8_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest9_accuracy=null;

	/**
	 * @return Returns the YesT9/accuracy.
	 */
	public Integer getYest9_accuracy() {
		try{
			if (_Yest9_accuracy==null){
				_Yest9_accuracy=getIntegerProperty("YesT9/accuracy");
				return _Yest9_accuracy;
			}else {
				return _Yest9_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT9/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest9_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT9/accuracy",v);
		_Yest9_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest9_responsetime=null;

	/**
	 * @return Returns the YesT9/responseTime.
	 */
	public Integer getYest9_responsetime() {
		try{
			if (_Yest9_responsetime==null){
				_Yest9_responsetime=getIntegerProperty("YesT9/responseTime");
				return _Yest9_responsetime;
			}else {
				return _Yest9_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT9/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest9_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT9/responseTime",v);
		_Yest9_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest10_accuracy=null;

	/**
	 * @return Returns the YesT10/accuracy.
	 */
	public Integer getYest10_accuracy() {
		try{
			if (_Yest10_accuracy==null){
				_Yest10_accuracy=getIntegerProperty("YesT10/accuracy");
				return _Yest10_accuracy;
			}else {
				return _Yest10_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT10/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest10_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT10/accuracy",v);
		_Yest10_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest10_responsetime=null;

	/**
	 * @return Returns the YesT10/responseTime.
	 */
	public Integer getYest10_responsetime() {
		try{
			if (_Yest10_responsetime==null){
				_Yest10_responsetime=getIntegerProperty("YesT10/responseTime");
				return _Yest10_responsetime;
			}else {
				return _Yest10_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT10/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest10_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT10/responseTime",v);
		_Yest10_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest11_accuracy=null;

	/**
	 * @return Returns the YesT11/accuracy.
	 */
	public Integer getYest11_accuracy() {
		try{
			if (_Yest11_accuracy==null){
				_Yest11_accuracy=getIntegerProperty("YesT11/accuracy");
				return _Yest11_accuracy;
			}else {
				return _Yest11_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT11/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest11_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT11/accuracy",v);
		_Yest11_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest11_responsetime=null;

	/**
	 * @return Returns the YesT11/responseTime.
	 */
	public Integer getYest11_responsetime() {
		try{
			if (_Yest11_responsetime==null){
				_Yest11_responsetime=getIntegerProperty("YesT11/responseTime");
				return _Yest11_responsetime;
			}else {
				return _Yest11_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT11/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest11_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT11/responseTime",v);
		_Yest11_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest12_accuracy=null;

	/**
	 * @return Returns the YesT12/accuracy.
	 */
	public Integer getYest12_accuracy() {
		try{
			if (_Yest12_accuracy==null){
				_Yest12_accuracy=getIntegerProperty("YesT12/accuracy");
				return _Yest12_accuracy;
			}else {
				return _Yest12_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT12/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest12_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT12/accuracy",v);
		_Yest12_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest12_responsetime=null;

	/**
	 * @return Returns the YesT12/responseTime.
	 */
	public Integer getYest12_responsetime() {
		try{
			if (_Yest12_responsetime==null){
				_Yest12_responsetime=getIntegerProperty("YesT12/responseTime");
				return _Yest12_responsetime;
			}else {
				return _Yest12_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT12/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest12_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT12/responseTime",v);
		_Yest12_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest13_accuracy=null;

	/**
	 * @return Returns the YesT13/accuracy.
	 */
	public Integer getYest13_accuracy() {
		try{
			if (_Yest13_accuracy==null){
				_Yest13_accuracy=getIntegerProperty("YesT13/accuracy");
				return _Yest13_accuracy;
			}else {
				return _Yest13_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT13/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest13_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT13/accuracy",v);
		_Yest13_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest13_responsetime=null;

	/**
	 * @return Returns the YesT13/responseTime.
	 */
	public Integer getYest13_responsetime() {
		try{
			if (_Yest13_responsetime==null){
				_Yest13_responsetime=getIntegerProperty("YesT13/responseTime");
				return _Yest13_responsetime;
			}else {
				return _Yest13_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT13/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest13_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT13/responseTime",v);
		_Yest13_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest14_accuracy=null;

	/**
	 * @return Returns the YesT14/accuracy.
	 */
	public Integer getYest14_accuracy() {
		try{
			if (_Yest14_accuracy==null){
				_Yest14_accuracy=getIntegerProperty("YesT14/accuracy");
				return _Yest14_accuracy;
			}else {
				return _Yest14_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT14/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest14_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT14/accuracy",v);
		_Yest14_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest14_responsetime=null;

	/**
	 * @return Returns the YesT14/responseTime.
	 */
	public Integer getYest14_responsetime() {
		try{
			if (_Yest14_responsetime==null){
				_Yest14_responsetime=getIntegerProperty("YesT14/responseTime");
				return _Yest14_responsetime;
			}else {
				return _Yest14_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT14/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest14_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT14/responseTime",v);
		_Yest14_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest15_accuracy=null;

	/**
	 * @return Returns the YesT15/accuracy.
	 */
	public Integer getYest15_accuracy() {
		try{
			if (_Yest15_accuracy==null){
				_Yest15_accuracy=getIntegerProperty("YesT15/accuracy");
				return _Yest15_accuracy;
			}else {
				return _Yest15_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT15/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest15_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT15/accuracy",v);
		_Yest15_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest15_responsetime=null;

	/**
	 * @return Returns the YesT15/responseTime.
	 */
	public Integer getYest15_responsetime() {
		try{
			if (_Yest15_responsetime==null){
				_Yest15_responsetime=getIntegerProperty("YesT15/responseTime");
				return _Yest15_responsetime;
			}else {
				return _Yest15_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT15/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest15_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT15/responseTime",v);
		_Yest15_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest16_accuracy=null;

	/**
	 * @return Returns the YesT16/accuracy.
	 */
	public Integer getYest16_accuracy() {
		try{
			if (_Yest16_accuracy==null){
				_Yest16_accuracy=getIntegerProperty("YesT16/accuracy");
				return _Yest16_accuracy;
			}else {
				return _Yest16_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT16/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest16_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT16/accuracy",v);
		_Yest16_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest16_responsetime=null;

	/**
	 * @return Returns the YesT16/responseTime.
	 */
	public Integer getYest16_responsetime() {
		try{
			if (_Yest16_responsetime==null){
				_Yest16_responsetime=getIntegerProperty("YesT16/responseTime");
				return _Yest16_responsetime;
			}else {
				return _Yest16_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT16/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest16_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT16/responseTime",v);
		_Yest16_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest17_accuracy=null;

	/**
	 * @return Returns the YesT17/accuracy.
	 */
	public Integer getYest17_accuracy() {
		try{
			if (_Yest17_accuracy==null){
				_Yest17_accuracy=getIntegerProperty("YesT17/accuracy");
				return _Yest17_accuracy;
			}else {
				return _Yest17_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT17/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest17_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT17/accuracy",v);
		_Yest17_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest17_responsetime=null;

	/**
	 * @return Returns the YesT17/responseTime.
	 */
	public Integer getYest17_responsetime() {
		try{
			if (_Yest17_responsetime==null){
				_Yest17_responsetime=getIntegerProperty("YesT17/responseTime");
				return _Yest17_responsetime;
			}else {
				return _Yest17_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT17/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest17_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT17/responseTime",v);
		_Yest17_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest18_accuracy=null;

	/**
	 * @return Returns the YesT18/accuracy.
	 */
	public Integer getYest18_accuracy() {
		try{
			if (_Yest18_accuracy==null){
				_Yest18_accuracy=getIntegerProperty("YesT18/accuracy");
				return _Yest18_accuracy;
			}else {
				return _Yest18_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT18/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest18_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT18/accuracy",v);
		_Yest18_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest18_responsetime=null;

	/**
	 * @return Returns the YesT18/responseTime.
	 */
	public Integer getYest18_responsetime() {
		try{
			if (_Yest18_responsetime==null){
				_Yest18_responsetime=getIntegerProperty("YesT18/responseTime");
				return _Yest18_responsetime;
			}else {
				return _Yest18_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT18/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest18_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT18/responseTime",v);
		_Yest18_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest19_accuracy=null;

	/**
	 * @return Returns the YesT19/accuracy.
	 */
	public Integer getYest19_accuracy() {
		try{
			if (_Yest19_accuracy==null){
				_Yest19_accuracy=getIntegerProperty("YesT19/accuracy");
				return _Yest19_accuracy;
			}else {
				return _Yest19_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT19/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest19_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT19/accuracy",v);
		_Yest19_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest19_responsetime=null;

	/**
	 * @return Returns the YesT19/responseTime.
	 */
	public Integer getYest19_responsetime() {
		try{
			if (_Yest19_responsetime==null){
				_Yest19_responsetime=getIntegerProperty("YesT19/responseTime");
				return _Yest19_responsetime;
			}else {
				return _Yest19_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT19/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest19_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT19/responseTime",v);
		_Yest19_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest20_accuracy=null;

	/**
	 * @return Returns the YesT20/accuracy.
	 */
	public Integer getYest20_accuracy() {
		try{
			if (_Yest20_accuracy==null){
				_Yest20_accuracy=getIntegerProperty("YesT20/accuracy");
				return _Yest20_accuracy;
			}else {
				return _Yest20_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT20/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest20_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT20/accuracy",v);
		_Yest20_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest20_responsetime=null;

	/**
	 * @return Returns the YesT20/responseTime.
	 */
	public Integer getYest20_responsetime() {
		try{
			if (_Yest20_responsetime==null){
				_Yest20_responsetime=getIntegerProperty("YesT20/responseTime");
				return _Yest20_responsetime;
			}else {
				return _Yest20_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT20/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest20_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT20/responseTime",v);
		_Yest20_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest21_accuracy=null;

	/**
	 * @return Returns the YesT21/accuracy.
	 */
	public Integer getYest21_accuracy() {
		try{
			if (_Yest21_accuracy==null){
				_Yest21_accuracy=getIntegerProperty("YesT21/accuracy");
				return _Yest21_accuracy;
			}else {
				return _Yest21_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT21/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest21_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT21/accuracy",v);
		_Yest21_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest21_responsetime=null;

	/**
	 * @return Returns the YesT21/responseTime.
	 */
	public Integer getYest21_responsetime() {
		try{
			if (_Yest21_responsetime==null){
				_Yest21_responsetime=getIntegerProperty("YesT21/responseTime");
				return _Yest21_responsetime;
			}else {
				return _Yest21_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT21/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest21_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT21/responseTime",v);
		_Yest21_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest22_accuracy=null;

	/**
	 * @return Returns the YesT22/accuracy.
	 */
	public Integer getYest22_accuracy() {
		try{
			if (_Yest22_accuracy==null){
				_Yest22_accuracy=getIntegerProperty("YesT22/accuracy");
				return _Yest22_accuracy;
			}else {
				return _Yest22_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT22/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest22_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT22/accuracy",v);
		_Yest22_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest22_responsetime=null;

	/**
	 * @return Returns the YesT22/responseTime.
	 */
	public Integer getYest22_responsetime() {
		try{
			if (_Yest22_responsetime==null){
				_Yest22_responsetime=getIntegerProperty("YesT22/responseTime");
				return _Yest22_responsetime;
			}else {
				return _Yest22_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT22/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest22_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT22/responseTime",v);
		_Yest22_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest23_accuracy=null;

	/**
	 * @return Returns the YesT23/accuracy.
	 */
	public Integer getYest23_accuracy() {
		try{
			if (_Yest23_accuracy==null){
				_Yest23_accuracy=getIntegerProperty("YesT23/accuracy");
				return _Yest23_accuracy;
			}else {
				return _Yest23_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT23/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest23_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT23/accuracy",v);
		_Yest23_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest23_responsetime=null;

	/**
	 * @return Returns the YesT23/responseTime.
	 */
	public Integer getYest23_responsetime() {
		try{
			if (_Yest23_responsetime==null){
				_Yest23_responsetime=getIntegerProperty("YesT23/responseTime");
				return _Yest23_responsetime;
			}else {
				return _Yest23_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT23/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest23_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT23/responseTime",v);
		_Yest23_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest24_accuracy=null;

	/**
	 * @return Returns the YesT24/accuracy.
	 */
	public Integer getYest24_accuracy() {
		try{
			if (_Yest24_accuracy==null){
				_Yest24_accuracy=getIntegerProperty("YesT24/accuracy");
				return _Yest24_accuracy;
			}else {
				return _Yest24_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT24/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest24_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT24/accuracy",v);
		_Yest24_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest24_responsetime=null;

	/**
	 * @return Returns the YesT24/responseTime.
	 */
	public Integer getYest24_responsetime() {
		try{
			if (_Yest24_responsetime==null){
				_Yest24_responsetime=getIntegerProperty("YesT24/responseTime");
				return _Yest24_responsetime;
			}else {
				return _Yest24_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT24/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest24_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT24/responseTime",v);
		_Yest24_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest25_accuracy=null;

	/**
	 * @return Returns the YesT25/accuracy.
	 */
	public Integer getYest25_accuracy() {
		try{
			if (_Yest25_accuracy==null){
				_Yest25_accuracy=getIntegerProperty("YesT25/accuracy");
				return _Yest25_accuracy;
			}else {
				return _Yest25_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT25/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest25_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT25/accuracy",v);
		_Yest25_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest25_responsetime=null;

	/**
	 * @return Returns the YesT25/responseTime.
	 */
	public Integer getYest25_responsetime() {
		try{
			if (_Yest25_responsetime==null){
				_Yest25_responsetime=getIntegerProperty("YesT25/responseTime");
				return _Yest25_responsetime;
			}else {
				return _Yest25_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT25/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest25_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT25/responseTime",v);
		_Yest25_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest26_accuracy=null;

	/**
	 * @return Returns the YesT26/accuracy.
	 */
	public Integer getYest26_accuracy() {
		try{
			if (_Yest26_accuracy==null){
				_Yest26_accuracy=getIntegerProperty("YesT26/accuracy");
				return _Yest26_accuracy;
			}else {
				return _Yest26_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT26/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest26_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT26/accuracy",v);
		_Yest26_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest26_responsetime=null;

	/**
	 * @return Returns the YesT26/responseTime.
	 */
	public Integer getYest26_responsetime() {
		try{
			if (_Yest26_responsetime==null){
				_Yest26_responsetime=getIntegerProperty("YesT26/responseTime");
				return _Yest26_responsetime;
			}else {
				return _Yest26_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT26/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest26_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT26/responseTime",v);
		_Yest26_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest27_accuracy=null;

	/**
	 * @return Returns the YesT27/accuracy.
	 */
	public Integer getYest27_accuracy() {
		try{
			if (_Yest27_accuracy==null){
				_Yest27_accuracy=getIntegerProperty("YesT27/accuracy");
				return _Yest27_accuracy;
			}else {
				return _Yest27_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT27/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest27_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT27/accuracy",v);
		_Yest27_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest27_responsetime=null;

	/**
	 * @return Returns the YesT27/responseTime.
	 */
	public Integer getYest27_responsetime() {
		try{
			if (_Yest27_responsetime==null){
				_Yest27_responsetime=getIntegerProperty("YesT27/responseTime");
				return _Yest27_responsetime;
			}else {
				return _Yest27_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT27/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest27_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT27/responseTime",v);
		_Yest27_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest28_accuracy=null;

	/**
	 * @return Returns the YesT28/accuracy.
	 */
	public Integer getYest28_accuracy() {
		try{
			if (_Yest28_accuracy==null){
				_Yest28_accuracy=getIntegerProperty("YesT28/accuracy");
				return _Yest28_accuracy;
			}else {
				return _Yest28_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT28/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest28_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT28/accuracy",v);
		_Yest28_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest28_responsetime=null;

	/**
	 * @return Returns the YesT28/responseTime.
	 */
	public Integer getYest28_responsetime() {
		try{
			if (_Yest28_responsetime==null){
				_Yest28_responsetime=getIntegerProperty("YesT28/responseTime");
				return _Yest28_responsetime;
			}else {
				return _Yest28_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT28/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest28_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT28/responseTime",v);
		_Yest28_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest29_accuracy=null;

	/**
	 * @return Returns the YesT29/accuracy.
	 */
	public Integer getYest29_accuracy() {
		try{
			if (_Yest29_accuracy==null){
				_Yest29_accuracy=getIntegerProperty("YesT29/accuracy");
				return _Yest29_accuracy;
			}else {
				return _Yest29_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT29/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest29_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT29/accuracy",v);
		_Yest29_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest29_responsetime=null;

	/**
	 * @return Returns the YesT29/responseTime.
	 */
	public Integer getYest29_responsetime() {
		try{
			if (_Yest29_responsetime==null){
				_Yest29_responsetime=getIntegerProperty("YesT29/responseTime");
				return _Yest29_responsetime;
			}else {
				return _Yest29_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT29/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest29_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT29/responseTime",v);
		_Yest29_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest30_accuracy=null;

	/**
	 * @return Returns the YesT30/accuracy.
	 */
	public Integer getYest30_accuracy() {
		try{
			if (_Yest30_accuracy==null){
				_Yest30_accuracy=getIntegerProperty("YesT30/accuracy");
				return _Yest30_accuracy;
			}else {
				return _Yest30_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT30/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest30_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT30/accuracy",v);
		_Yest30_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest30_responsetime=null;

	/**
	 * @return Returns the YesT30/responseTime.
	 */
	public Integer getYest30_responsetime() {
		try{
			if (_Yest30_responsetime==null){
				_Yest30_responsetime=getIntegerProperty("YesT30/responseTime");
				return _Yest30_responsetime;
			}else {
				return _Yest30_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT30/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest30_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT30/responseTime",v);
		_Yest30_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest31_accuracy=null;

	/**
	 * @return Returns the YesT31/accuracy.
	 */
	public Integer getYest31_accuracy() {
		try{
			if (_Yest31_accuracy==null){
				_Yest31_accuracy=getIntegerProperty("YesT31/accuracy");
				return _Yest31_accuracy;
			}else {
				return _Yest31_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT31/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest31_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT31/accuracy",v);
		_Yest31_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest31_responsetime=null;

	/**
	 * @return Returns the YesT31/responseTime.
	 */
	public Integer getYest31_responsetime() {
		try{
			if (_Yest31_responsetime==null){
				_Yest31_responsetime=getIntegerProperty("YesT31/responseTime");
				return _Yest31_responsetime;
			}else {
				return _Yest31_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT31/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest31_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT31/responseTime",v);
		_Yest31_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest32_accuracy=null;

	/**
	 * @return Returns the YesT32/accuracy.
	 */
	public Integer getYest32_accuracy() {
		try{
			if (_Yest32_accuracy==null){
				_Yest32_accuracy=getIntegerProperty("YesT32/accuracy");
				return _Yest32_accuracy;
			}else {
				return _Yest32_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT32/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest32_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT32/accuracy",v);
		_Yest32_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest32_responsetime=null;

	/**
	 * @return Returns the YesT32/responseTime.
	 */
	public Integer getYest32_responsetime() {
		try{
			if (_Yest32_responsetime==null){
				_Yest32_responsetime=getIntegerProperty("YesT32/responseTime");
				return _Yest32_responsetime;
			}else {
				return _Yest32_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT32/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest32_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT32/responseTime",v);
		_Yest32_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest33_accuracy=null;

	/**
	 * @return Returns the YesT33/accuracy.
	 */
	public Integer getYest33_accuracy() {
		try{
			if (_Yest33_accuracy==null){
				_Yest33_accuracy=getIntegerProperty("YesT33/accuracy");
				return _Yest33_accuracy;
			}else {
				return _Yest33_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT33/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest33_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT33/accuracy",v);
		_Yest33_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest33_responsetime=null;

	/**
	 * @return Returns the YesT33/responseTime.
	 */
	public Integer getYest33_responsetime() {
		try{
			if (_Yest33_responsetime==null){
				_Yest33_responsetime=getIntegerProperty("YesT33/responseTime");
				return _Yest33_responsetime;
			}else {
				return _Yest33_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT33/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest33_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT33/responseTime",v);
		_Yest33_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest34_accuracy=null;

	/**
	 * @return Returns the YesT34/accuracy.
	 */
	public Integer getYest34_accuracy() {
		try{
			if (_Yest34_accuracy==null){
				_Yest34_accuracy=getIntegerProperty("YesT34/accuracy");
				return _Yest34_accuracy;
			}else {
				return _Yest34_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT34/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest34_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT34/accuracy",v);
		_Yest34_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest34_responsetime=null;

	/**
	 * @return Returns the YesT34/responseTime.
	 */
	public Integer getYest34_responsetime() {
		try{
			if (_Yest34_responsetime==null){
				_Yest34_responsetime=getIntegerProperty("YesT34/responseTime");
				return _Yest34_responsetime;
			}else {
				return _Yest34_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT34/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest34_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT34/responseTime",v);
		_Yest34_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest35_accuracy=null;

	/**
	 * @return Returns the YesT35/accuracy.
	 */
	public Integer getYest35_accuracy() {
		try{
			if (_Yest35_accuracy==null){
				_Yest35_accuracy=getIntegerProperty("YesT35/accuracy");
				return _Yest35_accuracy;
			}else {
				return _Yest35_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT35/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest35_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT35/accuracy",v);
		_Yest35_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest35_responsetime=null;

	/**
	 * @return Returns the YesT35/responseTime.
	 */
	public Integer getYest35_responsetime() {
		try{
			if (_Yest35_responsetime==null){
				_Yest35_responsetime=getIntegerProperty("YesT35/responseTime");
				return _Yest35_responsetime;
			}else {
				return _Yest35_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT35/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest35_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT35/responseTime",v);
		_Yest35_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest36_accuracy=null;

	/**
	 * @return Returns the YesT36/accuracy.
	 */
	public Integer getYest36_accuracy() {
		try{
			if (_Yest36_accuracy==null){
				_Yest36_accuracy=getIntegerProperty("YesT36/accuracy");
				return _Yest36_accuracy;
			}else {
				return _Yest36_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT36/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest36_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT36/accuracy",v);
		_Yest36_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest36_responsetime=null;

	/**
	 * @return Returns the YesT36/responseTime.
	 */
	public Integer getYest36_responsetime() {
		try{
			if (_Yest36_responsetime==null){
				_Yest36_responsetime=getIntegerProperty("YesT36/responseTime");
				return _Yest36_responsetime;
			}else {
				return _Yest36_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT36/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest36_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT36/responseTime",v);
		_Yest36_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest37_accuracy=null;

	/**
	 * @return Returns the YesT37/accuracy.
	 */
	public Integer getYest37_accuracy() {
		try{
			if (_Yest37_accuracy==null){
				_Yest37_accuracy=getIntegerProperty("YesT37/accuracy");
				return _Yest37_accuracy;
			}else {
				return _Yest37_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT37/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest37_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT37/accuracy",v);
		_Yest37_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest37_responsetime=null;

	/**
	 * @return Returns the YesT37/responseTime.
	 */
	public Integer getYest37_responsetime() {
		try{
			if (_Yest37_responsetime==null){
				_Yest37_responsetime=getIntegerProperty("YesT37/responseTime");
				return _Yest37_responsetime;
			}else {
				return _Yest37_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT37/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest37_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT37/responseTime",v);
		_Yest37_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest38_accuracy=null;

	/**
	 * @return Returns the YesT38/accuracy.
	 */
	public Integer getYest38_accuracy() {
		try{
			if (_Yest38_accuracy==null){
				_Yest38_accuracy=getIntegerProperty("YesT38/accuracy");
				return _Yest38_accuracy;
			}else {
				return _Yest38_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT38/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest38_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT38/accuracy",v);
		_Yest38_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest38_responsetime=null;

	/**
	 * @return Returns the YesT38/responseTime.
	 */
	public Integer getYest38_responsetime() {
		try{
			if (_Yest38_responsetime==null){
				_Yest38_responsetime=getIntegerProperty("YesT38/responseTime");
				return _Yest38_responsetime;
			}else {
				return _Yest38_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT38/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest38_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT38/responseTime",v);
		_Yest38_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest39_accuracy=null;

	/**
	 * @return Returns the YesT39/accuracy.
	 */
	public Integer getYest39_accuracy() {
		try{
			if (_Yest39_accuracy==null){
				_Yest39_accuracy=getIntegerProperty("YesT39/accuracy");
				return _Yest39_accuracy;
			}else {
				return _Yest39_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT39/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest39_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT39/accuracy",v);
		_Yest39_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest39_responsetime=null;

	/**
	 * @return Returns the YesT39/responseTime.
	 */
	public Integer getYest39_responsetime() {
		try{
			if (_Yest39_responsetime==null){
				_Yest39_responsetime=getIntegerProperty("YesT39/responseTime");
				return _Yest39_responsetime;
			}else {
				return _Yest39_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT39/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest39_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT39/responseTime",v);
		_Yest39_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest40_accuracy=null;

	/**
	 * @return Returns the YesT40/accuracy.
	 */
	public Integer getYest40_accuracy() {
		try{
			if (_Yest40_accuracy==null){
				_Yest40_accuracy=getIntegerProperty("YesT40/accuracy");
				return _Yest40_accuracy;
			}else {
				return _Yest40_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT40/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest40_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT40/accuracy",v);
		_Yest40_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest40_responsetime=null;

	/**
	 * @return Returns the YesT40/responseTime.
	 */
	public Integer getYest40_responsetime() {
		try{
			if (_Yest40_responsetime==null){
				_Yest40_responsetime=getIntegerProperty("YesT40/responseTime");
				return _Yest40_responsetime;
			}else {
				return _Yest40_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT40/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest40_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT40/responseTime",v);
		_Yest40_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest41_accuracy=null;

	/**
	 * @return Returns the YesT41/accuracy.
	 */
	public Integer getYest41_accuracy() {
		try{
			if (_Yest41_accuracy==null){
				_Yest41_accuracy=getIntegerProperty("YesT41/accuracy");
				return _Yest41_accuracy;
			}else {
				return _Yest41_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT41/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest41_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT41/accuracy",v);
		_Yest41_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest41_responsetime=null;

	/**
	 * @return Returns the YesT41/responseTime.
	 */
	public Integer getYest41_responsetime() {
		try{
			if (_Yest41_responsetime==null){
				_Yest41_responsetime=getIntegerProperty("YesT41/responseTime");
				return _Yest41_responsetime;
			}else {
				return _Yest41_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT41/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest41_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT41/responseTime",v);
		_Yest41_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest42_accuracy=null;

	/**
	 * @return Returns the YesT42/accuracy.
	 */
	public Integer getYest42_accuracy() {
		try{
			if (_Yest42_accuracy==null){
				_Yest42_accuracy=getIntegerProperty("YesT42/accuracy");
				return _Yest42_accuracy;
			}else {
				return _Yest42_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT42/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest42_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT42/accuracy",v);
		_Yest42_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest42_responsetime=null;

	/**
	 * @return Returns the YesT42/responseTime.
	 */
	public Integer getYest42_responsetime() {
		try{
			if (_Yest42_responsetime==null){
				_Yest42_responsetime=getIntegerProperty("YesT42/responseTime");
				return _Yest42_responsetime;
			}else {
				return _Yest42_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT42/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest42_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT42/responseTime",v);
		_Yest42_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest43_accuracy=null;

	/**
	 * @return Returns the YesT43/accuracy.
	 */
	public Integer getYest43_accuracy() {
		try{
			if (_Yest43_accuracy==null){
				_Yest43_accuracy=getIntegerProperty("YesT43/accuracy");
				return _Yest43_accuracy;
			}else {
				return _Yest43_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT43/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest43_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT43/accuracy",v);
		_Yest43_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest43_responsetime=null;

	/**
	 * @return Returns the YesT43/responseTime.
	 */
	public Integer getYest43_responsetime() {
		try{
			if (_Yest43_responsetime==null){
				_Yest43_responsetime=getIntegerProperty("YesT43/responseTime");
				return _Yest43_responsetime;
			}else {
				return _Yest43_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT43/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest43_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT43/responseTime",v);
		_Yest43_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest44_accuracy=null;

	/**
	 * @return Returns the YesT44/accuracy.
	 */
	public Integer getYest44_accuracy() {
		try{
			if (_Yest44_accuracy==null){
				_Yest44_accuracy=getIntegerProperty("YesT44/accuracy");
				return _Yest44_accuracy;
			}else {
				return _Yest44_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT44/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest44_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT44/accuracy",v);
		_Yest44_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest44_responsetime=null;

	/**
	 * @return Returns the YesT44/responseTime.
	 */
	public Integer getYest44_responsetime() {
		try{
			if (_Yest44_responsetime==null){
				_Yest44_responsetime=getIntegerProperty("YesT44/responseTime");
				return _Yest44_responsetime;
			}else {
				return _Yest44_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT44/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest44_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT44/responseTime",v);
		_Yest44_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest45_accuracy=null;

	/**
	 * @return Returns the YesT45/accuracy.
	 */
	public Integer getYest45_accuracy() {
		try{
			if (_Yest45_accuracy==null){
				_Yest45_accuracy=getIntegerProperty("YesT45/accuracy");
				return _Yest45_accuracy;
			}else {
				return _Yest45_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT45/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest45_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT45/accuracy",v);
		_Yest45_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest45_responsetime=null;

	/**
	 * @return Returns the YesT45/responseTime.
	 */
	public Integer getYest45_responsetime() {
		try{
			if (_Yest45_responsetime==null){
				_Yest45_responsetime=getIntegerProperty("YesT45/responseTime");
				return _Yest45_responsetime;
			}else {
				return _Yest45_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT45/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest45_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT45/responseTime",v);
		_Yest45_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest46_accuracy=null;

	/**
	 * @return Returns the YesT46/accuracy.
	 */
	public Integer getYest46_accuracy() {
		try{
			if (_Yest46_accuracy==null){
				_Yest46_accuracy=getIntegerProperty("YesT46/accuracy");
				return _Yest46_accuracy;
			}else {
				return _Yest46_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT46/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest46_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT46/accuracy",v);
		_Yest46_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest46_responsetime=null;

	/**
	 * @return Returns the YesT46/responseTime.
	 */
	public Integer getYest46_responsetime() {
		try{
			if (_Yest46_responsetime==null){
				_Yest46_responsetime=getIntegerProperty("YesT46/responseTime");
				return _Yest46_responsetime;
			}else {
				return _Yest46_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT46/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest46_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT46/responseTime",v);
		_Yest46_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest47_accuracy=null;

	/**
	 * @return Returns the YesT47/accuracy.
	 */
	public Integer getYest47_accuracy() {
		try{
			if (_Yest47_accuracy==null){
				_Yest47_accuracy=getIntegerProperty("YesT47/accuracy");
				return _Yest47_accuracy;
			}else {
				return _Yest47_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT47/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest47_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT47/accuracy",v);
		_Yest47_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest47_responsetime=null;

	/**
	 * @return Returns the YesT47/responseTime.
	 */
	public Integer getYest47_responsetime() {
		try{
			if (_Yest47_responsetime==null){
				_Yest47_responsetime=getIntegerProperty("YesT47/responseTime");
				return _Yest47_responsetime;
			}else {
				return _Yest47_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT47/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest47_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT47/responseTime",v);
		_Yest47_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest48_accuracy=null;

	/**
	 * @return Returns the YesT48/accuracy.
	 */
	public Integer getYest48_accuracy() {
		try{
			if (_Yest48_accuracy==null){
				_Yest48_accuracy=getIntegerProperty("YesT48/accuracy");
				return _Yest48_accuracy;
			}else {
				return _Yest48_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT48/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest48_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT48/accuracy",v);
		_Yest48_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest48_responsetime=null;

	/**
	 * @return Returns the YesT48/responseTime.
	 */
	public Integer getYest48_responsetime() {
		try{
			if (_Yest48_responsetime==null){
				_Yest48_responsetime=getIntegerProperty("YesT48/responseTime");
				return _Yest48_responsetime;
			}else {
				return _Yest48_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT48/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest48_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT48/responseTime",v);
		_Yest48_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest49_accuracy=null;

	/**
	 * @return Returns the YesT49/accuracy.
	 */
	public Integer getYest49_accuracy() {
		try{
			if (_Yest49_accuracy==null){
				_Yest49_accuracy=getIntegerProperty("YesT49/accuracy");
				return _Yest49_accuracy;
			}else {
				return _Yest49_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT49/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest49_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT49/accuracy",v);
		_Yest49_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest49_responsetime=null;

	/**
	 * @return Returns the YesT49/responseTime.
	 */
	public Integer getYest49_responsetime() {
		try{
			if (_Yest49_responsetime==null){
				_Yest49_responsetime=getIntegerProperty("YesT49/responseTime");
				return _Yest49_responsetime;
			}else {
				return _Yest49_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT49/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest49_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT49/responseTime",v);
		_Yest49_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest50_accuracy=null;

	/**
	 * @return Returns the YesT50/accuracy.
	 */
	public Integer getYest50_accuracy() {
		try{
			if (_Yest50_accuracy==null){
				_Yest50_accuracy=getIntegerProperty("YesT50/accuracy");
				return _Yest50_accuracy;
			}else {
				return _Yest50_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT50/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest50_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT50/accuracy",v);
		_Yest50_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest50_responsetime=null;

	/**
	 * @return Returns the YesT50/responseTime.
	 */
	public Integer getYest50_responsetime() {
		try{
			if (_Yest50_responsetime==null){
				_Yest50_responsetime=getIntegerProperty("YesT50/responseTime");
				return _Yest50_responsetime;
			}else {
				return _Yest50_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT50/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest50_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT50/responseTime",v);
		_Yest50_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest51_accuracy=null;

	/**
	 * @return Returns the YesT51/accuracy.
	 */
	public Integer getYest51_accuracy() {
		try{
			if (_Yest51_accuracy==null){
				_Yest51_accuracy=getIntegerProperty("YesT51/accuracy");
				return _Yest51_accuracy;
			}else {
				return _Yest51_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT51/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest51_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT51/accuracy",v);
		_Yest51_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest51_responsetime=null;

	/**
	 * @return Returns the YesT51/responseTime.
	 */
	public Integer getYest51_responsetime() {
		try{
			if (_Yest51_responsetime==null){
				_Yest51_responsetime=getIntegerProperty("YesT51/responseTime");
				return _Yest51_responsetime;
			}else {
				return _Yest51_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT51/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest51_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT51/responseTime",v);
		_Yest51_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest52_accuracy=null;

	/**
	 * @return Returns the YesT52/accuracy.
	 */
	public Integer getYest52_accuracy() {
		try{
			if (_Yest52_accuracy==null){
				_Yest52_accuracy=getIntegerProperty("YesT52/accuracy");
				return _Yest52_accuracy;
			}else {
				return _Yest52_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT52/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest52_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT52/accuracy",v);
		_Yest52_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest52_responsetime=null;

	/**
	 * @return Returns the YesT52/responseTime.
	 */
	public Integer getYest52_responsetime() {
		try{
			if (_Yest52_responsetime==null){
				_Yest52_responsetime=getIntegerProperty("YesT52/responseTime");
				return _Yest52_responsetime;
			}else {
				return _Yest52_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT52/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest52_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT52/responseTime",v);
		_Yest52_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest53_accuracy=null;

	/**
	 * @return Returns the YesT53/accuracy.
	 */
	public Integer getYest53_accuracy() {
		try{
			if (_Yest53_accuracy==null){
				_Yest53_accuracy=getIntegerProperty("YesT53/accuracy");
				return _Yest53_accuracy;
			}else {
				return _Yest53_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT53/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest53_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT53/accuracy",v);
		_Yest53_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest53_responsetime=null;

	/**
	 * @return Returns the YesT53/responseTime.
	 */
	public Integer getYest53_responsetime() {
		try{
			if (_Yest53_responsetime==null){
				_Yest53_responsetime=getIntegerProperty("YesT53/responseTime");
				return _Yest53_responsetime;
			}else {
				return _Yest53_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT53/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest53_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT53/responseTime",v);
		_Yest53_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest54_accuracy=null;

	/**
	 * @return Returns the YesT54/accuracy.
	 */
	public Integer getYest54_accuracy() {
		try{
			if (_Yest54_accuracy==null){
				_Yest54_accuracy=getIntegerProperty("YesT54/accuracy");
				return _Yest54_accuracy;
			}else {
				return _Yest54_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT54/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest54_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT54/accuracy",v);
		_Yest54_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest54_responsetime=null;

	/**
	 * @return Returns the YesT54/responseTime.
	 */
	public Integer getYest54_responsetime() {
		try{
			if (_Yest54_responsetime==null){
				_Yest54_responsetime=getIntegerProperty("YesT54/responseTime");
				return _Yest54_responsetime;
			}else {
				return _Yest54_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT54/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest54_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT54/responseTime",v);
		_Yest54_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest55_accuracy=null;

	/**
	 * @return Returns the YesT55/accuracy.
	 */
	public Integer getYest55_accuracy() {
		try{
			if (_Yest55_accuracy==null){
				_Yest55_accuracy=getIntegerProperty("YesT55/accuracy");
				return _Yest55_accuracy;
			}else {
				return _Yest55_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT55/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest55_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT55/accuracy",v);
		_Yest55_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest55_responsetime=null;

	/**
	 * @return Returns the YesT55/responseTime.
	 */
	public Integer getYest55_responsetime() {
		try{
			if (_Yest55_responsetime==null){
				_Yest55_responsetime=getIntegerProperty("YesT55/responseTime");
				return _Yest55_responsetime;
			}else {
				return _Yest55_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT55/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest55_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT55/responseTime",v);
		_Yest55_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest56_accuracy=null;

	/**
	 * @return Returns the YesT56/accuracy.
	 */
	public Integer getYest56_accuracy() {
		try{
			if (_Yest56_accuracy==null){
				_Yest56_accuracy=getIntegerProperty("YesT56/accuracy");
				return _Yest56_accuracy;
			}else {
				return _Yest56_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT56/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest56_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT56/accuracy",v);
		_Yest56_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest56_responsetime=null;

	/**
	 * @return Returns the YesT56/responseTime.
	 */
	public Integer getYest56_responsetime() {
		try{
			if (_Yest56_responsetime==null){
				_Yest56_responsetime=getIntegerProperty("YesT56/responseTime");
				return _Yest56_responsetime;
			}else {
				return _Yest56_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT56/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest56_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT56/responseTime",v);
		_Yest56_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest57_accuracy=null;

	/**
	 * @return Returns the YesT57/accuracy.
	 */
	public Integer getYest57_accuracy() {
		try{
			if (_Yest57_accuracy==null){
				_Yest57_accuracy=getIntegerProperty("YesT57/accuracy");
				return _Yest57_accuracy;
			}else {
				return _Yest57_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT57/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest57_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT57/accuracy",v);
		_Yest57_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest57_responsetime=null;

	/**
	 * @return Returns the YesT57/responseTime.
	 */
	public Integer getYest57_responsetime() {
		try{
			if (_Yest57_responsetime==null){
				_Yest57_responsetime=getIntegerProperty("YesT57/responseTime");
				return _Yest57_responsetime;
			}else {
				return _Yest57_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT57/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest57_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT57/responseTime",v);
		_Yest57_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest58_accuracy=null;

	/**
	 * @return Returns the YesT58/accuracy.
	 */
	public Integer getYest58_accuracy() {
		try{
			if (_Yest58_accuracy==null){
				_Yest58_accuracy=getIntegerProperty("YesT58/accuracy");
				return _Yest58_accuracy;
			}else {
				return _Yest58_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT58/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest58_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT58/accuracy",v);
		_Yest58_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest58_responsetime=null;

	/**
	 * @return Returns the YesT58/responseTime.
	 */
	public Integer getYest58_responsetime() {
		try{
			if (_Yest58_responsetime==null){
				_Yest58_responsetime=getIntegerProperty("YesT58/responseTime");
				return _Yest58_responsetime;
			}else {
				return _Yest58_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT58/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest58_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT58/responseTime",v);
		_Yest58_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest59_accuracy=null;

	/**
	 * @return Returns the YesT59/accuracy.
	 */
	public Integer getYest59_accuracy() {
		try{
			if (_Yest59_accuracy==null){
				_Yest59_accuracy=getIntegerProperty("YesT59/accuracy");
				return _Yest59_accuracy;
			}else {
				return _Yest59_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT59/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest59_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT59/accuracy",v);
		_Yest59_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest59_responsetime=null;

	/**
	 * @return Returns the YesT59/responseTime.
	 */
	public Integer getYest59_responsetime() {
		try{
			if (_Yest59_responsetime==null){
				_Yest59_responsetime=getIntegerProperty("YesT59/responseTime");
				return _Yest59_responsetime;
			}else {
				return _Yest59_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT59/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest59_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT59/responseTime",v);
		_Yest59_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest60_accuracy=null;

	/**
	 * @return Returns the YesT60/accuracy.
	 */
	public Integer getYest60_accuracy() {
		try{
			if (_Yest60_accuracy==null){
				_Yest60_accuracy=getIntegerProperty("YesT60/accuracy");
				return _Yest60_accuracy;
			}else {
				return _Yest60_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT60/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest60_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT60/accuracy",v);
		_Yest60_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest60_responsetime=null;

	/**
	 * @return Returns the YesT60/responseTime.
	 */
	public Integer getYest60_responsetime() {
		try{
			if (_Yest60_responsetime==null){
				_Yest60_responsetime=getIntegerProperty("YesT60/responseTime");
				return _Yest60_responsetime;
			}else {
				return _Yest60_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT60/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest60_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT60/responseTime",v);
		_Yest60_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest61_accuracy=null;

	/**
	 * @return Returns the YesT61/accuracy.
	 */
	public Integer getYest61_accuracy() {
		try{
			if (_Yest61_accuracy==null){
				_Yest61_accuracy=getIntegerProperty("YesT61/accuracy");
				return _Yest61_accuracy;
			}else {
				return _Yest61_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT61/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest61_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT61/accuracy",v);
		_Yest61_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest61_responsetime=null;

	/**
	 * @return Returns the YesT61/responseTime.
	 */
	public Integer getYest61_responsetime() {
		try{
			if (_Yest61_responsetime==null){
				_Yest61_responsetime=getIntegerProperty("YesT61/responseTime");
				return _Yest61_responsetime;
			}else {
				return _Yest61_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT61/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest61_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT61/responseTime",v);
		_Yest61_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest62_accuracy=null;

	/**
	 * @return Returns the YesT62/accuracy.
	 */
	public Integer getYest62_accuracy() {
		try{
			if (_Yest62_accuracy==null){
				_Yest62_accuracy=getIntegerProperty("YesT62/accuracy");
				return _Yest62_accuracy;
			}else {
				return _Yest62_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT62/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest62_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT62/accuracy",v);
		_Yest62_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest62_responsetime=null;

	/**
	 * @return Returns the YesT62/responseTime.
	 */
	public Integer getYest62_responsetime() {
		try{
			if (_Yest62_responsetime==null){
				_Yest62_responsetime=getIntegerProperty("YesT62/responseTime");
				return _Yest62_responsetime;
			}else {
				return _Yest62_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT62/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest62_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT62/responseTime",v);
		_Yest62_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest63_accuracy=null;

	/**
	 * @return Returns the YesT63/accuracy.
	 */
	public Integer getYest63_accuracy() {
		try{
			if (_Yest63_accuracy==null){
				_Yest63_accuracy=getIntegerProperty("YesT63/accuracy");
				return _Yest63_accuracy;
			}else {
				return _Yest63_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT63/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest63_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT63/accuracy",v);
		_Yest63_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest63_responsetime=null;

	/**
	 * @return Returns the YesT63/responseTime.
	 */
	public Integer getYest63_responsetime() {
		try{
			if (_Yest63_responsetime==null){
				_Yest63_responsetime=getIntegerProperty("YesT63/responseTime");
				return _Yest63_responsetime;
			}else {
				return _Yest63_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT63/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest63_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT63/responseTime",v);
		_Yest63_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest64_accuracy=null;

	/**
	 * @return Returns the YesT64/accuracy.
	 */
	public Integer getYest64_accuracy() {
		try{
			if (_Yest64_accuracy==null){
				_Yest64_accuracy=getIntegerProperty("YesT64/accuracy");
				return _Yest64_accuracy;
			}else {
				return _Yest64_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT64/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest64_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT64/accuracy",v);
		_Yest64_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest64_responsetime=null;

	/**
	 * @return Returns the YesT64/responseTime.
	 */
	public Integer getYest64_responsetime() {
		try{
			if (_Yest64_responsetime==null){
				_Yest64_responsetime=getIntegerProperty("YesT64/responseTime");
				return _Yest64_responsetime;
			}else {
				return _Yest64_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT64/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest64_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT64/responseTime",v);
		_Yest64_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest65_accuracy=null;

	/**
	 * @return Returns the YesT65/accuracy.
	 */
	public Integer getYest65_accuracy() {
		try{
			if (_Yest65_accuracy==null){
				_Yest65_accuracy=getIntegerProperty("YesT65/accuracy");
				return _Yest65_accuracy;
			}else {
				return _Yest65_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT65/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest65_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT65/accuracy",v);
		_Yest65_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest65_responsetime=null;

	/**
	 * @return Returns the YesT65/responseTime.
	 */
	public Integer getYest65_responsetime() {
		try{
			if (_Yest65_responsetime==null){
				_Yest65_responsetime=getIntegerProperty("YesT65/responseTime");
				return _Yest65_responsetime;
			}else {
				return _Yest65_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT65/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest65_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT65/responseTime",v);
		_Yest65_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest66_accuracy=null;

	/**
	 * @return Returns the YesT66/accuracy.
	 */
	public Integer getYest66_accuracy() {
		try{
			if (_Yest66_accuracy==null){
				_Yest66_accuracy=getIntegerProperty("YesT66/accuracy");
				return _Yest66_accuracy;
			}else {
				return _Yest66_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT66/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest66_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT66/accuracy",v);
		_Yest66_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest66_responsetime=null;

	/**
	 * @return Returns the YesT66/responseTime.
	 */
	public Integer getYest66_responsetime() {
		try{
			if (_Yest66_responsetime==null){
				_Yest66_responsetime=getIntegerProperty("YesT66/responseTime");
				return _Yest66_responsetime;
			}else {
				return _Yest66_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT66/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest66_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT66/responseTime",v);
		_Yest66_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest67_accuracy=null;

	/**
	 * @return Returns the YesT67/accuracy.
	 */
	public Integer getYest67_accuracy() {
		try{
			if (_Yest67_accuracy==null){
				_Yest67_accuracy=getIntegerProperty("YesT67/accuracy");
				return _Yest67_accuracy;
			}else {
				return _Yest67_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT67/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest67_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT67/accuracy",v);
		_Yest67_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest67_responsetime=null;

	/**
	 * @return Returns the YesT67/responseTime.
	 */
	public Integer getYest67_responsetime() {
		try{
			if (_Yest67_responsetime==null){
				_Yest67_responsetime=getIntegerProperty("YesT67/responseTime");
				return _Yest67_responsetime;
			}else {
				return _Yest67_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT67/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest67_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT67/responseTime",v);
		_Yest67_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest68_accuracy=null;

	/**
	 * @return Returns the YesT68/accuracy.
	 */
	public Integer getYest68_accuracy() {
		try{
			if (_Yest68_accuracy==null){
				_Yest68_accuracy=getIntegerProperty("YesT68/accuracy");
				return _Yest68_accuracy;
			}else {
				return _Yest68_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT68/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest68_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT68/accuracy",v);
		_Yest68_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest68_responsetime=null;

	/**
	 * @return Returns the YesT68/responseTime.
	 */
	public Integer getYest68_responsetime() {
		try{
			if (_Yest68_responsetime==null){
				_Yest68_responsetime=getIntegerProperty("YesT68/responseTime");
				return _Yest68_responsetime;
			}else {
				return _Yest68_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT68/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest68_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT68/responseTime",v);
		_Yest68_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest69_accuracy=null;

	/**
	 * @return Returns the YesT69/accuracy.
	 */
	public Integer getYest69_accuracy() {
		try{
			if (_Yest69_accuracy==null){
				_Yest69_accuracy=getIntegerProperty("YesT69/accuracy");
				return _Yest69_accuracy;
			}else {
				return _Yest69_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT69/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest69_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT69/accuracy",v);
		_Yest69_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest69_responsetime=null;

	/**
	 * @return Returns the YesT69/responseTime.
	 */
	public Integer getYest69_responsetime() {
		try{
			if (_Yest69_responsetime==null){
				_Yest69_responsetime=getIntegerProperty("YesT69/responseTime");
				return _Yest69_responsetime;
			}else {
				return _Yest69_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT69/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest69_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT69/responseTime",v);
		_Yest69_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest70_accuracy=null;

	/**
	 * @return Returns the YesT70/accuracy.
	 */
	public Integer getYest70_accuracy() {
		try{
			if (_Yest70_accuracy==null){
				_Yest70_accuracy=getIntegerProperty("YesT70/accuracy");
				return _Yest70_accuracy;
			}else {
				return _Yest70_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT70/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest70_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT70/accuracy",v);
		_Yest70_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest70_responsetime=null;

	/**
	 * @return Returns the YesT70/responseTime.
	 */
	public Integer getYest70_responsetime() {
		try{
			if (_Yest70_responsetime==null){
				_Yest70_responsetime=getIntegerProperty("YesT70/responseTime");
				return _Yest70_responsetime;
			}else {
				return _Yest70_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT70/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest70_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT70/responseTime",v);
		_Yest70_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest71_accuracy=null;

	/**
	 * @return Returns the YesT71/accuracy.
	 */
	public Integer getYest71_accuracy() {
		try{
			if (_Yest71_accuracy==null){
				_Yest71_accuracy=getIntegerProperty("YesT71/accuracy");
				return _Yest71_accuracy;
			}else {
				return _Yest71_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT71/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest71_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT71/accuracy",v);
		_Yest71_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest71_responsetime=null;

	/**
	 * @return Returns the YesT71/responseTime.
	 */
	public Integer getYest71_responsetime() {
		try{
			if (_Yest71_responsetime==null){
				_Yest71_responsetime=getIntegerProperty("YesT71/responseTime");
				return _Yest71_responsetime;
			}else {
				return _Yest71_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT71/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest71_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT71/responseTime",v);
		_Yest71_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest72_accuracy=null;

	/**
	 * @return Returns the YesT72/accuracy.
	 */
	public Integer getYest72_accuracy() {
		try{
			if (_Yest72_accuracy==null){
				_Yest72_accuracy=getIntegerProperty("YesT72/accuracy");
				return _Yest72_accuracy;
			}else {
				return _Yest72_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT72/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest72_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT72/accuracy",v);
		_Yest72_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest72_responsetime=null;

	/**
	 * @return Returns the YesT72/responseTime.
	 */
	public Integer getYest72_responsetime() {
		try{
			if (_Yest72_responsetime==null){
				_Yest72_responsetime=getIntegerProperty("YesT72/responseTime");
				return _Yest72_responsetime;
			}else {
				return _Yest72_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT72/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest72_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT72/responseTime",v);
		_Yest72_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest73_accuracy=null;

	/**
	 * @return Returns the YesT73/accuracy.
	 */
	public Integer getYest73_accuracy() {
		try{
			if (_Yest73_accuracy==null){
				_Yest73_accuracy=getIntegerProperty("YesT73/accuracy");
				return _Yest73_accuracy;
			}else {
				return _Yest73_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT73/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest73_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT73/accuracy",v);
		_Yest73_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest73_responsetime=null;

	/**
	 * @return Returns the YesT73/responseTime.
	 */
	public Integer getYest73_responsetime() {
		try{
			if (_Yest73_responsetime==null){
				_Yest73_responsetime=getIntegerProperty("YesT73/responseTime");
				return _Yest73_responsetime;
			}else {
				return _Yest73_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT73/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest73_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT73/responseTime",v);
		_Yest73_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest74_accuracy=null;

	/**
	 * @return Returns the YesT74/accuracy.
	 */
	public Integer getYest74_accuracy() {
		try{
			if (_Yest74_accuracy==null){
				_Yest74_accuracy=getIntegerProperty("YesT74/accuracy");
				return _Yest74_accuracy;
			}else {
				return _Yest74_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT74/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest74_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT74/accuracy",v);
		_Yest74_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest74_responsetime=null;

	/**
	 * @return Returns the YesT74/responseTime.
	 */
	public Integer getYest74_responsetime() {
		try{
			if (_Yest74_responsetime==null){
				_Yest74_responsetime=getIntegerProperty("YesT74/responseTime");
				return _Yest74_responsetime;
			}else {
				return _Yest74_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT74/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest74_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT74/responseTime",v);
		_Yest74_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest75_accuracy=null;

	/**
	 * @return Returns the YesT75/accuracy.
	 */
	public Integer getYest75_accuracy() {
		try{
			if (_Yest75_accuracy==null){
				_Yest75_accuracy=getIntegerProperty("YesT75/accuracy");
				return _Yest75_accuracy;
			}else {
				return _Yest75_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT75/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest75_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT75/accuracy",v);
		_Yest75_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest75_responsetime=null;

	/**
	 * @return Returns the YesT75/responseTime.
	 */
	public Integer getYest75_responsetime() {
		try{
			if (_Yest75_responsetime==null){
				_Yest75_responsetime=getIntegerProperty("YesT75/responseTime");
				return _Yest75_responsetime;
			}else {
				return _Yest75_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT75/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest75_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT75/responseTime",v);
		_Yest75_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest76_accuracy=null;

	/**
	 * @return Returns the YesT76/accuracy.
	 */
	public Integer getYest76_accuracy() {
		try{
			if (_Yest76_accuracy==null){
				_Yest76_accuracy=getIntegerProperty("YesT76/accuracy");
				return _Yest76_accuracy;
			}else {
				return _Yest76_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT76/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest76_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT76/accuracy",v);
		_Yest76_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest76_responsetime=null;

	/**
	 * @return Returns the YesT76/responseTime.
	 */
	public Integer getYest76_responsetime() {
		try{
			if (_Yest76_responsetime==null){
				_Yest76_responsetime=getIntegerProperty("YesT76/responseTime");
				return _Yest76_responsetime;
			}else {
				return _Yest76_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT76/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest76_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT76/responseTime",v);
		_Yest76_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest77_accuracy=null;

	/**
	 * @return Returns the YesT77/accuracy.
	 */
	public Integer getYest77_accuracy() {
		try{
			if (_Yest77_accuracy==null){
				_Yest77_accuracy=getIntegerProperty("YesT77/accuracy");
				return _Yest77_accuracy;
			}else {
				return _Yest77_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT77/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest77_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT77/accuracy",v);
		_Yest77_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest77_responsetime=null;

	/**
	 * @return Returns the YesT77/responseTime.
	 */
	public Integer getYest77_responsetime() {
		try{
			if (_Yest77_responsetime==null){
				_Yest77_responsetime=getIntegerProperty("YesT77/responseTime");
				return _Yest77_responsetime;
			}else {
				return _Yest77_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT77/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest77_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT77/responseTime",v);
		_Yest77_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest78_accuracy=null;

	/**
	 * @return Returns the YesT78/accuracy.
	 */
	public Integer getYest78_accuracy() {
		try{
			if (_Yest78_accuracy==null){
				_Yest78_accuracy=getIntegerProperty("YesT78/accuracy");
				return _Yest78_accuracy;
			}else {
				return _Yest78_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT78/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest78_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT78/accuracy",v);
		_Yest78_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest78_responsetime=null;

	/**
	 * @return Returns the YesT78/responseTime.
	 */
	public Integer getYest78_responsetime() {
		try{
			if (_Yest78_responsetime==null){
				_Yest78_responsetime=getIntegerProperty("YesT78/responseTime");
				return _Yest78_responsetime;
			}else {
				return _Yest78_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT78/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest78_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT78/responseTime",v);
		_Yest78_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest79_accuracy=null;

	/**
	 * @return Returns the YesT79/accuracy.
	 */
	public Integer getYest79_accuracy() {
		try{
			if (_Yest79_accuracy==null){
				_Yest79_accuracy=getIntegerProperty("YesT79/accuracy");
				return _Yest79_accuracy;
			}else {
				return _Yest79_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT79/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest79_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT79/accuracy",v);
		_Yest79_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest79_responsetime=null;

	/**
	 * @return Returns the YesT79/responseTime.
	 */
	public Integer getYest79_responsetime() {
		try{
			if (_Yest79_responsetime==null){
				_Yest79_responsetime=getIntegerProperty("YesT79/responseTime");
				return _Yest79_responsetime;
			}else {
				return _Yest79_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT79/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest79_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT79/responseTime",v);
		_Yest79_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest80_accuracy=null;

	/**
	 * @return Returns the YesT80/accuracy.
	 */
	public Integer getYest80_accuracy() {
		try{
			if (_Yest80_accuracy==null){
				_Yest80_accuracy=getIntegerProperty("YesT80/accuracy");
				return _Yest80_accuracy;
			}else {
				return _Yest80_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT80/accuracy.
	 * @param v Value to Set.
	 */
	public void setYest80_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT80/accuracy",v);
		_Yest80_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yest80_responsetime=null;

	/**
	 * @return Returns the YesT80/responseTime.
	 */
	public Integer getYest80_responsetime() {
		try{
			if (_Yest80_responsetime==null){
				_Yest80_responsetime=getIntegerProperty("YesT80/responseTime");
				return _Yest80_responsetime;
			}else {
				return _Yest80_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for YesT80/responseTime.
	 * @param v Value to Set.
	 */
	public void setYest80_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/YesT80/responseTime",v);
		_Yest80_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not1_accuracy=null;

	/**
	 * @return Returns the NoT1/accuracy.
	 */
	public Integer getNot1_accuracy() {
		try{
			if (_Not1_accuracy==null){
				_Not1_accuracy=getIntegerProperty("NoT1/accuracy");
				return _Not1_accuracy;
			}else {
				return _Not1_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT1/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot1_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT1/accuracy",v);
		_Not1_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not1_responsetime=null;

	/**
	 * @return Returns the NoT1/responseTime.
	 */
	public Integer getNot1_responsetime() {
		try{
			if (_Not1_responsetime==null){
				_Not1_responsetime=getIntegerProperty("NoT1/responseTime");
				return _Not1_responsetime;
			}else {
				return _Not1_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT1/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot1_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT1/responseTime",v);
		_Not1_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not2_accuracy=null;

	/**
	 * @return Returns the NoT2/accuracy.
	 */
	public Integer getNot2_accuracy() {
		try{
			if (_Not2_accuracy==null){
				_Not2_accuracy=getIntegerProperty("NoT2/accuracy");
				return _Not2_accuracy;
			}else {
				return _Not2_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT2/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot2_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT2/accuracy",v);
		_Not2_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not2_responsetime=null;

	/**
	 * @return Returns the NoT2/responseTime.
	 */
	public Integer getNot2_responsetime() {
		try{
			if (_Not2_responsetime==null){
				_Not2_responsetime=getIntegerProperty("NoT2/responseTime");
				return _Not2_responsetime;
			}else {
				return _Not2_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT2/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot2_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT2/responseTime",v);
		_Not2_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not3_accuracy=null;

	/**
	 * @return Returns the NoT3/accuracy.
	 */
	public Integer getNot3_accuracy() {
		try{
			if (_Not3_accuracy==null){
				_Not3_accuracy=getIntegerProperty("NoT3/accuracy");
				return _Not3_accuracy;
			}else {
				return _Not3_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT3/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot3_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT3/accuracy",v);
		_Not3_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not3_responsetime=null;

	/**
	 * @return Returns the NoT3/responseTime.
	 */
	public Integer getNot3_responsetime() {
		try{
			if (_Not3_responsetime==null){
				_Not3_responsetime=getIntegerProperty("NoT3/responseTime");
				return _Not3_responsetime;
			}else {
				return _Not3_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT3/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot3_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT3/responseTime",v);
		_Not3_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not4_accuracy=null;

	/**
	 * @return Returns the NoT4/accuracy.
	 */
	public Integer getNot4_accuracy() {
		try{
			if (_Not4_accuracy==null){
				_Not4_accuracy=getIntegerProperty("NoT4/accuracy");
				return _Not4_accuracy;
			}else {
				return _Not4_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT4/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot4_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT4/accuracy",v);
		_Not4_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not4_responsetime=null;

	/**
	 * @return Returns the NoT4/responseTime.
	 */
	public Integer getNot4_responsetime() {
		try{
			if (_Not4_responsetime==null){
				_Not4_responsetime=getIntegerProperty("NoT4/responseTime");
				return _Not4_responsetime;
			}else {
				return _Not4_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT4/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot4_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT4/responseTime",v);
		_Not4_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not5_accuracy=null;

	/**
	 * @return Returns the NoT5/accuracy.
	 */
	public Integer getNot5_accuracy() {
		try{
			if (_Not5_accuracy==null){
				_Not5_accuracy=getIntegerProperty("NoT5/accuracy");
				return _Not5_accuracy;
			}else {
				return _Not5_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT5/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot5_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT5/accuracy",v);
		_Not5_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not5_responsetime=null;

	/**
	 * @return Returns the NoT5/responseTime.
	 */
	public Integer getNot5_responsetime() {
		try{
			if (_Not5_responsetime==null){
				_Not5_responsetime=getIntegerProperty("NoT5/responseTime");
				return _Not5_responsetime;
			}else {
				return _Not5_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT5/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot5_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT5/responseTime",v);
		_Not5_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not6_accuracy=null;

	/**
	 * @return Returns the NoT6/accuracy.
	 */
	public Integer getNot6_accuracy() {
		try{
			if (_Not6_accuracy==null){
				_Not6_accuracy=getIntegerProperty("NoT6/accuracy");
				return _Not6_accuracy;
			}else {
				return _Not6_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT6/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot6_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT6/accuracy",v);
		_Not6_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not6_responsetime=null;

	/**
	 * @return Returns the NoT6/responseTime.
	 */
	public Integer getNot6_responsetime() {
		try{
			if (_Not6_responsetime==null){
				_Not6_responsetime=getIntegerProperty("NoT6/responseTime");
				return _Not6_responsetime;
			}else {
				return _Not6_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT6/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot6_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT6/responseTime",v);
		_Not6_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not7_accuracy=null;

	/**
	 * @return Returns the NoT7/accuracy.
	 */
	public Integer getNot7_accuracy() {
		try{
			if (_Not7_accuracy==null){
				_Not7_accuracy=getIntegerProperty("NoT7/accuracy");
				return _Not7_accuracy;
			}else {
				return _Not7_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT7/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot7_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT7/accuracy",v);
		_Not7_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not7_responsetime=null;

	/**
	 * @return Returns the NoT7/responseTime.
	 */
	public Integer getNot7_responsetime() {
		try{
			if (_Not7_responsetime==null){
				_Not7_responsetime=getIntegerProperty("NoT7/responseTime");
				return _Not7_responsetime;
			}else {
				return _Not7_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT7/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot7_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT7/responseTime",v);
		_Not7_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not8_accuracy=null;

	/**
	 * @return Returns the NoT8/accuracy.
	 */
	public Integer getNot8_accuracy() {
		try{
			if (_Not8_accuracy==null){
				_Not8_accuracy=getIntegerProperty("NoT8/accuracy");
				return _Not8_accuracy;
			}else {
				return _Not8_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT8/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot8_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT8/accuracy",v);
		_Not8_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not8_responsetime=null;

	/**
	 * @return Returns the NoT8/responseTime.
	 */
	public Integer getNot8_responsetime() {
		try{
			if (_Not8_responsetime==null){
				_Not8_responsetime=getIntegerProperty("NoT8/responseTime");
				return _Not8_responsetime;
			}else {
				return _Not8_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT8/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot8_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT8/responseTime",v);
		_Not8_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not9_accuracy=null;

	/**
	 * @return Returns the NoT9/accuracy.
	 */
	public Integer getNot9_accuracy() {
		try{
			if (_Not9_accuracy==null){
				_Not9_accuracy=getIntegerProperty("NoT9/accuracy");
				return _Not9_accuracy;
			}else {
				return _Not9_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT9/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot9_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT9/accuracy",v);
		_Not9_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not9_responsetime=null;

	/**
	 * @return Returns the NoT9/responseTime.
	 */
	public Integer getNot9_responsetime() {
		try{
			if (_Not9_responsetime==null){
				_Not9_responsetime=getIntegerProperty("NoT9/responseTime");
				return _Not9_responsetime;
			}else {
				return _Not9_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT9/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot9_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT9/responseTime",v);
		_Not9_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not10_accuracy=null;

	/**
	 * @return Returns the NoT10/accuracy.
	 */
	public Integer getNot10_accuracy() {
		try{
			if (_Not10_accuracy==null){
				_Not10_accuracy=getIntegerProperty("NoT10/accuracy");
				return _Not10_accuracy;
			}else {
				return _Not10_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT10/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot10_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT10/accuracy",v);
		_Not10_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not10_responsetime=null;

	/**
	 * @return Returns the NoT10/responseTime.
	 */
	public Integer getNot10_responsetime() {
		try{
			if (_Not10_responsetime==null){
				_Not10_responsetime=getIntegerProperty("NoT10/responseTime");
				return _Not10_responsetime;
			}else {
				return _Not10_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT10/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot10_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT10/responseTime",v);
		_Not10_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not11_accuracy=null;

	/**
	 * @return Returns the NoT11/accuracy.
	 */
	public Integer getNot11_accuracy() {
		try{
			if (_Not11_accuracy==null){
				_Not11_accuracy=getIntegerProperty("NoT11/accuracy");
				return _Not11_accuracy;
			}else {
				return _Not11_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT11/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot11_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT11/accuracy",v);
		_Not11_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not11_responsetime=null;

	/**
	 * @return Returns the NoT11/responseTime.
	 */
	public Integer getNot11_responsetime() {
		try{
			if (_Not11_responsetime==null){
				_Not11_responsetime=getIntegerProperty("NoT11/responseTime");
				return _Not11_responsetime;
			}else {
				return _Not11_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT11/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot11_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT11/responseTime",v);
		_Not11_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not12_accuracy=null;

	/**
	 * @return Returns the NoT12/accuracy.
	 */
	public Integer getNot12_accuracy() {
		try{
			if (_Not12_accuracy==null){
				_Not12_accuracy=getIntegerProperty("NoT12/accuracy");
				return _Not12_accuracy;
			}else {
				return _Not12_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT12/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot12_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT12/accuracy",v);
		_Not12_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not12_responsetime=null;

	/**
	 * @return Returns the NoT12/responseTime.
	 */
	public Integer getNot12_responsetime() {
		try{
			if (_Not12_responsetime==null){
				_Not12_responsetime=getIntegerProperty("NoT12/responseTime");
				return _Not12_responsetime;
			}else {
				return _Not12_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT12/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot12_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT12/responseTime",v);
		_Not12_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not13_accuracy=null;

	/**
	 * @return Returns the NoT13/accuracy.
	 */
	public Integer getNot13_accuracy() {
		try{
			if (_Not13_accuracy==null){
				_Not13_accuracy=getIntegerProperty("NoT13/accuracy");
				return _Not13_accuracy;
			}else {
				return _Not13_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT13/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot13_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT13/accuracy",v);
		_Not13_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not13_responsetime=null;

	/**
	 * @return Returns the NoT13/responseTime.
	 */
	public Integer getNot13_responsetime() {
		try{
			if (_Not13_responsetime==null){
				_Not13_responsetime=getIntegerProperty("NoT13/responseTime");
				return _Not13_responsetime;
			}else {
				return _Not13_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT13/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot13_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT13/responseTime",v);
		_Not13_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not14_accuracy=null;

	/**
	 * @return Returns the NoT14/accuracy.
	 */
	public Integer getNot14_accuracy() {
		try{
			if (_Not14_accuracy==null){
				_Not14_accuracy=getIntegerProperty("NoT14/accuracy");
				return _Not14_accuracy;
			}else {
				return _Not14_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT14/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot14_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT14/accuracy",v);
		_Not14_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not14_responsetime=null;

	/**
	 * @return Returns the NoT14/responseTime.
	 */
	public Integer getNot14_responsetime() {
		try{
			if (_Not14_responsetime==null){
				_Not14_responsetime=getIntegerProperty("NoT14/responseTime");
				return _Not14_responsetime;
			}else {
				return _Not14_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT14/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot14_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT14/responseTime",v);
		_Not14_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not15_accuracy=null;

	/**
	 * @return Returns the NoT15/accuracy.
	 */
	public Integer getNot15_accuracy() {
		try{
			if (_Not15_accuracy==null){
				_Not15_accuracy=getIntegerProperty("NoT15/accuracy");
				return _Not15_accuracy;
			}else {
				return _Not15_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT15/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot15_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT15/accuracy",v);
		_Not15_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not15_responsetime=null;

	/**
	 * @return Returns the NoT15/responseTime.
	 */
	public Integer getNot15_responsetime() {
		try{
			if (_Not15_responsetime==null){
				_Not15_responsetime=getIntegerProperty("NoT15/responseTime");
				return _Not15_responsetime;
			}else {
				return _Not15_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT15/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot15_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT15/responseTime",v);
		_Not15_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not16_accuracy=null;

	/**
	 * @return Returns the NoT16/accuracy.
	 */
	public Integer getNot16_accuracy() {
		try{
			if (_Not16_accuracy==null){
				_Not16_accuracy=getIntegerProperty("NoT16/accuracy");
				return _Not16_accuracy;
			}else {
				return _Not16_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT16/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot16_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT16/accuracy",v);
		_Not16_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not16_responsetime=null;

	/**
	 * @return Returns the NoT16/responseTime.
	 */
	public Integer getNot16_responsetime() {
		try{
			if (_Not16_responsetime==null){
				_Not16_responsetime=getIntegerProperty("NoT16/responseTime");
				return _Not16_responsetime;
			}else {
				return _Not16_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT16/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot16_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT16/responseTime",v);
		_Not16_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not17_accuracy=null;

	/**
	 * @return Returns the NoT17/accuracy.
	 */
	public Integer getNot17_accuracy() {
		try{
			if (_Not17_accuracy==null){
				_Not17_accuracy=getIntegerProperty("NoT17/accuracy");
				return _Not17_accuracy;
			}else {
				return _Not17_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT17/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot17_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT17/accuracy",v);
		_Not17_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not17_responsetime=null;

	/**
	 * @return Returns the NoT17/responseTime.
	 */
	public Integer getNot17_responsetime() {
		try{
			if (_Not17_responsetime==null){
				_Not17_responsetime=getIntegerProperty("NoT17/responseTime");
				return _Not17_responsetime;
			}else {
				return _Not17_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT17/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot17_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT17/responseTime",v);
		_Not17_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not18_accuracy=null;

	/**
	 * @return Returns the NoT18/accuracy.
	 */
	public Integer getNot18_accuracy() {
		try{
			if (_Not18_accuracy==null){
				_Not18_accuracy=getIntegerProperty("NoT18/accuracy");
				return _Not18_accuracy;
			}else {
				return _Not18_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT18/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot18_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT18/accuracy",v);
		_Not18_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not18_responsetime=null;

	/**
	 * @return Returns the NoT18/responseTime.
	 */
	public Integer getNot18_responsetime() {
		try{
			if (_Not18_responsetime==null){
				_Not18_responsetime=getIntegerProperty("NoT18/responseTime");
				return _Not18_responsetime;
			}else {
				return _Not18_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT18/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot18_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT18/responseTime",v);
		_Not18_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not19_accuracy=null;

	/**
	 * @return Returns the NoT19/accuracy.
	 */
	public Integer getNot19_accuracy() {
		try{
			if (_Not19_accuracy==null){
				_Not19_accuracy=getIntegerProperty("NoT19/accuracy");
				return _Not19_accuracy;
			}else {
				return _Not19_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT19/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot19_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT19/accuracy",v);
		_Not19_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not19_responsetime=null;

	/**
	 * @return Returns the NoT19/responseTime.
	 */
	public Integer getNot19_responsetime() {
		try{
			if (_Not19_responsetime==null){
				_Not19_responsetime=getIntegerProperty("NoT19/responseTime");
				return _Not19_responsetime;
			}else {
				return _Not19_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT19/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot19_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT19/responseTime",v);
		_Not19_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not20_accuracy=null;

	/**
	 * @return Returns the NoT20/accuracy.
	 */
	public Integer getNot20_accuracy() {
		try{
			if (_Not20_accuracy==null){
				_Not20_accuracy=getIntegerProperty("NoT20/accuracy");
				return _Not20_accuracy;
			}else {
				return _Not20_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT20/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot20_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT20/accuracy",v);
		_Not20_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not20_responsetime=null;

	/**
	 * @return Returns the NoT20/responseTime.
	 */
	public Integer getNot20_responsetime() {
		try{
			if (_Not20_responsetime==null){
				_Not20_responsetime=getIntegerProperty("NoT20/responseTime");
				return _Not20_responsetime;
			}else {
				return _Not20_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT20/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot20_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT20/responseTime",v);
		_Not20_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not21_accuracy=null;

	/**
	 * @return Returns the NoT21/accuracy.
	 */
	public Integer getNot21_accuracy() {
		try{
			if (_Not21_accuracy==null){
				_Not21_accuracy=getIntegerProperty("NoT21/accuracy");
				return _Not21_accuracy;
			}else {
				return _Not21_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT21/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot21_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT21/accuracy",v);
		_Not21_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not21_responsetime=null;

	/**
	 * @return Returns the NoT21/responseTime.
	 */
	public Integer getNot21_responsetime() {
		try{
			if (_Not21_responsetime==null){
				_Not21_responsetime=getIntegerProperty("NoT21/responseTime");
				return _Not21_responsetime;
			}else {
				return _Not21_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT21/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot21_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT21/responseTime",v);
		_Not21_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not22_accuracy=null;

	/**
	 * @return Returns the NoT22/accuracy.
	 */
	public Integer getNot22_accuracy() {
		try{
			if (_Not22_accuracy==null){
				_Not22_accuracy=getIntegerProperty("NoT22/accuracy");
				return _Not22_accuracy;
			}else {
				return _Not22_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT22/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot22_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT22/accuracy",v);
		_Not22_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not22_responsetime=null;

	/**
	 * @return Returns the NoT22/responseTime.
	 */
	public Integer getNot22_responsetime() {
		try{
			if (_Not22_responsetime==null){
				_Not22_responsetime=getIntegerProperty("NoT22/responseTime");
				return _Not22_responsetime;
			}else {
				return _Not22_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT22/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot22_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT22/responseTime",v);
		_Not22_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not23_accuracy=null;

	/**
	 * @return Returns the NoT23/accuracy.
	 */
	public Integer getNot23_accuracy() {
		try{
			if (_Not23_accuracy==null){
				_Not23_accuracy=getIntegerProperty("NoT23/accuracy");
				return _Not23_accuracy;
			}else {
				return _Not23_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT23/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot23_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT23/accuracy",v);
		_Not23_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not23_responsetime=null;

	/**
	 * @return Returns the NoT23/responseTime.
	 */
	public Integer getNot23_responsetime() {
		try{
			if (_Not23_responsetime==null){
				_Not23_responsetime=getIntegerProperty("NoT23/responseTime");
				return _Not23_responsetime;
			}else {
				return _Not23_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT23/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot23_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT23/responseTime",v);
		_Not23_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not24_accuracy=null;

	/**
	 * @return Returns the NoT24/accuracy.
	 */
	public Integer getNot24_accuracy() {
		try{
			if (_Not24_accuracy==null){
				_Not24_accuracy=getIntegerProperty("NoT24/accuracy");
				return _Not24_accuracy;
			}else {
				return _Not24_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT24/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot24_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT24/accuracy",v);
		_Not24_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not24_responsetime=null;

	/**
	 * @return Returns the NoT24/responseTime.
	 */
	public Integer getNot24_responsetime() {
		try{
			if (_Not24_responsetime==null){
				_Not24_responsetime=getIntegerProperty("NoT24/responseTime");
				return _Not24_responsetime;
			}else {
				return _Not24_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT24/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot24_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT24/responseTime",v);
		_Not24_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not25_accuracy=null;

	/**
	 * @return Returns the NoT25/accuracy.
	 */
	public Integer getNot25_accuracy() {
		try{
			if (_Not25_accuracy==null){
				_Not25_accuracy=getIntegerProperty("NoT25/accuracy");
				return _Not25_accuracy;
			}else {
				return _Not25_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT25/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot25_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT25/accuracy",v);
		_Not25_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not25_responsetime=null;

	/**
	 * @return Returns the NoT25/responseTime.
	 */
	public Integer getNot25_responsetime() {
		try{
			if (_Not25_responsetime==null){
				_Not25_responsetime=getIntegerProperty("NoT25/responseTime");
				return _Not25_responsetime;
			}else {
				return _Not25_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT25/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot25_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT25/responseTime",v);
		_Not25_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not26_accuracy=null;

	/**
	 * @return Returns the NoT26/accuracy.
	 */
	public Integer getNot26_accuracy() {
		try{
			if (_Not26_accuracy==null){
				_Not26_accuracy=getIntegerProperty("NoT26/accuracy");
				return _Not26_accuracy;
			}else {
				return _Not26_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT26/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot26_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT26/accuracy",v);
		_Not26_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not26_responsetime=null;

	/**
	 * @return Returns the NoT26/responseTime.
	 */
	public Integer getNot26_responsetime() {
		try{
			if (_Not26_responsetime==null){
				_Not26_responsetime=getIntegerProperty("NoT26/responseTime");
				return _Not26_responsetime;
			}else {
				return _Not26_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT26/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot26_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT26/responseTime",v);
		_Not26_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not27_accuracy=null;

	/**
	 * @return Returns the NoT27/accuracy.
	 */
	public Integer getNot27_accuracy() {
		try{
			if (_Not27_accuracy==null){
				_Not27_accuracy=getIntegerProperty("NoT27/accuracy");
				return _Not27_accuracy;
			}else {
				return _Not27_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT27/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot27_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT27/accuracy",v);
		_Not27_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not27_responsetime=null;

	/**
	 * @return Returns the NoT27/responseTime.
	 */
	public Integer getNot27_responsetime() {
		try{
			if (_Not27_responsetime==null){
				_Not27_responsetime=getIntegerProperty("NoT27/responseTime");
				return _Not27_responsetime;
			}else {
				return _Not27_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT27/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot27_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT27/responseTime",v);
		_Not27_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not28_accuracy=null;

	/**
	 * @return Returns the NoT28/accuracy.
	 */
	public Integer getNot28_accuracy() {
		try{
			if (_Not28_accuracy==null){
				_Not28_accuracy=getIntegerProperty("NoT28/accuracy");
				return _Not28_accuracy;
			}else {
				return _Not28_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT28/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot28_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT28/accuracy",v);
		_Not28_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not28_responsetime=null;

	/**
	 * @return Returns the NoT28/responseTime.
	 */
	public Integer getNot28_responsetime() {
		try{
			if (_Not28_responsetime==null){
				_Not28_responsetime=getIntegerProperty("NoT28/responseTime");
				return _Not28_responsetime;
			}else {
				return _Not28_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT28/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot28_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT28/responseTime",v);
		_Not28_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not29_accuracy=null;

	/**
	 * @return Returns the NoT29/accuracy.
	 */
	public Integer getNot29_accuracy() {
		try{
			if (_Not29_accuracy==null){
				_Not29_accuracy=getIntegerProperty("NoT29/accuracy");
				return _Not29_accuracy;
			}else {
				return _Not29_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT29/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot29_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT29/accuracy",v);
		_Not29_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not29_responsetime=null;

	/**
	 * @return Returns the NoT29/responseTime.
	 */
	public Integer getNot29_responsetime() {
		try{
			if (_Not29_responsetime==null){
				_Not29_responsetime=getIntegerProperty("NoT29/responseTime");
				return _Not29_responsetime;
			}else {
				return _Not29_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT29/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot29_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT29/responseTime",v);
		_Not29_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not30_accuracy=null;

	/**
	 * @return Returns the NoT30/accuracy.
	 */
	public Integer getNot30_accuracy() {
		try{
			if (_Not30_accuracy==null){
				_Not30_accuracy=getIntegerProperty("NoT30/accuracy");
				return _Not30_accuracy;
			}else {
				return _Not30_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT30/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot30_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT30/accuracy",v);
		_Not30_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not30_responsetime=null;

	/**
	 * @return Returns the NoT30/responseTime.
	 */
	public Integer getNot30_responsetime() {
		try{
			if (_Not30_responsetime==null){
				_Not30_responsetime=getIntegerProperty("NoT30/responseTime");
				return _Not30_responsetime;
			}else {
				return _Not30_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT30/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot30_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT30/responseTime",v);
		_Not30_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not31_accuracy=null;

	/**
	 * @return Returns the NoT31/accuracy.
	 */
	public Integer getNot31_accuracy() {
		try{
			if (_Not31_accuracy==null){
				_Not31_accuracy=getIntegerProperty("NoT31/accuracy");
				return _Not31_accuracy;
			}else {
				return _Not31_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT31/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot31_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT31/accuracy",v);
		_Not31_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not31_responsetime=null;

	/**
	 * @return Returns the NoT31/responseTime.
	 */
	public Integer getNot31_responsetime() {
		try{
			if (_Not31_responsetime==null){
				_Not31_responsetime=getIntegerProperty("NoT31/responseTime");
				return _Not31_responsetime;
			}else {
				return _Not31_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT31/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot31_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT31/responseTime",v);
		_Not31_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not32_accuracy=null;

	/**
	 * @return Returns the NoT32/accuracy.
	 */
	public Integer getNot32_accuracy() {
		try{
			if (_Not32_accuracy==null){
				_Not32_accuracy=getIntegerProperty("NoT32/accuracy");
				return _Not32_accuracy;
			}else {
				return _Not32_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT32/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot32_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT32/accuracy",v);
		_Not32_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not32_responsetime=null;

	/**
	 * @return Returns the NoT32/responseTime.
	 */
	public Integer getNot32_responsetime() {
		try{
			if (_Not32_responsetime==null){
				_Not32_responsetime=getIntegerProperty("NoT32/responseTime");
				return _Not32_responsetime;
			}else {
				return _Not32_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT32/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot32_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT32/responseTime",v);
		_Not32_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not33_accuracy=null;

	/**
	 * @return Returns the NoT33/accuracy.
	 */
	public Integer getNot33_accuracy() {
		try{
			if (_Not33_accuracy==null){
				_Not33_accuracy=getIntegerProperty("NoT33/accuracy");
				return _Not33_accuracy;
			}else {
				return _Not33_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT33/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot33_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT33/accuracy",v);
		_Not33_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not33_responsetime=null;

	/**
	 * @return Returns the NoT33/responseTime.
	 */
	public Integer getNot33_responsetime() {
		try{
			if (_Not33_responsetime==null){
				_Not33_responsetime=getIntegerProperty("NoT33/responseTime");
				return _Not33_responsetime;
			}else {
				return _Not33_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT33/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot33_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT33/responseTime",v);
		_Not33_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not34_accuracy=null;

	/**
	 * @return Returns the NoT34/accuracy.
	 */
	public Integer getNot34_accuracy() {
		try{
			if (_Not34_accuracy==null){
				_Not34_accuracy=getIntegerProperty("NoT34/accuracy");
				return _Not34_accuracy;
			}else {
				return _Not34_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT34/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot34_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT34/accuracy",v);
		_Not34_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not34_responsetime=null;

	/**
	 * @return Returns the NoT34/responseTime.
	 */
	public Integer getNot34_responsetime() {
		try{
			if (_Not34_responsetime==null){
				_Not34_responsetime=getIntegerProperty("NoT34/responseTime");
				return _Not34_responsetime;
			}else {
				return _Not34_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT34/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot34_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT34/responseTime",v);
		_Not34_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not35_accuracy=null;

	/**
	 * @return Returns the NoT35/accuracy.
	 */
	public Integer getNot35_accuracy() {
		try{
			if (_Not35_accuracy==null){
				_Not35_accuracy=getIntegerProperty("NoT35/accuracy");
				return _Not35_accuracy;
			}else {
				return _Not35_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT35/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot35_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT35/accuracy",v);
		_Not35_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not35_responsetime=null;

	/**
	 * @return Returns the NoT35/responseTime.
	 */
	public Integer getNot35_responsetime() {
		try{
			if (_Not35_responsetime==null){
				_Not35_responsetime=getIntegerProperty("NoT35/responseTime");
				return _Not35_responsetime;
			}else {
				return _Not35_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT35/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot35_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT35/responseTime",v);
		_Not35_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not36_accuracy=null;

	/**
	 * @return Returns the NoT36/accuracy.
	 */
	public Integer getNot36_accuracy() {
		try{
			if (_Not36_accuracy==null){
				_Not36_accuracy=getIntegerProperty("NoT36/accuracy");
				return _Not36_accuracy;
			}else {
				return _Not36_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT36/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot36_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT36/accuracy",v);
		_Not36_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not36_responsetime=null;

	/**
	 * @return Returns the NoT36/responseTime.
	 */
	public Integer getNot36_responsetime() {
		try{
			if (_Not36_responsetime==null){
				_Not36_responsetime=getIntegerProperty("NoT36/responseTime");
				return _Not36_responsetime;
			}else {
				return _Not36_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT36/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot36_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT36/responseTime",v);
		_Not36_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not37_accuracy=null;

	/**
	 * @return Returns the NoT37/accuracy.
	 */
	public Integer getNot37_accuracy() {
		try{
			if (_Not37_accuracy==null){
				_Not37_accuracy=getIntegerProperty("NoT37/accuracy");
				return _Not37_accuracy;
			}else {
				return _Not37_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT37/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot37_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT37/accuracy",v);
		_Not37_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not37_responsetime=null;

	/**
	 * @return Returns the NoT37/responseTime.
	 */
	public Integer getNot37_responsetime() {
		try{
			if (_Not37_responsetime==null){
				_Not37_responsetime=getIntegerProperty("NoT37/responseTime");
				return _Not37_responsetime;
			}else {
				return _Not37_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT37/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot37_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT37/responseTime",v);
		_Not37_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not38_accuracy=null;

	/**
	 * @return Returns the NoT38/accuracy.
	 */
	public Integer getNot38_accuracy() {
		try{
			if (_Not38_accuracy==null){
				_Not38_accuracy=getIntegerProperty("NoT38/accuracy");
				return _Not38_accuracy;
			}else {
				return _Not38_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT38/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot38_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT38/accuracy",v);
		_Not38_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not38_responsetime=null;

	/**
	 * @return Returns the NoT38/responseTime.
	 */
	public Integer getNot38_responsetime() {
		try{
			if (_Not38_responsetime==null){
				_Not38_responsetime=getIntegerProperty("NoT38/responseTime");
				return _Not38_responsetime;
			}else {
				return _Not38_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT38/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot38_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT38/responseTime",v);
		_Not38_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not39_accuracy=null;

	/**
	 * @return Returns the NoT39/accuracy.
	 */
	public Integer getNot39_accuracy() {
		try{
			if (_Not39_accuracy==null){
				_Not39_accuracy=getIntegerProperty("NoT39/accuracy");
				return _Not39_accuracy;
			}else {
				return _Not39_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT39/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot39_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT39/accuracy",v);
		_Not39_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not39_responsetime=null;

	/**
	 * @return Returns the NoT39/responseTime.
	 */
	public Integer getNot39_responsetime() {
		try{
			if (_Not39_responsetime==null){
				_Not39_responsetime=getIntegerProperty("NoT39/responseTime");
				return _Not39_responsetime;
			}else {
				return _Not39_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT39/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot39_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT39/responseTime",v);
		_Not39_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not40_accuracy=null;

	/**
	 * @return Returns the NoT40/accuracy.
	 */
	public Integer getNot40_accuracy() {
		try{
			if (_Not40_accuracy==null){
				_Not40_accuracy=getIntegerProperty("NoT40/accuracy");
				return _Not40_accuracy;
			}else {
				return _Not40_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT40/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot40_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT40/accuracy",v);
		_Not40_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not40_responsetime=null;

	/**
	 * @return Returns the NoT40/responseTime.
	 */
	public Integer getNot40_responsetime() {
		try{
			if (_Not40_responsetime==null){
				_Not40_responsetime=getIntegerProperty("NoT40/responseTime");
				return _Not40_responsetime;
			}else {
				return _Not40_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT40/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot40_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT40/responseTime",v);
		_Not40_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not41_accuracy=null;

	/**
	 * @return Returns the NoT41/accuracy.
	 */
	public Integer getNot41_accuracy() {
		try{
			if (_Not41_accuracy==null){
				_Not41_accuracy=getIntegerProperty("NoT41/accuracy");
				return _Not41_accuracy;
			}else {
				return _Not41_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT41/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot41_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT41/accuracy",v);
		_Not41_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not41_responsetime=null;

	/**
	 * @return Returns the NoT41/responseTime.
	 */
	public Integer getNot41_responsetime() {
		try{
			if (_Not41_responsetime==null){
				_Not41_responsetime=getIntegerProperty("NoT41/responseTime");
				return _Not41_responsetime;
			}else {
				return _Not41_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT41/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot41_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT41/responseTime",v);
		_Not41_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not42_accuracy=null;

	/**
	 * @return Returns the NoT42/accuracy.
	 */
	public Integer getNot42_accuracy() {
		try{
			if (_Not42_accuracy==null){
				_Not42_accuracy=getIntegerProperty("NoT42/accuracy");
				return _Not42_accuracy;
			}else {
				return _Not42_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT42/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot42_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT42/accuracy",v);
		_Not42_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not42_responsetime=null;

	/**
	 * @return Returns the NoT42/responseTime.
	 */
	public Integer getNot42_responsetime() {
		try{
			if (_Not42_responsetime==null){
				_Not42_responsetime=getIntegerProperty("NoT42/responseTime");
				return _Not42_responsetime;
			}else {
				return _Not42_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT42/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot42_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT42/responseTime",v);
		_Not42_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not43_accuracy=null;

	/**
	 * @return Returns the NoT43/accuracy.
	 */
	public Integer getNot43_accuracy() {
		try{
			if (_Not43_accuracy==null){
				_Not43_accuracy=getIntegerProperty("NoT43/accuracy");
				return _Not43_accuracy;
			}else {
				return _Not43_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT43/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot43_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT43/accuracy",v);
		_Not43_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not43_responsetime=null;

	/**
	 * @return Returns the NoT43/responseTime.
	 */
	public Integer getNot43_responsetime() {
		try{
			if (_Not43_responsetime==null){
				_Not43_responsetime=getIntegerProperty("NoT43/responseTime");
				return _Not43_responsetime;
			}else {
				return _Not43_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT43/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot43_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT43/responseTime",v);
		_Not43_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not44_accuracy=null;

	/**
	 * @return Returns the NoT44/accuracy.
	 */
	public Integer getNot44_accuracy() {
		try{
			if (_Not44_accuracy==null){
				_Not44_accuracy=getIntegerProperty("NoT44/accuracy");
				return _Not44_accuracy;
			}else {
				return _Not44_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT44/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot44_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT44/accuracy",v);
		_Not44_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not44_responsetime=null;

	/**
	 * @return Returns the NoT44/responseTime.
	 */
	public Integer getNot44_responsetime() {
		try{
			if (_Not44_responsetime==null){
				_Not44_responsetime=getIntegerProperty("NoT44/responseTime");
				return _Not44_responsetime;
			}else {
				return _Not44_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT44/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot44_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT44/responseTime",v);
		_Not44_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not45_accuracy=null;

	/**
	 * @return Returns the NoT45/accuracy.
	 */
	public Integer getNot45_accuracy() {
		try{
			if (_Not45_accuracy==null){
				_Not45_accuracy=getIntegerProperty("NoT45/accuracy");
				return _Not45_accuracy;
			}else {
				return _Not45_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT45/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot45_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT45/accuracy",v);
		_Not45_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not45_responsetime=null;

	/**
	 * @return Returns the NoT45/responseTime.
	 */
	public Integer getNot45_responsetime() {
		try{
			if (_Not45_responsetime==null){
				_Not45_responsetime=getIntegerProperty("NoT45/responseTime");
				return _Not45_responsetime;
			}else {
				return _Not45_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT45/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot45_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT45/responseTime",v);
		_Not45_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not46_accuracy=null;

	/**
	 * @return Returns the NoT46/accuracy.
	 */
	public Integer getNot46_accuracy() {
		try{
			if (_Not46_accuracy==null){
				_Not46_accuracy=getIntegerProperty("NoT46/accuracy");
				return _Not46_accuracy;
			}else {
				return _Not46_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT46/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot46_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT46/accuracy",v);
		_Not46_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not46_responsetime=null;

	/**
	 * @return Returns the NoT46/responseTime.
	 */
	public Integer getNot46_responsetime() {
		try{
			if (_Not46_responsetime==null){
				_Not46_responsetime=getIntegerProperty("NoT46/responseTime");
				return _Not46_responsetime;
			}else {
				return _Not46_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT46/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot46_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT46/responseTime",v);
		_Not46_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not47_accuracy=null;

	/**
	 * @return Returns the NoT47/accuracy.
	 */
	public Integer getNot47_accuracy() {
		try{
			if (_Not47_accuracy==null){
				_Not47_accuracy=getIntegerProperty("NoT47/accuracy");
				return _Not47_accuracy;
			}else {
				return _Not47_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT47/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot47_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT47/accuracy",v);
		_Not47_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not47_responsetime=null;

	/**
	 * @return Returns the NoT47/responseTime.
	 */
	public Integer getNot47_responsetime() {
		try{
			if (_Not47_responsetime==null){
				_Not47_responsetime=getIntegerProperty("NoT47/responseTime");
				return _Not47_responsetime;
			}else {
				return _Not47_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT47/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot47_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT47/responseTime",v);
		_Not47_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not48_accuracy=null;

	/**
	 * @return Returns the NoT48/accuracy.
	 */
	public Integer getNot48_accuracy() {
		try{
			if (_Not48_accuracy==null){
				_Not48_accuracy=getIntegerProperty("NoT48/accuracy");
				return _Not48_accuracy;
			}else {
				return _Not48_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT48/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot48_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT48/accuracy",v);
		_Not48_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not48_responsetime=null;

	/**
	 * @return Returns the NoT48/responseTime.
	 */
	public Integer getNot48_responsetime() {
		try{
			if (_Not48_responsetime==null){
				_Not48_responsetime=getIntegerProperty("NoT48/responseTime");
				return _Not48_responsetime;
			}else {
				return _Not48_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT48/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot48_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT48/responseTime",v);
		_Not48_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not49_accuracy=null;

	/**
	 * @return Returns the NoT49/accuracy.
	 */
	public Integer getNot49_accuracy() {
		try{
			if (_Not49_accuracy==null){
				_Not49_accuracy=getIntegerProperty("NoT49/accuracy");
				return _Not49_accuracy;
			}else {
				return _Not49_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT49/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot49_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT49/accuracy",v);
		_Not49_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not49_responsetime=null;

	/**
	 * @return Returns the NoT49/responseTime.
	 */
	public Integer getNot49_responsetime() {
		try{
			if (_Not49_responsetime==null){
				_Not49_responsetime=getIntegerProperty("NoT49/responseTime");
				return _Not49_responsetime;
			}else {
				return _Not49_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT49/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot49_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT49/responseTime",v);
		_Not49_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not50_accuracy=null;

	/**
	 * @return Returns the NoT50/accuracy.
	 */
	public Integer getNot50_accuracy() {
		try{
			if (_Not50_accuracy==null){
				_Not50_accuracy=getIntegerProperty("NoT50/accuracy");
				return _Not50_accuracy;
			}else {
				return _Not50_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT50/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot50_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT50/accuracy",v);
		_Not50_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not50_responsetime=null;

	/**
	 * @return Returns the NoT50/responseTime.
	 */
	public Integer getNot50_responsetime() {
		try{
			if (_Not50_responsetime==null){
				_Not50_responsetime=getIntegerProperty("NoT50/responseTime");
				return _Not50_responsetime;
			}else {
				return _Not50_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT50/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot50_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT50/responseTime",v);
		_Not50_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not51_accuracy=null;

	/**
	 * @return Returns the NoT51/accuracy.
	 */
	public Integer getNot51_accuracy() {
		try{
			if (_Not51_accuracy==null){
				_Not51_accuracy=getIntegerProperty("NoT51/accuracy");
				return _Not51_accuracy;
			}else {
				return _Not51_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT51/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot51_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT51/accuracy",v);
		_Not51_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not51_responsetime=null;

	/**
	 * @return Returns the NoT51/responseTime.
	 */
	public Integer getNot51_responsetime() {
		try{
			if (_Not51_responsetime==null){
				_Not51_responsetime=getIntegerProperty("NoT51/responseTime");
				return _Not51_responsetime;
			}else {
				return _Not51_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT51/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot51_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT51/responseTime",v);
		_Not51_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not52_accuracy=null;

	/**
	 * @return Returns the NoT52/accuracy.
	 */
	public Integer getNot52_accuracy() {
		try{
			if (_Not52_accuracy==null){
				_Not52_accuracy=getIntegerProperty("NoT52/accuracy");
				return _Not52_accuracy;
			}else {
				return _Not52_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT52/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot52_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT52/accuracy",v);
		_Not52_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not52_responsetime=null;

	/**
	 * @return Returns the NoT52/responseTime.
	 */
	public Integer getNot52_responsetime() {
		try{
			if (_Not52_responsetime==null){
				_Not52_responsetime=getIntegerProperty("NoT52/responseTime");
				return _Not52_responsetime;
			}else {
				return _Not52_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT52/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot52_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT52/responseTime",v);
		_Not52_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not53_accuracy=null;

	/**
	 * @return Returns the NoT53/accuracy.
	 */
	public Integer getNot53_accuracy() {
		try{
			if (_Not53_accuracy==null){
				_Not53_accuracy=getIntegerProperty("NoT53/accuracy");
				return _Not53_accuracy;
			}else {
				return _Not53_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT53/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot53_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT53/accuracy",v);
		_Not53_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not53_responsetime=null;

	/**
	 * @return Returns the NoT53/responseTime.
	 */
	public Integer getNot53_responsetime() {
		try{
			if (_Not53_responsetime==null){
				_Not53_responsetime=getIntegerProperty("NoT53/responseTime");
				return _Not53_responsetime;
			}else {
				return _Not53_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT53/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot53_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT53/responseTime",v);
		_Not53_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not54_accuracy=null;

	/**
	 * @return Returns the NoT54/accuracy.
	 */
	public Integer getNot54_accuracy() {
		try{
			if (_Not54_accuracy==null){
				_Not54_accuracy=getIntegerProperty("NoT54/accuracy");
				return _Not54_accuracy;
			}else {
				return _Not54_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT54/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot54_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT54/accuracy",v);
		_Not54_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not54_responsetime=null;

	/**
	 * @return Returns the NoT54/responseTime.
	 */
	public Integer getNot54_responsetime() {
		try{
			if (_Not54_responsetime==null){
				_Not54_responsetime=getIntegerProperty("NoT54/responseTime");
				return _Not54_responsetime;
			}else {
				return _Not54_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT54/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot54_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT54/responseTime",v);
		_Not54_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not55_accuracy=null;

	/**
	 * @return Returns the NoT55/accuracy.
	 */
	public Integer getNot55_accuracy() {
		try{
			if (_Not55_accuracy==null){
				_Not55_accuracy=getIntegerProperty("NoT55/accuracy");
				return _Not55_accuracy;
			}else {
				return _Not55_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT55/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot55_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT55/accuracy",v);
		_Not55_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not55_responsetime=null;

	/**
	 * @return Returns the NoT55/responseTime.
	 */
	public Integer getNot55_responsetime() {
		try{
			if (_Not55_responsetime==null){
				_Not55_responsetime=getIntegerProperty("NoT55/responseTime");
				return _Not55_responsetime;
			}else {
				return _Not55_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT55/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot55_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT55/responseTime",v);
		_Not55_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not56_accuracy=null;

	/**
	 * @return Returns the NoT56/accuracy.
	 */
	public Integer getNot56_accuracy() {
		try{
			if (_Not56_accuracy==null){
				_Not56_accuracy=getIntegerProperty("NoT56/accuracy");
				return _Not56_accuracy;
			}else {
				return _Not56_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT56/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot56_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT56/accuracy",v);
		_Not56_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not56_responsetime=null;

	/**
	 * @return Returns the NoT56/responseTime.
	 */
	public Integer getNot56_responsetime() {
		try{
			if (_Not56_responsetime==null){
				_Not56_responsetime=getIntegerProperty("NoT56/responseTime");
				return _Not56_responsetime;
			}else {
				return _Not56_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT56/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot56_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT56/responseTime",v);
		_Not56_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not57_accuracy=null;

	/**
	 * @return Returns the NoT57/accuracy.
	 */
	public Integer getNot57_accuracy() {
		try{
			if (_Not57_accuracy==null){
				_Not57_accuracy=getIntegerProperty("NoT57/accuracy");
				return _Not57_accuracy;
			}else {
				return _Not57_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT57/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot57_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT57/accuracy",v);
		_Not57_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not57_responsetime=null;

	/**
	 * @return Returns the NoT57/responseTime.
	 */
	public Integer getNot57_responsetime() {
		try{
			if (_Not57_responsetime==null){
				_Not57_responsetime=getIntegerProperty("NoT57/responseTime");
				return _Not57_responsetime;
			}else {
				return _Not57_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT57/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot57_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT57/responseTime",v);
		_Not57_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not58_accuracy=null;

	/**
	 * @return Returns the NoT58/accuracy.
	 */
	public Integer getNot58_accuracy() {
		try{
			if (_Not58_accuracy==null){
				_Not58_accuracy=getIntegerProperty("NoT58/accuracy");
				return _Not58_accuracy;
			}else {
				return _Not58_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT58/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot58_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT58/accuracy",v);
		_Not58_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not58_responsetime=null;

	/**
	 * @return Returns the NoT58/responseTime.
	 */
	public Integer getNot58_responsetime() {
		try{
			if (_Not58_responsetime==null){
				_Not58_responsetime=getIntegerProperty("NoT58/responseTime");
				return _Not58_responsetime;
			}else {
				return _Not58_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT58/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot58_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT58/responseTime",v);
		_Not58_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not59_accuracy=null;

	/**
	 * @return Returns the NoT59/accuracy.
	 */
	public Integer getNot59_accuracy() {
		try{
			if (_Not59_accuracy==null){
				_Not59_accuracy=getIntegerProperty("NoT59/accuracy");
				return _Not59_accuracy;
			}else {
				return _Not59_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT59/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot59_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT59/accuracy",v);
		_Not59_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not59_responsetime=null;

	/**
	 * @return Returns the NoT59/responseTime.
	 */
	public Integer getNot59_responsetime() {
		try{
			if (_Not59_responsetime==null){
				_Not59_responsetime=getIntegerProperty("NoT59/responseTime");
				return _Not59_responsetime;
			}else {
				return _Not59_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT59/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot59_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT59/responseTime",v);
		_Not59_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not60_accuracy=null;

	/**
	 * @return Returns the NoT60/accuracy.
	 */
	public Integer getNot60_accuracy() {
		try{
			if (_Not60_accuracy==null){
				_Not60_accuracy=getIntegerProperty("NoT60/accuracy");
				return _Not60_accuracy;
			}else {
				return _Not60_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT60/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot60_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT60/accuracy",v);
		_Not60_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not60_responsetime=null;

	/**
	 * @return Returns the NoT60/responseTime.
	 */
	public Integer getNot60_responsetime() {
		try{
			if (_Not60_responsetime==null){
				_Not60_responsetime=getIntegerProperty("NoT60/responseTime");
				return _Not60_responsetime;
			}else {
				return _Not60_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT60/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot60_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT60/responseTime",v);
		_Not60_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not61_accuracy=null;

	/**
	 * @return Returns the NoT61/accuracy.
	 */
	public Integer getNot61_accuracy() {
		try{
			if (_Not61_accuracy==null){
				_Not61_accuracy=getIntegerProperty("NoT61/accuracy");
				return _Not61_accuracy;
			}else {
				return _Not61_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT61/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot61_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT61/accuracy",v);
		_Not61_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not61_responsetime=null;

	/**
	 * @return Returns the NoT61/responseTime.
	 */
	public Integer getNot61_responsetime() {
		try{
			if (_Not61_responsetime==null){
				_Not61_responsetime=getIntegerProperty("NoT61/responseTime");
				return _Not61_responsetime;
			}else {
				return _Not61_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT61/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot61_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT61/responseTime",v);
		_Not61_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not62_accuracy=null;

	/**
	 * @return Returns the NoT62/accuracy.
	 */
	public Integer getNot62_accuracy() {
		try{
			if (_Not62_accuracy==null){
				_Not62_accuracy=getIntegerProperty("NoT62/accuracy");
				return _Not62_accuracy;
			}else {
				return _Not62_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT62/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot62_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT62/accuracy",v);
		_Not62_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not62_responsetime=null;

	/**
	 * @return Returns the NoT62/responseTime.
	 */
	public Integer getNot62_responsetime() {
		try{
			if (_Not62_responsetime==null){
				_Not62_responsetime=getIntegerProperty("NoT62/responseTime");
				return _Not62_responsetime;
			}else {
				return _Not62_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT62/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot62_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT62/responseTime",v);
		_Not62_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not63_accuracy=null;

	/**
	 * @return Returns the NoT63/accuracy.
	 */
	public Integer getNot63_accuracy() {
		try{
			if (_Not63_accuracy==null){
				_Not63_accuracy=getIntegerProperty("NoT63/accuracy");
				return _Not63_accuracy;
			}else {
				return _Not63_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT63/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot63_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT63/accuracy",v);
		_Not63_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not63_responsetime=null;

	/**
	 * @return Returns the NoT63/responseTime.
	 */
	public Integer getNot63_responsetime() {
		try{
			if (_Not63_responsetime==null){
				_Not63_responsetime=getIntegerProperty("NoT63/responseTime");
				return _Not63_responsetime;
			}else {
				return _Not63_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT63/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot63_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT63/responseTime",v);
		_Not63_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not64_accuracy=null;

	/**
	 * @return Returns the NoT64/accuracy.
	 */
	public Integer getNot64_accuracy() {
		try{
			if (_Not64_accuracy==null){
				_Not64_accuracy=getIntegerProperty("NoT64/accuracy");
				return _Not64_accuracy;
			}else {
				return _Not64_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT64/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot64_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT64/accuracy",v);
		_Not64_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not64_responsetime=null;

	/**
	 * @return Returns the NoT64/responseTime.
	 */
	public Integer getNot64_responsetime() {
		try{
			if (_Not64_responsetime==null){
				_Not64_responsetime=getIntegerProperty("NoT64/responseTime");
				return _Not64_responsetime;
			}else {
				return _Not64_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT64/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot64_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT64/responseTime",v);
		_Not64_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not65_accuracy=null;

	/**
	 * @return Returns the NoT65/accuracy.
	 */
	public Integer getNot65_accuracy() {
		try{
			if (_Not65_accuracy==null){
				_Not65_accuracy=getIntegerProperty("NoT65/accuracy");
				return _Not65_accuracy;
			}else {
				return _Not65_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT65/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot65_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT65/accuracy",v);
		_Not65_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not65_responsetime=null;

	/**
	 * @return Returns the NoT65/responseTime.
	 */
	public Integer getNot65_responsetime() {
		try{
			if (_Not65_responsetime==null){
				_Not65_responsetime=getIntegerProperty("NoT65/responseTime");
				return _Not65_responsetime;
			}else {
				return _Not65_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT65/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot65_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT65/responseTime",v);
		_Not65_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not66_accuracy=null;

	/**
	 * @return Returns the NoT66/accuracy.
	 */
	public Integer getNot66_accuracy() {
		try{
			if (_Not66_accuracy==null){
				_Not66_accuracy=getIntegerProperty("NoT66/accuracy");
				return _Not66_accuracy;
			}else {
				return _Not66_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT66/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot66_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT66/accuracy",v);
		_Not66_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not66_responsetime=null;

	/**
	 * @return Returns the NoT66/responseTime.
	 */
	public Integer getNot66_responsetime() {
		try{
			if (_Not66_responsetime==null){
				_Not66_responsetime=getIntegerProperty("NoT66/responseTime");
				return _Not66_responsetime;
			}else {
				return _Not66_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT66/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot66_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT66/responseTime",v);
		_Not66_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not67_accuracy=null;

	/**
	 * @return Returns the NoT67/accuracy.
	 */
	public Integer getNot67_accuracy() {
		try{
			if (_Not67_accuracy==null){
				_Not67_accuracy=getIntegerProperty("NoT67/accuracy");
				return _Not67_accuracy;
			}else {
				return _Not67_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT67/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot67_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT67/accuracy",v);
		_Not67_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not67_responsetime=null;

	/**
	 * @return Returns the NoT67/responseTime.
	 */
	public Integer getNot67_responsetime() {
		try{
			if (_Not67_responsetime==null){
				_Not67_responsetime=getIntegerProperty("NoT67/responseTime");
				return _Not67_responsetime;
			}else {
				return _Not67_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT67/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot67_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT67/responseTime",v);
		_Not67_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not68_accuracy=null;

	/**
	 * @return Returns the NoT68/accuracy.
	 */
	public Integer getNot68_accuracy() {
		try{
			if (_Not68_accuracy==null){
				_Not68_accuracy=getIntegerProperty("NoT68/accuracy");
				return _Not68_accuracy;
			}else {
				return _Not68_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT68/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot68_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT68/accuracy",v);
		_Not68_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not68_responsetime=null;

	/**
	 * @return Returns the NoT68/responseTime.
	 */
	public Integer getNot68_responsetime() {
		try{
			if (_Not68_responsetime==null){
				_Not68_responsetime=getIntegerProperty("NoT68/responseTime");
				return _Not68_responsetime;
			}else {
				return _Not68_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT68/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot68_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT68/responseTime",v);
		_Not68_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not69_accuracy=null;

	/**
	 * @return Returns the NoT69/accuracy.
	 */
	public Integer getNot69_accuracy() {
		try{
			if (_Not69_accuracy==null){
				_Not69_accuracy=getIntegerProperty("NoT69/accuracy");
				return _Not69_accuracy;
			}else {
				return _Not69_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT69/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot69_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT69/accuracy",v);
		_Not69_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not69_responsetime=null;

	/**
	 * @return Returns the NoT69/responseTime.
	 */
	public Integer getNot69_responsetime() {
		try{
			if (_Not69_responsetime==null){
				_Not69_responsetime=getIntegerProperty("NoT69/responseTime");
				return _Not69_responsetime;
			}else {
				return _Not69_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT69/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot69_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT69/responseTime",v);
		_Not69_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not70_accuracy=null;

	/**
	 * @return Returns the NoT70/accuracy.
	 */
	public Integer getNot70_accuracy() {
		try{
			if (_Not70_accuracy==null){
				_Not70_accuracy=getIntegerProperty("NoT70/accuracy");
				return _Not70_accuracy;
			}else {
				return _Not70_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT70/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot70_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT70/accuracy",v);
		_Not70_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not70_responsetime=null;

	/**
	 * @return Returns the NoT70/responseTime.
	 */
	public Integer getNot70_responsetime() {
		try{
			if (_Not70_responsetime==null){
				_Not70_responsetime=getIntegerProperty("NoT70/responseTime");
				return _Not70_responsetime;
			}else {
				return _Not70_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT70/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot70_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT70/responseTime",v);
		_Not70_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not71_accuracy=null;

	/**
	 * @return Returns the NoT71/accuracy.
	 */
	public Integer getNot71_accuracy() {
		try{
			if (_Not71_accuracy==null){
				_Not71_accuracy=getIntegerProperty("NoT71/accuracy");
				return _Not71_accuracy;
			}else {
				return _Not71_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT71/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot71_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT71/accuracy",v);
		_Not71_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not71_responsetime=null;

	/**
	 * @return Returns the NoT71/responseTime.
	 */
	public Integer getNot71_responsetime() {
		try{
			if (_Not71_responsetime==null){
				_Not71_responsetime=getIntegerProperty("NoT71/responseTime");
				return _Not71_responsetime;
			}else {
				return _Not71_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT71/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot71_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT71/responseTime",v);
		_Not71_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not72_accuracy=null;

	/**
	 * @return Returns the NoT72/accuracy.
	 */
	public Integer getNot72_accuracy() {
		try{
			if (_Not72_accuracy==null){
				_Not72_accuracy=getIntegerProperty("NoT72/accuracy");
				return _Not72_accuracy;
			}else {
				return _Not72_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT72/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot72_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT72/accuracy",v);
		_Not72_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not72_responsetime=null;

	/**
	 * @return Returns the NoT72/responseTime.
	 */
	public Integer getNot72_responsetime() {
		try{
			if (_Not72_responsetime==null){
				_Not72_responsetime=getIntegerProperty("NoT72/responseTime");
				return _Not72_responsetime;
			}else {
				return _Not72_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT72/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot72_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT72/responseTime",v);
		_Not72_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not73_accuracy=null;

	/**
	 * @return Returns the NoT73/accuracy.
	 */
	public Integer getNot73_accuracy() {
		try{
			if (_Not73_accuracy==null){
				_Not73_accuracy=getIntegerProperty("NoT73/accuracy");
				return _Not73_accuracy;
			}else {
				return _Not73_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT73/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot73_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT73/accuracy",v);
		_Not73_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not73_responsetime=null;

	/**
	 * @return Returns the NoT73/responseTime.
	 */
	public Integer getNot73_responsetime() {
		try{
			if (_Not73_responsetime==null){
				_Not73_responsetime=getIntegerProperty("NoT73/responseTime");
				return _Not73_responsetime;
			}else {
				return _Not73_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT73/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot73_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT73/responseTime",v);
		_Not73_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not74_accuracy=null;

	/**
	 * @return Returns the NoT74/accuracy.
	 */
	public Integer getNot74_accuracy() {
		try{
			if (_Not74_accuracy==null){
				_Not74_accuracy=getIntegerProperty("NoT74/accuracy");
				return _Not74_accuracy;
			}else {
				return _Not74_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT74/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot74_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT74/accuracy",v);
		_Not74_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not74_responsetime=null;

	/**
	 * @return Returns the NoT74/responseTime.
	 */
	public Integer getNot74_responsetime() {
		try{
			if (_Not74_responsetime==null){
				_Not74_responsetime=getIntegerProperty("NoT74/responseTime");
				return _Not74_responsetime;
			}else {
				return _Not74_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT74/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot74_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT74/responseTime",v);
		_Not74_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not75_accuracy=null;

	/**
	 * @return Returns the NoT75/accuracy.
	 */
	public Integer getNot75_accuracy() {
		try{
			if (_Not75_accuracy==null){
				_Not75_accuracy=getIntegerProperty("NoT75/accuracy");
				return _Not75_accuracy;
			}else {
				return _Not75_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT75/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot75_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT75/accuracy",v);
		_Not75_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not75_responsetime=null;

	/**
	 * @return Returns the NoT75/responseTime.
	 */
	public Integer getNot75_responsetime() {
		try{
			if (_Not75_responsetime==null){
				_Not75_responsetime=getIntegerProperty("NoT75/responseTime");
				return _Not75_responsetime;
			}else {
				return _Not75_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT75/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot75_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT75/responseTime",v);
		_Not75_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not76_accuracy=null;

	/**
	 * @return Returns the NoT76/accuracy.
	 */
	public Integer getNot76_accuracy() {
		try{
			if (_Not76_accuracy==null){
				_Not76_accuracy=getIntegerProperty("NoT76/accuracy");
				return _Not76_accuracy;
			}else {
				return _Not76_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT76/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot76_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT76/accuracy",v);
		_Not76_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not76_responsetime=null;

	/**
	 * @return Returns the NoT76/responseTime.
	 */
	public Integer getNot76_responsetime() {
		try{
			if (_Not76_responsetime==null){
				_Not76_responsetime=getIntegerProperty("NoT76/responseTime");
				return _Not76_responsetime;
			}else {
				return _Not76_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT76/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot76_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT76/responseTime",v);
		_Not76_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not77_accuracy=null;

	/**
	 * @return Returns the NoT77/accuracy.
	 */
	public Integer getNot77_accuracy() {
		try{
			if (_Not77_accuracy==null){
				_Not77_accuracy=getIntegerProperty("NoT77/accuracy");
				return _Not77_accuracy;
			}else {
				return _Not77_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT77/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot77_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT77/accuracy",v);
		_Not77_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not77_responsetime=null;

	/**
	 * @return Returns the NoT77/responseTime.
	 */
	public Integer getNot77_responsetime() {
		try{
			if (_Not77_responsetime==null){
				_Not77_responsetime=getIntegerProperty("NoT77/responseTime");
				return _Not77_responsetime;
			}else {
				return _Not77_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT77/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot77_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT77/responseTime",v);
		_Not77_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not78_accuracy=null;

	/**
	 * @return Returns the NoT78/accuracy.
	 */
	public Integer getNot78_accuracy() {
		try{
			if (_Not78_accuracy==null){
				_Not78_accuracy=getIntegerProperty("NoT78/accuracy");
				return _Not78_accuracy;
			}else {
				return _Not78_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT78/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot78_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT78/accuracy",v);
		_Not78_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not78_responsetime=null;

	/**
	 * @return Returns the NoT78/responseTime.
	 */
	public Integer getNot78_responsetime() {
		try{
			if (_Not78_responsetime==null){
				_Not78_responsetime=getIntegerProperty("NoT78/responseTime");
				return _Not78_responsetime;
			}else {
				return _Not78_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT78/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot78_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT78/responseTime",v);
		_Not78_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not79_accuracy=null;

	/**
	 * @return Returns the NoT79/accuracy.
	 */
	public Integer getNot79_accuracy() {
		try{
			if (_Not79_accuracy==null){
				_Not79_accuracy=getIntegerProperty("NoT79/accuracy");
				return _Not79_accuracy;
			}else {
				return _Not79_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT79/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot79_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT79/accuracy",v);
		_Not79_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not79_responsetime=null;

	/**
	 * @return Returns the NoT79/responseTime.
	 */
	public Integer getNot79_responsetime() {
		try{
			if (_Not79_responsetime==null){
				_Not79_responsetime=getIntegerProperty("NoT79/responseTime");
				return _Not79_responsetime;
			}else {
				return _Not79_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT79/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot79_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT79/responseTime",v);
		_Not79_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not80_accuracy=null;

	/**
	 * @return Returns the NoT80/accuracy.
	 */
	public Integer getNot80_accuracy() {
		try{
			if (_Not80_accuracy==null){
				_Not80_accuracy=getIntegerProperty("NoT80/accuracy");
				return _Not80_accuracy;
			}else {
				return _Not80_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT80/accuracy.
	 * @param v Value to Set.
	 */
	public void setNot80_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT80/accuracy",v);
		_Not80_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Not80_responsetime=null;

	/**
	 * @return Returns the NoT80/responseTime.
	 */
	public Integer getNot80_responsetime() {
		try{
			if (_Not80_responsetime==null){
				_Not80_responsetime=getIntegerProperty("NoT80/responseTime");
				return _Not80_responsetime;
			}else {
				return _Not80_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NoT80/responseTime.
	 * @param v Value to Set.
	 */
	public void setNot80_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NoT80/responseTime",v);
		_Not80_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CbatCategorization> getAllCbatCategorizations(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CbatCategorization> al = new ArrayList<org.nrg.xdat.om.CbatCategorization>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CbatCategorization> getCbatCategorizationsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CbatCategorization> al = new ArrayList<org.nrg.xdat.om.CbatCategorization>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CbatCategorization> getCbatCategorizationsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CbatCategorization> al = new ArrayList<org.nrg.xdat.om.CbatCategorization>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CbatCategorization getCbatCategorizationsById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cbat:categorization/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CbatCategorization) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
