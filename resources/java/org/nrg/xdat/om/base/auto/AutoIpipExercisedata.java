/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:00 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoIpipExercisedata extends XnatSubjectassessordata implements org.nrg.xdat.model.IpipExercisedataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoIpipExercisedata.class);
	public static String SCHEMA_ELEMENT_NAME="ipip:exerciseData";

	public AutoIpipExercisedata(ItemI item)
	{
		super(item);
	}

	public AutoIpipExercisedata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoIpipExercisedata(UserI user)
	 **/
	public AutoIpipExercisedata(){}

	public AutoIpipExercisedata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "ipip:exerciseData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Walk=null;

	/**
	 * @return Returns the WALK.
	 */
	public Integer getWalk() {
		try{
			if (_Walk==null){
				_Walk=getIntegerProperty("WALK");
				return _Walk;
			}else {
				return _Walk;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WALK.
	 * @param v Value to Set.
	 */
	public void setWalk(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WALK",v);
		_Walk=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Jog=null;

	/**
	 * @return Returns the JOG.
	 */
	public Integer getJog() {
		try{
			if (_Jog==null){
				_Jog=getIntegerProperty("JOG");
				return _Jog;
			}else {
				return _Jog;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for JOG.
	 * @param v Value to Set.
	 */
	public void setJog(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/JOG",v);
		_Jog=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Run=null;

	/**
	 * @return Returns the RUN.
	 */
	public Integer getRun() {
		try{
			if (_Run==null){
				_Run=getIntegerProperty("RUN");
				return _Run;
			}else {
				return _Run;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RUN.
	 * @param v Value to Set.
	 */
	public void setRun(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RUN",v);
		_Run=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Bike=null;

	/**
	 * @return Returns the BIKE.
	 */
	public Integer getBike() {
		try{
			if (_Bike==null){
				_Bike=getIntegerProperty("BIKE");
				return _Bike;
			}else {
				return _Bike;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BIKE.
	 * @param v Value to Set.
	 */
	public void setBike(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BIKE",v);
		_Bike=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tennis=null;

	/**
	 * @return Returns the TENNIS.
	 */
	public Integer getTennis() {
		try{
			if (_Tennis==null){
				_Tennis=getIntegerProperty("TENNIS");
				return _Tennis;
			}else {
				return _Tennis;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TENNIS.
	 * @param v Value to Set.
	 */
	public void setTennis(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TENNIS",v);
		_Tennis=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Swim=null;

	/**
	 * @return Returns the SWIM.
	 */
	public Integer getSwim() {
		try{
			if (_Swim==null){
				_Swim=getIntegerProperty("SWIM");
				return _Swim;
			}else {
				return _Swim;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SWIM.
	 * @param v Value to Set.
	 */
	public void setSwim(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SWIM",v);
		_Swim=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Aerobics=null;

	/**
	 * @return Returns the AEROBICS.
	 */
	public Integer getAerobics() {
		try{
			if (_Aerobics==null){
				_Aerobics=getIntegerProperty("AEROBICS");
				return _Aerobics;
			}else {
				return _Aerobics;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AEROBICS.
	 * @param v Value to Set.
	 */
	public void setAerobics(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AEROBICS",v);
		_Aerobics=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Lowint=null;

	/**
	 * @return Returns the LOWINT.
	 */
	public Integer getLowint() {
		try{
			if (_Lowint==null){
				_Lowint=getIntegerProperty("LOWINT");
				return _Lowint;
			}else {
				return _Lowint;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LOWINT.
	 * @param v Value to Set.
	 */
	public void setLowint(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LOWINT",v);
		_Lowint=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Vigorous=null;

	/**
	 * @return Returns the VIGOROUS.
	 */
	public Integer getVigorous() {
		try{
			if (_Vigorous==null){
				_Vigorous=getIntegerProperty("VIGOROUS");
				return _Vigorous;
			}else {
				return _Vigorous;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VIGOROUS.
	 * @param v Value to Set.
	 */
	public void setVigorous(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VIGOROUS",v);
		_Vigorous=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Weight=null;

	/**
	 * @return Returns the WEIGHT.
	 */
	public Integer getWeight() {
		try{
			if (_Weight==null){
				_Weight=getIntegerProperty("WEIGHT");
				return _Weight;
			}else {
				return _Weight;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WEIGHT.
	 * @param v Value to Set.
	 */
	public void setWeight(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WEIGHT",v);
		_Weight=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.IpipExercisedata> getAllIpipExercisedatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.IpipExercisedata> al = new ArrayList<org.nrg.xdat.om.IpipExercisedata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.IpipExercisedata> getIpipExercisedatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.IpipExercisedata> al = new ArrayList<org.nrg.xdat.om.IpipExercisedata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.IpipExercisedata> getIpipExercisedatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.IpipExercisedata> al = new ArrayList<org.nrg.xdat.om.IpipExercisedata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static IpipExercisedata getIpipExercisedatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("ipip:exerciseData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (IpipExercisedata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
