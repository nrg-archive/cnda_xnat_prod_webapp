/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoSfDeficitsOtherdeficit extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.SfDeficitsOtherdeficitI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoSfDeficitsOtherdeficit.class);
	public static String SCHEMA_ELEMENT_NAME="sf:deficits_otherDeficit";

	public AutoSfDeficitsOtherdeficit(ItemI item)
	{
		super(item);
	}

	public AutoSfDeficitsOtherdeficit(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoSfDeficitsOtherdeficit(UserI user)
	 **/
	public AutoSfDeficitsOtherdeficit(){}

	public AutoSfDeficitsOtherdeficit(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "sf:deficits_otherDeficit";
	}

	//FIELD

	private String _Otherdeficitname=null;

	/**
	 * @return Returns the otherDeficitName.
	 */
	public String getOtherdeficitname(){
		try{
			if (_Otherdeficitname==null){
				_Otherdeficitname=getStringProperty("otherDeficitName");
				return _Otherdeficitname;
			}else {
				return _Otherdeficitname;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for otherDeficitName.
	 * @param v Value to Set.
	 */
	public void setOtherdeficitname(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/otherDeficitName",v);
		_Otherdeficitname=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Otherdeficitpresent=null;

	/**
	 * @return Returns the otherDeficitPresent.
	 */
	public Boolean getOtherdeficitpresent() {
		try{
			if (_Otherdeficitpresent==null){
				_Otherdeficitpresent=getBooleanProperty("otherDeficitPresent");
				return _Otherdeficitpresent;
			}else {
				return _Otherdeficitpresent;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for otherDeficitPresent.
	 * @param v Value to Set.
	 */
	public void setOtherdeficitpresent(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/otherDeficitPresent",v);
		_Otherdeficitpresent=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _SfDeficitsOtherdeficitId=null;

	/**
	 * @return Returns the sf_deficits_otherDeficit_id.
	 */
	public Integer getSfDeficitsOtherdeficitId() {
		try{
			if (_SfDeficitsOtherdeficitId==null){
				_SfDeficitsOtherdeficitId=getIntegerProperty("sf_deficits_otherDeficit_id");
				return _SfDeficitsOtherdeficitId;
			}else {
				return _SfDeficitsOtherdeficitId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sf_deficits_otherDeficit_id.
	 * @param v Value to Set.
	 */
	public void setSfDeficitsOtherdeficitId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sf_deficits_otherDeficit_id",v);
		_SfDeficitsOtherdeficitId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.SfDeficitsOtherdeficit> getAllSfDeficitsOtherdeficits(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfDeficitsOtherdeficit> al = new ArrayList<org.nrg.xdat.om.SfDeficitsOtherdeficit>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.SfDeficitsOtherdeficit> getSfDeficitsOtherdeficitsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfDeficitsOtherdeficit> al = new ArrayList<org.nrg.xdat.om.SfDeficitsOtherdeficit>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.SfDeficitsOtherdeficit> getSfDeficitsOtherdeficitsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfDeficitsOtherdeficit> al = new ArrayList<org.nrg.xdat.om.SfDeficitsOtherdeficit>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static SfDeficitsOtherdeficit getSfDeficitsOtherdeficitsBySfDeficitsOtherdeficitId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("sf:deficits_otherDeficit/sf_deficits_otherDeficit_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (SfDeficitsOtherdeficit) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
