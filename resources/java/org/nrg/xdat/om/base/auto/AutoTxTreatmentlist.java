/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoTxTreatmentlist extends XnatSubjectassessordata implements org.nrg.xdat.model.TxTreatmentlistI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoTxTreatmentlist.class);
	public static String SCHEMA_ELEMENT_NAME="tx:treatmentList";

	public AutoTxTreatmentlist(ItemI item)
	{
		super(item);
	}

	public AutoTxTreatmentlist(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoTxTreatmentlist(UserI user)
	 **/
	public AutoTxTreatmentlist(){}

	public AutoTxTreatmentlist(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "tx:treatmentList";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.TxTreatment> _Treatments_treatment =null;

	/**
	 * treatments/treatment
	 * @return Returns an List of org.nrg.xdat.om.TxTreatment
	 */
	public <A extends org.nrg.xdat.model.TxTreatmentI> List<A> getTreatments_treatment() {
		try{
			if (_Treatments_treatment==null){
				_Treatments_treatment=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("treatments/treatment"));
			}
			return (List<A>) _Treatments_treatment;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.TxTreatment>();}
	}

	/**
	 * Sets the value for treatments/treatment.
	 * @param v Value to Set.
	 */
	public void setTreatments_treatment(ItemI v) throws Exception{
		_Treatments_treatment =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/treatments/treatment",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/treatments/treatment",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * treatments/treatment
	 * Adds org.nrg.xdat.model.TxTreatmentI
	 */
	public <A extends org.nrg.xdat.model.TxTreatmentI> void addTreatments_treatment(A item) throws Exception{
	setTreatments_treatment((ItemI)item);
	}

	/**
	 * Removes the treatments/treatment of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeTreatments_treatment(int index) throws java.lang.IndexOutOfBoundsException {
		_Treatments_treatment =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/treatments/treatment",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.TxTreatmentlist> getAllTxTreatmentlists(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TxTreatmentlist> al = new ArrayList<org.nrg.xdat.om.TxTreatmentlist>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.TxTreatmentlist> getTxTreatmentlistsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TxTreatmentlist> al = new ArrayList<org.nrg.xdat.om.TxTreatmentlist>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.TxTreatmentlist> getTxTreatmentlistsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TxTreatmentlist> al = new ArrayList<org.nrg.xdat.om.TxTreatmentlist>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static TxTreatmentlist getTxTreatmentlistsById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("tx:treatmentList/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (TxTreatmentlist) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //treatments/treatment
	        for(org.nrg.xdat.model.TxTreatmentI childTreatments_treatment : this.getTreatments_treatment()){
	            if (childTreatments_treatment!=null){
	              for(ResourceFile rf: ((TxTreatment)childTreatments_treatment).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("treatments/treatment[" + ((TxTreatment)childTreatments_treatment).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("treatments/treatment/" + ((TxTreatment)childTreatments_treatment).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
