/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoUdsA4drugsdataA4drugs extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.UdsA4drugsdataA4drugsI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoUdsA4drugsdataA4drugs.class);
	public static String SCHEMA_ELEMENT_NAME="uds:a4drugsData_a4drugs";

	public AutoUdsA4drugsdataA4drugs(ItemI item)
	{
		super(item);
	}

	public AutoUdsA4drugsdataA4drugs(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoUdsA4drugsdataA4drugs(UserI user)
	 **/
	public AutoUdsA4drugsdataA4drugs(){}

	public AutoUdsA4drugsdataA4drugs(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "uds:a4drugsData_a4drugs";
	}

	//FIELD

	private Integer _Recno=null;

	/**
	 * @return Returns the RECNO.
	 */
	public Integer getRecno() {
		try{
			if (_Recno==null){
				_Recno=getIntegerProperty("RECNO");
				return _Recno;
			}else {
				return _Recno;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RECNO.
	 * @param v Value to Set.
	 */
	public void setRecno(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RECNO",v);
		_Recno=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Drugid=null;

	/**
	 * @return Returns the DRUGID.
	 */
	public String getDrugid(){
		try{
			if (_Drugid==null){
				_Drugid=getStringProperty("DRUGID");
				return _Drugid;
			}else {
				return _Drugid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DRUGID.
	 * @param v Value to Set.
	 */
	public void setDrugid(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DRUGID",v);
		_Drugid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Drugname=null;

	/**
	 * @return Returns the DRUGNAME.
	 */
	public String getDrugname(){
		try{
			if (_Drugname==null){
				_Drugname=getStringProperty("DRUGNAME");
				return _Drugname;
			}else {
				return _Drugname;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DRUGNAME.
	 * @param v Value to Set.
	 */
	public void setDrugname(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DRUGNAME",v);
		_Drugname=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _UdsA4drugsdataA4drugsId=null;

	/**
	 * @return Returns the uds_a4drugsData_a4drugs_id.
	 */
	public Integer getUdsA4drugsdataA4drugsId() {
		try{
			if (_UdsA4drugsdataA4drugsId==null){
				_UdsA4drugsdataA4drugsId=getIntegerProperty("uds_a4drugsData_a4drugs_id");
				return _UdsA4drugsdataA4drugsId;
			}else {
				return _UdsA4drugsdataA4drugsId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for uds_a4drugsData_a4drugs_id.
	 * @param v Value to Set.
	 */
	public void setUdsA4drugsdataA4drugsId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/uds_a4drugsData_a4drugs_id",v);
		_UdsA4drugsdataA4drugsId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.UdsA4drugsdataA4drugs> getAllUdsA4drugsdataA4drugss(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsA4drugsdataA4drugs> al = new ArrayList<org.nrg.xdat.om.UdsA4drugsdataA4drugs>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsA4drugsdataA4drugs> getUdsA4drugsdataA4drugssByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsA4drugsdataA4drugs> al = new ArrayList<org.nrg.xdat.om.UdsA4drugsdataA4drugs>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsA4drugsdataA4drugs> getUdsA4drugsdataA4drugssByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsA4drugsdataA4drugs> al = new ArrayList<org.nrg.xdat.om.UdsA4drugsdataA4drugs>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static UdsA4drugsdataA4drugs getUdsA4drugsdataA4drugssByUdsA4drugsdataA4drugsId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("uds:a4drugsData_a4drugs/uds_a4drugsData_a4drugs_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (UdsA4drugsdataA4drugs) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
