/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoTissueLabresultdata extends XnatSubjectassessordata implements org.nrg.xdat.model.TissueLabresultdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoTissueLabresultdata.class);
	public static String SCHEMA_ELEMENT_NAME="tissue:labResultData";

	public AutoTissueLabresultdata(ItemI item)
	{
		super(item);
	}

	public AutoTissueLabresultdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoTissueLabresultdata(UserI user)
	 **/
	public AutoTissueLabresultdata(){}

	public AutoTissueLabresultdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "tissue:labResultData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Dateresultsreport=null;

	/**
	 * @return Returns the dateResultsReport.
	 */
	public Object getDateresultsreport(){
		try{
			if (_Dateresultsreport==null){
				_Dateresultsreport=getProperty("dateResultsReport");
				return _Dateresultsreport;
			}else {
				return _Dateresultsreport;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for dateResultsReport.
	 * @param v Value to Set.
	 */
	public void setDateresultsreport(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/dateResultsReport",v);
		_Dateresultsreport=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Timeresultsreport=null;

	/**
	 * @return Returns the timeResultsReport.
	 */
	public Object getTimeresultsreport(){
		try{
			if (_Timeresultsreport==null){
				_Timeresultsreport=getProperty("timeResultsReport");
				return _Timeresultsreport;
			}else {
				return _Timeresultsreport;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for timeResultsReport.
	 * @param v Value to Set.
	 */
	public void setTimeresultsreport(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/timeResultsReport",v);
		_Timeresultsreport=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Nonnumericresults=null;

	/**
	 * @return Returns the nonNumericResults.
	 */
	public String getNonnumericresults(){
		try{
			if (_Nonnumericresults==null){
				_Nonnumericresults=getStringProperty("nonNumericResults");
				return _Nonnumericresults;
			}else {
				return _Nonnumericresults;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for nonNumericResults.
	 * @param v Value to Set.
	 */
	public void setNonnumericresults(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/nonNumericResults",v);
		_Nonnumericresults=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Numericresults=null;

	/**
	 * @return Returns the numericResults.
	 */
	public Double getNumericresults() {
		try{
			if (_Numericresults==null){
				_Numericresults=getDoubleProperty("numericResults");
				return _Numericresults;
			}else {
				return _Numericresults;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for numericResults.
	 * @param v Value to Set.
	 */
	public void setNumericresults(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/numericResults",v);
		_Numericresults=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Unitsnumericresults=null;

	/**
	 * @return Returns the unitsNumericResults.
	 */
	public Double getUnitsnumericresults() {
		try{
			if (_Unitsnumericresults==null){
				_Unitsnumericresults=getDoubleProperty("unitsNumericResults");
				return _Unitsnumericresults;
			}else {
				return _Unitsnumericresults;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for unitsNumericResults.
	 * @param v Value to Set.
	 */
	public void setUnitsnumericresults(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/unitsNumericResults",v);
		_Unitsnumericresults=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Tisscollid=null;

	/**
	 * @return Returns the tissCollID.
	 */
	public String getTisscollid(){
		try{
			if (_Tisscollid==null){
				_Tisscollid=getStringProperty("tissCollID");
				return _Tisscollid;
			}else {
				return _Tisscollid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tissCollID.
	 * @param v Value to Set.
	 */
	public void setTisscollid(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tissCollID",v);
		_Tisscollid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Specid=null;

	/**
	 * @return Returns the specID.
	 */
	public String getSpecid(){
		try{
			if (_Specid==null){
				_Specid=getStringProperty("specID");
				return _Specid;
			}else {
				return _Specid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for specID.
	 * @param v Value to Set.
	 */
	public void setSpecid(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/specID",v);
		_Specid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.TissueLabresultdata> getAllTissueLabresultdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TissueLabresultdata> al = new ArrayList<org.nrg.xdat.om.TissueLabresultdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.TissueLabresultdata> getTissueLabresultdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TissueLabresultdata> al = new ArrayList<org.nrg.xdat.om.TissueLabresultdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.TissueLabresultdata> getTissueLabresultdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TissueLabresultdata> al = new ArrayList<org.nrg.xdat.om.TissueLabresultdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static TissueLabresultdata getTissueLabresultdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("tissue:labResultData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (TissueLabresultdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
