/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:00 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianDermfibcoldata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianDermfibcoldataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianDermfibcoldata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:dermfibcolData";

	public AutoDianDermfibcoldata(ItemI item)
	{
		super(item);
	}

	public AutoDianDermfibcoldata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianDermfibcoldata(UserI user)
	 **/
	public AutoDianDermfibcoldata(){}

	public AutoDianDermfibcoldata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:dermfibcolData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Tissuedt=null;

	/**
	 * @return Returns the TISSUEDT.
	 */
	public Object getTissuedt(){
		try{
			if (_Tissuedt==null){
				_Tissuedt=getProperty("TISSUEDT");
				return _Tissuedt;
			}else {
				return _Tissuedt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TISSUEDT.
	 * @param v Value to Set.
	 */
	public void setTissuedt(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TISSUEDT",v);
		_Tissuedt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tissuetime=null;

	/**
	 * @return Returns the TISSUETIME.
	 */
	public Integer getTissuetime() {
		try{
			if (_Tissuetime==null){
				_Tissuetime=getIntegerProperty("TISSUETIME");
				return _Tissuetime;
			}else {
				return _Tissuetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TISSUETIME.
	 * @param v Value to Set.
	 */
	public void setTissuetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TISSUETIME",v);
		_Tissuetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Dfcclinnm=null;

	/**
	 * @return Returns the DFCCLINNM.
	 */
	public String getDfcclinnm(){
		try{
			if (_Dfcclinnm==null){
				_Dfcclinnm=getStringProperty("DFCCLINNM");
				return _Dfcclinnm;
			}else {
				return _Dfcclinnm;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DFCCLINNM.
	 * @param v Value to Set.
	 */
	public void setDfcclinnm(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DFCCLINNM",v);
		_Dfcclinnm=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Dfcship=null;

	/**
	 * @return Returns the DFCSHIP.
	 */
	public String getDfcship(){
		try{
			if (_Dfcship==null){
				_Dfcship=getStringProperty("DFCSHIP");
				return _Dfcship;
			}else {
				return _Dfcship;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DFCSHIP.
	 * @param v Value to Set.
	 */
	public void setDfcship(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DFCSHIP",v);
		_Dfcship=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Dfcshipdt=null;

	/**
	 * @return Returns the DFCSHIPDT.
	 */
	public Object getDfcshipdt(){
		try{
			if (_Dfcshipdt==null){
				_Dfcshipdt=getProperty("DFCSHIPDT");
				return _Dfcshipdt;
			}else {
				return _Dfcshipdt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DFCSHIPDT.
	 * @param v Value to Set.
	 */
	public void setDfcshipdt(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DFCSHIPDT",v);
		_Dfcshipdt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Dfctrackno=null;

	/**
	 * @return Returns the DFCTRACKNO.
	 */
	public String getDfctrackno(){
		try{
			if (_Dfctrackno==null){
				_Dfctrackno=getStringProperty("DFCTRACKNO");
				return _Dfctrackno;
			}else {
				return _Dfctrackno;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DFCTRACKNO.
	 * @param v Value to Set.
	 */
	public void setDfctrackno(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DFCTRACKNO",v);
		_Dfctrackno=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianDermfibcoldata> getAllDianDermfibcoldatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianDermfibcoldata> al = new ArrayList<org.nrg.xdat.om.DianDermfibcoldata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianDermfibcoldata> getDianDermfibcoldatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianDermfibcoldata> al = new ArrayList<org.nrg.xdat.om.DianDermfibcoldata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianDermfibcoldata> getDianDermfibcoldatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianDermfibcoldata> al = new ArrayList<org.nrg.xdat.om.DianDermfibcoldata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianDermfibcoldata getDianDermfibcoldatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:dermfibcolData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianDermfibcoldata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
