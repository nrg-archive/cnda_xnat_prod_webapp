/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCondrMetsMetsradencdata extends CndaExtSanode implements org.nrg.xdat.model.CondrMetsMetsradencdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCondrMetsMetsradencdata.class);
	public static String SCHEMA_ELEMENT_NAME="condr_mets:metsRadEncData";

	public AutoCondrMetsMetsradencdata(ItemI item)
	{
		super(item);
	}

	public AutoCondrMetsMetsradencdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCondrMetsMetsradencdata(UserI user)
	 **/
	public AutoCondrMetsMetsradencdata(){}

	public AutoCondrMetsMetsradencdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "condr_mets:metsRadEncData";
	}
	 private org.nrg.xdat.om.CndaExtSanode _Sanode =null;

	/**
	 * saNode
	 * @return org.nrg.xdat.om.CndaExtSanode
	 */
	public org.nrg.xdat.om.CndaExtSanode getSanode() {
		try{
			if (_Sanode==null){
				_Sanode=((CndaExtSanode)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("saNode")));
				return _Sanode;
			}else {
				return _Sanode;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for saNode.
	 * @param v Value to Set.
	 */
	public void setSanode(ItemI v) throws Exception{
		_Sanode =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/saNode",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/saNode",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * saNode
	 * set org.nrg.xdat.model.CndaExtSanodeI
	 */
	public <A extends org.nrg.xdat.model.CndaExtSanodeI> void setSanode(A item) throws Exception{
	setSanode((ItemI)item);
	}

	/**
	 * Removes the saNode.
	 * */
	public void removeSanode() {
		_Sanode =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/saNode",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Treated=null;

	/**
	 * @return Returns the treated.
	 */
	public Boolean getTreated() {
		try{
			if (_Treated==null){
				_Treated=getBooleanProperty("treated");
				return _Treated;
			}else {
				return _Treated;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for treated.
	 * @param v Value to Set.
	 */
	public void setTreated(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/treated",v);
		_Treated=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Treatmentvolume=null;

	/**
	 * @return Returns the treatmentVolume.
	 */
	public Double getTreatmentvolume() {
		try{
			if (_Treatmentvolume==null){
				_Treatmentvolume=getDoubleProperty("treatmentVolume");
				return _Treatmentvolume;
			}else {
				return _Treatmentvolume;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for treatmentVolume.
	 * @param v Value to Set.
	 */
	public void setTreatmentvolume(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/treatmentVolume",v);
		_Treatmentvolume=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Treatmentdose=null;

	/**
	 * @return Returns the treatmentDose.
	 */
	public Double getTreatmentdose() {
		try{
			if (_Treatmentdose==null){
				_Treatmentdose=getDoubleProperty("treatmentDose");
				return _Treatmentdose;
			}else {
				return _Treatmentdose;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for treatmentDose.
	 * @param v Value to Set.
	 */
	public void setTreatmentdose(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/treatmentDose",v);
		_Treatmentdose=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Isodoseline=null;

	/**
	 * @return Returns the isodoseLine.
	 */
	public Double getIsodoseline() {
		try{
			if (_Isodoseline==null){
				_Isodoseline=getDoubleProperty("isodoseLine");
				return _Isodoseline;
			}else {
				return _Isodoseline;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for isodoseLine.
	 * @param v Value to Set.
	 */
	public void setIsodoseline(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/isodoseLine",v);
		_Isodoseline=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Lesiontype=null;

	/**
	 * @return Returns the lesionType.
	 */
	public String getLesiontype(){
		try{
			if (_Lesiontype==null){
				_Lesiontype=getStringProperty("lesionType");
				return _Lesiontype;
			}else {
				return _Lesiontype;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for lesionType.
	 * @param v Value to Set.
	 */
	public void setLesiontype(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/lesionType",v);
		_Lesiontype=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Numberoffractions=null;

	/**
	 * @return Returns the numberOfFractions.
	 */
	public Integer getNumberoffractions() {
		try{
			if (_Numberoffractions==null){
				_Numberoffractions=getIntegerProperty("numberOfFractions");
				return _Numberoffractions;
			}else {
				return _Numberoffractions;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for numberOfFractions.
	 * @param v Value to Set.
	 */
	public void setNumberoffractions(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/numberOfFractions",v);
		_Numberoffractions=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Encnotes=null;

	/**
	 * @return Returns the encNotes.
	 */
	public String getEncnotes(){
		try{
			if (_Encnotes==null){
				_Encnotes=getStringProperty("encNotes");
				return _Encnotes;
			}else {
				return _Encnotes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for encNotes.
	 * @param v Value to Set.
	 */
	public void setEncnotes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/encNotes",v);
		_Encnotes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CondrMetsMetsradencdata> getAllCondrMetsMetsradencdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrMetsMetsradencdata> al = new ArrayList<org.nrg.xdat.om.CondrMetsMetsradencdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrMetsMetsradencdata> getCondrMetsMetsradencdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrMetsMetsradencdata> al = new ArrayList<org.nrg.xdat.om.CondrMetsMetsradencdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrMetsMetsradencdata> getCondrMetsMetsradencdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrMetsMetsradencdata> al = new ArrayList<org.nrg.xdat.om.CondrMetsMetsradencdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CondrMetsMetsradencdata getCondrMetsMetsradencdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("condr_mets:metsRadEncData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CondrMetsMetsradencdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //saNode
	        CndaExtSanode childSanode = (CndaExtSanode)this.getSanode();
	            if (childSanode!=null){
	              for(ResourceFile rf: ((CndaExtSanode)childSanode).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("saNode[" + ((CndaExtSanode)childSanode).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("saNode/" + ((CndaExtSanode)childSanode).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
