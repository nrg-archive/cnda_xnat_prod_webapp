/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoTxBasetreatment extends XnatSubjectassessordata implements org.nrg.xdat.model.TxBasetreatmentI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoTxBasetreatment.class);
	public static String SCHEMA_ELEMENT_NAME="tx:baseTreatment";

	public AutoTxBasetreatment(ItemI item)
	{
		super(item);
	}

	public AutoTxBasetreatment(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoTxBasetreatment(UserI user)
	 **/
	public AutoTxBasetreatment(){}

	public AutoTxBasetreatment(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "tx:baseTreatment";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Type=null;

	/**
	 * @return Returns the type.
	 */
	public String getType(){
		try{
			if (_Type==null){
				_Type=getStringProperty("type");
				return _Type;
			}else {
				return _Type;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for type.
	 * @param v Value to Set.
	 */
	public void setType(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/type",v);
		_Type=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Unit=null;

	/**
	 * @return Returns the unit.
	 */
	public String getUnit(){
		try{
			if (_Unit==null){
				_Unit=getStringProperty("unit");
				return _Unit;
			}else {
				return _Unit;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for unit.
	 * @param v Value to Set.
	 */
	public void setUnit(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/unit",v);
		_Unit=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Dose=null;

	/**
	 * @return Returns the dose.
	 */
	public Double getDose() {
		try{
			if (_Dose==null){
				_Dose=getDoubleProperty("dose");
				return _Dose;
			}else {
				return _Dose;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for dose.
	 * @param v Value to Set.
	 */
	public void setDose(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/dose",v);
		_Dose=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Timing=null;

	/**
	 * @return Returns the timing.
	 */
	public String getTiming(){
		try{
			if (_Timing==null){
				_Timing=getStringProperty("timing");
				return _Timing;
			}else {
				return _Timing;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for timing.
	 * @param v Value to Set.
	 */
	public void setTiming(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/timing",v);
		_Timing=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Startdate=null;

	/**
	 * @return Returns the startDate.
	 */
	public Object getStartdate(){
		try{
			if (_Startdate==null){
				_Startdate=getProperty("startDate");
				return _Startdate;
			}else {
				return _Startdate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for startDate.
	 * @param v Value to Set.
	 */
	public void setStartdate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/startDate",v);
		_Startdate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Startdatedaynotreported=null;

	/**
	 * @return Returns the startDateDayNotReported.
	 */
	public Boolean getStartdatedaynotreported() {
		try{
			if (_Startdatedaynotreported==null){
				_Startdatedaynotreported=getBooleanProperty("startDateDayNotReported");
				return _Startdatedaynotreported;
			}else {
				return _Startdatedaynotreported;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for startDateDayNotReported.
	 * @param v Value to Set.
	 */
	public void setStartdatedaynotreported(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/startDateDayNotReported",v);
		_Startdatedaynotreported=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Startdatemonthnotreported=null;

	/**
	 * @return Returns the startDateMonthNotReported.
	 */
	public Boolean getStartdatemonthnotreported() {
		try{
			if (_Startdatemonthnotreported==null){
				_Startdatemonthnotreported=getBooleanProperty("startDateMonthNotReported");
				return _Startdatemonthnotreported;
			}else {
				return _Startdatemonthnotreported;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for startDateMonthNotReported.
	 * @param v Value to Set.
	 */
	public void setStartdatemonthnotreported(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/startDateMonthNotReported",v);
		_Startdatemonthnotreported=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Startdateyearnotreported=null;

	/**
	 * @return Returns the startDateYearNotReported.
	 */
	public Boolean getStartdateyearnotreported() {
		try{
			if (_Startdateyearnotreported==null){
				_Startdateyearnotreported=getBooleanProperty("startDateYearNotReported");
				return _Startdateyearnotreported;
			}else {
				return _Startdateyearnotreported;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for startDateYearNotReported.
	 * @param v Value to Set.
	 */
	public void setStartdateyearnotreported(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/startDateYearNotReported",v);
		_Startdateyearnotreported=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Enddate=null;

	/**
	 * @return Returns the endDate.
	 */
	public Object getEnddate(){
		try{
			if (_Enddate==null){
				_Enddate=getProperty("endDate");
				return _Enddate;
			}else {
				return _Enddate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for endDate.
	 * @param v Value to Set.
	 */
	public void setEnddate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/endDate",v);
		_Enddate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Enddatedaynotreported=null;

	/**
	 * @return Returns the endDateDayNotReported.
	 */
	public Boolean getEnddatedaynotreported() {
		try{
			if (_Enddatedaynotreported==null){
				_Enddatedaynotreported=getBooleanProperty("endDateDayNotReported");
				return _Enddatedaynotreported;
			}else {
				return _Enddatedaynotreported;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for endDateDayNotReported.
	 * @param v Value to Set.
	 */
	public void setEnddatedaynotreported(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/endDateDayNotReported",v);
		_Enddatedaynotreported=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Enddatemonthnotreported=null;

	/**
	 * @return Returns the endDateMonthNotReported.
	 */
	public Boolean getEnddatemonthnotreported() {
		try{
			if (_Enddatemonthnotreported==null){
				_Enddatemonthnotreported=getBooleanProperty("endDateMonthNotReported");
				return _Enddatemonthnotreported;
			}else {
				return _Enddatemonthnotreported;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for endDateMonthNotReported.
	 * @param v Value to Set.
	 */
	public void setEnddatemonthnotreported(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/endDateMonthNotReported",v);
		_Enddatemonthnotreported=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Enddateyearnotreported=null;

	/**
	 * @return Returns the endDateYearNotReported.
	 */
	public Boolean getEnddateyearnotreported() {
		try{
			if (_Enddateyearnotreported==null){
				_Enddateyearnotreported=getBooleanProperty("endDateYearNotReported");
				return _Enddateyearnotreported;
			}else {
				return _Enddateyearnotreported;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for endDateYearNotReported.
	 * @param v Value to Set.
	 */
	public void setEnddateyearnotreported(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/endDateYearNotReported",v);
		_Enddateyearnotreported=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Completed=null;

	/**
	 * @return Returns the completed.
	 */
	public Boolean getCompleted() {
		try{
			if (_Completed==null){
				_Completed=getBooleanProperty("completed");
				return _Completed;
			}else {
				return _Completed;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for completed.
	 * @param v Value to Set.
	 */
	public void setCompleted(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/completed",v);
		_Completed=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Incompletereason=null;

	/**
	 * @return Returns the incompleteReason.
	 */
	public String getIncompletereason(){
		try{
			if (_Incompletereason==null){
				_Incompletereason=getStringProperty("incompleteReason");
				return _Incompletereason;
			}else {
				return _Incompletereason;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for incompleteReason.
	 * @param v Value to Set.
	 */
	public void setIncompletereason(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/incompleteReason",v);
		_Incompletereason=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Incompleteextent=null;

	/**
	 * @return Returns the incompleteExtent.
	 */
	public String getIncompleteextent(){
		try{
			if (_Incompleteextent==null){
				_Incompleteextent=getStringProperty("incompleteExtent");
				return _Incompleteextent;
			}else {
				return _Incompleteextent;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for incompleteExtent.
	 * @param v Value to Set.
	 */
	public void setIncompleteextent(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/incompleteExtent",v);
		_Incompleteextent=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Clinicaltrialname=null;

	/**
	 * @return Returns the clinicalTrialName.
	 */
	public String getClinicaltrialname(){
		try{
			if (_Clinicaltrialname==null){
				_Clinicaltrialname=getStringProperty("clinicalTrialName");
				return _Clinicaltrialname;
			}else {
				return _Clinicaltrialname;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for clinicalTrialName.
	 * @param v Value to Set.
	 */
	public void setClinicaltrialname(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/clinicalTrialName",v);
		_Clinicaltrialname=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Clinicaltrialarm=null;

	/**
	 * @return Returns the clinicalTrialArm.
	 */
	public String getClinicaltrialarm(){
		try{
			if (_Clinicaltrialarm==null){
				_Clinicaltrialarm=getStringProperty("clinicalTrialArm");
				return _Clinicaltrialarm;
			}else {
				return _Clinicaltrialarm;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for clinicalTrialArm.
	 * @param v Value to Set.
	 */
	public void setClinicaltrialarm(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/clinicalTrialArm",v);
		_Clinicaltrialarm=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Completednotes=null;

	/**
	 * @return Returns the completedNotes.
	 */
	public String getCompletednotes(){
		try{
			if (_Completednotes==null){
				_Completednotes=getStringProperty("completedNotes");
				return _Completednotes;
			}else {
				return _Completednotes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for completedNotes.
	 * @param v Value to Set.
	 */
	public void setCompletednotes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/completedNotes",v);
		_Completednotes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.TxBasetreatment> getAllTxBasetreatments(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TxBasetreatment> al = new ArrayList<org.nrg.xdat.om.TxBasetreatment>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.TxBasetreatment> getTxBasetreatmentsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TxBasetreatment> al = new ArrayList<org.nrg.xdat.om.TxBasetreatment>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.TxBasetreatment> getTxBasetreatmentsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TxBasetreatment> al = new ArrayList<org.nrg.xdat.om.TxBasetreatment>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static TxBasetreatment getTxBasetreatmentsById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("tx:baseTreatment/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (TxBasetreatment) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
