/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:01 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoClinPhysicaldata extends XnatSubjectassessordata implements org.nrg.xdat.model.ClinPhysicaldataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoClinPhysicaldata.class);
	public static String SCHEMA_ELEMENT_NAME="clin:physicalData";

	public AutoClinPhysicaldata(ItemI item)
	{
		super(item);
	}

	public AutoClinPhysicaldata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoClinPhysicaldata(UserI user)
	 **/
	public AutoClinPhysicaldata(){}

	public AutoClinPhysicaldata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "clin:physicalData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pxgenapp=null;

	/**
	 * @return Returns the PXGENAPP.
	 */
	public Integer getPxgenapp() {
		try{
			if (_Pxgenapp==null){
				_Pxgenapp=getIntegerProperty("PXGENAPP");
				return _Pxgenapp;
			}else {
				return _Pxgenapp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXGENAPP.
	 * @param v Value to Set.
	 */
	public void setPxgenapp(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXGENAPP",v);
		_Pxgenapp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Pxgendes=null;

	/**
	 * @return Returns the PXGENDES.
	 */
	public String getPxgendes(){
		try{
			if (_Pxgendes==null){
				_Pxgendes=getStringProperty("PXGENDES");
				return _Pxgendes;
			}else {
				return _Pxgendes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXGENDES.
	 * @param v Value to Set.
	 */
	public void setPxgendes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXGENDES",v);
		_Pxgendes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pxheadey=null;

	/**
	 * @return Returns the PXHEADEY.
	 */
	public Integer getPxheadey() {
		try{
			if (_Pxheadey==null){
				_Pxheadey=getIntegerProperty("PXHEADEY");
				return _Pxheadey;
			}else {
				return _Pxheadey;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXHEADEY.
	 * @param v Value to Set.
	 */
	public void setPxheadey(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXHEADEY",v);
		_Pxheadey=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Pxheadde=null;

	/**
	 * @return Returns the PXHEADDE.
	 */
	public String getPxheadde(){
		try{
			if (_Pxheadde==null){
				_Pxheadde=getStringProperty("PXHEADDE");
				return _Pxheadde;
			}else {
				return _Pxheadde;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXHEADDE.
	 * @param v Value to Set.
	 */
	public void setPxheadde(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXHEADDE",v);
		_Pxheadde=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pxneck=null;

	/**
	 * @return Returns the PXNECK.
	 */
	public Integer getPxneck() {
		try{
			if (_Pxneck==null){
				_Pxneck=getIntegerProperty("PXNECK");
				return _Pxneck;
			}else {
				return _Pxneck;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXNECK.
	 * @param v Value to Set.
	 */
	public void setPxneck(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXNECK",v);
		_Pxneck=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Pxnecdes=null;

	/**
	 * @return Returns the PXNECDES.
	 */
	public String getPxnecdes(){
		try{
			if (_Pxnecdes==null){
				_Pxnecdes=getStringProperty("PXNECDES");
				return _Pxnecdes;
			}else {
				return _Pxnecdes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXNECDES.
	 * @param v Value to Set.
	 */
	public void setPxnecdes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXNECDES",v);
		_Pxnecdes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pxchest=null;

	/**
	 * @return Returns the PXCHEST.
	 */
	public Integer getPxchest() {
		try{
			if (_Pxchest==null){
				_Pxchest=getIntegerProperty("PXCHEST");
				return _Pxchest;
			}else {
				return _Pxchest;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXCHEST.
	 * @param v Value to Set.
	 */
	public void setPxchest(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXCHEST",v);
		_Pxchest=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Pxchedes=null;

	/**
	 * @return Returns the PXCHEDES.
	 */
	public String getPxchedes(){
		try{
			if (_Pxchedes==null){
				_Pxchedes=getStringProperty("PXCHEDES");
				return _Pxchedes;
			}else {
				return _Pxchedes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXCHEDES.
	 * @param v Value to Set.
	 */
	public void setPxchedes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXCHEDES",v);
		_Pxchedes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pxheart=null;

	/**
	 * @return Returns the PXHEART.
	 */
	public Integer getPxheart() {
		try{
			if (_Pxheart==null){
				_Pxheart=getIntegerProperty("PXHEART");
				return _Pxheart;
			}else {
				return _Pxheart;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXHEART.
	 * @param v Value to Set.
	 */
	public void setPxheart(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXHEART",v);
		_Pxheart=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Pxheades=null;

	/**
	 * @return Returns the PXHEADES.
	 */
	public String getPxheades(){
		try{
			if (_Pxheades==null){
				_Pxheades=getStringProperty("PXHEADES");
				return _Pxheades;
			}else {
				return _Pxheades;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXHEADES.
	 * @param v Value to Set.
	 */
	public void setPxheades(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXHEADES",v);
		_Pxheades=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pxabdom=null;

	/**
	 * @return Returns the PXABDOM.
	 */
	public Integer getPxabdom() {
		try{
			if (_Pxabdom==null){
				_Pxabdom=getIntegerProperty("PXABDOM");
				return _Pxabdom;
			}else {
				return _Pxabdom;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXABDOM.
	 * @param v Value to Set.
	 */
	public void setPxabdom(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXABDOM",v);
		_Pxabdom=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Pxabddes=null;

	/**
	 * @return Returns the PXABDDES.
	 */
	public String getPxabddes(){
		try{
			if (_Pxabddes==null){
				_Pxabddes=getStringProperty("PXABDDES");
				return _Pxabddes;
			}else {
				return _Pxabddes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXABDDES.
	 * @param v Value to Set.
	 */
	public void setPxabddes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXABDDES",v);
		_Pxabddes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pxextrem=null;

	/**
	 * @return Returns the PXEXTREM.
	 */
	public Integer getPxextrem() {
		try{
			if (_Pxextrem==null){
				_Pxextrem=getIntegerProperty("PXEXTREM");
				return _Pxextrem;
			}else {
				return _Pxextrem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXEXTREM.
	 * @param v Value to Set.
	 */
	public void setPxextrem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXEXTREM",v);
		_Pxextrem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Pxextdes=null;

	/**
	 * @return Returns the PXEXTDES.
	 */
	public String getPxextdes(){
		try{
			if (_Pxextdes==null){
				_Pxextdes=getStringProperty("PXEXTDES");
				return _Pxextdes;
			}else {
				return _Pxextdes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXEXTDES.
	 * @param v Value to Set.
	 */
	public void setPxextdes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXEXTDES",v);
		_Pxextdes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pxedema=null;

	/**
	 * @return Returns the PXEDEMA.
	 */
	public Integer getPxedema() {
		try{
			if (_Pxedema==null){
				_Pxedema=getIntegerProperty("PXEDEMA");
				return _Pxedema;
			}else {
				return _Pxedema;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXEDEMA.
	 * @param v Value to Set.
	 */
	public void setPxedema(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXEDEMA",v);
		_Pxedema=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Pxededes=null;

	/**
	 * @return Returns the PXEDEDES.
	 */
	public String getPxededes(){
		try{
			if (_Pxededes==null){
				_Pxededes=getStringProperty("PXEDEDES");
				return _Pxededes;
			}else {
				return _Pxededes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXEDEDES.
	 * @param v Value to Set.
	 */
	public void setPxededes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXEDEDES",v);
		_Pxededes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pxperiph=null;

	/**
	 * @return Returns the PXPERIPH.
	 */
	public Integer getPxperiph() {
		try{
			if (_Pxperiph==null){
				_Pxperiph=getIntegerProperty("PXPERIPH");
				return _Pxperiph;
			}else {
				return _Pxperiph;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXPERIPH.
	 * @param v Value to Set.
	 */
	public void setPxperiph(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXPERIPH",v);
		_Pxperiph=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Pxperdes=null;

	/**
	 * @return Returns the PXPERDES.
	 */
	public String getPxperdes(){
		try{
			if (_Pxperdes==null){
				_Pxperdes=getStringProperty("PXPERDES");
				return _Pxperdes;
			}else {
				return _Pxperdes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXPERDES.
	 * @param v Value to Set.
	 */
	public void setPxperdes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXPERDES",v);
		_Pxperdes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pxskin=null;

	/**
	 * @return Returns the PXSKIN.
	 */
	public Integer getPxskin() {
		try{
			if (_Pxskin==null){
				_Pxskin=getIntegerProperty("PXSKIN");
				return _Pxskin;
			}else {
				return _Pxskin;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXSKIN.
	 * @param v Value to Set.
	 */
	public void setPxskin(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXSKIN",v);
		_Pxskin=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Pxskides=null;

	/**
	 * @return Returns the PXSKIDES.
	 */
	public String getPxskides(){
		try{
			if (_Pxskides==null){
				_Pxskides=getStringProperty("PXSKIDES");
				return _Pxskides;
			}else {
				return _Pxskides;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXSKIDES.
	 * @param v Value to Set.
	 */
	public void setPxskides(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXSKIDES",v);
		_Pxskides=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pxmuscul=null;

	/**
	 * @return Returns the PXMUSCUL.
	 */
	public Integer getPxmuscul() {
		try{
			if (_Pxmuscul==null){
				_Pxmuscul=getIntegerProperty("PXMUSCUL");
				return _Pxmuscul;
			}else {
				return _Pxmuscul;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXMUSCUL.
	 * @param v Value to Set.
	 */
	public void setPxmuscul(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXMUSCUL",v);
		_Pxmuscul=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Pxmusdes=null;

	/**
	 * @return Returns the PXMUSDES.
	 */
	public String getPxmusdes(){
		try{
			if (_Pxmusdes==null){
				_Pxmusdes=getStringProperty("PXMUSDES");
				return _Pxmusdes;
			}else {
				return _Pxmusdes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXMUSDES.
	 * @param v Value to Set.
	 */
	public void setPxmusdes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXMUSDES",v);
		_Pxmusdes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pxother=null;

	/**
	 * @return Returns the PXOTHER.
	 */
	public Integer getPxother() {
		try{
			if (_Pxother==null){
				_Pxother=getIntegerProperty("PXOTHER");
				return _Pxother;
			}else {
				return _Pxother;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXOTHER.
	 * @param v Value to Set.
	 */
	public void setPxother(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXOTHER",v);
		_Pxother=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Pxotrcom=null;

	/**
	 * @return Returns the PXOTRCOM.
	 */
	public String getPxotrcom(){
		try{
			if (_Pxotrcom==null){
				_Pxotrcom=getStringProperty("PXOTRCOM");
				return _Pxotrcom;
			}else {
				return _Pxotrcom;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXOTRCOM.
	 * @param v Value to Set.
	 */
	public void setPxotrcom(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXOTRCOM",v);
		_Pxotrcom=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Pxgencom=null;

	/**
	 * @return Returns the PXGENCOM.
	 */
	public String getPxgencom(){
		try{
			if (_Pxgencom==null){
				_Pxgencom=getStringProperty("PXGENCOM");
				return _Pxgencom;
			}else {
				return _Pxgencom;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PXGENCOM.
	 * @param v Value to Set.
	 */
	public void setPxgencom(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PXGENCOM",v);
		_Pxgencom=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.ClinPhysicaldata> getAllClinPhysicaldatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.ClinPhysicaldata> al = new ArrayList<org.nrg.xdat.om.ClinPhysicaldata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.ClinPhysicaldata> getClinPhysicaldatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.ClinPhysicaldata> al = new ArrayList<org.nrg.xdat.om.ClinPhysicaldata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.ClinPhysicaldata> getClinPhysicaldatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.ClinPhysicaldata> al = new ArrayList<org.nrg.xdat.om.ClinPhysicaldata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ClinPhysicaldata getClinPhysicaldatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("clin:physicalData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (ClinPhysicaldata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
