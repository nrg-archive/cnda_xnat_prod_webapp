/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:01 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoClinPregdataPreg extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.ClinPregdataPregI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoClinPregdataPreg.class);
	public static String SCHEMA_ELEMENT_NAME="clin:pregData_preg";

	public AutoClinPregdataPreg(ItemI item)
	{
		super(item);
	}

	public AutoClinPregdataPreg(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoClinPregdataPreg(UserI user)
	 **/
	public AutoClinPregdataPreg(){}

	public AutoClinPregdataPreg(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "clin:pregData_preg";
	}

	//FIELD

	private Integer _Recno=null;

	/**
	 * @return Returns the RECNO.
	 */
	public Integer getRecno() {
		try{
			if (_Recno==null){
				_Recno=getIntegerProperty("RECNO");
				return _Recno;
			}else {
				return _Recno;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RECNO.
	 * @param v Value to Set.
	 */
	public void setRecno(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RECNO",v);
		_Recno=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pregposs=null;

	/**
	 * @return Returns the PREGPOSS.
	 */
	public Integer getPregposs() {
		try{
			if (_Pregposs==null){
				_Pregposs=getIntegerProperty("PREGPOSS");
				return _Pregposs;
			}else {
				return _Pregposs;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PREGPOSS.
	 * @param v Value to Set.
	 */
	public void setPregposs(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PREGPOSS",v);
		_Pregposs=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Pregtest=null;

	/**
	 * @return Returns the PREGTEST.
	 */
	public Object getPregtest(){
		try{
			if (_Pregtest==null){
				_Pregtest=getProperty("PREGTEST");
				return _Pregtest;
			}else {
				return _Pregtest;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PREGTEST.
	 * @param v Value to Set.
	 */
	public void setPregtest(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PREGTEST",v);
		_Pregtest=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pregres=null;

	/**
	 * @return Returns the PREGRES.
	 */
	public Integer getPregres() {
		try{
			if (_Pregres==null){
				_Pregres=getIntegerProperty("PREGRES");
				return _Pregres;
			}else {
				return _Pregres;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PREGRES.
	 * @param v Value to Set.
	 */
	public void setPregres(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PREGRES",v);
		_Pregres=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _ClinPregdataPregId=null;

	/**
	 * @return Returns the clin_pregData_preg_id.
	 */
	public Integer getClinPregdataPregId() {
		try{
			if (_ClinPregdataPregId==null){
				_ClinPregdataPregId=getIntegerProperty("clin_pregData_preg_id");
				return _ClinPregdataPregId;
			}else {
				return _ClinPregdataPregId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for clin_pregData_preg_id.
	 * @param v Value to Set.
	 */
	public void setClinPregdataPregId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/clin_pregData_preg_id",v);
		_ClinPregdataPregId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.ClinPregdataPreg> getAllClinPregdataPregs(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.ClinPregdataPreg> al = new ArrayList<org.nrg.xdat.om.ClinPregdataPreg>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.ClinPregdataPreg> getClinPregdataPregsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.ClinPregdataPreg> al = new ArrayList<org.nrg.xdat.om.ClinPregdataPreg>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.ClinPregdataPreg> getClinPregdataPregsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.ClinPregdataPreg> al = new ArrayList<org.nrg.xdat.om.ClinPregdataPreg>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ClinPregdataPreg getClinPregdataPregsByClinPregdataPregId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("clin:pregData_preg/clin_pregData_preg_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (ClinPregdataPreg) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
