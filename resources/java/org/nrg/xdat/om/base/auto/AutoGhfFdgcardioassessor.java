/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:00 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoGhfFdgcardioassessor extends XnatImageassessordata implements org.nrg.xdat.model.GhfFdgcardioassessorI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoGhfFdgcardioassessor.class);
	public static String SCHEMA_ELEMENT_NAME="ghf:fdgCardioAssessor";

	public AutoGhfFdgcardioassessor(ItemI item)
	{
		super(item);
	}

	public AutoGhfFdgcardioassessor(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoGhfFdgcardioassessor(UserI user)
	 **/
	public AutoGhfFdgcardioassessor(){}

	public AutoGhfFdgcardioassessor(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "ghf:fdgCardioAssessor";
	}
	 private org.nrg.xdat.om.XnatImageassessordata _Imageassessordata =null;

	/**
	 * imageAssessorData
	 * @return org.nrg.xdat.om.XnatImageassessordata
	 */
	public org.nrg.xdat.om.XnatImageassessordata getImageassessordata() {
		try{
			if (_Imageassessordata==null){
				_Imageassessordata=((XnatImageassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("imageAssessorData")));
				return _Imageassessordata;
			}else {
				return _Imageassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for imageAssessorData.
	 * @param v Value to Set.
	 */
	public void setImageassessordata(ItemI v) throws Exception{
		_Imageassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/imageAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/imageAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * imageAssessorData
	 * set org.nrg.xdat.model.XnatImageassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatImageassessordataI> void setImageassessordata(A item) throws Exception{
	setImageassessordata((ItemI)item);
	}

	/**
	 * Removes the imageAssessorData.
	 * */
	public void removeImageassessordata() {
		_Imageassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/imageAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Pvscorrectfracs_fbb=null;

	/**
	 * @return Returns the pvsCorrectFracs/fbb.
	 */
	public Double getPvscorrectfracs_fbb() {
		try{
			if (_Pvscorrectfracs_fbb==null){
				_Pvscorrectfracs_fbb=getDoubleProperty("pvsCorrectFracs/fbb");
				return _Pvscorrectfracs_fbb;
			}else {
				return _Pvscorrectfracs_fbb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for pvsCorrectFracs/fbb.
	 * @param v Value to Set.
	 */
	public void setPvscorrectfracs_fbb(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/pvsCorrectFracs/fbb",v);
		_Pvscorrectfracs_fbb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Pvscorrectfracs_fbm=null;

	/**
	 * @return Returns the pvsCorrectFracs/fbm.
	 */
	public Double getPvscorrectfracs_fbm() {
		try{
			if (_Pvscorrectfracs_fbm==null){
				_Pvscorrectfracs_fbm=getDoubleProperty("pvsCorrectFracs/fbm");
				return _Pvscorrectfracs_fbm;
			}else {
				return _Pvscorrectfracs_fbm;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for pvsCorrectFracs/fbm.
	 * @param v Value to Set.
	 */
	public void setPvscorrectfracs_fbm(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/pvsCorrectFracs/fbm",v);
		_Pvscorrectfracs_fbm=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Pvscorrectfracs_fmb=null;

	/**
	 * @return Returns the pvsCorrectFracs/fmb.
	 */
	public Double getPvscorrectfracs_fmb() {
		try{
			if (_Pvscorrectfracs_fmb==null){
				_Pvscorrectfracs_fmb=getDoubleProperty("pvsCorrectFracs/fmb");
				return _Pvscorrectfracs_fmb;
			}else {
				return _Pvscorrectfracs_fmb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for pvsCorrectFracs/fmb.
	 * @param v Value to Set.
	 */
	public void setPvscorrectfracs_fmb(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/pvsCorrectFracs/fmb",v);
		_Pvscorrectfracs_fmb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Pvscorrectfracs_fmm=null;

	/**
	 * @return Returns the pvsCorrectFracs/fmm.
	 */
	public Double getPvscorrectfracs_fmm() {
		try{
			if (_Pvscorrectfracs_fmm==null){
				_Pvscorrectfracs_fmm=getDoubleProperty("pvsCorrectFracs/fmm");
				return _Pvscorrectfracs_fmm;
			}else {
				return _Pvscorrectfracs_fmm;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for pvsCorrectFracs/fmm.
	 * @param v Value to Set.
	 */
	public void setPvscorrectfracs_fmm(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/pvsCorrectFracs/fmm",v);
		_Pvscorrectfracs_fmm=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Baselineplasma_gluc=null;

	/**
	 * @return Returns the baselinePlasma/gluc.
	 */
	public Double getBaselineplasma_gluc() {
		try{
			if (_Baselineplasma_gluc==null){
				_Baselineplasma_gluc=getDoubleProperty("baselinePlasma/gluc");
				return _Baselineplasma_gluc;
			}else {
				return _Baselineplasma_gluc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for baselinePlasma/gluc.
	 * @param v Value to Set.
	 */
	public void setBaselineplasma_gluc(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/baselinePlasma/gluc",v);
		_Baselineplasma_gluc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Baselineplasma_ffa=null;

	/**
	 * @return Returns the baselinePlasma/ffa.
	 */
	public Double getBaselineplasma_ffa() {
		try{
			if (_Baselineplasma_ffa==null){
				_Baselineplasma_ffa=getDoubleProperty("baselinePlasma/ffa");
				return _Baselineplasma_ffa;
			}else {
				return _Baselineplasma_ffa;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for baselinePlasma/ffa.
	 * @param v Value to Set.
	 */
	public void setBaselineplasma_ffa(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/baselinePlasma/ffa",v);
		_Baselineplasma_ffa=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Fiveminplasma_gluc=null;

	/**
	 * @return Returns the fiveMinPlasma/gluc.
	 */
	public Double getFiveminplasma_gluc() {
		try{
			if (_Fiveminplasma_gluc==null){
				_Fiveminplasma_gluc=getDoubleProperty("fiveMinPlasma/gluc");
				return _Fiveminplasma_gluc;
			}else {
				return _Fiveminplasma_gluc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for fiveMinPlasma/gluc.
	 * @param v Value to Set.
	 */
	public void setFiveminplasma_gluc(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/fiveMinPlasma/gluc",v);
		_Fiveminplasma_gluc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Fiveminplasma_ffa=null;

	/**
	 * @return Returns the fiveMinPlasma/ffa.
	 */
	public Double getFiveminplasma_ffa() {
		try{
			if (_Fiveminplasma_ffa==null){
				_Fiveminplasma_ffa=getDoubleProperty("fiveMinPlasma/ffa");
				return _Fiveminplasma_ffa;
			}else {
				return _Fiveminplasma_ffa;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for fiveMinPlasma/ffa.
	 * @param v Value to Set.
	 */
	public void setFiveminplasma_ffa(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/fiveMinPlasma/ffa",v);
		_Fiveminplasma_ffa=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Fifteenminplasma_gluc=null;

	/**
	 * @return Returns the fifteenMinPlasma/gluc.
	 */
	public Double getFifteenminplasma_gluc() {
		try{
			if (_Fifteenminplasma_gluc==null){
				_Fifteenminplasma_gluc=getDoubleProperty("fifteenMinPlasma/gluc");
				return _Fifteenminplasma_gluc;
			}else {
				return _Fifteenminplasma_gluc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for fifteenMinPlasma/gluc.
	 * @param v Value to Set.
	 */
	public void setFifteenminplasma_gluc(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/fifteenMinPlasma/gluc",v);
		_Fifteenminplasma_gluc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Fifteenminplasma_ffa=null;

	/**
	 * @return Returns the fifteenMinPlasma/ffa.
	 */
	public Double getFifteenminplasma_ffa() {
		try{
			if (_Fifteenminplasma_ffa==null){
				_Fifteenminplasma_ffa=getDoubleProperty("fifteenMinPlasma/ffa");
				return _Fifteenminplasma_ffa;
			}else {
				return _Fifteenminplasma_ffa;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for fifteenMinPlasma/ffa.
	 * @param v Value to Set.
	 */
	public void setFifteenminplasma_ffa(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/fifteenMinPlasma/ffa",v);
		_Fifteenminplasma_ffa=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Thirtyminplasma_gluc=null;

	/**
	 * @return Returns the thirtyMinPlasma/gluc.
	 */
	public Double getThirtyminplasma_gluc() {
		try{
			if (_Thirtyminplasma_gluc==null){
				_Thirtyminplasma_gluc=getDoubleProperty("thirtyMinPlasma/gluc");
				return _Thirtyminplasma_gluc;
			}else {
				return _Thirtyminplasma_gluc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for thirtyMinPlasma/gluc.
	 * @param v Value to Set.
	 */
	public void setThirtyminplasma_gluc(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/thirtyMinPlasma/gluc",v);
		_Thirtyminplasma_gluc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Thirtyminplasma_ffa=null;

	/**
	 * @return Returns the thirtyMinPlasma/ffa.
	 */
	public Double getThirtyminplasma_ffa() {
		try{
			if (_Thirtyminplasma_ffa==null){
				_Thirtyminplasma_ffa=getDoubleProperty("thirtyMinPlasma/ffa");
				return _Thirtyminplasma_ffa;
			}else {
				return _Thirtyminplasma_ffa;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for thirtyMinPlasma/ffa.
	 * @param v Value to Set.
	 */
	public void setThirtyminplasma_ffa(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/thirtyMinPlasma/ffa",v);
		_Thirtyminplasma_ffa=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Sixtyminplasma_gluc=null;

	/**
	 * @return Returns the sixtyMinPlasma/gluc.
	 */
	public Double getSixtyminplasma_gluc() {
		try{
			if (_Sixtyminplasma_gluc==null){
				_Sixtyminplasma_gluc=getDoubleProperty("sixtyMinPlasma/gluc");
				return _Sixtyminplasma_gluc;
			}else {
				return _Sixtyminplasma_gluc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sixtyMinPlasma/gluc.
	 * @param v Value to Set.
	 */
	public void setSixtyminplasma_gluc(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sixtyMinPlasma/gluc",v);
		_Sixtyminplasma_gluc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Sixtyminplasma_ffa=null;

	/**
	 * @return Returns the sixtyMinPlasma/ffa.
	 */
	public Double getSixtyminplasma_ffa() {
		try{
			if (_Sixtyminplasma_ffa==null){
				_Sixtyminplasma_ffa=getDoubleProperty("sixtyMinPlasma/ffa");
				return _Sixtyminplasma_ffa;
			}else {
				return _Sixtyminplasma_ffa;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sixtyMinPlasma/ffa.
	 * @param v Value to Set.
	 */
	public void setSixtyminplasma_ffa(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sixtyMinPlasma/ffa",v);
		_Sixtyminplasma_ffa=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Baselineheart_sbp=null;

	/**
	 * @return Returns the baselineHeart/sbp.
	 */
	public Double getBaselineheart_sbp() {
		try{
			if (_Baselineheart_sbp==null){
				_Baselineheart_sbp=getDoubleProperty("baselineHeart/sbp");
				return _Baselineheart_sbp;
			}else {
				return _Baselineheart_sbp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for baselineHeart/sbp.
	 * @param v Value to Set.
	 */
	public void setBaselineheart_sbp(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/baselineHeart/sbp",v);
		_Baselineheart_sbp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Baselineheart_dbp=null;

	/**
	 * @return Returns the baselineHeart/dbp.
	 */
	public Double getBaselineheart_dbp() {
		try{
			if (_Baselineheart_dbp==null){
				_Baselineheart_dbp=getDoubleProperty("baselineHeart/dbp");
				return _Baselineheart_dbp;
			}else {
				return _Baselineheart_dbp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for baselineHeart/dbp.
	 * @param v Value to Set.
	 */
	public void setBaselineheart_dbp(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/baselineHeart/dbp",v);
		_Baselineheart_dbp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Baselineheart_hr=null;

	/**
	 * @return Returns the baselineHeart/hr.
	 */
	public Integer getBaselineheart_hr() {
		try{
			if (_Baselineheart_hr==null){
				_Baselineheart_hr=getIntegerProperty("baselineHeart/hr");
				return _Baselineheart_hr;
			}else {
				return _Baselineheart_hr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for baselineHeart/hr.
	 * @param v Value to Set.
	 */
	public void setBaselineheart_hr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/baselineHeart/hr",v);
		_Baselineheart_hr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Thirtyminheart_sbp=null;

	/**
	 * @return Returns the thirtyMinHeart/sbp.
	 */
	public Double getThirtyminheart_sbp() {
		try{
			if (_Thirtyminheart_sbp==null){
				_Thirtyminheart_sbp=getDoubleProperty("thirtyMinHeart/sbp");
				return _Thirtyminheart_sbp;
			}else {
				return _Thirtyminheart_sbp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for thirtyMinHeart/sbp.
	 * @param v Value to Set.
	 */
	public void setThirtyminheart_sbp(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/thirtyMinHeart/sbp",v);
		_Thirtyminheart_sbp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Thirtyminheart_dbp=null;

	/**
	 * @return Returns the thirtyMinHeart/dbp.
	 */
	public Double getThirtyminheart_dbp() {
		try{
			if (_Thirtyminheart_dbp==null){
				_Thirtyminheart_dbp=getDoubleProperty("thirtyMinHeart/dbp");
				return _Thirtyminheart_dbp;
			}else {
				return _Thirtyminheart_dbp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for thirtyMinHeart/dbp.
	 * @param v Value to Set.
	 */
	public void setThirtyminheart_dbp(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/thirtyMinHeart/dbp",v);
		_Thirtyminheart_dbp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Thirtyminheart_hr=null;

	/**
	 * @return Returns the thirtyMinHeart/hr.
	 */
	public Integer getThirtyminheart_hr() {
		try{
			if (_Thirtyminheart_hr==null){
				_Thirtyminheart_hr=getIntegerProperty("thirtyMinHeart/hr");
				return _Thirtyminheart_hr;
			}else {
				return _Thirtyminheart_hr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for thirtyMinHeart/hr.
	 * @param v Value to Set.
	 */
	public void setThirtyminheart_hr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/thirtyMinHeart/hr",v);
		_Thirtyminheart_hr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Sixtyminheart_sbp=null;

	/**
	 * @return Returns the sixtyMinHeart/sbp.
	 */
	public Double getSixtyminheart_sbp() {
		try{
			if (_Sixtyminheart_sbp==null){
				_Sixtyminheart_sbp=getDoubleProperty("sixtyMinHeart/sbp");
				return _Sixtyminheart_sbp;
			}else {
				return _Sixtyminheart_sbp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sixtyMinHeart/sbp.
	 * @param v Value to Set.
	 */
	public void setSixtyminheart_sbp(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sixtyMinHeart/sbp",v);
		_Sixtyminheart_sbp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Sixtyminheart_dbp=null;

	/**
	 * @return Returns the sixtyMinHeart/dbp.
	 */
	public Double getSixtyminheart_dbp() {
		try{
			if (_Sixtyminheart_dbp==null){
				_Sixtyminheart_dbp=getDoubleProperty("sixtyMinHeart/dbp");
				return _Sixtyminheart_dbp;
			}else {
				return _Sixtyminheart_dbp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sixtyMinHeart/dbp.
	 * @param v Value to Set.
	 */
	public void setSixtyminheart_dbp(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sixtyMinHeart/dbp",v);
		_Sixtyminheart_dbp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sixtyminheart_hr=null;

	/**
	 * @return Returns the sixtyMinHeart/hr.
	 */
	public Integer getSixtyminheart_hr() {
		try{
			if (_Sixtyminheart_hr==null){
				_Sixtyminheart_hr=getIntegerProperty("sixtyMinHeart/hr");
				return _Sixtyminheart_hr;
			}else {
				return _Sixtyminheart_hr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sixtyMinHeart/hr.
	 * @param v Value to Set.
	 */
	public void setSixtyminheart_hr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sixtyMinHeart/hr",v);
		_Sixtyminheart_hr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Ki_roi1=null;

	/**
	 * @return Returns the ki/roi1.
	 */
	public Double getKi_roi1() {
		try{
			if (_Ki_roi1==null){
				_Ki_roi1=getDoubleProperty("ki/roi1");
				return _Ki_roi1;
			}else {
				return _Ki_roi1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ki/roi1.
	 * @param v Value to Set.
	 */
	public void setKi_roi1(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ki/roi1",v);
		_Ki_roi1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Ki_roi2=null;

	/**
	 * @return Returns the ki/roi2.
	 */
	public Double getKi_roi2() {
		try{
			if (_Ki_roi2==null){
				_Ki_roi2=getDoubleProperty("ki/roi2");
				return _Ki_roi2;
			}else {
				return _Ki_roi2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ki/roi2.
	 * @param v Value to Set.
	 */
	public void setKi_roi2(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ki/roi2",v);
		_Ki_roi2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Ki_roi3=null;

	/**
	 * @return Returns the ki/roi3.
	 */
	public Double getKi_roi3() {
		try{
			if (_Ki_roi3==null){
				_Ki_roi3=getDoubleProperty("ki/roi3");
				return _Ki_roi3;
			}else {
				return _Ki_roi3;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ki/roi3.
	 * @param v Value to Set.
	 */
	public void setKi_roi3(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ki/roi3",v);
		_Ki_roi3=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.GhfFdgcardioassessor> getAllGhfFdgcardioassessors(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.GhfFdgcardioassessor> al = new ArrayList<org.nrg.xdat.om.GhfFdgcardioassessor>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.GhfFdgcardioassessor> getGhfFdgcardioassessorsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.GhfFdgcardioassessor> al = new ArrayList<org.nrg.xdat.om.GhfFdgcardioassessor>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.GhfFdgcardioassessor> getGhfFdgcardioassessorsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.GhfFdgcardioassessor> al = new ArrayList<org.nrg.xdat.om.GhfFdgcardioassessor>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static GhfFdgcardioassessor getGhfFdgcardioassessorsById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("ghf:fdgCardioAssessor/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (GhfFdgcardioassessor) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //imageAssessorData
	        XnatImageassessordata childImageassessordata = (XnatImageassessordata)this.getImageassessordata();
	            if (childImageassessordata!=null){
	              for(ResourceFile rf: ((XnatImageassessordata)childImageassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("imageAssessorData[" + ((XnatImageassessordata)childImageassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("imageAssessorData/" + ((XnatImageassessordata)childImageassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
