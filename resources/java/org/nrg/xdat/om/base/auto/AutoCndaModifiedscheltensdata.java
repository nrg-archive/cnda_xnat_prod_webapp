/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaModifiedscheltensdata extends XnatMrassessordata implements org.nrg.xdat.model.CndaModifiedscheltensdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaModifiedscheltensdata.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:modifiedScheltensData";

	public AutoCndaModifiedscheltensdata(ItemI item)
	{
		super(item);
	}

	public AutoCndaModifiedscheltensdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaModifiedscheltensdata(UserI user)
	 **/
	public AutoCndaModifiedscheltensdata(){}

	public AutoCndaModifiedscheltensdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:modifiedScheltensData";
	}
	 private org.nrg.xdat.om.XnatMrassessordata _Mrassessordata =null;

	/**
	 * mrAssessorData
	 * @return org.nrg.xdat.om.XnatMrassessordata
	 */
	public org.nrg.xdat.om.XnatMrassessordata getMrassessordata() {
		try{
			if (_Mrassessordata==null){
				_Mrassessordata=((XnatMrassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("mrAssessorData")));
				return _Mrassessordata;
			}else {
				return _Mrassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for mrAssessorData.
	 * @param v Value to Set.
	 */
	public void setMrassessordata(ItemI v) throws Exception{
		_Mrassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/mrAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/mrAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * mrAssessorData
	 * set org.nrg.xdat.model.XnatMrassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatMrassessordataI> void setMrassessordata(A item) throws Exception{
	setMrassessordata((ItemI)item);
	}

	/**
	 * Removes the mrAssessorData.
	 * */
	public void removeMrassessordata() {
		_Mrassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/mrAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.CndaModifiedscheltensregion _Assessment_regions_deepWhiteMatter_frontal =null;

	/**
	 * assessment/regions/deep_white_matter/frontal
	 * @return org.nrg.xdat.om.CndaModifiedscheltensregion
	 */
	public org.nrg.xdat.om.CndaModifiedscheltensregion getAssessment_regions_deepWhiteMatter_frontal() {
		try{
			if (_Assessment_regions_deepWhiteMatter_frontal==null){
				_Assessment_regions_deepWhiteMatter_frontal=((CndaModifiedscheltensregion)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("assessment/regions/deep_white_matter/frontal")));
				return _Assessment_regions_deepWhiteMatter_frontal;
			}else {
				return _Assessment_regions_deepWhiteMatter_frontal;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for assessment/regions/deep_white_matter/frontal.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_deepWhiteMatter_frontal(ItemI v) throws Exception{
		_Assessment_regions_deepWhiteMatter_frontal =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/deep_white_matter/frontal",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/deep_white_matter/frontal",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * assessment/regions/deep_white_matter/frontal
	 * set org.nrg.xdat.model.CndaModifiedscheltensregionI
	 */
	public <A extends org.nrg.xdat.model.CndaModifiedscheltensregionI> void setAssessment_regions_deepWhiteMatter_frontal(A item) throws Exception{
	setAssessment_regions_deepWhiteMatter_frontal((ItemI)item);
	}

	/**
	 * Removes the assessment/regions/deep_white_matter/frontal.
	 * */
	public void removeAssessment_regions_deepWhiteMatter_frontal() {
		_Assessment_regions_deepWhiteMatter_frontal =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/deep_white_matter/frontal",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Assessment_regions_deepWhiteMatter_frontalFK=null;

	/**
	 * @return Returns the cnda:modifiedScheltensData/assessment_regions_deep_white_matter_frontal_cnda_modifiedschel.
	 */
	public Integer getAssessment_regions_deepWhiteMatter_frontalFK(){
		try{
			if (_Assessment_regions_deepWhiteMatter_frontalFK==null){
				_Assessment_regions_deepWhiteMatter_frontalFK=getIntegerProperty("cnda:modifiedScheltensData/assessment_regions_deep_white_matter_frontal_cnda_modifiedschel");
				return _Assessment_regions_deepWhiteMatter_frontalFK;
			}else {
				return _Assessment_regions_deepWhiteMatter_frontalFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda:modifiedScheltensData/assessment_regions_deep_white_matter_frontal_cnda_modifiedschel.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_deepWhiteMatter_frontalFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/assessment_regions_deep_white_matter_frontal_cnda_modifiedschel",v);
		_Assessment_regions_deepWhiteMatter_frontalFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.CndaModifiedscheltensregion _Assessment_regions_deepWhiteMatter_parietal =null;

	/**
	 * assessment/regions/deep_white_matter/parietal
	 * @return org.nrg.xdat.om.CndaModifiedscheltensregion
	 */
	public org.nrg.xdat.om.CndaModifiedscheltensregion getAssessment_regions_deepWhiteMatter_parietal() {
		try{
			if (_Assessment_regions_deepWhiteMatter_parietal==null){
				_Assessment_regions_deepWhiteMatter_parietal=((CndaModifiedscheltensregion)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("assessment/regions/deep_white_matter/parietal")));
				return _Assessment_regions_deepWhiteMatter_parietal;
			}else {
				return _Assessment_regions_deepWhiteMatter_parietal;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for assessment/regions/deep_white_matter/parietal.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_deepWhiteMatter_parietal(ItemI v) throws Exception{
		_Assessment_regions_deepWhiteMatter_parietal =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/deep_white_matter/parietal",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/deep_white_matter/parietal",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * assessment/regions/deep_white_matter/parietal
	 * set org.nrg.xdat.model.CndaModifiedscheltensregionI
	 */
	public <A extends org.nrg.xdat.model.CndaModifiedscheltensregionI> void setAssessment_regions_deepWhiteMatter_parietal(A item) throws Exception{
	setAssessment_regions_deepWhiteMatter_parietal((ItemI)item);
	}

	/**
	 * Removes the assessment/regions/deep_white_matter/parietal.
	 * */
	public void removeAssessment_regions_deepWhiteMatter_parietal() {
		_Assessment_regions_deepWhiteMatter_parietal =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/deep_white_matter/parietal",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Assessment_regions_deepWhiteMatter_parietalFK=null;

	/**
	 * @return Returns the cnda:modifiedScheltensData/assessment_regions_deep_white_matter_parietal_cnda_modifiedsche.
	 */
	public Integer getAssessment_regions_deepWhiteMatter_parietalFK(){
		try{
			if (_Assessment_regions_deepWhiteMatter_parietalFK==null){
				_Assessment_regions_deepWhiteMatter_parietalFK=getIntegerProperty("cnda:modifiedScheltensData/assessment_regions_deep_white_matter_parietal_cnda_modifiedsche");
				return _Assessment_regions_deepWhiteMatter_parietalFK;
			}else {
				return _Assessment_regions_deepWhiteMatter_parietalFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda:modifiedScheltensData/assessment_regions_deep_white_matter_parietal_cnda_modifiedsche.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_deepWhiteMatter_parietalFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/assessment_regions_deep_white_matter_parietal_cnda_modifiedsche",v);
		_Assessment_regions_deepWhiteMatter_parietalFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.CndaModifiedscheltensregion _Assessment_regions_deepWhiteMatter_occipital =null;

	/**
	 * assessment/regions/deep_white_matter/occipital
	 * @return org.nrg.xdat.om.CndaModifiedscheltensregion
	 */
	public org.nrg.xdat.om.CndaModifiedscheltensregion getAssessment_regions_deepWhiteMatter_occipital() {
		try{
			if (_Assessment_regions_deepWhiteMatter_occipital==null){
				_Assessment_regions_deepWhiteMatter_occipital=((CndaModifiedscheltensregion)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("assessment/regions/deep_white_matter/occipital")));
				return _Assessment_regions_deepWhiteMatter_occipital;
			}else {
				return _Assessment_regions_deepWhiteMatter_occipital;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for assessment/regions/deep_white_matter/occipital.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_deepWhiteMatter_occipital(ItemI v) throws Exception{
		_Assessment_regions_deepWhiteMatter_occipital =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/deep_white_matter/occipital",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/deep_white_matter/occipital",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * assessment/regions/deep_white_matter/occipital
	 * set org.nrg.xdat.model.CndaModifiedscheltensregionI
	 */
	public <A extends org.nrg.xdat.model.CndaModifiedscheltensregionI> void setAssessment_regions_deepWhiteMatter_occipital(A item) throws Exception{
	setAssessment_regions_deepWhiteMatter_occipital((ItemI)item);
	}

	/**
	 * Removes the assessment/regions/deep_white_matter/occipital.
	 * */
	public void removeAssessment_regions_deepWhiteMatter_occipital() {
		_Assessment_regions_deepWhiteMatter_occipital =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/deep_white_matter/occipital",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Assessment_regions_deepWhiteMatter_occipitalFK=null;

	/**
	 * @return Returns the cnda:modifiedScheltensData/assessment_regions_deep_white_matter_occipital_cnda_modifiedsch.
	 */
	public Integer getAssessment_regions_deepWhiteMatter_occipitalFK(){
		try{
			if (_Assessment_regions_deepWhiteMatter_occipitalFK==null){
				_Assessment_regions_deepWhiteMatter_occipitalFK=getIntegerProperty("cnda:modifiedScheltensData/assessment_regions_deep_white_matter_occipital_cnda_modifiedsch");
				return _Assessment_regions_deepWhiteMatter_occipitalFK;
			}else {
				return _Assessment_regions_deepWhiteMatter_occipitalFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda:modifiedScheltensData/assessment_regions_deep_white_matter_occipital_cnda_modifiedsch.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_deepWhiteMatter_occipitalFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/assessment_regions_deep_white_matter_occipital_cnda_modifiedsch",v);
		_Assessment_regions_deepWhiteMatter_occipitalFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.CndaModifiedscheltensregion _Assessment_regions_deepWhiteMatter_temporal =null;

	/**
	 * assessment/regions/deep_white_matter/temporal
	 * @return org.nrg.xdat.om.CndaModifiedscheltensregion
	 */
	public org.nrg.xdat.om.CndaModifiedscheltensregion getAssessment_regions_deepWhiteMatter_temporal() {
		try{
			if (_Assessment_regions_deepWhiteMatter_temporal==null){
				_Assessment_regions_deepWhiteMatter_temporal=((CndaModifiedscheltensregion)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("assessment/regions/deep_white_matter/temporal")));
				return _Assessment_regions_deepWhiteMatter_temporal;
			}else {
				return _Assessment_regions_deepWhiteMatter_temporal;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for assessment/regions/deep_white_matter/temporal.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_deepWhiteMatter_temporal(ItemI v) throws Exception{
		_Assessment_regions_deepWhiteMatter_temporal =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/deep_white_matter/temporal",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/deep_white_matter/temporal",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * assessment/regions/deep_white_matter/temporal
	 * set org.nrg.xdat.model.CndaModifiedscheltensregionI
	 */
	public <A extends org.nrg.xdat.model.CndaModifiedscheltensregionI> void setAssessment_regions_deepWhiteMatter_temporal(A item) throws Exception{
	setAssessment_regions_deepWhiteMatter_temporal((ItemI)item);
	}

	/**
	 * Removes the assessment/regions/deep_white_matter/temporal.
	 * */
	public void removeAssessment_regions_deepWhiteMatter_temporal() {
		_Assessment_regions_deepWhiteMatter_temporal =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/deep_white_matter/temporal",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Assessment_regions_deepWhiteMatter_temporalFK=null;

	/**
	 * @return Returns the cnda:modifiedScheltensData/assessment_regions_deep_white_matter_temporal_cnda_modifiedsche.
	 */
	public Integer getAssessment_regions_deepWhiteMatter_temporalFK(){
		try{
			if (_Assessment_regions_deepWhiteMatter_temporalFK==null){
				_Assessment_regions_deepWhiteMatter_temporalFK=getIntegerProperty("cnda:modifiedScheltensData/assessment_regions_deep_white_matter_temporal_cnda_modifiedsche");
				return _Assessment_regions_deepWhiteMatter_temporalFK;
			}else {
				return _Assessment_regions_deepWhiteMatter_temporalFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda:modifiedScheltensData/assessment_regions_deep_white_matter_temporal_cnda_modifiedsche.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_deepWhiteMatter_temporalFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/assessment_regions_deep_white_matter_temporal_cnda_modifiedsche",v);
		_Assessment_regions_deepWhiteMatter_temporalFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.CndaModifiedscheltenspvregion _Assessment_regions_periventricular_frontalHorns =null;

	/**
	 * assessment/regions/periventricular/frontal_horns
	 * @return org.nrg.xdat.om.CndaModifiedscheltenspvregion
	 */
	public org.nrg.xdat.om.CndaModifiedscheltenspvregion getAssessment_regions_periventricular_frontalHorns() {
		try{
			if (_Assessment_regions_periventricular_frontalHorns==null){
				_Assessment_regions_periventricular_frontalHorns=((CndaModifiedscheltenspvregion)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("assessment/regions/periventricular/frontal_horns")));
				return _Assessment_regions_periventricular_frontalHorns;
			}else {
				return _Assessment_regions_periventricular_frontalHorns;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for assessment/regions/periventricular/frontal_horns.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_periventricular_frontalHorns(ItemI v) throws Exception{
		_Assessment_regions_periventricular_frontalHorns =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/periventricular/frontal_horns",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/periventricular/frontal_horns",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * assessment/regions/periventricular/frontal_horns
	 * set org.nrg.xdat.model.CndaModifiedscheltenspvregionI
	 */
	public <A extends org.nrg.xdat.model.CndaModifiedscheltenspvregionI> void setAssessment_regions_periventricular_frontalHorns(A item) throws Exception{
	setAssessment_regions_periventricular_frontalHorns((ItemI)item);
	}

	/**
	 * Removes the assessment/regions/periventricular/frontal_horns.
	 * */
	public void removeAssessment_regions_periventricular_frontalHorns() {
		_Assessment_regions_periventricular_frontalHorns =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/periventricular/frontal_horns",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Assessment_regions_periventricular_frontalHornsFK=null;

	/**
	 * @return Returns the cnda:modifiedScheltensData/assessment_regions_periventricular_frontal_horns_cnda_modifieds.
	 */
	public Integer getAssessment_regions_periventricular_frontalHornsFK(){
		try{
			if (_Assessment_regions_periventricular_frontalHornsFK==null){
				_Assessment_regions_periventricular_frontalHornsFK=getIntegerProperty("cnda:modifiedScheltensData/assessment_regions_periventricular_frontal_horns_cnda_modifieds");
				return _Assessment_regions_periventricular_frontalHornsFK;
			}else {
				return _Assessment_regions_periventricular_frontalHornsFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda:modifiedScheltensData/assessment_regions_periventricular_frontal_horns_cnda_modifieds.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_periventricular_frontalHornsFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/assessment_regions_periventricular_frontal_horns_cnda_modifieds",v);
		_Assessment_regions_periventricular_frontalHornsFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.CndaModifiedscheltenspvregion _Assessment_regions_periventricular_posteriorHorns =null;

	/**
	 * assessment/regions/periventricular/posterior_horns
	 * @return org.nrg.xdat.om.CndaModifiedscheltenspvregion
	 */
	public org.nrg.xdat.om.CndaModifiedscheltenspvregion getAssessment_regions_periventricular_posteriorHorns() {
		try{
			if (_Assessment_regions_periventricular_posteriorHorns==null){
				_Assessment_regions_periventricular_posteriorHorns=((CndaModifiedscheltenspvregion)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("assessment/regions/periventricular/posterior_horns")));
				return _Assessment_regions_periventricular_posteriorHorns;
			}else {
				return _Assessment_regions_periventricular_posteriorHorns;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for assessment/regions/periventricular/posterior_horns.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_periventricular_posteriorHorns(ItemI v) throws Exception{
		_Assessment_regions_periventricular_posteriorHorns =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/periventricular/posterior_horns",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/periventricular/posterior_horns",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * assessment/regions/periventricular/posterior_horns
	 * set org.nrg.xdat.model.CndaModifiedscheltenspvregionI
	 */
	public <A extends org.nrg.xdat.model.CndaModifiedscheltenspvregionI> void setAssessment_regions_periventricular_posteriorHorns(A item) throws Exception{
	setAssessment_regions_periventricular_posteriorHorns((ItemI)item);
	}

	/**
	 * Removes the assessment/regions/periventricular/posterior_horns.
	 * */
	public void removeAssessment_regions_periventricular_posteriorHorns() {
		_Assessment_regions_periventricular_posteriorHorns =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/periventricular/posterior_horns",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Assessment_regions_periventricular_posteriorHornsFK=null;

	/**
	 * @return Returns the cnda:modifiedScheltensData/assessment_regions_periventricular_posterior_horns_cnda_modifie.
	 */
	public Integer getAssessment_regions_periventricular_posteriorHornsFK(){
		try{
			if (_Assessment_regions_periventricular_posteriorHornsFK==null){
				_Assessment_regions_periventricular_posteriorHornsFK=getIntegerProperty("cnda:modifiedScheltensData/assessment_regions_periventricular_posterior_horns_cnda_modifie");
				return _Assessment_regions_periventricular_posteriorHornsFK;
			}else {
				return _Assessment_regions_periventricular_posteriorHornsFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda:modifiedScheltensData/assessment_regions_periventricular_posterior_horns_cnda_modifie.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_periventricular_posteriorHornsFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/assessment_regions_periventricular_posterior_horns_cnda_modifie",v);
		_Assessment_regions_periventricular_posteriorHornsFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.CndaModifiedscheltenspvregion _Assessment_regions_periventricular_ventricularBody =null;

	/**
	 * assessment/regions/periventricular/ventricular_body
	 * @return org.nrg.xdat.om.CndaModifiedscheltenspvregion
	 */
	public org.nrg.xdat.om.CndaModifiedscheltenspvregion getAssessment_regions_periventricular_ventricularBody() {
		try{
			if (_Assessment_regions_periventricular_ventricularBody==null){
				_Assessment_regions_periventricular_ventricularBody=((CndaModifiedscheltenspvregion)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("assessment/regions/periventricular/ventricular_body")));
				return _Assessment_regions_periventricular_ventricularBody;
			}else {
				return _Assessment_regions_periventricular_ventricularBody;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for assessment/regions/periventricular/ventricular_body.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_periventricular_ventricularBody(ItemI v) throws Exception{
		_Assessment_regions_periventricular_ventricularBody =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/periventricular/ventricular_body",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/periventricular/ventricular_body",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * assessment/regions/periventricular/ventricular_body
	 * set org.nrg.xdat.model.CndaModifiedscheltenspvregionI
	 */
	public <A extends org.nrg.xdat.model.CndaModifiedscheltenspvregionI> void setAssessment_regions_periventricular_ventricularBody(A item) throws Exception{
	setAssessment_regions_periventricular_ventricularBody((ItemI)item);
	}

	/**
	 * Removes the assessment/regions/periventricular/ventricular_body.
	 * */
	public void removeAssessment_regions_periventricular_ventricularBody() {
		_Assessment_regions_periventricular_ventricularBody =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/periventricular/ventricular_body",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Assessment_regions_periventricular_ventricularBodyFK=null;

	/**
	 * @return Returns the cnda:modifiedScheltensData/assessment_regions_periventricular_ventricular_body_cnda_modifi.
	 */
	public Integer getAssessment_regions_periventricular_ventricularBodyFK(){
		try{
			if (_Assessment_regions_periventricular_ventricularBodyFK==null){
				_Assessment_regions_periventricular_ventricularBodyFK=getIntegerProperty("cnda:modifiedScheltensData/assessment_regions_periventricular_ventricular_body_cnda_modifi");
				return _Assessment_regions_periventricular_ventricularBodyFK;
			}else {
				return _Assessment_regions_periventricular_ventricularBodyFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda:modifiedScheltensData/assessment_regions_periventricular_ventricular_body_cnda_modifi.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_periventricular_ventricularBodyFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/assessment_regions_periventricular_ventricular_body_cnda_modifi",v);
		_Assessment_regions_periventricular_ventricularBodyFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.CndaModifiedscheltensregion _Assessment_regions_basalGanglia_internalCapsule =null;

	/**
	 * assessment/regions/basal_ganglia/internal_capsule
	 * @return org.nrg.xdat.om.CndaModifiedscheltensregion
	 */
	public org.nrg.xdat.om.CndaModifiedscheltensregion getAssessment_regions_basalGanglia_internalCapsule() {
		try{
			if (_Assessment_regions_basalGanglia_internalCapsule==null){
				_Assessment_regions_basalGanglia_internalCapsule=((CndaModifiedscheltensregion)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("assessment/regions/basal_ganglia/internal_capsule")));
				return _Assessment_regions_basalGanglia_internalCapsule;
			}else {
				return _Assessment_regions_basalGanglia_internalCapsule;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for assessment/regions/basal_ganglia/internal_capsule.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_basalGanglia_internalCapsule(ItemI v) throws Exception{
		_Assessment_regions_basalGanglia_internalCapsule =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/basal_ganglia/internal_capsule",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/basal_ganglia/internal_capsule",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * assessment/regions/basal_ganglia/internal_capsule
	 * set org.nrg.xdat.model.CndaModifiedscheltensregionI
	 */
	public <A extends org.nrg.xdat.model.CndaModifiedscheltensregionI> void setAssessment_regions_basalGanglia_internalCapsule(A item) throws Exception{
	setAssessment_regions_basalGanglia_internalCapsule((ItemI)item);
	}

	/**
	 * Removes the assessment/regions/basal_ganglia/internal_capsule.
	 * */
	public void removeAssessment_regions_basalGanglia_internalCapsule() {
		_Assessment_regions_basalGanglia_internalCapsule =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/basal_ganglia/internal_capsule",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Assessment_regions_basalGanglia_internalCapsuleFK=null;

	/**
	 * @return Returns the cnda:modifiedScheltensData/assessment_regions_basal_ganglia_internal_capsule_cnda_modified.
	 */
	public Integer getAssessment_regions_basalGanglia_internalCapsuleFK(){
		try{
			if (_Assessment_regions_basalGanglia_internalCapsuleFK==null){
				_Assessment_regions_basalGanglia_internalCapsuleFK=getIntegerProperty("cnda:modifiedScheltensData/assessment_regions_basal_ganglia_internal_capsule_cnda_modified");
				return _Assessment_regions_basalGanglia_internalCapsuleFK;
			}else {
				return _Assessment_regions_basalGanglia_internalCapsuleFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda:modifiedScheltensData/assessment_regions_basal_ganglia_internal_capsule_cnda_modified.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_basalGanglia_internalCapsuleFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/assessment_regions_basal_ganglia_internal_capsule_cnda_modified",v);
		_Assessment_regions_basalGanglia_internalCapsuleFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.CndaModifiedscheltensregion _Assessment_regions_basalGanglia_caudate =null;

	/**
	 * assessment/regions/basal_ganglia/caudate
	 * @return org.nrg.xdat.om.CndaModifiedscheltensregion
	 */
	public org.nrg.xdat.om.CndaModifiedscheltensregion getAssessment_regions_basalGanglia_caudate() {
		try{
			if (_Assessment_regions_basalGanglia_caudate==null){
				_Assessment_regions_basalGanglia_caudate=((CndaModifiedscheltensregion)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("assessment/regions/basal_ganglia/caudate")));
				return _Assessment_regions_basalGanglia_caudate;
			}else {
				return _Assessment_regions_basalGanglia_caudate;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for assessment/regions/basal_ganglia/caudate.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_basalGanglia_caudate(ItemI v) throws Exception{
		_Assessment_regions_basalGanglia_caudate =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/basal_ganglia/caudate",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/basal_ganglia/caudate",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * assessment/regions/basal_ganglia/caudate
	 * set org.nrg.xdat.model.CndaModifiedscheltensregionI
	 */
	public <A extends org.nrg.xdat.model.CndaModifiedscheltensregionI> void setAssessment_regions_basalGanglia_caudate(A item) throws Exception{
	setAssessment_regions_basalGanglia_caudate((ItemI)item);
	}

	/**
	 * Removes the assessment/regions/basal_ganglia/caudate.
	 * */
	public void removeAssessment_regions_basalGanglia_caudate() {
		_Assessment_regions_basalGanglia_caudate =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/basal_ganglia/caudate",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Assessment_regions_basalGanglia_caudateFK=null;

	/**
	 * @return Returns the cnda:modifiedScheltensData/assessment_regions_basal_ganglia_caudate_cnda_modifiedscheltens.
	 */
	public Integer getAssessment_regions_basalGanglia_caudateFK(){
		try{
			if (_Assessment_regions_basalGanglia_caudateFK==null){
				_Assessment_regions_basalGanglia_caudateFK=getIntegerProperty("cnda:modifiedScheltensData/assessment_regions_basal_ganglia_caudate_cnda_modifiedscheltens");
				return _Assessment_regions_basalGanglia_caudateFK;
			}else {
				return _Assessment_regions_basalGanglia_caudateFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda:modifiedScheltensData/assessment_regions_basal_ganglia_caudate_cnda_modifiedscheltens.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_basalGanglia_caudateFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/assessment_regions_basal_ganglia_caudate_cnda_modifiedscheltens",v);
		_Assessment_regions_basalGanglia_caudateFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.CndaModifiedscheltensregion _Assessment_regions_basalGanglia_putamen =null;

	/**
	 * assessment/regions/basal_ganglia/putamen
	 * @return org.nrg.xdat.om.CndaModifiedscheltensregion
	 */
	public org.nrg.xdat.om.CndaModifiedscheltensregion getAssessment_regions_basalGanglia_putamen() {
		try{
			if (_Assessment_regions_basalGanglia_putamen==null){
				_Assessment_regions_basalGanglia_putamen=((CndaModifiedscheltensregion)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("assessment/regions/basal_ganglia/putamen")));
				return _Assessment_regions_basalGanglia_putamen;
			}else {
				return _Assessment_regions_basalGanglia_putamen;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for assessment/regions/basal_ganglia/putamen.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_basalGanglia_putamen(ItemI v) throws Exception{
		_Assessment_regions_basalGanglia_putamen =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/basal_ganglia/putamen",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/basal_ganglia/putamen",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * assessment/regions/basal_ganglia/putamen
	 * set org.nrg.xdat.model.CndaModifiedscheltensregionI
	 */
	public <A extends org.nrg.xdat.model.CndaModifiedscheltensregionI> void setAssessment_regions_basalGanglia_putamen(A item) throws Exception{
	setAssessment_regions_basalGanglia_putamen((ItemI)item);
	}

	/**
	 * Removes the assessment/regions/basal_ganglia/putamen.
	 * */
	public void removeAssessment_regions_basalGanglia_putamen() {
		_Assessment_regions_basalGanglia_putamen =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/basal_ganglia/putamen",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Assessment_regions_basalGanglia_putamenFK=null;

	/**
	 * @return Returns the cnda:modifiedScheltensData/assessment_regions_basal_ganglia_putamen_cnda_modifiedscheltens.
	 */
	public Integer getAssessment_regions_basalGanglia_putamenFK(){
		try{
			if (_Assessment_regions_basalGanglia_putamenFK==null){
				_Assessment_regions_basalGanglia_putamenFK=getIntegerProperty("cnda:modifiedScheltensData/assessment_regions_basal_ganglia_putamen_cnda_modifiedscheltens");
				return _Assessment_regions_basalGanglia_putamenFK;
			}else {
				return _Assessment_regions_basalGanglia_putamenFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda:modifiedScheltensData/assessment_regions_basal_ganglia_putamen_cnda_modifiedscheltens.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_basalGanglia_putamenFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/assessment_regions_basal_ganglia_putamen_cnda_modifiedscheltens",v);
		_Assessment_regions_basalGanglia_putamenFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.CndaModifiedscheltensregion _Assessment_regions_basalGanglia_globusPallidus =null;

	/**
	 * assessment/regions/basal_ganglia/globus_pallidus
	 * @return org.nrg.xdat.om.CndaModifiedscheltensregion
	 */
	public org.nrg.xdat.om.CndaModifiedscheltensregion getAssessment_regions_basalGanglia_globusPallidus() {
		try{
			if (_Assessment_regions_basalGanglia_globusPallidus==null){
				_Assessment_regions_basalGanglia_globusPallidus=((CndaModifiedscheltensregion)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("assessment/regions/basal_ganglia/globus_pallidus")));
				return _Assessment_regions_basalGanglia_globusPallidus;
			}else {
				return _Assessment_regions_basalGanglia_globusPallidus;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for assessment/regions/basal_ganglia/globus_pallidus.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_basalGanglia_globusPallidus(ItemI v) throws Exception{
		_Assessment_regions_basalGanglia_globusPallidus =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/basal_ganglia/globus_pallidus",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/basal_ganglia/globus_pallidus",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * assessment/regions/basal_ganglia/globus_pallidus
	 * set org.nrg.xdat.model.CndaModifiedscheltensregionI
	 */
	public <A extends org.nrg.xdat.model.CndaModifiedscheltensregionI> void setAssessment_regions_basalGanglia_globusPallidus(A item) throws Exception{
	setAssessment_regions_basalGanglia_globusPallidus((ItemI)item);
	}

	/**
	 * Removes the assessment/regions/basal_ganglia/globus_pallidus.
	 * */
	public void removeAssessment_regions_basalGanglia_globusPallidus() {
		_Assessment_regions_basalGanglia_globusPallidus =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/basal_ganglia/globus_pallidus",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Assessment_regions_basalGanglia_globusPallidusFK=null;

	/**
	 * @return Returns the cnda:modifiedScheltensData/assessment_regions_basal_ganglia_globus_pallidus_cnda_modifieds.
	 */
	public Integer getAssessment_regions_basalGanglia_globusPallidusFK(){
		try{
			if (_Assessment_regions_basalGanglia_globusPallidusFK==null){
				_Assessment_regions_basalGanglia_globusPallidusFK=getIntegerProperty("cnda:modifiedScheltensData/assessment_regions_basal_ganglia_globus_pallidus_cnda_modifieds");
				return _Assessment_regions_basalGanglia_globusPallidusFK;
			}else {
				return _Assessment_regions_basalGanglia_globusPallidusFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda:modifiedScheltensData/assessment_regions_basal_ganglia_globus_pallidus_cnda_modifieds.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_basalGanglia_globusPallidusFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/assessment_regions_basal_ganglia_globus_pallidus_cnda_modifieds",v);
		_Assessment_regions_basalGanglia_globusPallidusFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.CndaModifiedscheltensregion _Assessment_regions_basalGanglia_thalamus =null;

	/**
	 * assessment/regions/basal_ganglia/thalamus
	 * @return org.nrg.xdat.om.CndaModifiedscheltensregion
	 */
	public org.nrg.xdat.om.CndaModifiedscheltensregion getAssessment_regions_basalGanglia_thalamus() {
		try{
			if (_Assessment_regions_basalGanglia_thalamus==null){
				_Assessment_regions_basalGanglia_thalamus=((CndaModifiedscheltensregion)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("assessment/regions/basal_ganglia/thalamus")));
				return _Assessment_regions_basalGanglia_thalamus;
			}else {
				return _Assessment_regions_basalGanglia_thalamus;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for assessment/regions/basal_ganglia/thalamus.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_basalGanglia_thalamus(ItemI v) throws Exception{
		_Assessment_regions_basalGanglia_thalamus =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/basal_ganglia/thalamus",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/basal_ganglia/thalamus",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * assessment/regions/basal_ganglia/thalamus
	 * set org.nrg.xdat.model.CndaModifiedscheltensregionI
	 */
	public <A extends org.nrg.xdat.model.CndaModifiedscheltensregionI> void setAssessment_regions_basalGanglia_thalamus(A item) throws Exception{
	setAssessment_regions_basalGanglia_thalamus((ItemI)item);
	}

	/**
	 * Removes the assessment/regions/basal_ganglia/thalamus.
	 * */
	public void removeAssessment_regions_basalGanglia_thalamus() {
		_Assessment_regions_basalGanglia_thalamus =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/assessment/regions/basal_ganglia/thalamus",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Assessment_regions_basalGanglia_thalamusFK=null;

	/**
	 * @return Returns the cnda:modifiedScheltensData/assessment_regions_basal_ganglia_thalamus_cnda_modifiedschelten.
	 */
	public Integer getAssessment_regions_basalGanglia_thalamusFK(){
		try{
			if (_Assessment_regions_basalGanglia_thalamusFK==null){
				_Assessment_regions_basalGanglia_thalamusFK=getIntegerProperty("cnda:modifiedScheltensData/assessment_regions_basal_ganglia_thalamus_cnda_modifiedschelten");
				return _Assessment_regions_basalGanglia_thalamusFK;
			}else {
				return _Assessment_regions_basalGanglia_thalamusFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda:modifiedScheltensData/assessment_regions_basal_ganglia_thalamus_cnda_modifiedschelten.
	 * @param v Value to Set.
	 */
	public void setAssessment_regions_basalGanglia_thalamusFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/assessment_regions_basal_ganglia_thalamus_cnda_modifiedschelten",v);
		_Assessment_regions_basalGanglia_thalamusFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Assessment_virchowRobin_present=null;

	/**
	 * @return Returns the assessment/virchow_robin/present.
	 */
	public Boolean getAssessment_virchowRobin_present() {
		try{
			if (_Assessment_virchowRobin_present==null){
				_Assessment_virchowRobin_present=getBooleanProperty("assessment/virchow_robin/present");
				return _Assessment_virchowRobin_present;
			}else {
				return _Assessment_virchowRobin_present;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for assessment/virchow_robin/present.
	 * @param v Value to Set.
	 */
	public void setAssessment_virchowRobin_present(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/assessment/virchow_robin/present",v);
		_Assessment_virchowRobin_present=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Assessment_virchowRobin_location=null;

	/**
	 * @return Returns the assessment/virchow_robin/location.
	 */
	public String getAssessment_virchowRobin_location(){
		try{
			if (_Assessment_virchowRobin_location==null){
				_Assessment_virchowRobin_location=getStringProperty("assessment/virchow_robin/location");
				return _Assessment_virchowRobin_location;
			}else {
				return _Assessment_virchowRobin_location;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for assessment/virchow_robin/location.
	 * @param v Value to Set.
	 */
	public void setAssessment_virchowRobin_location(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/assessment/virchow_robin/location",v);
		_Assessment_virchowRobin_location=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Assessment_virchowRobin_potentialContribution=null;

	/**
	 * @return Returns the assessment/virchow_robin/potential_contribution.
	 */
	public Boolean getAssessment_virchowRobin_potentialContribution() {
		try{
			if (_Assessment_virchowRobin_potentialContribution==null){
				_Assessment_virchowRobin_potentialContribution=getBooleanProperty("assessment/virchow_robin/potential_contribution");
				return _Assessment_virchowRobin_potentialContribution;
			}else {
				return _Assessment_virchowRobin_potentialContribution;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for assessment/virchow_robin/potential_contribution.
	 * @param v Value to Set.
	 */
	public void setAssessment_virchowRobin_potentialContribution(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/assessment/virchow_robin/potential_contribution",v);
		_Assessment_virchowRobin_potentialContribution=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Assessment_virchowRobin_contributionLocation=null;

	/**
	 * @return Returns the assessment/virchow_robin/contribution_location.
	 */
	public String getAssessment_virchowRobin_contributionLocation(){
		try{
			if (_Assessment_virchowRobin_contributionLocation==null){
				_Assessment_virchowRobin_contributionLocation=getStringProperty("assessment/virchow_robin/contribution_location");
				return _Assessment_virchowRobin_contributionLocation;
			}else {
				return _Assessment_virchowRobin_contributionLocation;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for assessment/virchow_robin/contribution_location.
	 * @param v Value to Set.
	 */
	public void setAssessment_virchowRobin_contributionLocation(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/assessment/virchow_robin/contribution_location",v);
		_Assessment_virchowRobin_contributionLocation=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Assessment_confluent=null;

	/**
	 * @return Returns the assessment/confluent.
	 */
	public Boolean getAssessment_confluent() {
		try{
			if (_Assessment_confluent==null){
				_Assessment_confluent=getBooleanProperty("assessment/confluent");
				return _Assessment_confluent;
			}else {
				return _Assessment_confluent;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for assessment/confluent.
	 * @param v Value to Set.
	 */
	public void setAssessment_confluent(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/assessment/confluent",v);
		_Assessment_confluent=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaModifiedscheltensdata> getAllCndaModifiedscheltensdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaModifiedscheltensdata> al = new ArrayList<org.nrg.xdat.om.CndaModifiedscheltensdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaModifiedscheltensdata> getCndaModifiedscheltensdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaModifiedscheltensdata> al = new ArrayList<org.nrg.xdat.om.CndaModifiedscheltensdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaModifiedscheltensdata> getCndaModifiedscheltensdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaModifiedscheltensdata> al = new ArrayList<org.nrg.xdat.om.CndaModifiedscheltensdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaModifiedscheltensdata getCndaModifiedscheltensdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:modifiedScheltensData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaModifiedscheltensdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		XFTItem mr = org.nrg.xft.search.ItemSearch.GetItem("xnat:mrSessionData.ID",getItem().getProperty("cnda:modifiedScheltensData.imageSession_ID"),getItem().getUser(),false);
		al.add(mr);
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",mr.getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //mrAssessorData
	        XnatMrassessordata childMrassessordata = (XnatMrassessordata)this.getMrassessordata();
	            if (childMrassessordata!=null){
	              for(ResourceFile rf: ((XnatMrassessordata)childMrassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("mrAssessorData[" + ((XnatMrassessordata)childMrassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("mrAssessorData/" + ((XnatMrassessordata)childMrassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //assessment/regions/deep_white_matter/frontal
	        CndaModifiedscheltensregion childAssessment_regions_deepWhiteMatter_frontal = (CndaModifiedscheltensregion)this.getAssessment_regions_deepWhiteMatter_frontal();
	            if (childAssessment_regions_deepWhiteMatter_frontal!=null){
	              for(ResourceFile rf: ((CndaModifiedscheltensregion)childAssessment_regions_deepWhiteMatter_frontal).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("assessment/regions/deep_white_matter/frontal[" + ((CndaModifiedscheltensregion)childAssessment_regions_deepWhiteMatter_frontal).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("assessment/regions/deep_white_matter/frontal/" + ((CndaModifiedscheltensregion)childAssessment_regions_deepWhiteMatter_frontal).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //assessment/regions/deep_white_matter/parietal
	        CndaModifiedscheltensregion childAssessment_regions_deepWhiteMatter_parietal = (CndaModifiedscheltensregion)this.getAssessment_regions_deepWhiteMatter_parietal();
	            if (childAssessment_regions_deepWhiteMatter_parietal!=null){
	              for(ResourceFile rf: ((CndaModifiedscheltensregion)childAssessment_regions_deepWhiteMatter_parietal).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("assessment/regions/deep_white_matter/parietal[" + ((CndaModifiedscheltensregion)childAssessment_regions_deepWhiteMatter_parietal).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("assessment/regions/deep_white_matter/parietal/" + ((CndaModifiedscheltensregion)childAssessment_regions_deepWhiteMatter_parietal).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //assessment/regions/deep_white_matter/occipital
	        CndaModifiedscheltensregion childAssessment_regions_deepWhiteMatter_occipital = (CndaModifiedscheltensregion)this.getAssessment_regions_deepWhiteMatter_occipital();
	            if (childAssessment_regions_deepWhiteMatter_occipital!=null){
	              for(ResourceFile rf: ((CndaModifiedscheltensregion)childAssessment_regions_deepWhiteMatter_occipital).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("assessment/regions/deep_white_matter/occipital[" + ((CndaModifiedscheltensregion)childAssessment_regions_deepWhiteMatter_occipital).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("assessment/regions/deep_white_matter/occipital/" + ((CndaModifiedscheltensregion)childAssessment_regions_deepWhiteMatter_occipital).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //assessment/regions/deep_white_matter/temporal
	        CndaModifiedscheltensregion childAssessment_regions_deepWhiteMatter_temporal = (CndaModifiedscheltensregion)this.getAssessment_regions_deepWhiteMatter_temporal();
	            if (childAssessment_regions_deepWhiteMatter_temporal!=null){
	              for(ResourceFile rf: ((CndaModifiedscheltensregion)childAssessment_regions_deepWhiteMatter_temporal).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("assessment/regions/deep_white_matter/temporal[" + ((CndaModifiedscheltensregion)childAssessment_regions_deepWhiteMatter_temporal).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("assessment/regions/deep_white_matter/temporal/" + ((CndaModifiedscheltensregion)childAssessment_regions_deepWhiteMatter_temporal).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //assessment/regions/periventricular/frontal_horns
	        CndaModifiedscheltenspvregion childAssessment_regions_periventricular_frontalHorns = (CndaModifiedscheltenspvregion)this.getAssessment_regions_periventricular_frontalHorns();
	            if (childAssessment_regions_periventricular_frontalHorns!=null){
	              for(ResourceFile rf: ((CndaModifiedscheltenspvregion)childAssessment_regions_periventricular_frontalHorns).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("assessment/regions/periventricular/frontal_horns[" + ((CndaModifiedscheltenspvregion)childAssessment_regions_periventricular_frontalHorns).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("assessment/regions/periventricular/frontal_horns/" + ((CndaModifiedscheltenspvregion)childAssessment_regions_periventricular_frontalHorns).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //assessment/regions/periventricular/posterior_horns
	        CndaModifiedscheltenspvregion childAssessment_regions_periventricular_posteriorHorns = (CndaModifiedscheltenspvregion)this.getAssessment_regions_periventricular_posteriorHorns();
	            if (childAssessment_regions_periventricular_posteriorHorns!=null){
	              for(ResourceFile rf: ((CndaModifiedscheltenspvregion)childAssessment_regions_periventricular_posteriorHorns).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("assessment/regions/periventricular/posterior_horns[" + ((CndaModifiedscheltenspvregion)childAssessment_regions_periventricular_posteriorHorns).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("assessment/regions/periventricular/posterior_horns/" + ((CndaModifiedscheltenspvregion)childAssessment_regions_periventricular_posteriorHorns).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //assessment/regions/periventricular/ventricular_body
	        CndaModifiedscheltenspvregion childAssessment_regions_periventricular_ventricularBody = (CndaModifiedscheltenspvregion)this.getAssessment_regions_periventricular_ventricularBody();
	            if (childAssessment_regions_periventricular_ventricularBody!=null){
	              for(ResourceFile rf: ((CndaModifiedscheltenspvregion)childAssessment_regions_periventricular_ventricularBody).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("assessment/regions/periventricular/ventricular_body[" + ((CndaModifiedscheltenspvregion)childAssessment_regions_periventricular_ventricularBody).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("assessment/regions/periventricular/ventricular_body/" + ((CndaModifiedscheltenspvregion)childAssessment_regions_periventricular_ventricularBody).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //assessment/regions/basal_ganglia/internal_capsule
	        CndaModifiedscheltensregion childAssessment_regions_basalGanglia_internalCapsule = (CndaModifiedscheltensregion)this.getAssessment_regions_basalGanglia_internalCapsule();
	            if (childAssessment_regions_basalGanglia_internalCapsule!=null){
	              for(ResourceFile rf: ((CndaModifiedscheltensregion)childAssessment_regions_basalGanglia_internalCapsule).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("assessment/regions/basal_ganglia/internal_capsule[" + ((CndaModifiedscheltensregion)childAssessment_regions_basalGanglia_internalCapsule).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("assessment/regions/basal_ganglia/internal_capsule/" + ((CndaModifiedscheltensregion)childAssessment_regions_basalGanglia_internalCapsule).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //assessment/regions/basal_ganglia/caudate
	        CndaModifiedscheltensregion childAssessment_regions_basalGanglia_caudate = (CndaModifiedscheltensregion)this.getAssessment_regions_basalGanglia_caudate();
	            if (childAssessment_regions_basalGanglia_caudate!=null){
	              for(ResourceFile rf: ((CndaModifiedscheltensregion)childAssessment_regions_basalGanglia_caudate).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("assessment/regions/basal_ganglia/caudate[" + ((CndaModifiedscheltensregion)childAssessment_regions_basalGanglia_caudate).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("assessment/regions/basal_ganglia/caudate/" + ((CndaModifiedscheltensregion)childAssessment_regions_basalGanglia_caudate).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //assessment/regions/basal_ganglia/putamen
	        CndaModifiedscheltensregion childAssessment_regions_basalGanglia_putamen = (CndaModifiedscheltensregion)this.getAssessment_regions_basalGanglia_putamen();
	            if (childAssessment_regions_basalGanglia_putamen!=null){
	              for(ResourceFile rf: ((CndaModifiedscheltensregion)childAssessment_regions_basalGanglia_putamen).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("assessment/regions/basal_ganglia/putamen[" + ((CndaModifiedscheltensregion)childAssessment_regions_basalGanglia_putamen).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("assessment/regions/basal_ganglia/putamen/" + ((CndaModifiedscheltensregion)childAssessment_regions_basalGanglia_putamen).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //assessment/regions/basal_ganglia/globus_pallidus
	        CndaModifiedscheltensregion childAssessment_regions_basalGanglia_globusPallidus = (CndaModifiedscheltensregion)this.getAssessment_regions_basalGanglia_globusPallidus();
	            if (childAssessment_regions_basalGanglia_globusPallidus!=null){
	              for(ResourceFile rf: ((CndaModifiedscheltensregion)childAssessment_regions_basalGanglia_globusPallidus).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("assessment/regions/basal_ganglia/globus_pallidus[" + ((CndaModifiedscheltensregion)childAssessment_regions_basalGanglia_globusPallidus).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("assessment/regions/basal_ganglia/globus_pallidus/" + ((CndaModifiedscheltensregion)childAssessment_regions_basalGanglia_globusPallidus).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //assessment/regions/basal_ganglia/thalamus
	        CndaModifiedscheltensregion childAssessment_regions_basalGanglia_thalamus = (CndaModifiedscheltensregion)this.getAssessment_regions_basalGanglia_thalamus();
	            if (childAssessment_regions_basalGanglia_thalamus!=null){
	              for(ResourceFile rf: ((CndaModifiedscheltensregion)childAssessment_regions_basalGanglia_thalamus).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("assessment/regions/basal_ganglia/thalamus[" + ((CndaModifiedscheltensregion)childAssessment_regions_basalGanglia_thalamus).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("assessment/regions/basal_ganglia/thalamus/" + ((CndaModifiedscheltensregion)childAssessment_regions_basalGanglia_thalamus).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
