/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaManualvolumetryregion extends XnatVolumetricregion implements org.nrg.xdat.model.CndaManualvolumetryregionI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaManualvolumetryregion.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:manualVolumetryRegion";

	public AutoCndaManualvolumetryregion(ItemI item)
	{
		super(item);
	}

	public AutoCndaManualvolumetryregion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaManualvolumetryregion(UserI user)
	 **/
	public AutoCndaManualvolumetryregion(){}

	public AutoCndaManualvolumetryregion(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:manualVolumetryRegion";
	}
	 private org.nrg.xdat.om.XnatVolumetricregion _Volumetricregion =null;

	/**
	 * volumetricRegion
	 * @return org.nrg.xdat.om.XnatVolumetricregion
	 */
	public org.nrg.xdat.om.XnatVolumetricregion getVolumetricregion() {
		try{
			if (_Volumetricregion==null){
				_Volumetricregion=((XnatVolumetricregion)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("volumetricRegion")));
				return _Volumetricregion;
			}else {
				return _Volumetricregion;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for volumetricRegion.
	 * @param v Value to Set.
	 */
	public void setVolumetricregion(ItemI v) throws Exception{
		_Volumetricregion =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/volumetricRegion",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/volumetricRegion",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * volumetricRegion
	 * set org.nrg.xdat.model.XnatVolumetricregionI
	 */
	public <A extends org.nrg.xdat.model.XnatVolumetricregionI> void setVolumetricregion(A item) throws Exception{
	setVolumetricregion((ItemI)item);
	}

	/**
	 * Removes the volumetricRegion.
	 * */
	public void removeVolumetricregion() {
		_Volumetricregion =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/volumetricRegion",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.CndaManualvolumetryregionSlice> _Slice =null;

	/**
	 * slice
	 * @return Returns an List of org.nrg.xdat.om.CndaManualvolumetryregionSlice
	 */
	public <A extends org.nrg.xdat.model.CndaManualvolumetryregionSliceI> List<A> getSlice() {
		try{
			if (_Slice==null){
				_Slice=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("slice"));
			}
			return (List<A>) _Slice;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.CndaManualvolumetryregionSlice>();}
	}

	/**
	 * Sets the value for slice.
	 * @param v Value to Set.
	 */
	public void setSlice(ItemI v) throws Exception{
		_Slice =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/slice",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/slice",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * slice
	 * Adds org.nrg.xdat.model.CndaManualvolumetryregionSliceI
	 */
	public <A extends org.nrg.xdat.model.CndaManualvolumetryregionSliceI> void addSlice(A item) throws Exception{
	setSlice((ItemI)item);
	}

	/**
	 * Removes the slice of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeSlice(int index) throws java.lang.IndexOutOfBoundsException {
		_Slice =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/slice",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	//FIELD

	private String _ImageFile=null;

	/**
	 * @return Returns the image_file.
	 */
	public String getImageFile(){
		try{
			if (_ImageFile==null){
				_ImageFile=getStringProperty("image_file");
				return _ImageFile;
			}else {
				return _ImageFile;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for image_file.
	 * @param v Value to Set.
	 */
	public void setImageFile(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/image_file",v);
		_ImageFile=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _StatsFile=null;

	/**
	 * @return Returns the stats_file.
	 */
	public String getStatsFile(){
		try{
			if (_StatsFile==null){
				_StatsFile=getStringProperty("stats_file");
				return _StatsFile;
			}else {
				return _StatsFile;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for stats_file.
	 * @param v Value to Set.
	 */
	public void setStatsFile(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/stats_file",v);
		_StatsFile=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Regioninfoid=null;

	/**
	 * @return Returns the regionInfoId.
	 */
	public String getRegioninfoid(){
		try{
			if (_Regioninfoid==null){
				_Regioninfoid=getStringProperty("regionInfoId");
				return _Regioninfoid;
			}else {
				return _Regioninfoid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for regionInfoId.
	 * @param v Value to Set.
	 */
	public void setRegioninfoid(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/regionInfoId",v);
		_Regioninfoid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaManualvolumetryregion> getAllCndaManualvolumetryregions(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaManualvolumetryregion> al = new ArrayList<org.nrg.xdat.om.CndaManualvolumetryregion>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaManualvolumetryregion> getCndaManualvolumetryregionsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaManualvolumetryregion> al = new ArrayList<org.nrg.xdat.om.CndaManualvolumetryregion>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaManualvolumetryregion> getCndaManualvolumetryregionsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaManualvolumetryregion> al = new ArrayList<org.nrg.xdat.om.CndaManualvolumetryregion>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaManualvolumetryregion getCndaManualvolumetryregionsByXnatVolumetricregionId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:manualVolumetryRegion/xnat_volumetricregion_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaManualvolumetryregion) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //volumetricRegion
	        XnatVolumetricregion childVolumetricregion = (XnatVolumetricregion)this.getVolumetricregion();
	            if (childVolumetricregion!=null){
	              for(ResourceFile rf: ((XnatVolumetricregion)childVolumetricregion).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("volumetricRegion[" + ((XnatVolumetricregion)childVolumetricregion).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("volumetricRegion/" + ((XnatVolumetricregion)childVolumetricregion).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //slice
	        for(org.nrg.xdat.model.CndaManualvolumetryregionSliceI childSlice : this.getSlice()){
	            if (childSlice!=null){
	              for(ResourceFile rf: ((CndaManualvolumetryregionSlice)childSlice).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("slice[" + ((CndaManualvolumetryregionSlice)childSlice).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("slice/" + ((CndaManualvolumetryregionSlice)childSlice).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
