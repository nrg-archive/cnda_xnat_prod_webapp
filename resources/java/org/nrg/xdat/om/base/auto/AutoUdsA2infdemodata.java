/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoUdsA2infdemodata extends XnatSubjectassessordata implements org.nrg.xdat.model.UdsA2infdemodataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoUdsA2infdemodata.class);
	public static String SCHEMA_ELEMENT_NAME="uds:a2infdemoData";

	public AutoUdsA2infdemodata(ItemI item)
	{
		super(item);
	}

	public AutoUdsA2infdemodata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoUdsA2infdemodata(UserI user)
	 **/
	public AutoUdsA2infdemodata(){}

	public AutoUdsA2infdemodata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "uds:a2infdemoData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Examdate=null;

	/**
	 * @return Returns the EXAMDATE.
	 */
	public Object getExamdate(){
		try{
			if (_Examdate==null){
				_Examdate=getProperty("EXAMDATE");
				return _Examdate;
			}else {
				return _Examdate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for EXAMDATE.
	 * @param v Value to Set.
	 */
	public void setExamdate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/EXAMDATE",v);
		_Examdate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Inbirmo=null;

	/**
	 * @return Returns the INBIRMO.
	 */
	public Integer getInbirmo() {
		try{
			if (_Inbirmo==null){
				_Inbirmo=getIntegerProperty("INBIRMO");
				return _Inbirmo;
			}else {
				return _Inbirmo;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INBIRMO.
	 * @param v Value to Set.
	 */
	public void setInbirmo(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INBIRMO",v);
		_Inbirmo=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Inbiryr=null;

	/**
	 * @return Returns the INBIRYR.
	 */
	public Integer getInbiryr() {
		try{
			if (_Inbiryr==null){
				_Inbiryr=getIntegerProperty("INBIRYR");
				return _Inbiryr;
			}else {
				return _Inbiryr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INBIRYR.
	 * @param v Value to Set.
	 */
	public void setInbiryr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INBIRYR",v);
		_Inbiryr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Insex=null;

	/**
	 * @return Returns the INSEX.
	 */
	public Integer getInsex() {
		try{
			if (_Insex==null){
				_Insex=getIntegerProperty("INSEX");
				return _Insex;
			}else {
				return _Insex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INSEX.
	 * @param v Value to Set.
	 */
	public void setInsex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INSEX",v);
		_Insex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Newinf=null;

	/**
	 * @return Returns the NEWINF.
	 */
	public Integer getNewinf() {
		try{
			if (_Newinf==null){
				_Newinf=getIntegerProperty("NEWINF");
				return _Newinf;
			}else {
				return _Newinf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NEWINF.
	 * @param v Value to Set.
	 */
	public void setNewinf(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NEWINF",v);
		_Newinf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Inhisp=null;

	/**
	 * @return Returns the INHISP.
	 */
	public Integer getInhisp() {
		try{
			if (_Inhisp==null){
				_Inhisp=getIntegerProperty("INHISP");
				return _Inhisp;
			}else {
				return _Inhisp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INHISP.
	 * @param v Value to Set.
	 */
	public void setInhisp(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INHISP",v);
		_Inhisp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Inhispor=null;

	/**
	 * @return Returns the INHISPOR.
	 */
	public Integer getInhispor() {
		try{
			if (_Inhispor==null){
				_Inhispor=getIntegerProperty("INHISPOR");
				return _Inhispor;
			}else {
				return _Inhispor;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INHISPOR.
	 * @param v Value to Set.
	 */
	public void setInhispor(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INHISPOR",v);
		_Inhispor=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Inhispox=null;

	/**
	 * @return Returns the INHISPOX.
	 */
	public String getInhispox(){
		try{
			if (_Inhispox==null){
				_Inhispox=getStringProperty("INHISPOX");
				return _Inhispox;
			}else {
				return _Inhispox;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INHISPOX.
	 * @param v Value to Set.
	 */
	public void setInhispox(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INHISPOX",v);
		_Inhispox=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Inrace=null;

	/**
	 * @return Returns the INRACE.
	 */
	public Integer getInrace() {
		try{
			if (_Inrace==null){
				_Inrace=getIntegerProperty("INRACE");
				return _Inrace;
			}else {
				return _Inrace;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INRACE.
	 * @param v Value to Set.
	 */
	public void setInrace(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INRACE",v);
		_Inrace=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Inracex=null;

	/**
	 * @return Returns the INRACEX.
	 */
	public String getInracex(){
		try{
			if (_Inracex==null){
				_Inracex=getStringProperty("INRACEX");
				return _Inracex;
			}else {
				return _Inracex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INRACEX.
	 * @param v Value to Set.
	 */
	public void setInracex(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INRACEX",v);
		_Inracex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Inrasec=null;

	/**
	 * @return Returns the INRASEC.
	 */
	public Integer getInrasec() {
		try{
			if (_Inrasec==null){
				_Inrasec=getIntegerProperty("INRASEC");
				return _Inrasec;
			}else {
				return _Inrasec;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INRASEC.
	 * @param v Value to Set.
	 */
	public void setInrasec(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INRASEC",v);
		_Inrasec=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Inrasecx=null;

	/**
	 * @return Returns the INRASECX.
	 */
	public String getInrasecx(){
		try{
			if (_Inrasecx==null){
				_Inrasecx=getStringProperty("INRASECX");
				return _Inrasecx;
			}else {
				return _Inrasecx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INRASECX.
	 * @param v Value to Set.
	 */
	public void setInrasecx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INRASECX",v);
		_Inrasecx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Inrater=null;

	/**
	 * @return Returns the INRATER.
	 */
	public Integer getInrater() {
		try{
			if (_Inrater==null){
				_Inrater=getIntegerProperty("INRATER");
				return _Inrater;
			}else {
				return _Inrater;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INRATER.
	 * @param v Value to Set.
	 */
	public void setInrater(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INRATER",v);
		_Inrater=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Inraterx=null;

	/**
	 * @return Returns the INRATERX.
	 */
	public String getInraterx(){
		try{
			if (_Inraterx==null){
				_Inraterx=getStringProperty("INRATERX");
				return _Inraterx;
			}else {
				return _Inraterx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INRATERX.
	 * @param v Value to Set.
	 */
	public void setInraterx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INRATERX",v);
		_Inraterx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ineduc=null;

	/**
	 * @return Returns the INEDUC.
	 */
	public Integer getIneduc() {
		try{
			if (_Ineduc==null){
				_Ineduc=getIntegerProperty("INEDUC");
				return _Ineduc;
			}else {
				return _Ineduc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INEDUC.
	 * @param v Value to Set.
	 */
	public void setIneduc(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INEDUC",v);
		_Ineduc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Inrelto=null;

	/**
	 * @return Returns the INRELTO.
	 */
	public Integer getInrelto() {
		try{
			if (_Inrelto==null){
				_Inrelto=getIntegerProperty("INRELTO");
				return _Inrelto;
			}else {
				return _Inrelto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INRELTO.
	 * @param v Value to Set.
	 */
	public void setInrelto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INRELTO",v);
		_Inrelto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Inreltox=null;

	/**
	 * @return Returns the INRELTOX.
	 */
	public String getInreltox(){
		try{
			if (_Inreltox==null){
				_Inreltox=getStringProperty("INRELTOX");
				return _Inreltox;
			}else {
				return _Inreltox;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INRELTOX.
	 * @param v Value to Set.
	 */
	public void setInreltox(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INRELTOX",v);
		_Inreltox=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Inlivwth=null;

	/**
	 * @return Returns the INLIVWTH.
	 */
	public Integer getInlivwth() {
		try{
			if (_Inlivwth==null){
				_Inlivwth=getIntegerProperty("INLIVWTH");
				return _Inlivwth;
			}else {
				return _Inlivwth;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INLIVWTH.
	 * @param v Value to Set.
	 */
	public void setInlivwth(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INLIVWTH",v);
		_Inlivwth=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Invisits=null;

	/**
	 * @return Returns the INVISITS.
	 */
	public Integer getInvisits() {
		try{
			if (_Invisits==null){
				_Invisits=getIntegerProperty("INVISITS");
				return _Invisits;
			}else {
				return _Invisits;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INVISITS.
	 * @param v Value to Set.
	 */
	public void setInvisits(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INVISITS",v);
		_Invisits=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Incalls=null;

	/**
	 * @return Returns the INCALLS.
	 */
	public Integer getIncalls() {
		try{
			if (_Incalls==null){
				_Incalls=getIntegerProperty("INCALLS");
				return _Incalls;
			}else {
				return _Incalls;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INCALLS.
	 * @param v Value to Set.
	 */
	public void setIncalls(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INCALLS",v);
		_Incalls=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Inrely=null;

	/**
	 * @return Returns the INRELY.
	 */
	public Integer getInrely() {
		try{
			if (_Inrely==null){
				_Inrely=getIntegerProperty("INRELY");
				return _Inrely;
			}else {
				return _Inrely;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INRELY.
	 * @param v Value to Set.
	 */
	public void setInrely(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INRELY",v);
		_Inrely=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.UdsA2infdemodata> getAllUdsA2infdemodatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsA2infdemodata> al = new ArrayList<org.nrg.xdat.om.UdsA2infdemodata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsA2infdemodata> getUdsA2infdemodatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsA2infdemodata> al = new ArrayList<org.nrg.xdat.om.UdsA2infdemodata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsA2infdemodata> getUdsA2infdemodatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsA2infdemodata> al = new ArrayList<org.nrg.xdat.om.UdsA2infdemodata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static UdsA2infdemodata getUdsA2infdemodatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("uds:a2infdemoData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (UdsA2infdemodata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
