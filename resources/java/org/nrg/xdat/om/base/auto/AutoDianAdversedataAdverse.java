/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:00 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianAdversedataAdverse extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.DianAdversedataAdverseI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianAdversedataAdverse.class);
	public static String SCHEMA_ELEMENT_NAME="dian:adverseData_adverse";

	public AutoDianAdversedataAdverse(ItemI item)
	{
		super(item);
	}

	public AutoDianAdversedataAdverse(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianAdversedataAdverse(UserI user)
	 **/
	public AutoDianAdversedataAdverse(){}

	public AutoDianAdversedataAdverse(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:adverseData_adverse";
	}

	//FIELD

	private Integer _Recno=null;

	/**
	 * @return Returns the RECNO.
	 */
	public Integer getRecno() {
		try{
			if (_Recno==null){
				_Recno=getIntegerProperty("RECNO");
				return _Recno;
			}else {
				return _Recno;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RECNO.
	 * @param v Value to Set.
	 */
	public void setRecno(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RECNO",v);
		_Recno=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Aeevent=null;

	/**
	 * @return Returns the AEEVENT.
	 */
	public String getAeevent(){
		try{
			if (_Aeevent==null){
				_Aeevent=getStringProperty("AEEVENT");
				return _Aeevent;
			}else {
				return _Aeevent;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AEEVENT.
	 * @param v Value to Set.
	 */
	public void setAeevent(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AEEVENT",v);
		_Aeevent=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Aeondate=null;

	/**
	 * @return Returns the AEONDATE.
	 */
	public Object getAeondate(){
		try{
			if (_Aeondate==null){
				_Aeondate=getProperty("AEONDATE");
				return _Aeondate;
			}else {
				return _Aeondate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AEONDATE.
	 * @param v Value to Set.
	 */
	public void setAeondate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AEONDATE",v);
		_Aeondate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Aeonyear=null;

	/**
	 * @return Returns the AEONYEAR.
	 */
	public Integer getAeonyear() {
		try{
			if (_Aeonyear==null){
				_Aeonyear=getIntegerProperty("AEONYEAR");
				return _Aeonyear;
			}else {
				return _Aeonyear;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AEONYEAR.
	 * @param v Value to Set.
	 */
	public void setAeonyear(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AEONYEAR",v);
		_Aeonyear=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Aeonmonth=null;

	/**
	 * @return Returns the AEONMONTH.
	 */
	public Integer getAeonmonth() {
		try{
			if (_Aeonmonth==null){
				_Aeonmonth=getIntegerProperty("AEONMONTH");
				return _Aeonmonth;
			}else {
				return _Aeonmonth;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AEONMONTH.
	 * @param v Value to Set.
	 */
	public void setAeonmonth(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AEONMONTH",v);
		_Aeonmonth=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Aeonday=null;

	/**
	 * @return Returns the AEONDAY.
	 */
	public Integer getAeonday() {
		try{
			if (_Aeonday==null){
				_Aeonday=getIntegerProperty("AEONDAY");
				return _Aeonday;
			}else {
				return _Aeonday;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AEONDAY.
	 * @param v Value to Set.
	 */
	public void setAeonday(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AEONDAY",v);
		_Aeonday=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Aeongo=null;

	/**
	 * @return Returns the AEONGO.
	 */
	public Integer getAeongo() {
		try{
			if (_Aeongo==null){
				_Aeongo=getIntegerProperty("AEONGO");
				return _Aeongo;
			}else {
				return _Aeongo;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AEONGO.
	 * @param v Value to Set.
	 */
	public void setAeongo(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AEONGO",v);
		_Aeongo=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Aeoffdate=null;

	/**
	 * @return Returns the AEOFFDATE.
	 */
	public Object getAeoffdate(){
		try{
			if (_Aeoffdate==null){
				_Aeoffdate=getProperty("AEOFFDATE");
				return _Aeoffdate;
			}else {
				return _Aeoffdate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AEOFFDATE.
	 * @param v Value to Set.
	 */
	public void setAeoffdate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AEOFFDATE",v);
		_Aeoffdate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Aeoffyear=null;

	/**
	 * @return Returns the AEOFFYEAR.
	 */
	public Integer getAeoffyear() {
		try{
			if (_Aeoffyear==null){
				_Aeoffyear=getIntegerProperty("AEOFFYEAR");
				return _Aeoffyear;
			}else {
				return _Aeoffyear;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AEOFFYEAR.
	 * @param v Value to Set.
	 */
	public void setAeoffyear(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AEOFFYEAR",v);
		_Aeoffyear=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Aeoffmonth=null;

	/**
	 * @return Returns the AEOFFMONTH.
	 */
	public Integer getAeoffmonth() {
		try{
			if (_Aeoffmonth==null){
				_Aeoffmonth=getIntegerProperty("AEOFFMONTH");
				return _Aeoffmonth;
			}else {
				return _Aeoffmonth;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AEOFFMONTH.
	 * @param v Value to Set.
	 */
	public void setAeoffmonth(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AEOFFMONTH",v);
		_Aeoffmonth=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Aeoffday=null;

	/**
	 * @return Returns the AEOFFDAY.
	 */
	public Integer getAeoffday() {
		try{
			if (_Aeoffday==null){
				_Aeoffday=getIntegerProperty("AEOFFDAY");
				return _Aeoffday;
			}else {
				return _Aeoffday;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AEOFFDAY.
	 * @param v Value to Set.
	 */
	public void setAeoffday(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AEOFFDAY",v);
		_Aeoffday=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Aesevr=null;

	/**
	 * @return Returns the AESEVR.
	 */
	public Integer getAesevr() {
		try{
			if (_Aesevr==null){
				_Aesevr=getIntegerProperty("AESEVR");
				return _Aesevr;
			}else {
				return _Aesevr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AESEVR.
	 * @param v Value to Set.
	 */
	public void setAesevr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AESEVR",v);
		_Aesevr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Aelp=null;

	/**
	 * @return Returns the AELP.
	 */
	public Integer getAelp() {
		try{
			if (_Aelp==null){
				_Aelp=getIntegerProperty("AELP");
				return _Aelp;
			}else {
				return _Aelp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AELP.
	 * @param v Value to Set.
	 */
	public void setAelp(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AELP",v);
		_Aelp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Aerelat=null;

	/**
	 * @return Returns the AERELAT.
	 */
	public Integer getAerelat() {
		try{
			if (_Aerelat==null){
				_Aerelat=getIntegerProperty("AERELAT");
				return _Aerelat;
			}else {
				return _Aerelat;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AERELAT.
	 * @param v Value to Set.
	 */
	public void setAerelat(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AERELAT",v);
		_Aerelat=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Aerelato=null;

	/**
	 * @return Returns the AERELATO.
	 */
	public String getAerelato(){
		try{
			if (_Aerelato==null){
				_Aerelato=getStringProperty("AERELATO");
				return _Aerelato;
			}else {
				return _Aerelato;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AERELATO.
	 * @param v Value to Set.
	 */
	public void setAerelato(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AERELATO",v);
		_Aerelato=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Aecmed=null;

	/**
	 * @return Returns the AECMED.
	 */
	public Integer getAecmed() {
		try{
			if (_Aecmed==null){
				_Aecmed=getIntegerProperty("AECMED");
				return _Aecmed;
			}else {
				return _Aecmed;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AECMED.
	 * @param v Value to Set.
	 */
	public void setAecmed(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AECMED",v);
		_Aecmed=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sae=null;

	/**
	 * @return Returns the SAE.
	 */
	public Integer getSae() {
		try{
			if (_Sae==null){
				_Sae=getIntegerProperty("SAE");
				return _Sae;
			}else {
				return _Sae;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SAE.
	 * @param v Value to Set.
	 */
	public void setSae(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SAE",v);
		_Sae=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saereason=null;

	/**
	 * @return Returns the SAEREASON.
	 */
	public String getSaereason(){
		try{
			if (_Saereason==null){
				_Saereason=getStringProperty("SAEREASON");
				return _Saereason;
			}else {
				return _Saereason;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SAEREASON.
	 * @param v Value to Set.
	 */
	public void setSaereason(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SAEREASON",v);
		_Saereason=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Aeinpt=null;

	/**
	 * @return Returns the AEINPT.
	 */
	public Object getAeinpt(){
		try{
			if (_Aeinpt==null){
				_Aeinpt=getProperty("AEINPT");
				return _Aeinpt;
			}else {
				return _Aeinpt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AEINPT.
	 * @param v Value to Set.
	 */
	public void setAeinpt(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AEINPT",v);
		_Aeinpt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Aedisdate=null;

	/**
	 * @return Returns the AEDISDATE.
	 */
	public Object getAedisdate(){
		try{
			if (_Aedisdate==null){
				_Aedisdate=getProperty("AEDISDATE");
				return _Aedisdate;
			}else {
				return _Aedisdate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AEDISDATE.
	 * @param v Value to Set.
	 */
	public void setAedisdate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AEDISDATE",v);
		_Aedisdate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Aedeadt=null;

	/**
	 * @return Returns the AEDEADT.
	 */
	public Object getAedeadt(){
		try{
			if (_Aedeadt==null){
				_Aedeadt=getProperty("AEDEADT");
				return _Aedeadt;
			}else {
				return _Aedeadt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AEDEADT.
	 * @param v Value to Set.
	 */
	public void setAedeadt(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AEDEADT",v);
		_Aedeadt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Aedears=null;

	/**
	 * @return Returns the AEDEARS.
	 */
	public String getAedears(){
		try{
			if (_Aedears==null){
				_Aedears=getStringProperty("AEDEARS");
				return _Aedears;
			}else {
				return _Aedears;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AEDEARS.
	 * @param v Value to Set.
	 */
	public void setAedears(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AEDEARS",v);
		_Aedears=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Aeautop=null;

	/**
	 * @return Returns the AEAUTOP.
	 */
	public Integer getAeautop() {
		try{
			if (_Aeautop==null){
				_Aeautop=getIntegerProperty("AEAUTOP");
				return _Aeautop;
			}else {
				return _Aeautop;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AEAUTOP.
	 * @param v Value to Set.
	 */
	public void setAeautop(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AEAUTOP",v);
		_Aeautop=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Aecomm=null;

	/**
	 * @return Returns the AECOMM.
	 */
	public String getAecomm(){
		try{
			if (_Aecomm==null){
				_Aecomm=getStringProperty("AECOMM");
				return _Aecomm;
			}else {
				return _Aecomm;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AECOMM.
	 * @param v Value to Set.
	 */
	public void setAecomm(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AECOMM",v);
		_Aecomm=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _DianAdversedataAdverseId=null;

	/**
	 * @return Returns the dian_adverseData_adverse_id.
	 */
	public Integer getDianAdversedataAdverseId() {
		try{
			if (_DianAdversedataAdverseId==null){
				_DianAdversedataAdverseId=getIntegerProperty("dian_adverseData_adverse_id");
				return _DianAdversedataAdverseId;
			}else {
				return _DianAdversedataAdverseId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for dian_adverseData_adverse_id.
	 * @param v Value to Set.
	 */
	public void setDianAdversedataAdverseId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/dian_adverseData_adverse_id",v);
		_DianAdversedataAdverseId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianAdversedataAdverse> getAllDianAdversedataAdverses(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianAdversedataAdverse> al = new ArrayList<org.nrg.xdat.om.DianAdversedataAdverse>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianAdversedataAdverse> getDianAdversedataAdversesByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianAdversedataAdverse> al = new ArrayList<org.nrg.xdat.om.DianAdversedataAdverse>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianAdversedataAdverse> getDianAdversedataAdversesByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianAdversedataAdverse> al = new ArrayList<org.nrg.xdat.om.DianAdversedataAdverse>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianAdversedataAdverse getDianAdversedataAdversesByDianAdversedataAdverseId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:adverseData_adverse/dian_adverseData_adverse_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianAdversedataAdverse) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
