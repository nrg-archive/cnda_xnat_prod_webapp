/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:00 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianLpfollowdata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianLpfollowdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianLpfollowdata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:lpfollowData";

	public AutoDianLpfollowdata(ItemI item)
	{
		super(item);
	}

	public AutoDianLpfollowdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianLpfollowdata(UserI user)
	 **/
	public AutoDianLpfollowdata(){}

	public AutoDianLpfollowdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:lpfollowData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Lpfudate=null;

	/**
	 * @return Returns the LPFUDATE.
	 */
	public Object getLpfudate(){
		try{
			if (_Lpfudate==null){
				_Lpfudate=getProperty("LPFUDATE");
				return _Lpfudate;
			}else {
				return _Lpfudate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LPFUDATE.
	 * @param v Value to Set.
	 */
	public void setLpfudate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LPFUDATE",v);
		_Lpfudate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Lpfuact=null;

	/**
	 * @return Returns the LPFUACT.
	 */
	public Integer getLpfuact() {
		try{
			if (_Lpfuact==null){
				_Lpfuact=getIntegerProperty("LPFUACT");
				return _Lpfuact;
			}else {
				return _Lpfuact;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LPFUACT.
	 * @param v Value to Set.
	 */
	public void setLpfuact(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LPFUACT",v);
		_Lpfuact=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Lpfucomp=null;

	/**
	 * @return Returns the LPFUCOMP.
	 */
	public String getLpfucomp(){
		try{
			if (_Lpfucomp==null){
				_Lpfucomp=getStringProperty("LPFUCOMP");
				return _Lpfucomp;
			}else {
				return _Lpfucomp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LPFUCOMP.
	 * @param v Value to Set.
	 */
	public void setLpfucomp(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LPFUCOMP",v);
		_Lpfucomp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Lpfcom=null;

	/**
	 * @return Returns the LPFCOM.
	 */
	public String getLpfcom(){
		try{
			if (_Lpfcom==null){
				_Lpfcom=getStringProperty("LPFCOM");
				return _Lpfcom;
			}else {
				return _Lpfcom;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LPFCOM.
	 * @param v Value to Set.
	 */
	public void setLpfcom(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LPFCOM",v);
		_Lpfcom=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianLpfollowdata> getAllDianLpfollowdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianLpfollowdata> al = new ArrayList<org.nrg.xdat.om.DianLpfollowdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianLpfollowdata> getDianLpfollowdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianLpfollowdata> al = new ArrayList<org.nrg.xdat.om.DianLpfollowdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianLpfollowdata> getDianLpfollowdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianLpfollowdata> al = new ArrayList<org.nrg.xdat.om.DianLpfollowdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianLpfollowdata getDianLpfollowdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:lpfollowData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianLpfollowdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
