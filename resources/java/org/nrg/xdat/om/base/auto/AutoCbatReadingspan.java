/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCbatReadingspan extends XnatSubjectassessordata implements org.nrg.xdat.model.CbatReadingspanI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCbatReadingspan.class);
	public static String SCHEMA_ELEMENT_NAME="cbat:readingSpan";

	public AutoCbatReadingspan(ItemI item)
	{
		super(item);
	}

	public AutoCbatReadingspan(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCbatReadingspan(UserI user)
	 **/
	public AutoCbatReadingspan(){}

	public AutoCbatReadingspan(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cbat:readingSpan";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T1_accuracy=null;

	/**
	 * @return Returns the T1/accuracy.
	 */
	public Integer getT1_accuracy() {
		try{
			if (_T1_accuracy==null){
				_T1_accuracy=getIntegerProperty("T1/accuracy");
				return _T1_accuracy;
			}else {
				return _T1_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T1/accuracy.
	 * @param v Value to Set.
	 */
	public void setT1_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T1/accuracy",v);
		_T1_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T1_response=null;

	/**
	 * @return Returns the T1/response.
	 */
	public String getT1_response(){
		try{
			if (_T1_response==null){
				_T1_response=getStringProperty("T1/response");
				return _T1_response;
			}else {
				return _T1_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T1/response.
	 * @param v Value to Set.
	 */
	public void setT1_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T1/response",v);
		_T1_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T2_accuracy=null;

	/**
	 * @return Returns the T2/accuracy.
	 */
	public Integer getT2_accuracy() {
		try{
			if (_T2_accuracy==null){
				_T2_accuracy=getIntegerProperty("T2/accuracy");
				return _T2_accuracy;
			}else {
				return _T2_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T2/accuracy.
	 * @param v Value to Set.
	 */
	public void setT2_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T2/accuracy",v);
		_T2_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T2_response=null;

	/**
	 * @return Returns the T2/response.
	 */
	public String getT2_response(){
		try{
			if (_T2_response==null){
				_T2_response=getStringProperty("T2/response");
				return _T2_response;
			}else {
				return _T2_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T2/response.
	 * @param v Value to Set.
	 */
	public void setT2_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T2/response",v);
		_T2_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T3_accuracy=null;

	/**
	 * @return Returns the T3/accuracy.
	 */
	public Integer getT3_accuracy() {
		try{
			if (_T3_accuracy==null){
				_T3_accuracy=getIntegerProperty("T3/accuracy");
				return _T3_accuracy;
			}else {
				return _T3_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T3/accuracy.
	 * @param v Value to Set.
	 */
	public void setT3_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T3/accuracy",v);
		_T3_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T3_response=null;

	/**
	 * @return Returns the T3/response.
	 */
	public String getT3_response(){
		try{
			if (_T3_response==null){
				_T3_response=getStringProperty("T3/response");
				return _T3_response;
			}else {
				return _T3_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T3/response.
	 * @param v Value to Set.
	 */
	public void setT3_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T3/response",v);
		_T3_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T4_accuracy=null;

	/**
	 * @return Returns the T4/accuracy.
	 */
	public Integer getT4_accuracy() {
		try{
			if (_T4_accuracy==null){
				_T4_accuracy=getIntegerProperty("T4/accuracy");
				return _T4_accuracy;
			}else {
				return _T4_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T4/accuracy.
	 * @param v Value to Set.
	 */
	public void setT4_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T4/accuracy",v);
		_T4_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T4_response=null;

	/**
	 * @return Returns the T4/response.
	 */
	public String getT4_response(){
		try{
			if (_T4_response==null){
				_T4_response=getStringProperty("T4/response");
				return _T4_response;
			}else {
				return _T4_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T4/response.
	 * @param v Value to Set.
	 */
	public void setT4_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T4/response",v);
		_T4_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T5_accuracy=null;

	/**
	 * @return Returns the T5/accuracy.
	 */
	public Integer getT5_accuracy() {
		try{
			if (_T5_accuracy==null){
				_T5_accuracy=getIntegerProperty("T5/accuracy");
				return _T5_accuracy;
			}else {
				return _T5_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T5/accuracy.
	 * @param v Value to Set.
	 */
	public void setT5_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T5/accuracy",v);
		_T5_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T5_response=null;

	/**
	 * @return Returns the T5/response.
	 */
	public String getT5_response(){
		try{
			if (_T5_response==null){
				_T5_response=getStringProperty("T5/response");
				return _T5_response;
			}else {
				return _T5_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T5/response.
	 * @param v Value to Set.
	 */
	public void setT5_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T5/response",v);
		_T5_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T6_accuracy=null;

	/**
	 * @return Returns the T6/accuracy.
	 */
	public Integer getT6_accuracy() {
		try{
			if (_T6_accuracy==null){
				_T6_accuracy=getIntegerProperty("T6/accuracy");
				return _T6_accuracy;
			}else {
				return _T6_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T6/accuracy.
	 * @param v Value to Set.
	 */
	public void setT6_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T6/accuracy",v);
		_T6_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T6_response=null;

	/**
	 * @return Returns the T6/response.
	 */
	public String getT6_response(){
		try{
			if (_T6_response==null){
				_T6_response=getStringProperty("T6/response");
				return _T6_response;
			}else {
				return _T6_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T6/response.
	 * @param v Value to Set.
	 */
	public void setT6_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T6/response",v);
		_T6_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T7_accuracy=null;

	/**
	 * @return Returns the T7/accuracy.
	 */
	public Integer getT7_accuracy() {
		try{
			if (_T7_accuracy==null){
				_T7_accuracy=getIntegerProperty("T7/accuracy");
				return _T7_accuracy;
			}else {
				return _T7_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T7/accuracy.
	 * @param v Value to Set.
	 */
	public void setT7_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T7/accuracy",v);
		_T7_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T7_response=null;

	/**
	 * @return Returns the T7/response.
	 */
	public String getT7_response(){
		try{
			if (_T7_response==null){
				_T7_response=getStringProperty("T7/response");
				return _T7_response;
			}else {
				return _T7_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T7/response.
	 * @param v Value to Set.
	 */
	public void setT7_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T7/response",v);
		_T7_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T8_accuracy=null;

	/**
	 * @return Returns the T8/accuracy.
	 */
	public Integer getT8_accuracy() {
		try{
			if (_T8_accuracy==null){
				_T8_accuracy=getIntegerProperty("T8/accuracy");
				return _T8_accuracy;
			}else {
				return _T8_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T8/accuracy.
	 * @param v Value to Set.
	 */
	public void setT8_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T8/accuracy",v);
		_T8_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T8_response=null;

	/**
	 * @return Returns the T8/response.
	 */
	public String getT8_response(){
		try{
			if (_T8_response==null){
				_T8_response=getStringProperty("T8/response");
				return _T8_response;
			}else {
				return _T8_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T8/response.
	 * @param v Value to Set.
	 */
	public void setT8_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T8/response",v);
		_T8_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T9_accuracy=null;

	/**
	 * @return Returns the T9/accuracy.
	 */
	public Integer getT9_accuracy() {
		try{
			if (_T9_accuracy==null){
				_T9_accuracy=getIntegerProperty("T9/accuracy");
				return _T9_accuracy;
			}else {
				return _T9_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T9/accuracy.
	 * @param v Value to Set.
	 */
	public void setT9_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T9/accuracy",v);
		_T9_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T9_response=null;

	/**
	 * @return Returns the T9/response.
	 */
	public String getT9_response(){
		try{
			if (_T9_response==null){
				_T9_response=getStringProperty("T9/response");
				return _T9_response;
			}else {
				return _T9_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T9/response.
	 * @param v Value to Set.
	 */
	public void setT9_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T9/response",v);
		_T9_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T10_accuracy=null;

	/**
	 * @return Returns the T10/accuracy.
	 */
	public Integer getT10_accuracy() {
		try{
			if (_T10_accuracy==null){
				_T10_accuracy=getIntegerProperty("T10/accuracy");
				return _T10_accuracy;
			}else {
				return _T10_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T10/accuracy.
	 * @param v Value to Set.
	 */
	public void setT10_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T10/accuracy",v);
		_T10_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T10_response=null;

	/**
	 * @return Returns the T10/response.
	 */
	public String getT10_response(){
		try{
			if (_T10_response==null){
				_T10_response=getStringProperty("T10/response");
				return _T10_response;
			}else {
				return _T10_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T10/response.
	 * @param v Value to Set.
	 */
	public void setT10_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T10/response",v);
		_T10_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T11_accuracy=null;

	/**
	 * @return Returns the T11/accuracy.
	 */
	public Integer getT11_accuracy() {
		try{
			if (_T11_accuracy==null){
				_T11_accuracy=getIntegerProperty("T11/accuracy");
				return _T11_accuracy;
			}else {
				return _T11_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T11/accuracy.
	 * @param v Value to Set.
	 */
	public void setT11_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T11/accuracy",v);
		_T11_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T11_response=null;

	/**
	 * @return Returns the T11/response.
	 */
	public String getT11_response(){
		try{
			if (_T11_response==null){
				_T11_response=getStringProperty("T11/response");
				return _T11_response;
			}else {
				return _T11_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T11/response.
	 * @param v Value to Set.
	 */
	public void setT11_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T11/response",v);
		_T11_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T12_accuracy=null;

	/**
	 * @return Returns the T12/accuracy.
	 */
	public Integer getT12_accuracy() {
		try{
			if (_T12_accuracy==null){
				_T12_accuracy=getIntegerProperty("T12/accuracy");
				return _T12_accuracy;
			}else {
				return _T12_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T12/accuracy.
	 * @param v Value to Set.
	 */
	public void setT12_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T12/accuracy",v);
		_T12_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T12_response=null;

	/**
	 * @return Returns the T12/response.
	 */
	public String getT12_response(){
		try{
			if (_T12_response==null){
				_T12_response=getStringProperty("T12/response");
				return _T12_response;
			}else {
				return _T12_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T12/response.
	 * @param v Value to Set.
	 */
	public void setT12_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T12/response",v);
		_T12_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T13_accuracy=null;

	/**
	 * @return Returns the T13/accuracy.
	 */
	public Integer getT13_accuracy() {
		try{
			if (_T13_accuracy==null){
				_T13_accuracy=getIntegerProperty("T13/accuracy");
				return _T13_accuracy;
			}else {
				return _T13_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T13/accuracy.
	 * @param v Value to Set.
	 */
	public void setT13_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T13/accuracy",v);
		_T13_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T13_response=null;

	/**
	 * @return Returns the T13/response.
	 */
	public String getT13_response(){
		try{
			if (_T13_response==null){
				_T13_response=getStringProperty("T13/response");
				return _T13_response;
			}else {
				return _T13_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T13/response.
	 * @param v Value to Set.
	 */
	public void setT13_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T13/response",v);
		_T13_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T14_accuracy=null;

	/**
	 * @return Returns the T14/accuracy.
	 */
	public Integer getT14_accuracy() {
		try{
			if (_T14_accuracy==null){
				_T14_accuracy=getIntegerProperty("T14/accuracy");
				return _T14_accuracy;
			}else {
				return _T14_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T14/accuracy.
	 * @param v Value to Set.
	 */
	public void setT14_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T14/accuracy",v);
		_T14_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T14_response=null;

	/**
	 * @return Returns the T14/response.
	 */
	public String getT14_response(){
		try{
			if (_T14_response==null){
				_T14_response=getStringProperty("T14/response");
				return _T14_response;
			}else {
				return _T14_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T14/response.
	 * @param v Value to Set.
	 */
	public void setT14_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T14/response",v);
		_T14_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T15_accuracy=null;

	/**
	 * @return Returns the T15/accuracy.
	 */
	public Integer getT15_accuracy() {
		try{
			if (_T15_accuracy==null){
				_T15_accuracy=getIntegerProperty("T15/accuracy");
				return _T15_accuracy;
			}else {
				return _T15_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T15/accuracy.
	 * @param v Value to Set.
	 */
	public void setT15_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T15/accuracy",v);
		_T15_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T15_response=null;

	/**
	 * @return Returns the T15/response.
	 */
	public String getT15_response(){
		try{
			if (_T15_response==null){
				_T15_response=getStringProperty("T15/response");
				return _T15_response;
			}else {
				return _T15_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T15/response.
	 * @param v Value to Set.
	 */
	public void setT15_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T15/response",v);
		_T15_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T16_accuracy=null;

	/**
	 * @return Returns the T16/accuracy.
	 */
	public Integer getT16_accuracy() {
		try{
			if (_T16_accuracy==null){
				_T16_accuracy=getIntegerProperty("T16/accuracy");
				return _T16_accuracy;
			}else {
				return _T16_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T16/accuracy.
	 * @param v Value to Set.
	 */
	public void setT16_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T16/accuracy",v);
		_T16_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T16_response=null;

	/**
	 * @return Returns the T16/response.
	 */
	public String getT16_response(){
		try{
			if (_T16_response==null){
				_T16_response=getStringProperty("T16/response");
				return _T16_response;
			}else {
				return _T16_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T16/response.
	 * @param v Value to Set.
	 */
	public void setT16_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T16/response",v);
		_T16_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T17_accuracy=null;

	/**
	 * @return Returns the T17/accuracy.
	 */
	public Integer getT17_accuracy() {
		try{
			if (_T17_accuracy==null){
				_T17_accuracy=getIntegerProperty("T17/accuracy");
				return _T17_accuracy;
			}else {
				return _T17_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T17/accuracy.
	 * @param v Value to Set.
	 */
	public void setT17_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T17/accuracy",v);
		_T17_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T17_response=null;

	/**
	 * @return Returns the T17/response.
	 */
	public String getT17_response(){
		try{
			if (_T17_response==null){
				_T17_response=getStringProperty("T17/response");
				return _T17_response;
			}else {
				return _T17_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T17/response.
	 * @param v Value to Set.
	 */
	public void setT17_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T17/response",v);
		_T17_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T18_accuracy=null;

	/**
	 * @return Returns the T18/accuracy.
	 */
	public Integer getT18_accuracy() {
		try{
			if (_T18_accuracy==null){
				_T18_accuracy=getIntegerProperty("T18/accuracy");
				return _T18_accuracy;
			}else {
				return _T18_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T18/accuracy.
	 * @param v Value to Set.
	 */
	public void setT18_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T18/accuracy",v);
		_T18_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T18_response=null;

	/**
	 * @return Returns the T18/response.
	 */
	public String getT18_response(){
		try{
			if (_T18_response==null){
				_T18_response=getStringProperty("T18/response");
				return _T18_response;
			}else {
				return _T18_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T18/response.
	 * @param v Value to Set.
	 */
	public void setT18_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T18/response",v);
		_T18_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T19_accuracy=null;

	/**
	 * @return Returns the T19/accuracy.
	 */
	public Integer getT19_accuracy() {
		try{
			if (_T19_accuracy==null){
				_T19_accuracy=getIntegerProperty("T19/accuracy");
				return _T19_accuracy;
			}else {
				return _T19_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T19/accuracy.
	 * @param v Value to Set.
	 */
	public void setT19_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T19/accuracy",v);
		_T19_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T19_response=null;

	/**
	 * @return Returns the T19/response.
	 */
	public String getT19_response(){
		try{
			if (_T19_response==null){
				_T19_response=getStringProperty("T19/response");
				return _T19_response;
			}else {
				return _T19_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T19/response.
	 * @param v Value to Set.
	 */
	public void setT19_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T19/response",v);
		_T19_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T20_accuracy=null;

	/**
	 * @return Returns the T20/accuracy.
	 */
	public Integer getT20_accuracy() {
		try{
			if (_T20_accuracy==null){
				_T20_accuracy=getIntegerProperty("T20/accuracy");
				return _T20_accuracy;
			}else {
				return _T20_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T20/accuracy.
	 * @param v Value to Set.
	 */
	public void setT20_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T20/accuracy",v);
		_T20_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T20_response=null;

	/**
	 * @return Returns the T20/response.
	 */
	public String getT20_response(){
		try{
			if (_T20_response==null){
				_T20_response=getStringProperty("T20/response");
				return _T20_response;
			}else {
				return _T20_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T20/response.
	 * @param v Value to Set.
	 */
	public void setT20_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T20/response",v);
		_T20_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T21_accuracy=null;

	/**
	 * @return Returns the T21/accuracy.
	 */
	public Integer getT21_accuracy() {
		try{
			if (_T21_accuracy==null){
				_T21_accuracy=getIntegerProperty("T21/accuracy");
				return _T21_accuracy;
			}else {
				return _T21_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T21/accuracy.
	 * @param v Value to Set.
	 */
	public void setT21_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T21/accuracy",v);
		_T21_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T21_response=null;

	/**
	 * @return Returns the T21/response.
	 */
	public String getT21_response(){
		try{
			if (_T21_response==null){
				_T21_response=getStringProperty("T21/response");
				return _T21_response;
			}else {
				return _T21_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T21/response.
	 * @param v Value to Set.
	 */
	public void setT21_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T21/response",v);
		_T21_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CbatReadingspan> getAllCbatReadingspans(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CbatReadingspan> al = new ArrayList<org.nrg.xdat.om.CbatReadingspan>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CbatReadingspan> getCbatReadingspansByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CbatReadingspan> al = new ArrayList<org.nrg.xdat.om.CbatReadingspan>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CbatReadingspan> getCbatReadingspansByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CbatReadingspan> al = new ArrayList<org.nrg.xdat.om.CbatReadingspan>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CbatReadingspan getCbatReadingspansById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cbat:readingSpan/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CbatReadingspan) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
