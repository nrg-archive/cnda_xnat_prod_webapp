/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoAdirAdir2007data extends XnatSubjectassessordata implements org.nrg.xdat.model.AdirAdir2007dataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoAdirAdir2007data.class);
	public static String SCHEMA_ELEMENT_NAME="adir:adir2007Data";

	public AutoAdirAdir2007data(ItemI item)
	{
		super(item);
	}

	public AutoAdirAdir2007data(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoAdirAdir2007data(UserI user)
	 **/
	public AutoAdirAdir2007data(){}

	public AutoAdirAdir2007data(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "adir:adir2007Data";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Adirver=null;

	/**
	 * @return Returns the adirVer.
	 */
	public String getAdirver(){
		try{
			if (_Adirver==null){
				_Adirver=getStringProperty("adirVer");
				return _Adirver;
			}else {
				return _Adirver;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for adirVer.
	 * @param v Value to Set.
	 */
	public void setAdirver(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/adirVer",v);
		_Adirver=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Atotal=null;

	/**
	 * @return Returns the a_total.
	 */
	public Integer getAtotal() {
		try{
			if (_Atotal==null){
				_Atotal=getIntegerProperty("a_total");
				return _Atotal;
			}else {
				return _Atotal;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for a_total.
	 * @param v Value to Set.
	 */
	public void setAtotal(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/a_total",v);
		_Atotal=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _AtotalNote=null;

	/**
	 * @return Returns the a_total_note.
	 */
	public String getAtotalNote(){
		try{
			if (_AtotalNote==null){
				_AtotalNote=getStringProperty("a_total_note");
				return _AtotalNote;
			}else {
				return _AtotalNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for a_total_note.
	 * @param v Value to Set.
	 */
	public void setAtotalNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/a_total_note",v);
		_AtotalNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _BvTotal=null;

	/**
	 * @return Returns the b_v_total.
	 */
	public Integer getBvTotal() {
		try{
			if (_BvTotal==null){
				_BvTotal=getIntegerProperty("b_v_total");
				return _BvTotal;
			}else {
				return _BvTotal;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for b_v_total.
	 * @param v Value to Set.
	 */
	public void setBvTotal(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/b_v_total",v);
		_BvTotal=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _BvTotalNote=null;

	/**
	 * @return Returns the b_v_total_note.
	 */
	public String getBvTotalNote(){
		try{
			if (_BvTotalNote==null){
				_BvTotalNote=getStringProperty("b_v_total_note");
				return _BvTotalNote;
			}else {
				return _BvTotalNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for b_v_total_note.
	 * @param v Value to Set.
	 */
	public void setBvTotalNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/b_v_total_note",v);
		_BvTotalNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _BnvTotal=null;

	/**
	 * @return Returns the b_nv_total.
	 */
	public Integer getBnvTotal() {
		try{
			if (_BnvTotal==null){
				_BnvTotal=getIntegerProperty("b_nv_total");
				return _BnvTotal;
			}else {
				return _BnvTotal;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for b_nv_total.
	 * @param v Value to Set.
	 */
	public void setBnvTotal(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/b_nv_total",v);
		_BnvTotal=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _BnvTotalNote=null;

	/**
	 * @return Returns the b_nv_total_note.
	 */
	public String getBnvTotalNote(){
		try{
			if (_BnvTotalNote==null){
				_BnvTotalNote=getStringProperty("b_nv_total_note");
				return _BnvTotalNote;
			}else {
				return _BnvTotalNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for b_nv_total_note.
	 * @param v Value to Set.
	 */
	public void setBnvTotalNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/b_nv_total_note",v);
		_BnvTotalNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ctotal=null;

	/**
	 * @return Returns the c_total.
	 */
	public Integer getCtotal() {
		try{
			if (_Ctotal==null){
				_Ctotal=getIntegerProperty("c_total");
				return _Ctotal;
			}else {
				return _Ctotal;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for c_total.
	 * @param v Value to Set.
	 */
	public void setCtotal(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/c_total",v);
		_Ctotal=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _CtotalNote=null;

	/**
	 * @return Returns the c_total_note.
	 */
	public String getCtotalNote(){
		try{
			if (_CtotalNote==null){
				_CtotalNote=getStringProperty("c_total_note");
				return _CtotalNote;
			}else {
				return _CtotalNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for c_total_note.
	 * @param v Value to Set.
	 */
	public void setCtotalNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/c_total_note",v);
		_CtotalNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Dtotal=null;

	/**
	 * @return Returns the d_total.
	 */
	public Integer getDtotal() {
		try{
			if (_Dtotal==null){
				_Dtotal=getIntegerProperty("d_total");
				return _Dtotal;
			}else {
				return _Dtotal;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for d_total.
	 * @param v Value to Set.
	 */
	public void setDtotal(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/d_total",v);
		_Dtotal=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _DtotalNote=null;

	/**
	 * @return Returns the d_total_note.
	 */
	public String getDtotalNote(){
		try{
			if (_DtotalNote==null){
				_DtotalNote=getStringProperty("d_total_note");
				return _DtotalNote;
			}else {
				return _DtotalNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for d_total_note.
	 * @param v Value to Set.
	 */
	public void setDtotalNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/d_total_note",v);
		_DtotalNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _AdiRdx=null;

	/**
	 * @return Returns the adi_r_dx.
	 */
	public String getAdiRdx(){
		try{
			if (_AdiRdx==null){
				_AdiRdx=getStringProperty("adi_r_dx");
				return _AdiRdx;
			}else {
				return _AdiRdx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for adi_r_dx.
	 * @param v Value to Set.
	 */
	public void setAdiRdx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/adi_r_dx",v);
		_AdiRdx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _AdiRcl=null;

	/**
	 * @return Returns the adi_r_cl.
	 */
	public String getAdiRcl(){
		try{
			if (_AdiRcl==null){
				_AdiRcl=getStringProperty("adi_r_cl");
				return _AdiRcl;
			}else {
				return _AdiRcl;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for adi_r_cl.
	 * @param v Value to Set.
	 */
	public void setAdiRcl(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/adi_r_cl",v);
		_AdiRcl=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_resprltnsubj=null;

	/**
	 * @return Returns the ScoreForm/respRltnSubj.
	 */
	public String getScoreform_resprltnsubj(){
		try{
			if (_Scoreform_resprltnsubj==null){
				_Scoreform_resprltnsubj=getStringProperty("ScoreForm/respRltnSubj");
				return _Scoreform_resprltnsubj;
			}else {
				return _Scoreform_resprltnsubj;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/respRltnSubj.
	 * @param v Value to Set.
	 */
	public void setScoreform_resprltnsubj(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/respRltnSubj",v);
		_Scoreform_resprltnsubj=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_algused=null;

	/**
	 * @return Returns the ScoreForm/algUsed.
	 */
	public String getScoreform_algused(){
		try{
			if (_Scoreform_algused==null){
				_Scoreform_algused=getStringProperty("ScoreForm/algUsed");
				return _Scoreform_algused;
			}else {
				return _Scoreform_algused;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/algUsed.
	 * @param v Value to Set.
	 */
	public void setScoreform_algused(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/algUsed",v);
		_Scoreform_algused=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona1_dirgaze_code=null;

	/**
	 * @return Returns the ScoreForm/sectionA1/dirGaze/code.
	 */
	public Integer getScoreform_sectiona1_dirgaze_code() {
		try{
			if (_Scoreform_sectiona1_dirgaze_code==null){
				_Scoreform_sectiona1_dirgaze_code=getIntegerProperty("ScoreForm/sectionA1/dirGaze/code");
				return _Scoreform_sectiona1_dirgaze_code;
			}else {
				return _Scoreform_sectiona1_dirgaze_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA1/dirGaze/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona1_dirgaze_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA1/dirGaze/code",v);
		_Scoreform_sectiona1_dirgaze_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona1_dirgaze_score=null;

	/**
	 * @return Returns the ScoreForm/sectionA1/dirGaze/score.
	 */
	public Integer getScoreform_sectiona1_dirgaze_score() {
		try{
			if (_Scoreform_sectiona1_dirgaze_score==null){
				_Scoreform_sectiona1_dirgaze_score=getIntegerProperty("ScoreForm/sectionA1/dirGaze/score");
				return _Scoreform_sectiona1_dirgaze_score;
			}else {
				return _Scoreform_sectiona1_dirgaze_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA1/dirGaze/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona1_dirgaze_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA1/dirGaze/score",v);
		_Scoreform_sectiona1_dirgaze_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona1_socsmile_code=null;

	/**
	 * @return Returns the ScoreForm/sectionA1/socSmile/code.
	 */
	public Integer getScoreform_sectiona1_socsmile_code() {
		try{
			if (_Scoreform_sectiona1_socsmile_code==null){
				_Scoreform_sectiona1_socsmile_code=getIntegerProperty("ScoreForm/sectionA1/socSmile/code");
				return _Scoreform_sectiona1_socsmile_code;
			}else {
				return _Scoreform_sectiona1_socsmile_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA1/socSmile/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona1_socsmile_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA1/socSmile/code",v);
		_Scoreform_sectiona1_socsmile_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona1_socsmile_score=null;

	/**
	 * @return Returns the ScoreForm/sectionA1/socSmile/score.
	 */
	public Integer getScoreform_sectiona1_socsmile_score() {
		try{
			if (_Scoreform_sectiona1_socsmile_score==null){
				_Scoreform_sectiona1_socsmile_score=getIntegerProperty("ScoreForm/sectionA1/socSmile/score");
				return _Scoreform_sectiona1_socsmile_score;
			}else {
				return _Scoreform_sectiona1_socsmile_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA1/socSmile/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona1_socsmile_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA1/socSmile/score",v);
		_Scoreform_sectiona1_socsmile_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona1_facexpr_code=null;

	/**
	 * @return Returns the ScoreForm/sectionA1/facExpr/code.
	 */
	public Integer getScoreform_sectiona1_facexpr_code() {
		try{
			if (_Scoreform_sectiona1_facexpr_code==null){
				_Scoreform_sectiona1_facexpr_code=getIntegerProperty("ScoreForm/sectionA1/facExpr/code");
				return _Scoreform_sectiona1_facexpr_code;
			}else {
				return _Scoreform_sectiona1_facexpr_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA1/facExpr/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona1_facexpr_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA1/facExpr/code",v);
		_Scoreform_sectiona1_facexpr_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona1_facexpr_score=null;

	/**
	 * @return Returns the ScoreForm/sectionA1/facExpr/score.
	 */
	public Integer getScoreform_sectiona1_facexpr_score() {
		try{
			if (_Scoreform_sectiona1_facexpr_score==null){
				_Scoreform_sectiona1_facexpr_score=getIntegerProperty("ScoreForm/sectionA1/facExpr/score");
				return _Scoreform_sectiona1_facexpr_score;
			}else {
				return _Scoreform_sectiona1_facexpr_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA1/facExpr/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona1_facexpr_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA1/facExpr/score",v);
		_Scoreform_sectiona1_facexpr_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona2_imagplay_code=null;

	/**
	 * @return Returns the ScoreForm/sectionA2/imagPlay/code.
	 */
	public Integer getScoreform_sectiona2_imagplay_code() {
		try{
			if (_Scoreform_sectiona2_imagplay_code==null){
				_Scoreform_sectiona2_imagplay_code=getIntegerProperty("ScoreForm/sectionA2/imagPlay/code");
				return _Scoreform_sectiona2_imagplay_code;
			}else {
				return _Scoreform_sectiona2_imagplay_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA2/imagPlay/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona2_imagplay_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA2/imagPlay/code",v);
		_Scoreform_sectiona2_imagplay_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona2_imagplay_score=null;

	/**
	 * @return Returns the ScoreForm/sectionA2/imagPlay/score.
	 */
	public Integer getScoreform_sectiona2_imagplay_score() {
		try{
			if (_Scoreform_sectiona2_imagplay_score==null){
				_Scoreform_sectiona2_imagplay_score=getIntegerProperty("ScoreForm/sectionA2/imagPlay/score");
				return _Scoreform_sectiona2_imagplay_score;
			}else {
				return _Scoreform_sectiona2_imagplay_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA2/imagPlay/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona2_imagplay_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA2/imagPlay/score",v);
		_Scoreform_sectiona2_imagplay_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona2_intchild_code=null;

	/**
	 * @return Returns the ScoreForm/sectionA2/intChild/code.
	 */
	public Integer getScoreform_sectiona2_intchild_code() {
		try{
			if (_Scoreform_sectiona2_intchild_code==null){
				_Scoreform_sectiona2_intchild_code=getIntegerProperty("ScoreForm/sectionA2/intChild/code");
				return _Scoreform_sectiona2_intchild_code;
			}else {
				return _Scoreform_sectiona2_intchild_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA2/intChild/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona2_intchild_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA2/intChild/code",v);
		_Scoreform_sectiona2_intchild_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona2_intchild_score=null;

	/**
	 * @return Returns the ScoreForm/sectionA2/intChild/score.
	 */
	public Integer getScoreform_sectiona2_intchild_score() {
		try{
			if (_Scoreform_sectiona2_intchild_score==null){
				_Scoreform_sectiona2_intchild_score=getIntegerProperty("ScoreForm/sectionA2/intChild/score");
				return _Scoreform_sectiona2_intchild_score;
			}else {
				return _Scoreform_sectiona2_intchild_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA2/intChild/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona2_intchild_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA2/intChild/score",v);
		_Scoreform_sectiona2_intchild_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona2_respappr_code=null;

	/**
	 * @return Returns the ScoreForm/sectionA2/respAppr/code.
	 */
	public Integer getScoreform_sectiona2_respappr_code() {
		try{
			if (_Scoreform_sectiona2_respappr_code==null){
				_Scoreform_sectiona2_respappr_code=getIntegerProperty("ScoreForm/sectionA2/respAppr/code");
				return _Scoreform_sectiona2_respappr_code;
			}else {
				return _Scoreform_sectiona2_respappr_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA2/respAppr/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona2_respappr_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA2/respAppr/code",v);
		_Scoreform_sectiona2_respappr_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona2_respappr_score=null;

	/**
	 * @return Returns the ScoreForm/sectionA2/respAppr/score.
	 */
	public Integer getScoreform_sectiona2_respappr_score() {
		try{
			if (_Scoreform_sectiona2_respappr_score==null){
				_Scoreform_sectiona2_respappr_score=getIntegerProperty("ScoreForm/sectionA2/respAppr/score");
				return _Scoreform_sectiona2_respappr_score;
			}else {
				return _Scoreform_sectiona2_respappr_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA2/respAppr/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona2_respappr_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA2/respAppr/score",v);
		_Scoreform_sectiona2_respappr_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona2_grpplay_code=null;

	/**
	 * @return Returns the ScoreForm/sectionA2/grpPlay/code.
	 */
	public Integer getScoreform_sectiona2_grpplay_code() {
		try{
			if (_Scoreform_sectiona2_grpplay_code==null){
				_Scoreform_sectiona2_grpplay_code=getIntegerProperty("ScoreForm/sectionA2/grpPlay/code");
				return _Scoreform_sectiona2_grpplay_code;
			}else {
				return _Scoreform_sectiona2_grpplay_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA2/grpPlay/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona2_grpplay_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA2/grpPlay/code",v);
		_Scoreform_sectiona2_grpplay_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona2_grpplay_score=null;

	/**
	 * @return Returns the ScoreForm/sectionA2/grpPlay/score.
	 */
	public Integer getScoreform_sectiona2_grpplay_score() {
		try{
			if (_Scoreform_sectiona2_grpplay_score==null){
				_Scoreform_sectiona2_grpplay_score=getIntegerProperty("ScoreForm/sectionA2/grpPlay/score");
				return _Scoreform_sectiona2_grpplay_score;
			}else {
				return _Scoreform_sectiona2_grpplay_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA2/grpPlay/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona2_grpplay_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA2/grpPlay/score",v);
		_Scoreform_sectiona2_grpplay_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona2_friendships_code=null;

	/**
	 * @return Returns the ScoreForm/sectionA2/friendships/code.
	 */
	public Integer getScoreform_sectiona2_friendships_code() {
		try{
			if (_Scoreform_sectiona2_friendships_code==null){
				_Scoreform_sectiona2_friendships_code=getIntegerProperty("ScoreForm/sectionA2/friendships/code");
				return _Scoreform_sectiona2_friendships_code;
			}else {
				return _Scoreform_sectiona2_friendships_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA2/friendships/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona2_friendships_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA2/friendships/code",v);
		_Scoreform_sectiona2_friendships_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona2_friendships_score=null;

	/**
	 * @return Returns the ScoreForm/sectionA2/friendships/score.
	 */
	public Integer getScoreform_sectiona2_friendships_score() {
		try{
			if (_Scoreform_sectiona2_friendships_score==null){
				_Scoreform_sectiona2_friendships_score=getIntegerProperty("ScoreForm/sectionA2/friendships/score");
				return _Scoreform_sectiona2_friendships_score;
			}else {
				return _Scoreform_sectiona2_friendships_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA2/friendships/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona2_friendships_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA2/friendships/score",v);
		_Scoreform_sectiona2_friendships_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona3_showattn_code=null;

	/**
	 * @return Returns the ScoreForm/sectionA3/showAttn/code.
	 */
	public Integer getScoreform_sectiona3_showattn_code() {
		try{
			if (_Scoreform_sectiona3_showattn_code==null){
				_Scoreform_sectiona3_showattn_code=getIntegerProperty("ScoreForm/sectionA3/showAttn/code");
				return _Scoreform_sectiona3_showattn_code;
			}else {
				return _Scoreform_sectiona3_showattn_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA3/showAttn/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona3_showattn_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA3/showAttn/code",v);
		_Scoreform_sectiona3_showattn_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona3_showattn_score=null;

	/**
	 * @return Returns the ScoreForm/sectionA3/showAttn/score.
	 */
	public Integer getScoreform_sectiona3_showattn_score() {
		try{
			if (_Scoreform_sectiona3_showattn_score==null){
				_Scoreform_sectiona3_showattn_score=getIntegerProperty("ScoreForm/sectionA3/showAttn/score");
				return _Scoreform_sectiona3_showattn_score;
			}else {
				return _Scoreform_sectiona3_showattn_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA3/showAttn/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona3_showattn_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA3/showAttn/score",v);
		_Scoreform_sectiona3_showattn_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona3_offshare_code=null;

	/**
	 * @return Returns the ScoreForm/sectionA3/offShare/code.
	 */
	public Integer getScoreform_sectiona3_offshare_code() {
		try{
			if (_Scoreform_sectiona3_offshare_code==null){
				_Scoreform_sectiona3_offshare_code=getIntegerProperty("ScoreForm/sectionA3/offShare/code");
				return _Scoreform_sectiona3_offshare_code;
			}else {
				return _Scoreform_sectiona3_offshare_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA3/offShare/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona3_offshare_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA3/offShare/code",v);
		_Scoreform_sectiona3_offshare_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona3_offshare_score=null;

	/**
	 * @return Returns the ScoreForm/sectionA3/offShare/score.
	 */
	public Integer getScoreform_sectiona3_offshare_score() {
		try{
			if (_Scoreform_sectiona3_offshare_score==null){
				_Scoreform_sectiona3_offshare_score=getIntegerProperty("ScoreForm/sectionA3/offShare/score");
				return _Scoreform_sectiona3_offshare_score;
			}else {
				return _Scoreform_sectiona3_offshare_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA3/offShare/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona3_offshare_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA3/offShare/score",v);
		_Scoreform_sectiona3_offshare_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona3_shareenjoy_code=null;

	/**
	 * @return Returns the ScoreForm/sectionA3/shareEnjoy/code.
	 */
	public Integer getScoreform_sectiona3_shareenjoy_code() {
		try{
			if (_Scoreform_sectiona3_shareenjoy_code==null){
				_Scoreform_sectiona3_shareenjoy_code=getIntegerProperty("ScoreForm/sectionA3/shareEnjoy/code");
				return _Scoreform_sectiona3_shareenjoy_code;
			}else {
				return _Scoreform_sectiona3_shareenjoy_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA3/shareEnjoy/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona3_shareenjoy_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA3/shareEnjoy/code",v);
		_Scoreform_sectiona3_shareenjoy_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona3_shareenjoy_score=null;

	/**
	 * @return Returns the ScoreForm/sectionA3/shareEnjoy/score.
	 */
	public Integer getScoreform_sectiona3_shareenjoy_score() {
		try{
			if (_Scoreform_sectiona3_shareenjoy_score==null){
				_Scoreform_sectiona3_shareenjoy_score=getIntegerProperty("ScoreForm/sectionA3/shareEnjoy/score");
				return _Scoreform_sectiona3_shareenjoy_score;
			}else {
				return _Scoreform_sectiona3_shareenjoy_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA3/shareEnjoy/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona3_shareenjoy_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA3/shareEnjoy/score",v);
		_Scoreform_sectiona3_shareenjoy_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona4_othbodycomm_code=null;

	/**
	 * @return Returns the ScoreForm/sectionA4/othBodyComm/code.
	 */
	public Integer getScoreform_sectiona4_othbodycomm_code() {
		try{
			if (_Scoreform_sectiona4_othbodycomm_code==null){
				_Scoreform_sectiona4_othbodycomm_code=getIntegerProperty("ScoreForm/sectionA4/othBodyComm/code");
				return _Scoreform_sectiona4_othbodycomm_code;
			}else {
				return _Scoreform_sectiona4_othbodycomm_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA4/othBodyComm/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona4_othbodycomm_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA4/othBodyComm/code",v);
		_Scoreform_sectiona4_othbodycomm_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona4_othbodycomm_score=null;

	/**
	 * @return Returns the ScoreForm/sectionA4/othBodyComm/score.
	 */
	public Integer getScoreform_sectiona4_othbodycomm_score() {
		try{
			if (_Scoreform_sectiona4_othbodycomm_score==null){
				_Scoreform_sectiona4_othbodycomm_score=getIntegerProperty("ScoreForm/sectionA4/othBodyComm/score");
				return _Scoreform_sectiona4_othbodycomm_score;
			}else {
				return _Scoreform_sectiona4_othbodycomm_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA4/othBodyComm/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona4_othbodycomm_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA4/othBodyComm/score",v);
		_Scoreform_sectiona4_othbodycomm_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona4_offcomf_code=null;

	/**
	 * @return Returns the ScoreForm/sectionA4/offComf/code.
	 */
	public Integer getScoreform_sectiona4_offcomf_code() {
		try{
			if (_Scoreform_sectiona4_offcomf_code==null){
				_Scoreform_sectiona4_offcomf_code=getIntegerProperty("ScoreForm/sectionA4/offComf/code");
				return _Scoreform_sectiona4_offcomf_code;
			}else {
				return _Scoreform_sectiona4_offcomf_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA4/offComf/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona4_offcomf_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA4/offComf/code",v);
		_Scoreform_sectiona4_offcomf_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona4_offcomf_score=null;

	/**
	 * @return Returns the ScoreForm/sectionA4/offComf/score.
	 */
	public Integer getScoreform_sectiona4_offcomf_score() {
		try{
			if (_Scoreform_sectiona4_offcomf_score==null){
				_Scoreform_sectiona4_offcomf_score=getIntegerProperty("ScoreForm/sectionA4/offComf/score");
				return _Scoreform_sectiona4_offcomf_score;
			}else {
				return _Scoreform_sectiona4_offcomf_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA4/offComf/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona4_offcomf_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA4/offComf/score",v);
		_Scoreform_sectiona4_offcomf_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona4_qualsocovert_code=null;

	/**
	 * @return Returns the ScoreForm/sectionA4/qualSocOvert/code.
	 */
	public Integer getScoreform_sectiona4_qualsocovert_code() {
		try{
			if (_Scoreform_sectiona4_qualsocovert_code==null){
				_Scoreform_sectiona4_qualsocovert_code=getIntegerProperty("ScoreForm/sectionA4/qualSocOvert/code");
				return _Scoreform_sectiona4_qualsocovert_code;
			}else {
				return _Scoreform_sectiona4_qualsocovert_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA4/qualSocOvert/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona4_qualsocovert_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA4/qualSocOvert/code",v);
		_Scoreform_sectiona4_qualsocovert_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona4_qualsocovert_score=null;

	/**
	 * @return Returns the ScoreForm/sectionA4/qualSocOvert/score.
	 */
	public Integer getScoreform_sectiona4_qualsocovert_score() {
		try{
			if (_Scoreform_sectiona4_qualsocovert_score==null){
				_Scoreform_sectiona4_qualsocovert_score=getIntegerProperty("ScoreForm/sectionA4/qualSocOvert/score");
				return _Scoreform_sectiona4_qualsocovert_score;
			}else {
				return _Scoreform_sectiona4_qualsocovert_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA4/qualSocOvert/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona4_qualsocovert_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA4/qualSocOvert/score",v);
		_Scoreform_sectiona4_qualsocovert_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona4_inapfacexpr_code=null;

	/**
	 * @return Returns the ScoreForm/sectionA4/inapFacExpr/code.
	 */
	public Integer getScoreform_sectiona4_inapfacexpr_code() {
		try{
			if (_Scoreform_sectiona4_inapfacexpr_code==null){
				_Scoreform_sectiona4_inapfacexpr_code=getIntegerProperty("ScoreForm/sectionA4/inapFacExpr/code");
				return _Scoreform_sectiona4_inapfacexpr_code;
			}else {
				return _Scoreform_sectiona4_inapfacexpr_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA4/inapFacExpr/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona4_inapfacexpr_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA4/inapFacExpr/code",v);
		_Scoreform_sectiona4_inapfacexpr_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona4_inapfacexpr_score=null;

	/**
	 * @return Returns the ScoreForm/sectionA4/inapFacExpr/score.
	 */
	public Integer getScoreform_sectiona4_inapfacexpr_score() {
		try{
			if (_Scoreform_sectiona4_inapfacexpr_score==null){
				_Scoreform_sectiona4_inapfacexpr_score=getIntegerProperty("ScoreForm/sectionA4/inapFacExpr/score");
				return _Scoreform_sectiona4_inapfacexpr_score;
			}else {
				return _Scoreform_sectiona4_inapfacexpr_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA4/inapFacExpr/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona4_inapfacexpr_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA4/inapFacExpr/score",v);
		_Scoreform_sectiona4_inapfacexpr_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona4_apprsocresp_code=null;

	/**
	 * @return Returns the ScoreForm/sectionA4/apprSocResp/code.
	 */
	public Integer getScoreform_sectiona4_apprsocresp_code() {
		try{
			if (_Scoreform_sectiona4_apprsocresp_code==null){
				_Scoreform_sectiona4_apprsocresp_code=getIntegerProperty("ScoreForm/sectionA4/apprSocResp/code");
				return _Scoreform_sectiona4_apprsocresp_code;
			}else {
				return _Scoreform_sectiona4_apprsocresp_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA4/apprSocResp/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona4_apprsocresp_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA4/apprSocResp/code",v);
		_Scoreform_sectiona4_apprsocresp_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona4_apprsocresp_score=null;

	/**
	 * @return Returns the ScoreForm/sectionA4/apprSocResp/score.
	 */
	public Integer getScoreform_sectiona4_apprsocresp_score() {
		try{
			if (_Scoreform_sectiona4_apprsocresp_score==null){
				_Scoreform_sectiona4_apprsocresp_score=getIntegerProperty("ScoreForm/sectionA4/apprSocResp/score");
				return _Scoreform_sectiona4_apprsocresp_score;
			}else {
				return _Scoreform_sectiona4_apprsocresp_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA4/apprSocResp/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona4_apprsocresp_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA4/apprSocResp/score",v);
		_Scoreform_sectiona4_apprsocresp_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb1_pointexprint_code=null;

	/**
	 * @return Returns the ScoreForm/sectionB1/pointExprInt/code.
	 */
	public Integer getScoreform_sectionb1_pointexprint_code() {
		try{
			if (_Scoreform_sectionb1_pointexprint_code==null){
				_Scoreform_sectionb1_pointexprint_code=getIntegerProperty("ScoreForm/sectionB1/pointExprInt/code");
				return _Scoreform_sectionb1_pointexprint_code;
			}else {
				return _Scoreform_sectionb1_pointexprint_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB1/pointExprInt/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb1_pointexprint_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB1/pointExprInt/code",v);
		_Scoreform_sectionb1_pointexprint_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb1_pointexprint_score=null;

	/**
	 * @return Returns the ScoreForm/sectionB1/pointExprInt/score.
	 */
	public Integer getScoreform_sectionb1_pointexprint_score() {
		try{
			if (_Scoreform_sectionb1_pointexprint_score==null){
				_Scoreform_sectionb1_pointexprint_score=getIntegerProperty("ScoreForm/sectionB1/pointExprInt/score");
				return _Scoreform_sectionb1_pointexprint_score;
			}else {
				return _Scoreform_sectionb1_pointexprint_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB1/pointExprInt/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb1_pointexprint_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB1/pointExprInt/score",v);
		_Scoreform_sectionb1_pointexprint_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb1_nodding_code=null;

	/**
	 * @return Returns the ScoreForm/sectionB1/nodding/code.
	 */
	public Integer getScoreform_sectionb1_nodding_code() {
		try{
			if (_Scoreform_sectionb1_nodding_code==null){
				_Scoreform_sectionb1_nodding_code=getIntegerProperty("ScoreForm/sectionB1/nodding/code");
				return _Scoreform_sectionb1_nodding_code;
			}else {
				return _Scoreform_sectionb1_nodding_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB1/nodding/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb1_nodding_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB1/nodding/code",v);
		_Scoreform_sectionb1_nodding_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb1_nodding_score=null;

	/**
	 * @return Returns the ScoreForm/sectionB1/nodding/score.
	 */
	public Integer getScoreform_sectionb1_nodding_score() {
		try{
			if (_Scoreform_sectionb1_nodding_score==null){
				_Scoreform_sectionb1_nodding_score=getIntegerProperty("ScoreForm/sectionB1/nodding/score");
				return _Scoreform_sectionb1_nodding_score;
			}else {
				return _Scoreform_sectionb1_nodding_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB1/nodding/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb1_nodding_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB1/nodding/score",v);
		_Scoreform_sectionb1_nodding_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb1_headshak_code=null;

	/**
	 * @return Returns the ScoreForm/sectionB1/headShak/code.
	 */
	public Integer getScoreform_sectionb1_headshak_code() {
		try{
			if (_Scoreform_sectionb1_headshak_code==null){
				_Scoreform_sectionb1_headshak_code=getIntegerProperty("ScoreForm/sectionB1/headShak/code");
				return _Scoreform_sectionb1_headshak_code;
			}else {
				return _Scoreform_sectionb1_headshak_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB1/headShak/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb1_headshak_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB1/headShak/code",v);
		_Scoreform_sectionb1_headshak_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb1_headshak_score=null;

	/**
	 * @return Returns the ScoreForm/sectionB1/headShak/score.
	 */
	public Integer getScoreform_sectionb1_headshak_score() {
		try{
			if (_Scoreform_sectionb1_headshak_score==null){
				_Scoreform_sectionb1_headshak_score=getIntegerProperty("ScoreForm/sectionB1/headShak/score");
				return _Scoreform_sectionb1_headshak_score;
			}else {
				return _Scoreform_sectionb1_headshak_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB1/headShak/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb1_headshak_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB1/headShak/score",v);
		_Scoreform_sectionb1_headshak_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb1_convinstrgest_code=null;

	/**
	 * @return Returns the ScoreForm/sectionB1/convInstrGest/code.
	 */
	public Integer getScoreform_sectionb1_convinstrgest_code() {
		try{
			if (_Scoreform_sectionb1_convinstrgest_code==null){
				_Scoreform_sectionb1_convinstrgest_code=getIntegerProperty("ScoreForm/sectionB1/convInstrGest/code");
				return _Scoreform_sectionb1_convinstrgest_code;
			}else {
				return _Scoreform_sectionb1_convinstrgest_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB1/convInstrGest/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb1_convinstrgest_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB1/convInstrGest/code",v);
		_Scoreform_sectionb1_convinstrgest_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb1_convinstrgest_score=null;

	/**
	 * @return Returns the ScoreForm/sectionB1/convInstrGest/score.
	 */
	public Integer getScoreform_sectionb1_convinstrgest_score() {
		try{
			if (_Scoreform_sectionb1_convinstrgest_score==null){
				_Scoreform_sectionb1_convinstrgest_score=getIntegerProperty("ScoreForm/sectionB1/convInstrGest/score");
				return _Scoreform_sectionb1_convinstrgest_score;
			}else {
				return _Scoreform_sectionb1_convinstrgest_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB1/convInstrGest/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb1_convinstrgest_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB1/convInstrGest/score",v);
		_Scoreform_sectionb1_convinstrgest_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb4_spontimit_code=null;

	/**
	 * @return Returns the ScoreForm/sectionB4/spontImit/code.
	 */
	public Integer getScoreform_sectionb4_spontimit_code() {
		try{
			if (_Scoreform_sectionb4_spontimit_code==null){
				_Scoreform_sectionb4_spontimit_code=getIntegerProperty("ScoreForm/sectionB4/spontImit/code");
				return _Scoreform_sectionb4_spontimit_code;
			}else {
				return _Scoreform_sectionb4_spontimit_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB4/spontImit/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb4_spontimit_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB4/spontImit/code",v);
		_Scoreform_sectionb4_spontimit_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb4_spontimit_score=null;

	/**
	 * @return Returns the ScoreForm/sectionB4/spontImit/score.
	 */
	public Integer getScoreform_sectionb4_spontimit_score() {
		try{
			if (_Scoreform_sectionb4_spontimit_score==null){
				_Scoreform_sectionb4_spontimit_score=getIntegerProperty("ScoreForm/sectionB4/spontImit/score");
				return _Scoreform_sectionb4_spontimit_score;
			}else {
				return _Scoreform_sectionb4_spontimit_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB4/spontImit/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb4_spontimit_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB4/spontImit/score",v);
		_Scoreform_sectionb4_spontimit_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb4_imagplay_code=null;

	/**
	 * @return Returns the ScoreForm/sectionB4/imagPlay/code.
	 */
	public Integer getScoreform_sectionb4_imagplay_code() {
		try{
			if (_Scoreform_sectionb4_imagplay_code==null){
				_Scoreform_sectionb4_imagplay_code=getIntegerProperty("ScoreForm/sectionB4/imagPlay/code");
				return _Scoreform_sectionb4_imagplay_code;
			}else {
				return _Scoreform_sectionb4_imagplay_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB4/imagPlay/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb4_imagplay_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB4/imagPlay/code",v);
		_Scoreform_sectionb4_imagplay_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb4_imagplay_score=null;

	/**
	 * @return Returns the ScoreForm/sectionB4/imagPlay/score.
	 */
	public Integer getScoreform_sectionb4_imagplay_score() {
		try{
			if (_Scoreform_sectionb4_imagplay_score==null){
				_Scoreform_sectionb4_imagplay_score=getIntegerProperty("ScoreForm/sectionB4/imagPlay/score");
				return _Scoreform_sectionb4_imagplay_score;
			}else {
				return _Scoreform_sectionb4_imagplay_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB4/imagPlay/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb4_imagplay_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB4/imagPlay/score",v);
		_Scoreform_sectionb4_imagplay_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb4_imitplay_code=null;

	/**
	 * @return Returns the ScoreForm/sectionB4/imitPlay/code.
	 */
	public Integer getScoreform_sectionb4_imitplay_code() {
		try{
			if (_Scoreform_sectionb4_imitplay_code==null){
				_Scoreform_sectionb4_imitplay_code=getIntegerProperty("ScoreForm/sectionB4/imitPlay/code");
				return _Scoreform_sectionb4_imitplay_code;
			}else {
				return _Scoreform_sectionb4_imitplay_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB4/imitPlay/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb4_imitplay_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB4/imitPlay/code",v);
		_Scoreform_sectionb4_imitplay_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb4_imitplay_score=null;

	/**
	 * @return Returns the ScoreForm/sectionB4/imitPlay/score.
	 */
	public Integer getScoreform_sectionb4_imitplay_score() {
		try{
			if (_Scoreform_sectionb4_imitplay_score==null){
				_Scoreform_sectionb4_imitplay_score=getIntegerProperty("ScoreForm/sectionB4/imitPlay/score");
				return _Scoreform_sectionb4_imitplay_score;
			}else {
				return _Scoreform_sectionb4_imitplay_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB4/imitPlay/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb4_imitplay_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB4/imitPlay/score",v);
		_Scoreform_sectionb4_imitplay_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb2_socverb_code=null;

	/**
	 * @return Returns the ScoreForm/sectionB2/socVerb/code.
	 */
	public Integer getScoreform_sectionb2_socverb_code() {
		try{
			if (_Scoreform_sectionb2_socverb_code==null){
				_Scoreform_sectionb2_socverb_code=getIntegerProperty("ScoreForm/sectionB2/socVerb/code");
				return _Scoreform_sectionb2_socverb_code;
			}else {
				return _Scoreform_sectionb2_socverb_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB2/socVerb/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb2_socverb_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB2/socVerb/code",v);
		_Scoreform_sectionb2_socverb_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb2_socverb_score=null;

	/**
	 * @return Returns the ScoreForm/sectionB2/socVerb/score.
	 */
	public Integer getScoreform_sectionb2_socverb_score() {
		try{
			if (_Scoreform_sectionb2_socverb_score==null){
				_Scoreform_sectionb2_socverb_score=getIntegerProperty("ScoreForm/sectionB2/socVerb/score");
				return _Scoreform_sectionb2_socverb_score;
			}else {
				return _Scoreform_sectionb2_socverb_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB2/socVerb/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb2_socverb_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB2/socVerb/score",v);
		_Scoreform_sectionb2_socverb_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb2_recipconv_code=null;

	/**
	 * @return Returns the ScoreForm/sectionB2/recipConv/code.
	 */
	public Integer getScoreform_sectionb2_recipconv_code() {
		try{
			if (_Scoreform_sectionb2_recipconv_code==null){
				_Scoreform_sectionb2_recipconv_code=getIntegerProperty("ScoreForm/sectionB2/recipConv/code");
				return _Scoreform_sectionb2_recipconv_code;
			}else {
				return _Scoreform_sectionb2_recipconv_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB2/recipConv/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb2_recipconv_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB2/recipConv/code",v);
		_Scoreform_sectionb2_recipconv_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb2_recipconv_score=null;

	/**
	 * @return Returns the ScoreForm/sectionB2/recipConv/score.
	 */
	public Integer getScoreform_sectionb2_recipconv_score() {
		try{
			if (_Scoreform_sectionb2_recipconv_score==null){
				_Scoreform_sectionb2_recipconv_score=getIntegerProperty("ScoreForm/sectionB2/recipConv/score");
				return _Scoreform_sectionb2_recipconv_score;
			}else {
				return _Scoreform_sectionb2_recipconv_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB2/recipConv/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb2_recipconv_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB2/recipConv/score",v);
		_Scoreform_sectionb2_recipconv_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb3_stereoutter_code=null;

	/**
	 * @return Returns the ScoreForm/sectionB3/stereoUtter/code.
	 */
	public Integer getScoreform_sectionb3_stereoutter_code() {
		try{
			if (_Scoreform_sectionb3_stereoutter_code==null){
				_Scoreform_sectionb3_stereoutter_code=getIntegerProperty("ScoreForm/sectionB3/stereoUtter/code");
				return _Scoreform_sectionb3_stereoutter_code;
			}else {
				return _Scoreform_sectionb3_stereoutter_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB3/stereoUtter/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb3_stereoutter_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB3/stereoUtter/code",v);
		_Scoreform_sectionb3_stereoutter_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb3_stereoutter_score=null;

	/**
	 * @return Returns the ScoreForm/sectionB3/stereoUtter/score.
	 */
	public Integer getScoreform_sectionb3_stereoutter_score() {
		try{
			if (_Scoreform_sectionb3_stereoutter_score==null){
				_Scoreform_sectionb3_stereoutter_score=getIntegerProperty("ScoreForm/sectionB3/stereoUtter/score");
				return _Scoreform_sectionb3_stereoutter_score;
			}else {
				return _Scoreform_sectionb3_stereoutter_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB3/stereoUtter/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb3_stereoutter_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB3/stereoUtter/score",v);
		_Scoreform_sectionb3_stereoutter_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb3_inapprquest_code=null;

	/**
	 * @return Returns the ScoreForm/sectionB3/inapprQuest/code.
	 */
	public Integer getScoreform_sectionb3_inapprquest_code() {
		try{
			if (_Scoreform_sectionb3_inapprquest_code==null){
				_Scoreform_sectionb3_inapprquest_code=getIntegerProperty("ScoreForm/sectionB3/inapprQuest/code");
				return _Scoreform_sectionb3_inapprquest_code;
			}else {
				return _Scoreform_sectionb3_inapprquest_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB3/inapprQuest/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb3_inapprquest_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB3/inapprQuest/code",v);
		_Scoreform_sectionb3_inapprquest_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb3_inapprquest_score=null;

	/**
	 * @return Returns the ScoreForm/sectionB3/inapprQuest/score.
	 */
	public Integer getScoreform_sectionb3_inapprquest_score() {
		try{
			if (_Scoreform_sectionb3_inapprquest_score==null){
				_Scoreform_sectionb3_inapprquest_score=getIntegerProperty("ScoreForm/sectionB3/inapprQuest/score");
				return _Scoreform_sectionb3_inapprquest_score;
			}else {
				return _Scoreform_sectionb3_inapprquest_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB3/inapprQuest/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb3_inapprquest_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB3/inapprQuest/score",v);
		_Scoreform_sectionb3_inapprquest_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb3_pronomrevers_code=null;

	/**
	 * @return Returns the ScoreForm/sectionB3/pronomRevers/code.
	 */
	public Integer getScoreform_sectionb3_pronomrevers_code() {
		try{
			if (_Scoreform_sectionb3_pronomrevers_code==null){
				_Scoreform_sectionb3_pronomrevers_code=getIntegerProperty("ScoreForm/sectionB3/pronomRevers/code");
				return _Scoreform_sectionb3_pronomrevers_code;
			}else {
				return _Scoreform_sectionb3_pronomrevers_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB3/pronomRevers/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb3_pronomrevers_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB3/pronomRevers/code",v);
		_Scoreform_sectionb3_pronomrevers_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb3_pronomrevers_score=null;

	/**
	 * @return Returns the ScoreForm/sectionB3/pronomRevers/score.
	 */
	public Integer getScoreform_sectionb3_pronomrevers_score() {
		try{
			if (_Scoreform_sectionb3_pronomrevers_score==null){
				_Scoreform_sectionb3_pronomrevers_score=getIntegerProperty("ScoreForm/sectionB3/pronomRevers/score");
				return _Scoreform_sectionb3_pronomrevers_score;
			}else {
				return _Scoreform_sectionb3_pronomrevers_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB3/pronomRevers/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb3_pronomrevers_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB3/pronomRevers/score",v);
		_Scoreform_sectionb3_pronomrevers_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb3_neologidio_code=null;

	/**
	 * @return Returns the ScoreForm/sectionB3/neologIdio/code.
	 */
	public Integer getScoreform_sectionb3_neologidio_code() {
		try{
			if (_Scoreform_sectionb3_neologidio_code==null){
				_Scoreform_sectionb3_neologidio_code=getIntegerProperty("ScoreForm/sectionB3/neologIdio/code");
				return _Scoreform_sectionb3_neologidio_code;
			}else {
				return _Scoreform_sectionb3_neologidio_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB3/neologIdio/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb3_neologidio_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB3/neologIdio/code",v);
		_Scoreform_sectionb3_neologidio_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb3_neologidio_score=null;

	/**
	 * @return Returns the ScoreForm/sectionB3/neologIdio/score.
	 */
	public Integer getScoreform_sectionb3_neologidio_score() {
		try{
			if (_Scoreform_sectionb3_neologidio_score==null){
				_Scoreform_sectionb3_neologidio_score=getIntegerProperty("ScoreForm/sectionB3/neologIdio/score");
				return _Scoreform_sectionb3_neologidio_score;
			}else {
				return _Scoreform_sectionb3_neologidio_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB3/neologIdio/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb3_neologidio_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB3/neologIdio/score",v);
		_Scoreform_sectionb3_neologidio_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc1_unuspreocup_code=null;

	/**
	 * @return Returns the ScoreForm/sectionC1/unusPreocup/code.
	 */
	public Integer getScoreform_sectionc1_unuspreocup_code() {
		try{
			if (_Scoreform_sectionc1_unuspreocup_code==null){
				_Scoreform_sectionc1_unuspreocup_code=getIntegerProperty("ScoreForm/sectionC1/unusPreocup/code");
				return _Scoreform_sectionc1_unuspreocup_code;
			}else {
				return _Scoreform_sectionc1_unuspreocup_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC1/unusPreocup/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc1_unuspreocup_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC1/unusPreocup/code",v);
		_Scoreform_sectionc1_unuspreocup_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc1_unuspreocup_score=null;

	/**
	 * @return Returns the ScoreForm/sectionC1/unusPreocup/score.
	 */
	public Integer getScoreform_sectionc1_unuspreocup_score() {
		try{
			if (_Scoreform_sectionc1_unuspreocup_score==null){
				_Scoreform_sectionc1_unuspreocup_score=getIntegerProperty("ScoreForm/sectionC1/unusPreocup/score");
				return _Scoreform_sectionc1_unuspreocup_score;
			}else {
				return _Scoreform_sectionc1_unuspreocup_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC1/unusPreocup/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc1_unuspreocup_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC1/unusPreocup/score",v);
		_Scoreform_sectionc1_unuspreocup_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc1_circumints_code=null;

	/**
	 * @return Returns the ScoreForm/sectionC1/circumInts/code.
	 */
	public Integer getScoreform_sectionc1_circumints_code() {
		try{
			if (_Scoreform_sectionc1_circumints_code==null){
				_Scoreform_sectionc1_circumints_code=getIntegerProperty("ScoreForm/sectionC1/circumInts/code");
				return _Scoreform_sectionc1_circumints_code;
			}else {
				return _Scoreform_sectionc1_circumints_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC1/circumInts/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc1_circumints_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC1/circumInts/code",v);
		_Scoreform_sectionc1_circumints_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc1_circumints_score=null;

	/**
	 * @return Returns the ScoreForm/sectionC1/circumInts/score.
	 */
	public Integer getScoreform_sectionc1_circumints_score() {
		try{
			if (_Scoreform_sectionc1_circumints_score==null){
				_Scoreform_sectionc1_circumints_score=getIntegerProperty("ScoreForm/sectionC1/circumInts/score");
				return _Scoreform_sectionc1_circumints_score;
			}else {
				return _Scoreform_sectionc1_circumints_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC1/circumInts/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc1_circumints_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC1/circumInts/score",v);
		_Scoreform_sectionc1_circumints_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc2_verbrits_code=null;

	/**
	 * @return Returns the ScoreForm/sectionC2/verbRits/code.
	 */
	public Integer getScoreform_sectionc2_verbrits_code() {
		try{
			if (_Scoreform_sectionc2_verbrits_code==null){
				_Scoreform_sectionc2_verbrits_code=getIntegerProperty("ScoreForm/sectionC2/verbRits/code");
				return _Scoreform_sectionc2_verbrits_code;
			}else {
				return _Scoreform_sectionc2_verbrits_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC2/verbRits/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc2_verbrits_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC2/verbRits/code",v);
		_Scoreform_sectionc2_verbrits_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc2_verbrits_score=null;

	/**
	 * @return Returns the ScoreForm/sectionC2/verbRits/score.
	 */
	public Integer getScoreform_sectionc2_verbrits_score() {
		try{
			if (_Scoreform_sectionc2_verbrits_score==null){
				_Scoreform_sectionc2_verbrits_score=getIntegerProperty("ScoreForm/sectionC2/verbRits/score");
				return _Scoreform_sectionc2_verbrits_score;
			}else {
				return _Scoreform_sectionc2_verbrits_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC2/verbRits/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc2_verbrits_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC2/verbRits/score",v);
		_Scoreform_sectionc2_verbrits_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc2_compulrits_code=null;

	/**
	 * @return Returns the ScoreForm/sectionC2/compulRits/code.
	 */
	public Integer getScoreform_sectionc2_compulrits_code() {
		try{
			if (_Scoreform_sectionc2_compulrits_code==null){
				_Scoreform_sectionc2_compulrits_code=getIntegerProperty("ScoreForm/sectionC2/compulRits/code");
				return _Scoreform_sectionc2_compulrits_code;
			}else {
				return _Scoreform_sectionc2_compulrits_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC2/compulRits/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc2_compulrits_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC2/compulRits/code",v);
		_Scoreform_sectionc2_compulrits_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc2_compulrits_score=null;

	/**
	 * @return Returns the ScoreForm/sectionC2/compulRits/score.
	 */
	public Integer getScoreform_sectionc2_compulrits_score() {
		try{
			if (_Scoreform_sectionc2_compulrits_score==null){
				_Scoreform_sectionc2_compulrits_score=getIntegerProperty("ScoreForm/sectionC2/compulRits/score");
				return _Scoreform_sectionc2_compulrits_score;
			}else {
				return _Scoreform_sectionc2_compulrits_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC2/compulRits/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc2_compulrits_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC2/compulRits/score",v);
		_Scoreform_sectionc2_compulrits_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc3_handfingmanns_code=null;

	/**
	 * @return Returns the ScoreForm/sectionC3/handFingManns/code.
	 */
	public Integer getScoreform_sectionc3_handfingmanns_code() {
		try{
			if (_Scoreform_sectionc3_handfingmanns_code==null){
				_Scoreform_sectionc3_handfingmanns_code=getIntegerProperty("ScoreForm/sectionC3/handFingManns/code");
				return _Scoreform_sectionc3_handfingmanns_code;
			}else {
				return _Scoreform_sectionc3_handfingmanns_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC3/handFingManns/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc3_handfingmanns_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC3/handFingManns/code",v);
		_Scoreform_sectionc3_handfingmanns_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc3_handfingmanns_score=null;

	/**
	 * @return Returns the ScoreForm/sectionC3/handFingManns/score.
	 */
	public Integer getScoreform_sectionc3_handfingmanns_score() {
		try{
			if (_Scoreform_sectionc3_handfingmanns_score==null){
				_Scoreform_sectionc3_handfingmanns_score=getIntegerProperty("ScoreForm/sectionC3/handFingManns/score");
				return _Scoreform_sectionc3_handfingmanns_score;
			}else {
				return _Scoreform_sectionc3_handfingmanns_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC3/handFingManns/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc3_handfingmanns_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC3/handFingManns/score",v);
		_Scoreform_sectionc3_handfingmanns_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc3_othcomplxmanns_code=null;

	/**
	 * @return Returns the ScoreForm/sectionC3/othComplxManns/code.
	 */
	public Integer getScoreform_sectionc3_othcomplxmanns_code() {
		try{
			if (_Scoreform_sectionc3_othcomplxmanns_code==null){
				_Scoreform_sectionc3_othcomplxmanns_code=getIntegerProperty("ScoreForm/sectionC3/othComplxManns/code");
				return _Scoreform_sectionc3_othcomplxmanns_code;
			}else {
				return _Scoreform_sectionc3_othcomplxmanns_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC3/othComplxManns/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc3_othcomplxmanns_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC3/othComplxManns/code",v);
		_Scoreform_sectionc3_othcomplxmanns_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc3_othcomplxmanns_score=null;

	/**
	 * @return Returns the ScoreForm/sectionC3/othComplxManns/score.
	 */
	public Integer getScoreform_sectionc3_othcomplxmanns_score() {
		try{
			if (_Scoreform_sectionc3_othcomplxmanns_score==null){
				_Scoreform_sectionc3_othcomplxmanns_score=getIntegerProperty("ScoreForm/sectionC3/othComplxManns/score");
				return _Scoreform_sectionc3_othcomplxmanns_score;
			}else {
				return _Scoreform_sectionc3_othcomplxmanns_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC3/othComplxManns/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc3_othcomplxmanns_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC3/othComplxManns/score",v);
		_Scoreform_sectionc3_othcomplxmanns_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc4_repetuseobjs_code=null;

	/**
	 * @return Returns the ScoreForm/sectionC4/repetUseObjs/code.
	 */
	public Integer getScoreform_sectionc4_repetuseobjs_code() {
		try{
			if (_Scoreform_sectionc4_repetuseobjs_code==null){
				_Scoreform_sectionc4_repetuseobjs_code=getIntegerProperty("ScoreForm/sectionC4/repetUseObjs/code");
				return _Scoreform_sectionc4_repetuseobjs_code;
			}else {
				return _Scoreform_sectionc4_repetuseobjs_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC4/repetUseObjs/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc4_repetuseobjs_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC4/repetUseObjs/code",v);
		_Scoreform_sectionc4_repetuseobjs_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc4_repetuseobjs_score=null;

	/**
	 * @return Returns the ScoreForm/sectionC4/repetUseObjs/score.
	 */
	public Integer getScoreform_sectionc4_repetuseobjs_score() {
		try{
			if (_Scoreform_sectionc4_repetuseobjs_score==null){
				_Scoreform_sectionc4_repetuseobjs_score=getIntegerProperty("ScoreForm/sectionC4/repetUseObjs/score");
				return _Scoreform_sectionc4_repetuseobjs_score;
			}else {
				return _Scoreform_sectionc4_repetuseobjs_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC4/repetUseObjs/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc4_repetuseobjs_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC4/repetUseObjs/score",v);
		_Scoreform_sectionc4_repetuseobjs_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc4_unussensints_code=null;

	/**
	 * @return Returns the ScoreForm/sectionC4/unusSensInts/code.
	 */
	public Integer getScoreform_sectionc4_unussensints_code() {
		try{
			if (_Scoreform_sectionc4_unussensints_code==null){
				_Scoreform_sectionc4_unussensints_code=getIntegerProperty("ScoreForm/sectionC4/unusSensInts/code");
				return _Scoreform_sectionc4_unussensints_code;
			}else {
				return _Scoreform_sectionc4_unussensints_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC4/unusSensInts/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc4_unussensints_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC4/unusSensInts/code",v);
		_Scoreform_sectionc4_unussensints_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc4_unussensints_score=null;

	/**
	 * @return Returns the ScoreForm/sectionC4/unusSensInts/score.
	 */
	public Integer getScoreform_sectionc4_unussensints_score() {
		try{
			if (_Scoreform_sectionc4_unussensints_score==null){
				_Scoreform_sectionc4_unussensints_score=getIntegerProperty("ScoreForm/sectionC4/unusSensInts/score");
				return _Scoreform_sectionc4_unussensints_score;
			}else {
				return _Scoreform_sectionc4_unussensints_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC4/unusSensInts/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc4_unussensints_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC4/unusSensInts/score",v);
		_Scoreform_sectionc4_unussensints_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_agefirstnot_code=null;

	/**
	 * @return Returns the ScoreForm/sectionD/ageFirstNot/code.
	 */
	public Integer getScoreform_sectiond_agefirstnot_code() {
		try{
			if (_Scoreform_sectiond_agefirstnot_code==null){
				_Scoreform_sectiond_agefirstnot_code=getIntegerProperty("ScoreForm/sectionD/ageFirstNot/code");
				return _Scoreform_sectiond_agefirstnot_code;
			}else {
				return _Scoreform_sectiond_agefirstnot_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/ageFirstNot/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_agefirstnot_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/ageFirstNot/code",v);
		_Scoreform_sectiond_agefirstnot_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_agefirstnot_score=null;

	/**
	 * @return Returns the ScoreForm/sectionD/ageFirstNot/score.
	 */
	public Integer getScoreform_sectiond_agefirstnot_score() {
		try{
			if (_Scoreform_sectiond_agefirstnot_score==null){
				_Scoreform_sectiond_agefirstnot_score=getIntegerProperty("ScoreForm/sectionD/ageFirstNot/score");
				return _Scoreform_sectiond_agefirstnot_score;
			}else {
				return _Scoreform_sectiond_agefirstnot_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/ageFirstNot/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_agefirstnot_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/ageFirstNot/score",v);
		_Scoreform_sectiond_agefirstnot_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_agefirstword_code=null;

	/**
	 * @return Returns the ScoreForm/sectionD/ageFirstWord/code.
	 */
	public Integer getScoreform_sectiond_agefirstword_code() {
		try{
			if (_Scoreform_sectiond_agefirstword_code==null){
				_Scoreform_sectiond_agefirstword_code=getIntegerProperty("ScoreForm/sectionD/ageFirstWord/code");
				return _Scoreform_sectiond_agefirstword_code;
			}else {
				return _Scoreform_sectiond_agefirstword_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/ageFirstWord/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_agefirstword_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/ageFirstWord/code",v);
		_Scoreform_sectiond_agefirstword_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_agefirstword_score=null;

	/**
	 * @return Returns the ScoreForm/sectionD/ageFirstWord/score.
	 */
	public Integer getScoreform_sectiond_agefirstword_score() {
		try{
			if (_Scoreform_sectiond_agefirstword_score==null){
				_Scoreform_sectiond_agefirstword_score=getIntegerProperty("ScoreForm/sectionD/ageFirstWord/score");
				return _Scoreform_sectiond_agefirstword_score;
			}else {
				return _Scoreform_sectiond_agefirstword_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/ageFirstWord/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_agefirstword_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/ageFirstWord/score",v);
		_Scoreform_sectiond_agefirstword_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_agefirstphrase_code=null;

	/**
	 * @return Returns the ScoreForm/sectionD/ageFirstPhrase/code.
	 */
	public Integer getScoreform_sectiond_agefirstphrase_code() {
		try{
			if (_Scoreform_sectiond_agefirstphrase_code==null){
				_Scoreform_sectiond_agefirstphrase_code=getIntegerProperty("ScoreForm/sectionD/ageFirstPhrase/code");
				return _Scoreform_sectiond_agefirstphrase_code;
			}else {
				return _Scoreform_sectiond_agefirstphrase_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/ageFirstPhrase/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_agefirstphrase_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/ageFirstPhrase/code",v);
		_Scoreform_sectiond_agefirstphrase_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_agefirstphrase_score=null;

	/**
	 * @return Returns the ScoreForm/sectionD/ageFirstPhrase/score.
	 */
	public Integer getScoreform_sectiond_agefirstphrase_score() {
		try{
			if (_Scoreform_sectiond_agefirstphrase_score==null){
				_Scoreform_sectiond_agefirstphrase_score=getIntegerProperty("ScoreForm/sectionD/ageFirstPhrase/score");
				return _Scoreform_sectiond_agefirstphrase_score;
			}else {
				return _Scoreform_sectiond_agefirstphrase_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/ageFirstPhrase/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_agefirstphrase_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/ageFirstPhrase/score",v);
		_Scoreform_sectiond_agefirstphrase_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_agefirstevdnt_code=null;

	/**
	 * @return Returns the ScoreForm/sectionD/ageFirstEvdnt/code.
	 */
	public Integer getScoreform_sectiond_agefirstevdnt_code() {
		try{
			if (_Scoreform_sectiond_agefirstevdnt_code==null){
				_Scoreform_sectiond_agefirstevdnt_code=getIntegerProperty("ScoreForm/sectionD/ageFirstEvdnt/code");
				return _Scoreform_sectiond_agefirstevdnt_code;
			}else {
				return _Scoreform_sectiond_agefirstevdnt_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/ageFirstEvdnt/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_agefirstevdnt_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/ageFirstEvdnt/code",v);
		_Scoreform_sectiond_agefirstevdnt_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_agefirstevdnt_score=null;

	/**
	 * @return Returns the ScoreForm/sectionD/ageFirstEvdnt/score.
	 */
	public Integer getScoreform_sectiond_agefirstevdnt_score() {
		try{
			if (_Scoreform_sectiond_agefirstevdnt_score==null){
				_Scoreform_sectiond_agefirstevdnt_score=getIntegerProperty("ScoreForm/sectionD/ageFirstEvdnt/score");
				return _Scoreform_sectiond_agefirstevdnt_score;
			}else {
				return _Scoreform_sectiond_agefirstevdnt_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/ageFirstEvdnt/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_agefirstevdnt_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/ageFirstEvdnt/score",v);
		_Scoreform_sectiond_agefirstevdnt_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_agefirstmanifst_code=null;

	/**
	 * @return Returns the ScoreForm/sectionD/ageFirstManifst/code.
	 */
	public Integer getScoreform_sectiond_agefirstmanifst_code() {
		try{
			if (_Scoreform_sectiond_agefirstmanifst_code==null){
				_Scoreform_sectiond_agefirstmanifst_code=getIntegerProperty("ScoreForm/sectionD/ageFirstManifst/code");
				return _Scoreform_sectiond_agefirstmanifst_code;
			}else {
				return _Scoreform_sectiond_agefirstmanifst_code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/ageFirstManifst/code.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_agefirstmanifst_code(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/ageFirstManifst/code",v);
		_Scoreform_sectiond_agefirstmanifst_code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_agefirstmanifst_score=null;

	/**
	 * @return Returns the ScoreForm/sectionD/ageFirstManifst/score.
	 */
	public Integer getScoreform_sectiond_agefirstmanifst_score() {
		try{
			if (_Scoreform_sectiond_agefirstmanifst_score==null){
				_Scoreform_sectiond_agefirstmanifst_score=getIntegerProperty("ScoreForm/sectionD/ageFirstManifst/score");
				return _Scoreform_sectiond_agefirstmanifst_score;
			}else {
				return _Scoreform_sectiond_agefirstmanifst_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/ageFirstManifst/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_agefirstmanifst_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/ageFirstManifst/score",v);
		_Scoreform_sectiond_agefirstmanifst_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.AdirAdir2007data> getAllAdirAdir2007datas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.AdirAdir2007data> al = new ArrayList<org.nrg.xdat.om.AdirAdir2007data>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.AdirAdir2007data> getAdirAdir2007datasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.AdirAdir2007data> al = new ArrayList<org.nrg.xdat.om.AdirAdir2007data>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.AdirAdir2007data> getAdirAdir2007datasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.AdirAdir2007data> al = new ArrayList<org.nrg.xdat.om.AdirAdir2007data>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static AdirAdir2007data getAdirAdir2007datasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("adir:adir2007Data/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (AdirAdir2007data) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
