/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:01 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoOptiOisprocdata extends XnatImageassessordata implements org.nrg.xdat.model.OptiOisprocdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoOptiOisprocdata.class);
	public static String SCHEMA_ELEMENT_NAME="opti:oisProcData";

	public AutoOptiOisprocdata(ItemI item)
	{
		super(item);
	}

	public AutoOptiOisprocdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoOptiOisprocdata(UserI user)
	 **/
	public AutoOptiOisprocdata(){}

	public AutoOptiOisprocdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "opti:oisProcData";
	}
	 private org.nrg.xdat.om.XnatImageassessordata _Imageassessordata =null;

	/**
	 * imageAssessorData
	 * @return org.nrg.xdat.om.XnatImageassessordata
	 */
	public org.nrg.xdat.om.XnatImageassessordata getImageassessordata() {
		try{
			if (_Imageassessordata==null){
				_Imageassessordata=((XnatImageassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("imageAssessorData")));
				return _Imageassessordata;
			}else {
				return _Imageassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for imageAssessorData.
	 * @param v Value to Set.
	 */
	public void setImageassessordata(ItemI v) throws Exception{
		_Imageassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/imageAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/imageAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * imageAssessorData
	 * set org.nrg.xdat.model.XnatImageassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatImageassessordataI> void setImageassessordata(A item) throws Exception{
	setImageassessordata((ItemI)item);
	}

	/**
	 * Removes the imageAssessorData.
	 * */
	public void removeImageassessordata() {
		_Imageassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/imageAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Inputparams_rsSamplingrate=null;

	/**
	 * @return Returns the inputParams/rs_samplingrate.
	 */
	public Double getInputparams_rsSamplingrate() {
		try{
			if (_Inputparams_rsSamplingrate==null){
				_Inputparams_rsSamplingrate=getDoubleProperty("inputParams/rs_samplingrate");
				return _Inputparams_rsSamplingrate;
			}else {
				return _Inputparams_rsSamplingrate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for inputParams/rs_samplingrate.
	 * @param v Value to Set.
	 */
	public void setInputparams_rsSamplingrate(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/inputParams/rs_samplingrate",v);
		_Inputparams_rsSamplingrate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Inputparams_rsLowpass=null;

	/**
	 * @return Returns the inputParams/rs_lowpass.
	 */
	public Double getInputparams_rsLowpass() {
		try{
			if (_Inputparams_rsLowpass==null){
				_Inputparams_rsLowpass=getDoubleProperty("inputParams/rs_lowpass");
				return _Inputparams_rsLowpass;
			}else {
				return _Inputparams_rsLowpass;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for inputParams/rs_lowpass.
	 * @param v Value to Set.
	 */
	public void setInputparams_rsLowpass(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/inputParams/rs_lowpass",v);
		_Inputparams_rsLowpass=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Inputparams_rsHighpass=null;

	/**
	 * @return Returns the inputParams/rs_highpass.
	 */
	public Double getInputparams_rsHighpass() {
		try{
			if (_Inputparams_rsHighpass==null){
				_Inputparams_rsHighpass=getDoubleProperty("inputParams/rs_highpass");
				return _Inputparams_rsHighpass;
			}else {
				return _Inputparams_rsHighpass;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for inputParams/rs_highpass.
	 * @param v Value to Set.
	 */
	public void setInputparams_rsHighpass(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/inputParams/rs_highpass",v);
		_Inputparams_rsHighpass=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Inputparams_stimSamplingrate=null;

	/**
	 * @return Returns the inputParams/stim_samplingrate.
	 */
	public Double getInputparams_stimSamplingrate() {
		try{
			if (_Inputparams_stimSamplingrate==null){
				_Inputparams_stimSamplingrate=getDoubleProperty("inputParams/stim_samplingrate");
				return _Inputparams_stimSamplingrate;
			}else {
				return _Inputparams_stimSamplingrate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for inputParams/stim_samplingrate.
	 * @param v Value to Set.
	 */
	public void setInputparams_stimSamplingrate(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/inputParams/stim_samplingrate",v);
		_Inputparams_stimSamplingrate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Inputparams_stimLowpass=null;

	/**
	 * @return Returns the inputParams/stim_lowpass.
	 */
	public Double getInputparams_stimLowpass() {
		try{
			if (_Inputparams_stimLowpass==null){
				_Inputparams_stimLowpass=getDoubleProperty("inputParams/stim_lowpass");
				return _Inputparams_stimLowpass;
			}else {
				return _Inputparams_stimLowpass;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for inputParams/stim_lowpass.
	 * @param v Value to Set.
	 */
	public void setInputparams_stimLowpass(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/inputParams/stim_lowpass",v);
		_Inputparams_stimLowpass=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Inputparams_stimHighpass=null;

	/**
	 * @return Returns the inputParams/stim_highpass.
	 */
	public Double getInputparams_stimHighpass() {
		try{
			if (_Inputparams_stimHighpass==null){
				_Inputparams_stimHighpass=getDoubleProperty("inputParams/stim_highpass");
				return _Inputparams_stimHighpass;
			}else {
				return _Inputparams_stimHighpass;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for inputParams/stim_highpass.
	 * @param v Value to Set.
	 */
	public void setInputparams_stimHighpass(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/inputParams/stim_highpass",v);
		_Inputparams_stimHighpass=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Inputparams_stimBlocksize=null;

	/**
	 * @return Returns the inputParams/stim_blocksize.
	 */
	public Double getInputparams_stimBlocksize() {
		try{
			if (_Inputparams_stimBlocksize==null){
				_Inputparams_stimBlocksize=getDoubleProperty("inputParams/stim_blocksize");
				return _Inputparams_stimBlocksize;
			}else {
				return _Inputparams_stimBlocksize;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for inputParams/stim_blocksize.
	 * @param v Value to Set.
	 */
	public void setInputparams_stimBlocksize(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/inputParams/stim_blocksize",v);
		_Inputparams_stimBlocksize=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Inputparams_stimBaseline=null;

	/**
	 * @return Returns the inputParams/stim_baseline.
	 */
	public Double getInputparams_stimBaseline() {
		try{
			if (_Inputparams_stimBaseline==null){
				_Inputparams_stimBaseline=getDoubleProperty("inputParams/stim_baseline");
				return _Inputparams_stimBaseline;
			}else {
				return _Inputparams_stimBaseline;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for inputParams/stim_baseline.
	 * @param v Value to Set.
	 */
	public void setInputparams_stimBaseline(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/inputParams/stim_baseline",v);
		_Inputparams_stimBaseline=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Inputparams_stimDuration=null;

	/**
	 * @return Returns the inputParams/stim_duration.
	 */
	public Double getInputparams_stimDuration() {
		try{
			if (_Inputparams_stimDuration==null){
				_Inputparams_stimDuration=getDoubleProperty("inputParams/stim_duration");
				return _Inputparams_stimDuration;
			}else {
				return _Inputparams_stimDuration;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for inputParams/stim_duration.
	 * @param v Value to Set.
	 */
	public void setInputparams_stimDuration(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/inputParams/stim_duration",v);
		_Inputparams_stimDuration=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.OptiOisprocdataFramesHb> _FramesHbs_framesHb =null;

	/**
	 * frames_hbs/frames_hb
	 * @return Returns an List of org.nrg.xdat.om.OptiOisprocdataFramesHb
	 */
	public <A extends org.nrg.xdat.model.OptiOisprocdataFramesHbI> List<A> getFramesHbs_framesHb() {
		try{
			if (_FramesHbs_framesHb==null){
				_FramesHbs_framesHb=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("frames_hbs/frames_hb"));
			}
			return (List<A>) _FramesHbs_framesHb;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.OptiOisprocdataFramesHb>();}
	}

	/**
	 * Sets the value for frames_hbs/frames_hb.
	 * @param v Value to Set.
	 */
	public void setFramesHbs_framesHb(ItemI v) throws Exception{
		_FramesHbs_framesHb =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/frames_hbs/frames_hb",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/frames_hbs/frames_hb",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * frames_hbs/frames_hb
	 * Adds org.nrg.xdat.model.OptiOisprocdataFramesHbI
	 */
	public <A extends org.nrg.xdat.model.OptiOisprocdataFramesHbI> void addFramesHbs_framesHb(A item) throws Exception{
	setFramesHbs_framesHb((ItemI)item);
	}

	/**
	 * Removes the frames_hbs/frames_hb of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeFramesHbs_framesHb(int index) throws java.lang.IndexOutOfBoundsException {
		_FramesHbs_framesHb =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/frames_hbs/frames_hb",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.OptiOisprocdata> getAllOptiOisprocdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.OptiOisprocdata> al = new ArrayList<org.nrg.xdat.om.OptiOisprocdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.OptiOisprocdata> getOptiOisprocdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.OptiOisprocdata> al = new ArrayList<org.nrg.xdat.om.OptiOisprocdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.OptiOisprocdata> getOptiOisprocdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.OptiOisprocdata> al = new ArrayList<org.nrg.xdat.om.OptiOisprocdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static OptiOisprocdata getOptiOisprocdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("opti:oisProcData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (OptiOisprocdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //imageAssessorData
	        XnatImageassessordata childImageassessordata = (XnatImageassessordata)this.getImageassessordata();
	            if (childImageassessordata!=null){
	              for(ResourceFile rf: ((XnatImageassessordata)childImageassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("imageAssessorData[" + ((XnatImageassessordata)childImageassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("imageAssessorData/" + ((XnatImageassessordata)childImageassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //frames_hbs/frames_hb
	        for(org.nrg.xdat.model.OptiOisprocdataFramesHbI childFramesHbs_framesHb : this.getFramesHbs_framesHb()){
	            if (childFramesHbs_framesHb!=null){
	              for(ResourceFile rf: ((OptiOisprocdataFramesHb)childFramesHbs_framesHb).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("frames_hbs/frames_hb[" + ((OptiOisprocdataFramesHb)childFramesHbs_framesHb).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("frames_hbs/frames_hb/" + ((OptiOisprocdataFramesHb)childFramesHbs_framesHb).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
