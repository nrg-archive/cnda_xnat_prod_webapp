/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoLs2Lssubstance extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.Ls2LssubstanceI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoLs2Lssubstance.class);
	public static String SCHEMA_ELEMENT_NAME="ls2:lsSubstance";

	public AutoLs2Lssubstance(ItemI item)
	{
		super(item);
	}

	public AutoLs2Lssubstance(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoLs2Lssubstance(UserI user)
	 **/
	public AutoLs2Lssubstance(){}

	public AutoLs2Lssubstance(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "ls2:lsSubstance";
	}

	//FIELD

	private String _Current=null;

	/**
	 * @return Returns the current.
	 */
	public String getCurrent(){
		try{
			if (_Current==null){
				_Current=getStringProperty("current");
				return _Current;
			}else {
				return _Current;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for current.
	 * @param v Value to Set.
	 */
	public void setCurrent(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/current",v);
		_Current=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Past=null;

	/**
	 * @return Returns the past.
	 */
	public String getPast(){
		try{
			if (_Past==null){
				_Past=getStringProperty("past");
				return _Past;
			}else {
				return _Past;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for past.
	 * @param v Value to Set.
	 */
	public void setPast(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/past",v);
		_Past=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Name=null;

	/**
	 * @return Returns the name.
	 */
	public String getName(){
		try{
			if (_Name==null){
				_Name=getStringProperty("name");
				return _Name;
			}else {
				return _Name;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for name.
	 * @param v Value to Set.
	 */
	public void setName(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/name",v);
		_Name=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ls2LssubstanceId=null;

	/**
	 * @return Returns the ls2_lsSubstance_id.
	 */
	public Integer getLs2LssubstanceId() {
		try{
			if (_Ls2LssubstanceId==null){
				_Ls2LssubstanceId=getIntegerProperty("ls2_lsSubstance_id");
				return _Ls2LssubstanceId;
			}else {
				return _Ls2LssubstanceId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ls2_lsSubstance_id.
	 * @param v Value to Set.
	 */
	public void setLs2LssubstanceId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ls2_lsSubstance_id",v);
		_Ls2LssubstanceId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.Ls2Lssubstance> getAllLs2Lssubstances(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.Ls2Lssubstance> al = new ArrayList<org.nrg.xdat.om.Ls2Lssubstance>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.Ls2Lssubstance> getLs2LssubstancesByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.Ls2Lssubstance> al = new ArrayList<org.nrg.xdat.om.Ls2Lssubstance>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.Ls2Lssubstance> getLs2LssubstancesByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.Ls2Lssubstance> al = new ArrayList<org.nrg.xdat.om.Ls2Lssubstance>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static Ls2Lssubstance getLs2LssubstancesByLs2LssubstanceId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("ls2:lsSubstance/ls2_lsSubstance_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (Ls2Lssubstance) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
