/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:00 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianMonrevdata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianMonrevdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianMonrevdata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:monrevData";

	public AutoDianMonrevdata(ItemI item)
	{
		super(item);
	}

	public AutoDianMonrevdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianMonrevdata(UserI user)
	 **/
	public AutoDianMonrevdata(){}

	public AutoDianMonrevdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:monrevData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Userid2=null;

	/**
	 * @return Returns the USERID2.
	 */
	public String getUserid2(){
		try{
			if (_Userid2==null){
				_Userid2=getStringProperty("USERID2");
				return _Userid2;
			}else {
				return _Userid2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for USERID2.
	 * @param v Value to Set.
	 */
	public void setUserid2(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/USERID2",v);
		_Userid2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Userdate2=null;

	/**
	 * @return Returns the USERDATE2.
	 */
	public Object getUserdate2(){
		try{
			if (_Userdate2==null){
				_Userdate2=getProperty("USERDATE2");
				return _Userdate2;
			}else {
				return _Userdate2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for USERDATE2.
	 * @param v Value to Set.
	 */
	public void setUserdate2(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/USERDATE2",v);
		_Userdate2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Monpass=null;

	/**
	 * @return Returns the MONPASS.
	 */
	public Integer getMonpass() {
		try{
			if (_Monpass==null){
				_Monpass=getIntegerProperty("MONPASS");
				return _Monpass;
			}else {
				return _Monpass;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MONPASS.
	 * @param v Value to Set.
	 */
	public void setMonpass(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MONPASS",v);
		_Monpass=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Mondate=null;

	/**
	 * @return Returns the MONDATE.
	 */
	public Object getMondate(){
		try{
			if (_Mondate==null){
				_Mondate=getProperty("MONDATE");
				return _Mondate;
			}else {
				return _Mondate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MONDATE.
	 * @param v Value to Set.
	 */
	public void setMondate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MONDATE",v);
		_Mondate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Moncomm=null;

	/**
	 * @return Returns the MONCOMM.
	 */
	public String getMoncomm(){
		try{
			if (_Moncomm==null){
				_Moncomm=getStringProperty("MONCOMM");
				return _Moncomm;
			}else {
				return _Moncomm;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MONCOMM.
	 * @param v Value to Set.
	 */
	public void setMoncomm(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MONCOMM",v);
		_Moncomm=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianMonrevdata> getAllDianMonrevdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianMonrevdata> al = new ArrayList<org.nrg.xdat.om.DianMonrevdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianMonrevdata> getDianMonrevdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianMonrevdata> al = new ArrayList<org.nrg.xdat.om.DianMonrevdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianMonrevdata> getDianMonrevdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianMonrevdata> al = new ArrayList<org.nrg.xdat.om.DianMonrevdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianMonrevdata getDianMonrevdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:monrevData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianMonrevdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
