/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoGeneticsGenotypesessiondataGene extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.GeneticsGenotypesessiondataGeneI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoGeneticsGenotypesessiondataGene.class);
	public static String SCHEMA_ELEMENT_NAME="genetics:genotypeSessionData_gene";

	public AutoGeneticsGenotypesessiondataGene(ItemI item)
	{
		super(item);
	}

	public AutoGeneticsGenotypesessiondataGene(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoGeneticsGenotypesessiondataGene(UserI user)
	 **/
	public AutoGeneticsGenotypesessiondataGene(){}

	public AutoGeneticsGenotypesessiondataGene(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "genetics:genotypeSessionData_gene";
	}

	//FIELD

	private String _Gene=null;

	/**
	 * @return Returns the gene.
	 */
	public String getGene(){
		try{
			if (_Gene==null){
				_Gene=getStringProperty("gene");
				return _Gene;
			}else {
				return _Gene;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for gene.
	 * @param v Value to Set.
	 */
	public void setGene(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/gene",v);
		_Gene=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Name=null;

	/**
	 * @return Returns the name.
	 */
	public String getName(){
		try{
			if (_Name==null){
				_Name=getStringProperty("name");
				return _Name;
			}else {
				return _Name;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for name.
	 * @param v Value to Set.
	 */
	public void setName(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/name",v);
		_Name=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _GeneticsGenotypesessiondataGeneId=null;

	/**
	 * @return Returns the genetics_genotypeSessionData_gene_id.
	 */
	public Integer getGeneticsGenotypesessiondataGeneId() {
		try{
			if (_GeneticsGenotypesessiondataGeneId==null){
				_GeneticsGenotypesessiondataGeneId=getIntegerProperty("genetics_genotypeSessionData_gene_id");
				return _GeneticsGenotypesessiondataGeneId;
			}else {
				return _GeneticsGenotypesessiondataGeneId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for genetics_genotypeSessionData_gene_id.
	 * @param v Value to Set.
	 */
	public void setGeneticsGenotypesessiondataGeneId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/genetics_genotypeSessionData_gene_id",v);
		_GeneticsGenotypesessiondataGeneId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.GeneticsGenotypesessiondataGene> getAllGeneticsGenotypesessiondataGenes(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.GeneticsGenotypesessiondataGene> al = new ArrayList<org.nrg.xdat.om.GeneticsGenotypesessiondataGene>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.GeneticsGenotypesessiondataGene> getGeneticsGenotypesessiondataGenesByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.GeneticsGenotypesessiondataGene> al = new ArrayList<org.nrg.xdat.om.GeneticsGenotypesessiondataGene>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.GeneticsGenotypesessiondataGene> getGeneticsGenotypesessiondataGenesByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.GeneticsGenotypesessiondataGene> al = new ArrayList<org.nrg.xdat.om.GeneticsGenotypesessiondataGene>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static GeneticsGenotypesessiondataGene getGeneticsGenotypesessiondataGenesByGeneticsGenotypesessiondataGeneId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("genetics:genotypeSessionData_gene/genetics_genotypeSessionData_gene_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (GeneticsGenotypesessiondataGene) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
