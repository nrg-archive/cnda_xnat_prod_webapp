/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:01 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianCdrsuppdata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianCdrsuppdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianCdrsuppdata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:cdrsuppData";

	public AutoDianCdrsuppdata(ItemI item)
	{
		super(item);
	}

	public AutoDianCdrsuppdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianCdrsuppdata(UserI user)
	 **/
	public AutoDianCdrsuppdata(){}

	public AutoDianCdrsuppdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:cdrsuppData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Amend2=null;

	/**
	 * @return Returns the AMEND2.
	 */
	public Integer getAmend2() {
		try{
			if (_Amend2==null){
				_Amend2=getIntegerProperty("AMEND2");
				return _Amend2;
			}else {
				return _Amend2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AMEND2.
	 * @param v Value to Set.
	 */
	public void setAmend2(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AMEND2",v);
		_Amend2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Changes=null;

	/**
	 * @return Returns the CHANGES.
	 */
	public Integer getChanges() {
		try{
			if (_Changes==null){
				_Changes=getIntegerProperty("CHANGES");
				return _Changes;
			}else {
				return _Changes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CHANGES.
	 * @param v Value to Set.
	 */
	public void setChanges(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CHANGES",v);
		_Changes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Memory=null;

	/**
	 * @return Returns the MEMORY.
	 */
	public Integer getMemory() {
		try{
			if (_Memory==null){
				_Memory=getIntegerProperty("MEMORY");
				return _Memory;
			}else {
				return _Memory;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MEMORY.
	 * @param v Value to Set.
	 */
	public void setMemory(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MEMORY",v);
		_Memory=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Memorym=null;

	/**
	 * @return Returns the MEMORYM.
	 */
	public Integer getMemorym() {
		try{
			if (_Memorym==null){
				_Memorym=getIntegerProperty("MEMORYM");
				return _Memorym;
			}else {
				return _Memorym;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MEMORYM.
	 * @param v Value to Set.
	 */
	public void setMemorym(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MEMORYM",v);
		_Memorym=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Judgment=null;

	/**
	 * @return Returns the JUDGMENT.
	 */
	public Integer getJudgment() {
		try{
			if (_Judgment==null){
				_Judgment=getIntegerProperty("JUDGMENT");
				return _Judgment;
			}else {
				return _Judgment;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for JUDGMENT.
	 * @param v Value to Set.
	 */
	public void setJudgment(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/JUDGMENT",v);
		_Judgment=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Judgmentm=null;

	/**
	 * @return Returns the JUDGMENTM.
	 */
	public Integer getJudgmentm() {
		try{
			if (_Judgmentm==null){
				_Judgmentm=getIntegerProperty("JUDGMENTM");
				return _Judgmentm;
			}else {
				return _Judgmentm;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for JUDGMENTM.
	 * @param v Value to Set.
	 */
	public void setJudgmentm(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/JUDGMENTM",v);
		_Judgmentm=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Language=null;

	/**
	 * @return Returns the LANGUAGE.
	 */
	public Integer getLanguage() {
		try{
			if (_Language==null){
				_Language=getIntegerProperty("LANGUAGE");
				return _Language;
			}else {
				return _Language;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LANGUAGE.
	 * @param v Value to Set.
	 */
	public void setLanguage(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LANGUAGE",v);
		_Language=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Languagem=null;

	/**
	 * @return Returns the LANGUAGEM.
	 */
	public Integer getLanguagem() {
		try{
			if (_Languagem==null){
				_Languagem=getIntegerProperty("LANGUAGEM");
				return _Languagem;
			}else {
				return _Languagem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LANGUAGEM.
	 * @param v Value to Set.
	 */
	public void setLanguagem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LANGUAGEM",v);
		_Languagem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Visuospat=null;

	/**
	 * @return Returns the VISUOSPAT.
	 */
	public Integer getVisuospat() {
		try{
			if (_Visuospat==null){
				_Visuospat=getIntegerProperty("VISUOSPAT");
				return _Visuospat;
			}else {
				return _Visuospat;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VISUOSPAT.
	 * @param v Value to Set.
	 */
	public void setVisuospat(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VISUOSPAT",v);
		_Visuospat=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Visuospatm=null;

	/**
	 * @return Returns the VISUOSPATM.
	 */
	public Integer getVisuospatm() {
		try{
			if (_Visuospatm==null){
				_Visuospatm=getIntegerProperty("VISUOSPATM");
				return _Visuospatm;
			}else {
				return _Visuospatm;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VISUOSPATM.
	 * @param v Value to Set.
	 */
	public void setVisuospatm(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VISUOSPATM",v);
		_Visuospatm=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Attention=null;

	/**
	 * @return Returns the ATTENTION.
	 */
	public Integer getAttention() {
		try{
			if (_Attention==null){
				_Attention=getIntegerProperty("ATTENTION");
				return _Attention;
			}else {
				return _Attention;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ATTENTION.
	 * @param v Value to Set.
	 */
	public void setAttention(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ATTENTION",v);
		_Attention=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Attentionm=null;

	/**
	 * @return Returns the ATTENTIONM.
	 */
	public Integer getAttentionm() {
		try{
			if (_Attentionm==null){
				_Attentionm=getIntegerProperty("ATTENTIONM");
				return _Attentionm;
			}else {
				return _Attentionm;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ATTENTIONM.
	 * @param v Value to Set.
	 */
	public void setAttentionm(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ATTENTIONM",v);
		_Attentionm=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Behavior=null;

	/**
	 * @return Returns the BEHAVIOR.
	 */
	public Integer getBehavior() {
		try{
			if (_Behavior==null){
				_Behavior=getIntegerProperty("BEHAVIOR");
				return _Behavior;
			}else {
				return _Behavior;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BEHAVIOR.
	 * @param v Value to Set.
	 */
	public void setBehavior(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BEHAVIOR",v);
		_Behavior=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Behaviorm=null;

	/**
	 * @return Returns the BEHAVIORM.
	 */
	public Integer getBehaviorm() {
		try{
			if (_Behaviorm==null){
				_Behaviorm=getIntegerProperty("BEHAVIORM");
				return _Behaviorm;
			}else {
				return _Behaviorm;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BEHAVIORM.
	 * @param v Value to Set.
	 */
	public void setBehaviorm(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BEHAVIORM",v);
		_Behaviorm=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianCdrsuppdata> getAllDianCdrsuppdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianCdrsuppdata> al = new ArrayList<org.nrg.xdat.om.DianCdrsuppdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianCdrsuppdata> getDianCdrsuppdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianCdrsuppdata> al = new ArrayList<org.nrg.xdat.om.DianCdrsuppdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianCdrsuppdata> getDianCdrsuppdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianCdrsuppdata> al = new ArrayList<org.nrg.xdat.om.DianCdrsuppdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianCdrsuppdata getDianCdrsuppdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:cdrsuppData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianCdrsuppdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
