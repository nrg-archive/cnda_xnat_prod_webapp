/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaTicvideocountsdata extends XnatSubjectassessordata implements org.nrg.xdat.model.CndaTicvideocountsdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaTicvideocountsdata.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:ticVideoCountsData";

	public AutoCndaTicvideocountsdata(ItemI item)
	{
		super(item);
	}

	public AutoCndaTicvideocountsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaTicvideocountsdata(UserI user)
	 **/
	public AutoCndaTicvideocountsdata(){}

	public AutoCndaTicvideocountsdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:ticVideoCountsData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Tape=null;

	/**
	 * @return Returns the tape.
	 */
	public String getTape(){
		try{
			if (_Tape==null){
				_Tape=getStringProperty("tape");
				return _Tape;
			}else {
				return _Tape;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tape.
	 * @param v Value to Set.
	 */
	public void setTape(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tape",v);
		_Tape=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Tapetime=null;

	/**
	 * @return Returns the tapeTime.
	 */
	public String getTapetime(){
		try{
			if (_Tapetime==null){
				_Tapetime=getStringProperty("tapeTime");
				return _Tapetime;
			}else {
				return _Tapetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tapeTime.
	 * @param v Value to Set.
	 */
	public void setTapetime(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tapeTime",v);
		_Tapetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Tapeinfo=null;

	/**
	 * @return Returns the tapeInfo.
	 */
	public String getTapeinfo(){
		try{
			if (_Tapeinfo==null){
				_Tapeinfo=getStringProperty("tapeInfo");
				return _Tapeinfo;
			}else {
				return _Tapeinfo;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tapeInfo.
	 * @param v Value to Set.
	 */
	public void setTapeinfo(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tapeInfo",v);
		_Tapeinfo=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Boldnumber=null;

	/**
	 * @return Returns the boldNumber.
	 */
	public String getBoldnumber(){
		try{
			if (_Boldnumber==null){
				_Boldnumber=getStringProperty("boldNumber");
				return _Boldnumber;
			}else {
				return _Boldnumber;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for boldNumber.
	 * @param v Value to Set.
	 */
	public void setBoldnumber(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/boldNumber",v);
		_Boldnumber=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Drug=null;

	/**
	 * @return Returns the drug.
	 */
	public String getDrug(){
		try{
			if (_Drug==null){
				_Drug=getStringProperty("drug");
				return _Drug;
			}else {
				return _Drug;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for drug.
	 * @param v Value to Set.
	 */
	public void setDrug(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/drug",v);
		_Drug=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Examinerinroom=null;

	/**
	 * @return Returns the examinerInRoom.
	 */
	public Boolean getExaminerinroom() {
		try{
			if (_Examinerinroom==null){
				_Examinerinroom=getBooleanProperty("examinerInRoom");
				return _Examinerinroom;
			}else {
				return _Examinerinroom;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for examinerInRoom.
	 * @param v Value to Set.
	 */
	public void setExaminerinroom(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/examinerInRoom",v);
		_Examinerinroom=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Durationheadshoulders=null;

	/**
	 * @return Returns the durationHeadShoulders.
	 */
	public String getDurationheadshoulders(){
		try{
			if (_Durationheadshoulders==null){
				_Durationheadshoulders=getStringProperty("durationHeadShoulders");
				return _Durationheadshoulders;
			}else {
				return _Durationheadshoulders;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for durationHeadShoulders.
	 * @param v Value to Set.
	 */
	public void setDurationheadshoulders(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/durationHeadShoulders",v);
		_Durationheadshoulders=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Durationfullbody=null;

	/**
	 * @return Returns the durationFullBody.
	 */
	public String getDurationfullbody(){
		try{
			if (_Durationfullbody==null){
				_Durationfullbody=getStringProperty("durationFullBody");
				return _Durationfullbody;
			}else {
				return _Durationfullbody;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for durationFullBody.
	 * @param v Value to Set.
	 */
	public void setDurationfullbody(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/durationFullBody",v);
		_Durationfullbody=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rater=null;

	/**
	 * @return Returns the rater.
	 */
	public String getRater(){
		try{
			if (_Rater==null){
				_Rater=getStringProperty("rater");
				return _Rater;
			}else {
				return _Rater;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for rater.
	 * @param v Value to Set.
	 */
	public void setRater(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/rater",v);
		_Rater=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Eyes=null;

	/**
	 * @return Returns the eyes.
	 */
	public Boolean getEyes() {
		try{
			if (_Eyes==null){
				_Eyes=getBooleanProperty("eyes");
				return _Eyes;
			}else {
				return _Eyes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for eyes.
	 * @param v Value to Set.
	 */
	public void setEyes(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/eyes",v);
		_Eyes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Nose=null;

	/**
	 * @return Returns the nose.
	 */
	public Boolean getNose() {
		try{
			if (_Nose==null){
				_Nose=getBooleanProperty("nose");
				return _Nose;
			}else {
				return _Nose;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for nose.
	 * @param v Value to Set.
	 */
	public void setNose(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/nose",v);
		_Nose=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Mouth=null;

	/**
	 * @return Returns the mouth.
	 */
	public Boolean getMouth() {
		try{
			if (_Mouth==null){
				_Mouth=getBooleanProperty("mouth");
				return _Mouth;
			}else {
				return _Mouth;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for mouth.
	 * @param v Value to Set.
	 */
	public void setMouth(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/mouth",v);
		_Mouth=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Neck=null;

	/**
	 * @return Returns the neck.
	 */
	public Boolean getNeck() {
		try{
			if (_Neck==null){
				_Neck=getBooleanProperty("neck");
				return _Neck;
			}else {
				return _Neck;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for neck.
	 * @param v Value to Set.
	 */
	public void setNeck(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/neck",v);
		_Neck=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Shoulders=null;

	/**
	 * @return Returns the shoulders.
	 */
	public Boolean getShoulders() {
		try{
			if (_Shoulders==null){
				_Shoulders=getBooleanProperty("shoulders");
				return _Shoulders;
			}else {
				return _Shoulders;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for shoulders.
	 * @param v Value to Set.
	 */
	public void setShoulders(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/shoulders",v);
		_Shoulders=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Arms=null;

	/**
	 * @return Returns the arms.
	 */
	public Boolean getArms() {
		try{
			if (_Arms==null){
				_Arms=getBooleanProperty("arms");
				return _Arms;
			}else {
				return _Arms;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for arms.
	 * @param v Value to Set.
	 */
	public void setArms(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/arms",v);
		_Arms=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Hands=null;

	/**
	 * @return Returns the hands.
	 */
	public Boolean getHands() {
		try{
			if (_Hands==null){
				_Hands=getBooleanProperty("hands");
				return _Hands;
			}else {
				return _Hands;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for hands.
	 * @param v Value to Set.
	 */
	public void setHands(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/hands",v);
		_Hands=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Trunk=null;

	/**
	 * @return Returns the trunk.
	 */
	public Boolean getTrunk() {
		try{
			if (_Trunk==null){
				_Trunk=getBooleanProperty("trunk");
				return _Trunk;
			}else {
				return _Trunk;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for trunk.
	 * @param v Value to Set.
	 */
	public void setTrunk(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/trunk",v);
		_Trunk=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Pelvis=null;

	/**
	 * @return Returns the pelvis.
	 */
	public Boolean getPelvis() {
		try{
			if (_Pelvis==null){
				_Pelvis=getBooleanProperty("pelvis");
				return _Pelvis;
			}else {
				return _Pelvis;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for pelvis.
	 * @param v Value to Set.
	 */
	public void setPelvis(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/pelvis",v);
		_Pelvis=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Legs=null;

	/**
	 * @return Returns the legs.
	 */
	public Boolean getLegs() {
		try{
			if (_Legs==null){
				_Legs=getBooleanProperty("legs");
				return _Legs;
			}else {
				return _Legs;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for legs.
	 * @param v Value to Set.
	 */
	public void setLegs(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/legs",v);
		_Legs=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Feet=null;

	/**
	 * @return Returns the feet.
	 */
	public Boolean getFeet() {
		try{
			if (_Feet==null){
				_Feet=getBooleanProperty("feet");
				return _Feet;
			}else {
				return _Feet;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for feet.
	 * @param v Value to Set.
	 */
	public void setFeet(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/feet",v);
		_Feet=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Eyecount=null;

	/**
	 * @return Returns the eyeCount.
	 */
	public Integer getEyecount() {
		try{
			if (_Eyecount==null){
				_Eyecount=getIntegerProperty("eyeCount");
				return _Eyecount;
			}else {
				return _Eyecount;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for eyeCount.
	 * @param v Value to Set.
	 */
	public void setEyecount(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/eyeCount",v);
		_Eyecount=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Fnscount=null;

	/**
	 * @return Returns the fnsCount.
	 */
	public Integer getFnscount() {
		try{
			if (_Fnscount==null){
				_Fnscount=getIntegerProperty("fnsCount");
				return _Fnscount;
			}else {
				return _Fnscount;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for fnsCount.
	 * @param v Value to Set.
	 */
	public void setFnscount(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/fnsCount",v);
		_Fnscount=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Vocalizations=null;

	/**
	 * @return Returns the vocalizations.
	 */
	public Integer getVocalizations() {
		try{
			if (_Vocalizations==null){
				_Vocalizations=getIntegerProperty("vocalizations");
				return _Vocalizations;
			}else {
				return _Vocalizations;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for vocalizations.
	 * @param v Value to Set.
	 */
	public void setVocalizations(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/vocalizations",v);
		_Vocalizations=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Bodycount=null;

	/**
	 * @return Returns the bodyCount.
	 */
	public Integer getBodycount() {
		try{
			if (_Bodycount==null){
				_Bodycount=getIntegerProperty("bodyCount");
				return _Bodycount;
			}else {
				return _Bodycount;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for bodyCount.
	 * @param v Value to Set.
	 */
	public void setBodycount(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/bodyCount",v);
		_Bodycount=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Worstmotorseverity=null;

	/**
	 * @return Returns the worstMotorSeverity.
	 */
	public Integer getWorstmotorseverity() {
		try{
			if (_Worstmotorseverity==null){
				_Worstmotorseverity=getIntegerProperty("worstMotorSeverity");
				return _Worstmotorseverity;
			}else {
				return _Worstmotorseverity;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for worstMotorSeverity.
	 * @param v Value to Set.
	 */
	public void setWorstmotorseverity(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/worstMotorSeverity",v);
		_Worstmotorseverity=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Worstvocalseverity=null;

	/**
	 * @return Returns the worstVocalSeverity.
	 */
	public Integer getWorstvocalseverity() {
		try{
			if (_Worstvocalseverity==null){
				_Worstvocalseverity=getIntegerProperty("worstVocalSeverity");
				return _Worstvocalseverity;
			}else {
				return _Worstvocalseverity;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for worstVocalSeverity.
	 * @param v Value to Set.
	 */
	public void setWorstvocalseverity(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/worstVocalSeverity",v);
		_Worstvocalseverity=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaTicvideocountsdata> getAllCndaTicvideocountsdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaTicvideocountsdata> al = new ArrayList<org.nrg.xdat.om.CndaTicvideocountsdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaTicvideocountsdata> getCndaTicvideocountsdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaTicvideocountsdata> al = new ArrayList<org.nrg.xdat.om.CndaTicvideocountsdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaTicvideocountsdata> getCndaTicvideocountsdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaTicvideocountsdata> al = new ArrayList<org.nrg.xdat.om.CndaTicvideocountsdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaTicvideocountsdata getCndaTicvideocountsdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:ticVideoCountsData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaTicvideocountsdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
