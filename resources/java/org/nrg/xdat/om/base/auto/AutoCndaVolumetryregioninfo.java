/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaVolumetryregioninfo extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.CndaVolumetryregioninfoI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaVolumetryregioninfo.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:volumetryRegionInfo";

	public AutoCndaVolumetryregioninfo(ItemI item)
	{
		super(item);
	}

	public AutoCndaVolumetryregioninfo(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaVolumetryregioninfo(UserI user)
	 **/
	public AutoCndaVolumetryregioninfo(){}

	public AutoCndaVolumetryregioninfo(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:volumetryRegionInfo";
	}

	//FIELD

	private String _Regions_region_imageType=null;

	/**
	 * @return Returns the regions/region/image_type.
	 */
	public String getRegions_region_imageType(){
		try{
			if (_Regions_region_imageType==null){
				_Regions_region_imageType=getStringProperty("regions/region/image_type");
				return _Regions_region_imageType;
			}else {
				return _Regions_region_imageType;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for regions/region/image_type.
	 * @param v Value to Set.
	 */
	public void setRegions_region_imageType(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/regions/region/image_type",v);
		_Regions_region_imageType=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Regions_region_sliceDist=null;

	/**
	 * @return Returns the regions/region/slice_dist.
	 */
	public Double getRegions_region_sliceDist() {
		try{
			if (_Regions_region_sliceDist==null){
				_Regions_region_sliceDist=getDoubleProperty("regions/region/slice_dist");
				return _Regions_region_sliceDist;
			}else {
				return _Regions_region_sliceDist;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for regions/region/slice_dist.
	 * @param v Value to Set.
	 */
	public void setRegions_region_sliceDist(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/regions/region/slice_dist",v);
		_Regions_region_sliceDist=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Regions_region_orientation=null;

	/**
	 * @return Returns the regions/region/orientation.
	 */
	public String getRegions_region_orientation(){
		try{
			if (_Regions_region_orientation==null){
				_Regions_region_orientation=getStringProperty("regions/region/orientation");
				return _Regions_region_orientation;
			}else {
				return _Regions_region_orientation;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for regions/region/orientation.
	 * @param v Value to Set.
	 */
	public void setRegions_region_orientation(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/regions/region/orientation",v);
		_Regions_region_orientation=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Regions_region_voxelRes_x=null;

	/**
	 * @return Returns the regions/region/voxel_res/x.
	 */
	public Double getRegions_region_voxelRes_x() {
		try{
			if (_Regions_region_voxelRes_x==null){
				_Regions_region_voxelRes_x=getDoubleProperty("regions/region/voxel_res/x");
				return _Regions_region_voxelRes_x;
			}else {
				return _Regions_region_voxelRes_x;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for regions/region/voxel_res/x.
	 * @param v Value to Set.
	 */
	public void setRegions_region_voxelRes_x(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/regions/region/voxel_res/x",v);
		_Regions_region_voxelRes_x=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Regions_region_voxelRes_y=null;

	/**
	 * @return Returns the regions/region/voxel_res/y.
	 */
	public Double getRegions_region_voxelRes_y() {
		try{
			if (_Regions_region_voxelRes_y==null){
				_Regions_region_voxelRes_y=getDoubleProperty("regions/region/voxel_res/y");
				return _Regions_region_voxelRes_y;
			}else {
				return _Regions_region_voxelRes_y;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for regions/region/voxel_res/y.
	 * @param v Value to Set.
	 */
	public void setRegions_region_voxelRes_y(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/regions/region/voxel_res/y",v);
		_Regions_region_voxelRes_y=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Regions_region_voxelRes_thickness=null;

	/**
	 * @return Returns the regions/region/voxel_res/thickness.
	 */
	public Double getRegions_region_voxelRes_thickness() {
		try{
			if (_Regions_region_voxelRes_thickness==null){
				_Regions_region_voxelRes_thickness=getDoubleProperty("regions/region/voxel_res/thickness");
				return _Regions_region_voxelRes_thickness;
			}else {
				return _Regions_region_voxelRes_thickness;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for regions/region/voxel_res/thickness.
	 * @param v Value to Set.
	 */
	public void setRegions_region_voxelRes_thickness(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/regions/region/voxel_res/thickness",v);
		_Regions_region_voxelRes_thickness=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Regions_region_note=null;

	/**
	 * @return Returns the regions/region/note.
	 */
	public String getRegions_region_note(){
		try{
			if (_Regions_region_note==null){
				_Regions_region_note=getStringProperty("regions/region/note");
				return _Regions_region_note;
			}else {
				return _Regions_region_note;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for regions/region/note.
	 * @param v Value to Set.
	 */
	public void setRegions_region_note(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/regions/region/note",v);
		_Regions_region_note=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Regions_region_id=null;

	/**
	 * @return Returns the regions/region/id.
	 */
	public String getRegions_region_id(){
		try{
			if (_Regions_region_id==null){
				_Regions_region_id=getStringProperty("regions/region/id");
				return _Regions_region_id;
			}else {
				return _Regions_region_id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for regions/region/id.
	 * @param v Value to Set.
	 */
	public void setRegions_region_id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/regions/region/id",v);
		_Regions_region_id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Regions_region_name=null;

	/**
	 * @return Returns the regions/region/name.
	 */
	public String getRegions_region_name(){
		try{
			if (_Regions_region_name==null){
				_Regions_region_name=getStringProperty("regions/region/name");
				return _Regions_region_name;
			}else {
				return _Regions_region_name;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for regions/region/name.
	 * @param v Value to Set.
	 */
	public void setRegions_region_name(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/regions/region/name",v);
		_Regions_region_name=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CndaVolumetryregioninfoId=null;

	/**
	 * @return Returns the cnda_volumetryRegionInfo_id.
	 */
	public Integer getCndaVolumetryregioninfoId() {
		try{
			if (_CndaVolumetryregioninfoId==null){
				_CndaVolumetryregioninfoId=getIntegerProperty("cnda_volumetryRegionInfo_id");
				return _CndaVolumetryregioninfoId;
			}else {
				return _CndaVolumetryregioninfoId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda_volumetryRegionInfo_id.
	 * @param v Value to Set.
	 */
	public void setCndaVolumetryregioninfoId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/cnda_volumetryRegionInfo_id",v);
		_CndaVolumetryregioninfoId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaVolumetryregioninfo> getAllCndaVolumetryregioninfos(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaVolumetryregioninfo> al = new ArrayList<org.nrg.xdat.om.CndaVolumetryregioninfo>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaVolumetryregioninfo> getCndaVolumetryregioninfosByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaVolumetryregioninfo> al = new ArrayList<org.nrg.xdat.om.CndaVolumetryregioninfo>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaVolumetryregioninfo> getCndaVolumetryregioninfosByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaVolumetryregioninfo> al = new ArrayList<org.nrg.xdat.om.CndaVolumetryregioninfo>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaVolumetryregioninfo getCndaVolumetryregioninfosByCndaVolumetryregioninfoId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:volumetryRegionInfo/cnda_volumetryRegionInfo_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaVolumetryregioninfo) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
