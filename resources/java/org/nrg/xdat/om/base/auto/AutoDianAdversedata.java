/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:00 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianAdversedata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianAdversedataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianAdversedata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:adverseData";

	public AutoDianAdversedata(ItemI item)
	{
		super(item);
	}

	public AutoDianAdversedata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianAdversedata(UserI user)
	 **/
	public AutoDianAdversedata(){}

	public AutoDianAdversedata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:adverseData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.DianAdversedataAdverse> _Adverselist_adverse =null;

	/**
	 * adverseList/adverse
	 * @return Returns an List of org.nrg.xdat.om.DianAdversedataAdverse
	 */
	public <A extends org.nrg.xdat.model.DianAdversedataAdverseI> List<A> getAdverselist_adverse() {
		try{
			if (_Adverselist_adverse==null){
				_Adverselist_adverse=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("adverseList/adverse"));
			}
			return (List<A>) _Adverselist_adverse;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.DianAdversedataAdverse>();}
	}

	/**
	 * Sets the value for adverseList/adverse.
	 * @param v Value to Set.
	 */
	public void setAdverselist_adverse(ItemI v) throws Exception{
		_Adverselist_adverse =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/adverseList/adverse",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/adverseList/adverse",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * adverseList/adverse
	 * Adds org.nrg.xdat.model.DianAdversedataAdverseI
	 */
	public <A extends org.nrg.xdat.model.DianAdversedataAdverseI> void addAdverselist_adverse(A item) throws Exception{
	setAdverselist_adverse((ItemI)item);
	}

	/**
	 * Removes the adverseList/adverse of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeAdverselist_adverse(int index) throws java.lang.IndexOutOfBoundsException {
		_Adverselist_adverse =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/adverseList/adverse",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianAdversedata> getAllDianAdversedatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianAdversedata> al = new ArrayList<org.nrg.xdat.om.DianAdversedata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianAdversedata> getDianAdversedatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianAdversedata> al = new ArrayList<org.nrg.xdat.om.DianAdversedata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianAdversedata> getDianAdversedatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianAdversedata> al = new ArrayList<org.nrg.xdat.om.DianAdversedata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianAdversedata getDianAdversedatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:adverseData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianAdversedata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //adverseList/adverse
	        for(org.nrg.xdat.model.DianAdversedataAdverseI childAdverselist_adverse : this.getAdverselist_adverse()){
	            if (childAdverselist_adverse!=null){
	              for(ResourceFile rf: ((DianAdversedataAdverse)childAdverselist_adverse).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("adverseList/adverse[" + ((DianAdversedataAdverse)childAdverselist_adverse).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("adverseList/adverse/" + ((DianAdversedataAdverse)childAdverselist_adverse).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
