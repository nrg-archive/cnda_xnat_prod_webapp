/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCondrBraindata extends TissueSpecdata implements org.nrg.xdat.model.CondrBraindataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCondrBraindata.class);
	public static String SCHEMA_ELEMENT_NAME="condr:brainData";

	public AutoCondrBraindata(ItemI item)
	{
		super(item);
	}

	public AutoCondrBraindata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCondrBraindata(UserI user)
	 **/
	public AutoCondrBraindata(){}

	public AutoCondrBraindata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "condr:brainData";
	}
	 private org.nrg.xdat.om.TissueSpecdata _Specdata =null;

	/**
	 * specData
	 * @return org.nrg.xdat.om.TissueSpecdata
	 */
	public org.nrg.xdat.om.TissueSpecdata getSpecdata() {
		try{
			if (_Specdata==null){
				_Specdata=((TissueSpecdata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("specData")));
				return _Specdata;
			}else {
				return _Specdata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for specData.
	 * @param v Value to Set.
	 */
	public void setSpecdata(ItemI v) throws Exception{
		_Specdata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/specData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/specData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * specData
	 * set org.nrg.xdat.model.TissueSpecdataI
	 */
	public <A extends org.nrg.xdat.model.TissueSpecdataI> void setSpecdata(A item) throws Exception{
	setSpecdata((ItemI)item);
	}

	/**
	 * Removes the specData.
	 * */
	public void removeSpecdata() {
		_Specdata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/specData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Descloc=null;

	/**
	 * @return Returns the descLoc.
	 */
	public String getDescloc(){
		try{
			if (_Descloc==null){
				_Descloc=getStringProperty("descLoc");
				return _Descloc;
			}else {
				return _Descloc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for descLoc.
	 * @param v Value to Set.
	 */
	public void setDescloc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/descLoc",v);
		_Descloc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Coordx=null;

	/**
	 * @return Returns the coordX.
	 */
	public Double getCoordx() {
		try{
			if (_Coordx==null){
				_Coordx=getDoubleProperty("coordX");
				return _Coordx;
			}else {
				return _Coordx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for coordX.
	 * @param v Value to Set.
	 */
	public void setCoordx(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/coordX",v);
		_Coordx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Coordy=null;

	/**
	 * @return Returns the coordY.
	 */
	public Double getCoordy() {
		try{
			if (_Coordy==null){
				_Coordy=getDoubleProperty("coordY");
				return _Coordy;
			}else {
				return _Coordy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for coordY.
	 * @param v Value to Set.
	 */
	public void setCoordy(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/coordY",v);
		_Coordy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Coordz=null;

	/**
	 * @return Returns the coordZ.
	 */
	public Double getCoordz() {
		try{
			if (_Coordz==null){
				_Coordz=getDoubleProperty("coordZ");
				return _Coordz;
			}else {
				return _Coordz;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for coordZ.
	 * @param v Value to Set.
	 */
	public void setCoordz(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/coordZ",v);
		_Coordz=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Coordsource=null;

	/**
	 * @return Returns the coordSource.
	 */
	public String getCoordsource(){
		try{
			if (_Coordsource==null){
				_Coordsource=getStringProperty("coordSource");
				return _Coordsource;
			}else {
				return _Coordsource;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for coordSource.
	 * @param v Value to Set.
	 */
	public void setCoordsource(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/coordSource",v);
		_Coordsource=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Sampsplit=null;

	/**
	 * @return Returns the sampSplit.
	 */
	public Boolean getSampsplit() {
		try{
			if (_Sampsplit==null){
				_Sampsplit=getBooleanProperty("sampSplit");
				return _Sampsplit;
			}else {
				return _Sampsplit;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sampSplit.
	 * @param v Value to Set.
	 */
	public void setSampsplit(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/sampSplit",v);
		_Sampsplit=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Surgpath=null;

	/**
	 * @return Returns the surgPath.
	 */
	public Boolean getSurgpath() {
		try{
			if (_Surgpath==null){
				_Surgpath=getBooleanProperty("surgPath");
				return _Surgpath;
			}else {
				return _Surgpath;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for surgPath.
	 * @param v Value to Set.
	 */
	public void setSurgpath(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/surgPath",v);
		_Surgpath=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Tumorbank=null;

	/**
	 * @return Returns the tumorBank.
	 */
	public Boolean getTumorbank() {
		try{
			if (_Tumorbank==null){
				_Tumorbank=getBooleanProperty("tumorBank");
				return _Tumorbank;
			}else {
				return _Tumorbank;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tumorBank.
	 * @param v Value to Set.
	 */
	public void setTumorbank(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/tumorBank",v);
		_Tumorbank=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Surgerytimenotreported=null;

	/**
	 * @return Returns the surgeryTimeNotReported.
	 */
	public Boolean getSurgerytimenotreported() {
		try{
			if (_Surgerytimenotreported==null){
				_Surgerytimenotreported=getBooleanProperty("surgeryTimeNotReported");
				return _Surgerytimenotreported;
			}else {
				return _Surgerytimenotreported;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for surgeryTimeNotReported.
	 * @param v Value to Set.
	 */
	public void setSurgerytimenotreported(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/surgeryTimeNotReported",v);
		_Surgerytimenotreported=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Coordcoll=null;

	/**
	 * @return Returns the coordColl.
	 */
	public Boolean getCoordcoll() {
		try{
			if (_Coordcoll==null){
				_Coordcoll=getBooleanProperty("coordColl");
				return _Coordcoll;
			}else {
				return _Coordcoll;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for coordColl.
	 * @param v Value to Set.
	 */
	public void setCoordcoll(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/coordColl",v);
		_Coordcoll=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Screencapcoll=null;

	/**
	 * @return Returns the screenCapColl.
	 */
	public Boolean getScreencapcoll() {
		try{
			if (_Screencapcoll==null){
				_Screencapcoll=getBooleanProperty("screenCapColl");
				return _Screencapcoll;
			}else {
				return _Screencapcoll;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for screenCapColl.
	 * @param v Value to Set.
	 */
	public void setScreencapcoll(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/screenCapColl",v);
		_Screencapcoll=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rgraphapp=null;

	/**
	 * @return Returns the rgraphApp.
	 */
	public String getRgraphapp(){
		try{
			if (_Rgraphapp==null){
				_Rgraphapp=getStringProperty("rgraphApp");
				return _Rgraphapp;
			}else {
				return _Rgraphapp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for rgraphApp.
	 * @param v Value to Set.
	 */
	public void setRgraphapp(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/rgraphApp",v);
		_Rgraphapp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rgraphappnote=null;

	/**
	 * @return Returns the rgraphAppNote.
	 */
	public String getRgraphappnote(){
		try{
			if (_Rgraphappnote==null){
				_Rgraphappnote=getStringProperty("rgraphAppNote");
				return _Rgraphappnote;
			}else {
				return _Rgraphappnote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for rgraphAppNote.
	 * @param v Value to Set.
	 */
	public void setRgraphappnote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/rgraphAppNote",v);
		_Rgraphappnote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CondrBraindata> getAllCondrBraindatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrBraindata> al = new ArrayList<org.nrg.xdat.om.CondrBraindata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrBraindata> getCondrBraindatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrBraindata> al = new ArrayList<org.nrg.xdat.om.CondrBraindata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrBraindata> getCondrBraindatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrBraindata> al = new ArrayList<org.nrg.xdat.om.CondrBraindata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CondrBraindata getCondrBraindatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("condr:brainData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CondrBraindata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //specData
	        TissueSpecdata childSpecdata = (TissueSpecdata)this.getSpecdata();
	            if (childSpecdata!=null){
	              for(ResourceFile rf: ((TissueSpecdata)childSpecdata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("specData[" + ((TissueSpecdata)childSpecdata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("specData/" + ((TissueSpecdata)childSpecdata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
