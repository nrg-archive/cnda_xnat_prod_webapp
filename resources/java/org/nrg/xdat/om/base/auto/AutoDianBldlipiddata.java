/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:01 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianBldlipiddata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianBldlipiddataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianBldlipiddata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:bldLipidData";

	public AutoDianBldlipiddata(ItemI item)
	{
		super(item);
	}

	public AutoDianBldlipiddata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianBldlipiddata(UserI user)
	 **/
	public AutoDianBldlipiddata(){}

	public AutoDianBldlipiddata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:bldLipidData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Bldcollsess=null;

	/**
	 * @return Returns the bldCollSess.
	 */
	public String getBldcollsess(){
		try{
			if (_Bldcollsess==null){
				_Bldcollsess=getStringProperty("bldCollSess");
				return _Bldcollsess;
			}else {
				return _Bldcollsess;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for bldCollSess.
	 * @param v Value to Set.
	 */
	public void setBldcollsess(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/bldCollSess",v);
		_Bldcollsess=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Triglyc=null;

	/**
	 * @return Returns the Triglyc.
	 */
	public Integer getTriglyc() {
		try{
			if (_Triglyc==null){
				_Triglyc=getIntegerProperty("Triglyc");
				return _Triglyc;
			}else {
				return _Triglyc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Triglyc.
	 * @param v Value to Set.
	 */
	public void setTriglyc(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Triglyc",v);
		_Triglyc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Triglycunits=null;

	/**
	 * @return Returns the TriglycUnits.
	 */
	public String getTriglycunits(){
		try{
			if (_Triglycunits==null){
				_Triglycunits=getStringProperty("TriglycUnits");
				return _Triglycunits;
			}else {
				return _Triglycunits;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TriglycUnits.
	 * @param v Value to Set.
	 */
	public void setTriglycunits(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TriglycUnits",v);
		_Triglycunits=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Totcholes=null;

	/**
	 * @return Returns the TotCholes.
	 */
	public Integer getTotcholes() {
		try{
			if (_Totcholes==null){
				_Totcholes=getIntegerProperty("TotCholes");
				return _Totcholes;
			}else {
				return _Totcholes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TotCholes.
	 * @param v Value to Set.
	 */
	public void setTotcholes(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TotCholes",v);
		_Totcholes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Totcholesunits=null;

	/**
	 * @return Returns the TotCholesUnits.
	 */
	public String getTotcholesunits(){
		try{
			if (_Totcholesunits==null){
				_Totcholesunits=getStringProperty("TotCholesUnits");
				return _Totcholesunits;
			}else {
				return _Totcholesunits;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TotCholesUnits.
	 * @param v Value to Set.
	 */
	public void setTotcholesunits(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TotCholesUnits",v);
		_Totcholesunits=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Hdl=null;

	/**
	 * @return Returns the HDL.
	 */
	public Integer getHdl() {
		try{
			if (_Hdl==null){
				_Hdl=getIntegerProperty("HDL");
				return _Hdl;
			}else {
				return _Hdl;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HDL.
	 * @param v Value to Set.
	 */
	public void setHdl(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HDL",v);
		_Hdl=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Hdlunits=null;

	/**
	 * @return Returns the HDLUnits.
	 */
	public String getHdlunits(){
		try{
			if (_Hdlunits==null){
				_Hdlunits=getStringProperty("HDLUnits");
				return _Hdlunits;
			}else {
				return _Hdlunits;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HDLUnits.
	 * @param v Value to Set.
	 */
	public void setHdlunits(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HDLUnits",v);
		_Hdlunits=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ldl=null;

	/**
	 * @return Returns the LDL.
	 */
	public Integer getLdl() {
		try{
			if (_Ldl==null){
				_Ldl=getIntegerProperty("LDL");
				return _Ldl;
			}else {
				return _Ldl;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LDL.
	 * @param v Value to Set.
	 */
	public void setLdl(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LDL",v);
		_Ldl=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Ldlunits=null;

	/**
	 * @return Returns the LDLUnits.
	 */
	public String getLdlunits(){
		try{
			if (_Ldlunits==null){
				_Ldlunits=getStringProperty("LDLUnits");
				return _Ldlunits;
			}else {
				return _Ldlunits;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LDLUnits.
	 * @param v Value to Set.
	 */
	public void setLdlunits(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LDLUnits",v);
		_Ldlunits=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianBldlipiddata> getAllDianBldlipiddatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianBldlipiddata> al = new ArrayList<org.nrg.xdat.om.DianBldlipiddata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianBldlipiddata> getDianBldlipiddatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianBldlipiddata> al = new ArrayList<org.nrg.xdat.om.DianBldlipiddata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianBldlipiddata> getDianBldlipiddatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianBldlipiddata> al = new ArrayList<org.nrg.xdat.om.DianBldlipiddata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianBldlipiddata getDianBldlipiddatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:bldLipidData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianBldlipiddata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
