/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCbatCvoe extends XnatSubjectassessordata implements org.nrg.xdat.model.CbatCvoeI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCbatCvoe.class);
	public static String SCHEMA_ELEMENT_NAME="cbat:CVOE";

	public AutoCbatCvoe(ItemI item)
	{
		super(item);
	}

	public AutoCbatCvoe(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCbatCvoe(UserI user)
	 **/
	public AutoCbatCvoe(){}

	public AutoCbatCvoe(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cbat:CVOE";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt1_accuracy=null;

	/**
	 * @return Returns the CVT1/accuracy.
	 */
	public Integer getCvt1_accuracy() {
		try{
			if (_Cvt1_accuracy==null){
				_Cvt1_accuracy=getIntegerProperty("CVT1/accuracy");
				return _Cvt1_accuracy;
			}else {
				return _Cvt1_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT1/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt1_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT1/accuracy",v);
		_Cvt1_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt1_responsetime=null;

	/**
	 * @return Returns the CVT1/responseTime.
	 */
	public Integer getCvt1_responsetime() {
		try{
			if (_Cvt1_responsetime==null){
				_Cvt1_responsetime=getIntegerProperty("CVT1/responseTime");
				return _Cvt1_responsetime;
			}else {
				return _Cvt1_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT1/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt1_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT1/responseTime",v);
		_Cvt1_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt2_accuracy=null;

	/**
	 * @return Returns the CVT2/accuracy.
	 */
	public Integer getCvt2_accuracy() {
		try{
			if (_Cvt2_accuracy==null){
				_Cvt2_accuracy=getIntegerProperty("CVT2/accuracy");
				return _Cvt2_accuracy;
			}else {
				return _Cvt2_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT2/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt2_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT2/accuracy",v);
		_Cvt2_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt2_responsetime=null;

	/**
	 * @return Returns the CVT2/responseTime.
	 */
	public Integer getCvt2_responsetime() {
		try{
			if (_Cvt2_responsetime==null){
				_Cvt2_responsetime=getIntegerProperty("CVT2/responseTime");
				return _Cvt2_responsetime;
			}else {
				return _Cvt2_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT2/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt2_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT2/responseTime",v);
		_Cvt2_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt3_accuracy=null;

	/**
	 * @return Returns the CVT3/accuracy.
	 */
	public Integer getCvt3_accuracy() {
		try{
			if (_Cvt3_accuracy==null){
				_Cvt3_accuracy=getIntegerProperty("CVT3/accuracy");
				return _Cvt3_accuracy;
			}else {
				return _Cvt3_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT3/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt3_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT3/accuracy",v);
		_Cvt3_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt3_responsetime=null;

	/**
	 * @return Returns the CVT3/responseTime.
	 */
	public Integer getCvt3_responsetime() {
		try{
			if (_Cvt3_responsetime==null){
				_Cvt3_responsetime=getIntegerProperty("CVT3/responseTime");
				return _Cvt3_responsetime;
			}else {
				return _Cvt3_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT3/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt3_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT3/responseTime",v);
		_Cvt3_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt4_accuracy=null;

	/**
	 * @return Returns the CVT4/accuracy.
	 */
	public Integer getCvt4_accuracy() {
		try{
			if (_Cvt4_accuracy==null){
				_Cvt4_accuracy=getIntegerProperty("CVT4/accuracy");
				return _Cvt4_accuracy;
			}else {
				return _Cvt4_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT4/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt4_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT4/accuracy",v);
		_Cvt4_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt4_responsetime=null;

	/**
	 * @return Returns the CVT4/responseTime.
	 */
	public Integer getCvt4_responsetime() {
		try{
			if (_Cvt4_responsetime==null){
				_Cvt4_responsetime=getIntegerProperty("CVT4/responseTime");
				return _Cvt4_responsetime;
			}else {
				return _Cvt4_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT4/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt4_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT4/responseTime",v);
		_Cvt4_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt5_accuracy=null;

	/**
	 * @return Returns the CVT5/accuracy.
	 */
	public Integer getCvt5_accuracy() {
		try{
			if (_Cvt5_accuracy==null){
				_Cvt5_accuracy=getIntegerProperty("CVT5/accuracy");
				return _Cvt5_accuracy;
			}else {
				return _Cvt5_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT5/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt5_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT5/accuracy",v);
		_Cvt5_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt5_responsetime=null;

	/**
	 * @return Returns the CVT5/responseTime.
	 */
	public Integer getCvt5_responsetime() {
		try{
			if (_Cvt5_responsetime==null){
				_Cvt5_responsetime=getIntegerProperty("CVT5/responseTime");
				return _Cvt5_responsetime;
			}else {
				return _Cvt5_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT5/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt5_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT5/responseTime",v);
		_Cvt5_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt6_accuracy=null;

	/**
	 * @return Returns the CVT6/accuracy.
	 */
	public Integer getCvt6_accuracy() {
		try{
			if (_Cvt6_accuracy==null){
				_Cvt6_accuracy=getIntegerProperty("CVT6/accuracy");
				return _Cvt6_accuracy;
			}else {
				return _Cvt6_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT6/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt6_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT6/accuracy",v);
		_Cvt6_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt6_responsetime=null;

	/**
	 * @return Returns the CVT6/responseTime.
	 */
	public Integer getCvt6_responsetime() {
		try{
			if (_Cvt6_responsetime==null){
				_Cvt6_responsetime=getIntegerProperty("CVT6/responseTime");
				return _Cvt6_responsetime;
			}else {
				return _Cvt6_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT6/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt6_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT6/responseTime",v);
		_Cvt6_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt7_accuracy=null;

	/**
	 * @return Returns the CVT7/accuracy.
	 */
	public Integer getCvt7_accuracy() {
		try{
			if (_Cvt7_accuracy==null){
				_Cvt7_accuracy=getIntegerProperty("CVT7/accuracy");
				return _Cvt7_accuracy;
			}else {
				return _Cvt7_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT7/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt7_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT7/accuracy",v);
		_Cvt7_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt7_responsetime=null;

	/**
	 * @return Returns the CVT7/responseTime.
	 */
	public Integer getCvt7_responsetime() {
		try{
			if (_Cvt7_responsetime==null){
				_Cvt7_responsetime=getIntegerProperty("CVT7/responseTime");
				return _Cvt7_responsetime;
			}else {
				return _Cvt7_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT7/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt7_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT7/responseTime",v);
		_Cvt7_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt8_accuracy=null;

	/**
	 * @return Returns the CVT8/accuracy.
	 */
	public Integer getCvt8_accuracy() {
		try{
			if (_Cvt8_accuracy==null){
				_Cvt8_accuracy=getIntegerProperty("CVT8/accuracy");
				return _Cvt8_accuracy;
			}else {
				return _Cvt8_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT8/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt8_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT8/accuracy",v);
		_Cvt8_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt8_responsetime=null;

	/**
	 * @return Returns the CVT8/responseTime.
	 */
	public Integer getCvt8_responsetime() {
		try{
			if (_Cvt8_responsetime==null){
				_Cvt8_responsetime=getIntegerProperty("CVT8/responseTime");
				return _Cvt8_responsetime;
			}else {
				return _Cvt8_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT8/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt8_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT8/responseTime",v);
		_Cvt8_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt9_accuracy=null;

	/**
	 * @return Returns the CVT9/accuracy.
	 */
	public Integer getCvt9_accuracy() {
		try{
			if (_Cvt9_accuracy==null){
				_Cvt9_accuracy=getIntegerProperty("CVT9/accuracy");
				return _Cvt9_accuracy;
			}else {
				return _Cvt9_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT9/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt9_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT9/accuracy",v);
		_Cvt9_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt9_responsetime=null;

	/**
	 * @return Returns the CVT9/responseTime.
	 */
	public Integer getCvt9_responsetime() {
		try{
			if (_Cvt9_responsetime==null){
				_Cvt9_responsetime=getIntegerProperty("CVT9/responseTime");
				return _Cvt9_responsetime;
			}else {
				return _Cvt9_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT9/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt9_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT9/responseTime",v);
		_Cvt9_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt10_accuracy=null;

	/**
	 * @return Returns the CVT10/accuracy.
	 */
	public Integer getCvt10_accuracy() {
		try{
			if (_Cvt10_accuracy==null){
				_Cvt10_accuracy=getIntegerProperty("CVT10/accuracy");
				return _Cvt10_accuracy;
			}else {
				return _Cvt10_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT10/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt10_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT10/accuracy",v);
		_Cvt10_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt10_responsetime=null;

	/**
	 * @return Returns the CVT10/responseTime.
	 */
	public Integer getCvt10_responsetime() {
		try{
			if (_Cvt10_responsetime==null){
				_Cvt10_responsetime=getIntegerProperty("CVT10/responseTime");
				return _Cvt10_responsetime;
			}else {
				return _Cvt10_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT10/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt10_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT10/responseTime",v);
		_Cvt10_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt11_accuracy=null;

	/**
	 * @return Returns the CVT11/accuracy.
	 */
	public Integer getCvt11_accuracy() {
		try{
			if (_Cvt11_accuracy==null){
				_Cvt11_accuracy=getIntegerProperty("CVT11/accuracy");
				return _Cvt11_accuracy;
			}else {
				return _Cvt11_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT11/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt11_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT11/accuracy",v);
		_Cvt11_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt11_responsetime=null;

	/**
	 * @return Returns the CVT11/responseTime.
	 */
	public Integer getCvt11_responsetime() {
		try{
			if (_Cvt11_responsetime==null){
				_Cvt11_responsetime=getIntegerProperty("CVT11/responseTime");
				return _Cvt11_responsetime;
			}else {
				return _Cvt11_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT11/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt11_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT11/responseTime",v);
		_Cvt11_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt12_accuracy=null;

	/**
	 * @return Returns the CVT12/accuracy.
	 */
	public Integer getCvt12_accuracy() {
		try{
			if (_Cvt12_accuracy==null){
				_Cvt12_accuracy=getIntegerProperty("CVT12/accuracy");
				return _Cvt12_accuracy;
			}else {
				return _Cvt12_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT12/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt12_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT12/accuracy",v);
		_Cvt12_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt12_responsetime=null;

	/**
	 * @return Returns the CVT12/responseTime.
	 */
	public Integer getCvt12_responsetime() {
		try{
			if (_Cvt12_responsetime==null){
				_Cvt12_responsetime=getIntegerProperty("CVT12/responseTime");
				return _Cvt12_responsetime;
			}else {
				return _Cvt12_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT12/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt12_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT12/responseTime",v);
		_Cvt12_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt13_accuracy=null;

	/**
	 * @return Returns the CVT13/accuracy.
	 */
	public Integer getCvt13_accuracy() {
		try{
			if (_Cvt13_accuracy==null){
				_Cvt13_accuracy=getIntegerProperty("CVT13/accuracy");
				return _Cvt13_accuracy;
			}else {
				return _Cvt13_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT13/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt13_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT13/accuracy",v);
		_Cvt13_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt13_responsetime=null;

	/**
	 * @return Returns the CVT13/responseTime.
	 */
	public Integer getCvt13_responsetime() {
		try{
			if (_Cvt13_responsetime==null){
				_Cvt13_responsetime=getIntegerProperty("CVT13/responseTime");
				return _Cvt13_responsetime;
			}else {
				return _Cvt13_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT13/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt13_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT13/responseTime",v);
		_Cvt13_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt14_accuracy=null;

	/**
	 * @return Returns the CVT14/accuracy.
	 */
	public Integer getCvt14_accuracy() {
		try{
			if (_Cvt14_accuracy==null){
				_Cvt14_accuracy=getIntegerProperty("CVT14/accuracy");
				return _Cvt14_accuracy;
			}else {
				return _Cvt14_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT14/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt14_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT14/accuracy",v);
		_Cvt14_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt14_responsetime=null;

	/**
	 * @return Returns the CVT14/responseTime.
	 */
	public Integer getCvt14_responsetime() {
		try{
			if (_Cvt14_responsetime==null){
				_Cvt14_responsetime=getIntegerProperty("CVT14/responseTime");
				return _Cvt14_responsetime;
			}else {
				return _Cvt14_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT14/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt14_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT14/responseTime",v);
		_Cvt14_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt15_accuracy=null;

	/**
	 * @return Returns the CVT15/accuracy.
	 */
	public Integer getCvt15_accuracy() {
		try{
			if (_Cvt15_accuracy==null){
				_Cvt15_accuracy=getIntegerProperty("CVT15/accuracy");
				return _Cvt15_accuracy;
			}else {
				return _Cvt15_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT15/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt15_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT15/accuracy",v);
		_Cvt15_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt15_responsetime=null;

	/**
	 * @return Returns the CVT15/responseTime.
	 */
	public Integer getCvt15_responsetime() {
		try{
			if (_Cvt15_responsetime==null){
				_Cvt15_responsetime=getIntegerProperty("CVT15/responseTime");
				return _Cvt15_responsetime;
			}else {
				return _Cvt15_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT15/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt15_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT15/responseTime",v);
		_Cvt15_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt16_accuracy=null;

	/**
	 * @return Returns the CVT16/accuracy.
	 */
	public Integer getCvt16_accuracy() {
		try{
			if (_Cvt16_accuracy==null){
				_Cvt16_accuracy=getIntegerProperty("CVT16/accuracy");
				return _Cvt16_accuracy;
			}else {
				return _Cvt16_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT16/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt16_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT16/accuracy",v);
		_Cvt16_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt16_responsetime=null;

	/**
	 * @return Returns the CVT16/responseTime.
	 */
	public Integer getCvt16_responsetime() {
		try{
			if (_Cvt16_responsetime==null){
				_Cvt16_responsetime=getIntegerProperty("CVT16/responseTime");
				return _Cvt16_responsetime;
			}else {
				return _Cvt16_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT16/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt16_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT16/responseTime",v);
		_Cvt16_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt17_accuracy=null;

	/**
	 * @return Returns the CVT17/accuracy.
	 */
	public Integer getCvt17_accuracy() {
		try{
			if (_Cvt17_accuracy==null){
				_Cvt17_accuracy=getIntegerProperty("CVT17/accuracy");
				return _Cvt17_accuracy;
			}else {
				return _Cvt17_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT17/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt17_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT17/accuracy",v);
		_Cvt17_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt17_responsetime=null;

	/**
	 * @return Returns the CVT17/responseTime.
	 */
	public Integer getCvt17_responsetime() {
		try{
			if (_Cvt17_responsetime==null){
				_Cvt17_responsetime=getIntegerProperty("CVT17/responseTime");
				return _Cvt17_responsetime;
			}else {
				return _Cvt17_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT17/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt17_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT17/responseTime",v);
		_Cvt17_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt18_accuracy=null;

	/**
	 * @return Returns the CVT18/accuracy.
	 */
	public Integer getCvt18_accuracy() {
		try{
			if (_Cvt18_accuracy==null){
				_Cvt18_accuracy=getIntegerProperty("CVT18/accuracy");
				return _Cvt18_accuracy;
			}else {
				return _Cvt18_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT18/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt18_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT18/accuracy",v);
		_Cvt18_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt18_responsetime=null;

	/**
	 * @return Returns the CVT18/responseTime.
	 */
	public Integer getCvt18_responsetime() {
		try{
			if (_Cvt18_responsetime==null){
				_Cvt18_responsetime=getIntegerProperty("CVT18/responseTime");
				return _Cvt18_responsetime;
			}else {
				return _Cvt18_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT18/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt18_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT18/responseTime",v);
		_Cvt18_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt19_accuracy=null;

	/**
	 * @return Returns the CVT19/accuracy.
	 */
	public Integer getCvt19_accuracy() {
		try{
			if (_Cvt19_accuracy==null){
				_Cvt19_accuracy=getIntegerProperty("CVT19/accuracy");
				return _Cvt19_accuracy;
			}else {
				return _Cvt19_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT19/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt19_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT19/accuracy",v);
		_Cvt19_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt19_responsetime=null;

	/**
	 * @return Returns the CVT19/responseTime.
	 */
	public Integer getCvt19_responsetime() {
		try{
			if (_Cvt19_responsetime==null){
				_Cvt19_responsetime=getIntegerProperty("CVT19/responseTime");
				return _Cvt19_responsetime;
			}else {
				return _Cvt19_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT19/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt19_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT19/responseTime",v);
		_Cvt19_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt20_accuracy=null;

	/**
	 * @return Returns the CVT20/accuracy.
	 */
	public Integer getCvt20_accuracy() {
		try{
			if (_Cvt20_accuracy==null){
				_Cvt20_accuracy=getIntegerProperty("CVT20/accuracy");
				return _Cvt20_accuracy;
			}else {
				return _Cvt20_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT20/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt20_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT20/accuracy",v);
		_Cvt20_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt20_responsetime=null;

	/**
	 * @return Returns the CVT20/responseTime.
	 */
	public Integer getCvt20_responsetime() {
		try{
			if (_Cvt20_responsetime==null){
				_Cvt20_responsetime=getIntegerProperty("CVT20/responseTime");
				return _Cvt20_responsetime;
			}else {
				return _Cvt20_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT20/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt20_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT20/responseTime",v);
		_Cvt20_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt21_accuracy=null;

	/**
	 * @return Returns the CVT21/accuracy.
	 */
	public Integer getCvt21_accuracy() {
		try{
			if (_Cvt21_accuracy==null){
				_Cvt21_accuracy=getIntegerProperty("CVT21/accuracy");
				return _Cvt21_accuracy;
			}else {
				return _Cvt21_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT21/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt21_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT21/accuracy",v);
		_Cvt21_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt21_responsetime=null;

	/**
	 * @return Returns the CVT21/responseTime.
	 */
	public Integer getCvt21_responsetime() {
		try{
			if (_Cvt21_responsetime==null){
				_Cvt21_responsetime=getIntegerProperty("CVT21/responseTime");
				return _Cvt21_responsetime;
			}else {
				return _Cvt21_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT21/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt21_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT21/responseTime",v);
		_Cvt21_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt22_accuracy=null;

	/**
	 * @return Returns the CVT22/accuracy.
	 */
	public Integer getCvt22_accuracy() {
		try{
			if (_Cvt22_accuracy==null){
				_Cvt22_accuracy=getIntegerProperty("CVT22/accuracy");
				return _Cvt22_accuracy;
			}else {
				return _Cvt22_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT22/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt22_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT22/accuracy",v);
		_Cvt22_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt22_responsetime=null;

	/**
	 * @return Returns the CVT22/responseTime.
	 */
	public Integer getCvt22_responsetime() {
		try{
			if (_Cvt22_responsetime==null){
				_Cvt22_responsetime=getIntegerProperty("CVT22/responseTime");
				return _Cvt22_responsetime;
			}else {
				return _Cvt22_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT22/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt22_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT22/responseTime",v);
		_Cvt22_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt23_accuracy=null;

	/**
	 * @return Returns the CVT23/accuracy.
	 */
	public Integer getCvt23_accuracy() {
		try{
			if (_Cvt23_accuracy==null){
				_Cvt23_accuracy=getIntegerProperty("CVT23/accuracy");
				return _Cvt23_accuracy;
			}else {
				return _Cvt23_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT23/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt23_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT23/accuracy",v);
		_Cvt23_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt23_responsetime=null;

	/**
	 * @return Returns the CVT23/responseTime.
	 */
	public Integer getCvt23_responsetime() {
		try{
			if (_Cvt23_responsetime==null){
				_Cvt23_responsetime=getIntegerProperty("CVT23/responseTime");
				return _Cvt23_responsetime;
			}else {
				return _Cvt23_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT23/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt23_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT23/responseTime",v);
		_Cvt23_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt24_accuracy=null;

	/**
	 * @return Returns the CVT24/accuracy.
	 */
	public Integer getCvt24_accuracy() {
		try{
			if (_Cvt24_accuracy==null){
				_Cvt24_accuracy=getIntegerProperty("CVT24/accuracy");
				return _Cvt24_accuracy;
			}else {
				return _Cvt24_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT24/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt24_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT24/accuracy",v);
		_Cvt24_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt24_responsetime=null;

	/**
	 * @return Returns the CVT24/responseTime.
	 */
	public Integer getCvt24_responsetime() {
		try{
			if (_Cvt24_responsetime==null){
				_Cvt24_responsetime=getIntegerProperty("CVT24/responseTime");
				return _Cvt24_responsetime;
			}else {
				return _Cvt24_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT24/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt24_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT24/responseTime",v);
		_Cvt24_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt25_accuracy=null;

	/**
	 * @return Returns the CVT25/accuracy.
	 */
	public Integer getCvt25_accuracy() {
		try{
			if (_Cvt25_accuracy==null){
				_Cvt25_accuracy=getIntegerProperty("CVT25/accuracy");
				return _Cvt25_accuracy;
			}else {
				return _Cvt25_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT25/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt25_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT25/accuracy",v);
		_Cvt25_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt25_responsetime=null;

	/**
	 * @return Returns the CVT25/responseTime.
	 */
	public Integer getCvt25_responsetime() {
		try{
			if (_Cvt25_responsetime==null){
				_Cvt25_responsetime=getIntegerProperty("CVT25/responseTime");
				return _Cvt25_responsetime;
			}else {
				return _Cvt25_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT25/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt25_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT25/responseTime",v);
		_Cvt25_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt26_accuracy=null;

	/**
	 * @return Returns the CVT26/accuracy.
	 */
	public Integer getCvt26_accuracy() {
		try{
			if (_Cvt26_accuracy==null){
				_Cvt26_accuracy=getIntegerProperty("CVT26/accuracy");
				return _Cvt26_accuracy;
			}else {
				return _Cvt26_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT26/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt26_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT26/accuracy",v);
		_Cvt26_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt26_responsetime=null;

	/**
	 * @return Returns the CVT26/responseTime.
	 */
	public Integer getCvt26_responsetime() {
		try{
			if (_Cvt26_responsetime==null){
				_Cvt26_responsetime=getIntegerProperty("CVT26/responseTime");
				return _Cvt26_responsetime;
			}else {
				return _Cvt26_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT26/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt26_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT26/responseTime",v);
		_Cvt26_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt27_accuracy=null;

	/**
	 * @return Returns the CVT27/accuracy.
	 */
	public Integer getCvt27_accuracy() {
		try{
			if (_Cvt27_accuracy==null){
				_Cvt27_accuracy=getIntegerProperty("CVT27/accuracy");
				return _Cvt27_accuracy;
			}else {
				return _Cvt27_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT27/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt27_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT27/accuracy",v);
		_Cvt27_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt27_responsetime=null;

	/**
	 * @return Returns the CVT27/responseTime.
	 */
	public Integer getCvt27_responsetime() {
		try{
			if (_Cvt27_responsetime==null){
				_Cvt27_responsetime=getIntegerProperty("CVT27/responseTime");
				return _Cvt27_responsetime;
			}else {
				return _Cvt27_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT27/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt27_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT27/responseTime",v);
		_Cvt27_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt28_accuracy=null;

	/**
	 * @return Returns the CVT28/accuracy.
	 */
	public Integer getCvt28_accuracy() {
		try{
			if (_Cvt28_accuracy==null){
				_Cvt28_accuracy=getIntegerProperty("CVT28/accuracy");
				return _Cvt28_accuracy;
			}else {
				return _Cvt28_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT28/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt28_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT28/accuracy",v);
		_Cvt28_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt28_responsetime=null;

	/**
	 * @return Returns the CVT28/responseTime.
	 */
	public Integer getCvt28_responsetime() {
		try{
			if (_Cvt28_responsetime==null){
				_Cvt28_responsetime=getIntegerProperty("CVT28/responseTime");
				return _Cvt28_responsetime;
			}else {
				return _Cvt28_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT28/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt28_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT28/responseTime",v);
		_Cvt28_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt29_accuracy=null;

	/**
	 * @return Returns the CVT29/accuracy.
	 */
	public Integer getCvt29_accuracy() {
		try{
			if (_Cvt29_accuracy==null){
				_Cvt29_accuracy=getIntegerProperty("CVT29/accuracy");
				return _Cvt29_accuracy;
			}else {
				return _Cvt29_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT29/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt29_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT29/accuracy",v);
		_Cvt29_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt29_responsetime=null;

	/**
	 * @return Returns the CVT29/responseTime.
	 */
	public Integer getCvt29_responsetime() {
		try{
			if (_Cvt29_responsetime==null){
				_Cvt29_responsetime=getIntegerProperty("CVT29/responseTime");
				return _Cvt29_responsetime;
			}else {
				return _Cvt29_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT29/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt29_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT29/responseTime",v);
		_Cvt29_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt30_accuracy=null;

	/**
	 * @return Returns the CVT30/accuracy.
	 */
	public Integer getCvt30_accuracy() {
		try{
			if (_Cvt30_accuracy==null){
				_Cvt30_accuracy=getIntegerProperty("CVT30/accuracy");
				return _Cvt30_accuracy;
			}else {
				return _Cvt30_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT30/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt30_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT30/accuracy",v);
		_Cvt30_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt30_responsetime=null;

	/**
	 * @return Returns the CVT30/responseTime.
	 */
	public Integer getCvt30_responsetime() {
		try{
			if (_Cvt30_responsetime==null){
				_Cvt30_responsetime=getIntegerProperty("CVT30/responseTime");
				return _Cvt30_responsetime;
			}else {
				return _Cvt30_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT30/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt30_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT30/responseTime",v);
		_Cvt30_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt31_accuracy=null;

	/**
	 * @return Returns the CVT31/accuracy.
	 */
	public Integer getCvt31_accuracy() {
		try{
			if (_Cvt31_accuracy==null){
				_Cvt31_accuracy=getIntegerProperty("CVT31/accuracy");
				return _Cvt31_accuracy;
			}else {
				return _Cvt31_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT31/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt31_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT31/accuracy",v);
		_Cvt31_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt31_responsetime=null;

	/**
	 * @return Returns the CVT31/responseTime.
	 */
	public Integer getCvt31_responsetime() {
		try{
			if (_Cvt31_responsetime==null){
				_Cvt31_responsetime=getIntegerProperty("CVT31/responseTime");
				return _Cvt31_responsetime;
			}else {
				return _Cvt31_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT31/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt31_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT31/responseTime",v);
		_Cvt31_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt32_accuracy=null;

	/**
	 * @return Returns the CVT32/accuracy.
	 */
	public Integer getCvt32_accuracy() {
		try{
			if (_Cvt32_accuracy==null){
				_Cvt32_accuracy=getIntegerProperty("CVT32/accuracy");
				return _Cvt32_accuracy;
			}else {
				return _Cvt32_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT32/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt32_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT32/accuracy",v);
		_Cvt32_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt32_responsetime=null;

	/**
	 * @return Returns the CVT32/responseTime.
	 */
	public Integer getCvt32_responsetime() {
		try{
			if (_Cvt32_responsetime==null){
				_Cvt32_responsetime=getIntegerProperty("CVT32/responseTime");
				return _Cvt32_responsetime;
			}else {
				return _Cvt32_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT32/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt32_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT32/responseTime",v);
		_Cvt32_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt33_accuracy=null;

	/**
	 * @return Returns the CVT33/accuracy.
	 */
	public Integer getCvt33_accuracy() {
		try{
			if (_Cvt33_accuracy==null){
				_Cvt33_accuracy=getIntegerProperty("CVT33/accuracy");
				return _Cvt33_accuracy;
			}else {
				return _Cvt33_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT33/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt33_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT33/accuracy",v);
		_Cvt33_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt33_responsetime=null;

	/**
	 * @return Returns the CVT33/responseTime.
	 */
	public Integer getCvt33_responsetime() {
		try{
			if (_Cvt33_responsetime==null){
				_Cvt33_responsetime=getIntegerProperty("CVT33/responseTime");
				return _Cvt33_responsetime;
			}else {
				return _Cvt33_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT33/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt33_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT33/responseTime",v);
		_Cvt33_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt34_accuracy=null;

	/**
	 * @return Returns the CVT34/accuracy.
	 */
	public Integer getCvt34_accuracy() {
		try{
			if (_Cvt34_accuracy==null){
				_Cvt34_accuracy=getIntegerProperty("CVT34/accuracy");
				return _Cvt34_accuracy;
			}else {
				return _Cvt34_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT34/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt34_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT34/accuracy",v);
		_Cvt34_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt34_responsetime=null;

	/**
	 * @return Returns the CVT34/responseTime.
	 */
	public Integer getCvt34_responsetime() {
		try{
			if (_Cvt34_responsetime==null){
				_Cvt34_responsetime=getIntegerProperty("CVT34/responseTime");
				return _Cvt34_responsetime;
			}else {
				return _Cvt34_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT34/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt34_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT34/responseTime",v);
		_Cvt34_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt35_accuracy=null;

	/**
	 * @return Returns the CVT35/accuracy.
	 */
	public Integer getCvt35_accuracy() {
		try{
			if (_Cvt35_accuracy==null){
				_Cvt35_accuracy=getIntegerProperty("CVT35/accuracy");
				return _Cvt35_accuracy;
			}else {
				return _Cvt35_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT35/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt35_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT35/accuracy",v);
		_Cvt35_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt35_responsetime=null;

	/**
	 * @return Returns the CVT35/responseTime.
	 */
	public Integer getCvt35_responsetime() {
		try{
			if (_Cvt35_responsetime==null){
				_Cvt35_responsetime=getIntegerProperty("CVT35/responseTime");
				return _Cvt35_responsetime;
			}else {
				return _Cvt35_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT35/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt35_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT35/responseTime",v);
		_Cvt35_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt36_accuracy=null;

	/**
	 * @return Returns the CVT36/accuracy.
	 */
	public Integer getCvt36_accuracy() {
		try{
			if (_Cvt36_accuracy==null){
				_Cvt36_accuracy=getIntegerProperty("CVT36/accuracy");
				return _Cvt36_accuracy;
			}else {
				return _Cvt36_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT36/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt36_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT36/accuracy",v);
		_Cvt36_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt36_responsetime=null;

	/**
	 * @return Returns the CVT36/responseTime.
	 */
	public Integer getCvt36_responsetime() {
		try{
			if (_Cvt36_responsetime==null){
				_Cvt36_responsetime=getIntegerProperty("CVT36/responseTime");
				return _Cvt36_responsetime;
			}else {
				return _Cvt36_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT36/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt36_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT36/responseTime",v);
		_Cvt36_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt37_accuracy=null;

	/**
	 * @return Returns the CVT37/accuracy.
	 */
	public Integer getCvt37_accuracy() {
		try{
			if (_Cvt37_accuracy==null){
				_Cvt37_accuracy=getIntegerProperty("CVT37/accuracy");
				return _Cvt37_accuracy;
			}else {
				return _Cvt37_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT37/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt37_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT37/accuracy",v);
		_Cvt37_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt37_responsetime=null;

	/**
	 * @return Returns the CVT37/responseTime.
	 */
	public Integer getCvt37_responsetime() {
		try{
			if (_Cvt37_responsetime==null){
				_Cvt37_responsetime=getIntegerProperty("CVT37/responseTime");
				return _Cvt37_responsetime;
			}else {
				return _Cvt37_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT37/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt37_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT37/responseTime",v);
		_Cvt37_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt38_accuracy=null;

	/**
	 * @return Returns the CVT38/accuracy.
	 */
	public Integer getCvt38_accuracy() {
		try{
			if (_Cvt38_accuracy==null){
				_Cvt38_accuracy=getIntegerProperty("CVT38/accuracy");
				return _Cvt38_accuracy;
			}else {
				return _Cvt38_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT38/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt38_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT38/accuracy",v);
		_Cvt38_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt38_responsetime=null;

	/**
	 * @return Returns the CVT38/responseTime.
	 */
	public Integer getCvt38_responsetime() {
		try{
			if (_Cvt38_responsetime==null){
				_Cvt38_responsetime=getIntegerProperty("CVT38/responseTime");
				return _Cvt38_responsetime;
			}else {
				return _Cvt38_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT38/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt38_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT38/responseTime",v);
		_Cvt38_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt39_accuracy=null;

	/**
	 * @return Returns the CVT39/accuracy.
	 */
	public Integer getCvt39_accuracy() {
		try{
			if (_Cvt39_accuracy==null){
				_Cvt39_accuracy=getIntegerProperty("CVT39/accuracy");
				return _Cvt39_accuracy;
			}else {
				return _Cvt39_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT39/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt39_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT39/accuracy",v);
		_Cvt39_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt39_responsetime=null;

	/**
	 * @return Returns the CVT39/responseTime.
	 */
	public Integer getCvt39_responsetime() {
		try{
			if (_Cvt39_responsetime==null){
				_Cvt39_responsetime=getIntegerProperty("CVT39/responseTime");
				return _Cvt39_responsetime;
			}else {
				return _Cvt39_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT39/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt39_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT39/responseTime",v);
		_Cvt39_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt40_accuracy=null;

	/**
	 * @return Returns the CVT40/accuracy.
	 */
	public Integer getCvt40_accuracy() {
		try{
			if (_Cvt40_accuracy==null){
				_Cvt40_accuracy=getIntegerProperty("CVT40/accuracy");
				return _Cvt40_accuracy;
			}else {
				return _Cvt40_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT40/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvt40_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT40/accuracy",v);
		_Cvt40_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvt40_responsetime=null;

	/**
	 * @return Returns the CVT40/responseTime.
	 */
	public Integer getCvt40_responsetime() {
		try{
			if (_Cvt40_responsetime==null){
				_Cvt40_responsetime=getIntegerProperty("CVT40/responseTime");
				return _Cvt40_responsetime;
			}else {
				return _Cvt40_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVT40/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvt40_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVT40/responseTime",v);
		_Cvt40_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet1_accuracy=null;

	/**
	 * @return Returns the OET1/accuracy.
	 */
	public Integer getOet1_accuracy() {
		try{
			if (_Oet1_accuracy==null){
				_Oet1_accuracy=getIntegerProperty("OET1/accuracy");
				return _Oet1_accuracy;
			}else {
				return _Oet1_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET1/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet1_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET1/accuracy",v);
		_Oet1_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet1_responsetime=null;

	/**
	 * @return Returns the OET1/responseTime.
	 */
	public Integer getOet1_responsetime() {
		try{
			if (_Oet1_responsetime==null){
				_Oet1_responsetime=getIntegerProperty("OET1/responseTime");
				return _Oet1_responsetime;
			}else {
				return _Oet1_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET1/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet1_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET1/responseTime",v);
		_Oet1_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet2_accuracy=null;

	/**
	 * @return Returns the OET2/accuracy.
	 */
	public Integer getOet2_accuracy() {
		try{
			if (_Oet2_accuracy==null){
				_Oet2_accuracy=getIntegerProperty("OET2/accuracy");
				return _Oet2_accuracy;
			}else {
				return _Oet2_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET2/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet2_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET2/accuracy",v);
		_Oet2_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet2_responsetime=null;

	/**
	 * @return Returns the OET2/responseTime.
	 */
	public Integer getOet2_responsetime() {
		try{
			if (_Oet2_responsetime==null){
				_Oet2_responsetime=getIntegerProperty("OET2/responseTime");
				return _Oet2_responsetime;
			}else {
				return _Oet2_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET2/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet2_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET2/responseTime",v);
		_Oet2_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet3_accuracy=null;

	/**
	 * @return Returns the OET3/accuracy.
	 */
	public Integer getOet3_accuracy() {
		try{
			if (_Oet3_accuracy==null){
				_Oet3_accuracy=getIntegerProperty("OET3/accuracy");
				return _Oet3_accuracy;
			}else {
				return _Oet3_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET3/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet3_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET3/accuracy",v);
		_Oet3_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet3_responsetime=null;

	/**
	 * @return Returns the OET3/responseTime.
	 */
	public Integer getOet3_responsetime() {
		try{
			if (_Oet3_responsetime==null){
				_Oet3_responsetime=getIntegerProperty("OET3/responseTime");
				return _Oet3_responsetime;
			}else {
				return _Oet3_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET3/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet3_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET3/responseTime",v);
		_Oet3_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet4_accuracy=null;

	/**
	 * @return Returns the OET4/accuracy.
	 */
	public Integer getOet4_accuracy() {
		try{
			if (_Oet4_accuracy==null){
				_Oet4_accuracy=getIntegerProperty("OET4/accuracy");
				return _Oet4_accuracy;
			}else {
				return _Oet4_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET4/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet4_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET4/accuracy",v);
		_Oet4_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet4_responsetime=null;

	/**
	 * @return Returns the OET4/responseTime.
	 */
	public Integer getOet4_responsetime() {
		try{
			if (_Oet4_responsetime==null){
				_Oet4_responsetime=getIntegerProperty("OET4/responseTime");
				return _Oet4_responsetime;
			}else {
				return _Oet4_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET4/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet4_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET4/responseTime",v);
		_Oet4_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet5_accuracy=null;

	/**
	 * @return Returns the OET5/accuracy.
	 */
	public Integer getOet5_accuracy() {
		try{
			if (_Oet5_accuracy==null){
				_Oet5_accuracy=getIntegerProperty("OET5/accuracy");
				return _Oet5_accuracy;
			}else {
				return _Oet5_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET5/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet5_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET5/accuracy",v);
		_Oet5_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet5_responsetime=null;

	/**
	 * @return Returns the OET5/responseTime.
	 */
	public Integer getOet5_responsetime() {
		try{
			if (_Oet5_responsetime==null){
				_Oet5_responsetime=getIntegerProperty("OET5/responseTime");
				return _Oet5_responsetime;
			}else {
				return _Oet5_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET5/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet5_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET5/responseTime",v);
		_Oet5_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet6_accuracy=null;

	/**
	 * @return Returns the OET6/accuracy.
	 */
	public Integer getOet6_accuracy() {
		try{
			if (_Oet6_accuracy==null){
				_Oet6_accuracy=getIntegerProperty("OET6/accuracy");
				return _Oet6_accuracy;
			}else {
				return _Oet6_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET6/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet6_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET6/accuracy",v);
		_Oet6_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet6_responsetime=null;

	/**
	 * @return Returns the OET6/responseTime.
	 */
	public Integer getOet6_responsetime() {
		try{
			if (_Oet6_responsetime==null){
				_Oet6_responsetime=getIntegerProperty("OET6/responseTime");
				return _Oet6_responsetime;
			}else {
				return _Oet6_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET6/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet6_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET6/responseTime",v);
		_Oet6_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet7_accuracy=null;

	/**
	 * @return Returns the OET7/accuracy.
	 */
	public Integer getOet7_accuracy() {
		try{
			if (_Oet7_accuracy==null){
				_Oet7_accuracy=getIntegerProperty("OET7/accuracy");
				return _Oet7_accuracy;
			}else {
				return _Oet7_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET7/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet7_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET7/accuracy",v);
		_Oet7_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet7_responsetime=null;

	/**
	 * @return Returns the OET7/responseTime.
	 */
	public Integer getOet7_responsetime() {
		try{
			if (_Oet7_responsetime==null){
				_Oet7_responsetime=getIntegerProperty("OET7/responseTime");
				return _Oet7_responsetime;
			}else {
				return _Oet7_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET7/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet7_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET7/responseTime",v);
		_Oet7_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet8_accuracy=null;

	/**
	 * @return Returns the OET8/accuracy.
	 */
	public Integer getOet8_accuracy() {
		try{
			if (_Oet8_accuracy==null){
				_Oet8_accuracy=getIntegerProperty("OET8/accuracy");
				return _Oet8_accuracy;
			}else {
				return _Oet8_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET8/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet8_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET8/accuracy",v);
		_Oet8_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet8_responsetime=null;

	/**
	 * @return Returns the OET8/responseTime.
	 */
	public Integer getOet8_responsetime() {
		try{
			if (_Oet8_responsetime==null){
				_Oet8_responsetime=getIntegerProperty("OET8/responseTime");
				return _Oet8_responsetime;
			}else {
				return _Oet8_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET8/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet8_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET8/responseTime",v);
		_Oet8_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet9_accuracy=null;

	/**
	 * @return Returns the OET9/accuracy.
	 */
	public Integer getOet9_accuracy() {
		try{
			if (_Oet9_accuracy==null){
				_Oet9_accuracy=getIntegerProperty("OET9/accuracy");
				return _Oet9_accuracy;
			}else {
				return _Oet9_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET9/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet9_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET9/accuracy",v);
		_Oet9_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet9_responsetime=null;

	/**
	 * @return Returns the OET9/responseTime.
	 */
	public Integer getOet9_responsetime() {
		try{
			if (_Oet9_responsetime==null){
				_Oet9_responsetime=getIntegerProperty("OET9/responseTime");
				return _Oet9_responsetime;
			}else {
				return _Oet9_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET9/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet9_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET9/responseTime",v);
		_Oet9_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet10_accuracy=null;

	/**
	 * @return Returns the OET10/accuracy.
	 */
	public Integer getOet10_accuracy() {
		try{
			if (_Oet10_accuracy==null){
				_Oet10_accuracy=getIntegerProperty("OET10/accuracy");
				return _Oet10_accuracy;
			}else {
				return _Oet10_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET10/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet10_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET10/accuracy",v);
		_Oet10_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet10_responsetime=null;

	/**
	 * @return Returns the OET10/responseTime.
	 */
	public Integer getOet10_responsetime() {
		try{
			if (_Oet10_responsetime==null){
				_Oet10_responsetime=getIntegerProperty("OET10/responseTime");
				return _Oet10_responsetime;
			}else {
				return _Oet10_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET10/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet10_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET10/responseTime",v);
		_Oet10_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet11_accuracy=null;

	/**
	 * @return Returns the OET11/accuracy.
	 */
	public Integer getOet11_accuracy() {
		try{
			if (_Oet11_accuracy==null){
				_Oet11_accuracy=getIntegerProperty("OET11/accuracy");
				return _Oet11_accuracy;
			}else {
				return _Oet11_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET11/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet11_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET11/accuracy",v);
		_Oet11_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet11_responsetime=null;

	/**
	 * @return Returns the OET11/responseTime.
	 */
	public Integer getOet11_responsetime() {
		try{
			if (_Oet11_responsetime==null){
				_Oet11_responsetime=getIntegerProperty("OET11/responseTime");
				return _Oet11_responsetime;
			}else {
				return _Oet11_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET11/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet11_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET11/responseTime",v);
		_Oet11_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet12_accuracy=null;

	/**
	 * @return Returns the OET12/accuracy.
	 */
	public Integer getOet12_accuracy() {
		try{
			if (_Oet12_accuracy==null){
				_Oet12_accuracy=getIntegerProperty("OET12/accuracy");
				return _Oet12_accuracy;
			}else {
				return _Oet12_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET12/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet12_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET12/accuracy",v);
		_Oet12_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet12_responsetime=null;

	/**
	 * @return Returns the OET12/responseTime.
	 */
	public Integer getOet12_responsetime() {
		try{
			if (_Oet12_responsetime==null){
				_Oet12_responsetime=getIntegerProperty("OET12/responseTime");
				return _Oet12_responsetime;
			}else {
				return _Oet12_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET12/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet12_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET12/responseTime",v);
		_Oet12_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet13_accuracy=null;

	/**
	 * @return Returns the OET13/accuracy.
	 */
	public Integer getOet13_accuracy() {
		try{
			if (_Oet13_accuracy==null){
				_Oet13_accuracy=getIntegerProperty("OET13/accuracy");
				return _Oet13_accuracy;
			}else {
				return _Oet13_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET13/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet13_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET13/accuracy",v);
		_Oet13_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet13_responsetime=null;

	/**
	 * @return Returns the OET13/responseTime.
	 */
	public Integer getOet13_responsetime() {
		try{
			if (_Oet13_responsetime==null){
				_Oet13_responsetime=getIntegerProperty("OET13/responseTime");
				return _Oet13_responsetime;
			}else {
				return _Oet13_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET13/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet13_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET13/responseTime",v);
		_Oet13_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet14_accuracy=null;

	/**
	 * @return Returns the OET14/accuracy.
	 */
	public Integer getOet14_accuracy() {
		try{
			if (_Oet14_accuracy==null){
				_Oet14_accuracy=getIntegerProperty("OET14/accuracy");
				return _Oet14_accuracy;
			}else {
				return _Oet14_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET14/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet14_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET14/accuracy",v);
		_Oet14_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet14_responsetime=null;

	/**
	 * @return Returns the OET14/responseTime.
	 */
	public Integer getOet14_responsetime() {
		try{
			if (_Oet14_responsetime==null){
				_Oet14_responsetime=getIntegerProperty("OET14/responseTime");
				return _Oet14_responsetime;
			}else {
				return _Oet14_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET14/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet14_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET14/responseTime",v);
		_Oet14_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet15_accuracy=null;

	/**
	 * @return Returns the OET15/accuracy.
	 */
	public Integer getOet15_accuracy() {
		try{
			if (_Oet15_accuracy==null){
				_Oet15_accuracy=getIntegerProperty("OET15/accuracy");
				return _Oet15_accuracy;
			}else {
				return _Oet15_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET15/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet15_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET15/accuracy",v);
		_Oet15_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet15_responsetime=null;

	/**
	 * @return Returns the OET15/responseTime.
	 */
	public Integer getOet15_responsetime() {
		try{
			if (_Oet15_responsetime==null){
				_Oet15_responsetime=getIntegerProperty("OET15/responseTime");
				return _Oet15_responsetime;
			}else {
				return _Oet15_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET15/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet15_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET15/responseTime",v);
		_Oet15_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet16_accuracy=null;

	/**
	 * @return Returns the OET16/accuracy.
	 */
	public Integer getOet16_accuracy() {
		try{
			if (_Oet16_accuracy==null){
				_Oet16_accuracy=getIntegerProperty("OET16/accuracy");
				return _Oet16_accuracy;
			}else {
				return _Oet16_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET16/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet16_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET16/accuracy",v);
		_Oet16_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet16_responsetime=null;

	/**
	 * @return Returns the OET16/responseTime.
	 */
	public Integer getOet16_responsetime() {
		try{
			if (_Oet16_responsetime==null){
				_Oet16_responsetime=getIntegerProperty("OET16/responseTime");
				return _Oet16_responsetime;
			}else {
				return _Oet16_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET16/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet16_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET16/responseTime",v);
		_Oet16_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet17_accuracy=null;

	/**
	 * @return Returns the OET17/accuracy.
	 */
	public Integer getOet17_accuracy() {
		try{
			if (_Oet17_accuracy==null){
				_Oet17_accuracy=getIntegerProperty("OET17/accuracy");
				return _Oet17_accuracy;
			}else {
				return _Oet17_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET17/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet17_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET17/accuracy",v);
		_Oet17_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet17_responsetime=null;

	/**
	 * @return Returns the OET17/responseTime.
	 */
	public Integer getOet17_responsetime() {
		try{
			if (_Oet17_responsetime==null){
				_Oet17_responsetime=getIntegerProperty("OET17/responseTime");
				return _Oet17_responsetime;
			}else {
				return _Oet17_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET17/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet17_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET17/responseTime",v);
		_Oet17_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet18_accuracy=null;

	/**
	 * @return Returns the OET18/accuracy.
	 */
	public Integer getOet18_accuracy() {
		try{
			if (_Oet18_accuracy==null){
				_Oet18_accuracy=getIntegerProperty("OET18/accuracy");
				return _Oet18_accuracy;
			}else {
				return _Oet18_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET18/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet18_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET18/accuracy",v);
		_Oet18_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet18_responsetime=null;

	/**
	 * @return Returns the OET18/responseTime.
	 */
	public Integer getOet18_responsetime() {
		try{
			if (_Oet18_responsetime==null){
				_Oet18_responsetime=getIntegerProperty("OET18/responseTime");
				return _Oet18_responsetime;
			}else {
				return _Oet18_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET18/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet18_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET18/responseTime",v);
		_Oet18_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet19_accuracy=null;

	/**
	 * @return Returns the OET19/accuracy.
	 */
	public Integer getOet19_accuracy() {
		try{
			if (_Oet19_accuracy==null){
				_Oet19_accuracy=getIntegerProperty("OET19/accuracy");
				return _Oet19_accuracy;
			}else {
				return _Oet19_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET19/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet19_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET19/accuracy",v);
		_Oet19_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet19_responsetime=null;

	/**
	 * @return Returns the OET19/responseTime.
	 */
	public Integer getOet19_responsetime() {
		try{
			if (_Oet19_responsetime==null){
				_Oet19_responsetime=getIntegerProperty("OET19/responseTime");
				return _Oet19_responsetime;
			}else {
				return _Oet19_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET19/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet19_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET19/responseTime",v);
		_Oet19_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet20_accuracy=null;

	/**
	 * @return Returns the OET20/accuracy.
	 */
	public Integer getOet20_accuracy() {
		try{
			if (_Oet20_accuracy==null){
				_Oet20_accuracy=getIntegerProperty("OET20/accuracy");
				return _Oet20_accuracy;
			}else {
				return _Oet20_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET20/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet20_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET20/accuracy",v);
		_Oet20_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet20_responsetime=null;

	/**
	 * @return Returns the OET20/responseTime.
	 */
	public Integer getOet20_responsetime() {
		try{
			if (_Oet20_responsetime==null){
				_Oet20_responsetime=getIntegerProperty("OET20/responseTime");
				return _Oet20_responsetime;
			}else {
				return _Oet20_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET20/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet20_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET20/responseTime",v);
		_Oet20_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet21_accuracy=null;

	/**
	 * @return Returns the OET21/accuracy.
	 */
	public Integer getOet21_accuracy() {
		try{
			if (_Oet21_accuracy==null){
				_Oet21_accuracy=getIntegerProperty("OET21/accuracy");
				return _Oet21_accuracy;
			}else {
				return _Oet21_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET21/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet21_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET21/accuracy",v);
		_Oet21_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet21_responsetime=null;

	/**
	 * @return Returns the OET21/responseTime.
	 */
	public Integer getOet21_responsetime() {
		try{
			if (_Oet21_responsetime==null){
				_Oet21_responsetime=getIntegerProperty("OET21/responseTime");
				return _Oet21_responsetime;
			}else {
				return _Oet21_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET21/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet21_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET21/responseTime",v);
		_Oet21_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet22_accuracy=null;

	/**
	 * @return Returns the OET22/accuracy.
	 */
	public Integer getOet22_accuracy() {
		try{
			if (_Oet22_accuracy==null){
				_Oet22_accuracy=getIntegerProperty("OET22/accuracy");
				return _Oet22_accuracy;
			}else {
				return _Oet22_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET22/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet22_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET22/accuracy",v);
		_Oet22_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet22_responsetime=null;

	/**
	 * @return Returns the OET22/responseTime.
	 */
	public Integer getOet22_responsetime() {
		try{
			if (_Oet22_responsetime==null){
				_Oet22_responsetime=getIntegerProperty("OET22/responseTime");
				return _Oet22_responsetime;
			}else {
				return _Oet22_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET22/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet22_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET22/responseTime",v);
		_Oet22_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet23_accuracy=null;

	/**
	 * @return Returns the OET23/accuracy.
	 */
	public Integer getOet23_accuracy() {
		try{
			if (_Oet23_accuracy==null){
				_Oet23_accuracy=getIntegerProperty("OET23/accuracy");
				return _Oet23_accuracy;
			}else {
				return _Oet23_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET23/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet23_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET23/accuracy",v);
		_Oet23_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet23_responsetime=null;

	/**
	 * @return Returns the OET23/responseTime.
	 */
	public Integer getOet23_responsetime() {
		try{
			if (_Oet23_responsetime==null){
				_Oet23_responsetime=getIntegerProperty("OET23/responseTime");
				return _Oet23_responsetime;
			}else {
				return _Oet23_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET23/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet23_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET23/responseTime",v);
		_Oet23_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet24_accuracy=null;

	/**
	 * @return Returns the OET24/accuracy.
	 */
	public Integer getOet24_accuracy() {
		try{
			if (_Oet24_accuracy==null){
				_Oet24_accuracy=getIntegerProperty("OET24/accuracy");
				return _Oet24_accuracy;
			}else {
				return _Oet24_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET24/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet24_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET24/accuracy",v);
		_Oet24_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet24_responsetime=null;

	/**
	 * @return Returns the OET24/responseTime.
	 */
	public Integer getOet24_responsetime() {
		try{
			if (_Oet24_responsetime==null){
				_Oet24_responsetime=getIntegerProperty("OET24/responseTime");
				return _Oet24_responsetime;
			}else {
				return _Oet24_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET24/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet24_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET24/responseTime",v);
		_Oet24_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet25_accuracy=null;

	/**
	 * @return Returns the OET25/accuracy.
	 */
	public Integer getOet25_accuracy() {
		try{
			if (_Oet25_accuracy==null){
				_Oet25_accuracy=getIntegerProperty("OET25/accuracy");
				return _Oet25_accuracy;
			}else {
				return _Oet25_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET25/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet25_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET25/accuracy",v);
		_Oet25_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet25_responsetime=null;

	/**
	 * @return Returns the OET25/responseTime.
	 */
	public Integer getOet25_responsetime() {
		try{
			if (_Oet25_responsetime==null){
				_Oet25_responsetime=getIntegerProperty("OET25/responseTime");
				return _Oet25_responsetime;
			}else {
				return _Oet25_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET25/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet25_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET25/responseTime",v);
		_Oet25_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet26_accuracy=null;

	/**
	 * @return Returns the OET26/accuracy.
	 */
	public Integer getOet26_accuracy() {
		try{
			if (_Oet26_accuracy==null){
				_Oet26_accuracy=getIntegerProperty("OET26/accuracy");
				return _Oet26_accuracy;
			}else {
				return _Oet26_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET26/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet26_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET26/accuracy",v);
		_Oet26_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet26_responsetime=null;

	/**
	 * @return Returns the OET26/responseTime.
	 */
	public Integer getOet26_responsetime() {
		try{
			if (_Oet26_responsetime==null){
				_Oet26_responsetime=getIntegerProperty("OET26/responseTime");
				return _Oet26_responsetime;
			}else {
				return _Oet26_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET26/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet26_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET26/responseTime",v);
		_Oet26_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet27_accuracy=null;

	/**
	 * @return Returns the OET27/accuracy.
	 */
	public Integer getOet27_accuracy() {
		try{
			if (_Oet27_accuracy==null){
				_Oet27_accuracy=getIntegerProperty("OET27/accuracy");
				return _Oet27_accuracy;
			}else {
				return _Oet27_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET27/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet27_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET27/accuracy",v);
		_Oet27_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet27_responsetime=null;

	/**
	 * @return Returns the OET27/responseTime.
	 */
	public Integer getOet27_responsetime() {
		try{
			if (_Oet27_responsetime==null){
				_Oet27_responsetime=getIntegerProperty("OET27/responseTime");
				return _Oet27_responsetime;
			}else {
				return _Oet27_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET27/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet27_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET27/responseTime",v);
		_Oet27_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet28_accuracy=null;

	/**
	 * @return Returns the OET28/accuracy.
	 */
	public Integer getOet28_accuracy() {
		try{
			if (_Oet28_accuracy==null){
				_Oet28_accuracy=getIntegerProperty("OET28/accuracy");
				return _Oet28_accuracy;
			}else {
				return _Oet28_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET28/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet28_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET28/accuracy",v);
		_Oet28_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet28_responsetime=null;

	/**
	 * @return Returns the OET28/responseTime.
	 */
	public Integer getOet28_responsetime() {
		try{
			if (_Oet28_responsetime==null){
				_Oet28_responsetime=getIntegerProperty("OET28/responseTime");
				return _Oet28_responsetime;
			}else {
				return _Oet28_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET28/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet28_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET28/responseTime",v);
		_Oet28_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet29_accuracy=null;

	/**
	 * @return Returns the OET29/accuracy.
	 */
	public Integer getOet29_accuracy() {
		try{
			if (_Oet29_accuracy==null){
				_Oet29_accuracy=getIntegerProperty("OET29/accuracy");
				return _Oet29_accuracy;
			}else {
				return _Oet29_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET29/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet29_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET29/accuracy",v);
		_Oet29_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet29_responsetime=null;

	/**
	 * @return Returns the OET29/responseTime.
	 */
	public Integer getOet29_responsetime() {
		try{
			if (_Oet29_responsetime==null){
				_Oet29_responsetime=getIntegerProperty("OET29/responseTime");
				return _Oet29_responsetime;
			}else {
				return _Oet29_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET29/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet29_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET29/responseTime",v);
		_Oet29_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet30_accuracy=null;

	/**
	 * @return Returns the OET30/accuracy.
	 */
	public Integer getOet30_accuracy() {
		try{
			if (_Oet30_accuracy==null){
				_Oet30_accuracy=getIntegerProperty("OET30/accuracy");
				return _Oet30_accuracy;
			}else {
				return _Oet30_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET30/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet30_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET30/accuracy",v);
		_Oet30_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet30_responsetime=null;

	/**
	 * @return Returns the OET30/responseTime.
	 */
	public Integer getOet30_responsetime() {
		try{
			if (_Oet30_responsetime==null){
				_Oet30_responsetime=getIntegerProperty("OET30/responseTime");
				return _Oet30_responsetime;
			}else {
				return _Oet30_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET30/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet30_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET30/responseTime",v);
		_Oet30_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet31_accuracy=null;

	/**
	 * @return Returns the OET31/accuracy.
	 */
	public Integer getOet31_accuracy() {
		try{
			if (_Oet31_accuracy==null){
				_Oet31_accuracy=getIntegerProperty("OET31/accuracy");
				return _Oet31_accuracy;
			}else {
				return _Oet31_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET31/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet31_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET31/accuracy",v);
		_Oet31_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet31_responsetime=null;

	/**
	 * @return Returns the OET31/responseTime.
	 */
	public Integer getOet31_responsetime() {
		try{
			if (_Oet31_responsetime==null){
				_Oet31_responsetime=getIntegerProperty("OET31/responseTime");
				return _Oet31_responsetime;
			}else {
				return _Oet31_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET31/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet31_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET31/responseTime",v);
		_Oet31_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet32_accuracy=null;

	/**
	 * @return Returns the OET32/accuracy.
	 */
	public Integer getOet32_accuracy() {
		try{
			if (_Oet32_accuracy==null){
				_Oet32_accuracy=getIntegerProperty("OET32/accuracy");
				return _Oet32_accuracy;
			}else {
				return _Oet32_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET32/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet32_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET32/accuracy",v);
		_Oet32_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet32_responsetime=null;

	/**
	 * @return Returns the OET32/responseTime.
	 */
	public Integer getOet32_responsetime() {
		try{
			if (_Oet32_responsetime==null){
				_Oet32_responsetime=getIntegerProperty("OET32/responseTime");
				return _Oet32_responsetime;
			}else {
				return _Oet32_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET32/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet32_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET32/responseTime",v);
		_Oet32_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet33_accuracy=null;

	/**
	 * @return Returns the OET33/accuracy.
	 */
	public Integer getOet33_accuracy() {
		try{
			if (_Oet33_accuracy==null){
				_Oet33_accuracy=getIntegerProperty("OET33/accuracy");
				return _Oet33_accuracy;
			}else {
				return _Oet33_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET33/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet33_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET33/accuracy",v);
		_Oet33_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet33_responsetime=null;

	/**
	 * @return Returns the OET33/responseTime.
	 */
	public Integer getOet33_responsetime() {
		try{
			if (_Oet33_responsetime==null){
				_Oet33_responsetime=getIntegerProperty("OET33/responseTime");
				return _Oet33_responsetime;
			}else {
				return _Oet33_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET33/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet33_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET33/responseTime",v);
		_Oet33_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet34_accuracy=null;

	/**
	 * @return Returns the OET34/accuracy.
	 */
	public Integer getOet34_accuracy() {
		try{
			if (_Oet34_accuracy==null){
				_Oet34_accuracy=getIntegerProperty("OET34/accuracy");
				return _Oet34_accuracy;
			}else {
				return _Oet34_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET34/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet34_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET34/accuracy",v);
		_Oet34_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet34_responsetime=null;

	/**
	 * @return Returns the OET34/responseTime.
	 */
	public Integer getOet34_responsetime() {
		try{
			if (_Oet34_responsetime==null){
				_Oet34_responsetime=getIntegerProperty("OET34/responseTime");
				return _Oet34_responsetime;
			}else {
				return _Oet34_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET34/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet34_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET34/responseTime",v);
		_Oet34_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet35_accuracy=null;

	/**
	 * @return Returns the OET35/accuracy.
	 */
	public Integer getOet35_accuracy() {
		try{
			if (_Oet35_accuracy==null){
				_Oet35_accuracy=getIntegerProperty("OET35/accuracy");
				return _Oet35_accuracy;
			}else {
				return _Oet35_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET35/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet35_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET35/accuracy",v);
		_Oet35_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet35_responsetime=null;

	/**
	 * @return Returns the OET35/responseTime.
	 */
	public Integer getOet35_responsetime() {
		try{
			if (_Oet35_responsetime==null){
				_Oet35_responsetime=getIntegerProperty("OET35/responseTime");
				return _Oet35_responsetime;
			}else {
				return _Oet35_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET35/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet35_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET35/responseTime",v);
		_Oet35_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet36_accuracy=null;

	/**
	 * @return Returns the OET36/accuracy.
	 */
	public Integer getOet36_accuracy() {
		try{
			if (_Oet36_accuracy==null){
				_Oet36_accuracy=getIntegerProperty("OET36/accuracy");
				return _Oet36_accuracy;
			}else {
				return _Oet36_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET36/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet36_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET36/accuracy",v);
		_Oet36_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet36_responsetime=null;

	/**
	 * @return Returns the OET36/responseTime.
	 */
	public Integer getOet36_responsetime() {
		try{
			if (_Oet36_responsetime==null){
				_Oet36_responsetime=getIntegerProperty("OET36/responseTime");
				return _Oet36_responsetime;
			}else {
				return _Oet36_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET36/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet36_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET36/responseTime",v);
		_Oet36_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet37_accuracy=null;

	/**
	 * @return Returns the OET37/accuracy.
	 */
	public Integer getOet37_accuracy() {
		try{
			if (_Oet37_accuracy==null){
				_Oet37_accuracy=getIntegerProperty("OET37/accuracy");
				return _Oet37_accuracy;
			}else {
				return _Oet37_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET37/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet37_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET37/accuracy",v);
		_Oet37_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet37_responsetime=null;

	/**
	 * @return Returns the OET37/responseTime.
	 */
	public Integer getOet37_responsetime() {
		try{
			if (_Oet37_responsetime==null){
				_Oet37_responsetime=getIntegerProperty("OET37/responseTime");
				return _Oet37_responsetime;
			}else {
				return _Oet37_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET37/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet37_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET37/responseTime",v);
		_Oet37_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet38_accuracy=null;

	/**
	 * @return Returns the OET38/accuracy.
	 */
	public Integer getOet38_accuracy() {
		try{
			if (_Oet38_accuracy==null){
				_Oet38_accuracy=getIntegerProperty("OET38/accuracy");
				return _Oet38_accuracy;
			}else {
				return _Oet38_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET38/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet38_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET38/accuracy",v);
		_Oet38_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet38_responsetime=null;

	/**
	 * @return Returns the OET38/responseTime.
	 */
	public Integer getOet38_responsetime() {
		try{
			if (_Oet38_responsetime==null){
				_Oet38_responsetime=getIntegerProperty("OET38/responseTime");
				return _Oet38_responsetime;
			}else {
				return _Oet38_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET38/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet38_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET38/responseTime",v);
		_Oet38_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet39_accuracy=null;

	/**
	 * @return Returns the OET39/accuracy.
	 */
	public Integer getOet39_accuracy() {
		try{
			if (_Oet39_accuracy==null){
				_Oet39_accuracy=getIntegerProperty("OET39/accuracy");
				return _Oet39_accuracy;
			}else {
				return _Oet39_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET39/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet39_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET39/accuracy",v);
		_Oet39_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet39_responsetime=null;

	/**
	 * @return Returns the OET39/responseTime.
	 */
	public Integer getOet39_responsetime() {
		try{
			if (_Oet39_responsetime==null){
				_Oet39_responsetime=getIntegerProperty("OET39/responseTime");
				return _Oet39_responsetime;
			}else {
				return _Oet39_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET39/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet39_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET39/responseTime",v);
		_Oet39_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet40_accuracy=null;

	/**
	 * @return Returns the OET40/accuracy.
	 */
	public Integer getOet40_accuracy() {
		try{
			if (_Oet40_accuracy==null){
				_Oet40_accuracy=getIntegerProperty("OET40/accuracy");
				return _Oet40_accuracy;
			}else {
				return _Oet40_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET40/accuracy.
	 * @param v Value to Set.
	 */
	public void setOet40_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET40/accuracy",v);
		_Oet40_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Oet40_responsetime=null;

	/**
	 * @return Returns the OET40/responseTime.
	 */
	public Integer getOet40_responsetime() {
		try{
			if (_Oet40_responsetime==null){
				_Oet40_responsetime=getIntegerProperty("OET40/responseTime");
				return _Oet40_responsetime;
			}else {
				return _Oet40_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for OET40/responseTime.
	 * @param v Value to Set.
	 */
	public void setOet40_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/OET40/responseTime",v);
		_Oet40_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet1_accuracy=null;

	/**
	 * @return Returns the CVOET1/accuracy.
	 */
	public Integer getCvoet1_accuracy() {
		try{
			if (_Cvoet1_accuracy==null){
				_Cvoet1_accuracy=getIntegerProperty("CVOET1/accuracy");
				return _Cvoet1_accuracy;
			}else {
				return _Cvoet1_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET1/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet1_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET1/accuracy",v);
		_Cvoet1_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet1_responsetime=null;

	/**
	 * @return Returns the CVOET1/responseTime.
	 */
	public Integer getCvoet1_responsetime() {
		try{
			if (_Cvoet1_responsetime==null){
				_Cvoet1_responsetime=getIntegerProperty("CVOET1/responseTime");
				return _Cvoet1_responsetime;
			}else {
				return _Cvoet1_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET1/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet1_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET1/responseTime",v);
		_Cvoet1_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet2_accuracy=null;

	/**
	 * @return Returns the CVOET2/accuracy.
	 */
	public Integer getCvoet2_accuracy() {
		try{
			if (_Cvoet2_accuracy==null){
				_Cvoet2_accuracy=getIntegerProperty("CVOET2/accuracy");
				return _Cvoet2_accuracy;
			}else {
				return _Cvoet2_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET2/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet2_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET2/accuracy",v);
		_Cvoet2_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet2_responsetime=null;

	/**
	 * @return Returns the CVOET2/responseTime.
	 */
	public Integer getCvoet2_responsetime() {
		try{
			if (_Cvoet2_responsetime==null){
				_Cvoet2_responsetime=getIntegerProperty("CVOET2/responseTime");
				return _Cvoet2_responsetime;
			}else {
				return _Cvoet2_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET2/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet2_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET2/responseTime",v);
		_Cvoet2_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet3_accuracy=null;

	/**
	 * @return Returns the CVOET3/accuracy.
	 */
	public Integer getCvoet3_accuracy() {
		try{
			if (_Cvoet3_accuracy==null){
				_Cvoet3_accuracy=getIntegerProperty("CVOET3/accuracy");
				return _Cvoet3_accuracy;
			}else {
				return _Cvoet3_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET3/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet3_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET3/accuracy",v);
		_Cvoet3_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet3_responsetime=null;

	/**
	 * @return Returns the CVOET3/responseTime.
	 */
	public Integer getCvoet3_responsetime() {
		try{
			if (_Cvoet3_responsetime==null){
				_Cvoet3_responsetime=getIntegerProperty("CVOET3/responseTime");
				return _Cvoet3_responsetime;
			}else {
				return _Cvoet3_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET3/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet3_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET3/responseTime",v);
		_Cvoet3_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet4_accuracy=null;

	/**
	 * @return Returns the CVOET4/accuracy.
	 */
	public Integer getCvoet4_accuracy() {
		try{
			if (_Cvoet4_accuracy==null){
				_Cvoet4_accuracy=getIntegerProperty("CVOET4/accuracy");
				return _Cvoet4_accuracy;
			}else {
				return _Cvoet4_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET4/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet4_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET4/accuracy",v);
		_Cvoet4_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet4_responsetime=null;

	/**
	 * @return Returns the CVOET4/responseTime.
	 */
	public Integer getCvoet4_responsetime() {
		try{
			if (_Cvoet4_responsetime==null){
				_Cvoet4_responsetime=getIntegerProperty("CVOET4/responseTime");
				return _Cvoet4_responsetime;
			}else {
				return _Cvoet4_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET4/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet4_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET4/responseTime",v);
		_Cvoet4_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet5_accuracy=null;

	/**
	 * @return Returns the CVOET5/accuracy.
	 */
	public Integer getCvoet5_accuracy() {
		try{
			if (_Cvoet5_accuracy==null){
				_Cvoet5_accuracy=getIntegerProperty("CVOET5/accuracy");
				return _Cvoet5_accuracy;
			}else {
				return _Cvoet5_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET5/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet5_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET5/accuracy",v);
		_Cvoet5_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet5_responsetime=null;

	/**
	 * @return Returns the CVOET5/responseTime.
	 */
	public Integer getCvoet5_responsetime() {
		try{
			if (_Cvoet5_responsetime==null){
				_Cvoet5_responsetime=getIntegerProperty("CVOET5/responseTime");
				return _Cvoet5_responsetime;
			}else {
				return _Cvoet5_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET5/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet5_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET5/responseTime",v);
		_Cvoet5_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet6_accuracy=null;

	/**
	 * @return Returns the CVOET6/accuracy.
	 */
	public Integer getCvoet6_accuracy() {
		try{
			if (_Cvoet6_accuracy==null){
				_Cvoet6_accuracy=getIntegerProperty("CVOET6/accuracy");
				return _Cvoet6_accuracy;
			}else {
				return _Cvoet6_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET6/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet6_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET6/accuracy",v);
		_Cvoet6_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet6_responsetime=null;

	/**
	 * @return Returns the CVOET6/responseTime.
	 */
	public Integer getCvoet6_responsetime() {
		try{
			if (_Cvoet6_responsetime==null){
				_Cvoet6_responsetime=getIntegerProperty("CVOET6/responseTime");
				return _Cvoet6_responsetime;
			}else {
				return _Cvoet6_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET6/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet6_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET6/responseTime",v);
		_Cvoet6_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet7_accuracy=null;

	/**
	 * @return Returns the CVOET7/accuracy.
	 */
	public Integer getCvoet7_accuracy() {
		try{
			if (_Cvoet7_accuracy==null){
				_Cvoet7_accuracy=getIntegerProperty("CVOET7/accuracy");
				return _Cvoet7_accuracy;
			}else {
				return _Cvoet7_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET7/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet7_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET7/accuracy",v);
		_Cvoet7_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet7_responsetime=null;

	/**
	 * @return Returns the CVOET7/responseTime.
	 */
	public Integer getCvoet7_responsetime() {
		try{
			if (_Cvoet7_responsetime==null){
				_Cvoet7_responsetime=getIntegerProperty("CVOET7/responseTime");
				return _Cvoet7_responsetime;
			}else {
				return _Cvoet7_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET7/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet7_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET7/responseTime",v);
		_Cvoet7_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet8_accuracy=null;

	/**
	 * @return Returns the CVOET8/accuracy.
	 */
	public Integer getCvoet8_accuracy() {
		try{
			if (_Cvoet8_accuracy==null){
				_Cvoet8_accuracy=getIntegerProperty("CVOET8/accuracy");
				return _Cvoet8_accuracy;
			}else {
				return _Cvoet8_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET8/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet8_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET8/accuracy",v);
		_Cvoet8_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet8_responsetime=null;

	/**
	 * @return Returns the CVOET8/responseTime.
	 */
	public Integer getCvoet8_responsetime() {
		try{
			if (_Cvoet8_responsetime==null){
				_Cvoet8_responsetime=getIntegerProperty("CVOET8/responseTime");
				return _Cvoet8_responsetime;
			}else {
				return _Cvoet8_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET8/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet8_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET8/responseTime",v);
		_Cvoet8_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet9_accuracy=null;

	/**
	 * @return Returns the CVOET9/accuracy.
	 */
	public Integer getCvoet9_accuracy() {
		try{
			if (_Cvoet9_accuracy==null){
				_Cvoet9_accuracy=getIntegerProperty("CVOET9/accuracy");
				return _Cvoet9_accuracy;
			}else {
				return _Cvoet9_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET9/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet9_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET9/accuracy",v);
		_Cvoet9_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet9_responsetime=null;

	/**
	 * @return Returns the CVOET9/responseTime.
	 */
	public Integer getCvoet9_responsetime() {
		try{
			if (_Cvoet9_responsetime==null){
				_Cvoet9_responsetime=getIntegerProperty("CVOET9/responseTime");
				return _Cvoet9_responsetime;
			}else {
				return _Cvoet9_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET9/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet9_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET9/responseTime",v);
		_Cvoet9_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet10_accuracy=null;

	/**
	 * @return Returns the CVOET10/accuracy.
	 */
	public Integer getCvoet10_accuracy() {
		try{
			if (_Cvoet10_accuracy==null){
				_Cvoet10_accuracy=getIntegerProperty("CVOET10/accuracy");
				return _Cvoet10_accuracy;
			}else {
				return _Cvoet10_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET10/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet10_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET10/accuracy",v);
		_Cvoet10_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet10_responsetime=null;

	/**
	 * @return Returns the CVOET10/responseTime.
	 */
	public Integer getCvoet10_responsetime() {
		try{
			if (_Cvoet10_responsetime==null){
				_Cvoet10_responsetime=getIntegerProperty("CVOET10/responseTime");
				return _Cvoet10_responsetime;
			}else {
				return _Cvoet10_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET10/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet10_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET10/responseTime",v);
		_Cvoet10_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet11_accuracy=null;

	/**
	 * @return Returns the CVOET11/accuracy.
	 */
	public Integer getCvoet11_accuracy() {
		try{
			if (_Cvoet11_accuracy==null){
				_Cvoet11_accuracy=getIntegerProperty("CVOET11/accuracy");
				return _Cvoet11_accuracy;
			}else {
				return _Cvoet11_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET11/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet11_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET11/accuracy",v);
		_Cvoet11_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet11_responsetime=null;

	/**
	 * @return Returns the CVOET11/responseTime.
	 */
	public Integer getCvoet11_responsetime() {
		try{
			if (_Cvoet11_responsetime==null){
				_Cvoet11_responsetime=getIntegerProperty("CVOET11/responseTime");
				return _Cvoet11_responsetime;
			}else {
				return _Cvoet11_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET11/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet11_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET11/responseTime",v);
		_Cvoet11_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet12_accuracy=null;

	/**
	 * @return Returns the CVOET12/accuracy.
	 */
	public Integer getCvoet12_accuracy() {
		try{
			if (_Cvoet12_accuracy==null){
				_Cvoet12_accuracy=getIntegerProperty("CVOET12/accuracy");
				return _Cvoet12_accuracy;
			}else {
				return _Cvoet12_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET12/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet12_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET12/accuracy",v);
		_Cvoet12_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet12_responsetime=null;

	/**
	 * @return Returns the CVOET12/responseTime.
	 */
	public Integer getCvoet12_responsetime() {
		try{
			if (_Cvoet12_responsetime==null){
				_Cvoet12_responsetime=getIntegerProperty("CVOET12/responseTime");
				return _Cvoet12_responsetime;
			}else {
				return _Cvoet12_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET12/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet12_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET12/responseTime",v);
		_Cvoet12_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet13_accuracy=null;

	/**
	 * @return Returns the CVOET13/accuracy.
	 */
	public Integer getCvoet13_accuracy() {
		try{
			if (_Cvoet13_accuracy==null){
				_Cvoet13_accuracy=getIntegerProperty("CVOET13/accuracy");
				return _Cvoet13_accuracy;
			}else {
				return _Cvoet13_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET13/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet13_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET13/accuracy",v);
		_Cvoet13_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet13_responsetime=null;

	/**
	 * @return Returns the CVOET13/responseTime.
	 */
	public Integer getCvoet13_responsetime() {
		try{
			if (_Cvoet13_responsetime==null){
				_Cvoet13_responsetime=getIntegerProperty("CVOET13/responseTime");
				return _Cvoet13_responsetime;
			}else {
				return _Cvoet13_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET13/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet13_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET13/responseTime",v);
		_Cvoet13_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet14_accuracy=null;

	/**
	 * @return Returns the CVOET14/accuracy.
	 */
	public Integer getCvoet14_accuracy() {
		try{
			if (_Cvoet14_accuracy==null){
				_Cvoet14_accuracy=getIntegerProperty("CVOET14/accuracy");
				return _Cvoet14_accuracy;
			}else {
				return _Cvoet14_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET14/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet14_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET14/accuracy",v);
		_Cvoet14_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet14_responsetime=null;

	/**
	 * @return Returns the CVOET14/responseTime.
	 */
	public Integer getCvoet14_responsetime() {
		try{
			if (_Cvoet14_responsetime==null){
				_Cvoet14_responsetime=getIntegerProperty("CVOET14/responseTime");
				return _Cvoet14_responsetime;
			}else {
				return _Cvoet14_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET14/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet14_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET14/responseTime",v);
		_Cvoet14_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet15_accuracy=null;

	/**
	 * @return Returns the CVOET15/accuracy.
	 */
	public Integer getCvoet15_accuracy() {
		try{
			if (_Cvoet15_accuracy==null){
				_Cvoet15_accuracy=getIntegerProperty("CVOET15/accuracy");
				return _Cvoet15_accuracy;
			}else {
				return _Cvoet15_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET15/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet15_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET15/accuracy",v);
		_Cvoet15_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet15_responsetime=null;

	/**
	 * @return Returns the CVOET15/responseTime.
	 */
	public Integer getCvoet15_responsetime() {
		try{
			if (_Cvoet15_responsetime==null){
				_Cvoet15_responsetime=getIntegerProperty("CVOET15/responseTime");
				return _Cvoet15_responsetime;
			}else {
				return _Cvoet15_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET15/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet15_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET15/responseTime",v);
		_Cvoet15_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet16_accuracy=null;

	/**
	 * @return Returns the CVOET16/accuracy.
	 */
	public Integer getCvoet16_accuracy() {
		try{
			if (_Cvoet16_accuracy==null){
				_Cvoet16_accuracy=getIntegerProperty("CVOET16/accuracy");
				return _Cvoet16_accuracy;
			}else {
				return _Cvoet16_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET16/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet16_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET16/accuracy",v);
		_Cvoet16_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet16_responsetime=null;

	/**
	 * @return Returns the CVOET16/responseTime.
	 */
	public Integer getCvoet16_responsetime() {
		try{
			if (_Cvoet16_responsetime==null){
				_Cvoet16_responsetime=getIntegerProperty("CVOET16/responseTime");
				return _Cvoet16_responsetime;
			}else {
				return _Cvoet16_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET16/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet16_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET16/responseTime",v);
		_Cvoet16_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet17_accuracy=null;

	/**
	 * @return Returns the CVOET17/accuracy.
	 */
	public Integer getCvoet17_accuracy() {
		try{
			if (_Cvoet17_accuracy==null){
				_Cvoet17_accuracy=getIntegerProperty("CVOET17/accuracy");
				return _Cvoet17_accuracy;
			}else {
				return _Cvoet17_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET17/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet17_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET17/accuracy",v);
		_Cvoet17_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet17_responsetime=null;

	/**
	 * @return Returns the CVOET17/responseTime.
	 */
	public Integer getCvoet17_responsetime() {
		try{
			if (_Cvoet17_responsetime==null){
				_Cvoet17_responsetime=getIntegerProperty("CVOET17/responseTime");
				return _Cvoet17_responsetime;
			}else {
				return _Cvoet17_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET17/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet17_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET17/responseTime",v);
		_Cvoet17_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet18_accuracy=null;

	/**
	 * @return Returns the CVOET18/accuracy.
	 */
	public Integer getCvoet18_accuracy() {
		try{
			if (_Cvoet18_accuracy==null){
				_Cvoet18_accuracy=getIntegerProperty("CVOET18/accuracy");
				return _Cvoet18_accuracy;
			}else {
				return _Cvoet18_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET18/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet18_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET18/accuracy",v);
		_Cvoet18_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet18_responsetime=null;

	/**
	 * @return Returns the CVOET18/responseTime.
	 */
	public Integer getCvoet18_responsetime() {
		try{
			if (_Cvoet18_responsetime==null){
				_Cvoet18_responsetime=getIntegerProperty("CVOET18/responseTime");
				return _Cvoet18_responsetime;
			}else {
				return _Cvoet18_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET18/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet18_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET18/responseTime",v);
		_Cvoet18_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet19_accuracy=null;

	/**
	 * @return Returns the CVOET19/accuracy.
	 */
	public Integer getCvoet19_accuracy() {
		try{
			if (_Cvoet19_accuracy==null){
				_Cvoet19_accuracy=getIntegerProperty("CVOET19/accuracy");
				return _Cvoet19_accuracy;
			}else {
				return _Cvoet19_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET19/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet19_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET19/accuracy",v);
		_Cvoet19_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet19_responsetime=null;

	/**
	 * @return Returns the CVOET19/responseTime.
	 */
	public Integer getCvoet19_responsetime() {
		try{
			if (_Cvoet19_responsetime==null){
				_Cvoet19_responsetime=getIntegerProperty("CVOET19/responseTime");
				return _Cvoet19_responsetime;
			}else {
				return _Cvoet19_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET19/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet19_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET19/responseTime",v);
		_Cvoet19_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet20_accuracy=null;

	/**
	 * @return Returns the CVOET20/accuracy.
	 */
	public Integer getCvoet20_accuracy() {
		try{
			if (_Cvoet20_accuracy==null){
				_Cvoet20_accuracy=getIntegerProperty("CVOET20/accuracy");
				return _Cvoet20_accuracy;
			}else {
				return _Cvoet20_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET20/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet20_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET20/accuracy",v);
		_Cvoet20_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet20_responsetime=null;

	/**
	 * @return Returns the CVOET20/responseTime.
	 */
	public Integer getCvoet20_responsetime() {
		try{
			if (_Cvoet20_responsetime==null){
				_Cvoet20_responsetime=getIntegerProperty("CVOET20/responseTime");
				return _Cvoet20_responsetime;
			}else {
				return _Cvoet20_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET20/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet20_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET20/responseTime",v);
		_Cvoet20_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet21_accuracy=null;

	/**
	 * @return Returns the CVOET21/accuracy.
	 */
	public Integer getCvoet21_accuracy() {
		try{
			if (_Cvoet21_accuracy==null){
				_Cvoet21_accuracy=getIntegerProperty("CVOET21/accuracy");
				return _Cvoet21_accuracy;
			}else {
				return _Cvoet21_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET21/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet21_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET21/accuracy",v);
		_Cvoet21_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet21_responsetime=null;

	/**
	 * @return Returns the CVOET21/responseTime.
	 */
	public Integer getCvoet21_responsetime() {
		try{
			if (_Cvoet21_responsetime==null){
				_Cvoet21_responsetime=getIntegerProperty("CVOET21/responseTime");
				return _Cvoet21_responsetime;
			}else {
				return _Cvoet21_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET21/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet21_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET21/responseTime",v);
		_Cvoet21_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet22_accuracy=null;

	/**
	 * @return Returns the CVOET22/accuracy.
	 */
	public Integer getCvoet22_accuracy() {
		try{
			if (_Cvoet22_accuracy==null){
				_Cvoet22_accuracy=getIntegerProperty("CVOET22/accuracy");
				return _Cvoet22_accuracy;
			}else {
				return _Cvoet22_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET22/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet22_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET22/accuracy",v);
		_Cvoet22_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet22_responsetime=null;

	/**
	 * @return Returns the CVOET22/responseTime.
	 */
	public Integer getCvoet22_responsetime() {
		try{
			if (_Cvoet22_responsetime==null){
				_Cvoet22_responsetime=getIntegerProperty("CVOET22/responseTime");
				return _Cvoet22_responsetime;
			}else {
				return _Cvoet22_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET22/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet22_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET22/responseTime",v);
		_Cvoet22_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet23_accuracy=null;

	/**
	 * @return Returns the CVOET23/accuracy.
	 */
	public Integer getCvoet23_accuracy() {
		try{
			if (_Cvoet23_accuracy==null){
				_Cvoet23_accuracy=getIntegerProperty("CVOET23/accuracy");
				return _Cvoet23_accuracy;
			}else {
				return _Cvoet23_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET23/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet23_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET23/accuracy",v);
		_Cvoet23_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet23_responsetime=null;

	/**
	 * @return Returns the CVOET23/responseTime.
	 */
	public Integer getCvoet23_responsetime() {
		try{
			if (_Cvoet23_responsetime==null){
				_Cvoet23_responsetime=getIntegerProperty("CVOET23/responseTime");
				return _Cvoet23_responsetime;
			}else {
				return _Cvoet23_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET23/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet23_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET23/responseTime",v);
		_Cvoet23_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet24_accuracy=null;

	/**
	 * @return Returns the CVOET24/accuracy.
	 */
	public Integer getCvoet24_accuracy() {
		try{
			if (_Cvoet24_accuracy==null){
				_Cvoet24_accuracy=getIntegerProperty("CVOET24/accuracy");
				return _Cvoet24_accuracy;
			}else {
				return _Cvoet24_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET24/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet24_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET24/accuracy",v);
		_Cvoet24_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet24_responsetime=null;

	/**
	 * @return Returns the CVOET24/responseTime.
	 */
	public Integer getCvoet24_responsetime() {
		try{
			if (_Cvoet24_responsetime==null){
				_Cvoet24_responsetime=getIntegerProperty("CVOET24/responseTime");
				return _Cvoet24_responsetime;
			}else {
				return _Cvoet24_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET24/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet24_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET24/responseTime",v);
		_Cvoet24_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet25_accuracy=null;

	/**
	 * @return Returns the CVOET25/accuracy.
	 */
	public Integer getCvoet25_accuracy() {
		try{
			if (_Cvoet25_accuracy==null){
				_Cvoet25_accuracy=getIntegerProperty("CVOET25/accuracy");
				return _Cvoet25_accuracy;
			}else {
				return _Cvoet25_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET25/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet25_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET25/accuracy",v);
		_Cvoet25_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet25_responsetime=null;

	/**
	 * @return Returns the CVOET25/responseTime.
	 */
	public Integer getCvoet25_responsetime() {
		try{
			if (_Cvoet25_responsetime==null){
				_Cvoet25_responsetime=getIntegerProperty("CVOET25/responseTime");
				return _Cvoet25_responsetime;
			}else {
				return _Cvoet25_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET25/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet25_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET25/responseTime",v);
		_Cvoet25_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet26_accuracy=null;

	/**
	 * @return Returns the CVOET26/accuracy.
	 */
	public Integer getCvoet26_accuracy() {
		try{
			if (_Cvoet26_accuracy==null){
				_Cvoet26_accuracy=getIntegerProperty("CVOET26/accuracy");
				return _Cvoet26_accuracy;
			}else {
				return _Cvoet26_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET26/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet26_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET26/accuracy",v);
		_Cvoet26_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet26_responsetime=null;

	/**
	 * @return Returns the CVOET26/responseTime.
	 */
	public Integer getCvoet26_responsetime() {
		try{
			if (_Cvoet26_responsetime==null){
				_Cvoet26_responsetime=getIntegerProperty("CVOET26/responseTime");
				return _Cvoet26_responsetime;
			}else {
				return _Cvoet26_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET26/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet26_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET26/responseTime",v);
		_Cvoet26_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet27_accuracy=null;

	/**
	 * @return Returns the CVOET27/accuracy.
	 */
	public Integer getCvoet27_accuracy() {
		try{
			if (_Cvoet27_accuracy==null){
				_Cvoet27_accuracy=getIntegerProperty("CVOET27/accuracy");
				return _Cvoet27_accuracy;
			}else {
				return _Cvoet27_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET27/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet27_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET27/accuracy",v);
		_Cvoet27_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet27_responsetime=null;

	/**
	 * @return Returns the CVOET27/responseTime.
	 */
	public Integer getCvoet27_responsetime() {
		try{
			if (_Cvoet27_responsetime==null){
				_Cvoet27_responsetime=getIntegerProperty("CVOET27/responseTime");
				return _Cvoet27_responsetime;
			}else {
				return _Cvoet27_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET27/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet27_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET27/responseTime",v);
		_Cvoet27_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet28_accuracy=null;

	/**
	 * @return Returns the CVOET28/accuracy.
	 */
	public Integer getCvoet28_accuracy() {
		try{
			if (_Cvoet28_accuracy==null){
				_Cvoet28_accuracy=getIntegerProperty("CVOET28/accuracy");
				return _Cvoet28_accuracy;
			}else {
				return _Cvoet28_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET28/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet28_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET28/accuracy",v);
		_Cvoet28_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet28_responsetime=null;

	/**
	 * @return Returns the CVOET28/responseTime.
	 */
	public Integer getCvoet28_responsetime() {
		try{
			if (_Cvoet28_responsetime==null){
				_Cvoet28_responsetime=getIntegerProperty("CVOET28/responseTime");
				return _Cvoet28_responsetime;
			}else {
				return _Cvoet28_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET28/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet28_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET28/responseTime",v);
		_Cvoet28_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet29_accuracy=null;

	/**
	 * @return Returns the CVOET29/accuracy.
	 */
	public Integer getCvoet29_accuracy() {
		try{
			if (_Cvoet29_accuracy==null){
				_Cvoet29_accuracy=getIntegerProperty("CVOET29/accuracy");
				return _Cvoet29_accuracy;
			}else {
				return _Cvoet29_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET29/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet29_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET29/accuracy",v);
		_Cvoet29_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet29_responsetime=null;

	/**
	 * @return Returns the CVOET29/responseTime.
	 */
	public Integer getCvoet29_responsetime() {
		try{
			if (_Cvoet29_responsetime==null){
				_Cvoet29_responsetime=getIntegerProperty("CVOET29/responseTime");
				return _Cvoet29_responsetime;
			}else {
				return _Cvoet29_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET29/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet29_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET29/responseTime",v);
		_Cvoet29_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet30_accuracy=null;

	/**
	 * @return Returns the CVOET30/accuracy.
	 */
	public Integer getCvoet30_accuracy() {
		try{
			if (_Cvoet30_accuracy==null){
				_Cvoet30_accuracy=getIntegerProperty("CVOET30/accuracy");
				return _Cvoet30_accuracy;
			}else {
				return _Cvoet30_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET30/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet30_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET30/accuracy",v);
		_Cvoet30_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet30_responsetime=null;

	/**
	 * @return Returns the CVOET30/responseTime.
	 */
	public Integer getCvoet30_responsetime() {
		try{
			if (_Cvoet30_responsetime==null){
				_Cvoet30_responsetime=getIntegerProperty("CVOET30/responseTime");
				return _Cvoet30_responsetime;
			}else {
				return _Cvoet30_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET30/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet30_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET30/responseTime",v);
		_Cvoet30_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet31_accuracy=null;

	/**
	 * @return Returns the CVOET31/accuracy.
	 */
	public Integer getCvoet31_accuracy() {
		try{
			if (_Cvoet31_accuracy==null){
				_Cvoet31_accuracy=getIntegerProperty("CVOET31/accuracy");
				return _Cvoet31_accuracy;
			}else {
				return _Cvoet31_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET31/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet31_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET31/accuracy",v);
		_Cvoet31_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet31_responsetime=null;

	/**
	 * @return Returns the CVOET31/responseTime.
	 */
	public Integer getCvoet31_responsetime() {
		try{
			if (_Cvoet31_responsetime==null){
				_Cvoet31_responsetime=getIntegerProperty("CVOET31/responseTime");
				return _Cvoet31_responsetime;
			}else {
				return _Cvoet31_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET31/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet31_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET31/responseTime",v);
		_Cvoet31_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet32_accuracy=null;

	/**
	 * @return Returns the CVOET32/accuracy.
	 */
	public Integer getCvoet32_accuracy() {
		try{
			if (_Cvoet32_accuracy==null){
				_Cvoet32_accuracy=getIntegerProperty("CVOET32/accuracy");
				return _Cvoet32_accuracy;
			}else {
				return _Cvoet32_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET32/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet32_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET32/accuracy",v);
		_Cvoet32_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet32_responsetime=null;

	/**
	 * @return Returns the CVOET32/responseTime.
	 */
	public Integer getCvoet32_responsetime() {
		try{
			if (_Cvoet32_responsetime==null){
				_Cvoet32_responsetime=getIntegerProperty("CVOET32/responseTime");
				return _Cvoet32_responsetime;
			}else {
				return _Cvoet32_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET32/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet32_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET32/responseTime",v);
		_Cvoet32_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet33_accuracy=null;

	/**
	 * @return Returns the CVOET33/accuracy.
	 */
	public Integer getCvoet33_accuracy() {
		try{
			if (_Cvoet33_accuracy==null){
				_Cvoet33_accuracy=getIntegerProperty("CVOET33/accuracy");
				return _Cvoet33_accuracy;
			}else {
				return _Cvoet33_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET33/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet33_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET33/accuracy",v);
		_Cvoet33_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet33_responsetime=null;

	/**
	 * @return Returns the CVOET33/responseTime.
	 */
	public Integer getCvoet33_responsetime() {
		try{
			if (_Cvoet33_responsetime==null){
				_Cvoet33_responsetime=getIntegerProperty("CVOET33/responseTime");
				return _Cvoet33_responsetime;
			}else {
				return _Cvoet33_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET33/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet33_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET33/responseTime",v);
		_Cvoet33_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet34_accuracy=null;

	/**
	 * @return Returns the CVOET34/accuracy.
	 */
	public Integer getCvoet34_accuracy() {
		try{
			if (_Cvoet34_accuracy==null){
				_Cvoet34_accuracy=getIntegerProperty("CVOET34/accuracy");
				return _Cvoet34_accuracy;
			}else {
				return _Cvoet34_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET34/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet34_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET34/accuracy",v);
		_Cvoet34_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet34_responsetime=null;

	/**
	 * @return Returns the CVOET34/responseTime.
	 */
	public Integer getCvoet34_responsetime() {
		try{
			if (_Cvoet34_responsetime==null){
				_Cvoet34_responsetime=getIntegerProperty("CVOET34/responseTime");
				return _Cvoet34_responsetime;
			}else {
				return _Cvoet34_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET34/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet34_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET34/responseTime",v);
		_Cvoet34_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet35_accuracy=null;

	/**
	 * @return Returns the CVOET35/accuracy.
	 */
	public Integer getCvoet35_accuracy() {
		try{
			if (_Cvoet35_accuracy==null){
				_Cvoet35_accuracy=getIntegerProperty("CVOET35/accuracy");
				return _Cvoet35_accuracy;
			}else {
				return _Cvoet35_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET35/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet35_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET35/accuracy",v);
		_Cvoet35_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet35_responsetime=null;

	/**
	 * @return Returns the CVOET35/responseTime.
	 */
	public Integer getCvoet35_responsetime() {
		try{
			if (_Cvoet35_responsetime==null){
				_Cvoet35_responsetime=getIntegerProperty("CVOET35/responseTime");
				return _Cvoet35_responsetime;
			}else {
				return _Cvoet35_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET35/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet35_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET35/responseTime",v);
		_Cvoet35_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet36_accuracy=null;

	/**
	 * @return Returns the CVOET36/accuracy.
	 */
	public Integer getCvoet36_accuracy() {
		try{
			if (_Cvoet36_accuracy==null){
				_Cvoet36_accuracy=getIntegerProperty("CVOET36/accuracy");
				return _Cvoet36_accuracy;
			}else {
				return _Cvoet36_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET36/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet36_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET36/accuracy",v);
		_Cvoet36_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet36_responsetime=null;

	/**
	 * @return Returns the CVOET36/responseTime.
	 */
	public Integer getCvoet36_responsetime() {
		try{
			if (_Cvoet36_responsetime==null){
				_Cvoet36_responsetime=getIntegerProperty("CVOET36/responseTime");
				return _Cvoet36_responsetime;
			}else {
				return _Cvoet36_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET36/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet36_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET36/responseTime",v);
		_Cvoet36_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet37_accuracy=null;

	/**
	 * @return Returns the CVOET37/accuracy.
	 */
	public Integer getCvoet37_accuracy() {
		try{
			if (_Cvoet37_accuracy==null){
				_Cvoet37_accuracy=getIntegerProperty("CVOET37/accuracy");
				return _Cvoet37_accuracy;
			}else {
				return _Cvoet37_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET37/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet37_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET37/accuracy",v);
		_Cvoet37_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet37_responsetime=null;

	/**
	 * @return Returns the CVOET37/responseTime.
	 */
	public Integer getCvoet37_responsetime() {
		try{
			if (_Cvoet37_responsetime==null){
				_Cvoet37_responsetime=getIntegerProperty("CVOET37/responseTime");
				return _Cvoet37_responsetime;
			}else {
				return _Cvoet37_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET37/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet37_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET37/responseTime",v);
		_Cvoet37_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet38_accuracy=null;

	/**
	 * @return Returns the CVOET38/accuracy.
	 */
	public Integer getCvoet38_accuracy() {
		try{
			if (_Cvoet38_accuracy==null){
				_Cvoet38_accuracy=getIntegerProperty("CVOET38/accuracy");
				return _Cvoet38_accuracy;
			}else {
				return _Cvoet38_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET38/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet38_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET38/accuracy",v);
		_Cvoet38_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet38_responsetime=null;

	/**
	 * @return Returns the CVOET38/responseTime.
	 */
	public Integer getCvoet38_responsetime() {
		try{
			if (_Cvoet38_responsetime==null){
				_Cvoet38_responsetime=getIntegerProperty("CVOET38/responseTime");
				return _Cvoet38_responsetime;
			}else {
				return _Cvoet38_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET38/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet38_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET38/responseTime",v);
		_Cvoet38_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet39_accuracy=null;

	/**
	 * @return Returns the CVOET39/accuracy.
	 */
	public Integer getCvoet39_accuracy() {
		try{
			if (_Cvoet39_accuracy==null){
				_Cvoet39_accuracy=getIntegerProperty("CVOET39/accuracy");
				return _Cvoet39_accuracy;
			}else {
				return _Cvoet39_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET39/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet39_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET39/accuracy",v);
		_Cvoet39_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet39_responsetime=null;

	/**
	 * @return Returns the CVOET39/responseTime.
	 */
	public Integer getCvoet39_responsetime() {
		try{
			if (_Cvoet39_responsetime==null){
				_Cvoet39_responsetime=getIntegerProperty("CVOET39/responseTime");
				return _Cvoet39_responsetime;
			}else {
				return _Cvoet39_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET39/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet39_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET39/responseTime",v);
		_Cvoet39_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet40_accuracy=null;

	/**
	 * @return Returns the CVOET40/accuracy.
	 */
	public Integer getCvoet40_accuracy() {
		try{
			if (_Cvoet40_accuracy==null){
				_Cvoet40_accuracy=getIntegerProperty("CVOET40/accuracy");
				return _Cvoet40_accuracy;
			}else {
				return _Cvoet40_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET40/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet40_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET40/accuracy",v);
		_Cvoet40_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet40_responsetime=null;

	/**
	 * @return Returns the CVOET40/responseTime.
	 */
	public Integer getCvoet40_responsetime() {
		try{
			if (_Cvoet40_responsetime==null){
				_Cvoet40_responsetime=getIntegerProperty("CVOET40/responseTime");
				return _Cvoet40_responsetime;
			}else {
				return _Cvoet40_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET40/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet40_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET40/responseTime",v);
		_Cvoet40_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet41_accuracy=null;

	/**
	 * @return Returns the CVOET41/accuracy.
	 */
	public Integer getCvoet41_accuracy() {
		try{
			if (_Cvoet41_accuracy==null){
				_Cvoet41_accuracy=getIntegerProperty("CVOET41/accuracy");
				return _Cvoet41_accuracy;
			}else {
				return _Cvoet41_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET41/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet41_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET41/accuracy",v);
		_Cvoet41_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet41_responsetime=null;

	/**
	 * @return Returns the CVOET41/responseTime.
	 */
	public Integer getCvoet41_responsetime() {
		try{
			if (_Cvoet41_responsetime==null){
				_Cvoet41_responsetime=getIntegerProperty("CVOET41/responseTime");
				return _Cvoet41_responsetime;
			}else {
				return _Cvoet41_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET41/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet41_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET41/responseTime",v);
		_Cvoet41_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet42_accuracy=null;

	/**
	 * @return Returns the CVOET42/accuracy.
	 */
	public Integer getCvoet42_accuracy() {
		try{
			if (_Cvoet42_accuracy==null){
				_Cvoet42_accuracy=getIntegerProperty("CVOET42/accuracy");
				return _Cvoet42_accuracy;
			}else {
				return _Cvoet42_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET42/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet42_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET42/accuracy",v);
		_Cvoet42_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet42_responsetime=null;

	/**
	 * @return Returns the CVOET42/responseTime.
	 */
	public Integer getCvoet42_responsetime() {
		try{
			if (_Cvoet42_responsetime==null){
				_Cvoet42_responsetime=getIntegerProperty("CVOET42/responseTime");
				return _Cvoet42_responsetime;
			}else {
				return _Cvoet42_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET42/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet42_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET42/responseTime",v);
		_Cvoet42_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet43_accuracy=null;

	/**
	 * @return Returns the CVOET43/accuracy.
	 */
	public Integer getCvoet43_accuracy() {
		try{
			if (_Cvoet43_accuracy==null){
				_Cvoet43_accuracy=getIntegerProperty("CVOET43/accuracy");
				return _Cvoet43_accuracy;
			}else {
				return _Cvoet43_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET43/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet43_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET43/accuracy",v);
		_Cvoet43_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet43_responsetime=null;

	/**
	 * @return Returns the CVOET43/responseTime.
	 */
	public Integer getCvoet43_responsetime() {
		try{
			if (_Cvoet43_responsetime==null){
				_Cvoet43_responsetime=getIntegerProperty("CVOET43/responseTime");
				return _Cvoet43_responsetime;
			}else {
				return _Cvoet43_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET43/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet43_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET43/responseTime",v);
		_Cvoet43_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet44_accuracy=null;

	/**
	 * @return Returns the CVOET44/accuracy.
	 */
	public Integer getCvoet44_accuracy() {
		try{
			if (_Cvoet44_accuracy==null){
				_Cvoet44_accuracy=getIntegerProperty("CVOET44/accuracy");
				return _Cvoet44_accuracy;
			}else {
				return _Cvoet44_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET44/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet44_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET44/accuracy",v);
		_Cvoet44_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet44_responsetime=null;

	/**
	 * @return Returns the CVOET44/responseTime.
	 */
	public Integer getCvoet44_responsetime() {
		try{
			if (_Cvoet44_responsetime==null){
				_Cvoet44_responsetime=getIntegerProperty("CVOET44/responseTime");
				return _Cvoet44_responsetime;
			}else {
				return _Cvoet44_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET44/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet44_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET44/responseTime",v);
		_Cvoet44_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet45_accuracy=null;

	/**
	 * @return Returns the CVOET45/accuracy.
	 */
	public Integer getCvoet45_accuracy() {
		try{
			if (_Cvoet45_accuracy==null){
				_Cvoet45_accuracy=getIntegerProperty("CVOET45/accuracy");
				return _Cvoet45_accuracy;
			}else {
				return _Cvoet45_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET45/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet45_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET45/accuracy",v);
		_Cvoet45_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet45_responsetime=null;

	/**
	 * @return Returns the CVOET45/responseTime.
	 */
	public Integer getCvoet45_responsetime() {
		try{
			if (_Cvoet45_responsetime==null){
				_Cvoet45_responsetime=getIntegerProperty("CVOET45/responseTime");
				return _Cvoet45_responsetime;
			}else {
				return _Cvoet45_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET45/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet45_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET45/responseTime",v);
		_Cvoet45_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet46_accuracy=null;

	/**
	 * @return Returns the CVOET46/accuracy.
	 */
	public Integer getCvoet46_accuracy() {
		try{
			if (_Cvoet46_accuracy==null){
				_Cvoet46_accuracy=getIntegerProperty("CVOET46/accuracy");
				return _Cvoet46_accuracy;
			}else {
				return _Cvoet46_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET46/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet46_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET46/accuracy",v);
		_Cvoet46_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet46_responsetime=null;

	/**
	 * @return Returns the CVOET46/responseTime.
	 */
	public Integer getCvoet46_responsetime() {
		try{
			if (_Cvoet46_responsetime==null){
				_Cvoet46_responsetime=getIntegerProperty("CVOET46/responseTime");
				return _Cvoet46_responsetime;
			}else {
				return _Cvoet46_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET46/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet46_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET46/responseTime",v);
		_Cvoet46_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet47_accuracy=null;

	/**
	 * @return Returns the CVOET47/accuracy.
	 */
	public Integer getCvoet47_accuracy() {
		try{
			if (_Cvoet47_accuracy==null){
				_Cvoet47_accuracy=getIntegerProperty("CVOET47/accuracy");
				return _Cvoet47_accuracy;
			}else {
				return _Cvoet47_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET47/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet47_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET47/accuracy",v);
		_Cvoet47_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet47_responsetime=null;

	/**
	 * @return Returns the CVOET47/responseTime.
	 */
	public Integer getCvoet47_responsetime() {
		try{
			if (_Cvoet47_responsetime==null){
				_Cvoet47_responsetime=getIntegerProperty("CVOET47/responseTime");
				return _Cvoet47_responsetime;
			}else {
				return _Cvoet47_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET47/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet47_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET47/responseTime",v);
		_Cvoet47_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet48_accuracy=null;

	/**
	 * @return Returns the CVOET48/accuracy.
	 */
	public Integer getCvoet48_accuracy() {
		try{
			if (_Cvoet48_accuracy==null){
				_Cvoet48_accuracy=getIntegerProperty("CVOET48/accuracy");
				return _Cvoet48_accuracy;
			}else {
				return _Cvoet48_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET48/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet48_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET48/accuracy",v);
		_Cvoet48_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet48_responsetime=null;

	/**
	 * @return Returns the CVOET48/responseTime.
	 */
	public Integer getCvoet48_responsetime() {
		try{
			if (_Cvoet48_responsetime==null){
				_Cvoet48_responsetime=getIntegerProperty("CVOET48/responseTime");
				return _Cvoet48_responsetime;
			}else {
				return _Cvoet48_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET48/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet48_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET48/responseTime",v);
		_Cvoet48_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet49_accuracy=null;

	/**
	 * @return Returns the CVOET49/accuracy.
	 */
	public Integer getCvoet49_accuracy() {
		try{
			if (_Cvoet49_accuracy==null){
				_Cvoet49_accuracy=getIntegerProperty("CVOET49/accuracy");
				return _Cvoet49_accuracy;
			}else {
				return _Cvoet49_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET49/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet49_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET49/accuracy",v);
		_Cvoet49_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet49_responsetime=null;

	/**
	 * @return Returns the CVOET49/responseTime.
	 */
	public Integer getCvoet49_responsetime() {
		try{
			if (_Cvoet49_responsetime==null){
				_Cvoet49_responsetime=getIntegerProperty("CVOET49/responseTime");
				return _Cvoet49_responsetime;
			}else {
				return _Cvoet49_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET49/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet49_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET49/responseTime",v);
		_Cvoet49_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet50_accuracy=null;

	/**
	 * @return Returns the CVOET50/accuracy.
	 */
	public Integer getCvoet50_accuracy() {
		try{
			if (_Cvoet50_accuracy==null){
				_Cvoet50_accuracy=getIntegerProperty("CVOET50/accuracy");
				return _Cvoet50_accuracy;
			}else {
				return _Cvoet50_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET50/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet50_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET50/accuracy",v);
		_Cvoet50_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet50_responsetime=null;

	/**
	 * @return Returns the CVOET50/responseTime.
	 */
	public Integer getCvoet50_responsetime() {
		try{
			if (_Cvoet50_responsetime==null){
				_Cvoet50_responsetime=getIntegerProperty("CVOET50/responseTime");
				return _Cvoet50_responsetime;
			}else {
				return _Cvoet50_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET50/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet50_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET50/responseTime",v);
		_Cvoet50_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet51_accuracy=null;

	/**
	 * @return Returns the CVOET51/accuracy.
	 */
	public Integer getCvoet51_accuracy() {
		try{
			if (_Cvoet51_accuracy==null){
				_Cvoet51_accuracy=getIntegerProperty("CVOET51/accuracy");
				return _Cvoet51_accuracy;
			}else {
				return _Cvoet51_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET51/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet51_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET51/accuracy",v);
		_Cvoet51_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet51_responsetime=null;

	/**
	 * @return Returns the CVOET51/responseTime.
	 */
	public Integer getCvoet51_responsetime() {
		try{
			if (_Cvoet51_responsetime==null){
				_Cvoet51_responsetime=getIntegerProperty("CVOET51/responseTime");
				return _Cvoet51_responsetime;
			}else {
				return _Cvoet51_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET51/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet51_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET51/responseTime",v);
		_Cvoet51_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet52_accuracy=null;

	/**
	 * @return Returns the CVOET52/accuracy.
	 */
	public Integer getCvoet52_accuracy() {
		try{
			if (_Cvoet52_accuracy==null){
				_Cvoet52_accuracy=getIntegerProperty("CVOET52/accuracy");
				return _Cvoet52_accuracy;
			}else {
				return _Cvoet52_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET52/accuracy.
	 * @param v Value to Set.
	 */
	public void setCvoet52_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET52/accuracy",v);
		_Cvoet52_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvoet52_responsetime=null;

	/**
	 * @return Returns the CVOET52/responseTime.
	 */
	public Integer getCvoet52_responsetime() {
		try{
			if (_Cvoet52_responsetime==null){
				_Cvoet52_responsetime=getIntegerProperty("CVOET52/responseTime");
				return _Cvoet52_responsetime;
			}else {
				return _Cvoet52_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVOET52/responseTime.
	 * @param v Value to Set.
	 */
	public void setCvoet52_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVOET52/responseTime",v);
		_Cvoet52_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CbatCvoe> getAllCbatCvoes(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CbatCvoe> al = new ArrayList<org.nrg.xdat.om.CbatCvoe>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CbatCvoe> getCbatCvoesByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CbatCvoe> al = new ArrayList<org.nrg.xdat.om.CbatCvoe>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CbatCvoe> getCbatCvoesByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CbatCvoe> al = new ArrayList<org.nrg.xdat.om.CbatCvoe>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CbatCvoe getCbatCvoesById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cbat:CVOE/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CbatCvoe) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
