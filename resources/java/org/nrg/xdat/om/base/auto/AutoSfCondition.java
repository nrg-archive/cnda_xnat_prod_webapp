/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoSfCondition extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.SfConditionI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoSfCondition.class);
	public static String SCHEMA_ELEMENT_NAME="sf:condition";

	public AutoSfCondition(ItemI item)
	{
		super(item);
	}

	public AutoSfCondition(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoSfCondition(UserI user)
	 **/
	public AutoSfCondition(){}

	public AutoSfCondition(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "sf:condition";
	}

	//FIELD

	private String _Conditionstatus=null;

	/**
	 * @return Returns the conditionStatus.
	 */
	public String getConditionstatus(){
		try{
			if (_Conditionstatus==null){
				_Conditionstatus=getStringProperty("conditionStatus");
				return _Conditionstatus;
			}else {
				return _Conditionstatus;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for conditionStatus.
	 * @param v Value to Set.
	 */
	public void setConditionstatus(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/conditionStatus",v);
		_Conditionstatus=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Bodysystem=null;

	/**
	 * @return Returns the bodySystem.
	 */
	public String getBodysystem(){
		try{
			if (_Bodysystem==null){
				_Bodysystem=getStringProperty("bodySystem");
				return _Bodysystem;
			}else {
				return _Bodysystem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for bodySystem.
	 * @param v Value to Set.
	 */
	public void setBodysystem(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/bodySystem",v);
		_Bodysystem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Condition=null;

	/**
	 * @return Returns the condition.
	 */
	public String getCondition(){
		try{
			if (_Condition==null){
				_Condition=getStringProperty("condition");
				return _Condition;
			}else {
				return _Condition;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for condition.
	 * @param v Value to Set.
	 */
	public void setCondition(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/condition",v);
		_Condition=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Conditionnotes=null;

	/**
	 * @return Returns the conditionNotes.
	 */
	public String getConditionnotes(){
		try{
			if (_Conditionnotes==null){
				_Conditionnotes=getStringProperty("conditionNotes");
				return _Conditionnotes;
			}else {
				return _Conditionnotes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for conditionNotes.
	 * @param v Value to Set.
	 */
	public void setConditionnotes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/conditionNotes",v);
		_Conditionnotes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Diagnosisdate=null;

	/**
	 * @return Returns the diagnosisDate.
	 */
	public Object getDiagnosisdate(){
		try{
			if (_Diagnosisdate==null){
				_Diagnosisdate=getProperty("diagnosisDate");
				return _Diagnosisdate;
			}else {
				return _Diagnosisdate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for diagnosisDate.
	 * @param v Value to Set.
	 */
	public void setDiagnosisdate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/diagnosisDate",v);
		_Diagnosisdate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Diagnosisdatedaynotreported=null;

	/**
	 * @return Returns the diagnosisDateDayNotReported.
	 */
	public Boolean getDiagnosisdatedaynotreported() {
		try{
			if (_Diagnosisdatedaynotreported==null){
				_Diagnosisdatedaynotreported=getBooleanProperty("diagnosisDateDayNotReported");
				return _Diagnosisdatedaynotreported;
			}else {
				return _Diagnosisdatedaynotreported;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for diagnosisDateDayNotReported.
	 * @param v Value to Set.
	 */
	public void setDiagnosisdatedaynotreported(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/diagnosisDateDayNotReported",v);
		_Diagnosisdatedaynotreported=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Diagnosisdatemonthnotreported=null;

	/**
	 * @return Returns the diagnosisDateMonthNotReported.
	 */
	public Boolean getDiagnosisdatemonthnotreported() {
		try{
			if (_Diagnosisdatemonthnotreported==null){
				_Diagnosisdatemonthnotreported=getBooleanProperty("diagnosisDateMonthNotReported");
				return _Diagnosisdatemonthnotreported;
			}else {
				return _Diagnosisdatemonthnotreported;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for diagnosisDateMonthNotReported.
	 * @param v Value to Set.
	 */
	public void setDiagnosisdatemonthnotreported(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/diagnosisDateMonthNotReported",v);
		_Diagnosisdatemonthnotreported=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Diagnosisdateyearnotreported=null;

	/**
	 * @return Returns the diagnosisDateYearNotReported.
	 */
	public Boolean getDiagnosisdateyearnotreported() {
		try{
			if (_Diagnosisdateyearnotreported==null){
				_Diagnosisdateyearnotreported=getBooleanProperty("diagnosisDateYearNotReported");
				return _Diagnosisdateyearnotreported;
			}else {
				return _Diagnosisdateyearnotreported;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for diagnosisDateYearNotReported.
	 * @param v Value to Set.
	 */
	public void setDiagnosisdateyearnotreported(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/diagnosisDateYearNotReported",v);
		_Diagnosisdateyearnotreported=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _SfConditionId=null;

	/**
	 * @return Returns the sf_condition_id.
	 */
	public Integer getSfConditionId() {
		try{
			if (_SfConditionId==null){
				_SfConditionId=getIntegerProperty("sf_condition_id");
				return _SfConditionId;
			}else {
				return _SfConditionId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sf_condition_id.
	 * @param v Value to Set.
	 */
	public void setSfConditionId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sf_condition_id",v);
		_SfConditionId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.SfCondition> getAllSfConditions(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfCondition> al = new ArrayList<org.nrg.xdat.om.SfCondition>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.SfCondition> getSfConditionsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfCondition> al = new ArrayList<org.nrg.xdat.om.SfCondition>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.SfCondition> getSfConditionsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfCondition> al = new ArrayList<org.nrg.xdat.om.SfCondition>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static SfCondition getSfConditionsBySfConditionId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("sf:condition/sf_condition_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (SfCondition) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
