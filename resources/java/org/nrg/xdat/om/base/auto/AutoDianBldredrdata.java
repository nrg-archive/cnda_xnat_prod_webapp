/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:00 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianBldredrdata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianBldredrdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianBldredrdata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:bldredrData";

	public AutoDianBldredrdata(ItemI item)
	{
		super(item);
	}

	public AutoDianBldredrdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianBldredrdata(UserI user)
	 **/
	public AutoDianBldredrdata(){}

	public AutoDianBldredrdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:bldredrData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Biocoll=null;

	/**
	 * @return Returns the BIOCOLL.
	 */
	public Integer getBiocoll() {
		try{
			if (_Biocoll==null){
				_Biocoll=getIntegerProperty("BIOCOLL");
				return _Biocoll;
			}else {
				return _Biocoll;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BIOCOLL.
	 * @param v Value to Set.
	 */
	public void setBiocoll(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BIOCOLL",v);
		_Biocoll=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Biodate=null;

	/**
	 * @return Returns the BIODATE.
	 */
	public Object getBiodate(){
		try{
			if (_Biodate==null){
				_Biodate=getProperty("BIODATE");
				return _Biodate;
			}else {
				return _Biodate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BIODATE.
	 * @param v Value to Set.
	 */
	public void setBiodate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BIODATE",v);
		_Biodate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Biotime=null;

	/**
	 * @return Returns the BIOTIME.
	 */
	public Integer getBiotime() {
		try{
			if (_Biotime==null){
				_Biotime=getIntegerProperty("BIOTIME");
				return _Biotime;
			}else {
				return _Biotime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BIOTIME.
	 * @param v Value to Set.
	 */
	public void setBiotime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BIOTIME",v);
		_Biotime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Biofast=null;

	/**
	 * @return Returns the BIOFAST.
	 */
	public Integer getBiofast() {
		try{
			if (_Biofast==null){
				_Biofast=getIntegerProperty("BIOFAST");
				return _Biofast;
			}else {
				return _Biofast;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BIOFAST.
	 * @param v Value to Set.
	 */
	public void setBiofast(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BIOFAST",v);
		_Biofast=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Plasvol=null;

	/**
	 * @return Returns the PLASVOL.
	 */
	public Integer getPlasvol() {
		try{
			if (_Plasvol==null){
				_Plasvol=getIntegerProperty("PLASVOL");
				return _Plasvol;
			}else {
				return _Plasvol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PLASVOL.
	 * @param v Value to Set.
	 */
	public void setPlasvol(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PLASVOL",v);
		_Plasvol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Plasfroz=null;

	/**
	 * @return Returns the PLASFROZ.
	 */
	public Integer getPlasfroz() {
		try{
			if (_Plasfroz==null){
				_Plasfroz=getIntegerProperty("PLASFROZ");
				return _Plasfroz;
			}else {
				return _Plasfroz;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PLASFROZ.
	 * @param v Value to Set.
	 */
	public void setPlasfroz(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PLASFROZ",v);
		_Plasfroz=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Bioship=null;

	/**
	 * @return Returns the BIOSHIP.
	 */
	public Integer getBioship() {
		try{
			if (_Bioship==null){
				_Bioship=getIntegerProperty("BIOSHIP");
				return _Bioship;
			}else {
				return _Bioship;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BIOSHIP.
	 * @param v Value to Set.
	 */
	public void setBioship(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BIOSHIP",v);
		_Bioship=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Bioshdt=null;

	/**
	 * @return Returns the BIOSHDT.
	 */
	public Object getBioshdt(){
		try{
			if (_Bioshdt==null){
				_Bioshdt=getProperty("BIOSHDT");
				return _Bioshdt;
			}else {
				return _Bioshdt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BIOSHDT.
	 * @param v Value to Set.
	 */
	public void setBioshdt(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BIOSHDT",v);
		_Bioshdt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Bioshnum=null;

	/**
	 * @return Returns the BIOSHNUM.
	 */
	public String getBioshnum(){
		try{
			if (_Bioshnum==null){
				_Bioshnum=getStringProperty("BIOSHNUM");
				return _Bioshnum;
			}else {
				return _Bioshnum;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BIOSHNUM.
	 * @param v Value to Set.
	 */
	public void setBioshnum(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BIOSHNUM",v);
		_Bioshnum=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Bufvol=null;

	/**
	 * @return Returns the BUFVOL.
	 */
	public Integer getBufvol() {
		try{
			if (_Bufvol==null){
				_Bufvol=getIntegerProperty("BUFVOL");
				return _Bufvol;
			}else {
				return _Bufvol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BUFVOL.
	 * @param v Value to Set.
	 */
	public void setBufvol(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BUFVOL",v);
		_Bufvol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Buffroz=null;

	/**
	 * @return Returns the BUFFROZ.
	 */
	public Integer getBuffroz() {
		try{
			if (_Buffroz==null){
				_Buffroz=getIntegerProperty("BUFFROZ");
				return _Buffroz;
			}else {
				return _Buffroz;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BUFFROZ.
	 * @param v Value to Set.
	 */
	public void setBuffroz(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BUFFROZ",v);
		_Buffroz=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianBldredrdata> getAllDianBldredrdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianBldredrdata> al = new ArrayList<org.nrg.xdat.om.DianBldredrdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianBldredrdata> getDianBldredrdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianBldredrdata> al = new ArrayList<org.nrg.xdat.om.DianBldredrdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianBldredrdata> getDianBldredrdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianBldredrdata> al = new ArrayList<org.nrg.xdat.om.DianBldredrdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianBldredrdata getDianBldredrdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:bldredrData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianBldredrdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
