/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoTissueSpecdata extends XnatSubjectassessordata implements org.nrg.xdat.model.TissueSpecdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoTissueSpecdata.class);
	public static String SCHEMA_ELEMENT_NAME="tissue:specData";

	public AutoTissueSpecdata(ItemI item)
	{
		super(item);
	}

	public AutoTissueSpecdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoTissueSpecdata(UserI user)
	 **/
	public AutoTissueSpecdata(){}

	public AutoTissueSpecdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "tissue:specData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Contlabel=null;

	/**
	 * @return Returns the contLabel.
	 */
	public String getContlabel(){
		try{
			if (_Contlabel==null){
				_Contlabel=getStringProperty("contLabel");
				return _Contlabel;
			}else {
				return _Contlabel;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for contLabel.
	 * @param v Value to Set.
	 */
	public void setContlabel(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/contLabel",v);
		_Contlabel=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Colltime=null;

	/**
	 * @return Returns the collTime.
	 */
	public Object getColltime(){
		try{
			if (_Colltime==null){
				_Colltime=getProperty("collTime");
				return _Colltime;
			}else {
				return _Colltime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for collTime.
	 * @param v Value to Set.
	 */
	public void setColltime(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/collTime",v);
		_Colltime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Status=null;

	/**
	 * @return Returns the status.
	 */
	public String getStatus(){
		try{
			if (_Status==null){
				_Status=getStringProperty("status");
				return _Status;
			}else {
				return _Status;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for status.
	 * @param v Value to Set.
	 */
	public void setStatus(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/status",v);
		_Status=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.XnatResourcecatalog _File =null;

	/**
	 * file
	 * @return org.nrg.xdat.om.XnatResourcecatalog
	 */
	public org.nrg.xdat.om.XnatResourcecatalog getFile() {
		try{
			if (_File==null){
				_File=((XnatResourcecatalog)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("file")));
				return _File;
			}else {
				return _File;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for file.
	 * @param v Value to Set.
	 */
	public void setFile(ItemI v) throws Exception{
		_File =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/file",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/file",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * file
	 * set org.nrg.xdat.model.XnatResourcecatalogI
	 */
	public <A extends org.nrg.xdat.model.XnatResourcecatalogI> void setFile(A item) throws Exception{
	setFile((ItemI)item);
	}

	/**
	 * Removes the file.
	 * */
	public void removeFile() {
		_File =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/file",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _FileFK=null;

	/**
	 * @return Returns the tissue:specData/file_xnat_abstractresource_id.
	 */
	public Integer getFileFK(){
		try{
			if (_FileFK==null){
				_FileFK=getIntegerProperty("tissue:specData/file_xnat_abstractresource_id");
				return _FileFK;
			}else {
				return _FileFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tissue:specData/file_xnat_abstractresource_id.
	 * @param v Value to Set.
	 */
	public void setFileFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/file_xnat_abstractresource_id",v);
		_FileFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Volume=null;

	/**
	 * @return Returns the volume.
	 */
	public Double getVolume() {
		try{
			if (_Volume==null){
				_Volume=getDoubleProperty("volume");
				return _Volume;
			}else {
				return _Volume;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for volume.
	 * @param v Value to Set.
	 */
	public void setVolume(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/volume",v);
		_Volume=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Unitsvolume=null;

	/**
	 * @return Returns the unitsVolume.
	 */
	public String getUnitsvolume(){
		try{
			if (_Unitsvolume==null){
				_Unitsvolume=getStringProperty("unitsVolume");
				return _Unitsvolume;
			}else {
				return _Unitsvolume;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for unitsVolume.
	 * @param v Value to Set.
	 */
	public void setUnitsvolume(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/unitsVolume",v);
		_Unitsvolume=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Mass=null;

	/**
	 * @return Returns the mass.
	 */
	public Double getMass() {
		try{
			if (_Mass==null){
				_Mass=getDoubleProperty("mass");
				return _Mass;
			}else {
				return _Mass;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for mass.
	 * @param v Value to Set.
	 */
	public void setMass(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/mass",v);
		_Mass=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Unitsmass=null;

	/**
	 * @return Returns the unitsMass.
	 */
	public String getUnitsmass(){
		try{
			if (_Unitsmass==null){
				_Unitsmass=getStringProperty("unitsMass");
				return _Unitsmass;
			}else {
				return _Unitsmass;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for unitsMass.
	 * @param v Value to Set.
	 */
	public void setUnitsmass(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/unitsMass",v);
		_Unitsmass=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Typecont=null;

	/**
	 * @return Returns the typeCont.
	 */
	public String getTypecont(){
		try{
			if (_Typecont==null){
				_Typecont=getStringProperty("typeCont");
				return _Typecont;
			}else {
				return _Typecont;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for typeCont.
	 * @param v Value to Set.
	 */
	public void setTypecont(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/typeCont",v);
		_Typecont=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Altlabel1=null;

	/**
	 * @return Returns the altLabel1.
	 */
	public String getAltlabel1(){
		try{
			if (_Altlabel1==null){
				_Altlabel1=getStringProperty("altLabel1");
				return _Altlabel1;
			}else {
				return _Altlabel1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for altLabel1.
	 * @param v Value to Set.
	 */
	public void setAltlabel1(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/altLabel1",v);
		_Altlabel1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Altlabel1ref=null;

	/**
	 * @return Returns the altLabel1Ref.
	 */
	public String getAltlabel1ref(){
		try{
			if (_Altlabel1ref==null){
				_Altlabel1ref=getStringProperty("altLabel1Ref");
				return _Altlabel1ref;
			}else {
				return _Altlabel1ref;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for altLabel1Ref.
	 * @param v Value to Set.
	 */
	public void setAltlabel1ref(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/altLabel1Ref",v);
		_Altlabel1ref=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Altlabel2=null;

	/**
	 * @return Returns the altLabel2.
	 */
	public String getAltlabel2(){
		try{
			if (_Altlabel2==null){
				_Altlabel2=getStringProperty("altLabel2");
				return _Altlabel2;
			}else {
				return _Altlabel2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for altLabel2.
	 * @param v Value to Set.
	 */
	public void setAltlabel2(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/altLabel2",v);
		_Altlabel2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Altlabel2ref=null;

	/**
	 * @return Returns the altLabel2Ref.
	 */
	public String getAltlabel2ref(){
		try{
			if (_Altlabel2ref==null){
				_Altlabel2ref=getStringProperty("altLabel2Ref");
				return _Altlabel2ref;
			}else {
				return _Altlabel2ref;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for altLabel2Ref.
	 * @param v Value to Set.
	 */
	public void setAltlabel2ref(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/altLabel2Ref",v);
		_Altlabel2ref=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Altlabel3=null;

	/**
	 * @return Returns the altLabel3.
	 */
	public String getAltlabel3(){
		try{
			if (_Altlabel3==null){
				_Altlabel3=getStringProperty("altLabel3");
				return _Altlabel3;
			}else {
				return _Altlabel3;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for altLabel3.
	 * @param v Value to Set.
	 */
	public void setAltlabel3(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/altLabel3",v);
		_Altlabel3=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Altlabel3ref=null;

	/**
	 * @return Returns the altLabel3Ref.
	 */
	public String getAltlabel3ref(){
		try{
			if (_Altlabel3ref==null){
				_Altlabel3ref=getStringProperty("altLabel3Ref");
				return _Altlabel3ref;
			}else {
				return _Altlabel3ref;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for altLabel3Ref.
	 * @param v Value to Set.
	 */
	public void setAltlabel3ref(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/altLabel3Ref",v);
		_Altlabel3ref=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Tisscollid=null;

	/**
	 * @return Returns the tissCollID.
	 */
	public String getTisscollid(){
		try{
			if (_Tisscollid==null){
				_Tisscollid=getStringProperty("tissCollID");
				return _Tisscollid;
			}else {
				return _Tisscollid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tissCollID.
	 * @param v Value to Set.
	 */
	public void setTisscollid(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tissCollID",v);
		_Tisscollid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.TissueSpecdata> getAllTissueSpecdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TissueSpecdata> al = new ArrayList<org.nrg.xdat.om.TissueSpecdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.TissueSpecdata> getTissueSpecdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TissueSpecdata> al = new ArrayList<org.nrg.xdat.om.TissueSpecdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.TissueSpecdata> getTissueSpecdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TissueSpecdata> al = new ArrayList<org.nrg.xdat.om.TissueSpecdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static TissueSpecdata getTissueSpecdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("tissue:specData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (TissueSpecdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //file
	        org.nrg.xdat.model.XnatAbstractresourceI childFile = (XnatResourcecatalog)this.getFile();
	            if (childFile!=null){
	              int counterFile=0;
	              for(java.io.File f: ((XnatAbstractresource)childFile).getCorrespondingFiles(rootPath)){
	                 ResourceFile rf = new ResourceFile(f);
	                 rf.setXpath("file[xnat_abstractresource_id=" + ((XnatAbstractresource)childFile).getXnatAbstractresourceId() + "]/file/" + counterFile +"");
	                 rf.setXdatPath("file/" + ((XnatAbstractresource)childFile).getXnatAbstractresourceId() + "/" + counterFile++);
	                 rf.setSize(f.length());
	                 rf.setAbsolutePath(f.getAbsolutePath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
