/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaVitalsdata extends XnatSubjectassessordata implements org.nrg.xdat.model.CndaVitalsdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaVitalsdata.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:vitalsData";

	public AutoCndaVitalsdata(ItemI item)
	{
		super(item);
	}

	public AutoCndaVitalsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaVitalsdata(UserI user)
	 **/
	public AutoCndaVitalsdata(){}

	public AutoCndaVitalsdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:vitalsData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Height=null;

	/**
	 * @return Returns the height.
	 */
	public Double getHeight() {
		try{
			if (_Height==null){
				_Height=getDoubleProperty("height");
				return _Height;
			}else {
				return _Height;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for height.
	 * @param v Value to Set.
	 */
	public void setHeight(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/height",v);
		_Height=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Height_units=null;

	/**
	 * @return Returns the height/units.
	 */
	public String getHeight_units(){
		try{
			if (_Height_units==null){
				_Height_units=getStringProperty("height/units");
				return _Height_units;
			}else {
				return _Height_units;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for height/units.
	 * @param v Value to Set.
	 */
	public void setHeight_units(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/height/units",v);
		_Height_units=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Weight=null;

	/**
	 * @return Returns the weight.
	 */
	public Double getWeight() {
		try{
			if (_Weight==null){
				_Weight=getDoubleProperty("weight");
				return _Weight;
			}else {
				return _Weight;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for weight.
	 * @param v Value to Set.
	 */
	public void setWeight(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/weight",v);
		_Weight=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Weight_units=null;

	/**
	 * @return Returns the weight/units.
	 */
	public String getWeight_units(){
		try{
			if (_Weight_units==null){
				_Weight_units=getStringProperty("weight/units");
				return _Weight_units;
			}else {
				return _Weight_units;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for weight/units.
	 * @param v Value to Set.
	 */
	public void setWeight_units(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/weight/units",v);
		_Weight_units=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Respirations=null;

	/**
	 * @return Returns the respirations.
	 */
	public Integer getRespirations() {
		try{
			if (_Respirations==null){
				_Respirations=getIntegerProperty("respirations");
				return _Respirations;
			}else {
				return _Respirations;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for respirations.
	 * @param v Value to Set.
	 */
	public void setRespirations(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/respirations",v);
		_Respirations=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.CndaVitalsdataPulse> _Pulse =null;

	/**
	 * pulse
	 * @return Returns an List of org.nrg.xdat.om.CndaVitalsdataPulse
	 */
	public <A extends org.nrg.xdat.model.CndaVitalsdataPulseI> List<A> getPulse() {
		try{
			if (_Pulse==null){
				_Pulse=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("pulse"));
			}
			return (List<A>) _Pulse;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.CndaVitalsdataPulse>();}
	}

	/**
	 * Sets the value for pulse.
	 * @param v Value to Set.
	 */
	public void setPulse(ItemI v) throws Exception{
		_Pulse =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/pulse",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/pulse",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * pulse
	 * Adds org.nrg.xdat.model.CndaVitalsdataPulseI
	 */
	public <A extends org.nrg.xdat.model.CndaVitalsdataPulseI> void addPulse(A item) throws Exception{
	setPulse((ItemI)item);
	}

	/**
	 * Removes the pulse of the given index.
	 * @param index Index of child to remove.
	 */
	public void removePulse(int index) throws java.lang.IndexOutOfBoundsException {
		_Pulse =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/pulse",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.CndaVitalsdataBloodpressure> _Bloodpressure =null;

	/**
	 * bloodPressure
	 * @return Returns an List of org.nrg.xdat.om.CndaVitalsdataBloodpressure
	 */
	public <A extends org.nrg.xdat.model.CndaVitalsdataBloodpressureI> List<A> getBloodpressure() {
		try{
			if (_Bloodpressure==null){
				_Bloodpressure=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("bloodPressure"));
			}
			return (List<A>) _Bloodpressure;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.CndaVitalsdataBloodpressure>();}
	}

	/**
	 * Sets the value for bloodPressure.
	 * @param v Value to Set.
	 */
	public void setBloodpressure(ItemI v) throws Exception{
		_Bloodpressure =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/bloodPressure",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/bloodPressure",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * bloodPressure
	 * Adds org.nrg.xdat.model.CndaVitalsdataBloodpressureI
	 */
	public <A extends org.nrg.xdat.model.CndaVitalsdataBloodpressureI> void addBloodpressure(A item) throws Exception{
	setBloodpressure((ItemI)item);
	}

	/**
	 * Removes the bloodPressure of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeBloodpressure(int index) throws java.lang.IndexOutOfBoundsException {
		_Bloodpressure =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/bloodPressure",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Saturationo2=null;

	/**
	 * @return Returns the saturationO2.
	 */
	public Double getSaturationo2() {
		try{
			if (_Saturationo2==null){
				_Saturationo2=getDoubleProperty("saturationO2");
				return _Saturationo2;
			}else {
				return _Saturationo2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saturationO2.
	 * @param v Value to Set.
	 */
	public void setSaturationo2(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saturationO2",v);
		_Saturationo2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Problem=null;

	/**
	 * @return Returns the problem.
	 */
	public Boolean getProblem() {
		try{
			if (_Problem==null){
				_Problem=getBooleanProperty("problem");
				return _Problem;
			}else {
				return _Problem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for problem.
	 * @param v Value to Set.
	 */
	public void setProblem(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/problem",v);
		_Problem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaVitalsdata> getAllCndaVitalsdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaVitalsdata> al = new ArrayList<org.nrg.xdat.om.CndaVitalsdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaVitalsdata> getCndaVitalsdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaVitalsdata> al = new ArrayList<org.nrg.xdat.om.CndaVitalsdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaVitalsdata> getCndaVitalsdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaVitalsdata> al = new ArrayList<org.nrg.xdat.om.CndaVitalsdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaVitalsdata getCndaVitalsdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:vitalsData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaVitalsdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //pulse
	        for(org.nrg.xdat.model.CndaVitalsdataPulseI childPulse : this.getPulse()){
	            if (childPulse!=null){
	              for(ResourceFile rf: ((CndaVitalsdataPulse)childPulse).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("pulse[" + ((CndaVitalsdataPulse)childPulse).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("pulse/" + ((CndaVitalsdataPulse)childPulse).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	        //bloodPressure
	        for(org.nrg.xdat.model.CndaVitalsdataBloodpressureI childBloodpressure : this.getBloodpressure()){
	            if (childBloodpressure!=null){
	              for(ResourceFile rf: ((CndaVitalsdataBloodpressure)childBloodpressure).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("bloodPressure[" + ((CndaVitalsdataBloodpressure)childBloodpressure).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("bloodPressure/" + ((CndaVitalsdataBloodpressure)childBloodpressure).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
