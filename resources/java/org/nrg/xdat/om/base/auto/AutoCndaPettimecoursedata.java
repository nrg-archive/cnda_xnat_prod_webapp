/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaPettimecoursedata extends XnatPetassessordata implements org.nrg.xdat.model.CndaPettimecoursedataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaPettimecoursedata.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:petTimeCourseData";

	public AutoCndaPettimecoursedata(ItemI item)
	{
		super(item);
	}

	public AutoCndaPettimecoursedata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaPettimecoursedata(UserI user)
	 **/
	public AutoCndaPettimecoursedata(){}

	public AutoCndaPettimecoursedata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:petTimeCourseData";
	}
	 private org.nrg.xdat.om.XnatPetassessordata _Petassessordata =null;

	/**
	 * petAssessorData
	 * @return org.nrg.xdat.om.XnatPetassessordata
	 */
	public org.nrg.xdat.om.XnatPetassessordata getPetassessordata() {
		try{
			if (_Petassessordata==null){
				_Petassessordata=((XnatPetassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("petAssessorData")));
				return _Petassessordata;
			}else {
				return _Petassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for petAssessorData.
	 * @param v Value to Set.
	 */
	public void setPetassessordata(ItemI v) throws Exception{
		_Petassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/petAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/petAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * petAssessorData
	 * set org.nrg.xdat.model.XnatPetassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatPetassessordataI> void setPetassessordata(A item) throws Exception{
	setPetassessordata((ItemI)item);
	}

	/**
	 * Removes the petAssessorData.
	 * */
	public void removePetassessordata() {
		_Petassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/petAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.CndaPettimecoursedataRegion> _Regions_region =null;

	/**
	 * regions/region
	 * @return Returns an List of org.nrg.xdat.om.CndaPettimecoursedataRegion
	 */
	public <A extends org.nrg.xdat.model.CndaPettimecoursedataRegionI> List<A> getRegions_region() {
		try{
			if (_Regions_region==null){
				_Regions_region=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("regions/region"));
			}
			return (List<A>) _Regions_region;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.CndaPettimecoursedataRegion>();}
	}

	/**
	 * Sets the value for regions/region.
	 * @param v Value to Set.
	 */
	public void setRegions_region(ItemI v) throws Exception{
		_Regions_region =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/regions/region",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/regions/region",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * regions/region
	 * Adds org.nrg.xdat.model.CndaPettimecoursedataRegionI
	 */
	public <A extends org.nrg.xdat.model.CndaPettimecoursedataRegionI> void addRegions_region(A item) throws Exception{
	setRegions_region((ItemI)item);
	}

	/**
	 * Removes the regions/region of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeRegions_region(int index) throws java.lang.IndexOutOfBoundsException {
		_Regions_region =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/regions/region",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.CndaPettimecoursedataDuration> _Durations_duration =null;

	/**
	 * durations/duration
	 * @return Returns an List of org.nrg.xdat.om.CndaPettimecoursedataDuration
	 */
	public <A extends org.nrg.xdat.model.CndaPettimecoursedataDurationI> List<A> getDurations_duration() {
		try{
			if (_Durations_duration==null){
				_Durations_duration=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("durations/duration"));
			}
			return (List<A>) _Durations_duration;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.CndaPettimecoursedataDuration>();}
	}

	/**
	 * Sets the value for durations/duration.
	 * @param v Value to Set.
	 */
	public void setDurations_duration(ItemI v) throws Exception{
		_Durations_duration =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/durations/duration",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/durations/duration",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * durations/duration
	 * Adds org.nrg.xdat.model.CndaPettimecoursedataDurationI
	 */
	public <A extends org.nrg.xdat.model.CndaPettimecoursedataDurationI> void addDurations_duration(A item) throws Exception{
	setDurations_duration((ItemI)item);
	}

	/**
	 * Removes the durations/duration of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeDurations_duration(int index) throws java.lang.IndexOutOfBoundsException {
		_Durations_duration =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/durations/duration",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaPettimecoursedata> getAllCndaPettimecoursedatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaPettimecoursedata> al = new ArrayList<org.nrg.xdat.om.CndaPettimecoursedata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaPettimecoursedata> getCndaPettimecoursedatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaPettimecoursedata> al = new ArrayList<org.nrg.xdat.om.CndaPettimecoursedata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaPettimecoursedata> getCndaPettimecoursedatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaPettimecoursedata> al = new ArrayList<org.nrg.xdat.om.CndaPettimecoursedata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaPettimecoursedata getCndaPettimecoursedatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:petTimeCourseData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaPettimecoursedata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //petAssessorData
	        XnatPetassessordata childPetassessordata = (XnatPetassessordata)this.getPetassessordata();
	            if (childPetassessordata!=null){
	              for(ResourceFile rf: ((XnatPetassessordata)childPetassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("petAssessorData[" + ((XnatPetassessordata)childPetassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("petAssessorData/" + ((XnatPetassessordata)childPetassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //regions/region
	        for(org.nrg.xdat.model.CndaPettimecoursedataRegionI childRegions_region : this.getRegions_region()){
	            if (childRegions_region!=null){
	              for(ResourceFile rf: ((CndaPettimecoursedataRegion)childRegions_region).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("regions/region[" + ((CndaPettimecoursedataRegion)childRegions_region).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("regions/region/" + ((CndaPettimecoursedataRegion)childRegions_region).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	        //durations/duration
	        for(org.nrg.xdat.model.CndaPettimecoursedataDurationI childDurations_duration : this.getDurations_duration()){
	            if (childDurations_duration!=null){
	              for(ResourceFile rf: ((CndaPettimecoursedataDuration)childDurations_duration).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("durations/duration[" + ((CndaPettimecoursedataDuration)childDurations_duration).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("durations/duration/" + ((CndaPettimecoursedataDuration)childDurations_duration).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
