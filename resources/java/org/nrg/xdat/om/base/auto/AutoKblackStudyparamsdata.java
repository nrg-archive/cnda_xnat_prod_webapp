/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoKblackStudyparamsdata extends XnatSubjectassessordata implements org.nrg.xdat.model.KblackStudyparamsdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoKblackStudyparamsdata.class);
	public static String SCHEMA_ELEMENT_NAME="kblack:studyParamsData";

	public AutoKblackStudyparamsdata(ItemI item)
	{
		super(item);
	}

	public AutoKblackStudyparamsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoKblackStudyparamsdata(UserI user)
	 **/
	public AutoKblackStudyparamsdata(){}

	public AutoKblackStudyparamsdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "kblack:studyParamsData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Subjectcode=null;

	/**
	 * @return Returns the subjectCode.
	 */
	public String getSubjectcode(){
		try{
			if (_Subjectcode==null){
				_Subjectcode=getStringProperty("subjectCode");
				return _Subjectcode;
			}else {
				return _Subjectcode;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for subjectCode.
	 * @param v Value to Set.
	 */
	public void setSubjectcode(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/subjectCode",v);
		_Subjectcode=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Wasscreeningday=null;

	/**
	 * @return Returns the wasScreeningDay.
	 */
	public Boolean getWasscreeningday() {
		try{
			if (_Wasscreeningday==null){
				_Wasscreeningday=getBooleanProperty("wasScreeningDay");
				return _Wasscreeningday;
			}else {
				return _Wasscreeningday;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wasScreeningDay.
	 * @param v Value to Set.
	 */
	public void setWasscreeningday(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/wasScreeningDay",v);
		_Wasscreeningday=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Studytype=null;

	/**
	 * @return Returns the studyType.
	 */
	public String getStudytype(){
		try{
			if (_Studytype==null){
				_Studytype=getStringProperty("studyType");
				return _Studytype;
			}else {
				return _Studytype;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for studyType.
	 * @param v Value to Set.
	 */
	public void setStudytype(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/studyType",v);
		_Studytype=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Weight=null;

	/**
	 * @return Returns the weight.
	 */
	public Double getWeight() {
		try{
			if (_Weight==null){
				_Weight=getDoubleProperty("weight");
				return _Weight;
			}else {
				return _Weight;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for weight.
	 * @param v Value to Set.
	 */
	public void setWeight(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/weight",v);
		_Weight=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Weight_units=null;

	/**
	 * @return Returns the weight/units.
	 */
	public String getWeight_units(){
		try{
			if (_Weight_units==null){
				_Weight_units=getStringProperty("weight/units");
				return _Weight_units;
			}else {
				return _Weight_units;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for weight/units.
	 * @param v Value to Set.
	 */
	public void setWeight_units(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/weight/units",v);
		_Weight_units=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Infused=null;

	/**
	 * @return Returns the infused.
	 */
	public String getInfused(){
		try{
			if (_Infused==null){
				_Infused=getStringProperty("infused");
				return _Infused;
			}else {
				return _Infused;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for infused.
	 * @param v Value to Set.
	 */
	public void setInfused(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/infused",v);
		_Infused=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Cddose=null;

	/**
	 * @return Returns the CDdose.
	 */
	public Double getCddose() {
		try{
			if (_Cddose==null){
				_Cddose=getDoubleProperty("CDdose");
				return _Cddose;
			}else {
				return _Cddose;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CDdose.
	 * @param v Value to Set.
	 */
	public void setCddose(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CDdose",v);
		_Cddose=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Cdtime=null;

	/**
	 * @return Returns the CDtime.
	 */
	public Object getCdtime(){
		try{
			if (_Cdtime==null){
				_Cdtime=getProperty("CDtime");
				return _Cdtime;
			}else {
				return _Cdtime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CDtime.
	 * @param v Value to Set.
	 */
	public void setCdtime(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CDtime",v);
		_Cdtime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Infusionstarttime=null;

	/**
	 * @return Returns the infusionStartTime.
	 */
	public Object getInfusionstarttime(){
		try{
			if (_Infusionstarttime==null){
				_Infusionstarttime=getProperty("infusionStartTime");
				return _Infusionstarttime;
			}else {
				return _Infusionstarttime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for infusionStartTime.
	 * @param v Value to Set.
	 */
	public void setInfusionstarttime(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/infusionStartTime",v);
		_Infusionstarttime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Method=null;

	/**
	 * @return Returns the method.
	 */
	public String getMethod(){
		try{
			if (_Method==null){
				_Method=getStringProperty("method");
				return _Method;
			}else {
				return _Method;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for method.
	 * @param v Value to Set.
	 */
	public void setMethod(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/method",v);
		_Method=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Loadduration=null;

	/**
	 * @return Returns the loadDuration.
	 */
	public String getLoadduration(){
		try{
			if (_Loadduration==null){
				_Loadduration=getStringProperty("loadDuration");
				return _Loadduration;
			}else {
				return _Loadduration;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for loadDuration.
	 * @param v Value to Set.
	 */
	public void setLoadduration(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/loadDuration",v);
		_Loadduration=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Loadrate=null;

	/**
	 * @return Returns the loadRate.
	 */
	public Double getLoadrate() {
		try{
			if (_Loadrate==null){
				_Loadrate=getDoubleProperty("loadRate");
				return _Loadrate;
			}else {
				return _Loadrate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for loadRate.
	 * @param v Value to Set.
	 */
	public void setLoadrate(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/loadRate",v);
		_Loadrate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Maintrate=null;

	/**
	 * @return Returns the maintRate.
	 */
	public Double getMaintrate() {
		try{
			if (_Maintrate==null){
				_Maintrate=getDoubleProperty("maintRate");
				return _Maintrate;
			}else {
				return _Maintrate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for maintRate.
	 * @param v Value to Set.
	 */
	public void setMaintrate(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/maintRate",v);
		_Maintrate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Bloodisavailable=null;

	/**
	 * @return Returns the bloodIsAvailable.
	 */
	public Boolean getBloodisavailable() {
		try{
			if (_Bloodisavailable==null){
				_Bloodisavailable=getBooleanProperty("bloodIsAvailable");
				return _Bloodisavailable;
			}else {
				return _Bloodisavailable;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for bloodIsAvailable.
	 * @param v Value to Set.
	 */
	public void setBloodisavailable(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/bloodIsAvailable",v);
		_Bloodisavailable=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Samplesite=null;

	/**
	 * @return Returns the sampleSite.
	 */
	public String getSamplesite(){
		try{
			if (_Samplesite==null){
				_Samplesite=getStringProperty("sampleSite");
				return _Samplesite;
			}else {
				return _Samplesite;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sampleSite.
	 * @param v Value to Set.
	 */
	public void setSamplesite(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sampleSite",v);
		_Samplesite=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Nausea=null;

	/**
	 * @return Returns the nausea.
	 */
	public Boolean getNausea() {
		try{
			if (_Nausea==null){
				_Nausea=getBooleanProperty("nausea");
				return _Nausea;
			}else {
				return _Nausea;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for nausea.
	 * @param v Value to Set.
	 */
	public void setNausea(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/nausea",v);
		_Nausea=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Vomiting=null;

	/**
	 * @return Returns the vomiting.
	 */
	public Boolean getVomiting() {
		try{
			if (_Vomiting==null){
				_Vomiting=getBooleanProperty("vomiting");
				return _Vomiting;
			}else {
				return _Vomiting;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for vomiting.
	 * @param v Value to Set.
	 */
	public void setVomiting(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/vomiting",v);
		_Vomiting=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Sedation=null;

	/**
	 * @return Returns the sedation.
	 */
	public Boolean getSedation() {
		try{
			if (_Sedation==null){
				_Sedation=getBooleanProperty("sedation");
				return _Sedation;
			}else {
				return _Sedation;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sedation.
	 * @param v Value to Set.
	 */
	public void setSedation(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/sedation",v);
		_Sedation=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Dizzy=null;

	/**
	 * @return Returns the dizzy.
	 */
	public Boolean getDizzy() {
		try{
			if (_Dizzy==null){
				_Dizzy=getBooleanProperty("dizzy");
				return _Dizzy;
			}else {
				return _Dizzy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for dizzy.
	 * @param v Value to Set.
	 */
	public void setDizzy(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/dizzy",v);
		_Dizzy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Dyskinesias=null;

	/**
	 * @return Returns the dyskinesias.
	 */
	public Boolean getDyskinesias() {
		try{
			if (_Dyskinesias==null){
				_Dyskinesias=getBooleanProperty("dyskinesias");
				return _Dyskinesias;
			}else {
				return _Dyskinesias;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for dyskinesias.
	 * @param v Value to Set.
	 */
	public void setDyskinesias(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/dyskinesias",v);
		_Dyskinesias=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Psychosis=null;

	/**
	 * @return Returns the psychosis.
	 */
	public Boolean getPsychosis() {
		try{
			if (_Psychosis==null){
				_Psychosis=getBooleanProperty("psychosis");
				return _Psychosis;
			}else {
				return _Psychosis;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for psychosis.
	 * @param v Value to Set.
	 */
	public void setPsychosis(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/psychosis",v);
		_Psychosis=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Ticsworse=null;

	/**
	 * @return Returns the ticsWorse.
	 */
	public Boolean getTicsworse() {
		try{
			if (_Ticsworse==null){
				_Ticsworse=getBooleanProperty("ticsWorse");
				return _Ticsworse;
			}else {
				return _Ticsworse;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ticsWorse.
	 * @param v Value to Set.
	 */
	public void setTicsworse(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/ticsWorse",v);
		_Ticsworse=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Ticsbetter=null;

	/**
	 * @return Returns the ticsBetter.
	 */
	public Boolean getTicsbetter() {
		try{
			if (_Ticsbetter==null){
				_Ticsbetter=getBooleanProperty("ticsBetter");
				return _Ticsbetter;
			}else {
				return _Ticsbetter;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ticsBetter.
	 * @param v Value to Set.
	 */
	public void setTicsbetter(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/ticsBetter",v);
		_Ticsbetter=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Othersideeffects=null;

	/**
	 * @return Returns the otherSideEffects.
	 */
	public Boolean getOthersideeffects() {
		try{
			if (_Othersideeffects==null){
				_Othersideeffects=getBooleanProperty("otherSideEffects");
				return _Othersideeffects;
			}else {
				return _Othersideeffects;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for otherSideEffects.
	 * @param v Value to Set.
	 */
	public void setOthersideeffects(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/otherSideEffects",v);
		_Othersideeffects=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Problem=null;

	/**
	 * @return Returns the problem.
	 */
	public Boolean getProblem() {
		try{
			if (_Problem==null){
				_Problem=getBooleanProperty("problem");
				return _Problem;
			}else {
				return _Problem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for problem.
	 * @param v Value to Set.
	 */
	public void setProblem(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/problem",v);
		_Problem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Knownforivld1=null;

	/**
	 * @return Returns the knownForIVLD1.
	 */
	public Boolean getKnownforivld1() {
		try{
			if (_Knownforivld1==null){
				_Knownforivld1=getBooleanProperty("knownForIVLD1");
				return _Knownforivld1;
			}else {
				return _Knownforivld1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for knownForIVLD1.
	 * @param v Value to Set.
	 */
	public void setKnownforivld1(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/knownForIVLD1",v);
		_Knownforivld1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Rejectedforivld1=null;

	/**
	 * @return Returns the rejectedForIVLD1.
	 */
	public Boolean getRejectedforivld1() {
		try{
			if (_Rejectedforivld1==null){
				_Rejectedforivld1=getBooleanProperty("rejectedForIVLD1");
				return _Rejectedforivld1;
			}else {
				return _Rejectedforivld1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for rejectedForIVLD1.
	 * @param v Value to Set.
	 */
	public void setRejectedforivld1(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/rejectedForIVLD1",v);
		_Rejectedforivld1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Blindnesssubject=null;

	/**
	 * @return Returns the blindnessSubject.
	 */
	public Boolean getBlindnesssubject() {
		try{
			if (_Blindnesssubject==null){
				_Blindnesssubject=getBooleanProperty("blindnessSubject");
				return _Blindnesssubject;
			}else {
				return _Blindnesssubject;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for blindnessSubject.
	 * @param v Value to Set.
	 */
	public void setBlindnesssubject(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/blindnessSubject",v);
		_Blindnesssubject=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Blindnessinvestigator=null;

	/**
	 * @return Returns the blindnessInvestigator.
	 */
	public Boolean getBlindnessinvestigator() {
		try{
			if (_Blindnessinvestigator==null){
				_Blindnessinvestigator=getBooleanProperty("blindnessInvestigator");
				return _Blindnessinvestigator;
			}else {
				return _Blindnessinvestigator;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for blindnessInvestigator.
	 * @param v Value to Set.
	 */
	public void setBlindnessinvestigator(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/blindnessInvestigator",v);
		_Blindnessinvestigator=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Blindnesscoordinator=null;

	/**
	 * @return Returns the blindnessCoordinator.
	 */
	public Boolean getBlindnesscoordinator() {
		try{
			if (_Blindnesscoordinator==null){
				_Blindnesscoordinator=getBooleanProperty("blindnessCoordinator");
				return _Blindnesscoordinator;
			}else {
				return _Blindnesscoordinator;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for blindnessCoordinator.
	 * @param v Value to Set.
	 */
	public void setBlindnesscoordinator(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/blindnessCoordinator",v);
		_Blindnesscoordinator=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Pregnancytest=null;

	/**
	 * @return Returns the pregnancyTest.
	 */
	public Boolean getPregnancytest() {
		try{
			if (_Pregnancytest==null){
				_Pregnancytest=getBooleanProperty("pregnancyTest");
				return _Pregnancytest;
			}else {
				return _Pregnancytest;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for pregnancyTest.
	 * @param v Value to Set.
	 */
	public void setPregnancytest(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/pregnancyTest",v);
		_Pregnancytest=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.KblackStudyparamsdata> getAllKblackStudyparamsdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.KblackStudyparamsdata> al = new ArrayList<org.nrg.xdat.om.KblackStudyparamsdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.KblackStudyparamsdata> getKblackStudyparamsdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.KblackStudyparamsdata> al = new ArrayList<org.nrg.xdat.om.KblackStudyparamsdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.KblackStudyparamsdata> getKblackStudyparamsdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.KblackStudyparamsdata> al = new ArrayList<org.nrg.xdat.om.KblackStudyparamsdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static KblackStudyparamsdata getKblackStudyparamsdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("kblack:studyParamsData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (KblackStudyparamsdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
