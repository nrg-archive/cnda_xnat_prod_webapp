/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaManualvolumetryregionSlice extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.CndaManualvolumetryregionSliceI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaManualvolumetryregionSlice.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:manualVolumetryRegion_slice";

	public AutoCndaManualvolumetryregionSlice(ItemI item)
	{
		super(item);
	}

	public AutoCndaManualvolumetryregionSlice(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaManualvolumetryregionSlice(UserI user)
	 **/
	public AutoCndaManualvolumetryregionSlice(){}

	public AutoCndaManualvolumetryregionSlice(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:manualVolumetryRegion_slice";
	}

	//FIELD

	private Integer _Num=null;

	/**
	 * @return Returns the num.
	 */
	public Integer getNum() {
		try{
			if (_Num==null){
				_Num=getIntegerProperty("num");
				return _Num;
			}else {
				return _Num;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for num.
	 * @param v Value to Set.
	 */
	public void setNum(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/num",v);
		_Num=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Voxels=null;

	/**
	 * @return Returns the voxels.
	 */
	public Integer getVoxels() {
		try{
			if (_Voxels==null){
				_Voxels=getIntegerProperty("voxels");
				return _Voxels;
			}else {
				return _Voxels;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for voxels.
	 * @param v Value to Set.
	 */
	public void setVoxels(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/voxels",v);
		_Voxels=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Area=null;

	/**
	 * @return Returns the area.
	 */
	public Double getArea() {
		try{
			if (_Area==null){
				_Area=getDoubleProperty("area");
				return _Area;
			}else {
				return _Area;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for area.
	 * @param v Value to Set.
	 */
	public void setArea(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/area",v);
		_Area=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Volume=null;

	/**
	 * @return Returns the volume.
	 */
	public Double getVolume() {
		try{
			if (_Volume==null){
				_Volume=getDoubleProperty("volume");
				return _Volume;
			}else {
				return _Volume;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for volume.
	 * @param v Value to Set.
	 */
	public void setVolume(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/volume",v);
		_Volume=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CndaManualvolumetryregionSliceId=null;

	/**
	 * @return Returns the cnda_manualVolumetryRegion_slice_id.
	 */
	public Integer getCndaManualvolumetryregionSliceId() {
		try{
			if (_CndaManualvolumetryregionSliceId==null){
				_CndaManualvolumetryregionSliceId=getIntegerProperty("cnda_manualVolumetryRegion_slice_id");
				return _CndaManualvolumetryregionSliceId;
			}else {
				return _CndaManualvolumetryregionSliceId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda_manualVolumetryRegion_slice_id.
	 * @param v Value to Set.
	 */
	public void setCndaManualvolumetryregionSliceId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/cnda_manualVolumetryRegion_slice_id",v);
		_CndaManualvolumetryregionSliceId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaManualvolumetryregionSlice> getAllCndaManualvolumetryregionSlices(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaManualvolumetryregionSlice> al = new ArrayList<org.nrg.xdat.om.CndaManualvolumetryregionSlice>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaManualvolumetryregionSlice> getCndaManualvolumetryregionSlicesByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaManualvolumetryregionSlice> al = new ArrayList<org.nrg.xdat.om.CndaManualvolumetryregionSlice>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaManualvolumetryregionSlice> getCndaManualvolumetryregionSlicesByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaManualvolumetryregionSlice> al = new ArrayList<org.nrg.xdat.om.CndaManualvolumetryregionSlice>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaManualvolumetryregionSlice getCndaManualvolumetryregionSlicesByCndaManualvolumetryregionSliceId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:manualVolumetryRegion_slice/cnda_manualVolumetryRegion_slice_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaManualvolumetryregionSlice) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
