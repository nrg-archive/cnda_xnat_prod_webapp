/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoUdsB3updrsdata extends XnatSubjectassessordata implements org.nrg.xdat.model.UdsB3updrsdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoUdsB3updrsdata.class);
	public static String SCHEMA_ELEMENT_NAME="uds:b3updrsData";

	public AutoUdsB3updrsdata(ItemI item)
	{
		super(item);
	}

	public AutoUdsB3updrsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoUdsB3updrsdata(UserI user)
	 **/
	public AutoUdsB3updrsdata(){}

	public AutoUdsB3updrsdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "uds:b3updrsData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Initials=null;

	/**
	 * @return Returns the INITIALS.
	 */
	public String getInitials(){
		try{
			if (_Initials==null){
				_Initials=getStringProperty("INITIALS");
				return _Initials;
			}else {
				return _Initials;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INITIALS.
	 * @param v Value to Set.
	 */
	public void setInitials(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INITIALS",v);
		_Initials=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pdnormal=null;

	/**
	 * @return Returns the PDNORMAL.
	 */
	public Integer getPdnormal() {
		try{
			if (_Pdnormal==null){
				_Pdnormal=getIntegerProperty("PDNORMAL");
				return _Pdnormal;
			}else {
				return _Pdnormal;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PDNORMAL.
	 * @param v Value to Set.
	 */
	public void setPdnormal(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PDNORMAL",v);
		_Pdnormal=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Speech=null;

	/**
	 * @return Returns the SPEECH.
	 */
	public Integer getSpeech() {
		try{
			if (_Speech==null){
				_Speech=getIntegerProperty("SPEECH");
				return _Speech;
			}else {
				return _Speech;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SPEECH.
	 * @param v Value to Set.
	 */
	public void setSpeech(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SPEECH",v);
		_Speech=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Speechx=null;

	/**
	 * @return Returns the SPEECHX.
	 */
	public String getSpeechx(){
		try{
			if (_Speechx==null){
				_Speechx=getStringProperty("SPEECHX");
				return _Speechx;
			}else {
				return _Speechx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SPEECHX.
	 * @param v Value to Set.
	 */
	public void setSpeechx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SPEECHX",v);
		_Speechx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Facexp=null;

	/**
	 * @return Returns the FACEXP.
	 */
	public Integer getFacexp() {
		try{
			if (_Facexp==null){
				_Facexp=getIntegerProperty("FACEXP");
				return _Facexp;
			}else {
				return _Facexp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for FACEXP.
	 * @param v Value to Set.
	 */
	public void setFacexp(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/FACEXP",v);
		_Facexp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Facexpx=null;

	/**
	 * @return Returns the FACEXPX.
	 */
	public String getFacexpx(){
		try{
			if (_Facexpx==null){
				_Facexpx=getStringProperty("FACEXPX");
				return _Facexpx;
			}else {
				return _Facexpx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for FACEXPX.
	 * @param v Value to Set.
	 */
	public void setFacexpx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/FACEXPX",v);
		_Facexpx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Trestfac=null;

	/**
	 * @return Returns the TRESTFAC.
	 */
	public Integer getTrestfac() {
		try{
			if (_Trestfac==null){
				_Trestfac=getIntegerProperty("TRESTFAC");
				return _Trestfac;
			}else {
				return _Trestfac;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRESTFAC.
	 * @param v Value to Set.
	 */
	public void setTrestfac(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRESTFAC",v);
		_Trestfac=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Trestfax=null;

	/**
	 * @return Returns the TRESTFAX.
	 */
	public String getTrestfax(){
		try{
			if (_Trestfax==null){
				_Trestfax=getStringProperty("TRESTFAX");
				return _Trestfax;
			}else {
				return _Trestfax;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRESTFAX.
	 * @param v Value to Set.
	 */
	public void setTrestfax(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRESTFAX",v);
		_Trestfax=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Trestrhd=null;

	/**
	 * @return Returns the TRESTRHD.
	 */
	public Integer getTrestrhd() {
		try{
			if (_Trestrhd==null){
				_Trestrhd=getIntegerProperty("TRESTRHD");
				return _Trestrhd;
			}else {
				return _Trestrhd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRESTRHD.
	 * @param v Value to Set.
	 */
	public void setTrestrhd(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRESTRHD",v);
		_Trestrhd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Trestrhx=null;

	/**
	 * @return Returns the TRESTRHX.
	 */
	public String getTrestrhx(){
		try{
			if (_Trestrhx==null){
				_Trestrhx=getStringProperty("TRESTRHX");
				return _Trestrhx;
			}else {
				return _Trestrhx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRESTRHX.
	 * @param v Value to Set.
	 */
	public void setTrestrhx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRESTRHX",v);
		_Trestrhx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Trestlhd=null;

	/**
	 * @return Returns the TRESTLHD.
	 */
	public Integer getTrestlhd() {
		try{
			if (_Trestlhd==null){
				_Trestlhd=getIntegerProperty("TRESTLHD");
				return _Trestlhd;
			}else {
				return _Trestlhd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRESTLHD.
	 * @param v Value to Set.
	 */
	public void setTrestlhd(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRESTLHD",v);
		_Trestlhd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Trestlhx=null;

	/**
	 * @return Returns the TRESTLHX.
	 */
	public String getTrestlhx(){
		try{
			if (_Trestlhx==null){
				_Trestlhx=getStringProperty("TRESTLHX");
				return _Trestlhx;
			}else {
				return _Trestlhx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRESTLHX.
	 * @param v Value to Set.
	 */
	public void setTrestlhx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRESTLHX",v);
		_Trestlhx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Trestrft=null;

	/**
	 * @return Returns the TRESTRFT.
	 */
	public Integer getTrestrft() {
		try{
			if (_Trestrft==null){
				_Trestrft=getIntegerProperty("TRESTRFT");
				return _Trestrft;
			}else {
				return _Trestrft;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRESTRFT.
	 * @param v Value to Set.
	 */
	public void setTrestrft(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRESTRFT",v);
		_Trestrft=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Trestrfx=null;

	/**
	 * @return Returns the TRESTRFX.
	 */
	public String getTrestrfx(){
		try{
			if (_Trestrfx==null){
				_Trestrfx=getStringProperty("TRESTRFX");
				return _Trestrfx;
			}else {
				return _Trestrfx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRESTRFX.
	 * @param v Value to Set.
	 */
	public void setTrestrfx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRESTRFX",v);
		_Trestrfx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Trestlft=null;

	/**
	 * @return Returns the TRESTLFT.
	 */
	public Integer getTrestlft() {
		try{
			if (_Trestlft==null){
				_Trestlft=getIntegerProperty("TRESTLFT");
				return _Trestlft;
			}else {
				return _Trestlft;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRESTLFT.
	 * @param v Value to Set.
	 */
	public void setTrestlft(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRESTLFT",v);
		_Trestlft=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Trestlfx=null;

	/**
	 * @return Returns the TRESTLFX.
	 */
	public String getTrestlfx(){
		try{
			if (_Trestlfx==null){
				_Trestlfx=getStringProperty("TRESTLFX");
				return _Trestlfx;
			}else {
				return _Trestlfx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRESTLFX.
	 * @param v Value to Set.
	 */
	public void setTrestlfx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRESTLFX",v);
		_Trestlfx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tractrhd=null;

	/**
	 * @return Returns the TRACTRHD.
	 */
	public Integer getTractrhd() {
		try{
			if (_Tractrhd==null){
				_Tractrhd=getIntegerProperty("TRACTRHD");
				return _Tractrhd;
			}else {
				return _Tractrhd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRACTRHD.
	 * @param v Value to Set.
	 */
	public void setTractrhd(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRACTRHD",v);
		_Tractrhd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Tractrhx=null;

	/**
	 * @return Returns the TRACTRHX.
	 */
	public String getTractrhx(){
		try{
			if (_Tractrhx==null){
				_Tractrhx=getStringProperty("TRACTRHX");
				return _Tractrhx;
			}else {
				return _Tractrhx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRACTRHX.
	 * @param v Value to Set.
	 */
	public void setTractrhx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRACTRHX",v);
		_Tractrhx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tractlhd=null;

	/**
	 * @return Returns the TRACTLHD.
	 */
	public Integer getTractlhd() {
		try{
			if (_Tractlhd==null){
				_Tractlhd=getIntegerProperty("TRACTLHD");
				return _Tractlhd;
			}else {
				return _Tractlhd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRACTLHD.
	 * @param v Value to Set.
	 */
	public void setTractlhd(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRACTLHD",v);
		_Tractlhd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Tractlhx=null;

	/**
	 * @return Returns the TRACTLHX.
	 */
	public String getTractlhx(){
		try{
			if (_Tractlhx==null){
				_Tractlhx=getStringProperty("TRACTLHX");
				return _Tractlhx;
			}else {
				return _Tractlhx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TRACTLHX.
	 * @param v Value to Set.
	 */
	public void setTractlhx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TRACTLHX",v);
		_Tractlhx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rigdneck=null;

	/**
	 * @return Returns the RIGDNECK.
	 */
	public Integer getRigdneck() {
		try{
			if (_Rigdneck==null){
				_Rigdneck=getIntegerProperty("RIGDNECK");
				return _Rigdneck;
			}else {
				return _Rigdneck;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RIGDNECK.
	 * @param v Value to Set.
	 */
	public void setRigdneck(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RIGDNECK",v);
		_Rigdneck=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rigdnex=null;

	/**
	 * @return Returns the RIGDNEX.
	 */
	public String getRigdnex(){
		try{
			if (_Rigdnex==null){
				_Rigdnex=getStringProperty("RIGDNEX");
				return _Rigdnex;
			}else {
				return _Rigdnex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RIGDNEX.
	 * @param v Value to Set.
	 */
	public void setRigdnex(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RIGDNEX",v);
		_Rigdnex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rigduprt=null;

	/**
	 * @return Returns the RIGDUPRT.
	 */
	public Integer getRigduprt() {
		try{
			if (_Rigduprt==null){
				_Rigduprt=getIntegerProperty("RIGDUPRT");
				return _Rigduprt;
			}else {
				return _Rigduprt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RIGDUPRT.
	 * @param v Value to Set.
	 */
	public void setRigduprt(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RIGDUPRT",v);
		_Rigduprt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rigduprx=null;

	/**
	 * @return Returns the RIGDUPRX.
	 */
	public String getRigduprx(){
		try{
			if (_Rigduprx==null){
				_Rigduprx=getStringProperty("RIGDUPRX");
				return _Rigduprx;
			}else {
				return _Rigduprx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RIGDUPRX.
	 * @param v Value to Set.
	 */
	public void setRigduprx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RIGDUPRX",v);
		_Rigduprx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rigduplf=null;

	/**
	 * @return Returns the RIGDUPLF.
	 */
	public Integer getRigduplf() {
		try{
			if (_Rigduplf==null){
				_Rigduplf=getIntegerProperty("RIGDUPLF");
				return _Rigduplf;
			}else {
				return _Rigduplf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RIGDUPLF.
	 * @param v Value to Set.
	 */
	public void setRigduplf(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RIGDUPLF",v);
		_Rigduplf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rigduplx=null;

	/**
	 * @return Returns the RIGDUPLX.
	 */
	public String getRigduplx(){
		try{
			if (_Rigduplx==null){
				_Rigduplx=getStringProperty("RIGDUPLX");
				return _Rigduplx;
			}else {
				return _Rigduplx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RIGDUPLX.
	 * @param v Value to Set.
	 */
	public void setRigduplx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RIGDUPLX",v);
		_Rigduplx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rigdlort=null;

	/**
	 * @return Returns the RIGDLORT.
	 */
	public Integer getRigdlort() {
		try{
			if (_Rigdlort==null){
				_Rigdlort=getIntegerProperty("RIGDLORT");
				return _Rigdlort;
			}else {
				return _Rigdlort;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RIGDLORT.
	 * @param v Value to Set.
	 */
	public void setRigdlort(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RIGDLORT",v);
		_Rigdlort=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rigdlorx=null;

	/**
	 * @return Returns the RIGDLORX.
	 */
	public String getRigdlorx(){
		try{
			if (_Rigdlorx==null){
				_Rigdlorx=getStringProperty("RIGDLORX");
				return _Rigdlorx;
			}else {
				return _Rigdlorx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RIGDLORX.
	 * @param v Value to Set.
	 */
	public void setRigdlorx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RIGDLORX",v);
		_Rigdlorx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rigdlolf=null;

	/**
	 * @return Returns the RIGDLOLF.
	 */
	public Integer getRigdlolf() {
		try{
			if (_Rigdlolf==null){
				_Rigdlolf=getIntegerProperty("RIGDLOLF");
				return _Rigdlolf;
			}else {
				return _Rigdlolf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RIGDLOLF.
	 * @param v Value to Set.
	 */
	public void setRigdlolf(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RIGDLOLF",v);
		_Rigdlolf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rigdlolx=null;

	/**
	 * @return Returns the RIGDLOLX.
	 */
	public String getRigdlolx(){
		try{
			if (_Rigdlolx==null){
				_Rigdlolx=getStringProperty("RIGDLOLX");
				return _Rigdlolx;
			}else {
				return _Rigdlolx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RIGDLOLX.
	 * @param v Value to Set.
	 */
	public void setRigdlolx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RIGDLOLX",v);
		_Rigdlolx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tapsrt=null;

	/**
	 * @return Returns the TAPSRT.
	 */
	public Integer getTapsrt() {
		try{
			if (_Tapsrt==null){
				_Tapsrt=getIntegerProperty("TAPSRT");
				return _Tapsrt;
			}else {
				return _Tapsrt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TAPSRT.
	 * @param v Value to Set.
	 */
	public void setTapsrt(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TAPSRT",v);
		_Tapsrt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Tapsrtx=null;

	/**
	 * @return Returns the TAPSRTX.
	 */
	public String getTapsrtx(){
		try{
			if (_Tapsrtx==null){
				_Tapsrtx=getStringProperty("TAPSRTX");
				return _Tapsrtx;
			}else {
				return _Tapsrtx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TAPSRTX.
	 * @param v Value to Set.
	 */
	public void setTapsrtx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TAPSRTX",v);
		_Tapsrtx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tapslf=null;

	/**
	 * @return Returns the TAPSLF.
	 */
	public Integer getTapslf() {
		try{
			if (_Tapslf==null){
				_Tapslf=getIntegerProperty("TAPSLF");
				return _Tapslf;
			}else {
				return _Tapslf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TAPSLF.
	 * @param v Value to Set.
	 */
	public void setTapslf(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TAPSLF",v);
		_Tapslf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Tapslfx=null;

	/**
	 * @return Returns the TAPSLFX.
	 */
	public String getTapslfx(){
		try{
			if (_Tapslfx==null){
				_Tapslfx=getStringProperty("TAPSLFX");
				return _Tapslfx;
			}else {
				return _Tapslfx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TAPSLFX.
	 * @param v Value to Set.
	 */
	public void setTapslfx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TAPSLFX",v);
		_Tapslfx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Handmovr=null;

	/**
	 * @return Returns the HANDMOVR.
	 */
	public Integer getHandmovr() {
		try{
			if (_Handmovr==null){
				_Handmovr=getIntegerProperty("HANDMOVR");
				return _Handmovr;
			}else {
				return _Handmovr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HANDMOVR.
	 * @param v Value to Set.
	 */
	public void setHandmovr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HANDMOVR",v);
		_Handmovr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Handmvrx=null;

	/**
	 * @return Returns the HANDMVRX.
	 */
	public String getHandmvrx(){
		try{
			if (_Handmvrx==null){
				_Handmvrx=getStringProperty("HANDMVRX");
				return _Handmvrx;
			}else {
				return _Handmvrx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HANDMVRX.
	 * @param v Value to Set.
	 */
	public void setHandmvrx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HANDMVRX",v);
		_Handmvrx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Handmovl=null;

	/**
	 * @return Returns the HANDMOVL.
	 */
	public Integer getHandmovl() {
		try{
			if (_Handmovl==null){
				_Handmovl=getIntegerProperty("HANDMOVL");
				return _Handmovl;
			}else {
				return _Handmovl;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HANDMOVL.
	 * @param v Value to Set.
	 */
	public void setHandmovl(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HANDMOVL",v);
		_Handmovl=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Handmvlx=null;

	/**
	 * @return Returns the HANDMVLX.
	 */
	public String getHandmvlx(){
		try{
			if (_Handmvlx==null){
				_Handmvlx=getStringProperty("HANDMVLX");
				return _Handmvlx;
			}else {
				return _Handmvlx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HANDMVLX.
	 * @param v Value to Set.
	 */
	public void setHandmvlx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HANDMVLX",v);
		_Handmvlx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Handaltr=null;

	/**
	 * @return Returns the HANDALTR.
	 */
	public Integer getHandaltr() {
		try{
			if (_Handaltr==null){
				_Handaltr=getIntegerProperty("HANDALTR");
				return _Handaltr;
			}else {
				return _Handaltr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HANDALTR.
	 * @param v Value to Set.
	 */
	public void setHandaltr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HANDALTR",v);
		_Handaltr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Handatrx=null;

	/**
	 * @return Returns the HANDATRX.
	 */
	public String getHandatrx(){
		try{
			if (_Handatrx==null){
				_Handatrx=getStringProperty("HANDATRX");
				return _Handatrx;
			}else {
				return _Handatrx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HANDATRX.
	 * @param v Value to Set.
	 */
	public void setHandatrx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HANDATRX",v);
		_Handatrx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Handaltl=null;

	/**
	 * @return Returns the HANDALTL.
	 */
	public Integer getHandaltl() {
		try{
			if (_Handaltl==null){
				_Handaltl=getIntegerProperty("HANDALTL");
				return _Handaltl;
			}else {
				return _Handaltl;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HANDALTL.
	 * @param v Value to Set.
	 */
	public void setHandaltl(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HANDALTL",v);
		_Handaltl=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Handatlx=null;

	/**
	 * @return Returns the HANDATLX.
	 */
	public String getHandatlx(){
		try{
			if (_Handatlx==null){
				_Handatlx=getStringProperty("HANDATLX");
				return _Handatlx;
			}else {
				return _Handatlx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HANDATLX.
	 * @param v Value to Set.
	 */
	public void setHandatlx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HANDATLX",v);
		_Handatlx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Legrt=null;

	/**
	 * @return Returns the LEGRT.
	 */
	public Integer getLegrt() {
		try{
			if (_Legrt==null){
				_Legrt=getIntegerProperty("LEGRT");
				return _Legrt;
			}else {
				return _Legrt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LEGRT.
	 * @param v Value to Set.
	 */
	public void setLegrt(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LEGRT",v);
		_Legrt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Legrtx=null;

	/**
	 * @return Returns the LEGRTX.
	 */
	public String getLegrtx(){
		try{
			if (_Legrtx==null){
				_Legrtx=getStringProperty("LEGRTX");
				return _Legrtx;
			}else {
				return _Legrtx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LEGRTX.
	 * @param v Value to Set.
	 */
	public void setLegrtx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LEGRTX",v);
		_Legrtx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Leglf=null;

	/**
	 * @return Returns the LEGLF.
	 */
	public Integer getLeglf() {
		try{
			if (_Leglf==null){
				_Leglf=getIntegerProperty("LEGLF");
				return _Leglf;
			}else {
				return _Leglf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LEGLF.
	 * @param v Value to Set.
	 */
	public void setLeglf(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LEGLF",v);
		_Leglf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Leglfx=null;

	/**
	 * @return Returns the LEGLFX.
	 */
	public String getLeglfx(){
		try{
			if (_Leglfx==null){
				_Leglfx=getStringProperty("LEGLFX");
				return _Leglfx;
			}else {
				return _Leglfx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for LEGLFX.
	 * @param v Value to Set.
	 */
	public void setLeglfx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/LEGLFX",v);
		_Leglfx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Arising=null;

	/**
	 * @return Returns the ARISING.
	 */
	public Integer getArising() {
		try{
			if (_Arising==null){
				_Arising=getIntegerProperty("ARISING");
				return _Arising;
			}else {
				return _Arising;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ARISING.
	 * @param v Value to Set.
	 */
	public void setArising(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ARISING",v);
		_Arising=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Arisingx=null;

	/**
	 * @return Returns the ARISINGX.
	 */
	public String getArisingx(){
		try{
			if (_Arisingx==null){
				_Arisingx=getStringProperty("ARISINGX");
				return _Arisingx;
			}else {
				return _Arisingx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ARISINGX.
	 * @param v Value to Set.
	 */
	public void setArisingx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ARISINGX",v);
		_Arisingx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Posture=null;

	/**
	 * @return Returns the POSTURE.
	 */
	public Integer getPosture() {
		try{
			if (_Posture==null){
				_Posture=getIntegerProperty("POSTURE");
				return _Posture;
			}else {
				return _Posture;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for POSTURE.
	 * @param v Value to Set.
	 */
	public void setPosture(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/POSTURE",v);
		_Posture=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Posturex=null;

	/**
	 * @return Returns the POSTUREX.
	 */
	public String getPosturex(){
		try{
			if (_Posturex==null){
				_Posturex=getStringProperty("POSTUREX");
				return _Posturex;
			}else {
				return _Posturex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for POSTUREX.
	 * @param v Value to Set.
	 */
	public void setPosturex(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/POSTUREX",v);
		_Posturex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Gait=null;

	/**
	 * @return Returns the GAIT.
	 */
	public Integer getGait() {
		try{
			if (_Gait==null){
				_Gait=getIntegerProperty("GAIT");
				return _Gait;
			}else {
				return _Gait;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GAIT.
	 * @param v Value to Set.
	 */
	public void setGait(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GAIT",v);
		_Gait=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Gaitx=null;

	/**
	 * @return Returns the GAITX.
	 */
	public String getGaitx(){
		try{
			if (_Gaitx==null){
				_Gaitx=getStringProperty("GAITX");
				return _Gaitx;
			}else {
				return _Gaitx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GAITX.
	 * @param v Value to Set.
	 */
	public void setGaitx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GAITX",v);
		_Gaitx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Posstab=null;

	/**
	 * @return Returns the POSSTAB.
	 */
	public Integer getPosstab() {
		try{
			if (_Posstab==null){
				_Posstab=getIntegerProperty("POSSTAB");
				return _Posstab;
			}else {
				return _Posstab;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for POSSTAB.
	 * @param v Value to Set.
	 */
	public void setPosstab(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/POSSTAB",v);
		_Posstab=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Posstabx=null;

	/**
	 * @return Returns the POSSTABX.
	 */
	public String getPosstabx(){
		try{
			if (_Posstabx==null){
				_Posstabx=getStringProperty("POSSTABX");
				return _Posstabx;
			}else {
				return _Posstabx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for POSSTABX.
	 * @param v Value to Set.
	 */
	public void setPosstabx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/POSSTABX",v);
		_Posstabx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Bradykin=null;

	/**
	 * @return Returns the BRADYKIN.
	 */
	public Integer getBradykin() {
		try{
			if (_Bradykin==null){
				_Bradykin=getIntegerProperty("BRADYKIN");
				return _Bradykin;
			}else {
				return _Bradykin;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BRADYKIN.
	 * @param v Value to Set.
	 */
	public void setBradykin(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BRADYKIN",v);
		_Bradykin=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Bradykix=null;

	/**
	 * @return Returns the BRADYKIX.
	 */
	public String getBradykix(){
		try{
			if (_Bradykix==null){
				_Bradykix=getStringProperty("BRADYKIX");
				return _Bradykix;
			}else {
				return _Bradykix;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BRADYKIX.
	 * @param v Value to Set.
	 */
	public void setBradykix(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BRADYKIX",v);
		_Bradykix=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.UdsB3updrsdata> getAllUdsB3updrsdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsB3updrsdata> al = new ArrayList<org.nrg.xdat.om.UdsB3updrsdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsB3updrsdata> getUdsB3updrsdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsB3updrsdata> al = new ArrayList<org.nrg.xdat.om.UdsB3updrsdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsB3updrsdata> getUdsB3updrsdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsB3updrsdata> al = new ArrayList<org.nrg.xdat.om.UdsB3updrsdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static UdsB3updrsdata getUdsB3updrsdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("uds:b3updrsData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (UdsB3updrsdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
