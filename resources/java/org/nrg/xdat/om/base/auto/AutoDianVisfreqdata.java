/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:01 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianVisfreqdata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianVisfreqdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianVisfreqdata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:visFreqData";

	public AutoDianVisfreqdata(ItemI item)
	{
		super(item);
	}

	public AutoDianVisfreqdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianVisfreqdata(UserI user)
	 **/
	public AutoDianVisfreqdata(){}

	public AutoDianVisfreqdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:visFreqData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Visitage=null;

	/**
	 * @return Returns the VISITAGE.
	 */
	public Integer getVisitage() {
		try{
			if (_Visitage==null){
				_Visitage=getIntegerProperty("VISITAGE");
				return _Visitage;
			}else {
				return _Visitage;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VISITAGE.
	 * @param v Value to Set.
	 */
	public void setVisitage(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VISITAGE",v);
		_Visitage=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Parentao=null;

	/**
	 * @return Returns the PARENTAO.
	 */
	public Integer getParentao() {
		try{
			if (_Parentao==null){
				_Parentao=getIntegerProperty("PARENTAO");
				return _Parentao;
			}else {
				return _Parentao;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PARENTAO.
	 * @param v Value to Set.
	 */
	public void setParentao(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PARENTAO",v);
		_Parentao=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mqrscan=null;

	/**
	 * @return Returns the MQRSCAN.
	 */
	public Integer getMqrscan() {
		try{
			if (_Mqrscan==null){
				_Mqrscan=getIntegerProperty("MQRSCAN");
				return _Mqrscan;
			}else {
				return _Mqrscan;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MQRSCAN.
	 * @param v Value to Set.
	 */
	public void setMqrscan(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MQRSCAN",v);
		_Mqrscan=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Nxtann=null;

	/**
	 * @return Returns the NXTANN.
	 */
	public Integer getNxtann() {
		try{
			if (_Nxtann==null){
				_Nxtann=getIntegerProperty("NXTANN");
				return _Nxtann;
			}else {
				return _Nxtann;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NXTANN.
	 * @param v Value to Set.
	 */
	public void setNxtann(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NXTANN",v);
		_Nxtann=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianVisfreqdata> getAllDianVisfreqdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianVisfreqdata> al = new ArrayList<org.nrg.xdat.om.DianVisfreqdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianVisfreqdata> getDianVisfreqdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianVisfreqdata> al = new ArrayList<org.nrg.xdat.om.DianVisfreqdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianVisfreqdata> getDianVisfreqdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianVisfreqdata> al = new ArrayList<org.nrg.xdat.om.DianVisfreqdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianVisfreqdata getDianVisfreqdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:visFreqData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianVisfreqdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
