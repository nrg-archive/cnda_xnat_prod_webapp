/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoIqWasi1999data extends IqAbstractiqtest implements org.nrg.xdat.model.IqWasi1999dataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoIqWasi1999data.class);
	public static String SCHEMA_ELEMENT_NAME="iq:wasi1999Data";

	public AutoIqWasi1999data(ItemI item)
	{
		super(item);
	}

	public AutoIqWasi1999data(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoIqWasi1999data(UserI user)
	 **/
	public AutoIqWasi1999data(){}

	public AutoIqWasi1999data(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "iq:wasi1999Data";
	}
	 private org.nrg.xdat.om.IqAbstractiqtest _Abstractiqtest =null;

	/**
	 * abstractIQTest
	 * @return org.nrg.xdat.om.IqAbstractiqtest
	 */
	public org.nrg.xdat.om.IqAbstractiqtest getAbstractiqtest() {
		try{
			if (_Abstractiqtest==null){
				_Abstractiqtest=((IqAbstractiqtest)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("abstractIQTest")));
				return _Abstractiqtest;
			}else {
				return _Abstractiqtest;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for abstractIQTest.
	 * @param v Value to Set.
	 */
	public void setAbstractiqtest(ItemI v) throws Exception{
		_Abstractiqtest =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/abstractIQTest",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/abstractIQTest",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * abstractIQTest
	 * set org.nrg.xdat.model.IqAbstractiqtestI
	 */
	public <A extends org.nrg.xdat.model.IqAbstractiqtestI> void setAbstractiqtest(A item) throws Exception{
	setAbstractiqtest((ItemI)item);
	}

	/**
	 * Removes the abstractIQTest.
	 * */
	public void removeAbstractiqtest() {
		_Abstractiqtest =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/abstractIQTest",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _WasiVraw=null;

	/**
	 * @return Returns the wasi_v_raw.
	 */
	public Integer getWasiVraw() {
		try{
			if (_WasiVraw==null){
				_WasiVraw=getIntegerProperty("wasi_v_raw");
				return _WasiVraw;
			}else {
				return _WasiVraw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wasi_v_raw.
	 * @param v Value to Set.
	 */
	public void setWasiVraw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wasi_v_raw",v);
		_WasiVraw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _WasiVt=null;

	/**
	 * @return Returns the wasi_v_t.
	 */
	public Integer getWasiVt() {
		try{
			if (_WasiVt==null){
				_WasiVt=getIntegerProperty("wasi_v_t");
				return _WasiVt;
			}else {
				return _WasiVt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wasi_v_t.
	 * @param v Value to Set.
	 */
	public void setWasiVt(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wasi_v_t",v);
		_WasiVt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _WasiVnote=null;

	/**
	 * @return Returns the wasi_v_note.
	 */
	public String getWasiVnote(){
		try{
			if (_WasiVnote==null){
				_WasiVnote=getStringProperty("wasi_v_note");
				return _WasiVnote;
			}else {
				return _WasiVnote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wasi_v_note.
	 * @param v Value to Set.
	 */
	public void setWasiVnote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wasi_v_note",v);
		_WasiVnote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _WasiBraw=null;

	/**
	 * @return Returns the wasi_b_raw.
	 */
	public Integer getWasiBraw() {
		try{
			if (_WasiBraw==null){
				_WasiBraw=getIntegerProperty("wasi_b_raw");
				return _WasiBraw;
			}else {
				return _WasiBraw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wasi_b_raw.
	 * @param v Value to Set.
	 */
	public void setWasiBraw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wasi_b_raw",v);
		_WasiBraw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _WasiBt=null;

	/**
	 * @return Returns the wasi_b_t.
	 */
	public Integer getWasiBt() {
		try{
			if (_WasiBt==null){
				_WasiBt=getIntegerProperty("wasi_b_t");
				return _WasiBt;
			}else {
				return _WasiBt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wasi_b_t.
	 * @param v Value to Set.
	 */
	public void setWasiBt(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wasi_b_t",v);
		_WasiBt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _WasiBnote=null;

	/**
	 * @return Returns the wasi_b_note.
	 */
	public String getWasiBnote(){
		try{
			if (_WasiBnote==null){
				_WasiBnote=getStringProperty("wasi_b_note");
				return _WasiBnote;
			}else {
				return _WasiBnote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wasi_b_note.
	 * @param v Value to Set.
	 */
	public void setWasiBnote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wasi_b_note",v);
		_WasiBnote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _WasiSraw=null;

	/**
	 * @return Returns the wasi_s_raw.
	 */
	public Integer getWasiSraw() {
		try{
			if (_WasiSraw==null){
				_WasiSraw=getIntegerProperty("wasi_s_raw");
				return _WasiSraw;
			}else {
				return _WasiSraw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wasi_s_raw.
	 * @param v Value to Set.
	 */
	public void setWasiSraw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wasi_s_raw",v);
		_WasiSraw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _WasiSt=null;

	/**
	 * @return Returns the wasi_s_t.
	 */
	public Integer getWasiSt() {
		try{
			if (_WasiSt==null){
				_WasiSt=getIntegerProperty("wasi_s_t");
				return _WasiSt;
			}else {
				return _WasiSt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wasi_s_t.
	 * @param v Value to Set.
	 */
	public void setWasiSt(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wasi_s_t",v);
		_WasiSt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _WasiSnote=null;

	/**
	 * @return Returns the wasi_s_note.
	 */
	public String getWasiSnote(){
		try{
			if (_WasiSnote==null){
				_WasiSnote=getStringProperty("wasi_s_note");
				return _WasiSnote;
			}else {
				return _WasiSnote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wasi_s_note.
	 * @param v Value to Set.
	 */
	public void setWasiSnote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wasi_s_note",v);
		_WasiSnote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _WasiMraw=null;

	/**
	 * @return Returns the wasi_m_raw.
	 */
	public Integer getWasiMraw() {
		try{
			if (_WasiMraw==null){
				_WasiMraw=getIntegerProperty("wasi_m_raw");
				return _WasiMraw;
			}else {
				return _WasiMraw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wasi_m_raw.
	 * @param v Value to Set.
	 */
	public void setWasiMraw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wasi_m_raw",v);
		_WasiMraw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _WasiMt=null;

	/**
	 * @return Returns the wasi_m_t.
	 */
	public Integer getWasiMt() {
		try{
			if (_WasiMt==null){
				_WasiMt=getIntegerProperty("wasi_m_t");
				return _WasiMt;
			}else {
				return _WasiMt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wasi_m_t.
	 * @param v Value to Set.
	 */
	public void setWasiMt(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wasi_m_t",v);
		_WasiMt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _WasiMnote=null;

	/**
	 * @return Returns the wasi_m_note.
	 */
	public String getWasiMnote(){
		try{
			if (_WasiMnote==null){
				_WasiMnote=getStringProperty("wasi_m_note");
				return _WasiMnote;
			}else {
				return _WasiMnote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wasi_m_note.
	 * @param v Value to Set.
	 */
	public void setWasiMnote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wasi_m_note",v);
		_WasiMnote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab1_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab1/response.
	 */
	public String getScoreform_section1_vocab1_response(){
		try{
			if (_Scoreform_section1_vocab1_response==null){
				_Scoreform_section1_vocab1_response=getStringProperty("ScoreForm/section1/vocab1/response");
				return _Scoreform_section1_vocab1_response;
			}else {
				return _Scoreform_section1_vocab1_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab1/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab1_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab1/response",v);
		_Scoreform_section1_vocab1_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab1_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab1/score.
	 */
	public Integer getScoreform_section1_vocab1_score() {
		try{
			if (_Scoreform_section1_vocab1_score==null){
				_Scoreform_section1_vocab1_score=getIntegerProperty("ScoreForm/section1/vocab1/score");
				return _Scoreform_section1_vocab1_score;
			}else {
				return _Scoreform_section1_vocab1_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab1/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab1_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab1/score",v);
		_Scoreform_section1_vocab1_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab2_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab2/response.
	 */
	public String getScoreform_section1_vocab2_response(){
		try{
			if (_Scoreform_section1_vocab2_response==null){
				_Scoreform_section1_vocab2_response=getStringProperty("ScoreForm/section1/vocab2/response");
				return _Scoreform_section1_vocab2_response;
			}else {
				return _Scoreform_section1_vocab2_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab2/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab2_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab2/response",v);
		_Scoreform_section1_vocab2_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab2_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab2/score.
	 */
	public Integer getScoreform_section1_vocab2_score() {
		try{
			if (_Scoreform_section1_vocab2_score==null){
				_Scoreform_section1_vocab2_score=getIntegerProperty("ScoreForm/section1/vocab2/score");
				return _Scoreform_section1_vocab2_score;
			}else {
				return _Scoreform_section1_vocab2_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab2/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab2_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab2/score",v);
		_Scoreform_section1_vocab2_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab3_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab3/response.
	 */
	public String getScoreform_section1_vocab3_response(){
		try{
			if (_Scoreform_section1_vocab3_response==null){
				_Scoreform_section1_vocab3_response=getStringProperty("ScoreForm/section1/vocab3/response");
				return _Scoreform_section1_vocab3_response;
			}else {
				return _Scoreform_section1_vocab3_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab3/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab3_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab3/response",v);
		_Scoreform_section1_vocab3_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab3_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab3/score.
	 */
	public Integer getScoreform_section1_vocab3_score() {
		try{
			if (_Scoreform_section1_vocab3_score==null){
				_Scoreform_section1_vocab3_score=getIntegerProperty("ScoreForm/section1/vocab3/score");
				return _Scoreform_section1_vocab3_score;
			}else {
				return _Scoreform_section1_vocab3_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab3/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab3_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab3/score",v);
		_Scoreform_section1_vocab3_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab4_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab4/response.
	 */
	public String getScoreform_section1_vocab4_response(){
		try{
			if (_Scoreform_section1_vocab4_response==null){
				_Scoreform_section1_vocab4_response=getStringProperty("ScoreForm/section1/vocab4/response");
				return _Scoreform_section1_vocab4_response;
			}else {
				return _Scoreform_section1_vocab4_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab4/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab4_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab4/response",v);
		_Scoreform_section1_vocab4_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab4_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab4/score.
	 */
	public Integer getScoreform_section1_vocab4_score() {
		try{
			if (_Scoreform_section1_vocab4_score==null){
				_Scoreform_section1_vocab4_score=getIntegerProperty("ScoreForm/section1/vocab4/score");
				return _Scoreform_section1_vocab4_score;
			}else {
				return _Scoreform_section1_vocab4_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab4/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab4_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab4/score",v);
		_Scoreform_section1_vocab4_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab5_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab5/response.
	 */
	public String getScoreform_section1_vocab5_response(){
		try{
			if (_Scoreform_section1_vocab5_response==null){
				_Scoreform_section1_vocab5_response=getStringProperty("ScoreForm/section1/vocab5/response");
				return _Scoreform_section1_vocab5_response;
			}else {
				return _Scoreform_section1_vocab5_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab5/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab5_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab5/response",v);
		_Scoreform_section1_vocab5_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab5_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab5/score.
	 */
	public Integer getScoreform_section1_vocab5_score() {
		try{
			if (_Scoreform_section1_vocab5_score==null){
				_Scoreform_section1_vocab5_score=getIntegerProperty("ScoreForm/section1/vocab5/score");
				return _Scoreform_section1_vocab5_score;
			}else {
				return _Scoreform_section1_vocab5_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab5/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab5_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab5/score",v);
		_Scoreform_section1_vocab5_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab6_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab6/response.
	 */
	public String getScoreform_section1_vocab6_response(){
		try{
			if (_Scoreform_section1_vocab6_response==null){
				_Scoreform_section1_vocab6_response=getStringProperty("ScoreForm/section1/vocab6/response");
				return _Scoreform_section1_vocab6_response;
			}else {
				return _Scoreform_section1_vocab6_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab6/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab6_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab6/response",v);
		_Scoreform_section1_vocab6_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab6_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab6/score.
	 */
	public Integer getScoreform_section1_vocab6_score() {
		try{
			if (_Scoreform_section1_vocab6_score==null){
				_Scoreform_section1_vocab6_score=getIntegerProperty("ScoreForm/section1/vocab6/score");
				return _Scoreform_section1_vocab6_score;
			}else {
				return _Scoreform_section1_vocab6_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab6/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab6_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab6/score",v);
		_Scoreform_section1_vocab6_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab7_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab7/response.
	 */
	public String getScoreform_section1_vocab7_response(){
		try{
			if (_Scoreform_section1_vocab7_response==null){
				_Scoreform_section1_vocab7_response=getStringProperty("ScoreForm/section1/vocab7/response");
				return _Scoreform_section1_vocab7_response;
			}else {
				return _Scoreform_section1_vocab7_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab7/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab7_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab7/response",v);
		_Scoreform_section1_vocab7_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab7_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab7/score.
	 */
	public Integer getScoreform_section1_vocab7_score() {
		try{
			if (_Scoreform_section1_vocab7_score==null){
				_Scoreform_section1_vocab7_score=getIntegerProperty("ScoreForm/section1/vocab7/score");
				return _Scoreform_section1_vocab7_score;
			}else {
				return _Scoreform_section1_vocab7_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab7/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab7_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab7/score",v);
		_Scoreform_section1_vocab7_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab8_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab8/response.
	 */
	public String getScoreform_section1_vocab8_response(){
		try{
			if (_Scoreform_section1_vocab8_response==null){
				_Scoreform_section1_vocab8_response=getStringProperty("ScoreForm/section1/vocab8/response");
				return _Scoreform_section1_vocab8_response;
			}else {
				return _Scoreform_section1_vocab8_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab8/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab8_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab8/response",v);
		_Scoreform_section1_vocab8_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab8_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab8/score.
	 */
	public Integer getScoreform_section1_vocab8_score() {
		try{
			if (_Scoreform_section1_vocab8_score==null){
				_Scoreform_section1_vocab8_score=getIntegerProperty("ScoreForm/section1/vocab8/score");
				return _Scoreform_section1_vocab8_score;
			}else {
				return _Scoreform_section1_vocab8_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab8/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab8_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab8/score",v);
		_Scoreform_section1_vocab8_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab9_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab9/response.
	 */
	public String getScoreform_section1_vocab9_response(){
		try{
			if (_Scoreform_section1_vocab9_response==null){
				_Scoreform_section1_vocab9_response=getStringProperty("ScoreForm/section1/vocab9/response");
				return _Scoreform_section1_vocab9_response;
			}else {
				return _Scoreform_section1_vocab9_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab9/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab9_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab9/response",v);
		_Scoreform_section1_vocab9_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab9_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab9/score.
	 */
	public Integer getScoreform_section1_vocab9_score() {
		try{
			if (_Scoreform_section1_vocab9_score==null){
				_Scoreform_section1_vocab9_score=getIntegerProperty("ScoreForm/section1/vocab9/score");
				return _Scoreform_section1_vocab9_score;
			}else {
				return _Scoreform_section1_vocab9_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab9/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab9_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab9/score",v);
		_Scoreform_section1_vocab9_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab10_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab10/response.
	 */
	public String getScoreform_section1_vocab10_response(){
		try{
			if (_Scoreform_section1_vocab10_response==null){
				_Scoreform_section1_vocab10_response=getStringProperty("ScoreForm/section1/vocab10/response");
				return _Scoreform_section1_vocab10_response;
			}else {
				return _Scoreform_section1_vocab10_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab10/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab10_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab10/response",v);
		_Scoreform_section1_vocab10_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab10_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab10/score.
	 */
	public Integer getScoreform_section1_vocab10_score() {
		try{
			if (_Scoreform_section1_vocab10_score==null){
				_Scoreform_section1_vocab10_score=getIntegerProperty("ScoreForm/section1/vocab10/score");
				return _Scoreform_section1_vocab10_score;
			}else {
				return _Scoreform_section1_vocab10_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab10/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab10_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab10/score",v);
		_Scoreform_section1_vocab10_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab11_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab11/response.
	 */
	public String getScoreform_section1_vocab11_response(){
		try{
			if (_Scoreform_section1_vocab11_response==null){
				_Scoreform_section1_vocab11_response=getStringProperty("ScoreForm/section1/vocab11/response");
				return _Scoreform_section1_vocab11_response;
			}else {
				return _Scoreform_section1_vocab11_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab11/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab11_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab11/response",v);
		_Scoreform_section1_vocab11_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab11_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab11/score.
	 */
	public Integer getScoreform_section1_vocab11_score() {
		try{
			if (_Scoreform_section1_vocab11_score==null){
				_Scoreform_section1_vocab11_score=getIntegerProperty("ScoreForm/section1/vocab11/score");
				return _Scoreform_section1_vocab11_score;
			}else {
				return _Scoreform_section1_vocab11_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab11/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab11_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab11/score",v);
		_Scoreform_section1_vocab11_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab12_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab12/response.
	 */
	public String getScoreform_section1_vocab12_response(){
		try{
			if (_Scoreform_section1_vocab12_response==null){
				_Scoreform_section1_vocab12_response=getStringProperty("ScoreForm/section1/vocab12/response");
				return _Scoreform_section1_vocab12_response;
			}else {
				return _Scoreform_section1_vocab12_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab12/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab12_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab12/response",v);
		_Scoreform_section1_vocab12_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab12_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab12/score.
	 */
	public Integer getScoreform_section1_vocab12_score() {
		try{
			if (_Scoreform_section1_vocab12_score==null){
				_Scoreform_section1_vocab12_score=getIntegerProperty("ScoreForm/section1/vocab12/score");
				return _Scoreform_section1_vocab12_score;
			}else {
				return _Scoreform_section1_vocab12_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab12/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab12_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab12/score",v);
		_Scoreform_section1_vocab12_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab13_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab13/response.
	 */
	public String getScoreform_section1_vocab13_response(){
		try{
			if (_Scoreform_section1_vocab13_response==null){
				_Scoreform_section1_vocab13_response=getStringProperty("ScoreForm/section1/vocab13/response");
				return _Scoreform_section1_vocab13_response;
			}else {
				return _Scoreform_section1_vocab13_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab13/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab13_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab13/response",v);
		_Scoreform_section1_vocab13_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab13_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab13/score.
	 */
	public Integer getScoreform_section1_vocab13_score() {
		try{
			if (_Scoreform_section1_vocab13_score==null){
				_Scoreform_section1_vocab13_score=getIntegerProperty("ScoreForm/section1/vocab13/score");
				return _Scoreform_section1_vocab13_score;
			}else {
				return _Scoreform_section1_vocab13_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab13/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab13_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab13/score",v);
		_Scoreform_section1_vocab13_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab14_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab14/response.
	 */
	public String getScoreform_section1_vocab14_response(){
		try{
			if (_Scoreform_section1_vocab14_response==null){
				_Scoreform_section1_vocab14_response=getStringProperty("ScoreForm/section1/vocab14/response");
				return _Scoreform_section1_vocab14_response;
			}else {
				return _Scoreform_section1_vocab14_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab14/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab14_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab14/response",v);
		_Scoreform_section1_vocab14_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab14_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab14/score.
	 */
	public Integer getScoreform_section1_vocab14_score() {
		try{
			if (_Scoreform_section1_vocab14_score==null){
				_Scoreform_section1_vocab14_score=getIntegerProperty("ScoreForm/section1/vocab14/score");
				return _Scoreform_section1_vocab14_score;
			}else {
				return _Scoreform_section1_vocab14_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab14/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab14_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab14/score",v);
		_Scoreform_section1_vocab14_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab15_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab15/response.
	 */
	public String getScoreform_section1_vocab15_response(){
		try{
			if (_Scoreform_section1_vocab15_response==null){
				_Scoreform_section1_vocab15_response=getStringProperty("ScoreForm/section1/vocab15/response");
				return _Scoreform_section1_vocab15_response;
			}else {
				return _Scoreform_section1_vocab15_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab15/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab15_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab15/response",v);
		_Scoreform_section1_vocab15_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab15_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab15/score.
	 */
	public Integer getScoreform_section1_vocab15_score() {
		try{
			if (_Scoreform_section1_vocab15_score==null){
				_Scoreform_section1_vocab15_score=getIntegerProperty("ScoreForm/section1/vocab15/score");
				return _Scoreform_section1_vocab15_score;
			}else {
				return _Scoreform_section1_vocab15_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab15/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab15_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab15/score",v);
		_Scoreform_section1_vocab15_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab16_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab16/response.
	 */
	public String getScoreform_section1_vocab16_response(){
		try{
			if (_Scoreform_section1_vocab16_response==null){
				_Scoreform_section1_vocab16_response=getStringProperty("ScoreForm/section1/vocab16/response");
				return _Scoreform_section1_vocab16_response;
			}else {
				return _Scoreform_section1_vocab16_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab16/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab16_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab16/response",v);
		_Scoreform_section1_vocab16_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab16_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab16/score.
	 */
	public Integer getScoreform_section1_vocab16_score() {
		try{
			if (_Scoreform_section1_vocab16_score==null){
				_Scoreform_section1_vocab16_score=getIntegerProperty("ScoreForm/section1/vocab16/score");
				return _Scoreform_section1_vocab16_score;
			}else {
				return _Scoreform_section1_vocab16_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab16/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab16_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab16/score",v);
		_Scoreform_section1_vocab16_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab17_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab17/response.
	 */
	public String getScoreform_section1_vocab17_response(){
		try{
			if (_Scoreform_section1_vocab17_response==null){
				_Scoreform_section1_vocab17_response=getStringProperty("ScoreForm/section1/vocab17/response");
				return _Scoreform_section1_vocab17_response;
			}else {
				return _Scoreform_section1_vocab17_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab17/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab17_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab17/response",v);
		_Scoreform_section1_vocab17_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab17_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab17/score.
	 */
	public Integer getScoreform_section1_vocab17_score() {
		try{
			if (_Scoreform_section1_vocab17_score==null){
				_Scoreform_section1_vocab17_score=getIntegerProperty("ScoreForm/section1/vocab17/score");
				return _Scoreform_section1_vocab17_score;
			}else {
				return _Scoreform_section1_vocab17_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab17/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab17_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab17/score",v);
		_Scoreform_section1_vocab17_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab18_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab18/response.
	 */
	public String getScoreform_section1_vocab18_response(){
		try{
			if (_Scoreform_section1_vocab18_response==null){
				_Scoreform_section1_vocab18_response=getStringProperty("ScoreForm/section1/vocab18/response");
				return _Scoreform_section1_vocab18_response;
			}else {
				return _Scoreform_section1_vocab18_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab18/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab18_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab18/response",v);
		_Scoreform_section1_vocab18_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab18_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab18/score.
	 */
	public Integer getScoreform_section1_vocab18_score() {
		try{
			if (_Scoreform_section1_vocab18_score==null){
				_Scoreform_section1_vocab18_score=getIntegerProperty("ScoreForm/section1/vocab18/score");
				return _Scoreform_section1_vocab18_score;
			}else {
				return _Scoreform_section1_vocab18_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab18/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab18_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab18/score",v);
		_Scoreform_section1_vocab18_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab19_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab19/response.
	 */
	public String getScoreform_section1_vocab19_response(){
		try{
			if (_Scoreform_section1_vocab19_response==null){
				_Scoreform_section1_vocab19_response=getStringProperty("ScoreForm/section1/vocab19/response");
				return _Scoreform_section1_vocab19_response;
			}else {
				return _Scoreform_section1_vocab19_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab19/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab19_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab19/response",v);
		_Scoreform_section1_vocab19_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab19_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab19/score.
	 */
	public Integer getScoreform_section1_vocab19_score() {
		try{
			if (_Scoreform_section1_vocab19_score==null){
				_Scoreform_section1_vocab19_score=getIntegerProperty("ScoreForm/section1/vocab19/score");
				return _Scoreform_section1_vocab19_score;
			}else {
				return _Scoreform_section1_vocab19_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab19/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab19_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab19/score",v);
		_Scoreform_section1_vocab19_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab20_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab20/response.
	 */
	public String getScoreform_section1_vocab20_response(){
		try{
			if (_Scoreform_section1_vocab20_response==null){
				_Scoreform_section1_vocab20_response=getStringProperty("ScoreForm/section1/vocab20/response");
				return _Scoreform_section1_vocab20_response;
			}else {
				return _Scoreform_section1_vocab20_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab20/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab20_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab20/response",v);
		_Scoreform_section1_vocab20_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab20_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab20/score.
	 */
	public Integer getScoreform_section1_vocab20_score() {
		try{
			if (_Scoreform_section1_vocab20_score==null){
				_Scoreform_section1_vocab20_score=getIntegerProperty("ScoreForm/section1/vocab20/score");
				return _Scoreform_section1_vocab20_score;
			}else {
				return _Scoreform_section1_vocab20_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab20/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab20_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab20/score",v);
		_Scoreform_section1_vocab20_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab21_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab21/response.
	 */
	public String getScoreform_section1_vocab21_response(){
		try{
			if (_Scoreform_section1_vocab21_response==null){
				_Scoreform_section1_vocab21_response=getStringProperty("ScoreForm/section1/vocab21/response");
				return _Scoreform_section1_vocab21_response;
			}else {
				return _Scoreform_section1_vocab21_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab21/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab21_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab21/response",v);
		_Scoreform_section1_vocab21_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab21_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab21/score.
	 */
	public Integer getScoreform_section1_vocab21_score() {
		try{
			if (_Scoreform_section1_vocab21_score==null){
				_Scoreform_section1_vocab21_score=getIntegerProperty("ScoreForm/section1/vocab21/score");
				return _Scoreform_section1_vocab21_score;
			}else {
				return _Scoreform_section1_vocab21_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab21/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab21_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab21/score",v);
		_Scoreform_section1_vocab21_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab22_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab22/response.
	 */
	public String getScoreform_section1_vocab22_response(){
		try{
			if (_Scoreform_section1_vocab22_response==null){
				_Scoreform_section1_vocab22_response=getStringProperty("ScoreForm/section1/vocab22/response");
				return _Scoreform_section1_vocab22_response;
			}else {
				return _Scoreform_section1_vocab22_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab22/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab22_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab22/response",v);
		_Scoreform_section1_vocab22_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab22_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab22/score.
	 */
	public Integer getScoreform_section1_vocab22_score() {
		try{
			if (_Scoreform_section1_vocab22_score==null){
				_Scoreform_section1_vocab22_score=getIntegerProperty("ScoreForm/section1/vocab22/score");
				return _Scoreform_section1_vocab22_score;
			}else {
				return _Scoreform_section1_vocab22_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab22/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab22_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab22/score",v);
		_Scoreform_section1_vocab22_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab23_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab23/response.
	 */
	public String getScoreform_section1_vocab23_response(){
		try{
			if (_Scoreform_section1_vocab23_response==null){
				_Scoreform_section1_vocab23_response=getStringProperty("ScoreForm/section1/vocab23/response");
				return _Scoreform_section1_vocab23_response;
			}else {
				return _Scoreform_section1_vocab23_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab23/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab23_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab23/response",v);
		_Scoreform_section1_vocab23_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab23_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab23/score.
	 */
	public Integer getScoreform_section1_vocab23_score() {
		try{
			if (_Scoreform_section1_vocab23_score==null){
				_Scoreform_section1_vocab23_score=getIntegerProperty("ScoreForm/section1/vocab23/score");
				return _Scoreform_section1_vocab23_score;
			}else {
				return _Scoreform_section1_vocab23_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab23/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab23_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab23/score",v);
		_Scoreform_section1_vocab23_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab24_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab24/response.
	 */
	public String getScoreform_section1_vocab24_response(){
		try{
			if (_Scoreform_section1_vocab24_response==null){
				_Scoreform_section1_vocab24_response=getStringProperty("ScoreForm/section1/vocab24/response");
				return _Scoreform_section1_vocab24_response;
			}else {
				return _Scoreform_section1_vocab24_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab24/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab24_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab24/response",v);
		_Scoreform_section1_vocab24_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab24_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab24/score.
	 */
	public Integer getScoreform_section1_vocab24_score() {
		try{
			if (_Scoreform_section1_vocab24_score==null){
				_Scoreform_section1_vocab24_score=getIntegerProperty("ScoreForm/section1/vocab24/score");
				return _Scoreform_section1_vocab24_score;
			}else {
				return _Scoreform_section1_vocab24_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab24/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab24_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab24/score",v);
		_Scoreform_section1_vocab24_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab25_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab25/response.
	 */
	public String getScoreform_section1_vocab25_response(){
		try{
			if (_Scoreform_section1_vocab25_response==null){
				_Scoreform_section1_vocab25_response=getStringProperty("ScoreForm/section1/vocab25/response");
				return _Scoreform_section1_vocab25_response;
			}else {
				return _Scoreform_section1_vocab25_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab25/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab25_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab25/response",v);
		_Scoreform_section1_vocab25_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab25_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab25/score.
	 */
	public Integer getScoreform_section1_vocab25_score() {
		try{
			if (_Scoreform_section1_vocab25_score==null){
				_Scoreform_section1_vocab25_score=getIntegerProperty("ScoreForm/section1/vocab25/score");
				return _Scoreform_section1_vocab25_score;
			}else {
				return _Scoreform_section1_vocab25_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab25/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab25_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab25/score",v);
		_Scoreform_section1_vocab25_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab26_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab26/response.
	 */
	public String getScoreform_section1_vocab26_response(){
		try{
			if (_Scoreform_section1_vocab26_response==null){
				_Scoreform_section1_vocab26_response=getStringProperty("ScoreForm/section1/vocab26/response");
				return _Scoreform_section1_vocab26_response;
			}else {
				return _Scoreform_section1_vocab26_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab26/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab26_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab26/response",v);
		_Scoreform_section1_vocab26_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab26_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab26/score.
	 */
	public Integer getScoreform_section1_vocab26_score() {
		try{
			if (_Scoreform_section1_vocab26_score==null){
				_Scoreform_section1_vocab26_score=getIntegerProperty("ScoreForm/section1/vocab26/score");
				return _Scoreform_section1_vocab26_score;
			}else {
				return _Scoreform_section1_vocab26_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab26/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab26_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab26/score",v);
		_Scoreform_section1_vocab26_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab27_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab27/response.
	 */
	public String getScoreform_section1_vocab27_response(){
		try{
			if (_Scoreform_section1_vocab27_response==null){
				_Scoreform_section1_vocab27_response=getStringProperty("ScoreForm/section1/vocab27/response");
				return _Scoreform_section1_vocab27_response;
			}else {
				return _Scoreform_section1_vocab27_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab27/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab27_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab27/response",v);
		_Scoreform_section1_vocab27_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab27_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab27/score.
	 */
	public Integer getScoreform_section1_vocab27_score() {
		try{
			if (_Scoreform_section1_vocab27_score==null){
				_Scoreform_section1_vocab27_score=getIntegerProperty("ScoreForm/section1/vocab27/score");
				return _Scoreform_section1_vocab27_score;
			}else {
				return _Scoreform_section1_vocab27_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab27/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab27_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab27/score",v);
		_Scoreform_section1_vocab27_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab28_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab28/response.
	 */
	public String getScoreform_section1_vocab28_response(){
		try{
			if (_Scoreform_section1_vocab28_response==null){
				_Scoreform_section1_vocab28_response=getStringProperty("ScoreForm/section1/vocab28/response");
				return _Scoreform_section1_vocab28_response;
			}else {
				return _Scoreform_section1_vocab28_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab28/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab28_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab28/response",v);
		_Scoreform_section1_vocab28_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab28_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab28/score.
	 */
	public Integer getScoreform_section1_vocab28_score() {
		try{
			if (_Scoreform_section1_vocab28_score==null){
				_Scoreform_section1_vocab28_score=getIntegerProperty("ScoreForm/section1/vocab28/score");
				return _Scoreform_section1_vocab28_score;
			}else {
				return _Scoreform_section1_vocab28_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab28/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab28_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab28/score",v);
		_Scoreform_section1_vocab28_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab29_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab29/response.
	 */
	public String getScoreform_section1_vocab29_response(){
		try{
			if (_Scoreform_section1_vocab29_response==null){
				_Scoreform_section1_vocab29_response=getStringProperty("ScoreForm/section1/vocab29/response");
				return _Scoreform_section1_vocab29_response;
			}else {
				return _Scoreform_section1_vocab29_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab29/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab29_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab29/response",v);
		_Scoreform_section1_vocab29_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab29_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab29/score.
	 */
	public Integer getScoreform_section1_vocab29_score() {
		try{
			if (_Scoreform_section1_vocab29_score==null){
				_Scoreform_section1_vocab29_score=getIntegerProperty("ScoreForm/section1/vocab29/score");
				return _Scoreform_section1_vocab29_score;
			}else {
				return _Scoreform_section1_vocab29_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab29/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab29_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab29/score",v);
		_Scoreform_section1_vocab29_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab30_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab30/response.
	 */
	public String getScoreform_section1_vocab30_response(){
		try{
			if (_Scoreform_section1_vocab30_response==null){
				_Scoreform_section1_vocab30_response=getStringProperty("ScoreForm/section1/vocab30/response");
				return _Scoreform_section1_vocab30_response;
			}else {
				return _Scoreform_section1_vocab30_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab30/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab30_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab30/response",v);
		_Scoreform_section1_vocab30_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab30_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab30/score.
	 */
	public Integer getScoreform_section1_vocab30_score() {
		try{
			if (_Scoreform_section1_vocab30_score==null){
				_Scoreform_section1_vocab30_score=getIntegerProperty("ScoreForm/section1/vocab30/score");
				return _Scoreform_section1_vocab30_score;
			}else {
				return _Scoreform_section1_vocab30_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab30/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab30_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab30/score",v);
		_Scoreform_section1_vocab30_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab31_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab31/response.
	 */
	public String getScoreform_section1_vocab31_response(){
		try{
			if (_Scoreform_section1_vocab31_response==null){
				_Scoreform_section1_vocab31_response=getStringProperty("ScoreForm/section1/vocab31/response");
				return _Scoreform_section1_vocab31_response;
			}else {
				return _Scoreform_section1_vocab31_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab31/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab31_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab31/response",v);
		_Scoreform_section1_vocab31_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab31_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab31/score.
	 */
	public Integer getScoreform_section1_vocab31_score() {
		try{
			if (_Scoreform_section1_vocab31_score==null){
				_Scoreform_section1_vocab31_score=getIntegerProperty("ScoreForm/section1/vocab31/score");
				return _Scoreform_section1_vocab31_score;
			}else {
				return _Scoreform_section1_vocab31_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab31/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab31_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab31/score",v);
		_Scoreform_section1_vocab31_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab32_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab32/response.
	 */
	public String getScoreform_section1_vocab32_response(){
		try{
			if (_Scoreform_section1_vocab32_response==null){
				_Scoreform_section1_vocab32_response=getStringProperty("ScoreForm/section1/vocab32/response");
				return _Scoreform_section1_vocab32_response;
			}else {
				return _Scoreform_section1_vocab32_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab32/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab32_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab32/response",v);
		_Scoreform_section1_vocab32_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab32_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab32/score.
	 */
	public Integer getScoreform_section1_vocab32_score() {
		try{
			if (_Scoreform_section1_vocab32_score==null){
				_Scoreform_section1_vocab32_score=getIntegerProperty("ScoreForm/section1/vocab32/score");
				return _Scoreform_section1_vocab32_score;
			}else {
				return _Scoreform_section1_vocab32_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab32/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab32_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab32/score",v);
		_Scoreform_section1_vocab32_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab33_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab33/response.
	 */
	public String getScoreform_section1_vocab33_response(){
		try{
			if (_Scoreform_section1_vocab33_response==null){
				_Scoreform_section1_vocab33_response=getStringProperty("ScoreForm/section1/vocab33/response");
				return _Scoreform_section1_vocab33_response;
			}else {
				return _Scoreform_section1_vocab33_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab33/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab33_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab33/response",v);
		_Scoreform_section1_vocab33_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab33_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab33/score.
	 */
	public Integer getScoreform_section1_vocab33_score() {
		try{
			if (_Scoreform_section1_vocab33_score==null){
				_Scoreform_section1_vocab33_score=getIntegerProperty("ScoreForm/section1/vocab33/score");
				return _Scoreform_section1_vocab33_score;
			}else {
				return _Scoreform_section1_vocab33_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab33/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab33_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab33/score",v);
		_Scoreform_section1_vocab33_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab34_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab34/response.
	 */
	public String getScoreform_section1_vocab34_response(){
		try{
			if (_Scoreform_section1_vocab34_response==null){
				_Scoreform_section1_vocab34_response=getStringProperty("ScoreForm/section1/vocab34/response");
				return _Scoreform_section1_vocab34_response;
			}else {
				return _Scoreform_section1_vocab34_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab34/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab34_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab34/response",v);
		_Scoreform_section1_vocab34_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab34_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab34/score.
	 */
	public Integer getScoreform_section1_vocab34_score() {
		try{
			if (_Scoreform_section1_vocab34_score==null){
				_Scoreform_section1_vocab34_score=getIntegerProperty("ScoreForm/section1/vocab34/score");
				return _Scoreform_section1_vocab34_score;
			}else {
				return _Scoreform_section1_vocab34_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab34/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab34_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab34/score",v);
		_Scoreform_section1_vocab34_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab35_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab35/response.
	 */
	public String getScoreform_section1_vocab35_response(){
		try{
			if (_Scoreform_section1_vocab35_response==null){
				_Scoreform_section1_vocab35_response=getStringProperty("ScoreForm/section1/vocab35/response");
				return _Scoreform_section1_vocab35_response;
			}else {
				return _Scoreform_section1_vocab35_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab35/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab35_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab35/response",v);
		_Scoreform_section1_vocab35_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab35_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab35/score.
	 */
	public Integer getScoreform_section1_vocab35_score() {
		try{
			if (_Scoreform_section1_vocab35_score==null){
				_Scoreform_section1_vocab35_score=getIntegerProperty("ScoreForm/section1/vocab35/score");
				return _Scoreform_section1_vocab35_score;
			}else {
				return _Scoreform_section1_vocab35_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab35/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab35_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab35/score",v);
		_Scoreform_section1_vocab35_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab36_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab36/response.
	 */
	public String getScoreform_section1_vocab36_response(){
		try{
			if (_Scoreform_section1_vocab36_response==null){
				_Scoreform_section1_vocab36_response=getStringProperty("ScoreForm/section1/vocab36/response");
				return _Scoreform_section1_vocab36_response;
			}else {
				return _Scoreform_section1_vocab36_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab36/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab36_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab36/response",v);
		_Scoreform_section1_vocab36_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab36_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab36/score.
	 */
	public Integer getScoreform_section1_vocab36_score() {
		try{
			if (_Scoreform_section1_vocab36_score==null){
				_Scoreform_section1_vocab36_score=getIntegerProperty("ScoreForm/section1/vocab36/score");
				return _Scoreform_section1_vocab36_score;
			}else {
				return _Scoreform_section1_vocab36_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab36/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab36_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab36/score",v);
		_Scoreform_section1_vocab36_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab37_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab37/response.
	 */
	public String getScoreform_section1_vocab37_response(){
		try{
			if (_Scoreform_section1_vocab37_response==null){
				_Scoreform_section1_vocab37_response=getStringProperty("ScoreForm/section1/vocab37/response");
				return _Scoreform_section1_vocab37_response;
			}else {
				return _Scoreform_section1_vocab37_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab37/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab37_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab37/response",v);
		_Scoreform_section1_vocab37_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab37_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab37/score.
	 */
	public Integer getScoreform_section1_vocab37_score() {
		try{
			if (_Scoreform_section1_vocab37_score==null){
				_Scoreform_section1_vocab37_score=getIntegerProperty("ScoreForm/section1/vocab37/score");
				return _Scoreform_section1_vocab37_score;
			}else {
				return _Scoreform_section1_vocab37_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab37/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab37_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab37/score",v);
		_Scoreform_section1_vocab37_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab38_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab38/response.
	 */
	public String getScoreform_section1_vocab38_response(){
		try{
			if (_Scoreform_section1_vocab38_response==null){
				_Scoreform_section1_vocab38_response=getStringProperty("ScoreForm/section1/vocab38/response");
				return _Scoreform_section1_vocab38_response;
			}else {
				return _Scoreform_section1_vocab38_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab38/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab38_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab38/response",v);
		_Scoreform_section1_vocab38_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab38_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab38/score.
	 */
	public Integer getScoreform_section1_vocab38_score() {
		try{
			if (_Scoreform_section1_vocab38_score==null){
				_Scoreform_section1_vocab38_score=getIntegerProperty("ScoreForm/section1/vocab38/score");
				return _Scoreform_section1_vocab38_score;
			}else {
				return _Scoreform_section1_vocab38_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab38/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab38_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab38/score",v);
		_Scoreform_section1_vocab38_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab39_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab39/response.
	 */
	public String getScoreform_section1_vocab39_response(){
		try{
			if (_Scoreform_section1_vocab39_response==null){
				_Scoreform_section1_vocab39_response=getStringProperty("ScoreForm/section1/vocab39/response");
				return _Scoreform_section1_vocab39_response;
			}else {
				return _Scoreform_section1_vocab39_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab39/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab39_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab39/response",v);
		_Scoreform_section1_vocab39_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab39_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab39/score.
	 */
	public Integer getScoreform_section1_vocab39_score() {
		try{
			if (_Scoreform_section1_vocab39_score==null){
				_Scoreform_section1_vocab39_score=getIntegerProperty("ScoreForm/section1/vocab39/score");
				return _Scoreform_section1_vocab39_score;
			}else {
				return _Scoreform_section1_vocab39_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab39/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab39_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab39/score",v);
		_Scoreform_section1_vocab39_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab40_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab40/response.
	 */
	public String getScoreform_section1_vocab40_response(){
		try{
			if (_Scoreform_section1_vocab40_response==null){
				_Scoreform_section1_vocab40_response=getStringProperty("ScoreForm/section1/vocab40/response");
				return _Scoreform_section1_vocab40_response;
			}else {
				return _Scoreform_section1_vocab40_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab40/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab40_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab40/response",v);
		_Scoreform_section1_vocab40_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab40_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab40/score.
	 */
	public Integer getScoreform_section1_vocab40_score() {
		try{
			if (_Scoreform_section1_vocab40_score==null){
				_Scoreform_section1_vocab40_score=getIntegerProperty("ScoreForm/section1/vocab40/score");
				return _Scoreform_section1_vocab40_score;
			}else {
				return _Scoreform_section1_vocab40_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab40/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab40_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab40/score",v);
		_Scoreform_section1_vocab40_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab41_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab41/response.
	 */
	public String getScoreform_section1_vocab41_response(){
		try{
			if (_Scoreform_section1_vocab41_response==null){
				_Scoreform_section1_vocab41_response=getStringProperty("ScoreForm/section1/vocab41/response");
				return _Scoreform_section1_vocab41_response;
			}else {
				return _Scoreform_section1_vocab41_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab41/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab41_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab41/response",v);
		_Scoreform_section1_vocab41_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab41_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab41/score.
	 */
	public Integer getScoreform_section1_vocab41_score() {
		try{
			if (_Scoreform_section1_vocab41_score==null){
				_Scoreform_section1_vocab41_score=getIntegerProperty("ScoreForm/section1/vocab41/score");
				return _Scoreform_section1_vocab41_score;
			}else {
				return _Scoreform_section1_vocab41_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab41/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab41_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab41/score",v);
		_Scoreform_section1_vocab41_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section1_vocab42_response=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab42/response.
	 */
	public String getScoreform_section1_vocab42_response(){
		try{
			if (_Scoreform_section1_vocab42_response==null){
				_Scoreform_section1_vocab42_response=getStringProperty("ScoreForm/section1/vocab42/response");
				return _Scoreform_section1_vocab42_response;
			}else {
				return _Scoreform_section1_vocab42_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab42/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab42_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab42/response",v);
		_Scoreform_section1_vocab42_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section1_vocab42_score=null;

	/**
	 * @return Returns the ScoreForm/section1/vocab42/score.
	 */
	public Integer getScoreform_section1_vocab42_score() {
		try{
			if (_Scoreform_section1_vocab42_score==null){
				_Scoreform_section1_vocab42_score=getIntegerProperty("ScoreForm/section1/vocab42/score");
				return _Scoreform_section1_vocab42_score;
			}else {
				return _Scoreform_section1_vocab42_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section1/vocab42/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section1_vocab42_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section1/vocab42/score",v);
		_Scoreform_section1_vocab42_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block1_time=null;

	/**
	 * @return Returns the ScoreForm/section2/block1/time.
	 */
	public Integer getScoreform_section2_block1_time() {
		try{
			if (_Scoreform_section2_block1_time==null){
				_Scoreform_section2_block1_time=getIntegerProperty("ScoreForm/section2/block1/time");
				return _Scoreform_section2_block1_time;
			}else {
				return _Scoreform_section2_block1_time;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block1/time.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block1_time(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block1/time",v);
		_Scoreform_section2_block1_time=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block1_correct=null;

	/**
	 * @return Returns the ScoreForm/section2/block1/correct.
	 */
	public Integer getScoreform_section2_block1_correct() {
		try{
			if (_Scoreform_section2_block1_correct==null){
				_Scoreform_section2_block1_correct=getIntegerProperty("ScoreForm/section2/block1/correct");
				return _Scoreform_section2_block1_correct;
			}else {
				return _Scoreform_section2_block1_correct;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block1/correct.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block1_correct(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block1/correct",v);
		_Scoreform_section2_block1_correct=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block1_score=null;

	/**
	 * @return Returns the ScoreForm/section2/block1/score.
	 */
	public Integer getScoreform_section2_block1_score() {
		try{
			if (_Scoreform_section2_block1_score==null){
				_Scoreform_section2_block1_score=getIntegerProperty("ScoreForm/section2/block1/score");
				return _Scoreform_section2_block1_score;
			}else {
				return _Scoreform_section2_block1_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block1/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block1_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block1/score",v);
		_Scoreform_section2_block1_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block2_time=null;

	/**
	 * @return Returns the ScoreForm/section2/block2/time.
	 */
	public Integer getScoreform_section2_block2_time() {
		try{
			if (_Scoreform_section2_block2_time==null){
				_Scoreform_section2_block2_time=getIntegerProperty("ScoreForm/section2/block2/time");
				return _Scoreform_section2_block2_time;
			}else {
				return _Scoreform_section2_block2_time;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block2/time.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block2_time(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block2/time",v);
		_Scoreform_section2_block2_time=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block2_correct=null;

	/**
	 * @return Returns the ScoreForm/section2/block2/correct.
	 */
	public Integer getScoreform_section2_block2_correct() {
		try{
			if (_Scoreform_section2_block2_correct==null){
				_Scoreform_section2_block2_correct=getIntegerProperty("ScoreForm/section2/block2/correct");
				return _Scoreform_section2_block2_correct;
			}else {
				return _Scoreform_section2_block2_correct;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block2/correct.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block2_correct(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block2/correct",v);
		_Scoreform_section2_block2_correct=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block2_score=null;

	/**
	 * @return Returns the ScoreForm/section2/block2/score.
	 */
	public Integer getScoreform_section2_block2_score() {
		try{
			if (_Scoreform_section2_block2_score==null){
				_Scoreform_section2_block2_score=getIntegerProperty("ScoreForm/section2/block2/score");
				return _Scoreform_section2_block2_score;
			}else {
				return _Scoreform_section2_block2_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block2/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block2_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block2/score",v);
		_Scoreform_section2_block2_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block3_time=null;

	/**
	 * @return Returns the ScoreForm/section2/block3/time.
	 */
	public Integer getScoreform_section2_block3_time() {
		try{
			if (_Scoreform_section2_block3_time==null){
				_Scoreform_section2_block3_time=getIntegerProperty("ScoreForm/section2/block3/time");
				return _Scoreform_section2_block3_time;
			}else {
				return _Scoreform_section2_block3_time;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block3/time.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block3_time(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block3/time",v);
		_Scoreform_section2_block3_time=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block3_correct=null;

	/**
	 * @return Returns the ScoreForm/section2/block3/correct.
	 */
	public Integer getScoreform_section2_block3_correct() {
		try{
			if (_Scoreform_section2_block3_correct==null){
				_Scoreform_section2_block3_correct=getIntegerProperty("ScoreForm/section2/block3/correct");
				return _Scoreform_section2_block3_correct;
			}else {
				return _Scoreform_section2_block3_correct;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block3/correct.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block3_correct(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block3/correct",v);
		_Scoreform_section2_block3_correct=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block3_score=null;

	/**
	 * @return Returns the ScoreForm/section2/block3/score.
	 */
	public Integer getScoreform_section2_block3_score() {
		try{
			if (_Scoreform_section2_block3_score==null){
				_Scoreform_section2_block3_score=getIntegerProperty("ScoreForm/section2/block3/score");
				return _Scoreform_section2_block3_score;
			}else {
				return _Scoreform_section2_block3_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block3/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block3_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block3/score",v);
		_Scoreform_section2_block3_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block4_time=null;

	/**
	 * @return Returns the ScoreForm/section2/block4/time.
	 */
	public Integer getScoreform_section2_block4_time() {
		try{
			if (_Scoreform_section2_block4_time==null){
				_Scoreform_section2_block4_time=getIntegerProperty("ScoreForm/section2/block4/time");
				return _Scoreform_section2_block4_time;
			}else {
				return _Scoreform_section2_block4_time;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block4/time.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block4_time(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block4/time",v);
		_Scoreform_section2_block4_time=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block4_correct=null;

	/**
	 * @return Returns the ScoreForm/section2/block4/correct.
	 */
	public Integer getScoreform_section2_block4_correct() {
		try{
			if (_Scoreform_section2_block4_correct==null){
				_Scoreform_section2_block4_correct=getIntegerProperty("ScoreForm/section2/block4/correct");
				return _Scoreform_section2_block4_correct;
			}else {
				return _Scoreform_section2_block4_correct;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block4/correct.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block4_correct(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block4/correct",v);
		_Scoreform_section2_block4_correct=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block4_score=null;

	/**
	 * @return Returns the ScoreForm/section2/block4/score.
	 */
	public Integer getScoreform_section2_block4_score() {
		try{
			if (_Scoreform_section2_block4_score==null){
				_Scoreform_section2_block4_score=getIntegerProperty("ScoreForm/section2/block4/score");
				return _Scoreform_section2_block4_score;
			}else {
				return _Scoreform_section2_block4_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block4/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block4_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block4/score",v);
		_Scoreform_section2_block4_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block5_time=null;

	/**
	 * @return Returns the ScoreForm/section2/block5/time.
	 */
	public Integer getScoreform_section2_block5_time() {
		try{
			if (_Scoreform_section2_block5_time==null){
				_Scoreform_section2_block5_time=getIntegerProperty("ScoreForm/section2/block5/time");
				return _Scoreform_section2_block5_time;
			}else {
				return _Scoreform_section2_block5_time;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block5/time.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block5_time(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block5/time",v);
		_Scoreform_section2_block5_time=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block5_correct=null;

	/**
	 * @return Returns the ScoreForm/section2/block5/correct.
	 */
	public Integer getScoreform_section2_block5_correct() {
		try{
			if (_Scoreform_section2_block5_correct==null){
				_Scoreform_section2_block5_correct=getIntegerProperty("ScoreForm/section2/block5/correct");
				return _Scoreform_section2_block5_correct;
			}else {
				return _Scoreform_section2_block5_correct;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block5/correct.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block5_correct(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block5/correct",v);
		_Scoreform_section2_block5_correct=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block5_score=null;

	/**
	 * @return Returns the ScoreForm/section2/block5/score.
	 */
	public Integer getScoreform_section2_block5_score() {
		try{
			if (_Scoreform_section2_block5_score==null){
				_Scoreform_section2_block5_score=getIntegerProperty("ScoreForm/section2/block5/score");
				return _Scoreform_section2_block5_score;
			}else {
				return _Scoreform_section2_block5_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block5/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block5_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block5/score",v);
		_Scoreform_section2_block5_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block6_time=null;

	/**
	 * @return Returns the ScoreForm/section2/block6/time.
	 */
	public Integer getScoreform_section2_block6_time() {
		try{
			if (_Scoreform_section2_block6_time==null){
				_Scoreform_section2_block6_time=getIntegerProperty("ScoreForm/section2/block6/time");
				return _Scoreform_section2_block6_time;
			}else {
				return _Scoreform_section2_block6_time;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block6/time.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block6_time(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block6/time",v);
		_Scoreform_section2_block6_time=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block6_correct=null;

	/**
	 * @return Returns the ScoreForm/section2/block6/correct.
	 */
	public Integer getScoreform_section2_block6_correct() {
		try{
			if (_Scoreform_section2_block6_correct==null){
				_Scoreform_section2_block6_correct=getIntegerProperty("ScoreForm/section2/block6/correct");
				return _Scoreform_section2_block6_correct;
			}else {
				return _Scoreform_section2_block6_correct;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block6/correct.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block6_correct(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block6/correct",v);
		_Scoreform_section2_block6_correct=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block6_score=null;

	/**
	 * @return Returns the ScoreForm/section2/block6/score.
	 */
	public Integer getScoreform_section2_block6_score() {
		try{
			if (_Scoreform_section2_block6_score==null){
				_Scoreform_section2_block6_score=getIntegerProperty("ScoreForm/section2/block6/score");
				return _Scoreform_section2_block6_score;
			}else {
				return _Scoreform_section2_block6_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block6/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block6_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block6/score",v);
		_Scoreform_section2_block6_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block7_time=null;

	/**
	 * @return Returns the ScoreForm/section2/block7/time.
	 */
	public Integer getScoreform_section2_block7_time() {
		try{
			if (_Scoreform_section2_block7_time==null){
				_Scoreform_section2_block7_time=getIntegerProperty("ScoreForm/section2/block7/time");
				return _Scoreform_section2_block7_time;
			}else {
				return _Scoreform_section2_block7_time;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block7/time.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block7_time(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block7/time",v);
		_Scoreform_section2_block7_time=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block7_correct=null;

	/**
	 * @return Returns the ScoreForm/section2/block7/correct.
	 */
	public Integer getScoreform_section2_block7_correct() {
		try{
			if (_Scoreform_section2_block7_correct==null){
				_Scoreform_section2_block7_correct=getIntegerProperty("ScoreForm/section2/block7/correct");
				return _Scoreform_section2_block7_correct;
			}else {
				return _Scoreform_section2_block7_correct;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block7/correct.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block7_correct(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block7/correct",v);
		_Scoreform_section2_block7_correct=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block7_score=null;

	/**
	 * @return Returns the ScoreForm/section2/block7/score.
	 */
	public Integer getScoreform_section2_block7_score() {
		try{
			if (_Scoreform_section2_block7_score==null){
				_Scoreform_section2_block7_score=getIntegerProperty("ScoreForm/section2/block7/score");
				return _Scoreform_section2_block7_score;
			}else {
				return _Scoreform_section2_block7_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block7/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block7_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block7/score",v);
		_Scoreform_section2_block7_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block8_time=null;

	/**
	 * @return Returns the ScoreForm/section2/block8/time.
	 */
	public Integer getScoreform_section2_block8_time() {
		try{
			if (_Scoreform_section2_block8_time==null){
				_Scoreform_section2_block8_time=getIntegerProperty("ScoreForm/section2/block8/time");
				return _Scoreform_section2_block8_time;
			}else {
				return _Scoreform_section2_block8_time;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block8/time.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block8_time(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block8/time",v);
		_Scoreform_section2_block8_time=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block8_correct=null;

	/**
	 * @return Returns the ScoreForm/section2/block8/correct.
	 */
	public Integer getScoreform_section2_block8_correct() {
		try{
			if (_Scoreform_section2_block8_correct==null){
				_Scoreform_section2_block8_correct=getIntegerProperty("ScoreForm/section2/block8/correct");
				return _Scoreform_section2_block8_correct;
			}else {
				return _Scoreform_section2_block8_correct;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block8/correct.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block8_correct(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block8/correct",v);
		_Scoreform_section2_block8_correct=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block8_score=null;

	/**
	 * @return Returns the ScoreForm/section2/block8/score.
	 */
	public Integer getScoreform_section2_block8_score() {
		try{
			if (_Scoreform_section2_block8_score==null){
				_Scoreform_section2_block8_score=getIntegerProperty("ScoreForm/section2/block8/score");
				return _Scoreform_section2_block8_score;
			}else {
				return _Scoreform_section2_block8_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block8/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block8_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block8/score",v);
		_Scoreform_section2_block8_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block9_time=null;

	/**
	 * @return Returns the ScoreForm/section2/block9/time.
	 */
	public Integer getScoreform_section2_block9_time() {
		try{
			if (_Scoreform_section2_block9_time==null){
				_Scoreform_section2_block9_time=getIntegerProperty("ScoreForm/section2/block9/time");
				return _Scoreform_section2_block9_time;
			}else {
				return _Scoreform_section2_block9_time;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block9/time.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block9_time(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block9/time",v);
		_Scoreform_section2_block9_time=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block9_correct=null;

	/**
	 * @return Returns the ScoreForm/section2/block9/correct.
	 */
	public Integer getScoreform_section2_block9_correct() {
		try{
			if (_Scoreform_section2_block9_correct==null){
				_Scoreform_section2_block9_correct=getIntegerProperty("ScoreForm/section2/block9/correct");
				return _Scoreform_section2_block9_correct;
			}else {
				return _Scoreform_section2_block9_correct;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block9/correct.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block9_correct(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block9/correct",v);
		_Scoreform_section2_block9_correct=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block9_score=null;

	/**
	 * @return Returns the ScoreForm/section2/block9/score.
	 */
	public Integer getScoreform_section2_block9_score() {
		try{
			if (_Scoreform_section2_block9_score==null){
				_Scoreform_section2_block9_score=getIntegerProperty("ScoreForm/section2/block9/score");
				return _Scoreform_section2_block9_score;
			}else {
				return _Scoreform_section2_block9_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block9/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block9_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block9/score",v);
		_Scoreform_section2_block9_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block10_time=null;

	/**
	 * @return Returns the ScoreForm/section2/block10/time.
	 */
	public Integer getScoreform_section2_block10_time() {
		try{
			if (_Scoreform_section2_block10_time==null){
				_Scoreform_section2_block10_time=getIntegerProperty("ScoreForm/section2/block10/time");
				return _Scoreform_section2_block10_time;
			}else {
				return _Scoreform_section2_block10_time;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block10/time.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block10_time(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block10/time",v);
		_Scoreform_section2_block10_time=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block10_correct=null;

	/**
	 * @return Returns the ScoreForm/section2/block10/correct.
	 */
	public Integer getScoreform_section2_block10_correct() {
		try{
			if (_Scoreform_section2_block10_correct==null){
				_Scoreform_section2_block10_correct=getIntegerProperty("ScoreForm/section2/block10/correct");
				return _Scoreform_section2_block10_correct;
			}else {
				return _Scoreform_section2_block10_correct;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block10/correct.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block10_correct(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block10/correct",v);
		_Scoreform_section2_block10_correct=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block10_score=null;

	/**
	 * @return Returns the ScoreForm/section2/block10/score.
	 */
	public Integer getScoreform_section2_block10_score() {
		try{
			if (_Scoreform_section2_block10_score==null){
				_Scoreform_section2_block10_score=getIntegerProperty("ScoreForm/section2/block10/score");
				return _Scoreform_section2_block10_score;
			}else {
				return _Scoreform_section2_block10_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block10/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block10_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block10/score",v);
		_Scoreform_section2_block10_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block11_time=null;

	/**
	 * @return Returns the ScoreForm/section2/block11/time.
	 */
	public Integer getScoreform_section2_block11_time() {
		try{
			if (_Scoreform_section2_block11_time==null){
				_Scoreform_section2_block11_time=getIntegerProperty("ScoreForm/section2/block11/time");
				return _Scoreform_section2_block11_time;
			}else {
				return _Scoreform_section2_block11_time;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block11/time.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block11_time(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block11/time",v);
		_Scoreform_section2_block11_time=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block11_correct=null;

	/**
	 * @return Returns the ScoreForm/section2/block11/correct.
	 */
	public Integer getScoreform_section2_block11_correct() {
		try{
			if (_Scoreform_section2_block11_correct==null){
				_Scoreform_section2_block11_correct=getIntegerProperty("ScoreForm/section2/block11/correct");
				return _Scoreform_section2_block11_correct;
			}else {
				return _Scoreform_section2_block11_correct;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block11/correct.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block11_correct(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block11/correct",v);
		_Scoreform_section2_block11_correct=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block11_score=null;

	/**
	 * @return Returns the ScoreForm/section2/block11/score.
	 */
	public Integer getScoreform_section2_block11_score() {
		try{
			if (_Scoreform_section2_block11_score==null){
				_Scoreform_section2_block11_score=getIntegerProperty("ScoreForm/section2/block11/score");
				return _Scoreform_section2_block11_score;
			}else {
				return _Scoreform_section2_block11_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block11/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block11_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block11/score",v);
		_Scoreform_section2_block11_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block12_time=null;

	/**
	 * @return Returns the ScoreForm/section2/block12/time.
	 */
	public Integer getScoreform_section2_block12_time() {
		try{
			if (_Scoreform_section2_block12_time==null){
				_Scoreform_section2_block12_time=getIntegerProperty("ScoreForm/section2/block12/time");
				return _Scoreform_section2_block12_time;
			}else {
				return _Scoreform_section2_block12_time;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block12/time.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block12_time(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block12/time",v);
		_Scoreform_section2_block12_time=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block12_correct=null;

	/**
	 * @return Returns the ScoreForm/section2/block12/correct.
	 */
	public Integer getScoreform_section2_block12_correct() {
		try{
			if (_Scoreform_section2_block12_correct==null){
				_Scoreform_section2_block12_correct=getIntegerProperty("ScoreForm/section2/block12/correct");
				return _Scoreform_section2_block12_correct;
			}else {
				return _Scoreform_section2_block12_correct;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block12/correct.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block12_correct(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block12/correct",v);
		_Scoreform_section2_block12_correct=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block12_score=null;

	/**
	 * @return Returns the ScoreForm/section2/block12/score.
	 */
	public Integer getScoreform_section2_block12_score() {
		try{
			if (_Scoreform_section2_block12_score==null){
				_Scoreform_section2_block12_score=getIntegerProperty("ScoreForm/section2/block12/score");
				return _Scoreform_section2_block12_score;
			}else {
				return _Scoreform_section2_block12_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block12/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block12_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block12/score",v);
		_Scoreform_section2_block12_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block13_time=null;

	/**
	 * @return Returns the ScoreForm/section2/block13/time.
	 */
	public Integer getScoreform_section2_block13_time() {
		try{
			if (_Scoreform_section2_block13_time==null){
				_Scoreform_section2_block13_time=getIntegerProperty("ScoreForm/section2/block13/time");
				return _Scoreform_section2_block13_time;
			}else {
				return _Scoreform_section2_block13_time;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block13/time.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block13_time(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block13/time",v);
		_Scoreform_section2_block13_time=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block13_correct=null;

	/**
	 * @return Returns the ScoreForm/section2/block13/correct.
	 */
	public Integer getScoreform_section2_block13_correct() {
		try{
			if (_Scoreform_section2_block13_correct==null){
				_Scoreform_section2_block13_correct=getIntegerProperty("ScoreForm/section2/block13/correct");
				return _Scoreform_section2_block13_correct;
			}else {
				return _Scoreform_section2_block13_correct;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block13/correct.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block13_correct(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block13/correct",v);
		_Scoreform_section2_block13_correct=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section2_block13_score=null;

	/**
	 * @return Returns the ScoreForm/section2/block13/score.
	 */
	public Integer getScoreform_section2_block13_score() {
		try{
			if (_Scoreform_section2_block13_score==null){
				_Scoreform_section2_block13_score=getIntegerProperty("ScoreForm/section2/block13/score");
				return _Scoreform_section2_block13_score;
			}else {
				return _Scoreform_section2_block13_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section2/block13/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section2_block13_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section2/block13/score",v);
		_Scoreform_section2_block13_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar1_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar1/response.
	 */
	public String getScoreform_section3_similar1_response(){
		try{
			if (_Scoreform_section3_similar1_response==null){
				_Scoreform_section3_similar1_response=getStringProperty("ScoreForm/section3/similar1/response");
				return _Scoreform_section3_similar1_response;
			}else {
				return _Scoreform_section3_similar1_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar1/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar1_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar1/response",v);
		_Scoreform_section3_similar1_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar1_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar1/score.
	 */
	public Integer getScoreform_section3_similar1_score() {
		try{
			if (_Scoreform_section3_similar1_score==null){
				_Scoreform_section3_similar1_score=getIntegerProperty("ScoreForm/section3/similar1/score");
				return _Scoreform_section3_similar1_score;
			}else {
				return _Scoreform_section3_similar1_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar1/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar1_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar1/score",v);
		_Scoreform_section3_similar1_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar2_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar2/response.
	 */
	public String getScoreform_section3_similar2_response(){
		try{
			if (_Scoreform_section3_similar2_response==null){
				_Scoreform_section3_similar2_response=getStringProperty("ScoreForm/section3/similar2/response");
				return _Scoreform_section3_similar2_response;
			}else {
				return _Scoreform_section3_similar2_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar2/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar2_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar2/response",v);
		_Scoreform_section3_similar2_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar2_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar2/score.
	 */
	public Integer getScoreform_section3_similar2_score() {
		try{
			if (_Scoreform_section3_similar2_score==null){
				_Scoreform_section3_similar2_score=getIntegerProperty("ScoreForm/section3/similar2/score");
				return _Scoreform_section3_similar2_score;
			}else {
				return _Scoreform_section3_similar2_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar2/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar2_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar2/score",v);
		_Scoreform_section3_similar2_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar3_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar3/response.
	 */
	public String getScoreform_section3_similar3_response(){
		try{
			if (_Scoreform_section3_similar3_response==null){
				_Scoreform_section3_similar3_response=getStringProperty("ScoreForm/section3/similar3/response");
				return _Scoreform_section3_similar3_response;
			}else {
				return _Scoreform_section3_similar3_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar3/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar3_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar3/response",v);
		_Scoreform_section3_similar3_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar3_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar3/score.
	 */
	public Integer getScoreform_section3_similar3_score() {
		try{
			if (_Scoreform_section3_similar3_score==null){
				_Scoreform_section3_similar3_score=getIntegerProperty("ScoreForm/section3/similar3/score");
				return _Scoreform_section3_similar3_score;
			}else {
				return _Scoreform_section3_similar3_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar3/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar3_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar3/score",v);
		_Scoreform_section3_similar3_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar4_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar4/response.
	 */
	public String getScoreform_section3_similar4_response(){
		try{
			if (_Scoreform_section3_similar4_response==null){
				_Scoreform_section3_similar4_response=getStringProperty("ScoreForm/section3/similar4/response");
				return _Scoreform_section3_similar4_response;
			}else {
				return _Scoreform_section3_similar4_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar4/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar4_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar4/response",v);
		_Scoreform_section3_similar4_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar4_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar4/score.
	 */
	public Integer getScoreform_section3_similar4_score() {
		try{
			if (_Scoreform_section3_similar4_score==null){
				_Scoreform_section3_similar4_score=getIntegerProperty("ScoreForm/section3/similar4/score");
				return _Scoreform_section3_similar4_score;
			}else {
				return _Scoreform_section3_similar4_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar4/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar4_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar4/score",v);
		_Scoreform_section3_similar4_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar5_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar5/response.
	 */
	public String getScoreform_section3_similar5_response(){
		try{
			if (_Scoreform_section3_similar5_response==null){
				_Scoreform_section3_similar5_response=getStringProperty("ScoreForm/section3/similar5/response");
				return _Scoreform_section3_similar5_response;
			}else {
				return _Scoreform_section3_similar5_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar5/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar5_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar5/response",v);
		_Scoreform_section3_similar5_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar5_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar5/score.
	 */
	public Integer getScoreform_section3_similar5_score() {
		try{
			if (_Scoreform_section3_similar5_score==null){
				_Scoreform_section3_similar5_score=getIntegerProperty("ScoreForm/section3/similar5/score");
				return _Scoreform_section3_similar5_score;
			}else {
				return _Scoreform_section3_similar5_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar5/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar5_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar5/score",v);
		_Scoreform_section3_similar5_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar6_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar6/response.
	 */
	public String getScoreform_section3_similar6_response(){
		try{
			if (_Scoreform_section3_similar6_response==null){
				_Scoreform_section3_similar6_response=getStringProperty("ScoreForm/section3/similar6/response");
				return _Scoreform_section3_similar6_response;
			}else {
				return _Scoreform_section3_similar6_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar6/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar6_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar6/response",v);
		_Scoreform_section3_similar6_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar6_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar6/score.
	 */
	public Integer getScoreform_section3_similar6_score() {
		try{
			if (_Scoreform_section3_similar6_score==null){
				_Scoreform_section3_similar6_score=getIntegerProperty("ScoreForm/section3/similar6/score");
				return _Scoreform_section3_similar6_score;
			}else {
				return _Scoreform_section3_similar6_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar6/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar6_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar6/score",v);
		_Scoreform_section3_similar6_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar7_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar7/response.
	 */
	public String getScoreform_section3_similar7_response(){
		try{
			if (_Scoreform_section3_similar7_response==null){
				_Scoreform_section3_similar7_response=getStringProperty("ScoreForm/section3/similar7/response");
				return _Scoreform_section3_similar7_response;
			}else {
				return _Scoreform_section3_similar7_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar7/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar7_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar7/response",v);
		_Scoreform_section3_similar7_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar7_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar7/score.
	 */
	public Integer getScoreform_section3_similar7_score() {
		try{
			if (_Scoreform_section3_similar7_score==null){
				_Scoreform_section3_similar7_score=getIntegerProperty("ScoreForm/section3/similar7/score");
				return _Scoreform_section3_similar7_score;
			}else {
				return _Scoreform_section3_similar7_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar7/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar7_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar7/score",v);
		_Scoreform_section3_similar7_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar8_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar8/response.
	 */
	public String getScoreform_section3_similar8_response(){
		try{
			if (_Scoreform_section3_similar8_response==null){
				_Scoreform_section3_similar8_response=getStringProperty("ScoreForm/section3/similar8/response");
				return _Scoreform_section3_similar8_response;
			}else {
				return _Scoreform_section3_similar8_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar8/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar8_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar8/response",v);
		_Scoreform_section3_similar8_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar8_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar8/score.
	 */
	public Integer getScoreform_section3_similar8_score() {
		try{
			if (_Scoreform_section3_similar8_score==null){
				_Scoreform_section3_similar8_score=getIntegerProperty("ScoreForm/section3/similar8/score");
				return _Scoreform_section3_similar8_score;
			}else {
				return _Scoreform_section3_similar8_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar8/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar8_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar8/score",v);
		_Scoreform_section3_similar8_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar9_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar9/response.
	 */
	public String getScoreform_section3_similar9_response(){
		try{
			if (_Scoreform_section3_similar9_response==null){
				_Scoreform_section3_similar9_response=getStringProperty("ScoreForm/section3/similar9/response");
				return _Scoreform_section3_similar9_response;
			}else {
				return _Scoreform_section3_similar9_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar9/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar9_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar9/response",v);
		_Scoreform_section3_similar9_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar9_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar9/score.
	 */
	public Integer getScoreform_section3_similar9_score() {
		try{
			if (_Scoreform_section3_similar9_score==null){
				_Scoreform_section3_similar9_score=getIntegerProperty("ScoreForm/section3/similar9/score");
				return _Scoreform_section3_similar9_score;
			}else {
				return _Scoreform_section3_similar9_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar9/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar9_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar9/score",v);
		_Scoreform_section3_similar9_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar10_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar10/response.
	 */
	public String getScoreform_section3_similar10_response(){
		try{
			if (_Scoreform_section3_similar10_response==null){
				_Scoreform_section3_similar10_response=getStringProperty("ScoreForm/section3/similar10/response");
				return _Scoreform_section3_similar10_response;
			}else {
				return _Scoreform_section3_similar10_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar10/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar10_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar10/response",v);
		_Scoreform_section3_similar10_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar10_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar10/score.
	 */
	public Integer getScoreform_section3_similar10_score() {
		try{
			if (_Scoreform_section3_similar10_score==null){
				_Scoreform_section3_similar10_score=getIntegerProperty("ScoreForm/section3/similar10/score");
				return _Scoreform_section3_similar10_score;
			}else {
				return _Scoreform_section3_similar10_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar10/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar10_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar10/score",v);
		_Scoreform_section3_similar10_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar11_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar11/response.
	 */
	public String getScoreform_section3_similar11_response(){
		try{
			if (_Scoreform_section3_similar11_response==null){
				_Scoreform_section3_similar11_response=getStringProperty("ScoreForm/section3/similar11/response");
				return _Scoreform_section3_similar11_response;
			}else {
				return _Scoreform_section3_similar11_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar11/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar11_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar11/response",v);
		_Scoreform_section3_similar11_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar11_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar11/score.
	 */
	public Integer getScoreform_section3_similar11_score() {
		try{
			if (_Scoreform_section3_similar11_score==null){
				_Scoreform_section3_similar11_score=getIntegerProperty("ScoreForm/section3/similar11/score");
				return _Scoreform_section3_similar11_score;
			}else {
				return _Scoreform_section3_similar11_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar11/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar11_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar11/score",v);
		_Scoreform_section3_similar11_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar12_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar12/response.
	 */
	public String getScoreform_section3_similar12_response(){
		try{
			if (_Scoreform_section3_similar12_response==null){
				_Scoreform_section3_similar12_response=getStringProperty("ScoreForm/section3/similar12/response");
				return _Scoreform_section3_similar12_response;
			}else {
				return _Scoreform_section3_similar12_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar12/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar12_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar12/response",v);
		_Scoreform_section3_similar12_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar12_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar12/score.
	 */
	public Integer getScoreform_section3_similar12_score() {
		try{
			if (_Scoreform_section3_similar12_score==null){
				_Scoreform_section3_similar12_score=getIntegerProperty("ScoreForm/section3/similar12/score");
				return _Scoreform_section3_similar12_score;
			}else {
				return _Scoreform_section3_similar12_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar12/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar12_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar12/score",v);
		_Scoreform_section3_similar12_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar13_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar13/response.
	 */
	public String getScoreform_section3_similar13_response(){
		try{
			if (_Scoreform_section3_similar13_response==null){
				_Scoreform_section3_similar13_response=getStringProperty("ScoreForm/section3/similar13/response");
				return _Scoreform_section3_similar13_response;
			}else {
				return _Scoreform_section3_similar13_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar13/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar13_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar13/response",v);
		_Scoreform_section3_similar13_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar13_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar13/score.
	 */
	public Integer getScoreform_section3_similar13_score() {
		try{
			if (_Scoreform_section3_similar13_score==null){
				_Scoreform_section3_similar13_score=getIntegerProperty("ScoreForm/section3/similar13/score");
				return _Scoreform_section3_similar13_score;
			}else {
				return _Scoreform_section3_similar13_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar13/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar13_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar13/score",v);
		_Scoreform_section3_similar13_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar14_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar14/response.
	 */
	public String getScoreform_section3_similar14_response(){
		try{
			if (_Scoreform_section3_similar14_response==null){
				_Scoreform_section3_similar14_response=getStringProperty("ScoreForm/section3/similar14/response");
				return _Scoreform_section3_similar14_response;
			}else {
				return _Scoreform_section3_similar14_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar14/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar14_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar14/response",v);
		_Scoreform_section3_similar14_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar14_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar14/score.
	 */
	public Integer getScoreform_section3_similar14_score() {
		try{
			if (_Scoreform_section3_similar14_score==null){
				_Scoreform_section3_similar14_score=getIntegerProperty("ScoreForm/section3/similar14/score");
				return _Scoreform_section3_similar14_score;
			}else {
				return _Scoreform_section3_similar14_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar14/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar14_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar14/score",v);
		_Scoreform_section3_similar14_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar15_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar15/response.
	 */
	public String getScoreform_section3_similar15_response(){
		try{
			if (_Scoreform_section3_similar15_response==null){
				_Scoreform_section3_similar15_response=getStringProperty("ScoreForm/section3/similar15/response");
				return _Scoreform_section3_similar15_response;
			}else {
				return _Scoreform_section3_similar15_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar15/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar15_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar15/response",v);
		_Scoreform_section3_similar15_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar15_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar15/score.
	 */
	public Integer getScoreform_section3_similar15_score() {
		try{
			if (_Scoreform_section3_similar15_score==null){
				_Scoreform_section3_similar15_score=getIntegerProperty("ScoreForm/section3/similar15/score");
				return _Scoreform_section3_similar15_score;
			}else {
				return _Scoreform_section3_similar15_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar15/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar15_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar15/score",v);
		_Scoreform_section3_similar15_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar16_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar16/response.
	 */
	public String getScoreform_section3_similar16_response(){
		try{
			if (_Scoreform_section3_similar16_response==null){
				_Scoreform_section3_similar16_response=getStringProperty("ScoreForm/section3/similar16/response");
				return _Scoreform_section3_similar16_response;
			}else {
				return _Scoreform_section3_similar16_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar16/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar16_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar16/response",v);
		_Scoreform_section3_similar16_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar16_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar16/score.
	 */
	public Integer getScoreform_section3_similar16_score() {
		try{
			if (_Scoreform_section3_similar16_score==null){
				_Scoreform_section3_similar16_score=getIntegerProperty("ScoreForm/section3/similar16/score");
				return _Scoreform_section3_similar16_score;
			}else {
				return _Scoreform_section3_similar16_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar16/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar16_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar16/score",v);
		_Scoreform_section3_similar16_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar17_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar17/response.
	 */
	public String getScoreform_section3_similar17_response(){
		try{
			if (_Scoreform_section3_similar17_response==null){
				_Scoreform_section3_similar17_response=getStringProperty("ScoreForm/section3/similar17/response");
				return _Scoreform_section3_similar17_response;
			}else {
				return _Scoreform_section3_similar17_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar17/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar17_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar17/response",v);
		_Scoreform_section3_similar17_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar17_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar17/score.
	 */
	public Integer getScoreform_section3_similar17_score() {
		try{
			if (_Scoreform_section3_similar17_score==null){
				_Scoreform_section3_similar17_score=getIntegerProperty("ScoreForm/section3/similar17/score");
				return _Scoreform_section3_similar17_score;
			}else {
				return _Scoreform_section3_similar17_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar17/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar17_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar17/score",v);
		_Scoreform_section3_similar17_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar18_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar18/response.
	 */
	public String getScoreform_section3_similar18_response(){
		try{
			if (_Scoreform_section3_similar18_response==null){
				_Scoreform_section3_similar18_response=getStringProperty("ScoreForm/section3/similar18/response");
				return _Scoreform_section3_similar18_response;
			}else {
				return _Scoreform_section3_similar18_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar18/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar18_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar18/response",v);
		_Scoreform_section3_similar18_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar18_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar18/score.
	 */
	public Integer getScoreform_section3_similar18_score() {
		try{
			if (_Scoreform_section3_similar18_score==null){
				_Scoreform_section3_similar18_score=getIntegerProperty("ScoreForm/section3/similar18/score");
				return _Scoreform_section3_similar18_score;
			}else {
				return _Scoreform_section3_similar18_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar18/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar18_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar18/score",v);
		_Scoreform_section3_similar18_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar19_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar19/response.
	 */
	public String getScoreform_section3_similar19_response(){
		try{
			if (_Scoreform_section3_similar19_response==null){
				_Scoreform_section3_similar19_response=getStringProperty("ScoreForm/section3/similar19/response");
				return _Scoreform_section3_similar19_response;
			}else {
				return _Scoreform_section3_similar19_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar19/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar19_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar19/response",v);
		_Scoreform_section3_similar19_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar19_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar19/score.
	 */
	public Integer getScoreform_section3_similar19_score() {
		try{
			if (_Scoreform_section3_similar19_score==null){
				_Scoreform_section3_similar19_score=getIntegerProperty("ScoreForm/section3/similar19/score");
				return _Scoreform_section3_similar19_score;
			}else {
				return _Scoreform_section3_similar19_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar19/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar19_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar19/score",v);
		_Scoreform_section3_similar19_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar20_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar20/response.
	 */
	public String getScoreform_section3_similar20_response(){
		try{
			if (_Scoreform_section3_similar20_response==null){
				_Scoreform_section3_similar20_response=getStringProperty("ScoreForm/section3/similar20/response");
				return _Scoreform_section3_similar20_response;
			}else {
				return _Scoreform_section3_similar20_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar20/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar20_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar20/response",v);
		_Scoreform_section3_similar20_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar20_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar20/score.
	 */
	public Integer getScoreform_section3_similar20_score() {
		try{
			if (_Scoreform_section3_similar20_score==null){
				_Scoreform_section3_similar20_score=getIntegerProperty("ScoreForm/section3/similar20/score");
				return _Scoreform_section3_similar20_score;
			}else {
				return _Scoreform_section3_similar20_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar20/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar20_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar20/score",v);
		_Scoreform_section3_similar20_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar21_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar21/response.
	 */
	public String getScoreform_section3_similar21_response(){
		try{
			if (_Scoreform_section3_similar21_response==null){
				_Scoreform_section3_similar21_response=getStringProperty("ScoreForm/section3/similar21/response");
				return _Scoreform_section3_similar21_response;
			}else {
				return _Scoreform_section3_similar21_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar21/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar21_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar21/response",v);
		_Scoreform_section3_similar21_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar21_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar21/score.
	 */
	public Integer getScoreform_section3_similar21_score() {
		try{
			if (_Scoreform_section3_similar21_score==null){
				_Scoreform_section3_similar21_score=getIntegerProperty("ScoreForm/section3/similar21/score");
				return _Scoreform_section3_similar21_score;
			}else {
				return _Scoreform_section3_similar21_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar21/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar21_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar21/score",v);
		_Scoreform_section3_similar21_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar22_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar22/response.
	 */
	public String getScoreform_section3_similar22_response(){
		try{
			if (_Scoreform_section3_similar22_response==null){
				_Scoreform_section3_similar22_response=getStringProperty("ScoreForm/section3/similar22/response");
				return _Scoreform_section3_similar22_response;
			}else {
				return _Scoreform_section3_similar22_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar22/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar22_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar22/response",v);
		_Scoreform_section3_similar22_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar22_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar22/score.
	 */
	public Integer getScoreform_section3_similar22_score() {
		try{
			if (_Scoreform_section3_similar22_score==null){
				_Scoreform_section3_similar22_score=getIntegerProperty("ScoreForm/section3/similar22/score");
				return _Scoreform_section3_similar22_score;
			}else {
				return _Scoreform_section3_similar22_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar22/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar22_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar22/score",v);
		_Scoreform_section3_similar22_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar23_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar23/response.
	 */
	public String getScoreform_section3_similar23_response(){
		try{
			if (_Scoreform_section3_similar23_response==null){
				_Scoreform_section3_similar23_response=getStringProperty("ScoreForm/section3/similar23/response");
				return _Scoreform_section3_similar23_response;
			}else {
				return _Scoreform_section3_similar23_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar23/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar23_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar23/response",v);
		_Scoreform_section3_similar23_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar23_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar23/score.
	 */
	public Integer getScoreform_section3_similar23_score() {
		try{
			if (_Scoreform_section3_similar23_score==null){
				_Scoreform_section3_similar23_score=getIntegerProperty("ScoreForm/section3/similar23/score");
				return _Scoreform_section3_similar23_score;
			}else {
				return _Scoreform_section3_similar23_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar23/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar23_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar23/score",v);
		_Scoreform_section3_similar23_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar24_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar24/response.
	 */
	public String getScoreform_section3_similar24_response(){
		try{
			if (_Scoreform_section3_similar24_response==null){
				_Scoreform_section3_similar24_response=getStringProperty("ScoreForm/section3/similar24/response");
				return _Scoreform_section3_similar24_response;
			}else {
				return _Scoreform_section3_similar24_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar24/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar24_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar24/response",v);
		_Scoreform_section3_similar24_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar24_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar24/score.
	 */
	public Integer getScoreform_section3_similar24_score() {
		try{
			if (_Scoreform_section3_similar24_score==null){
				_Scoreform_section3_similar24_score=getIntegerProperty("ScoreForm/section3/similar24/score");
				return _Scoreform_section3_similar24_score;
			}else {
				return _Scoreform_section3_similar24_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar24/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar24_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar24/score",v);
		_Scoreform_section3_similar24_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar25_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar25/response.
	 */
	public String getScoreform_section3_similar25_response(){
		try{
			if (_Scoreform_section3_similar25_response==null){
				_Scoreform_section3_similar25_response=getStringProperty("ScoreForm/section3/similar25/response");
				return _Scoreform_section3_similar25_response;
			}else {
				return _Scoreform_section3_similar25_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar25/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar25_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar25/response",v);
		_Scoreform_section3_similar25_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar25_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar25/score.
	 */
	public Integer getScoreform_section3_similar25_score() {
		try{
			if (_Scoreform_section3_similar25_score==null){
				_Scoreform_section3_similar25_score=getIntegerProperty("ScoreForm/section3/similar25/score");
				return _Scoreform_section3_similar25_score;
			}else {
				return _Scoreform_section3_similar25_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar25/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar25_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar25/score",v);
		_Scoreform_section3_similar25_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_section3_similar26_response=null;

	/**
	 * @return Returns the ScoreForm/section3/similar26/response.
	 */
	public String getScoreform_section3_similar26_response(){
		try{
			if (_Scoreform_section3_similar26_response==null){
				_Scoreform_section3_similar26_response=getStringProperty("ScoreForm/section3/similar26/response");
				return _Scoreform_section3_similar26_response;
			}else {
				return _Scoreform_section3_similar26_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar26/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar26_response(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar26/response",v);
		_Scoreform_section3_similar26_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section3_similar26_score=null;

	/**
	 * @return Returns the ScoreForm/section3/similar26/score.
	 */
	public Integer getScoreform_section3_similar26_score() {
		try{
			if (_Scoreform_section3_similar26_score==null){
				_Scoreform_section3_similar26_score=getIntegerProperty("ScoreForm/section3/similar26/score");
				return _Scoreform_section3_similar26_score;
			}else {
				return _Scoreform_section3_similar26_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section3/similar26/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section3_similar26_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section3/similar26/score",v);
		_Scoreform_section3_similar26_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrixa_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrixA/response.
	 */
	public Integer getScoreform_section4_matrixa_response() {
		try{
			if (_Scoreform_section4_matrixa_response==null){
				_Scoreform_section4_matrixa_response=getIntegerProperty("ScoreForm/section4/matrixA/response");
				return _Scoreform_section4_matrixa_response;
			}else {
				return _Scoreform_section4_matrixa_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrixA/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrixa_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrixA/response",v);
		_Scoreform_section4_matrixa_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrixa_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrixA/score.
	 */
	public Integer getScoreform_section4_matrixa_score() {
		try{
			if (_Scoreform_section4_matrixa_score==null){
				_Scoreform_section4_matrixa_score=getIntegerProperty("ScoreForm/section4/matrixA/score");
				return _Scoreform_section4_matrixa_score;
			}else {
				return _Scoreform_section4_matrixa_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrixA/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrixa_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrixA/score",v);
		_Scoreform_section4_matrixa_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrixb_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrixB/response.
	 */
	public Integer getScoreform_section4_matrixb_response() {
		try{
			if (_Scoreform_section4_matrixb_response==null){
				_Scoreform_section4_matrixb_response=getIntegerProperty("ScoreForm/section4/matrixB/response");
				return _Scoreform_section4_matrixb_response;
			}else {
				return _Scoreform_section4_matrixb_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrixB/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrixb_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrixB/response",v);
		_Scoreform_section4_matrixb_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrixb_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrixB/score.
	 */
	public Integer getScoreform_section4_matrixb_score() {
		try{
			if (_Scoreform_section4_matrixb_score==null){
				_Scoreform_section4_matrixb_score=getIntegerProperty("ScoreForm/section4/matrixB/score");
				return _Scoreform_section4_matrixb_score;
			}else {
				return _Scoreform_section4_matrixb_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrixB/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrixb_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrixB/score",v);
		_Scoreform_section4_matrixb_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix1_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix1/response.
	 */
	public Integer getScoreform_section4_matrix1_response() {
		try{
			if (_Scoreform_section4_matrix1_response==null){
				_Scoreform_section4_matrix1_response=getIntegerProperty("ScoreForm/section4/matrix1/response");
				return _Scoreform_section4_matrix1_response;
			}else {
				return _Scoreform_section4_matrix1_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix1/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix1_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix1/response",v);
		_Scoreform_section4_matrix1_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix1_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix1/score.
	 */
	public Integer getScoreform_section4_matrix1_score() {
		try{
			if (_Scoreform_section4_matrix1_score==null){
				_Scoreform_section4_matrix1_score=getIntegerProperty("ScoreForm/section4/matrix1/score");
				return _Scoreform_section4_matrix1_score;
			}else {
				return _Scoreform_section4_matrix1_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix1/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix1_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix1/score",v);
		_Scoreform_section4_matrix1_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix2_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix2/response.
	 */
	public Integer getScoreform_section4_matrix2_response() {
		try{
			if (_Scoreform_section4_matrix2_response==null){
				_Scoreform_section4_matrix2_response=getIntegerProperty("ScoreForm/section4/matrix2/response");
				return _Scoreform_section4_matrix2_response;
			}else {
				return _Scoreform_section4_matrix2_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix2/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix2_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix2/response",v);
		_Scoreform_section4_matrix2_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix2_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix2/score.
	 */
	public Integer getScoreform_section4_matrix2_score() {
		try{
			if (_Scoreform_section4_matrix2_score==null){
				_Scoreform_section4_matrix2_score=getIntegerProperty("ScoreForm/section4/matrix2/score");
				return _Scoreform_section4_matrix2_score;
			}else {
				return _Scoreform_section4_matrix2_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix2/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix2_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix2/score",v);
		_Scoreform_section4_matrix2_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix3_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix3/response.
	 */
	public Integer getScoreform_section4_matrix3_response() {
		try{
			if (_Scoreform_section4_matrix3_response==null){
				_Scoreform_section4_matrix3_response=getIntegerProperty("ScoreForm/section4/matrix3/response");
				return _Scoreform_section4_matrix3_response;
			}else {
				return _Scoreform_section4_matrix3_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix3/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix3_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix3/response",v);
		_Scoreform_section4_matrix3_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix3_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix3/score.
	 */
	public Integer getScoreform_section4_matrix3_score() {
		try{
			if (_Scoreform_section4_matrix3_score==null){
				_Scoreform_section4_matrix3_score=getIntegerProperty("ScoreForm/section4/matrix3/score");
				return _Scoreform_section4_matrix3_score;
			}else {
				return _Scoreform_section4_matrix3_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix3/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix3_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix3/score",v);
		_Scoreform_section4_matrix3_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix4_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix4/response.
	 */
	public Integer getScoreform_section4_matrix4_response() {
		try{
			if (_Scoreform_section4_matrix4_response==null){
				_Scoreform_section4_matrix4_response=getIntegerProperty("ScoreForm/section4/matrix4/response");
				return _Scoreform_section4_matrix4_response;
			}else {
				return _Scoreform_section4_matrix4_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix4/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix4_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix4/response",v);
		_Scoreform_section4_matrix4_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix4_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix4/score.
	 */
	public Integer getScoreform_section4_matrix4_score() {
		try{
			if (_Scoreform_section4_matrix4_score==null){
				_Scoreform_section4_matrix4_score=getIntegerProperty("ScoreForm/section4/matrix4/score");
				return _Scoreform_section4_matrix4_score;
			}else {
				return _Scoreform_section4_matrix4_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix4/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix4_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix4/score",v);
		_Scoreform_section4_matrix4_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix5_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix5/response.
	 */
	public Integer getScoreform_section4_matrix5_response() {
		try{
			if (_Scoreform_section4_matrix5_response==null){
				_Scoreform_section4_matrix5_response=getIntegerProperty("ScoreForm/section4/matrix5/response");
				return _Scoreform_section4_matrix5_response;
			}else {
				return _Scoreform_section4_matrix5_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix5/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix5_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix5/response",v);
		_Scoreform_section4_matrix5_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix5_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix5/score.
	 */
	public Integer getScoreform_section4_matrix5_score() {
		try{
			if (_Scoreform_section4_matrix5_score==null){
				_Scoreform_section4_matrix5_score=getIntegerProperty("ScoreForm/section4/matrix5/score");
				return _Scoreform_section4_matrix5_score;
			}else {
				return _Scoreform_section4_matrix5_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix5/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix5_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix5/score",v);
		_Scoreform_section4_matrix5_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix6_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix6/response.
	 */
	public Integer getScoreform_section4_matrix6_response() {
		try{
			if (_Scoreform_section4_matrix6_response==null){
				_Scoreform_section4_matrix6_response=getIntegerProperty("ScoreForm/section4/matrix6/response");
				return _Scoreform_section4_matrix6_response;
			}else {
				return _Scoreform_section4_matrix6_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix6/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix6_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix6/response",v);
		_Scoreform_section4_matrix6_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix6_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix6/score.
	 */
	public Integer getScoreform_section4_matrix6_score() {
		try{
			if (_Scoreform_section4_matrix6_score==null){
				_Scoreform_section4_matrix6_score=getIntegerProperty("ScoreForm/section4/matrix6/score");
				return _Scoreform_section4_matrix6_score;
			}else {
				return _Scoreform_section4_matrix6_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix6/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix6_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix6/score",v);
		_Scoreform_section4_matrix6_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix7_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix7/response.
	 */
	public Integer getScoreform_section4_matrix7_response() {
		try{
			if (_Scoreform_section4_matrix7_response==null){
				_Scoreform_section4_matrix7_response=getIntegerProperty("ScoreForm/section4/matrix7/response");
				return _Scoreform_section4_matrix7_response;
			}else {
				return _Scoreform_section4_matrix7_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix7/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix7_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix7/response",v);
		_Scoreform_section4_matrix7_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix7_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix7/score.
	 */
	public Integer getScoreform_section4_matrix7_score() {
		try{
			if (_Scoreform_section4_matrix7_score==null){
				_Scoreform_section4_matrix7_score=getIntegerProperty("ScoreForm/section4/matrix7/score");
				return _Scoreform_section4_matrix7_score;
			}else {
				return _Scoreform_section4_matrix7_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix7/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix7_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix7/score",v);
		_Scoreform_section4_matrix7_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix8_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix8/response.
	 */
	public Integer getScoreform_section4_matrix8_response() {
		try{
			if (_Scoreform_section4_matrix8_response==null){
				_Scoreform_section4_matrix8_response=getIntegerProperty("ScoreForm/section4/matrix8/response");
				return _Scoreform_section4_matrix8_response;
			}else {
				return _Scoreform_section4_matrix8_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix8/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix8_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix8/response",v);
		_Scoreform_section4_matrix8_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix8_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix8/score.
	 */
	public Integer getScoreform_section4_matrix8_score() {
		try{
			if (_Scoreform_section4_matrix8_score==null){
				_Scoreform_section4_matrix8_score=getIntegerProperty("ScoreForm/section4/matrix8/score");
				return _Scoreform_section4_matrix8_score;
			}else {
				return _Scoreform_section4_matrix8_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix8/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix8_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix8/score",v);
		_Scoreform_section4_matrix8_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix9_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix9/response.
	 */
	public Integer getScoreform_section4_matrix9_response() {
		try{
			if (_Scoreform_section4_matrix9_response==null){
				_Scoreform_section4_matrix9_response=getIntegerProperty("ScoreForm/section4/matrix9/response");
				return _Scoreform_section4_matrix9_response;
			}else {
				return _Scoreform_section4_matrix9_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix9/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix9_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix9/response",v);
		_Scoreform_section4_matrix9_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix9_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix9/score.
	 */
	public Integer getScoreform_section4_matrix9_score() {
		try{
			if (_Scoreform_section4_matrix9_score==null){
				_Scoreform_section4_matrix9_score=getIntegerProperty("ScoreForm/section4/matrix9/score");
				return _Scoreform_section4_matrix9_score;
			}else {
				return _Scoreform_section4_matrix9_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix9/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix9_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix9/score",v);
		_Scoreform_section4_matrix9_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix10_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix10/response.
	 */
	public Integer getScoreform_section4_matrix10_response() {
		try{
			if (_Scoreform_section4_matrix10_response==null){
				_Scoreform_section4_matrix10_response=getIntegerProperty("ScoreForm/section4/matrix10/response");
				return _Scoreform_section4_matrix10_response;
			}else {
				return _Scoreform_section4_matrix10_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix10/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix10_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix10/response",v);
		_Scoreform_section4_matrix10_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix10_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix10/score.
	 */
	public Integer getScoreform_section4_matrix10_score() {
		try{
			if (_Scoreform_section4_matrix10_score==null){
				_Scoreform_section4_matrix10_score=getIntegerProperty("ScoreForm/section4/matrix10/score");
				return _Scoreform_section4_matrix10_score;
			}else {
				return _Scoreform_section4_matrix10_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix10/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix10_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix10/score",v);
		_Scoreform_section4_matrix10_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix11_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix11/response.
	 */
	public Integer getScoreform_section4_matrix11_response() {
		try{
			if (_Scoreform_section4_matrix11_response==null){
				_Scoreform_section4_matrix11_response=getIntegerProperty("ScoreForm/section4/matrix11/response");
				return _Scoreform_section4_matrix11_response;
			}else {
				return _Scoreform_section4_matrix11_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix11/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix11_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix11/response",v);
		_Scoreform_section4_matrix11_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix11_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix11/score.
	 */
	public Integer getScoreform_section4_matrix11_score() {
		try{
			if (_Scoreform_section4_matrix11_score==null){
				_Scoreform_section4_matrix11_score=getIntegerProperty("ScoreForm/section4/matrix11/score");
				return _Scoreform_section4_matrix11_score;
			}else {
				return _Scoreform_section4_matrix11_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix11/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix11_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix11/score",v);
		_Scoreform_section4_matrix11_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix12_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix12/response.
	 */
	public Integer getScoreform_section4_matrix12_response() {
		try{
			if (_Scoreform_section4_matrix12_response==null){
				_Scoreform_section4_matrix12_response=getIntegerProperty("ScoreForm/section4/matrix12/response");
				return _Scoreform_section4_matrix12_response;
			}else {
				return _Scoreform_section4_matrix12_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix12/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix12_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix12/response",v);
		_Scoreform_section4_matrix12_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix12_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix12/score.
	 */
	public Integer getScoreform_section4_matrix12_score() {
		try{
			if (_Scoreform_section4_matrix12_score==null){
				_Scoreform_section4_matrix12_score=getIntegerProperty("ScoreForm/section4/matrix12/score");
				return _Scoreform_section4_matrix12_score;
			}else {
				return _Scoreform_section4_matrix12_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix12/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix12_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix12/score",v);
		_Scoreform_section4_matrix12_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix13_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix13/response.
	 */
	public Integer getScoreform_section4_matrix13_response() {
		try{
			if (_Scoreform_section4_matrix13_response==null){
				_Scoreform_section4_matrix13_response=getIntegerProperty("ScoreForm/section4/matrix13/response");
				return _Scoreform_section4_matrix13_response;
			}else {
				return _Scoreform_section4_matrix13_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix13/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix13_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix13/response",v);
		_Scoreform_section4_matrix13_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix13_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix13/score.
	 */
	public Integer getScoreform_section4_matrix13_score() {
		try{
			if (_Scoreform_section4_matrix13_score==null){
				_Scoreform_section4_matrix13_score=getIntegerProperty("ScoreForm/section4/matrix13/score");
				return _Scoreform_section4_matrix13_score;
			}else {
				return _Scoreform_section4_matrix13_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix13/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix13_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix13/score",v);
		_Scoreform_section4_matrix13_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix14_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix14/response.
	 */
	public Integer getScoreform_section4_matrix14_response() {
		try{
			if (_Scoreform_section4_matrix14_response==null){
				_Scoreform_section4_matrix14_response=getIntegerProperty("ScoreForm/section4/matrix14/response");
				return _Scoreform_section4_matrix14_response;
			}else {
				return _Scoreform_section4_matrix14_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix14/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix14_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix14/response",v);
		_Scoreform_section4_matrix14_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix14_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix14/score.
	 */
	public Integer getScoreform_section4_matrix14_score() {
		try{
			if (_Scoreform_section4_matrix14_score==null){
				_Scoreform_section4_matrix14_score=getIntegerProperty("ScoreForm/section4/matrix14/score");
				return _Scoreform_section4_matrix14_score;
			}else {
				return _Scoreform_section4_matrix14_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix14/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix14_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix14/score",v);
		_Scoreform_section4_matrix14_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix15_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix15/response.
	 */
	public Integer getScoreform_section4_matrix15_response() {
		try{
			if (_Scoreform_section4_matrix15_response==null){
				_Scoreform_section4_matrix15_response=getIntegerProperty("ScoreForm/section4/matrix15/response");
				return _Scoreform_section4_matrix15_response;
			}else {
				return _Scoreform_section4_matrix15_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix15/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix15_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix15/response",v);
		_Scoreform_section4_matrix15_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix15_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix15/score.
	 */
	public Integer getScoreform_section4_matrix15_score() {
		try{
			if (_Scoreform_section4_matrix15_score==null){
				_Scoreform_section4_matrix15_score=getIntegerProperty("ScoreForm/section4/matrix15/score");
				return _Scoreform_section4_matrix15_score;
			}else {
				return _Scoreform_section4_matrix15_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix15/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix15_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix15/score",v);
		_Scoreform_section4_matrix15_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix16_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix16/response.
	 */
	public Integer getScoreform_section4_matrix16_response() {
		try{
			if (_Scoreform_section4_matrix16_response==null){
				_Scoreform_section4_matrix16_response=getIntegerProperty("ScoreForm/section4/matrix16/response");
				return _Scoreform_section4_matrix16_response;
			}else {
				return _Scoreform_section4_matrix16_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix16/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix16_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix16/response",v);
		_Scoreform_section4_matrix16_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix16_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix16/score.
	 */
	public Integer getScoreform_section4_matrix16_score() {
		try{
			if (_Scoreform_section4_matrix16_score==null){
				_Scoreform_section4_matrix16_score=getIntegerProperty("ScoreForm/section4/matrix16/score");
				return _Scoreform_section4_matrix16_score;
			}else {
				return _Scoreform_section4_matrix16_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix16/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix16_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix16/score",v);
		_Scoreform_section4_matrix16_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix17_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix17/response.
	 */
	public Integer getScoreform_section4_matrix17_response() {
		try{
			if (_Scoreform_section4_matrix17_response==null){
				_Scoreform_section4_matrix17_response=getIntegerProperty("ScoreForm/section4/matrix17/response");
				return _Scoreform_section4_matrix17_response;
			}else {
				return _Scoreform_section4_matrix17_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix17/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix17_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix17/response",v);
		_Scoreform_section4_matrix17_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix17_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix17/score.
	 */
	public Integer getScoreform_section4_matrix17_score() {
		try{
			if (_Scoreform_section4_matrix17_score==null){
				_Scoreform_section4_matrix17_score=getIntegerProperty("ScoreForm/section4/matrix17/score");
				return _Scoreform_section4_matrix17_score;
			}else {
				return _Scoreform_section4_matrix17_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix17/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix17_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix17/score",v);
		_Scoreform_section4_matrix17_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix18_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix18/response.
	 */
	public Integer getScoreform_section4_matrix18_response() {
		try{
			if (_Scoreform_section4_matrix18_response==null){
				_Scoreform_section4_matrix18_response=getIntegerProperty("ScoreForm/section4/matrix18/response");
				return _Scoreform_section4_matrix18_response;
			}else {
				return _Scoreform_section4_matrix18_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix18/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix18_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix18/response",v);
		_Scoreform_section4_matrix18_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix18_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix18/score.
	 */
	public Integer getScoreform_section4_matrix18_score() {
		try{
			if (_Scoreform_section4_matrix18_score==null){
				_Scoreform_section4_matrix18_score=getIntegerProperty("ScoreForm/section4/matrix18/score");
				return _Scoreform_section4_matrix18_score;
			}else {
				return _Scoreform_section4_matrix18_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix18/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix18_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix18/score",v);
		_Scoreform_section4_matrix18_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix19_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix19/response.
	 */
	public Integer getScoreform_section4_matrix19_response() {
		try{
			if (_Scoreform_section4_matrix19_response==null){
				_Scoreform_section4_matrix19_response=getIntegerProperty("ScoreForm/section4/matrix19/response");
				return _Scoreform_section4_matrix19_response;
			}else {
				return _Scoreform_section4_matrix19_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix19/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix19_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix19/response",v);
		_Scoreform_section4_matrix19_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix19_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix19/score.
	 */
	public Integer getScoreform_section4_matrix19_score() {
		try{
			if (_Scoreform_section4_matrix19_score==null){
				_Scoreform_section4_matrix19_score=getIntegerProperty("ScoreForm/section4/matrix19/score");
				return _Scoreform_section4_matrix19_score;
			}else {
				return _Scoreform_section4_matrix19_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix19/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix19_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix19/score",v);
		_Scoreform_section4_matrix19_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix20_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix20/response.
	 */
	public Integer getScoreform_section4_matrix20_response() {
		try{
			if (_Scoreform_section4_matrix20_response==null){
				_Scoreform_section4_matrix20_response=getIntegerProperty("ScoreForm/section4/matrix20/response");
				return _Scoreform_section4_matrix20_response;
			}else {
				return _Scoreform_section4_matrix20_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix20/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix20_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix20/response",v);
		_Scoreform_section4_matrix20_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix20_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix20/score.
	 */
	public Integer getScoreform_section4_matrix20_score() {
		try{
			if (_Scoreform_section4_matrix20_score==null){
				_Scoreform_section4_matrix20_score=getIntegerProperty("ScoreForm/section4/matrix20/score");
				return _Scoreform_section4_matrix20_score;
			}else {
				return _Scoreform_section4_matrix20_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix20/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix20_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix20/score",v);
		_Scoreform_section4_matrix20_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix21_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix21/response.
	 */
	public Integer getScoreform_section4_matrix21_response() {
		try{
			if (_Scoreform_section4_matrix21_response==null){
				_Scoreform_section4_matrix21_response=getIntegerProperty("ScoreForm/section4/matrix21/response");
				return _Scoreform_section4_matrix21_response;
			}else {
				return _Scoreform_section4_matrix21_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix21/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix21_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix21/response",v);
		_Scoreform_section4_matrix21_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix21_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix21/score.
	 */
	public Integer getScoreform_section4_matrix21_score() {
		try{
			if (_Scoreform_section4_matrix21_score==null){
				_Scoreform_section4_matrix21_score=getIntegerProperty("ScoreForm/section4/matrix21/score");
				return _Scoreform_section4_matrix21_score;
			}else {
				return _Scoreform_section4_matrix21_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix21/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix21_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix21/score",v);
		_Scoreform_section4_matrix21_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix22_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix22/response.
	 */
	public Integer getScoreform_section4_matrix22_response() {
		try{
			if (_Scoreform_section4_matrix22_response==null){
				_Scoreform_section4_matrix22_response=getIntegerProperty("ScoreForm/section4/matrix22/response");
				return _Scoreform_section4_matrix22_response;
			}else {
				return _Scoreform_section4_matrix22_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix22/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix22_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix22/response",v);
		_Scoreform_section4_matrix22_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix22_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix22/score.
	 */
	public Integer getScoreform_section4_matrix22_score() {
		try{
			if (_Scoreform_section4_matrix22_score==null){
				_Scoreform_section4_matrix22_score=getIntegerProperty("ScoreForm/section4/matrix22/score");
				return _Scoreform_section4_matrix22_score;
			}else {
				return _Scoreform_section4_matrix22_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix22/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix22_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix22/score",v);
		_Scoreform_section4_matrix22_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix23_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix23/response.
	 */
	public Integer getScoreform_section4_matrix23_response() {
		try{
			if (_Scoreform_section4_matrix23_response==null){
				_Scoreform_section4_matrix23_response=getIntegerProperty("ScoreForm/section4/matrix23/response");
				return _Scoreform_section4_matrix23_response;
			}else {
				return _Scoreform_section4_matrix23_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix23/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix23_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix23/response",v);
		_Scoreform_section4_matrix23_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix23_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix23/score.
	 */
	public Integer getScoreform_section4_matrix23_score() {
		try{
			if (_Scoreform_section4_matrix23_score==null){
				_Scoreform_section4_matrix23_score=getIntegerProperty("ScoreForm/section4/matrix23/score");
				return _Scoreform_section4_matrix23_score;
			}else {
				return _Scoreform_section4_matrix23_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix23/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix23_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix23/score",v);
		_Scoreform_section4_matrix23_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix24_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix24/response.
	 */
	public Integer getScoreform_section4_matrix24_response() {
		try{
			if (_Scoreform_section4_matrix24_response==null){
				_Scoreform_section4_matrix24_response=getIntegerProperty("ScoreForm/section4/matrix24/response");
				return _Scoreform_section4_matrix24_response;
			}else {
				return _Scoreform_section4_matrix24_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix24/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix24_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix24/response",v);
		_Scoreform_section4_matrix24_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix24_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix24/score.
	 */
	public Integer getScoreform_section4_matrix24_score() {
		try{
			if (_Scoreform_section4_matrix24_score==null){
				_Scoreform_section4_matrix24_score=getIntegerProperty("ScoreForm/section4/matrix24/score");
				return _Scoreform_section4_matrix24_score;
			}else {
				return _Scoreform_section4_matrix24_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix24/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix24_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix24/score",v);
		_Scoreform_section4_matrix24_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix25_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix25/response.
	 */
	public Integer getScoreform_section4_matrix25_response() {
		try{
			if (_Scoreform_section4_matrix25_response==null){
				_Scoreform_section4_matrix25_response=getIntegerProperty("ScoreForm/section4/matrix25/response");
				return _Scoreform_section4_matrix25_response;
			}else {
				return _Scoreform_section4_matrix25_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix25/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix25_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix25/response",v);
		_Scoreform_section4_matrix25_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix25_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix25/score.
	 */
	public Integer getScoreform_section4_matrix25_score() {
		try{
			if (_Scoreform_section4_matrix25_score==null){
				_Scoreform_section4_matrix25_score=getIntegerProperty("ScoreForm/section4/matrix25/score");
				return _Scoreform_section4_matrix25_score;
			}else {
				return _Scoreform_section4_matrix25_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix25/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix25_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix25/score",v);
		_Scoreform_section4_matrix25_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix26_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix26/response.
	 */
	public Integer getScoreform_section4_matrix26_response() {
		try{
			if (_Scoreform_section4_matrix26_response==null){
				_Scoreform_section4_matrix26_response=getIntegerProperty("ScoreForm/section4/matrix26/response");
				return _Scoreform_section4_matrix26_response;
			}else {
				return _Scoreform_section4_matrix26_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix26/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix26_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix26/response",v);
		_Scoreform_section4_matrix26_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix26_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix26/score.
	 */
	public Integer getScoreform_section4_matrix26_score() {
		try{
			if (_Scoreform_section4_matrix26_score==null){
				_Scoreform_section4_matrix26_score=getIntegerProperty("ScoreForm/section4/matrix26/score");
				return _Scoreform_section4_matrix26_score;
			}else {
				return _Scoreform_section4_matrix26_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix26/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix26_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix26/score",v);
		_Scoreform_section4_matrix26_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix27_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix27/response.
	 */
	public Integer getScoreform_section4_matrix27_response() {
		try{
			if (_Scoreform_section4_matrix27_response==null){
				_Scoreform_section4_matrix27_response=getIntegerProperty("ScoreForm/section4/matrix27/response");
				return _Scoreform_section4_matrix27_response;
			}else {
				return _Scoreform_section4_matrix27_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix27/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix27_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix27/response",v);
		_Scoreform_section4_matrix27_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix27_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix27/score.
	 */
	public Integer getScoreform_section4_matrix27_score() {
		try{
			if (_Scoreform_section4_matrix27_score==null){
				_Scoreform_section4_matrix27_score=getIntegerProperty("ScoreForm/section4/matrix27/score");
				return _Scoreform_section4_matrix27_score;
			}else {
				return _Scoreform_section4_matrix27_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix27/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix27_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix27/score",v);
		_Scoreform_section4_matrix27_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix28_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix28/response.
	 */
	public Integer getScoreform_section4_matrix28_response() {
		try{
			if (_Scoreform_section4_matrix28_response==null){
				_Scoreform_section4_matrix28_response=getIntegerProperty("ScoreForm/section4/matrix28/response");
				return _Scoreform_section4_matrix28_response;
			}else {
				return _Scoreform_section4_matrix28_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix28/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix28_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix28/response",v);
		_Scoreform_section4_matrix28_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix28_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix28/score.
	 */
	public Integer getScoreform_section4_matrix28_score() {
		try{
			if (_Scoreform_section4_matrix28_score==null){
				_Scoreform_section4_matrix28_score=getIntegerProperty("ScoreForm/section4/matrix28/score");
				return _Scoreform_section4_matrix28_score;
			}else {
				return _Scoreform_section4_matrix28_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix28/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix28_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix28/score",v);
		_Scoreform_section4_matrix28_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix29_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix29/response.
	 */
	public Integer getScoreform_section4_matrix29_response() {
		try{
			if (_Scoreform_section4_matrix29_response==null){
				_Scoreform_section4_matrix29_response=getIntegerProperty("ScoreForm/section4/matrix29/response");
				return _Scoreform_section4_matrix29_response;
			}else {
				return _Scoreform_section4_matrix29_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix29/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix29_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix29/response",v);
		_Scoreform_section4_matrix29_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix29_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix29/score.
	 */
	public Integer getScoreform_section4_matrix29_score() {
		try{
			if (_Scoreform_section4_matrix29_score==null){
				_Scoreform_section4_matrix29_score=getIntegerProperty("ScoreForm/section4/matrix29/score");
				return _Scoreform_section4_matrix29_score;
			}else {
				return _Scoreform_section4_matrix29_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix29/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix29_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix29/score",v);
		_Scoreform_section4_matrix29_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix30_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix30/response.
	 */
	public Integer getScoreform_section4_matrix30_response() {
		try{
			if (_Scoreform_section4_matrix30_response==null){
				_Scoreform_section4_matrix30_response=getIntegerProperty("ScoreForm/section4/matrix30/response");
				return _Scoreform_section4_matrix30_response;
			}else {
				return _Scoreform_section4_matrix30_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix30/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix30_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix30/response",v);
		_Scoreform_section4_matrix30_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix30_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix30/score.
	 */
	public Integer getScoreform_section4_matrix30_score() {
		try{
			if (_Scoreform_section4_matrix30_score==null){
				_Scoreform_section4_matrix30_score=getIntegerProperty("ScoreForm/section4/matrix30/score");
				return _Scoreform_section4_matrix30_score;
			}else {
				return _Scoreform_section4_matrix30_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix30/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix30_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix30/score",v);
		_Scoreform_section4_matrix30_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix31_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix31/response.
	 */
	public Integer getScoreform_section4_matrix31_response() {
		try{
			if (_Scoreform_section4_matrix31_response==null){
				_Scoreform_section4_matrix31_response=getIntegerProperty("ScoreForm/section4/matrix31/response");
				return _Scoreform_section4_matrix31_response;
			}else {
				return _Scoreform_section4_matrix31_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix31/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix31_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix31/response",v);
		_Scoreform_section4_matrix31_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix31_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix31/score.
	 */
	public Integer getScoreform_section4_matrix31_score() {
		try{
			if (_Scoreform_section4_matrix31_score==null){
				_Scoreform_section4_matrix31_score=getIntegerProperty("ScoreForm/section4/matrix31/score");
				return _Scoreform_section4_matrix31_score;
			}else {
				return _Scoreform_section4_matrix31_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix31/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix31_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix31/score",v);
		_Scoreform_section4_matrix31_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix32_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix32/response.
	 */
	public Integer getScoreform_section4_matrix32_response() {
		try{
			if (_Scoreform_section4_matrix32_response==null){
				_Scoreform_section4_matrix32_response=getIntegerProperty("ScoreForm/section4/matrix32/response");
				return _Scoreform_section4_matrix32_response;
			}else {
				return _Scoreform_section4_matrix32_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix32/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix32_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix32/response",v);
		_Scoreform_section4_matrix32_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix32_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix32/score.
	 */
	public Integer getScoreform_section4_matrix32_score() {
		try{
			if (_Scoreform_section4_matrix32_score==null){
				_Scoreform_section4_matrix32_score=getIntegerProperty("ScoreForm/section4/matrix32/score");
				return _Scoreform_section4_matrix32_score;
			}else {
				return _Scoreform_section4_matrix32_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix32/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix32_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix32/score",v);
		_Scoreform_section4_matrix32_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix33_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix33/response.
	 */
	public Integer getScoreform_section4_matrix33_response() {
		try{
			if (_Scoreform_section4_matrix33_response==null){
				_Scoreform_section4_matrix33_response=getIntegerProperty("ScoreForm/section4/matrix33/response");
				return _Scoreform_section4_matrix33_response;
			}else {
				return _Scoreform_section4_matrix33_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix33/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix33_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix33/response",v);
		_Scoreform_section4_matrix33_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix33_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix33/score.
	 */
	public Integer getScoreform_section4_matrix33_score() {
		try{
			if (_Scoreform_section4_matrix33_score==null){
				_Scoreform_section4_matrix33_score=getIntegerProperty("ScoreForm/section4/matrix33/score");
				return _Scoreform_section4_matrix33_score;
			}else {
				return _Scoreform_section4_matrix33_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix33/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix33_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix33/score",v);
		_Scoreform_section4_matrix33_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix34_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix34/response.
	 */
	public Integer getScoreform_section4_matrix34_response() {
		try{
			if (_Scoreform_section4_matrix34_response==null){
				_Scoreform_section4_matrix34_response=getIntegerProperty("ScoreForm/section4/matrix34/response");
				return _Scoreform_section4_matrix34_response;
			}else {
				return _Scoreform_section4_matrix34_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix34/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix34_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix34/response",v);
		_Scoreform_section4_matrix34_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix34_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix34/score.
	 */
	public Integer getScoreform_section4_matrix34_score() {
		try{
			if (_Scoreform_section4_matrix34_score==null){
				_Scoreform_section4_matrix34_score=getIntegerProperty("ScoreForm/section4/matrix34/score");
				return _Scoreform_section4_matrix34_score;
			}else {
				return _Scoreform_section4_matrix34_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix34/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix34_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix34/score",v);
		_Scoreform_section4_matrix34_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix35_response=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix35/response.
	 */
	public Integer getScoreform_section4_matrix35_response() {
		try{
			if (_Scoreform_section4_matrix35_response==null){
				_Scoreform_section4_matrix35_response=getIntegerProperty("ScoreForm/section4/matrix35/response");
				return _Scoreform_section4_matrix35_response;
			}else {
				return _Scoreform_section4_matrix35_response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix35/response.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix35_response(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix35/response",v);
		_Scoreform_section4_matrix35_response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_section4_matrix35_score=null;

	/**
	 * @return Returns the ScoreForm/section4/matrix35/score.
	 */
	public Integer getScoreform_section4_matrix35_score() {
		try{
			if (_Scoreform_section4_matrix35_score==null){
				_Scoreform_section4_matrix35_score=getIntegerProperty("ScoreForm/section4/matrix35/score");
				return _Scoreform_section4_matrix35_score;
			}else {
				return _Scoreform_section4_matrix35_score;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/section4/matrix35/score.
	 * @param v Value to Set.
	 */
	public void setScoreform_section4_matrix35_score(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/section4/matrix35/score",v);
		_Scoreform_section4_matrix35_score=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_behavob_reasonrefer=null;

	/**
	 * @return Returns the ScoreForm/behavOb/reasonRefer.
	 */
	public String getScoreform_behavob_reasonrefer(){
		try{
			if (_Scoreform_behavob_reasonrefer==null){
				_Scoreform_behavob_reasonrefer=getStringProperty("ScoreForm/behavOb/reasonRefer");
				return _Scoreform_behavob_reasonrefer;
			}else {
				return _Scoreform_behavob_reasonrefer;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/behavOb/reasonRefer.
	 * @param v Value to Set.
	 */
	public void setScoreform_behavob_reasonrefer(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/behavOb/reasonRefer",v);
		_Scoreform_behavob_reasonrefer=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_behavob_attitude=null;

	/**
	 * @return Returns the ScoreForm/behavOb/attitude.
	 */
	public String getScoreform_behavob_attitude(){
		try{
			if (_Scoreform_behavob_attitude==null){
				_Scoreform_behavob_attitude=getStringProperty("ScoreForm/behavOb/attitude");
				return _Scoreform_behavob_attitude;
			}else {
				return _Scoreform_behavob_attitude;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/behavOb/attitude.
	 * @param v Value to Set.
	 */
	public void setScoreform_behavob_attitude(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/behavOb/attitude",v);
		_Scoreform_behavob_attitude=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_behavob_physappear=null;

	/**
	 * @return Returns the ScoreForm/behavOb/physAppear.
	 */
	public String getScoreform_behavob_physappear(){
		try{
			if (_Scoreform_behavob_physappear==null){
				_Scoreform_behavob_physappear=getStringProperty("ScoreForm/behavOb/physAppear");
				return _Scoreform_behavob_physappear;
			}else {
				return _Scoreform_behavob_physappear;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/behavOb/physAppear.
	 * @param v Value to Set.
	 */
	public void setScoreform_behavob_physappear(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/behavOb/physAppear",v);
		_Scoreform_behavob_physappear=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_behavob_attention=null;

	/**
	 * @return Returns the ScoreForm/behavOb/attention.
	 */
	public String getScoreform_behavob_attention(){
		try{
			if (_Scoreform_behavob_attention==null){
				_Scoreform_behavob_attention=getStringProperty("ScoreForm/behavOb/attention");
				return _Scoreform_behavob_attention;
			}else {
				return _Scoreform_behavob_attention;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/behavOb/attention.
	 * @param v Value to Set.
	 */
	public void setScoreform_behavob_attention(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/behavOb/attention",v);
		_Scoreform_behavob_attention=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_behavob_visaudmotor=null;

	/**
	 * @return Returns the ScoreForm/behavOb/visAudMotor.
	 */
	public String getScoreform_behavob_visaudmotor(){
		try{
			if (_Scoreform_behavob_visaudmotor==null){
				_Scoreform_behavob_visaudmotor=getStringProperty("ScoreForm/behavOb/visAudMotor");
				return _Scoreform_behavob_visaudmotor;
			}else {
				return _Scoreform_behavob_visaudmotor;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/behavOb/visAudMotor.
	 * @param v Value to Set.
	 */
	public void setScoreform_behavob_visaudmotor(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/behavOb/visAudMotor",v);
		_Scoreform_behavob_visaudmotor=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_behavob_lang=null;

	/**
	 * @return Returns the ScoreForm/behavOb/lang.
	 */
	public String getScoreform_behavob_lang(){
		try{
			if (_Scoreform_behavob_lang==null){
				_Scoreform_behavob_lang=getStringProperty("ScoreForm/behavOb/lang");
				return _Scoreform_behavob_lang;
			}else {
				return _Scoreform_behavob_lang;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/behavOb/lang.
	 * @param v Value to Set.
	 */
	public void setScoreform_behavob_lang(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/behavOb/lang",v);
		_Scoreform_behavob_lang=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_behavob_affectmood=null;

	/**
	 * @return Returns the ScoreForm/behavOb/affectMood.
	 */
	public String getScoreform_behavob_affectmood(){
		try{
			if (_Scoreform_behavob_affectmood==null){
				_Scoreform_behavob_affectmood=getStringProperty("ScoreForm/behavOb/affectMood");
				return _Scoreform_behavob_affectmood;
			}else {
				return _Scoreform_behavob_affectmood;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/behavOb/affectMood.
	 * @param v Value to Set.
	 */
	public void setScoreform_behavob_affectmood(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/behavOb/affectMood",v);
		_Scoreform_behavob_affectmood=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_behavob_unusualbehav=null;

	/**
	 * @return Returns the ScoreForm/behavOb/unusualBehav.
	 */
	public String getScoreform_behavob_unusualbehav(){
		try{
			if (_Scoreform_behavob_unusualbehav==null){
				_Scoreform_behavob_unusualbehav=getStringProperty("ScoreForm/behavOb/unusualBehav");
				return _Scoreform_behavob_unusualbehav;
			}else {
				return _Scoreform_behavob_unusualbehav;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/behavOb/unusualBehav.
	 * @param v Value to Set.
	 */
	public void setScoreform_behavob_unusualbehav(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/behavOb/unusualBehav",v);
		_Scoreform_behavob_unusualbehav=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_behavob_other=null;

	/**
	 * @return Returns the ScoreForm/behavOb/other.
	 */
	public String getScoreform_behavob_other(){
		try{
			if (_Scoreform_behavob_other==null){
				_Scoreform_behavob_other=getStringProperty("ScoreForm/behavOb/other");
				return _Scoreform_behavob_other;
			}else {
				return _Scoreform_behavob_other;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/behavOb/other.
	 * @param v Value to Set.
	 */
	public void setScoreform_behavob_other(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/behavOb/other",v);
		_Scoreform_behavob_other=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.IqWasi1999data> getAllIqWasi1999datas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.IqWasi1999data> al = new ArrayList<org.nrg.xdat.om.IqWasi1999data>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.IqWasi1999data> getIqWasi1999datasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.IqWasi1999data> al = new ArrayList<org.nrg.xdat.om.IqWasi1999data>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.IqWasi1999data> getIqWasi1999datasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.IqWasi1999data> al = new ArrayList<org.nrg.xdat.om.IqWasi1999data>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static IqWasi1999data getIqWasi1999datasByIqAbstractiqtestId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("iq:wasi1999Data/iq_abstractiqtest_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (IqWasi1999data) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //abstractIQTest
	        IqAbstractiqtest childAbstractiqtest = (IqAbstractiqtest)this.getAbstractiqtest();
	            if (childAbstractiqtest!=null){
	              for(ResourceFile rf: ((IqAbstractiqtest)childAbstractiqtest).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("abstractIQTest[" + ((IqAbstractiqtest)childAbstractiqtest).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("abstractIQTest/" + ((IqAbstractiqtest)childAbstractiqtest).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
