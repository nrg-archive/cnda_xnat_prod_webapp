/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoMpetManualpettimecoursedataRoi extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.MpetManualpettimecoursedataRoiI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoMpetManualpettimecoursedataRoi.class);
	public static String SCHEMA_ELEMENT_NAME="mpet:manualpetTimeCourseData_roi";

	public AutoMpetManualpettimecoursedataRoi(ItemI item)
	{
		super(item);
	}

	public AutoMpetManualpettimecoursedataRoi(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoMpetManualpettimecoursedataRoi(UserI user)
	 **/
	public AutoMpetManualpettimecoursedataRoi(){}

	public AutoMpetManualpettimecoursedataRoi(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "mpet:manualpetTimeCourseData_roi";
	}

	//FIELD

	private Double _Slope=null;

	/**
	 * @return Returns the Slope.
	 */
	public Double getSlope() {
		try{
			if (_Slope==null){
				_Slope=getDoubleProperty("Slope");
				return _Slope;
			}else {
				return _Slope;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Slope.
	 * @param v Value to Set.
	 */
	public void setSlope(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Slope",v);
		_Slope=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Correlation=null;

	/**
	 * @return Returns the Correlation.
	 */
	public Double getCorrelation() {
		try{
			if (_Correlation==null){
				_Correlation=getDoubleProperty("Correlation");
				return _Correlation;
			}else {
				return _Correlation;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Correlation.
	 * @param v Value to Set.
	 */
	public void setCorrelation(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Correlation",v);
		_Correlation=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Bp=null;

	/**
	 * @return Returns the BP.
	 */
	public Double getBp() {
		try{
			if (_Bp==null){
				_Bp=getDoubleProperty("BP");
				return _Bp;
			}else {
				return _Bp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BP.
	 * @param v Value to Set.
	 */
	public void setBp(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BP",v);
		_Bp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Suvr=null;

	/**
	 * @return Returns the SUVR.
	 */
	public Double getSuvr() {
		try{
			if (_Suvr==null){
				_Suvr=getDoubleProperty("SUVR");
				return _Suvr;
			}else {
				return _Suvr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SUVR.
	 * @param v Value to Set.
	 */
	public void setSuvr(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SUVR",v);
		_Suvr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Numvoxels=null;

	/**
	 * @return Returns the NumVoxels.
	 */
	public Integer getNumvoxels() {
		try{
			if (_Numvoxels==null){
				_Numvoxels=getIntegerProperty("NumVoxels");
				return _Numvoxels;
			}else {
				return _Numvoxels;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NumVoxels.
	 * @param v Value to Set.
	 */
	public void setNumvoxels(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NumVoxels",v);
		_Numvoxels=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Intercept=null;

	/**
	 * @return Returns the Intercept.
	 */
	public Double getIntercept() {
		try{
			if (_Intercept==null){
				_Intercept=getDoubleProperty("Intercept");
				return _Intercept;
			}else {
				return _Intercept;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Intercept.
	 * @param v Value to Set.
	 */
	public void setIntercept(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Intercept",v);
		_Intercept=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Rsquared=null;

	/**
	 * @return Returns the RSquared.
	 */
	public Double getRsquared() {
		try{
			if (_Rsquared==null){
				_Rsquared=getDoubleProperty("RSquared");
				return _Rsquared;
			}else {
				return _Rsquared;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RSquared.
	 * @param v Value to Set.
	 */
	public void setRsquared(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RSquared",v);
		_Rsquared=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Name=null;

	/**
	 * @return Returns the name.
	 */
	public String getName(){
		try{
			if (_Name==null){
				_Name=getStringProperty("name");
				return _Name;
			}else {
				return _Name;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for name.
	 * @param v Value to Set.
	 */
	public void setName(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/name",v);
		_Name=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _MpetManualpettimecoursedataRoiId=null;

	/**
	 * @return Returns the mpet_manualpetTimeCourseData_roi_id.
	 */
	public Integer getMpetManualpettimecoursedataRoiId() {
		try{
			if (_MpetManualpettimecoursedataRoiId==null){
				_MpetManualpettimecoursedataRoiId=getIntegerProperty("mpet_manualpetTimeCourseData_roi_id");
				return _MpetManualpettimecoursedataRoiId;
			}else {
				return _MpetManualpettimecoursedataRoiId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for mpet_manualpetTimeCourseData_roi_id.
	 * @param v Value to Set.
	 */
	public void setMpetManualpettimecoursedataRoiId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/mpet_manualpetTimeCourseData_roi_id",v);
		_MpetManualpettimecoursedataRoiId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.MpetManualpettimecoursedataRoi> getAllMpetManualpettimecoursedataRois(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.MpetManualpettimecoursedataRoi> al = new ArrayList<org.nrg.xdat.om.MpetManualpettimecoursedataRoi>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.MpetManualpettimecoursedataRoi> getMpetManualpettimecoursedataRoisByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.MpetManualpettimecoursedataRoi> al = new ArrayList<org.nrg.xdat.om.MpetManualpettimecoursedataRoi>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.MpetManualpettimecoursedataRoi> getMpetManualpettimecoursedataRoisByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.MpetManualpettimecoursedataRoi> al = new ArrayList<org.nrg.xdat.om.MpetManualpettimecoursedataRoi>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static MpetManualpettimecoursedataRoi getMpetManualpettimecoursedataRoisByMpetManualpettimecoursedataRoiId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("mpet:manualpetTimeCourseData_roi/mpet_manualpetTimeCourseData_roi_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (MpetManualpettimecoursedataRoi) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
