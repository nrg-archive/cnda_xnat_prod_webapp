/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:01 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoClinVitalsdata extends XnatSubjectassessordata implements org.nrg.xdat.model.ClinVitalsdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoClinVitalsdata.class);
	public static String SCHEMA_ELEMENT_NAME="clin:vitalsData";

	public AutoClinVitalsdata(ItemI item)
	{
		super(item);
	}

	public AutoClinVitalsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoClinVitalsdata(UserI user)
	 **/
	public AutoClinVitalsdata(){}

	public AutoClinVitalsdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "clin:vitalsData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Vsweight=null;

	/**
	 * @return Returns the VSWEIGHT.
	 */
	public Double getVsweight() {
		try{
			if (_Vsweight==null){
				_Vsweight=getDoubleProperty("VSWEIGHT");
				return _Vsweight;
			}else {
				return _Vsweight;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VSWEIGHT.
	 * @param v Value to Set.
	 */
	public void setVsweight(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VSWEIGHT",v);
		_Vsweight=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Vswtunit=null;

	/**
	 * @return Returns the VSWTUNIT.
	 */
	public Integer getVswtunit() {
		try{
			if (_Vswtunit==null){
				_Vswtunit=getIntegerProperty("VSWTUNIT");
				return _Vswtunit;
			}else {
				return _Vswtunit;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VSWTUNIT.
	 * @param v Value to Set.
	 */
	public void setVswtunit(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VSWTUNIT",v);
		_Vswtunit=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Vsheight=null;

	/**
	 * @return Returns the VSHEIGHT.
	 */
	public Double getVsheight() {
		try{
			if (_Vsheight==null){
				_Vsheight=getDoubleProperty("VSHEIGHT");
				return _Vsheight;
			}else {
				return _Vsheight;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VSHEIGHT.
	 * @param v Value to Set.
	 */
	public void setVsheight(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VSHEIGHT",v);
		_Vsheight=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Vshtunit=null;

	/**
	 * @return Returns the VSHTUNIT.
	 */
	public Integer getVshtunit() {
		try{
			if (_Vshtunit==null){
				_Vshtunit=getIntegerProperty("VSHTUNIT");
				return _Vshtunit;
			}else {
				return _Vshtunit;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VSHTUNIT.
	 * @param v Value to Set.
	 */
	public void setVshtunit(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VSHTUNIT",v);
		_Vshtunit=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Vsbpsys=null;

	/**
	 * @return Returns the VSBPSYS.
	 */
	public Integer getVsbpsys() {
		try{
			if (_Vsbpsys==null){
				_Vsbpsys=getIntegerProperty("VSBPSYS");
				return _Vsbpsys;
			}else {
				return _Vsbpsys;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VSBPSYS.
	 * @param v Value to Set.
	 */
	public void setVsbpsys(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VSBPSYS",v);
		_Vsbpsys=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Vsbpdia=null;

	/**
	 * @return Returns the VSBPDIA.
	 */
	public Integer getVsbpdia() {
		try{
			if (_Vsbpdia==null){
				_Vsbpdia=getIntegerProperty("VSBPDIA");
				return _Vsbpdia;
			}else {
				return _Vsbpdia;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VSBPDIA.
	 * @param v Value to Set.
	 */
	public void setVsbpdia(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VSBPDIA",v);
		_Vsbpdia=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Vspulse=null;

	/**
	 * @return Returns the VSPULSE.
	 */
	public Integer getVspulse() {
		try{
			if (_Vspulse==null){
				_Vspulse=getIntegerProperty("VSPULSE");
				return _Vspulse;
			}else {
				return _Vspulse;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VSPULSE.
	 * @param v Value to Set.
	 */
	public void setVspulse(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VSPULSE",v);
		_Vspulse=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Vsresp=null;

	/**
	 * @return Returns the VSRESP.
	 */
	public Integer getVsresp() {
		try{
			if (_Vsresp==null){
				_Vsresp=getIntegerProperty("VSRESP");
				return _Vsresp;
			}else {
				return _Vsresp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VSRESP.
	 * @param v Value to Set.
	 */
	public void setVsresp(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VSRESP",v);
		_Vsresp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Vstemp=null;

	/**
	 * @return Returns the VSTEMP.
	 */
	public Double getVstemp() {
		try{
			if (_Vstemp==null){
				_Vstemp=getDoubleProperty("VSTEMP");
				return _Vstemp;
			}else {
				return _Vstemp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VSTEMP.
	 * @param v Value to Set.
	 */
	public void setVstemp(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VSTEMP",v);
		_Vstemp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Vstmpunt=null;

	/**
	 * @return Returns the VSTMPUNT.
	 */
	public Integer getVstmpunt() {
		try{
			if (_Vstmpunt==null){
				_Vstmpunt=getIntegerProperty("VSTMPUNT");
				return _Vstmpunt;
			}else {
				return _Vstmpunt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VSTMPUNT.
	 * @param v Value to Set.
	 */
	public void setVstmpunt(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VSTMPUNT",v);
		_Vstmpunt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Vstmpsrc=null;

	/**
	 * @return Returns the VSTMPSRC.
	 */
	public Integer getVstmpsrc() {
		try{
			if (_Vstmpsrc==null){
				_Vstmpsrc=getIntegerProperty("VSTMPSRC");
				return _Vstmpsrc;
			}else {
				return _Vstmpsrc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VSTMPSRC.
	 * @param v Value to Set.
	 */
	public void setVstmpsrc(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VSTMPSRC",v);
		_Vstmpsrc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Vscomm=null;

	/**
	 * @return Returns the VSCOMM.
	 */
	public String getVscomm(){
		try{
			if (_Vscomm==null){
				_Vscomm=getStringProperty("VSCOMM");
				return _Vscomm;
			}else {
				return _Vscomm;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VSCOMM.
	 * @param v Value to Set.
	 */
	public void setVscomm(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VSCOMM",v);
		_Vscomm=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.ClinVitalsdata> getAllClinVitalsdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.ClinVitalsdata> al = new ArrayList<org.nrg.xdat.om.ClinVitalsdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.ClinVitalsdata> getClinVitalsdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.ClinVitalsdata> al = new ArrayList<org.nrg.xdat.om.ClinVitalsdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.ClinVitalsdata> getClinVitalsdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.ClinVitalsdata> al = new ArrayList<org.nrg.xdat.om.ClinVitalsdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ClinVitalsdata getClinVitalsdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("clin:vitalsData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (ClinVitalsdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
