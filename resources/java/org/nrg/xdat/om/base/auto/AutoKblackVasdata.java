/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoKblackVasdata extends XnatSubjectassessordata implements org.nrg.xdat.model.KblackVasdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoKblackVasdata.class);
	public static String SCHEMA_ELEMENT_NAME="kblack:vasData";

	public AutoKblackVasdata(ItemI item)
	{
		super(item);
	}

	public AutoKblackVasdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoKblackVasdata(UserI user)
	 **/
	public AutoKblackVasdata(){}

	public AutoKblackVasdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "kblack:vasData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _NauseaVomiting=null;

	/**
	 * @return Returns the nausea_vomiting.
	 */
	public Integer getNauseaVomiting() {
		try{
			if (_NauseaVomiting==null){
				_NauseaVomiting=getIntegerProperty("nausea_vomiting");
				return _NauseaVomiting;
			}else {
				return _NauseaVomiting;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for nausea_vomiting.
	 * @param v Value to Set.
	 */
	public void setNauseaVomiting(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/nausea_vomiting",v);
		_NauseaVomiting=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sleepiness=null;

	/**
	 * @return Returns the sleepiness.
	 */
	public Integer getSleepiness() {
		try{
			if (_Sleepiness==null){
				_Sleepiness=getIntegerProperty("sleepiness");
				return _Sleepiness;
			}else {
				return _Sleepiness;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sleepiness.
	 * @param v Value to Set.
	 */
	public void setSleepiness(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sleepiness",v);
		_Sleepiness=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _LightheadednessDizziness=null;

	/**
	 * @return Returns the lightheadedness_dizziness.
	 */
	public Integer getLightheadednessDizziness() {
		try{
			if (_LightheadednessDizziness==null){
				_LightheadednessDizziness=getIntegerProperty("lightheadedness_dizziness");
				return _LightheadednessDizziness;
			}else {
				return _LightheadednessDizziness;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for lightheadedness_dizziness.
	 * @param v Value to Set.
	 */
	public void setLightheadednessDizziness(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/lightheadedness_dizziness",v);
		_LightheadednessDizziness=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Overallwellbeing=null;

	/**
	 * @return Returns the overallWellBeing.
	 */
	public Integer getOverallwellbeing() {
		try{
			if (_Overallwellbeing==null){
				_Overallwellbeing=getIntegerProperty("overallWellBeing");
				return _Overallwellbeing;
			}else {
				return _Overallwellbeing;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for overallWellBeing.
	 * @param v Value to Set.
	 */
	public void setOverallwellbeing(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/overallWellBeing",v);
		_Overallwellbeing=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Problem=null;

	/**
	 * @return Returns the problem.
	 */
	public Boolean getProblem() {
		try{
			if (_Problem==null){
				_Problem=getBooleanProperty("problem");
				return _Problem;
			}else {
				return _Problem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for problem.
	 * @param v Value to Set.
	 */
	public void setProblem(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/problem",v);
		_Problem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.KblackVasdata> getAllKblackVasdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.KblackVasdata> al = new ArrayList<org.nrg.xdat.om.KblackVasdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.KblackVasdata> getKblackVasdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.KblackVasdata> al = new ArrayList<org.nrg.xdat.om.KblackVasdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.KblackVasdata> getKblackVasdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.KblackVasdata> al = new ArrayList<org.nrg.xdat.om.KblackVasdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static KblackVasdata getKblackVasdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("kblack:vasData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (KblackVasdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
