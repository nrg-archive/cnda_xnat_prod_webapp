/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCbatVisualspatialtest2 extends XnatSubjectassessordata implements org.nrg.xdat.model.CbatVisualspatialtest2I {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCbatVisualspatialtest2.class);
	public static String SCHEMA_ELEMENT_NAME="cbat:visualSpatialTest2";

	public AutoCbatVisualspatialtest2(ItemI item)
	{
		super(item);
	}

	public AutoCbatVisualspatialtest2(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCbatVisualspatialtest2(UserI user)
	 **/
	public AutoCbatVisualspatialtest2(){}

	public AutoCbatVisualspatialtest2(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cbat:visualSpatialTest2";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T1_accuracy=null;

	/**
	 * @return Returns the T1/accuracy.
	 */
	public Integer getT1_accuracy() {
		try{
			if (_T1_accuracy==null){
				_T1_accuracy=getIntegerProperty("T1/accuracy");
				return _T1_accuracy;
			}else {
				return _T1_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T1/accuracy.
	 * @param v Value to Set.
	 */
	public void setT1_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T1/accuracy",v);
		_T1_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T2_accuracy=null;

	/**
	 * @return Returns the T2/accuracy.
	 */
	public Integer getT2_accuracy() {
		try{
			if (_T2_accuracy==null){
				_T2_accuracy=getIntegerProperty("T2/accuracy");
				return _T2_accuracy;
			}else {
				return _T2_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T2/accuracy.
	 * @param v Value to Set.
	 */
	public void setT2_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T2/accuracy",v);
		_T2_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T3_accuracy=null;

	/**
	 * @return Returns the T3/accuracy.
	 */
	public Integer getT3_accuracy() {
		try{
			if (_T3_accuracy==null){
				_T3_accuracy=getIntegerProperty("T3/accuracy");
				return _T3_accuracy;
			}else {
				return _T3_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T3/accuracy.
	 * @param v Value to Set.
	 */
	public void setT3_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T3/accuracy",v);
		_T3_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T4_accuracy=null;

	/**
	 * @return Returns the T4/accuracy.
	 */
	public Integer getT4_accuracy() {
		try{
			if (_T4_accuracy==null){
				_T4_accuracy=getIntegerProperty("T4/accuracy");
				return _T4_accuracy;
			}else {
				return _T4_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T4/accuracy.
	 * @param v Value to Set.
	 */
	public void setT4_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T4/accuracy",v);
		_T4_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T5_accuracy=null;

	/**
	 * @return Returns the T5/accuracy.
	 */
	public Integer getT5_accuracy() {
		try{
			if (_T5_accuracy==null){
				_T5_accuracy=getIntegerProperty("T5/accuracy");
				return _T5_accuracy;
			}else {
				return _T5_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T5/accuracy.
	 * @param v Value to Set.
	 */
	public void setT5_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T5/accuracy",v);
		_T5_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T6_accuracy=null;

	/**
	 * @return Returns the T6/accuracy.
	 */
	public Integer getT6_accuracy() {
		try{
			if (_T6_accuracy==null){
				_T6_accuracy=getIntegerProperty("T6/accuracy");
				return _T6_accuracy;
			}else {
				return _T6_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T6/accuracy.
	 * @param v Value to Set.
	 */
	public void setT6_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T6/accuracy",v);
		_T6_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T7_accuracy=null;

	/**
	 * @return Returns the T7/accuracy.
	 */
	public Integer getT7_accuracy() {
		try{
			if (_T7_accuracy==null){
				_T7_accuracy=getIntegerProperty("T7/accuracy");
				return _T7_accuracy;
			}else {
				return _T7_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T7/accuracy.
	 * @param v Value to Set.
	 */
	public void setT7_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T7/accuracy",v);
		_T7_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T8_accuracy=null;

	/**
	 * @return Returns the T8/accuracy.
	 */
	public Integer getT8_accuracy() {
		try{
			if (_T8_accuracy==null){
				_T8_accuracy=getIntegerProperty("T8/accuracy");
				return _T8_accuracy;
			}else {
				return _T8_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T8/accuracy.
	 * @param v Value to Set.
	 */
	public void setT8_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T8/accuracy",v);
		_T8_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T9_accuracy=null;

	/**
	 * @return Returns the T9/accuracy.
	 */
	public Integer getT9_accuracy() {
		try{
			if (_T9_accuracy==null){
				_T9_accuracy=getIntegerProperty("T9/accuracy");
				return _T9_accuracy;
			}else {
				return _T9_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T9/accuracy.
	 * @param v Value to Set.
	 */
	public void setT9_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T9/accuracy",v);
		_T9_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T10_accuracy=null;

	/**
	 * @return Returns the T10/accuracy.
	 */
	public Integer getT10_accuracy() {
		try{
			if (_T10_accuracy==null){
				_T10_accuracy=getIntegerProperty("T10/accuracy");
				return _T10_accuracy;
			}else {
				return _T10_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T10/accuracy.
	 * @param v Value to Set.
	 */
	public void setT10_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T10/accuracy",v);
		_T10_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T11_accuracy=null;

	/**
	 * @return Returns the T11/accuracy.
	 */
	public Integer getT11_accuracy() {
		try{
			if (_T11_accuracy==null){
				_T11_accuracy=getIntegerProperty("T11/accuracy");
				return _T11_accuracy;
			}else {
				return _T11_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T11/accuracy.
	 * @param v Value to Set.
	 */
	public void setT11_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T11/accuracy",v);
		_T11_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T12_accuracy=null;

	/**
	 * @return Returns the T12/accuracy.
	 */
	public Integer getT12_accuracy() {
		try{
			if (_T12_accuracy==null){
				_T12_accuracy=getIntegerProperty("T12/accuracy");
				return _T12_accuracy;
			}else {
				return _T12_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T12/accuracy.
	 * @param v Value to Set.
	 */
	public void setT12_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T12/accuracy",v);
		_T12_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CbatVisualspatialtest2> getAllCbatVisualspatialtest2s(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CbatVisualspatialtest2> al = new ArrayList<org.nrg.xdat.om.CbatVisualspatialtest2>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CbatVisualspatialtest2> getCbatVisualspatialtest2sByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CbatVisualspatialtest2> al = new ArrayList<org.nrg.xdat.om.CbatVisualspatialtest2>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CbatVisualspatialtest2> getCbatVisualspatialtest2sByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CbatVisualspatialtest2> al = new ArrayList<org.nrg.xdat.om.CbatVisualspatialtest2>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CbatVisualspatialtest2 getCbatVisualspatialtest2sById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cbat:visualSpatialTest2/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CbatVisualspatialtest2) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
