/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoLs2Lsneuropsychdata extends XnatSubjectassessordata implements org.nrg.xdat.model.Ls2LsneuropsychdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoLs2Lsneuropsychdata.class);
	public static String SCHEMA_ELEMENT_NAME="ls2:lsNeuropsychData";

	public AutoLs2Lsneuropsychdata(ItemI item)
	{
		super(item);
	}

	public AutoLs2Lsneuropsychdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoLs2Lsneuropsychdata(UserI user)
	 **/
	public AutoLs2Lsneuropsychdata(){}

	public AutoLs2Lsneuropsychdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "ls2:lsNeuropsychData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Teststatus_tasksmissing=null;

	/**
	 * @return Returns the testStatus/tasksMissing.
	 */
	public String getTeststatus_tasksmissing(){
		try{
			if (_Teststatus_tasksmissing==null){
				_Teststatus_tasksmissing=getStringProperty("testStatus/tasksMissing");
				return _Teststatus_tasksmissing;
			}else {
				return _Teststatus_tasksmissing;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for testStatus/tasksMissing.
	 * @param v Value to Set.
	 */
	public void setTeststatus_tasksmissing(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/testStatus/tasksMissing",v);
		_Teststatus_tasksmissing=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Teststatus_complete=null;

	/**
	 * @return Returns the testStatus/complete.
	 */
	public String getTeststatus_complete(){
		try{
			if (_Teststatus_complete==null){
				_Teststatus_complete=getStringProperty("testStatus/complete");
				return _Teststatus_complete;
			}else {
				return _Teststatus_complete;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for testStatus/complete.
	 * @param v Value to Set.
	 */
	public void setTeststatus_complete(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/testStatus/complete",v);
		_Teststatus_complete=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Source=null;

	/**
	 * @return Returns the source.
	 */
	public String getSource(){
		try{
			if (_Source==null){
				_Source=getStringProperty("source");
				return _Source;
			}else {
				return _Source;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for source.
	 * @param v Value to Set.
	 */
	public void setSource(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/source",v);
		_Source=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Oasisdate=null;

	/**
	 * @return Returns the oasisDate.
	 */
	public Object getOasisdate(){
		try{
			if (_Oasisdate==null){
				_Oasisdate=getProperty("oasisDate");
				return _Oasisdate;
			}else {
				return _Oasisdate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for oasisDate.
	 * @param v Value to Set.
	 */
	public void setOasisdate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/oasisDate",v);
		_Oasisdate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mmse=null;

	/**
	 * @return Returns the MMSE.
	 */
	public Integer getMmse() {
		try{
			if (_Mmse==null){
				_Mmse=getIntegerProperty("MMSE");
				return _Mmse;
			}else {
				return _Mmse;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MMSE.
	 * @param v Value to Set.
	 */
	public void setMmse(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MMSE",v);
		_Mmse=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Blessed=null;

	/**
	 * @return Returns the Blessed.
	 */
	public Integer getBlessed() {
		try{
			if (_Blessed==null){
				_Blessed=getIntegerProperty("Blessed");
				return _Blessed;
			}else {
				return _Blessed;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Blessed.
	 * @param v Value to Set.
	 */
	public void setBlessed(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Blessed",v);
		_Blessed=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CvltIi_cTa1=null;

	/**
	 * @return Returns the CVLT_II/c_ta1.
	 */
	public Integer getCvltIi_cTa1() {
		try{
			if (_CvltIi_cTa1==null){
				_CvltIi_cTa1=getIntegerProperty("CVLT_II/c_ta1");
				return _CvltIi_cTa1;
			}else {
				return _CvltIi_cTa1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_ta1.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cTa1(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_ta1",v);
		_CvltIi_cTa1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CvltIi_cTa5=null;

	/**
	 * @return Returns the CVLT_II/c_ta5.
	 */
	public Integer getCvltIi_cTa5() {
		try{
			if (_CvltIi_cTa5==null){
				_CvltIi_cTa5=getIntegerProperty("CVLT_II/c_ta5");
				return _CvltIi_cTa5;
			}else {
				return _CvltIi_cTa5;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_ta5.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cTa5(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_ta5",v);
		_CvltIi_cTa5=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CvltIi_cTotal=null;

	/**
	 * @return Returns the CVLT_II/c_total.
	 */
	public Integer getCvltIi_cTotal() {
		try{
			if (_CvltIi_cTotal==null){
				_CvltIi_cTotal=getIntegerProperty("CVLT_II/c_total");
				return _CvltIi_cTotal;
			}else {
				return _CvltIi_cTotal;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_total.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cTotal(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_total",v);
		_CvltIi_cTotal=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CvltIi_cListb=null;

	/**
	 * @return Returns the CVLT_II/c_listb.
	 */
	public Integer getCvltIi_cListb() {
		try{
			if (_CvltIi_cListb==null){
				_CvltIi_cListb=getIntegerProperty("CVLT_II/c_listb");
				return _CvltIi_cListb;
			}else {
				return _CvltIi_cListb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_listb.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cListb(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_listb",v);
		_CvltIi_cListb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CvltIi_cSdfree=null;

	/**
	 * @return Returns the CVLT_II/c_sdfree.
	 */
	public Integer getCvltIi_cSdfree() {
		try{
			if (_CvltIi_cSdfree==null){
				_CvltIi_cSdfree=getIntegerProperty("CVLT_II/c_sdfree");
				return _CvltIi_cSdfree;
			}else {
				return _CvltIi_cSdfree;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_sdfree.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cSdfree(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_sdfree",v);
		_CvltIi_cSdfree=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CvltIi_cSdcue=null;

	/**
	 * @return Returns the CVLT_II/c_sdcue.
	 */
	public Integer getCvltIi_cSdcue() {
		try{
			if (_CvltIi_cSdcue==null){
				_CvltIi_cSdcue=getIntegerProperty("CVLT_II/c_sdcue");
				return _CvltIi_cSdcue;
			}else {
				return _CvltIi_cSdcue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_sdcue.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cSdcue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_sdcue",v);
		_CvltIi_cSdcue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CvltIi_cLdfree=null;

	/**
	 * @return Returns the CVLT_II/c_ldfree.
	 */
	public Integer getCvltIi_cLdfree() {
		try{
			if (_CvltIi_cLdfree==null){
				_CvltIi_cLdfree=getIntegerProperty("CVLT_II/c_ldfree");
				return _CvltIi_cLdfree;
			}else {
				return _CvltIi_cLdfree;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_ldfree.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cLdfree(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_ldfree",v);
		_CvltIi_cLdfree=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CvltIi_cLdcue=null;

	/**
	 * @return Returns the CVLT_II/c_ldcue.
	 */
	public Integer getCvltIi_cLdcue() {
		try{
			if (_CvltIi_cLdcue==null){
				_CvltIi_cLdcue=getIntegerProperty("CVLT_II/c_ldcue");
				return _CvltIi_cLdcue;
			}else {
				return _CvltIi_cLdcue;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_ldcue.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cLdcue(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_ldcue",v);
		_CvltIi_cLdcue=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CvltIi_cSemclu=null;

	/**
	 * @return Returns the CVLT_II/c_semclu.
	 */
	public Double getCvltIi_cSemclu() {
		try{
			if (_CvltIi_cSemclu==null){
				_CvltIi_cSemclu=getDoubleProperty("CVLT_II/c_semclu");
				return _CvltIi_cSemclu;
			}else {
				return _CvltIi_cSemclu;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_semclu.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cSemclu(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_semclu",v);
		_CvltIi_cSemclu=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CvltIi_cSerclu=null;

	/**
	 * @return Returns the CVLT_II/c_serclu.
	 */
	public Double getCvltIi_cSerclu() {
		try{
			if (_CvltIi_cSerclu==null){
				_CvltIi_cSerclu=getDoubleProperty("CVLT_II/c_serclu");
				return _CvltIi_cSerclu;
			}else {
				return _CvltIi_cSerclu;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_serclu.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cSerclu(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_serclu",v);
		_CvltIi_cSerclu=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CvltIi_cPrimac=null;

	/**
	 * @return Returns the CVLT_II/c_primac.
	 */
	public Double getCvltIi_cPrimac() {
		try{
			if (_CvltIi_cPrimac==null){
				_CvltIi_cPrimac=getDoubleProperty("CVLT_II/c_primac");
				return _CvltIi_cPrimac;
			}else {
				return _CvltIi_cPrimac;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_primac.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cPrimac(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_primac",v);
		_CvltIi_cPrimac=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CvltIi_cMiddle=null;

	/**
	 * @return Returns the CVLT_II/c_middle.
	 */
	public Double getCvltIi_cMiddle() {
		try{
			if (_CvltIi_cMiddle==null){
				_CvltIi_cMiddle=getDoubleProperty("CVLT_II/c_middle");
				return _CvltIi_cMiddle;
			}else {
				return _CvltIi_cMiddle;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_middle.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cMiddle(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_middle",v);
		_CvltIi_cMiddle=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CvltIi_cRecenc=null;

	/**
	 * @return Returns the CVLT_II/c_recenc.
	 */
	public Double getCvltIi_cRecenc() {
		try{
			if (_CvltIi_cRecenc==null){
				_CvltIi_cRecenc=getDoubleProperty("CVLT_II/c_recenc");
				return _CvltIi_cRecenc;
			}else {
				return _CvltIi_cRecenc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_recenc.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cRecenc(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_recenc",v);
		_CvltIi_cRecenc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CvltIi_cSlope=null;

	/**
	 * @return Returns the CVLT_II/c_slope.
	 */
	public Double getCvltIi_cSlope() {
		try{
			if (_CvltIi_cSlope==null){
				_CvltIi_cSlope=getDoubleProperty("CVLT_II/c_slope");
				return _CvltIi_cSlope;
			}else {
				return _CvltIi_cSlope;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_slope.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cSlope(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_slope",v);
		_CvltIi_cSlope=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CvltIi_cConsis=null;

	/**
	 * @return Returns the CVLT_II/c_consis.
	 */
	public Integer getCvltIi_cConsis() {
		try{
			if (_CvltIi_cConsis==null){
				_CvltIi_cConsis=getIntegerProperty("CVLT_II/c_consis");
				return _CvltIi_cConsis;
			}else {
				return _CvltIi_cConsis;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_consis.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cConsis(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_consis",v);
		_CvltIi_cConsis=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CvltIi_cProint=null;

	/**
	 * @return Returns the CVLT_II/c_proint.
	 */
	public Double getCvltIi_cProint() {
		try{
			if (_CvltIi_cProint==null){
				_CvltIi_cProint=getDoubleProperty("CVLT_II/c_proint");
				return _CvltIi_cProint;
			}else {
				return _CvltIi_cProint;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_proint.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cProint(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_proint",v);
		_CvltIi_cProint=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CvltIi_cRetroint=null;

	/**
	 * @return Returns the CVLT_II/c_retroint.
	 */
	public Double getCvltIi_cRetroint() {
		try{
			if (_CvltIi_cRetroint==null){
				_CvltIi_cRetroint=getDoubleProperty("CVLT_II/c_retroint");
				return _CvltIi_cRetroint;
			}else {
				return _CvltIi_cRetroint;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_retroint.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cRetroint(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_retroint",v);
		_CvltIi_cRetroint=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CvltIi_cPersev=null;

	/**
	 * @return Returns the CVLT_II/c_persev.
	 */
	public Integer getCvltIi_cPersev() {
		try{
			if (_CvltIi_cPersev==null){
				_CvltIi_cPersev=getIntegerProperty("CVLT_II/c_persev");
				return _CvltIi_cPersev;
			}else {
				return _CvltIi_cPersev;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_persev.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cPersev(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_persev",v);
		_CvltIi_cPersev=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CvltIi_cIntrus=null;

	/**
	 * @return Returns the CVLT_II/c_intrus.
	 */
	public Integer getCvltIi_cIntrus() {
		try{
			if (_CvltIi_cIntrus==null){
				_CvltIi_cIntrus=getIntegerProperty("CVLT_II/c_intrus");
				return _CvltIi_cIntrus;
			}else {
				return _CvltIi_cIntrus;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_intrus.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cIntrus(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_intrus",v);
		_CvltIi_cIntrus=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CvltIi_cTiri=null;

	/**
	 * @return Returns the CVLT_II/c_tiri.
	 */
	public Integer getCvltIi_cTiri() {
		try{
			if (_CvltIi_cTiri==null){
				_CvltIi_cTiri=getIntegerProperty("CVLT_II/c_tiri");
				return _CvltIi_cTiri;
			}else {
				return _CvltIi_cTiri;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_tiri.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cTiri(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_tiri",v);
		_CvltIi_cTiri=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CvltIi_cTdri=null;

	/**
	 * @return Returns the CVLT_II/c_tdri.
	 */
	public Integer getCvltIi_cTdri() {
		try{
			if (_CvltIi_cTdri==null){
				_CvltIi_cTdri=getIntegerProperty("CVLT_II/c_tdri");
				return _CvltIi_cTdri;
			}else {
				return _CvltIi_cTdri;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_tdri.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cTdri(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_tdri",v);
		_CvltIi_cTdri=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CvltIi_cTfri=null;

	/**
	 * @return Returns the CVLT_II/c_tfri.
	 */
	public Integer getCvltIi_cTfri() {
		try{
			if (_CvltIi_cTfri==null){
				_CvltIi_cTfri=getIntegerProperty("CVLT_II/c_tfri");
				return _CvltIi_cTfri;
			}else {
				return _CvltIi_cTfri;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_tfri.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cTfri(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_tfri",v);
		_CvltIi_cTfri=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CvltIi_cTcri=null;

	/**
	 * @return Returns the CVLT_II/c_tcri.
	 */
	public Integer getCvltIi_cTcri() {
		try{
			if (_CvltIi_cTcri==null){
				_CvltIi_cTcri=getIntegerProperty("CVLT_II/c_tcri");
				return _CvltIi_cTcri;
			}else {
				return _CvltIi_cTcri;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_tcri.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cTcri(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_tcri",v);
		_CvltIi_cTcri=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CvltIi_cTcnci=null;

	/**
	 * @return Returns the CVLT_II/c_tcnci.
	 */
	public Integer getCvltIi_cTcnci() {
		try{
			if (_CvltIi_cTcnci==null){
				_CvltIi_cTcnci=getIntegerProperty("CVLT_II/c_tcnci");
				return _CvltIi_cTcnci;
			}else {
				return _CvltIi_cTcnci;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_tcnci.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cTcnci(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_tcnci",v);
		_CvltIi_cTcnci=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CvltIi_cTssi=null;

	/**
	 * @return Returns the CVLT_II/c_tssi.
	 */
	public Integer getCvltIi_cTssi() {
		try{
			if (_CvltIi_cTssi==null){
				_CvltIi_cTssi=getIntegerProperty("CVLT_II/c_tssi");
				return _CvltIi_cTssi;
			}else {
				return _CvltIi_cTssi;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_tssi.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cTssi(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_tssi",v);
		_CvltIi_cTssi=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CvltIi_cTali=null;

	/**
	 * @return Returns the CVLT_II/c_tali.
	 */
	public Integer getCvltIi_cTali() {
		try{
			if (_CvltIi_cTali==null){
				_CvltIi_cTali=getIntegerProperty("CVLT_II/c_tali");
				return _CvltIi_cTali;
			}else {
				return _CvltIi_cTali;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_tali.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cTali(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_tali",v);
		_CvltIi_cTali=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CvltIi_cTrd=null;

	/**
	 * @return Returns the CVLT_II/c_trd.
	 */
	public Double getCvltIi_cTrd() {
		try{
			if (_CvltIi_cTrd==null){
				_CvltIi_cTrd=getDoubleProperty("CVLT_II/c_trd");
				return _CvltIi_cTrd;
			}else {
				return _CvltIi_cTrd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_trd.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cTrd(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_trd",v);
		_CvltIi_cTrd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CvltIi_cIrd=null;

	/**
	 * @return Returns the CVLT_II/c_ird.
	 */
	public Double getCvltIi_cIrd() {
		try{
			if (_CvltIi_cIrd==null){
				_CvltIi_cIrd=getDoubleProperty("CVLT_II/c_ird");
				return _CvltIi_cIrd;
			}else {
				return _CvltIi_cIrd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_ird.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cIrd(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_ird",v);
		_CvltIi_cIrd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CvltIi_cDrd=null;

	/**
	 * @return Returns the CVLT_II/c_drd.
	 */
	public Double getCvltIi_cDrd() {
		try{
			if (_CvltIi_cDrd==null){
				_CvltIi_cDrd=getDoubleProperty("CVLT_II/c_drd");
				return _CvltIi_cDrd;
			}else {
				return _CvltIi_cDrd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_drd.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cDrd(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_drd",v);
		_CvltIi_cDrd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CvltIi_cFrd=null;

	/**
	 * @return Returns the CVLT_II/c_frd.
	 */
	public Double getCvltIi_cFrd() {
		try{
			if (_CvltIi_cFrd==null){
				_CvltIi_cFrd=getDoubleProperty("CVLT_II/c_frd");
				return _CvltIi_cFrd;
			}else {
				return _CvltIi_cFrd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_frd.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cFrd(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_frd",v);
		_CvltIi_cFrd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CvltIi_cCrd=null;

	/**
	 * @return Returns the CVLT_II/c_crd.
	 */
	public Double getCvltIi_cCrd() {
		try{
			if (_CvltIi_cCrd==null){
				_CvltIi_cCrd=getDoubleProperty("CVLT_II/c_crd");
				return _CvltIi_cCrd;
			}else {
				return _CvltIi_cCrd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_crd.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cCrd(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_crd",v);
		_CvltIi_cCrd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CvltIi_cRechits=null;

	/**
	 * @return Returns the CVLT_II/c_rechits.
	 */
	public Integer getCvltIi_cRechits() {
		try{
			if (_CvltIi_cRechits==null){
				_CvltIi_cRechits=getIntegerProperty("CVLT_II/c_rechits");
				return _CvltIi_cRechits;
			}else {
				return _CvltIi_cRechits;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_rechits.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cRechits(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_rechits",v);
		_CvltIi_cRechits=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CvltIi_cRecfp=null;

	/**
	 * @return Returns the CVLT_II/c_recfp.
	 */
	public Double getCvltIi_cRecfp() {
		try{
			if (_CvltIi_cRecfp==null){
				_CvltIi_cRecfp=getDoubleProperty("CVLT_II/c_recfp");
				return _CvltIi_cRecfp;
			}else {
				return _CvltIi_cRecfp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_recfp.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cRecfp(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_recfp",v);
		_CvltIi_cRecfp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CvltIi_cRecdisc=null;

	/**
	 * @return Returns the CVLT_II/c_recdisc.
	 */
	public Double getCvltIi_cRecdisc() {
		try{
			if (_CvltIi_cRecdisc==null){
				_CvltIi_cRecdisc=getDoubleProperty("CVLT_II/c_recdisc");
				return _CvltIi_cRecdisc;
			}else {
				return _CvltIi_cRecdisc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_recdisc.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cRecdisc(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_recdisc",v);
		_CvltIi_cRecdisc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _CvltIi_cBias=null;

	/**
	 * @return Returns the CVLT_II/c_bias.
	 */
	public Double getCvltIi_cBias() {
		try{
			if (_CvltIi_cBias==null){
				_CvltIi_cBias=getDoubleProperty("CVLT_II/c_bias");
				return _CvltIi_cBias;
			}else {
				return _CvltIi_cBias;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVLT_II/c_bias.
	 * @param v Value to Set.
	 */
	public void setCvltIi_cBias(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVLT_II/c_bias",v);
		_CvltIi_cBias=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Temporalordermemory_tomRecall=null;

	/**
	 * @return Returns the temporalOrderMemory/TOM_recall.
	 */
	public Double getTemporalordermemory_tomRecall() {
		try{
			if (_Temporalordermemory_tomRecall==null){
				_Temporalordermemory_tomRecall=getDoubleProperty("temporalOrderMemory/TOM_recall");
				return _Temporalordermemory_tomRecall;
			}else {
				return _Temporalordermemory_tomRecall;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for temporalOrderMemory/TOM_recall.
	 * @param v Value to Set.
	 */
	public void setTemporalordermemory_tomRecall(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/temporalOrderMemory/TOM_recall",v);
		_Temporalordermemory_tomRecall=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Temporalordermemory_tomHits=null;

	/**
	 * @return Returns the temporalOrderMemory/TOM_hits.
	 */
	public Integer getTemporalordermemory_tomHits() {
		try{
			if (_Temporalordermemory_tomHits==null){
				_Temporalordermemory_tomHits=getIntegerProperty("temporalOrderMemory/TOM_hits");
				return _Temporalordermemory_tomHits;
			}else {
				return _Temporalordermemory_tomHits;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for temporalOrderMemory/TOM_hits.
	 * @param v Value to Set.
	 */
	public void setTemporalordermemory_tomHits(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/temporalOrderMemory/TOM_hits",v);
		_Temporalordermemory_tomHits=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Temporalordermemory_tomFa=null;

	/**
	 * @return Returns the temporalOrderMemory/TOM_fa.
	 */
	public Integer getTemporalordermemory_tomFa() {
		try{
			if (_Temporalordermemory_tomFa==null){
				_Temporalordermemory_tomFa=getIntegerProperty("temporalOrderMemory/TOM_fa");
				return _Temporalordermemory_tomFa;
			}else {
				return _Temporalordermemory_tomFa;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for temporalOrderMemory/TOM_fa.
	 * @param v Value to Set.
	 */
	public void setTemporalordermemory_tomFa(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/temporalOrderMemory/TOM_fa",v);
		_Temporalordermemory_tomFa=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Temporalordermemory_tomDiscrim=null;

	/**
	 * @return Returns the temporalOrderMemory/TOM_discrim.
	 */
	public Integer getTemporalordermemory_tomDiscrim() {
		try{
			if (_Temporalordermemory_tomDiscrim==null){
				_Temporalordermemory_tomDiscrim=getIntegerProperty("temporalOrderMemory/TOM_discrim");
				return _Temporalordermemory_tomDiscrim;
			}else {
				return _Temporalordermemory_tomDiscrim;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for temporalOrderMemory/TOM_discrim.
	 * @param v Value to Set.
	 */
	public void setTemporalordermemory_tomDiscrim(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/temporalOrderMemory/TOM_discrim",v);
		_Temporalordermemory_tomDiscrim=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _WmsIii_verbalpairedassociates_vpaIm=null;

	/**
	 * @return Returns the WMS_III/verbalPairedAssociates/vpa_im.
	 */
	public Integer getWmsIii_verbalpairedassociates_vpaIm() {
		try{
			if (_WmsIii_verbalpairedassociates_vpaIm==null){
				_WmsIii_verbalpairedassociates_vpaIm=getIntegerProperty("WMS_III/verbalPairedAssociates/vpa_im");
				return _WmsIii_verbalpairedassociates_vpaIm;
			}else {
				return _WmsIii_verbalpairedassociates_vpaIm;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WMS_III/verbalPairedAssociates/vpa_im.
	 * @param v Value to Set.
	 */
	public void setWmsIii_verbalpairedassociates_vpaIm(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WMS_III/verbalPairedAssociates/vpa_im",v);
		_WmsIii_verbalpairedassociates_vpaIm=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _WmsIii_verbalpairedassociates_vpaDel=null;

	/**
	 * @return Returns the WMS_III/verbalPairedAssociates/vpa_del.
	 */
	public Integer getWmsIii_verbalpairedassociates_vpaDel() {
		try{
			if (_WmsIii_verbalpairedassociates_vpaDel==null){
				_WmsIii_verbalpairedassociates_vpaDel=getIntegerProperty("WMS_III/verbalPairedAssociates/vpa_del");
				return _WmsIii_verbalpairedassociates_vpaDel;
			}else {
				return _WmsIii_verbalpairedassociates_vpaDel;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WMS_III/verbalPairedAssociates/vpa_del.
	 * @param v Value to Set.
	 */
	public void setWmsIii_verbalpairedassociates_vpaDel(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WMS_III/verbalPairedAssociates/vpa_del",v);
		_WmsIii_verbalpairedassociates_vpaDel=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _WmsIii_verbalpairedassociates_vpaRecog=null;

	/**
	 * @return Returns the WMS_III/verbalPairedAssociates/vpa_recog.
	 */
	public Integer getWmsIii_verbalpairedassociates_vpaRecog() {
		try{
			if (_WmsIii_verbalpairedassociates_vpaRecog==null){
				_WmsIii_verbalpairedassociates_vpaRecog=getIntegerProperty("WMS_III/verbalPairedAssociates/vpa_recog");
				return _WmsIii_verbalpairedassociates_vpaRecog;
			}else {
				return _WmsIii_verbalpairedassociates_vpaRecog;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WMS_III/verbalPairedAssociates/vpa_recog.
	 * @param v Value to Set.
	 */
	public void setWmsIii_verbalpairedassociates_vpaRecog(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WMS_III/verbalPairedAssociates/vpa_recog",v);
		_WmsIii_verbalpairedassociates_vpaRecog=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _WmsIii_logicalmemory_lmIm=null;

	/**
	 * @return Returns the WMS_III/logicalMemory/lm_im.
	 */
	public Integer getWmsIii_logicalmemory_lmIm() {
		try{
			if (_WmsIii_logicalmemory_lmIm==null){
				_WmsIii_logicalmemory_lmIm=getIntegerProperty("WMS_III/logicalMemory/lm_im");
				return _WmsIii_logicalmemory_lmIm;
			}else {
				return _WmsIii_logicalmemory_lmIm;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WMS_III/logicalMemory/lm_im.
	 * @param v Value to Set.
	 */
	public void setWmsIii_logicalmemory_lmIm(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WMS_III/logicalMemory/lm_im",v);
		_WmsIii_logicalmemory_lmIm=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _WmsIii_logicalmemory_lmDel=null;

	/**
	 * @return Returns the WMS_III/logicalMemory/lm_del.
	 */
	public Integer getWmsIii_logicalmemory_lmDel() {
		try{
			if (_WmsIii_logicalmemory_lmDel==null){
				_WmsIii_logicalmemory_lmDel=getIntegerProperty("WMS_III/logicalMemory/lm_del");
				return _WmsIii_logicalmemory_lmDel;
			}else {
				return _WmsIii_logicalmemory_lmDel;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WMS_III/logicalMemory/lm_del.
	 * @param v Value to Set.
	 */
	public void setWmsIii_logicalmemory_lmDel(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WMS_III/logicalMemory/lm_del",v);
		_WmsIii_logicalmemory_lmDel=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _WmsIii_logicalmemory_lmRecog=null;

	/**
	 * @return Returns the WMS_III/logicalMemory/lm_recog.
	 */
	public Integer getWmsIii_logicalmemory_lmRecog() {
		try{
			if (_WmsIii_logicalmemory_lmRecog==null){
				_WmsIii_logicalmemory_lmRecog=getIntegerProperty("WMS_III/logicalMemory/lm_recog");
				return _WmsIii_logicalmemory_lmRecog;
			}else {
				return _WmsIii_logicalmemory_lmRecog;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WMS_III/logicalMemory/lm_recog.
	 * @param v Value to Set.
	 */
	public void setWmsIii_logicalmemory_lmRecog(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WMS_III/logicalMemory/lm_recog",v);
		_WmsIii_logicalmemory_lmRecog=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _WmsIii_audRecDelay=null;

	/**
	 * @return Returns the WMS_III/aud_rec_delay.
	 */
	public Integer getWmsIii_audRecDelay() {
		try{
			if (_WmsIii_audRecDelay==null){
				_WmsIii_audRecDelay=getIntegerProperty("WMS_III/aud_rec_delay");
				return _WmsIii_audRecDelay;
			}else {
				return _WmsIii_audRecDelay;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WMS_III/aud_rec_delay.
	 * @param v Value to Set.
	 */
	public void setWmsIii_audRecDelay(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WMS_III/aud_rec_delay",v);
		_WmsIii_audRecDelay=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _WmsIii_lnseq=null;

	/**
	 * @return Returns the WMS_III/lnSeq.
	 */
	public Integer getWmsIii_lnseq() {
		try{
			if (_WmsIii_lnseq==null){
				_WmsIii_lnseq=getIntegerProperty("WMS_III/lnSeq");
				return _WmsIii_lnseq;
			}else {
				return _WmsIii_lnseq;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WMS_III/lnSeq.
	 * @param v Value to Set.
	 */
	public void setWmsIii_lnseq(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WMS_III/lnSeq",v);
		_WmsIii_lnseq=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Digsym=null;

	/**
	 * @return Returns the digSym.
	 */
	public Integer getDigsym() {
		try{
			if (_Digsym==null){
				_Digsym=getIntegerProperty("digSym");
				return _Digsym;
			}else {
				return _Digsym;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for digSym.
	 * @param v Value to Set.
	 */
	public void setDigsym(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/digSym",v);
		_Digsym=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Dotcomparison_dcomp1=null;

	/**
	 * @return Returns the dotComparison/dcomp1.
	 */
	public Integer getDotcomparison_dcomp1() {
		try{
			if (_Dotcomparison_dcomp1==null){
				_Dotcomparison_dcomp1=getIntegerProperty("dotComparison/dcomp1");
				return _Dotcomparison_dcomp1;
			}else {
				return _Dotcomparison_dcomp1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for dotComparison/dcomp1.
	 * @param v Value to Set.
	 */
	public void setDotcomparison_dcomp1(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/dotComparison/dcomp1",v);
		_Dotcomparison_dcomp1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Dotcomparison_dcomp2=null;

	/**
	 * @return Returns the dotComparison/dcomp2.
	 */
	public Integer getDotcomparison_dcomp2() {
		try{
			if (_Dotcomparison_dcomp2==null){
				_Dotcomparison_dcomp2=getIntegerProperty("dotComparison/dcomp2");
				return _Dotcomparison_dcomp2;
			}else {
				return _Dotcomparison_dcomp2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for dotComparison/dcomp2.
	 * @param v Value to Set.
	 */
	public void setDotcomparison_dcomp2(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/dotComparison/dcomp2",v);
		_Dotcomparison_dcomp2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Dotcomparison_dcomp3=null;

	/**
	 * @return Returns the dotComparison/dcomp3.
	 */
	public Integer getDotcomparison_dcomp3() {
		try{
			if (_Dotcomparison_dcomp3==null){
				_Dotcomparison_dcomp3=getIntegerProperty("dotComparison/dcomp3");
				return _Dotcomparison_dcomp3;
			}else {
				return _Dotcomparison_dcomp3;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for dotComparison/dcomp3.
	 * @param v Value to Set.
	 */
	public void setDotcomparison_dcomp3(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/dotComparison/dcomp3",v);
		_Dotcomparison_dcomp3=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Dotcomparison_dcompa=null;

	/**
	 * @return Returns the dotComparison/dcompa.
	 */
	public Double getDotcomparison_dcompa() {
		try{
			if (_Dotcomparison_dcompa==null){
				_Dotcomparison_dcompa=getDoubleProperty("dotComparison/dcompa");
				return _Dotcomparison_dcompa;
			}else {
				return _Dotcomparison_dcompa;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for dotComparison/dcompa.
	 * @param v Value to Set.
	 */
	public void setDotcomparison_dcompa(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/dotComparison/dcompa",v);
		_Dotcomparison_dcompa=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Trailmaking_tma=null;

	/**
	 * @return Returns the trailMaking/tma.
	 */
	public Integer getTrailmaking_tma() {
		try{
			if (_Trailmaking_tma==null){
				_Trailmaking_tma=getIntegerProperty("trailMaking/tma");
				return _Trailmaking_tma;
			}else {
				return _Trailmaking_tma;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for trailMaking/tma.
	 * @param v Value to Set.
	 */
	public void setTrailmaking_tma(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/trailMaking/tma",v);
		_Trailmaking_tma=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Trailmaking_tmb=null;

	/**
	 * @return Returns the trailMaking/tmb.
	 */
	public Integer getTrailmaking_tmb() {
		try{
			if (_Trailmaking_tmb==null){
				_Trailmaking_tmb=getIntegerProperty("trailMaking/tmb");
				return _Trailmaking_tmb;
			}else {
				return _Trailmaking_tmb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for trailMaking/tmb.
	 * @param v Value to Set.
	 */
	public void setTrailmaking_tmb(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/trailMaking/tmb",v);
		_Trailmaking_tmb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Buildmem=null;

	/**
	 * @return Returns the buildMem.
	 */
	public Integer getBuildmem() {
		try{
			if (_Buildmem==null){
				_Buildmem=getIntegerProperty("buildMem");
				return _Buildmem;
			}else {
				return _Buildmem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for buildMem.
	 * @param v Value to Set.
	 */
	public void setBuildmem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/buildMem",v);
		_Buildmem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Stroop_strpc=null;

	/**
	 * @return Returns the Stroop/strpc.
	 */
	public Integer getStroop_strpc() {
		try{
			if (_Stroop_strpc==null){
				_Stroop_strpc=getIntegerProperty("Stroop/strpc");
				return _Stroop_strpc;
			}else {
				return _Stroop_strpc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Stroop/strpc.
	 * @param v Value to Set.
	 */
	public void setStroop_strpc(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Stroop/strpc",v);
		_Stroop_strpc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Stroop_strpw=null;

	/**
	 * @return Returns the Stroop/strpw.
	 */
	public Integer getStroop_strpw() {
		try{
			if (_Stroop_strpw==null){
				_Stroop_strpw=getIntegerProperty("Stroop/strpw");
				return _Stroop_strpw;
			}else {
				return _Stroop_strpw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Stroop/strpw.
	 * @param v Value to Set.
	 */
	public void setStroop_strpw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Stroop/strpw",v);
		_Stroop_strpw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Stroop_strpcw=null;

	/**
	 * @return Returns the Stroop/strpcw.
	 */
	public Integer getStroop_strpcw() {
		try{
			if (_Stroop_strpcw==null){
				_Stroop_strpcw=getIntegerProperty("Stroop/strpcw");
				return _Stroop_strpcw;
			}else {
				return _Stroop_strpcw;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Stroop/strpcw.
	 * @param v Value to Set.
	 */
	public void setStroop_strpcw(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Stroop/strpcw",v);
		_Stroop_strpcw=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Stroop_strpint=null;

	/**
	 * @return Returns the Stroop/strpint.
	 */
	public Integer getStroop_strpint() {
		try{
			if (_Stroop_strpint==null){
				_Stroop_strpint=getIntegerProperty("Stroop/strpint");
				return _Stroop_strpint;
			}else {
				return _Stroop_strpint;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Stroop/strpint.
	 * @param v Value to Set.
	 */
	public void setStroop_strpint(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Stroop/strpint",v);
		_Stroop_strpint=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _PfA=null;

	/**
	 * @return Returns the pf_a.
	 */
	public Integer getPfA() {
		try{
			if (_PfA==null){
				_PfA=getIntegerProperty("pf_a");
				return _PfA;
			}else {
				return _PfA;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for pf_a.
	 * @param v Value to Set.
	 */
	public void setPfA(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/pf_a",v);
		_PfA=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _SfRound=null;

	/**
	 * @return Returns the sf_round.
	 */
	public Integer getSfRound() {
		try{
			if (_SfRound==null){
				_SfRound=getIntegerProperty("sf_round");
				return _SfRound;
			}else {
				return _SfRound;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sf_round.
	 * @param v Value to Set.
	 */
	public void setSfRound(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sf_round",v);
		_SfRound=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Shipley=null;

	/**
	 * @return Returns the Shipley.
	 */
	public Integer getShipley() {
		try{
			if (_Shipley==null){
				_Shipley=getIntegerProperty("Shipley");
				return _Shipley;
			}else {
				return _Shipley;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Shipley.
	 * @param v Value to Set.
	 */
	public void setShipley(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Shipley",v);
		_Shipley=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _WjiiiLwi=null;

	/**
	 * @return Returns the wjIII_lwi.
	 */
	public Integer getWjiiiLwi() {
		try{
			if (_WjiiiLwi==null){
				_WjiiiLwi=getIntegerProperty("wjIII_lwi");
				return _WjiiiLwi;
			}else {
				return _WjiiiLwi;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wjIII_lwi.
	 * @param v Value to Set.
	 */
	public void setWjiiiLwi(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wjIII_lwi",v);
		_WjiiiLwi=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wasi_wasiVocab=null;

	/**
	 * @return Returns the WASI/wasi_vocab.
	 */
	public Integer getWasi_wasiVocab() {
		try{
			if (_Wasi_wasiVocab==null){
				_Wasi_wasiVocab=getIntegerProperty("WASI/wasi_vocab");
				return _Wasi_wasiVocab;
			}else {
				return _Wasi_wasiVocab;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WASI/wasi_vocab.
	 * @param v Value to Set.
	 */
	public void setWasi_wasiVocab(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WASI/wasi_vocab",v);
		_Wasi_wasiVocab=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wasi_wasiMr=null;

	/**
	 * @return Returns the WASI/wasi_mr.
	 */
	public Integer getWasi_wasiMr() {
		try{
			if (_Wasi_wasiMr==null){
				_Wasi_wasiMr=getIntegerProperty("WASI/wasi_mr");
				return _Wasi_wasiMr;
			}else {
				return _Wasi_wasiMr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WASI/wasi_mr.
	 * @param v Value to Set.
	 */
	public void setWasi_wasiMr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WASI/wasi_mr",v);
		_Wasi_wasiMr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wasi_wasiSim=null;

	/**
	 * @return Returns the WASI/wasi_sim.
	 */
	public Integer getWasi_wasiSim() {
		try{
			if (_Wasi_wasiSim==null){
				_Wasi_wasiSim=getIntegerProperty("WASI/wasi_sim");
				return _Wasi_wasiSim;
			}else {
				return _Wasi_wasiSim;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WASI/wasi_sim.
	 * @param v Value to Set.
	 */
	public void setWasi_wasiSim(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WASI/wasi_sim",v);
		_Wasi_wasiSim=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wasi_wasiBd=null;

	/**
	 * @return Returns the WASI/wasi_bd.
	 */
	public Integer getWasi_wasiBd() {
		try{
			if (_Wasi_wasiBd==null){
				_Wasi_wasiBd=getIntegerProperty("WASI/wasi_bd");
				return _Wasi_wasiBd;
			}else {
				return _Wasi_wasiBd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WASI/wasi_bd.
	 * @param v Value to Set.
	 */
	public void setWasi_wasiBd(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WASI/wasi_bd",v);
		_Wasi_wasiBd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.Ls2Lsneuropsychdata> getAllLs2Lsneuropsychdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.Ls2Lsneuropsychdata> al = new ArrayList<org.nrg.xdat.om.Ls2Lsneuropsychdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.Ls2Lsneuropsychdata> getLs2LsneuropsychdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.Ls2Lsneuropsychdata> al = new ArrayList<org.nrg.xdat.om.Ls2Lsneuropsychdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.Ls2Lsneuropsychdata> getLs2LsneuropsychdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.Ls2Lsneuropsychdata> al = new ArrayList<org.nrg.xdat.om.Ls2Lsneuropsychdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static Ls2Lsneuropsychdata getLs2LsneuropsychdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("ls2:lsNeuropsychData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (Ls2Lsneuropsychdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
