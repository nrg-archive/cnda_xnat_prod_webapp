/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaLevelsdata extends XnatSubjectassessordata implements org.nrg.xdat.model.CndaLevelsdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaLevelsdata.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:levelsData";

	public AutoCndaLevelsdata(ItemI item)
	{
		super(item);
	}

	public AutoCndaLevelsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaLevelsdata(UserI user)
	 **/
	public AutoCndaLevelsdata(){}

	public AutoCndaLevelsdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:levelsData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Plasmald=null;

	/**
	 * @return Returns the plasmaLD.
	 */
	public Double getPlasmald() {
		try{
			if (_Plasmald==null){
				_Plasmald=getDoubleProperty("plasmaLD");
				return _Plasmald;
			}else {
				return _Plasmald;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for plasmaLD.
	 * @param v Value to Set.
	 */
	public void setPlasmald(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/plasmaLD",v);
		_Plasmald=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Plasmada=null;

	/**
	 * @return Returns the plasmaDA.
	 */
	public Double getPlasmada() {
		try{
			if (_Plasmada==null){
				_Plasmada=getDoubleProperty("plasmaDA");
				return _Plasmada;
			}else {
				return _Plasmada;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for plasmaDA.
	 * @param v Value to Set.
	 */
	public void setPlasmada(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/plasmaDA",v);
		_Plasmada=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Plasma3ome=null;

	/**
	 * @return Returns the plasma3OMe.
	 */
	public Double getPlasma3ome() {
		try{
			if (_Plasma3ome==null){
				_Plasma3ome=getDoubleProperty("plasma3OMe");
				return _Plasma3ome;
			}else {
				return _Plasma3ome;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for plasma3OMe.
	 * @param v Value to Set.
	 */
	public void setPlasma3ome(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/plasma3OMe",v);
		_Plasma3ome=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Plasmadc=null;

	/**
	 * @return Returns the plasmaDC.
	 */
	public Double getPlasmadc() {
		try{
			if (_Plasmadc==null){
				_Plasmadc=getDoubleProperty("plasmaDC");
				return _Plasmadc;
			}else {
				return _Plasmadc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for plasmaDC.
	 * @param v Value to Set.
	 */
	public void setPlasmadc(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/plasmaDC",v);
		_Plasmadc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Plasmacd=null;

	/**
	 * @return Returns the plasmaCD.
	 */
	public Double getPlasmacd() {
		try{
			if (_Plasmacd==null){
				_Plasmacd=getDoubleProperty("plasmaCD");
				return _Plasmacd;
			}else {
				return _Plasmacd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for plasmaCD.
	 * @param v Value to Set.
	 */
	public void setPlasmacd(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/plasmaCD",v);
		_Plasmacd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Problem=null;

	/**
	 * @return Returns the problem.
	 */
	public Boolean getProblem() {
		try{
			if (_Problem==null){
				_Problem=getBooleanProperty("problem");
				return _Problem;
			}else {
				return _Problem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for problem.
	 * @param v Value to Set.
	 */
	public void setProblem(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/problem",v);
		_Problem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Questionableld=null;

	/**
	 * @return Returns the questionableLD.
	 */
	public Boolean getQuestionableld() {
		try{
			if (_Questionableld==null){
				_Questionableld=getBooleanProperty("questionableLD");
				return _Questionableld;
			}else {
				return _Questionableld;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for questionableLD.
	 * @param v Value to Set.
	 */
	public void setQuestionableld(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/questionableLD",v);
		_Questionableld=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Questionableda=null;

	/**
	 * @return Returns the questionableDA.
	 */
	public Boolean getQuestionableda() {
		try{
			if (_Questionableda==null){
				_Questionableda=getBooleanProperty("questionableDA");
				return _Questionableda;
			}else {
				return _Questionableda;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for questionableDA.
	 * @param v Value to Set.
	 */
	public void setQuestionableda(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/questionableDA",v);
		_Questionableda=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Questionable3ome=null;

	/**
	 * @return Returns the questionable3OMe.
	 */
	public Boolean getQuestionable3ome() {
		try{
			if (_Questionable3ome==null){
				_Questionable3ome=getBooleanProperty("questionable3OMe");
				return _Questionable3ome;
			}else {
				return _Questionable3ome;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for questionable3OMe.
	 * @param v Value to Set.
	 */
	public void setQuestionable3ome(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/questionable3OMe",v);
		_Questionable3ome=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Questionabledc=null;

	/**
	 * @return Returns the questionableDC.
	 */
	public Boolean getQuestionabledc() {
		try{
			if (_Questionabledc==null){
				_Questionabledc=getBooleanProperty("questionableDC");
				return _Questionabledc;
			}else {
				return _Questionabledc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for questionableDC.
	 * @param v Value to Set.
	 */
	public void setQuestionabledc(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/questionableDC",v);
		_Questionabledc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Questionablecd=null;

	/**
	 * @return Returns the questionableCD.
	 */
	public Boolean getQuestionablecd() {
		try{
			if (_Questionablecd==null){
				_Questionablecd=getBooleanProperty("questionableCD");
				return _Questionablecd;
			}else {
				return _Questionablecd;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for questionableCD.
	 * @param v Value to Set.
	 */
	public void setQuestionablecd(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/questionableCD",v);
		_Questionablecd=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Knownforivld1=null;

	/**
	 * @return Returns the knownForIVLD1.
	 */
	public Boolean getKnownforivld1() {
		try{
			if (_Knownforivld1==null){
				_Knownforivld1=getBooleanProperty("knownForIVLD1");
				return _Knownforivld1;
			}else {
				return _Knownforivld1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for knownForIVLD1.
	 * @param v Value to Set.
	 */
	public void setKnownforivld1(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/knownForIVLD1",v);
		_Knownforivld1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Rejectedforivld1=null;

	/**
	 * @return Returns the rejectedForIVLD1.
	 */
	public Boolean getRejectedforivld1() {
		try{
			if (_Rejectedforivld1==null){
				_Rejectedforivld1=getBooleanProperty("rejectedForIVLD1");
				return _Rejectedforivld1;
			}else {
				return _Rejectedforivld1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for rejectedForIVLD1.
	 * @param v Value to Set.
	 */
	public void setRejectedforivld1(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/rejectedForIVLD1",v);
		_Rejectedforivld1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaLevelsdata> getAllCndaLevelsdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaLevelsdata> al = new ArrayList<org.nrg.xdat.om.CndaLevelsdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaLevelsdata> getCndaLevelsdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaLevelsdata> al = new ArrayList<org.nrg.xdat.om.CndaLevelsdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaLevelsdata> getCndaLevelsdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaLevelsdata> al = new ArrayList<org.nrg.xdat.om.CndaLevelsdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaLevelsdata getCndaLevelsdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:levelsData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaLevelsdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
