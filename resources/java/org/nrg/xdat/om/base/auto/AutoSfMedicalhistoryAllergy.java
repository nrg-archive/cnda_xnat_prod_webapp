/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoSfMedicalhistoryAllergy extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.SfMedicalhistoryAllergyI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoSfMedicalhistoryAllergy.class);
	public static String SCHEMA_ELEMENT_NAME="sf:medicalHistory_allergy";

	public AutoSfMedicalhistoryAllergy(ItemI item)
	{
		super(item);
	}

	public AutoSfMedicalhistoryAllergy(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoSfMedicalhistoryAllergy(UserI user)
	 **/
	public AutoSfMedicalhistoryAllergy(){}

	public AutoSfMedicalhistoryAllergy(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "sf:medicalHistory_allergy";
	}

	//FIELD

	private String _Allergy=null;

	/**
	 * @return Returns the allergy.
	 */
	public String getAllergy(){
		try{
			if (_Allergy==null){
				_Allergy=getStringProperty("allergy");
				return _Allergy;
			}else {
				return _Allergy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for allergy.
	 * @param v Value to Set.
	 */
	public void setAllergy(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/allergy",v);
		_Allergy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _SfMedicalhistoryAllergyId=null;

	/**
	 * @return Returns the sf_medicalHistory_allergy_id.
	 */
	public Integer getSfMedicalhistoryAllergyId() {
		try{
			if (_SfMedicalhistoryAllergyId==null){
				_SfMedicalhistoryAllergyId=getIntegerProperty("sf_medicalHistory_allergy_id");
				return _SfMedicalhistoryAllergyId;
			}else {
				return _SfMedicalhistoryAllergyId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sf_medicalHistory_allergy_id.
	 * @param v Value to Set.
	 */
	public void setSfMedicalhistoryAllergyId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sf_medicalHistory_allergy_id",v);
		_SfMedicalhistoryAllergyId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.SfMedicalhistoryAllergy> getAllSfMedicalhistoryAllergys(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfMedicalhistoryAllergy> al = new ArrayList<org.nrg.xdat.om.SfMedicalhistoryAllergy>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.SfMedicalhistoryAllergy> getSfMedicalhistoryAllergysByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfMedicalhistoryAllergy> al = new ArrayList<org.nrg.xdat.om.SfMedicalhistoryAllergy>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.SfMedicalhistoryAllergy> getSfMedicalhistoryAllergysByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfMedicalhistoryAllergy> al = new ArrayList<org.nrg.xdat.om.SfMedicalhistoryAllergy>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static SfMedicalhistoryAllergy getSfMedicalhistoryAllergysBySfMedicalhistoryAllergyId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("sf:medicalHistory_allergy/sf_medicalHistory_allergy_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (SfMedicalhistoryAllergy) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
