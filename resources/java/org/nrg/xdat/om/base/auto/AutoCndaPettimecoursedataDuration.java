/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:59 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaPettimecoursedataDuration extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.CndaPettimecoursedataDurationI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaPettimecoursedataDuration.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:petTimeCourseData_duration";

	public AutoCndaPettimecoursedataDuration(ItemI item)
	{
		super(item);
	}

	public AutoCndaPettimecoursedataDuration(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaPettimecoursedataDuration(UserI user)
	 **/
	public AutoCndaPettimecoursedataDuration(){}

	public AutoCndaPettimecoursedataDuration(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:petTimeCourseData_duration";
	}
	 private ArrayList<org.nrg.xdat.om.CndaPettimecoursedataDurationBp> _Bp =null;

	/**
	 * bp
	 * @return Returns an List of org.nrg.xdat.om.CndaPettimecoursedataDurationBp
	 */
	public <A extends org.nrg.xdat.model.CndaPettimecoursedataDurationBpI> List<A> getBp() {
		try{
			if (_Bp==null){
				_Bp=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("bp"));
			}
			return (List<A>) _Bp;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.CndaPettimecoursedataDurationBp>();}
	}

	/**
	 * Sets the value for bp.
	 * @param v Value to Set.
	 */
	public void setBp(ItemI v) throws Exception{
		_Bp =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/bp",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/bp",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * bp
	 * Adds org.nrg.xdat.model.CndaPettimecoursedataDurationBpI
	 */
	public <A extends org.nrg.xdat.model.CndaPettimecoursedataDurationBpI> void addBp(A item) throws Exception{
	setBp((ItemI)item);
	}

	/**
	 * Removes the bp of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeBp(int index) throws java.lang.IndexOutOfBoundsException {
		_Bp =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/bp",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Span=null;

	/**
	 * @return Returns the span.
	 */
	public String getSpan(){
		try{
			if (_Span==null){
				_Span=getStringProperty("span");
				return _Span;
			}else {
				return _Span;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for span.
	 * @param v Value to Set.
	 */
	public void setSpan(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/span",v);
		_Span=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CndaPettimecoursedataDurationId=null;

	/**
	 * @return Returns the cnda_petTimeCourseData_duration_id.
	 */
	public Integer getCndaPettimecoursedataDurationId() {
		try{
			if (_CndaPettimecoursedataDurationId==null){
				_CndaPettimecoursedataDurationId=getIntegerProperty("cnda_petTimeCourseData_duration_id");
				return _CndaPettimecoursedataDurationId;
			}else {
				return _CndaPettimecoursedataDurationId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda_petTimeCourseData_duration_id.
	 * @param v Value to Set.
	 */
	public void setCndaPettimecoursedataDurationId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/cnda_petTimeCourseData_duration_id",v);
		_CndaPettimecoursedataDurationId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaPettimecoursedataDuration> getAllCndaPettimecoursedataDurations(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaPettimecoursedataDuration> al = new ArrayList<org.nrg.xdat.om.CndaPettimecoursedataDuration>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaPettimecoursedataDuration> getCndaPettimecoursedataDurationsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaPettimecoursedataDuration> al = new ArrayList<org.nrg.xdat.om.CndaPettimecoursedataDuration>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaPettimecoursedataDuration> getCndaPettimecoursedataDurationsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaPettimecoursedataDuration> al = new ArrayList<org.nrg.xdat.om.CndaPettimecoursedataDuration>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaPettimecoursedataDuration getCndaPettimecoursedataDurationsByCndaPettimecoursedataDurationId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:petTimeCourseData_duration/cnda_petTimeCourseData_duration_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaPettimecoursedataDuration) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //bp
	        for(org.nrg.xdat.model.CndaPettimecoursedataDurationBpI childBp : this.getBp()){
	            if (childBp!=null){
	              for(ResourceFile rf: ((CndaPettimecoursedataDurationBp)childBp).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("bp[" + ((CndaPettimecoursedataDurationBp)childBp).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("bp/" + ((CndaPettimecoursedataDurationBp)childBp).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
