/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCbatSimon extends XnatSubjectassessordata implements org.nrg.xdat.model.CbatSimonI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCbatSimon.class);
	public static String SCHEMA_ELEMENT_NAME="cbat:simon";

	public AutoCbatSimon(ItemI item)
	{
		super(item);
	}

	public AutoCbatSimon(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCbatSimon(UserI user)
	 **/
	public AutoCbatSimon(){}

	public AutoCbatSimon(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cbat:simon";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T1_accuracy=null;

	/**
	 * @return Returns the T1/accuracy.
	 */
	public Integer getT1_accuracy() {
		try{
			if (_T1_accuracy==null){
				_T1_accuracy=getIntegerProperty("T1/accuracy");
				return _T1_accuracy;
			}else {
				return _T1_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T1/accuracy.
	 * @param v Value to Set.
	 */
	public void setT1_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T1/accuracy",v);
		_T1_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T1_responsetime=null;

	/**
	 * @return Returns the T1/responseTime.
	 */
	public Integer getT1_responsetime() {
		try{
			if (_T1_responsetime==null){
				_T1_responsetime=getIntegerProperty("T1/responseTime");
				return _T1_responsetime;
			}else {
				return _T1_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T1/responseTime.
	 * @param v Value to Set.
	 */
	public void setT1_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T1/responseTime",v);
		_T1_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T1_congruency=null;

	/**
	 * @return Returns the T1/congruency.
	 */
	public String getT1_congruency(){
		try{
			if (_T1_congruency==null){
				_T1_congruency=getStringProperty("T1/congruency");
				return _T1_congruency;
			}else {
				return _T1_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T1/congruency.
	 * @param v Value to Set.
	 */
	public void setT1_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T1/congruency",v);
		_T1_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T2_accuracy=null;

	/**
	 * @return Returns the T2/accuracy.
	 */
	public Integer getT2_accuracy() {
		try{
			if (_T2_accuracy==null){
				_T2_accuracy=getIntegerProperty("T2/accuracy");
				return _T2_accuracy;
			}else {
				return _T2_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T2/accuracy.
	 * @param v Value to Set.
	 */
	public void setT2_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T2/accuracy",v);
		_T2_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T2_responsetime=null;

	/**
	 * @return Returns the T2/responseTime.
	 */
	public Integer getT2_responsetime() {
		try{
			if (_T2_responsetime==null){
				_T2_responsetime=getIntegerProperty("T2/responseTime");
				return _T2_responsetime;
			}else {
				return _T2_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T2/responseTime.
	 * @param v Value to Set.
	 */
	public void setT2_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T2/responseTime",v);
		_T2_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T2_congruency=null;

	/**
	 * @return Returns the T2/congruency.
	 */
	public String getT2_congruency(){
		try{
			if (_T2_congruency==null){
				_T2_congruency=getStringProperty("T2/congruency");
				return _T2_congruency;
			}else {
				return _T2_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T2/congruency.
	 * @param v Value to Set.
	 */
	public void setT2_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T2/congruency",v);
		_T2_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T3_accuracy=null;

	/**
	 * @return Returns the T3/accuracy.
	 */
	public Integer getT3_accuracy() {
		try{
			if (_T3_accuracy==null){
				_T3_accuracy=getIntegerProperty("T3/accuracy");
				return _T3_accuracy;
			}else {
				return _T3_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T3/accuracy.
	 * @param v Value to Set.
	 */
	public void setT3_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T3/accuracy",v);
		_T3_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T3_responsetime=null;

	/**
	 * @return Returns the T3/responseTime.
	 */
	public Integer getT3_responsetime() {
		try{
			if (_T3_responsetime==null){
				_T3_responsetime=getIntegerProperty("T3/responseTime");
				return _T3_responsetime;
			}else {
				return _T3_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T3/responseTime.
	 * @param v Value to Set.
	 */
	public void setT3_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T3/responseTime",v);
		_T3_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T3_congruency=null;

	/**
	 * @return Returns the T3/congruency.
	 */
	public String getT3_congruency(){
		try{
			if (_T3_congruency==null){
				_T3_congruency=getStringProperty("T3/congruency");
				return _T3_congruency;
			}else {
				return _T3_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T3/congruency.
	 * @param v Value to Set.
	 */
	public void setT3_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T3/congruency",v);
		_T3_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T4_accuracy=null;

	/**
	 * @return Returns the T4/accuracy.
	 */
	public Integer getT4_accuracy() {
		try{
			if (_T4_accuracy==null){
				_T4_accuracy=getIntegerProperty("T4/accuracy");
				return _T4_accuracy;
			}else {
				return _T4_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T4/accuracy.
	 * @param v Value to Set.
	 */
	public void setT4_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T4/accuracy",v);
		_T4_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T4_responsetime=null;

	/**
	 * @return Returns the T4/responseTime.
	 */
	public Integer getT4_responsetime() {
		try{
			if (_T4_responsetime==null){
				_T4_responsetime=getIntegerProperty("T4/responseTime");
				return _T4_responsetime;
			}else {
				return _T4_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T4/responseTime.
	 * @param v Value to Set.
	 */
	public void setT4_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T4/responseTime",v);
		_T4_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T4_congruency=null;

	/**
	 * @return Returns the T4/congruency.
	 */
	public String getT4_congruency(){
		try{
			if (_T4_congruency==null){
				_T4_congruency=getStringProperty("T4/congruency");
				return _T4_congruency;
			}else {
				return _T4_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T4/congruency.
	 * @param v Value to Set.
	 */
	public void setT4_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T4/congruency",v);
		_T4_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T5_accuracy=null;

	/**
	 * @return Returns the T5/accuracy.
	 */
	public Integer getT5_accuracy() {
		try{
			if (_T5_accuracy==null){
				_T5_accuracy=getIntegerProperty("T5/accuracy");
				return _T5_accuracy;
			}else {
				return _T5_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T5/accuracy.
	 * @param v Value to Set.
	 */
	public void setT5_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T5/accuracy",v);
		_T5_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T5_responsetime=null;

	/**
	 * @return Returns the T5/responseTime.
	 */
	public Integer getT5_responsetime() {
		try{
			if (_T5_responsetime==null){
				_T5_responsetime=getIntegerProperty("T5/responseTime");
				return _T5_responsetime;
			}else {
				return _T5_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T5/responseTime.
	 * @param v Value to Set.
	 */
	public void setT5_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T5/responseTime",v);
		_T5_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T5_congruency=null;

	/**
	 * @return Returns the T5/congruency.
	 */
	public String getT5_congruency(){
		try{
			if (_T5_congruency==null){
				_T5_congruency=getStringProperty("T5/congruency");
				return _T5_congruency;
			}else {
				return _T5_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T5/congruency.
	 * @param v Value to Set.
	 */
	public void setT5_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T5/congruency",v);
		_T5_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T6_accuracy=null;

	/**
	 * @return Returns the T6/accuracy.
	 */
	public Integer getT6_accuracy() {
		try{
			if (_T6_accuracy==null){
				_T6_accuracy=getIntegerProperty("T6/accuracy");
				return _T6_accuracy;
			}else {
				return _T6_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T6/accuracy.
	 * @param v Value to Set.
	 */
	public void setT6_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T6/accuracy",v);
		_T6_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T6_responsetime=null;

	/**
	 * @return Returns the T6/responseTime.
	 */
	public Integer getT6_responsetime() {
		try{
			if (_T6_responsetime==null){
				_T6_responsetime=getIntegerProperty("T6/responseTime");
				return _T6_responsetime;
			}else {
				return _T6_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T6/responseTime.
	 * @param v Value to Set.
	 */
	public void setT6_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T6/responseTime",v);
		_T6_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T6_congruency=null;

	/**
	 * @return Returns the T6/congruency.
	 */
	public String getT6_congruency(){
		try{
			if (_T6_congruency==null){
				_T6_congruency=getStringProperty("T6/congruency");
				return _T6_congruency;
			}else {
				return _T6_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T6/congruency.
	 * @param v Value to Set.
	 */
	public void setT6_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T6/congruency",v);
		_T6_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T7_accuracy=null;

	/**
	 * @return Returns the T7/accuracy.
	 */
	public Integer getT7_accuracy() {
		try{
			if (_T7_accuracy==null){
				_T7_accuracy=getIntegerProperty("T7/accuracy");
				return _T7_accuracy;
			}else {
				return _T7_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T7/accuracy.
	 * @param v Value to Set.
	 */
	public void setT7_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T7/accuracy",v);
		_T7_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T7_responsetime=null;

	/**
	 * @return Returns the T7/responseTime.
	 */
	public Integer getT7_responsetime() {
		try{
			if (_T7_responsetime==null){
				_T7_responsetime=getIntegerProperty("T7/responseTime");
				return _T7_responsetime;
			}else {
				return _T7_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T7/responseTime.
	 * @param v Value to Set.
	 */
	public void setT7_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T7/responseTime",v);
		_T7_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T7_congruency=null;

	/**
	 * @return Returns the T7/congruency.
	 */
	public String getT7_congruency(){
		try{
			if (_T7_congruency==null){
				_T7_congruency=getStringProperty("T7/congruency");
				return _T7_congruency;
			}else {
				return _T7_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T7/congruency.
	 * @param v Value to Set.
	 */
	public void setT7_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T7/congruency",v);
		_T7_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T8_accuracy=null;

	/**
	 * @return Returns the T8/accuracy.
	 */
	public Integer getT8_accuracy() {
		try{
			if (_T8_accuracy==null){
				_T8_accuracy=getIntegerProperty("T8/accuracy");
				return _T8_accuracy;
			}else {
				return _T8_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T8/accuracy.
	 * @param v Value to Set.
	 */
	public void setT8_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T8/accuracy",v);
		_T8_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T8_responsetime=null;

	/**
	 * @return Returns the T8/responseTime.
	 */
	public Integer getT8_responsetime() {
		try{
			if (_T8_responsetime==null){
				_T8_responsetime=getIntegerProperty("T8/responseTime");
				return _T8_responsetime;
			}else {
				return _T8_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T8/responseTime.
	 * @param v Value to Set.
	 */
	public void setT8_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T8/responseTime",v);
		_T8_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T8_congruency=null;

	/**
	 * @return Returns the T8/congruency.
	 */
	public String getT8_congruency(){
		try{
			if (_T8_congruency==null){
				_T8_congruency=getStringProperty("T8/congruency");
				return _T8_congruency;
			}else {
				return _T8_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T8/congruency.
	 * @param v Value to Set.
	 */
	public void setT8_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T8/congruency",v);
		_T8_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T9_accuracy=null;

	/**
	 * @return Returns the T9/accuracy.
	 */
	public Integer getT9_accuracy() {
		try{
			if (_T9_accuracy==null){
				_T9_accuracy=getIntegerProperty("T9/accuracy");
				return _T9_accuracy;
			}else {
				return _T9_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T9/accuracy.
	 * @param v Value to Set.
	 */
	public void setT9_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T9/accuracy",v);
		_T9_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T9_responsetime=null;

	/**
	 * @return Returns the T9/responseTime.
	 */
	public Integer getT9_responsetime() {
		try{
			if (_T9_responsetime==null){
				_T9_responsetime=getIntegerProperty("T9/responseTime");
				return _T9_responsetime;
			}else {
				return _T9_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T9/responseTime.
	 * @param v Value to Set.
	 */
	public void setT9_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T9/responseTime",v);
		_T9_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T9_congruency=null;

	/**
	 * @return Returns the T9/congruency.
	 */
	public String getT9_congruency(){
		try{
			if (_T9_congruency==null){
				_T9_congruency=getStringProperty("T9/congruency");
				return _T9_congruency;
			}else {
				return _T9_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T9/congruency.
	 * @param v Value to Set.
	 */
	public void setT9_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T9/congruency",v);
		_T9_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T10_accuracy=null;

	/**
	 * @return Returns the T10/accuracy.
	 */
	public Integer getT10_accuracy() {
		try{
			if (_T10_accuracy==null){
				_T10_accuracy=getIntegerProperty("T10/accuracy");
				return _T10_accuracy;
			}else {
				return _T10_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T10/accuracy.
	 * @param v Value to Set.
	 */
	public void setT10_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T10/accuracy",v);
		_T10_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T10_responsetime=null;

	/**
	 * @return Returns the T10/responseTime.
	 */
	public Integer getT10_responsetime() {
		try{
			if (_T10_responsetime==null){
				_T10_responsetime=getIntegerProperty("T10/responseTime");
				return _T10_responsetime;
			}else {
				return _T10_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T10/responseTime.
	 * @param v Value to Set.
	 */
	public void setT10_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T10/responseTime",v);
		_T10_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T10_congruency=null;

	/**
	 * @return Returns the T10/congruency.
	 */
	public String getT10_congruency(){
		try{
			if (_T10_congruency==null){
				_T10_congruency=getStringProperty("T10/congruency");
				return _T10_congruency;
			}else {
				return _T10_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T10/congruency.
	 * @param v Value to Set.
	 */
	public void setT10_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T10/congruency",v);
		_T10_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T11_accuracy=null;

	/**
	 * @return Returns the T11/accuracy.
	 */
	public Integer getT11_accuracy() {
		try{
			if (_T11_accuracy==null){
				_T11_accuracy=getIntegerProperty("T11/accuracy");
				return _T11_accuracy;
			}else {
				return _T11_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T11/accuracy.
	 * @param v Value to Set.
	 */
	public void setT11_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T11/accuracy",v);
		_T11_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T11_responsetime=null;

	/**
	 * @return Returns the T11/responseTime.
	 */
	public Integer getT11_responsetime() {
		try{
			if (_T11_responsetime==null){
				_T11_responsetime=getIntegerProperty("T11/responseTime");
				return _T11_responsetime;
			}else {
				return _T11_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T11/responseTime.
	 * @param v Value to Set.
	 */
	public void setT11_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T11/responseTime",v);
		_T11_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T11_congruency=null;

	/**
	 * @return Returns the T11/congruency.
	 */
	public String getT11_congruency(){
		try{
			if (_T11_congruency==null){
				_T11_congruency=getStringProperty("T11/congruency");
				return _T11_congruency;
			}else {
				return _T11_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T11/congruency.
	 * @param v Value to Set.
	 */
	public void setT11_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T11/congruency",v);
		_T11_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T12_accuracy=null;

	/**
	 * @return Returns the T12/accuracy.
	 */
	public Integer getT12_accuracy() {
		try{
			if (_T12_accuracy==null){
				_T12_accuracy=getIntegerProperty("T12/accuracy");
				return _T12_accuracy;
			}else {
				return _T12_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T12/accuracy.
	 * @param v Value to Set.
	 */
	public void setT12_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T12/accuracy",v);
		_T12_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T12_responsetime=null;

	/**
	 * @return Returns the T12/responseTime.
	 */
	public Integer getT12_responsetime() {
		try{
			if (_T12_responsetime==null){
				_T12_responsetime=getIntegerProperty("T12/responseTime");
				return _T12_responsetime;
			}else {
				return _T12_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T12/responseTime.
	 * @param v Value to Set.
	 */
	public void setT12_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T12/responseTime",v);
		_T12_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T12_congruency=null;

	/**
	 * @return Returns the T12/congruency.
	 */
	public String getT12_congruency(){
		try{
			if (_T12_congruency==null){
				_T12_congruency=getStringProperty("T12/congruency");
				return _T12_congruency;
			}else {
				return _T12_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T12/congruency.
	 * @param v Value to Set.
	 */
	public void setT12_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T12/congruency",v);
		_T12_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T13_accuracy=null;

	/**
	 * @return Returns the T13/accuracy.
	 */
	public Integer getT13_accuracy() {
		try{
			if (_T13_accuracy==null){
				_T13_accuracy=getIntegerProperty("T13/accuracy");
				return _T13_accuracy;
			}else {
				return _T13_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T13/accuracy.
	 * @param v Value to Set.
	 */
	public void setT13_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T13/accuracy",v);
		_T13_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T13_responsetime=null;

	/**
	 * @return Returns the T13/responseTime.
	 */
	public Integer getT13_responsetime() {
		try{
			if (_T13_responsetime==null){
				_T13_responsetime=getIntegerProperty("T13/responseTime");
				return _T13_responsetime;
			}else {
				return _T13_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T13/responseTime.
	 * @param v Value to Set.
	 */
	public void setT13_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T13/responseTime",v);
		_T13_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T13_congruency=null;

	/**
	 * @return Returns the T13/congruency.
	 */
	public String getT13_congruency(){
		try{
			if (_T13_congruency==null){
				_T13_congruency=getStringProperty("T13/congruency");
				return _T13_congruency;
			}else {
				return _T13_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T13/congruency.
	 * @param v Value to Set.
	 */
	public void setT13_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T13/congruency",v);
		_T13_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T14_accuracy=null;

	/**
	 * @return Returns the T14/accuracy.
	 */
	public Integer getT14_accuracy() {
		try{
			if (_T14_accuracy==null){
				_T14_accuracy=getIntegerProperty("T14/accuracy");
				return _T14_accuracy;
			}else {
				return _T14_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T14/accuracy.
	 * @param v Value to Set.
	 */
	public void setT14_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T14/accuracy",v);
		_T14_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T14_responsetime=null;

	/**
	 * @return Returns the T14/responseTime.
	 */
	public Integer getT14_responsetime() {
		try{
			if (_T14_responsetime==null){
				_T14_responsetime=getIntegerProperty("T14/responseTime");
				return _T14_responsetime;
			}else {
				return _T14_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T14/responseTime.
	 * @param v Value to Set.
	 */
	public void setT14_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T14/responseTime",v);
		_T14_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T14_congruency=null;

	/**
	 * @return Returns the T14/congruency.
	 */
	public String getT14_congruency(){
		try{
			if (_T14_congruency==null){
				_T14_congruency=getStringProperty("T14/congruency");
				return _T14_congruency;
			}else {
				return _T14_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T14/congruency.
	 * @param v Value to Set.
	 */
	public void setT14_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T14/congruency",v);
		_T14_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T15_accuracy=null;

	/**
	 * @return Returns the T15/accuracy.
	 */
	public Integer getT15_accuracy() {
		try{
			if (_T15_accuracy==null){
				_T15_accuracy=getIntegerProperty("T15/accuracy");
				return _T15_accuracy;
			}else {
				return _T15_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T15/accuracy.
	 * @param v Value to Set.
	 */
	public void setT15_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T15/accuracy",v);
		_T15_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T15_responsetime=null;

	/**
	 * @return Returns the T15/responseTime.
	 */
	public Integer getT15_responsetime() {
		try{
			if (_T15_responsetime==null){
				_T15_responsetime=getIntegerProperty("T15/responseTime");
				return _T15_responsetime;
			}else {
				return _T15_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T15/responseTime.
	 * @param v Value to Set.
	 */
	public void setT15_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T15/responseTime",v);
		_T15_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T15_congruency=null;

	/**
	 * @return Returns the T15/congruency.
	 */
	public String getT15_congruency(){
		try{
			if (_T15_congruency==null){
				_T15_congruency=getStringProperty("T15/congruency");
				return _T15_congruency;
			}else {
				return _T15_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T15/congruency.
	 * @param v Value to Set.
	 */
	public void setT15_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T15/congruency",v);
		_T15_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T16_accuracy=null;

	/**
	 * @return Returns the T16/accuracy.
	 */
	public Integer getT16_accuracy() {
		try{
			if (_T16_accuracy==null){
				_T16_accuracy=getIntegerProperty("T16/accuracy");
				return _T16_accuracy;
			}else {
				return _T16_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T16/accuracy.
	 * @param v Value to Set.
	 */
	public void setT16_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T16/accuracy",v);
		_T16_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T16_responsetime=null;

	/**
	 * @return Returns the T16/responseTime.
	 */
	public Integer getT16_responsetime() {
		try{
			if (_T16_responsetime==null){
				_T16_responsetime=getIntegerProperty("T16/responseTime");
				return _T16_responsetime;
			}else {
				return _T16_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T16/responseTime.
	 * @param v Value to Set.
	 */
	public void setT16_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T16/responseTime",v);
		_T16_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T16_congruency=null;

	/**
	 * @return Returns the T16/congruency.
	 */
	public String getT16_congruency(){
		try{
			if (_T16_congruency==null){
				_T16_congruency=getStringProperty("T16/congruency");
				return _T16_congruency;
			}else {
				return _T16_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T16/congruency.
	 * @param v Value to Set.
	 */
	public void setT16_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T16/congruency",v);
		_T16_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T17_accuracy=null;

	/**
	 * @return Returns the T17/accuracy.
	 */
	public Integer getT17_accuracy() {
		try{
			if (_T17_accuracy==null){
				_T17_accuracy=getIntegerProperty("T17/accuracy");
				return _T17_accuracy;
			}else {
				return _T17_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T17/accuracy.
	 * @param v Value to Set.
	 */
	public void setT17_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T17/accuracy",v);
		_T17_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T17_responsetime=null;

	/**
	 * @return Returns the T17/responseTime.
	 */
	public Integer getT17_responsetime() {
		try{
			if (_T17_responsetime==null){
				_T17_responsetime=getIntegerProperty("T17/responseTime");
				return _T17_responsetime;
			}else {
				return _T17_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T17/responseTime.
	 * @param v Value to Set.
	 */
	public void setT17_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T17/responseTime",v);
		_T17_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T17_congruency=null;

	/**
	 * @return Returns the T17/congruency.
	 */
	public String getT17_congruency(){
		try{
			if (_T17_congruency==null){
				_T17_congruency=getStringProperty("T17/congruency");
				return _T17_congruency;
			}else {
				return _T17_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T17/congruency.
	 * @param v Value to Set.
	 */
	public void setT17_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T17/congruency",v);
		_T17_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T18_accuracy=null;

	/**
	 * @return Returns the T18/accuracy.
	 */
	public Integer getT18_accuracy() {
		try{
			if (_T18_accuracy==null){
				_T18_accuracy=getIntegerProperty("T18/accuracy");
				return _T18_accuracy;
			}else {
				return _T18_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T18/accuracy.
	 * @param v Value to Set.
	 */
	public void setT18_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T18/accuracy",v);
		_T18_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T18_responsetime=null;

	/**
	 * @return Returns the T18/responseTime.
	 */
	public Integer getT18_responsetime() {
		try{
			if (_T18_responsetime==null){
				_T18_responsetime=getIntegerProperty("T18/responseTime");
				return _T18_responsetime;
			}else {
				return _T18_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T18/responseTime.
	 * @param v Value to Set.
	 */
	public void setT18_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T18/responseTime",v);
		_T18_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T18_congruency=null;

	/**
	 * @return Returns the T18/congruency.
	 */
	public String getT18_congruency(){
		try{
			if (_T18_congruency==null){
				_T18_congruency=getStringProperty("T18/congruency");
				return _T18_congruency;
			}else {
				return _T18_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T18/congruency.
	 * @param v Value to Set.
	 */
	public void setT18_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T18/congruency",v);
		_T18_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T19_accuracy=null;

	/**
	 * @return Returns the T19/accuracy.
	 */
	public Integer getT19_accuracy() {
		try{
			if (_T19_accuracy==null){
				_T19_accuracy=getIntegerProperty("T19/accuracy");
				return _T19_accuracy;
			}else {
				return _T19_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T19/accuracy.
	 * @param v Value to Set.
	 */
	public void setT19_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T19/accuracy",v);
		_T19_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T19_responsetime=null;

	/**
	 * @return Returns the T19/responseTime.
	 */
	public Integer getT19_responsetime() {
		try{
			if (_T19_responsetime==null){
				_T19_responsetime=getIntegerProperty("T19/responseTime");
				return _T19_responsetime;
			}else {
				return _T19_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T19/responseTime.
	 * @param v Value to Set.
	 */
	public void setT19_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T19/responseTime",v);
		_T19_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T19_congruency=null;

	/**
	 * @return Returns the T19/congruency.
	 */
	public String getT19_congruency(){
		try{
			if (_T19_congruency==null){
				_T19_congruency=getStringProperty("T19/congruency");
				return _T19_congruency;
			}else {
				return _T19_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T19/congruency.
	 * @param v Value to Set.
	 */
	public void setT19_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T19/congruency",v);
		_T19_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T20_accuracy=null;

	/**
	 * @return Returns the T20/accuracy.
	 */
	public Integer getT20_accuracy() {
		try{
			if (_T20_accuracy==null){
				_T20_accuracy=getIntegerProperty("T20/accuracy");
				return _T20_accuracy;
			}else {
				return _T20_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T20/accuracy.
	 * @param v Value to Set.
	 */
	public void setT20_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T20/accuracy",v);
		_T20_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T20_responsetime=null;

	/**
	 * @return Returns the T20/responseTime.
	 */
	public Integer getT20_responsetime() {
		try{
			if (_T20_responsetime==null){
				_T20_responsetime=getIntegerProperty("T20/responseTime");
				return _T20_responsetime;
			}else {
				return _T20_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T20/responseTime.
	 * @param v Value to Set.
	 */
	public void setT20_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T20/responseTime",v);
		_T20_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T20_congruency=null;

	/**
	 * @return Returns the T20/congruency.
	 */
	public String getT20_congruency(){
		try{
			if (_T20_congruency==null){
				_T20_congruency=getStringProperty("T20/congruency");
				return _T20_congruency;
			}else {
				return _T20_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T20/congruency.
	 * @param v Value to Set.
	 */
	public void setT20_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T20/congruency",v);
		_T20_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T21_accuracy=null;

	/**
	 * @return Returns the T21/accuracy.
	 */
	public Integer getT21_accuracy() {
		try{
			if (_T21_accuracy==null){
				_T21_accuracy=getIntegerProperty("T21/accuracy");
				return _T21_accuracy;
			}else {
				return _T21_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T21/accuracy.
	 * @param v Value to Set.
	 */
	public void setT21_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T21/accuracy",v);
		_T21_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T21_responsetime=null;

	/**
	 * @return Returns the T21/responseTime.
	 */
	public Integer getT21_responsetime() {
		try{
			if (_T21_responsetime==null){
				_T21_responsetime=getIntegerProperty("T21/responseTime");
				return _T21_responsetime;
			}else {
				return _T21_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T21/responseTime.
	 * @param v Value to Set.
	 */
	public void setT21_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T21/responseTime",v);
		_T21_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T21_congruency=null;

	/**
	 * @return Returns the T21/congruency.
	 */
	public String getT21_congruency(){
		try{
			if (_T21_congruency==null){
				_T21_congruency=getStringProperty("T21/congruency");
				return _T21_congruency;
			}else {
				return _T21_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T21/congruency.
	 * @param v Value to Set.
	 */
	public void setT21_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T21/congruency",v);
		_T21_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T22_accuracy=null;

	/**
	 * @return Returns the T22/accuracy.
	 */
	public Integer getT22_accuracy() {
		try{
			if (_T22_accuracy==null){
				_T22_accuracy=getIntegerProperty("T22/accuracy");
				return _T22_accuracy;
			}else {
				return _T22_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T22/accuracy.
	 * @param v Value to Set.
	 */
	public void setT22_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T22/accuracy",v);
		_T22_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T22_responsetime=null;

	/**
	 * @return Returns the T22/responseTime.
	 */
	public Integer getT22_responsetime() {
		try{
			if (_T22_responsetime==null){
				_T22_responsetime=getIntegerProperty("T22/responseTime");
				return _T22_responsetime;
			}else {
				return _T22_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T22/responseTime.
	 * @param v Value to Set.
	 */
	public void setT22_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T22/responseTime",v);
		_T22_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T22_congruency=null;

	/**
	 * @return Returns the T22/congruency.
	 */
	public String getT22_congruency(){
		try{
			if (_T22_congruency==null){
				_T22_congruency=getStringProperty("T22/congruency");
				return _T22_congruency;
			}else {
				return _T22_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T22/congruency.
	 * @param v Value to Set.
	 */
	public void setT22_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T22/congruency",v);
		_T22_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T23_accuracy=null;

	/**
	 * @return Returns the T23/accuracy.
	 */
	public Integer getT23_accuracy() {
		try{
			if (_T23_accuracy==null){
				_T23_accuracy=getIntegerProperty("T23/accuracy");
				return _T23_accuracy;
			}else {
				return _T23_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T23/accuracy.
	 * @param v Value to Set.
	 */
	public void setT23_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T23/accuracy",v);
		_T23_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T23_responsetime=null;

	/**
	 * @return Returns the T23/responseTime.
	 */
	public Integer getT23_responsetime() {
		try{
			if (_T23_responsetime==null){
				_T23_responsetime=getIntegerProperty("T23/responseTime");
				return _T23_responsetime;
			}else {
				return _T23_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T23/responseTime.
	 * @param v Value to Set.
	 */
	public void setT23_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T23/responseTime",v);
		_T23_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T23_congruency=null;

	/**
	 * @return Returns the T23/congruency.
	 */
	public String getT23_congruency(){
		try{
			if (_T23_congruency==null){
				_T23_congruency=getStringProperty("T23/congruency");
				return _T23_congruency;
			}else {
				return _T23_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T23/congruency.
	 * @param v Value to Set.
	 */
	public void setT23_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T23/congruency",v);
		_T23_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T24_accuracy=null;

	/**
	 * @return Returns the T24/accuracy.
	 */
	public Integer getT24_accuracy() {
		try{
			if (_T24_accuracy==null){
				_T24_accuracy=getIntegerProperty("T24/accuracy");
				return _T24_accuracy;
			}else {
				return _T24_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T24/accuracy.
	 * @param v Value to Set.
	 */
	public void setT24_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T24/accuracy",v);
		_T24_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T24_responsetime=null;

	/**
	 * @return Returns the T24/responseTime.
	 */
	public Integer getT24_responsetime() {
		try{
			if (_T24_responsetime==null){
				_T24_responsetime=getIntegerProperty("T24/responseTime");
				return _T24_responsetime;
			}else {
				return _T24_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T24/responseTime.
	 * @param v Value to Set.
	 */
	public void setT24_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T24/responseTime",v);
		_T24_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T24_congruency=null;

	/**
	 * @return Returns the T24/congruency.
	 */
	public String getT24_congruency(){
		try{
			if (_T24_congruency==null){
				_T24_congruency=getStringProperty("T24/congruency");
				return _T24_congruency;
			}else {
				return _T24_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T24/congruency.
	 * @param v Value to Set.
	 */
	public void setT24_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T24/congruency",v);
		_T24_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T25_accuracy=null;

	/**
	 * @return Returns the T25/accuracy.
	 */
	public Integer getT25_accuracy() {
		try{
			if (_T25_accuracy==null){
				_T25_accuracy=getIntegerProperty("T25/accuracy");
				return _T25_accuracy;
			}else {
				return _T25_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T25/accuracy.
	 * @param v Value to Set.
	 */
	public void setT25_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T25/accuracy",v);
		_T25_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T25_responsetime=null;

	/**
	 * @return Returns the T25/responseTime.
	 */
	public Integer getT25_responsetime() {
		try{
			if (_T25_responsetime==null){
				_T25_responsetime=getIntegerProperty("T25/responseTime");
				return _T25_responsetime;
			}else {
				return _T25_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T25/responseTime.
	 * @param v Value to Set.
	 */
	public void setT25_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T25/responseTime",v);
		_T25_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T25_congruency=null;

	/**
	 * @return Returns the T25/congruency.
	 */
	public String getT25_congruency(){
		try{
			if (_T25_congruency==null){
				_T25_congruency=getStringProperty("T25/congruency");
				return _T25_congruency;
			}else {
				return _T25_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T25/congruency.
	 * @param v Value to Set.
	 */
	public void setT25_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T25/congruency",v);
		_T25_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T26_accuracy=null;

	/**
	 * @return Returns the T26/accuracy.
	 */
	public Integer getT26_accuracy() {
		try{
			if (_T26_accuracy==null){
				_T26_accuracy=getIntegerProperty("T26/accuracy");
				return _T26_accuracy;
			}else {
				return _T26_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T26/accuracy.
	 * @param v Value to Set.
	 */
	public void setT26_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T26/accuracy",v);
		_T26_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T26_responsetime=null;

	/**
	 * @return Returns the T26/responseTime.
	 */
	public Integer getT26_responsetime() {
		try{
			if (_T26_responsetime==null){
				_T26_responsetime=getIntegerProperty("T26/responseTime");
				return _T26_responsetime;
			}else {
				return _T26_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T26/responseTime.
	 * @param v Value to Set.
	 */
	public void setT26_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T26/responseTime",v);
		_T26_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T26_congruency=null;

	/**
	 * @return Returns the T26/congruency.
	 */
	public String getT26_congruency(){
		try{
			if (_T26_congruency==null){
				_T26_congruency=getStringProperty("T26/congruency");
				return _T26_congruency;
			}else {
				return _T26_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T26/congruency.
	 * @param v Value to Set.
	 */
	public void setT26_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T26/congruency",v);
		_T26_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T27_accuracy=null;

	/**
	 * @return Returns the T27/accuracy.
	 */
	public Integer getT27_accuracy() {
		try{
			if (_T27_accuracy==null){
				_T27_accuracy=getIntegerProperty("T27/accuracy");
				return _T27_accuracy;
			}else {
				return _T27_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T27/accuracy.
	 * @param v Value to Set.
	 */
	public void setT27_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T27/accuracy",v);
		_T27_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T27_responsetime=null;

	/**
	 * @return Returns the T27/responseTime.
	 */
	public Integer getT27_responsetime() {
		try{
			if (_T27_responsetime==null){
				_T27_responsetime=getIntegerProperty("T27/responseTime");
				return _T27_responsetime;
			}else {
				return _T27_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T27/responseTime.
	 * @param v Value to Set.
	 */
	public void setT27_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T27/responseTime",v);
		_T27_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T27_congruency=null;

	/**
	 * @return Returns the T27/congruency.
	 */
	public String getT27_congruency(){
		try{
			if (_T27_congruency==null){
				_T27_congruency=getStringProperty("T27/congruency");
				return _T27_congruency;
			}else {
				return _T27_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T27/congruency.
	 * @param v Value to Set.
	 */
	public void setT27_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T27/congruency",v);
		_T27_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T28_accuracy=null;

	/**
	 * @return Returns the T28/accuracy.
	 */
	public Integer getT28_accuracy() {
		try{
			if (_T28_accuracy==null){
				_T28_accuracy=getIntegerProperty("T28/accuracy");
				return _T28_accuracy;
			}else {
				return _T28_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T28/accuracy.
	 * @param v Value to Set.
	 */
	public void setT28_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T28/accuracy",v);
		_T28_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T28_responsetime=null;

	/**
	 * @return Returns the T28/responseTime.
	 */
	public Integer getT28_responsetime() {
		try{
			if (_T28_responsetime==null){
				_T28_responsetime=getIntegerProperty("T28/responseTime");
				return _T28_responsetime;
			}else {
				return _T28_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T28/responseTime.
	 * @param v Value to Set.
	 */
	public void setT28_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T28/responseTime",v);
		_T28_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T28_congruency=null;

	/**
	 * @return Returns the T28/congruency.
	 */
	public String getT28_congruency(){
		try{
			if (_T28_congruency==null){
				_T28_congruency=getStringProperty("T28/congruency");
				return _T28_congruency;
			}else {
				return _T28_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T28/congruency.
	 * @param v Value to Set.
	 */
	public void setT28_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T28/congruency",v);
		_T28_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T29_accuracy=null;

	/**
	 * @return Returns the T29/accuracy.
	 */
	public Integer getT29_accuracy() {
		try{
			if (_T29_accuracy==null){
				_T29_accuracy=getIntegerProperty("T29/accuracy");
				return _T29_accuracy;
			}else {
				return _T29_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T29/accuracy.
	 * @param v Value to Set.
	 */
	public void setT29_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T29/accuracy",v);
		_T29_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T29_responsetime=null;

	/**
	 * @return Returns the T29/responseTime.
	 */
	public Integer getT29_responsetime() {
		try{
			if (_T29_responsetime==null){
				_T29_responsetime=getIntegerProperty("T29/responseTime");
				return _T29_responsetime;
			}else {
				return _T29_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T29/responseTime.
	 * @param v Value to Set.
	 */
	public void setT29_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T29/responseTime",v);
		_T29_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T29_congruency=null;

	/**
	 * @return Returns the T29/congruency.
	 */
	public String getT29_congruency(){
		try{
			if (_T29_congruency==null){
				_T29_congruency=getStringProperty("T29/congruency");
				return _T29_congruency;
			}else {
				return _T29_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T29/congruency.
	 * @param v Value to Set.
	 */
	public void setT29_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T29/congruency",v);
		_T29_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T30_accuracy=null;

	/**
	 * @return Returns the T30/accuracy.
	 */
	public Integer getT30_accuracy() {
		try{
			if (_T30_accuracy==null){
				_T30_accuracy=getIntegerProperty("T30/accuracy");
				return _T30_accuracy;
			}else {
				return _T30_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T30/accuracy.
	 * @param v Value to Set.
	 */
	public void setT30_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T30/accuracy",v);
		_T30_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T30_responsetime=null;

	/**
	 * @return Returns the T30/responseTime.
	 */
	public Integer getT30_responsetime() {
		try{
			if (_T30_responsetime==null){
				_T30_responsetime=getIntegerProperty("T30/responseTime");
				return _T30_responsetime;
			}else {
				return _T30_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T30/responseTime.
	 * @param v Value to Set.
	 */
	public void setT30_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T30/responseTime",v);
		_T30_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T30_congruency=null;

	/**
	 * @return Returns the T30/congruency.
	 */
	public String getT30_congruency(){
		try{
			if (_T30_congruency==null){
				_T30_congruency=getStringProperty("T30/congruency");
				return _T30_congruency;
			}else {
				return _T30_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T30/congruency.
	 * @param v Value to Set.
	 */
	public void setT30_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T30/congruency",v);
		_T30_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T31_accuracy=null;

	/**
	 * @return Returns the T31/accuracy.
	 */
	public Integer getT31_accuracy() {
		try{
			if (_T31_accuracy==null){
				_T31_accuracy=getIntegerProperty("T31/accuracy");
				return _T31_accuracy;
			}else {
				return _T31_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T31/accuracy.
	 * @param v Value to Set.
	 */
	public void setT31_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T31/accuracy",v);
		_T31_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T31_responsetime=null;

	/**
	 * @return Returns the T31/responseTime.
	 */
	public Integer getT31_responsetime() {
		try{
			if (_T31_responsetime==null){
				_T31_responsetime=getIntegerProperty("T31/responseTime");
				return _T31_responsetime;
			}else {
				return _T31_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T31/responseTime.
	 * @param v Value to Set.
	 */
	public void setT31_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T31/responseTime",v);
		_T31_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T31_congruency=null;

	/**
	 * @return Returns the T31/congruency.
	 */
	public String getT31_congruency(){
		try{
			if (_T31_congruency==null){
				_T31_congruency=getStringProperty("T31/congruency");
				return _T31_congruency;
			}else {
				return _T31_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T31/congruency.
	 * @param v Value to Set.
	 */
	public void setT31_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T31/congruency",v);
		_T31_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T32_accuracy=null;

	/**
	 * @return Returns the T32/accuracy.
	 */
	public Integer getT32_accuracy() {
		try{
			if (_T32_accuracy==null){
				_T32_accuracy=getIntegerProperty("T32/accuracy");
				return _T32_accuracy;
			}else {
				return _T32_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T32/accuracy.
	 * @param v Value to Set.
	 */
	public void setT32_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T32/accuracy",v);
		_T32_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T32_responsetime=null;

	/**
	 * @return Returns the T32/responseTime.
	 */
	public Integer getT32_responsetime() {
		try{
			if (_T32_responsetime==null){
				_T32_responsetime=getIntegerProperty("T32/responseTime");
				return _T32_responsetime;
			}else {
				return _T32_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T32/responseTime.
	 * @param v Value to Set.
	 */
	public void setT32_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T32/responseTime",v);
		_T32_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T32_congruency=null;

	/**
	 * @return Returns the T32/congruency.
	 */
	public String getT32_congruency(){
		try{
			if (_T32_congruency==null){
				_T32_congruency=getStringProperty("T32/congruency");
				return _T32_congruency;
			}else {
				return _T32_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T32/congruency.
	 * @param v Value to Set.
	 */
	public void setT32_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T32/congruency",v);
		_T32_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T33_accuracy=null;

	/**
	 * @return Returns the T33/accuracy.
	 */
	public Integer getT33_accuracy() {
		try{
			if (_T33_accuracy==null){
				_T33_accuracy=getIntegerProperty("T33/accuracy");
				return _T33_accuracy;
			}else {
				return _T33_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T33/accuracy.
	 * @param v Value to Set.
	 */
	public void setT33_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T33/accuracy",v);
		_T33_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T33_responsetime=null;

	/**
	 * @return Returns the T33/responseTime.
	 */
	public Integer getT33_responsetime() {
		try{
			if (_T33_responsetime==null){
				_T33_responsetime=getIntegerProperty("T33/responseTime");
				return _T33_responsetime;
			}else {
				return _T33_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T33/responseTime.
	 * @param v Value to Set.
	 */
	public void setT33_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T33/responseTime",v);
		_T33_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T33_congruency=null;

	/**
	 * @return Returns the T33/congruency.
	 */
	public String getT33_congruency(){
		try{
			if (_T33_congruency==null){
				_T33_congruency=getStringProperty("T33/congruency");
				return _T33_congruency;
			}else {
				return _T33_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T33/congruency.
	 * @param v Value to Set.
	 */
	public void setT33_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T33/congruency",v);
		_T33_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T34_accuracy=null;

	/**
	 * @return Returns the T34/accuracy.
	 */
	public Integer getT34_accuracy() {
		try{
			if (_T34_accuracy==null){
				_T34_accuracy=getIntegerProperty("T34/accuracy");
				return _T34_accuracy;
			}else {
				return _T34_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T34/accuracy.
	 * @param v Value to Set.
	 */
	public void setT34_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T34/accuracy",v);
		_T34_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T34_responsetime=null;

	/**
	 * @return Returns the T34/responseTime.
	 */
	public Integer getT34_responsetime() {
		try{
			if (_T34_responsetime==null){
				_T34_responsetime=getIntegerProperty("T34/responseTime");
				return _T34_responsetime;
			}else {
				return _T34_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T34/responseTime.
	 * @param v Value to Set.
	 */
	public void setT34_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T34/responseTime",v);
		_T34_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T34_congruency=null;

	/**
	 * @return Returns the T34/congruency.
	 */
	public String getT34_congruency(){
		try{
			if (_T34_congruency==null){
				_T34_congruency=getStringProperty("T34/congruency");
				return _T34_congruency;
			}else {
				return _T34_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T34/congruency.
	 * @param v Value to Set.
	 */
	public void setT34_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T34/congruency",v);
		_T34_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T35_accuracy=null;

	/**
	 * @return Returns the T35/accuracy.
	 */
	public Integer getT35_accuracy() {
		try{
			if (_T35_accuracy==null){
				_T35_accuracy=getIntegerProperty("T35/accuracy");
				return _T35_accuracy;
			}else {
				return _T35_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T35/accuracy.
	 * @param v Value to Set.
	 */
	public void setT35_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T35/accuracy",v);
		_T35_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T35_responsetime=null;

	/**
	 * @return Returns the T35/responseTime.
	 */
	public Integer getT35_responsetime() {
		try{
			if (_T35_responsetime==null){
				_T35_responsetime=getIntegerProperty("T35/responseTime");
				return _T35_responsetime;
			}else {
				return _T35_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T35/responseTime.
	 * @param v Value to Set.
	 */
	public void setT35_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T35/responseTime",v);
		_T35_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T35_congruency=null;

	/**
	 * @return Returns the T35/congruency.
	 */
	public String getT35_congruency(){
		try{
			if (_T35_congruency==null){
				_T35_congruency=getStringProperty("T35/congruency");
				return _T35_congruency;
			}else {
				return _T35_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T35/congruency.
	 * @param v Value to Set.
	 */
	public void setT35_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T35/congruency",v);
		_T35_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T36_accuracy=null;

	/**
	 * @return Returns the T36/accuracy.
	 */
	public Integer getT36_accuracy() {
		try{
			if (_T36_accuracy==null){
				_T36_accuracy=getIntegerProperty("T36/accuracy");
				return _T36_accuracy;
			}else {
				return _T36_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T36/accuracy.
	 * @param v Value to Set.
	 */
	public void setT36_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T36/accuracy",v);
		_T36_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T36_responsetime=null;

	/**
	 * @return Returns the T36/responseTime.
	 */
	public Integer getT36_responsetime() {
		try{
			if (_T36_responsetime==null){
				_T36_responsetime=getIntegerProperty("T36/responseTime");
				return _T36_responsetime;
			}else {
				return _T36_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T36/responseTime.
	 * @param v Value to Set.
	 */
	public void setT36_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T36/responseTime",v);
		_T36_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T36_congruency=null;

	/**
	 * @return Returns the T36/congruency.
	 */
	public String getT36_congruency(){
		try{
			if (_T36_congruency==null){
				_T36_congruency=getStringProperty("T36/congruency");
				return _T36_congruency;
			}else {
				return _T36_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T36/congruency.
	 * @param v Value to Set.
	 */
	public void setT36_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T36/congruency",v);
		_T36_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T37_accuracy=null;

	/**
	 * @return Returns the T37/accuracy.
	 */
	public Integer getT37_accuracy() {
		try{
			if (_T37_accuracy==null){
				_T37_accuracy=getIntegerProperty("T37/accuracy");
				return _T37_accuracy;
			}else {
				return _T37_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T37/accuracy.
	 * @param v Value to Set.
	 */
	public void setT37_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T37/accuracy",v);
		_T37_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T37_responsetime=null;

	/**
	 * @return Returns the T37/responseTime.
	 */
	public Integer getT37_responsetime() {
		try{
			if (_T37_responsetime==null){
				_T37_responsetime=getIntegerProperty("T37/responseTime");
				return _T37_responsetime;
			}else {
				return _T37_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T37/responseTime.
	 * @param v Value to Set.
	 */
	public void setT37_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T37/responseTime",v);
		_T37_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T37_congruency=null;

	/**
	 * @return Returns the T37/congruency.
	 */
	public String getT37_congruency(){
		try{
			if (_T37_congruency==null){
				_T37_congruency=getStringProperty("T37/congruency");
				return _T37_congruency;
			}else {
				return _T37_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T37/congruency.
	 * @param v Value to Set.
	 */
	public void setT37_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T37/congruency",v);
		_T37_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T38_accuracy=null;

	/**
	 * @return Returns the T38/accuracy.
	 */
	public Integer getT38_accuracy() {
		try{
			if (_T38_accuracy==null){
				_T38_accuracy=getIntegerProperty("T38/accuracy");
				return _T38_accuracy;
			}else {
				return _T38_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T38/accuracy.
	 * @param v Value to Set.
	 */
	public void setT38_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T38/accuracy",v);
		_T38_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T38_responsetime=null;

	/**
	 * @return Returns the T38/responseTime.
	 */
	public Integer getT38_responsetime() {
		try{
			if (_T38_responsetime==null){
				_T38_responsetime=getIntegerProperty("T38/responseTime");
				return _T38_responsetime;
			}else {
				return _T38_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T38/responseTime.
	 * @param v Value to Set.
	 */
	public void setT38_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T38/responseTime",v);
		_T38_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T38_congruency=null;

	/**
	 * @return Returns the T38/congruency.
	 */
	public String getT38_congruency(){
		try{
			if (_T38_congruency==null){
				_T38_congruency=getStringProperty("T38/congruency");
				return _T38_congruency;
			}else {
				return _T38_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T38/congruency.
	 * @param v Value to Set.
	 */
	public void setT38_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T38/congruency",v);
		_T38_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T39_accuracy=null;

	/**
	 * @return Returns the T39/accuracy.
	 */
	public Integer getT39_accuracy() {
		try{
			if (_T39_accuracy==null){
				_T39_accuracy=getIntegerProperty("T39/accuracy");
				return _T39_accuracy;
			}else {
				return _T39_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T39/accuracy.
	 * @param v Value to Set.
	 */
	public void setT39_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T39/accuracy",v);
		_T39_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T39_responsetime=null;

	/**
	 * @return Returns the T39/responseTime.
	 */
	public Integer getT39_responsetime() {
		try{
			if (_T39_responsetime==null){
				_T39_responsetime=getIntegerProperty("T39/responseTime");
				return _T39_responsetime;
			}else {
				return _T39_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T39/responseTime.
	 * @param v Value to Set.
	 */
	public void setT39_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T39/responseTime",v);
		_T39_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T39_congruency=null;

	/**
	 * @return Returns the T39/congruency.
	 */
	public String getT39_congruency(){
		try{
			if (_T39_congruency==null){
				_T39_congruency=getStringProperty("T39/congruency");
				return _T39_congruency;
			}else {
				return _T39_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T39/congruency.
	 * @param v Value to Set.
	 */
	public void setT39_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T39/congruency",v);
		_T39_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T40_accuracy=null;

	/**
	 * @return Returns the T40/accuracy.
	 */
	public Integer getT40_accuracy() {
		try{
			if (_T40_accuracy==null){
				_T40_accuracy=getIntegerProperty("T40/accuracy");
				return _T40_accuracy;
			}else {
				return _T40_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T40/accuracy.
	 * @param v Value to Set.
	 */
	public void setT40_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T40/accuracy",v);
		_T40_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T40_responsetime=null;

	/**
	 * @return Returns the T40/responseTime.
	 */
	public Integer getT40_responsetime() {
		try{
			if (_T40_responsetime==null){
				_T40_responsetime=getIntegerProperty("T40/responseTime");
				return _T40_responsetime;
			}else {
				return _T40_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T40/responseTime.
	 * @param v Value to Set.
	 */
	public void setT40_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T40/responseTime",v);
		_T40_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T40_congruency=null;

	/**
	 * @return Returns the T40/congruency.
	 */
	public String getT40_congruency(){
		try{
			if (_T40_congruency==null){
				_T40_congruency=getStringProperty("T40/congruency");
				return _T40_congruency;
			}else {
				return _T40_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T40/congruency.
	 * @param v Value to Set.
	 */
	public void setT40_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T40/congruency",v);
		_T40_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T41_accuracy=null;

	/**
	 * @return Returns the T41/accuracy.
	 */
	public Integer getT41_accuracy() {
		try{
			if (_T41_accuracy==null){
				_T41_accuracy=getIntegerProperty("T41/accuracy");
				return _T41_accuracy;
			}else {
				return _T41_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T41/accuracy.
	 * @param v Value to Set.
	 */
	public void setT41_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T41/accuracy",v);
		_T41_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T41_responsetime=null;

	/**
	 * @return Returns the T41/responseTime.
	 */
	public Integer getT41_responsetime() {
		try{
			if (_T41_responsetime==null){
				_T41_responsetime=getIntegerProperty("T41/responseTime");
				return _T41_responsetime;
			}else {
				return _T41_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T41/responseTime.
	 * @param v Value to Set.
	 */
	public void setT41_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T41/responseTime",v);
		_T41_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T41_congruency=null;

	/**
	 * @return Returns the T41/congruency.
	 */
	public String getT41_congruency(){
		try{
			if (_T41_congruency==null){
				_T41_congruency=getStringProperty("T41/congruency");
				return _T41_congruency;
			}else {
				return _T41_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T41/congruency.
	 * @param v Value to Set.
	 */
	public void setT41_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T41/congruency",v);
		_T41_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T42_accuracy=null;

	/**
	 * @return Returns the T42/accuracy.
	 */
	public Integer getT42_accuracy() {
		try{
			if (_T42_accuracy==null){
				_T42_accuracy=getIntegerProperty("T42/accuracy");
				return _T42_accuracy;
			}else {
				return _T42_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T42/accuracy.
	 * @param v Value to Set.
	 */
	public void setT42_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T42/accuracy",v);
		_T42_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T42_responsetime=null;

	/**
	 * @return Returns the T42/responseTime.
	 */
	public Integer getT42_responsetime() {
		try{
			if (_T42_responsetime==null){
				_T42_responsetime=getIntegerProperty("T42/responseTime");
				return _T42_responsetime;
			}else {
				return _T42_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T42/responseTime.
	 * @param v Value to Set.
	 */
	public void setT42_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T42/responseTime",v);
		_T42_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T42_congruency=null;

	/**
	 * @return Returns the T42/congruency.
	 */
	public String getT42_congruency(){
		try{
			if (_T42_congruency==null){
				_T42_congruency=getStringProperty("T42/congruency");
				return _T42_congruency;
			}else {
				return _T42_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T42/congruency.
	 * @param v Value to Set.
	 */
	public void setT42_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T42/congruency",v);
		_T42_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T43_accuracy=null;

	/**
	 * @return Returns the T43/accuracy.
	 */
	public Integer getT43_accuracy() {
		try{
			if (_T43_accuracy==null){
				_T43_accuracy=getIntegerProperty("T43/accuracy");
				return _T43_accuracy;
			}else {
				return _T43_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T43/accuracy.
	 * @param v Value to Set.
	 */
	public void setT43_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T43/accuracy",v);
		_T43_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T43_responsetime=null;

	/**
	 * @return Returns the T43/responseTime.
	 */
	public Integer getT43_responsetime() {
		try{
			if (_T43_responsetime==null){
				_T43_responsetime=getIntegerProperty("T43/responseTime");
				return _T43_responsetime;
			}else {
				return _T43_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T43/responseTime.
	 * @param v Value to Set.
	 */
	public void setT43_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T43/responseTime",v);
		_T43_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T43_congruency=null;

	/**
	 * @return Returns the T43/congruency.
	 */
	public String getT43_congruency(){
		try{
			if (_T43_congruency==null){
				_T43_congruency=getStringProperty("T43/congruency");
				return _T43_congruency;
			}else {
				return _T43_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T43/congruency.
	 * @param v Value to Set.
	 */
	public void setT43_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T43/congruency",v);
		_T43_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T44_accuracy=null;

	/**
	 * @return Returns the T44/accuracy.
	 */
	public Integer getT44_accuracy() {
		try{
			if (_T44_accuracy==null){
				_T44_accuracy=getIntegerProperty("T44/accuracy");
				return _T44_accuracy;
			}else {
				return _T44_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T44/accuracy.
	 * @param v Value to Set.
	 */
	public void setT44_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T44/accuracy",v);
		_T44_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T44_responsetime=null;

	/**
	 * @return Returns the T44/responseTime.
	 */
	public Integer getT44_responsetime() {
		try{
			if (_T44_responsetime==null){
				_T44_responsetime=getIntegerProperty("T44/responseTime");
				return _T44_responsetime;
			}else {
				return _T44_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T44/responseTime.
	 * @param v Value to Set.
	 */
	public void setT44_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T44/responseTime",v);
		_T44_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T44_congruency=null;

	/**
	 * @return Returns the T44/congruency.
	 */
	public String getT44_congruency(){
		try{
			if (_T44_congruency==null){
				_T44_congruency=getStringProperty("T44/congruency");
				return _T44_congruency;
			}else {
				return _T44_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T44/congruency.
	 * @param v Value to Set.
	 */
	public void setT44_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T44/congruency",v);
		_T44_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T45_accuracy=null;

	/**
	 * @return Returns the T45/accuracy.
	 */
	public Integer getT45_accuracy() {
		try{
			if (_T45_accuracy==null){
				_T45_accuracy=getIntegerProperty("T45/accuracy");
				return _T45_accuracy;
			}else {
				return _T45_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T45/accuracy.
	 * @param v Value to Set.
	 */
	public void setT45_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T45/accuracy",v);
		_T45_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T45_responsetime=null;

	/**
	 * @return Returns the T45/responseTime.
	 */
	public Integer getT45_responsetime() {
		try{
			if (_T45_responsetime==null){
				_T45_responsetime=getIntegerProperty("T45/responseTime");
				return _T45_responsetime;
			}else {
				return _T45_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T45/responseTime.
	 * @param v Value to Set.
	 */
	public void setT45_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T45/responseTime",v);
		_T45_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T45_congruency=null;

	/**
	 * @return Returns the T45/congruency.
	 */
	public String getT45_congruency(){
		try{
			if (_T45_congruency==null){
				_T45_congruency=getStringProperty("T45/congruency");
				return _T45_congruency;
			}else {
				return _T45_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T45/congruency.
	 * @param v Value to Set.
	 */
	public void setT45_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T45/congruency",v);
		_T45_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T46_accuracy=null;

	/**
	 * @return Returns the T46/accuracy.
	 */
	public Integer getT46_accuracy() {
		try{
			if (_T46_accuracy==null){
				_T46_accuracy=getIntegerProperty("T46/accuracy");
				return _T46_accuracy;
			}else {
				return _T46_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T46/accuracy.
	 * @param v Value to Set.
	 */
	public void setT46_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T46/accuracy",v);
		_T46_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T46_responsetime=null;

	/**
	 * @return Returns the T46/responseTime.
	 */
	public Integer getT46_responsetime() {
		try{
			if (_T46_responsetime==null){
				_T46_responsetime=getIntegerProperty("T46/responseTime");
				return _T46_responsetime;
			}else {
				return _T46_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T46/responseTime.
	 * @param v Value to Set.
	 */
	public void setT46_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T46/responseTime",v);
		_T46_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T46_congruency=null;

	/**
	 * @return Returns the T46/congruency.
	 */
	public String getT46_congruency(){
		try{
			if (_T46_congruency==null){
				_T46_congruency=getStringProperty("T46/congruency");
				return _T46_congruency;
			}else {
				return _T46_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T46/congruency.
	 * @param v Value to Set.
	 */
	public void setT46_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T46/congruency",v);
		_T46_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T47_accuracy=null;

	/**
	 * @return Returns the T47/accuracy.
	 */
	public Integer getT47_accuracy() {
		try{
			if (_T47_accuracy==null){
				_T47_accuracy=getIntegerProperty("T47/accuracy");
				return _T47_accuracy;
			}else {
				return _T47_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T47/accuracy.
	 * @param v Value to Set.
	 */
	public void setT47_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T47/accuracy",v);
		_T47_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T47_responsetime=null;

	/**
	 * @return Returns the T47/responseTime.
	 */
	public Integer getT47_responsetime() {
		try{
			if (_T47_responsetime==null){
				_T47_responsetime=getIntegerProperty("T47/responseTime");
				return _T47_responsetime;
			}else {
				return _T47_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T47/responseTime.
	 * @param v Value to Set.
	 */
	public void setT47_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T47/responseTime",v);
		_T47_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T47_congruency=null;

	/**
	 * @return Returns the T47/congruency.
	 */
	public String getT47_congruency(){
		try{
			if (_T47_congruency==null){
				_T47_congruency=getStringProperty("T47/congruency");
				return _T47_congruency;
			}else {
				return _T47_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T47/congruency.
	 * @param v Value to Set.
	 */
	public void setT47_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T47/congruency",v);
		_T47_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T48_accuracy=null;

	/**
	 * @return Returns the T48/accuracy.
	 */
	public Integer getT48_accuracy() {
		try{
			if (_T48_accuracy==null){
				_T48_accuracy=getIntegerProperty("T48/accuracy");
				return _T48_accuracy;
			}else {
				return _T48_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T48/accuracy.
	 * @param v Value to Set.
	 */
	public void setT48_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T48/accuracy",v);
		_T48_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T48_responsetime=null;

	/**
	 * @return Returns the T48/responseTime.
	 */
	public Integer getT48_responsetime() {
		try{
			if (_T48_responsetime==null){
				_T48_responsetime=getIntegerProperty("T48/responseTime");
				return _T48_responsetime;
			}else {
				return _T48_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T48/responseTime.
	 * @param v Value to Set.
	 */
	public void setT48_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T48/responseTime",v);
		_T48_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T48_congruency=null;

	/**
	 * @return Returns the T48/congruency.
	 */
	public String getT48_congruency(){
		try{
			if (_T48_congruency==null){
				_T48_congruency=getStringProperty("T48/congruency");
				return _T48_congruency;
			}else {
				return _T48_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T48/congruency.
	 * @param v Value to Set.
	 */
	public void setT48_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T48/congruency",v);
		_T48_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T49_accuracy=null;

	/**
	 * @return Returns the T49/accuracy.
	 */
	public Integer getT49_accuracy() {
		try{
			if (_T49_accuracy==null){
				_T49_accuracy=getIntegerProperty("T49/accuracy");
				return _T49_accuracy;
			}else {
				return _T49_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T49/accuracy.
	 * @param v Value to Set.
	 */
	public void setT49_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T49/accuracy",v);
		_T49_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T49_responsetime=null;

	/**
	 * @return Returns the T49/responseTime.
	 */
	public Integer getT49_responsetime() {
		try{
			if (_T49_responsetime==null){
				_T49_responsetime=getIntegerProperty("T49/responseTime");
				return _T49_responsetime;
			}else {
				return _T49_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T49/responseTime.
	 * @param v Value to Set.
	 */
	public void setT49_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T49/responseTime",v);
		_T49_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T49_congruency=null;

	/**
	 * @return Returns the T49/congruency.
	 */
	public String getT49_congruency(){
		try{
			if (_T49_congruency==null){
				_T49_congruency=getStringProperty("T49/congruency");
				return _T49_congruency;
			}else {
				return _T49_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T49/congruency.
	 * @param v Value to Set.
	 */
	public void setT49_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T49/congruency",v);
		_T49_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T50_accuracy=null;

	/**
	 * @return Returns the T50/accuracy.
	 */
	public Integer getT50_accuracy() {
		try{
			if (_T50_accuracy==null){
				_T50_accuracy=getIntegerProperty("T50/accuracy");
				return _T50_accuracy;
			}else {
				return _T50_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T50/accuracy.
	 * @param v Value to Set.
	 */
	public void setT50_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T50/accuracy",v);
		_T50_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T50_responsetime=null;

	/**
	 * @return Returns the T50/responseTime.
	 */
	public Integer getT50_responsetime() {
		try{
			if (_T50_responsetime==null){
				_T50_responsetime=getIntegerProperty("T50/responseTime");
				return _T50_responsetime;
			}else {
				return _T50_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T50/responseTime.
	 * @param v Value to Set.
	 */
	public void setT50_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T50/responseTime",v);
		_T50_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T50_congruency=null;

	/**
	 * @return Returns the T50/congruency.
	 */
	public String getT50_congruency(){
		try{
			if (_T50_congruency==null){
				_T50_congruency=getStringProperty("T50/congruency");
				return _T50_congruency;
			}else {
				return _T50_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T50/congruency.
	 * @param v Value to Set.
	 */
	public void setT50_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T50/congruency",v);
		_T50_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T51_accuracy=null;

	/**
	 * @return Returns the T51/accuracy.
	 */
	public Integer getT51_accuracy() {
		try{
			if (_T51_accuracy==null){
				_T51_accuracy=getIntegerProperty("T51/accuracy");
				return _T51_accuracy;
			}else {
				return _T51_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T51/accuracy.
	 * @param v Value to Set.
	 */
	public void setT51_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T51/accuracy",v);
		_T51_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T51_responsetime=null;

	/**
	 * @return Returns the T51/responseTime.
	 */
	public Integer getT51_responsetime() {
		try{
			if (_T51_responsetime==null){
				_T51_responsetime=getIntegerProperty("T51/responseTime");
				return _T51_responsetime;
			}else {
				return _T51_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T51/responseTime.
	 * @param v Value to Set.
	 */
	public void setT51_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T51/responseTime",v);
		_T51_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T51_congruency=null;

	/**
	 * @return Returns the T51/congruency.
	 */
	public String getT51_congruency(){
		try{
			if (_T51_congruency==null){
				_T51_congruency=getStringProperty("T51/congruency");
				return _T51_congruency;
			}else {
				return _T51_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T51/congruency.
	 * @param v Value to Set.
	 */
	public void setT51_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T51/congruency",v);
		_T51_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T52_accuracy=null;

	/**
	 * @return Returns the T52/accuracy.
	 */
	public Integer getT52_accuracy() {
		try{
			if (_T52_accuracy==null){
				_T52_accuracy=getIntegerProperty("T52/accuracy");
				return _T52_accuracy;
			}else {
				return _T52_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T52/accuracy.
	 * @param v Value to Set.
	 */
	public void setT52_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T52/accuracy",v);
		_T52_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T52_responsetime=null;

	/**
	 * @return Returns the T52/responseTime.
	 */
	public Integer getT52_responsetime() {
		try{
			if (_T52_responsetime==null){
				_T52_responsetime=getIntegerProperty("T52/responseTime");
				return _T52_responsetime;
			}else {
				return _T52_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T52/responseTime.
	 * @param v Value to Set.
	 */
	public void setT52_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T52/responseTime",v);
		_T52_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T52_congruency=null;

	/**
	 * @return Returns the T52/congruency.
	 */
	public String getT52_congruency(){
		try{
			if (_T52_congruency==null){
				_T52_congruency=getStringProperty("T52/congruency");
				return _T52_congruency;
			}else {
				return _T52_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T52/congruency.
	 * @param v Value to Set.
	 */
	public void setT52_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T52/congruency",v);
		_T52_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T53_accuracy=null;

	/**
	 * @return Returns the T53/accuracy.
	 */
	public Integer getT53_accuracy() {
		try{
			if (_T53_accuracy==null){
				_T53_accuracy=getIntegerProperty("T53/accuracy");
				return _T53_accuracy;
			}else {
				return _T53_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T53/accuracy.
	 * @param v Value to Set.
	 */
	public void setT53_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T53/accuracy",v);
		_T53_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T53_responsetime=null;

	/**
	 * @return Returns the T53/responseTime.
	 */
	public Integer getT53_responsetime() {
		try{
			if (_T53_responsetime==null){
				_T53_responsetime=getIntegerProperty("T53/responseTime");
				return _T53_responsetime;
			}else {
				return _T53_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T53/responseTime.
	 * @param v Value to Set.
	 */
	public void setT53_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T53/responseTime",v);
		_T53_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T53_congruency=null;

	/**
	 * @return Returns the T53/congruency.
	 */
	public String getT53_congruency(){
		try{
			if (_T53_congruency==null){
				_T53_congruency=getStringProperty("T53/congruency");
				return _T53_congruency;
			}else {
				return _T53_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T53/congruency.
	 * @param v Value to Set.
	 */
	public void setT53_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T53/congruency",v);
		_T53_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T54_accuracy=null;

	/**
	 * @return Returns the T54/accuracy.
	 */
	public Integer getT54_accuracy() {
		try{
			if (_T54_accuracy==null){
				_T54_accuracy=getIntegerProperty("T54/accuracy");
				return _T54_accuracy;
			}else {
				return _T54_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T54/accuracy.
	 * @param v Value to Set.
	 */
	public void setT54_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T54/accuracy",v);
		_T54_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T54_responsetime=null;

	/**
	 * @return Returns the T54/responseTime.
	 */
	public Integer getT54_responsetime() {
		try{
			if (_T54_responsetime==null){
				_T54_responsetime=getIntegerProperty("T54/responseTime");
				return _T54_responsetime;
			}else {
				return _T54_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T54/responseTime.
	 * @param v Value to Set.
	 */
	public void setT54_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T54/responseTime",v);
		_T54_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T54_congruency=null;

	/**
	 * @return Returns the T54/congruency.
	 */
	public String getT54_congruency(){
		try{
			if (_T54_congruency==null){
				_T54_congruency=getStringProperty("T54/congruency");
				return _T54_congruency;
			}else {
				return _T54_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T54/congruency.
	 * @param v Value to Set.
	 */
	public void setT54_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T54/congruency",v);
		_T54_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T55_accuracy=null;

	/**
	 * @return Returns the T55/accuracy.
	 */
	public Integer getT55_accuracy() {
		try{
			if (_T55_accuracy==null){
				_T55_accuracy=getIntegerProperty("T55/accuracy");
				return _T55_accuracy;
			}else {
				return _T55_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T55/accuracy.
	 * @param v Value to Set.
	 */
	public void setT55_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T55/accuracy",v);
		_T55_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T55_responsetime=null;

	/**
	 * @return Returns the T55/responseTime.
	 */
	public Integer getT55_responsetime() {
		try{
			if (_T55_responsetime==null){
				_T55_responsetime=getIntegerProperty("T55/responseTime");
				return _T55_responsetime;
			}else {
				return _T55_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T55/responseTime.
	 * @param v Value to Set.
	 */
	public void setT55_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T55/responseTime",v);
		_T55_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T55_congruency=null;

	/**
	 * @return Returns the T55/congruency.
	 */
	public String getT55_congruency(){
		try{
			if (_T55_congruency==null){
				_T55_congruency=getStringProperty("T55/congruency");
				return _T55_congruency;
			}else {
				return _T55_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T55/congruency.
	 * @param v Value to Set.
	 */
	public void setT55_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T55/congruency",v);
		_T55_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T56_accuracy=null;

	/**
	 * @return Returns the T56/accuracy.
	 */
	public Integer getT56_accuracy() {
		try{
			if (_T56_accuracy==null){
				_T56_accuracy=getIntegerProperty("T56/accuracy");
				return _T56_accuracy;
			}else {
				return _T56_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T56/accuracy.
	 * @param v Value to Set.
	 */
	public void setT56_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T56/accuracy",v);
		_T56_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T56_responsetime=null;

	/**
	 * @return Returns the T56/responseTime.
	 */
	public Integer getT56_responsetime() {
		try{
			if (_T56_responsetime==null){
				_T56_responsetime=getIntegerProperty("T56/responseTime");
				return _T56_responsetime;
			}else {
				return _T56_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T56/responseTime.
	 * @param v Value to Set.
	 */
	public void setT56_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T56/responseTime",v);
		_T56_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T56_congruency=null;

	/**
	 * @return Returns the T56/congruency.
	 */
	public String getT56_congruency(){
		try{
			if (_T56_congruency==null){
				_T56_congruency=getStringProperty("T56/congruency");
				return _T56_congruency;
			}else {
				return _T56_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T56/congruency.
	 * @param v Value to Set.
	 */
	public void setT56_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T56/congruency",v);
		_T56_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T57_accuracy=null;

	/**
	 * @return Returns the T57/accuracy.
	 */
	public Integer getT57_accuracy() {
		try{
			if (_T57_accuracy==null){
				_T57_accuracy=getIntegerProperty("T57/accuracy");
				return _T57_accuracy;
			}else {
				return _T57_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T57/accuracy.
	 * @param v Value to Set.
	 */
	public void setT57_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T57/accuracy",v);
		_T57_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T57_responsetime=null;

	/**
	 * @return Returns the T57/responseTime.
	 */
	public Integer getT57_responsetime() {
		try{
			if (_T57_responsetime==null){
				_T57_responsetime=getIntegerProperty("T57/responseTime");
				return _T57_responsetime;
			}else {
				return _T57_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T57/responseTime.
	 * @param v Value to Set.
	 */
	public void setT57_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T57/responseTime",v);
		_T57_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T57_congruency=null;

	/**
	 * @return Returns the T57/congruency.
	 */
	public String getT57_congruency(){
		try{
			if (_T57_congruency==null){
				_T57_congruency=getStringProperty("T57/congruency");
				return _T57_congruency;
			}else {
				return _T57_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T57/congruency.
	 * @param v Value to Set.
	 */
	public void setT57_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T57/congruency",v);
		_T57_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T58_accuracy=null;

	/**
	 * @return Returns the T58/accuracy.
	 */
	public Integer getT58_accuracy() {
		try{
			if (_T58_accuracy==null){
				_T58_accuracy=getIntegerProperty("T58/accuracy");
				return _T58_accuracy;
			}else {
				return _T58_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T58/accuracy.
	 * @param v Value to Set.
	 */
	public void setT58_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T58/accuracy",v);
		_T58_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T58_responsetime=null;

	/**
	 * @return Returns the T58/responseTime.
	 */
	public Integer getT58_responsetime() {
		try{
			if (_T58_responsetime==null){
				_T58_responsetime=getIntegerProperty("T58/responseTime");
				return _T58_responsetime;
			}else {
				return _T58_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T58/responseTime.
	 * @param v Value to Set.
	 */
	public void setT58_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T58/responseTime",v);
		_T58_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T58_congruency=null;

	/**
	 * @return Returns the T58/congruency.
	 */
	public String getT58_congruency(){
		try{
			if (_T58_congruency==null){
				_T58_congruency=getStringProperty("T58/congruency");
				return _T58_congruency;
			}else {
				return _T58_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T58/congruency.
	 * @param v Value to Set.
	 */
	public void setT58_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T58/congruency",v);
		_T58_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T59_accuracy=null;

	/**
	 * @return Returns the T59/accuracy.
	 */
	public Integer getT59_accuracy() {
		try{
			if (_T59_accuracy==null){
				_T59_accuracy=getIntegerProperty("T59/accuracy");
				return _T59_accuracy;
			}else {
				return _T59_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T59/accuracy.
	 * @param v Value to Set.
	 */
	public void setT59_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T59/accuracy",v);
		_T59_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T59_responsetime=null;

	/**
	 * @return Returns the T59/responseTime.
	 */
	public Integer getT59_responsetime() {
		try{
			if (_T59_responsetime==null){
				_T59_responsetime=getIntegerProperty("T59/responseTime");
				return _T59_responsetime;
			}else {
				return _T59_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T59/responseTime.
	 * @param v Value to Set.
	 */
	public void setT59_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T59/responseTime",v);
		_T59_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T59_congruency=null;

	/**
	 * @return Returns the T59/congruency.
	 */
	public String getT59_congruency(){
		try{
			if (_T59_congruency==null){
				_T59_congruency=getStringProperty("T59/congruency");
				return _T59_congruency;
			}else {
				return _T59_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T59/congruency.
	 * @param v Value to Set.
	 */
	public void setT59_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T59/congruency",v);
		_T59_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T60_accuracy=null;

	/**
	 * @return Returns the T60/accuracy.
	 */
	public Integer getT60_accuracy() {
		try{
			if (_T60_accuracy==null){
				_T60_accuracy=getIntegerProperty("T60/accuracy");
				return _T60_accuracy;
			}else {
				return _T60_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T60/accuracy.
	 * @param v Value to Set.
	 */
	public void setT60_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T60/accuracy",v);
		_T60_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T60_responsetime=null;

	/**
	 * @return Returns the T60/responseTime.
	 */
	public Integer getT60_responsetime() {
		try{
			if (_T60_responsetime==null){
				_T60_responsetime=getIntegerProperty("T60/responseTime");
				return _T60_responsetime;
			}else {
				return _T60_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T60/responseTime.
	 * @param v Value to Set.
	 */
	public void setT60_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T60/responseTime",v);
		_T60_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T60_congruency=null;

	/**
	 * @return Returns the T60/congruency.
	 */
	public String getT60_congruency(){
		try{
			if (_T60_congruency==null){
				_T60_congruency=getStringProperty("T60/congruency");
				return _T60_congruency;
			}else {
				return _T60_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T60/congruency.
	 * @param v Value to Set.
	 */
	public void setT60_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T60/congruency",v);
		_T60_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T61_accuracy=null;

	/**
	 * @return Returns the T61/accuracy.
	 */
	public Integer getT61_accuracy() {
		try{
			if (_T61_accuracy==null){
				_T61_accuracy=getIntegerProperty("T61/accuracy");
				return _T61_accuracy;
			}else {
				return _T61_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T61/accuracy.
	 * @param v Value to Set.
	 */
	public void setT61_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T61/accuracy",v);
		_T61_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T61_responsetime=null;

	/**
	 * @return Returns the T61/responseTime.
	 */
	public Integer getT61_responsetime() {
		try{
			if (_T61_responsetime==null){
				_T61_responsetime=getIntegerProperty("T61/responseTime");
				return _T61_responsetime;
			}else {
				return _T61_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T61/responseTime.
	 * @param v Value to Set.
	 */
	public void setT61_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T61/responseTime",v);
		_T61_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T61_congruency=null;

	/**
	 * @return Returns the T61/congruency.
	 */
	public String getT61_congruency(){
		try{
			if (_T61_congruency==null){
				_T61_congruency=getStringProperty("T61/congruency");
				return _T61_congruency;
			}else {
				return _T61_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T61/congruency.
	 * @param v Value to Set.
	 */
	public void setT61_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T61/congruency",v);
		_T61_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T62_accuracy=null;

	/**
	 * @return Returns the T62/accuracy.
	 */
	public Integer getT62_accuracy() {
		try{
			if (_T62_accuracy==null){
				_T62_accuracy=getIntegerProperty("T62/accuracy");
				return _T62_accuracy;
			}else {
				return _T62_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T62/accuracy.
	 * @param v Value to Set.
	 */
	public void setT62_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T62/accuracy",v);
		_T62_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T62_responsetime=null;

	/**
	 * @return Returns the T62/responseTime.
	 */
	public Integer getT62_responsetime() {
		try{
			if (_T62_responsetime==null){
				_T62_responsetime=getIntegerProperty("T62/responseTime");
				return _T62_responsetime;
			}else {
				return _T62_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T62/responseTime.
	 * @param v Value to Set.
	 */
	public void setT62_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T62/responseTime",v);
		_T62_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T62_congruency=null;

	/**
	 * @return Returns the T62/congruency.
	 */
	public String getT62_congruency(){
		try{
			if (_T62_congruency==null){
				_T62_congruency=getStringProperty("T62/congruency");
				return _T62_congruency;
			}else {
				return _T62_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T62/congruency.
	 * @param v Value to Set.
	 */
	public void setT62_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T62/congruency",v);
		_T62_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T63_accuracy=null;

	/**
	 * @return Returns the T63/accuracy.
	 */
	public Integer getT63_accuracy() {
		try{
			if (_T63_accuracy==null){
				_T63_accuracy=getIntegerProperty("T63/accuracy");
				return _T63_accuracy;
			}else {
				return _T63_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T63/accuracy.
	 * @param v Value to Set.
	 */
	public void setT63_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T63/accuracy",v);
		_T63_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T63_responsetime=null;

	/**
	 * @return Returns the T63/responseTime.
	 */
	public Integer getT63_responsetime() {
		try{
			if (_T63_responsetime==null){
				_T63_responsetime=getIntegerProperty("T63/responseTime");
				return _T63_responsetime;
			}else {
				return _T63_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T63/responseTime.
	 * @param v Value to Set.
	 */
	public void setT63_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T63/responseTime",v);
		_T63_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T63_congruency=null;

	/**
	 * @return Returns the T63/congruency.
	 */
	public String getT63_congruency(){
		try{
			if (_T63_congruency==null){
				_T63_congruency=getStringProperty("T63/congruency");
				return _T63_congruency;
			}else {
				return _T63_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T63/congruency.
	 * @param v Value to Set.
	 */
	public void setT63_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T63/congruency",v);
		_T63_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T64_accuracy=null;

	/**
	 * @return Returns the T64/accuracy.
	 */
	public Integer getT64_accuracy() {
		try{
			if (_T64_accuracy==null){
				_T64_accuracy=getIntegerProperty("T64/accuracy");
				return _T64_accuracy;
			}else {
				return _T64_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T64/accuracy.
	 * @param v Value to Set.
	 */
	public void setT64_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T64/accuracy",v);
		_T64_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T64_responsetime=null;

	/**
	 * @return Returns the T64/responseTime.
	 */
	public Integer getT64_responsetime() {
		try{
			if (_T64_responsetime==null){
				_T64_responsetime=getIntegerProperty("T64/responseTime");
				return _T64_responsetime;
			}else {
				return _T64_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T64/responseTime.
	 * @param v Value to Set.
	 */
	public void setT64_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T64/responseTime",v);
		_T64_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T64_congruency=null;

	/**
	 * @return Returns the T64/congruency.
	 */
	public String getT64_congruency(){
		try{
			if (_T64_congruency==null){
				_T64_congruency=getStringProperty("T64/congruency");
				return _T64_congruency;
			}else {
				return _T64_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T64/congruency.
	 * @param v Value to Set.
	 */
	public void setT64_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T64/congruency",v);
		_T64_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T65_accuracy=null;

	/**
	 * @return Returns the T65/accuracy.
	 */
	public Integer getT65_accuracy() {
		try{
			if (_T65_accuracy==null){
				_T65_accuracy=getIntegerProperty("T65/accuracy");
				return _T65_accuracy;
			}else {
				return _T65_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T65/accuracy.
	 * @param v Value to Set.
	 */
	public void setT65_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T65/accuracy",v);
		_T65_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T65_responsetime=null;

	/**
	 * @return Returns the T65/responseTime.
	 */
	public Integer getT65_responsetime() {
		try{
			if (_T65_responsetime==null){
				_T65_responsetime=getIntegerProperty("T65/responseTime");
				return _T65_responsetime;
			}else {
				return _T65_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T65/responseTime.
	 * @param v Value to Set.
	 */
	public void setT65_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T65/responseTime",v);
		_T65_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T65_congruency=null;

	/**
	 * @return Returns the T65/congruency.
	 */
	public String getT65_congruency(){
		try{
			if (_T65_congruency==null){
				_T65_congruency=getStringProperty("T65/congruency");
				return _T65_congruency;
			}else {
				return _T65_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T65/congruency.
	 * @param v Value to Set.
	 */
	public void setT65_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T65/congruency",v);
		_T65_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T66_accuracy=null;

	/**
	 * @return Returns the T66/accuracy.
	 */
	public Integer getT66_accuracy() {
		try{
			if (_T66_accuracy==null){
				_T66_accuracy=getIntegerProperty("T66/accuracy");
				return _T66_accuracy;
			}else {
				return _T66_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T66/accuracy.
	 * @param v Value to Set.
	 */
	public void setT66_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T66/accuracy",v);
		_T66_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T66_responsetime=null;

	/**
	 * @return Returns the T66/responseTime.
	 */
	public Integer getT66_responsetime() {
		try{
			if (_T66_responsetime==null){
				_T66_responsetime=getIntegerProperty("T66/responseTime");
				return _T66_responsetime;
			}else {
				return _T66_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T66/responseTime.
	 * @param v Value to Set.
	 */
	public void setT66_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T66/responseTime",v);
		_T66_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T66_congruency=null;

	/**
	 * @return Returns the T66/congruency.
	 */
	public String getT66_congruency(){
		try{
			if (_T66_congruency==null){
				_T66_congruency=getStringProperty("T66/congruency");
				return _T66_congruency;
			}else {
				return _T66_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T66/congruency.
	 * @param v Value to Set.
	 */
	public void setT66_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T66/congruency",v);
		_T66_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T67_accuracy=null;

	/**
	 * @return Returns the T67/accuracy.
	 */
	public Integer getT67_accuracy() {
		try{
			if (_T67_accuracy==null){
				_T67_accuracy=getIntegerProperty("T67/accuracy");
				return _T67_accuracy;
			}else {
				return _T67_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T67/accuracy.
	 * @param v Value to Set.
	 */
	public void setT67_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T67/accuracy",v);
		_T67_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T67_responsetime=null;

	/**
	 * @return Returns the T67/responseTime.
	 */
	public Integer getT67_responsetime() {
		try{
			if (_T67_responsetime==null){
				_T67_responsetime=getIntegerProperty("T67/responseTime");
				return _T67_responsetime;
			}else {
				return _T67_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T67/responseTime.
	 * @param v Value to Set.
	 */
	public void setT67_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T67/responseTime",v);
		_T67_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T67_congruency=null;

	/**
	 * @return Returns the T67/congruency.
	 */
	public String getT67_congruency(){
		try{
			if (_T67_congruency==null){
				_T67_congruency=getStringProperty("T67/congruency");
				return _T67_congruency;
			}else {
				return _T67_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T67/congruency.
	 * @param v Value to Set.
	 */
	public void setT67_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T67/congruency",v);
		_T67_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T68_accuracy=null;

	/**
	 * @return Returns the T68/accuracy.
	 */
	public Integer getT68_accuracy() {
		try{
			if (_T68_accuracy==null){
				_T68_accuracy=getIntegerProperty("T68/accuracy");
				return _T68_accuracy;
			}else {
				return _T68_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T68/accuracy.
	 * @param v Value to Set.
	 */
	public void setT68_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T68/accuracy",v);
		_T68_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T68_responsetime=null;

	/**
	 * @return Returns the T68/responseTime.
	 */
	public Integer getT68_responsetime() {
		try{
			if (_T68_responsetime==null){
				_T68_responsetime=getIntegerProperty("T68/responseTime");
				return _T68_responsetime;
			}else {
				return _T68_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T68/responseTime.
	 * @param v Value to Set.
	 */
	public void setT68_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T68/responseTime",v);
		_T68_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T68_congruency=null;

	/**
	 * @return Returns the T68/congruency.
	 */
	public String getT68_congruency(){
		try{
			if (_T68_congruency==null){
				_T68_congruency=getStringProperty("T68/congruency");
				return _T68_congruency;
			}else {
				return _T68_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T68/congruency.
	 * @param v Value to Set.
	 */
	public void setT68_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T68/congruency",v);
		_T68_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T69_accuracy=null;

	/**
	 * @return Returns the T69/accuracy.
	 */
	public Integer getT69_accuracy() {
		try{
			if (_T69_accuracy==null){
				_T69_accuracy=getIntegerProperty("T69/accuracy");
				return _T69_accuracy;
			}else {
				return _T69_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T69/accuracy.
	 * @param v Value to Set.
	 */
	public void setT69_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T69/accuracy",v);
		_T69_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T69_responsetime=null;

	/**
	 * @return Returns the T69/responseTime.
	 */
	public Integer getT69_responsetime() {
		try{
			if (_T69_responsetime==null){
				_T69_responsetime=getIntegerProperty("T69/responseTime");
				return _T69_responsetime;
			}else {
				return _T69_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T69/responseTime.
	 * @param v Value to Set.
	 */
	public void setT69_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T69/responseTime",v);
		_T69_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T69_congruency=null;

	/**
	 * @return Returns the T69/congruency.
	 */
	public String getT69_congruency(){
		try{
			if (_T69_congruency==null){
				_T69_congruency=getStringProperty("T69/congruency");
				return _T69_congruency;
			}else {
				return _T69_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T69/congruency.
	 * @param v Value to Set.
	 */
	public void setT69_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T69/congruency",v);
		_T69_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T70_accuracy=null;

	/**
	 * @return Returns the T70/accuracy.
	 */
	public Integer getT70_accuracy() {
		try{
			if (_T70_accuracy==null){
				_T70_accuracy=getIntegerProperty("T70/accuracy");
				return _T70_accuracy;
			}else {
				return _T70_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T70/accuracy.
	 * @param v Value to Set.
	 */
	public void setT70_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T70/accuracy",v);
		_T70_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T70_responsetime=null;

	/**
	 * @return Returns the T70/responseTime.
	 */
	public Integer getT70_responsetime() {
		try{
			if (_T70_responsetime==null){
				_T70_responsetime=getIntegerProperty("T70/responseTime");
				return _T70_responsetime;
			}else {
				return _T70_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T70/responseTime.
	 * @param v Value to Set.
	 */
	public void setT70_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T70/responseTime",v);
		_T70_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T70_congruency=null;

	/**
	 * @return Returns the T70/congruency.
	 */
	public String getT70_congruency(){
		try{
			if (_T70_congruency==null){
				_T70_congruency=getStringProperty("T70/congruency");
				return _T70_congruency;
			}else {
				return _T70_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T70/congruency.
	 * @param v Value to Set.
	 */
	public void setT70_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T70/congruency",v);
		_T70_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T71_accuracy=null;

	/**
	 * @return Returns the T71/accuracy.
	 */
	public Integer getT71_accuracy() {
		try{
			if (_T71_accuracy==null){
				_T71_accuracy=getIntegerProperty("T71/accuracy");
				return _T71_accuracy;
			}else {
				return _T71_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T71/accuracy.
	 * @param v Value to Set.
	 */
	public void setT71_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T71/accuracy",v);
		_T71_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T71_responsetime=null;

	/**
	 * @return Returns the T71/responseTime.
	 */
	public Integer getT71_responsetime() {
		try{
			if (_T71_responsetime==null){
				_T71_responsetime=getIntegerProperty("T71/responseTime");
				return _T71_responsetime;
			}else {
				return _T71_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T71/responseTime.
	 * @param v Value to Set.
	 */
	public void setT71_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T71/responseTime",v);
		_T71_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T71_congruency=null;

	/**
	 * @return Returns the T71/congruency.
	 */
	public String getT71_congruency(){
		try{
			if (_T71_congruency==null){
				_T71_congruency=getStringProperty("T71/congruency");
				return _T71_congruency;
			}else {
				return _T71_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T71/congruency.
	 * @param v Value to Set.
	 */
	public void setT71_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T71/congruency",v);
		_T71_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T72_accuracy=null;

	/**
	 * @return Returns the T72/accuracy.
	 */
	public Integer getT72_accuracy() {
		try{
			if (_T72_accuracy==null){
				_T72_accuracy=getIntegerProperty("T72/accuracy");
				return _T72_accuracy;
			}else {
				return _T72_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T72/accuracy.
	 * @param v Value to Set.
	 */
	public void setT72_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T72/accuracy",v);
		_T72_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T72_responsetime=null;

	/**
	 * @return Returns the T72/responseTime.
	 */
	public Integer getT72_responsetime() {
		try{
			if (_T72_responsetime==null){
				_T72_responsetime=getIntegerProperty("T72/responseTime");
				return _T72_responsetime;
			}else {
				return _T72_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T72/responseTime.
	 * @param v Value to Set.
	 */
	public void setT72_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T72/responseTime",v);
		_T72_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T72_congruency=null;

	/**
	 * @return Returns the T72/congruency.
	 */
	public String getT72_congruency(){
		try{
			if (_T72_congruency==null){
				_T72_congruency=getStringProperty("T72/congruency");
				return _T72_congruency;
			}else {
				return _T72_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T72/congruency.
	 * @param v Value to Set.
	 */
	public void setT72_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T72/congruency",v);
		_T72_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T73_accuracy=null;

	/**
	 * @return Returns the T73/accuracy.
	 */
	public Integer getT73_accuracy() {
		try{
			if (_T73_accuracy==null){
				_T73_accuracy=getIntegerProperty("T73/accuracy");
				return _T73_accuracy;
			}else {
				return _T73_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T73/accuracy.
	 * @param v Value to Set.
	 */
	public void setT73_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T73/accuracy",v);
		_T73_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T73_responsetime=null;

	/**
	 * @return Returns the T73/responseTime.
	 */
	public Integer getT73_responsetime() {
		try{
			if (_T73_responsetime==null){
				_T73_responsetime=getIntegerProperty("T73/responseTime");
				return _T73_responsetime;
			}else {
				return _T73_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T73/responseTime.
	 * @param v Value to Set.
	 */
	public void setT73_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T73/responseTime",v);
		_T73_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T73_congruency=null;

	/**
	 * @return Returns the T73/congruency.
	 */
	public String getT73_congruency(){
		try{
			if (_T73_congruency==null){
				_T73_congruency=getStringProperty("T73/congruency");
				return _T73_congruency;
			}else {
				return _T73_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T73/congruency.
	 * @param v Value to Set.
	 */
	public void setT73_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T73/congruency",v);
		_T73_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T74_accuracy=null;

	/**
	 * @return Returns the T74/accuracy.
	 */
	public Integer getT74_accuracy() {
		try{
			if (_T74_accuracy==null){
				_T74_accuracy=getIntegerProperty("T74/accuracy");
				return _T74_accuracy;
			}else {
				return _T74_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T74/accuracy.
	 * @param v Value to Set.
	 */
	public void setT74_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T74/accuracy",v);
		_T74_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T74_responsetime=null;

	/**
	 * @return Returns the T74/responseTime.
	 */
	public Integer getT74_responsetime() {
		try{
			if (_T74_responsetime==null){
				_T74_responsetime=getIntegerProperty("T74/responseTime");
				return _T74_responsetime;
			}else {
				return _T74_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T74/responseTime.
	 * @param v Value to Set.
	 */
	public void setT74_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T74/responseTime",v);
		_T74_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T74_congruency=null;

	/**
	 * @return Returns the T74/congruency.
	 */
	public String getT74_congruency(){
		try{
			if (_T74_congruency==null){
				_T74_congruency=getStringProperty("T74/congruency");
				return _T74_congruency;
			}else {
				return _T74_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T74/congruency.
	 * @param v Value to Set.
	 */
	public void setT74_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T74/congruency",v);
		_T74_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T75_accuracy=null;

	/**
	 * @return Returns the T75/accuracy.
	 */
	public Integer getT75_accuracy() {
		try{
			if (_T75_accuracy==null){
				_T75_accuracy=getIntegerProperty("T75/accuracy");
				return _T75_accuracy;
			}else {
				return _T75_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T75/accuracy.
	 * @param v Value to Set.
	 */
	public void setT75_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T75/accuracy",v);
		_T75_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T75_responsetime=null;

	/**
	 * @return Returns the T75/responseTime.
	 */
	public Integer getT75_responsetime() {
		try{
			if (_T75_responsetime==null){
				_T75_responsetime=getIntegerProperty("T75/responseTime");
				return _T75_responsetime;
			}else {
				return _T75_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T75/responseTime.
	 * @param v Value to Set.
	 */
	public void setT75_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T75/responseTime",v);
		_T75_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T75_congruency=null;

	/**
	 * @return Returns the T75/congruency.
	 */
	public String getT75_congruency(){
		try{
			if (_T75_congruency==null){
				_T75_congruency=getStringProperty("T75/congruency");
				return _T75_congruency;
			}else {
				return _T75_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T75/congruency.
	 * @param v Value to Set.
	 */
	public void setT75_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T75/congruency",v);
		_T75_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T76_accuracy=null;

	/**
	 * @return Returns the T76/accuracy.
	 */
	public Integer getT76_accuracy() {
		try{
			if (_T76_accuracy==null){
				_T76_accuracy=getIntegerProperty("T76/accuracy");
				return _T76_accuracy;
			}else {
				return _T76_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T76/accuracy.
	 * @param v Value to Set.
	 */
	public void setT76_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T76/accuracy",v);
		_T76_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T76_responsetime=null;

	/**
	 * @return Returns the T76/responseTime.
	 */
	public Integer getT76_responsetime() {
		try{
			if (_T76_responsetime==null){
				_T76_responsetime=getIntegerProperty("T76/responseTime");
				return _T76_responsetime;
			}else {
				return _T76_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T76/responseTime.
	 * @param v Value to Set.
	 */
	public void setT76_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T76/responseTime",v);
		_T76_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T76_congruency=null;

	/**
	 * @return Returns the T76/congruency.
	 */
	public String getT76_congruency(){
		try{
			if (_T76_congruency==null){
				_T76_congruency=getStringProperty("T76/congruency");
				return _T76_congruency;
			}else {
				return _T76_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T76/congruency.
	 * @param v Value to Set.
	 */
	public void setT76_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T76/congruency",v);
		_T76_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T77_accuracy=null;

	/**
	 * @return Returns the T77/accuracy.
	 */
	public Integer getT77_accuracy() {
		try{
			if (_T77_accuracy==null){
				_T77_accuracy=getIntegerProperty("T77/accuracy");
				return _T77_accuracy;
			}else {
				return _T77_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T77/accuracy.
	 * @param v Value to Set.
	 */
	public void setT77_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T77/accuracy",v);
		_T77_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T77_responsetime=null;

	/**
	 * @return Returns the T77/responseTime.
	 */
	public Integer getT77_responsetime() {
		try{
			if (_T77_responsetime==null){
				_T77_responsetime=getIntegerProperty("T77/responseTime");
				return _T77_responsetime;
			}else {
				return _T77_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T77/responseTime.
	 * @param v Value to Set.
	 */
	public void setT77_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T77/responseTime",v);
		_T77_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T77_congruency=null;

	/**
	 * @return Returns the T77/congruency.
	 */
	public String getT77_congruency(){
		try{
			if (_T77_congruency==null){
				_T77_congruency=getStringProperty("T77/congruency");
				return _T77_congruency;
			}else {
				return _T77_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T77/congruency.
	 * @param v Value to Set.
	 */
	public void setT77_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T77/congruency",v);
		_T77_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T78_accuracy=null;

	/**
	 * @return Returns the T78/accuracy.
	 */
	public Integer getT78_accuracy() {
		try{
			if (_T78_accuracy==null){
				_T78_accuracy=getIntegerProperty("T78/accuracy");
				return _T78_accuracy;
			}else {
				return _T78_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T78/accuracy.
	 * @param v Value to Set.
	 */
	public void setT78_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T78/accuracy",v);
		_T78_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T78_responsetime=null;

	/**
	 * @return Returns the T78/responseTime.
	 */
	public Integer getT78_responsetime() {
		try{
			if (_T78_responsetime==null){
				_T78_responsetime=getIntegerProperty("T78/responseTime");
				return _T78_responsetime;
			}else {
				return _T78_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T78/responseTime.
	 * @param v Value to Set.
	 */
	public void setT78_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T78/responseTime",v);
		_T78_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T78_congruency=null;

	/**
	 * @return Returns the T78/congruency.
	 */
	public String getT78_congruency(){
		try{
			if (_T78_congruency==null){
				_T78_congruency=getStringProperty("T78/congruency");
				return _T78_congruency;
			}else {
				return _T78_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T78/congruency.
	 * @param v Value to Set.
	 */
	public void setT78_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T78/congruency",v);
		_T78_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T79_accuracy=null;

	/**
	 * @return Returns the T79/accuracy.
	 */
	public Integer getT79_accuracy() {
		try{
			if (_T79_accuracy==null){
				_T79_accuracy=getIntegerProperty("T79/accuracy");
				return _T79_accuracy;
			}else {
				return _T79_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T79/accuracy.
	 * @param v Value to Set.
	 */
	public void setT79_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T79/accuracy",v);
		_T79_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T79_responsetime=null;

	/**
	 * @return Returns the T79/responseTime.
	 */
	public Integer getT79_responsetime() {
		try{
			if (_T79_responsetime==null){
				_T79_responsetime=getIntegerProperty("T79/responseTime");
				return _T79_responsetime;
			}else {
				return _T79_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T79/responseTime.
	 * @param v Value to Set.
	 */
	public void setT79_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T79/responseTime",v);
		_T79_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T79_congruency=null;

	/**
	 * @return Returns the T79/congruency.
	 */
	public String getT79_congruency(){
		try{
			if (_T79_congruency==null){
				_T79_congruency=getStringProperty("T79/congruency");
				return _T79_congruency;
			}else {
				return _T79_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T79/congruency.
	 * @param v Value to Set.
	 */
	public void setT79_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T79/congruency",v);
		_T79_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T80_accuracy=null;

	/**
	 * @return Returns the T80/accuracy.
	 */
	public Integer getT80_accuracy() {
		try{
			if (_T80_accuracy==null){
				_T80_accuracy=getIntegerProperty("T80/accuracy");
				return _T80_accuracy;
			}else {
				return _T80_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T80/accuracy.
	 * @param v Value to Set.
	 */
	public void setT80_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T80/accuracy",v);
		_T80_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T80_responsetime=null;

	/**
	 * @return Returns the T80/responseTime.
	 */
	public Integer getT80_responsetime() {
		try{
			if (_T80_responsetime==null){
				_T80_responsetime=getIntegerProperty("T80/responseTime");
				return _T80_responsetime;
			}else {
				return _T80_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T80/responseTime.
	 * @param v Value to Set.
	 */
	public void setT80_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T80/responseTime",v);
		_T80_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T80_congruency=null;

	/**
	 * @return Returns the T80/congruency.
	 */
	public String getT80_congruency(){
		try{
			if (_T80_congruency==null){
				_T80_congruency=getStringProperty("T80/congruency");
				return _T80_congruency;
			}else {
				return _T80_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T80/congruency.
	 * @param v Value to Set.
	 */
	public void setT80_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T80/congruency",v);
		_T80_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T81_accuracy=null;

	/**
	 * @return Returns the T81/accuracy.
	 */
	public Integer getT81_accuracy() {
		try{
			if (_T81_accuracy==null){
				_T81_accuracy=getIntegerProperty("T81/accuracy");
				return _T81_accuracy;
			}else {
				return _T81_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T81/accuracy.
	 * @param v Value to Set.
	 */
	public void setT81_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T81/accuracy",v);
		_T81_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T81_responsetime=null;

	/**
	 * @return Returns the T81/responseTime.
	 */
	public Integer getT81_responsetime() {
		try{
			if (_T81_responsetime==null){
				_T81_responsetime=getIntegerProperty("T81/responseTime");
				return _T81_responsetime;
			}else {
				return _T81_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T81/responseTime.
	 * @param v Value to Set.
	 */
	public void setT81_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T81/responseTime",v);
		_T81_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T81_congruency=null;

	/**
	 * @return Returns the T81/congruency.
	 */
	public String getT81_congruency(){
		try{
			if (_T81_congruency==null){
				_T81_congruency=getStringProperty("T81/congruency");
				return _T81_congruency;
			}else {
				return _T81_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T81/congruency.
	 * @param v Value to Set.
	 */
	public void setT81_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T81/congruency",v);
		_T81_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T82_accuracy=null;

	/**
	 * @return Returns the T82/accuracy.
	 */
	public Integer getT82_accuracy() {
		try{
			if (_T82_accuracy==null){
				_T82_accuracy=getIntegerProperty("T82/accuracy");
				return _T82_accuracy;
			}else {
				return _T82_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T82/accuracy.
	 * @param v Value to Set.
	 */
	public void setT82_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T82/accuracy",v);
		_T82_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T82_responsetime=null;

	/**
	 * @return Returns the T82/responseTime.
	 */
	public Integer getT82_responsetime() {
		try{
			if (_T82_responsetime==null){
				_T82_responsetime=getIntegerProperty("T82/responseTime");
				return _T82_responsetime;
			}else {
				return _T82_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T82/responseTime.
	 * @param v Value to Set.
	 */
	public void setT82_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T82/responseTime",v);
		_T82_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T82_congruency=null;

	/**
	 * @return Returns the T82/congruency.
	 */
	public String getT82_congruency(){
		try{
			if (_T82_congruency==null){
				_T82_congruency=getStringProperty("T82/congruency");
				return _T82_congruency;
			}else {
				return _T82_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T82/congruency.
	 * @param v Value to Set.
	 */
	public void setT82_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T82/congruency",v);
		_T82_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T83_accuracy=null;

	/**
	 * @return Returns the T83/accuracy.
	 */
	public Integer getT83_accuracy() {
		try{
			if (_T83_accuracy==null){
				_T83_accuracy=getIntegerProperty("T83/accuracy");
				return _T83_accuracy;
			}else {
				return _T83_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T83/accuracy.
	 * @param v Value to Set.
	 */
	public void setT83_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T83/accuracy",v);
		_T83_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T83_responsetime=null;

	/**
	 * @return Returns the T83/responseTime.
	 */
	public Integer getT83_responsetime() {
		try{
			if (_T83_responsetime==null){
				_T83_responsetime=getIntegerProperty("T83/responseTime");
				return _T83_responsetime;
			}else {
				return _T83_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T83/responseTime.
	 * @param v Value to Set.
	 */
	public void setT83_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T83/responseTime",v);
		_T83_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T83_congruency=null;

	/**
	 * @return Returns the T83/congruency.
	 */
	public String getT83_congruency(){
		try{
			if (_T83_congruency==null){
				_T83_congruency=getStringProperty("T83/congruency");
				return _T83_congruency;
			}else {
				return _T83_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T83/congruency.
	 * @param v Value to Set.
	 */
	public void setT83_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T83/congruency",v);
		_T83_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T84_accuracy=null;

	/**
	 * @return Returns the T84/accuracy.
	 */
	public Integer getT84_accuracy() {
		try{
			if (_T84_accuracy==null){
				_T84_accuracy=getIntegerProperty("T84/accuracy");
				return _T84_accuracy;
			}else {
				return _T84_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T84/accuracy.
	 * @param v Value to Set.
	 */
	public void setT84_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T84/accuracy",v);
		_T84_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T84_responsetime=null;

	/**
	 * @return Returns the T84/responseTime.
	 */
	public Integer getT84_responsetime() {
		try{
			if (_T84_responsetime==null){
				_T84_responsetime=getIntegerProperty("T84/responseTime");
				return _T84_responsetime;
			}else {
				return _T84_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T84/responseTime.
	 * @param v Value to Set.
	 */
	public void setT84_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T84/responseTime",v);
		_T84_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T84_congruency=null;

	/**
	 * @return Returns the T84/congruency.
	 */
	public String getT84_congruency(){
		try{
			if (_T84_congruency==null){
				_T84_congruency=getStringProperty("T84/congruency");
				return _T84_congruency;
			}else {
				return _T84_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T84/congruency.
	 * @param v Value to Set.
	 */
	public void setT84_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T84/congruency",v);
		_T84_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T85_accuracy=null;

	/**
	 * @return Returns the T85/accuracy.
	 */
	public Integer getT85_accuracy() {
		try{
			if (_T85_accuracy==null){
				_T85_accuracy=getIntegerProperty("T85/accuracy");
				return _T85_accuracy;
			}else {
				return _T85_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T85/accuracy.
	 * @param v Value to Set.
	 */
	public void setT85_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T85/accuracy",v);
		_T85_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T85_responsetime=null;

	/**
	 * @return Returns the T85/responseTime.
	 */
	public Integer getT85_responsetime() {
		try{
			if (_T85_responsetime==null){
				_T85_responsetime=getIntegerProperty("T85/responseTime");
				return _T85_responsetime;
			}else {
				return _T85_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T85/responseTime.
	 * @param v Value to Set.
	 */
	public void setT85_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T85/responseTime",v);
		_T85_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T85_congruency=null;

	/**
	 * @return Returns the T85/congruency.
	 */
	public String getT85_congruency(){
		try{
			if (_T85_congruency==null){
				_T85_congruency=getStringProperty("T85/congruency");
				return _T85_congruency;
			}else {
				return _T85_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T85/congruency.
	 * @param v Value to Set.
	 */
	public void setT85_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T85/congruency",v);
		_T85_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T86_accuracy=null;

	/**
	 * @return Returns the T86/accuracy.
	 */
	public Integer getT86_accuracy() {
		try{
			if (_T86_accuracy==null){
				_T86_accuracy=getIntegerProperty("T86/accuracy");
				return _T86_accuracy;
			}else {
				return _T86_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T86/accuracy.
	 * @param v Value to Set.
	 */
	public void setT86_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T86/accuracy",v);
		_T86_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T86_responsetime=null;

	/**
	 * @return Returns the T86/responseTime.
	 */
	public Integer getT86_responsetime() {
		try{
			if (_T86_responsetime==null){
				_T86_responsetime=getIntegerProperty("T86/responseTime");
				return _T86_responsetime;
			}else {
				return _T86_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T86/responseTime.
	 * @param v Value to Set.
	 */
	public void setT86_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T86/responseTime",v);
		_T86_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T86_congruency=null;

	/**
	 * @return Returns the T86/congruency.
	 */
	public String getT86_congruency(){
		try{
			if (_T86_congruency==null){
				_T86_congruency=getStringProperty("T86/congruency");
				return _T86_congruency;
			}else {
				return _T86_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T86/congruency.
	 * @param v Value to Set.
	 */
	public void setT86_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T86/congruency",v);
		_T86_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T87_accuracy=null;

	/**
	 * @return Returns the T87/accuracy.
	 */
	public Integer getT87_accuracy() {
		try{
			if (_T87_accuracy==null){
				_T87_accuracy=getIntegerProperty("T87/accuracy");
				return _T87_accuracy;
			}else {
				return _T87_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T87/accuracy.
	 * @param v Value to Set.
	 */
	public void setT87_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T87/accuracy",v);
		_T87_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T87_responsetime=null;

	/**
	 * @return Returns the T87/responseTime.
	 */
	public Integer getT87_responsetime() {
		try{
			if (_T87_responsetime==null){
				_T87_responsetime=getIntegerProperty("T87/responseTime");
				return _T87_responsetime;
			}else {
				return _T87_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T87/responseTime.
	 * @param v Value to Set.
	 */
	public void setT87_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T87/responseTime",v);
		_T87_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T87_congruency=null;

	/**
	 * @return Returns the T87/congruency.
	 */
	public String getT87_congruency(){
		try{
			if (_T87_congruency==null){
				_T87_congruency=getStringProperty("T87/congruency");
				return _T87_congruency;
			}else {
				return _T87_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T87/congruency.
	 * @param v Value to Set.
	 */
	public void setT87_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T87/congruency",v);
		_T87_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T88_accuracy=null;

	/**
	 * @return Returns the T88/accuracy.
	 */
	public Integer getT88_accuracy() {
		try{
			if (_T88_accuracy==null){
				_T88_accuracy=getIntegerProperty("T88/accuracy");
				return _T88_accuracy;
			}else {
				return _T88_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T88/accuracy.
	 * @param v Value to Set.
	 */
	public void setT88_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T88/accuracy",v);
		_T88_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T88_responsetime=null;

	/**
	 * @return Returns the T88/responseTime.
	 */
	public Integer getT88_responsetime() {
		try{
			if (_T88_responsetime==null){
				_T88_responsetime=getIntegerProperty("T88/responseTime");
				return _T88_responsetime;
			}else {
				return _T88_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T88/responseTime.
	 * @param v Value to Set.
	 */
	public void setT88_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T88/responseTime",v);
		_T88_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T88_congruency=null;

	/**
	 * @return Returns the T88/congruency.
	 */
	public String getT88_congruency(){
		try{
			if (_T88_congruency==null){
				_T88_congruency=getStringProperty("T88/congruency");
				return _T88_congruency;
			}else {
				return _T88_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T88/congruency.
	 * @param v Value to Set.
	 */
	public void setT88_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T88/congruency",v);
		_T88_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T89_accuracy=null;

	/**
	 * @return Returns the T89/accuracy.
	 */
	public Integer getT89_accuracy() {
		try{
			if (_T89_accuracy==null){
				_T89_accuracy=getIntegerProperty("T89/accuracy");
				return _T89_accuracy;
			}else {
				return _T89_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T89/accuracy.
	 * @param v Value to Set.
	 */
	public void setT89_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T89/accuracy",v);
		_T89_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T89_responsetime=null;

	/**
	 * @return Returns the T89/responseTime.
	 */
	public Integer getT89_responsetime() {
		try{
			if (_T89_responsetime==null){
				_T89_responsetime=getIntegerProperty("T89/responseTime");
				return _T89_responsetime;
			}else {
				return _T89_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T89/responseTime.
	 * @param v Value to Set.
	 */
	public void setT89_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T89/responseTime",v);
		_T89_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T89_congruency=null;

	/**
	 * @return Returns the T89/congruency.
	 */
	public String getT89_congruency(){
		try{
			if (_T89_congruency==null){
				_T89_congruency=getStringProperty("T89/congruency");
				return _T89_congruency;
			}else {
				return _T89_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T89/congruency.
	 * @param v Value to Set.
	 */
	public void setT89_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T89/congruency",v);
		_T89_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T90_accuracy=null;

	/**
	 * @return Returns the T90/accuracy.
	 */
	public Integer getT90_accuracy() {
		try{
			if (_T90_accuracy==null){
				_T90_accuracy=getIntegerProperty("T90/accuracy");
				return _T90_accuracy;
			}else {
				return _T90_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T90/accuracy.
	 * @param v Value to Set.
	 */
	public void setT90_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T90/accuracy",v);
		_T90_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T90_responsetime=null;

	/**
	 * @return Returns the T90/responseTime.
	 */
	public Integer getT90_responsetime() {
		try{
			if (_T90_responsetime==null){
				_T90_responsetime=getIntegerProperty("T90/responseTime");
				return _T90_responsetime;
			}else {
				return _T90_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T90/responseTime.
	 * @param v Value to Set.
	 */
	public void setT90_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T90/responseTime",v);
		_T90_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T90_congruency=null;

	/**
	 * @return Returns the T90/congruency.
	 */
	public String getT90_congruency(){
		try{
			if (_T90_congruency==null){
				_T90_congruency=getStringProperty("T90/congruency");
				return _T90_congruency;
			}else {
				return _T90_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T90/congruency.
	 * @param v Value to Set.
	 */
	public void setT90_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T90/congruency",v);
		_T90_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T91_accuracy=null;

	/**
	 * @return Returns the T91/accuracy.
	 */
	public Integer getT91_accuracy() {
		try{
			if (_T91_accuracy==null){
				_T91_accuracy=getIntegerProperty("T91/accuracy");
				return _T91_accuracy;
			}else {
				return _T91_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T91/accuracy.
	 * @param v Value to Set.
	 */
	public void setT91_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T91/accuracy",v);
		_T91_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T91_responsetime=null;

	/**
	 * @return Returns the T91/responseTime.
	 */
	public Integer getT91_responsetime() {
		try{
			if (_T91_responsetime==null){
				_T91_responsetime=getIntegerProperty("T91/responseTime");
				return _T91_responsetime;
			}else {
				return _T91_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T91/responseTime.
	 * @param v Value to Set.
	 */
	public void setT91_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T91/responseTime",v);
		_T91_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T91_congruency=null;

	/**
	 * @return Returns the T91/congruency.
	 */
	public String getT91_congruency(){
		try{
			if (_T91_congruency==null){
				_T91_congruency=getStringProperty("T91/congruency");
				return _T91_congruency;
			}else {
				return _T91_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T91/congruency.
	 * @param v Value to Set.
	 */
	public void setT91_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T91/congruency",v);
		_T91_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T92_accuracy=null;

	/**
	 * @return Returns the T92/accuracy.
	 */
	public Integer getT92_accuracy() {
		try{
			if (_T92_accuracy==null){
				_T92_accuracy=getIntegerProperty("T92/accuracy");
				return _T92_accuracy;
			}else {
				return _T92_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T92/accuracy.
	 * @param v Value to Set.
	 */
	public void setT92_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T92/accuracy",v);
		_T92_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T92_responsetime=null;

	/**
	 * @return Returns the T92/responseTime.
	 */
	public Integer getT92_responsetime() {
		try{
			if (_T92_responsetime==null){
				_T92_responsetime=getIntegerProperty("T92/responseTime");
				return _T92_responsetime;
			}else {
				return _T92_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T92/responseTime.
	 * @param v Value to Set.
	 */
	public void setT92_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T92/responseTime",v);
		_T92_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T92_congruency=null;

	/**
	 * @return Returns the T92/congruency.
	 */
	public String getT92_congruency(){
		try{
			if (_T92_congruency==null){
				_T92_congruency=getStringProperty("T92/congruency");
				return _T92_congruency;
			}else {
				return _T92_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T92/congruency.
	 * @param v Value to Set.
	 */
	public void setT92_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T92/congruency",v);
		_T92_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T93_accuracy=null;

	/**
	 * @return Returns the T93/accuracy.
	 */
	public Integer getT93_accuracy() {
		try{
			if (_T93_accuracy==null){
				_T93_accuracy=getIntegerProperty("T93/accuracy");
				return _T93_accuracy;
			}else {
				return _T93_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T93/accuracy.
	 * @param v Value to Set.
	 */
	public void setT93_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T93/accuracy",v);
		_T93_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T93_responsetime=null;

	/**
	 * @return Returns the T93/responseTime.
	 */
	public Integer getT93_responsetime() {
		try{
			if (_T93_responsetime==null){
				_T93_responsetime=getIntegerProperty("T93/responseTime");
				return _T93_responsetime;
			}else {
				return _T93_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T93/responseTime.
	 * @param v Value to Set.
	 */
	public void setT93_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T93/responseTime",v);
		_T93_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T93_congruency=null;

	/**
	 * @return Returns the T93/congruency.
	 */
	public String getT93_congruency(){
		try{
			if (_T93_congruency==null){
				_T93_congruency=getStringProperty("T93/congruency");
				return _T93_congruency;
			}else {
				return _T93_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T93/congruency.
	 * @param v Value to Set.
	 */
	public void setT93_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T93/congruency",v);
		_T93_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T94_accuracy=null;

	/**
	 * @return Returns the T94/accuracy.
	 */
	public Integer getT94_accuracy() {
		try{
			if (_T94_accuracy==null){
				_T94_accuracy=getIntegerProperty("T94/accuracy");
				return _T94_accuracy;
			}else {
				return _T94_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T94/accuracy.
	 * @param v Value to Set.
	 */
	public void setT94_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T94/accuracy",v);
		_T94_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T94_responsetime=null;

	/**
	 * @return Returns the T94/responseTime.
	 */
	public Integer getT94_responsetime() {
		try{
			if (_T94_responsetime==null){
				_T94_responsetime=getIntegerProperty("T94/responseTime");
				return _T94_responsetime;
			}else {
				return _T94_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T94/responseTime.
	 * @param v Value to Set.
	 */
	public void setT94_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T94/responseTime",v);
		_T94_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T94_congruency=null;

	/**
	 * @return Returns the T94/congruency.
	 */
	public String getT94_congruency(){
		try{
			if (_T94_congruency==null){
				_T94_congruency=getStringProperty("T94/congruency");
				return _T94_congruency;
			}else {
				return _T94_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T94/congruency.
	 * @param v Value to Set.
	 */
	public void setT94_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T94/congruency",v);
		_T94_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T95_accuracy=null;

	/**
	 * @return Returns the T95/accuracy.
	 */
	public Integer getT95_accuracy() {
		try{
			if (_T95_accuracy==null){
				_T95_accuracy=getIntegerProperty("T95/accuracy");
				return _T95_accuracy;
			}else {
				return _T95_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T95/accuracy.
	 * @param v Value to Set.
	 */
	public void setT95_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T95/accuracy",v);
		_T95_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T95_responsetime=null;

	/**
	 * @return Returns the T95/responseTime.
	 */
	public Integer getT95_responsetime() {
		try{
			if (_T95_responsetime==null){
				_T95_responsetime=getIntegerProperty("T95/responseTime");
				return _T95_responsetime;
			}else {
				return _T95_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T95/responseTime.
	 * @param v Value to Set.
	 */
	public void setT95_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T95/responseTime",v);
		_T95_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T95_congruency=null;

	/**
	 * @return Returns the T95/congruency.
	 */
	public String getT95_congruency(){
		try{
			if (_T95_congruency==null){
				_T95_congruency=getStringProperty("T95/congruency");
				return _T95_congruency;
			}else {
				return _T95_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T95/congruency.
	 * @param v Value to Set.
	 */
	public void setT95_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T95/congruency",v);
		_T95_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T96_accuracy=null;

	/**
	 * @return Returns the T96/accuracy.
	 */
	public Integer getT96_accuracy() {
		try{
			if (_T96_accuracy==null){
				_T96_accuracy=getIntegerProperty("T96/accuracy");
				return _T96_accuracy;
			}else {
				return _T96_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T96/accuracy.
	 * @param v Value to Set.
	 */
	public void setT96_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T96/accuracy",v);
		_T96_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T96_responsetime=null;

	/**
	 * @return Returns the T96/responseTime.
	 */
	public Integer getT96_responsetime() {
		try{
			if (_T96_responsetime==null){
				_T96_responsetime=getIntegerProperty("T96/responseTime");
				return _T96_responsetime;
			}else {
				return _T96_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T96/responseTime.
	 * @param v Value to Set.
	 */
	public void setT96_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T96/responseTime",v);
		_T96_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T96_congruency=null;

	/**
	 * @return Returns the T96/congruency.
	 */
	public String getT96_congruency(){
		try{
			if (_T96_congruency==null){
				_T96_congruency=getStringProperty("T96/congruency");
				return _T96_congruency;
			}else {
				return _T96_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T96/congruency.
	 * @param v Value to Set.
	 */
	public void setT96_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T96/congruency",v);
		_T96_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T97_accuracy=null;

	/**
	 * @return Returns the T97/accuracy.
	 */
	public Integer getT97_accuracy() {
		try{
			if (_T97_accuracy==null){
				_T97_accuracy=getIntegerProperty("T97/accuracy");
				return _T97_accuracy;
			}else {
				return _T97_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T97/accuracy.
	 * @param v Value to Set.
	 */
	public void setT97_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T97/accuracy",v);
		_T97_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T97_responsetime=null;

	/**
	 * @return Returns the T97/responseTime.
	 */
	public Integer getT97_responsetime() {
		try{
			if (_T97_responsetime==null){
				_T97_responsetime=getIntegerProperty("T97/responseTime");
				return _T97_responsetime;
			}else {
				return _T97_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T97/responseTime.
	 * @param v Value to Set.
	 */
	public void setT97_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T97/responseTime",v);
		_T97_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T97_congruency=null;

	/**
	 * @return Returns the T97/congruency.
	 */
	public String getT97_congruency(){
		try{
			if (_T97_congruency==null){
				_T97_congruency=getStringProperty("T97/congruency");
				return _T97_congruency;
			}else {
				return _T97_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T97/congruency.
	 * @param v Value to Set.
	 */
	public void setT97_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T97/congruency",v);
		_T97_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T98_accuracy=null;

	/**
	 * @return Returns the T98/accuracy.
	 */
	public Integer getT98_accuracy() {
		try{
			if (_T98_accuracy==null){
				_T98_accuracy=getIntegerProperty("T98/accuracy");
				return _T98_accuracy;
			}else {
				return _T98_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T98/accuracy.
	 * @param v Value to Set.
	 */
	public void setT98_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T98/accuracy",v);
		_T98_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T98_responsetime=null;

	/**
	 * @return Returns the T98/responseTime.
	 */
	public Integer getT98_responsetime() {
		try{
			if (_T98_responsetime==null){
				_T98_responsetime=getIntegerProperty("T98/responseTime");
				return _T98_responsetime;
			}else {
				return _T98_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T98/responseTime.
	 * @param v Value to Set.
	 */
	public void setT98_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T98/responseTime",v);
		_T98_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T98_congruency=null;

	/**
	 * @return Returns the T98/congruency.
	 */
	public String getT98_congruency(){
		try{
			if (_T98_congruency==null){
				_T98_congruency=getStringProperty("T98/congruency");
				return _T98_congruency;
			}else {
				return _T98_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T98/congruency.
	 * @param v Value to Set.
	 */
	public void setT98_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T98/congruency",v);
		_T98_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T99_accuracy=null;

	/**
	 * @return Returns the T99/accuracy.
	 */
	public Integer getT99_accuracy() {
		try{
			if (_T99_accuracy==null){
				_T99_accuracy=getIntegerProperty("T99/accuracy");
				return _T99_accuracy;
			}else {
				return _T99_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T99/accuracy.
	 * @param v Value to Set.
	 */
	public void setT99_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T99/accuracy",v);
		_T99_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T99_responsetime=null;

	/**
	 * @return Returns the T99/responseTime.
	 */
	public Integer getT99_responsetime() {
		try{
			if (_T99_responsetime==null){
				_T99_responsetime=getIntegerProperty("T99/responseTime");
				return _T99_responsetime;
			}else {
				return _T99_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T99/responseTime.
	 * @param v Value to Set.
	 */
	public void setT99_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T99/responseTime",v);
		_T99_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T99_congruency=null;

	/**
	 * @return Returns the T99/congruency.
	 */
	public String getT99_congruency(){
		try{
			if (_T99_congruency==null){
				_T99_congruency=getStringProperty("T99/congruency");
				return _T99_congruency;
			}else {
				return _T99_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T99/congruency.
	 * @param v Value to Set.
	 */
	public void setT99_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T99/congruency",v);
		_T99_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T100_accuracy=null;

	/**
	 * @return Returns the T100/accuracy.
	 */
	public Integer getT100_accuracy() {
		try{
			if (_T100_accuracy==null){
				_T100_accuracy=getIntegerProperty("T100/accuracy");
				return _T100_accuracy;
			}else {
				return _T100_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T100/accuracy.
	 * @param v Value to Set.
	 */
	public void setT100_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T100/accuracy",v);
		_T100_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T100_responsetime=null;

	/**
	 * @return Returns the T100/responseTime.
	 */
	public Integer getT100_responsetime() {
		try{
			if (_T100_responsetime==null){
				_T100_responsetime=getIntegerProperty("T100/responseTime");
				return _T100_responsetime;
			}else {
				return _T100_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T100/responseTime.
	 * @param v Value to Set.
	 */
	public void setT100_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T100/responseTime",v);
		_T100_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T100_congruency=null;

	/**
	 * @return Returns the T100/congruency.
	 */
	public String getT100_congruency(){
		try{
			if (_T100_congruency==null){
				_T100_congruency=getStringProperty("T100/congruency");
				return _T100_congruency;
			}else {
				return _T100_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T100/congruency.
	 * @param v Value to Set.
	 */
	public void setT100_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T100/congruency",v);
		_T100_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T101_accuracy=null;

	/**
	 * @return Returns the T101/accuracy.
	 */
	public Integer getT101_accuracy() {
		try{
			if (_T101_accuracy==null){
				_T101_accuracy=getIntegerProperty("T101/accuracy");
				return _T101_accuracy;
			}else {
				return _T101_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T101/accuracy.
	 * @param v Value to Set.
	 */
	public void setT101_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T101/accuracy",v);
		_T101_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T101_responsetime=null;

	/**
	 * @return Returns the T101/responseTime.
	 */
	public Integer getT101_responsetime() {
		try{
			if (_T101_responsetime==null){
				_T101_responsetime=getIntegerProperty("T101/responseTime");
				return _T101_responsetime;
			}else {
				return _T101_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T101/responseTime.
	 * @param v Value to Set.
	 */
	public void setT101_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T101/responseTime",v);
		_T101_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T101_congruency=null;

	/**
	 * @return Returns the T101/congruency.
	 */
	public String getT101_congruency(){
		try{
			if (_T101_congruency==null){
				_T101_congruency=getStringProperty("T101/congruency");
				return _T101_congruency;
			}else {
				return _T101_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T101/congruency.
	 * @param v Value to Set.
	 */
	public void setT101_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T101/congruency",v);
		_T101_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T102_accuracy=null;

	/**
	 * @return Returns the T102/accuracy.
	 */
	public Integer getT102_accuracy() {
		try{
			if (_T102_accuracy==null){
				_T102_accuracy=getIntegerProperty("T102/accuracy");
				return _T102_accuracy;
			}else {
				return _T102_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T102/accuracy.
	 * @param v Value to Set.
	 */
	public void setT102_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T102/accuracy",v);
		_T102_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T102_responsetime=null;

	/**
	 * @return Returns the T102/responseTime.
	 */
	public Integer getT102_responsetime() {
		try{
			if (_T102_responsetime==null){
				_T102_responsetime=getIntegerProperty("T102/responseTime");
				return _T102_responsetime;
			}else {
				return _T102_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T102/responseTime.
	 * @param v Value to Set.
	 */
	public void setT102_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T102/responseTime",v);
		_T102_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T102_congruency=null;

	/**
	 * @return Returns the T102/congruency.
	 */
	public String getT102_congruency(){
		try{
			if (_T102_congruency==null){
				_T102_congruency=getStringProperty("T102/congruency");
				return _T102_congruency;
			}else {
				return _T102_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T102/congruency.
	 * @param v Value to Set.
	 */
	public void setT102_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T102/congruency",v);
		_T102_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T103_accuracy=null;

	/**
	 * @return Returns the T103/accuracy.
	 */
	public Integer getT103_accuracy() {
		try{
			if (_T103_accuracy==null){
				_T103_accuracy=getIntegerProperty("T103/accuracy");
				return _T103_accuracy;
			}else {
				return _T103_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T103/accuracy.
	 * @param v Value to Set.
	 */
	public void setT103_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T103/accuracy",v);
		_T103_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T103_responsetime=null;

	/**
	 * @return Returns the T103/responseTime.
	 */
	public Integer getT103_responsetime() {
		try{
			if (_T103_responsetime==null){
				_T103_responsetime=getIntegerProperty("T103/responseTime");
				return _T103_responsetime;
			}else {
				return _T103_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T103/responseTime.
	 * @param v Value to Set.
	 */
	public void setT103_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T103/responseTime",v);
		_T103_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T103_congruency=null;

	/**
	 * @return Returns the T103/congruency.
	 */
	public String getT103_congruency(){
		try{
			if (_T103_congruency==null){
				_T103_congruency=getStringProperty("T103/congruency");
				return _T103_congruency;
			}else {
				return _T103_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T103/congruency.
	 * @param v Value to Set.
	 */
	public void setT103_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T103/congruency",v);
		_T103_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T104_accuracy=null;

	/**
	 * @return Returns the T104/accuracy.
	 */
	public Integer getT104_accuracy() {
		try{
			if (_T104_accuracy==null){
				_T104_accuracy=getIntegerProperty("T104/accuracy");
				return _T104_accuracy;
			}else {
				return _T104_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T104/accuracy.
	 * @param v Value to Set.
	 */
	public void setT104_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T104/accuracy",v);
		_T104_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T104_responsetime=null;

	/**
	 * @return Returns the T104/responseTime.
	 */
	public Integer getT104_responsetime() {
		try{
			if (_T104_responsetime==null){
				_T104_responsetime=getIntegerProperty("T104/responseTime");
				return _T104_responsetime;
			}else {
				return _T104_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T104/responseTime.
	 * @param v Value to Set.
	 */
	public void setT104_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T104/responseTime",v);
		_T104_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T104_congruency=null;

	/**
	 * @return Returns the T104/congruency.
	 */
	public String getT104_congruency(){
		try{
			if (_T104_congruency==null){
				_T104_congruency=getStringProperty("T104/congruency");
				return _T104_congruency;
			}else {
				return _T104_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T104/congruency.
	 * @param v Value to Set.
	 */
	public void setT104_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T104/congruency",v);
		_T104_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T105_accuracy=null;

	/**
	 * @return Returns the T105/accuracy.
	 */
	public Integer getT105_accuracy() {
		try{
			if (_T105_accuracy==null){
				_T105_accuracy=getIntegerProperty("T105/accuracy");
				return _T105_accuracy;
			}else {
				return _T105_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T105/accuracy.
	 * @param v Value to Set.
	 */
	public void setT105_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T105/accuracy",v);
		_T105_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T105_responsetime=null;

	/**
	 * @return Returns the T105/responseTime.
	 */
	public Integer getT105_responsetime() {
		try{
			if (_T105_responsetime==null){
				_T105_responsetime=getIntegerProperty("T105/responseTime");
				return _T105_responsetime;
			}else {
				return _T105_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T105/responseTime.
	 * @param v Value to Set.
	 */
	public void setT105_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T105/responseTime",v);
		_T105_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T105_congruency=null;

	/**
	 * @return Returns the T105/congruency.
	 */
	public String getT105_congruency(){
		try{
			if (_T105_congruency==null){
				_T105_congruency=getStringProperty("T105/congruency");
				return _T105_congruency;
			}else {
				return _T105_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T105/congruency.
	 * @param v Value to Set.
	 */
	public void setT105_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T105/congruency",v);
		_T105_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T106_accuracy=null;

	/**
	 * @return Returns the T106/accuracy.
	 */
	public Integer getT106_accuracy() {
		try{
			if (_T106_accuracy==null){
				_T106_accuracy=getIntegerProperty("T106/accuracy");
				return _T106_accuracy;
			}else {
				return _T106_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T106/accuracy.
	 * @param v Value to Set.
	 */
	public void setT106_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T106/accuracy",v);
		_T106_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T106_responsetime=null;

	/**
	 * @return Returns the T106/responseTime.
	 */
	public Integer getT106_responsetime() {
		try{
			if (_T106_responsetime==null){
				_T106_responsetime=getIntegerProperty("T106/responseTime");
				return _T106_responsetime;
			}else {
				return _T106_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T106/responseTime.
	 * @param v Value to Set.
	 */
	public void setT106_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T106/responseTime",v);
		_T106_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T106_congruency=null;

	/**
	 * @return Returns the T106/congruency.
	 */
	public String getT106_congruency(){
		try{
			if (_T106_congruency==null){
				_T106_congruency=getStringProperty("T106/congruency");
				return _T106_congruency;
			}else {
				return _T106_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T106/congruency.
	 * @param v Value to Set.
	 */
	public void setT106_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T106/congruency",v);
		_T106_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T107_accuracy=null;

	/**
	 * @return Returns the T107/accuracy.
	 */
	public Integer getT107_accuracy() {
		try{
			if (_T107_accuracy==null){
				_T107_accuracy=getIntegerProperty("T107/accuracy");
				return _T107_accuracy;
			}else {
				return _T107_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T107/accuracy.
	 * @param v Value to Set.
	 */
	public void setT107_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T107/accuracy",v);
		_T107_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T107_responsetime=null;

	/**
	 * @return Returns the T107/responseTime.
	 */
	public Integer getT107_responsetime() {
		try{
			if (_T107_responsetime==null){
				_T107_responsetime=getIntegerProperty("T107/responseTime");
				return _T107_responsetime;
			}else {
				return _T107_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T107/responseTime.
	 * @param v Value to Set.
	 */
	public void setT107_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T107/responseTime",v);
		_T107_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T107_congruency=null;

	/**
	 * @return Returns the T107/congruency.
	 */
	public String getT107_congruency(){
		try{
			if (_T107_congruency==null){
				_T107_congruency=getStringProperty("T107/congruency");
				return _T107_congruency;
			}else {
				return _T107_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T107/congruency.
	 * @param v Value to Set.
	 */
	public void setT107_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T107/congruency",v);
		_T107_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T108_accuracy=null;

	/**
	 * @return Returns the T108/accuracy.
	 */
	public Integer getT108_accuracy() {
		try{
			if (_T108_accuracy==null){
				_T108_accuracy=getIntegerProperty("T108/accuracy");
				return _T108_accuracy;
			}else {
				return _T108_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T108/accuracy.
	 * @param v Value to Set.
	 */
	public void setT108_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T108/accuracy",v);
		_T108_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T108_responsetime=null;

	/**
	 * @return Returns the T108/responseTime.
	 */
	public Integer getT108_responsetime() {
		try{
			if (_T108_responsetime==null){
				_T108_responsetime=getIntegerProperty("T108/responseTime");
				return _T108_responsetime;
			}else {
				return _T108_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T108/responseTime.
	 * @param v Value to Set.
	 */
	public void setT108_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T108/responseTime",v);
		_T108_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T108_congruency=null;

	/**
	 * @return Returns the T108/congruency.
	 */
	public String getT108_congruency(){
		try{
			if (_T108_congruency==null){
				_T108_congruency=getStringProperty("T108/congruency");
				return _T108_congruency;
			}else {
				return _T108_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T108/congruency.
	 * @param v Value to Set.
	 */
	public void setT108_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T108/congruency",v);
		_T108_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T109_accuracy=null;

	/**
	 * @return Returns the T109/accuracy.
	 */
	public Integer getT109_accuracy() {
		try{
			if (_T109_accuracy==null){
				_T109_accuracy=getIntegerProperty("T109/accuracy");
				return _T109_accuracy;
			}else {
				return _T109_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T109/accuracy.
	 * @param v Value to Set.
	 */
	public void setT109_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T109/accuracy",v);
		_T109_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T109_responsetime=null;

	/**
	 * @return Returns the T109/responseTime.
	 */
	public Integer getT109_responsetime() {
		try{
			if (_T109_responsetime==null){
				_T109_responsetime=getIntegerProperty("T109/responseTime");
				return _T109_responsetime;
			}else {
				return _T109_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T109/responseTime.
	 * @param v Value to Set.
	 */
	public void setT109_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T109/responseTime",v);
		_T109_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T109_congruency=null;

	/**
	 * @return Returns the T109/congruency.
	 */
	public String getT109_congruency(){
		try{
			if (_T109_congruency==null){
				_T109_congruency=getStringProperty("T109/congruency");
				return _T109_congruency;
			}else {
				return _T109_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T109/congruency.
	 * @param v Value to Set.
	 */
	public void setT109_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T109/congruency",v);
		_T109_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T110_accuracy=null;

	/**
	 * @return Returns the T110/accuracy.
	 */
	public Integer getT110_accuracy() {
		try{
			if (_T110_accuracy==null){
				_T110_accuracy=getIntegerProperty("T110/accuracy");
				return _T110_accuracy;
			}else {
				return _T110_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T110/accuracy.
	 * @param v Value to Set.
	 */
	public void setT110_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T110/accuracy",v);
		_T110_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T110_responsetime=null;

	/**
	 * @return Returns the T110/responseTime.
	 */
	public Integer getT110_responsetime() {
		try{
			if (_T110_responsetime==null){
				_T110_responsetime=getIntegerProperty("T110/responseTime");
				return _T110_responsetime;
			}else {
				return _T110_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T110/responseTime.
	 * @param v Value to Set.
	 */
	public void setT110_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T110/responseTime",v);
		_T110_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T110_congruency=null;

	/**
	 * @return Returns the T110/congruency.
	 */
	public String getT110_congruency(){
		try{
			if (_T110_congruency==null){
				_T110_congruency=getStringProperty("T110/congruency");
				return _T110_congruency;
			}else {
				return _T110_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T110/congruency.
	 * @param v Value to Set.
	 */
	public void setT110_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T110/congruency",v);
		_T110_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T111_accuracy=null;

	/**
	 * @return Returns the T111/accuracy.
	 */
	public Integer getT111_accuracy() {
		try{
			if (_T111_accuracy==null){
				_T111_accuracy=getIntegerProperty("T111/accuracy");
				return _T111_accuracy;
			}else {
				return _T111_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T111/accuracy.
	 * @param v Value to Set.
	 */
	public void setT111_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T111/accuracy",v);
		_T111_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T111_responsetime=null;

	/**
	 * @return Returns the T111/responseTime.
	 */
	public Integer getT111_responsetime() {
		try{
			if (_T111_responsetime==null){
				_T111_responsetime=getIntegerProperty("T111/responseTime");
				return _T111_responsetime;
			}else {
				return _T111_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T111/responseTime.
	 * @param v Value to Set.
	 */
	public void setT111_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T111/responseTime",v);
		_T111_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T111_congruency=null;

	/**
	 * @return Returns the T111/congruency.
	 */
	public String getT111_congruency(){
		try{
			if (_T111_congruency==null){
				_T111_congruency=getStringProperty("T111/congruency");
				return _T111_congruency;
			}else {
				return _T111_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T111/congruency.
	 * @param v Value to Set.
	 */
	public void setT111_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T111/congruency",v);
		_T111_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T112_accuracy=null;

	/**
	 * @return Returns the T112/accuracy.
	 */
	public Integer getT112_accuracy() {
		try{
			if (_T112_accuracy==null){
				_T112_accuracy=getIntegerProperty("T112/accuracy");
				return _T112_accuracy;
			}else {
				return _T112_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T112/accuracy.
	 * @param v Value to Set.
	 */
	public void setT112_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T112/accuracy",v);
		_T112_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T112_responsetime=null;

	/**
	 * @return Returns the T112/responseTime.
	 */
	public Integer getT112_responsetime() {
		try{
			if (_T112_responsetime==null){
				_T112_responsetime=getIntegerProperty("T112/responseTime");
				return _T112_responsetime;
			}else {
				return _T112_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T112/responseTime.
	 * @param v Value to Set.
	 */
	public void setT112_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T112/responseTime",v);
		_T112_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T112_congruency=null;

	/**
	 * @return Returns the T112/congruency.
	 */
	public String getT112_congruency(){
		try{
			if (_T112_congruency==null){
				_T112_congruency=getStringProperty("T112/congruency");
				return _T112_congruency;
			}else {
				return _T112_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T112/congruency.
	 * @param v Value to Set.
	 */
	public void setT112_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T112/congruency",v);
		_T112_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T113_accuracy=null;

	/**
	 * @return Returns the T113/accuracy.
	 */
	public Integer getT113_accuracy() {
		try{
			if (_T113_accuracy==null){
				_T113_accuracy=getIntegerProperty("T113/accuracy");
				return _T113_accuracy;
			}else {
				return _T113_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T113/accuracy.
	 * @param v Value to Set.
	 */
	public void setT113_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T113/accuracy",v);
		_T113_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T113_responsetime=null;

	/**
	 * @return Returns the T113/responseTime.
	 */
	public Integer getT113_responsetime() {
		try{
			if (_T113_responsetime==null){
				_T113_responsetime=getIntegerProperty("T113/responseTime");
				return _T113_responsetime;
			}else {
				return _T113_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T113/responseTime.
	 * @param v Value to Set.
	 */
	public void setT113_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T113/responseTime",v);
		_T113_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T113_congruency=null;

	/**
	 * @return Returns the T113/congruency.
	 */
	public String getT113_congruency(){
		try{
			if (_T113_congruency==null){
				_T113_congruency=getStringProperty("T113/congruency");
				return _T113_congruency;
			}else {
				return _T113_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T113/congruency.
	 * @param v Value to Set.
	 */
	public void setT113_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T113/congruency",v);
		_T113_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T114_accuracy=null;

	/**
	 * @return Returns the T114/accuracy.
	 */
	public Integer getT114_accuracy() {
		try{
			if (_T114_accuracy==null){
				_T114_accuracy=getIntegerProperty("T114/accuracy");
				return _T114_accuracy;
			}else {
				return _T114_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T114/accuracy.
	 * @param v Value to Set.
	 */
	public void setT114_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T114/accuracy",v);
		_T114_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T114_responsetime=null;

	/**
	 * @return Returns the T114/responseTime.
	 */
	public Integer getT114_responsetime() {
		try{
			if (_T114_responsetime==null){
				_T114_responsetime=getIntegerProperty("T114/responseTime");
				return _T114_responsetime;
			}else {
				return _T114_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T114/responseTime.
	 * @param v Value to Set.
	 */
	public void setT114_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T114/responseTime",v);
		_T114_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T114_congruency=null;

	/**
	 * @return Returns the T114/congruency.
	 */
	public String getT114_congruency(){
		try{
			if (_T114_congruency==null){
				_T114_congruency=getStringProperty("T114/congruency");
				return _T114_congruency;
			}else {
				return _T114_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T114/congruency.
	 * @param v Value to Set.
	 */
	public void setT114_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T114/congruency",v);
		_T114_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T115_accuracy=null;

	/**
	 * @return Returns the T115/accuracy.
	 */
	public Integer getT115_accuracy() {
		try{
			if (_T115_accuracy==null){
				_T115_accuracy=getIntegerProperty("T115/accuracy");
				return _T115_accuracy;
			}else {
				return _T115_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T115/accuracy.
	 * @param v Value to Set.
	 */
	public void setT115_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T115/accuracy",v);
		_T115_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T115_responsetime=null;

	/**
	 * @return Returns the T115/responseTime.
	 */
	public Integer getT115_responsetime() {
		try{
			if (_T115_responsetime==null){
				_T115_responsetime=getIntegerProperty("T115/responseTime");
				return _T115_responsetime;
			}else {
				return _T115_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T115/responseTime.
	 * @param v Value to Set.
	 */
	public void setT115_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T115/responseTime",v);
		_T115_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T115_congruency=null;

	/**
	 * @return Returns the T115/congruency.
	 */
	public String getT115_congruency(){
		try{
			if (_T115_congruency==null){
				_T115_congruency=getStringProperty("T115/congruency");
				return _T115_congruency;
			}else {
				return _T115_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T115/congruency.
	 * @param v Value to Set.
	 */
	public void setT115_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T115/congruency",v);
		_T115_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T116_accuracy=null;

	/**
	 * @return Returns the T116/accuracy.
	 */
	public Integer getT116_accuracy() {
		try{
			if (_T116_accuracy==null){
				_T116_accuracy=getIntegerProperty("T116/accuracy");
				return _T116_accuracy;
			}else {
				return _T116_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T116/accuracy.
	 * @param v Value to Set.
	 */
	public void setT116_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T116/accuracy",v);
		_T116_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T116_responsetime=null;

	/**
	 * @return Returns the T116/responseTime.
	 */
	public Integer getT116_responsetime() {
		try{
			if (_T116_responsetime==null){
				_T116_responsetime=getIntegerProperty("T116/responseTime");
				return _T116_responsetime;
			}else {
				return _T116_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T116/responseTime.
	 * @param v Value to Set.
	 */
	public void setT116_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T116/responseTime",v);
		_T116_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T116_congruency=null;

	/**
	 * @return Returns the T116/congruency.
	 */
	public String getT116_congruency(){
		try{
			if (_T116_congruency==null){
				_T116_congruency=getStringProperty("T116/congruency");
				return _T116_congruency;
			}else {
				return _T116_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T116/congruency.
	 * @param v Value to Set.
	 */
	public void setT116_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T116/congruency",v);
		_T116_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T117_accuracy=null;

	/**
	 * @return Returns the T117/accuracy.
	 */
	public Integer getT117_accuracy() {
		try{
			if (_T117_accuracy==null){
				_T117_accuracy=getIntegerProperty("T117/accuracy");
				return _T117_accuracy;
			}else {
				return _T117_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T117/accuracy.
	 * @param v Value to Set.
	 */
	public void setT117_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T117/accuracy",v);
		_T117_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T117_responsetime=null;

	/**
	 * @return Returns the T117/responseTime.
	 */
	public Integer getT117_responsetime() {
		try{
			if (_T117_responsetime==null){
				_T117_responsetime=getIntegerProperty("T117/responseTime");
				return _T117_responsetime;
			}else {
				return _T117_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T117/responseTime.
	 * @param v Value to Set.
	 */
	public void setT117_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T117/responseTime",v);
		_T117_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T117_congruency=null;

	/**
	 * @return Returns the T117/congruency.
	 */
	public String getT117_congruency(){
		try{
			if (_T117_congruency==null){
				_T117_congruency=getStringProperty("T117/congruency");
				return _T117_congruency;
			}else {
				return _T117_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T117/congruency.
	 * @param v Value to Set.
	 */
	public void setT117_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T117/congruency",v);
		_T117_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T118_accuracy=null;

	/**
	 * @return Returns the T118/accuracy.
	 */
	public Integer getT118_accuracy() {
		try{
			if (_T118_accuracy==null){
				_T118_accuracy=getIntegerProperty("T118/accuracy");
				return _T118_accuracy;
			}else {
				return _T118_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T118/accuracy.
	 * @param v Value to Set.
	 */
	public void setT118_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T118/accuracy",v);
		_T118_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T118_responsetime=null;

	/**
	 * @return Returns the T118/responseTime.
	 */
	public Integer getT118_responsetime() {
		try{
			if (_T118_responsetime==null){
				_T118_responsetime=getIntegerProperty("T118/responseTime");
				return _T118_responsetime;
			}else {
				return _T118_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T118/responseTime.
	 * @param v Value to Set.
	 */
	public void setT118_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T118/responseTime",v);
		_T118_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T118_congruency=null;

	/**
	 * @return Returns the T118/congruency.
	 */
	public String getT118_congruency(){
		try{
			if (_T118_congruency==null){
				_T118_congruency=getStringProperty("T118/congruency");
				return _T118_congruency;
			}else {
				return _T118_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T118/congruency.
	 * @param v Value to Set.
	 */
	public void setT118_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T118/congruency",v);
		_T118_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T119_accuracy=null;

	/**
	 * @return Returns the T119/accuracy.
	 */
	public Integer getT119_accuracy() {
		try{
			if (_T119_accuracy==null){
				_T119_accuracy=getIntegerProperty("T119/accuracy");
				return _T119_accuracy;
			}else {
				return _T119_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T119/accuracy.
	 * @param v Value to Set.
	 */
	public void setT119_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T119/accuracy",v);
		_T119_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T119_responsetime=null;

	/**
	 * @return Returns the T119/responseTime.
	 */
	public Integer getT119_responsetime() {
		try{
			if (_T119_responsetime==null){
				_T119_responsetime=getIntegerProperty("T119/responseTime");
				return _T119_responsetime;
			}else {
				return _T119_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T119/responseTime.
	 * @param v Value to Set.
	 */
	public void setT119_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T119/responseTime",v);
		_T119_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T119_congruency=null;

	/**
	 * @return Returns the T119/congruency.
	 */
	public String getT119_congruency(){
		try{
			if (_T119_congruency==null){
				_T119_congruency=getStringProperty("T119/congruency");
				return _T119_congruency;
			}else {
				return _T119_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T119/congruency.
	 * @param v Value to Set.
	 */
	public void setT119_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T119/congruency",v);
		_T119_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T120_accuracy=null;

	/**
	 * @return Returns the T120/accuracy.
	 */
	public Integer getT120_accuracy() {
		try{
			if (_T120_accuracy==null){
				_T120_accuracy=getIntegerProperty("T120/accuracy");
				return _T120_accuracy;
			}else {
				return _T120_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T120/accuracy.
	 * @param v Value to Set.
	 */
	public void setT120_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T120/accuracy",v);
		_T120_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _T120_responsetime=null;

	/**
	 * @return Returns the T120/responseTime.
	 */
	public Integer getT120_responsetime() {
		try{
			if (_T120_responsetime==null){
				_T120_responsetime=getIntegerProperty("T120/responseTime");
				return _T120_responsetime;
			}else {
				return _T120_responsetime;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T120/responseTime.
	 * @param v Value to Set.
	 */
	public void setT120_responsetime(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T120/responseTime",v);
		_T120_responsetime=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T120_congruency=null;

	/**
	 * @return Returns the T120/congruency.
	 */
	public String getT120_congruency(){
		try{
			if (_T120_congruency==null){
				_T120_congruency=getStringProperty("T120/congruency");
				return _T120_congruency;
			}else {
				return _T120_congruency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for T120/congruency.
	 * @param v Value to Set.
	 */
	public void setT120_congruency(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/T120/congruency",v);
		_T120_congruency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CbatSimon> getAllCbatSimons(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CbatSimon> al = new ArrayList<org.nrg.xdat.om.CbatSimon>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CbatSimon> getCbatSimonsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CbatSimon> al = new ArrayList<org.nrg.xdat.om.CbatSimon>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CbatSimon> getCbatSimonsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CbatSimon> al = new ArrayList<org.nrg.xdat.om.CbatSimon>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CbatSimon getCbatSimonsById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cbat:simon/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CbatSimon) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
