/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:02 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoTxTreatment extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.TxTreatmentI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoTxTreatment.class);
	public static String SCHEMA_ELEMENT_NAME="tx:treatment";

	public AutoTxTreatment(ItemI item)
	{
		super(item);
	}

	public AutoTxTreatment(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoTxTreatment(UserI user)
	 **/
	public AutoTxTreatment(){}

	public AutoTxTreatment(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "tx:treatment";
	}

	//FIELD

	private String _Code=null;

	/**
	 * @return Returns the code.
	 */
	public String getCode(){
		try{
			if (_Code==null){
				_Code=getStringProperty("code");
				return _Code;
			}else {
				return _Code;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for code.
	 * @param v Value to Set.
	 */
	public void setCode(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/code",v);
		_Code=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Codetype=null;

	/**
	 * @return Returns the codeType.
	 */
	public String getCodetype(){
		try{
			if (_Codetype==null){
				_Codetype=getStringProperty("codeType");
				return _Codetype;
			}else {
				return _Codetype;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for codeType.
	 * @param v Value to Set.
	 */
	public void setCodetype(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/codeType",v);
		_Codetype=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Name=null;

	/**
	 * @return Returns the name.
	 */
	public String getName(){
		try{
			if (_Name==null){
				_Name=getStringProperty("name");
				return _Name;
			}else {
				return _Name;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for name.
	 * @param v Value to Set.
	 */
	public void setName(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/name",v);
		_Name=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Status=null;

	/**
	 * @return Returns the status.
	 */
	public String getStatus(){
		try{
			if (_Status==null){
				_Status=getStringProperty("status");
				return _Status;
			}else {
				return _Status;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for status.
	 * @param v Value to Set.
	 */
	public void setStatus(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/status",v);
		_Status=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Doseamount=null;

	/**
	 * @return Returns the doseAmount.
	 */
	public Double getDoseamount() {
		try{
			if (_Doseamount==null){
				_Doseamount=getDoubleProperty("doseAmount");
				return _Doseamount;
			}else {
				return _Doseamount;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for doseAmount.
	 * @param v Value to Set.
	 */
	public void setDoseamount(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/doseAmount",v);
		_Doseamount=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Doseunit=null;

	/**
	 * @return Returns the doseUnit.
	 */
	public String getDoseunit(){
		try{
			if (_Doseunit==null){
				_Doseunit=getStringProperty("doseUnit");
				return _Doseunit;
			}else {
				return _Doseunit;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for doseUnit.
	 * @param v Value to Set.
	 */
	public void setDoseunit(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/doseUnit",v);
		_Doseunit=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Doseschedule=null;

	/**
	 * @return Returns the doseSchedule.
	 */
	public String getDoseschedule(){
		try{
			if (_Doseschedule==null){
				_Doseschedule=getStringProperty("doseSchedule");
				return _Doseschedule;
			}else {
				return _Doseschedule;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for doseSchedule.
	 * @param v Value to Set.
	 */
	public void setDoseschedule(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/doseSchedule",v);
		_Doseschedule=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Route=null;

	/**
	 * @return Returns the route.
	 */
	public String getRoute(){
		try{
			if (_Route==null){
				_Route=getStringProperty("route");
				return _Route;
			}else {
				return _Route;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for route.
	 * @param v Value to Set.
	 */
	public void setRoute(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/route",v);
		_Route=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Indication=null;

	/**
	 * @return Returns the indication.
	 */
	public String getIndication(){
		try{
			if (_Indication==null){
				_Indication=getStringProperty("indication");
				return _Indication;
			}else {
				return _Indication;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for indication.
	 * @param v Value to Set.
	 */
	public void setIndication(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/indication",v);
		_Indication=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Startdate=null;

	/**
	 * @return Returns the startDate.
	 */
	public Object getStartdate(){
		try{
			if (_Startdate==null){
				_Startdate=getProperty("startDate");
				return _Startdate;
			}else {
				return _Startdate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for startDate.
	 * @param v Value to Set.
	 */
	public void setStartdate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/startDate",v);
		_Startdate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Startdatedaynotreported=null;

	/**
	 * @return Returns the startDateDayNotReported.
	 */
	public Boolean getStartdatedaynotreported() {
		try{
			if (_Startdatedaynotreported==null){
				_Startdatedaynotreported=getBooleanProperty("startDateDayNotReported");
				return _Startdatedaynotreported;
			}else {
				return _Startdatedaynotreported;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for startDateDayNotReported.
	 * @param v Value to Set.
	 */
	public void setStartdatedaynotreported(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/startDateDayNotReported",v);
		_Startdatedaynotreported=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Startdatemonthnotreported=null;

	/**
	 * @return Returns the startDateMonthNotReported.
	 */
	public Boolean getStartdatemonthnotreported() {
		try{
			if (_Startdatemonthnotreported==null){
				_Startdatemonthnotreported=getBooleanProperty("startDateMonthNotReported");
				return _Startdatemonthnotreported;
			}else {
				return _Startdatemonthnotreported;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for startDateMonthNotReported.
	 * @param v Value to Set.
	 */
	public void setStartdatemonthnotreported(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/startDateMonthNotReported",v);
		_Startdatemonthnotreported=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Startdateyearnotreported=null;

	/**
	 * @return Returns the startDateYearNotReported.
	 */
	public Boolean getStartdateyearnotreported() {
		try{
			if (_Startdateyearnotreported==null){
				_Startdateyearnotreported=getBooleanProperty("startDateYearNotReported");
				return _Startdateyearnotreported;
			}else {
				return _Startdateyearnotreported;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for startDateYearNotReported.
	 * @param v Value to Set.
	 */
	public void setStartdateyearnotreported(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/startDateYearNotReported",v);
		_Startdateyearnotreported=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Enddate=null;

	/**
	 * @return Returns the endDate.
	 */
	public Object getEnddate(){
		try{
			if (_Enddate==null){
				_Enddate=getProperty("endDate");
				return _Enddate;
			}else {
				return _Enddate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for endDate.
	 * @param v Value to Set.
	 */
	public void setEnddate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/endDate",v);
		_Enddate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Enddatedaynotreported=null;

	/**
	 * @return Returns the endDateDayNotReported.
	 */
	public Boolean getEnddatedaynotreported() {
		try{
			if (_Enddatedaynotreported==null){
				_Enddatedaynotreported=getBooleanProperty("endDateDayNotReported");
				return _Enddatedaynotreported;
			}else {
				return _Enddatedaynotreported;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for endDateDayNotReported.
	 * @param v Value to Set.
	 */
	public void setEnddatedaynotreported(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/endDateDayNotReported",v);
		_Enddatedaynotreported=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Enddatemonthnotreported=null;

	/**
	 * @return Returns the endDateMonthNotReported.
	 */
	public Boolean getEnddatemonthnotreported() {
		try{
			if (_Enddatemonthnotreported==null){
				_Enddatemonthnotreported=getBooleanProperty("endDateMonthNotReported");
				return _Enddatemonthnotreported;
			}else {
				return _Enddatemonthnotreported;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for endDateMonthNotReported.
	 * @param v Value to Set.
	 */
	public void setEnddatemonthnotreported(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/endDateMonthNotReported",v);
		_Enddatemonthnotreported=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Enddateyearnotreported=null;

	/**
	 * @return Returns the endDateYearNotReported.
	 */
	public Boolean getEnddateyearnotreported() {
		try{
			if (_Enddateyearnotreported==null){
				_Enddateyearnotreported=getBooleanProperty("endDateYearNotReported");
				return _Enddateyearnotreported;
			}else {
				return _Enddateyearnotreported;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for endDateYearNotReported.
	 * @param v Value to Set.
	 */
	public void setEnddateyearnotreported(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/endDateYearNotReported",v);
		_Enddateyearnotreported=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Clinicaltrialname=null;

	/**
	 * @return Returns the clinicalTrialName.
	 */
	public String getClinicaltrialname(){
		try{
			if (_Clinicaltrialname==null){
				_Clinicaltrialname=getStringProperty("clinicalTrialName");
				return _Clinicaltrialname;
			}else {
				return _Clinicaltrialname;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for clinicalTrialName.
	 * @param v Value to Set.
	 */
	public void setClinicaltrialname(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/clinicalTrialName",v);
		_Clinicaltrialname=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Clinicaltrialarm=null;

	/**
	 * @return Returns the clinicalTrialArm.
	 */
	public String getClinicaltrialarm(){
		try{
			if (_Clinicaltrialarm==null){
				_Clinicaltrialarm=getStringProperty("clinicalTrialArm");
				return _Clinicaltrialarm;
			}else {
				return _Clinicaltrialarm;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for clinicalTrialArm.
	 * @param v Value to Set.
	 */
	public void setClinicaltrialarm(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/clinicalTrialArm",v);
		_Clinicaltrialarm=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Treatmentnotes=null;

	/**
	 * @return Returns the treatmentNotes.
	 */
	public String getTreatmentnotes(){
		try{
			if (_Treatmentnotes==null){
				_Treatmentnotes=getStringProperty("treatmentNotes");
				return _Treatmentnotes;
			}else {
				return _Treatmentnotes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for treatmentNotes.
	 * @param v Value to Set.
	 */
	public void setTreatmentnotes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/treatmentNotes",v);
		_Treatmentnotes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _TxTreatmentId=null;

	/**
	 * @return Returns the tx_treatment_id.
	 */
	public Integer getTxTreatmentId() {
		try{
			if (_TxTreatmentId==null){
				_TxTreatmentId=getIntegerProperty("tx_treatment_id");
				return _TxTreatmentId;
			}else {
				return _TxTreatmentId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tx_treatment_id.
	 * @param v Value to Set.
	 */
	public void setTxTreatmentId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tx_treatment_id",v);
		_TxTreatmentId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.TxTreatment> getAllTxTreatments(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TxTreatment> al = new ArrayList<org.nrg.xdat.om.TxTreatment>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.TxTreatment> getTxTreatmentsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TxTreatment> al = new ArrayList<org.nrg.xdat.om.TxTreatment>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.TxTreatment> getTxTreatmentsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.TxTreatment> al = new ArrayList<org.nrg.xdat.om.TxTreatment>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static TxTreatment getTxTreatmentsByTxTreatmentId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("tx:treatment/tx_treatment_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (TxTreatment) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
