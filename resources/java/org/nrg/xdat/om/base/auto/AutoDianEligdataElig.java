/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:20:00 CDT 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianEligdataElig extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.DianEligdataEligI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianEligdataElig.class);
	public static String SCHEMA_ELEMENT_NAME="dian:eligData_elig";

	public AutoDianEligdataElig(ItemI item)
	{
		super(item);
	}

	public AutoDianEligdataElig(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianEligdataElig(UserI user)
	 **/
	public AutoDianEligdataElig(){}

	public AutoDianEligdataElig(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:eligData_elig";
	}

	//FIELD

	private Integer _Recno=null;

	/**
	 * @return Returns the RECNO.
	 */
	public Integer getRecno() {
		try{
			if (_Recno==null){
				_Recno=getIntegerProperty("RECNO");
				return _Recno;
			}else {
				return _Recno;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RECNO.
	 * @param v Value to Set.
	 */
	public void setRecno(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RECNO",v);
		_Recno=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Date=null;

	/**
	 * @return Returns the DATE.
	 */
	public Object getDate(){
		try{
			if (_Date==null){
				_Date=getProperty("DATE");
				return _Date;
			}else {
				return _Date;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DATE.
	 * @param v Value to Set.
	 */
	public void setDate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DATE",v);
		_Date=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Incage=null;

	/**
	 * @return Returns the INCAGE.
	 */
	public Integer getIncage() {
		try{
			if (_Incage==null){
				_Incage=getIntegerProperty("INCAGE");
				return _Incage;
			}else {
				return _Incage;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INCAGE.
	 * @param v Value to Set.
	 */
	public void setIncage(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INCAGE",v);
		_Incage=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Mutation=null;

	/**
	 * @return Returns the MUTATION.
	 */
	public String getMutation(){
		try{
			if (_Mutation==null){
				_Mutation=getStringProperty("MUTATION");
				return _Mutation;
			}else {
				return _Mutation;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MUTATION.
	 * @param v Value to Set.
	 */
	public void setMutation(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MUTATION",v);
		_Mutation=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Muttype=null;

	/**
	 * @return Returns the MUTTYPE.
	 */
	public String getMuttype(){
		try{
			if (_Muttype==null){
				_Muttype=getStringProperty("MUTTYPE");
				return _Muttype;
			}else {
				return _Muttype;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MUTTYPE.
	 * @param v Value to Set.
	 */
	public void setMuttype(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MUTTYPE",v);
		_Muttype=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Mutgene=null;

	/**
	 * @return Returns the MUTGENE.
	 */
	public String getMutgene(){
		try{
			if (_Mutgene==null){
				_Mutgene=getStringProperty("MUTGENE");
				return _Mutgene;
			}else {
				return _Mutgene;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MUTGENE.
	 * @param v Value to Set.
	 */
	public void setMutgene(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MUTGENE",v);
		_Mutgene=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Mutaa=null;

	/**
	 * @return Returns the MUTAA.
	 */
	public String getMutaa(){
		try{
			if (_Mutaa==null){
				_Mutaa=getStringProperty("MUTAA");
				return _Mutaa;
			}else {
				return _Mutaa;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MUTAA.
	 * @param v Value to Set.
	 */
	public void setMutaa(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MUTAA",v);
		_Mutaa=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mutpos=null;

	/**
	 * @return Returns the MUTPOS.
	 */
	public Integer getMutpos() {
		try{
			if (_Mutpos==null){
				_Mutpos=getIntegerProperty("MUTPOS");
				return _Mutpos;
			}else {
				return _Mutpos;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MUTPOS.
	 * @param v Value to Set.
	 */
	public void setMutpos(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MUTPOS",v);
		_Mutpos=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Mutaachg=null;

	/**
	 * @return Returns the MUTAACHG.
	 */
	public String getMutaachg(){
		try{
			if (_Mutaachg==null){
				_Mutaachg=getStringProperty("MUTAACHG");
				return _Mutaachg;
			}else {
				return _Mutaachg;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MUTAACHG.
	 * @param v Value to Set.
	 */
	public void setMutaachg(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MUTAACHG",v);
		_Mutaachg=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Mutdes=null;

	/**
	 * @return Returns the MUTDES.
	 */
	public String getMutdes(){
		try{
			if (_Mutdes==null){
				_Mutdes=getStringProperty("MUTDES");
				return _Mutdes;
			}else {
				return _Mutdes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MUTDES.
	 * @param v Value to Set.
	 */
	public void setMutdes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MUTDES",v);
		_Mutdes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Inccog=null;

	/**
	 * @return Returns the INCCOG.
	 */
	public Integer getInccog() {
		try{
			if (_Inccog==null){
				_Inccog=getIntegerProperty("INCCOG");
				return _Inccog;
			}else {
				return _Inccog;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INCCOG.
	 * @param v Value to Set.
	 */
	public void setInccog(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INCCOG",v);
		_Inccog=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Inclang=null;

	/**
	 * @return Returns the INCLANG.
	 */
	public Integer getInclang() {
		try{
			if (_Inclang==null){
				_Inclang=getIntegerProperty("INCLANG");
				return _Inclang;
			}else {
				return _Inclang;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INCLANG.
	 * @param v Value to Set.
	 */
	public void setInclang(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INCLANG",v);
		_Inclang=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Inccs=null;

	/**
	 * @return Returns the INCCS.
	 */
	public Integer getInccs() {
		try{
			if (_Inccs==null){
				_Inccs=getIntegerProperty("INCCS");
				return _Inccs;
			}else {
				return _Inccs;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INCCS.
	 * @param v Value to Set.
	 */
	public void setInccs(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INCCS",v);
		_Inccs=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Excmed=null;

	/**
	 * @return Returns the EXCMED.
	 */
	public Integer getExcmed() {
		try{
			if (_Excmed==null){
				_Excmed=getIntegerProperty("EXCMED");
				return _Excmed;
			}else {
				return _Excmed;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for EXCMED.
	 * @param v Value to Set.
	 */
	public void setExcmed(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/EXCMED",v);
		_Excmed=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Excsnf=null;

	/**
	 * @return Returns the EXCSNF.
	 */
	public Integer getExcsnf() {
		try{
			if (_Excsnf==null){
				_Excsnf=getIntegerProperty("EXCSNF");
				return _Excsnf;
			}else {
				return _Excsnf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for EXCSNF.
	 * @param v Value to Set.
	 */
	public void setExcsnf(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/EXCSNF",v);
		_Excsnf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Excmri=null;

	/**
	 * @return Returns the EXCMRI.
	 */
	public Integer getExcmri() {
		try{
			if (_Excmri==null){
				_Excmri=getIntegerProperty("EXCMRI");
				return _Excmri;
			}else {
				return _Excmri;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for EXCMRI.
	 * @param v Value to Set.
	 */
	public void setExcmri(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/EXCMRI",v);
		_Excmri=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Excpet=null;

	/**
	 * @return Returns the EXCPET.
	 */
	public Integer getExcpet() {
		try{
			if (_Excpet==null){
				_Excpet=getIntegerProperty("EXCPET");
				return _Excpet;
			}else {
				return _Excpet;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for EXCPET.
	 * @param v Value to Set.
	 */
	public void setExcpet(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/EXCPET",v);
		_Excpet=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Excpet2=null;

	/**
	 * @return Returns the EXCPET2.
	 */
	public Integer getExcpet2() {
		try{
			if (_Excpet2==null){
				_Excpet2=getIntegerProperty("EXCPET2");
				return _Excpet2;
			}else {
				return _Excpet2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for EXCPET2.
	 * @param v Value to Set.
	 */
	public void setExcpet2(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/EXCPET2",v);
		_Excpet2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Exclp=null;

	/**
	 * @return Returns the EXCLP.
	 */
	public Integer getExclp() {
		try{
			if (_Exclp==null){
				_Exclp=getIntegerProperty("EXCLP");
				return _Exclp;
			}else {
				return _Exclp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for EXCLP.
	 * @param v Value to Set.
	 */
	public void setExclp(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/EXCLP",v);
		_Exclp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mutstat=null;

	/**
	 * @return Returns the MUTSTAT.
	 */
	public Integer getMutstat() {
		try{
			if (_Mutstat==null){
				_Mutstat=getIntegerProperty("MUTSTAT");
				return _Mutstat;
			}else {
				return _Mutstat;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MUTSTAT.
	 * @param v Value to Set.
	 */
	public void setMutstat(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MUTSTAT",v);
		_Mutstat=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Neuro=null;

	/**
	 * @return Returns the NEURO.
	 */
	public Integer getNeuro() {
		try{
			if (_Neuro==null){
				_Neuro=getIntegerProperty("NEURO");
				return _Neuro;
			}else {
				return _Neuro;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NEURO.
	 * @param v Value to Set.
	 */
	public void setNeuro(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NEURO",v);
		_Neuro=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _DianEligdataEligId=null;

	/**
	 * @return Returns the dian_eligData_elig_id.
	 */
	public Integer getDianEligdataEligId() {
		try{
			if (_DianEligdataEligId==null){
				_DianEligdataEligId=getIntegerProperty("dian_eligData_elig_id");
				return _DianEligdataEligId;
			}else {
				return _DianEligdataEligId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for dian_eligData_elig_id.
	 * @param v Value to Set.
	 */
	public void setDianEligdataEligId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/dian_eligData_elig_id",v);
		_DianEligdataEligId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianEligdataElig> getAllDianEligdataEligs(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianEligdataElig> al = new ArrayList<org.nrg.xdat.om.DianEligdataElig>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianEligdataElig> getDianEligdataEligsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianEligdataElig> al = new ArrayList<org.nrg.xdat.om.DianEligdataElig>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianEligdataElig> getDianEligdataEligsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianEligdataElig> al = new ArrayList<org.nrg.xdat.om.DianEligdataElig>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianEligdataElig getDianEligdataEligsByDianEligdataEligId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:eligData_elig/dian_eligData_elig_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianEligdataElig) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
