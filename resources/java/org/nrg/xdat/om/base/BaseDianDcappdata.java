/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseDianDcappdata extends AutoDianDcappdata {

	public BaseDianDcappdata(ItemI item)
	{
		super(item);
	}

	public BaseDianDcappdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseDianDcappdata(UserI user)
	 **/
	public BaseDianDcappdata()
	{}

	public BaseDianDcappdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
