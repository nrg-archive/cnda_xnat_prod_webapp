/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCondrCondrmedicalhistory extends AutoCondrCondrmedicalhistory {

	public BaseCondrCondrmedicalhistory(ItemI item)
	{
		super(item);
	}

	public BaseCondrCondrmedicalhistory(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCondrCondrmedicalhistory(UserI user)
	 **/
	public BaseCondrCondrmedicalhistory()
	{}

	public BaseCondrCondrmedicalhistory(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
