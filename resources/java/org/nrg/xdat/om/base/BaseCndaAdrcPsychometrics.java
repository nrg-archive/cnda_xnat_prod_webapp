/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCndaAdrcPsychometrics extends AutoCndaAdrcPsychometrics {

	public BaseCndaAdrcPsychometrics(ItemI item)
	{
		super(item);
	}

	public BaseCndaAdrcPsychometrics(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaAdrcPsychometrics(UserI user)
	 **/
	public BaseCndaAdrcPsychometrics()
	{}

	public BaseCndaAdrcPsychometrics(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
