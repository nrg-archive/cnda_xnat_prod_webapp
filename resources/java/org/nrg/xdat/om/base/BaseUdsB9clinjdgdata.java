/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseUdsB9clinjdgdata extends AutoUdsB9clinjdgdata {

	public BaseUdsB9clinjdgdata(ItemI item)
	{
		super(item);
	}

	public BaseUdsB9clinjdgdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsB9clinjdgdata(UserI user)
	 **/
	public BaseUdsB9clinjdgdata()
	{}

	public BaseUdsB9clinjdgdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
