/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseUdsB9suppdata extends AutoUdsB9suppdata {

	public BaseUdsB9suppdata(ItemI item)
	{
		super(item);
	}

	public BaseUdsB9suppdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsB9suppdata(UserI user)
	 **/
	public BaseUdsB9suppdata()
	{}

	public BaseUdsB9suppdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
