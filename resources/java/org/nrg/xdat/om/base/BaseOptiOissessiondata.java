/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseOptiOissessiondata extends AutoOptiOissessiondata {

	public BaseOptiOissessiondata(ItemI item)
	{
		super(item);
	}

	public BaseOptiOissessiondata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseOptiOissessiondata(UserI user)
	 **/
	public BaseOptiOissessiondata()
	{}

	public BaseOptiOissessiondata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
