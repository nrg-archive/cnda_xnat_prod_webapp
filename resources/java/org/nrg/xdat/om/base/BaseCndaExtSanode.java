/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCndaExtSanode extends AutoCndaExtSanode {

	public BaseCndaExtSanode(ItemI item)
	{
		super(item);
	}

	public BaseCndaExtSanode(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaExtSanode(UserI user)
	 **/
	public BaseCndaExtSanode()
	{}

	public BaseCndaExtSanode(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
