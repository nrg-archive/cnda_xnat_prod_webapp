/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCondrProgressionlogentry extends AutoCondrProgressionlogentry {

	public BaseCondrProgressionlogentry(ItemI item)
	{
		super(item);
	}

	public BaseCondrProgressionlogentry(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCondrProgressionlogentry(UserI user)
	 **/
	public BaseCondrProgressionlogentry()
	{}

	public BaseCondrProgressionlogentry(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
