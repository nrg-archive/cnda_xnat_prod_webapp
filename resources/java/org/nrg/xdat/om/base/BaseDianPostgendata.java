/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseDianPostgendata extends AutoDianPostgendata {

	public BaseDianPostgendata(ItemI item)
	{
		super(item);
	}

	public BaseDianPostgendata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseDianPostgendata(UserI user)
	 **/
	public BaseDianPostgendata()
	{}

	public BaseDianPostgendata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
