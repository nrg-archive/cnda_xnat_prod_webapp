/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCbatSimon extends AutoCbatSimon {

	public BaseCbatSimon(ItemI item)
	{
		super(item);
	}

	public BaseCbatSimon(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCbatSimon(UserI user)
	 **/
	public BaseCbatSimon()
	{}

	public BaseCbatSimon(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
