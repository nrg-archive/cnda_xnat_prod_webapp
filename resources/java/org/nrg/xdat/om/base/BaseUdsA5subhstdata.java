/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseUdsA5subhstdata extends AutoUdsA5subhstdata {

	public BaseUdsA5subhstdata(ItemI item)
	{
		super(item);
	}

	public BaseUdsA5subhstdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsA5subhstdata(UserI user)
	 **/
	public BaseUdsA5subhstdata()
	{}

	public BaseUdsA5subhstdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
