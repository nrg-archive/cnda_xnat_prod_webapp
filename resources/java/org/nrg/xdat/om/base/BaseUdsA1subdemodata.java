/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseUdsA1subdemodata extends AutoUdsA1subdemodata {

	public BaseUdsA1subdemodata(ItemI item)
	{
		super(item);
	}

	public BaseUdsA1subdemodata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsA1subdemodata(UserI user)
	 **/
	public BaseUdsA1subdemodata()
	{}

	public BaseUdsA1subdemodata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
