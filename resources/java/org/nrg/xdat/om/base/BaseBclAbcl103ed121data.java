/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseBclAbcl103ed121data extends AutoBclAbcl103ed121data {

	public BaseBclAbcl103ed121data(ItemI item)
	{
		super(item);
	}

	public BaseBclAbcl103ed121data(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseBclAbcl103ed121data(UserI user)
	 **/
	public BaseBclAbcl103ed121data()
	{}

	public BaseBclAbcl103ed121data(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
