/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseLs2Lshealthdata extends AutoLs2Lshealthdata {

	public BaseLs2Lshealthdata(ItemI item)
	{
		super(item);
	}

	public BaseLs2Lshealthdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseLs2Lshealthdata(UserI user)
	 **/
	public BaseLs2Lshealthdata()
	{}

	public BaseLs2Lshealthdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
