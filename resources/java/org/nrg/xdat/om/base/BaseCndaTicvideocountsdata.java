/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCndaTicvideocountsdata extends AutoCndaTicvideocountsdata {

	public BaseCndaTicvideocountsdata(ItemI item)
	{
		super(item);
	}

	public BaseCndaTicvideocountsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaTicvideocountsdata(UserI user)
	 **/
	public BaseCndaTicvideocountsdata()
	{}

	public BaseCndaTicvideocountsdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
