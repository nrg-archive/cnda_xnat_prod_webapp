/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseOptiDotscandata extends AutoOptiDotscandata {

	public BaseOptiDotscandata(ItemI item)
	{
		super(item);
	}

	public BaseOptiDotscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseOptiDotscandata(UserI user)
	 **/
	public BaseOptiDotscandata()
	{}

	public BaseOptiDotscandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
