/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCndaAtrophynildataPeak extends AutoCndaAtrophynildataPeak {

	public BaseCndaAtrophynildataPeak(ItemI item)
	{
		super(item);
	}

	public BaseCndaAtrophynildataPeak(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaAtrophynildataPeak(UserI user)
	 **/
	public BaseCndaAtrophynildataPeak()
	{}

	public BaseCndaAtrophynildataPeak(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
