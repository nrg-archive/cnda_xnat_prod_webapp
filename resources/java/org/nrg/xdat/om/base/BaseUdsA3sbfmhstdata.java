/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseUdsA3sbfmhstdata extends AutoUdsA3sbfmhstdata {

	public BaseUdsA3sbfmhstdata(ItemI item)
	{
		super(item);
	}

	public BaseUdsA3sbfmhstdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsA3sbfmhstdata(UserI user)
	 **/
	public BaseUdsA3sbfmhstdata()
	{}

	public BaseUdsA3sbfmhstdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
