/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseIqWasi1999data extends AutoIqWasi1999data {

	public BaseIqWasi1999data(ItemI item)
	{
		super(item);
	}

	public BaseIqWasi1999data(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseIqWasi1999data(UserI user)
	 **/
	public BaseIqWasi1999data()
	{}

	public BaseIqWasi1999data(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
