/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseAdosAdos2001module2data extends AutoAdosAdos2001module2data {

	public BaseAdosAdos2001module2data(ItemI item)
	{
		super(item);
	}

	public BaseAdosAdos2001module2data(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseAdosAdos2001module2data(UserI user)
	 **/
	public BaseAdosAdos2001module2data()
	{}

	public BaseAdosAdos2001module2data(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
