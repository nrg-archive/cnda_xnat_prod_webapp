/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseUdsD1dxdata extends AutoUdsD1dxdata {

	public BaseUdsD1dxdata(ItemI item)
	{
		super(item);
	}

	public BaseUdsD1dxdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsD1dxdata(UserI user)
	 **/
	public BaseUdsD1dxdata()
	{}

	public BaseUdsD1dxdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
