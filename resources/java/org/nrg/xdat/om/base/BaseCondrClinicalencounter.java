/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCondrClinicalencounter extends AutoCondrClinicalencounter {

	public BaseCondrClinicalencounter(ItemI item)
	{
		super(item);
	}

	public BaseCondrClinicalencounter(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCondrClinicalencounter(UserI user)
	 **/
	public BaseCondrClinicalencounter()
	{}

	public BaseCondrClinicalencounter(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
