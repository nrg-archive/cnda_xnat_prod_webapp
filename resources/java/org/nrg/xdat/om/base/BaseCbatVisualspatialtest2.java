/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCbatVisualspatialtest2 extends AutoCbatVisualspatialtest2 {

	public BaseCbatVisualspatialtest2(ItemI item)
	{
		super(item);
	}

	public BaseCbatVisualspatialtest2(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCbatVisualspatialtest2(UserI user)
	 **/
	public BaseCbatVisualspatialtest2()
	{}

	public BaseCbatVisualspatialtest2(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
