/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCndaPettimecoursedataDurationBp extends AutoCndaPettimecoursedataDurationBp {

	public BaseCndaPettimecoursedataDurationBp(ItemI item)
	{
		super(item);
	}

	public BaseCndaPettimecoursedataDurationBp(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaPettimecoursedataDurationBp(UserI user)
	 **/
	public BaseCndaPettimecoursedataDurationBp()
	{}

	public BaseCndaPettimecoursedataDurationBp(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
