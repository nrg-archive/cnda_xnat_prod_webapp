/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCndaPettimecoursedataRegion extends AutoCndaPettimecoursedataRegion {

	public BaseCndaPettimecoursedataRegion(ItemI item)
	{
		super(item);
	}

	public BaseCndaPettimecoursedataRegion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaPettimecoursedataRegion(UserI user)
	 **/
	public BaseCndaPettimecoursedataRegion()
	{}

	public BaseCndaPettimecoursedataRegion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
