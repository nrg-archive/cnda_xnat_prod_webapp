/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseDianAv45metadata extends AutoDianAv45metadata {

	public BaseDianAv45metadata(ItemI item)
	{
		super(item);
	}

	public BaseDianAv45metadata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseDianAv45metadata(UserI user)
	 **/
	public BaseDianAv45metadata()
	{}

	public BaseDianAv45metadata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
