/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCondrProgressionlog extends AutoCondrProgressionlog {

	public BaseCondrProgressionlog(ItemI item)
	{
		super(item);
	}

	public BaseCondrProgressionlog(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCondrProgressionlog(UserI user)
	 **/
	public BaseCondrProgressionlog()
	{}

	public BaseCondrProgressionlog(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
