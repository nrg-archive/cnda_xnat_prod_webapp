/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseDianBldlipiddata extends AutoDianBldlipiddata {

	public BaseDianBldlipiddata(ItemI item)
	{
		super(item);
	}

	public BaseDianBldlipiddata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseDianBldlipiddata(UserI user)
	 **/
	public BaseDianBldlipiddata()
	{}

	public BaseDianBldlipiddata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
