/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class BclCbcl6101ed201data extends BaseBclCbcl6101ed201data {

	public BclCbcl6101ed201data(ItemI item)
	{
		super(item);
	}

	public BclCbcl6101ed201data(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseBclCbcl6101ed201data(UserI user)
	 **/
	public BclCbcl6101ed201data()
	{}

	public BclCbcl6101ed201data(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
