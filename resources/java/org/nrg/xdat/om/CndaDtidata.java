//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Aug 18 11:41:40 CDT 2005
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class CndaDtidata extends BaseCndaDtidata {

	public CndaDtidata(ItemI item)
	{
		super(item);
	}

	public CndaDtidata(UserI user)
	{
		super(user);
	}

	public CndaDtidata()
	{}

	public CndaDtidata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

	public ArrayList getRegions()
	{
	    try {
            return (ArrayList)getRegions_region();
        } catch (Exception e) {
            logger.error("",e);
            return new ArrayList();
        }
	}

}
