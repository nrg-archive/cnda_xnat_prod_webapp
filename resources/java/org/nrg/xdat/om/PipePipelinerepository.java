// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Mon Aug 04 09:43:33 CDT 2008
 *
 */
package org.nrg.xdat.om;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nrg.pipeline.PipelineRepositoryManager;
import org.nrg.xdat.om.base.BasePipePipelinerepository;
import org.nrg.xdat.security.ElementSecurity;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xft.ItemI;
import org.nrg.xft.XFTItem;
import org.nrg.xft.XFTTable;
import org.nrg.xft.collections.ItemCollection;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.ajax.writer.ResponseWriterI;
import org.nrg.xnat.ajax.writer.WriterFactory;
import org.nrg.xnat.turbine.utils.ArcSpecManager;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class PipePipelinerepository extends BasePipePipelinerepository {


	public PipePipelinerepository(ItemI item)
	{
		super(item);
	}

	public PipePipelinerepository(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BasePipePipelinerepository(UserI user)
	 **/
	public PipePipelinerepository()
	{}

	public PipePipelinerepository(Hashtable properties, UserI user)
	{
		super(properties,user);
	}


}
