/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class DianRemoteptdata extends BaseDianRemoteptdata {

	public DianRemoteptdata(ItemI item)
	{
		super(item);
	}

	public DianRemoteptdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseDianRemoteptdata(UserI user)
	 **/
	public DianRemoteptdata()
	{}

	public DianRemoteptdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
