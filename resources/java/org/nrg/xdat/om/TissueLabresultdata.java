/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class TissueLabresultdata extends BaseTissueLabresultdata {

	public TissueLabresultdata(ItemI item)
	{
		super(item);
	}

	public TissueLabresultdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseTissueLabresultdata(UserI user)
	 **/
	public TissueLabresultdata()
	{}

	public TissueLabresultdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
