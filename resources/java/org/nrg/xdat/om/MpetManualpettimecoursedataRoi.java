/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class MpetManualpettimecoursedataRoi extends BaseMpetManualpettimecoursedataRoi {

	public MpetManualpettimecoursedataRoi(ItemI item)
	{
		super(item);
	}

	public MpetManualpettimecoursedataRoi(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseMpetManualpettimecoursedataRoi(UserI user)
	 **/
	public MpetManualpettimecoursedataRoi()
	{}

	public MpetManualpettimecoursedataRoi(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
