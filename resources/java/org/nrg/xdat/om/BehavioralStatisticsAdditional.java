/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class BehavioralStatisticsAdditional extends BaseBehavioralStatisticsAdditional {

	public BehavioralStatisticsAdditional(ItemI item)
	{
		super(item);
	}

	public BehavioralStatisticsAdditional(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseBehavioralStatisticsAdditional(UserI user)
	 **/
	public BehavioralStatisticsAdditional()
	{}

	public BehavioralStatisticsAdditional(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
