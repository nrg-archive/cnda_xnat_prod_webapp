/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class UdsA2infdemodata extends BaseUdsA2infdemodata {

	public UdsA2infdemodata(ItemI item)
	{
		super(item);
	}

	public UdsA2infdemodata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsA2infdemodata(UserI user)
	 **/
	public UdsA2infdemodata()
	{}

	public UdsA2infdemodata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
