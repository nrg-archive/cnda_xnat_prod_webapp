/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CondrBraindata extends BaseCondrBraindata {

	public CondrBraindata(ItemI item)
	{
		super(item);
	}

	public CondrBraindata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCondrBraindata(UserI user)
	 **/
	public CondrBraindata()
	{}

	public CondrBraindata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
