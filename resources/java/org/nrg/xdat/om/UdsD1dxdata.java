/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class UdsD1dxdata extends BaseUdsD1dxdata {

	public UdsD1dxdata(ItemI item)
	{
		super(item);
	}

	public UdsD1dxdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsD1dxdata(UserI user)
	 **/
	public UdsD1dxdata()
	{}

	public UdsD1dxdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
