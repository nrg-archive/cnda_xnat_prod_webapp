/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class UdsB3updrsdata extends BaseUdsB3updrsdata {

	public UdsB3updrsdata(ItemI item)
	{
		super(item);
	}

	public UdsB3updrsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsB3updrsdata(UserI user)
	 **/
	public UdsB3updrsdata()
	{}

	public UdsB3updrsdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
