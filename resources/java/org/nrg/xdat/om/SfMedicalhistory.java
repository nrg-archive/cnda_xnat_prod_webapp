/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class SfMedicalhistory extends BaseSfMedicalhistory {

	public SfMedicalhistory(ItemI item)
	{
		super(item);
	}

	public SfMedicalhistory(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseSfMedicalhistory(UserI user)
	 **/
	public SfMedicalhistory()
	{}

	public SfMedicalhistory(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
