/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class Ls2Lshealthdata extends BaseLs2Lshealthdata {

	public Ls2Lshealthdata(ItemI item)
	{
		super(item);
	}

	public Ls2Lshealthdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseLs2Lshealthdata(UserI user)
	 **/
	public Ls2Lshealthdata()
	{}

	public Ls2Lshealthdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
