/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class Ls2Lssubstance extends BaseLs2Lssubstance {

	public Ls2Lssubstance(ItemI item)
	{
		super(item);
	}

	public Ls2Lssubstance(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseLs2Lssubstance(UserI user)
	 **/
	public Ls2Lssubstance()
	{}

	public Ls2Lssubstance(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
