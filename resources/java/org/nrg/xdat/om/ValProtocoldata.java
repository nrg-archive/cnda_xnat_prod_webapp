/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:17:18 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class ValProtocoldata extends BaseValProtocoldata {

	public ValProtocoldata(ItemI item)
	{
		super(item);
	}

	public ValProtocoldata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseValProtocoldata(UserI user)
	 **/
	public ValProtocoldata()
	{}

	public ValProtocoldata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
