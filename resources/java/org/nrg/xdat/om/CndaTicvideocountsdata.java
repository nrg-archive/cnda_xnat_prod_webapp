/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CndaTicvideocountsdata extends BaseCndaTicvideocountsdata {

	public CndaTicvideocountsdata(ItemI item)
	{
		super(item);
	}

	public CndaTicvideocountsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaTicvideocountsdata(UserI user)
	 **/
	public CndaTicvideocountsdata()
	{}

	public CndaTicvideocountsdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
