/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CndaDiagnosesdata extends BaseCndaDiagnosesdata {

	public CndaDiagnosesdata(ItemI item)
	{
		super(item);
	}

	public CndaDiagnosesdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaDiagnosesdata(UserI user)
	 **/
	public CndaDiagnosesdata()
	{}

	public CndaDiagnosesdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
