/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class FsAparcregionanalysisHemisphereRegion extends BaseFsAparcregionanalysisHemisphereRegion {

	public FsAparcregionanalysisHemisphereRegion(ItemI item)
	{
		super(item);
	}

	public FsAparcregionanalysisHemisphereRegion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsAparcregionanalysisHemisphereRegion(UserI user)
	 **/
	public FsAparcregionanalysisHemisphereRegion()
	{}

	public FsAparcregionanalysisHemisphereRegion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
