/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CndaPettimecoursedataDuration extends BaseCndaPettimecoursedataDuration {

	public CndaPettimecoursedataDuration(ItemI item)
	{
		super(item);
	}

	public CndaPettimecoursedataDuration(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaPettimecoursedataDuration(UserI user)
	 **/
	public CndaPettimecoursedataDuration()
	{}

	public CndaPettimecoursedataDuration(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
