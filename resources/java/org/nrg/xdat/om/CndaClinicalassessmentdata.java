//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Aug 18 11:41:39 CDT 2005
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class CndaClinicalassessmentdata extends BaseCndaClinicalassessmentdata {
	public CndaClinicalassessmentdata(ItemI item)
	{
		super(item);
	}

	public CndaClinicalassessmentdata(UserI user)
	{
		super(user);
	}

	public CndaClinicalassessmentdata()
	{}

	public CndaClinicalassessmentdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}
	
	public int getDiagnosisCount()
	{
	    try {
            return getChildItems("Diagnosis").size();
        } catch (Exception e) {
            logger.error("",e);
            return 0;
        }
	}
	
	public ArrayList getDiagnosiss()
	{
	    try {
            return CndaClinicalassessmentdataDiagnosis.wrapItems(getChildItems("Diagnosis"));
        } catch (Exception e) {
            logger.error("",e);
            return new ArrayList();
        }
	}
	
	public int getMedicationCount()
	{
	    try {
            return getChildItems("Medication").size();
        } catch (Exception e) {
            logger.error("",e);
            return 0;
        }
	}
	
	public ArrayList getMedications()
	{
	    try {
            return CndaClinicalassessmentdataMedication.wrapItems(getChildItems("Medication"));
        } catch (Exception e) {
            logger.error("",e);
            return new ArrayList();
        }
	}

}
