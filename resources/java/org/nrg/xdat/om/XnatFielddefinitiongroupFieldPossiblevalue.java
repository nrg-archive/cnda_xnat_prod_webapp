/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:17:18 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatFielddefinitiongroupFieldPossiblevalue extends BaseXnatFielddefinitiongroupFieldPossiblevalue {

	public XnatFielddefinitiongroupFieldPossiblevalue(ItemI item)
	{
		super(item);
	}

	public XnatFielddefinitiongroupFieldPossiblevalue(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatFielddefinitiongroupFieldPossiblevalue(UserI user)
	 **/
	public XnatFielddefinitiongroupFieldPossiblevalue()
	{}

	public XnatFielddefinitiongroupFieldPossiblevalue(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
