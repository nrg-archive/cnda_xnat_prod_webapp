// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Mon Aug 04 12:50:13 CDT 2008
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xdat.security.ElementSecurity;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class PipePipelinedetails extends BasePipePipelinedetails {

	public PipePipelinedetails(ItemI item)
	{
		super(item);
	}

	public PipePipelinedetails(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BasePipePipelinedetails(UserI user)
	 **/
	public PipePipelinedetails()
	{}

	public PipePipelinedetails(Hashtable properties, UserI user)
	{
		super(properties,user);
	}


	public String getRawNamesElementsGeneratedBy() {
		String rtn = "";
		ArrayList<PipePipelinedetailsElement> generatedElements = (ArrayList)getGenerateselements_element(); 
		if (generatedElements.size() > 0) {
				for (int i = 0; i < generatedElements.size(); i++) {
					rtn += generatedElements.get(i).getElement() + ",";
				}
		}
		if (rtn.endsWith(",")) rtn = rtn.substring(0, rtn.length() - 1);
		return rtn;
	}

	
	
}
