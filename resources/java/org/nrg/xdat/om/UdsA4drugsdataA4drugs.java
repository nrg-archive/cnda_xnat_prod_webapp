/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class UdsA4drugsdataA4drugs extends BaseUdsA4drugsdataA4drugs {

	public UdsA4drugsdataA4drugs(ItemI item)
	{
		super(item);
	}

	public UdsA4drugsdataA4drugs(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsA4drugsdataA4drugs(UserI user)
	 **/
	public UdsA4drugsdataA4drugs()
	{}

	public UdsA4drugsdataA4drugs(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
