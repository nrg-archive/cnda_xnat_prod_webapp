/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:17:18 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatRtimagescandata extends BaseXnatRtimagescandata {

	public XnatRtimagescandata(ItemI item)
	{
		super(item);
	}

	public XnatRtimagescandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatRtimagescandata(UserI user)
	 **/
	public XnatRtimagescandata()
	{}

	public XnatRtimagescandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
