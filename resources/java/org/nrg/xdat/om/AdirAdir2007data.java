/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class AdirAdir2007data extends BaseAdirAdir2007data {

	public AdirAdir2007data(ItemI item)
	{
		super(item);
	}

	public AdirAdir2007data(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseAdirAdir2007data(UserI user)
	 **/
	public AdirAdir2007data()
	{}

	public AdirAdir2007data(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
