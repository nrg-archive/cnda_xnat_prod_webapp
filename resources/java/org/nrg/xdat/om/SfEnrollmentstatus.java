/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class SfEnrollmentstatus extends BaseSfEnrollmentstatus {

	public SfEnrollmentstatus(ItemI item)
	{
		super(item);
	}

	public SfEnrollmentstatus(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseSfEnrollmentstatus(UserI user)
	 **/
	public SfEnrollmentstatus()
	{}

	public SfEnrollmentstatus(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
