/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class UdsB5behavasdata extends BaseUdsB5behavasdata {

	public UdsB5behavasdata(ItemI item)
	{
		super(item);
	}

	public UdsB5behavasdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsB5behavasdata(UserI user)
	 **/
	public UdsB5behavasdata()
	{}

	public UdsB5behavasdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
