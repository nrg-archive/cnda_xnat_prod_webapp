/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class GhfFdgcardioassessor extends BaseGhfFdgcardioassessor {

	public GhfFdgcardioassessor(ItemI item)
	{
		super(item);
	}

	public GhfFdgcardioassessor(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseGhfFdgcardioassessor(UserI user)
	 **/
	public GhfFdgcardioassessor()
	{}

	public GhfFdgcardioassessor(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
