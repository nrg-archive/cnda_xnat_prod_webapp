/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:57 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class DianEligdataElig extends BaseDianEligdataElig {

	public DianEligdataElig(ItemI item)
	{
		super(item);
	}

	public DianEligdataElig(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseDianEligdataElig(UserI user)
	 **/
	public DianEligdataElig()
	{}

	public DianEligdataElig(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
