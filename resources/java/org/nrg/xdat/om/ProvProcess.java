/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:17:19 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class ProvProcess extends BaseProvProcess {

	public ProvProcess(ItemI item)
	{
		super(item);
	}

	public ProvProcess(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseProvProcess(UserI user)
	 **/
	public ProvProcess()
	{}

	public ProvProcess(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
