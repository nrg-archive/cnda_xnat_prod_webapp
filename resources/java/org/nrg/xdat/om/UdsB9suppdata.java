/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:58 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class UdsB9suppdata extends BaseUdsB9suppdata {

	public UdsB9suppdata(ItemI item)
	{
		super(item);
	}

	public UdsB9suppdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsB9suppdata(UserI user)
	 **/
	public UdsB9suppdata()
	{}

	public UdsB9suppdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
