/*
 * GENERATED FILE
 * Created on Thu Sep 28 11:19:56 CDT 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CndaAtrophynildata extends BaseCndaAtrophynildata {

	public CndaAtrophynildata(ItemI item)
	{
		super(item);
	}

	public CndaAtrophynildata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaAtrophynildata(UserI user)
	 **/
	public CndaAtrophynildata()
	{}

	public CndaAtrophynildata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
