package org.nrg.pipeline.launchers;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.XnatPipelineLauncher;
import org.nrg.pipeline.utils.PipelineFileUtils;
import org.nrg.pipeline.xmlbeans.ParameterData;
import org.nrg.pipeline.xmlbeans.ParametersDocument.Parameters;
import org.nrg.xdat.base.BaseElement;
import org.nrg.xdat.om.FsFsdata;
import org.nrg.xdat.om.XnatAbstractresource;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.om.XnatResource;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;
import org.nrg.xft.collections.ItemCollection;
import org.nrg.xft.security.UserI;

public class DianPetProcessing extends PipelineLauncher{

	static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DianPetProcessing.class);

	public static final	String STDBUILDTEMPLATE = "PipelineScreen_PetProcessing.vm";


	public boolean launch(RunData data, Context context) {
		boolean rtn = false;
		try {
		ItemI data_item = TurbineUtils.GetItemBySearch(data);
		XnatPetsessiondata imageSession = new XnatPetsessiondata(data_item);
	    String mrsessionId = data.getParameters().get("mprage_sessionId");
	    UserI user = TurbineUtils.getUser(data);
		XnatPipelineLauncher xnatPipelineLauncher = XnatPipelineLauncher.GetLauncher(data, context, imageSession);
		String pipelineName = data.getParameters().get("pipelinename");
		xnatPipelineLauncher.setPipelineName(pipelineName);
		String buildDir = PipelineFileUtils.getBuildDir(imageSession.getProject(), true);
		buildDir +=   "pet"  ;
		xnatPipelineLauncher.setBuildDir(buildDir);
		xnatPipelineLauncher.setNeedsBuildDir(false);

		Parameters parameters = Parameters.Factory.newInstance();
        ParameterData param = parameters.addNewParameter();
    	param.setName("sessionId");
    	param.addNewValues().setUnique(imageSession.getLabel());

		param = parameters.addNewParameter();
    	param.setName("mr_id");
    	param.addNewValues().setUnique(mrsessionId);

		param = parameters.addNewParameter();
    	param.setName("tracer");
    	param.addNewValues().setUnique(imageSession.getTracer_name());


	    //Get all Freesurfers for the selected MR
	    //If more than one, choose by latest in date
	    ArrayList<FsFsdata> fsList = FsFsdata.getFsFsdatasByField("fs:fsData.imageSession_ID", mrsessionId, user, false);
	    String fs_id=null;
	    if (fsList.size()>1) {
	    	ArrayList items = new ArrayList();
	    	for (int j=0; j< fsList.size(); j++) {
	    		items.add(fsList.get(j).getItem());
	    	}
	    	ItemCollection itemsCollection  = new ItemCollection(items);
	    	ArrayList fsSortedByDate = itemsCollection.getItems("fs:fsData.Date","DESC");
	    	fs_id =  ((FsFsdata)BaseElement.GetGeneratedItem((ItemI)fsSortedByDate.get(0))).getId();
	    }else {
	    	fs_id = fsList.get(0).getId();
	    }
	    if (fs_id != null) {
			param = parameters.addNewParameter();
	    	param.setName("fs_id");
	    	param.addNewValues().setUnique(fs_id);
	    }

		String input_param = data.getParameters().get("fwhm");
		param = parameters.addNewParameter();
    	param.setName("fwhm");
    	param.addNewValues().setUnique(input_param);

    	input_param = data.getParameters().get("mst");
		param = parameters.addNewParameter();
    	param.setName("mst");
    	param.addNewValues().setUnique(input_param);

    	input_param = data.getParameters().get("mdt");
		param = parameters.addNewParameter();
    	param.setName("mdt");
    	param.addNewValues().setUnique(input_param);

    	input_param = data.getParameters().get("roisfn");
		param = parameters.addNewParameter();
    	param.setName("roisfn");
    	param.addNewValues().setUnique(input_param);

    	input_param = data.getParameters().get("fslut");
		param = parameters.addNewParameter();
    	param.setName("fslut");
    	param.addNewValues().setUnique(input_param);

    	input_param = data.getParameters().get("pet_scan_id");
		param = parameters.addNewParameter();
    	param.setName("pet_scan_id");
    	param.addNewValues().setUnique(input_param);

    	XnatImagescandata scan = imageSession.getScanById(input_param);
    	List<XnatAbstractresource> files = scan.getFile();
    	boolean isEcat = false;
    	boolean found = false;
    	for (int i=0; i< files.size(); i++) {
    		XnatAbstractresource resource = files.get(i);
    		if (resource instanceof XnatResource) {
    			XnatResource cat  = (XnatResource)resource;
    			String format = cat.getFormat().toUpperCase();
    			if (format!= null) {
    				if (format.equals("DICOM")) {
    					found = true;
    				}else  {
    					isEcat = true;
    					found = true;
    				}
    			}
    		}
    		if (found) break;
    	}

    	if (found) {
    		param = parameters.addNewParameter();
        	param.setName("isecat");
        	param.addNewValues().setUnique(isEcat?"1":"0");
    	}
      	param = parameters.addNewParameter();
        param.setName("petfn");
        param.addNewValues().setUnique("*");


    	input_param = data.getParameters().get("T1");
		param = parameters.addNewParameter();
    	param.setName("t1");
    	param.addNewValues().setUnique(input_param);

    	input_param = data.getParameters().get("WMPARC");
		param = parameters.addNewParameter();
    	param.setName("wmparc");
    	param.addNewValues().setUnique(input_param);

       	input_param = data.getParameters().get("isstat");
		param = parameters.addNewParameter();
    	param.setName("isstat");
    	param.addNewValues().setUnique(input_param);
    	Date date = new Date();
	    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
	    String s = formatter.format(date);

		boolean build = true;
		if (build) {
			String paramFileName = getName(pipelineName);

			paramFileName += "_params_" + s + ".xml";

			String paramFilePath = saveParameters(buildDir+File.separator + imageSession.getLabel(),paramFileName,parameters);

			xnatPipelineLauncher.setParameterFile(paramFilePath);
			rtn = xnatPipelineLauncher.launch();
		}else rtn = true;

		}catch(Exception e) {
			e.printStackTrace();
			logger.debug(e);
		}
		return rtn;
	}


}
