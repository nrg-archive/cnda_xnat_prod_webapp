/*
 * org.nrg.pipeline.launchers.FreesurferLauncher
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/5/13 2:38 PM
 */

package org.nrg.pipeline.launchers;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.XnatPipelineLauncher;
import org.nrg.pipeline.utils.PipelineFileUtils;
import org.nrg.pipeline.xmlbeans.ParameterData;
import org.nrg.pipeline.xmlbeans.ParameterData.Values;
import org.nrg.pipeline.xmlbeans.ParametersDocument.Parameters;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.turbine.utils.TurbineUtils;

public class FreesurferLauncher extends PipelineLauncher{
    ArrayList<String> mprageScans = null;
    static org.apache.log4j.Logger logger = Logger.getLogger(FreesurferLauncher.class);

    public FreesurferLauncher(ArrayList<String> mprs) {
        mprageScans = mprs;
    }

    public FreesurferLauncher(RunData data) {
        mprageScans = getCheckBoxSelections(data,"MPRAGE");
    }

    // Need to override this abstract method in PipelineLauncher
    public boolean launch(RunData data, Context context) {
        return false;
    }


    public boolean launch(RunData data, Context context, XnatImagesessiondata image) {
        try {
            XnatPipelineLauncher xnatPipelineLauncher = XnatPipelineLauncher.GetLauncher(data, context, image);

            String pipelineName = ((String)TurbineUtils.GetPassedParameter("freesurfer_pipelinename",data));

            xnatPipelineLauncher.setPipelineName(pipelineName);
            xnatPipelineLauncher.setSupressNotification(true);

            String buildDir = PipelineFileUtils.getBuildDir(image.getProject(), true);
            buildDir +=  "fsrfer"  ;

            xnatPipelineLauncher.setBuildDir(buildDir);
            xnatPipelineLauncher.setNeedsBuildDir(false);

            Parameters parameters = Parameters.Factory.newInstance();
            ParameterData param;

            if (TurbineUtils.HasPassedParameter("custom_command", data)) {
                param = parameters.addNewParameter();
                param.setName("custom_command");
                param.addNewValues().setUnique(((String)TurbineUtils.GetPassedParameter("custom_command",data)));
            }else {

                param = parameters.addNewParameter();
                param.setName("sessionId");
                param.addNewValues().setUnique(image.getLabel());

                param = parameters.addNewParameter();
                param.setName("isDicom");
                param.addNewValues().setUnique(((String)TurbineUtils.GetPassedParameter("isDicom",data)));

                // Add MPRAGE list
                param = parameters.addNewParameter();
                param.setName("mprs");
                Values values = param.addNewValues();
                if (mprageScans.size() == 1) {
                    values.setUnique(mprageScans.get(0));
                }else {
                    for (int i = 0; i < mprageScans.size(); i++) {
                        values.addList(mprageScans.get(i));
                    }
                }

                param = parameters.addNewParameter();
                param.setName("useall_t1s");
                if (TurbineUtils.HasPassedParameter("useall_t1s", data)) {
                    param.addNewValues().setUnique("1");
                }else {
                    param.addNewValues().setUnique("0");
                }
            }

            // This launcher is never invoked when autorunning
            param = parameters.addNewParameter();
            param.setName("autorun");
            param.addNewValues().setUnique("0");

            param = parameters.addNewParameter();
            param.setName("format");
            param.addNewValues().setUnique(TurbineUtils.HasPassedParameter("format", data) ? (String)TurbineUtils.GetPassedParameter("format",data) : "DICOM");

            String emailsStr = TurbineUtils.getUser(data).getEmail() + "," + data.getParameters().get("emailField");
            String[] emails = emailsStr.trim().split(",");
            for (int i = 0 ; i < emails.length; i++) {
                if (emails[i]!=null && !emails[i].equals(""))  xnatPipelineLauncher.notify(emails[i]);
            }

            String paramFileName = getName(pipelineName);
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            String s = formatter.format(date);

            paramFileName += "_params_" + s + ".xml";

            String paramFilePath = saveParameters(buildDir + File.separator + image.getLabel(),paramFileName,parameters);

            xnatPipelineLauncher.setParameterFile(paramFilePath);

            return xnatPipelineLauncher.launch();
        }catch(Exception e) {
            logger.error(e.getCause() + " " + e.getLocalizedMessage());
            return false;
        }
    }

}
