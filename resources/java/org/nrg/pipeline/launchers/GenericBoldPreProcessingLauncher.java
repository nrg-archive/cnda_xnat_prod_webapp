/*
 * Copyright Washington University in St Louis 2006
 * All rights reserved
 *
 * @author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.nrg.pipeline.launchers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.XnatPipelineLauncher;
import org.nrg.pipeline.utils.PipelineFileUtils;
import org.nrg.pipeline.xmlbeans.ParameterData;
import org.nrg.pipeline.xmlbeans.ParameterData.Values;
import org.nrg.pipeline.xmlbeans.ParametersDocument.Parameters;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;

public class GenericBoldPreProcessingLauncher extends PipelineLauncher{

    static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(GenericBoldPreProcessingLauncher.class);

    public static final String NAME = "GenericBoldPreprocessing.xml";
    public static final String LOCATION = "build-tools";
    public static final String STDBUILDTEMPLATE = "PipelineScreen_GenericBoldPreProcessing.vm";


    public static final String MPRAGE = "MPRAGE";
    public static final String MPRAGE_PARAM = "mprs";
    public static final String TSE = "TSE";
    public static final String TSE_PARAM = "tse";
    public static final String T1W = "T1W";
    public static final String T1W_PARAM = "t1w";
    public static final String PDT2 = "PDT2";
    public static final String PDT2_PARAM = "pdt2";
    public static final String EPI = "BOLD";
    public static final String EPI_PARAM = "fstd";


    public boolean launch(RunData data, Context context) {
        boolean rtn = false;
        try {
            ItemI data_item = TurbineUtils.GetItemBySearch(data);
            XnatMrsessiondata mr = new XnatMrsessiondata(data_item);
            XnatPipelineLauncher xnatPipelineLauncher = XnatPipelineLauncher.GetLauncher(data, context, mr);
            String pipelineName = data.getParameters().get("pipelinename");

            xnatPipelineLauncher.setPipelineName(pipelineName);
            String buildDir = PipelineFileUtils.getBuildDir(mr.getProject(), true);
            buildDir += "stdb" ;
            xnatPipelineLauncher.setBuildDir(buildDir);
            xnatPipelineLauncher.setNeedsBuildDir(false);

            Parameters parameters = Parameters.Factory.newInstance();

            ParameterData param = parameters.addNewParameter();
            param.setName("rm_prev_folder");
            param.addNewValues().setUnique("0");

            param = parameters.addNewParameter();
            param.setName("sessionId");
            param.addNewValues().setUnique(mr.getLabel());

            param = parameters.addNewParameter();
            param.setName("project");
            param.addNewValues().setUnique(mr.getProject());


            boolean build = false;

            for (String paramName : new String[] {"target","TR_vol","skip","nx","ny","TR_slc"}) {
                if (TurbineUtils.HasPassedParameter(paramName, data)) {
                    String val = data.getParameters().get(paramName);
                    if (StringUtils.isNotBlank(val)) {
                        param = parameters.addNewParameter();
                        param.setName(paramName);
                        param.addNewValues().setUnique(val);
                    }
                }
            }

            if (TurbineUtils.HasPassedParameter("cross_day_register", data)) {
                String day1_sessionId = data.getParameters().get("cross_day_register");
                String day1_resourceLabel = data.getParameters().get("recon");
                if (!day1_sessionId.equals("-1")) {
                    XnatImagesessiondata cross = XnatImagesessiondata.getXnatImagesessiondatasById(day1_sessionId,TurbineUtils.getUser(data),false);
                    if (cross != null) {
                        param = parameters.addNewParameter();
                        param.setName("day1_sessionId");
                        param.addNewValues().setUnique(day1_sessionId);

                        param = parameters.addNewParameter();
                        param.setName("day1_sessionLabel");
                        param.addNewValues().setUnique(cross.getLabel());

                        param = parameters.addNewParameter();
                        param.setName("day1_resourceLabel");
                        param.addNewValues().setUnique(day1_resourceLabel);

                        param = parameters.addNewParameter();
                        param.setName("do_x_day_reg");
                        param.addNewValues().setUnique("true");
                    }
                }
            }



            ArrayList<String> bold = getCheckBoxSelections(data,mr,EPI);
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            String s = formatter.format(date);

            HashMap<String,String> scanParamNames = new HashMap<String, String>();
            scanParamNames.put(MPRAGE,MPRAGE_PARAM);
            scanParamNames.put(TSE,TSE_PARAM);
            scanParamNames.put(T1W,T1W_PARAM);
            scanParamNames.put(PDT2,PDT2_PARAM);

            for (Map.Entry<String,String> scanParamName : scanParamNames.entrySet()){
                if (TurbineUtils.HasPassedParameter("build_"+scanParamName.getKey(),data)) {
                    ArrayList<String> scans = getCheckBoxSelections(data,mr,scanParamName.getKey());

                    param = parameters.addNewParameter();
                    param.setName(scanParamName.getValue());
                    Values values = param.addNewValues();

                    for (String scan : scans) {
                        values.addList(scan);
                    }
                    build = true;
                }
            }


            if (TurbineUtils.HasPassedParameter("build_" + EPI, data)) {
                // xnatPipelineLauncher.setParameter("fstd",bold);
                param = parameters.addNewParameter();
                param.setName("fstd");
                Values values = param.addNewValues();
                for (String b : bold) {
                    values.addList(b);
                }

                if (bold.size() > 0) {
                    ArrayList<String> irun = getRunLabels(data,EPI);
                    param = parameters.addNewParameter();
                    param.setName("irun");
                    values = param.addNewValues();
                    for (String ir : irun) {
                        values.addList(ir);
                    }

                    for (String paramName : new String[] {"epidir","epi2atl","normode","economy","Siemens_interleave"}) {
                        if (TurbineUtils.HasPassedParameter(paramName, data)) {
                            if (StringUtils.isNotBlank(data.getParameters().get(paramName))) {
                                param = parameters.addNewParameter();
                                param.setName(paramName);
                                param.addNewValues().setUnique(data.getParameters().get(paramName));
                            }
                        }
                    }

                    ArrayList<String> functionalScans = getCheckBoxSelections(data,mr,"functional_"+EPI, data.getParameters().getInt(EPI+"_rowcount"));
                    if (functionalScans.size() > 0) {
                        ArrayList<String> functional = getRunLabels(bold,irun,functionalScans);
                        // xnatPipelineLauncher.setParameter("fcbolds",functional);
                        param = parameters.addNewParameter();
                        param.setName("fcbolds");
                        values = param.addNewValues();
                        for (String fc : functional) {
                            values.addList(fc);
                        }
                        // xnatPipelineLauncher.setParameter("preprocessFunctional","1");
                        param = parameters.addNewParameter();
                        param.setName("preprocessFunctional");
                        param.addNewValues().setUnique("1");
                    }
                    createRunLabelAssignmentFile(buildDir+File.separator + mr.getLabel(), bold, irun, mr);
                }
                build = true;
            }

            if (build) {
                String paramFileName = getName(pipelineName);

                paramFileName += "_params_" + s + ".xml";

                String paramFilePath = saveParameters(buildDir+File.separator + mr.getLabel(),paramFileName,parameters);

                xnatPipelineLauncher.setParameterFile(paramFilePath);
                rtn = xnatPipelineLauncher.launch();
            }else rtn = true;

        }catch(Exception e) {
            logger.debug(e);
        }
        return rtn;
    }

    private boolean createRunLabelAssignmentFile(String rootpath, ArrayList bolds, ArrayList runlabels, XnatMrsessiondata mr) {
        boolean rtn = false;
        Writer output = null;
        File dir = new File(rootpath);
        if (!dir.exists()) dir.mkdirs();

        File file = new File(rootpath + File.separator + "scanmapping.txt");
        try {
            output = new BufferedWriter(new FileWriter(file));
            output.write("Folder \t Scan Number \t Scan Type" + "\n");
            for (int i = 0; i < bolds.size(); i++) {
                String scanid = (String)bolds.get(i);
                String runlabel = (String)runlabels.get(i);
                XnatImagescandata imageScan = mr.getScanById(scanid);
                output.write("bold" + runlabel + " \t " + scanid + " \t " + imageScan.getType() + "\n");
            }
        }catch(Exception e ) {
            logger.debug("Unable to save scanmapping file ",e);
        }finally {
            if (output != null) {
                try {
                    output.close();
                }catch(IOException ioe) {
                    logger.debug(ioe);
                }
            }
        }
        return rtn;
    }

    private ArrayList<String> getRunLabels(RunData data) {
        int totalCount = data.getParameters().getInt(EPI+"_rowcount");
        ArrayList<String> rtn = new ArrayList<String>();
        for (int i = 0; i < totalCount; i++) {
            if (TurbineUtils.HasPassedParameter(EPI+"_"+i, data)) {
                rtn.add(data.getParameters().get("boldrunlabel_" + EPI+"_"+i).trim());
            }
        }
        return rtn;
    }


    private ArrayList<String> getRunLabels(RunData data, String type) {
        ArrayList<String> rtn = null;
        int totalCount = data.getParameters().getInt(type+"_rowcount");
        rtn = new ArrayList<String>();
        for (int i = 0; i < totalCount; i++) {
            String label = data.getParameters().getString("boldrunlabel_"+type+"_"+i);
            if (label != null && !label.equals("")) {
                rtn.add(label);
            }
        }
        return rtn;
    }

    private ArrayList<String> getRunLabels(ArrayList<String> boldScans) {
        ArrayList<String> rtn = new ArrayList<String>();
        for (int i = 0; i < boldScans.size(); i++) {
            rtn.add("run"+(i+1));
        }
        return rtn;
    }

    private ArrayList<String> getRunLabels(ArrayList<String> boldScans,ArrayList<String> runLabels,ArrayList<String> functionalScans ) {
        ArrayList<String> rtn = new ArrayList<String>();
        for (int i = 0; i < functionalScans.size(); i++) {
            for (int j=0; j<boldScans.size(); j++) {
                if (boldScans.get(j).equals(functionalScans.get(i))) {
                    rtn.add(runLabels.get(j));
                    break;
                }
            }
        }
        return rtn;
    }

}
