/*
 * org.nrg.pipeline.launchers.RegisterMRToAtlasLauncher
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
*/

package org.nrg.pipeline.launchers;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.XnatPipelineLauncher;
import org.nrg.pipeline.utils.PipelineFileUtils;
import org.nrg.pipeline.xmlbeans.ParameterData;
import org.nrg.pipeline.xmlbeans.ParameterData.Values;
import org.nrg.pipeline.xmlbeans.ParametersDocument.Parameters;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;

public class RegisterMRToAtlasLauncher extends PipelineLauncher{
    static org.apache.log4j.Logger logger = Logger.getLogger(RegisterMRToAtlasLauncher.class);

    public boolean launch(RunData data, Context context) {
        try {
            ItemI data_item = TurbineUtils.GetItemBySearch(data);
            XnatImagesessiondata image = new XnatImagesessiondata(data_item);

            XnatPipelineLauncher xnatPipelineLauncher = XnatPipelineLauncher.GetLauncher(data, context, image);

            String pipelineName = ((String)TurbineUtils.GetPassedParameter("registermr_pipelinename",data));

            xnatPipelineLauncher.setPipelineName(pipelineName);
            xnatPipelineLauncher.setSupressNotification(true);

            String buildDir = PipelineFileUtils.getBuildDir(image.getProject(), true);
            buildDir +=  "registermr"  ;

            xnatPipelineLauncher.setBuildDir(buildDir);
            xnatPipelineLauncher.setNeedsBuildDir(false);

            Parameters parameters = Parameters.Factory.newInstance();
            ParameterData param;

            String[] stringParams = new String[] {"atlas","format"};
            for (String paramName : stringParams) {
                if (TurbineUtils.HasPassedParameter(paramName, data)) {
                    param = parameters.addNewParameter();
                    param.setName(paramName);
                    param.addNewValues().setUnique((String) TurbineUtils.GetPassedParameter(paramName, data));
                }
            }

            // MR list
            ArrayList<String> mrScans = getCheckBoxSelections(data,"scan");
            param = parameters.addNewParameter();
            param.setName("scanIds");
            Values values = param.addNewValues();
            values.setListArray(mrScans.toArray(new String[mrScans.size()]));

            // We will never use this launcher when autorunning, so always set it to "0"
            param = parameters.addNewParameter();
            param.setName("autorun");
            param.addNewValues().setUnique("0");

            String paramFileName = getName(pipelineName);
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            String s = formatter.format(date);

            paramFileName += "_params_" + s + ".xml";

            String paramFilePath = saveParameters(buildDir + File.separator + image.getLabel(),paramFileName,parameters);

            xnatPipelineLauncher.setParameterFile(paramFilePath);

            return xnatPipelineLauncher.launch();
        }catch(Exception e) {
            logger.error(e.getCause() + " " + e.getLocalizedMessage());
            return false;
        }
    }

}
