<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2//EN">
$page.setTitle("CONDR Medical History Details")
$page.setLinkColor($ui.alink)
$page.setVlinkColor($ui.vlink)
#if ($data.getParameters().getString("popup"))
#set ($popup = $data.getParameters().getString("popup") )
#set ($popup = "false")
#end
#if($project)

#else
#set($project=$om.getProject())
#end

<style type="text/css">
	.report th { text-align: left; }
	.standard { width: 400px; }
	.report, .report td, .report th { border: 1px solid gray; border-collapse:collapse; }
</style>

<!-- abbreviate long text fields -->
<script type="text/javascript">
YAHOO.util.Event.onDOMReady(function() {
	var longTexts = YAHOO.util.Dom.getElementsByClassName("longText");

	YAHOO.util.Dom.batch(longTexts, function (el) {
    	el.title = el.alt = el.innerHTML;
		if(el.innerHTML.length > 30) {
			el.innerHTML = el.innerHTML.slice(0, 30) + "...";
		}
    })
});
</script>

<table width="100%">
	<tr>
		<td>#parse($turbineUtils.getTemplateName("_report",$om.getXSIType(),$project))
			
		</td>
		<td valign="top" align="right">
			#elementActionsBox($element $search_field $search_value $data.getSession().getAttribute("user") $item)
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<font face="$ui.sansSerifFonts" size="3"><b>CONDR Medical History Details</b></font><br/><br/>

			<BR/><font face="$ui.sansSerifFonts" size="2"><b>Medical History</B></font><BR/><BR/>

			<TABLE class="report">
				<TR>
					<TH>Status</TH>
					<TH>Body System</TH>
					<TH>Condition</TH>
					<TH>Condition Notes</TH>
					<TH>Diagnosis Date</TH>
				</TR>
				
				#set($conditions = ["hypertension", "diabetes", "hypercholesterolemia", "stroke", "autoimmuneDisease", "headache", "seizure" ])
				
				#foreach($condition in $conditions)
				<TR>
					<TD>$!item.getStringProperty("condr:condrMedicalHistory/${condition}/conditionStatus")</TD>
					<TD>$!item.getStringProperty("condr:condrMedicalHistory/${condition}/bodySystem")</TD>
					<TD>$!item.getStringProperty("condr:condrMedicalHistory/${condition}/condition")</TD>
					#if ($!item.getStringProperty("condr:condrMedicalHistory/${condition}/conditionStatus") == "Current" || $!item.getStringProperty("condr:condrMedicalHistory/${condition}/conditionStatus") == "Past")
					<TD class="longText">$!item.getStringProperty("condr:condrMedicalHistory/${condition}/conditionNotes")</TD>
					<TD>#xdatViewDateNotReported("condr:condrMedicalHistory/${condition}/diagnosisDate" $item)</TD>
					#else
					<TD class="longText">&nbsp;</TD>
					<TD>&nbsp;</TD>
					#end
				</TR>
				#end
				
				#if($item.getChildItems("condr:condrMedicalHistory/conditions/condition"))
					#set($sf_condition_1_NUM_ROWS = $item.getChildItems("condr:condrMedicalHistory/conditions/condition").size() - 1)
				#else
					#set($sf_condition_1_NUM_ROWS = -1)
				#end
				
				#if($sf_condition_1_NUM_ROWS >= 0)
					#foreach($sf_condition_1_COUNTER in [0..$sf_condition_1_NUM_ROWS])
					<TR>
						<TD>$!item.getStringProperty("condr:condrMedicalHistory/conditions/condition[$sf_condition_1_COUNTER]/conditionStatus")</TD>
						<TD>$!item.getStringProperty("condr:condrMedicalHistory/conditions/condition[$sf_condition_1_COUNTER]/bodySystem")</TD>
						<TD>$!item.getStringProperty("condr:condrMedicalHistory/conditions/condition[$sf_condition_1_COUNTER]/condition")</TD>
						#if ($!item.getStringProperty("condr:condrMedicalHistory/conditions/condition[$sf_condition_1_COUNTER]/conditionStatus") == "Current" || $!item.getStringProperty("condr:condrMedicalHistory/conditions/condition[$sf_condition_1_COUNTER]/conditionStatus") == "Past")
						<TD class="longText">$!item.getStringProperty("condr:condrMedicalHistory/conditions/condition[$sf_condition_1_COUNTER]/conditionNotes")</TD>
						<TD>#xdatViewDateNotReported("condr:condrMedicalHistory/conditions/condition[$sf_condition_1_COUNTER]/diagnosisDate" $item)</TD>
						#else
						<TD class="longText">&nbsp;</TD>
						<TD>&nbsp;</TD>
						#end						
					</TR>
					#end
				#end
			</TABLE>

			<BR/><font face="$ui.sansSerifFonts" size="2"><b>Neurological Symptoms at Enrollment</B></font><BR/><BR/>
			
			<TABLE class="standard report">
				<TR><TH>Deficit Name</TH><TH>Present?</TH></TR>
				<TR><TD>Motor Weakness</TD><TD>$!item.getBooleanProperty("condr:condrMedicalHistory/deficits/motorWeakness")</TD></TR>
				<TR><TD>Sensory Change</TD><TD>$!item.getBooleanProperty("condr:condrMedicalHistory/deficits/sensoryChange")</TD></TR>
				<TR><TD>Severe Headache</TD><TD>$!item.getBooleanProperty("condr:condrMedicalHistory/deficits/severeHeadache")</TD></TR>
				<TR><TD>Nausea</TD><TD>$!item.getBooleanProperty("condr:condrMedicalHistory/deficits/nausea")</TD></TR>
				<TR><TD>Visual Loss</TD><TD>$!item.getBooleanProperty("condr:condrMedicalHistory/deficits/visualLoss")</TD></TR>
				<TR><TD>Cognitive Decline</TD><TD>$!item.getBooleanProperty("condr:condrMedicalHistory/deficits/cognitiveDecline")</TD></TR>
				<TR><TD>Seizure</TD><TD>$!item.getBooleanProperty("condr:condrMedicalHistory/deficits/seizure")</TD></TR>

				#if($item.getChildItems("condr:condrMedicalHistory/deficits/otherDeficits/otherDeficit"))
					#set($sf_deficits_otherDeficit_13_NUM_ROWS = $item.getChildItems("condr:condrMedicalHistory/deficits/otherDeficits/otherDeficit").size() - 1)
				#else
					#set($sf_deficits_otherDeficit_13_NUM_ROWS = -1)
				#end
				
				#if($sf_deficits_otherDeficit_13_NUM_ROWS >= 0)
					#foreach($sf_deficits_otherDeficit_13_COUNTER in [0..$sf_deficits_otherDeficit_13_NUM_ROWS])
						<TR>
							<TD>$!item.getStringProperty("condr:condrMedicalHistory/deficits/otherDeficits/otherDeficit[$sf_deficits_otherDeficit_13_COUNTER]/otherDeficitName")</TD>
							<TD>$!item.getBooleanProperty("condr:condrMedicalHistory/deficits/otherDeficits/otherDeficit[$sf_deficits_otherDeficit_13_COUNTER]/otherDeficitPresent")</TD>
						</TR>
					#end
				#end
			</TABLE>
			
			<BR/><font face="$ui.sansSerifFonts" size="2"><b>Allergies</B></font><BR/><BR/>
			
			<TABLE class="standard report">
				#if($item.getChildItems("condr:condrMedicalHistory/allergies/allergy").size())
					#set($sf_medicalHistory_allergy_11_NUM_ROWS = $item.getChildItems("condr:condrMedicalHistory/allergies/allergy").size() - 1)
				#else
					#set($sf_medicalHistory_allergy_11_NUM_ROWS = -1)
				#end
				
				#if($sf_medicalHistory_allergy_11_NUM_ROWS >= 0)
					#foreach($sf_medicalHistory_allergy_11_COUNTER in [0..$sf_medicalHistory_allergy_11_NUM_ROWS])
						<TR><TD>$!item.getStringProperty("condr:condrMedicalHistory/allergies/allergy[$sf_medicalHistory_allergy_11_COUNTER]/allergy")</TD></TR>
					#end
				#end
			</TABLE>
			
			<BR/><font face="$ui.sansSerifFonts" size="2"><b>Social History</B></font><BR/><BR/>
			
			<TABLE class="standard report">
				<TR>
					<TH>Condition</TH>
					<TH>Status</TH>
					<TH>Extent/Notes</TH>
				</TR>
				<TR>
					<TD>Smoking</TD>
					<TD>$!item.getStringProperty("condr:condrMedicalHistory/smokingStatus")</TD>
					<TD>$!item.getProperty("condr:condrMedicalHistory/smokingPpd")</TD>
				</TR>
				<TR>
					<TD>Alcohol</TD>
					<TD>$!item.getStringProperty("condr:condrMedicalHistory/alcoholStatus")</TD>
					<TD>$!item.getStringProperty("condr:condrMedicalHistory/alcoholUse")</TD>
				</TR>
				<TR>
					<TD>Illicit Drugs</TD>
					<TD>$!item.getStringProperty("condr:condrMedicalHistory/illicitDrugStatus")</TD>
					<TD>$!item.getStringProperty("condr:condrMedicalHistory/illicitDrugDetails")</TD>
				</TR>
			</TABLE>
			
			<BR/><font face="$ui.sansSerifFonts" size="2"><b>Family History</B></font><BR/><BR/>
			
			<TABLE class="standard report">
				<TR><TD>Family History of Neurological Tumor Syndrome:</TD><TD>$!item.getBooleanProperty("condr:condrMedicalHistory/familyTumorSyndrome")</TD></TR>
				#if($!item.getBooleanProperty("condr:condrMedicalHistory/familyTumorSyndrome"))
				<TR><TD>Tumor Predisposition:</TD><TD>$!item.getStringProperty("condr:condrMedicalHistory/tumorPredisposition")</TD></TR>
				#end				
				<TR><TD>Family History of Non-Neurological Tumor:</TD><TD>$!item.getBooleanProperty("condr:condrMedicalHistory/familyTumorSpecificType")</TD></TR>
				<TR><TD>Family History Notes:</TD><TD>$!item.getStringProperty("condr:condrMedicalHistory/familyTumorNotes")</TD></TR>
			</TABLE>
			
			<BR/><font face="$ui.sansSerifFonts" size="2"><b>Other</B></font><BR/><BR/>
			
			<TABLE class="standard report">
				<TR><TD>Medical History Notes:</TD><TD>$!item.getStringProperty("condr:condrMedicalHistory/medicalHistoryNotes")</TD></TR>
			</TABLE>
		</TD>
	</TR>
	
</TABLE><BR>#parse("/screens/ReportProjectSpecificFields.vm")
