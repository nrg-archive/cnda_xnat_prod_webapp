##Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2//EN">
    #set ($template = $data.getTemplateInfo())
    $!template.setLayoutTemplate("/Popup_empty.vm")

<!-- get Javascript Dependencies -->
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<link href="$content.getURI("/style/pipemodal.css")" rel="stylesheet" />

<script language="Javascript">
    var launchInProgress = false;

    function validate() {
        var scansSelected = true;
        var requiredFields = {
            t1s: 'input',
            flairs: 'input'
        };
        for (var fieldName in requiredFields) {
            var eleVal = [];
            if (requiredFields[fieldName] === 'checkbox') {
                eleVal = [];
                $('input[name=' + fieldName + ']:checked').each(function () {
                    eleVal.push($(this).val());
                });
                if (eleVal.length === 0) {
                    scansSelected = false;
                    break;
                }
            } else {
                eleVal = $(requiredFields[fieldName] + "[name=" + fieldName + "]").val()
                if (eleVal.length === 0) {
                    scansSelected = false;
                    break;
                }
            }

        }
        return scansSelected;
    }

    function noneClicked(noneObj) {
        var type = noneObj.getAttribute("class")
        if (noneObj.checked) {
            var checkedScans = $("input[name^="+type+"]:checked");
            var l = checkedScans.length;
            for (var i = 0; i < l; i++) {
                $(checkedScans[i]).prop("checked", false)
            }
        }
    }

    function scanClicked(scanObj) {
        var type = scanObj.getAttribute("class")
        if (scanObj.checked) {
            $("#x"+type).prop("checked", false)
        } else {
            var checkedScans = $("input[name^="+type+"]:checked");
            if (checkedScans && checkedScans.length > 0) {
                $("#x"+type).prop("checked", "checked")
            }
        }
    }


    /* page config */

    // set popup window title properly
    $(document).prop("title","Launch White Matter Hypointensities pipeline for $om.label");

    $(document).ready(function(){
        // set display height and width according to popup size
        $("#pipeline_modal").width($(window).width());
        $("#pipeline_modal").height($(window).height());
        $("#pipeline_modal .body").height($(window).height()-$("#pipeline_modal .footer").height());

        // Show the DICOM / NIFTI selection if any NIFTI exist on the session
        if ("$hasNifti" == "true") {
            $("#format-param").show();
        }
        clicker('t1s');
        clicker('flairs');

    });

    // toggle visibility of advanced parameters. (parameter values are always passed to form, whether or not they are visible)
    // $("#advanced-params-toggle").on("click",function(){
    //     var adv = $("#advanced-params");
    //     if ($(adv).hasClass("hidden")) {
    //         $(this).html("Hide");
    //         $(adv).removeClass("hidden");
    //     } else {
    //         $(this).html("Show");
    //         $(adv).addClass("hidden");
    //     }
    // });
    // function advanced_params_toggle(){
//         var adv = $("#advanced-params");
//         if ($(adv).hasClass("hidden")) {
//             $(this).html("Hide");
//             $(adv).removeClass("hidden");
//         } else {
//             $(this).html("Show");
//             $(adv).addClass("hidden");
//         }
//     }

    function clicker(type) {
        var clickedVals = [];
        $('input.' + type + '-clicker:checked').each(function(){
            clickedVals.push($(this).val());
        });
        $('input[name=' + type + ']').val(clickedVals.join());
    }

    function confirm() {
        xmodal.confirm({
            content: "Run pipeline with these parameters?",
            cancelLabel: "Cancel",
            cancelAction: "Return",
            okAction: launch
        });
    }

    function popupCloser(){
        // reload the session window that opened this popup, then close the popup
        window.opener.location.reload();
        self.close();
    }

    function launch(){
        var valid = validate();
        if (valid && !launchInProgress) {
            launchInProgress = true;

            // add brackets around scan id list
            var t1List = $('input[name=t1s]').val();
            if (t1List.indexOf('[') > -1 && t1List.indexOf(']') > -1) {
                $('input[name=t1s]').val("["+t1List+"]");
            }

            var flairList = $('input[name=flairs]').val();
            if (flairList.indexOf('[') > -1 && flairList.indexOf(']') > -1) {
                $('input[name=flairs]').val("["+flairList+"]");
            }

            // collect all form parameters and submit to pipeline handler
            var params = $('form[name=LaunchWMH]').serialize();
            $.ajax({ url: '/REST/projects/$project/pipelines/WMH/experiments/$om.getId()?'+params, method: 'POST' })
                    .done( function(){ launchInProgress = false; } )
                    .error( function(){ alert("Something went wrong,") })
                    .success( function(){
                        xmodal.message({
                            content: "Pipeline successfully queued.",
                            okAction: popupCloser
                        });
                    });
        } else if (!valid) {
            xmodal.message("Required fields are empty. Please check your inputs.");
        }
    }



</script>

<!-- create an xmodal container inside the popup window -->
<div id="pipeline_modal" class="pipemodal xmodal fixed embedded">
    <div class="body content scroll">
        <!-- handle error messages -->
        #if ($data.message)
            <div class="error">$data.message</div>
        #end

        <div class="inner">
            <!-- modal body start -->

            <form name="LaunchWMH" method="post" onsubmit="return validate();" class="optOutOfXnatDefaultFormValidation">
                <h2>Set Execution Parameters for $om.label</h2>

                <fieldset>
                    <h3>Select T1 scan from $om.label</h3>

                    #foreach ($scan in $t1Scans)
                        #if ($scan.getQuality().equalsIgnoreCase("usable"))
                            #set ($attrib="checked")
                        #else
                            #set ($attrib="")
                        #end
                        <p><label><input type="checkbox" value="$scan.Id" $attrib class="scan t1s-clicker" data-format="${allScanResourceLabels.get("t1-${scan.Id}")}">$scan.Id (<b>$!scan.getType()</b> Quality: $scan.Quality)</label></p>

                    #end
                    <input name="t1s" value="" type="hidden" />
                </fieldset>

                <script>
                    $('input.t1s-clicker').on('click',function(){
                        clicker('t1s');
                    });
                </script>

                <fieldset>
                    <h3>Select FLAIR scan</h3>

                    #foreach ($flairSession in $flairScanMap.keySet())
                        #foreach ($scan in $flairScanMap.get($flairSession))
                            #if ($scan.getQuality().equalsIgnoreCase("usable"))
                                #set ($attrib="checked")
                            #else
                                #set ($attrib="")
                            #end
                            <p><label><input type="checkbox" value="${flairSession.Id}-${scan.Id}" $attrib class="scan flairs-clicker" data-format="${allScanResourceLabels.get("${flairSession.Id}-${scan.Id}")}">$flairSession.label: $scan.Id (<b>$!scan.getType()</b> Quality: $scan.Quality)</label></p>
                        #end
                    #end
                    <input name="flairs" value="" type="hidden" />
                </fieldset>

                <script>
                    $('input.flairs-clicker').on('click',function(){
                        clicker('flairs');
                    });
                </script>

                <div class="hidden" id="format-param">
                    <fieldset>
                    <!-- <p>Advanced Parameters (<a href="javascript:" id="advanced-params-toggle" onclick="advanced_params_toggle()">Show</a>)</p>
                    <div class="hidden" id="advanced-params"> -->
                            <p><label for="format" title="File type to pass to pipeline">Input data file type</label>
                                <input type="radio" name="format" id="format" value="DICOM" checked>DICOM</input>
                                <input type="radio" name="format" id="format" value="NIFTI">NIFTI</input>
                            </p>
                    <!-- </div> -->
                    <p></p>

                    </fieldset>
                </div>

            </form>

            <!-- modal body end -->
        </div>
    </div>
    <div class="footer" style="background-color: rgb(240, 240, 240); border-color: rgb(224, 224, 224);">
        <!-- multi-panel controls are placed in footer -->
        <div class="inner">
            <span class="buttons">
                <a class="cancel close button" href="javascript:self.close()">Cancel</a>
                <a id="button-advance" class="default button panel-toggle" href="javascript:" onclick="confirm()">Submit Pipeline</a>
            </span>
        </div>
    </div>
</div>
<!-- end of xmodal container -->
