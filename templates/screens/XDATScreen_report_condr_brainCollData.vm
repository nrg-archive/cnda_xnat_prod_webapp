<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2//EN">
## #set(#debugMode = false)

$page.setTitle("BrainColl Details")
$page.setLinkColor($ui.alink)
$page.setVlinkColor($ui.vlink)
#if ($data.getParameters().getString("popup"))
	#set ($popup = $data.getParameters().getString("popup") )
	#set ($popup = "false")
#end

#set($years = [ 2011..1900])
#set($tumorTypeOptions = [["Primary","Primary"],["Metastatic","Metastatic"]])
#set($primaryTumorTypeOptions = ["Astrocytoma","Oligodendroglioma","Oligoastrocytoma","Ependymoma","Lymphoma","Meningioma","N/A"])
#set($whoGradeOptions = [["I","I"],["II","II"],["III","III"],["IV","IV"],["N/A","N/A"]])
#set($metastatOrigOptions = ["Breast","Lung - Non-Small Cell","Lung - Small Cell","Colon","Melanoma","Ovarian","Lymphoma","N/A"])
#set($onep19q = [["Present","Present"],["Absent","Absent"],["Not Reported","Not Reported"]])
#set($mgmtPromoterStatus = [["Methylated","Methylated"],["Unmethylated","Unmethylated"],["Not Reported","Not Reported"]])
#set($pten = [["Wild Type","Wild Type"],["Deleted/Mutated","Deleted/Mutated"],["Not Reported","Not Reported"]])
#set($p53 = [["Wild Type","Wild Type"],["Deleted/Mutated","Deleted/Mutated"],["Not Reported","Not Reported"]])
#set($coordSource = [["Screen Capture","Screen Capture"],["Log File","Log File"],["Manual Review","Manual Review"]])

#set($user = $data.getSession().getAttribute("user"))
#set($canCreate = $item.canCreate($user))
#set($canEdit = $item.canEdit($user))
#set($canDelete = $item.canDelete($user))

#set($radiographicAppearenceOptions=["Necrotic","Enhancing","Non-Enchancing Tumor","Other Abnormal","Elevated Perfusion","Normal","Not Reported"])

#set($deleteIcon = "/images/close.jpg")
#set($imageIcon = "/images/camera.gif")
#set($addCommentIcon = "/images/plus.gif")
#set($editBrainDataIcon = "/images/e.gif")
#set($toggleBrainDataIcon = "/images/plus.jpg")
#set($toggleBrainDataIconMinus = "/images/minus.jpg")
#set($brainSpecColSpan = "11")
#set($commentSeperator = "**********")
#set($commentSplit = "~~~~~~~~~~")
#set($maxOtherFeatLength = 25000)

#cndaMacroJavascript()

<link rel="stylesheet" type="text/css" href="$content.getURI('scripts/yui/build/button/assets/skins/sam/button.css')" /> 
<link rel="stylesheet" type="text/css" href="$content.getURI('scripts/yui/build/container/assets/skins/sam/container.css')" />

<!-- Manage files support -->
<script type="text/javascript" src="$content.getURI("scripts/FileViewer.js")"></script>
<script type="text/javascript">
var obj = new Object();
obj.uri = serverRoot + "/REST/projects/$!om.getProject()/subjects/$!item.getProperty('subject_id')/experiments/$!om.getId()";

#if ($item.canEdit($data.getSession().getAttribute("user")))
obj.canEdit=true;
#else
obj.canEdit=false;
#end

obj.catalogs=new Object();
obj.catalogs.ids=new Array();
window.viewer=new FileViewer(obj);

function showFiles(){
	window.viewer.render();
}
</script>

<!-- Dependencies -->
<script src="$content.getURI('scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js')" type="text/javascript"></script>
  
<!-- Source file -->
<script src="$content.getURI('scripts/yui/build/animation/animation-min.js" type="text/javascript')"></script>

<script type="text/javascript" src="$content.getURI('scripts/yui/build/element/element-min.js')"></script> 
<script type="text/javascript" src="$content.getURI('scripts/yui/build/button/button-min.js')"></script> 
<script type="text/javascript" src="$content.getURI('scripts/yui/build/dragdrop/dragdrop-min.js')"></script> 
<script type="text/javascript" src="$content.getURI('scripts/yui/build/container/container-min.js')"></script> 

<script type="text/javascript" src="$content.getURI('scripts/CONDR/PathologyReport.js')"></script>
<script type="text/javascript" src="$content.getURI('scripts/CONDR/FileManager.js')"></script>

<style type="text/css">
## img { -ms-interpolation-mode:bicubic; }
.specDetailsDiv {
	overflow: hidden;
	position: relative;
}
td.label {
	font-weight: bold
}
.button {		
				display: inline-block;
					height:13px; 
					width:13px;
					margin: 1px;
					padding:2px;
					border: 1px solid;
 			   	border-radius: 3px;
					-moz-border-radius: 3px;
					-webkit-border-radius: 3px;
					-khtml-border-radius: 3px;
				font: 14px/12px Verdana, Geneva, sans-serif;
					font-weight: bold;
					text-align:center;
}
.smallButton {		
				display: inline-block;
					height: 10px; 
					width: 10px;
					margin: 1px;
					padding: 1px;
					border: 1px solid;
 			   	border-radius: 2px;
					-moz-border-radius: 2px;
			    	-webkit-border-radius: 2px;
			    	-khtml-border-radius: 2px;
				font: 9px/9px Verdana, Geneva, sans-serif;
					font-weight: bold;
					text-align: center;
}
.noImage {
		background:#eee;
		color:#ccc;
		border-color: #ccc
}
.noImage:hover {
	background:#fff; 
}
.needsImage {
	background:#a00;
	color:#ffc;
	border-color: #c00;
}
.needsImage:hover {
	background:#F00;
}
.hasImage {
		background:#efe;
		color:#050;
		border-color: #333;
}
.hasImage:hover	{
	background: #cf0;
}
table.ruled {
	background:#bbb;
}
table.ruled tr {
	background:#fff;
	text-align:center;
}
div.notes {
	height: 70px;
	overflow: auto;
	border-style:solid;
	border-width:1px;
}
img.thumbnailImg {
	border-style: none;
	width: 120px;
	height: auto;
}
##div.thumbnailDiv {
div.mediaDiv {
	position: relative;
	margin: 0px;
	border-style: none;
	width = 120px;
##	height: auto;
##	white-space: nowrap;
}
##href.thumbnailLink {
href.mediaLink {
	margin: 0px;
	border-style: none;
##	height: auto;	
}
td.mediaTableContainer {
	vertical-align: top;
	text-align: center;
	width: 120px;
	padding: 0px;
	border-left: 1px solid white;
}


table.fileImageTable {
	width: 120px;
	padding: 0px;
	cell-spacing: 0px;
	border-spacing: 0px;
	border-collapse:collapse;
}
form.uploadForm {
	margin:0 auto;
}
div.uploadContainer {
	background-color:yellow;
	position: relative;
	overflow:hidden;
	width: 120px;
	height: 27px;
}
input.uploadFileReal {
	position: relative;
	text-align: right;
	opacity: 0;
	filter:alpha(opacity: 0);
	z-index: 2;
	width: 120px;
}
div.uploadFileFake {
	position: absolute;
	top: 0px;
	left: 0px;
	z-index: 1;
	vertical-align:text-top;
}
input.fakeFileInput {
	width: 100px;
}
img.uploadButton {
	width: 20px;
}
tr.uploadTRHidden {
	display: none;
}
tr.uploadTRVisible {
}
div.fileContainer {
	position: relative;
}
img.deleteImageImg {
	border-style: none;
	width: 12px;
	height: 12px;
	position: absolute;
	top: 0px;
	left: 108px;
	z-index: 2;
}
img.deleteFileImg {
}
td.captionTD {
	font-size: 12px;
	padding: 0px 0px 10px 0px;
}
input.fileTextInput {
	width: 120px;
}
input.fileActionButton {
	width: 60px;
}
</style>

<script type="text/javascript">

var pathReport = new PathologyReport("pathReport", "pathReportDiv");
pathReport.canDelete = $!canDelete;
pathReport.canEdit = $!canEdit;
pathReport.canCreate = $!canCreate;
pathReport.projectId = "$project";
pathReport.projectLabel = "$!om.getProject()";
pathReport.subjectId = "$!om.getSubjectId()";
pathReport.tissCollID = "$!item.getStringProperty('condr:brainCollData/ID')";

#**********************************************************
******************* Comment Manager ***********************
****************** UNDER DEVELOPMENT **********************
***********************************************************
//var specimenComments = new CommentManager();
//specimenComments.projectId = "$!item.getProperty('project')";
//specimenComments.subjectId = "$!item.getProperty('subject_id')";
//specimenComments.existingCommentContainerDivId = "specimenCommentDiv";
//specimenComments.commentHeaderDivId = "specimenCommentDivHeader"
//specimenComments.commentsDelimiter = "$commentSplit";
//specimenComments.commentSplitter = "$commentSeperator";
***********************************************************
******************** UNDER DEVELOPMENT ********************
**********************************************************#

## NEEDED? var brainCollArray = new Array();
var currentBrainSpecLabel = "";
var currentBrainSpecId = "";

#**********************************************************
****************** Dialog Configuration *******************
**********************************************************#
YAHOO.namespace("specimenComment.container");
YAHOO.namespace("editSpecimen.container");
YAHOO.namespace("editPathReport.container");
YAHOO.namespace("uploadScreenshot.container");

YAHOO.util.Event.onDOMReady(function () {	
	
	function handleSpecimenCommentSubmit() { 
		var totalComment = document.getElementById("originalComment").value;
		var timestamp = new Date();
		var comment = document.getElementById("specimenComment");
		var month = parseInt(timestamp.getMonth()) + 1
		var minutes = parseInt(timestamp.getMinutes()) < 10 ? "0" + timestamp.getMinutes() : timestamp.getMinutes();
		var timestampString = month + "/" + timestamp.getDate() + "/" + timestamp.getFullYear() + " " + timestamp.getHours() + ":" + minutes;
		
		totalComment += timestampString + " - $data.getSession().getAttribute('user').getUsername()"
		totalComment += "$commentSeperator";
		totalComment += comment.value;
		totalComment += "$commentSplit";
		
		saveComment(currentBrainSpecLabel, totalComment);
##TESTCOMMENT		speciminComments.save(currentBrainSpecLabel, totalComment)
		
		//call to submit rest query. Concatinate the prior comment contents with this and resubmit
		this.submit();
	}

	function handleSpecimenCommentCancel() { 
		this.cancel();
	}

	function handleSpecimenCommentSuccess(o) {
//		alert("Submission success!\n" + o.responseText);
	}

	function handleSpecimenCommentFailure(o) {
		alert("Submission failed: " + o.status);
	}

	YAHOO.util.Dom.removeClass("specimenCommentDiv", "yui-pe-content");
	
// Instantiate the Dialog 
	YAHOO.specimenComment.container.specimenCommentDiv = new YAHOO.widget.Dialog("specimenCommentDiv",  
            { width : "515px", 
              fixedcenter : true, 
              visible : false,
              hideaftersubmit : true,
              buttons : [ { text:"Submit", handler:handleSpecimenCommentSubmit, isDefault:true },
              		  { text:"Cancel", handler:handleSpecimenCommentCancel } ] 
            } );

	YAHOO.specimenComment.container.specimenCommentDiv.callback = { success: handleSpecimenCommentSuccess,
						     			failure: handleSpecimenCommentFailure };

	YAHOO.specimenComment.container.specimenCommentDiv.render();

	//YAHOO.util.Event.addListener("show", "click", YAHOO.specimenComment.container.specimenCommentDiv.show, YAHOO.specimenComment.container.specimenCommentDiv, true);
	//YAHOO.util.Event.addListener("hide", "click", YAHOO.specimenComment.container.specimenCommentDiv.hide, YAHOO.specimenComment.container.specimenCommentDiv, true);
	
	getBrainColl("$!item.getStringProperty("condr:brainCollData/ID")")
	pathReport.getPathReport("$!item.getStringProperty("condr:brainCollData/ID")")
	
	
	function handleEditSpecimenSubmit() { 
		try {
			saveBrainSpec();
			this.submit();
		} catch (e) {
			alert(e.message);
		}
	}

	function handleEditSpecimenCancel() { 
		this.cancel();
	}

	function handleEditSpecimenSuccess(o) {
//		alert("Submission success!\n" + o.responseText);
	}

	function handleEditSpecimenFailure(o) {
		alert("Edit specimen submission failed: " + o.status);
	}
	
	YAHOO.util.Dom.removeClass("editSpecimenDiv", "yui-pe-content");
	
// Instantiate the Dialog 
	YAHOO.editSpecimen.container.editSpecimenDiv = new YAHOO.widget.Dialog("editSpecimenDiv",  
            { width : "515px", 
              fixedcenter : true, 
              visible : false,  
              constraintoviewport : true, 
              buttons : [ { text:"Submit", handler:handleEditSpecimenSubmit, isDefault:true },
              		  { text:"Cancel", handler:handleEditSpecimenCancel } ] 
            } );

##	YAHOO.editSpecimen.container.editSpecimenDiv.callback = { success: handleEditSpecimenSuccess,
##						     failure: handleEditSpecimenFailure };

	YAHOO.editSpecimen.container.editSpecimenDiv.render();

	//YAHOO.util.Event.addListener("show", "click", YAHOO.editSpecimen.container.editSpecimenDiv.show, YAHOO.editSpecimen.container.editSpecimenDiv, true);
	//YAHOO.util.Event.addListener("hide", "click", YAHOO.editSpecimen.container.editSpecimenDiv.hide, YAHOO.editSpecimen.container.editSpecimenDiv, true);
	
	//getBrainData("$!item.getStringProperty("condr:brainCollData/ID")")
	//getPathReport("$!item.getStringProperty("condr:brainCollData/ID")")
	
	
	function handleEditPathReportSubmit() { 
		if (document.getElementById("pathReport_otherFeatures").value.length > $maxOtherFeatLength) {
			alert("Other Features field contains " + document.getElementById("pathReport_otherFeatures").value.length + " characters. Maximum characters allowed is $maxOtherFeatLength.");
			return;
		}
		pathReport.addPathReport();
		this.submit();
	}

	function handleEditPathReportCancel() { 
		this.cancel();
	}

	function handleEditPathReportSuccess(o) {
//		alert("Submission success!\n" + o.responseText);
	}

	function handleEditPathReportFailure(o) {
		alert("Submission failed: " + o.status);
	}
	
	YAHOO.util.Dom.removeClass("editPathReportDiv", "yui-pe-content");
	
// Instantiate the Dialog 
	YAHOO.editPathReport.container.editPathReportDiv = new YAHOO.widget.Dialog("editPathReportDiv",  
            { width : "515px", 
              fixedcenter : true, 
              visible : false,  
              constraintoviewport : true, 
              buttons : [ { text:"Submit", handler:handleEditPathReportSubmit, isDefault:true },
              		  { text:"Cancel", handler:handleEditPathReportCancel } ] 
            } );

	YAHOO.editPathReport.container.editPathReportDiv.callback = { success: handleEditPathReportSuccess,
						     failure: handleEditPathReportFailure };

	YAHOO.editPathReport.container.editPathReportDiv.render();

	function handleScreenshotUploadSubmit() { 
		//this.submit();
		uploadScreenshot(document.getElementById("screenshotBrainSpecId").value);
		//alert("edit specimen")
	}

	function handleScreenshotUploadCancel() { 
		this.cancel();
	}

	YAHOO.util.Dom.removeClass("uploadScreenshotDiv", "yui-pe-content");
	
// Instantiate the Dialog 
	YAHOO.uploadScreenshot.container.uploadScreenshotDiv = new YAHOO.widget.Dialog("uploadScreenshotDiv",  
            { width : "515px", 
              fixedcenter : true, 
              visible : false,  
              constraintoviewport : true, 
              buttons : [ { text: "Submit", handler: handleScreenshotUploadSubmit, isDefault:true },
              		  { text: "Cancel", handler: handleScreenshotUploadCancel } ] 
            } );

##	YAHOO.example.container.dialog1.callback = { success: handleSuccess,
##						     failure: handleFailure };

	YAHOO.uploadScreenshot.container.uploadScreenshotDiv.render();
});

</script>

<TABLE width="100%">
	<tr>
		<td>
			#parse($turbineUtils.getTemplateName("_report",$om.getXSIType(),$project))
		</td>
		<td valign="top" align="right">
			#elementActionsBox($element $search_field $search_value $data.getSession().getAttribute("user") $item)
		</td>
	</tr>
	<TR>
		<TD>
			<table width="100%">
				<TR>
					<TD align="left" valign="middle" valign="top">
						<font face="$ui.sansSerifFonts" size="3"><b>Surgical Encounter</b></font>						
					</TD>
					<td valign="top">
					<font face="$ui.sansSerifFonts" size="3"><b>Clinical Pathology Report on Surgical Encounter</b></font>	
					</td>
				</TR>
				<TR>
				<TD valign="top">
						<TABLE>
							#set($complications = $!item.getBooleanProperty("condr:brainCollData/complicsBool"))
							#set($gliadelUser = $!item.getBooleanProperty("condr:brainCollData/gliadelUsed"))
							##<TR><TD>Date</TD><TD>$!item.getProperty("condr:brainCollData/date")</TD></TR>
							##<TR><TD>Time</TD><TD>$!item.getProperty("condr:brainCollData/time")</TD></TR>
							<TR><TD class="label">Location</TD>
								<TD>
									#set($locIndex=0)
									#foreach ($pValue in $item.getChildItems("condr:brainCollData/tumorLocs/tumorLoc"))
										$pValue.getProperty("tumorLoc")
										#if ( $locIndex < $item.getChildItems("condr:brainCollData/tumorLocs/tumorLoc").size() - 1 )
										,
										#end
										#set($locIndex = $locIndex + 1)
									#end
								</TD>
							</TR>
							<TR><TD class="label">Hemisphere</TD><TD>$!item.getStringProperty("condr:brainCollData/tumorHemisphere")</TD></TR>
							<TR><TD class="label">Surgical Extent</TD>
								<TD>$!item.getStringProperty("condr:brainCollData/surgicalExt")
									#set($surgicalExtIndex=0)
									#foreach ($pValue in $item.getChildItems("condr:brainCollData/surgicalExts/surgicalExt"))
										$pValue.getProperty("surgicalExt")
										#if ( $surgicalExtIndex < $item.getChildItems("condr:brainCollData/surgicalExts/surgicalExt").size() - 1 )
										,
										#end
										#set($surgicalExtIndex = $surgicalExtIndex + 1)
									#end								
								</TD>
							</TR>
							<TR><TD class="label">Gliadel Used</TD><TD>
							#if ($!item.getBooleanProperty("condr:brainCollData/gliadelUsed"))
								Yes
							#else 
								No
							#end
							</TD></TR>
							<TR><TD class="label">Surgeon</TD>
								<TD>
									#set($surgeonIndex=0)
									#foreach ($pValue in $item.getChildItems("condr:brainCollData/surgeons/surgeon"))
										$pValue.getProperty("surgeon")
										#if ( $surgeonIndex < $item.getChildItems("condr:brainCollData/surgeons/surgeon").size() - 1 )
										,
										#end
										#set($surgeonIndex = $surgeonIndex + 1)
									#end
								</TD>
							</TR>
							<TR><TD class="label">Blood Collected</TD><TD>
							#if ($!item.getBooleanProperty("condr:brainCollData/bloodColl"))
								Yes
							#else 
								No
							#end
							</TD></TR>
							<TR><TD class="label">Red Top</TD><TD>
							#if ($!item.getBooleanProperty("condr:brainCollData/redTop"))
								Yes
							#else 
								No
							#end
							</TD></TR>
							<TR><TD class="label">Purple Top</TD><TD>
							#if ($!item.getBooleanProperty("condr:brainCollData/purpleTop"))
								Yes
							#else 
								No
							#end
							</TD></TR>
	
							<TR><TD class="label">Sent to Lab</TD><TD>
							#if ($!item.getBooleanProperty("condr:brainCollData/sentToLab"))
								Yes
							#else 
								No
							#end
							</TD></TR>
							<TR><TD class="label" colspan=2>Sent To Lab Details</TD>
							</TR>
							<TR>
							<TD colspan=2>$!item.getStringProperty("condr:brainCollData/sentToLabDetails")</TD></TR>
	
							<TR><TD class="label">Complications</TD><TD>
							#if ($!item.getBooleanProperty("condr:brainCollData/complications"))
								Yes
							#else 
								No
							#end
							</TD></TR>
							#if ($!item.getStringProperty("condr:brainCollData/complicationsDetails") != "")
							<TR><TD class="label" colspan=2>Complication Details</TD>
							</TR>
							<TR>
							<TD colspan=2>$!item.getStringProperty("condr:brainCollData/complicationsDetails")</TD></TR>
							#end
							##<TR><TD>Comments</TD><TD>$!item.getStringProperty("condr:brainCollData/note")</TD></TR>
						</TABLE>
					</TD>
					<TD valign="top">
					<table>
						<thead id="pathReportHead">
						</thead>
						<tbody id="pathReportBody">
						</tbody>
					</table>	
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
	<TR>
		<TD valign="top">
			<font face="$ui.sansSerifFonts" size="3"><b>Brain Specimen</b></font>
			<table cellPadding="0" cellSpacing="0" width="90%">
				<thead>
					<tr style="border-style:none;">
						<th rowspan=2 colspan=1 style="border-bottom-color:rgb(255,153,51); border-top-style:none; border-right-style:none; border-bottom-style:solid; border-left-style:none;" align=left valign=bottom>Label</th>
						<th rowspan=2 colspan=3 style="border-bottom-color:rgb(255,153,51); border-top-style:none; border-right-style:none; border-bottom-style:solid; border-left-style:none;" align=center valign=bottom>Media</th>
						<th rowspan=2 style="width:40px; border-bottom-color:rgb(255,153,51); border-top-style:none; border-right-style:none; border-bottom-style:solid; border-left-style:none;" align=left valign=bottom>Time</th>		
						<th colspan=3 style="border-bottom-color:rgb(255,153,51); border-top-style:none; border-right-style:none; border-bottom-style:solid; border-left-style:none;" align=center>Coordinates</th>
						<th rowspan=2 colspan=3 style="border-bottom-color:rgb(255,153,51); border-top-style:none; border-right-style:none; border-bottom-style:solid; border-left-style:none;" align=center valign=bottom>Actions</th>
					</tr>
					<tr style="border-style:none;">
						<th style="width:40px; border-bottom-color:rgb(255,153,51); border-top-style:none; border-right-style:none; border-bottom-style:solid; border-left-style:none;" align=left>X</th>
						<th style="width:40px; border-bottom-color:rgb(255,153,51); border-top-style:none; border-right-style:none; border-bottom-style:solid; border-left-style:none;" align=left>Y</th>
						<th style="width:40px; border-bottom-color:rgb(255,153,51); border-top-style:none; border-right-style:none; border-bottom-style:solid; border-left-style:none;" align=left>Z</th>
					</tr>
				</thead>
				<tbody id=brainDataBody>
				</tbody>
				<tfoot><tr>
				<td colspan=$brainSpecColSpan style="border-top-color:rgb(255,153,51); border-bottom-style:none; border-right-style:none; border-top-style:solid; border-left-style:none;">
					#if ( $canCreate )
					<a href="#" onClick="newBrainSpecimen(); return false;">
					<b>Add Brain Specimen</b></a>
					#end
					&nbsp;
				</td></tr></tfoot>				
			</table>
			<br><br>	
		</td>
	</tr>
</TABLE>
<input type="hidden" id="originalComment" value=""> 

#**********************************************************
**************** Specimen Comment Dialog ******************
**********************************************************#
<div id="specimenCommentDiv" class="yui-pe-content">
	<div class="hd" id="specimenCommentDivHeader">Add Comment</div>
	<div class="bd">
		Existing comment data.<br>
		<div id="specimenCommentExisting" class="notes"></div>
		<br>
		Enter comment to be added. It will be timestamped and added to the comments field.<br>
		#xdatTextArea("specimenComment" "" "" $vr 4 60)
	</div>
</div>

#**********************************************************
****************** Edit Specimen Dialog *******************
**********************************************************#
<div id="editSpecimenDiv" class="yui-pe-content">
	<div class="hd" id="editSpecimenDivHeader">Edit Specimen Data</div>
	<div class="bd">
		<table>
			<TR>
				<TD>Specimen Label</TD>
				<TD colspan=2>
					<div id="brainDataLabelEditDiv"></div>
					<div id="brainDataLabelNewDiv">$!om.SubjectData.getLabel()_CONDR<input id="specLabel_inputA" size=1>_<input id="specLabel_inputB" size=1>_$turbineUtils.formatDate($!om.getDateProperty("condr:brainCollData/date"),"yyyyMMdd")</div>
					<input type="hidden" id="brainData_label" value="">
				</TD>
			</TR>
			<TR>
				<TD>Screen Captures Collected</TD>
				<TD colspan=2>
					#cndaRadioYesNoSimple("brainData_screenCapColl" $item "" $vr)
				</TD>
			</TR>
			<TR>
				<TD>Coordinates Collected</TD>
				<TD colspan=2>
					#cndaRadioYesNoSimple("brainData_coordColl" $item "" $vr)
				</TD>
			</TR>
			<TR>
				<TD align=left width=5%>Screen Coordinates </TD>
				<TD colspan=2 width=95%>
					x: #xdatStringBoxWSize("brainData_coordX" "" "" $vr 3)
					&nbsp;&nbsp;y: #xdatStringBoxWSize("brainData_coordY" "" "" $vr 3)
					&nbsp;&nbsp;z: #xdatStringBoxWSize("brainData_coordZ" "" "" $vr 3)
				</TD>
			</TR>
##			<TR>
##				<TD align=left>Coordinates Source</TD>
##				<TD colspan=2>#cndaSelectBoxSimple("condr:brainData_coordSource" $item $coordSource $vr)</TD>
##			</TR>
##			<TR>
##				<TD align=left>Scan Number</TD>
##				<TD colspan=2>#xdatStringBox("brainData_scanNumber" "" "" $vr)</TD>
##			</TR>			
			<TR>
				<TD align=left>Surgery Time</TD>
				<TD colspan=2>#cndaTimeBoxNoSeconds("brainData_time" "" $vr)</TD>
			</TR>
			<TR>
				<TD></TD>
				<TD colspan=2>#cndaCheckbox("brainData_timeNotReported" "1" "0" $vr false "toggleBrainData_time")Not Reported</TD></TR>				
#*
			<script type="text/javascript">
				brainData_timeHours = document.getElementById("brainData_time.hours")
										
				if (brainData_timeHours.addEventListener)
				{
					eval("brainData_timeHours.addEventListener('change', function() {changeBrainData_time()}, false)");
				}
				else
				{
					eval("brainData_timeHours.attachEvent('onchange', function() {changeBrainData_time()})");
				}
				
				brainData_timeMinutes = document.getElementById("brainData_time.minutes")
										
				if (brainData_timeMinutes.addEventListener)
				{
					eval("brainData_timeMinutes.addEventListener('change', function() {changeBrainData_time()}, false)");
				}
				else
				{
					eval("brainData_timeMinutes.attachEvent('onchange', function() {changeBrainData_time()})");
				}			
			</script>
*#
			<TR>
				<TD align=left nowrap>Sample Split</TD>
				##<TD colspan=2>#xdatBooleanRadio("brainData_sampSplit" "" false $vr)</TD>
				<TD colspan=2>#cndaRadioYesNoSimple("brainData_sampSplit" $item "" $vr)</TD>
			</TR>
			<TR>
				<TD align=left nowrap>Surgical Path</TD>
				##<TD colspan=2>#xdatBooleanRadio("brainData_surgPath" "" false $vr)</TD>
				<TD colspan=2>#cndaRadioYesNoSimple("brainData_surgPath" $item "" $vr)</TD>
			</TR>
			<TR>
				<TD align=left>Tumor Bank</TD>
				##<TD colspan=2>#xdatBooleanRadio("brainData_tumorBank" "" false $vr)</TD>
				<TD colspan=2>#cndaRadioYesNoSimple("brainData_tumorBank" $item "" $vr)</TD>
			</TR>
			<TR>
				<TD align=left>Location</TD>
				<TD colspan=2>#xdatStringBox("brainData_descLoc" "" "" $vr)</TD>
			</TR>
			<TR>
				<TD align=left valign=top nowrap colspan=2>Radiographic Appearence</TD>
				<!--<TD width=95%>#cndaOptionSelectBoxSimple("brainData_radiographicAppearence" $item $radiographicAppearenceOptions [] 0 1 1 0 0 $vr)</TD>-->
				<TD width=95%>#xdatTextBox("brainData_radiographicAppearence" $item "" $vr)</TD>
			</TR>
			<TR>
				<TD colspan=3 align=left>Radiographic Appearence Comments</TD>
			</TR>
			<TR>
				<TD colspan=3>#xdatTextArea("brainData_radiographicAppearenceComments" "" "" $vr 6 58)</TD>
			</TR>
##			<TR>
##				<TD colspan=3 align=left>Reason for Making Modifications.</TD>
##			</TR>
##			<TR>
##				<TD colspan=3>#xdatTextArea("editReason" "" "" $vr 6 60)</TD>
##			</TR>
		</table>
	</div>
</div>


#**********************************************************
**************** Pathology Report Dialog ******************
**********************************************************#
<div id="editPathReportDiv" class="yui-pe-content">
	<div class="hd">Edit Pathology Report</div>
	<div class="bd">
		<table>
			<TR><TD class="label">Report Label</TD>
			<TD>
				<div id="reportLabelDiv"></div>
				
				$!om.SubjectData.getLabel()_CLINPATH_$turbineUtils.formatDate($!om.getDateProperty("condr:brainCollData/date"),"yyyyMMdd")
				<input type="hidden" id="pathReport_label" value="$!om.SubjectData.getLabel()_CLINPATH_$turbineUtils.formatDate($!om.getDateProperty('condr:brainCollData/date'),'yyyyMMdd')">
				<input type="hidden" id="pathReport_id" value="">
			</TD></TR>
##			<TR><TD class="label">Date</TD><TD>#cndaDateBox("pathReport_date" "" $vr $years)</TD></TR>
			<TR><TD class="label">Tumor Type</TD><TD>#cndaSelectBoxSimple("pathReport_tumorType" $item $tumorTypeOptions $vr)</TD></TR>
			<script type="text/javascript">
				tumorType = document.getElementById("pathReport_tumorType")
										
				if (tumorType.addEventListener)
				{
					eval("tumorType.addEventListener('change', function() {pathReport.selectTumorType()}, false)");
				}
				else
				{
					eval("tumorType.attachEvent('onchange', function() {pathReport.selectTumorType()})");
				}			
			</script>
			<TR><TD class="label">Primary Tumor Type</TD><TD>#xdatTextBox("pathReport_primaryTumorType" $item "" $vr)</TD></TR>
			<TR><TD class="label">WHO Grade</TD><TD>#cndaSelectBoxSimple("pathReport_whoGrade" $item $whoGradeOptions $vr)</TD></TR>
			<TR><TD class="label">Metastatic Origin</TD><TD>#xdatTextBox("pathReport_metastatOrig" $item $vr)</TD></TR>			
			<TR><TD class="label">1p19q Deletions</TD><TD>#cndaSelectBoxSimple("pathReport_1p19q" $item $onep19q $vr)</TD></TR>
			<TR><TD class="label">MGMT Promoter Status</TD><TD>#cndaSelectBoxSimple("pathReport_mgmtPromoterStatus" $item $mgmtPromoterStatus $vr)</TD></TR>
			<TR><TD class="label">MIB 1 Index</TD><TD>#xdatStringBox("pathReport_mib1Index" $item "" $vr)%</TD></TR>
			<TR><TD class="label"></TD><TD>#cndaCheckbox("pathReport_mib1IndexNotReported" "1" "0" $vr false "toggleMIB1Index")Not Reported</TD></TR>
#*			
			<script type="text/javascript">
				mib1Index = document.getElementById("pathReport_mib1Index")
				checkBox = document.getElementById("mib1IndexCheckBox")
										
				if (mib1Index.addEventListener)
				{
					eval("mib1Index.addEventListener('blur', function() {blurMIB1Index()}, false)");
				}
				else
				{
					eval("mib1Index.attachEvent('onblure', function() {blurMIB1Index()})");
				}			
			</script>
*#
			<TR><TD class="label">PTEN</TD><TD>#cndaSelectBoxSimple("pathReport_pten" $item $pten $vr)</TD></TR>
			<TR><TD class="label">P53</TD><TD>#cndaSelectBoxSimple("pathReport_p53" $item $p53 $vr)</TD></TR>
			<TR><TD class="label" colspan=2>Other Features</TD></TR>
			<TR><TD colspan=2>#xdatTextArea("pathReport_otherFeatures" "" "" $vr 5 50)</TD></TR>
		</table>
	</div>
</div>

#**********************************************************
******************* Upload File Dialog ********************
**********************************************************#
#**************** \/ Work in Progress \/ *****************#
#*
<div id="uploadScreenshotDiv">  
##	<div class="hd">Upload Screenshot</div>
##	<div class="bd">
		<TABLE>
			<TR>
				<TD>
					Select Screen Shot
				</TD>
				<TD>	
					<input type="hidden" id="screenshotBrainSpecId" value="">
					<form name='uploadScreenshotForm' action='nothing.html' enctype='multipart/form-data' method='PUT' id='uploadScreenshotForm'>
					<label for="screenshotFile">THE FILE</label><input type="file" id="screenshotFile">
					</form>
				</TD>
			</TR>
			<TR>
				<TD>
					Set Caption
				</TD>
				<TD>
					<input type="text" id="screenshotCaption">
				</TD>
			</TR>
			<TR>
				<TD colspan=2>
					<input type=button value="Upload">
					<input type=button value="Cancel">
				</TD>
			</TR>
		</TABLE>

##	</div>
</div>
*#
#**************** /\ Work in Progress /\ *****************#

<BR>
#parse("/scripts/CONDR/SurgicalEncounter.vm")

#parse("/screens/ReportProjectSpecificFields.vm")
