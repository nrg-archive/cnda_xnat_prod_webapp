$page.setTitle("ELIG")
$page.setLinkColor($ui.alink)
$page.setVlinkColor($ui.vlink)
#if ($data.getParameters().getString("popup"))
#set ($popup = $data.getParameters().getString("popup") )
#set ($popup = "false")
#end
#if($project)

#else
#set($project=$om.getProject())
#end

<table width="100%">
	<tr>
		<td>
			#parse($turbineUtils.getTemplateName("_report",$om.getXSIType(),$project))
		</td>
		<td valign="top" align="right">
			#elementActionsBox($element $search_field $search_value $data.getSession().getAttribute("user") $item)
		</td>
	</tr>
	<tr>
		<td colspan="2">

<style>
#uds_form {
	border-collapse: collapse;
	border-spacing:0px;
	border: 1px solid #000;
}
#uds_form td, #uds_form th {
	vertical-align:top;
	text-align:left;
	border:1px solid #000;
	min-width: 70px;
}
</style>

#set($column_count = $item.getChildItems("dian:eligData/eligList/elig").size() + 1)
#set($elig_num = $item.getChildItems("dian:eligData/eligList/elig").size() - 1)
    
<table id="uds_form">
	<tr>
		<th>Date of eligibility screening</th>
		#foreach($column_counter in [0..$elig_num])
            <td>$!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/DATE")</td>
        #end
	</tr>
	<tr>
		<th colspan="$column_count">Inclusion Criteria<p>All inclusion criteria should be "Yes." Contact Angie Berry at 314-286-2442 or berrya@abraxas.wustl.edu to request any includsion criteria deviations.</th>
	</tr>	
	<tr>
		<th style="padding-left:20px;">Participant is aged >=18 inclusive and the child of an individual with a known mutation (clinically or by testing) in a pedigree with autosomal dominant Alzheimer's disease.</th>
		#foreach($column_counter in [0..$elig_num])
            <td style="min-width:80px">
                $!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/INCAGE")
			    #if ($!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/INCAGE") == "1")
				    - Yes
			    #elseif ($!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/INCAGE") == "0")
				    - No
			    #end
			</td>
		#end
	</tr>
	<tr>
		<th style="padding-left:20px;">Autosomal Dominant Mutation</th>
		#foreach($column_counter in [0..$elig_num])
            <td>$!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/MUTATION")</td>
        #end
	</tr>
	<tr>
		<th style="padding-left:20px;">Mutation type</th>
		#foreach($column_counter in [0..$elig_num])
            <td>$!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/MUTTYPE")</td>
        #end
	</tr>
	<tr>
		<th style="padding-left:20px;">Mutated gene</th>
		#foreach($column_counter in [0..$elig_num])
            <td>$!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/MUTGENE")</td>
        #end
	</tr>	
	<tr>
		<th style="padding-left:20px;">Expected amino acid (wild type)</th>
		#foreach($column_counter in [0..$elig_num])
            <td>$!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/MUTAA")</td>
        #end
	</tr>	
	<tr>
		<th style="padding-left:20px;">Position</th>
		#foreach($column_counter in [0..$elig_num])
            <td>$!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/MUTPOS")</td>
        #end
	</tr>	
	<tr>
		<th style="padding-left:20px;">Existing amino acid (mutation or change)</th>
		#foreach($column_counter in [0..$elig_num])
            <td>$!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/MUTAACHG")</td>
        #end
	</tr>	
	<tr>
		<th style="padding-left:20px;">If insertion or deletion:</th>
		#foreach($column_counter in [0..$elig_num])
            <td>$!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/MUTDES")</td>
        #end
	</tr>		
	#set ($INCCOG = $item.getStringProperty("INCCOG"))
	<tr>
		<th style="padding-left:20px;">Participant is cognitively normal or if demented, does not require nursing home level care and approval for enrollment has been granted by the DIAN Coordinating Center</th>
	    #foreach($column_counter in [0..$elig_num])
            <td>
                $!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/INCCOG")
		        #if ($!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/INCCOG") == "1")
			        - Yes
		        #elseif ($!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/INCCOG") == "0")
			        - No
		        #end
		    </td>
	    #end
	</tr>
	<tr>
		<th style="padding-left:20px;">Participant is fluent in English at the 6th grade level or above</th>
		
		#foreach($column_counter in [0..$elig_num])
            <td>
                $!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/INCLANG")
		        #if ($!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/INCLANG") == "1")
			        - Yes
		        #elseif ($!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/INCLANG") == "0")
			        - No
		        #end
		    </td>
	    #end
	</tr>
	<tr>
		<th style="padding-left:20px;">Participant has identified two individuals (minimum of one) with whom they do not share the affected parent who can serve as a collateral source for the study.</th>
		#foreach($column_counter in [0..$elig_num])
            <td>
                $!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/INCCS")
		        #if ($!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/INCCS") == "1")
			        - Yes
		        #elseif ($!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/INCCS") == "0")
			        - No
		        #end
		    </td>
	    #end
	</tr>
	<tr>
		<th colspan="$column_count">General Exclusion Criteria<p>All exclusion criteria should be "No." Contact Angie Berry at 314-286-2442 or berrya@abraxas.wustl.edu to request any exclusion criteria deviations.</th>
	</tr>	
	<tr>
		<th style="padding-left:20px;">Participant has a medical or psychiatric illness that would interfere with completing initial and follow-up assessments</th>
		#foreach($column_counter in [0..$elig_num])
            <td>
                $!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/EXCMED")
		        #if ($!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/EXCMED") == "1")
			        - Yes
		        #elseif ($!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/EXCMED") == "0")
			        - No
		        #end
		    </td>
	    #end
	</tr>


	<tr>
		<th style="padding-left:20px;">Participant resides in a skilled nursing facility or requires nursing home level care</th>
		#foreach($column_counter in [0..$elig_num])
            <td>
                $!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/EXCSNF")
		        #if ($!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/EXCSNF") == "1")
			        - Yes
		        #elseif ($!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/EXCSNF") == "0")
			        - No
		        #end
		    </td>
	    #end
	</tr>
	<tr>
		<th colspan="$column_count">Procedure Specific Screening Questions<p>If "Yes," conduct in-depth screening to ensure that the person can participate in the study procedure.</th>
	</tr>	
	<tr>
		<th style="padding-left:20px;">MRI--Participant has metal implants or fragments, claustrophobia, or physical size that prohibits MRI</th>
		#foreach($column_counter in [0..$elig_num])
            <td>
                $!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/EXCMRI")
		        #if ($!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/EXCMRI") == "1")
			        - Yes
		        #elseif ($!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/EXCMRI") == "0")
			        - No
		        #end
		    </td>
	    #end
	</tr>
	<tr>
		<th style="padding-left:20px;">PET--Participant has received within the last 12 months radiation above limits set by local institutional authorities</th>
		#foreach($column_counter in [0..$elig_num])
            <td>
                $!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/EXCPET")
		        #if ($!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/EXCPET") == "1")
			        - Yes
		        #elseif ($!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/EXCPET") == "0")
			        - No
		        #end
		    </td>
	    #end
	</tr>
	<tr>
		<th style="padding-left:20px;">LP--Participant is on blood thinners, has a coagulation disorder, or an active infectious process</th>
		#foreach($column_counter in [0..$elig_num])
            <td>
                $!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/EXCLP")
		        #if ($!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/EXCLP") == "1")
			        - Yes
		        #elseif ($!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/EXCLP") == "0")
			        - No
		        #end
		    </td>
	    #end
	</tr>
	<tr>
		<th colspan="$column_count">Confounding Variables</th>
	</tr>	
	<tr>
		<th style="padding-left:20px;">Participant is aware of mutation status (status should not be disclosed to site)</th>
		#foreach($column_counter in [0..$elig_num])
            <td>
                $!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/MUTSTAT")
		        #if ($!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/MUTSTAT") == "1")
			        - Yes
		        #elseif ($!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/MUTSTAT") == "0")
			        - No
		        #end
		    </td>
	    #end
	</tr>
	<tr>
		<th style="padding-left:20px;">Participant has used an investigational drug for AD within 30 days of the screening visit or currently receives neuropsychiatric measures more than once a year (these may interfere with DIAN measures)</th>
		#foreach($column_counter in [0..$elig_num])
            <td>
                $!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/NEURO")
		        #if ($!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/NEURO") == "1")
			        - Yes
		        #elseif ($!item.getStringProperty("dian:eligData/eligList/elig[$column_counter]/NEURO") == "0")
			        - No
		        #end
		    </td>
	    #end
	</tr>
</table>
		</td>
	</tr>
</table>

