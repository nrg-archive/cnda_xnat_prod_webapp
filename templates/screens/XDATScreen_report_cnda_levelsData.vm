<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2//EN">
#macro( propValueRow $name $prop )
  #if ( $!item.getProperty("$prop") )
		<tr><td class="attr_name">$name:</td><td class="attr_value">$item.getProperty("$prop")</td></tr>
	#end
#end

#macro( propValueRow $name $prop $addText)
  #if ( $!item.getProperty("$prop") )
		<tr><td class="attr_name">$name:</td><td class="attr_value">$item.getProperty("$prop") $addText</td></tr>
	#end
#end

<!-- Generates display row, plus "(questionable)" if qProp is true -->
#macro(propValueQRow $name $prop $units $qProp)
	#if (true == $item.getBooleanProperty("$qProp"))
    #set($qFlag = " (questionable)")
  #else
		#set($qFlag = "")
  #end
  #if ($!item.getProperty("$prop"))
		<tr>
			<td class="attr_name">$name:</td>
			<td class="attr_value">$item.getProperty("$prop") $units$qFlag</td>
		</tr>
	#end
#end

<!-- TODO: better presentation -->
#macro( propBoolValueRow $name $prop )
	#if ($!item.getBooleanProperty("$prop"))
		<tr><td class="attr_name">$name:</td><td class="attr_value">$item.getBooleanProperty("$prop")</td></tr>
	#end
#end

<style type="text/css">
td.attr_name { <!-- in imitation of xdat.css (th) -->
	border-collapse: collapse;
	border-left-style: none;
	border-right-style: none;
	border-top-style: none;
	font-size: 11px;
	font-weight: 700;
	line-height: 13px;
	margin: 0px;
	padding-left: 4px;
	padding-right: 4px;
}

td.attr_value { <!-- in imitation of xdat.css (td) -->
	font-family: verdana,geneva,helvetica;
	font-size: 10px;
	line-height: 14px;
	padding-left: 4px;
	padding-right: 4px;
}

td.subtable {
	valign: top;
}
</style>

$page.setTitle("Levels : $!item.getProperty('cnda:levelsData.ID')")
$page.setLinkColor($ui.alink)
$page.setVlinkColor($ui.vlink)

#if ($data.message)
<div class="error">$data.message</div><br>
#end

#if ($data.getParameters().getString("popup"))
	#set ($popup = $data.getParameters().getString("popup") )
	#set ($popup = "false")
#end


#if (!$project)
#set($project=$item.getProperty("project"))
#end

#set($subject = $om.getSubjectData())
#set($part_id = $subject.getStringProperty("ID"))

<div class="edit_title">
Levels:
#if($project)
	$om.getIdentifier($project)
#else
	$!item.getProperty("levelData.ID")
#end
</div>

#parse("/screens/workflow_alert.vm")

<table width="100%">
	<tr>
		<td valign="top"><table>
		<!-- left table -->
		#propValueRow("Date" "cnda:levelsData/date")
		#propValueRow("Time" "cnda:levelsData/time")
		#propValueRow("Visit ID" "cnda:levelsData/visit_id")
		</table></td>
		<td valign="top"><table>
		<!-- middle table -->
				<td class="attr_name">Subject:</td>
				<td>
					<A CLASS=b HREF="$link.setAction('DisplayItemAction').addPathInfo('search_element','xnat:subjectData').addPathInfo('search_field','xnat:subjectData.ID').addPathInfo('search_value',$part_id).addPathInfo('popup','$!popup').addPathInfo('project','$!project')">
					#if($project)
						$!subject.getIdentifier($project)
					#else
						$!subject.getId()
					#end
					</A>
				</td>
			</tr>
			#if($!subject.getGenderText())
			<tr>
				<td class="attr_name">Gender:</td>
				<td class="attr_value">$subject.getGenderText()</td>
			</tr>
			#end
			#propValueRow("Age" "cnda:levelsData/age")
		</table></td>
		<td valign="top">
		#parse("/screens/xnat_experimentData_shareDisplay.vm")
		</td>
		<td valign="top">
			#elementActionsBox($element $search_field $search_value $data.getSession().getAttribute("user") $item)
		</td>
	</tr>
</table>

#if ($!item.getStringProperty("note"))
<table>
  <tr>
		<td valign="top" class="attr_name">Notes:</td>
		<td valign="top" class="attr_value">$!item.getStringProperty("note")</td>
	</tr>
</table>
#end

<br>

<table>
#propValueQRow("Plasma Levodopa" "cnda:levelsData/plasmaLD" "ng/ml" "cnda:levelsData/questionableLD")
#propValueQRow("Plasma Dopamine" "cnda:levelsData/plasmaDA" "ng/ml" "cnda:levelsData/questionableDA")
#propValueQRow("Plasma 3OMe" "cnda:levelsData/plasma3OMe" "ng/ml" "cnda:levelsData/questionable3OMe")
#propValueQRow("Plasma DOPAC" "cnda:levelsData/plasmaDC" "ng/ml" "cnda:levelsData/questionableDC")
#propValueQRow("Plasma Carbidopa" "cnda:levelsData/plasmaCD" "ng/ml" "cnda:levelsData/questionableCD")
#propBoolValueRow("Problem?" "cnda:levelsData/problem")
#propBoolValueRow("Known for IVLD1?" "cnda:levelsData/knownForIVLD1")
#propBoolValueRow("Rejected for IVLD1?" "cnda:levelsData/rejectedForIVLD1")
</table>

<br>

#set($xnat_experimentData_field_4_NUM_ROWS=$item.getChildItems("cnda:levelsData/fields/field").size() - 1)
#if($xnat_experimentData_field_4_NUM_ROWS>=0)
	#foreach($xnat_experimentData_field_4_COUNTER in [0..$xnat_experimentData_field_4_NUM_ROWS])
<!-- BEGIN cnda:levelsData/fields/field[$xnat_experimentData_field_4_COUNTER] -->
		<TABLE>
			<TR><TH align="left"><BR><font face="$ui.sansSerifFonts" size="2">cnda:levelsData/fields/field[$xnat_experimentData_field_4_COUNTER]</font></TH></TR>
			<TR>
				<TD align="left" valign="top">
					<TABLE>
						<TR><TD>field</TD><TD>$!item.getStringProperty("cnda:levelsData/fields/field[$xnat_experimentData_field_4_COUNTER]/field")</TD></TR>
						<TR><TD>name</TD><TD>$!item.getStringProperty("cnda:levelsData/fields/field[$xnat_experimentData_field_4_COUNTER]/name")</TD></TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
<!-- END cnda:levelsData/fields/field[$xnat_experimentData_field_4_COUNTER] -->
	#end
#end

<BR>#parse("/screens/ReportProjectSpecificFields.vm")
