##Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2//EN">

#set ($template = $data.getTemplateInfo())
$!template.setLayoutTemplate("/Popup.vm")
$page.setLinkColor($ui.alink)
$page.setVlinkColor($ui.vlink)

<script src="/scripts/xModal/xModal.js"></script>

<link href="/scripts/xModal/xModal.css" rel="stylesheet" />
<style type="text/css">
    body { margin: 0; padding: 0; font-family: Arial, Helvetica, sans-serif; font-size: 100%; background-color: #f3f3f3; }
    .hidden { display: none; }
    .col-2 { width: 45%; float:left; padding-right: 5%; }

    .xmodal div.error { margin-top: 0; }

    .xmodal fieldset { border-color: #ccc; border-style: solid; border-width: 1px 0 0; margin: 2em 0; padding-top: 10px; }
    .xmodal fieldset legend { border-radius: 3px; padding: 3px; background-color: inherit; }
    .xmodal fieldset label { display: inline-block; font-weight: bold; min-width: 120px; padding-right: 10px;  }
    .xmodal fieldset .col-2 label { min-width: 75px; }

    .xmodal fieldset.error { background-color: #fffcc7; }
    .xmodal fieldset.error legend { color: #c33; }
    .xmodal fieldset.error .required { border-color: #c33; }

    .xmodal input, .xmodal select, .xmodal textarea { border: 1px solid #999; padding: 2px 1px; }

    .xmodal table { border: 1px solid #ccc; }
    .xmodal td, .inputWrapper th { padding: 3px; }
    .xmodal td { border-top: 1px solid #ccc; }
    .xmodal th { text-align: left; background-color: #f3f3f3; }
    tr.empty { background-color: #fffcc7; color: #933; }

    div.xmodal .footer { position: fixed; bottom: 0; right: 0; width: inherit; }
    div.xmodal .footer .buttons .textlink { font-size: 12px; padding: 0 8px; position: relative; top: 5px; }
    div.xmodal.embedded { top: 0; left: 0; border: none; box-shadow: none; display: block; z-index: auto;  }
</style>

<script language="Javascript">

    function disableNext() {
        $("#button-advance").addClass("disabled");
    }

    function enableNext() {
        $("#button-advance").removeClass("disabled");
    }


    function validate() {
        var checkedScans = $("input[name^=scan]:checked");
        var ret = checkedScans && checkedScans.length > 0
        if (ret) {
            enableNext();
        } else {
            alert("At least one scan must be selected.");
            disableSubmit();
            disableNext();
        }

        return ret
    }

    function noneClicked(noneObj) {
        var type = noneObj.getAttribute("class")
        if (noneObj.checked) {
            var checkedScans = $("input[name^="+type+"]:checked");
            var l = checkedScans.length;
            for (var i = 0; i < l; i++) {
                $(checkedScans[i]).prop("checked", false)
            }
        }
    }

    function scanClicked(scanObj) {
        var type = scanObj.getAttribute("class")
        if (scanObj.checked) {
            $("#x"+type).prop("checked", false)
        } else {
            var checkedScans = $("input[name^="+type+"]:checked");
            if (checkedScans && checkedScans.length > 0) {
                $("#x"+type).prop("checked", "checked")
            }
        }
    }

    function validateResource() {
        var format = $("#format:checked").val();
        var checkedScansThatDoNotHaveFormat = $("input.scan:checked").filter(function(){
            if ($(this).data("format").indexOf(format)<0) {
                return true;
            }
            return false;
        });

        if (checkedScansThatDoNotHaveFormat.length > 0) {
            return false;
        }
        return true;
    }


    // set popup window title properly
    $(document).prop("title","Register $om.getLabel() to atlas");

    $(document).ready(function(){
        // set display height and width according to popup size
        $("#pipeline_modal").width($(window).width());
        $("#pipeline_modal").height($(window).height());
        $("#pipeline_modal .body").height($(window).height()-$("#pipeline_modal .footer").height());

        // Show the DICOM / NIFTI selection if any NIFTI exist on the session
        var resources = [];
        #foreach ($label in $scanResourceLabelSet)
        resources.push("$label");
        #end
        if (resources.indexOf("NIFTI") > -1) {
            $("#format-param").removeClass("hidden");
        }

        $("#button-advance").on("click",function(){
            // check that all selected scans have selected resource. If not, error.
            if (!validateResource()) {
                alert("ERROR: All selected scans do not have a "+$("#format:checked").val()+" resource.");
                return;
            }

            if (confirm ("Build pipeline with these parameters?")) {
                // Submit the form
                $("#submitForm").click();
            }
        });
    });

    function advanced_params_toggle(){
        var adv = $("#advanced-params");
        if ($(adv).hasClass("hidden")) {
            $(this).html("Hide");
            $(adv).removeClass("hidden");
        } else {
            $(this).html("Show");
            $(adv).addClass("hidden");
        }
    }


</script>


<div id="pipeline_modal" class="xmodal fixed embedded">
    <div class="body content scroll">
        #if ($data.message)
            <div class="error">$data.message</div><br>
        #end
        <div class="inner">
            <form name="RegisterMR" method="post" action="$link.setAction("BuildAction")" onsubmit="return validate();">
                <h2>Register $om.getLabel() to atlas</h2>

                <fieldset>
                    <legend>Input parameters</legend>

                    #set ($j = 0)
                    #foreach ($scan in $mrScans)
                        #if ($scan.getQuality().equalsIgnoreCase("usable"))
                            #set ($attrib="checked")
                        #else
                            #set ($attrib="")
                        #end
                        <p><label for="scan_$j"><input type="checkbox" name="scan_$j" id="scan_$j" value="$scan.Id" $attrib onclick="validate()" class="scan" data-format="$allScanResourceLabels.get($scan.Id)">$scan.Id (<b>$!scan.getType()</b> Quality: $scan.Quality)</label></p>

                        #set ($j = $j + 1)
                    #end
                    <input type="hidden" name="scan_rowcount" id="scan_rowcount" value="$j">

                    #set ($at = "atlas")
                    <p><label for="$at" title="Registration atlas">Registration atlas</label>
                    <input type="text" name="$at" id="$at" value="$!projectSettings.get($at).getCsvvalues()" /></p>
                </fieldset>

                <div class="hidden" id="format-param">
                    <fieldset style="background-color: #def">
                        <legend>Advanced Parameters (<a href="javascript:" id="advanced-params-toggle" onclick="advanced_params_toggle()">Show</a>)</legend>
                        <div class="hidden" id="advanced-params">
                            <p><label for="format" title="File type">Input data file type</label>
                                <input type="radio" name="format" id="format" value="DICOM" checked>DICOM</input>
                                <input type="radio" name="format" id="format" value="NIFTI">NIFTI</input>
                            </p>
                        </div>
                    </fieldset>
                </div>
                <br/>

                #xdatPassItemFormFields($search_element $search_field $search_value $project)

                <input type="hidden" name="registermr_pipelinename" value="$pipeline.getLocation()">

                <div class="hidden">
                    <input type="submit" id="submitForm" $disabled name="eventSubmit_doRegistermr" value="Queue Job" />
                </div>
            </form>
        </div>
    </div>
    <div class="footer" style="background-color: rgb(240, 240, 240); border-color: rgb(224, 224, 224);">
        <!-- multi-panel controls are placed in footer -->
        <div class="inner">
            <span class="buttons">
                <a class="cancel close textlink" href="javascript:self.close()">Cancel</a>
                <a id="button-advance" class="default button panel-toggle" href="javascript:">Submit Pipeline</a>
            </span>
        </div>
    </div>
</div>
