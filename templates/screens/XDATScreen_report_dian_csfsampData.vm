## XDATScreen_dian_csfsampData.vm
## Modified: 1/17/2013

$page.setTitle("CSFSAMP")
$page.setLinkColor($ui.alink)
$page.setVlinkColor($ui.vlink)

#if ($data.getParameters().getString("popup"))
   #set ($popup = $data.getParameters().getString("popup") )
   #set ($popup = "false")
#end

#if(!$project)
   #set($project=$om.getProject())
#end

#macro (createTableRow $e $header $options $keys)
   #set($elm = "")  ## Clear out variable
   #set($elm = $item.getStringProperty($e))
   #dian_createTableRow($elm $header $options $keys)
#end

#macro (createSimpleTableRow $e $header)
   #createTableRow($elm $header "" "")
#end

<style>
table.uds_form {
   border-collapse: collapse;
   border-spacing:0px;
   border: 1px solid #000;
}
table.uds_form td,table.uds_form th{
   vertical-align:top;
   text-align:left;   
   border-top:1px solid #000;
   border-bottom:1px solid #000;
}
h1.sectionHeader{ font-size: 13px; }
</style>

<table width="100%">
   <tr>
      <td>#parse($turbineUtils.getTemplateName("_report",$om.getXSIType(),$project))</td>
      <td valign="top" align="right">#elementActionsBox($element $search_field $search_value $data.getSession().getAttribute("user") $item)</td>
   </tr>
   <tr>
      <td colspan="2">
      <table class="uds_form"  width="700px">
         ## Create the CSF for Biomarker Core table row
         #set ( $KEYS = ["0","1"] )
         #set ( $OPTIONS = ["No","Yes"] )
         #createTableRow("CSFCOLL" "CSF for Biomarker Core - Sample Collected?" $OPTIONS $KEYS )
         
         ## Create CSF collector inital table row
         ##createSimpleTableRow("CSFINIT" "CSF collector initials")
		 <tr>
		 <th width="400px">CSF collector initials</th>
			#set ($CSFINIT = $item.getStringProperty("CSFINIT"))
				<td>
				$!CSFINIT
				</td>
		 </tr>
         ## Create Date of LP Table row
         ##createSimpleTableRow("CSFDATE" "Date of LP")
		 <tr>
		 <th width="400px">Date of LP</th>
		 #set ($CSFDATE = $item.getStringProperty("CSFDATE"))
         	<td>
				$!CSFDATE
			</td>
		 </tr>
         ## Create Overnight fast table row
         #set ( $OPTIONS = ["No","Yes"] )
         #createTableRow("CSFFAST" "Overnight fast (at least 8 hours)" $OPTIONS $KEYS )
      </table><br/>
      <h1 class="sectionHeader">Pre-procedure Vital Signs</h1>
      <table class="uds_form"  width="700px">
         ## Create pre procedure temerature table row
         ##createSimpleTableRow("PRETEMP" "Temperature")
		  <tr>
		 <th width="400px">Temperature</th>
		 #set ($PRETEMP = $item.getStringProperty("PRETEMP"))
         	<td>
				$!PRETEMP
			</td>
		 </tr>
         
         ## Create the Pre procedure temerature unit table row
         #set ( $KEYS = ["1","2"] )
         #set ( $OPTIONS = ["Celsius","Farenheit"] )
         #createTableRow("PRETMPUNT" "Temperature Units" $OPTIONS $KEYS )
         
         ## Create pre procedure blood pressure systolic table row
         ##createSimpleTableRow("PRESYS" "Blood Pressure - Systolic")
		  <tr>
		 <th width="400px">Blood Pressure - Systolic</th>
		 #set ($PRESYS = $item.getStringProperty("PRESYS"))
         	<td>
				$!PRESYS
			</td>
		 </tr>
         
         ## Create the pre procedure blood pressure diastolic table row
         ##createSimpleTableRow("PREDIA" "Blood Pressure - Diastolic")
		 		  <tr>
		 <th width="400px">Blood Pressure - Diastolic</th>
		 #set ($PREDIA = $item.getStringProperty("PREDIA"))
         	<td>
				$!PREDIA
			</td>
		 </tr>
        
         ## Create the pre procedure seated pulse rate table row
         ##createSimpleTableRow("PREPULSE" "Seated Pulse Rate")
         <tr>
		 <th width="400px">Seated Pulse Rate</th>
		 #set ($PREPULSE = $item.getStringProperty("PREPULSE"))
         	<td>
				$!PREPULSE
			</td>
		 </tr>
		 
         ## Create the pre procedure respirations table row
         ##createSimpleTableRow("PRERESP" "Respirations")
         <tr>
		 <th width="400px">Respirations</th>
		 #set ($PRERESP = $item.getStringProperty("PRERESP"))
         	<td>
				$!PRERESP
			</td>
		 </tr>		 
        
         ## Create the spinal needle table row
         #set ( $KEYS = ["1","2","3"] )
         #set ( $OPTIONS = ["22g","24g","Other"] )
         #createTableRow("NEEDLE" "Spinal Needle" $OPTIONS $KEYS )
         ##createSimpleTableRow("NEEDLEOTH" "If other, specify")
         <tr>
		 <th width="400px">If other, specify</th>
		 #set ($NEEDLEOTH = $item.getStringProperty("NEEDLEOTH"))
         	<td>
				$!NEEDLEOTH
			</td>
		 </tr>	
        
         ## Create the collection method table row
         #set ( $KEYS = ["1","2"] )
         #set ( $OPTIONS = ["Gravity","Aspiration"] )
         #createTableRow("METHOD" "Collection Method" $OPTIONS $KEYS )
        
         ## Create the LP site table row
         #set ( $KEYS = ["1","2","3","4"] )
         #set ( $OPTIONS = ["L3-L4 inter space","L2-L3 inter space","L4-L5 inter space","Other"] )
         #createTableRow("LPSITE" "LP was performed at the:" $OPTIONS $KEYS )
         ##createSimpleTableRow("LPSITEOTH" "If other, specify")
         <tr>
		 <th width="400px">If other, specify</th>
		 #set ($LPSITEOTH = $item.getStringProperty("LPSITEOTH"))
         	<td>
				$!LPSITEOTH
			</td>
		 </tr>	
        
         ## Create the patient position table row
         #set ( $KEYS = ["1","2","3"] )
         #set ( $OPTIONS = ["Sitting, leaned over (preferred)","Lying, curled up on side"] )
         #createTableRow("POSITION2" "Patient Position:" $OPTIONS $KEYS )
        
         ## Create the table collection tube table row
         #set ( $KEYS = ["1","2"] )
         #set ( $OPTIONS = ["Confirm ONLY POLYPROPYLENE tube used for collection","Check if protocol violated and polystyrene tube used"] )
         #createTableRow("TUBECOLL" "Collection Tube" $OPTIONS $KEYS )
        
         ## Create the shipping tube table row
         #createTableRow("TUBESHIP" "Shipping Tube" $OPTIONS $KEYS )

         ## Create the time in polystyrene tube table row
         ##createSimpleTableRow("STYRTIME" "If collected in polystyrene and shipped in polypropylene, time in polystyrene collection tube:")
         <tr>
		 <th width="400px">If collected in polystyrene and shipped in polypropylene, time in polystyrene collection tube:</th>
		 #set ($STYRTIME = $item.getStringProperty("STYRTIME"))
         	<td>
				$!STYRTIME
			</td>
		 </tr>	
   
         ## Create the Time CSF collected table row
         ##createSimpleTableRow("CSFTIME" "Time CSF collected")
         <tr>
		 <th width="400px">Time CSF collected</th>
		 #set ($CSFTIME = $item.getStringProperty("CSFTIME"))
         	<td>
				$!CSFTIME
			</td>
		 </tr>
        
         ## Create the CSF Transfer Time table row
         ##createSimpleTableRow("CSFTRANS" "CSF Transfer Time")
         <tr>
		 <th width="400px">CSF Transfer Time</th>
		 #set ($CSFTRANS = $item.getStringProperty("CSFTRANS"))
         	<td>
				$!CSFTRANS
			</td>
		 </tr>
        
         ## Create the volume CSF collected table row
         ##createSimpleTableRow("VOLCOLL" "Volume CSF collected")
         <tr>
		 <th width="400px">Volume CSF collected</th>
		 #set ($VOLCOLL = $item.getStringProperty("VOLCOLL"))
         	<td>
				$!VOLCOLL
			</td>
		 </tr>
         
         ## Create the volume CSF transferred table row
         ##createSimpleTableRow("VOLTRANS" "Volume CSF transferred to final transfer tube")
         <tr>
		 <th width="400px">Volume CSF transferred to final transfer tube</th>
		 #set ($VOLTRANS = $item.getStringProperty("VOLTRANS"))
         	<td>
				$!VOLTRANS
			</td>
		 </tr>
         
         ## Create the volume CSF sent to local lab table row
         #set ( $KEYS = ["0","1"] )
         #set ( $OPTIONS = ["No","Yes"] )
         #createTableRow("LOCLAB" "CSF sent to local lab for protein, glucose, and white and red cell counts" $OPTIONS $KEYS )
         
         ## Create the time frozen table row
         ##createSimpleTableRow("TIMEFROZ" "Time put on dry ice/frozen")
         <tr>
		 <th width="400px">Time put on dry ice/frozen</th>
		 #set ($TIMEFROZ = $item.getStringProperty("TIMEFROZ"))
         	<td>
				$!TIMEFROZ
			</td>
		 </tr>
         
         ## Create the additional procedures table row
         ##createSimpleTableRow("NOTES" "Additional procedures/notes/dates:")
         <tr>
		 <th width="400px">Additional procedures/notes/dates:</th>
		 #set ($NOTES = $item.getStringProperty("NOTES"))
         	<td>
				$!NOTES
			</td>
		 </tr>
        
         ## Create the Shipping table row
         #set ( $KEYS = ["1","2","3"] )
         #set ( $OPTIONS = ["Shipped day of visit (US sites ONLY)","Not shipped day of visit (US sites ONLY)","Frozen for batch shipping (International sites ONLY)"] )
         #createTableRow("CSFSHIP" "Shipping" $OPTIONS $KEYS )
        
         ## Create the date shipped table row
         ##createSimpleTableRow("CSFSHDT" "Date shipped")
         <tr>
		 <th width="400px">Date shipped</th>
		 #set ($CSFSHDT = $item.getStringProperty("CSFSHDT"))
         	<td>
				$!CSFSHDT
			</td>
		 </tr>
   
         ## Create the tracking number table row
         ##createSimpleTableRow("CSFSHNUM" "Tracking Number")
         <tr>
		 <th width="400px">Tracking Number</th>
		 #set ($CSFSHNUM = $item.getStringProperty("CSFSHNUM"))
         	<td>
				$!CSFSHNUM
			</td>
		 </tr>
	
         ## Create the date genetics shipment received table row
         ##createSimpleTableRow("CSFDATERVD" "Date the genetics shipment was received")	
         <tr>
		 <th width="400px">Date the genetics shipment was received</th>
		 #set ($CSFDATERVD = $item.getStringProperty("CSFDATERVD"))
         	<td>
				$!CSFDATERVD
			</td>
		 </tr>
		 
         ## Create the Volume of genetics blood received table row
         ##createSimpleTableRow("CSFVOLRVD" "Volume of genetics blood received")	
         <tr>
		 <th width="400px">Volume of genetics blood received</th>
		 #set ($CSFVOLRVD = $item.getStringProperty("CSFVOLRVD"))
         	<td>
				$!CSFVOLRVD
			</td>
		 </tr>
		 
         ## Create the Number of vials of genetics blood received table row
         ##createSimpleTableRow("CSFVIALSRVD" "Number of vials of genetics blood received")	
         <tr>
		 <th width="400px">Number of vials of genetics blood received</th>
		 #set ($CSFVIALSRVD = $item.getStringProperty("CSFVIALSRVD"))
         	<td>
				$!CSFVIALSRVD
			</td>
		 </tr>		 
		 
      </table><br/>
      <h1 class="sectionHeader">Vital Signs Immediately Post Procedure<br/> (Participant should be lying down for all post-procedure vitals)</h1>
      <table class="uds_form"  width="700px">
         ## Create the post procedure patient position
         #set ( $KEYS = ["1","2"] )
         #set ( $OPTIONS = ["Lying Down","Seated (not recommended immediately post-procedure)"] )
         #createTableRow("POSITION3" "Patient Position - Vitals" $OPTIONS $KEYS )
         
         ## Create the post procedure patient temerature
         ##createSimpleTableRow("POSTTEMP" "Temperature")
         <tr>
		 <th width="400px">Temperature</th>
		 #set ($POSTTEMP = $item.getStringProperty("POSTTEMP"))
         	<td>
				$!POSTTEMP
			</td>
		 </tr>	
         
         ## Create the post procedure patient temp unit
         #set ( $KEYS = ["1","2"] )
         #set ( $OPTIONS = ["Celsius","Farenheit"] )
         #createTableRow("POSTTMPU" "Temperature Units" $OPTIONS $KEYS )
         
         ## Create the post procedure patient blood pressure systolic table row
         ##createSimpleTableRow("POSTSYS" "Blood Pressure - Systolic")
         <tr>
		 <th width="400px">Blood Pressure - Systolic</th>
		 #set ($POSTSYS = $item.getStringProperty("POSTSYS"))
         	<td>
				$!POSTSYS
			</td>
		 </tr>	
         
         ## Create the post procedure patient blood pressure diastolic table row
         ##createSimpleTableRow("POSTDIA" "Blood Pressure - Diastolic")
         <tr>
		 <th width="400px">Blood Pressure - Diastolic</th>
		 #set ($POSTDIA = $item.getStringProperty("POSTDIA"))
         	<td>
				$!POSTDIA
			</td>
		 </tr>
         
         ## Create the post procedure patient pulse table row
         ##createSimpleTableRow("POSTPULSE" "Pulse Rate")
         <tr>
		 <th width="400px">Pulse Rate</th>
		 #set ($POSTPULSE = $item.getStringProperty("POSTPULSE"))
         	<td>
				$!POSTPULSE
			</td>
		 </tr>
         
         ## Create the post procedure respierations table row
         ##createSimpleTableRow("POSTRESP" "Respirations")
         <tr>
		 <th width="400px">Respirations</th>
		 #set ($POSTRESP = $item.getStringProperty("POSTRESP"))
         	<td>
				$!POSTRESP
			</td>
		 </tr>
      </table><br/>
	  
      <h1 class="sectionHeader">Vital Signs 1 Hour Post Procedure</h1>
      <table class="uds_form"  width="700px">
         ## Create the 1 hour post patient position table row
         #set ( $KEYS = ["1","2"] )
         #set ( $OPTIONS = ["Lying Down","Seated"] )
         ##createTableRow("POSITION4" "Patient Position - Vitals" $OPTIONS $KEYS )
   
         ## Create the 1 hour post patient temperature table row
         ##createSimpleTableRow("HRTEMP" "Temperature")
         <tr>
		 <th width="400px">Temperature</th>
		 #set ($HRTEMP = $item.getStringProperty("HRTEMP"))
         	<td>
				$!HRTEMP
			</td>
		 </tr>
         
         ## Create the 1 hour post patient temp units table row
         #set ( $KEYS = ["1","2"] )
         #set ( $OPTIONS = ["Celsius","Farenheit"] )
         #createTableRow("HRTMPU" "Temperature Units" $OPTIONS $KEYS )

         ## Create the 1 hour post patient blood pressure systolic table row
         ##createSimpleTableRow("HRSYS" "Blood Pressure - Systolic")
         <tr>
		 <th width="400px">Blood Pressure - Systolic</th>
		 #set ($HRSYS = $item.getStringProperty("HRSYS"))
         	<td>
				$!HRSYS
			</td>
		 </tr>
         
         ## Create the 1 hour post patient blood pressure diastolic table row
         ##createSimpleTableRow("HRDIA" "Blood Pressure - Diastolic")
         <tr>
		 <th width="400px">Blood Pressure - Diastolic</th>
		 #set ($HRDIA = $item.getStringProperty("HRDIA"))
         	<td>
				$!HRDIA
			</td>
		 </tr>
         
         ## Create the 1 hour post pulse rate table row
         ##createSimpleTableRow("HRPULSE" "Pulse Rate")
         <tr>
		 <th width="400px">Pulse Rate</th>
		 #set ($HRPULSE = $item.getStringProperty("HRPULSE"))
         	<td>
				$!HRPULSE
			</td>
		 </tr>
         
         ## Create the 1 hour post resperations table row
         ##createSimpleTableRow("HRRESP" "Respirations")
         <tr>
		 <th width="400px">Respirations</th>
		 #set ($HRRESP = $item.getStringProperty("HRRESP"))
         	<td>
				$!HRRESP
			</td>
		 </tr>
         
         ## Create the Immediate complications table row
         #set ( $KEYS = ["1","2","3"] )
         #set ( $OPTIONS = ["None","Headache","Other"] )
         #createTableRow("COMP" "Immediate Complications" $OPTIONS $KEYS )
         ##createSimpleTableRow("COMPOTH" "Other, specify")
         <tr>
		 <th width="400px">Other, specify</th>
		 #set ($COMPOTH = $item.getStringProperty("COMPOTH"))
         	<td>
				$!COMPOTH
			</td>
		 </tr>
      </table>
      </td>
   </tr>
</table>